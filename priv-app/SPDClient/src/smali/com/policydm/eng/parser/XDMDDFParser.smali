.class public Lcom/policydm/eng/parser/XDMDDFParser;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "XDMDDFParser.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;
.implements Lcom/policydm/eng/core/XDMXml;
.implements Lcom/policydm/interfaces/XDMDefInterface;
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field public static CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

.field public static XmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

.field public static bNodeChangeMode:Z

.field public static gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

.field public static gTndsWbxmlHeaderInfo:[C

.field public static g_om:Lcom/policydm/eng/core/XDMOmTree;

.field public static g_szDmManagementObjectIdPath:[Ljava/lang/String;

.field public static g_szDmManagementObjectIdType:[Ljava/lang/String;

.field public static g_szDmXmlOmaTags:[Ljava/lang/String;

.field public static g_szDmXmlTagString:[Ljava/lang/String;

.field public static g_szNewAccPath:Ljava/lang/String;

.field public static g_szTndsTokenStr:[Ljava/lang/String;

.field public static gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;


# instance fields
.field public gTagCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/16 v3, 0x2f

    .line 101
    const/16 v0, 0x3d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 102
    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 103
    const-string v2, ""

    aput-object v2, v0, v1

    .line 104
    const-string v1, ""

    aput-object v1, v0, v4

    .line 105
    const-string v1, ""

    aput-object v1, v0, v5

    .line 106
    const-string v1, ""

    aput-object v1, v0, v6

    .line 107
    const-string v1, "AccessType"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 108
    const-string v2, "ACL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 109
    const-string v2, "Add"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 110
    const-string v2, "b64"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 111
    const-string v2, "bin"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 112
    const-string v2, "bool"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 113
    const-string v2, "chr"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 114
    const-string v2, "CaseSense"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 115
    const-string v2, "CIS"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 116
    const-string v2, "Copy"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 117
    const-string v2, "CS"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 118
    const-string v2, "data"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 119
    const-string v2, "DDFName"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 120
    const-string v2, "DefaultValue"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 121
    const-string v2, "Delete"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 122
    const-string v2, "Description"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 123
    const-string v2, "DFFormat"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 124
    const-string v2, "DFProperties"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 125
    const-string v2, "DFTitle"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 126
    const-string v2, "DFType"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 127
    const-string v2, "Dynamic"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 128
    const-string v2, "Exec"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 129
    const-string v2, "float"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 130
    const-string v2, "Format"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 131
    const-string v2, "Get"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 132
    const-string v2, "int"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 133
    const-string v2, "Man"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 134
    const-string v2, "MgmtTree"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 135
    const-string v2, "MIME"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 136
    const-string v2, "Mod"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 137
    const-string v2, "Name"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 138
    const-string v2, "Node"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 139
    const-string v2, "node"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 140
    const-string v2, "NodeName"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 141
    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 142
    const-string v2, "Occurrence"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 143
    const-string v2, "One"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 144
    const-string v2, "OneOrMore"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 145
    const-string v2, "OneOrN"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 146
    const-string v2, "Path"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 147
    const-string v2, "Permanent"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 148
    const-string v2, "Replace"

    aput-object v2, v0, v1

    .line 149
    const-string v1, "RTProperties"

    aput-object v1, v0, v3

    const/16 v1, 0x30

    .line 150
    const-string v2, "Scope"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    .line 151
    const-string v2, "Size"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    .line 152
    const-string v2, "time"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 153
    const-string v2, "Title"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    .line 154
    const-string v2, "TStamp"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    .line 155
    const-string v2, "Type"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 156
    const-string v2, "Value"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 157
    const-string v2, "VerDTD"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    .line 158
    const-string v2, "VerNo"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    .line 159
    const-string v2, "xml"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    .line 160
    const-string v2, "ZeroOrMore"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    .line 161
    const-string v2, "ZeroOrN"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    .line 162
    const-string v2, "ZeroOrOne"

    aput-object v2, v0, v1

    .line 101
    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    .line 164
    const/16 v0, 0x2a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 165
    const-string v2, "<MgmtTree>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 166
    const-string v2, "</MgmtTree>\n"

    aput-object v2, v0, v1

    .line 167
    const-string v1, "<VerDTD>"

    aput-object v1, v0, v4

    .line 168
    const-string v1, "</VerDTD>\n"

    aput-object v1, v0, v5

    .line 169
    const-string v1, "<Node>"

    aput-object v1, v0, v6

    .line 170
    const-string v1, "</Node>\n"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 171
    const-string v2, "<NodeName>"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 172
    const-string v2, "</NodeName>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 173
    const-string v2, "<Path>"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 174
    const-string v2, "</Path>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 175
    const-string v2, "<Value>"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 176
    const-string v2, "</Value>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 177
    const-string v2, "<RTProperties>"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 178
    const-string v2, "</RTProperties>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 179
    const-string v2, "<ACL>"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 180
    const-string v2, "</ACL>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 181
    const-string v2, "<Format>"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 182
    const-string v2, "</Format>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 183
    const-string v2, "<Type>"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 184
    const-string v2, "</Type>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 185
    const-string v2, "<Add>"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 186
    const-string v2, "</Add>"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 187
    const-string v2, "<Get>"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 188
    const-string v2, "</Get>"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 189
    const-string v2, "<Replace>"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 190
    const-string v2, "</Replace>"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 191
    const-string v2, "<Delete>"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 192
    const-string v2, "</Delete>"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 193
    const-string v2, "<Exec>"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 194
    const-string v2, "</Exec>"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 195
    const-string v2, "<AccessType>"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 196
    const-string v2, "</AccessType>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 197
    const-string v2, "<![CDATA["

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 198
    const-string v2, "]]>"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 199
    const-string v2, "<SyncML>"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 200
    const-string v2, "</SyncML>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 201
    const-string v2, "<ResultCode>"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 202
    const-string v2, "</ResultCode>"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 203
    const-string v2, "<Identifier>"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 204
    const-string v2, "</Identifier>"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 205
    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 206
    const-string v2, ""

    aput-object v2, v0, v1

    .line 164
    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    .line 208
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x1

    .line 209
    const-string v2, "./SyncML/DMAcc"

    aput-object v2, v0, v1

    const-string v1, "./SyncML/DMAcc"

    aput-object v1, v0, v4

    const-string v1, "./SyncML/DMAcc"

    aput-object v1, v0, v5

    const-string v1, "./SyncML/DMAcc"

    aput-object v1, v0, v6

    const-string v1, "./SyncML/DMAcc"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    const-string v2, "./SyncML/DMAcc"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "./SyncML/DMAcc"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "./DevInfo"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "./DevDetail"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "./Inbox"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "./FUMO"

    aput-object v2, v0, v1

    .line 208
    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmManagementObjectIdPath:[Ljava/lang/String;

    .line 211
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x1

    .line 213
    const-string v2, "org.openmobilealliance/1.0/w1"

    aput-object v2, v0, v1

    .line 214
    const-string v1, "org.openmobilealliance/1.0/w2"

    aput-object v1, v0, v4

    .line 215
    const-string v1, "org.openmobilealliance/1.0/w3"

    aput-object v1, v0, v5

    .line 216
    const-string v1, "org.openmobilealliance/1.0/w4"

    aput-object v1, v0, v6

    .line 217
    const-string v1, "org.openmobilealliance/1.0/w5"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 218
    const-string v2, "org.openmobilealliance/1.0/w6"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 219
    const-string v2, "org.openmobilealliance/1.0/w7"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 220
    const-string v2, "org.openmobilealliance.dm/1.0/DevInfo"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 221
    const-string v2, "org.openmobilealliance.dm/1.0/DevDetail"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 222
    const-string v2, "org.openmobilealliance.dm/1.0/Inbox"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 223
    const-string v2, "org.openmobilealliance/1.0/FirmwareUpdateManagementObject"

    aput-object v2, v0, v1

    .line 211
    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmManagementObjectIdType:[Ljava/lang/String;

    .line 226
    const/16 v0, 0x38

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 227
    const-string v2, "AccessType"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 228
    const-string v2, "ACL"

    aput-object v2, v0, v1

    .line 229
    const-string v1, "Add"

    aput-object v1, v0, v4

    .line 230
    const-string v1, "b64"

    aput-object v1, v0, v5

    .line 231
    const-string v1, "bin"

    aput-object v1, v0, v6

    .line 232
    const-string v1, "bool"

    aput-object v1, v0, v7

    const/4 v1, 0x6

    .line 233
    const-string v2, "chr"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 234
    const-string v2, "CaseSense"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 235
    const-string v2, "CIS"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 236
    const-string v2, "Copy"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 237
    const-string v2, "CS"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 238
    const-string v2, "date"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 239
    const-string v2, "DDFName"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 240
    const-string v2, "DefaultValue"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 241
    const-string v2, "Delete"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 242
    const-string v2, "Description"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 243
    const-string v2, "DFFormat"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 244
    const-string v2, "DFProperties"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 245
    const-string v2, "DFTitle"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 246
    const-string v2, "DFType"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 247
    const-string v2, "Dynamic"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 248
    const-string v2, "Exec"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 249
    const-string v2, "float"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 250
    const-string v2, "Format"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 251
    const-string v2, "Get"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 252
    const-string v2, "int"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 253
    const-string v2, "Man"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 254
    const-string v2, "MgmtTree"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 255
    const-string v2, "MIME"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 256
    const-string v2, "Mod"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 257
    const-string v2, "Name"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 258
    const-string v2, "Node"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 259
    const-string v2, "node"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 260
    const-string v2, "NodeName"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 261
    const-string v2, "null"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 262
    const-string v2, "Occurrence"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 263
    const-string v2, "One"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 264
    const-string v2, "OneOrMore"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 265
    const-string v2, "OneOrN"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 266
    const-string v2, "Path"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 267
    const-string v2, "Permanent"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 268
    const-string v2, "Replace"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 269
    const-string v2, "RTProperties"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 270
    const-string v2, "Scope"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 271
    const-string v2, "Size"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 272
    const-string v2, "time"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 273
    const-string v2, "Title"

    aput-object v2, v0, v1

    .line 274
    const-string v1, "TStamp"

    aput-object v1, v0, v3

    const/16 v1, 0x30

    .line 275
    const-string v2, "Type"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    .line 276
    const-string v2, "Value"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    .line 277
    const-string v2, "VerDTD"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    .line 278
    const-string v2, "VerNo"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    .line 279
    const-string v2, "xml"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    .line 280
    const-string v2, "ZerOrMore"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    .line 281
    const-string v2, "ZeroOrN"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    .line 282
    const-string v2, "ZeroOrOne"

    aput-object v2, v0, v1

    .line 226
    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szTndsTokenStr:[Ljava/lang/String;

    .line 284
    const/16 v0, 0x1f

    new-array v0, v0, [C

    const/4 v1, 0x0

    .line 285
    aput-char v4, v0, v1

    .line 287
    const/16 v1, 0x6a

    aput-char v1, v0, v5

    .line 288
    const/16 v1, 0x1a

    aput-char v1, v0, v6

    .line 289
    const/16 v1, 0x2d

    aput-char v1, v0, v7

    const/4 v1, 0x6

    aput-char v3, v0, v1

    const/4 v1, 0x7

    aput-char v3, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x4f

    aput-char v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x4d

    aput-char v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x41

    aput-char v2, v0, v1

    const/16 v1, 0xb

    aput-char v3, v0, v1

    const/16 v1, 0xc

    aput-char v3, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x54

    aput-char v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x2d

    aput-char v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x4d

    aput-char v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x2d

    aput-char v2, v0, v1

    const/16 v1, 0x14

    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0x15

    const/16 v2, 0x44

    aput-char v2, v0, v1

    const/16 v1, 0x16

    const/16 v2, 0x46

    aput-char v2, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x20

    aput-char v2, v0, v1

    const/16 v1, 0x18

    const/16 v2, 0x31

    aput-char v2, v0, v1

    const/16 v1, 0x19

    const/16 v2, 0x2e

    aput-char v2, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0x32

    aput-char v2, v0, v1

    const/16 v1, 0x1b

    aput-char v3, v0, v1

    const/16 v1, 0x1c

    aput-char v3, v0, v1

    const/16 v1, 0x1d

    const/16 v2, 0x45

    aput-char v2, v0, v1

    const/16 v1, 0x1e

    const/16 v2, 0x4e

    aput-char v2, v0, v1

    .line 284
    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsWbxmlHeaderInfo:[C

    .line 290
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 94
    sput-boolean v1, Lcom/policydm/eng/parser/XDMDDFParser;->bNodeChangeMode:Z

    .line 95
    new-instance v0, Lcom/policydm/eng/parser/XDM_DM_Tree;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDM_DM_Tree;-><init>()V

    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    .line 96
    iput v1, p0, Lcom/policydm/eng/parser/XDMDDFParser;->gTagCode:I

    .line 97
    new-instance v0, Lcom/policydm/eng/core/XDMOmTree;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMOmTree;-><init>()V

    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    .line 98
    const/4 v0, 0x0

    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szNewAccPath:Ljava/lang/String;

    .line 99
    return-void
.end method

.method private static OMSETPATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V
    .locals 6
    .param p0, "g_om"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "aclValue"    # I
    .param p3, "scope"    # I

    .prologue
    const/4 v2, 0x0

    .line 294
    invoke-static {p0, p1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 296
    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 297
    invoke-static {p0, p1, p2, p3}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 299
    :cond_0
    return-void
.end method

.method public static xdmAgentVerifyNewAccount(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "omt"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szAccName"    # Ljava/lang/String;

    .prologue
    .line 865
    const-string v2, ""

    .line 866
    .local v2, "szTmpAccPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 867
    .local v1, "node":Lcom/policydm/eng/core/XDMVnode;
    const/4 v0, 0x0

    .line 869
    .local v0, "bRet":Z
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 871
    move-object v2, p1

    .line 872
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 873
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 874
    invoke-virtual {v2, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 876
    :cond_0
    invoke-static {p0, v2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 877
    if-nez v1, :cond_2

    .line 879
    sput-object v2, Lcom/policydm/eng/parser/XDMDDFParser;->g_szNewAccPath:Ljava/lang/String;

    .line 880
    const/4 v0, 0x1

    .line 890
    :cond_1
    :goto_0
    return v0

    .line 884
    :cond_2
    const/4 v3, 0x0

    sput-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->g_szNewAccPath:Ljava/lang/String;

    .line 885
    const/4 v2, 0x0

    .line 886
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmDDFCheckInbox(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "szType"    # Ljava/lang/String;

    .prologue
    .line 804
    const/4 v1, 0x0

    .line 805
    .local v1, "szId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 806
    .local v0, "i":I
    const/4 v3, 0x0

    .line 807
    .local v3, "szPath":Ljava/lang/String;
    const/4 v2, 0x0

    .line 809
    .local v2, "szOut":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 811
    const/4 v4, 0x0

    .line 839
    :goto_0
    return-object v4

    .line 814
    :cond_0
    const/4 v0, 0x1

    :goto_1
    const/16 v4, 0xc

    if-lt v0, v4, :cond_1

    :goto_2
    move-object v4, v2

    .line 839
    goto :goto_0

    .line 816
    :cond_1
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFGetMOType(I)Ljava/lang/String;

    move-result-object v1

    .line 817
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 814
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 822
    :cond_3
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 824
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFGetMOPath(I)Ljava/lang/String;

    move-result-object v3

    .line 825
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 827
    const/4 v2, 0x0

    .line 828
    move-object v2, v3

    .line 829
    goto :goto_2

    .line 833
    :cond_4
    const/4 v2, 0x0

    .line 834
    goto :goto_2
.end method

.method public static xdmDDFConvertAddTndsCodePage()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2336
    const/4 v2, 0x0

    .line 2338
    .local v2, "szWbxmlData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2341
    .local v0, "nTmp":C
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 2342
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 2344
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    .line 2346
    .local v3, "tmpWbxml":[B
    add-int/lit8 v1, v1, -0x1

    .line 2347
    aget-byte v4, v3, v1

    int-to-char v0, v4

    .line 2349
    invoke-virtual {v2, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 2351
    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2352
    add-int/lit8 v1, v1, 0x1

    .line 2354
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2355
    add-int/lit8 v1, v1, 0x1

    .line 2357
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2358
    add-int/lit8 v1, v1, 0x1

    .line 2360
    invoke-static {v1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2361
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2362
    return-void
.end method

.method public static xdmDDFConvertCheckElement(C)V
    .locals 2
    .param p0, "cHex"    # C

    .prologue
    .line 2094
    int-to-byte v0, p0

    packed-switch v0, :pswitch_data_0

    .line 2138
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "# ERROR # What? [value : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]!!! ###"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2141
    :goto_0
    :pswitch_1
    return-void

    .line 2114
    :pswitch_2
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFProcessConvertStringData()V

    goto :goto_0

    .line 2124
    :pswitch_3
    invoke-static {p0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFProcessConvertHexData(C)V

    goto :goto_0

    .line 2128
    :pswitch_4
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFConvertAddTndsCodePage()V

    goto :goto_0

    .line 2094
    nop

    :pswitch_data_0
    .packed-switch 0x45
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static xdmDDFConvertGetXMLTag(I)Ljava/lang/String;
    .locals 1
    .param p0, "nIndex"    # I

    .prologue
    .line 2089
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szTndsTokenStr:[Ljava/lang/String;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static xdmDDFConvertString2WbxmlHex(Ljava/lang/String;)C
    .locals 6
    .param p0, "szString"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 2061
    const/4 v0, 0x0

    .line 2062
    .local v0, "nIndex":I
    const/4 v1, 0x0

    .line 2065
    .local v1, "nStringLen":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 2067
    :goto_0
    const/16 v3, 0x38

    if-ne v0, v3, :cond_0

    .line 2078
    const-string v3, "SyncML"

    const-string v4, "SyncML"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p0, v5, v3, v5, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2080
    const/16 v3, 0x6d

    .line 2084
    :goto_1
    return v3

    .line 2069
    :cond_0
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFConvertGetXMLTag(I)Ljava/lang/String;

    move-result-object v2

    .line 2071
    .local v2, "szXmlTag":Ljava/lang/String;
    invoke-virtual {p0, v5, v2, v5, v1}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2073
    add-int/lit8 v3, v0, 0x5

    add-int/lit8 v3, v3, 0x40

    int-to-char v3, v3

    goto :goto_1

    .line 2075
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2083
    .end local v2    # "szXmlTag":Ljava/lang/String;
    :cond_2
    const-string v3, "# ERROR # Not Found String !!! ###"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 2084
    const v3, 0xffff

    goto :goto_1
.end method

.method public static xdmDDFCreateNode(Lcom/policydm/eng/parser/XDM_DM_Tree;Ljava/lang/String;)Z
    .locals 7
    .param p0, "pTree"    # Lcom/policydm/eng/parser/XDM_DM_Tree;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 1134
    if-nez p0, :cond_1

    .line 1189
    :cond_0
    :goto_0
    return v3

    .line 1138
    :cond_1
    const/4 v0, 0x0

    .line 1139
    .local v0, "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DM_Tree;->childlist:Lcom/policydm/eng/core/XDMLinkedList;

    .line 1141
    .local v1, "childlist":Lcom/policydm/eng/core/XDMLinkedList;
    const/4 v4, 0x0

    .line 1144
    .local v4, "szPathTemp":Ljava/lang/String;
    if-nez v1, :cond_2

    move v3, v5

    .line 1146
    goto :goto_0

    .line 1149
    :cond_2
    invoke-static {v1, v3}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V

    .line 1151
    :goto_1
    if-nez p0, :cond_3

    move v3, v5

    .line 1189
    goto :goto_0

    .line 1153
    :cond_3
    invoke-static {v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "pTree":Lcom/policydm/eng/parser/XDM_DM_Tree;
    check-cast p0, Lcom/policydm/eng/parser/XDM_DM_Tree;

    .line 1155
    .restart local p0    # "pTree":Lcom/policydm/eng/parser/XDM_DM_Tree;
    if-nez p0, :cond_4

    move v3, v5

    .line 1157
    goto :goto_0

    .line 1160
    :cond_4
    iget-object v0, p0, Lcom/policydm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .end local v0    # "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    check-cast v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    .line 1162
    .restart local v0    # "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFXmlTagCode(Ljava/lang/String;)I

    move-result v2

    .line 1163
    .local v2, "nTagCode":I
    packed-switch v2, :pswitch_data_0

    .line 1183
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1184
    sget-object v6, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 1166
    :pswitch_0
    iput-object p1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 1167
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateNodeToOM(Lcom/policydm/eng/parser/XDM_DDFXmlElement;)Z

    move-result v3

    .line 1168
    .local v3, "ret":Z
    if-eqz v3, :cond_0

    .line 1173
    const-string v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1174
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1175
    invoke-static {p0, p1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateNode(Lcom/policydm/eng/parser/XDM_DM_Tree;Ljava/lang/String;)Z

    .line 1178
    const/16 v6, 0x2f

    invoke-static {p1, v6}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrrchr(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v4

    .line 1179
    move-object p1, v4

    .line 1180
    goto :goto_1

    .line 1163
    nop

    :pswitch_data_0
    .packed-switch 0x24
        :pswitch_0
    .end packed-switch
.end method

.method public static xdmDDFCreateNodeToOM(Lcom/policydm/eng/parser/XDM_DDFXmlElement;)Z
    .locals 15
    .param p0, "element"    # Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    .prologue
    const/16 v12, 0xc

    const/4 v10, 0x1

    const/4 v0, 0x0

    .line 945
    iget v6, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->scope:I

    .line 946
    .local v6, "scope":I
    iget v7, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    .line 947
    .local v7, "format":I
    iget v5, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    .line 948
    .local v5, "aclValue":I
    iget-object v2, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    .line 949
    .local v2, "szData":Ljava/lang/String;
    const/4 v3, 0x0

    .line 950
    .local v3, "nLen":I
    const-string v1, ""

    .line 951
    .local v1, "szNodeName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 952
    .local v8, "bRet":Z
    const-string v9, ""

    .line 954
    .local v9, "szTmpBuf":Ljava/lang/String;
    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 956
    const-string v4, "Not exist nodename."

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1045
    :goto_0
    return v0

    .line 962
    :cond_0
    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    const-string v11, "."

    invoke-static {v4, v11}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 964
    const/4 v8, 0x0

    .line 971
    :goto_1
    if-nez v8, :cond_1

    .line 973
    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 975
    const-string v4, "."

    invoke-virtual {v9, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 976
    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-virtual {v9, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 977
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 979
    iput-object v9, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 988
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    iget-object v11, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v4, v11, v10}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetXNodePath(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 990
    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    const-string v11, "AAuthData"

    invoke-virtual {v4, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 992
    const/4 v7, 0x1

    .line 996
    :cond_2
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 997
    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 998
    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1000
    if-nez v5, :cond_3

    .line 1002
    const/16 v5, 0x1b

    .line 1005
    :cond_3
    if-nez v6, :cond_4

    .line 1007
    const/4 v6, 0x2

    .line 1010
    :cond_4
    if-ne v7, v12, :cond_5

    .line 1012
    const/4 v7, 0x6

    .line 1015
    :cond_5
    const/4 v4, 0x6

    if-eq v7, v4, :cond_6

    const/4 v4, 0x7

    if-eq v7, v4, :cond_6

    if-ne v7, v12, :cond_9

    .line 1017
    :cond_6
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFSetOMTree(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;III)V

    .line 1018
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    invoke-static {v0, v1, v5, v6}, Lcom/policydm/eng/parser/XDMDDFParser;->OMSETPATH(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1043
    :goto_3
    const/4 v1, 0x0

    move v0, v10

    .line 1045
    goto :goto_0

    .line 968
    :cond_7
    const/4 v8, 0x1

    goto :goto_1

    .line 983
    :cond_8
    const-string v4, "."

    iput-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    goto :goto_2

    .line 1022
    :cond_9
    const/4 v4, 0x2

    if-ne v7, v4, :cond_b

    .line 1024
    const/4 v3, 0x0

    .line 1035
    :goto_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ltz v4, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v11, v4

    const-wide/16 v13, 0x100

    cmp-long v4, v11, v13

    if-ltz v4, :cond_d

    .line 1037
    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "Size["

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "]. Fatal ERROR."

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1028
    :cond_b
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1030
    const-string v2, "null"

    .line 1031
    iput-object v2, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    .line 1033
    :cond_c
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    goto :goto_4

    .line 1040
    :cond_d
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    iget-object v4, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFSetOMTree(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;III)V

    goto :goto_3
.end method

.method public static xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/policydm/eng/core/XDMOmTree;)I
    .locals 8
    .param p0, "szData"    # Ljava/lang/String;
    .param p1, "nLen"    # I
    .param p2, "omt"    # Lcom/policydm/eng/core/XDMOmTree;

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1223
    const/4 v0, 0x0

    .line 1224
    .local v0, "ret":Z
    const/4 v2, 0x0

    .line 1226
    .local v2, "totalSize":I
    new-instance v3, Lcom/policydm/eng/parser/XDM_XMLStream;

    invoke-direct {v3}, Lcom/policydm/eng/parser/XDM_XMLStream;-><init>()V

    .line 1227
    .local v3, "xmlstream":Lcom/policydm/eng/parser/XDM_XMLStream;
    new-instance v4, Lcom/policydm/eng/parser/XDM_DM_Tree;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDM_DM_Tree;-><init>()V

    .line 1229
    .local v4, "xmltree":Lcom/policydm/eng/parser/XDM_DM_Tree;
    invoke-static {p0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFParseCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1231
    .local v1, "szTmpData":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1233
    invoke-static {p0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFParseCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/policydm/eng/parser/XDM_XMLStream;->m_szData:Ljava/lang/String;

    .line 1234
    iput v2, v3, Lcom/policydm/eng/parser/XDM_XMLStream;->size:I

    .line 1242
    :goto_0
    invoke-static {v3, v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFParsing(Lcom/policydm/eng/parser/XDM_XMLStream;Lcom/policydm/eng/parser/XDM_DM_Tree;)I

    move-result v6

    if-eqz v6, :cond_1

    .line 1244
    const-string v6, "Parsing Fail."

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1245
    sput-object v7, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    .line 1272
    :goto_1
    return v5

    .line 1238
    :cond_0
    iput-object p0, v3, Lcom/policydm/eng/parser/XDM_XMLStream;->m_szData:Ljava/lang/String;

    .line 1239
    iput p1, v3, Lcom/policydm/eng/parser/XDM_XMLStream;->size:I

    goto :goto_0

    .line 1249
    :cond_1
    if-nez p2, :cond_2

    .line 1251
    const-string v6, "OM is NULL."

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1252
    sput-object v7, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    goto :goto_1

    .line 1256
    :cond_2
    sput-object p2, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    .line 1258
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->XmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    .line 1260
    invoke-static {v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFXmlTagParsing(Lcom/policydm/eng/parser/XDM_DM_Tree;)Z

    move-result v0

    .line 1262
    if-nez v0, :cond_3

    .line 1264
    const-string v6, "Create Node Fail. Check the xml file."

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1265
    sput-object v7, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    goto :goto_1

    .line 1269
    :cond_3
    const/4 v0, 0x1

    .line 1270
    const-string v5, "Success."

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1272
    const/4 v5, 0x1

    goto :goto_1
.end method

.method public static xdmDDFCreateTNDSNodeFromFile(ILcom/policydm/eng/core/XDMOmTree;)I
    .locals 6
    .param p0, "nFileID"    # I
    .param p1, "omt"    # Lcom/policydm/eng/core/XDMOmTree;

    .prologue
    const/4 v5, 0x0

    .line 1196
    const/4 v3, 0x0

    .line 1197
    .local v3, "ret":I
    const/4 v2, 0x0

    .line 1198
    .local v2, "r":Z
    const/4 v0, 0x0

    .line 1199
    .local v0, "nLen":I
    const/4 v1, 0x0

    .line 1200
    .local v1, "pData":[B
    const/4 v4, 0x0

    .line 1202
    .local v4, "szData":Ljava/lang/String;
    invoke-static {p0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetFileSize(I)I

    move-result v0

    .line 1203
    if-gtz v0, :cond_1

    .line 1217
    :cond_0
    :goto_0
    return v5

    .line 1207
    :cond_1
    new-array v1, v0, [B

    .line 1208
    invoke-static {p0, v5, v0, v1}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpReadFile(III[B)Z

    move-result v2

    .line 1210
    if-eqz v2, :cond_0

    .line 1214
    new-instance v4, Ljava/lang/String;

    .end local v4    # "szData":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 1215
    .restart local v4    # "szData":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5, p1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateTNDSNode(Ljava/lang/String;ILcom/policydm/eng/core/XDMOmTree;)I

    move-result v3

    move v5, v3

    .line 1217
    goto :goto_0
.end method

.method public static xdmDDFGetMOPath(I)Ljava/lang/String;
    .locals 2
    .param p0, "nId"    # I

    .prologue
    .line 844
    if-lez p0, :cond_0

    const/16 v0, 0xc

    if-lt p0, v0, :cond_1

    .line 846
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "wrong nId. ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 847
    const/4 v0, 0x0

    .line 849
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmManagementObjectIdPath:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static xdmDDFGetMOType(I)Ljava/lang/String;
    .locals 2
    .param p0, "nId"    # I

    .prologue
    .line 854
    if-lez p0, :cond_0

    const/16 v0, 0xc

    if-lt p0, v0, :cond_1

    .line 856
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "wrong nId. ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 857
    const/4 v0, 0x0

    .line 860
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmManagementObjectIdType:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static xdmDDFParseCDATA(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1277
    const-string v3, ""

    .line 1278
    .local v3, "szRet":Ljava/lang/String;
    const-string v4, ""

    .line 1279
    .local v4, "szStart":Ljava/lang/String;
    const-string v1, ""

    .line 1280
    .local v1, "szEnd":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1281
    .local v0, "cDataLen":I
    sget-object v7, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v8, 0x21

    aget-object v2, v7, v8

    .line 1282
    .local v2, "szEndStr":Ljava/lang/String;
    sget-object v7, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlTagString:[Ljava/lang/String;

    const/16 v8, 0x20

    aget-object v5, v7, v8

    .line 1284
    .local v5, "szStartStr":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1311
    :cond_0
    :goto_0
    return-object v6

    .line 1289
    :cond_1
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {p0, v5, v7}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrncmp(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    if-nez v7, :cond_0

    .line 1294
    invoke-static {p0, v2}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrstr(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1296
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1301
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1302
    invoke-virtual {v4, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1304
    if-lez v0, :cond_0

    .line 1309
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    move-object v6, v3

    .line 1311
    goto :goto_0
.end method

.method public static xdmDDFParsing(Lcom/policydm/eng/parser/XDM_XMLStream;Lcom/policydm/eng/parser/XDM_DM_Tree;)I
    .locals 3
    .param p0, "stream"    # Lcom/policydm/eng/parser/XDM_XMLStream;
    .param p1, "xmltree"    # Lcom/policydm/eng/parser/XDM_DM_Tree;

    .prologue
    .line 666
    const/4 v0, 0x0

    .line 668
    .local v0, "wRC":I
    if-eqz p0, :cond_0

    iget-object v2, p0, Lcom/policydm/eng/parser/XDM_XMLStream;->m_szData:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p1, :cond_1

    .line 670
    :cond_0
    const/16 v0, 0xbb9

    move v1, v0

    .line 675
    .end local v0    # "wRC":I
    .local v1, "wRC":I
    :goto_0
    return v1

    .line 674
    .end local v1    # "wRC":I
    .restart local v0    # "wRC":I
    :cond_1
    sput-object p1, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    move v1, v0

    .line 675
    .end local v0    # "wRC":I
    .restart local v1    # "wRC":I
    goto :goto_0
.end method

.method public static xdmDDFPrintNodePropert(Lcom/policydm/eng/parser/XDM_DDFXmlElement;)V
    .locals 5
    .param p0, "element"    # Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    .prologue
    const-wide/16 v3, 0x400

    .line 895
    const-string v0, ""

    .line 897
    .local v0, "szPrintData":Ljava/lang/String;
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 899
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 902
    :cond_0
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 904
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    int-to-long v1, v1

    cmp-long v1, v3, v1

    if-gtz v1, :cond_1

    .line 906
    const-string v1, "Buffer Overflow. Increase the space. for element->name."

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 907
    const/4 v0, 0x0

    .line 941
    :goto_0
    return-void

    .line 910
    :cond_1
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 911
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 914
    :cond_2
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 916
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    int-to-long v1, v1

    cmp-long v1, v3, v1

    if-gtz v1, :cond_3

    .line 918
    const-string v1, "Buffer Overflow. Increase the space. for element->data."

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 919
    const/4 v0, 0x0

    .line 920
    goto :goto_0

    .line 922
    :cond_3
    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 923
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 926
    :cond_4
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 928
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    int-to-long v1, v1

    cmp-long v1, v3, v1

    if-gtz v1, :cond_5

    .line 930
    const-string v1, "Buffer Overflow. Increase the space. element->type."

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 931
    const/4 v0, 0x0

    .line 932
    goto :goto_0

    .line 934
    :cond_5
    const-string v1, "] ["

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 935
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 937
    :cond_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 938
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[DDF]["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->scope:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 940
    :cond_7
    const/4 v0, 0x0

    .line 941
    goto/16 :goto_0
.end method

.method public static xdmDDFProcessConvertAccessTypeElement()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2285
    const/4 v4, 0x0

    .line 2287
    .local v4, "szWbxmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2291
    .local v5, "szXmlData":Ljava/lang/String;
    const-string v3, ""

    .line 2294
    .local v3, "szData":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 2295
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v4

    .line 2296
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v5

    .line 2297
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2298
    .local v0, "nChar":C
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v2

    .line 2300
    .local v2, "nXmlSize":I
    const/16 v6, 0x3c

    if-ne v0, v6, :cond_0

    .line 2302
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2303
    add-int/lit8 v2, v2, -0x1

    .line 2304
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2305
    :goto_0
    const/16 v6, 0x2f

    if-ne v0, v6, :cond_1

    .line 2313
    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2314
    add-int/lit8 v2, v2, -0x2

    .line 2317
    :cond_0
    const/4 v0, 0x0

    .line 2318
    invoke-static {v3}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFConvertString2WbxmlHex(Ljava/lang/String;)C

    move-result v0

    .line 2319
    add-int/lit8 v6, v0, -0x40

    int-to-char v0, v6

    .line 2322
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2323
    add-int/lit8 v1, v1, 0x1

    .line 2324
    invoke-static {v1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2325
    invoke-static {v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2328
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2329
    invoke-static {v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2331
    const/4 v3, 0x0

    .line 2332
    return-void

    .line 2307
    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2308
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2309
    add-int/lit8 v2, v2, -0x1

    .line 2311
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method public static xdmDDFProcessConvertFormatElement()V
    .locals 10

    .prologue
    const/16 v9, 0x3c

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2210
    const/4 v4, 0x0

    .line 2212
    .local v4, "szWbxmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2216
    .local v5, "szXmlData":Ljava/lang/String;
    const-string v3, ""

    .line 2219
    .local v3, "szData":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 2220
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v4

    .line 2221
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v5

    .line 2222
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2223
    .local v0, "nChar":C
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v2

    .line 2225
    .local v2, "nXmlSize":I
    if-ne v0, v9, :cond_3

    .line 2227
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2228
    add-int/lit8 v2, v2, -0x1

    .line 2229
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2230
    :cond_0
    :goto_0
    const/16 v6, 0x2f

    if-ne v0, v6, :cond_2

    .line 2246
    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2247
    add-int/lit8 v2, v2, -0x2

    .line 2266
    :cond_1
    const/4 v0, 0x0

    .line 2267
    invoke-static {v3}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFConvertString2WbxmlHex(Ljava/lang/String;)C

    move-result v0

    .line 2268
    add-int/lit8 v6, v0, -0x40

    int-to-char v0, v6

    .line 2271
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2272
    add-int/lit8 v1, v1, 0x1

    .line 2273
    invoke-static {v1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2274
    invoke-static {v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2277
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2278
    invoke-static {v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2280
    const/4 v3, 0x0

    .line 2281
    return-void

    .line 2232
    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2234
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2235
    add-int/lit8 v2, v2, -0x1

    .line 2237
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2238
    const/16 v6, 0x20

    if-ne v0, v6, :cond_0

    .line 2240
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2241
    add-int/lit8 v2, v2, -0x1

    .line 2243
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0

    .line 2249
    :cond_3
    if-eq v0, v9, :cond_1

    .line 2251
    :goto_1
    if-eq v0, v9, :cond_1

    .line 2253
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2255
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2256
    add-int/lit8 v2, v2, -0x1

    .line 2258
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_1
.end method

.method public static xdmDDFProcessConvertHexData(C)V
    .locals 0
    .param p0, "cHex"    # C

    .prologue
    .line 2189
    sparse-switch p0, :sswitch_data_0

    .line 2206
    :goto_0
    return-void

    .line 2193
    :sswitch_0
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFProcessConvertFormatElement()V

    goto :goto_0

    .line 2197
    :sswitch_1
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFProcessConvertAccessTypeElement()V

    goto :goto_0

    .line 2189
    :sswitch_data_0
    .sparse-switch
        0x45 -> :sswitch_1
        0x55 -> :sswitch_0
        0x5c -> :sswitch_0
    .end sparse-switch
.end method

.method public static xdmDDFProcessConvertStringData()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2145
    const/4 v4, 0x0

    .line 2147
    .local v4, "szWbxmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2151
    .local v5, "szXmlData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 2154
    .local v1, "nStringLen":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 2155
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v4

    .line 2156
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v5

    .line 2157
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2158
    .local v0, "nChar":C
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 2161
    .local v3, "nXmlSize":I
    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2162
    add-int/lit8 v2, v2, 0x1

    .line 2165
    :goto_0
    const/16 v6, 0x3c

    if-ne v0, v6, :cond_0

    .line 2175
    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2176
    add-int/lit8 v2, v2, 0x1

    .line 2179
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2180
    invoke-static {v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2183
    sub-int v6, v3, v1

    invoke-static {v6}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2184
    invoke-static {v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2185
    return-void

    .line 2167
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2168
    add-int/lit8 v2, v2, 0x1

    .line 2169
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2170
    add-int/lit8 v1, v1, 0x1

    .line 2171
    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method

.method public static xdmDDFSetOMTree(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;III)V
    .locals 7
    .param p0, "omt"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szData"    # Ljava/lang/String;
    .param p3, "nLen"    # I
    .param p4, "szMime"    # Ljava/lang/String;
    .param p5, "aclValue"    # I
    .param p6, "scope"    # I
    .param p7, "format"    # I

    .prologue
    const/4 v1, 0x3

    .line 1050
    const/4 v6, 0x0

    .line 1052
    .local v6, "node":Lcom/policydm/eng/core/XDMVnode;
    invoke-static {p0, p1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v6

    .line 1054
    invoke-static {v1}, Lcom/policydm/agent/XDMAgent;->xdmAgentSetSyncMode(I)Z

    .line 1056
    if-eqz v6, :cond_0

    invoke-static {}, Lcom/policydm/agent/XDMAgent;->xdmAgentGetSyncMode()I

    move-result v0

    if-ne v0, v1, :cond_1

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p7

    .line 1058
    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFSetOMTreeProperty(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V

    .line 1059
    invoke-static {p0, p1, p5, p6}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1062
    :cond_1
    sget-boolean v0, Lcom/policydm/eng/parser/XDMDDFParser;->bNodeChangeMode:Z

    if-eqz v0, :cond_2

    .line 1064
    const-string v0, "bNodeChangeMode Change Node."

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p7

    .line 1065
    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFSetOMTreeProperty(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V

    .line 1066
    invoke-static {p0, p1, p5, p6}, Lcom/policydm/agent/XDMAgent;->xdmAgentMakeDefaultAcl(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;II)V

    .line 1068
    :cond_2
    return-void
.end method

.method public static xdmDDFSetOMTreeProperty(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V
    .locals 14
    .param p0, "omt"    # Lcom/policydm/eng/core/XDMOmTree;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "szData"    # Ljava/lang/String;
    .param p3, "nLen"    # I
    .param p4, "szMime"    # Ljava/lang/String;
    .param p5, "format"    # I

    .prologue
    .line 1075
    const/16 v1, 0x100

    new-array v13, v1, [C

    .line 1076
    .local v13, "tmpbuf":[C
    const/4 v7, 0x0

    .line 1078
    .local v7, "index":I
    invoke-static {p1, v13}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmMakeParentPath(Ljava/lang/String;[C)V

    .line 1080
    const-string v12, ""

    .line 1082
    .local v12, "szTmp":Ljava/lang/String;
    :goto_0
    aget-char v1, v13, v7

    if-nez v1, :cond_5

    .line 1088
    invoke-static {p0, v12}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v11

    .line 1090
    .local v11, "node":Lcom/policydm/eng/core/XDMVnode;
    if-nez v11, :cond_0

    .line 1092
    const/16 v1, 0x1b

    const/4 v2, 0x1

    invoke-static {p0, v12, v1, v2}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmProcessCmdImplicitAdd(Ljava/lang/Object;Ljava/lang/String;II)Z

    .line 1095
    :cond_0
    const/4 v1, 0x6

    move/from16 v0, p5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    move/from16 v0, p5

    if-eq v0, v1, :cond_1

    const/16 v1, 0xc

    move/from16 v0, p5

    if-ne v0, v1, :cond_6

    .line 1097
    :cond_1
    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, ""

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v1 .. v6}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    .line 1108
    :cond_2
    :goto_1
    invoke-static {p0, p1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmGetNodeProp(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v11

    .line 1109
    if-eqz v11, :cond_4

    .line 1111
    iget-object v1, v11, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v1, :cond_3

    .line 1113
    iget-object v1, v11, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v1}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 1116
    :cond_3
    new-instance v8, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v8}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 1118
    .local v8, "list":Lcom/policydm/eng/core/XDMOmList;
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1120
    move-object/from16 v0, p4

    iput-object v0, v8, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 1126
    :goto_2
    const/4 v1, 0x0

    iput-object v1, v8, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 1127
    iput-object v8, v11, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 1128
    move/from16 v0, p5

    iput v0, v11, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 1130
    .end local v8    # "list":Lcom/policydm/eng/core/XDMOmList;
    :cond_4
    return-void

    .line 1084
    .end local v11    # "node":Lcom/policydm/eng/core/XDMVnode;
    :cond_5
    aget-char v1, v13, v7

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1085
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1101
    .restart local v11    # "node":Lcom/policydm/eng/core/XDMVnode;
    :cond_6
    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move/from16 v3, p3

    move-object/from16 v5, p2

    move/from16 v6, p3

    invoke-static/range {v1 .. v6}, Lcom/policydm/eng/core/XDMOmLib;->xdmOmWrite(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;IILjava/lang/Object;I)I

    move-result v1

    int-to-long v9, v1

    .line 1102
    .local v9, "nSize":J
    const-wide/16 v1, 0x0

    cmp-long v1, v9, v1

    if-gtz v1, :cond_2

    .line 1104
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Size["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_1

    .line 1124
    .end local v9    # "nSize":J
    .restart local v8    # "list":Lcom/policydm/eng/core/XDMOmList;
    :cond_7
    const-string v1, "text/plain"

    iput-object v1, v8, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    goto :goto_2
.end method

.method public static xdmDDFTNDSAllocXMLData()Z
    .locals 2

    .prologue
    .line 1448
    const-string v0, ""

    .line 1450
    .local v0, "szXMLBuf":Ljava/lang/String;
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1452
    const-string v1, "# ERROR #  Alloc Error !!! ###"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1453
    const/4 v1, 0x0

    .line 1458
    :goto_0
    return v1

    .line 1456
    :cond_0
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1458
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static xdmDDFTNDSAppendNameSpace()V
    .locals 4

    .prologue
    .line 1472
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v2

    .line 1473
    .local v2, "szXmlData":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v1

    .line 1475
    .local v1, "nXmlSize":I
    const-string v3, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1476
    const-string v3, "<SyncML xmlns=\'syncml:dmddf1.2\'>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1478
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    .line 1480
    .local v0, "nTmpLen":I
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1481
    add-int v3, v1, v0

    invoke-static {v3}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1482
    return-void
.end method

.method public static xdmDDFTNDSAppendSyncMLCloseTag()V
    .locals 4

    .prologue
    .line 2044
    const/4 v2, 0x0

    .line 2048
    .local v2, "szXmlData":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v2

    .line 2049
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v1

    .line 2051
    .local v1, "nXmlSize":I
    const-string v3, "</SyncML>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2053
    const-string v3, "</SyncML>"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 2055
    .local v0, "nTmpLen":I
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2056
    add-int v3, v1, v0

    invoke-static {v3}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2057
    return-void
.end method

.method public static xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z
    .locals 1
    .param p0, "ptr"    # Ljava/lang/Object;

    .prologue
    .line 1463
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmDDFTNDSGetTagString(I)Ljava/lang/String;
    .locals 2
    .param p0, "eTokenIndex"    # I

    .prologue
    .line 1708
    const/4 v0, 0x0

    .line 1710
    .local v0, "nIndex":I
    const/16 v1, 0x7d

    if-ge p0, v1, :cond_0

    .line 1712
    add-int/lit8 v0, p0, -0x45

    .line 1713
    sget-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->g_szTndsTokenStr:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 1722
    :goto_0
    return-object v1

    .line 1715
    :cond_0
    const/16 v1, 0x88

    if-ne p0, v1, :cond_1

    .line 1717
    const-string v1, "SyncML"

    goto :goto_0

    .line 1722
    :cond_1
    const-string v1, "NULL"

    goto :goto_0
.end method

.method public static xdmDDFTNDSGetWbxmlData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1423
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMTndsData;->m_szWbxmlData:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmDDFTNDSGetWbxmlDataStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1443
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMTndsData;->m_szWbxmlDataStart:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmDDFTNDSGetWbxmlSize()I
    .locals 1

    .prologue
    .line 1418
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iget v0, v0, Lcom/policydm/eng/parser/XDMTndsData;->nWbxmlDataSize:I

    return v0
.end method

.method public static xdmDDFTNDSGetXMLData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1433
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMTndsData;->m_szXMLData:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmDDFTNDSGetXMLDataStart()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1438
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMTndsData;->m_szXMLDataStart:Ljava/lang/String;

    return-object v0
.end method

.method public static xdmDDFTNDSGetXMLSize()I
    .locals 1

    .prologue
    .line 1428
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iget v0, v0, Lcom/policydm/eng/parser/XDMTndsData;->nXMLDataSize:I

    return v0
.end method

.method public static xdmDDFTNDSInitParse(Ljava/lang/String;I)V
    .locals 1
    .param p0, "szInData"    # Ljava/lang/String;
    .param p1, "nInSize"    # I

    .prologue
    .line 1374
    new-instance v0, Lcom/policydm/eng/parser/XDMTndsData;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMTndsData;-><init>()V

    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    .line 1376
    invoke-static {p1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1377
    invoke-static {p0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1378
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSAllocXMLData()Z

    .line 1380
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSAppendNameSpace()V

    .line 1382
    new-instance v0, Lcom/policydm/eng/parser/XDMTndsTagManage;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMTndsTagManage;-><init>()V

    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    .line 1384
    return-void
.end method

.method public static xdmDDFTNDSMakeCloseTagString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1969
    const/4 v0, 0x0

    .line 1970
    .local v0, "eCloseTokenIndex":I
    const-string v1, ""

    .line 1972
    .local v1, "szCloseTagName":Ljava/lang/String;
    invoke-static {v1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1974
    const-string v2, "# ERROR # Alloc Error !!! ###"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1975
    const/4 v2, 0x0

    .line 1984
    :goto_0
    return-object v2

    .line 1978
    :cond_0
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSManagePopTag()I

    move-result v0

    .line 1980
    const-string v1, "</"

    .line 1981
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetTagString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1982
    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 1984
    goto :goto_0
.end method

.method public static xdmDDFTNDSMakeOpenTagString(I)Ljava/lang/String;
    .locals 2
    .param p0, "eTokenIndex"    # I

    .prologue
    .line 1691
    const-string v0, ""

    .line 1693
    .local v0, "szOpenTagName":Ljava/lang/String;
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSCheckMem(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1695
    const-string v1, "# ERROR # Alloc Error !!! ###"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1696
    const/4 v1, 0x0

    .line 1703
    :goto_0
    return-object v1

    .line 1699
    :cond_0
    const-string v0, "<"

    .line 1700
    invoke-static {p0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetTagString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1701
    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1703
    goto :goto_0
.end method

.method public static xdmDDFTNDSManagePopTag()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1989
    const/4 v0, 0x0

    .line 1991
    .local v0, "eCloseTagID":I
    sget-object v2, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget v2, v2, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    if-nez v2, :cond_0

    .line 1993
    const-string v2, "# ERROR # TagSP EMPTY !!! ###"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v0, v1

    .line 2001
    :goto_0
    return v0

    .line 1997
    :cond_0
    sget-object v2, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget v3, v2, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    .line 1998
    sget-object v2, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget-object v2, v2, Lcom/policydm/eng/parser/XDMTndsTagManage;->eTagID:[I

    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget v3, v3, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    aget v0, v2, v3

    .line 1999
    sget-object v2, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget-object v2, v2, Lcom/policydm/eng/parser/XDMTndsTagManage;->eTagID:[I

    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget v3, v3, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    aput v1, v2, v3

    goto :goto_0
.end method

.method public static xdmDDFTNDSManagePushTag(I)Z
    .locals 2
    .param p0, "eTokenIndex"    # I

    .prologue
    .line 1571
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget v0, v0, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_0

    .line 1573
    const-string v0, "# ERROR # TagSP FULL !!! ###"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1574
    const/4 v0, 0x0

    .line 1580
    :goto_0
    return v0

    .line 1577
    :cond_0
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMTndsTagManage;->eTagID:[I

    sget-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget v1, v1, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    aput p0, v0, v1

    .line 1578
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    iget v1, v0, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/policydm/eng/parser/XDMTndsTagManage;->nTagSP:I

    .line 1580
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static xdmDDFTNDSParsingACLTag()V
    .locals 0

    .prologue
    .line 1647
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1648
    return-void
.end method

.method public static xdmDDFTNDSParsingAccessTypeChildElement()V
    .locals 8

    .prologue
    .line 1796
    const/4 v6, 0x0

    .line 1798
    .local v6, "szWbxmlData":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1803
    .local v4, "pXmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1806
    .local v5, "szTypeBuf":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 1807
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v6

    .line 1808
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1809
    .local v0, "nTag":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v4

    .line 1810
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 1812
    .local v3, "nXmlSize":I
    add-int/lit8 v0, v0, -0x40

    .line 1814
    sparse-switch v0, :sswitch_data_0

    .line 1842
    :goto_0
    return-void

    .line 1822
    :sswitch_0
    const-string v5, "<"

    .line 1823
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetTagString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1824
    const-string v7, "/>"

    invoke-virtual {v5, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1832
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    .line 1834
    .local v1, "nTagLen":I
    add-int v7, v3, v1

    invoke-static {v7}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1835
    invoke-static {v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1838
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1839
    add-int/lit8 v2, v2, -0x1

    .line 1840
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1841
    invoke-static {v6}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    goto :goto_0

    .line 1814
    :sswitch_data_0
    .sparse-switch
        0x47 -> :sswitch_0
        0x4e -> :sswitch_0
        0x53 -> :sswitch_0
        0x5a -> :sswitch_0
        0x5d -> :sswitch_0
        0x6e -> :sswitch_0
    .end sparse-switch
.end method

.method public static xdmDDFTNDSParsingAccessTypeTag()V
    .locals 0

    .prologue
    .line 1636
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1637
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingAccessTypeChildElement()V

    .line 1638
    return-void
.end method

.method public static xdmDDFTNDSParsingCloseTag()V
    .locals 7

    .prologue
    .line 1935
    const/4 v4, 0x0

    .line 1937
    .local v4, "szWbxmlData":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1940
    .local v5, "szXmlData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1941
    .local v0, "nTagLen":I
    const/4 v3, 0x0

    .line 1944
    .local v3, "szTag":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 1945
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v4

    .line 1946
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v5

    .line 1947
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v2

    .line 1950
    .local v2, "nXmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSMakeCloseTagString()Ljava/lang/String;

    move-result-object v3

    .line 1951
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1953
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 1955
    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1957
    :cond_0
    add-int v6, v2, v0

    invoke-static {v6}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1958
    invoke-static {v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1961
    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1962
    add-int/lit8 v1, v1, -0x1

    .line 1963
    invoke-static {v1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1964
    invoke-static {v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1965
    return-void
.end method

.method public static xdmDDFTNDSParsingCodePage()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1486
    const/4 v1, 0x0

    .line 1487
    .local v1, "nWbxmlSize":I
    const/4 v0, 0x0

    .line 1488
    .local v0, "index":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 1491
    .local v2, "szWbxmlData":Ljava/lang/String;
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1492
    add-int/lit8 v1, v1, 0x1

    .line 1494
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    .line 1498
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-eqz v4, :cond_0

    .line 1504
    const-string v3, "### ERROR ### TNDS Tag Right ###"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1505
    const/4 v3, 0x0

    .line 1513
    :goto_0
    return v3

    .line 1508
    :cond_0
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1509
    add-int/lit8 v1, v1, 0x1

    .line 1511
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1512
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-static {v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    goto :goto_0
.end method

.method public static xdmDDFTNDSParsingDDFNameTag()V
    .locals 0

    .prologue
    .line 1621
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1622
    return-void
.end method

.method public static xdmDDFTNDSParsingFormatChildElement()V
    .locals 9

    .prologue
    .line 1728
    const/4 v5, 0x0

    .line 1730
    .local v5, "szWbxmlData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1735
    .local v6, "szXmlData":Ljava/lang/String;
    const-string v4, ""

    .line 1738
    .local v4, "szTypeBuf":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 1739
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v5

    .line 1740
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1741
    .local v0, "nTag":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v6

    .line 1742
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 1744
    .local v3, "nXmlSize":I
    add-int/lit8 v0, v0, 0x40

    .line 1746
    sparse-switch v0, :sswitch_data_0

    .line 1777
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "#ERROR!!!#  child tag vlaue is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  ###"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1782
    :goto_0
    :sswitch_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 1783
    .local v1, "nTagLen":I
    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1784
    add-int v7, v3, v1

    invoke-static {v7}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1785
    invoke-static {v6}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1788
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1789
    add-int/lit8 v2, v2, -0x1

    .line 1790
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1791
    invoke-static {v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1792
    return-void

    .line 1766
    .end local v1    # "nTagLen":I
    :sswitch_1
    const-string v4, "<"

    .line 1767
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetTagString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1768
    const-string v7, "/>"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1769
    goto :goto_0

    .line 1746
    nop

    :sswitch_data_0
    .sparse-switch
        0x48 -> :sswitch_1
        0x49 -> :sswitch_1
        0x4a -> :sswitch_1
        0x4b -> :sswitch_1
        0x50 -> :sswitch_1
        0x5b -> :sswitch_1
        0x5e -> :sswitch_1
        0x65 -> :sswitch_1
        0x67 -> :sswitch_1
        0x69 -> :sswitch_1
        0x6a -> :sswitch_1
        0x6b -> :sswitch_0
        0x72 -> :sswitch_1
        0x79 -> :sswitch_1
        0x7a -> :sswitch_1
        0x7b -> :sswitch_0
        0x7c -> :sswitch_1
    .end sparse-switch
.end method

.method public static xdmDDFTNDSParsingFormatTag()V
    .locals 0

    .prologue
    .line 1610
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1611
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingFormatChildElement()V

    .line 1612
    return-void
.end method

.method public static xdmDDFTNDSParsingMIMETag()V
    .locals 0

    .prologue
    .line 1631
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1632
    return-void
.end method

.method public static xdmDDFTNDSParsingMgmtTreeTag()V
    .locals 0

    .prologue
    .line 1585
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1586
    return-void
.end method

.method public static xdmDDFTNDSParsingNodeNameTag()V
    .locals 0

    .prologue
    .line 1600
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1601
    return-void
.end method

.method public static xdmDDFTNDSParsingNodeTag()V
    .locals 0

    .prologue
    .line 1595
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1596
    return-void
.end method

.method public static xdmDDFTNDSParsingOpenTag()V
    .locals 8

    .prologue
    .line 1652
    const/4 v5, 0x0

    .line 1654
    .local v5, "szWbxmlData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1657
    .local v6, "szXmlData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1658
    .local v1, "nTagLen":I
    const/4 v4, 0x0

    .line 1662
    .local v4, "szTag":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 1663
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v5

    .line 1664
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1665
    .local v0, "nTag":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v6

    .line 1666
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 1669
    .local v3, "nXmlSize":I
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSManagePushTag(I)Z

    .line 1670
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSMakeOpenTagString(I)Ljava/lang/String;

    move-result-object v4

    .line 1671
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1673
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    .line 1675
    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1677
    :cond_0
    add-int v7, v3, v1

    invoke-static {v7}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 1678
    invoke-static {v6}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 1681
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1682
    add-int/lit8 v2, v2, -0x1

    .line 1683
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1684
    invoke-static {v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1686
    const/4 v4, 0x0

    .line 1687
    return-void
.end method

.method public static xdmDDFTNDSParsingPathTag()V
    .locals 0

    .prologue
    .line 1642
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1643
    return-void
.end method

.method public static xdmDDFTNDSParsingRTPropertiesTag()V
    .locals 0

    .prologue
    .line 1605
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1606
    return-void
.end method

.method public static xdmDDFTNDSParsingSyncMLTag()V
    .locals 4

    .prologue
    .line 1549
    const/4 v2, 0x0

    .line 1554
    .local v2, "szWbxmlData":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 1555
    .local v1, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 1557
    const/16 v0, 0x88

    .line 1560
    .local v0, "nTag":I
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSManagePushTag(I)Z

    .line 1563
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1564
    add-int/lit8 v1, v1, -0x1

    .line 1565
    invoke-static {v1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1566
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1567
    return-void
.end method

.method public static xdmDDFTNDSParsingTypeTag()V
    .locals 0

    .prologue
    .line 1616
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1617
    return-void
.end method

.method public static xdmDDFTNDSParsingValueTag()V
    .locals 0

    .prologue
    .line 1626
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1627
    return-void
.end method

.method public static xdmDDFTNDSParsingVerdtdTag()V
    .locals 0

    .prologue
    .line 1590
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingOpenTag()V

    .line 1591
    return-void
.end method

.method public static xdmDDFTNDSParsingWbxmlHeader()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1518
    const/4 v1, 0x0

    .line 1519
    .local v1, "nWbxmlHeaderSize":I
    const/4 v0, 0x0

    .line 1520
    .local v0, "nStringTableSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 1523
    .local v2, "szWbxmlHeaderData":Ljava/lang/String;
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1524
    add-int/lit8 v1, v1, 0x1

    .line 1527
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1528
    add-int/lit8 v1, v1, 0x2

    .line 1531
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1532
    add-int/lit8 v1, v1, 0x1

    .line 1535
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1536
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1537
    add-int/lit8 v1, v1, 0x1

    .line 1540
    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1541
    add-int/lit8 v1, v0, 0x5

    .line 1543
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 1544
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v3

    sub-int/2addr v3, v1

    invoke-static {v3}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 1545
    return-void
.end method

.method public static xdmDDFTNDSProcessStringData()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2006
    const/4 v5, 0x0

    .line 2008
    .local v5, "szWbxmlData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2009
    .local v6, "szXmlData":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 2013
    .local v4, "pBufXmlData":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 2015
    .local v0, "nDataLen":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v2

    .line 2016
    .local v2, "nWbxmlSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v5

    .line 2017
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v6

    .line 2018
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2019
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLSize()I

    move-result v3

    .line 2021
    .local v3, "nXmlSize":I
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2022
    add-int/lit8 v0, v0, 0x1

    .line 2023
    invoke-virtual {v5, v9}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2025
    .local v1, "nTag":I
    :goto_0
    if-nez v1, :cond_0

    .line 2033
    add-int v7, v3, v0

    add-int/lit8 v7, v7, -0x1

    invoke-static {v7}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLSize(I)V

    .line 2034
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetXMLData(Ljava/lang/String;)V

    .line 2036
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2037
    add-int/lit8 v7, v0, 0x1

    sub-int/2addr v2, v7

    .line 2038
    invoke-static {v2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlSize(I)V

    .line 2039
    invoke-static {v5}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V

    .line 2040
    return-void

    .line 2027
    :cond_0
    int-to-char v7, v1

    invoke-virtual {v4, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2028
    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2029
    add-int/lit8 v0, v0, 0x1

    .line 2030
    invoke-virtual {v5, v9}, Ljava/lang/String;->charAt(I)C

    move-result v1

    goto :goto_0
.end method

.method public static xdmDDFTNDSSetWbxmlData(Ljava/lang/String;)V
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 1393
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iput-object p0, v0, Lcom/policydm/eng/parser/XDMTndsData;->m_szWbxmlData:Ljava/lang/String;

    .line 1394
    return-void
.end method

.method public static xdmDDFTNDSSetWbxmlDataStart(Ljava/lang/String;)V
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 1413
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iput-object p0, v0, Lcom/policydm/eng/parser/XDMTndsData;->m_szWbxmlDataStart:Ljava/lang/String;

    .line 1414
    return-void
.end method

.method public static xdmDDFTNDSSetWbxmlSize(I)V
    .locals 1
    .param p0, "nSize"    # I

    .prologue
    .line 1388
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iput p0, v0, Lcom/policydm/eng/parser/XDMTndsData;->nWbxmlDataSize:I

    .line 1389
    return-void
.end method

.method public static xdmDDFTNDSSetXMLData(Ljava/lang/String;)V
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 1403
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iput-object p0, v0, Lcom/policydm/eng/parser/XDMTndsData;->m_szXMLData:Ljava/lang/String;

    .line 1404
    return-void
.end method

.method public static xdmDDFTNDSSetXMLDataStart(Ljava/lang/String;)V
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 1408
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iput-object p0, v0, Lcom/policydm/eng/parser/XDMTndsData;->m_szXMLDataStart:Ljava/lang/String;

    .line 1409
    return-void
.end method

.method public static xdmDDFTNDSSetXMLSize(I)V
    .locals 1
    .param p0, "nSize"    # I

    .prologue
    .line 1398
    sget-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    iput p0, v0, Lcom/policydm/eng/parser/XDMTndsData;->nXMLDataSize:I

    .line 1399
    return-void
.end method

.method public static xdmDDFTNDSUderMgmtTreeTagParse()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1847
    const/4 v1, 0x0

    .line 1850
    .local v1, "szData":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v0

    .line 1851
    .local v0, "nSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v1

    .line 1854
    :goto_0
    if-eqz v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1930
    :cond_0
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 1856
    :cond_1
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 1858
    .local v2, "wbxmlData":[B
    aget-byte v4, v2, v3

    sparse-switch v4, :sswitch_data_0

    .line 1921
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1922
    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 1861
    :sswitch_0
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingCloseTag()V

    .line 1926
    :goto_2
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v0

    .line 1927
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v1

    :goto_3
    goto :goto_0

    .line 1865
    :sswitch_1
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSProcessStringData()V

    goto :goto_2

    .line 1869
    :sswitch_2
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingNodeTag()V

    goto :goto_2

    .line 1873
    :sswitch_3
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingNodeNameTag()V

    goto :goto_2

    .line 1877
    :sswitch_4
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingRTPropertiesTag()V

    goto :goto_2

    .line 1881
    :sswitch_5
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingFormatTag()V

    goto :goto_2

    .line 1885
    :sswitch_6
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingTypeTag()V

    goto :goto_2

    .line 1889
    :sswitch_7
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingDDFNameTag()V

    goto :goto_2

    .line 1893
    :sswitch_8
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingValueTag()V

    goto :goto_2

    .line 1897
    :sswitch_9
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingMIMETag()V

    goto :goto_2

    .line 1901
    :sswitch_a
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingAccessTypeTag()V

    goto :goto_2

    .line 1905
    :sswitch_b
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingPathTag()V

    goto :goto_2

    .line 1909
    :sswitch_c
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingACLTag()V

    goto :goto_2

    .line 1913
    :sswitch_d
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingVerdtdTag()V

    goto :goto_2

    .line 1917
    :sswitch_e
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingCodePage()Z

    goto :goto_2

    .line 1927
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    .line 1858
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_e
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x45 -> :sswitch_a
        0x46 -> :sswitch_c
        0x51 -> :sswitch_7
        0x5c -> :sswitch_5
        0x61 -> :sswitch_9
        0x64 -> :sswitch_2
        0x66 -> :sswitch_3
        0x6c -> :sswitch_b
        0x6f -> :sswitch_4
        0x75 -> :sswitch_6
        0x76 -> :sswitch_8
        0x77 -> :sswitch_d
    .end sparse-switch
.end method

.method public static xdmDDFXmlTagCode(Ljava/lang/String;)I
    .locals 2
    .param p0, "szName"    # Ljava/lang/String;

    .prologue
    .line 653
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 661
    const/4 v0, 0x0

    .end local v0    # "i":I
    :cond_0
    return v0

    .line 655
    .restart local v0    # "i":I
    :cond_1
    sget-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 653
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static xdmDDFXmlTagParsing(Lcom/policydm/eng/parser/XDM_DM_Tree;)Z
    .locals 9
    .param p0, "pTree"    # Lcom/policydm/eng/parser/XDM_DM_Tree;

    .prologue
    .line 681
    if-nez p0, :cond_1

    .line 683
    const/4 v3, 0x0

    .line 799
    :cond_0
    :goto_0
    return v3

    .line 685
    :cond_1
    const/4 v0, 0x0

    .line 686
    .local v0, "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    iget-object v1, p0, Lcom/policydm/eng/parser/XDM_DM_Tree;->childlist:Lcom/policydm/eng/core/XDMLinkedList;

    .line 688
    .local v1, "childlist":Lcom/policydm/eng/core/XDMLinkedList;
    const/4 v3, 0x1

    .line 689
    .local v3, "ret":Z
    const-string v4, ""

    .line 690
    .local v4, "szPath":Ljava/lang/String;
    const-string v5, ""

    .line 692
    .local v5, "szPathTemp":Ljava/lang/String;
    :goto_1
    if-eqz p0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/policydm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .end local v0    # "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    check-cast v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    .line 696
    .restart local v0    # "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 698
    const-string v6, "Tag:"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 699
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 702
    :cond_2
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 704
    const-string v6, "Name:"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 705
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 708
    :cond_3
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 710
    const-string v6, "Path:"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 711
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 714
    :cond_4
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 716
    const-string v6, "Data:"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 717
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 720
    :cond_5
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFXmlTagCode(Ljava/lang/String;)I

    move-result v2

    .line 722
    .local v2, "nTagCode":I
    iget-object v6, p0, Lcom/policydm/eng/parser/XDM_DM_Tree;->parent:Lcom/policydm/eng/parser/XDM_DM_Tree;

    if-nez v6, :cond_6

    .line 724
    const/4 v4, 0x0

    .line 726
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 728
    iget-object v4, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 735
    :goto_2
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 737
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    invoke-static {v6}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCheckInbox(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 738
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 740
    iput-object v4, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 747
    :cond_6
    sget-object v6, Lcom/policydm/eng/parser/XDMDDFParser;->g_om:Lcom/policydm/eng/core/XDMOmTree;

    iget-object v7, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    iget-object v8, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmAgentVerifyNewAccount(Lcom/policydm/eng/core/XDMOmTree;Ljava/lang/String;Ljava/lang/String;)Z

    .line 749
    sparse-switch v2, :sswitch_data_0

    .line 791
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 792
    sget-object v6, Lcom/policydm/eng/parser/XDMDDFParser;->g_szDmXmlOmaTags:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 795
    :cond_7
    :goto_3
    :sswitch_0
    invoke-static {v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "pTree":Lcom/policydm/eng/parser/XDM_DM_Tree;
    check-cast p0, Lcom/policydm/eng/parser/XDM_DM_Tree;

    .restart local p0    # "pTree":Lcom/policydm/eng/parser/XDM_DM_Tree;
    goto/16 :goto_1

    .line 732
    :cond_8
    const-string v6, "Path is NULL."

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_2

    .line 752
    :sswitch_1
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFPrintNodePropert(Lcom/policydm/eng/parser/XDM_DDFXmlElement;)V

    .line 753
    invoke-static {v0}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateNodeToOM(Lcom/policydm/eng/parser/XDM_DDFXmlElement;)Z

    move-result v3

    .line 755
    if-nez v3, :cond_9

    .line 757
    const/4 v4, 0x0

    .line 758
    goto/16 :goto_0

    .line 762
    :cond_9
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    const-string v7, "./"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 764
    const-string v4, "/"

    .line 767
    :cond_a
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 769
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 771
    iget-object v6, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 773
    :cond_b
    invoke-static {p0, v4}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFCreateNode(Lcom/policydm/eng/parser/XDM_DM_Tree;Ljava/lang/String;)Z

    .line 775
    const/16 v6, 0x2f

    invoke-static {v4, v6}, Lcom/policydm/eng/core/XDMMem;->xdmLibStrrchr(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v5

    .line 777
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 779
    move-object v4, v5

    .line 782
    goto :goto_3

    .line 749
    :sswitch_data_0
    .sparse-switch
        0x1f -> :sswitch_0
        0x20 -> :sswitch_0
        0x22 -> :sswitch_0
        0x24 -> :sswitch_1
        0x37 -> :sswitch_0
    .end sparse-switch
.end method

.method public static xdmTndsParseFinish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2366
    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gTndsData:Lcom/policydm/eng/parser/XDMTndsData;

    .line 2367
    sput-object v0, Lcom/policydm/eng/parser/XDMDDFParser;->gstTagManage:Lcom/policydm/eng/parser/XDMTndsTagManage;

    .line 2368
    return-void
.end method

.method public static xdmTndsWbxmlParse(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p0, "szInData"    # Ljava/lang/String;
    .param p1, "nInSize"    # I

    .prologue
    const/4 v5, 0x0

    .line 1317
    const-string v2, ""

    .line 1319
    .local v2, "szData":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1320
    .local v0, "bSyncMLTag":Z
    const-string v3, ""

    .line 1322
    .local v3, "szOutData":Ljava/lang/String;
    invoke-static {p0, p1}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSInitParse(Ljava/lang/String;I)V

    .line 1324
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 1325
    .local v1, "nSize":I
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    .line 1328
    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1362
    :cond_0
    if-nez v0, :cond_1

    .line 1364
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSAppendSyncMLCloseTag()V

    .line 1367
    :cond_1
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetXMLData()Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    .line 1369
    :goto_1
    return-object v5

    .line 1330
    :cond_2
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v4

    .line 1332
    .local v4, "wbxmlData":[B
    const/4 v6, 0x0

    aget-byte v6, v4, v6

    sparse-switch v6, :sswitch_data_0

    .line 1353
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1354
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 1335
    :sswitch_0
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingCodePage()Z

    .line 1358
    :goto_2
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlSize()I

    move-result v1

    .line 1359
    if-eqz v1, :cond_3

    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSGetWbxmlData()Ljava/lang/String;

    move-result-object v2

    :goto_3
    goto :goto_0

    .line 1339
    :sswitch_1
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingWbxmlHeader()V

    goto :goto_2

    .line 1343
    :sswitch_2
    const/4 v0, 0x1

    .line 1344
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingSyncMLTag()V

    goto :goto_2

    .line 1348
    :sswitch_3
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSParsingMgmtTreeTag()V

    .line 1349
    invoke-static {}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFTNDSUderMgmtTreeTagParse()Z

    goto :goto_2

    :cond_3
    move-object v2, v5

    .line 1359
    goto :goto_3

    .line 1332
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x60 -> :sswitch_3
        0x6d -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public characters([CII)V
    .locals 7
    .param p1, "ch"    # [C
    .param p2, "start"    # I
    .param p3, "length"    # I

    .prologue
    const/4 v6, 0x0

    .line 493
    new-instance v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDM_DDFXmlElement;-><init>()V

    .line 495
    .local v0, "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "characters =     "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v5, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 497
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    if-nez v4, :cond_1

    .line 647
    :cond_0
    :goto_0
    return-void

    .line 502
    :cond_1
    sget-object v4, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    iget-object v0, v4, Lcom/policydm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .end local v0    # "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    check-cast v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    .line 504
    .restart local v0    # "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    if-eqz v0, :cond_0

    .line 509
    iget v4, p0, Lcom/policydm/eng/parser/XDMDDFParser;->gTagCode:I

    sparse-switch v4, :sswitch_data_0

    goto :goto_0

    .line 556
    :sswitch_0
    iget-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szDDFName:Ljava/lang/String;

    .line 558
    .local v2, "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 560
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 561
    .local v1, "old_len":I
    const-string v3, ""

    .line 563
    .local v3, "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 564
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 566
    move-object v2, v3

    .line 574
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_1
    iput-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szDDFName:Ljava/lang/String;

    goto :goto_0

    .line 512
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_1
    iget-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    .line 514
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 516
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 517
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 519
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 520
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 522
    move-object v2, v3

    .line 530
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_2
    iput-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    goto :goto_0

    .line 526
    :cond_2
    const-string v2, ""

    .line 527
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 534
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_2
    iget-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    .line 536
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 538
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 539
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 541
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 542
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 544
    move-object v2, v3

    .line 552
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_3
    iput-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szPath:Ljava/lang/String;

    goto/16 :goto_0

    .line 548
    :cond_3
    const-string v2, ""

    .line 549
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 570
    :cond_4
    const-string v2, ""

    .line 571
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 578
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_3
    iget-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szMIME:Ljava/lang/String;

    .line 580
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 582
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 583
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 585
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 586
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 588
    move-object v2, v3

    .line 596
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_4
    iput-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szMIME:Ljava/lang/String;

    goto/16 :goto_0

    .line 592
    :cond_5
    const-string v2, ""

    .line 593
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 600
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_4
    iget-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    .line 602
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 604
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 605
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 607
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 608
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 610
    move-object v2, v3

    .line 618
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_5
    iput-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szType:Ljava/lang/String;

    goto/16 :goto_0

    .line 614
    :cond_6
    const-string v2, ""

    .line 615
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 623
    .end local v2    # "szData":Ljava/lang/String;
    :sswitch_5
    iget-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    .line 625
    .restart local v2    # "szData":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 627
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 628
    .restart local v1    # "old_len":I
    const-string v3, ""

    .line 630
    .restart local v3    # "szTemp":Ljava/lang/String;
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 631
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v1, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 633
    move-object v2, v3

    .line 641
    .end local v1    # "old_len":I
    .end local v3    # "szTemp":Ljava/lang/String;
    :goto_6
    iput-object v2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    goto/16 :goto_0

    .line 637
    :cond_7
    const-string v2, ""

    .line 638
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, p1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v4, v6, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    .line 509
    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_5
        0x21 -> :sswitch_3
        0x26 -> :sswitch_1
        0x2c -> :sswitch_2
        0x35 -> :sswitch_4
        0x36 -> :sswitch_5
    .end sparse-switch
.end method

.method public endDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 305
    invoke-super {p0}, Lorg/xml/sax/helpers/DefaultHandler;->endDocument()V

    .line 306
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "szNameSpaceURI"    # Ljava/lang/String;
    .param p2, "szLocalName"    # Ljava/lang/String;
    .param p3, "szQualifiedName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 366
    new-instance v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDM_DDFXmlElement;-><init>()V

    .line 367
    .local v0, "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "end =            "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 369
    sget-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    if-nez v1, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    sget-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    iget-object v0, v1, Lcom/policydm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .end local v0    # "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    check-cast v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    .line 376
    .restart local v0    # "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    invoke-static {p2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFXmlTagCode(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/policydm/eng/parser/XDMDDFParser;->gTagCode:I

    .line 378
    if-eqz v0, :cond_0

    .line 383
    iget v1, p0, Lcom/policydm/eng/parser/XDMDDFParser;->gTagCode:I

    packed-switch v1, :pswitch_data_0

    .line 486
    :cond_2
    :goto_1
    :pswitch_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/policydm/eng/parser/XDMDDFParser;->gTagCode:I

    goto :goto_0

    .line 386
    :pswitch_1
    sget-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    iget-object v1, v1, Lcom/policydm/eng/parser/XDM_DM_Tree;->parent:Lcom/policydm/eng/parser/XDM_DM_Tree;

    sput-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    goto :goto_1

    .line 394
    :pswitch_2
    iget-object v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 396
    const-string v1, "Node"

    iput-object v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szName:Ljava/lang/String;

    goto :goto_1

    .line 405
    :pswitch_3
    iget-object v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 407
    const-string v1, "No Data"

    iput-object v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szData:Ljava/lang/String;

    goto :goto_1

    .line 412
    :pswitch_4
    iget v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 416
    :pswitch_5
    iget v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 420
    :pswitch_6
    iget v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 424
    :pswitch_7
    iget v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 428
    :pswitch_8
    iget v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->acl:I

    goto :goto_1

    .line 432
    :pswitch_9
    iput v4, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->scope:I

    goto :goto_1

    .line 436
    :pswitch_a
    iput v3, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->scope:I

    goto :goto_1

    .line 440
    :pswitch_b
    const/4 v1, 0x6

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 444
    :pswitch_c
    const/4 v1, 0x4

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 448
    :pswitch_d
    const/4 v1, 0x5

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 452
    :pswitch_e
    const/16 v1, 0x8

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 456
    :pswitch_f
    const/4 v1, 0x7

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 460
    :pswitch_10
    const/4 v1, 0x3

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 464
    :pswitch_11
    iput v4, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 468
    :pswitch_12
    iput v3, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 472
    :pswitch_13
    const/16 v1, 0x9

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 476
    :pswitch_14
    const/16 v1, 0xa

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 480
    :pswitch_15
    const/16 v1, 0xb

    iput v1, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->format:I

    goto :goto_1

    .line 383
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_4
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_13
        :pswitch_0
        :pswitch_5
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_b
        :pswitch_2
        :pswitch_f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_e
    .end packed-switch
.end method

.method public startDocument()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 312
    invoke-super {p0}, Lorg/xml/sax/helpers/DefaultHandler;->startDocument()V

    .line 313
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 5
    .param p1, "szNameSpaceURI"    # Ljava/lang/String;
    .param p2, "szLocalName"    # Ljava/lang/String;
    .param p3, "szQualifiedName"    # Ljava/lang/String;
    .param p4, "atts"    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    .prologue
    .line 322
    invoke-static {p2}, Lcom/policydm/eng/parser/XDMDDFParser;->xdmDDFXmlTagCode(Ljava/lang/String;)I

    move-result v2

    .line 323
    .local v2, "nTagCode":I
    iput v2, p0, Lcom/policydm/eng/parser/XDMDDFParser;->gTagCode:I

    .line 324
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "start =          "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 326
    sparse-switch v2, :sswitch_data_0

    .line 361
    :goto_0
    :sswitch_0
    return-void

    .line 329
    :sswitch_1
    new-instance v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDM_DDFXmlElement;-><init>()V

    .line 331
    .local v0, "XmlElement":Lcom/policydm/eng/parser/XDM_DDFXmlElement;
    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    if-nez v3, :cond_0

    .line 333
    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    iput-object v0, v3, Lcom/policydm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .line 335
    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    sput-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->XmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    .line 352
    :goto_1
    iput-object p2, v0, Lcom/policydm/eng/parser/XDM_DDFXmlElement;->m_szTag:Ljava/lang/String;

    goto :goto_0

    .line 339
    :cond_0
    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDM_DM_Tree;->childlist:Lcom/policydm/eng/core/XDMLinkedList;

    if-nez v3, :cond_1

    .line 341
    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v4

    iput-object v4, v3, Lcom/policydm/eng/parser/XDM_DM_Tree;->childlist:Lcom/policydm/eng/core/XDMLinkedList;

    .line 344
    :cond_1
    new-instance v1, Lcom/policydm/eng/parser/XDM_DM_Tree;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDM_DM_Tree;-><init>()V

    .line 345
    .local v1, "childtree":Lcom/policydm/eng/parser/XDM_DM_Tree;
    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    iput-object v3, v1, Lcom/policydm/eng/parser/XDM_DM_Tree;->parent:Lcom/policydm/eng/parser/XDM_DM_Tree;

    .line 346
    iput-object v0, v1, Lcom/policydm/eng/parser/XDM_DM_Tree;->object:Ljava/lang/Object;

    .line 348
    sget-object v3, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    iget-object v3, v3, Lcom/policydm/eng/parser/XDM_DM_Tree;->childlist:Lcom/policydm/eng/core/XDMLinkedList;

    invoke-static {v3, v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V

    .line 350
    sput-object v1, Lcom/policydm/eng/parser/XDMDDFParser;->CurXmlTree:Lcom/policydm/eng/parser/XDM_DM_Tree;

    goto :goto_1

    .line 326
    nop

    :sswitch_data_0
    .sparse-switch
        0x16 -> :sswitch_0
        0x24 -> :sswitch_1
    .end sparse-switch
.end method
