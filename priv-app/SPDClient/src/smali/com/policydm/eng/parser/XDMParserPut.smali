.class public Lcom/policydm/eng/parser/XDMParserPut;
.super Ljava/lang/Object;
.source "XDMParserPut.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public cmdid:I

.field public cred:Lcom/policydm/eng/parser/XDMParserCred;

.field public is_noresp:I

.field public itemlist:Lcom/policydm/eng/core/XDMList;

.field public lang:I

.field public meta:Lcom/policydm/eng/parser/XDMParserMeta;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParserPut;->itemlist:Lcom/policydm/eng/core/XDMList;

    .line 9
    return-void
.end method


# virtual methods
.method public xdmParParsePut(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 5
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    const/4 v3, 0x0

    .line 25
    const/4 v1, -0x1

    .line 26
    .local v1, "id":I
    const/4 v2, 0x0

    .line 28
    .local v2, "res":I
    const/16 v4, 0x1f

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 30
    if-eqz v2, :cond_1

    move v3, v2

    .line 116
    :cond_0
    :goto_0
    return v3

    .line 35
    :cond_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 36
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 40
    if-eqz v2, :cond_2

    move v3, v2

    .line 42
    goto :goto_0

    .line 49
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 56
    :goto_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_3

    .line 58
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 59
    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 62
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v1, :sswitch_data_0

    .line 108
    const/4 v2, 0x2

    .line 111
    :goto_2
    if-eqz v2, :cond_2

    move v3, v2

    .line 113
    goto :goto_0

    .line 65
    :sswitch_0
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 66
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->cmdid:I

    goto :goto_2

    .line 70
    :sswitch_1
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->is_noresp:I

    goto :goto_2

    .line 74
    :sswitch_2
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 75
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->lang:I

    goto :goto_2

    .line 79
    :sswitch_3
    new-instance v4, Lcom/policydm/eng/parser/XDMParserCred;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserCred;-><init>()V

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 80
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserCred;->xdmParParseCred(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v2

    .line 81
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->Cred:Lcom/policydm/eng/parser/XDMParserCred;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->cred:Lcom/policydm/eng/parser/XDMParserCred;

    goto :goto_2

    .line 85
    :sswitch_4
    new-instance v4, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 86
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v2

    .line 87
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 91
    :sswitch_5
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->itemlist:Lcom/policydm/eng/core/XDMList;

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserPut;->itemlist:Lcom/policydm/eng/core/XDMList;

    goto :goto_2

    .line 95
    :sswitch_6
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 96
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 98
    if-eqz v2, :cond_4

    move v3, v2

    .line 100
    goto/16 :goto_0

    .line 103
    :cond_4
    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto :goto_2

    .line 62
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_6
        0xb -> :sswitch_0
        0xe -> :sswitch_3
        0x14 -> :sswitch_5
        0x15 -> :sswitch_2
        0x1a -> :sswitch_4
        0x1d -> :sswitch_1
    .end sparse-switch
.end method
