.class public Lcom/policydm/eng/parser/XDMParserCred;
.super Ljava/lang/Object;
.source "XDMParserCred.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public m_szData:Ljava/lang/String;

.field public meta:Lcom/policydm/eng/parser/XDMParserMeta;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParParseCred(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 5
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    .line 20
    const/4 v1, -0x1

    .line 21
    .local v1, "id":I
    const/4 v2, 0x0

    .line 23
    .local v2, "res":I
    const/16 v4, 0xe

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 24
    if-eqz v2, :cond_0

    move v3, v2

    .line 78
    .end local v2    # "res":I
    .local v3, "res":I
    :goto_0
    return v3

    .line 33
    .end local v3    # "res":I
    .restart local v2    # "res":I
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 40
    :goto_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_1

    .line 42
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 77
    iput-object p0, p1, Lcom/policydm/eng/parser/XDMParser;->Cred:Lcom/policydm/eng/parser/XDMParserCred;

    move v3, v2

    .line 78
    .end local v2    # "res":I
    .restart local v3    # "res":I
    goto :goto_0

    .line 35
    .end local v3    # "res":I
    .restart local v2    # "res":I
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 46
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    sparse-switch v1, :sswitch_data_0

    .line 68
    const/4 v2, 0x2

    .line 71
    :goto_2
    if-eqz v2, :cond_0

    move v3, v2

    .line 73
    .end local v2    # "res":I
    .restart local v3    # "res":I
    goto :goto_0

    .line 49
    .end local v3    # "res":I
    .restart local v2    # "res":I
    :sswitch_0
    new-instance v4, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 50
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-virtual {v4, p1}, Lcom/policydm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v2

    .line 51
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserCred;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 55
    :sswitch_1
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 56
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserCred;->m_szData:Ljava/lang/String;

    goto :goto_2

    .line 60
    :sswitch_2
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 61
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 63
    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto :goto_2

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0xf -> :sswitch_1
        0x1a -> :sswitch_0
    .end sparse-switch
.end method
