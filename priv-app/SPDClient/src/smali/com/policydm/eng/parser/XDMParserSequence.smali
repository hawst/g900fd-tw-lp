.class public Lcom/policydm/eng/parser/XDMParserSequence;
.super Lcom/policydm/agent/XDMHandleCmd;
.source "XDMParserSequence.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public add:Lcom/policydm/eng/parser/XDMParserAdd;

.field public alert:Lcom/policydm/eng/parser/XDMParserAlert;

.field public atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

.field public cmdid:I

.field public copy:Lcom/policydm/eng/parser/XDMParserCopy;

.field public delete:Lcom/policydm/eng/parser/XDMParserDelete;

.field public exec:Lcom/policydm/eng/parser/XDMParserExec;

.field public get:Lcom/policydm/eng/parser/XDMParserGet;

.field public is_noresp:I

.field public itemlist:Lcom/policydm/eng/core/XDMLinkedList;

.field public map:Lcom/policydm/eng/parser/XDMParserMap;

.field public meta:Lcom/policydm/eng/parser/XDMParserMeta;

.field public replace:Lcom/policydm/eng/parser/XDMParserReplace;

.field public sync:Lcom/policydm/eng/parser/XDMParserSync;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/policydm/agent/XDMHandleCmd;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParParseSequence(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 6
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    const/4 v4, 0x0

    .line 34
    const/4 v2, -0x1

    .line 35
    .local v2, "id":I
    const/4 v3, 0x0

    .line 36
    .local v3, "res":I
    const/4 v0, 0x1

    .line 38
    .local v0, "call_start_seq":Z
    const/16 v5, 0x24

    invoke-virtual {p1, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v3

    .line 39
    if-eqz v3, :cond_1

    move v4, v3

    .line 204
    :cond_0
    :goto_0
    return v4

    .line 44
    :cond_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v3

    .line 45
    const/16 v5, 0x8

    if-eq v3, v5, :cond_0

    .line 49
    if-eqz v3, :cond_2

    .line 51
    const-string v4, "not WBXML_ERR_OK"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v4, v3

    .line 52
    goto :goto_0

    .line 59
    :cond_2
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 66
    :goto_1
    const/4 v5, 0x1

    if-ne v2, v5, :cond_4

    .line 68
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v2

    .line 196
    if-eqz v0, :cond_3

    .line 198
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 199
    const/4 v0, 0x0

    .line 202
    :cond_3
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceEnd(Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 71
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    sparse-switch v2, :sswitch_data_0

    .line 188
    const/4 v3, 0x2

    .line 190
    :goto_2
    if-eqz v3, :cond_2

    move v4, v3

    .line 192
    goto :goto_0

    .line 74
    :sswitch_0
    invoke-virtual {p1, v2}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v3

    .line 75
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->cmdid:I

    goto :goto_2

    .line 79
    :sswitch_1
    invoke-virtual {p1, v2}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v5

    iput v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->is_noresp:I

    goto :goto_2

    .line 83
    :sswitch_2
    new-instance v5, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 84
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v3

    .line 85
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->meta:Lcom/policydm/eng/parser/XDMParserMeta;

    goto :goto_2

    .line 89
    :sswitch_3
    if-eqz v0, :cond_5

    .line 91
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 92
    const/4 v0, 0x0

    .line 94
    :cond_5
    new-instance v5, Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserAlert;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->alert:Lcom/policydm/eng/parser/XDMParserAlert;

    .line 95
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->alert:Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserAlert;->xdmParParseAlert(Lcom/policydm/eng/parser/XDMParser;)I

    goto :goto_2

    .line 98
    :sswitch_4
    if-eqz v0, :cond_6

    .line 100
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 101
    const/4 v0, 0x0

    .line 103
    :cond_6
    new-instance v5, Lcom/policydm/eng/parser/XDMParserAdd;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserAdd;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->add:Lcom/policydm/eng/parser/XDMParserAdd;

    .line 104
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->add:Lcom/policydm/eng/parser/XDMParserAdd;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserAdd;->xdmParParseAdd(Lcom/policydm/eng/parser/XDMParser;)I

    goto :goto_2

    .line 107
    :sswitch_5
    if-eqz v0, :cond_7

    .line 109
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 110
    const/4 v0, 0x0

    .line 112
    :cond_7
    new-instance v5, Lcom/policydm/eng/parser/XDMParserReplace;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserReplace;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->replace:Lcom/policydm/eng/parser/XDMParserReplace;

    .line 113
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->replace:Lcom/policydm/eng/parser/XDMParserReplace;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserReplace;->xdmParParseReplace(Lcom/policydm/eng/parser/XDMParser;)I

    goto :goto_2

    .line 116
    :sswitch_6
    if-eqz v0, :cond_8

    .line 118
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 119
    const/4 v0, 0x0

    .line 121
    :cond_8
    new-instance v5, Lcom/policydm/eng/parser/XDMParserDelete;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserDelete;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->delete:Lcom/policydm/eng/parser/XDMParserDelete;

    .line 122
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->delete:Lcom/policydm/eng/parser/XDMParserDelete;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserDelete;->xdmParParseDelete(Lcom/policydm/eng/parser/XDMParser;)I

    goto :goto_2

    .line 125
    :sswitch_7
    if-eqz v0, :cond_9

    .line 127
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 128
    const/4 v0, 0x0

    .line 130
    :cond_9
    new-instance v5, Lcom/policydm/eng/parser/XDMParserCopy;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserCopy;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->copy:Lcom/policydm/eng/parser/XDMParserCopy;

    .line 131
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->copy:Lcom/policydm/eng/parser/XDMParserCopy;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserCopy;->xdmParParseCopy(Lcom/policydm/eng/parser/XDMParser;)I

    goto/16 :goto_2

    .line 134
    :sswitch_8
    if-eqz v0, :cond_a

    .line 136
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 137
    const/4 v0, 0x0

    .line 139
    :cond_a
    new-instance v5, Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserAtomic;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    .line 140
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserAtomic;->xdmParParseAtomic(Lcom/policydm/eng/parser/XDMParser;)I

    goto/16 :goto_2

    .line 143
    :sswitch_9
    if-eqz v0, :cond_b

    .line 145
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 146
    const/4 v0, 0x0

    .line 148
    :cond_b
    new-instance v5, Lcom/policydm/eng/parser/XDMParserMap;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserMap;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->map:Lcom/policydm/eng/parser/XDMParserMap;

    .line 149
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->map:Lcom/policydm/eng/parser/XDMParserMap;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserMap;->xdmParParseMap(Lcom/policydm/eng/parser/XDMParser;)I

    goto/16 :goto_2

    .line 153
    :sswitch_a
    if-eqz v0, :cond_c

    .line 155
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 156
    const/4 v0, 0x0

    .line 158
    :cond_c
    new-instance v5, Lcom/policydm/eng/parser/XDMParserGet;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserGet;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->get:Lcom/policydm/eng/parser/XDMParserGet;

    .line 159
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->get:Lcom/policydm/eng/parser/XDMParserGet;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserGet;->xdmParParseGet(Lcom/policydm/eng/parser/XDMParser;)I

    goto/16 :goto_2

    .line 163
    :sswitch_b
    if-eqz v0, :cond_d

    .line 165
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 166
    const/4 v0, 0x0

    .line 168
    :cond_d
    new-instance v5, Lcom/policydm/eng/parser/XDMParserSync;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserSync;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->sync:Lcom/policydm/eng/parser/XDMParserSync;

    .line 169
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->sync:Lcom/policydm/eng/parser/XDMParserSync;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserSync;->xdmParParseSync(Lcom/policydm/eng/parser/XDMParser;)I

    goto/16 :goto_2

    .line 172
    :sswitch_c
    if-eqz v0, :cond_e

    .line 174
    iget-object v5, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    invoke-virtual {p0, v5, p0}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmAgentHdlCmdSequenceStart(Ljava/lang/Object;Lcom/policydm/eng/parser/XDMParserSequence;)V

    .line 175
    const/4 v0, 0x0

    .line 177
    :cond_e
    new-instance v5, Lcom/policydm/eng/parser/XDMParserExec;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserExec;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->exec:Lcom/policydm/eng/parser/XDMParserExec;

    .line 178
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParserSequence;->exec:Lcom/policydm/eng/parser/XDMParserExec;

    invoke-virtual {v5, p1}, Lcom/policydm/eng/parser/XDMParserExec;->xdmParParseExec(Lcom/policydm/eng/parser/XDMParser;)I

    goto/16 :goto_2

    .line 182
    :sswitch_d
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v2

    .line 183
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v2

    .line 185
    iput v2, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto/16 :goto_2

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_d
        0x5 -> :sswitch_4
        0x6 -> :sswitch_3
        0x8 -> :sswitch_8
        0xb -> :sswitch_0
        0xd -> :sswitch_7
        0x10 -> :sswitch_6
        0x11 -> :sswitch_c
        0x13 -> :sswitch_a
        0x18 -> :sswitch_9
        0x1a -> :sswitch_2
        0x1d -> :sswitch_1
        0x20 -> :sswitch_5
        0x2a -> :sswitch_b
    .end sparse-switch
.end method
