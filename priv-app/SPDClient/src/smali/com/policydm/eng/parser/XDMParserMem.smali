.class public Lcom/policydm/eng/parser/XDMParserMem;
.super Ljava/lang/Object;
.source "XDMParserMem.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public free:I

.field public freeid:I

.field public m_szShared:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public xdmParParseMem(Lcom/policydm/eng/parser/XDMParser;)I
    .locals 5
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    const/4 v3, 0x0

    .line 16
    const/4 v1, -0x1

    .line 17
    .local v1, "id":I
    const/4 v2, 0x0

    .line 19
    .local v2, "res":I
    const/16 v4, 0xd

    invoke-virtual {p1, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 20
    if-eqz v2, :cond_1

    move v3, v2

    .line 85
    :cond_0
    :goto_0
    return v3

    .line 25
    :cond_1
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 26
    const/16 v4, 0x8

    if-eq v2, v4, :cond_0

    .line 30
    if-eqz v2, :cond_2

    move v3, v2

    .line 32
    goto :goto_0

    .line 35
    :cond_2
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseSkipLiteralElement()I

    move-result v2

    .line 36
    if-eqz v2, :cond_3

    move v3, v2

    .line 38
    goto :goto_0

    .line 45
    :cond_3
    :try_start_0
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 52
    :goto_1
    const/4 v4, 0x1

    if-ne v1, v4, :cond_4

    .line 54
    invoke-virtual {p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v1

    .line 55
    goto :goto_0

    .line 47
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 58
    .end local v0    # "e":Ljava/io/IOException;
    :cond_4
    sparse-switch v1, :sswitch_data_0

    .line 76
    const/4 v2, 0x2

    .line 79
    :goto_2
    if-eqz v2, :cond_3

    move v3, v2

    .line 81
    goto :goto_0

    .line 61
    :sswitch_0
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 62
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    iput-object v4, p0, Lcom/policydm/eng/parser/XDMParserMem;->m_szShared:Ljava/lang/String;

    goto :goto_2

    .line 66
    :sswitch_1
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 67
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserMem;->free:I

    goto :goto_2

    .line 71
    :sswitch_2
    invoke-virtual {p1, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 72
    iget-object v4, p1, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/policydm/eng/parser/XDMParserMem;->freeid:I

    goto :goto_2

    .line 58
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_2
        0x9 -> :sswitch_1
        0x11 -> :sswitch_0
    .end sparse-switch
.end method
