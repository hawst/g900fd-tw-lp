.class public Lcom/policydm/eng/parser/XDMParser;
.super Lcom/policydm/eng/core/XDMWbxmlDecoder;
.source "XDMParser.java"


# instance fields
.field public Chal:Lcom/policydm/eng/parser/XDMParserMeta;

.field public Cred:Lcom/policydm/eng/parser/XDMParserCred;

.field public Meta:Lcom/policydm/eng/parser/XDMParserMeta;

.field public _pItem:Lcom/policydm/eng/parser/XDMParserItem;

.field public _pMapitem:Lcom/policydm/eng/parser/XDMParserMapItem;

.field public charset:I

.field public codePage:I

.field public in:Ljava/io/ByteArrayInputStream;

.field public m_szParserElement:Ljava/lang/String;

.field public m_szSource:Ljava/lang/String;

.field public m_szStringtable:Ljava/lang/String;

.field public m_szTarget:Ljava/lang/String;

.field public puid:I

.field public stsize:I

.field public userdata:Ljava/lang/Object;

.field public version:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->_pItem:Lcom/policydm/eng/parser/XDMParserItem;

    .line 24
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->_pMapitem:Lcom/policydm/eng/parser/XDMParserMapItem;

    .line 25
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->m_szTarget:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->m_szSource:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 29
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->Chal:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 30
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->Cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 42
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->wbxbuff:[B

    .line 43
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "buf"    # [B

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->_pItem:Lcom/policydm/eng/parser/XDMParserItem;

    .line 24
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->_pMapitem:Lcom/policydm/eng/parser/XDMParserMapItem;

    .line 25
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->m_szTarget:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->m_szSource:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 29
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->Chal:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 30
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->Cred:Lcom/policydm/eng/parser/XDMParserCred;

    .line 35
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->wbxbuff:[B

    .line 36
    iput-object p1, p0, Lcom/policydm/eng/parser/XDMParser;->wbxbuff:[B

    .line 37
    return-void
.end method


# virtual methods
.method public xdmHdlCmdSyncEnd(Ljava/lang/Object;I)V
    .locals 2
    .param p1, "userdata"    # Ljava/lang/Object;
    .param p2, "isfinal"    # I

    .prologue
    .line 342
    move-object v0, p1

    check-cast v0, Lcom/policydm/eng/core/XDMWorkspace;

    .line 343
    .local v0, "ws":Lcom/policydm/eng/core/XDMWorkspace;
    if-lez p2, :cond_0

    .line 345
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->isFinal:Z

    .line 352
    :goto_0
    return-void

    .line 349
    :cond_0
    const-string v1, "didn\'t catch FINAL"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 350
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/policydm/eng/core/XDMWorkspace;->isFinal:Z

    goto :goto_0
.end method

.method public xdmParDevinfo()I
    .locals 1

    .prologue
    .line 955
    const/4 v0, 0x0

    return v0
.end method

.method public xdmParParse()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 60
    const-string v4, "xdmParParse"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 61
    const/4 v2, 0x0

    .line 63
    .local v2, "result":I
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParser;->wbxbuff:[B

    if-nez v4, :cond_0

    .line 96
    :goto_0
    return v3

    .line 66
    :cond_0
    iput v3, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    .line 67
    iget-object v3, p0, Lcom/policydm/eng/parser/XDMParser;->wbxbuff:[B

    iget v4, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    invoke-virtual {p0, v3, v4}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecInit([BI)V

    .line 68
    invoke-virtual {p0, p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecParseStartdoc(Lcom/policydm/eng/parser/XDMParser;)V

    .line 70
    const/4 v1, -0x1

    .line 73
    .local v1, "id":I
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 80
    :goto_1
    const/16 v3, 0x2d

    if-eq v1, v3, :cond_1

    .line 82
    const-string v3, "not WBXML_TAG_SYNCML"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 83
    const/4 v3, 0x2

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 88
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseSyncml()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    :goto_2
    move v3, v2

    .line 96
    goto :goto_0

    .line 90
    :catch_1
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x1

    .line 93
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public xdmParParseBlankElement(I)I
    .locals 6
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x1

    .line 788
    const/4 v1, 0x0

    .line 793
    .local v1, "haveend":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I

    move-result v3

    .line 794
    .local v3, "tmp":I
    and-int/lit8 v5, v3, 0x40

    if-eqz v5, :cond_0

    .line 796
    const/4 v1, 0x1

    .line 799
    :cond_0
    invoke-virtual {p0, p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 800
    .local v2, "res":I
    if-eqz v2, :cond_2

    .line 819
    .end local v2    # "res":I
    .end local v3    # "tmp":I
    :cond_1
    :goto_0
    return v2

    .line 805
    .restart local v2    # "res":I
    .restart local v3    # "tmp":I
    :cond_2
    if-eqz v1, :cond_3

    .line 807
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 808
    if-nez v2, :cond_1

    .end local v2    # "res":I
    .end local v3    # "tmp":I
    :cond_3
    :goto_1
    move v2, v4

    .line 819
    goto :goto_0

    .line 814
    :catch_0
    move-exception v0

    .line 816
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public xdmParParseChal()I
    .locals 3

    .prologue
    .line 663
    new-instance v0, Lcom/policydm/eng/parser/XDMParserMeta;

    invoke-direct {v0}, Lcom/policydm/eng/parser/XDMParserMeta;-><init>()V

    .line 664
    .local v0, "meta":Lcom/policydm/eng/parser/XDMParserMeta;
    const/4 v1, 0x0

    .line 666
    .local v1, "res":I
    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v1

    .line 667
    if-eqz v1, :cond_0

    move v2, v1

    .line 697
    :goto_0
    return v2

    .line 672
    :cond_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v1

    .line 673
    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 675
    const/4 v2, 0x0

    goto :goto_0

    .line 677
    :cond_1
    if-eqz v1, :cond_2

    .line 679
    const-string v2, "not WBXML_ERR_OK"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v2, v1

    .line 680
    goto :goto_0

    .line 683
    :cond_2
    invoke-virtual {v0, p0}, Lcom/policydm/eng/parser/XDMParserMeta;->xdmParParseMeta(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v1

    .line 684
    if-eqz v1, :cond_3

    move v2, v1

    .line 686
    goto :goto_0

    .line 688
    :cond_3
    iget-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->Meta:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 689
    iput-object v0, p0, Lcom/policydm/eng/parser/XDMParser;->Chal:Lcom/policydm/eng/parser/XDMParserMeta;

    .line 691
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v1

    .line 692
    if-eqz v1, :cond_4

    move v2, v1

    .line 694
    goto :goto_0

    :cond_4
    move v2, v1

    .line 697
    goto :goto_0
.end method

.method public xdmParParseCheckElement(I)I
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 838
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v0

    .line 840
    .local v0, "e":I
    if-eq p1, v0, :cond_0

    .line 842
    const-string v1, "xdmParParseReadElement is WBXML_ERR_UNKNOWN_ELEMENT"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 843
    const/4 v1, 0x2

    .line 846
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public xdmParParseContent()Ljava/lang/String;
    .locals 5

    .prologue
    .line 578
    const/4 v3, 0x0

    .line 584
    .local v3, "szContent":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I

    move-result v1

    .line 585
    .local v1, "id":I
    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    .line 587
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecParseStr_i()Ljava/lang/String;

    move-result-object v3

    .end local v1    # "id":I
    :cond_0
    :goto_0
    move-object v4, v3

    .line 613
    :goto_1
    return-object v4

    .line 589
    .restart local v1    # "id":I
    :cond_1
    const/16 v4, 0x83

    if-ne v1, v4, :cond_2

    .line 591
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecParseStr_t()Ljava/lang/String;

    move-result-object v3

    .line 592
    goto :goto_0

    .line 593
    :cond_2
    const/16 v4, 0xc3

    if-ne v1, v4, :cond_3

    .line 595
    invoke-virtual {p0, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecParseExtension(I)Ljava/lang/String;

    move-result-object v3

    .line 596
    goto :goto_0

    .line 599
    :cond_3
    iget v4, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    .line 600
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseSkipElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 602
    .local v2, "res":I
    if-eqz v2, :cond_0

    .line 604
    const/4 v4, 0x0

    goto :goto_1

    .line 608
    .end local v1    # "id":I
    .end local v2    # "res":I
    :catch_0
    move-exception v0

    .line 610
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xdmParParseCurrentElement()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 824
    const/4 v0, 0x0

    .line 827
    .local v0, "cur":I
    iget-object v2, p0, Lcom/policydm/eng/parser/XDMParser;->wbxbuff:[B

    iget v3, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    aget-byte v2, v2, v3

    and-int/lit16 v1, v2, 0xff

    .line 828
    .local v1, "tmp":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 829
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Unexpected EOF"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 831
    :cond_0
    and-int/lit8 v2, v1, 0x3f

    and-int/lit8 v0, v2, 0x7f

    .line 832
    return v0
.end method

.method public xdmParParseElelist(ILcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;
    .locals 7
    .param p1, "eleid"    # I
    .param p2, "data"    # Lcom/policydm/eng/core/XDMList;

    .prologue
    .line 624
    const/4 v2, -0x1

    .line 626
    .local v2, "id":I
    const/4 v1, 0x0

    .line 627
    .local v1, "h":Lcom/policydm/eng/core/XDMList;
    const/4 v5, 0x0

    .line 633
    .local v5, "t":Lcom/policydm/eng/core/XDMList;
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 640
    :goto_1
    if-eq v2, p1, :cond_0

    .line 654
    .end local p2    # "data":Lcom/policydm/eng/core/XDMList;
    :goto_2
    return-object p2

    .line 635
    .restart local p2    # "data":Lcom/policydm/eng/core/XDMList;
    :catch_0
    move-exception v0

    .line 637
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 645
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v3

    .line 646
    .local v3, "res":I
    if-eqz v3, :cond_1

    .line 648
    const/4 p2, 0x0

    goto :goto_2

    .line 651
    :cond_1
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    .line 652
    .local v4, "szItem":Ljava/lang/String;
    invoke-static {v1, v5, v4}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object p2

    .line 629
    goto :goto_0
.end method

.method public xdmParParseElement(I)I
    .locals 8
    .param p1, "id"    # I

    .prologue
    const/4 v7, 0x1

    .line 707
    const/4 v5, 0x0

    .local v5, "szData":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    const-string v6, ""

    invoke-direct {v3, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 708
    .local v3, "result":Ljava/lang/String;
    const/4 v2, 0x0

    .line 709
    .local v2, "res":I
    const/4 v0, 0x1

    .line 710
    .local v0, "do_content":Z
    const/4 v4, 0x0

    .line 712
    .local v4, "szContent":Ljava/lang/String;
    const-string v6, ""

    iput-object v6, p0, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    .line 714
    invoke-virtual {p0, p1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 715
    if-eqz v2, :cond_0

    move v6, v2

    .line 776
    :goto_0
    return v6

    .line 720
    :cond_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 721
    const/16 v6, 0x8

    if-ne v2, v6, :cond_1

    .line 723
    const/4 v6, 0x0

    goto :goto_0

    .line 725
    :cond_1
    if-eqz v2, :cond_2

    .line 727
    const-string v6, "not WBXML_ERR_OK"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v6, v2

    .line 728
    goto :goto_0

    .line 731
    :cond_2
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseSkipLiteralElement()I

    move-result v2

    .line 732
    if-eqz v2, :cond_5

    move v6, v2

    .line 734
    goto :goto_0

    .line 740
    :cond_3
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseContent()Ljava/lang/String;

    move-result-object v5

    .line 741
    invoke-virtual {v3, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    .line 744
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I

    move-result p1

    .line 745
    const/16 v6, 0x83

    if-ne p1, v6, :cond_6

    .line 747
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecParseStr_t()Ljava/lang/String;

    move-result-object v4

    .line 748
    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    .line 749
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I

    move-result p1

    .line 750
    if-ne p1, v7, :cond_4

    .line 752
    iget v6, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 770
    :goto_1
    invoke-virtual {p0, v7}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 771
    if-eqz v2, :cond_7

    move v6, v2

    .line 773
    goto :goto_0

    .line 756
    :cond_4
    :try_start_1
    iget v6, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    .line 738
    :cond_5
    :goto_2
    if-nez v0, :cond_3

    goto :goto_1

    .line 760
    :cond_6
    iget v6, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 764
    :catch_0
    move-exception v1

    .line 766
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .end local v1    # "e":Ljava/io/IOException;
    :cond_7
    move v6, v2

    .line 776
    goto :goto_0
.end method

.method public xdmParParseInit(Lcom/policydm/eng/parser/XDMParser;Ljava/lang/Object;)V
    .locals 1
    .param p1, "p"    # Lcom/policydm/eng/parser/XDMParser;
    .param p2, "userdata"    # Ljava/lang/Object;

    .prologue
    .line 50
    const/4 v0, 0x0

    iput v0, p1, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    .line 51
    iput-object p2, p1, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public xdmParParseItemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;
    .locals 7
    .param p1, "itemlist"    # Lcom/policydm/eng/core/XDMList;

    .prologue
    .line 361
    const/4 v3, 0x0

    .line 362
    .local v3, "res":I
    move-object v1, p1

    .local v1, "h":Lcom/policydm/eng/core/XDMList;
    const/4 v4, 0x0

    .line 363
    .local v4, "t":Lcom/policydm/eng/core/XDMList;
    const/4 v2, 0x0

    .line 369
    .local v2, "id":I
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 376
    :goto_1
    const/16 v5, 0x14

    if-eq v2, v5, :cond_0

    .line 391
    .end local v1    # "h":Lcom/policydm/eng/core/XDMList;
    :goto_2
    return-object v1

    .line 371
    .restart local v1    # "h":Lcom/policydm/eng/core/XDMList;
    :catch_0
    move-exception v0

    .line 373
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 380
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    new-instance v5, Lcom/policydm/eng/parser/XDMParserItem;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserItem;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->_pItem:Lcom/policydm/eng/parser/XDMParserItem;

    .line 381
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->_pItem:Lcom/policydm/eng/parser/XDMParserItem;

    iget-object v6, p0, Lcom/policydm/eng/parser/XDMParser;->_pItem:Lcom/policydm/eng/parser/XDMParserItem;

    invoke-virtual {v5, p0, v6}, Lcom/policydm/eng/parser/XDMParserItem;->xdmParParseItem(Lcom/policydm/eng/parser/XDMParser;Lcom/policydm/eng/parser/XDMParserItem;)I

    move-result v3

    .line 383
    if-eqz v3, :cond_1

    .line 385
    const/4 v1, 0x0

    goto :goto_2

    .line 388
    :cond_1
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->_pItem:Lcom/policydm/eng/parser/XDMParserItem;

    invoke-static {v1, v4, v5}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 365
    goto :goto_0
.end method

.method public xdmParParseMapitemlist(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;
    .locals 7
    .param p1, "itemlist"    # Lcom/policydm/eng/core/XDMList;

    .prologue
    .line 401
    move-object v1, p1

    .local v1, "h":Lcom/policydm/eng/core/XDMList;
    const/4 v4, 0x0

    .line 402
    .local v4, "t":Lcom/policydm/eng/core/XDMList;
    const/4 v2, -0x1

    .line 403
    .local v2, "id":I
    const/4 v3, 0x0

    .line 409
    .local v3, "res":I
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 416
    :goto_1
    const/16 v5, 0x19

    if-eq v2, v5, :cond_0

    .line 432
    .end local v1    # "h":Lcom/policydm/eng/core/XDMList;
    :goto_2
    return-object v1

    .line 411
    .restart local v1    # "h":Lcom/policydm/eng/core/XDMList;
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 421
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    new-instance v5, Lcom/policydm/eng/parser/XDMParserMapItem;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserMapItem;-><init>()V

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->_pMapitem:Lcom/policydm/eng/parser/XDMParserMapItem;

    .line 422
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->_pMapitem:Lcom/policydm/eng/parser/XDMParserMapItem;

    iget-object v6, p0, Lcom/policydm/eng/parser/XDMParser;->_pMapitem:Lcom/policydm/eng/parser/XDMParserMapItem;

    invoke-virtual {v5, p0, v6}, Lcom/policydm/eng/parser/XDMParserMapItem;->xdmParParseMapitem(Lcom/policydm/eng/parser/XDMParser;Lcom/policydm/eng/parser/XDMParserMapItem;)I

    move-result v3

    .line 423
    if-eqz v3, :cond_1

    .line 425
    const/4 v1, 0x0

    goto :goto_2

    .line 429
    :cond_1
    iget-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->_pMapitem:Lcom/policydm/eng/parser/XDMParserMapItem;

    invoke-static {v1, v4, v5}, Lcom/policydm/eng/core/XDMList;->xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;

    move-result-object v1

    .line 405
    goto :goto_0
.end method

.method public xdmParParseReadElement()I
    .locals 4

    .prologue
    .line 179
    const/4 v1, -0x1

    .line 183
    .local v1, "id":I
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 184
    .local v2, "tmp":I
    and-int/lit8 v3, v2, 0x3f

    and-int/lit8 v1, v3, 0x7f

    .line 191
    .end local v2    # "tmp":I
    :goto_0
    return v1

    .line 186
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xdmParParseSkipElement()I
    .locals 4

    .prologue
    .line 876
    const/4 v1, 0x0

    .line 882
    .local v1, "level":I
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I

    move-result v2

    .line 883
    .local v2, "tmp":I
    if-nez v2, :cond_2

    .line 886
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I

    .line 887
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 922
    .end local v2    # "tmp":I
    :catch_0
    move-exception v0

    .line 924
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 926
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    const/4 v3, 0x0

    return v3

    .line 889
    .restart local v2    # "tmp":I
    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 891
    :try_start_1
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I

    .line 892
    add-int/lit8 v1, v1, -0x1

    .line 893
    if-nez v1, :cond_0

    .line 911
    :goto_1
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I

    move-result v2

    .line 912
    if-nez v2, :cond_1

    .line 917
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I

    .line 918
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I

    goto :goto_1

    .line 898
    :cond_3
    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    const/16 v3, 0x83

    if-eq v2, v3, :cond_4

    const/16 v3, 0xc3

    if-ne v2, v3, :cond_5

    .line 900
    :cond_4
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseContent()Ljava/lang/String;

    goto :goto_0

    .line 904
    :cond_5
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 905
    add-int/lit8 v1, v1, 0x1

    .line 880
    goto :goto_0
.end method

.method public xdmParParseSkipLiteralElement()I
    .locals 3

    .prologue
    .line 931
    const/4 v1, -0x1

    .line 936
    .local v1, "id":I
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I

    move-result v1

    .line 937
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 941
    :cond_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmWbxDecReadBufferByte()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 942
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 950
    :cond_1
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 945
    :catch_0
    move-exception v0

    .line 947
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xdmParParseSource()I
    .locals 6

    .prologue
    .line 509
    const/4 v4, 0x0

    .line 510
    .local v4, "szSource":Ljava/lang/String;
    const/4 v3, 0x0

    .line 511
    .local v3, "sourcename":[C
    const/4 v1, -0x1

    .line 514
    .local v1, "id":I
    const/16 v5, 0x27

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 515
    .local v2, "res":I
    if-eqz v2, :cond_0

    move v5, v2

    .line 568
    :goto_0
    return v5

    .line 520
    :cond_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 521
    const/16 v5, 0x8

    if-ne v2, v5, :cond_1

    .line 523
    const/4 v5, 0x0

    goto :goto_0

    .line 525
    :cond_1
    if-eqz v2, :cond_2

    .line 527
    const-string v5, "not WBXML_ERR_OK"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v5, v2

    .line 528
    goto :goto_0

    .line 531
    :cond_2
    const/16 v5, 0x17

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 532
    if-eqz v2, :cond_3

    move v5, v2

    .line 534
    goto :goto_0

    .line 537
    :cond_3
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    .line 540
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I

    move-result v1

    .line 542
    const/16 v5, 0x16

    if-ne v1, v5, :cond_4

    .line 544
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseSkipElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 552
    :cond_4
    :goto_1
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 553
    if-eqz v2, :cond_5

    move v5, v2

    .line 555
    goto :goto_0

    .line 547
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 558
    .end local v0    # "e":Ljava/io/IOException;
    :cond_5
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 560
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 561
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->m_szSource:Ljava/lang/String;

    :goto_2
    move v5, v2

    .line 568
    goto :goto_0

    .line 565
    :cond_6
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->m_szSource:Ljava/lang/String;

    goto :goto_2
.end method

.method public xdmParParseSyncbody()I
    .locals 23

    .prologue
    .line 200
    const/4 v11, -0x1

    .line 202
    .local v11, "id":I
    const/16 v20, 0x0

    .line 204
    .local v20, "tmp":I
    const/16 v21, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v15

    .line 205
    .local v15, "res":I
    if-eqz v15, :cond_0

    move/from16 v21, v15

    .line 332
    :goto_0
    return v21

    .line 210
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v15

    .line 211
    const/16 v21, 0x8

    move/from16 v0, v21

    if-ne v15, v0, :cond_1

    .line 213
    const/16 v21, 0x0

    goto :goto_0

    .line 215
    :cond_1
    if-eqz v15, :cond_2

    .line 217
    const-string v21, "not WBXML_ERR_OK"

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move/from16 v21, v15

    .line 218
    goto :goto_0

    .line 225
    :cond_2
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v11

    .line 232
    :goto_1
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v11, v0, :cond_3

    .line 234
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v11

    .line 329
    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "xdmParParseSyncbody end tmp = "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/eng/parser/XDMParser;->userdata:Ljava/lang/Object;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/policydm/eng/parser/XDMParser;->xdmHdlCmdSyncEnd(Ljava/lang/Object;I)V

    .line 332
    const/16 v21, 0x0

    goto :goto_0

    .line 227
    :catch_0
    move-exception v8

    .line 229
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 238
    .end local v8    # "e":Ljava/io/IOException;
    :cond_3
    sparse-switch v11, :sswitch_data_0

    .line 320
    const/4 v15, 0x2

    .line 324
    :goto_2
    if-eqz v15, :cond_2

    move/from16 v21, v15

    .line 326
    goto :goto_0

    .line 241
    :sswitch_0
    new-instance v4, Lcom/policydm/eng/parser/XDMParserAlert;

    invoke-direct {v4}, Lcom/policydm/eng/parser/XDMParserAlert;-><init>()V

    .line 242
    .local v4, "alert":Lcom/policydm/eng/parser/XDMParserAlert;
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/policydm/eng/parser/XDMParserAlert;->xdmParParseAlert(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 243
    goto :goto_2

    .line 246
    .end local v4    # "alert":Lcom/policydm/eng/parser/XDMParserAlert;
    :sswitch_1
    new-instance v3, Lcom/policydm/eng/parser/XDMParserAdd;

    invoke-direct {v3}, Lcom/policydm/eng/parser/XDMParserAdd;-><init>()V

    .line 247
    .local v3, "add":Lcom/policydm/eng/parser/XDMParserAdd;
    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/policydm/eng/parser/XDMParserAdd;->xdmParParseAdd(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 248
    goto :goto_2

    .line 251
    .end local v3    # "add":Lcom/policydm/eng/parser/XDMParserAdd;
    :sswitch_2
    new-instance v14, Lcom/policydm/eng/parser/XDMParserReplace;

    invoke-direct {v14}, Lcom/policydm/eng/parser/XDMParserReplace;-><init>()V

    .line 252
    .local v14, "replace":Lcom/policydm/eng/parser/XDMParserReplace;
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/policydm/eng/parser/XDMParserReplace;->xdmParParseReplace(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 253
    goto :goto_2

    .line 256
    .end local v14    # "replace":Lcom/policydm/eng/parser/XDMParserReplace;
    :sswitch_3
    new-instance v10, Lcom/policydm/eng/parser/XDMParserGet;

    invoke-direct {v10}, Lcom/policydm/eng/parser/XDMParserGet;-><init>()V

    .line 257
    .local v10, "get":Lcom/policydm/eng/parser/XDMParserGet;
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Lcom/policydm/eng/parser/XDMParserGet;->xdmParParseGet(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 258
    goto :goto_2

    .line 261
    .end local v10    # "get":Lcom/policydm/eng/parser/XDMParserGet;
    :sswitch_4
    new-instance v12, Lcom/policydm/eng/parser/XDMParserMap;

    invoke-direct {v12}, Lcom/policydm/eng/parser/XDMParserMap;-><init>()V

    .line 262
    .local v12, "map":Lcom/policydm/eng/parser/XDMParserMap;
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/policydm/eng/parser/XDMParserMap;->xdmParParseMap(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 263
    goto :goto_2

    .line 266
    .end local v12    # "map":Lcom/policydm/eng/parser/XDMParserMap;
    :sswitch_5
    new-instance v13, Lcom/policydm/eng/parser/XDMParserPut;

    invoke-direct {v13}, Lcom/policydm/eng/parser/XDMParserPut;-><init>()V

    .line 267
    .local v13, "put":Lcom/policydm/eng/parser/XDMParserPut;
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/policydm/eng/parser/XDMParserPut;->xdmParParsePut(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 268
    goto :goto_2

    .line 271
    .end local v13    # "put":Lcom/policydm/eng/parser/XDMParserPut;
    :sswitch_6
    new-instance v16, Lcom/policydm/eng/parser/XDMParserResults;

    invoke-direct/range {v16 .. v16}, Lcom/policydm/eng/parser/XDMParserResults;-><init>()V

    .line 272
    .local v16, "results":Lcom/policydm/eng/parser/XDMParserResults;
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/policydm/eng/parser/XDMParserResults;->xdmParParseResults(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 273
    goto :goto_2

    .line 276
    .end local v16    # "results":Lcom/policydm/eng/parser/XDMParserResults;
    :sswitch_7
    new-instance v18, Lcom/policydm/eng/parser/XDMParserStatus;

    invoke-direct/range {v18 .. v18}, Lcom/policydm/eng/parser/XDMParserStatus;-><init>()V

    .line 277
    .local v18, "status":Lcom/policydm/eng/parser/XDMParserStatus;
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/policydm/eng/parser/XDMParserStatus;->xdmParParseStatus(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 278
    goto :goto_2

    .line 281
    .end local v18    # "status":Lcom/policydm/eng/parser/XDMParserStatus;
    :sswitch_8
    new-instance v5, Lcom/policydm/eng/parser/XDMParserAtomic;

    invoke-direct {v5}, Lcom/policydm/eng/parser/XDMParserAtomic;-><init>()V

    .line 282
    .local v5, "atomic":Lcom/policydm/eng/parser/XDMParserAtomic;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/policydm/eng/parser/XDMParserAtomic;->xdmParParseAtomic(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 283
    goto :goto_2

    .line 285
    .end local v5    # "atomic":Lcom/policydm/eng/parser/XDMParserAtomic;
    :sswitch_9
    new-instance v17, Lcom/policydm/eng/parser/XDMParserSequence;

    invoke-direct/range {v17 .. v17}, Lcom/policydm/eng/parser/XDMParserSequence;-><init>()V

    .line 286
    .local v17, "sequence":Lcom/policydm/eng/parser/XDMParserSequence;
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/policydm/eng/parser/XDMParserSequence;->xdmParParseSequence(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 287
    goto/16 :goto_2

    .line 289
    .end local v17    # "sequence":Lcom/policydm/eng/parser/XDMParserSequence;
    :sswitch_a
    new-instance v19, Lcom/policydm/eng/parser/XDMParserSync;

    invoke-direct/range {v19 .. v19}, Lcom/policydm/eng/parser/XDMParserSync;-><init>()V

    .line 290
    .local v19, "sync":Lcom/policydm/eng/parser/XDMParserSync;
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/policydm/eng/parser/XDMParserSync;->xdmParParseSync(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 291
    goto/16 :goto_2

    .line 294
    .end local v19    # "sync":Lcom/policydm/eng/parser/XDMParserSync;
    :sswitch_b
    new-instance v7, Lcom/policydm/eng/parser/XDMParserDelete;

    invoke-direct {v7}, Lcom/policydm/eng/parser/XDMParserDelete;-><init>()V

    .line 295
    .local v7, "delete":Lcom/policydm/eng/parser/XDMParserDelete;
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Lcom/policydm/eng/parser/XDMParserDelete;->xdmParParseDelete(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 296
    goto/16 :goto_2

    .line 299
    .end local v7    # "delete":Lcom/policydm/eng/parser/XDMParserDelete;
    :sswitch_c
    new-instance v6, Lcom/policydm/eng/parser/XDMParserCopy;

    invoke-direct {v6}, Lcom/policydm/eng/parser/XDMParserCopy;-><init>()V

    .line 300
    .local v6, "copy":Lcom/policydm/eng/parser/XDMParserCopy;
    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Lcom/policydm/eng/parser/XDMParserCopy;->xdmParParseCopy(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 301
    goto/16 :goto_2

    .line 304
    .end local v6    # "copy":Lcom/policydm/eng/parser/XDMParserCopy;
    :sswitch_d
    new-instance v9, Lcom/policydm/eng/parser/XDMParserExec;

    invoke-direct {v9}, Lcom/policydm/eng/parser/XDMParserExec;-><init>()V

    .line 305
    .local v9, "exec":Lcom/policydm/eng/parser/XDMParserExec;
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Lcom/policydm/eng/parser/XDMParserExec;->xdmParParseExec(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v15

    .line 306
    goto/16 :goto_2

    .line 309
    .end local v9    # "exec":Lcom/policydm/eng/parser/XDMParserExec;
    :sswitch_e
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseBlankElement(I)I

    move-result v20

    .line 310
    goto/16 :goto_2

    .line 313
    :sswitch_f
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v11

    .line 314
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v11

    .line 316
    move-object/from16 v0, p0

    iput v11, v0, Lcom/policydm/eng/parser/XDMParser;->codePage:I

    goto/16 :goto_2

    .line 238
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_f
        0x5 -> :sswitch_1
        0x6 -> :sswitch_0
        0x8 -> :sswitch_8
        0xd -> :sswitch_c
        0x10 -> :sswitch_b
        0x11 -> :sswitch_d
        0x12 -> :sswitch_e
        0x13 -> :sswitch_3
        0x18 -> :sswitch_4
        0x1f -> :sswitch_5
        0x20 -> :sswitch_2
        0x22 -> :sswitch_6
        0x24 -> :sswitch_9
        0x29 -> :sswitch_7
        0x2a -> :sswitch_a
    .end sparse-switch
.end method

.method public xdmParParseSyncml()I
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 105
    const/4 v2, -0x1

    .line 108
    .local v2, "id":I
    const/16 v5, 0x2d

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v3

    .line 109
    .local v3, "res":I
    if-eqz v3, :cond_1

    .line 111
    const-string v4, "not WBXML_ERR_OK"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v4, v3

    .line 169
    :cond_0
    :goto_0
    return v4

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v3

    .line 116
    const/16 v5, 0x8

    if-eq v3, v5, :cond_0

    .line 120
    if-eqz v3, :cond_2

    .line 122
    const-string v4, "not WBXML_ERR_OK"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v4, v3

    .line 123
    goto :goto_0

    .line 130
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 137
    :goto_1
    const/4 v5, 0x1

    if-eq v2, v5, :cond_0

    .line 142
    sparse-switch v2, :sswitch_data_0

    .line 159
    const/4 v3, 0x2

    .line 163
    :goto_2
    if-eqz v3, :cond_2

    move v4, v3

    .line 165
    goto :goto_0

    .line 132
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 145
    .end local v0    # "e":Ljava/io/IOException;
    :sswitch_0
    new-instance v1, Lcom/policydm/eng/parser/XDMParserSyncheader;

    invoke-direct {v1}, Lcom/policydm/eng/parser/XDMParserSyncheader;-><init>()V

    .line 146
    .local v1, "header":Lcom/policydm/eng/parser/XDMParserSyncheader;
    invoke-virtual {v1, p0}, Lcom/policydm/eng/parser/XDMParserSyncheader;->xdmParParseSyncheader(Lcom/policydm/eng/parser/XDMParser;)I

    move-result v3

    .line 147
    goto :goto_2

    .line 150
    .end local v1    # "header":Lcom/policydm/eng/parser/XDMParserSyncheader;
    :sswitch_1
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseSyncbody()I

    move-result v3

    .line 151
    goto :goto_2

    .line 154
    :sswitch_2
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v2

    .line 155
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseReadElement()I

    move-result v2

    .line 156
    goto :goto_2

    .line 142
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x2b -> :sswitch_1
        0x2c -> :sswitch_0
    .end sparse-switch
.end method

.method public xdmParParseTarget()I
    .locals 6

    .prologue
    .line 441
    const/4 v2, 0x0

    .line 442
    .local v2, "res":I
    const/4 v3, 0x0

    .line 443
    .local v3, "szTarget":Ljava/lang/String;
    const/4 v4, 0x0

    .line 444
    .local v4, "targetname":[C
    const/4 v1, -0x1

    .line 446
    .local v1, "id":I
    const/16 v5, 0x2e

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 448
    if-eqz v2, :cond_0

    move v5, v2

    .line 500
    :goto_0
    return v5

    .line 453
    :cond_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseZeroBitTagCheck()I

    move-result v2

    .line 454
    const/16 v5, 0x8

    if-ne v2, v5, :cond_1

    .line 456
    const/4 v5, 0x0

    goto :goto_0

    .line 458
    :cond_1
    if-eqz v2, :cond_2

    .line 460
    const-string v5, "not WBXML_ERR_OK"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v5, v2

    .line 461
    goto :goto_0

    .line 464
    :cond_2
    const/16 v5, 0x17

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseElement(I)I

    move-result v2

    .line 465
    if-eqz v2, :cond_3

    move v5, v2

    .line 467
    goto :goto_0

    .line 469
    :cond_3
    iget-object v3, p0, Lcom/policydm/eng/parser/XDMParser;->m_szParserElement:Ljava/lang/String;

    .line 473
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCurrentElement()I

    move-result v1

    .line 475
    const/16 v5, 0x16

    if-ne v1, v5, :cond_4

    .line 477
    invoke-virtual {p0}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseSkipElement()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 486
    :cond_4
    :goto_1
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/policydm/eng/parser/XDMParser;->xdmParParseCheckElement(I)I

    move-result v2

    .line 487
    if-eqz v2, :cond_5

    move v5, v2

    .line 489
    goto :goto_0

    .line 480
    :catch_0
    move-exception v0

    .line 482
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 492
    .end local v0    # "e":Ljava/io/IOException;
    :cond_5
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 494
    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .line 495
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->m_szTarget:Ljava/lang/String;

    :goto_2
    move v5, v2

    .line 500
    goto :goto_0

    .line 498
    :cond_6
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/policydm/eng/parser/XDMParser;->m_szTarget:Ljava/lang/String;

    goto :goto_2
.end method

.method public xdmParParseZeroBitTagCheck()I
    .locals 6

    .prologue
    .line 851
    const/4 v2, 0x0

    .line 852
    .local v2, "ret":I
    const/4 v1, 0x0

    .line 853
    .local v1, "id":I
    iget-object v4, p0, Lcom/policydm/eng/parser/XDMParser;->wbxbuff:[B

    iget v5, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    aget-byte v4, v4, v5

    and-int/lit16 v0, v4, 0xff

    .line 854
    .local v0, "data":I
    const/4 v3, 0x0

    .line 855
    .local v3, "zerobit":I
    const/4 v4, -0x1

    if-ne v0, v4, :cond_0

    .line 856
    const/4 v4, 0x1

    .line 870
    :goto_0
    return v4

    .line 858
    :cond_0
    and-int/lit8 v4, v0, 0x3f

    and-int/lit8 v1, v4, 0x7f

    .line 860
    const/4 v4, 0x5

    if-lt v1, v4, :cond_1

    const/16 v4, 0x3c

    if-gt v1, v4, :cond_1

    .line 862
    and-int/lit8 v3, v0, 0x40

    .line 863
    if-nez v3, :cond_1

    .line 865
    const-string v4, "WBXML_ERR_ZEROBIT_TAG"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 866
    const/16 v2, 0x8

    .line 869
    :cond_1
    iget v4, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/policydm/eng/parser/XDMParser;->wbxindex:I

    move v4, v2

    .line 870
    goto :goto_0
.end method
