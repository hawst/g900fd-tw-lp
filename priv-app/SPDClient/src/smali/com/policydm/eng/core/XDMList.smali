.class public Lcom/policydm/eng/core/XDMList;
.super Ljava/lang/Object;
.source "XDMList.java"


# instance fields
.field public item:Ljava/lang/Object;

.field public next:Lcom/policydm/eng/core/XDMList;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmListAppend(Lcom/policydm/eng/core/XDMList;Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;
    .locals 2
    .param p0, "header"    # Lcom/policydm/eng/core/XDMList;
    .param p1, "tail"    # Lcom/policydm/eng/core/XDMList;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 17
    const/4 v0, 0x0

    .line 19
    .local v0, "tmp":Lcom/policydm/eng/core/XDMList;
    new-instance v0, Lcom/policydm/eng/core/XDMList;

    .end local v0    # "tmp":Lcom/policydm/eng/core/XDMList;
    invoke-direct {v0}, Lcom/policydm/eng/core/XDMList;-><init>()V

    .line 21
    .restart local v0    # "tmp":Lcom/policydm/eng/core/XDMList;
    iput-object p2, v0, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .line 22
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 24
    if-nez p0, :cond_0

    .line 26
    move-object p0, v0

    .line 27
    move-object p1, p0

    .line 42
    :goto_0
    return-object p0

    .line 31
    :cond_0
    if-nez p1, :cond_1

    .line 33
    move-object p1, p0

    .line 34
    :goto_1
    iget-object v1, p1, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    if-nez v1, :cond_2

    .line 38
    :cond_1
    iput-object v0, p1, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 39
    move-object p1, v0

    goto :goto_0

    .line 35
    :cond_2
    iget-object p1, p1, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    goto :goto_1
.end method

.method public static xdmListAppendStrText(Lcom/policydm/eng/core/XDMText;Ljava/lang/String;)Lcom/policydm/eng/core/XDMText;
    .locals 3
    .param p0, "target"    # Lcom/policydm/eng/core/XDMText;
    .param p1, "szAppendText"    # Ljava/lang/String;

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "len":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 145
    iget v1, p0, Lcom/policydm/eng/core/XDMText;->len:I

    add-int/2addr v1, v0

    invoke-static {p0, v1}, Lcom/policydm/eng/core/XDMList;->xdmListVerifyTextSize(Lcom/policydm/eng/core/XDMText;I)Lcom/policydm/eng/core/XDMText;

    move-result-object p0

    .line 146
    iget-object v1, p0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 148
    iget v1, p0, Lcom/policydm/eng/core/XDMText;->len:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/policydm/eng/core/XDMText;->len:I

    .line 149
    return-object p0
.end method

.method public static xdmListAppendText(Lcom/policydm/eng/core/XDMText;Lcom/policydm/eng/core/XDMText;)Lcom/policydm/eng/core/XDMText;
    .locals 2
    .param p0, "target"    # Lcom/policydm/eng/core/XDMText;
    .param p1, "tail"    # Lcom/policydm/eng/core/XDMText;

    .prologue
    .line 192
    iget v0, p0, Lcom/policydm/eng/core/XDMText;->len:I

    iget v1, p1, Lcom/policydm/eng/core/XDMText;->len:I

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Lcom/policydm/eng/core/XDMList;->xdmListVerifyTextSize(Lcom/policydm/eng/core/XDMText;I)Lcom/policydm/eng/core/XDMText;

    move-result-object p0

    .line 194
    iget-object v0, p0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 195
    iget v0, p0, Lcom/policydm/eng/core/XDMText;->len:I

    iget v1, p1, Lcom/policydm/eng/core/XDMText;->len:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/policydm/eng/core/XDMText;->len:I

    .line 197
    return-object p0
.end method

.method public static xdmListCopyStrText(Lcom/policydm/eng/core/XDMText;Ljava/lang/String;)Lcom/policydm/eng/core/XDMText;
    .locals 2
    .param p0, "target"    # Lcom/policydm/eng/core/XDMText;
    .param p1, "szCopyText"    # Ljava/lang/String;

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "len":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 157
    iget v1, p0, Lcom/policydm/eng/core/XDMText;->len:I

    add-int/2addr v1, v0

    invoke-static {p0, v1}, Lcom/policydm/eng/core/XDMList;->xdmListVerifyTextSize(Lcom/policydm/eng/core/XDMText;I)Lcom/policydm/eng/core/XDMText;

    move-result-object p0

    .line 158
    iput-object p1, p0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 160
    iput v0, p0, Lcom/policydm/eng/core/XDMText;->len:I

    .line 161
    return-object p0
.end method

.method public static xdmListCreateText(ILjava/lang/Object;)Lcom/policydm/eng/core/XDMText;
    .locals 2
    .param p0, "size"    # I
    .param p1, "initText"    # Ljava/lang/Object;

    .prologue
    .line 117
    new-instance v0, Lcom/policydm/eng/core/XDMText;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMText;-><init>()V

    .line 119
    .local v0, "text":Lcom/policydm/eng/core/XDMText;
    if-eqz p1, :cond_0

    .line 121
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 122
    iget-object v1, v0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, v0, Lcom/policydm/eng/core/XDMText;->len:I

    .line 123
    iget v1, v0, Lcom/policydm/eng/core/XDMText;->len:I

    iput v1, v0, Lcom/policydm/eng/core/XDMText;->size:I

    .line 131
    :goto_0
    return-object v0

    .line 127
    :cond_0
    const-string v1, ""

    iput-object v1, v0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 128
    iput p0, v0, Lcom/policydm/eng/core/XDMText;->size:I

    .line 129
    const/4 v1, 0x0

    iput v1, v0, Lcom/policydm/eng/core/XDMText;->len:I

    goto :goto_0
.end method

.method public static xdmListGetItem(Lcom/policydm/eng/core/XDMList;)Ljava/lang/Object;
    .locals 1
    .param p0, "header"    # Lcom/policydm/eng/core/XDMList;

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 80
    .local v0, "item":Ljava/lang/Object;
    if-nez p0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 88
    .end local v0    # "item":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 85
    .restart local v0    # "item":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .line 88
    goto :goto_0
.end method

.method public static xdmListGetItemPtr(Lcom/policydm/eng/core/XDMList;)Lcom/policydm/eng/core/XDMList;
    .locals 0
    .param p0, "header"    # Lcom/policydm/eng/core/XDMList;

    .prologue
    .line 98
    if-nez p0, :cond_0

    .line 100
    const/4 p0, 0x0

    .line 105
    :goto_0
    return-object p0

    .line 103
    :cond_0
    iget-object p0, p0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 105
    goto :goto_0
.end method

.method public static xdmListVerifyTextSize(Lcom/policydm/eng/core/XDMText;I)Lcom/policydm/eng/core/XDMText;
    .locals 2
    .param p0, "text"    # Lcom/policydm/eng/core/XDMText;
    .param p1, "size"    # I

    .prologue
    .line 172
    iget v1, p0, Lcom/policydm/eng/core/XDMText;->size:I

    if-ge v1, p1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 175
    .local v0, "szOld":Ljava/lang/String;
    const-string v1, ""

    iput-object v1, p0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 176
    iput p1, p0, Lcom/policydm/eng/core/XDMText;->size:I

    .line 177
    iput-object v0, p0, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 180
    .end local v0    # "szOld":Ljava/lang/String;
    :cond_0
    return-object p0
.end method


# virtual methods
.method public xdmListAppend2(Lcom/policydm/eng/core/XDMList;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMList;
    .locals 3
    .param p1, "list"    # Lcom/policydm/eng/core/XDMList;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 55
    new-instance v0, Lcom/policydm/eng/core/XDMList;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMList;-><init>()V

    .line 57
    .local v0, "entry":Lcom/policydm/eng/core/XDMList;
    iput-object p2, v0, Lcom/policydm/eng/core/XDMList;->item:Ljava/lang/Object;

    .line 58
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    .line 60
    if-nez p1, :cond_0

    .line 68
    .end local v0    # "entry":Lcom/policydm/eng/core/XDMList;
    :goto_0
    return-object v0

    .line 63
    .restart local v0    # "entry":Lcom/policydm/eng/core/XDMList;
    :cond_0
    move-object v1, p1

    .line 64
    .local v1, "prev":Lcom/policydm/eng/core/XDMList;
    :goto_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    if-nez v2, :cond_1

    .line 67
    iput-object v0, v1, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    move-object v0, p1

    .line 68
    goto :goto_0

    .line 65
    :cond_1
    iget-object v1, v1, Lcom/policydm/eng/core/XDMList;->next:Lcom/policydm/eng/core/XDMList;

    goto :goto_1
.end method
