.class public Lcom/policydm/eng/core/XDMMsg;
.super Ljava/lang/Object;
.source "XDMMsg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;,
        Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    }
.end annotation


# static fields
.field private static final rcMsgQueueObj:Ljava/lang/Object;

.field private static final syncMsgQueueObj:Ljava/lang/Object;

.field private static final syncUIMsgQueueObj:Ljava/lang/Object;


# instance fields
.field public msgItem:Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/policydm/eng/core/XDMMsg;->syncMsgQueueObj:Ljava/lang/Object;

    .line 15
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/policydm/eng/core/XDMMsg;->syncUIMsgQueueObj:Ljava/lang/Object;

    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/policydm/eng/core/XDMMsg;->rcMsgQueueObj:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMMsg;->msgItem:Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;

    .line 21
    return-void
.end method

.method public static xdmCreateAbortMessage(IZ)Lcom/policydm/eng/core/XDMAbortMsgParam;
    .locals 1
    .param p0, "abortCode"    # I
    .param p1, "userReq"    # Z

    .prologue
    .line 259
    const/4 v0, 0x0

    .line 261
    .local v0, "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    new-instance v0, Lcom/policydm/eng/core/XDMAbortMsgParam;

    .end local v0    # "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    invoke-direct {v0}, Lcom/policydm/eng/core/XDMAbortMsgParam;-><init>()V

    .line 262
    .restart local v0    # "pAbortParam":Lcom/policydm/eng/core/XDMAbortMsgParam;
    iput p0, v0, Lcom/policydm/eng/core/XDMAbortMsgParam;->abortCode:I

    .line 263
    iput-boolean p1, v0, Lcom/policydm/eng/core/XDMAbortMsgParam;->userReq:Z

    .line 264
    return-object v0
.end method

.method public static xdmSendMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10
    .param p0, "type"    # I
    .param p1, "param"    # Ljava/lang/Object;
    .param p2, "paramFree"    # Ljava/lang/Object;

    .prologue
    .line 44
    const/4 v5, 0x0

    .line 45
    .local v5, "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    const/4 v3, 0x0

    .line 47
    .local v3, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    if-eqz p1, :cond_0

    .line 49
    new-instance v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    .end local v5    # "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    invoke-direct {v5}, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;-><init>()V

    .line 50
    .restart local v5    # "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    iput-object p1, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    .line 52
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 53
    if-eqz p2, :cond_0

    .line 55
    iput-object p2, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 59
    :cond_0
    sget-object v7, Lcom/policydm/eng/core/XDMMsg;->syncMsgQueueObj:Ljava/lang/Object;

    monitor-enter v7

    .line 61
    :try_start_0
    new-instance v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;

    invoke-direct {v4}, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 63
    .end local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .local v4, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :try_start_1
    sget-object v6, Lcom/policydm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_1

    .line 66
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x5

    if-lt v1, v6, :cond_2

    .line 87
    .end local v1    # "i":I
    :cond_1
    if-eqz v4, :cond_3

    .line 89
    :try_start_2
    iput p0, v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->type:I

    .line 90
    iput-object v5, v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    .line 92
    sget-object v6, Lcom/policydm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 93
    .local v2, "msg":Landroid/os/Message;
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 94
    sget-object v6, Lcom/policydm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 59
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 106
    return-void

    .line 70
    .restart local v1    # "i":I
    :cond_2
    :try_start_4
    const-string v6, "waiting for DM_TaskHandler create"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 71
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 78
    :goto_2
    :try_start_5
    sget-object v6, Lcom/policydm/agent/XDMTask;->g_hDmTask:Landroid/os/Handler;

    if-nez v6, :cond_1

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 59
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    :catchall_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :goto_3
    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 98
    .end local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :cond_3
    :try_start_7
    const-string v6, "Can\'t send message"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 101
    :catch_1
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v6, "Can\'t send message"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 59
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :catchall_1
    move-exception v6

    goto :goto_3
.end method

.method public static xdmSendRCMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10
    .param p0, "type"    # I
    .param p1, "param"    # Ljava/lang/Object;
    .param p2, "paramFree"    # Ljava/lang/Object;

    .prologue
    .line 188
    const/4 v5, 0x0

    .line 189
    .local v5, "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    const/4 v3, 0x0

    .line 191
    .local v3, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    if-eqz p1, :cond_0

    .line 193
    new-instance v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    .end local v5    # "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    invoke-direct {v5}, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;-><init>()V

    .line 194
    .restart local v5    # "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    iput-object p1, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    .line 196
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 197
    if-eqz p2, :cond_0

    .line 199
    iput-object p2, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 203
    :cond_0
    sget-object v7, Lcom/policydm/eng/core/XDMMsg;->rcMsgQueueObj:Ljava/lang/Object;

    monitor-enter v7

    .line 205
    :try_start_0
    new-instance v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;

    invoke-direct {v4}, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 207
    .end local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .local v4, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :try_start_1
    sget-object v6, Lcom/policydm/restclient/XRCProcess;->hRestClient:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_1

    .line 209
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x5

    if-lt v1, v6, :cond_2

    .line 230
    .end local v1    # "i":I
    :cond_1
    if-eqz v4, :cond_3

    .line 232
    :try_start_2
    iput p0, v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->type:I

    .line 233
    iput-object v5, v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    .line 235
    sget-object v6, Lcom/policydm/restclient/XRCProcess;->hRestClient:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 236
    .local v2, "msg":Landroid/os/Message;
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 237
    sget-object v6, Lcom/policydm/restclient/XRCProcess;->hRestClient:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 203
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 249
    return-void

    .line 213
    .restart local v1    # "i":I
    :cond_2
    :try_start_4
    const-string v6, "waiting for hRestClient create"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 214
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 221
    :goto_2
    :try_start_5
    sget-object v6, Lcom/policydm/restclient/XRCProcess;->hRestClient:Landroid/os/Handler;

    if-nez v6, :cond_1

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 203
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    :catchall_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :goto_3
    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 241
    .end local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :cond_3
    :try_start_7
    const-string v6, "Can\'t send rc message"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 244
    :catch_1
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v6, "Can\'t send rc message"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 203
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :catchall_1
    move-exception v6

    goto :goto_3
.end method

.method public static xdmSendUIMessage(ILjava/lang/Object;Ljava/lang/Object;)V
    .locals 10
    .param p0, "type"    # I
    .param p1, "param"    # Ljava/lang/Object;
    .param p2, "paramFree"    # Ljava/lang/Object;

    .prologue
    .line 116
    const/4 v5, 0x0

    .line 117
    .local v5, "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    const/4 v3, 0x0

    .line 119
    .local v3, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    if-eqz p1, :cond_0

    .line 121
    new-instance v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    .end local v5    # "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    invoke-direct {v5}, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;-><init>()V

    .line 122
    .restart local v5    # "msgParam":Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;
    iput-object p1, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->param:Ljava/lang/Object;

    .line 124
    const/4 v6, 0x0

    iput-object v6, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 125
    if-eqz p2, :cond_0

    .line 127
    iput-object p2, v5, Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;->paramFree:Ljava/lang/Object;

    .line 131
    :cond_0
    sget-object v7, Lcom/policydm/eng/core/XDMMsg;->syncUIMsgQueueObj:Ljava/lang/Object;

    monitor-enter v7

    .line 133
    :try_start_0
    new-instance v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;

    invoke-direct {v4}, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 135
    .end local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .local v4, "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :try_start_1
    sget-object v6, Lcom/policydm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v6, :cond_1

    .line 138
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v6, 0x5

    if-lt v1, v6, :cond_2

    .line 159
    .end local v1    # "i":I
    :cond_1
    if-eqz v4, :cond_3

    .line 161
    :try_start_2
    iput p0, v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->type:I

    .line 162
    iput-object v5, v4, Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;->param:Lcom/policydm/eng/core/XDMMsg$XDMMsgParam;

    .line 164
    sget-object v6, Lcom/policydm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 165
    .local v2, "msg":Landroid/os/Message;
    iput-object v4, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 166
    sget-object v6, Lcom/policydm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 131
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 178
    return-void

    .line 142
    .restart local v1    # "i":I
    :cond_2
    :try_start_4
    const-string v6, "waiting for hUITask create"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 143
    const-wide/16 v8, 0x1f4

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 150
    :goto_2
    :try_start_5
    sget-object v6, Lcom/policydm/agent/XDMUITask;->g_hDmUiTask:Landroid/os/Handler;

    if-nez v6, :cond_1

    .line 138
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 131
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    :catchall_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :goto_3
    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v6

    .line 170
    .end local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :cond_3
    :try_start_7
    const-string v6, "Can\'t send UI message"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 173
    :catch_1
    move-exception v0

    .line 175
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v6, "Can\'t send UI message"

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 131
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    .restart local v3    # "msgItem":Lcom/policydm/eng/core/XDMMsg$XDMMsgItem;
    :catchall_1
    move-exception v6

    goto :goto_3
.end method
