.class public Lcom/policydm/eng/core/XDMOmList;
.super Ljava/lang/Object;
.source "XDMOmList.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;


# instance fields
.field public data:Ljava/lang/Object;

.field public next:Lcom/policydm/eng/core/XDMOmList;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmOmDeleteAclList(Lcom/policydm/eng/core/XDMOmList;)V
    .locals 2
    .param p0, "h"    # Lcom/policydm/eng/core/XDMOmList;

    .prologue
    .line 17
    move-object v0, p0

    .line 19
    .local v0, "cur":Lcom/policydm/eng/core/XDMOmList;
    :goto_0
    if-nez v0, :cond_0

    .line 26
    return-void

    .line 21
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 23
    .local v1, "next":Lcom/policydm/eng/core/XDMOmList;
    const/4 v0, 0x0

    .line 24
    move-object v0, v1

    goto :goto_0
.end method

.method public static xdmOmGetFormatFromString(Ljava/lang/String;)I
    .locals 1
    .param p0, "szData"    # Ljava/lang/String;

    .prologue
    .line 108
    const-string v0, "b64"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x1

    .line 132
    :goto_0
    return v0

    .line 110
    :cond_0
    const-string v0, "bin"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 111
    const/4 v0, 0x2

    goto :goto_0

    .line 112
    :cond_1
    const-string v0, "bool"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 113
    const/4 v0, 0x3

    goto :goto_0

    .line 114
    :cond_2
    const-string v0, "chr"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 115
    const/4 v0, 0x4

    goto :goto_0

    .line 116
    :cond_3
    const-string v0, "int"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 117
    const/4 v0, 0x5

    goto :goto_0

    .line 118
    :cond_4
    const-string v0, "node"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 119
    const/4 v0, 0x6

    goto :goto_0

    .line 120
    :cond_5
    const-string v0, "null"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 121
    const/4 v0, 0x7

    goto :goto_0

    .line 122
    :cond_6
    const-string v0, "xml"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    .line 123
    const/16 v0, 0x8

    goto :goto_0

    .line 124
    :cond_7
    const-string v0, "float"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 125
    const/16 v0, 0x9

    goto :goto_0

    .line 126
    :cond_8
    const-string v0, "time"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_9

    .line 127
    const/16 v0, 0xb

    goto :goto_0

    .line 128
    :cond_9
    const-string v0, "date"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_a

    .line 129
    const/16 v0, 0xa

    goto :goto_0

    .line 132
    :cond_a
    const/16 v0, 0xc

    goto :goto_0
.end method

.method public static xdmOmGetFormatString(I)Ljava/lang/String;
    .locals 1
    .param p0, "szFormat"    # I

    .prologue
    .line 56
    const/4 v0, 0x0

    .line 58
    .local v0, "szOutbuf":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 98
    :goto_0
    return-object v0

    .line 61
    :pswitch_0
    const-string v0, "b64"

    .line 62
    goto :goto_0

    .line 64
    :pswitch_1
    const-string v0, "bin"

    .line 65
    goto :goto_0

    .line 67
    :pswitch_2
    const-string v0, "bool"

    .line 68
    goto :goto_0

    .line 70
    :pswitch_3
    const-string v0, "chr"

    .line 71
    goto :goto_0

    .line 73
    :pswitch_4
    const-string v0, "int"

    .line 74
    goto :goto_0

    .line 76
    :pswitch_5
    const-string v0, "node"

    .line 77
    goto :goto_0

    .line 79
    :pswitch_6
    const-string v0, "null"

    .line 80
    goto :goto_0

    .line 82
    :pswitch_7
    const-string v0, "xml"

    .line 83
    goto :goto_0

    .line 85
    :pswitch_8
    const-string v0, "float"

    .line 86
    goto :goto_0

    .line 88
    :pswitch_9
    const-string v0, "time"

    .line 89
    goto :goto_0

    .line 91
    :pswitch_a
    const-string v0, "date"

    .line 92
    goto :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public xdmOmDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V
    .locals 3
    .param p1, "h"    # Lcom/policydm/eng/core/XDMOmList;

    .prologue
    .line 37
    move-object v0, p1

    .line 39
    .local v0, "cur":Lcom/policydm/eng/core/XDMOmList;
    :goto_0
    if-nez v0, :cond_0

    .line 47
    return-void

    .line 41
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 43
    .local v1, "next":Lcom/policydm/eng/core/XDMOmList;
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 44
    const/4 v0, 0x0

    .line 45
    move-object v0, v1

    goto :goto_0
.end method
