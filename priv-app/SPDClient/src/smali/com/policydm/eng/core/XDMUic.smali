.class public Lcom/policydm/eng/core/XDMUic;
.super Ljava/lang/Object;
.source "XDMUic.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XUICInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmUicCopyResult(Lcom/policydm/eng/core/XDMUicResult;Lcom/policydm/eng/core/XDMUicResult;)Lcom/policydm/eng/core/XDMUicResult;
    .locals 5
    .param p0, "target"    # Lcom/policydm/eng/core/XDMUicResult;
    .param p1, "source"    # Lcom/policydm/eng/core/XDMUicResult;

    .prologue
    .line 193
    move-object v2, p0

    .line 194
    .local v2, "pTarget":Lcom/policydm/eng/core/XDMUicResult;
    move-object v1, p1

    .line 195
    .local v1, "pSource":Lcom/policydm/eng/core/XDMUicResult;
    const-string v3, ""

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 196
    if-nez v1, :cond_1

    .line 220
    :cond_0
    return-object p0

    .line 200
    :cond_1
    iget v3, v1, Lcom/policydm/eng/core/XDMUicResult;->appId:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->appId:I

    .line 201
    iget v3, v1, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    .line 202
    iget v3, v1, Lcom/policydm/eng/core/XDMUicResult;->result:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->result:I

    .line 203
    iget v3, v1, Lcom/policydm/eng/core/XDMUicResult;->SingleSelected:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->SingleSelected:I

    .line 205
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    if-eqz v3, :cond_2

    .line 207
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget v4, v4, Lcom/policydm/eng/core/XDMText;->len:I

    iput v4, v3, Lcom/policydm/eng/core/XDMText;->len:I

    .line 208
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget v4, v4, Lcom/policydm/eng/core/XDMText;->size:I

    iput v4, v3, Lcom/policydm/eng/core/XDMText;->size:I

    .line 210
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v3, v3, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 212
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 215
    :cond_2
    iget v3, v1, Lcom/policydm/eng/core/XDMUicResult;->MenuNumbers:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicResult;->MenuNumbers:I

    .line 217
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 218
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicResult;->MultiSelected:[I

    aget v4, v4, v0

    aput v4, v3, v0

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static xdmUicCopyUicOption(Ljava/lang/Object;Ljava/lang/Object;)Lcom/policydm/eng/core/XDMUicOption;
    .locals 6
    .param p0, "target"    # Ljava/lang/Object;
    .param p1, "source"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 98
    move-object v2, p0

    check-cast v2, Lcom/policydm/eng/core/XDMUicOption;

    .local v2, "pTarget":Lcom/policydm/eng/core/XDMUicOption;
    move-object v1, p1

    .line 99
    check-cast v1, Lcom/policydm/eng/core/XDMUicOption;

    .line 100
    .local v1, "pSource":Lcom/policydm/eng/core/XDMUicOption;
    const/4 v0, 0x0

    .line 101
    .local v0, "i":I
    const-string v3, ""

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 102
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->appId:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->appId:I

    .line 103
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->inputType:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->inputType:I

    .line 104
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->echoType:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->echoType:I

    .line 105
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->maxDT:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->maxDT:I

    .line 106
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->maxLen:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->maxLen:I

    .line 107
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->minDT:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->minDT:I

    .line 108
    iget-wide v3, v1, Lcom/policydm/eng/core/XDMUicOption;->progrCurSize:J

    iput-wide v3, v2, Lcom/policydm/eng/core/XDMUicOption;->progrCurSize:J

    .line 109
    iget-wide v3, v1, Lcom/policydm/eng/core/XDMUicOption;->progrMaxSize:J

    iput-wide v3, v2, Lcom/policydm/eng/core/XDMUicOption;->progrMaxSize:J

    .line 110
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->progrType:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->progrType:I

    .line 111
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    .line 112
    new-instance v3, Lcom/policydm/eng/core/XDMText;

    invoke-direct {v3}, Lcom/policydm/eng/core/XDMText;-><init>()V

    iput-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    .line 113
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    if-eqz v3, :cond_5

    .line 115
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iget v4, v4, Lcom/policydm/eng/core/XDMText;->len:I

    iput v4, v3, Lcom/policydm/eng/core/XDMText;->len:I

    .line 116
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iget v4, v4, Lcom/policydm/eng/core/XDMText;->size:I

    iput v4, v3, Lcom/policydm/eng/core/XDMText;->size:I

    .line 117
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v3, v3, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 119
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 128
    :cond_0
    :goto_0
    new-instance v3, Lcom/policydm/eng/core/XDMText;

    invoke-direct {v3}, Lcom/policydm/eng/core/XDMText;-><init>()V

    iput-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    .line 129
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    if-eqz v3, :cond_6

    .line 131
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget v4, v4, Lcom/policydm/eng/core/XDMText;->len:I

    iput v4, v3, Lcom/policydm/eng/core/XDMText;->len:I

    .line 132
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget v4, v4, Lcom/policydm/eng/core/XDMText;->size:I

    iput v4, v3, Lcom/policydm/eng/core/XDMText;->size:I

    .line 133
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v3, v3, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 135
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iget-object v4, v4, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    iput-object v4, v3, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 144
    :cond_1
    :goto_1
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_2

    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->UICType:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_3

    .line 146
    :cond_2
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    if-nez v3, :cond_3

    .line 148
    const-string v3, "xdmUicCopyUicOption uicMenuNumbers = 0 !!!"

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 151
    :cond_3
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    iput v3, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    .line 152
    const/4 v0, 0x0

    :goto_2
    iget v3, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    if-lt v0, v3, :cond_7

    .line 163
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->m_szUicMenuTitle:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 165
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->m_szUicMenuTitle:Ljava/lang/String;

    iput-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->m_szUicMenuTitle:Ljava/lang/String;

    .line 167
    :cond_4
    check-cast p0, Lcom/policydm/eng/core/XDMUicOption;

    .end local p0    # "target":Ljava/lang/Object;
    return-object p0

    .line 124
    .restart local p0    # "target":Ljava/lang/Object;
    :cond_5
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iput v5, v3, Lcom/policydm/eng/core/XDMText;->len:I

    .line 125
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    iput v5, v3, Lcom/policydm/eng/core/XDMText;->size:I

    goto :goto_0

    .line 140
    :cond_6
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iput v5, v3, Lcom/policydm/eng/core/XDMText;->len:I

    .line 141
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    iput v5, v3, Lcom/policydm/eng/core/XDMText;->size:I

    goto :goto_1

    .line 154
    :cond_7
    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 156
    const/16 v3, 0x20

    if-ge v0, v3, :cond_8

    iget-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_8

    .line 158
    iget-object v3, v2, Lcom/policydm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    iget-object v4, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    aget-object v4, v4, v0

    aput-object v4, v3, v0

    .line 152
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public static xdmUicCreateResult()Lcom/policydm/eng/core/XDMUicResult;
    .locals 3

    .prologue
    .line 177
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 178
    new-instance v0, Lcom/policydm/eng/core/XDMUicResult;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMUicResult;-><init>()V

    .line 179
    .local v0, "res":Lcom/policydm/eng/core/XDMUicResult;
    new-instance v1, Lcom/policydm/eng/core/XDMText;

    invoke-direct {v1}, Lcom/policydm/eng/core/XDMText;-><init>()V

    iput-object v1, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    .line 180
    const/16 v1, 0x80

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMList;->xdmListCreateText(ILjava/lang/Object;)Lcom/policydm/eng/core/XDMText;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    .line 182
    return-object v0
.end method

.method public static xdmUicCreateUicOption()Lcom/policydm/eng/core/XDMUicOption;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x80

    .line 22
    new-instance v0, Lcom/policydm/eng/core/XDMUicOption;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMUicOption;-><init>()V

    .line 23
    .local v0, "opt":Lcom/policydm/eng/core/XDMUicOption;
    new-instance v1, Lcom/policydm/eng/core/XDMText;

    invoke-direct {v1}, Lcom/policydm/eng/core/XDMText;-><init>()V

    iput-object v1, v0, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    .line 24
    new-instance v1, Lcom/policydm/eng/core/XDMText;

    invoke-direct {v1}, Lcom/policydm/eng/core/XDMText;-><init>()V

    iput-object v1, v0, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    .line 25
    invoke-static {v2, v3}, Lcom/policydm/eng/core/XDMList;->xdmListCreateText(ILjava/lang/Object;)Lcom/policydm/eng/core/XDMText;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    .line 26
    invoke-static {v2, v3}, Lcom/policydm/eng/core/XDMList;->xdmListCreateText(ILjava/lang/Object;)Lcom/policydm/eng/core/XDMText;

    move-result-object v1

    iput-object v1, v0, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    .line 28
    return-object v0
.end method

.method public static xdmUicFreeResult(Lcom/policydm/eng/core/XDMUicResult;)Ljava/lang/Object;
    .locals 3
    .param p0, "uicResult"    # Lcom/policydm/eng/core/XDMUicResult;

    .prologue
    const/4 v2, 0x0

    .line 230
    move-object v0, p0

    .line 231
    .local v0, "obj":Lcom/policydm/eng/core/XDMUicResult;
    const-string v1, "xdmUicFreeResult"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 232
    iget-object v1, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iput-object v2, v1, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    .line 233
    iput-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    .line 235
    const/4 v0, 0x0

    .line 236
    return-object v2
.end method

.method public static xdmUicFreeUicOption(Lcom/policydm/eng/core/XDMUicOption;)Lcom/policydm/eng/core/XDMUicOption;
    .locals 4
    .param p0, "uicOption"    # Lcom/policydm/eng/core/XDMUicOption;

    .prologue
    const/4 v3, 0x0

    .line 38
    move-object v1, p0

    .line 39
    .local v1, "obj":Lcom/policydm/eng/core/XDMUicOption;
    const/4 v0, 0x0

    .line 41
    .local v0, "i":I
    if-eqz v1, :cond_0

    .line 43
    iput-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->text:Lcom/policydm/eng/core/XDMText;

    .line 44
    iput-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    .line 46
    const/4 v0, 0x0

    :goto_0
    iget v2, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuNumbers:I

    if-lt v0, v2, :cond_1

    .line 50
    iput-object v3, v1, Lcom/policydm/eng/core/XDMUicOption;->m_szUicMenuTitle:Ljava/lang/String;

    .line 51
    const/4 v1, 0x0

    .line 54
    :cond_0
    return-object p0

    .line 48
    :cond_1
    iget-object v2, v1, Lcom/policydm/eng/core/XDMUicOption;->uicMenuList:[Ljava/lang/String;

    aput-object v3, v2, v0

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static xdmUicGetResultKeep()Lcom/policydm/eng/core/XDMUicResult;
    .locals 4

    .prologue
    .line 455
    const/4 v1, 0x0

    .line 456
    .local v1, "ptUicResultKeep":Lcom/policydm/db/XDBUICResultKeepInfo;
    const/4 v0, 0x0

    .line 458
    .local v0, "ptUicResult":Lcom/policydm/eng/core/XDMUicResult;
    invoke-static {}, Lcom/policydm/eng/core/XDMUic;->xdmUicCreateResult()Lcom/policydm/eng/core/XDMUicResult;

    move-result-object v0

    .line 459
    if-nez v0, :cond_1

    .line 460
    const/4 v0, 0x0

    .line 477
    .end local v0    # "ptUicResult":Lcom/policydm/eng/core/XDMUicResult;
    :cond_0
    :goto_0
    return-object v0

    .line 462
    .restart local v0    # "ptUicResult":Lcom/policydm/eng/core/XDMUicResult;
    :cond_1
    new-instance v1, Lcom/policydm/db/XDBUICResultKeepInfo;

    .end local v1    # "ptUicResultKeep":Lcom/policydm/db/XDBUICResultKeepInfo;
    invoke-direct {v1}, Lcom/policydm/db/XDBUICResultKeepInfo;-><init>()V

    .line 463
    .restart local v1    # "ptUicResultKeep":Lcom/policydm/db/XDBUICResultKeepInfo;
    const/16 v2, 0xf

    invoke-static {v2}, Lcom/policydm/db/XDB;->xdbRead(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "ptUicResultKeep":Lcom/policydm/db/XDBUICResultKeepInfo;
    check-cast v1, Lcom/policydm/db/XDBUICResultKeepInfo;

    .line 465
    .restart local v1    # "ptUicResultKeep":Lcom/policydm/db/XDBUICResultKeepInfo;
    iget v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->appId:I

    iput v2, v0, Lcom/policydm/eng/core/XDMUicResult;->appId:I

    .line 466
    iget v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->UICType:I

    iput v2, v0, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    .line 467
    iget v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->result:I

    iput v2, v0, Lcom/policydm/eng/core/XDMUicResult;->result:I

    .line 468
    iget v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->number:I

    iput v2, v0, Lcom/policydm/eng/core/XDMUicResult;->MenuNumbers:I

    .line 470
    iget-object v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->m_szText:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 472
    iget-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget v3, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->nLen:I

    iput v3, v2, Lcom/policydm/eng/core/XDMText;->len:I

    .line 473
    iget-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget v3, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->nSize:I

    iput v3, v2, Lcom/policydm/eng/core/XDMText;->size:I

    .line 474
    iget-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v3, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->m_szText:Ljava/lang/String;

    iput-object v3, v2, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    goto :goto_0
.end method

.method public static xdmUicGetUicType(Ljava/lang/String;)I
    .locals 3
    .param p0, "szType"    # Ljava/lang/String;

    .prologue
    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x0

    .line 66
    .local v0, "type":I
    const-string v1, "1100"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 68
    const/4 v0, 0x1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    const-string v1, "1101"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 72
    const/4 v0, 0x2

    .line 73
    goto :goto_0

    .line 74
    :cond_2
    const-string v1, "1102"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 76
    const/4 v0, 0x3

    .line 77
    goto :goto_0

    .line 78
    :cond_3
    const-string v1, "1103"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_4

    .line 80
    const/4 v0, 0x4

    .line 81
    goto :goto_0

    .line 82
    :cond_4
    const-string v1, "1104"

    invoke-virtual {v1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 84
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public static xdmUicOptionProcess(Ljava/lang/String;Lcom/policydm/eng/core/XDMUicOption;)Ljava/lang/String;
    .locals 13
    .param p0, "pUicOptions"    # Ljava/lang/String;
    .param p1, "uicOption"    # Lcom/policydm/eng/core/XDMUicOption;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/16 v10, 0x26

    const/4 v9, 0x0

    .line 247
    const/4 v2, 0x0

    .line 248
    .local v2, "i":I
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .line 249
    .local v4, "pOption":[C
    const/4 v0, 0x0

    .line 250
    .local v0, "chTmp":I
    const/4 v3, 0x0

    .line 252
    .local v3, "ipOption":I
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "pUicOptions :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 253
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "uicOption :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 255
    aget-char v7, v4, v3

    if-nez v7, :cond_1

    .line 256
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    .line 450
    :goto_0
    return-object v7

    .line 259
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 258
    :cond_1
    aget-char v7, v4, v3

    const/16 v8, 0x20

    if-eq v7, v8, :cond_0

    aget-char v7, v4, v3

    const/16 v8, 0x9

    if-eq v7, v8, :cond_0

    .line 261
    :goto_1
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_2

    add-int v7, v3, v2

    aget-char v7, v4, v7

    const/16 v8, 0x3d

    if-ne v7, v8, :cond_5

    .line 264
    :cond_2
    array-length v7, v4

    sub-int/2addr v7, v3

    new-array v5, v7, [C

    .line 265
    .local v5, "sOption":[C
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    array-length v8, v4

    sub-int/2addr v8, v3

    invoke-virtual {v7, v3, v8, v5, v9}, Ljava/lang/String;->getChars(II[CI)V

    .line 267
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "MINDT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "MDT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 269
    :cond_3
    add-int v7, v3, v2

    add-int/lit8 v3, v7, 0x1

    .line 270
    const/4 v2, 0x0

    .line 271
    :goto_2
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_4

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-eq v7, v10, :cond_4

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-nez v7, :cond_6

    .line 274
    :cond_4
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_7

    .line 276
    add-int v7, v3, v2

    aget-char v0, v4, v7

    .line 277
    add-int v7, v3, v2

    aput-char v9, v4, v7

    .line 282
    :goto_3
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    add-int v8, v3, v2

    invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 285
    .local v6, "szTemp":Ljava/lang/String;
    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p1, Lcom/policydm/eng/core/XDMUicOption;->minDT:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 440
    .end local v6    # "szTemp":Ljava/lang/String;
    :goto_4
    if-ne v0, v10, :cond_1d

    .line 442
    add-int v7, v3, v2

    add-int/lit8 v3, v7, 0x1

    .line 444
    const/4 v5, 0x0

    .line 445
    array-length v7, v4

    sub-int/2addr v7, v3

    new-array v5, v7, [C

    .line 446
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    array-length v8, v4

    invoke-virtual {v7, v3, v8, v5, v9}, Ljava/lang/String;->getChars(II[CI)V

    .line 448
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v5}, Ljava/lang/String;-><init>([C)V

    invoke-static {v7, p1}, Lcom/policydm/eng/core/XDMUic;->xdmUicOptionProcess(Ljava/lang/String;Lcom/policydm/eng/core/XDMUicOption;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 262
    .end local v5    # "sOption":[C
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 272
    .restart local v5    # "sOption":[C
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 280
    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    .line 287
    .restart local v6    # "szTemp":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 290
    iput v9, p1, Lcom/policydm/eng/core/XDMUicOption;->minDT:I

    goto :goto_4

    .line 293
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v6    # "szTemp":Ljava/lang/String;
    :cond_8
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "MAXDT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 295
    add-int v7, v3, v2

    add-int/lit8 v3, v7, 0x1

    .line 296
    const/4 v2, 0x0

    .line 297
    :goto_5
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_9

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-eq v7, v10, :cond_9

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-nez v7, :cond_a

    .line 300
    :cond_9
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_b

    .line 302
    add-int v7, v3, v2

    aget-char v0, v4, v7

    .line 303
    add-int v7, v3, v2

    aput-char v9, v4, v7

    .line 308
    :goto_6
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    add-int v8, v3, v2

    invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 309
    .restart local v6    # "szTemp":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "temp :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 312
    :try_start_1
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p1, Lcom/policydm/eng/core/XDMUicOption;->maxDT:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_4

    .line 314
    :catch_1
    move-exception v1

    .line 316
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 317
    iput v9, p1, Lcom/policydm/eng/core/XDMUicOption;->maxDT:I

    goto/16 :goto_4

    .line 298
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v6    # "szTemp":Ljava/lang/String;
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 306
    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    .line 320
    :cond_c
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "DR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 322
    add-int v7, v3, v2

    add-int/lit8 v3, v7, 0x1

    .line 323
    const/4 v2, 0x0

    .line 324
    :goto_7
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_d

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-eq v7, v10, :cond_d

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-nez v7, :cond_e

    .line 327
    :cond_d
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_f

    .line 329
    add-int v7, v3, v2

    aget-char v0, v4, v7

    .line 330
    add-int v7, v3, v2

    aput-char v9, v4, v7

    .line 335
    :goto_8
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    add-int v8, v3, v2

    invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 336
    .restart local v6    # "szTemp":Ljava/lang/String;
    iget-object v7, p1, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    invoke-static {v7, v6}, Lcom/policydm/eng/core/XDMList;->xdmListCopyStrText(Lcom/policydm/eng/core/XDMText;Ljava/lang/String;)Lcom/policydm/eng/core/XDMText;

    move-result-object v7

    iput-object v7, p1, Lcom/policydm/eng/core/XDMUicOption;->defaultResponse:Lcom/policydm/eng/core/XDMText;

    goto/16 :goto_4

    .line 325
    .end local v6    # "szTemp":Ljava/lang/String;
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 333
    :cond_f
    const/4 v0, 0x0

    goto :goto_8

    .line 338
    :cond_10
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "MAXLEN"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 340
    add-int v7, v3, v2

    add-int/lit8 v3, v7, 0x1

    .line 341
    const/4 v2, 0x0

    .line 342
    :goto_9
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_11

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-eq v7, v10, :cond_11

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-nez v7, :cond_12

    .line 345
    :cond_11
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_13

    .line 347
    add-int v7, v3, v2

    aget-char v0, v4, v7

    .line 348
    add-int v7, v3, v2

    aput-char v9, v4, v7

    .line 353
    :goto_a
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    add-int v8, v3, v2

    invoke-virtual {v7, v3, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 356
    .restart local v6    # "szTemp":Ljava/lang/String;
    :try_start_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p1, Lcom/policydm/eng/core/XDMUicOption;->maxLen:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_4

    .line 358
    :catch_2
    move-exception v1

    .line 360
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 361
    iput v9, p1, Lcom/policydm/eng/core/XDMUicOption;->maxLen:I

    goto/16 :goto_4

    .line 343
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v6    # "szTemp":Ljava/lang/String;
    :cond_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 351
    :cond_13
    const/4 v0, 0x0

    goto :goto_a

    .line 364
    :cond_14
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "IT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_18

    .line 366
    add-int v7, v3, v2

    add-int/lit8 v3, v7, 0x1

    .line 367
    const/4 v2, 0x0

    .line 368
    :goto_b
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_15

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-eq v7, v10, :cond_15

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-nez v7, :cond_16

    .line 371
    :cond_15
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_17

    .line 373
    add-int v7, v3, v2

    aget-char v0, v4, v7

    .line 374
    add-int v7, v3, v2

    aput-char v9, v4, v7

    .line 379
    :goto_c
    const/4 v5, 0x0

    .line 380
    new-array v5, v2, [C

    .line 381
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    add-int v8, v3, v2

    invoke-virtual {v7, v3, v8, v5, v9}, Ljava/lang/String;->getChars(II[CI)V

    .line 382
    aget-char v7, v4, v3

    sparse-switch v7, :sswitch_data_0

    goto/16 :goto_4

    .line 385
    :sswitch_0
    iput v11, p1, Lcom/policydm/eng/core/XDMUicOption;->inputType:I

    goto/16 :goto_4

    .line 369
    :cond_16
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 377
    :cond_17
    const/4 v0, 0x0

    goto :goto_c

    .line 388
    :sswitch_1
    iput v12, p1, Lcom/policydm/eng/core/XDMUicOption;->inputType:I

    goto/16 :goto_4

    .line 391
    :sswitch_2
    const/4 v7, 0x3

    iput v7, p1, Lcom/policydm/eng/core/XDMUicOption;->inputType:I

    goto/16 :goto_4

    .line 394
    :sswitch_3
    const/4 v7, 0x4

    iput v7, p1, Lcom/policydm/eng/core/XDMUicOption;->inputType:I

    goto/16 :goto_4

    .line 397
    :sswitch_4
    const/4 v7, 0x5

    iput v7, p1, Lcom/policydm/eng/core/XDMUicOption;->inputType:I

    goto/16 :goto_4

    .line 400
    :sswitch_5
    const/4 v7, 0x6

    iput v7, p1, Lcom/policydm/eng/core/XDMUicOption;->inputType:I

    goto/16 :goto_4

    .line 406
    :cond_18
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    const-string v8, "ET"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1c

    .line 408
    add-int v7, v3, v2

    add-int/lit8 v3, v7, 0x1

    .line 409
    const/4 v2, 0x0

    .line 410
    :goto_d
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_19

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-eq v7, v10, :cond_19

    add-int v7, v3, v2

    aget-char v7, v4, v7

    if-nez v7, :cond_1a

    .line 413
    :cond_19
    add-int v7, v3, v2

    array-length v8, v4

    if-ge v7, v8, :cond_1b

    .line 415
    add-int v7, v3, v2

    aget-char v0, v4, v7

    .line 416
    add-int v7, v3, v2

    aput-char v9, v4, v7

    .line 420
    :goto_e
    const/4 v5, 0x0

    .line 421
    new-array v5, v2, [C

    .line 422
    invoke-static {v4}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    add-int v8, v3, v2

    invoke-virtual {v7, v3, v8, v5, v9}, Ljava/lang/String;->getChars(II[CI)V

    .line 423
    aget-char v7, v4, v3

    sparse-switch v7, :sswitch_data_1

    goto/16 :goto_4

    .line 429
    :sswitch_6
    iput v12, p1, Lcom/policydm/eng/core/XDMUicOption;->echoType:I

    goto/16 :goto_4

    .line 411
    :cond_1a
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 419
    :cond_1b
    const/4 v0, 0x0

    goto :goto_e

    .line 426
    :sswitch_7
    iput v11, p1, Lcom/policydm/eng/core/XDMUicOption;->echoType:I

    goto/16 :goto_4

    .line 437
    :cond_1c
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 450
    :cond_1d
    invoke-static {v5}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 382
    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x44 -> :sswitch_2
        0x49 -> :sswitch_5
        0x4e -> :sswitch_1
        0x50 -> :sswitch_4
        0x54 -> :sswitch_3
    .end sparse-switch

    .line 423
    :sswitch_data_1
    .sparse-switch
        0x50 -> :sswitch_6
        0x54 -> :sswitch_7
    .end sparse-switch
.end method

.method public static xdmUicSetResultKeep(Lcom/policydm/eng/core/XDMUicResult;I)Lcom/policydm/eng/core/XDMUicResult;
    .locals 4
    .param p0, "pData"    # Lcom/policydm/eng/core/XDMUicResult;
    .param p1, "pUicResultKeepFlag"    # I

    .prologue
    .line 488
    move-object v0, p0

    .line 489
    .local v0, "ptUicResult":Lcom/policydm/eng/core/XDMUicResult;
    const/4 v1, 0x0

    .line 491
    .local v1, "ptUicResultKeep":Lcom/policydm/db/XDBUICResultKeepInfo;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "xdmUicSetResultKeep pUicResultKeepFlag ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 492
    if-nez v0, :cond_0

    .line 494
    const-string v2, "ptUicResult is NULL!"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 519
    :goto_0
    return-object v0

    .line 498
    :cond_0
    new-instance v1, Lcom/policydm/db/XDBUICResultKeepInfo;

    .end local v1    # "ptUicResultKeep":Lcom/policydm/db/XDBUICResultKeepInfo;
    invoke-direct {v1}, Lcom/policydm/db/XDBUICResultKeepInfo;-><init>()V

    .line 500
    .restart local v1    # "ptUicResultKeep":Lcom/policydm/db/XDBUICResultKeepInfo;
    iput p1, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->eStatus:I

    .line 501
    iget v2, v0, Lcom/policydm/eng/core/XDMUicResult;->appId:I

    iput v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->appId:I

    .line 502
    iget v2, v0, Lcom/policydm/eng/core/XDMUicResult;->UICType:I

    iput v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->UICType:I

    .line 503
    iget v2, v0, Lcom/policydm/eng/core/XDMUicResult;->result:I

    iput v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->result:I

    .line 504
    iget v2, v0, Lcom/policydm/eng/core/XDMUicResult;->MenuNumbers:I

    iput v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->number:I

    .line 506
    iget-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    if-eqz v2, :cond_1

    .line 508
    iget-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget v2, v2, Lcom/policydm/eng/core/XDMText;->len:I

    iput v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->nLen:I

    .line 509
    iget-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget v2, v2, Lcom/policydm/eng/core/XDMText;->size:I

    iput v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->nSize:I

    .line 510
    iget-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 512
    iget-object v2, v0, Lcom/policydm/eng/core/XDMUicResult;->text:Lcom/policydm/eng/core/XDMText;

    iget-object v2, v2, Lcom/policydm/eng/core/XDMText;->text:Ljava/lang/String;

    iput-object v2, v1, Lcom/policydm/db/XDBUICResultKeepInfo;->m_szText:Ljava/lang/String;

    .line 516
    :cond_1
    const/16 v2, 0xf

    invoke-static {v2, v1}, Lcom/policydm/db/XDB;->xdbWrite(ILjava/lang/Object;)V

    .line 517
    const/4 v1, 0x0

    .line 519
    goto :goto_0
.end method
