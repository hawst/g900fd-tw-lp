.class public Lcom/policydm/eng/core/XDMOmVfs;
.super Ljava/lang/Object;
.source "XDMOmVfs.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field private static final OM_MAX_LEN:I = 0x200

.field private static index:I


# instance fields
.field public root:Lcom/policydm/eng/core/XDMVnode;

.field public stdobj_space:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 24
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    .line 29
    const v0, 0xa000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/policydm/eng/core/XDMOmVfs;->stdobj_space:[B

    .line 30
    return-void
.end method

.method public static xdmOmVfsAppendList(Lcom/policydm/eng/core/XDMOmList;Lcom/policydm/eng/core/XDMOmList;)Lcom/policydm/eng/core/XDMOmList;
    .locals 3
    .param p0, "h"    # Lcom/policydm/eng/core/XDMOmList;
    .param p1, "node"    # Lcom/policydm/eng/core/XDMOmList;

    .prologue
    const/4 v2, 0x0

    .line 529
    if-nez p0, :cond_0

    .line 531
    move-object p0, p1

    .line 532
    iput-object v2, p0, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 545
    :goto_0
    return-object p0

    .line 536
    :cond_0
    move-object v0, p0

    .line 537
    .local v0, "tmp":Lcom/policydm/eng/core/XDMOmList;
    :goto_1
    iget-object v1, v0, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    if-nez v1, :cond_1

    .line 542
    iput-object v2, p1, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 543
    iput-object p1, v0, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    goto :goto_0

    .line 539
    :cond_1
    iget-object v0, v0, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    goto :goto_1
.end method

.method public static xdmOmVfsAppendNode(Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)I
    .locals 3
    .param p0, "ptParent"    # Lcom/policydm/eng/core/XDMVnode;
    .param p1, "ptChild"    # Lcom/policydm/eng/core/XDMVnode;

    .prologue
    const/4 v1, 0x0

    .line 552
    invoke-static {p0, p1}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsHaveThisChild(Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 554
    const/4 v1, -0x2

    .line 576
    :goto_0
    return v1

    .line 557
    :cond_0
    iget-object v2, p0, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    if-nez v2, :cond_1

    .line 559
    iput-object p1, p0, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 562
    iput-object p0, p1, Lcom/policydm/eng/core/XDMVnode;->ptParentNode:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_0

    .line 566
    :cond_1
    iget-object v0, p0, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 567
    .local v0, "last":Lcom/policydm/eng/core/XDMVnode;
    :goto_1
    iget-object v2, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    if-nez v2, :cond_2

    .line 571
    iput-object p1, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    .line 575
    iput-object p0, p1, Lcom/policydm/eng/core/XDMVnode;->ptParentNode:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_0

    .line 569
    :cond_2
    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_1
.end method

.method public static xdmOmVfsCreateNewNode(Ljava/lang/String;Z)Lcom/policydm/eng/core/XDMVnode;
    .locals 5
    .param p0, "szName"    # Ljava/lang/String;
    .param p1, "defaultacl"    # Z

    .prologue
    const/4 v4, 0x0

    .line 68
    new-instance v2, Lcom/policydm/eng/core/XDMVnode;

    invoke-direct {v2}, Lcom/policydm/eng/core/XDMVnode;-><init>()V

    .line 70
    .local v2, "ptNode":Lcom/policydm/eng/core/XDMVnode;
    if-eqz p1, :cond_0

    .line 72
    new-instance v0, Lcom/policydm/eng/core/XDMOmAcl;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMOmAcl;-><init>()V

    .line 73
    .local v0, "acl":Lcom/policydm/eng/core/XDMOmAcl;
    const-string v3, "*"

    iput-object v3, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    .line 74
    const/16 v3, 0x1b

    iput v3, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    .line 76
    new-instance v1, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v1}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 77
    .local v1, "item":Lcom/policydm/eng/core/XDMOmList;
    iput-object v0, v1, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 78
    const/4 v3, 0x0

    iput-object v3, v1, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 80
    iput-object v1, v2, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    .line 83
    .end local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    .end local v1    # "item":Lcom/policydm/eng/core/XDMOmList;
    :cond_0
    iput-object p0, v2, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 84
    const/4 v3, 0x6

    iput v3, v2, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 85
    iput v4, v2, Lcom/policydm/eng/core/XDMVnode;->verno:I

    .line 86
    iput v4, v2, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 87
    const/4 v3, -0x1

    iput v3, v2, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    .line 88
    const/4 v3, 0x2

    iput v3, v2, Lcom/policydm/eng/core/XDMVnode;->scope:I

    .line 90
    return-object v2
.end method

.method public static xdmOmVfsCreatePath(Lcom/policydm/eng/core/XDMOmVfs;Ljava/lang/String;)I
    .locals 11
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 666
    const-string v6, ""

    .line 667
    .local v6, "szNodeName":Ljava/lang/String;
    move-object v7, p1

    .line 669
    .local v7, "szTempPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 670
    .local v1, "index":I
    const/4 v0, 0x0

    .line 671
    .local v0, "i":I
    iget-object v4, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    .line 673
    .local v4, "ptBaseNode":Lcom/policydm/eng/core/XDMVnode;
    const-string v9, "/"

    invoke-virtual {v7, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 674
    .local v3, "nodenamesplit":[Ljava/lang/String;
    array-length v2, v3

    .line 676
    .local v2, "l":I
    aget-object v9, v3, v8

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_0

    .line 677
    const/4 v1, 0x1

    .line 681
    :goto_0
    move v0, v1

    :goto_1
    if-lt v0, v2, :cond_1

    .line 703
    :goto_2
    return v8

    .line 679
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 683
    :cond_1
    aget-object v6, v3, v0

    .line 684
    add-int/lit8 v9, v0, 0x1

    if-ne v9, v2, :cond_3

    .line 686
    invoke-static {p0, v6, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetNode(Lcom/policydm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 688
    const/4 v8, -0x2

    goto :goto_2

    .line 691
    :cond_2
    const/4 v9, 0x1

    invoke-static {v6, v9}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsCreateNewNode(Ljava/lang/String;Z)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 692
    .local v5, "ptNode":Lcom/policydm/eng/core/XDMVnode;
    invoke-static {v4, v5}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsAppendNode(Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)I

    goto :goto_2

    .line 695
    .end local v5    # "ptNode":Lcom/policydm/eng/core/XDMVnode;
    :cond_3
    invoke-static {p0, v6, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetNode(Lcom/policydm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v5

    .line 696
    .restart local v5    # "ptNode":Lcom/policydm/eng/core/XDMVnode;
    if-nez v5, :cond_4

    .line 698
    const/4 v8, -0x3

    goto :goto_2

    .line 701
    :cond_4
    move-object v4, v5

    .line 681
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static xdmOmVfsDeleteAclList(Lcom/policydm/eng/core/XDMOmList;)V
    .locals 2
    .param p0, "h"    # Lcom/policydm/eng/core/XDMOmList;

    .prologue
    .line 966
    move-object v0, p0

    .line 968
    .local v0, "cur":Lcom/policydm/eng/core/XDMOmList;
    :goto_0
    if-nez v0, :cond_0

    .line 975
    return-void

    .line 970
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 972
    .local v1, "next":Lcom/policydm/eng/core/XDMOmList;
    const/4 v0, 0x0

    .line 973
    move-object v0, v1

    goto :goto_0
.end method

.method public static xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V
    .locals 3
    .param p0, "h"    # Lcom/policydm/eng/core/XDMOmList;

    .prologue
    .line 982
    move-object v0, p0

    .line 984
    .local v0, "cur":Lcom/policydm/eng/core/XDMOmList;
    :goto_0
    if-nez v0, :cond_0

    .line 993
    return-void

    .line 986
    :cond_0
    iget-object v1, v0, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    .line 988
    .local v1, "next":Lcom/policydm/eng/core/XDMOmList;
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 989
    const/4 v0, 0x0

    .line 991
    move-object v0, v1

    goto :goto_0
.end method

.method private static xdmOmVfsDeleteOmFile()V
    .locals 1

    .prologue
    .line 1070
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdObjectTreeInfo()I

    move-result v0

    .line 1071
    .local v0, "nFileId":I
    invoke-static {v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 1073
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdObjectData()I

    move-result v0

    .line 1074
    invoke-static {v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpDeleteFile(I)I

    .line 1075
    return-void
.end method

.method public static xdmOmVfsDeleteVfs(Lcom/policydm/eng/core/XDMVnode;)V
    .locals 4
    .param p0, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;

    .prologue
    const/4 v3, 0x0

    .line 1041
    iget-object v0, p0, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 1044
    .local v0, "cur":Lcom/policydm/eng/core/XDMVnode;
    :goto_0
    if-nez v0, :cond_2

    .line 1051
    iget-object v2, p0, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v2, :cond_0

    .line 1053
    iget-object v2, p0, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v2}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsDeleteAclList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 1055
    :cond_0
    iget-object v2, p0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v2, :cond_1

    .line 1057
    iget-object v2, p0, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v2}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 1059
    :cond_1
    iput-object v3, p0, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 1060
    iput-object v3, p0, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 1061
    iput-object v3, p0, Lcom/policydm/eng/core/XDMVnode;->m_szTstamp:Ljava/lang/String;

    .line 1062
    iput-object v3, p0, Lcom/policydm/eng/core/XDMVnode;->m_szDdfName:Ljava/lang/String;

    .line 1063
    const/4 p0, 0x0

    .line 1064
    return-void

    .line 1046
    :cond_2
    iget-object v1, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    .line 1047
    .local v1, "tmp":Lcom/policydm/eng/core/XDMVnode;
    invoke-static {v0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsDeleteVfs(Lcom/policydm/eng/core/XDMVnode;)V

    .line 1048
    move-object v0, v1

    goto :goto_0
.end method

.method public static xdmOmVfsEnd(Lcom/policydm/eng/core/XDMOmVfs;)V
    .locals 1
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    invoke-static {v0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsDeleteVfs(Lcom/policydm/eng/core/XDMVnode;)V

    .line 1037
    return-void
.end method

.method public static xdmOmVfsFindVaddr(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVfspace;)V
    .locals 5
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;
    .param p2, "pSpace"    # Lcom/policydm/eng/core/XDMVfspace;

    .prologue
    .line 815
    iget-object v0, p1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 817
    .local v0, "cur":Lcom/policydm/eng/core/XDMVnode;
    :goto_0
    if-nez v0, :cond_1

    .line 823
    iget v1, p1, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    if-ltz v1, :cond_0

    iget v1, p1, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-lez v1, :cond_0

    .line 825
    iget-object v1, p2, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    iget v2, p2, Lcom/policydm/eng/core/XDMVfspace;->i:I

    iget v3, p1, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    aput v3, v1, v2

    .line 826
    iget-object v1, p2, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    iget v2, p2, Lcom/policydm/eng/core/XDMVfspace;->i:I

    iget v3, p1, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    iget v4, p1, Lcom/policydm/eng/core/XDMVnode;->size:I

    add-int/2addr v3, v4

    aput v3, v1, v2

    .line 827
    iget v1, p2, Lcom/policydm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p2, Lcom/policydm/eng/core/XDMVfspace;->i:I

    .line 829
    :cond_0
    return-void

    .line 819
    :cond_1
    invoke-static {p0, v0, p2}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsFindVaddr(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVfspace;)V

    .line 820
    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_0
.end method

.method public static xdmOmVfsGetData(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;[C)I
    .locals 4
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;
    .param p2, "pBuff"    # [C

    .prologue
    const/4 v1, -0x4

    .line 999
    iget v2, p1, Lcom/policydm/eng/core/XDMVnode;->size:I

    if-lez v2, :cond_0

    iget v2, p1, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    if-ltz v2, :cond_0

    .line 1001
    iget v2, p1, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    iget v3, p1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {p0, v2, p2, v3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsLoadFsData(Lcom/policydm/eng/core/XDMOmVfs;I[CI)I

    move-result v0

    .line 1002
    .local v0, "ret":I
    if-eqz v0, :cond_1

    .line 1008
    .end local v0    # "ret":I
    :cond_0
    :goto_0
    return v1

    .line 1006
    .restart local v0    # "ret":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static xdmOmVfsGetFreeVaddr(Lcom/policydm/eng/core/XDMOmVfs;I)I
    .locals 12
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "nSize"    # I

    .prologue
    const/4 v11, 0x0

    .line 751
    const/4 v3, 0x0

    .line 756
    .local v3, "pSpace":Lcom/policydm/eng/core/XDMVfspace;
    new-instance v3, Lcom/policydm/eng/core/XDMVfspace;

    .end local v3    # "pSpace":Lcom/policydm/eng/core/XDMVfspace;
    invoke-direct {v3}, Lcom/policydm/eng/core/XDMVfspace;-><init>()V

    .line 757
    .restart local v3    # "pSpace":Lcom/policydm/eng/core/XDMVfspace;
    iget-object v8, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    invoke-static {p0, v8, v3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsFindVaddr(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVfspace;)V

    .line 759
    iget v8, v3, Lcom/policydm/eng/core/XDMVfspace;->i:I

    if-nez v8, :cond_0

    .line 761
    const/4 v4, 0x0

    .line 762
    .local v4, "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 810
    .end local v4    # "ret":I
    .local v5, "ret":I
    :goto_0
    return v5

    .line 766
    .end local v5    # "ret":I
    :cond_0
    iget v8, v3, Lcom/policydm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v1, v8, -0x1

    .local v1, "i":I
    :goto_1
    const/4 v8, 0x1

    if-ge v1, v8, :cond_1

    .line 782
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    aget v8, v8, v11

    if-lez v8, :cond_4

    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    aget v8, v8, v11

    add-int/lit8 v8, v8, 0x1

    if-lt v8, p1, :cond_4

    .line 784
    const/4 v4, 0x0

    .line 785
    .restart local v4    # "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 786
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto :goto_0

    .line 768
    .end local v5    # "ret":I
    :cond_1
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_2
    add-int/lit8 v8, v1, -0x1

    if-le v2, v8, :cond_2

    .line 766
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 770
    :cond_2
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    add-int/lit8 v9, v2, 0x1

    aget v8, v8, v9

    iget-object v9, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    aget v9, v9, v2

    if-ge v8, v9, :cond_3

    .line 772
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    add-int/lit8 v9, v2, 0x1

    aget v7, v8, v9

    .line 773
    .local v7, "start":I
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    add-int/lit8 v9, v2, 0x1

    aget v0, v8, v9

    .line 774
    .local v0, "end":I
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    add-int/lit8 v9, v2, 0x1

    iget-object v10, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    aget v10, v10, v2

    aput v10, v8, v9

    .line 775
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    add-int/lit8 v9, v2, 0x1

    iget-object v10, v3, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    aget v10, v10, v2

    aput v10, v8, v9

    .line 776
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    aput v7, v8, v2

    .line 777
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    aput v0, v8, v2

    .line 768
    .end local v0    # "end":I
    .end local v7    # "start":I
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 789
    .end local v2    # "k":I
    :cond_4
    const/4 v1, 0x0

    :goto_3
    iget v8, v3, Lcom/policydm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v8, v8, -0x1

    if-lt v1, v8, :cond_5

    .line 800
    const-wide/32 v8, 0xa000

    iget-object v10, v3, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    iget v11, v3, Lcom/policydm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v11, v11, -0x1

    aget v10, v10, v11

    int-to-long v10, v10

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    int-to-long v10, p1

    cmp-long v8, v8, v10

    if-ltz v8, :cond_7

    .line 802
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    iget v9, v3, Lcom/policydm/eng/core/XDMVfspace;->i:I

    add-int/lit8 v9, v9, -0x1

    aget v4, v8, v9

    .line 803
    .restart local v4    # "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 804
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto :goto_0

    .line 791
    .end local v5    # "ret":I
    :cond_5
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->start:[I

    add-int/lit8 v9, v1, 0x1

    aget v8, v8, v9

    iget-object v9, v3, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    aget v9, v9, v1

    sub-int/2addr v8, v9

    add-int/lit8 v6, v8, -0x1

    .line 792
    .local v6, "s":I
    if-lt v6, p1, :cond_6

    .line 794
    iget-object v8, v3, Lcom/policydm/eng/core/XDMVfspace;->end:[I

    aget v4, v8, v1

    .line 795
    .restart local v4    # "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 796
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto/16 :goto_0

    .line 789
    .end local v5    # "ret":I
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 807
    .end local v6    # "s":I
    :cond_7
    const/4 v4, -0x5

    .line 809
    .restart local v4    # "ret":I
    const/4 v3, 0x0

    move v5, v4

    .line 810
    .end local v4    # "ret":I
    .restart local v5    # "ret":I
    goto/16 :goto_0
.end method

.method public static xdmOmVfsGetNode(Lcom/policydm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;
    .locals 3
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "szNodeName"    # Ljava/lang/String;
    .param p2, "ptBaseNode"    # Lcom/policydm/eng/core/XDMVnode;

    .prologue
    const/4 v1, 0x0

    .line 637
    const/4 v0, 0x0

    .line 639
    .local v0, "cur":Lcom/policydm/eng/core/XDMVnode;
    if-eqz p2, :cond_0

    .line 641
    iget-object v0, p2, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 644
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 660
    :goto_0
    return-object v1

    .line 647
    :cond_1
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-nez v2, :cond_5

    .line 649
    :cond_2
    iget-object v1, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_0

    .line 654
    :cond_3
    iget-object v2, v0, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v1, v0

    .line 656
    goto :goto_0

    .line 658
    :cond_4
    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    .line 652
    :cond_5
    if-nez v0, :cond_3

    goto :goto_0
.end method

.method public static xdmOmVfsGetParent(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;
    .locals 3
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "ptBaseNode"    # Lcom/policydm/eng/core/XDMVnode;
    .param p2, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;

    .prologue
    .line 939
    move-object v1, p1

    .line 940
    .local v1, "ptParent":Lcom/policydm/eng/core/XDMVnode;
    iget-object v0, v1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 942
    .local v0, "ptChild":Lcom/policydm/eng/core/XDMVnode;
    :goto_0
    if-nez v0, :cond_1

    .line 949
    iget-object v0, v1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 950
    :goto_1
    if-nez v0, :cond_2

    .line 960
    const/4 v1, 0x0

    .end local v1    # "ptParent":Lcom/policydm/eng/core/XDMVnode;
    :cond_0
    :goto_2
    return-object v1

    .line 944
    .restart local v1    # "ptParent":Lcom/policydm/eng/core/XDMVnode;
    :cond_1
    if-eq v0, p2, :cond_0

    .line 946
    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_0

    .line 952
    :cond_2
    invoke-static {p0, v0, p2}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetParent(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v2

    .line 953
    .local v2, "tmp":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v2, :cond_3

    move-object v1, v2

    .line 955
    goto :goto_2

    .line 957
    :cond_3
    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_1
.end method

.method public static xdmOmVfsHaveThisChild(Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)Z
    .locals 3
    .param p0, "ptParent"    # Lcom/policydm/eng/core/XDMVnode;
    .param p1, "ptChild"    # Lcom/policydm/eng/core/XDMVnode;

    .prologue
    .line 581
    const/4 v0, 0x0

    .line 583
    .local v0, "cur":Lcom/policydm/eng/core/XDMVnode;
    if-eqz p0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 588
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 597
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 590
    :cond_1
    iget-object v1, v0, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    iget-object v2, p1, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 592
    const/4 v1, 0x1

    goto :goto_1

    .line 594
    :cond_2
    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_0
.end method

.method public static xdmOmVfsInit(Lcom/policydm/eng/core/XDMOmVfs;)I
    .locals 3
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "nRet":I
    iget-object v1, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    if-nez v1, :cond_0

    .line 49
    const-string v1, "/"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsCreateNewNode(Ljava/lang/String;Z)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    iput-object v1, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    .line 52
    :cond_0
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsLoadFs(Lcom/policydm/eng/core/XDMOmVfs;)I

    move-result v0

    .line 53
    return v0
.end method

.method public static xdmOmVfsLoadFs(Lcom/policydm/eng/core/XDMOmVfs;)I
    .locals 11
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;

    .prologue
    const/4 v10, 0x0

    .line 100
    const/4 v8, 0x0

    .line 101
    .local v8, "nFileId":I
    const/4 v1, 0x0

    .line 102
    .local v1, "Input":Ljava/io/DataInputStream;
    const/4 v2, 0x0

    .line 103
    .local v2, "pBuff":I
    const/4 v5, 0x0

    .line 104
    .local v5, "nSize":I
    const/4 v9, 0x0

    .line 106
    .local v9, "szFileName":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdObjectTreeInfo()I

    move-result v8

    .line 107
    invoke-static {v8}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v9

    .line 108
    invoke-static {v8}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetFileSize(I)I

    move-result v5

    .line 109
    if-gtz v5, :cond_0

    move v0, v10

    .line 174
    :goto_0
    return v0

    .line 114
    :cond_0
    new-array v4, v5, [B

    .line 115
    .local v4, "tmp":[B
    invoke-static {v8, v10, v5, v4}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpReadFile(III[B)Z

    .line 118
    :try_start_0
    new-instance v6, Ljava/io/DataInputStream;

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v9}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "Input":Ljava/io/DataInputStream;
    .local v6, "Input":Ljava/io/DataInputStream;
    move-object v1, v6

    .line 125
    .end local v6    # "Input":Ljava/io/DataInputStream;
    .restart local v1    # "Input":Ljava/io/DataInputStream;
    :goto_1
    sget v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    aget-byte v2, v4, v0

    .line 126
    :cond_1
    const/16 v0, 0x42

    if-eq v2, v0, :cond_3

    .line 160
    if-eqz v1, :cond_2

    .line 161
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 168
    :cond_2
    :goto_2
    const/4 v4, 0x0

    .line 169
    sput v10, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 170
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdObjectData()I

    move-result v8

    .line 171
    invoke-static {v8}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetFileSize(I)I

    move-result v5

    .line 172
    iget-object v0, p0, Lcom/policydm/eng/core/XDMOmVfs;->stdobj_space:[B

    invoke-static {v8, v10, v5, v0}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpReadFile(III[B)Z

    move v0, v10

    .line 174
    goto :goto_0

    .line 120
    :catch_0
    move-exception v7

    .line 122
    .local v7, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 130
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :try_start_2
    iget-object v3, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackFsNode(Lcom/policydm/eng/core/XDMOmVfs;Ljava/io/DataInputStream;ILcom/policydm/eng/core/XDMVnode;[BI)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result v2

    .line 141
    if-nez v2, :cond_1

    .line 145
    if-eqz v1, :cond_4

    .line 146
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 152
    :cond_4
    :goto_3
    const/4 v4, 0x0

    .line 153
    sput v10, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 154
    const/4 v0, -0x4

    goto :goto_0

    .line 132
    :catch_1
    move-exception v7

    .line 134
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 137
    invoke-direct {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsResetStdobj()V

    .line 138
    invoke-static {}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsDeleteOmFile()V

    move v0, v10

    .line 139
    goto :goto_0

    .line 148
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v7

    .line 150
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 163
    .end local v7    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    .line 165
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static xdmOmVfsLoadFsData(Lcom/policydm/eng/core/XDMOmVfs;I[CI)I
    .locals 3
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "addr"    # I
    .param p2, "pBuff"    # [C
    .param p3, "nSize"    # I

    .prologue
    .line 847
    const/4 v0, 0x0

    .line 848
    .local v0, "i":I
    const/4 v0, 0x0

    :goto_0
    if-lt v0, p3, :cond_0

    .line 852
    const/4 v1, 0x0

    return v1

    .line 850
    :cond_0
    iget-object v1, p0, Lcom/policydm/eng/core/XDMOmVfs;->stdobj_space:[B

    add-int v2, p1, v0

    aget-byte v1, v1, v2

    int-to-char v1, v1

    aput-char v1, p2, v0

    .line 848
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static xdmOmVfsPackByte(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;
    .locals 0
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 331
    return-object p0
.end method

.method public static xdmOmVfsPackEnd(Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;
    .locals 1
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 324
    const/16 v0, 0x44

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 325
    return-object p0
.end method

.method public static xdmOmVfsPackFsNode(Ljava/io/DataOutputStream;Lcom/policydm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;
    .locals 1
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 224
    const/4 v0, 0x0

    .line 225
    .local v0, "cur":Lcom/policydm/eng/core/XDMVnode;
    if-nez p1, :cond_0

    .line 242
    :goto_0
    return-object p0

    .line 230
    :cond_0
    iget-object v0, p1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 232
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackStart(Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 233
    invoke-static {p0, p1}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackNode(Ljava/io/DataOutputStream;Lcom/policydm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 235
    :goto_1
    if-nez v0, :cond_1

    .line 241
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackEnd(Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 242
    goto :goto_0

    .line 237
    :cond_1
    invoke-static {p0, v0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackFsNode(Ljava/io/DataOutputStream;Lcom/policydm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 238
    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_1
.end method

.method public static xdmOmVfsPackInt16(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;
    .locals 0
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 371
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 372
    return-object p0
.end method

.method public static xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;
    .locals 0
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 365
    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 366
    return-object p0
.end method

.method public static xdmOmVfsPackNode(Ljava/io/DataOutputStream;Lcom/policydm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;
    .locals 5
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 254
    const/4 v2, 0x0

    .line 259
    .local v2, "num":I
    iget-object v1, p1, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    .line 260
    .local v1, "item":Lcom/policydm/eng/core/XDMOmList;
    :goto_0
    if-nez v1, :cond_1

    .line 266
    invoke-static {p0, v2}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackByte(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 268
    iget-object v1, p1, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    .line 269
    :goto_1
    if-nez v1, :cond_2

    .line 277
    iget v4, p1, Lcom/policydm/eng/core/XDMVnode;->format:I

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 278
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 279
    iget v4, p1, Lcom/policydm/eng/core/XDMVnode;->size:I

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 280
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 281
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->m_szTstamp:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 282
    iget v4, p1, Lcom/policydm/eng/core/XDMVnode;->scope:I

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 284
    const/4 v2, 0x0

    .line 285
    iget-object v1, p1, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 286
    :goto_2
    if-nez v1, :cond_3

    .line 291
    invoke-static {p0, v2}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackByte(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 292
    if-lez v2, :cond_0

    .line 294
    iget-object v1, p1, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 295
    :goto_3
    if-nez v1, :cond_4

    .line 303
    :cond_0
    iget v4, p1, Lcom/policydm/eng/core/XDMVnode;->verno:I

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackInt16(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 304
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->m_szDdfName:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 305
    iget v4, p1, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackInt32(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 307
    return-object p0

    .line 262
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 263
    iget-object v1, v1, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    goto :goto_0

    .line 271
    :cond_2
    iget-object v0, v1, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v0, Lcom/policydm/eng/core/XDMOmAcl;

    .line 272
    .local v0, "acl":Lcom/policydm/eng/core/XDMOmAcl;
    iget-object v4, v0, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 273
    iget v4, v0, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    invoke-static {p0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackByte(Ljava/io/DataOutputStream;I)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 274
    iget-object v1, v1, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    goto :goto_1

    .line 288
    .end local v0    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 289
    iget-object v1, v1, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    goto :goto_2

    .line 297
    :cond_4
    iget-object v3, v1, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 298
    .local v3, "szData":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;

    move-result-object p0

    .line 299
    iget-object v1, v1, Lcom/policydm/eng/core/XDMOmList;->next:Lcom/policydm/eng/core/XDMOmList;

    goto :goto_3
.end method

.method public static xdmOmVfsPackStart(Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;
    .locals 1
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 318
    const/16 v0, 0x42

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 319
    return-object p0
.end method

.method public static xdmOmVfsPackStr(Ljava/io/DataOutputStream;Ljava/lang/String;)Ljava/io/DataOutputStream;
    .locals 3
    .param p0, "pBuff"    # Ljava/io/DataOutputStream;
    .param p1, "szData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 336
    const/4 v1, 0x0

    .line 338
    .local v1, "len":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 340
    const/4 v1, 0x0

    .line 346
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 348
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 352
    :try_start_0
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :cond_0
    :goto_1
    return-object p0

    .line 344
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0

    .line 354
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static xdmOmVfsPath2Node(Lcom/policydm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;
    .locals 9
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 603
    const-string v5, ""

    .line 604
    .local v5, "szNodeName":Ljava/lang/String;
    move-object v6, p1

    .line 605
    .local v6, "szTempPath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 607
    .local v4, "ptNode":Lcom/policydm/eng/core/XDMVnode;
    iget-object v3, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    .line 609
    .local v3, "ptBaseNode":Lcom/policydm/eng/core/XDMVnode;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 632
    :cond_0
    :goto_0
    return-object v7

    .line 612
    :cond_1
    const-string v8, "."

    invoke-virtual {p1, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "./"

    invoke-virtual {p1, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_3

    .line 614
    :cond_2
    iget-object v7, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_0

    .line 617
    :cond_3
    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 618
    .local v2, "nodenamesplit":[Ljava/lang/String;
    array-length v1, v2

    .line 619
    .local v1, "l":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_4

    move-object v7, v4

    .line 632
    goto :goto_0

    .line 621
    :cond_4
    aget-object v5, v2, v0

    .line 623
    invoke-static {p0, v5, v3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetNode(Lcom/policydm/eng/core/XDMOmVfs;Ljava/lang/String;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v4

    .line 624
    if-eqz v4, :cond_0

    .line 629
    move-object v3, v4

    .line 619
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static xdmOmVfsRemoveNode(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Z)I
    .locals 7
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;
    .param p2, "deletechilds"    # Z

    .prologue
    const/4 v4, -0x4

    const/4 v6, 0x0

    .line 857
    iget-object v0, p1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 861
    .local v0, "cur":Lcom/policydm/eng/core/XDMVnode;
    if-eqz v0, :cond_3

    .line 863
    if-nez p2, :cond_2

    move v2, v4

    .line 931
    :cond_0
    :goto_0
    return v2

    .line 870
    :cond_1
    const/4 v5, 0x1

    invoke-static {p0, v0, v5}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsRemoveNode(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Z)I

    move-result v2

    .line 871
    .local v2, "ret":I
    if-nez v2, :cond_0

    .line 876
    iget-object v5, p1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    if-nez v5, :cond_4

    .line 878
    const/4 v0, 0x0

    .line 868
    .end local v2    # "ret":I
    :cond_2
    :goto_1
    if-nez v0, :cond_1

    .line 887
    :cond_3
    iget-object v5, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    invoke-static {p0, v5, p1}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetParent(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v1

    .line 888
    .local v1, "ptParent":Lcom/policydm/eng/core/XDMVnode;
    if-nez v1, :cond_5

    move v2, v4

    .line 890
    goto :goto_0

    .line 882
    .end local v1    # "ptParent":Lcom/policydm/eng/core/XDMVnode;
    .restart local v2    # "ret":I
    :cond_4
    iget-object v0, p1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_1

    .line 892
    .end local v2    # "ret":I
    .restart local v1    # "ptParent":Lcom/policydm/eng/core/XDMVnode;
    :cond_5
    iget-object v4, v1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    if-ne v4, p1, :cond_9

    .line 894
    iget-object v3, p1, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    .line 895
    .local v3, "tmp":Lcom/policydm/eng/core/XDMVnode;
    iput-object v3, v1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 911
    .end local v3    # "tmp":Lcom/policydm/eng/core/XDMVnode;
    :cond_6
    :goto_2
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v4, :cond_7

    .line 913
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsDeleteAclList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 915
    :cond_7
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    if-eqz v4, :cond_8

    .line 917
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsDeleteMimeList(Lcom/policydm/eng/core/XDMOmList;)V

    .line 920
    :cond_8
    iput-object v6, p1, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 921
    iput-object v6, p1, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 922
    iput-object v6, p1, Lcom/policydm/eng/core/XDMVnode;->m_szTstamp:Ljava/lang/String;

    .line 923
    iput-object v6, p1, Lcom/policydm/eng/core/XDMVnode;->m_szDdfName:Ljava/lang/String;

    .line 925
    iput-object v6, p1, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    .line 928
    iput-object v6, p1, Lcom/policydm/eng/core/XDMVnode;->ptParentNode:Lcom/policydm/eng/core/XDMVnode;

    .line 929
    const/4 p1, 0x0

    .line 931
    const/4 v2, 0x0

    goto :goto_0

    .line 899
    :cond_9
    iget-object v0, v1, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;

    .line 900
    :goto_3
    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    if-eqz v4, :cond_6

    .line 902
    iget-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    if-ne v4, p1, :cond_a

    .line 904
    iget-object v4, p1, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    iput-object v4, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_2

    .line 907
    :cond_a
    iget-object v0, v0, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;

    goto :goto_3
.end method

.method private xdmOmVfsResetStdobj()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMOmVfs;->stdobj_space:[B

    .line 36
    const v0, 0xa000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/policydm/eng/core/XDMOmVfs;->stdobj_space:[B

    .line 37
    return-void
.end method

.method public static xdmOmVfsSaveFs(Lcom/policydm/eng/core/XDMOmVfs;)I
    .locals 8
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    const/4 v0, 0x0

    .line 186
    .local v0, "Data":Ljava/io/DataOutputStream;
    const/4 v3, 0x0

    .line 188
    .local v3, "nFileId":I
    const/4 v5, 0x0

    .line 189
    .local v5, "szFileName":Ljava/lang/String;
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdObjectTreeInfo()I

    move-result v3

    .line 190
    invoke-static {v3}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpGetNameFromCallerID(I)Ljava/lang/String;

    move-result-object v5

    .line 194
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .local v1, "Data":Ljava/io/DataOutputStream;
    :try_start_1
    iget-object v6, p0, Lcom/policydm/eng/core/XDMOmVfs;->root:Lcom/policydm/eng/core/XDMVnode;

    iget-object v4, v6, Lcom/policydm/eng/core/XDMVnode;->childlist:Lcom/policydm/eng/core/XDMVnode;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .local v4, "ptNode":Lcom/policydm/eng/core/XDMVnode;
    move-object v0, v1

    .line 197
    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    :goto_0
    if-nez v4, :cond_0

    .line 202
    :try_start_2
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 209
    .end local v4    # "ptNode":Lcom/policydm/eng/core/XDMVnode;
    :goto_1
    invoke-static {}, Lcom/policydm/db/XDB;->xdbGetFileIdObjectData()I

    move-result v3

    .line 210
    const v6, 0xa000

    iget-object v7, p0, Lcom/policydm/eng/core/XDMOmVfs;->stdobj_space:[B

    invoke-static {v3, v6, v7}, Lcom/policydm/db/XDBFileAdapter;->xdbAdpWriteFile(IILjava/lang/Object;)Z

    .line 212
    const/4 v6, 0x0

    return v6

    .line 199
    .restart local v4    # "ptNode":Lcom/policydm/eng/core/XDMVnode;
    :cond_0
    :try_start_3
    invoke-static {v0, v4}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPackFsNode(Ljava/io/DataOutputStream;Lcom/policydm/eng/core/XDMVnode;)Ljava/io/DataOutputStream;

    move-result-object v0

    .line 200
    iget-object v4, v4, Lcom/policydm/eng/core/XDMVnode;->next:Lcom/policydm/eng/core/XDMVnode;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 204
    .end local v4    # "ptNode":Lcom/policydm/eng/core/XDMVnode;
    :catch_0
    move-exception v2

    .line 206
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1

    .line 204
    .end local v0    # "Data":Ljava/io/DataOutputStream;
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "Data":Ljava/io/DataOutputStream;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "Data":Ljava/io/DataOutputStream;
    .restart local v0    # "Data":Ljava/io/DataOutputStream;
    goto :goto_2
.end method

.method public static xdmOmVfsSaveFsData(Lcom/policydm/eng/core/XDMOmVfs;ILjava/lang/Object;I)I
    .locals 6
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "l"    # I
    .param p2, "pBuff"    # Ljava/lang/Object;
    .param p3, "nSize"    # I

    .prologue
    .line 833
    const/4 v1, 0x0

    .line 834
    .local v1, "i":I
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 835
    .local v2, "szTmp":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 836
    .local v0, "data":[B
    const/4 v1, 0x0

    :goto_0
    if-lt v1, p3, :cond_1

    .line 842
    :cond_0
    const/4 v3, 0x0

    return v3

    .line 838
    :cond_1
    array-length v3, v0

    if-le v3, v1, :cond_0

    .line 840
    iget-object v3, p0, Lcom/policydm/eng/core/XDMOmVfs;->stdobj_space:[B

    add-int v4, p1, v1

    aget-byte v5, v0, v1

    aput-byte v5, v3, v4

    .line 836
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static xdmOmVfsSetData(Lcom/policydm/eng/core/XDMOmVfs;Lcom/policydm/eng/core/XDMVnode;Ljava/lang/Object;I)I
    .locals 2
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;
    .param p2, "pBuff"    # Ljava/lang/Object;
    .param p3, "nBuffSize"    # I

    .prologue
    .line 1016
    invoke-static {p0, p3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetFreeVaddr(Lcom/policydm/eng/core/XDMOmVfs;I)I

    move-result v0

    .line 1017
    .local v0, "addr":I
    if-gez v0, :cond_0

    .line 1031
    .end local v0    # "addr":I
    :goto_0
    return v0

    .line 1022
    .restart local v0    # "addr":I
    :cond_0
    iput v0, p1, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    .line 1023
    iput p3, p1, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 1025
    invoke-static {p0, v0, p2, p3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsSaveFsData(Lcom/policydm/eng/core/XDMOmVfs;ILjava/lang/Object;I)I

    move-result v1

    .line 1026
    .local v1, "ret":I
    if-eqz v1, :cond_1

    .line 1028
    const/4 v0, -0x4

    goto :goto_0

    .line 1031
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static xdmOmVfsUnpackFsNode(Lcom/policydm/eng/core/XDMOmVfs;Ljava/io/DataInputStream;ILcom/policydm/eng/core/XDMVnode;[BI)I
    .locals 9
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "in"    # Ljava/io/DataInputStream;
    .param p2, "pBuff"    # I
    .param p3, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;
    .param p4, "buf"    # [B
    .param p5, "nSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v8, 0x42

    const/4 v6, 0x0

    const/16 v7, 0x44

    .line 378
    move v2, p2

    .line 380
    .local v2, "ptr":I
    if-nez p1, :cond_2

    move v0, v6

    .line 416
    :goto_0
    return v0

    .line 385
    :cond_0
    if-ne v2, v8, :cond_5

    .line 387
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 388
    sget v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 389
    new-instance v3, Lcom/policydm/eng/core/XDMVnode;

    invoke-direct {v3}, Lcom/policydm/eng/core/XDMVnode;-><init>()V

    .line 390
    .local v3, "ptChild":Lcom/policydm/eng/core/XDMVnode;
    invoke-static {p1, v3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackNode(Ljava/io/DataInputStream;Lcom/policydm/eng/core/XDMVnode;)Ljava/io/DataInputStream;

    move-result-object p1

    .line 391
    invoke-static {p3, v3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsAppendNode(Lcom/policydm/eng/core/XDMVnode;Lcom/policydm/eng/core/XDMVnode;)I

    .line 393
    sget v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    aget-byte v2, p4, v0

    .line 394
    :cond_1
    :goto_1
    if-ne v2, v7, :cond_3

    .line 383
    .end local v3    # "ptChild":Lcom/policydm/eng/core/XDMVnode;
    :cond_2
    if-ne v2, v7, :cond_0

    .line 411
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v2

    .line 412
    sget v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 413
    sget v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    if-ne v0, p5, :cond_6

    move v0, v7

    .line 414
    goto :goto_0

    .line 396
    .restart local v3    # "ptChild":Lcom/policydm/eng/core/XDMVnode;
    :cond_3
    if-ne v2, v8, :cond_4

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    move v5, p5

    .line 398
    invoke-static/range {v0 .. v5}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackFsNode(Lcom/policydm/eng/core/XDMOmVfs;Ljava/io/DataInputStream;ILcom/policydm/eng/core/XDMVnode;[BI)I

    move-result v2

    .line 399
    goto :goto_1

    .line 400
    :cond_4
    if-eq v2, v7, :cond_1

    move v0, v6

    .line 402
    goto :goto_0

    .line 406
    .end local v3    # "ptChild":Lcom/policydm/eng/core/XDMVnode;
    :cond_5
    if-eq v2, v7, :cond_2

    move v0, v6

    .line 408
    goto :goto_0

    .line 416
    :cond_6
    sget v0, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    aget-byte v0, p4, v0

    goto :goto_0
.end method

.method public static xdmOmVfsUnpackNode(Ljava/io/DataInputStream;Lcom/policydm/eng/core/XDMVnode;)Ljava/io/DataInputStream;
    .locals 8
    .param p0, "in"    # Ljava/io/DataInputStream;
    .param p1, "ptNode"    # Lcom/policydm/eng/core/XDMVnode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 421
    const/4 v4, 0x0

    .line 424
    .local v4, "num":I
    const/4 v6, 0x0

    .line 426
    .local v6, "szTmp":Ljava/lang/String;
    const-string v5, ""

    .line 428
    .local v5, "szData":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 429
    sget v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 430
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_1

    .line 443
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/policydm/eng/core/XDMVnode;->format:I

    .line 444
    sget v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 445
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p1, Lcom/policydm/eng/core/XDMVnode;->m_szName:Ljava/lang/String;

    .line 446
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 447
    sget v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 448
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p1, Lcom/policydm/eng/core/XDMVnode;->title:Ljava/lang/String;

    .line 449
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p1, Lcom/policydm/eng/core/XDMVnode;->m_szTstamp:Ljava/lang/String;

    .line 450
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/policydm/eng/core/XDMVnode;->scope:I

    .line 451
    sget v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 452
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    .line 453
    sget v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 454
    if-lez v4, :cond_0

    .line 456
    const/4 v2, 0x0

    :goto_1
    if-lt v2, v4, :cond_2

    .line 465
    :cond_0
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/policydm/eng/core/XDMVnode;->verno:I

    .line 466
    sget v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 467
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p1, Lcom/policydm/eng/core/XDMVnode;->m_szDdfName:Ljava/lang/String;

    .line 468
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    iput v7, p1, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    .line 469
    sget v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 471
    return-object p0

    .line 432
    :cond_1
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStr(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v6

    .line 433
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 434
    .local v0, "ac":I
    sget v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v7, v7, 0x4

    sput v7, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 436
    new-instance v1, Lcom/policydm/eng/core/XDMOmAcl;

    invoke-direct {v1}, Lcom/policydm/eng/core/XDMOmAcl;-><init>()V

    .line 437
    .local v1, "acl":Lcom/policydm/eng/core/XDMOmAcl;
    iput-object v6, v1, Lcom/policydm/eng/core/XDMOmAcl;->m_szServerid:Ljava/lang/String;

    .line 438
    iput v0, v1, Lcom/policydm/eng/core/XDMOmAcl;->ac:I

    .line 439
    new-instance v3, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v3}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 440
    .local v3, "item":Lcom/policydm/eng/core/XDMOmList;
    iput-object v1, v3, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 441
    iget-object v7, p1, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v7, v3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsAppendList(Lcom/policydm/eng/core/XDMOmList;Lcom/policydm/eng/core/XDMOmList;)Lcom/policydm/eng/core/XDMOmList;

    move-result-object v7

    iput-object v7, p1, Lcom/policydm/eng/core/XDMVnode;->acl:Lcom/policydm/eng/core/XDMOmList;

    .line 430
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 458
    .end local v0    # "ac":I
    .end local v1    # "acl":Lcom/policydm/eng/core/XDMOmAcl;
    .end local v3    # "item":Lcom/policydm/eng/core/XDMOmList;
    :cond_2
    invoke-static {p0}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsUnpackStr(Ljava/io/DataInputStream;)Ljava/lang/String;

    move-result-object v6

    .line 459
    move-object v5, v6

    .line 460
    new-instance v3, Lcom/policydm/eng/core/XDMOmList;

    invoke-direct {v3}, Lcom/policydm/eng/core/XDMOmList;-><init>()V

    .line 461
    .restart local v3    # "item":Lcom/policydm/eng/core/XDMOmList;
    iput-object v5, v3, Lcom/policydm/eng/core/XDMOmList;->data:Ljava/lang/Object;

    .line 462
    iget-object v7, p1, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    invoke-static {v7, v3}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsAppendList(Lcom/policydm/eng/core/XDMOmList;Lcom/policydm/eng/core/XDMOmList;)Lcom/policydm/eng/core/XDMOmList;

    move-result-object v7

    iput-object v7, p1, Lcom/policydm/eng/core/XDMVnode;->type:Lcom/policydm/eng/core/XDMOmList;

    .line 456
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static xdmOmVfsUnpackStr(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 5
    .param p0, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 476
    const/4 v0, 0x0

    .line 477
    .local v0, "len":I
    const/4 v2, 0x0

    .line 478
    .local v2, "tmp":[B
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 479
    sget v3, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v3, v3, 0x4

    sput v3, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 481
    if-nez v0, :cond_0

    .line 483
    const/4 v1, 0x0

    .line 496
    :goto_0
    return-object v1

    .line 485
    :cond_0
    const/16 v3, 0x200

    if-le v0, v3, :cond_1

    .line 487
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "OM_MAX_LEN over"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 490
    :cond_1
    new-array v2, v0, [B

    .line 491
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v0}, Ljava/io/DataInputStream;->read([BII)I

    .line 493
    sget v3, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/2addr v3, v0

    sput v3, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 495
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 496
    .local v1, "szRet":Ljava/lang/String;
    goto :goto_0
.end method

.method public static xdmOmVfsUnpackStrDup(Ljava/io/DataInputStream;)Ljava/lang/String;
    .locals 5
    .param p0, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 502
    const/4 v2, 0x0

    .line 503
    .local v2, "tmp":[B
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 504
    .local v0, "len":I
    sget v3, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/lit8 v3, v3, 0x4

    sput v3, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 506
    if-nez v0, :cond_0

    .line 508
    const/4 v1, 0x0

    .line 522
    :goto_0
    return-object v1

    .line 510
    :cond_0
    const/16 v3, 0x200

    if-le v0, v3, :cond_1

    .line 512
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "OM_MAX_LEN over"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 515
    :cond_1
    new-array v2, v0, [B

    .line 516
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v0}, Ljava/io/DataInputStream;->read([BII)I

    .line 518
    sget v3, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    add-int/2addr v3, v0

    sput v3, Lcom/policydm/eng/core/XDMOmVfs;->index:I

    .line 520
    new-instance v1, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 522
    .local v1, "szRet":Ljava/lang/String;
    goto :goto_0
.end method

.method public static xdmOmVfsWriteObj(Lcom/policydm/eng/core/XDMOmVfs;Ljava/lang/String;IILjava/lang/Object;I)I
    .locals 5
    .param p0, "pVfs"    # Lcom/policydm/eng/core/XDMOmVfs;
    .param p1, "szPath"    # Ljava/lang/String;
    .param p2, "nTotalSize"    # I
    .param p3, "nOffset"    # I
    .param p4, "pBuff"    # Ljava/lang/Object;
    .param p5, "nBuffSize"    # I

    .prologue
    .line 709
    const/4 v0, 0x0

    .line 713
    .local v0, "addr":I
    invoke-static {p0, p1}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsPath2Node(Lcom/policydm/eng/core/XDMOmVfs;Ljava/lang/String;)Lcom/policydm/eng/core/XDMVnode;

    move-result-object v2

    .line 714
    .local v2, "ptNode":Lcom/policydm/eng/core/XDMVnode;
    if-nez v2, :cond_0

    .line 716
    const/4 v4, -0x1

    .line 745
    :goto_0
    return v4

    .line 718
    :cond_0
    if-nez p3, :cond_3

    .line 721
    invoke-static {p0, p2}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsGetFreeVaddr(Lcom/policydm/eng/core/XDMOmVfs;I)I

    move-result v0

    .line 722
    if-gez v0, :cond_1

    move v4, v0

    .line 724
    goto :goto_0

    .line 726
    :cond_1
    iput v0, v2, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    .line 727
    iput p2, v2, Lcom/policydm/eng/core/XDMVnode;->size:I

    .line 734
    :goto_1
    add-int v1, p3, p5

    .line 735
    .local v1, "blocksize":I
    if-le v1, p2, :cond_2

    .line 737
    sub-int v4, v1, p2

    sub-int/2addr p5, v4

    .line 740
    :cond_2
    add-int v4, v0, p3

    invoke-static {p0, v4, p4, p5}, Lcom/policydm/eng/core/XDMOmVfs;->xdmOmVfsSaveFsData(Lcom/policydm/eng/core/XDMOmVfs;ILjava/lang/Object;I)I

    move-result v3

    .line 741
    .local v3, "ret":I
    if-eqz v3, :cond_4

    .line 743
    const/4 v4, -0x4

    goto :goto_0

    .line 731
    .end local v1    # "blocksize":I
    .end local v3    # "ret":I
    :cond_3
    iget v0, v2, Lcom/policydm/eng/core/XDMVnode;->vaddr:I

    goto :goto_1

    .restart local v1    # "blocksize":I
    .restart local v3    # "ret":I
    :cond_4
    move v4, p5

    .line 745
    goto :goto_0
.end method
