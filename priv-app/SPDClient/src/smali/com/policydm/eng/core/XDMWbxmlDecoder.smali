.class public Lcom/policydm/eng/core/XDMWbxmlDecoder;
.super Ljava/lang/Object;
.source "XDMWbxmlDecoder.java"

# interfaces
.implements Lcom/policydm/eng/core/XDMWbxml;


# instance fields
.field public m_szStringT:Ljava/lang/String;

.field protected wbxbuff:[B

.field public wbxindex:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object v1, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->wbxbuff:[B

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->wbxindex:I

    .line 14
    iput-object v1, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->m_szStringT:Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public xdmWbxDecInit([BI)V
    .locals 0
    .param p1, "input"    # [B
    .param p2, "index"    # I

    .prologue
    .line 18
    iput-object p1, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->wbxbuff:[B

    .line 19
    iput p2, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->wbxindex:I

    .line 20
    return-void
.end method

.method public xdmWbxDecParseExtension(I)Ljava/lang/String;
    .locals 9
    .param p1, "type"    # I

    .prologue
    .line 100
    const/4 v5, 0x0

    .line 101
    .local v5, "szRet":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 105
    .local v0, "buf":Ljava/io/ByteArrayOutputStream;
    const/16 v7, 0xc3

    if-ne p1, v7, :cond_0

    .line 107
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferMbUINT32()I

    move-result v4

    .line 108
    .local v4, "len":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_1

    .line 113
    new-instance v6, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .end local v5    # "szRet":Ljava/lang/String;
    .local v6, "szRet":Ljava/lang/String;
    move-object v5, v6

    .line 121
    .end local v2    # "i":I
    .end local v4    # "len":I
    .end local v6    # "szRet":Ljava/lang/String;
    .restart local v5    # "szRet":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v5

    .line 110
    .restart local v2    # "i":I
    .restart local v4    # "len":I
    :cond_1
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferByte()I

    move-result v3

    .line 111
    .local v3, "j":I
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 116
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "len":I
    :catch_0
    move-exception v1

    .line 118
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public xdmWbxDecParseStartdoc(Lcom/policydm/eng/parser/XDMParser;)V
    .locals 3
    .param p1, "parser"    # Lcom/policydm/eng/parser/XDMParser;

    .prologue
    .line 26
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferByte()I

    move-result v1

    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->version:I

    .line 27
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferMbUINT32()I

    move-result v1

    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->puid:I

    .line 29
    iget v1, p1, Lcom/policydm/eng/parser/XDMParser;->puid:I

    if-nez v1, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferMbUINT32()I

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferMbUINT32()I

    move-result v1

    iput v1, p1, Lcom/policydm/eng/parser/XDMParser;->charset:I

    .line 36
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecParseStringtable()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/policydm/eng/parser/XDMParser;->m_szStringtable:Ljava/lang/String;

    .line 37
    new-instance v1, Ljava/lang/String;

    iget-object v2, p1, Lcom/policydm/eng/parser/XDMParser;->m_szStringtable:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->m_szStringT:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xdmWbxDecParseStr_i()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 78
    .local v0, "buf":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferByte()I

    move-result v1

    .line 80
    .local v1, "i":I
    if-nez v1, :cond_0

    .line 93
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 94
    .local v2, "szRet":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 95
    return-object v2

    .line 85
    .end local v2    # "szRet":Ljava/lang/String;
    :cond_0
    const/4 v3, -0x1

    if-ne v1, v3, :cond_1

    .line 87
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Unexpected EOF wbxdec_parse_str_i"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 90
    :cond_1
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0
.end method

.method public xdmWbxDecParseStr_t()Ljava/lang/String;
    .locals 8

    .prologue
    .line 48
    const/4 v4, 0x0

    .line 50
    .local v4, "len":I
    const/4 v2, 0x0

    .line 51
    .local v2, "i":I
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 54
    .local v0, "buf":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferMbUINT32()I

    move-result v4

    .line 55
    move v2, v4

    .line 56
    :goto_0
    iget-object v6, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->m_szStringT:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/String;->charAt(I)C
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_0

    .line 68
    :goto_1
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 69
    .local v5, "szRet":Ljava/lang/String;
    return-object v5

    .line 58
    .end local v5    # "szRet":Ljava/lang/String;
    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->m_szStringT:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 59
    .local v3, "j":I
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 60
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 63
    .end local v3    # "j":I
    :catch_0
    move-exception v1

    .line 65
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public xdmWbxDecParseStringtable()Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    const/4 v5, 0x0

    .line 127
    .local v5, "szRet":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 132
    .local v0, "buf":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferMbUINT32()I

    move-result v4

    .line 134
    .local v4, "len":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v4, :cond_0

    .line 140
    new-instance v6, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .end local v5    # "szRet":Ljava/lang/String;
    .local v6, "szRet":Ljava/lang/String;
    move-object v5, v6

    .line 147
    .end local v2    # "i":I
    .end local v4    # "len":I
    .end local v6    # "szRet":Ljava/lang/String;
    .restart local v5    # "szRet":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 136
    .restart local v2    # "i":I
    .restart local v4    # "len":I
    :cond_0
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferByte()I

    move-result v3

    .line 137
    .local v3, "j":I
    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 142
    .end local v2    # "i":I
    .end local v3    # "j":I
    .end local v4    # "len":I
    :catch_0
    move-exception v1

    .line 144
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public xdmWbxDecReadBufferByte()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 177
    iget-object v1, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->wbxbuff:[B

    iget v2, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->wbxindex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/policydm/eng/core/XDMWbxmlDecoder;->wbxindex:I

    aget-byte v1, v1, v2

    and-int/lit16 v0, v1, 0xff

    .line 178
    .local v0, "data":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 179
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unexpected EOF wbxdec_buffer_read_byte"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 181
    :cond_0
    return v0
.end method

.method public xdmWbxDecReadBufferMbUINT32()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    const/4 v2, 0x0

    .line 153
    .local v2, "result":I
    const/4 v4, 0x0

    .line 156
    .local v4, "uint":I
    const/4 v0, 0x0

    .local v0, "byte_pos":I
    :goto_0
    const/4 v5, 0x5

    if-lt v0, v5, :cond_0

    move v3, v2

    .line 172
    .end local v2    # "result":I
    .local v3, "result":I
    :goto_1
    return v3

    .line 158
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :cond_0
    invoke-virtual {p0}, Lcom/policydm/eng/core/XDMWbxmlDecoder;->xdmWbxDecReadBufferByte()I

    move-result v1

    .local v1, "cur_byte":I
    if-gez v1, :cond_1

    move v3, v2

    .line 160
    .end local v2    # "result":I
    .restart local v3    # "result":I
    goto :goto_1

    .line 163
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :cond_1
    shl-int/lit8 v5, v4, 0x7

    and-int/lit8 v6, v1, 0x7f

    or-int v4, v5, v6

    .line 165
    and-int/lit16 v5, v1, 0x80

    if-nez v5, :cond_2

    .line 167
    move v2, v4

    move v3, v2

    .line 168
    .end local v2    # "result":I
    .restart local v3    # "result":I
    goto :goto_1

    .line 156
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
