.class public Lcom/policydm/eng/core/XDMAccXNode;
.super Lcom/policydm/db/XDBAccXNodeInfo;
.source "XDMAccXNode.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/policydm/db/XDBAccXNodeInfo;-><init>()V

    .line 11
    new-instance v0, Ljava/lang/String;

    const-string v1, "./DMAcc/SPD"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAccount:Ljava/lang/String;

    .line 12
    new-instance v0, Ljava/lang/String;

    const-string v1, "./DMAcc/SPD/AppAddr/spd"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddr:Ljava/lang/String;

    .line 13
    new-instance v0, Ljava/lang/String;

    const-string v1, "./DMAcc/SPD/AppAddr/spd/Port/spd"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMAccXNode;->m_szAppAddrPort:Ljava/lang/String;

    .line 14
    new-instance v0, Ljava/lang/String;

    const-string v1, "./DMAcc/SPD/AppAuth/client"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMAccXNode;->m_szClientAppAuth:Ljava/lang/String;

    .line 15
    new-instance v0, Ljava/lang/String;

    const-string v1, "./DMAcc/SPD/AppAuth/server"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMAccXNode;->m_szServerAppAuth:Ljava/lang/String;

    .line 16
    new-instance v0, Ljava/lang/String;

    const-string v1, "./DMAcc/SPD/ToConRef/Connectivity Reference Name"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMAccXNode;->m_szToConRef:Ljava/lang/String;

    .line 17
    return-void
.end method
