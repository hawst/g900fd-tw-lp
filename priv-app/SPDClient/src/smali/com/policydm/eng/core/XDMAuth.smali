.class public Lcom/policydm/eng/core/XDMAuth;
.super Ljava/lang/Object;
.source "XDMAuth.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;


# static fields
.field private static final SHA_KEY_PAD_LEN:I = 0x40

.field private static final SHA_KEY_PAD_LEN_:I = 0x40


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmAuthAAuthType2String(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 48
    packed-switch p0, :pswitch_data_0

    .line 57
    const-string v0, "NONE"

    :goto_0
    return-object v0

    .line 51
    :pswitch_0
    const-string v0, "BASIC"

    goto :goto_0

    .line 53
    :pswitch_1
    const-string v0, "DIGEST"

    goto :goto_0

    .line 55
    :pswitch_2
    const-string v0, "HMAC"

    goto :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdmAuthAAuthtring2Type(Ljava/lang/String;)I
    .locals 1
    .param p0, "szType"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string v0, "BASIC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 64
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    .line 65
    :cond_0
    const-string v0, "DIGEST"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 66
    const/4 v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    const-string v0, "HMAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 68
    const/4 v0, 0x2

    goto :goto_0

    .line 70
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xdmAuthCredString2Type(Ljava/lang/String;)I
    .locals 1
    .param p0, "szType"    # Ljava/lang/String;

    .prologue
    .line 36
    const-string v0, "syncml:auth-basic"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 37
    const/4 v0, 0x0

    .line 43
    :goto_0
    return v0

    .line 38
    :cond_0
    const-string v0, "syncml:auth-md5"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 39
    const/4 v0, 0x1

    goto :goto_0

    .line 40
    :cond_1
    const-string v0, "syncml:auth-MAC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 41
    const/4 v0, 0x2

    goto :goto_0

    .line 43
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xdmAuthCredType2String(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 20
    packed-switch p0, :pswitch_data_0

    .line 29
    const-string v0, "Not Support Auth Type"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 23
    :pswitch_0
    const-string v0, "syncml:auth-basic"

    goto :goto_0

    .line 25
    :pswitch_1
    const-string v0, "syncml:auth-md5"

    goto :goto_0

    .line 27
    :pswitch_2
    const-string v0, "syncml:auth-MAC"

    goto :goto_0

    .line 20
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static xdmAuthMakeDigest(ILjava/lang/String;Ljava/lang/String;[BI[BI)Ljava/lang/String;
    .locals 16
    .param p0, "authType"    # I
    .param p1, "szUserName"    # Ljava/lang/String;
    .param p2, "szPassWord"    # Ljava/lang/String;
    .param p3, "nonce"    # [B
    .param p4, "nonceLength"    # I
    .param p5, "packetBody"    # [B
    .param p6, "bodyLength"    # I

    .prologue
    .line 75
    const/4 v13, 0x0

    .line 76
    .local v13, "szRet":Ljava/lang/String;
    const/4 v10, 0x0

    .line 78
    .local v10, "szCredData":Ljava/lang/String;
    const/16 v14, 0x10

    new-array v3, v14, [B

    .line 80
    .local v3, "digest":[B
    packed-switch p0, :pswitch_data_0

    .line 112
    const-string v14, "Not Support Auth Type"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 113
    const/4 v14, 0x0

    .line 192
    :goto_0
    return-object v14

    .line 84
    :pswitch_0
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_0

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 86
    :cond_0
    const-string v14, "userName or passWord is NULL"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 87
    const/4 v14, 0x0

    goto :goto_0

    .line 93
    :pswitch_1
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    if-eqz p3, :cond_1

    if-gtz p4, :cond_3

    .line 95
    :cond_1
    const-string v14, "userName or passWord or nonce or nonceLength is NULL"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 96
    const/4 v14, 0x0

    goto :goto_0

    .line 103
    :pswitch_2
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2

    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2

    if-eqz p3, :cond_2

    if-lez p4, :cond_2

    if-eqz p5, :cond_2

    if-gtz p6, :cond_3

    .line 105
    :cond_2
    const-string v14, "userName or passWord or nonce or nonceLength or packetBody or bodyLength is NULL"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 106
    const/4 v14, 0x0

    goto :goto_0

    .line 117
    :cond_3
    packed-switch p0, :pswitch_data_1

    :goto_1
    move-object v14, v13

    .line 192
    goto :goto_0

    .line 121
    :pswitch_3
    move-object/from16 v10, p1

    .line 122
    const-string v14, ":"

    invoke-virtual {v10, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 123
    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 124
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v3

    .line 125
    new-instance v13, Ljava/lang/String;

    .end local v13    # "szRet":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v13, v3, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 127
    .restart local v13    # "szRet":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "XDM_CRED_TYPE_BASIC cred:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ret:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 128
    const/4 v10, 0x0

    .line 129
    goto :goto_1

    .line 133
    :pswitch_4
    new-instance v8, Lcom/policydm/eng/core/XDMMD5;

    invoke-direct {v8}, Lcom/policydm/eng/core/XDMMD5;-><init>()V

    .line 134
    .local v8, "md5":Lcom/policydm/eng/core/XDMMD5;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v8, v0, v1, v2}, Lcom/policydm/eng/core/XDMMD5;->xdmComputeMD5Credentials(Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v9

    .line 135
    .local v9, "md5digest":[B
    new-instance v13, Ljava/lang/String;

    .end local v13    # "szRet":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v13, v9, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 137
    .restart local v13    # "szRet":Ljava/lang/String;
    new-instance v12, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-direct {v12, v0, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 138
    .local v12, "szNon":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "XDM_CRED_TYPE_MD5 nonce= "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " ret= "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 145
    .end local v8    # "md5":Lcom/policydm/eng/core/XDMMD5;
    .end local v9    # "md5digest":[B
    .end local v12    # "szNon":Ljava/lang/String;
    :pswitch_5
    const/4 v8, 0x0

    .line 148
    .local v8, "md5":Ljava/security/MessageDigest;
    :try_start_0
    const-string v14, "MD5"

    invoke-static {v14}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 156
    :goto_2
    if-nez v8, :cond_4

    .line 157
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 150
    :catch_0
    move-exception v4

    .line 152
    .local v4, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 159
    .end local v4    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_4
    move-object/from16 v10, p1

    .line 160
    const-string v14, ":"

    invoke-virtual {v10, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 161
    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 162
    invoke-virtual {v8}, Ljava/security/MessageDigest;->reset()V

    .line 164
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v14

    invoke-virtual {v8, v14}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    .line 165
    invoke-static {v3}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v5

    .line 167
    .local v5, "encoder":[B
    new-instance v10, Ljava/lang/String;

    .end local v10    # "szCredData":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v10, v5, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 168
    .restart local v10    # "szCredData":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-virtual {v8, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    .line 169
    invoke-static {v3}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v6

    .line 170
    .local v6, "encoder2":[B
    new-instance v11, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v11, v6, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 172
    .local v11, "szDataBody":Ljava/lang/String;
    const-string v14, ":"

    invoke-virtual {v10, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 173
    new-instance v14, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-direct {v14, v0, v15}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v10, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 174
    const-string v14, ":"

    invoke-virtual {v10, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 175
    invoke-virtual {v10, v11}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 176
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-virtual {v10, v14}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v14

    invoke-virtual {v8, v14}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    .line 177
    const/4 v14, 0x2

    move/from16 v0, p0

    if-ne v0, v14, :cond_5

    .line 179
    invoke-static {v3}, Lcom/policydm/eng/core/XDMBase64;->xdmBase64Encode([B)[B

    move-result-object v7

    .line 180
    .local v7, "encoder3":[B
    new-instance v13, Ljava/lang/String;

    .end local v13    # "szRet":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v13, v7, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 181
    .restart local v13    # "szRet":Ljava/lang/String;
    goto/16 :goto_1

    .line 185
    .end local v7    # "encoder3":[B
    :cond_5
    new-instance v13, Ljava/lang/String;

    .end local v13    # "szRet":Ljava/lang/String;
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-direct {v13, v3, v14}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 187
    .restart local v13    # "szRet":Ljava/lang/String;
    goto/16 :goto_1

    .line 80
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 117
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static xdmAuthMakeDigestSHA1(I[BI[B)Ljava/lang/String;
    .locals 11
    .param p0, "nAuthType"    # I
    .param p1, "pszSecretKey"    # [B
    .param p2, "nSecertLen"    # I
    .param p3, "pszPacketBody"    # [B

    .prologue
    .line 197
    move v4, p2

    .line 198
    .local v4, "nSecretLen":I
    const/16 v10, 0x40

    new-array v7, v10, [B

    .line 199
    .local v7, "szK_IPad":[B
    const/16 v10, 0x40

    new-array v8, v10, [B

    .line 200
    .local v8, "szK_OPad":[B
    const/4 v9, 0x0

    .line 201
    .local v9, "szTemp":[B
    const/4 v0, 0x0

    .line 202
    .local v0, "digest":[B
    const-string v6, ""

    .line 204
    .local v6, "szDigest":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 213
    const-string v10, "Not Support Auth Type."

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 214
    const/4 v10, 0x0

    .line 266
    :goto_0
    return-object v10

    .line 207
    :pswitch_0
    if-eqz p1, :cond_0

    if-nez p3, :cond_1

    .line 209
    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    .line 217
    :cond_1
    const/16 v10, 0x40

    if-le v4, v10, :cond_3

    .line 229
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/16 v10, 0x40

    if-lt v2, v10, :cond_4

    .line 234
    const/4 v3, 0x0

    .local v3, "nCount":I
    :goto_2
    const/16 v10, 0x40

    if-lt v3, v10, :cond_5

    .line 240
    const/4 v5, 0x0

    .line 243
    .local v5, "sha":Ljava/security/MessageDigest;
    :try_start_0
    const-string v10, "SHA-1"

    invoke-static {v10}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 251
    :goto_3
    if-nez v5, :cond_6

    .line 252
    const/4 v10, 0x0

    goto :goto_0

    .line 223
    .end local v2    # "i":I
    .end local v3    # "nCount":I
    .end local v5    # "sha":Ljava/security/MessageDigest;
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    if-ge v2, v4, :cond_2

    .line 225
    aget-byte v10, p1, v2

    aput-byte v10, v7, v2

    .line 223
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 231
    :cond_4
    aget-byte v10, v7, v2

    aput-byte v10, v8, v2

    .line 229
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 236
    .restart local v3    # "nCount":I
    :cond_5
    aget-byte v10, v7, v3

    xor-int/lit8 v10, v10, 0x36

    int-to-byte v10, v10

    aput-byte v10, v7, v3

    .line 237
    aget-byte v10, v8, v3

    xor-int/lit8 v10, v10, 0x5c

    int-to-byte v10, v10

    aput-byte v10, v8, v3

    .line 234
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 245
    .restart local v5    # "sha":Ljava/security/MessageDigest;
    :catch_0
    move-exception v1

    .line 247
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_3

    .line 254
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_6
    invoke-virtual {v5, v7}, Ljava/security/MessageDigest;->update([B)V

    .line 255
    invoke-virtual {v5, p3}, Ljava/security/MessageDigest;->update([B)V

    .line 256
    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v9

    .line 258
    invoke-virtual {v5, v8}, Ljava/security/MessageDigest;->update([B)V

    .line 259
    invoke-virtual {v5, v9}, Ljava/security/MessageDigest;->update([B)V

    .line 260
    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    .line 262
    invoke-static {v0}, Lcom/policydm/eng/core/XDMMem;->xdmLibBytesToHexString([B)Ljava/lang/String;

    move-result-object v6

    .line 263
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 264
    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v6, v10}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    :cond_7
    move-object v10, v6

    .line 266
    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
