.class public Lcom/policydm/eng/core/XDMLinkedList;
.super Ljava/lang/Object;
.source "XDMLinkedList.java"


# instance fields
.field public count:I

.field public cur:Lcom/policydm/eng/core/XDMNode;

.field public top:Lcom/policydm/eng/core/XDMNode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static xdmListAddObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V
    .locals 3
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 98
    iget-object v1, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 99
    .local v1, "top":Lcom/policydm/eng/core/XDMNode;
    new-instance v0, Lcom/policydm/eng/core/XDMNode;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMNode;-><init>()V

    .line 101
    .local v0, "node":Lcom/policydm/eng/core/XDMNode;
    invoke-static {v0, p1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListBindObjectToNode(Lcom/policydm/eng/core/XDMNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v2, v1, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    iput-object v2, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 104
    iput-object v1, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 105
    iput-object v0, v1, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 106
    iget-object v2, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    iput-object v0, v2, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 108
    iget v2, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    .line 109
    return-void
.end method

.method public static xdmListAddObjAtLast(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;)V
    .locals 3
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 118
    iget-object v1, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 119
    .local v1, "top":Lcom/policydm/eng/core/XDMNode;
    new-instance v0, Lcom/policydm/eng/core/XDMNode;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMNode;-><init>()V

    .line 121
    .local v0, "node":Lcom/policydm/eng/core/XDMNode;
    invoke-static {v0, p1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListBindObjectToNode(Lcom/policydm/eng/core/XDMNode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    if-eqz v0, :cond_0

    .line 125
    iput-object v1, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 126
    iget-object v2, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iput-object v2, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 127
    iget-object v2, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iput-object v0, v2, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 128
    iput-object v0, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 130
    iget v2, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    .line 132
    :cond_0
    return-void
.end method

.method public static xdmListBindObjectToNode(Lcom/policydm/eng/core/XDMNode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "node"    # Lcom/policydm/eng/core/XDMNode;
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 239
    const/4 v0, 0x0

    .line 241
    .local v0, "ret":Ljava/lang/Object;
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/policydm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    .line 244
    iput-object p1, p0, Lcom/policydm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    .line 247
    .end local v0    # "ret":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public static xdmListClearLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V
    .locals 3
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 63
    iget-object v1, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 64
    .local v1, "top":Lcom/policydm/eng/core/XDMNode;
    iget-object v0, v1, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 66
    .local v0, "cur":Lcom/policydm/eng/core/XDMNode;
    :goto_0
    if-ne v0, v1, :cond_0

    .line 72
    iput-object v1, v1, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 73
    iput-object v1, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 74
    iput-object v1, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 76
    const/4 v2, 0x0

    iput v2, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    .line 77
    return-void

    .line 68
    :cond_0
    iget-object v0, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 69
    iget-object v2, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    invoke-static {v2}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/policydm/eng/core/XDMNode;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x0

    .line 18
    .local v0, "list":Lcom/policydm/eng/core/XDMLinkedList;
    const/4 v1, 0x0

    .line 20
    .local v1, "node":Lcom/policydm/eng/core/XDMNode;
    new-instance v0, Lcom/policydm/eng/core/XDMLinkedList;

    .end local v0    # "list":Lcom/policydm/eng/core/XDMLinkedList;
    invoke-direct {v0}, Lcom/policydm/eng/core/XDMLinkedList;-><init>()V

    .line 22
    .restart local v0    # "list":Lcom/policydm/eng/core/XDMLinkedList;
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateNodeFromMemory()Lcom/policydm/eng/core/XDMNode;

    move-result-object v1

    .line 23
    if-nez v1, :cond_0

    .line 25
    const-string v2, "Create node memory alloc failed"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 26
    const/4 v0, 0x0

    .line 27
    const/4 v0, 0x0

    .line 36
    .end local v0    # "list":Lcom/policydm/eng/core/XDMLinkedList;
    :goto_0
    return-object v0

    .line 30
    .restart local v0    # "list":Lcom/policydm/eng/core/XDMLinkedList;
    :cond_0
    iput-object v1, v1, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 31
    iput-object v1, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 33
    iput-object v1, v0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 34
    const/4 v2, 0x0

    iput v2, v0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    goto :goto_0
.end method

.method public static xdmListCreateNodeFromMemory()Lcom/policydm/eng/core/XDMNode;
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 86
    .local v0, "node":Lcom/policydm/eng/core/XDMNode;
    new-instance v0, Lcom/policydm/eng/core/XDMNode;

    .end local v0    # "node":Lcom/policydm/eng/core/XDMNode;
    invoke-direct {v0}, Lcom/policydm/eng/core/XDMNode;-><init>()V

    .line 88
    .restart local v0    # "node":Lcom/policydm/eng/core/XDMNode;
    return-object v0
.end method

.method public static xdmListFreeLinkedList(Lcom/policydm/eng/core/XDMLinkedList;)V
    .locals 2
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 45
    iget-object v1, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 46
    .local v1, "top":Lcom/policydm/eng/core/XDMNode;
    iget-object v0, v1, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 48
    .local v0, "cur":Lcom/policydm/eng/core/XDMNode;
    :goto_0
    if-ne v0, v1, :cond_0

    .line 53
    invoke-static {v1}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/policydm/eng/core/XDMNode;)Ljava/lang/Object;

    .line 54
    const/4 p0, 0x0

    .line 55
    return-void

    .line 50
    :cond_0
    iget-object v0, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    goto :goto_0
.end method

.method public static xdmListFreeNodeFromMemory(Lcom/policydm/eng/core/XDMNode;)Ljava/lang/Object;
    .locals 1
    .param p0, "node"    # Lcom/policydm/eng/core/XDMNode;

    .prologue
    .line 257
    const/4 v0, 0x0

    .line 258
    .local v0, "ret":Ljava/lang/Object;
    if-eqz p0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/policydm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    .line 261
    const/4 p0, 0x0

    .line 263
    .end local v0    # "ret":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public static xdmListGetNextObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;
    .locals 2
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 292
    iget-object v0, p0, Lcom/policydm/eng/core/XDMLinkedList;->cur:Lcom/policydm/eng/core/XDMNode;

    .line 294
    .local v0, "cur":Lcom/policydm/eng/core/XDMNode;
    iget-object v1, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    if-ne v0, v1, :cond_0

    .line 296
    const/4 v1, 0x0

    .line 303
    :goto_0
    return-object v1

    .line 300
    :cond_0
    iget-object v0, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 302
    iput-object v0, p0, Lcom/policydm/eng/core/XDMLinkedList;->cur:Lcom/policydm/eng/core/XDMNode;

    .line 303
    iget-object v1, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iget-object v1, v1, Lcom/policydm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    goto :goto_0
.end method

.method public static xdmListGetObj(Lcom/policydm/eng/core/XDMLinkedList;I)Ljava/lang/Object;
    .locals 4
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;
    .param p1, "idx"    # I

    .prologue
    .line 142
    iget-object v2, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 143
    .local v2, "top":Lcom/policydm/eng/core/XDMNode;
    move-object v0, v2

    .line 145
    .local v0, "cur":Lcom/policydm/eng/core/XDMNode;
    iget v3, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    if-ge p1, v3, :cond_0

    if-gez p1, :cond_2

    .line 147
    :cond_0
    const/4 v3, 0x0

    .line 155
    :goto_0
    return-object v3

    .line 152
    :cond_1
    iget-object v0, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    move v1, p1

    .line 150
    .end local p1    # "idx":I
    .local v1, "idx":I
    :goto_1
    add-int/lit8 p1, v1, -0x1

    .end local v1    # "idx":I
    .restart local p1    # "idx":I
    if-gez v1, :cond_1

    .line 155
    iget-object v3, v0, Lcom/policydm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    move v1, p1

    .end local p1    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_1
.end method

.method public static xdmListRemoveObjAt(Lcom/policydm/eng/core/XDMLinkedList;I)Ljava/lang/Object;
    .locals 6
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;
    .param p1, "idx"    # I

    .prologue
    .line 198
    iget-object v3, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 199
    .local v3, "top":Lcom/policydm/eng/core/XDMNode;
    move-object v0, v3

    .line 202
    .local v0, "cur":Lcom/policydm/eng/core/XDMNode;
    iget v4, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    if-ge p1, v4, :cond_0

    if-gez p1, :cond_2

    .line 204
    :cond_0
    const/4 v2, 0x0

    .line 218
    :goto_0
    return-object v2

    .line 209
    :cond_1
    iget-object v0, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    move v1, p1

    .line 207
    .end local p1    # "idx":I
    .local v1, "idx":I
    :goto_1
    add-int/lit8 p1, v1, -0x1

    .end local v1    # "idx":I
    .restart local p1    # "idx":I
    if-gez v1, :cond_1

    .line 212
    iget-object v4, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iget-object v5, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    iput-object v5, v4, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 213
    iget-object v4, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    iget-object v5, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iput-object v5, v4, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 215
    invoke-static {v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/policydm/eng/core/XDMNode;)Ljava/lang/Object;

    move-result-object v2

    .line 216
    .local v2, "obj":Ljava/lang/Object;
    iget v4, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    goto :goto_0

    .end local v2    # "obj":Ljava/lang/Object;
    :cond_2
    move v1, p1

    .end local p1    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_1
.end method

.method public static xdmListRemoveObjAtFirst(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;
    .locals 1
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 228
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListRemoveObjAt(Lcom/policydm/eng/core/XDMLinkedList;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static xdmListRemovePreviousObj(Lcom/policydm/eng/core/XDMLinkedList;)Ljava/lang/Object;
    .locals 3
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;

    .prologue
    .line 313
    iget-object v0, p0, Lcom/policydm/eng/core/XDMLinkedList;->cur:Lcom/policydm/eng/core/XDMNode;

    .line 314
    .local v0, "cur":Lcom/policydm/eng/core/XDMNode;
    iget-object v1, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iget-object v2, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    if-eq v1, v2, :cond_0

    .line 316
    iget-object v0, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 318
    iget-object v1, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    iput-object v2, v1, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 319
    iget-object v1, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    iget-object v2, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iput-object v2, v1, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 321
    iget v1, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    .line 322
    invoke-static {v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/policydm/eng/core/XDMNode;)Ljava/lang/Object;

    move-result-object v1

    .line 325
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static xdmListSetCurrentObj(Lcom/policydm/eng/core/XDMLinkedList;I)V
    .locals 3
    .param p0, "list"    # Lcom/policydm/eng/core/XDMLinkedList;
    .param p1, "idx"    # I

    .prologue
    .line 273
    iget-object v0, p0, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 274
    .local v0, "cur":Lcom/policydm/eng/core/XDMNode;
    if-ltz p1, :cond_0

    iget v2, p0, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    if-ge p1, v2, :cond_0

    move v1, p1

    .line 276
    .end local p1    # "idx":I
    .local v1, "idx":I
    :goto_0
    add-int/lit8 p1, v1, -0x1

    .end local v1    # "idx":I
    .restart local p1    # "idx":I
    if-gez v1, :cond_1

    .line 282
    :cond_0
    iput-object v0, p0, Lcom/policydm/eng/core/XDMLinkedList;->cur:Lcom/policydm/eng/core/XDMNode;

    .line 283
    return-void

    .line 278
    :cond_1
    iget-object v0, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    move v1, p1

    .end local p1    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_0
.end method


# virtual methods
.method public xdmListRemoveObj(Lcom/policydm/eng/core/XDMLinkedList;Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 5
    .param p1, "list"    # Lcom/policydm/eng/core/XDMLinkedList;
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "size"    # I

    .prologue
    .line 167
    iget-object v2, p1, Lcom/policydm/eng/core/XDMLinkedList;->top:Lcom/policydm/eng/core/XDMNode;

    .line 168
    .local v2, "top":Lcom/policydm/eng/core/XDMNode;
    iget-object v0, v2, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 169
    .local v0, "cur":Lcom/policydm/eng/core/XDMNode;
    const/4 v1, 0x0

    .line 171
    .local v1, "ret":Ljava/lang/Object;
    :goto_0
    if-ne v0, v2, :cond_0

    .line 187
    .end local v1    # "ret":Ljava/lang/Object;
    :goto_1
    return-object v1

    .line 173
    .restart local v1    # "ret":Ljava/lang/Object;
    :cond_0
    iget-object v3, v0, Lcom/policydm/eng/core/XDMNode;->obj:Ljava/lang/Object;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 175
    iget-object v3, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iget-object v4, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    iput-object v4, v3, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    .line 176
    iget-object v3, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    iget-object v4, v0, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    iput-object v4, v3, Lcom/policydm/eng/core/XDMNode;->previous:Lcom/policydm/eng/core/XDMNode;

    .line 177
    invoke-static {v0}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListFreeNodeFromMemory(Lcom/policydm/eng/core/XDMNode;)Ljava/lang/Object;

    move-result-object v1

    .line 178
    iget v3, p1, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p1, Lcom/policydm/eng/core/XDMLinkedList;->count:I

    goto :goto_1

    .line 183
    :cond_1
    iget-object v0, v0, Lcom/policydm/eng/core/XDMNode;->next:Lcom/policydm/eng/core/XDMNode;

    goto :goto_0
.end method
