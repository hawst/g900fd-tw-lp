.class public Lcom/policydm/eng/core/XDMWorkspace;
.super Ljava/lang/Object;
.source "XDMWorkspace.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XUICInterface;


# instance fields
.field public IsSequenceProcessing:Z

.field public appId:I

.field public atomic:Lcom/policydm/eng/parser/XDMParserAtomic;

.field public atomicFlag:Z

.field public atomicList:Lcom/policydm/eng/core/XDMLinkedList;

.field public atomicStep:Lcom/policydm/interfaces/XDMInterface$XDMAtomicStep;

.field public authCount:I

.field public authState:I

.field public buf:Ljava/io/ByteArrayOutputStream;

.field public bufsize:I

.field public cmdID:I

.field public credType:I

.field public dataBuffered:Z

.field public dataTotalSize:I

.field public dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

.field public e:Lcom/policydm/eng/core/XDMEncoder;

.field public endOfMsg:Z

.field public inAtomicCmd:Z

.field public inSequenceCmd:Z

.field public isFinal:Z

.field public list:Lcom/policydm/eng/core/XDMLinkedList;

.field public m_szClientPW:Ljava/lang/String;

.field public m_szHostname:Ljava/lang/String;

.field public m_szMsgRef:Ljava/lang/String;

.field public m_szProtocol:Ljava/lang/String;

.field public m_szServerID:Ljava/lang/String;

.field public m_szServerPW:Ljava/lang/String;

.field public m_szSessionID:Ljava/lang/String;

.field public m_szSourceURI:Ljava/lang/String;

.field public m_szStatusReturnCode:Ljava/lang/String;

.field public m_szTargetURI:Ljava/lang/String;

.field public m_szUserName:Ljava/lang/String;

.field public maxMsgSize:I

.field public maxObjSize:I

.field public msgID:I

.field public nTNDSFlag:Z

.field public nextMsg:Z

.field public nextNonce:[B

.field public numAction:I

.field public om:Lcom/policydm/eng/core/XDMOmTree;

.field public p:Lcom/policydm/eng/parser/XDMParser;

.field public port:I

.field public prevBufPos:I

.field public procState:Lcom/policydm/interfaces/XDMInterface$XDMProcessingState;

.field public procStep:I

.field public recvHmacData:Lcom/policydm/eng/core/XDMHmacData;

.field public results:Lcom/policydm/eng/parser/XDMParserResults;

.field public resultsList:Lcom/policydm/eng/core/XDMLinkedList;

.field public sendChal:Z

.field public sendPos:I

.field public sendRemain:Z

.field public sequence:Lcom/policydm/eng/parser/XDMParserSequence;

.field public sequenceList:Lcom/policydm/eng/core/XDMLinkedList;

.field public serverAuthState:I

.field public serverCredType:I

.field public serverMaxMsgSize:I

.field public serverMaxObjSize:I

.field public serverNextNonce:[B

.field public sessionAbort:I

.field public sourceRefList:Lcom/policydm/eng/core/XDMLinkedList;

.field public state:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

.field public statusList:Lcom/policydm/eng/core/XDMLinkedList;

.field public syncHeader:Lcom/policydm/eng/parser/XDMParserSyncheader;

.field public targetRefList:Lcom/policydm/eng/core/XDMLinkedList;

.field public tempResults:Lcom/policydm/eng/parser/XDMParserResults;

.field public tmpItem:Lcom/policydm/eng/parser/XDMParserItem;

.field public uicAlert:Lcom/policydm/eng/parser/XDMParserAlert;

.field public uicData:Lcom/policydm/eng/core/XDMList;

.field public uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

.field public uicOption:Lcom/policydm/eng/core/XDMUicOption;

.field public userData:Ljava/lang/Object;

.field public ws:Lcom/policydm/eng/core/XDMWorkspace;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, -0x8

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szUserName:Ljava/lang/String;

    .line 99
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szStatusReturnCode:Ljava/lang/String;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerID:Ljava/lang/String;

    .line 101
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szServerPW:Ljava/lang/String;

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szClientPW:Ljava/lang/String;

    .line 103
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->nextNonce:[B

    .line 104
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverNextNonce:[B

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szProtocol:Ljava/lang/String;

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szHostname:Ljava/lang/String;

    .line 107
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szSourceURI:Ljava/lang/String;

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szTargetURI:Ljava/lang/String;

    .line 109
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szSessionID:Ljava/lang/String;

    .line 110
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->m_szMsgRef:Ljava/lang/String;

    .line 111
    iput-object v2, p0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 113
    iput v3, p0, Lcom/policydm/eng/core/XDMWorkspace;->authState:I

    .line 114
    iput v3, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverAuthState:I

    .line 115
    iput v4, p0, Lcom/policydm/eng/core/XDMWorkspace;->credType:I

    .line 116
    iput v4, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverCredType:I

    .line 117
    iput-boolean v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->sendChal:Z

    .line 118
    iput-boolean v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->inAtomicCmd:Z

    .line 119
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->atomicList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 120
    iput-boolean v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->inSequenceCmd:Z

    .line 121
    iput-object v2, p0, Lcom/policydm/eng/core/XDMWorkspace;->sequenceList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 123
    new-instance v0, Lcom/policydm/eng/core/XDMOmTree;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMOmTree;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->om:Lcom/policydm/eng/core/XDMOmTree;

    .line 124
    new-instance v0, Lcom/policydm/eng/core/XDMEncoder;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMEncoder;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->e:Lcom/policydm/eng/core/XDMEncoder;

    .line 125
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->targetRefList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 126
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->sourceRefList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 127
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    .line 128
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 129
    invoke-static {}, Lcom/policydm/eng/core/XDMLinkedList;->xdmListCreateLinkedList()Lcom/policydm/eng/core/XDMLinkedList;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 130
    iput-object v2, p0, Lcom/policydm/eng/core/XDMWorkspace;->results:Lcom/policydm/eng/parser/XDMParserResults;

    .line 132
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->buf:Ljava/io/ByteArrayOutputStream;

    .line 134
    const/16 v0, 0x1c00

    iput v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->bufsize:I

    .line 135
    const/16 v0, 0x1400

    iput v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->maxMsgSize:I

    .line 136
    const/high16 v0, 0x100000

    iput v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->maxObjSize:I

    .line 138
    const/16 v0, 0x1400

    iput v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxMsgSize:I

    .line 139
    const/high16 v0, 0x100000

    iput v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->serverMaxObjSize:I

    .line 141
    iput-boolean v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->endOfMsg:Z

    .line 142
    iput-object v2, p0, Lcom/policydm/eng/core/XDMWorkspace;->syncHeader:Lcom/policydm/eng/parser/XDMParserSyncheader;

    .line 143
    iput v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->sessionAbort:I

    .line 144
    sget-object v0, Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;->XDM_STATE_INIT:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->dmState:Lcom/policydm/interfaces/XDMInterface$XDMSyncMLState;

    .line 145
    iput v5, p0, Lcom/policydm/eng/core/XDMWorkspace;->cmdID:I

    .line 146
    iput v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->appId:I

    .line 147
    iput v5, p0, Lcom/policydm/eng/core/XDMWorkspace;->msgID:I

    .line 148
    iput v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->authCount:I

    .line 150
    iput-boolean v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->dataBuffered:Z

    .line 151
    iput-boolean v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->IsSequenceProcessing:Z

    .line 152
    new-instance v0, Lcom/policydm/eng/core/XDMHmacData;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMHmacData;-><init>()V

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->recvHmacData:Lcom/policydm/eng/core/XDMHmacData;

    .line 154
    sget-object v0, Lcom/policydm/interfaces/XUICInterface$XUICFlag;->XUIC_NONE:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    iput-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->uicFlag:Lcom/policydm/interfaces/XUICInterface$XUICFlag;

    .line 155
    iput-object v2, p0, Lcom/policydm/eng/core/XDMWorkspace;->uicData:Lcom/policydm/eng/core/XDMList;

    .line 156
    return-void
.end method


# virtual methods
.method public xdmFreeWorkSpace()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 163
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->targetRefList:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v0, :cond_0

    .line 165
    iput-object v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->targetRefList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->sourceRefList:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v0, :cond_1

    .line 169
    iput-object v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->sourceRefList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v0, :cond_2

    .line 173
    iput-object v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->list:Lcom/policydm/eng/core/XDMLinkedList;

    .line 175
    :cond_2
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v0, :cond_3

    .line 177
    iput-object v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->statusList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v0, :cond_4

    .line 181
    iput-object v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->resultsList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 184
    :cond_4
    iget-object v0, p0, Lcom/policydm/eng/core/XDMWorkspace;->atomicList:Lcom/policydm/eng/core/XDMLinkedList;

    if-eqz v0, :cond_5

    .line 186
    iput-object v1, p0, Lcom/policydm/eng/core/XDMWorkspace;->atomicList:Lcom/policydm/eng/core/XDMLinkedList;

    .line 189
    :cond_5
    return-void
.end method
