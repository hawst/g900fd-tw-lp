.class public Lcom/policydm/tp/XTPAdapter;
.super Ljava/lang/Object;
.source "XTPAdapter.java"

# interfaces
.implements Lcom/policydm/interfaces/XDBInterface;
.implements Lcom/policydm/interfaces/XDLInterface;
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XEventInterface;
.implements Lcom/policydm/interfaces/XTPInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;,
        Lcom/policydm/tp/XTPAdapter$XTPTrustManager;
    }
.end annotation


# static fields
.field private static final RECEIVE_BUFFER_SIZE:I = 0x1400

.field public static g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

.field private static m_nHttpDebugCount:I

.field private static m_szCookie:Ljava/lang/String;


# instance fields
.field private m_HMacData:Lcom/policydm/eng/core/XDMHmacData;

.field private m_Input:Ljava/io/InputStream;

.field private m_Output:Ljava/io/OutputStream;

.field private m_SSLContext:Ljavax/net/ssl/SSLContext;

.field private m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

.field private m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

.field private m_SSLSocket:Ljavax/net/ssl/SSLSocket;

.field private m_Socket:Ljava/net/Socket;

.field private m_WakeLock:Landroid/os/PowerManager$WakeLock;

.field private m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

.field private m_bIsProxyServer:Z

.field private m_conProxy:Ljava/net/Proxy;

.field private m_dlhandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

.field private m_nHttpBodyLength:I

.field private m_szHttpHeaderData:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    sput-object v0, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    .line 63
    sput-object v0, Lcom/policydm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    .line 81
    const/4 v0, 0x0

    sput v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpDebugCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_conProxy:Ljava/net/Proxy;

    .line 65
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_dlhandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    .line 67
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_HMacData:Lcom/policydm/eng/core/XDMHmacData;

    .line 69
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    .line 70
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    .line 71
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    .line 72
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 73
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    .line 74
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    .line 75
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    .line 76
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/policydm/tp/XTPAdapter;->m_bIsProxyServer:Z

    .line 83
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 84
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 90
    sget-object v0, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    if-nez v0, :cond_0

    .line 91
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/policydm/tp/XTPHttpObj;

    sput-object v0, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    .line 92
    :cond_0
    return-void
.end method

.method private xtpAdpGetHttpInfo(I)I
    .locals 12
    .param p1, "appId"    # I

    .prologue
    const/16 v6, -0xb

    .line 1291
    const/4 v3, 0x0

    .line 1293
    .local v3, "ret":I
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    if-nez v7, :cond_1

    move v3, v6

    .line 1364
    .end local v3    # "ret":I
    :cond_0
    :goto_0
    return v3

    .line 1296
    .restart local v3    # "ret":I
    :cond_1
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v7, v7, p1

    iput p1, v7, Lcom/policydm/tp/XTPHttpObj;->appId:I

    .line 1298
    const-string v5, ""

    .line 1299
    .local v5, "szServerURL":Ljava/lang/String;
    const-string v4, ""

    .line 1301
    .local v4, "szProxyAddress":Ljava/lang/String;
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v7, v7, p1

    iget v7, v7, Lcom/policydm/tp/XTPHttpObj;->appId:I

    invoke-static {v7}, Lcom/policydm/db/XDB;->xdbGetServerUrl(I)Ljava/lang/String;

    move-result-object v5

    .line 1302
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1304
    const-string v7, "ServerURL is null!!"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1305
    if-nez p1, :cond_2

    move v3, v6

    .line 1307
    goto :goto_0

    .line 1311
    :cond_2
    const/16 v3, -0x11

    goto :goto_0

    .line 1317
    :cond_3
    invoke-static {}, Lcom/policydm/db/XDBProfileAdp;->xdbGetConRef()Lcom/policydm/db/XDBInfoConRef;

    move-result-object v0

    .line 1319
    .local v0, "Conref":Lcom/policydm/db/XDBInfoConRef;
    if-nez v0, :cond_4

    .line 1321
    const-string v7, "Get Conref from DB is failed"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v6

    .line 1322
    goto :goto_0

    .line 1327
    :cond_4
    :try_start_0
    iget-object v7, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v7, v7, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v7, v7, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    const-string v8, "0.0.0.0"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1329
    :cond_5
    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/policydm/db/XDBInfoConRef;->bProxyUse:Z

    .line 1330
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetIsProxy(Z)V

    .line 1346
    :goto_1
    invoke-static {v5}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v2

    .line 1347
    .local v2, "parser":Lcom/policydm/db/XDBUrlInfo;
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v7, v7, p1

    iget-object v8, v2, Lcom/policydm/db/XDBUrlInfo;->pURL:Ljava/lang/String;

    iput-object v8, v7, Lcom/policydm/tp/XTPHttpObj;->m_szServerURL:Ljava/lang/String;

    .line 1348
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v7, v7, p1

    iget-object v8, v2, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    iput-object v8, v7, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    .line 1349
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v7, v7, p1

    iget v8, v2, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    iput v8, v7, Lcom/policydm/tp/XTPHttpObj;->nServerPort:I

    .line 1350
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v7, v7, p1

    invoke-static {v5}, Lcom/policydm/tp/XTPHttpUtil;->xtpHttpGetConnectType(Ljava/lang/String;)I

    move-result v8

    iput v8, v7, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    .line 1352
    invoke-virtual {p0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1354
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v7, v7, p1

    iget-object v8, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v8, v8, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    iput-object v8, v7, Lcom/policydm/tp/XTPHttpObj;->m_szProxyAddr:Ljava/lang/String;

    .line 1355
    sget-object v7, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v7, v7, p1

    iget-object v8, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget v8, v8, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    iput v8, v7, Lcom/policydm/tp/XTPHttpObj;->proxyPort:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1358
    .end local v2    # "parser":Lcom/policydm/db/XDBUrlInfo;
    :catch_0
    move-exception v1

    .line 1360
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v3, v6

    .line 1361
    goto/16 :goto_0

    .line 1334
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    :try_start_1
    const-string v7, "Proxy Mode"

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1335
    const/4 v7, 0x1

    iput-boolean v7, v0, Lcom/policydm/db/XDBInfoConRef;->bProxyUse:Z

    .line 1336
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetIsProxy(Z)V

    .line 1338
    const-string v4, "http://"

    .line 1339
    iget-object v7, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v7, v7, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1340
    const-string v7, ":"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1341
    iget-object v7, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget v7, v7, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1342
    new-instance v7, Ljava/net/Proxy;

    sget-object v8, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    new-instance v9, Ljava/net/InetSocketAddress;

    iget-object v10, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v10, v10, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    iget-object v11, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget v11, v11, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    invoke-direct {v9, v10, v11}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v7, v8, v9}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    iput-object v7, p0, Lcom/policydm/tp/XTPAdapter;->m_conProxy:Ljava/net/Proxy;

    .line 1343
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "PX addr :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget-object v8, v8, Lcom/policydm/db/XDBConRefPX;->Addr:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", and Port : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Lcom/policydm/db/XDBInfoConRef;->PX:Lcom/policydm/db/XDBConRefPX;

    iget v8, v8, Lcom/policydm/db/XDBConRefPX;->nPortNbr:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method private xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "szData1"    # Ljava/lang/String;
    .param p2, "szData2"    # Ljava/lang/String;

    .prologue
    .line 369
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 372
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 373
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 374
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 376
    :cond_0
    return-void
.end method

.method private xtpAdpHttpChunkedHeadRead(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v3, 0x0

    .line 1735
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v4, ""

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1736
    .local v1, "data":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 1740
    .local v0, "c":I
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1741
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 1767
    :goto_0
    return-object v3

    .line 1744
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    .line 1746
    :goto_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ltz v0, :cond_1

    .line 1748
    const/16 v4, 0xd

    if-ne v0, v4, :cond_3

    .line 1750
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1751
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/16 v4, 0xa

    if-ne v0, v4, :cond_2

    .line 1767
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1754
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1761
    :catch_0
    move-exception v2

    .line 1763
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 1757
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    int-to-char v4, v0

    :try_start_2
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public static xtpAdpHttpCookieClear()V
    .locals 1

    .prologue
    .line 1657
    const-string v0, ""

    sput-object v0, Lcom/policydm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    .line 1658
    const-string v0, ""

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1659
    return-void
.end method

.method private xtpAdpHttpHeadRead(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 8
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v3, 0x0

    const/16 v7, 0xd

    const/16 v6, 0xa

    .line 1468
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v4, ""

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1469
    .local v1, "data":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 1473
    .local v0, "c":I
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1474
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 1500
    :goto_0
    return-object v3

    .line 1477
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    .line 1479
    :goto_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    if-ltz v0, :cond_1

    .line 1481
    if-eqz v0, :cond_1

    if-eq v0, v6, :cond_1

    if-ne v0, v7, :cond_3

    .line 1487
    :cond_1
    if-ne v0, v7, :cond_2

    .line 1489
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/io/InputStream;->mark(I)V

    .line 1490
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v4

    if-eq v4, v6, :cond_2

    .line 1491
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1500
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 1484
    :cond_3
    int-to-char v4, v0

    :try_start_1
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1494
    :catch_0
    move-exception v2

    .line 1496
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private xtpAdpHttpHeaderParser(ILjava/io/InputStream;)I
    .locals 15
    .param p1, "appId"    # I
    .param p2, "in"    # Ljava/io/InputStream;

    .prologue
    .line 1505
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v12, ""

    invoke-direct {v2, v12}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1506
    .local v2, "header":Ljava/lang/StringBuffer;
    const-string v7, ""

    .line 1507
    .local v7, "szData":Ljava/lang/String;
    const/4 v5, -0x1

    .line 1508
    .local v5, "pos":I
    const-string v9, ""

    .line 1509
    .local v9, "szHmac":Ljava/lang/String;
    const-string v8, ""

    .line 1510
    .local v8, "szEncoding":Ljava/lang/String;
    const-string v6, ""

    .line 1511
    .local v6, "szContentrange":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1512
    .local v4, "nContentLen":I
    const/4 v10, 0x0

    .local v10, "szHost":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1514
    .local v3, "nConnection":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpHeadRead(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v7

    .line 1515
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1518
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpHeadRead(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v7

    .line 1519
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1521
    const-string v12, "data is null "

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1522
    const/16 v12, -0xe

    .line 1640
    :goto_0
    return v12

    .line 1525
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\r\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1526
    const/16 v12, 0x20

    invoke-virtual {v7, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 1527
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "http"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    if-ltz v5, :cond_1

    const-string v12, " "

    add-int/lit8 v13, v5, 0x1

    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v12

    if-ltz v12, :cond_1

    .line 1529
    add-int/lit8 v12, v5, 0x1

    const-string v13, " "

    add-int/lit8 v14, v5, 0x1

    invoke-virtual {v7, v13, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v13

    invoke-virtual {v7, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    .line 1532
    .local v11, "szRet":Ljava/lang/String;
    :try_start_0
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    iput v13, v12, Lcom/policydm/tp/XTPHttpObj;->nHttpReturnStatusValue:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1541
    .end local v11    # "szRet":Ljava/lang/String;
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "pHttpObj[appId].nHttpReturnStatusValue="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v13, v13, p1

    iget v13, v13, Lcom/policydm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1544
    :cond_2
    :goto_1
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpHeadRead(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    .line 1547
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v12

    if-nez v12, :cond_6

    .line 1590
    :cond_3
    const-string v12, "\r\n"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1592
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    iput v13, v12, Lcom/policydm/tp/XTPHttpObj;->nHeaderLength:I

    .line 1593
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v12

    const/16 v13, 0x400

    if-ge v12, v13, :cond_4

    .line 1594
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "\r\n [_____Receive Header_____]\r\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1596
    :cond_4
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    iput-object v10, v12, Lcom/policydm/tp/XTPHttpObj;->m_szHttpHost:Ljava/lang/String;

    .line 1597
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    iput v4, v12, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    .line 1598
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    iput-object v3, v12, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    .line 1600
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "chunked = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1601
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "pHttpObj[appId].nHeaderLength ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v13, v13, p1

    iget v13, v13, Lcom/policydm/tp/XTPHttpObj;->nHeaderLength:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1602
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "pHttpObj[appId].nContentLength ="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v13, v13, p1

    iget v13, v13, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1604
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_e

    .line 1606
    const-string v12, "chunked"

    invoke-virtual {v8, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_d

    .line 1607
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    const/4 v13, 0x1

    iput v13, v12, Lcom/policydm/tp/XTPHttpObj;->nTransferCoding:I

    .line 1614
    :goto_2
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_f

    .line 1616
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    iget v12, v12, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    invoke-virtual {p0, v9, v12}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpPsrParserHMAC(Ljava/lang/String;I)Lcom/policydm/eng/core/XDMHmacData;

    move-result-object v12

    iput-object v12, p0, Lcom/policydm/tp/XTPAdapter;->m_HMacData:Lcom/policydm/eng/core/XDMHmacData;

    .line 1625
    :goto_3
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_10

    .line 1627
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    iput-object v6, v12, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    .line 1628
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "pContentRange"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v13, v13, p1

    iget-object v13, v13, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1635
    :goto_4
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_5

    .line 1637
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    sget-object v13, Lcom/policydm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    iput-object v13, v12, Lcom/policydm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    .line 1640
    :cond_5
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 1534
    .restart local v11    # "szRet":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1536
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1537
    const/16 v12, -0xe

    goto/16 :goto_0

    .line 1551
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v11    # "szRet":Ljava/lang/String;
    :cond_6
    const-string v12, "\r\n"

    invoke-virtual {v7, v12}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1552
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1556
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "host:"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1557
    if-ltz v5, :cond_7

    .line 1558
    add-int/lit8 v12, v5, 0x5

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 1561
    :cond_7
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "content-length:"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1562
    if-ltz v5, :cond_8

    .line 1563
    add-int/lit8 v12, v5, 0xf

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1565
    :cond_8
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "connection:"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1566
    if-ltz v5, :cond_9

    .line 1567
    add-int/lit8 v12, v5, 0xb

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 1569
    :cond_9
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "x-syncml-hmac:"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1570
    if-ltz v5, :cond_a

    .line 1571
    add-int/lit8 v12, v5, 0xe

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 1573
    :cond_a
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "transfer-encoding:"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1574
    if-ltz v5, :cond_b

    .line 1575
    add-int/lit8 v12, v5, 0x12

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 1577
    :cond_b
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "content-range:"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1578
    if-ltz v5, :cond_c

    .line 1579
    add-int/lit8 v12, v5, 0xe

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1581
    :cond_c
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "set-cookie:"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1582
    if-ltz v5, :cond_2

    .line 1584
    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "jsessionid"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1586
    add-int/lit8 v12, v5, 0xb

    invoke-virtual {v7, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/policydm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    goto/16 :goto_1

    .line 1609
    :cond_d
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    const/4 v13, 0x0

    iput v13, v12, Lcom/policydm/tp/XTPHttpObj;->nTransferCoding:I

    goto/16 :goto_2

    .line 1612
    :cond_e
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    const/4 v13, 0x0

    iput v13, v12, Lcom/policydm/tp/XTPHttpObj;->nTransferCoding:I

    goto/16 :goto_2

    .line 1621
    :cond_f
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/policydm/tp/XTPAdapter;->m_HMacData:Lcom/policydm/eng/core/XDMHmacData;

    .line 1622
    const-string v12, "szHMAC null"

    invoke-static {v12}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1632
    :cond_10
    sget-object v12, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v12, v12, p1

    const/4 v13, 0x0

    iput-object v13, v12, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    goto/16 :goto_4
.end method

.method private xtpAdpHttpInit(I)I
    .locals 7
    .param p1, "appId"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1235
    const/4 v0, 0x0

    .line 1237
    .local v0, "ret":I
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput p1, v1, Lcom/policydm/tp/XTPHttpObj;->appId:I

    .line 1238
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v2, v1, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    .line 1239
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v4, v1, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    .line 1240
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v4, v1, Lcom/policydm/tp/XTPHttpObj;->nHeaderLength:I

    .line 1241
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v2, v1, Lcom/policydm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    .line 1243
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput-object v3, v1, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    .line 1245
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v2, v1, Lcom/policydm/tp/XTPHttpObj;->nTunnelMode:I

    .line 1246
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v2, v1, Lcom/policydm/tp/XTPHttpObj;->nTunnelConnected:I

    .line 1248
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const-string v2, "POST"

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    .line 1250
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const-string v2, "HTTP/1.1"

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    .line 1251
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput-object v3, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpHost:Ljava/lang/String;

    .line 1252
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput-object v3, v1, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    .line 1253
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput-object v3, v1, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    .line 1254
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v4, v1, Lcom/policydm/tp/XTPHttpObj;->nTransferCoding:I

    .line 1256
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    invoke-static {p1}, Lcom/policydm/db/XDB;->xdbGetConnectType(I)I

    move-result v2

    iput v2, v1, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    .line 1258
    if-nez p1, :cond_2

    .line 1260
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v6, v1, Lcom/policydm/tp/XTPHttpObj;->nHttpConnection:I

    .line 1261
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const-string v2, "Keep-Alive"

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    .line 1262
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const-string v2, "application/vnd.syncml.dm+wbxml"

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    .line 1263
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const-string v2, "application/vnd.syncml.dm+wbxml"

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    .line 1272
    :cond_0
    :goto_0
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    invoke-static {}, Lcom/policydm/adapter/XDMDevinfAdapter;->xdmDevAdpGetHttpUserAgent()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    .line 1274
    invoke-direct {p0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetHttpInfo(I)I

    move-result v0

    .line 1275
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iget v1, v1, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    if-ne v1, v5, :cond_3

    invoke-virtual {p0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1277
    const-string v1, "XTP_SSL_TUNNEL_MODE_ACTIVE"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1278
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const/4 v2, 0x3

    iput v2, v1, Lcom/policydm/tp/XTPHttpObj;->nTunnelMode:I

    .line 1286
    :cond_1
    :goto_1
    return v0

    .line 1265
    :cond_2
    if-ne p1, v5, :cond_0

    .line 1267
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v6, v1, Lcom/policydm/tp/XTPHttpObj;->nHttpConnection:I

    .line 1268
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const-string v2, "Keep-Alive"

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    .line 1269
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const-string v2, ""

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    .line 1270
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const-string v2, "application/zip"

    iput-object v2, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    goto :goto_0

    .line 1280
    :cond_3
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iget v1, v1, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    if-ne v1, v5, :cond_1

    invoke-virtual {p0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1282
    const-string v1, "XTP_SSL_TUNNEL_MODE_DEACTIVE"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1283
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    iput v6, v1, Lcom/policydm/tp/XTPHttpObj;->nTunnelMode:I

    goto :goto_1
.end method

.method private xtpAdpHttpPsrMakeHeader(II)Ljava/lang/String;
    .locals 7
    .param p1, "conLength"    # I
    .param p2, "appId"    # I

    .prologue
    const/4 v3, 0x0

    .line 380
    const-string v4, ""

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 381
    invoke-virtual {p0, p2}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetHttpOpenMode(I)Ljava/lang/String;

    move-result-object v1

    .line 382
    .local v1, "szOpenMode":Ljava/lang/String;
    const-string v0, ""

    .line 383
    .local v0, "szHeaderlog":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 463
    :goto_0
    return-object v3

    .line 386
    :cond_0
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 388
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 390
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 391
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 397
    :goto_1
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 398
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 400
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 402
    :cond_1
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 404
    const-string v4, "no-store, private"

    const-string v5, "Cache-Control"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 408
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    const-string v5, "Connection"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :cond_2
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 413
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    const-string v5, "User-Agent"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_3
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 418
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    const-string v5, "Accept"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :cond_4
    const-string v4, "en"

    const-string v5, "Accept-Language"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const-string v4, "utf-8"

    const-string v5, "Accept-Charset"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 426
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget v5, v5, Lcom/policydm/tp/XTPHttpObj;->nServerPort:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 427
    .local v2, "szServerHost":Ljava/lang/String;
    const-string v4, "Host"

    invoke-direct {p0, v2, v4}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    .end local v2    # "szServerHost":Ljava/lang/String;
    :cond_5
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 432
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    const-string v5, "Content-Type"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_6
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 437
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    const-string v5, "Cookie"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :cond_7
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 442
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bytes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Range"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_8
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, "Content-Length: "

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 446
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 447
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 449
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    if-eqz v4, :cond_9

    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iget-object v4, v4, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    array-length v4, v4

    if-eqz v4, :cond_9

    .line 451
    new-instance v4, Ljava/lang/String;

    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    const-string v5, "x-syncml-hmac"

    invoke-direct {p0, v4, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    sget-object v4, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v4, v4, p2

    iput-object v3, v4, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    .line 454
    :cond_9
    iget-object v3, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 456
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 461
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\r\n [_____Make Header_____]\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 463
    iget-object v3, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    goto/16 :goto_0

    .line 395
    :cond_a
    const-string v4, "PATH is NULL. Checking the pHttpObj->pRequestUri"

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static xtpAdpLookupHost(Ljava/lang/String;)I
    .locals 6
    .param p0, "szHostName"    # Ljava/lang/String;

    .prologue
    .line 1917
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 1925
    .local v3, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v3}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    .line 1926
    .local v1, "addrBytes":[B
    const/4 v4, 0x3

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    const/4 v5, 0x2

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    const/4 v5, 0x1

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    const/4 v5, 0x0

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    or-int v0, v4, v5

    .line 1927
    .end local v1    # "addrBytes":[B
    .end local v3    # "inetAddress":Ljava/net/InetAddress;
    :goto_0
    return v0

    .line 1919
    :catch_0
    move-exception v2

    .line 1921
    .local v2, "e":Ljava/net/UnknownHostException;
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static xtpAdpNetGetProfileData(Lcom/policydm/adapter/XDMTelephonyAdapter;)V
    .locals 0
    .param p0, "pNetInfo"    # Lcom/policydm/adapter/XDMTelephonyAdapter;

    .prologue
    .line 1673
    invoke-virtual {p0, p0}, Lcom/policydm/adapter/XDMTelephonyAdapter;->xdmAgentAppGetNetInfo(Lcom/policydm/adapter/XDMTelephonyAdapter;)V

    .line 1674
    return-void
.end method

.method public static xtpAdpNetSaveProfile(Lcom/policydm/adapter/XDMTelephonyAdapter;)I
    .locals 2
    .param p0, "pNetInfo"    # Lcom/policydm/adapter/XDMTelephonyAdapter;

    .prologue
    const/4 v0, 0x0

    .line 1678
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1679
    invoke-virtual {p0, v0, p0}, Lcom/policydm/adapter/XDMTelephonyAdapter;->xdmAgentAppProtoSetAccount(ILcom/policydm/adapter/XDMTelephonyAdapter;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1681
    const-string v0, "The Account Info Change Failed"

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1682
    const/4 v0, -0x1

    .line 1684
    :cond_0
    return v0
.end method

.method public static xtpAdpResetWBXMLLog()V
    .locals 1

    .prologue
    .line 1908
    const/4 v0, 0x0

    sput v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpDebugCount:I

    .line 1909
    return-void
.end method

.method private xtpAdpTunnelHandshake(Ljava/net/Socket;I)I
    .locals 13
    .param p1, "tunnel"    # Ljava/net/Socket;
    .param p2, "appId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/16 v10, -0xc

    .line 304
    invoke-virtual {p0, p2}, Lcom/policydm/tp/XTPAdapter;->xtpAdpMakeSSLTunneling(I)Ljava/lang/String;

    move-result-object v8

    .line 305
    .local v8, "szMsg":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 306
    const/4 v10, -0x1

    .line 364
    :goto_0
    return v10

    .line 308
    :cond_0
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v12

    iput-object v12, p0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    .line 311
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 312
    .local v0, "b":[B
    iget-object v12, p0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    invoke-virtual {v12, v0}, Ljava/io/OutputStream;->write([B)V

    .line 313
    iget-object v12, p0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    invoke-virtual {v12}, Ljava/io/OutputStream;->flush()V

    .line 315
    const/16 v12, 0xc8

    new-array v5, v12, [B

    .line 316
    .local v5, "reply":[B
    const/4 v6, 0x0

    .line 317
    .local v6, "replyLen":I
    const/4 v4, 0x0

    .line 318
    .local v4, "newlinesSeen":I
    const/4 v2, 0x0

    .line 320
    .local v2, "headerDone":Z
    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    iput-object v12, p0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    move v7, v6

    .line 322
    .end local v6    # "replyLen":I
    .local v7, "replyLen":I
    :goto_1
    const/4 v12, 0x2

    if-ge v4, v12, :cond_3

    .line 324
    iget-object v12, p0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    invoke-virtual {v12}, Ljava/io/InputStream;->read()I

    move-result v3

    .line 325
    .local v3, "i":I
    if-gez v3, :cond_1

    .line 327
    const-string v11, "Unable to tunnel"

    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 330
    :try_start_0
    iget-object v11, p0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    .line 331
    iget-object v11, p0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 333
    :catch_0
    move-exception v1

    .line 335
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0

    .line 339
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    const/16 v12, 0xa

    if-ne v3, v12, :cond_2

    .line 341
    const/4 v2, 0x1

    .line 342
    add-int/lit8 v4, v4, 0x1

    move v6, v7

    .end local v7    # "replyLen":I
    .restart local v6    # "replyLen":I
    :goto_2
    move v7, v6

    .line 352
    .end local v6    # "replyLen":I
    .restart local v7    # "replyLen":I
    goto :goto_1

    .line 344
    :cond_2
    const/16 v12, 0xd

    if-eq v3, v12, :cond_6

    .line 346
    const/4 v4, 0x0

    .line 347
    if-nez v2, :cond_6

    array-length v12, v5

    if-ge v7, v12, :cond_6

    .line 349
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "replyLen":I
    .restart local v6    # "replyLen":I
    int-to-byte v12, v3

    aput-byte v12, v5, v7

    goto :goto_2

    .line 355
    .end local v3    # "i":I
    .end local v6    # "replyLen":I
    .restart local v7    # "replyLen":I
    :cond_3
    new-instance v9, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v12

    invoke-direct {v9, v5, v11, v7, v12}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 358
    .local v9, "szReply":Ljava/lang/String;
    const-string v12, "HTTP/1.1 200"

    invoke-virtual {v9, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_4

    const-string v12, "HTTP/1.0 200"

    invoke-virtual {v9, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    :cond_4
    move v10, v11

    .line 360
    goto :goto_0

    .line 363
    :cond_5
    const-string v11, "Unable to tunnel through Proxy"

    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v9    # "szReply":Ljava/lang/String;
    .restart local v3    # "i":I
    :cond_6
    move v6, v7

    .end local v7    # "replyLen":I
    .restart local v6    # "replyLen":I
    goto :goto_2
.end method

.method private xtpAdpWBXMLLog(I[B)V
    .locals 3
    .param p1, "appId"    # I
    .param p2, "buffer"    # [B

    .prologue
    .line 1887
    invoke-static {}, Lcom/policydm/agent/XDMDebug;->xdmGetWbxmlFileStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    .line 1891
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "data/data/com.policydm/httpdata"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/policydm/tp/XTPAdapter;->m_nHttpDebugCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".wbxml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/policydm/agent/XDMDebug;->xdmWriteFile(Ljava/lang/String;[B)V

    .line 1892
    sget v1, Lcom/policydm/tp/XTPAdapter;->m_nHttpDebugCount:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/policydm/tp/XTPAdapter;->m_nHttpDebugCount:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1900
    :cond_0
    :goto_0
    invoke-static {}, Lcom/policydm/agent/XDMDebug;->xdmGetWbxmlDumpStatus()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 1902
    const/4 v1, 0x0

    invoke-static {p2, v1}, Lcom/policydm/agent/XDMDebug;->XDM_DUMP([BI)V

    .line 1904
    :cond_1
    return-void

    .line 1894
    :catch_0
    move-exception v0

    .line 1896
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public xtpAdpClose(I)V
    .locals 3
    .param p1, "appId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1184
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1185
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 1187
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1188
    iput-object v2, p0, Lcom/policydm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1190
    :cond_0
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-eqz v1, :cond_1

    .line 1192
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    .line 1193
    iput-object v2, p0, Lcom/policydm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 1196
    :cond_1
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    if-nez v1, :cond_3

    .line 1231
    :cond_2
    :goto_0
    return-void

    .line 1201
    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    if-eqz v1, :cond_4

    .line 1202
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1204
    :cond_4
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    if-eqz v1, :cond_5

    .line 1205
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 1207
    :cond_5
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    if-eqz v1, :cond_6

    .line 1208
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    .line 1210
    :cond_6
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    if-eqz v1, :cond_8

    .line 1212
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    if-eqz v1, :cond_7

    .line 1214
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    iget-object v2, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    invoke-virtual {v1, v2}, Ljavax/net/ssl/SSLSocket;->removeHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 1215
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    .line 1217
    :cond_7
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->close()V

    .line 1220
    :cond_8
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_9

    .line 1221
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 1223
    :cond_9
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    if-eqz v1, :cond_2

    .line 1224
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1227
    :catch_0
    move-exception v0

    .line 1229
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xtpAdpCloseNetWork(I)V
    .locals 2
    .param p1, "appId"    # I

    .prologue
    .line 1663
    sget-object v0, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 1664
    return-void
.end method

.method public xtpAdpGetCurHMACData()Lcom/policydm/eng/core/XDMHmacData;
    .locals 1

    .prologue
    .line 1718
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_HMacData:Lcom/policydm/eng/core/XDMHmacData;

    return-object v0
.end method

.method public xtpAdpGetHttpOpenMode(I)Ljava/lang/String;
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 1668
    sget-object v0, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    return-object v0
.end method

.method public xtpAdpGetIsProxy()Z
    .locals 2

    .prologue
    .line 1645
    invoke-static {}, Lcom/policydm/XDMApplication;->xdmProtoIsWIFIConnected()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1646
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/policydm/tp/XTPAdapter;->m_bIsProxyServer:Z

    .line 1647
    :cond_0
    iget-boolean v0, p0, Lcom/policydm/tp/XTPAdapter;->m_bIsProxyServer:Z

    return v0
.end method

.method public xtpAdpHttpMakeSSLTunnelHeader(I)Ljava/lang/String;
    .locals 3
    .param p1, "appId"    # I

    .prologue
    .line 630
    const/4 v0, 0x0

    .line 631
    .local v0, "szHeader":Ljava/lang/String;
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    const/4 v2, 0x2

    iput v2, v1, Lcom/policydm/tp/XTPHttpObj;->nTunnelConnected:I

    .line 632
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpPsrMakeSslTunnelHeader(II)Ljava/lang/String;

    move-result-object v0

    .line 633
    return-object v0
.end method

.method public xtpAdpHttpPsrChunkSizeParsing(Ljava/io/InputStream;)I
    .locals 14
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v12, 0x0

    .line 1772
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v11, ""

    invoke-direct {v3, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1773
    .local v3, "data":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 1774
    .local v1, "c":I
    const/4 v7, 0x1

    .line 1775
    .local v7, "p_num":I
    const/4 v0, 0x0

    .line 1776
    .local v0, "ChunkedLen":I
    const/4 v2, 0x0

    .line 1777
    .local v2, "ch":C
    const/16 v9, 0x61

    .line 1778
    .local v9, "small_A":C
    const/16 v6, 0x41

    .line 1782
    .local v6, "large_A":C
    const/4 v11, 0x1

    :try_start_0
    invoke-virtual {p1, v11}, Ljava/io/InputStream;->mark(I)V

    .line 1783
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v11

    const/4 v13, -0x1

    if-ne v11, v13, :cond_0

    move v11, v12

    .line 1830
    :goto_0
    return v11

    .line 1786
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V

    .line 1788
    :goto_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-ltz v1, :cond_1

    .line 1790
    const/16 v11, 0xd

    if-ne v1, v11, :cond_3

    .line 1792
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, Ljava/io/InputStream;->mark(I)V

    .line 1793
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/16 v11, 0xa

    if-ne v1, v11, :cond_2

    .line 1808
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1810
    .local v10, "szSize":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    .line 1811
    .local v8, "size":[C
    array-length v11, v8

    add-int/lit8 v5, v11, -0x1

    .local v5, "j":I
    :goto_2
    if-ltz v5, :cond_7

    .line 1813
    aget-char v11, v8, v5

    invoke-static {v11}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v11

    if-nez v11, :cond_4

    move v11, v12

    .line 1815
    goto :goto_0

    .line 1796
    .end local v5    # "j":I
    .end local v8    # "size":[C
    .end local v10    # "szSize":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1803
    :catch_0
    move-exception v4

    .line 1805
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v11, v12

    .line 1806
    goto :goto_0

    .line 1799
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_3
    int-to-char v11, v1

    :try_start_2
    invoke-virtual {v3, v11}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 1817
    .restart local v5    # "j":I
    .restart local v8    # "size":[C
    .restart local v10    # "szSize":Ljava/lang/String;
    :cond_4
    aget-char v11, v8, v5

    invoke-static {v11}, Ljava/lang/Character;->isDigit(C)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1819
    aget-char v11, v8, v5

    add-int/lit8 v11, v11, -0x30

    mul-int/2addr v11, v7

    add-int/2addr v0, v11

    .line 1827
    :goto_3
    mul-int/lit8 v7, v7, 0x10

    .line 1811
    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    .line 1823
    :cond_5
    aget-char v11, v8, v5

    if-ge v11, v9, :cond_6

    aget-char v11, v8, v5

    add-int/2addr v11, v9

    sub-int/2addr v11, v6

    :goto_4
    int-to-char v2, v11

    .line 1824
    sub-int v11, v2, v9

    add-int/lit8 v11, v11, 0xa

    mul-int/2addr v11, v7

    add-int/2addr v0, v11

    goto :goto_3

    .line 1823
    :cond_6
    aget-char v11, v8, v5

    goto :goto_4

    :cond_7
    move v11, v0

    .line 1830
    goto :goto_0
.end method

.method public xtpAdpHttpPsrGetContentLengthByRange(Ljava/lang/String;)I
    .locals 12
    .param p1, "szContentRange"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 1836
    const/4 v0, 0x0

    .line 1837
    .local v0, "contentlength":I
    const/4 v5, 0x0

    .local v5, "szHttpRange":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1838
    .local v6, "szRetRange":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "pos":I
    const/4 v3, 0x0

    .local v3, "len":I
    const/4 v2, 0x0

    .line 1840
    .local v2, "index":I
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "bytes "

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1841
    if-gez v4, :cond_1

    .line 1882
    :cond_0
    :goto_0
    return v8

    .line 1844
    :cond_1
    add-int/lit8 v9, v4, 0x6

    invoke-virtual {p1, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 1845
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1848
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    .line 1849
    if-eqz v3, :cond_0

    .line 1853
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 1855
    invoke-virtual {v5, v1}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x2f

    if-ne v9, v10, :cond_3

    .line 1857
    move v2, v1

    .line 1861
    :cond_2
    if-nez v2, :cond_4

    .line 1863
    move-object v6, v5

    .line 1869
    :goto_2
    const-string v9, "-"

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1870
    .local v7, "szTmp":[Ljava/lang/String;
    aget-object v9, v7, v8

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    aget-object v9, v7, v11

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1872
    aget-object v9, v7, v11

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aget-object v10, v7, v8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    sub-int v0, v9, v10

    .line 1873
    if-lez v0, :cond_0

    .line 1877
    add-int/lit8 v0, v0, 0x1

    move v8, v0

    .line 1882
    goto :goto_0

    .line 1853
    .end local v7    # "szTmp":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1867
    :cond_4
    invoke-virtual {v5, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method

.method public xtpAdpHttpPsrMakeSslTunnelHeader(II)Ljava/lang/String;
    .locals 8
    .param p1, "conLength"    # I
    .param p2, "appId"    # I

    .prologue
    const/4 v4, 0x0

    .line 638
    const-string v5, ""

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 639
    const/4 v3, 0x0

    .line 640
    .local v3, "szServerURL":Ljava/lang/String;
    invoke-virtual {p0, p2}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetHttpOpenMode(I)Ljava/lang/String;

    move-result-object v1

    .line 641
    .local v1, "szOpenMode":Ljava/lang/String;
    const-string v0, ""

    .line 642
    .local v0, "szHeaderlog":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 754
    :goto_0
    return-object v4

    .line 645
    :cond_0
    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 647
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    const-string v6, "CONNECT"

    if-ne v5, v6, :cond_b

    .line 649
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 651
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 652
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v5}, Lcom/policydm/tp/XTPHttpUtil;->xtpHttpParserServerAddrWithPort(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 653
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 660
    :goto_1
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 661
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 663
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget-object v6, v6, Lcom/policydm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 665
    :cond_1
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 667
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 669
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget-object v6, v6, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget v6, v6, Lcom/policydm/tp/XTPHttpObj;->nServerPort:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 670
    .local v2, "szServerHost":Ljava/lang/String;
    const-string v5, "Host"

    invoke-direct {p0, v2, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    .end local v2    # "szServerHost":Ljava/lang/String;
    :cond_2
    :goto_2
    const-string v5, "no-store, private"

    const-string v6, "Cache-Control"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 703
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    const-string v6, "Connection"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :cond_3
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 708
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    const-string v6, "User-Agent"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :cond_4
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 713
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    const-string v6, "Accept"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    :cond_5
    const-string v5, "en"

    const-string v6, "Accept-Language"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    const-string v5, "utf-8"

    const-string v6, "Accept-Charset"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 721
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    const-string v6, "Content-Type"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :cond_6
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 726
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    const-string v6, "Cookie"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :cond_7
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 731
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "bytes="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget-object v6, v6, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Range"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    :cond_8
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, "Content-Length: "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 735
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 736
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 738
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    if-eqz v5, :cond_9

    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    array-length v5, v5

    if-eqz v5, :cond_9

    .line 740
    new-instance v5, Ljava/lang/String;

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget-object v6, v6, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    const-string v6, "x-syncml-hmac"

    invoke-direct {p0, v5, v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iput-object v4, v5, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    .line 743
    :cond_9
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 745
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 752
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\r\n [_____SSL Proxy Make Header_____]\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 754
    iget-object v4, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    goto/16 :goto_0

    .line 657
    :cond_a
    const-string v5, "PATH is NULL. Checking the pHttpObj->pRequestUri"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 675
    :cond_b
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 677
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 678
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget-object v6, v6, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 685
    :goto_3
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 686
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 688
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget-object v6, v6, Lcom/policydm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 690
    :cond_c
    iget-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/policydm/tp/XTPAdapter;->m_szHttpHeaderData:Ljava/lang/String;

    .line 692
    sget-object v5, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v5, v5, p2

    iget-object v5, v5, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 694
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget-object v6, v6, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v6, v6, p2

    iget v6, v6, Lcom/policydm/tp/XTPHttpObj;->nServerPort:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 695
    .restart local v2    # "szServerHost":Ljava/lang/String;
    const-string v5, "Host"

    invoke-direct {p0, v2, v5}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpAppendHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 682
    .end local v2    # "szServerHost":Ljava/lang/String;
    :cond_d
    const-string v5, "PATH is NULL. Checking the pHttpObj->pRequestUri"

    invoke-static {v5}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public xtpAdpHttpPsrParserHMAC(Ljava/lang/String;I)Lcom/policydm/eng/core/XDMHmacData;
    .locals 8
    .param p1, "szValue"    # Ljava/lang/String;
    .param p2, "ConLen"    # I

    .prologue
    .line 1689
    new-instance v0, Lcom/policydm/eng/core/XDMHmacData;

    invoke-direct {v0}, Lcom/policydm/eng/core/XDMHmacData;-><init>()V

    .line 1690
    .local v0, "MacData":Lcom/policydm/eng/core/XDMHmacData;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1692
    const/4 v0, 0x0

    .line 1713
    .end local v0    # "MacData":Lcom/policydm/eng/core/XDMHmacData;
    :goto_0
    return-object v0

    .line 1695
    .restart local v0    # "MacData":Lcom/policydm/eng/core/XDMHmacData;
    :cond_0
    const-string v6, "algorithm="

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1696
    .local v1, "nStartAlgorithm":I
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1697
    .local v4, "szData":Ljava/lang/String;
    const/16 v6, 0x2c

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 1698
    .local v5, "token":I
    const-string v6, "algorithm="

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    .line 1699
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "algorithm:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacAlgorithm:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1701
    const-string v6, "username=\""

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1702
    .local v2, "nStartUserName":I
    const-string v6, "username=\""

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1703
    const-string v6, "\""

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 1704
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacUserName:Ljava/lang/String;

    .line 1707
    const-string v6, "mac="

    invoke-virtual {p1, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 1708
    .local v3, "nStartmac":I
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 1709
    const-string v6, "mac="

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    .line 1710
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "nStartmac:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/policydm/eng/core/XDMHmacData;->m_szHmacDigest:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1712
    iput p2, v0, Lcom/policydm/eng/core/XDMHmacData;->httpContentLength:I

    goto/16 :goto_0
.end method

.method public xtpAdpInit(I)I
    .locals 3
    .param p1, "appId"    # I

    .prologue
    .line 96
    const/4 v0, 0x0

    .line 98
    .local v0, "ret":I
    const-string v1, ""

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 100
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v1, v1, p1

    if-nez v1, :cond_0

    .line 101
    sget-object v1, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    new-instance v2, Lcom/policydm/tp/XTPHttpObj;

    invoke-direct {v2}, Lcom/policydm/tp/XTPHttpObj;-><init>()V

    aput-object v2, v1, p1

    .line 103
    :cond_0
    invoke-static {}, Lcom/policydm/adapter/XDMNetworkAdapter;->xdbAdpGetProxyData()Ljava/lang/Object;

    .line 104
    invoke-direct {p0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpInit(I)I

    move-result v0

    .line 105
    if-eqz v0, :cond_1

    .line 107
    const-string v1, "xtpAdpHttpInit Fail!!"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 109
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/policydm/dlagent/XFOTADlAgent;->xfotaDlAgentSetWriteStatus(Z)Z

    .line 110
    return v0
.end method

.method public xtpAdpMakeSSLTunneling(I)Ljava/lang/String;
    .locals 9
    .param p1, "appId"    # I

    .prologue
    .line 610
    const/4 v8, 0x0

    .line 611
    .local v8, "szHeader":Ljava/lang/String;
    const-string v2, ""

    .line 612
    .local v2, "szHmacData":Ljava/lang/String;
    const-string v3, ""

    .line 616
    .local v3, "szContentRange":Ljava/lang/String;
    :try_start_0
    sget-object v0, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v0, v0, p1

    iget-object v1, v0, Lcom/policydm/tp/XTPHttpObj;->m_szServerURL:Ljava/lang/String;

    const-string v4, "CONNECT"

    const/4 v6, 0x0

    move-object v0, p0

    move v5, p1

    invoke-virtual/range {v0 .. v6}, Lcom/policydm/tp/XTPAdapter;->xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 623
    invoke-virtual {p0, p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpMakeSSLTunnelHeader(I)Ljava/lang/String;

    move-result-object v8

    move-object v0, v8

    .line 625
    :goto_0
    return-object v0

    .line 618
    :catch_0
    move-exception v7

    .line 620
    .local v7, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 621
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public xtpAdpOpen(I)I
    .locals 20
    .param p1, "appId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 115
    const/4 v7, 0x0

    .line 117
    .local v7, "nRet":I
    const-string v14, ""

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 118
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    if-nez v14, :cond_0

    .line 120
    invoke-virtual/range {p0 .. p1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpInit(I)I

    move-result v7

    .line 122
    if-eqz v7, :cond_0

    .line 124
    const-string v14, "xtpAdpInit Fail!!"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    move v14, v7

    .line 299
    :goto_0
    return v14

    .line 129
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v14, :cond_2

    .line 131
    const-string v14, "power"

    invoke-static {v14}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/PowerManager;

    .line 132
    .local v8, "pm":Landroid/os/PowerManager;
    if-nez v8, :cond_1

    .line 134
    const-string v14, "PowerManager is null!!"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 135
    const/16 v14, -0xc

    goto :goto_0

    .line 138
    :cond_1
    const/4 v14, 0x1

    const-string v15, "wakeLock"

    invoke-virtual {v8, v14, v15}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    .line 139
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 140
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_WakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v14}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 143
    .end local v8    # "pm":Landroid/os/PowerManager;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    if-nez v14, :cond_4

    .line 145
    const-string v14, "wifi"

    invoke-static {v14}, Lcom/policydm/XDMApplication;->xdmGetServiceManager(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/wifi/WifiManager;

    .line 146
    .local v13, "wifiManager":Landroid/net/wifi/WifiManager;
    if-nez v13, :cond_3

    .line 148
    const-string v14, "WifiManager is null!!"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 149
    const/16 v14, -0xc

    goto :goto_0

    .line 151
    :cond_3
    const-string v14, "wifilock"

    invoke-virtual {v13, v14}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 152
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    .line 153
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_WifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v14}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    .line 156
    .end local v13    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_4
    new-instance v14, Lcom/policydm/tp/XTPNetConnectTimer;

    move/from16 v0, p1

    invoke-direct {v14, v0}, Lcom/policydm/tp/XTPNetConnectTimer;-><init>(I)V

    .line 158
    const/4 v9, 0x0

    .line 160
    .local v9, "socketAddress":Ljava/net/SocketAddress;
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget v14, v14, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_6

    .line 164
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 166
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_conProxy:Ljava/net/Proxy;

    invoke-virtual {v14}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v9

    .line 174
    :goto_1
    new-instance v14, Ljava/net/Socket;

    invoke-direct {v14}, Ljava/net/Socket;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    .line 175
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const v15, 0xea60

    invoke-virtual {v14, v9, v15}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 177
    new-instance v14, Ljava/io/BufferedInputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    invoke-virtual {v15}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    const/16 v16, 0x1000

    invoke-direct/range {v14 .. v16}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    .line 178
    new-instance v14, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    invoke-virtual {v15}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v15

    const/16 v16, 0x400

    invoke-direct/range {v14 .. v16}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :goto_2
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 299
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 171
    :cond_5
    :try_start_1
    new-instance v10, Ljava/net/InetSocketAddress;

    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget-object v14, v14, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    sget-object v15, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v15, v15, p1

    iget v15, v15, Lcom/policydm/tp/XTPHttpObj;->nServerPort:I

    invoke-direct {v10, v14, v15}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .end local v9    # "socketAddress":Ljava/net/SocketAddress;
    .local v10, "socketAddress":Ljava/net/SocketAddress;
    move-object v9, v10

    .end local v10    # "socketAddress":Ljava/net/SocketAddress;
    .restart local v9    # "socketAddress":Ljava/net/SocketAddress;
    goto :goto_1

    .line 180
    :catch_0
    move-exception v5

    .line 182
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 183
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 184
    const/16 v14, -0xc

    goto/16 :goto_0

    .line 187
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_6
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget v14, v14, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_b

    .line 189
    const/4 v11, 0x0

    .local v11, "szSSLHost":Ljava/lang/String;
    const/4 v2, 0x0

    .line 190
    .local v2, "SSLPXHost":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "SSLPort":I
    const/4 v3, 0x0

    .line 193
    .local v3, "SSLPXPort":I
    :try_start_2
    const-string v14, "TLS"

    invoke-static {v14}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    .line 194
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    if-eqz v14, :cond_7

    .line 197
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    const/4 v15, 0x0

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    new-instance v18, Lcom/policydm/tp/XTPAdapter$XTPTrustManager;

    const/16 v19, 0x0

    invoke-direct/range {v18 .. v19}, Lcom/policydm/tp/XTPAdapter$XTPTrustManager;-><init>(Ljava/security/KeyStore;)V

    aput-object v18, v16, v17

    new-instance v17, Ljava/security/SecureRandom;

    invoke-direct/range {v17 .. v17}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual/range {v14 .. v17}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLContext;->getServerSessionContext()Ljavax/net/ssl/SSLSessionContext;

    move-result-object v14

    const v15, 0xea60

    invoke-interface {v14, v15}, Ljavax/net/ssl/SSLSessionContext;->setSessionTimeout(I)V

    .line 199
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLContext:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 215
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v14

    if-eqz v14, :cond_a

    .line 217
    new-instance v14, Ljava/net/Socket;

    invoke-direct {v14}, Ljava/net/Socket;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    .line 218
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget-object v2, v14, Lcom/policydm/tp/XTPHttpObj;->m_szProxyAddr:Ljava/lang/String;

    .line 219
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget v3, v14, Lcom/policydm/tp/XTPHttpObj;->proxyPort:I

    .line 221
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget-object v11, v14, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    .line 222
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget v4, v14, Lcom/policydm/tp/XTPHttpObj;->nServerPort:I

    .line 226
    :try_start_3
    new-instance v12, Ljava/net/InetSocketAddress;

    invoke-direct {v12, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 227
    .local v12, "tunnelAddr":Ljava/net/InetSocketAddress;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const v15, 0xea60

    invoke-virtual {v14, v12, v15}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 229
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v14, v1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpTunnelHandshake(Ljava/net/Socket;I)I

    move-result v7

    .line 230
    if-eqz v7, :cond_8

    .line 232
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 233
    const/16 v14, -0xc

    goto/16 :goto_0

    .line 202
    .end local v12    # "tunnelAddr":Ljava/net/InetSocketAddress;
    :catch_1
    move-exception v5

    .line 204
    .local v5, "e":Ljava/lang/RuntimeException;
    const-string v14, "HttpsConnection: failed to initialize the socket factory"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 205
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 206
    const/16 v14, -0xc

    goto/16 :goto_0

    .line 208
    .end local v5    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v5

    .line 210
    .local v5, "e":Ljava/lang/Exception;
    const-string v14, "HttpsConnection: failed to initialize the socket factory"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 211
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 212
    const/16 v14, -0xc

    goto/16 :goto_0

    .line 237
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v12    # "tunnelAddr":Ljava/net/InetSocketAddress;
    :cond_8
    :try_start_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v14, v15, v11, v4, v0}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v14

    check-cast v14, Ljavax/net/ssl/SSLSocket;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    .line 238
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    const v15, 0xea60

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 239
    new-instance v14, Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;-><init>(Lcom/policydm/tp/XTPAdapter;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    .line 240
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 241
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 283
    .end local v12    # "tunnelAddr":Ljava/net/InetSocketAddress;
    :goto_3
    :try_start_5
    new-instance v14, Ljava/io/BufferedInputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v15}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v15

    const/16 v16, 0x1000

    invoke-direct/range {v14 .. v16}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    .line 284
    new-instance v14, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v15}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v15

    const/16 v16, 0x400

    invoke-direct/range {v14 .. v16}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_2

    .line 286
    :catch_3
    move-exception v5

    .line 288
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 289
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 290
    const/16 v14, -0xc

    goto/16 :goto_0

    .line 243
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    .line 245
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 248
    :try_start_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    if-eqz v14, :cond_9

    .line 249
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLSocket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 255
    :cond_9
    :goto_4
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 256
    const/16 v14, -0xc

    goto/16 :goto_0

    .line 251
    :catch_5
    move-exception v6

    .line 253
    .local v6, "e1":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_4

    .line 261
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/io/IOException;
    :cond_a
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget-object v11, v14, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    .line 262
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p1

    iget v4, v14, Lcom/policydm/tp/XTPHttpObj;->nServerPort:I

    .line 266
    :try_start_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLFactory:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLSocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v14

    check-cast v14, Ljavax/net/ssl/SSLSocket;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    .line 267
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    new-instance v15, Ljava/net/InetSocketAddress;

    invoke-direct {v15, v11, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const v16, 0xea60

    invoke-virtual/range {v14 .. v16}, Ljavax/net/ssl/SSLSocket;->connect(Ljava/net/SocketAddress;I)V

    .line 268
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    const v15, 0xea60

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 269
    new-instance v14, Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;-><init>(Lcom/policydm/tp/XTPAdapter;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    .line 270
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLListener:Lcom/policydm/tp/XTPAdapter$xtpHandshakeListener;

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v14}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_3

    .line 273
    :catch_6
    move-exception v5

    .line 275
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 276
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V

    .line 277
    const/16 v14, -0xc

    goto/16 :goto_0

    .line 295
    .end local v2    # "SSLPXHost":Ljava/lang/String;
    .end local v3    # "SSLPXPort":I
    .end local v4    # "SSLPort":I
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v11    # "szSSLHost":Ljava/lang/String;
    :cond_b
    const-string v14, "Other ProtocolType"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public xtpAdpReceiveData(Ljava/io/ByteArrayOutputStream;I)I
    .locals 21
    .param p1, "pData"    # Ljava/io/ByteArrayOutputStream;
    .param p2, "appId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 759
    const/4 v15, 0x0

    .line 760
    .local v15, "nRet":I
    const/4 v14, 0x0

    .line 761
    .local v14, "nFumoStatus":I
    const/4 v8, 0x0

    .line 762
    .local v8, "actualBuff":[B
    const/4 v10, 0x0

    .line 763
    .local v10, "chunkedlen":I
    const/4 v5, 0x0

    .line 764
    .local v5, "aBuff":Ljava/io/ByteArrayInputStream;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    .line 766
    .local v12, "in":Ljava/io/InputStream;
    const/4 v9, 0x0

    .line 767
    .local v9, "bytesread":I
    const/4 v4, 0x0

    .line 768
    .local v4, "ContentBytesread":I
    const/4 v7, 0x0

    .line 770
    .local v7, "actual":I
    const-string v17, ""

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 772
    if-nez v12, :cond_0

    .line 774
    const/16 v17, -0xc

    .line 1179
    :goto_0
    return v17

    .line 777
    :cond_0
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 781
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    move-object/from16 v17, v0

    const v18, 0xea60

    invoke-virtual/range {v17 .. v18}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 782
    new-instance v17, Lcom/policydm/tp/XTPNetRecvTimer;

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/tp/XTPNetRecvTimer;-><init>(I)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    .line 810
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1, v12}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpHeaderParser(ILjava/io/InputStream;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result v15

    .line 818
    :goto_2
    if-eqz v15, :cond_2

    .line 820
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 821
    const/16 v17, -0xe

    goto :goto_0

    .line 784
    :catch_0
    move-exception v11

    .line 786
    .local v11, "e":Ljava/net/SocketException;
    invoke-virtual {v11}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 787
    const-string v17, "Time out"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 788
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 789
    const/16 v17, -0xe

    goto :goto_0

    .line 796
    .end local v11    # "e":Ljava/net/SocketException;
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    move-object/from16 v17, v0

    const v18, 0xea60

    invoke-virtual/range {v17 .. v18}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 797
    new-instance v17, Lcom/policydm/tp/XTPNetRecvTimer;

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/tp/XTPNetRecvTimer;-><init>(I)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 799
    :catch_1
    move-exception v11

    .line 801
    .restart local v11    # "e":Ljava/net/SocketException;
    invoke-virtual {v11}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 802
    const-string v17, "Time out"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 803
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 804
    const/16 v17, -0xe

    goto :goto_0

    .line 812
    .end local v11    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v11

    .line 814
    .local v11, "e":Ljava/lang/Exception;
    const/16 v15, -0xe

    .line 815
    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_2

    .line 824
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_2
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    move/from16 v17, v0

    const/16 v18, 0xc8

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_3

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    move/from16 v17, v0

    const/16 v18, 0x12c

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_5

    .line 826
    :cond_3
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 828
    const/16 v17, 0x1

    move/from16 v0, p2

    move/from16 v1, v17

    if-ne v0, v1, :cond_4

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    move/from16 v17, v0

    const/16 v18, 0x1a0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 829
    const/16 v17, -0x10

    goto/16 :goto_0

    .line 831
    :cond_4
    const/16 v17, -0xf

    goto/16 :goto_0

    .line 835
    :cond_5
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    move/from16 v17, v0

    if-nez v17, :cond_c

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nTransferCoding:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_c

    .line 837
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_6

    .line 839
    const-string v17, "Content-length 0, Content-Range Use"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 840
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    sget-object v18, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v18, v18, p2

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpPsrGetContentLengthByRange(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    .line 848
    :cond_6
    :goto_3
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    move/from16 v17, v0

    if-eqz v17, :cond_7

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nTransferCoding:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 850
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    .line 853
    :cond_7
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    move/from16 v17, v0

    if-lez v17, :cond_e

    .line 854
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    .line 858
    :goto_4
    invoke-static {}, Lcom/policydm/db/XDBFumoAdp;->xdbGetFUMOStatus()I

    move-result v14

    .line 859
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-ne v14, v0, :cond_8

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/tp/XTPAdapter;->m_dlhandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    move-object/from16 v17, v0

    if-nez v17, :cond_8

    .line 862
    new-instance v17, Lcom/policydm/dlagent/XFOTADlAgentHandler;

    invoke-direct/range {v17 .. v17}, Lcom/policydm/dlagent/XFOTADlAgentHandler;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/policydm/tp/XTPAdapter;->m_dlhandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    .line 865
    :cond_8
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nTransferCoding:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_22

    .line 867
    const-string v17, "SYNCMLDM CHUNKED"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 868
    if-nez p2, :cond_14

    .line 870
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const/16 v18, 0x1400

    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    .line 873
    :goto_5
    :try_start_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpPsrChunkSizeParsing(Ljava/io/InputStream;)I

    move-result v10

    if-lez v10, :cond_11

    .line 875
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "chunkedlen:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 876
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    new-array v0, v10, [B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/policydm/tp/XTPHttpObj;->pChunkBuffer:[B
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-object v6, v5

    .line 877
    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .local v6, "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_9
    :goto_6
    if-eq v10, v4, :cond_10

    .line 879
    :try_start_4
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pChunkBuffer:[B

    move-object/from16 v17, v0

    sub-int v18, v10, v4

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    if-lez v7, :cond_9

    .line 881
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "ContentBytesread:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " , actual :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 882
    new-array v8, v7, [B

    .line 883
    new-instance v5, Ljava/io/ByteArrayInputStream;

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pChunkBuffer:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v5, v0, v4, v7}, Ljava/io/ByteArrayInputStream;-><init>([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_14

    .line 887
    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    :try_start_5
    invoke-virtual {v5, v8}, Ljava/io/ByteArrayInputStream;->read([B)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v13

    .line 888
    .local v13, "nByteSize":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v13, v0, :cond_a

    .line 901
    :cond_a
    if-eqz v5, :cond_b

    .line 902
    :try_start_6
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 909
    .end local v13    # "nByteSize":I
    :cond_b
    :goto_7
    const/16 v17, 0x0

    :try_start_7
    sget-object v18, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v18, v18, p2

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v18, v0

    array-length v0, v8

    move/from16 v19, v0

    move/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 910
    add-int/2addr v4, v7

    .line 912
    const/4 v7, -0x1

    move-object v6, v5

    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto :goto_6

    .line 843
    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_c
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    move/from16 v17, v0

    if-eqz v17, :cond_d

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget v0, v0, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    const-wide/32 v19, 0x7fffffff

    cmp-long v17, v17, v19

    if-nez v17, :cond_6

    .line 845
    :cond_d
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    goto/16 :goto_3

    .line 856
    :cond_e
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    goto/16 :goto_4

    .line 904
    .restart local v13    # "nByteSize":I
    :catch_3
    move-exception v11

    .line 906
    .local v11, "e":Ljava/io/IOException;
    :try_start_8
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_7

    .line 919
    .end local v11    # "e":Ljava/io/IOException;
    .end local v13    # "nByteSize":I
    :catch_4
    move-exception v11

    .line 921
    .restart local v11    # "e":Ljava/io/IOException;
    :goto_8
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 922
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 923
    const/16 v17, -0xe

    goto/16 :goto_0

    .line 893
    .end local v11    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v11

    .line 895
    .restart local v11    # "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 901
    if-eqz v5, :cond_b

    .line 902
    :try_start_a
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_7

    .line 904
    :catch_6
    move-exception v11

    .line 906
    :try_start_b
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto/16 :goto_7

    .line 899
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v17

    .line 901
    if-eqz v5, :cond_f

    .line 902
    :try_start_c
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    .line 907
    :cond_f
    :goto_9
    :try_start_d
    throw v17

    .line 904
    :catch_7
    move-exception v11

    .line 906
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4

    goto :goto_9

    .line 915
    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_10
    :try_start_e
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpChunkedHeadRead(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_14

    .line 916
    const/4 v10, 0x0

    move-object v5, v6

    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_5

    .line 926
    :cond_11
    move-object/from16 v0, p0

    iput v4, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    .line 927
    const/4 v4, 0x0

    .line 928
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "CHUNKED pHttpObj[appId].pReceiveBuffer.length = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    sget-object v18, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v18, v18, p2

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 929
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "CHUNKED nHttpBodyLength = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 931
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-eq v14, v0, :cond_12

    .line 933
    invoke-virtual/range {p1 .. p1}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 934
    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v17, v0

    if-lez v17, :cond_13

    .line 935
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 1158
    :cond_12
    :goto_a
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/policydm/tp/XTPAdapter;->xtpAdpWBXMLLog(I[B)V

    .line 1160
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 1163
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_30

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Close"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_30

    .line 1165
    const-string v17, "_______HTTP_CONNECTION_TYPE_CLOSE MODE_______"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1166
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/policydm/tp/XTPHttpObj;->nHttpConnection:I

    .line 1167
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/policydm/tp/XTPAdapter;->xtpAdpClose(I)V

    .line 1179
    :goto_b
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 937
    :cond_13
    const/16 p1, 0x0

    goto :goto_a

    .line 942
    :cond_14
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-eq v14, v0, :cond_15

    .line 944
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const/16 v18, 0x1400

    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    .line 945
    const-string v17, "DL MODE BUT NOT DOWNLOAD_IN_PROGRESS"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 950
    :cond_15
    :goto_c
    :try_start_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpPsrChunkSizeParsing(Ljava/io/InputStream;)I

    move-result v10

    if-lez v10, :cond_20

    .line 952
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "chunkedlen:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 953
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    new-array v0, v10, [B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/policydm/tp/XTPHttpObj;->pChunkBuffer:[B
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_9

    move-object v6, v5

    .line 954
    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_16
    :goto_d
    if-eq v10, v4, :cond_34

    .line 956
    :try_start_10
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pChunkBuffer:[B

    move-object/from16 v17, v0

    sub-int v18, v10, v4

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    if-lez v7, :cond_16

    .line 958
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "ContentBytesread:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " , bytesread:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " , actual :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 959
    new-array v8, v7, [B

    .line 960
    new-instance v5, Ljava/io/ByteArrayInputStream;

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pChunkBuffer:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v5, v0, v4, v7}, Ljava/io/ByteArrayInputStream;-><init>([BII)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_13

    .line 964
    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    :try_start_11
    invoke-virtual {v5, v8}, Ljava/io/ByteArrayInputStream;->read([B)I

    move-result v16

    .line 965
    .local v16, "ret":I
    const/16 v17, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_17

    .line 967
    const-string v17, "Buff read fail"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 978
    :cond_17
    if-eqz v5, :cond_18

    .line 979
    :try_start_12
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_8

    .line 987
    .end local v16    # "ret":I
    :cond_18
    :goto_e
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-ne v14, v0, :cond_1d

    .line 989
    :try_start_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/tp/XTPAdapter;->m_dlhandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7, v8}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrWriteFirmwareObject(I[B)I

    move-result v15

    .line 990
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 991
    new-instance v17, Lcom/policydm/tp/XTPNetRecvTimer;

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/tp/XTPNetRecvTimer;-><init>(I)V

    .line 993
    const/16 v17, 0x4

    move/from16 v0, v17

    if-ne v15, v0, :cond_1a

    .line 994
    const/16 v17, -0x12

    goto/16 :goto_0

    .line 981
    .restart local v16    # "ret":I
    :catch_8
    move-exception v11

    .line 983
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_9

    goto :goto_e

    .line 1020
    .end local v11    # "e":Ljava/io/IOException;
    .end local v16    # "ret":I
    :catch_9
    move-exception v11

    .line 1022
    .restart local v11    # "e":Ljava/io/IOException;
    :goto_f
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1023
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 1024
    const/16 v17, -0xe

    goto/16 :goto_0

    .line 970
    .end local v11    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v11

    .line 972
    .restart local v11    # "e":Ljava/io/IOException;
    :try_start_14
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 978
    if-eqz v5, :cond_18

    .line 979
    :try_start_15
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_b

    goto :goto_e

    .line 981
    :catch_b
    move-exception v11

    .line 983
    :try_start_16
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_9

    goto :goto_e

    .line 976
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v17

    .line 978
    if-eqz v5, :cond_19

    .line 979
    :try_start_17
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_c

    .line 984
    :cond_19
    :goto_10
    :try_start_18
    throw v17

    .line 981
    :catch_c
    move-exception v11

    .line 983
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_10

    .line 995
    .end local v11    # "e":Ljava/io/IOException;
    :cond_1a
    const/16 v17, 0x5

    move/from16 v0, v17

    if-eq v15, v0, :cond_1b

    const/16 v17, 0x3

    move/from16 v0, v17

    if-ne v15, v0, :cond_1c

    .line 996
    :cond_1b
    const/16 v17, -0x13

    goto/16 :goto_0

    .line 997
    :cond_1c
    if-eqz v15, :cond_1f

    .line 998
    const/16 v17, -0xe

    goto/16 :goto_0

    .line 1000
    :cond_1d
    const/16 v17, 0x14

    move/from16 v0, v17

    if-ne v14, v0, :cond_1e

    .line 1014
    :goto_11
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpChunkedHeadRead(Ljava/io/InputStream;)Ljava/lang/String;

    .line 1015
    const/4 v10, 0x0

    .line 1016
    const/4 v4, 0x0

    goto/16 :goto_c

    .line 1006
    :cond_1e
    const/16 v17, 0x0

    sget-object v18, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v18, v18, p2

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v18, v0

    array-length v0, v8

    move/from16 v19, v0

    move/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v8, v0, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_9

    .line 1009
    :cond_1f
    add-int/2addr v4, v7

    .line 1010
    add-int/2addr v9, v7

    .line 1011
    const/4 v7, -0x1

    move-object v6, v5

    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_d

    .line 1027
    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_20
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "CHUNKED nHttpBodyLength = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1028
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-eq v14, v0, :cond_12

    .line 1034
    move-object/from16 v0, p0

    iput v9, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    .line 1035
    invoke-virtual/range {p1 .. p1}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 1036
    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v17, v0

    if-lez v17, :cond_21

    .line 1037
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto/16 :goto_a

    .line 1039
    :cond_21
    const/16 p1, 0x0

    goto/16 :goto_a

    .line 1046
    :cond_22
    const/16 v17, 0x28

    move/from16 v0, v17

    if-eq v14, v0, :cond_33

    const/16 v17, 0x14

    move/from16 v0, v17

    if-eq v14, v0, :cond_33

    .line 1048
    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v17, v0

    if-nez v17, :cond_33

    .line 1050
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 1051
    const/16 v17, -0xe

    goto/16 :goto_0

    .line 1129
    :cond_23
    if-eqz v8, :cond_24

    .line 1130
    const/4 v8, 0x0

    .line 1132
    :cond_24
    add-int/2addr v9, v7

    .line 1133
    add-int/2addr v4, v7

    move-object v6, v5

    .line 1057
    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    :goto_12
    :try_start_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v4, v0, :cond_32

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v7, v0, :cond_32

    .line 1059
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-ne v14, v0, :cond_28

    .line 1061
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const/16 v18, 0x1400

    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    .line 1062
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-lez v7, :cond_2a

    .line 1064
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "ContentBytesread:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " , actual :"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1065
    new-array v8, v7, [B

    .line 1066
    new-instance v5, Ljava/io/ByteArrayInputStream;

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v5, v0, v1, v7}, Ljava/io/ByteArrayInputStream;-><init>([BII)V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_12

    .line 1070
    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    :try_start_1a
    invoke-virtual {v5, v8}, Ljava/io/ByteArrayInputStream;->read([B)I

    move-result v16

    .line 1071
    .restart local v16    # "ret":I
    const/16 v17, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_25

    .line 1073
    const-string v17, "Buff read fail"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_f
    .catchall {:try_start_1a .. :try_end_1a} :catchall_2

    .line 1084
    :cond_25
    if-eqz v5, :cond_26

    .line 1085
    :try_start_1b
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_d

    .line 1111
    .end local v16    # "ret":I
    :cond_26
    :goto_13
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-ne v14, v0, :cond_2e

    .line 1113
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/policydm/tp/XTPAdapter;->m_dlhandler:Lcom/policydm/dlagent/XFOTADlAgentHandler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7, v8}, Lcom/policydm/dlagent/XFOTADlAgentHandler;->xfotaDlAgentHdlrWriteFirmwareObject(I[B)I

    move-result v15

    .line 1114
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 1115
    new-instance v17, Lcom/policydm/tp/XTPNetRecvTimer;

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/policydm/tp/XTPNetRecvTimer;-><init>(I)V

    .line 1117
    const/16 v17, 0x4

    move/from16 v0, v17

    if-ne v15, v0, :cond_2b

    .line 1118
    const/16 v17, -0x12

    goto/16 :goto_0

    .line 1087
    .restart local v16    # "ret":I
    :catch_d
    move-exception v11

    .line 1089
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_e

    goto :goto_13

    .line 1137
    .end local v11    # "e":Ljava/io/IOException;
    .end local v16    # "ret":I
    :catch_e
    move-exception v11

    .line 1139
    .restart local v11    # "e":Ljava/io/IOException;
    :goto_14
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1140
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 1141
    const/16 v17, -0xe

    goto/16 :goto_0

    .line 1076
    .end local v11    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v11

    .line 1078
    .restart local v11    # "e":Ljava/io/IOException;
    :try_start_1d
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    .line 1084
    if-eqz v5, :cond_26

    .line 1085
    :try_start_1e
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_10

    goto :goto_13

    .line 1087
    :catch_10
    move-exception v11

    .line 1089
    :try_start_1f
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_e

    goto :goto_13

    .line 1082
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v17

    .line 1084
    if-eqz v5, :cond_27

    .line 1085
    :try_start_20
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_11

    .line 1090
    :cond_27
    :goto_15
    :try_start_21
    throw v17

    .line 1087
    :catch_11
    move-exception v11

    .line 1089
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_e

    goto :goto_15

    .line 1096
    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_28
    :try_start_22
    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v17, v0

    if-lez v17, :cond_2a

    .line 1099
    if-nez v4, :cond_29

    .line 1101
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v0, v0, [B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    .line 1103
    :cond_29
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v18, v0

    sub-int v18, v18, v4

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v12, v0, v4, v1}, Ljava/io/InputStream;->read([BII)I
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_12

    move-result v7

    .line 1104
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v7, v0, :cond_2a

    :cond_2a
    move-object v5, v6

    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_13

    .line 1119
    :cond_2b
    const/16 v17, 0x5

    move/from16 v0, v17

    if-eq v15, v0, :cond_2c

    const/16 v17, 0x3

    move/from16 v0, v17

    if-ne v15, v0, :cond_2d

    .line 1120
    :cond_2c
    const/16 v17, -0x13

    goto/16 :goto_0

    .line 1121
    :cond_2d
    if-eqz v15, :cond_23

    .line 1122
    const/16 v17, -0xe

    goto/16 :goto_0

    .line 1124
    :cond_2e
    const/16 v17, 0x14

    move/from16 v0, v17

    if-ne v14, v0, :cond_23

    .line 1144
    :goto_16
    const/16 v17, 0x1e

    move/from16 v0, v17

    if-eq v14, v0, :cond_12

    .line 1150
    invoke-virtual/range {p1 .. p1}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 1151
    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v17, v0

    if-lez v17, :cond_2f

    .line 1152
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->pReceiveBuffer:[B

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/policydm/tp/XTPAdapter;->m_nHttpBodyLength:I

    move/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto/16 :goto_a

    .line 1154
    :cond_2f
    const/16 p1, 0x0

    goto/16 :goto_a

    .line 1169
    :cond_30
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_31

    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Keep-Alive"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_31

    .line 1171
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/policydm/tp/XTPHttpObj;->nHttpConnection:I

    goto/16 :goto_b

    .line 1175
    :cond_31
    const-string v17, "_______HTTP_CONNECTION_TYPE_NONE MODE_______"

    invoke-static/range {v17 .. v17}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1176
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/policydm/tp/XTPHttpObj;->nHttpConnection:I

    .line 1177
    sget-object v17, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v17, v17, p2

    const-string v18, "Keep-Alive"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    goto/16 :goto_b

    .line 1137
    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    :catch_12
    move-exception v11

    move-object v5, v6

    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_14

    .line 1020
    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    :catch_13
    move-exception v11

    move-object v5, v6

    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_f

    .line 919
    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    :catch_14
    move-exception v11

    move-object v5, v6

    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_8

    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    :cond_32
    move-object v5, v6

    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_16

    :cond_33
    move-object v6, v5

    .end local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_12

    :cond_34
    move-object v5, v6

    .end local v6    # "aBuff":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "aBuff":Ljava/io/ByteArrayInputStream;
    goto/16 :goto_11
.end method

.method public xtpAdpSendData([BII)I
    .locals 17
    .param p1, "pData"    # [B
    .param p2, "dataSize"    # I
    .param p3, "appId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 468
    const/4 v7, 0x0

    .line 469
    .local v7, "nRet":I
    const/4 v12, 0x0

    .line 470
    .local v12, "szHttpHeaderData":Ljava/lang/String;
    const/4 v9, 0x0

    .line 471
    .local v9, "pSendBuffer":[B
    const/4 v4, 0x0

    .line 473
    .local v4, "SendDataLen":I
    const-string v14, ""

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 475
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    if-eqz v14, :cond_0

    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p3

    if-nez v14, :cond_1

    .line 477
    :cond_0
    const/16 v7, -0xc

    .line 605
    .end local v7    # "nRet":I
    :goto_0
    return v7

    .line 482
    .restart local v7    # "nRet":I
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    if-eqz v14, :cond_2

    .line 484
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/io/InputStream;->mark(I)V

    .line 485
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Input:Ljava/io/InputStream;

    invoke-virtual {v14}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    :cond_2
    if-eqz p1, :cond_4

    .line 497
    const/4 v14, 0x1

    move/from16 v0, p3

    if-ne v0, v14, :cond_3

    .line 498
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "DownloadServer : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    new-instance v15, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 499
    :cond_3
    move-object/from16 v9, p1

    .line 500
    array-length v4, v9

    .line 507
    :goto_1
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p3

    iget v14, v14, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_5

    .line 511
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_Socket:Ljava/net/Socket;

    const v15, 0xea60

    invoke-virtual {v14, v15}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 512
    new-instance v14, Lcom/policydm/tp/XTPNetSendTimer;

    move/from16 v0, p3

    invoke-direct {v14, v0}, Lcom/policydm/tp/XTPNetSendTimer;-><init>(I)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1

    .line 520
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpPsrMakeHeader(II)Ljava/lang/String;

    move-result-object v12

    .line 521
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 523
    const-string v14, "pHttpHeaderData is null"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 524
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 525
    const/16 v7, -0xd

    goto :goto_0

    .line 488
    :catch_0
    move-exception v5

    .line 490
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 491
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 492
    const/16 v7, -0xd

    goto/16 :goto_0

    .line 504
    .end local v5    # "e":Ljava/io/IOException;
    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    .line 514
    :catch_1
    move-exception v5

    .line 516
    .local v5, "e":Ljava/net/SocketException;
    invoke-virtual {v5}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 517
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 518
    const/16 v7, -0xd

    goto/16 :goto_0

    .line 528
    .end local v5    # "e":Ljava/net/SocketException;
    :cond_5
    sget-object v14, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v14, v14, p3

    iget v14, v14, Lcom/policydm/tp/XTPHttpObj;->protocol:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_7

    .line 532
    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/policydm/tp/XTPAdapter;->m_SSLSocket:Ljavax/net/ssl/SSLSocket;

    const v15, 0xea60

    invoke-virtual {v14, v15}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 533
    new-instance v14, Lcom/policydm/tp/XTPNetSendTimer;

    move/from16 v0, p3

    invoke-direct {v14, v0}, Lcom/policydm/tp/XTPNetSendTimer;-><init>(I)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_2

    .line 541
    invoke-virtual/range {p0 .. p0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 543
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpPsrMakeSslTunnelHeader(II)Ljava/lang/String;

    move-result-object v12

    .line 544
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 546
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 547
    const/16 v7, -0xd

    goto/16 :goto_0

    .line 535
    :catch_2
    move-exception v5

    .line 537
    .restart local v5    # "e":Ljava/net/SocketException;
    invoke-virtual {v5}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 538
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 539
    const/16 v7, -0xd

    goto/16 :goto_0

    .line 552
    .end local v5    # "e":Ljava/net/SocketException;
    :cond_6
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/policydm/tp/XTPAdapter;->xtpAdpHttpPsrMakeHeader(II)Ljava/lang/String;

    move-result-object v12

    .line 553
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 555
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 556
    const/16 v7, -0xd

    goto/16 :goto_0

    .line 562
    :cond_7
    const-string v14, "Other ProtocolType"

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 563
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 564
    const/16 v7, -0xf

    goto/16 :goto_0

    .line 567
    :cond_8
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1, v9}, Lcom/policydm/tp/XTPAdapter;->xtpAdpWBXMLLog(I[B)V

    .line 569
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    .line 570
    .local v6, "headbyte":[B
    array-length v14, v6

    add-int/2addr v14, v4

    new-array v3, v14, [B

    .line 572
    .local v3, "SendBuf":[B
    const/4 v14, 0x0

    const/4 v15, 0x0

    array-length v0, v6

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v6, v14, v3, v15, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 573
    if-eqz v4, :cond_9

    if-eqz v9, :cond_9

    .line 574
    const/4 v14, 0x0

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v15

    array-length v15, v15

    invoke-static {v9, v14, v3, v15, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 576
    :cond_9
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/policydm/tp/XTPAdapter;->m_Output:Ljava/io/OutputStream;

    .line 577
    .local v8, "out":Ljava/io/OutputStream;
    const/4 v10, 0x0

    .line 578
    .local v10, "position":I
    array-length v13, v3

    .line 581
    .local v13, "totalLength":I
    :goto_2
    if-eq v10, v13, :cond_b

    .line 583
    sub-int v14, v13, v10

    const/16 v15, 0x100

    if-le v14, v15, :cond_a

    const/16 v11, 0x100

    .line 586
    .local v11, "sendLength":I
    :goto_3
    :try_start_3
    invoke-virtual {v8, v3, v10, v11}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 594
    add-int/2addr v10, v11

    .line 595
    goto :goto_2

    .line 583
    .end local v11    # "sendLength":I
    :cond_a
    sub-int v11, v13, v10

    goto :goto_3

    .line 588
    .restart local v11    # "sendLength":I
    :catch_3
    move-exception v5

    .line 590
    .local v5, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 591
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 592
    const/16 v7, -0xd

    goto/16 :goto_0

    .line 596
    .end local v5    # "e":Ljava/io/IOException;
    .end local v11    # "sendLength":I
    :cond_b
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 604
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    goto/16 :goto_0

    .line 598
    :catch_4
    move-exception v5

    .line 600
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 601
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 602
    const/16 v7, -0xd

    goto/16 :goto_0
.end method

.method public xtpAdpSetCurHMACData(Lcom/policydm/eng/core/XDMHmacData;)I
    .locals 2
    .param p1, "MacData"    # Lcom/policydm/eng/core/XDMHmacData;

    .prologue
    .line 1723
    const/4 v0, 0x0

    .line 1725
    .local v0, "nRet":I
    iget-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_HMacData:Lcom/policydm/eng/core/XDMHmacData;

    if-nez v1, :cond_0

    .line 1726
    new-instance v1, Lcom/policydm/eng/core/XDMHmacData;

    invoke-direct {v1}, Lcom/policydm/eng/core/XDMHmacData;-><init>()V

    iput-object v1, p0, Lcom/policydm/tp/XTPAdapter;->m_HMacData:Lcom/policydm/eng/core/XDMHmacData;

    .line 1728
    :cond_0
    iput-object p1, p0, Lcom/policydm/tp/XTPAdapter;->m_HMacData:Lcom/policydm/eng/core/XDMHmacData;

    .line 1730
    return v0
.end method

.method public xtpAdpSetHttpObj(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)I
    .locals 11
    .param p1, "szRequest"    # Ljava/lang/String;
    .param p2, "szHmacData"    # Ljava/lang/String;
    .param p3, "szContentRange"    # Ljava/lang/String;
    .param p4, "szHttpOpenMode"    # Ljava/lang/String;
    .param p5, "appId"    # I
    .param p6, "nDownloadMode"    # Z

    .prologue
    .line 1369
    const/4 v6, 0x0

    .line 1370
    .local v6, "ret":I
    const/4 v5, 0x0

    .line 1372
    .local v5, "parser":Lcom/policydm/db/XDBUrlInfo;
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    if-nez v9, :cond_1

    .line 1373
    const/16 v6, -0xb

    .line 1463
    .end local v6    # "ret":I
    :cond_0
    :goto_0
    return v6

    .line 1375
    .restart local v6    # "ret":I
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_8

    .line 1377
    if-nez p5, :cond_7

    .line 1379
    const/4 v4, 0x0

    .local v4, "nSrcType":I
    const/4 v2, 0x0

    .line 1380
    .local v2, "nDstType":I
    const/4 v3, 0x0

    .line 1383
    .local v3, "nPortOrg":I
    invoke-static {p1}, Lcom/policydm/tp/XTPHttpUtil;->xtpURLParser(Ljava/lang/String;)Lcom/policydm/db/XDBUrlInfo;

    move-result-object v5

    .line 1384
    invoke-static/range {p5 .. p5}, Lcom/policydm/db/XDB;->xdbGetConnectType(I)I

    move-result v4

    .line 1385
    iget-object v9, v5, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-static {v9}, Lcom/policydm/tp/XTPHttpUtil;->xtpHttpExchangeProtocolType(Ljava/lang/String;)I

    move-result v2

    .line 1386
    invoke-static/range {p5 .. p5}, Lcom/policydm/db/XDB;->xdbGetServerAddress(I)Ljava/lang/String;

    move-result-object v7

    .line 1387
    .local v7, "szAddressOrg":Ljava/lang/String;
    invoke-static/range {p5 .. p5}, Lcom/policydm/db/XDB;->xdbGetServerPort(I)I

    move-result v3

    .line 1390
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1392
    const-string v9, "AddressOrg is null"

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 1393
    const/16 v6, -0xb

    goto :goto_0

    .line 1396
    :cond_2
    if-ne v4, v2, :cond_3

    iget-object v9, v5, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_3

    iget v9, v5, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    if-eq v3, v9, :cond_6

    .line 1398
    :cond_3
    const-string v8, ""

    .line 1399
    .local v8, "szURL":Ljava/lang/String;
    const/16 v9, 0x3f

    invoke-virtual {p1, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1400
    .local v1, "firstQuestion":I
    if-lez v1, :cond_5

    .line 1401
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 1404
    :goto_1
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_4

    .line 1406
    invoke-static {v8}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerUrl(Ljava/lang/String;)V

    .line 1409
    :cond_4
    iget-object v9, v5, Lcom/policydm/db/XDBUrlInfo;->pAddress:Ljava/lang/String;

    invoke-static {v9}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerAddress(Ljava/lang/String;)V

    .line 1410
    iget v9, v5, Lcom/policydm/db/XDBUrlInfo;->nPort:I

    invoke-static {v9}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerPort(I)V

    .line 1411
    iget-object v9, v5, Lcom/policydm/db/XDBUrlInfo;->pProtocol:Ljava/lang/String;

    invoke-static {v9}, Lcom/policydm/db/XDBProfileAdp;->xdbSetServerProtocol(Ljava/lang/String;)V

    .line 1412
    const/4 v6, 0x4

    goto :goto_0

    .line 1403
    :cond_5
    move-object v8, p1

    goto :goto_1

    .line 1415
    .end local v1    # "firstQuestion":I
    .end local v8    # "szURL":Ljava/lang/String;
    :cond_6
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 1417
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    sget-object v10, Lcom/policydm/tp/XTPAdapter;->m_szCookie:Ljava/lang/String;

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    .line 1421
    .end local v2    # "nDstType":I
    .end local v3    # "nPortOrg":I
    .end local v4    # "nSrcType":I
    .end local v7    # "szAddressOrg":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lcom/policydm/tp/XTPAdapter;->xtpAdpGetIsProxy()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1422
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    iput-object p1, v9, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    .line 1426
    :goto_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "requestURI = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v10, v10, p5

    iget-object v10, v10, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_PRIVATE(Ljava/lang/String;)V

    .line 1429
    :cond_8
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 1431
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    iget-object v9, v9, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    if-nez v9, :cond_9

    .line 1433
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v10

    new-array v10, v10, [B

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    .line 1435
    :cond_9
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v10

    invoke-virtual {p2, v10}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v10

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    .line 1440
    :goto_3
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 1441
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    iput-object p4, v9, Lcom/policydm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    .line 1445
    :goto_4
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_d

    .line 1446
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    iput-object p3, v9, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    .line 1450
    :goto_5
    if-eqz p6, :cond_e

    .line 1451
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    move/from16 v0, p6

    iput-boolean v0, v9, Lcom/policydm/tp/XTPHttpObj;->nDownloadMode:Z

    .line 1455
    :goto_6
    const/4 v9, 0x1

    move/from16 v0, p5

    if-ne v0, v9, :cond_0

    .line 1457
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    .line 1458
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    .line 1460
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    const-string v10, ""

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    .line 1461
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    const-string v10, "application/zip"

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    goto/16 :goto_0

    .line 1424
    :cond_a
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    invoke-static {p1}, Lcom/policydm/tp/XTPHttpUtil;->xtpHttpParsePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    goto/16 :goto_2

    .line 1438
    :cond_b
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->pHmacData:[B

    goto :goto_3

    .line 1443
    :cond_c
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    goto :goto_4

    .line 1448
    :cond_d
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    const/4 v10, 0x0

    iput-object v10, v9, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    goto :goto_5

    .line 1453
    :cond_e
    sget-object v9, Lcom/policydm/tp/XTPAdapter;->g_HttpObj:[Lcom/policydm/tp/XTPHttpObj;

    aget-object v9, v9, p5

    const/4 v10, 0x0

    iput-boolean v10, v9, Lcom/policydm/tp/XTPHttpObj;->nDownloadMode:Z

    goto :goto_6
.end method

.method public xtpAdpSetIsProxy(Z)V
    .locals 0
    .param p1, "isProxy"    # Z

    .prologue
    .line 1652
    iput-boolean p1, p0, Lcom/policydm/tp/XTPAdapter;->m_bIsProxyServer:Z

    .line 1653
    return-void
.end method
