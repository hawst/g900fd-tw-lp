.class public Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;
.super Ljava/util/TimerTask;
.source "XTPNetRecvTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/tp/XTPNetRecvTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XTPHttpRecvTimerTask"
.end annotation


# instance fields
.field private isCloseTimer:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;->isCloseTimer:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 68
    # getter for: Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->access$000()I

    move-result v0

    const/16 v1, 0x46

    if-lt v0, v1, :cond_1

    .line 70
    const/4 v0, 0x0

    # setter for: Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I
    invoke-static {v0}, Lcom/policydm/tp/XTPNetRecvTimer;->access$002(I)I

    .line 71
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvEndTimer()V

    .line 72
    const-string v0, "===Receive Fail==="

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 73
    # getter for: Lcom/policydm/tp/XTPNetRecvTimer;->m_nAppId:I
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->access$100()I

    move-result v0

    if-nez v0, :cond_0

    .line 74
    sget-object v0, Lcom/policydm/XDMApplication;->g_Task:Lcom/policydm/agent/XDMTask;

    invoke-virtual {v0}, Lcom/policydm/agent/XDMTask;->xdmAgentDmXXXFail()V

    .line 85
    :goto_0
    return-void

    .line 76
    :cond_0
    sget-object v0, Lcom/policydm/XDMApplication;->g_Task:Lcom/policydm/agent/XDMTask;

    invoke-virtual {v0}, Lcom/policydm/agent/XDMTask;->xdmAgentDlXXXFail()V

    goto :goto_0

    .line 80
    :cond_1
    const-string v0, "============================="

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "== recv Timer["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    # getter for: Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->access$000()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 82
    const-string v0, "============================="

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 84
    # operator++ for: Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->access$008()I

    goto :goto_0
.end method

.method public xtpHttpRecvGetCloseTimer()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;->isCloseTimer:Z

    return v0
.end method

.method public xtpHttpRecvSetCloseTimer(Z)V
    .locals 0
    .param p1, "isclose"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;->isCloseTimer:Z

    .line 95
    return-void
.end method
