.class public Lcom/policydm/tp/XTPNetSendTimer$XTPHttpSendTimerTask;
.super Ljava/util/TimerTask;
.source "XTPNetSendTimer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/tp/XTPNetSendTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XTPHttpSendTimerTask"
.end annotation


# instance fields
.field private isCloseTimer:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/policydm/tp/XTPNetSendTimer$XTPHttpSendTimerTask;->isCloseTimer:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 67
    # getter for: Lcom/policydm/tp/XTPNetSendTimer;->m_nSendCount:I
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->access$000()I

    move-result v0

    const/16 v1, 0x46

    if-lt v0, v1, :cond_1

    .line 69
    const/4 v0, 0x0

    # setter for: Lcom/policydm/tp/XTPNetSendTimer;->m_nSendCount:I
    invoke-static {v0}, Lcom/policydm/tp/XTPNetSendTimer;->access$002(I)I

    .line 70
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 71
    const-string v0, "===Send Fail==="

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 72
    # getter for: Lcom/policydm/tp/XTPNetSendTimer;->m_nAppId:I
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->access$100()I

    move-result v0

    if-nez v0, :cond_0

    .line 73
    sget-object v0, Lcom/policydm/XDMApplication;->g_Task:Lcom/policydm/agent/XDMTask;

    invoke-virtual {v0}, Lcom/policydm/agent/XDMTask;->xdmAgentDmXXXFail()V

    .line 84
    :goto_0
    return-void

    .line 75
    :cond_0
    sget-object v0, Lcom/policydm/XDMApplication;->g_Task:Lcom/policydm/agent/XDMTask;

    invoke-virtual {v0}, Lcom/policydm/agent/XDMTask;->xdmAgentDlXXXFail()V

    goto :goto_0

    .line 79
    :cond_1
    const-string v0, "============================="

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "== send Timer["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    # getter for: Lcom/policydm/tp/XTPNetSendTimer;->m_nSendCount:I
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->access$000()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 81
    const-string v0, "============================="

    invoke-static {v0}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 83
    # operator++ for: Lcom/policydm/tp/XTPNetSendTimer;->m_nSendCount:I
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->access$008()I

    goto :goto_0
.end method

.method public xtpGetCloseTimer()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/policydm/tp/XTPNetSendTimer$XTPHttpSendTimerTask;->isCloseTimer:Z

    return v0
.end method

.method public xtpSetCloseTimer(Z)V
    .locals 0
    .param p1, "isclose"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/policydm/tp/XTPNetSendTimer$XTPHttpSendTimerTask;->isCloseTimer:Z

    .line 94
    return-void
.end method
