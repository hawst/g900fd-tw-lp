.class public Lcom/policydm/tp/XTPHttpObj;
.super Ljava/lang/Object;
.source "XTPHttpObj.java"


# instance fields
.field public appId:I

.field public eCode:I

.field public m_szContentRange:Ljava/lang/String;

.field public m_szHttpAccept:Ljava/lang/String;

.field public m_szHttpConnection:Ljava/lang/String;

.field public m_szHttpHost:Ljava/lang/String;

.field public m_szHttpMimeType:Ljava/lang/String;

.field public m_szHttpOpenMode:Ljava/lang/String;

.field public m_szHttpUserAgent:Ljava/lang/String;

.field public m_szHttpVersion:Ljava/lang/String;

.field public m_szHttpcookie:Ljava/lang/String;

.field public m_szProxyAddr:Ljava/lang/String;

.field public m_szRequestUri:Ljava/lang/String;

.field public m_szServerAddr:Ljava/lang/String;

.field public m_szServerURL:Ljava/lang/String;

.field public nContentLength:I

.field public nDownloadMode:Z

.field public nHeaderLength:I

.field public nHttpConnection:I

.field public nHttpReturnStatusValue:I

.field public nPort:I

.field public nServerPort:I

.field public nTransferCoding:I

.field public nTunnelConnected:I

.field public nTunnelMode:I

.field public pChunkBuffer:[B

.field public pHmacData:[B

.field public pReceiveBuffer:[B

.field public protocol:I

.field public proxyPort:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szServerURL:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szProxyAddr:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szServerAddr:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpOpenMode:Ljava/lang/String;

    .line 53
    iput v1, p0, Lcom/policydm/tp/XTPHttpObj;->nHttpConnection:I

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpConnection:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpMimeType:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpVersion:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpUserAgent:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpHost:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpAccept:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szRequestUri:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szContentRange:Ljava/lang/String;

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/policydm/tp/XTPHttpObj;->m_szHttpcookie:Ljava/lang/String;

    .line 64
    iput v1, p0, Lcom/policydm/tp/XTPHttpObj;->nHttpReturnStatusValue:I

    .line 65
    iput v1, p0, Lcom/policydm/tp/XTPHttpObj;->nContentLength:I

    .line 66
    iput v1, p0, Lcom/policydm/tp/XTPHttpObj;->nTransferCoding:I

    .line 67
    return-void
.end method
