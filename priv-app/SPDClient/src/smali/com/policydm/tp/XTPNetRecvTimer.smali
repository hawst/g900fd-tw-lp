.class public Lcom/policydm/tp/XTPNetRecvTimer;
.super Ljava/lang/Object;
.source "XTPNetRecvTimer.java"

# interfaces
.implements Lcom/policydm/interfaces/XDMInterface;
.implements Lcom/policydm/interfaces/XTPInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;
    }
.end annotation


# static fields
.field private static final XTP_RECEIVE_TIMER:I = 0x46

.field private static m_RecvTimer:Ljava/util/Timer;

.field private static m_TpRecvTimer:Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

.field private static m_nAppId:I

.field private static m_nRecvCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 14
    sput-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    .line 15
    sput-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    .line 16
    sput v0, Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    .line 17
    sput v0, Lcom/policydm/tp/XTPNetRecvTimer;->m_nAppId:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "appId"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    invoke-direct {v0}, Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;-><init>()V

    sput-object v0, Lcom/policydm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    .line 24
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/policydm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    .line 25
    invoke-static {}, Lcom/policydm/tp/XTPNetRecvTimer;->xtpNetRecvStartTimer()V

    .line 27
    sput p1, Lcom/policydm/tp/XTPNetRecvTimer;->m_nAppId:I

    .line 28
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 12
    sget v0, Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 12
    sput p0, Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    return p0
.end method

.method static synthetic access$008()I
    .locals 2

    .prologue
    .line 12
    sget v0, Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 12
    sget v0, Lcom/policydm/tp/XTPNetRecvTimer;->m_nAppId:I

    return v0
.end method

.method public static xtpNetRecvEndTimer()V
    .locals 2

    .prologue
    .line 39
    const/4 v1, 0x0

    :try_start_0
    sput v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_nRecvCount:I

    .line 40
    const/4 v1, 0x0

    sput v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_nAppId:I

    .line 42
    sget-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    if-nez v1, :cond_1

    .line 59
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void

    .line 45
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v1, "=====================>> endTimer(recv)"

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG(Ljava/lang/String;)V

    .line 46
    sget-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 47
    sget-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    invoke-virtual {v1}, Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;->cancel()Z

    .line 49
    const/4 v1, 0x0

    sput-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    .line 50
    const/4 v1, 0x0

    sput-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    .line 52
    invoke-static {}, Lcom/policydm/tp/XTPNetSendTimer;->xtpEndTimer()V

    .line 53
    invoke-static {}, Lcom/policydm/tp/XTPNetConnectTimer;->xtpNetConnEndTimer()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 57
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static xtpNetRecvStartTimer()V
    .locals 5

    .prologue
    .line 32
    sget-object v0, Lcom/policydm/tp/XTPNetRecvTimer;->m_RecvTimer:Ljava/util/Timer;

    sget-object v1, Lcom/policydm/tp/XTPNetRecvTimer;->m_TpRecvTimer:Lcom/policydm/tp/XTPNetRecvTimer$XTPHttpRecvTimerTask;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;Ljava/util/Date;J)V

    .line 33
    return-void
.end method
