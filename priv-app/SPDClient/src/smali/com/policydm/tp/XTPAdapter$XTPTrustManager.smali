.class public Lcom/policydm/tp/XTPAdapter$XTPTrustManager;
.super Ljava/lang/Object;
.source "XTPAdapter.java"

# interfaces
.implements Ljavax/net/ssl/X509TrustManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/policydm/tp/XTPAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "XTPTrustManager"
.end annotation


# instance fields
.field private trustManager:Ljavax/net/ssl/X509TrustManager;


# direct methods
.method constructor <init>(Ljava/security/KeyStore;)V
    .locals 4
    .param p1, "localTrustStore"    # Ljava/security/KeyStore;

    .prologue
    .line 1935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1938
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v1

    .line 1939
    .local v1, "tmf":Ljavax/net/ssl/TrustManagerFactory;
    invoke-virtual {v1, p1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 1940
    invoke-direct {p0, v1}, Lcom/policydm/tp/XTPAdapter$XTPTrustManager;->getX509TrustManager(Ljavax/net/ssl/TrustManagerFactory;)Ljavax/net/ssl/X509TrustManager;

    move-result-object v2

    iput-object v2, p0, Lcom/policydm/tp/XTPAdapter$XTPTrustManager;->trustManager:Ljavax/net/ssl/X509TrustManager;

    .line 1942
    iget-object v2, p0, Lcom/policydm/tp/XTPAdapter$XTPTrustManager;->trustManager:Ljavax/net/ssl/X509TrustManager;

    if-nez v2, :cond_0

    .line 1944
    const-string v2, "X509TrustManager is null"

    invoke-static {v2}, Lcom/policydm/agent/XDMDebug;->XDM_DEBUG_EXCEPTION(Ljava/lang/String;)V

    .line 1945
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "X509TrustManager is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1948
    .end local v1    # "tmf":Ljavax/net/ssl/TrustManagerFactory;
    :catch_0
    move-exception v0

    .line 1950
    .local v0, "e":Ljava/security/GeneralSecurityException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 1952
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    .restart local v1    # "tmf":Ljavax/net/ssl/TrustManagerFactory;
    :cond_0
    return-void
.end method

.method private getX509TrustManager(Ljavax/net/ssl/TrustManagerFactory;)Ljavax/net/ssl/X509TrustManager;
    .locals 3
    .param p1, "tmf"    # Ljavax/net/ssl/TrustManagerFactory;

    .prologue
    .line 1971
    invoke-virtual {p1}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v1

    .line 1972
    .local v1, "tms":[Ljavax/net/ssl/TrustManager;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1974
    aget-object v2, v1, v0

    instance-of v2, v2, Ljavax/net/ssl/X509TrustManager;

    if-eqz v2, :cond_0

    .line 1976
    aget-object v2, v1, v0

    check-cast v2, Ljavax/net/ssl/X509TrustManager;

    .line 1979
    :goto_1
    return-object v2

    .line 1972
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1979
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1
    .param p1, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p2, "authType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 1956
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter$XTPTrustManager;->trustManager:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v0, p1, p2}, Ljavax/net/ssl/X509TrustManager;->checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 1957
    return-void
.end method

.method public checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1
    .param p1, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p2, "authType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter$XTPTrustManager;->trustManager:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v0, p1, p2}, Ljavax/net/ssl/X509TrustManager;->checkServerTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 1962
    return-void
.end method

.method public getAcceptedIssuers()[Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 1966
    iget-object v0, p0, Lcom/policydm/tp/XTPAdapter$XTPTrustManager;->trustManager:Ljavax/net/ssl/X509TrustManager;

    invoke-interface {v0}, Ljavax/net/ssl/X509TrustManager;->getAcceptedIssuers()[Ljava/security/cert/X509Certificate;

    move-result-object v0

    return-object v0
.end method
