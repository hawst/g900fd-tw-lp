.class Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;
.super Ljava/util/TimerTask;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/stub/paywithpaypal/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProgressTimeTask"
.end annotation


# instance fields
.field mPkgName:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)V
    .locals 0
    .param p2, "DownloadPkgName"    # Ljava/lang/String;

    .prologue
    .line 889
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 890
    iput-object p2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;->mPkgName:Ljava/lang/String;

    .line 891
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 895
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 896
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->MESSAGE_ID:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;->mPkgName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    const-string v2, "MESSAGE_CMD"

    const-string v3, "DOWNLODING_PROGRESS_UPDATE"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 900
    .local v1, "message":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 901
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$4200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 902
    return-void
.end method
