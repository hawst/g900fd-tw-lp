.class public Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
.super Landroid/os/AsyncTask;
.source "UpdateDownloadTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field private static final CTC_SERVER_URL:Ljava/lang/String; = "http://cn-ms.samsungapps.com/getCNVasURL.as"

.field private static final DOWNLOAD_CHECK_URL:Ljava/lang/String; = "https://vas.samsungapps.com/stub/stubDownload.as"

.field private static final PD_TEST_PATH:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "StubAppActivation"

.field private static mFirstDownload:Z

.field private static mRequestInstallation:Z


# instance fields
.field private MESSAGE_DATA_URI:Ljava/lang/String;

.field private MESSAGE_ID:Ljava/lang/String;

.field private MESSAGE_RESULT:Ljava/lang/String;

.field private _HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

.field private _Handler:Landroid/os/Handler;

.field private mAppApkName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDownloadUri:Ljava/lang/String;

.field private mDownloadUriCTC:Ljava/lang/String;

.field private mFlagCancel:Z

.field private mHandler:Landroid/os/Handler;

.field private mID:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mResult:Ljava/lang/String;

.field private mSignature:Ljava/lang/String;

.field private mStartFromFirst:Z

.field private mUpdateUriCTC:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFirstDownload:Z

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/go_to_andromeda.test"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->PD_TEST_PATH:Ljava/lang/String;

    .line 141
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mRequestInstallation:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "id"    # Ljava/lang/String;
    .param p4, "result"    # Ljava/lang/String;
    .param p5, "downloadUri"    # Ljava/lang/String;
    .param p6, "startFromFirst"    # Ljava/lang/Boolean;
    .param p7, "apkName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 157
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 96
    const-string v0, "id of the message"

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->MESSAGE_ID:Ljava/lang/String;

    .line 100
    const-string v0, "result of the message"

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->MESSAGE_RESULT:Ljava/lang/String;

    .line 104
    const-string v0, "uri of the message"

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->MESSAGE_DATA_URI:Ljava/lang/String;

    .line 108
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_Handler:Landroid/os/Handler;

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mStartFromFirst:Z

    .line 120
    iput-boolean v1, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFlagCancel:Z

    .line 124
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mID:Ljava/lang/String;

    .line 128
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUri:Ljava/lang/String;

    .line 130
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mSignature:Ljava/lang/String;

    .line 132
    iput-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mUpdateUriCTC:Ljava/lang/String;

    .line 134
    iput-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUriCTC:Ljava/lang/String;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mResult:Ljava/lang/String;

    .line 159
    const-string v0, "StubAppActivation"

    const-string v1, "UpdateDownloadTask()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    .line 165
    iput-object p3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mPackageName:Ljava/lang/String;

    .line 169
    iput-object p7, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mAppApkName:Ljava/lang/String;

    .line 173
    new-instance v0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mAppApkName:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    .line 175
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->prepare()Z

    .line 177
    iput-object p2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    .line 179
    iput-object p3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mID:Ljava/lang/String;

    .line 181
    iput-object p5, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUri:Ljava/lang/String;

    .line 183
    iput-object p4, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mResult:Ljava/lang/String;

    .line 185
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mStartFromFirst:Z

    .line 187
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)Lcom/sec/android/stub/paywithpaypal/HFileUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->installApk()Z

    move-result v0

    return v0
.end method

.method private checkApkSignature(Ljava/lang/String;)Z
    .locals 3
    .param p1, "apkFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1220
    const/4 v1, 0x0

    .line 1221
    .local v1, "result":Z
    new-instance v0, Lcom/sec/android/stub/paywithpaypal/SigChecker;

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/stub/paywithpaypal/SigChecker;-><init>(Landroid/content/Context;)V

    .line 1222
    .local v0, "c":Lcom/sec/android/stub/paywithpaypal/SigChecker;
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mSignature:Ljava/lang/String;

    invoke-virtual {v0, p1, v2}, Lcom/sec/android/stub/paywithpaypal/SigChecker;->validate(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1224
    const/4 v1, 0x1

    .line 1227
    :cond_0
    return v1
.end method

.method private checkDownload()Z
    .locals 34

    .prologue
    .line 279
    sget-object v25, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 281
    .local v25, "szModel":Ljava/lang/String;
    const-string v26, "SAMSUNG-"

    .line 284
    .local v26, "szPrefix":Ljava/lang/String;
    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-eqz v31, :cond_0

    .line 286
    const-string v31, ""

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 294
    :cond_0
    const-string v21, ""

    .line 296
    .local v21, "szMCC":Ljava/lang/String;
    const-string v24, ""

    .line 310
    .local v24, "szMNC":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    const-string v32, "connectivity"

    invoke-virtual/range {v31 .. v32}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    .line 314
    .local v6, "cManager":Landroid/net/ConnectivityManager;
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v15

    .line 316
    .local v15, "mobile":Landroid/net/NetworkInfo;
    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v30

    .line 318
    .local v30, "wifi":Landroid/net/NetworkInfo;
    const/16 v31, 0x7

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v5

    .line 320
    .local v5, "bluetooth":Landroid/net/NetworkInfo;
    const/16 v31, 0x9

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v9

    .line 326
    .local v9, "ethernet":Landroid/net/NetworkInfo;
    if-eqz v30, :cond_9

    invoke-virtual/range {v30 .. v30}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v31

    if-eqz v31, :cond_9

    .line 328
    const-string v21, "505"

    .line 330
    const-string v24, "00"

    .line 366
    :goto_0
    const-string v29, "https://vas.samsungapps.com/stub/stubDownload.as"

    .line 369
    .local v29, "url":Ljava/lang/String;
    const-string v31, "460"

    move-object/from16 v0, v31

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_1

    const-string v31, "03"

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_1

    .line 371
    const-string v31, "StubAppActivation"

    const-string v32, "checkDownload : CTC network "

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    const-string v31, "http://cn-ms.samsungapps.com/getCNVasURL.as"

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->getUrlForCTC(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_1

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUriCTC:Ljava/lang/String;

    move-object/from16 v31, v0

    if-eqz v31, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUriCTC:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->isEmpty()Z

    move-result v31

    if-nez v31, :cond_d

    .line 376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUriCTC:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 378
    const-string v31, "StubAppActivation"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "checkDownload : url = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_1
    :goto_1
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "?appId="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mPackageName:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 393
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&encImei="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->getIMEI()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 395
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&deviceId="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 397
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&mcc="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v31, "1"

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->getPD()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v31

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_e

    const-string v31, "000"

    :goto_2
    move-object/from16 v0, v32

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 399
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&mnc="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 401
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&csc="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->getCSC()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 403
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&sdkVer="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    sget v32, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static/range {v32 .. v32}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 405
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&pd="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->getPD()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 414
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v10

    .line 416
    .local v10, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v10}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v16

    .line 420
    .local v16, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v11, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v11}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 446
    .local v11, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v12, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-direct {v12, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7

    .line 450
    .local v12, "httpget":Lorg/apache/http/client/methods/HttpGet;
    const/16 v18, 0x0

    .line 454
    .local v18, "response":Lorg/apache/http/HttpResponse;
    :try_start_1
    invoke-interface {v11, v12}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v18

    .line 458
    const-string v31, "Praeda"

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7

    move-result-object v8

    .line 470
    .local v8, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v8, :cond_2

    .line 478
    :try_start_2
    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v14

    .line 482
    .local v14, "instream":Ljava/io/InputStream;
    const/16 v31, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v31

    invoke-interface {v0, v14, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_6

    .line 506
    .end local v14    # "instream":Ljava/io/InputStream;
    :cond_2
    :goto_3
    :try_start_3
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v17

    .line 510
    .local v17, "parserEvent":I
    const-string v13, ""

    .local v13, "id":Ljava/lang/String;
    const-string v19, ""

    .line 512
    .local v19, "result":Ljava/lang/String;
    const-string v4, ""

    .line 514
    .local v4, "DownloadURI":Ljava/lang/String;
    const-string v20, ""

    .line 520
    .local v20, "signature":Ljava/lang/String;
    :goto_4
    const/16 v31, 0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-eq v0, v1, :cond_f

    .line 522
    const/16 v31, 0x2

    move/from16 v0, v17

    move/from16 v1, v31

    if-ne v0, v1, :cond_7

    .line 524
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v27

    .line 526
    .local v27, "tag":Ljava/lang/String;
    const-string v31, "appId"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_3

    .line 528
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v28

    .line 530
    .local v28, "type":I
    const/16 v31, 0x4

    move/from16 v0, v28

    move/from16 v1, v31

    if-ne v0, v1, :cond_3

    .line 532
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v13

    .line 538
    .end local v28    # "type":I
    :cond_3
    const-string v31, "resultCode"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 540
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v28

    .line 542
    .restart local v28    # "type":I
    const/16 v31, 0x4

    move/from16 v0, v28

    move/from16 v1, v31

    if-ne v0, v1, :cond_4

    .line 544
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v19

    .line 550
    .end local v28    # "type":I
    :cond_4
    const-string v31, "downloadURI"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_5

    .line 552
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v28

    .line 554
    .restart local v28    # "type":I
    const/16 v31, 0x4

    move/from16 v0, v28

    move/from16 v1, v31

    if-ne v0, v1, :cond_5

    .line 556
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v4

    .line 562
    .end local v28    # "type":I
    :cond_5
    const-string v31, "contentSize"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_6

    .line 564
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v28

    .line 566
    .restart local v28    # "type":I
    const/16 v31, 0x4

    move/from16 v0, v28

    move/from16 v1, v31

    if-ne v0, v1, :cond_6

    .line 574
    .end local v28    # "type":I
    :cond_6
    const-string v31, "signature"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_7

    .line 576
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v28

    .line 578
    .restart local v28    # "type":I
    const/16 v31, 0x4

    move/from16 v0, v28

    move/from16 v1, v31

    if-ne v0, v1, :cond_7

    .line 580
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v20

    .line 582
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mSignature:Ljava/lang/String;

    .line 591
    .end local v27    # "tag":Ljava/lang/String;
    .end local v28    # "type":I
    :cond_7
    const/16 v31, 0x3

    move/from16 v0, v17

    move/from16 v1, v31

    if-ne v0, v1, :cond_8

    .line 593
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v27

    .line 595
    .restart local v27    # "tag":Ljava/lang/String;
    const-string v31, "downloadURI"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_8

    .line 597
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mID:Ljava/lang/String;

    .line 599
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mResult:Ljava/lang/String;

    .line 601
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUri:Ljava/lang/String;

    .line 603
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mSignature:Ljava/lang/String;

    .line 625
    .end local v27    # "tag":Ljava/lang/String;
    :cond_8
    invoke-interface/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    move-result v17

    goto/16 :goto_4

    .line 332
    .end local v4    # "DownloadURI":Ljava/lang/String;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v13    # "id":Ljava/lang/String;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v17    # "parserEvent":I
    .end local v18    # "response":Lorg/apache/http/HttpResponse;
    .end local v19    # "result":Ljava/lang/String;
    .end local v20    # "signature":Ljava/lang/String;
    .end local v29    # "url":Ljava/lang/String;
    :cond_9
    if-eqz v15, :cond_a

    invoke-virtual {v15}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v31

    if-eqz v31, :cond_a

    .line 334
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->getMCC()Ljava/lang/String;

    move-result-object v21

    .line 336
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->getMNC()Ljava/lang/String;

    move-result-object v24

    .line 338
    const-string v31, "StubAppActivation"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "getResultUpdateCheck() : szMCC = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " szMNC = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 340
    :cond_a
    if-eqz v5, :cond_b

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v31

    if-eqz v31, :cond_b

    .line 342
    const-string v21, "505"

    .line 344
    const-string v24, "00"

    .line 346
    const-string v31, "StubAppActivation"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, " run() : szMCC = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " szMNC = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 348
    :cond_b
    if-eqz v9, :cond_c

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v31

    if-eqz v31, :cond_c

    .line 350
    const-string v21, "505"

    .line 352
    const-string v24, "00"

    .line 354
    const-string v31, "StubAppActivation"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, " run() : szMCC = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " szMNC = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 358
    :cond_c
    const-string v31, "StubAppActivation"

    const-string v32, "Connection failed"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const/16 v31, 0x0

    .line 693
    :goto_5
    return v31

    .line 382
    .restart local v29    # "url":Ljava/lang/String;
    :cond_d
    const-string v29, "https://vas.samsungapps.com/stub/stubDownload.as"

    .line 384
    const-string v31, "StubAppActivation"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "checkDownload : url = "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_e
    move-object/from16 v31, v21

    .line 397
    goto/16 :goto_2

    .line 484
    .restart local v8    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .restart local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v18    # "response":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v7

    .line 486
    .local v7, "e":Ljava/lang/IllegalStateException;
    :try_start_4
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    goto/16 :goto_3

    .line 496
    .end local v7    # "e":Ljava/lang/IllegalStateException;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    :catch_1
    move-exception v7

    .line 498
    .local v7, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    .line 500
    const/16 v31, 0x0

    goto :goto_5

    .line 488
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v8    # "entity":Lorg/apache/http/HttpEntity;
    :catch_2
    move-exception v7

    .line 490
    .local v7, "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    goto/16 :goto_3

    .line 659
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v18    # "response":Lorg/apache/http/HttpResponse;
    :catch_3
    move-exception v7

    .line 661
    .local v7, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v31, "StubAppActivation"

    const-string v32, "xml parsing error"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 665
    const/16 v31, 0x0

    goto :goto_5

    .line 629
    .end local v7    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v4    # "DownloadURI":Ljava/lang/String;
    .restart local v8    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .restart local v13    # "id":Ljava/lang/String;
    .restart local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v17    # "parserEvent":I
    .restart local v18    # "response":Lorg/apache/http/HttpResponse;
    .restart local v19    # "result":Ljava/lang/String;
    .restart local v20    # "signature":Ljava/lang/String;
    :cond_f
    :try_start_7
    const-string v31, "0"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_10

    .line 630
    const/16 v31, 0x0

    goto :goto_5

    .line 631
    :cond_10
    new-instance v12, Lorg/apache/http/client/methods/HttpGet;

    .end local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    invoke-direct {v12, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    .line 635
    .restart local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    :try_start_8
    invoke-interface {v11, v12}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/net/SocketException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    move-result-object v18

    .line 645
    :goto_6
    const-wide/16 v22, 0x0

    .line 647
    .local v22, "sizeTotal":D
    if-eqz v18, :cond_11

    .line 649
    :try_start_9
    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-double v0, v0

    move-wide/from16 v22, v0

    .line 653
    :cond_11
    const-string v31, "APK_SIZE"

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-wide/from16 v2, v22

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;D)V

    .line 693
    .end local v4    # "DownloadURI":Ljava/lang/String;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v13    # "id":Ljava/lang/String;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v17    # "parserEvent":I
    .end local v18    # "response":Lorg/apache/http/HttpResponse;
    .end local v19    # "result":Ljava/lang/String;
    .end local v20    # "signature":Ljava/lang/String;
    .end local v22    # "sizeTotal":D
    :goto_7
    const/16 v31, 0x1

    goto :goto_5

    .line 637
    .restart local v4    # "DownloadURI":Ljava/lang/String;
    .restart local v8    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .restart local v13    # "id":Ljava/lang/String;
    .restart local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v17    # "parserEvent":I
    .restart local v18    # "response":Lorg/apache/http/HttpResponse;
    .restart local v19    # "result":Ljava/lang/String;
    .restart local v20    # "signature":Ljava/lang/String;
    :catch_4
    move-exception v7

    .line 639
    .local v7, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_9
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_6

    .line 667
    .end local v4    # "DownloadURI":Ljava/lang/String;
    .end local v7    # "e":Ljava/lang/IllegalStateException;
    .end local v8    # "entity":Lorg/apache/http/HttpEntity;
    .end local v10    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v11    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v12    # "httpget":Lorg/apache/http/client/methods/HttpGet;
    .end local v13    # "id":Ljava/lang/String;
    .end local v16    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v17    # "parserEvent":I
    .end local v18    # "response":Lorg/apache/http/HttpResponse;
    .end local v19    # "result":Ljava/lang/String;
    .end local v20    # "signature":Ljava/lang/String;
    :catch_5
    move-exception v7

    .line 669
    .local v7, "e":Ljava/net/SocketException;
    invoke-virtual {v7}, Ljava/net/SocketException;->printStackTrace()V

    .line 671
    const-string v31, "StubAppActivation"

    const-string v32, "network is unavailable"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    const/16 v31, 0x0

    goto/16 :goto_5

    .line 675
    .end local v7    # "e":Ljava/net/SocketException;
    :catch_6
    move-exception v7

    .line 677
    .local v7, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v7}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 679
    const-string v31, "StubAppActivation"

    const-string v32, "server is not response"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 681
    .end local v7    # "e":Ljava/net/UnknownHostException;
    :catch_7
    move-exception v7

    .line 683
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    .line 685
    const-string v31, "StubAppActivation"

    const-string v32, "network error"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    const/16 v31, 0x0

    goto/16 :goto_5
.end method

.method private downloadApk(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 22
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "URI"    # Ljava/lang/String;

    .prologue
    .line 700
    const-string v13, "StubAppActivation"

    const-string v18, "downloadApk()"

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    const-string v13, "1"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    const-string v13, ""

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_9

    .line 704
    const/4 v8, 0x0

    .line 706
    .local v8, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 708
    .local v5, "InStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 710
    .local v4, "Fout":Ljava/io/FileOutputStream;
    new-instance v10, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v10}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 712
    .local v10, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v9, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v9}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 716
    .local v9, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    invoke-virtual {v13}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v8

    .line 720
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 722
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mStartFromFirst:Z

    if-eqz v13, :cond_2

    .line 724
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 726
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    invoke-virtual {v13}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v8

    .line 728
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mStartFromFirst:Z

    .line 744
    :cond_0
    :goto_0
    const-wide/16 v16, 0x0

    .line 746
    .local v16, "sizeTotal":D
    :try_start_0
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v14

    .line 750
    .local v14, "sizeDownload":J
    new-instance v13, Ljava/net/URI;

    move-object/from16 v0, p3

    invoke-direct {v13, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v13}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 752
    invoke-interface {v10, v9}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 754
    .local v12, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 758
    sget-boolean v13, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFirstDownload:Z

    if-eqz v13, :cond_3

    .line 760
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 762
    const-string v13, "StubAppActivation"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "1st sizeTotal : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v18

    const v19, 0x8001

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 782
    const/16 v13, 0x400

    new-array v6, v13, [B

    .line 784
    .local v6, "buffer":[B
    const/4 v11, 0x0

    .line 792
    .local v11, "len1":I
    :goto_2
    invoke-virtual {v5, v6}, Ljava/io/InputStream;->read([B)I

    move-result v11

    if-lez v11, :cond_7

    .line 794
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFlagCancel:Z

    if-eqz v13, :cond_4

    .line 796
    const-string v13, "StubAppActivation"

    const-string v18, "CANCELLED(mFlagCancel)"

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 804
    const/4 v13, 0x0

    .line 844
    if-eqz v4, :cond_1

    .line 846
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 878
    .end local v4    # "Fout":Ljava/io/FileOutputStream;
    .end local v5    # "InStream":Ljava/io/InputStream;
    .end local v6    # "buffer":[B
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v11    # "len1":I
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sizeDownload":J
    .end local v16    # "sizeTotal":D
    :cond_1
    :goto_3
    return v13

    .line 732
    .restart local v4    # "Fout":Ljava/io/FileOutputStream;
    .restart local v5    # "InStream":Ljava/io/InputStream;
    .restart local v8    # "file":Ljava/io/File;
    .restart local v9    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .restart local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    :cond_2
    const-string v13, "Range"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "bytes="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v13, v0}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    const/4 v13, 0x0

    sput-boolean v13, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFirstDownload:Z

    goto/16 :goto_0

    .line 766
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sizeDownload":J
    .restart local v16    # "sizeTotal":D
    :cond_3
    :try_start_2
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v18

    add-long v18, v18, v14

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v16, v0

    .line 768
    const-string v13, "StubAppActivation"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "sizeTotal : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " sizeDownload : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 828
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sizeDownload":J
    :catch_0
    move-exception v7

    .line 830
    .local v7, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 832
    const-string v13, "StubAppActivation"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Download fail - "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    const-string v13, "DOWNLOAD_STATUS"

    const/16 v18, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 838
    const/4 v13, 0x0

    .line 844
    if-eqz v4, :cond_1

    .line 846
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_3

    .line 848
    :catch_1
    move-exception v7

    .line 850
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 848
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v6    # "buffer":[B
    .restart local v11    # "len1":I
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sizeDownload":J
    :catch_2
    move-exception v7

    .line 850
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 810
    .end local v7    # "e":Ljava/io/IOException;
    :cond_4
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->isCancelled()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v13

    if-eqz v13, :cond_5

    .line 812
    const/4 v13, 0x0

    .line 844
    if-eqz v4, :cond_1

    .line 846
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_3

    .line 848
    :catch_3
    move-exception v7

    .line 850
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 816
    .end local v7    # "e":Ljava/io/IOException;
    :cond_5
    const/4 v13, 0x0

    :try_start_7
    invoke-virtual {v4, v6, v13, v11}, Ljava/io/FileOutputStream;->write([BII)V

    .line 820
    int-to-long v0, v11

    move-wide/from16 v18, v0

    add-long v14, v14, v18

    .line 822
    const-string v13, "DOWNLOA_RATIO"

    const-wide/16 v18, 0x64

    mul-long v18, v18, v14

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v18, v18, v16

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 842
    .end local v6    # "buffer":[B
    .end local v11    # "len1":I
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sizeDownload":J
    :catchall_0
    move-exception v13

    .line 844
    if-eqz v4, :cond_6

    .line 846
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 852
    :cond_6
    :goto_4
    throw v13

    .line 826
    .restart local v6    # "buffer":[B
    .restart local v11    # "len1":I
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v14    # "sizeDownload":J
    :cond_7
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 844
    if-eqz v4, :cond_8

    .line 846
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 870
    :cond_8
    :goto_5
    const-string v13, "StubAppActivation"

    const-string v18, "Download complete."

    move-object/from16 v0, v18

    invoke-static {v13, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    const-string v13, "DOWNLOAD_STATUS"

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v13, v1}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 874
    const/4 v13, 0x1

    goto/16 :goto_3

    .line 848
    :catch_4
    move-exception v7

    .line 850
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 848
    .end local v6    # "buffer":[B
    .end local v7    # "e":Ljava/io/IOException;
    .end local v11    # "len1":I
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v14    # "sizeDownload":J
    :catch_5
    move-exception v7

    .line 850
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 878
    .end local v4    # "Fout":Ljava/io/FileOutputStream;
    .end local v5    # "InStream":Ljava/io/InputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v10    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v16    # "sizeTotal":D
    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_3
.end method

.method private downloadApkNoProxy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 26
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "URI"    # Ljava/lang/String;

    .prologue
    .line 886
    const-string v21, "StubAppActivation"

    const-string v22, "downloadApkNoProxy()"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    const-string v21, "1"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    const-string v21, ""

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_b

    .line 890
    const/4 v9, 0x0

    .line 892
    .local v9, "file":Ljava/io/File;
    const/4 v5, 0x0

    .line 894
    .local v5, "InStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 896
    .local v4, "Fout":Ljava/io/FileOutputStream;
    new-instance v12, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v12}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 898
    .local v12, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v10, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v10}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 904
    .local v10, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    const/16 v15, 0x78

    .line 906
    .local v15, "timeout":I
    invoke-interface {v12}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    .line 908
    .local v11, "httpParams":Lorg/apache/http/params/HttpParams;
    const v21, 0x1d4c0

    move/from16 v0, v21

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 910
    const v21, 0x1d4c0

    move/from16 v0, v21

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 914
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v9

    .line 918
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 920
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mStartFromFirst:Z

    move/from16 v21, v0

    if-eqz v21, :cond_3

    .line 922
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 924
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v9

    .line 926
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mStartFromFirst:Z

    .line 944
    :cond_0
    :goto_0
    const-wide/16 v18, 0x0

    .line 946
    .local v18, "sizeTotal":D
    :try_start_0
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v16

    .line 950
    .local v16, "sizeDownload":J
    new-instance v20, Ljava/net/URL;

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 952
    .local v20, "url":Ljava/net/URL;
    sget-object v21, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual/range {v20 .. v21}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljava/net/HttpURLConnection;

    .line 956
    .local v7, "conn":Ljava/net/HttpURLConnection;
    const-string v21, "StubAppActivation"

    const-string v22, "HttpURLConnection() : no proxy set"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    if-eqz v7, :cond_1

    .line 962
    const v21, 0x1d4c0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 964
    const v21, 0x1d4c0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 966
    const-string v21, "GET"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 968
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 972
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v14

    .line 974
    .local v14, "response":I
    const-string v21, "StubAppActivation"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "response : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 982
    sget-boolean v21, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFirstDownload:Z

    if-eqz v21, :cond_4

    .line 984
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v21

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v18, v0

    .line 986
    const-string v21, "StubAppActivation"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "1st sizeTotal : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    .end local v14    # "response":I
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v22

    const v23, 0x8001

    invoke-virtual/range {v21 .. v23}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 1006
    const/16 v21, 0x400

    move/from16 v0, v21

    new-array v6, v0, [B

    .line 1008
    .local v6, "buffer":[B
    const/4 v13, 0x0

    .line 1016
    .local v13, "len1":I
    :goto_2
    invoke-virtual {v5, v6}, Ljava/io/InputStream;->read([B)I

    move-result v13

    if-lez v13, :cond_8

    .line 1018
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFlagCancel:Z

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 1020
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1026
    const/16 v21, 0x0

    .line 1074
    if-eqz v4, :cond_2

    .line 1076
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1110
    .end local v4    # "Fout":Ljava/io/FileOutputStream;
    .end local v5    # "InStream":Ljava/io/InputStream;
    .end local v6    # "buffer":[B
    .end local v7    # "conn":Ljava/net/HttpURLConnection;
    .end local v9    # "file":Ljava/io/File;
    .end local v10    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v11    # "httpParams":Lorg/apache/http/params/HttpParams;
    .end local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v13    # "len1":I
    .end local v15    # "timeout":I
    .end local v16    # "sizeDownload":J
    .end local v18    # "sizeTotal":D
    .end local v20    # "url":Ljava/net/URL;
    :cond_2
    :goto_3
    return v21

    .line 930
    .restart local v4    # "Fout":Ljava/io/FileOutputStream;
    .restart local v5    # "InStream":Ljava/io/InputStream;
    .restart local v9    # "file":Ljava/io/File;
    .restart local v10    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .restart local v11    # "httpParams":Lorg/apache/http/params/HttpParams;
    .restart local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v15    # "timeout":I
    :cond_3
    const-string v21, "Range"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "bytes="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "-"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v10, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    const-string v21, "StubAppActivation"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "file length() : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    const/16 v21, 0x0

    sput-boolean v21, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFirstDownload:Z

    goto/16 :goto_0

    .line 990
    .restart local v7    # "conn":Ljava/net/HttpURLConnection;
    .restart local v14    # "response":I
    .restart local v16    # "sizeDownload":J
    .restart local v18    # "sizeTotal":D
    .restart local v20    # "url":Ljava/net/URL;
    :cond_4
    :try_start_2
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v21

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v22, v22, v16

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v18, v0

    .line 992
    const-string v21, "StubAppActivation"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "sizeTotal : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " sizeDownload : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 1058
    .end local v7    # "conn":Ljava/net/HttpURLConnection;
    .end local v14    # "response":I
    .end local v16    # "sizeDownload":J
    .end local v20    # "url":Ljava/net/URL;
    :catch_0
    move-exception v8

    .line 1060
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 1062
    const-string v21, "StubAppActivation"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Download fail - "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    const-string v21, "DOWNLOAD_STATUS"

    const/16 v22, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1068
    const/16 v21, 0x0

    .line 1074
    if-eqz v4, :cond_2

    .line 1076
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_3

    .line 1078
    :catch_1
    move-exception v8

    .line 1080
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 1078
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v6    # "buffer":[B
    .restart local v7    # "conn":Ljava/net/HttpURLConnection;
    .restart local v13    # "len1":I
    .restart local v16    # "sizeDownload":J
    .restart local v20    # "url":Ljava/net/URL;
    :catch_2
    move-exception v8

    .line 1080
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 1032
    .end local v8    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->isCancelled()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v21

    if-eqz v21, :cond_6

    .line 1034
    const/16 v21, 0x0

    .line 1074
    if-eqz v4, :cond_2

    .line 1076
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_3

    .line 1078
    :catch_3
    move-exception v8

    .line 1080
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 1038
    .end local v8    # "e":Ljava/io/IOException;
    :cond_6
    const/16 v21, 0x0

    :try_start_7
    move/from16 v0, v21

    invoke-virtual {v4, v6, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    .line 1042
    int-to-long v0, v13

    move-wide/from16 v22, v0

    add-long v16, v16, v22

    .line 1044
    const-string v21, "DOWNLOA_RATIO"

    const-wide/16 v22, 0x64

    mul-long v22, v22, v16

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v22, v22, v18

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 1072
    .end local v6    # "buffer":[B
    .end local v7    # "conn":Ljava/net/HttpURLConnection;
    .end local v13    # "len1":I
    .end local v16    # "sizeDownload":J
    .end local v20    # "url":Ljava/net/URL;
    :catchall_0
    move-exception v21

    .line 1074
    if-eqz v4, :cond_7

    .line 1076
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 1082
    :cond_7
    :goto_4
    throw v21

    .line 1048
    .restart local v6    # "buffer":[B
    .restart local v7    # "conn":Ljava/net/HttpURLConnection;
    .restart local v13    # "len1":I
    .restart local v16    # "sizeDownload":J
    .restart local v20    # "url":Ljava/net/URL;
    :cond_8
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 1052
    if-eqz v7, :cond_9

    .line 1054
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1074
    :cond_9
    if-eqz v4, :cond_a

    .line 1076
    :try_start_a
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 1102
    :cond_a
    :goto_5
    const-string v21, "StubAppActivation"

    const-string v22, "Download complete."

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    const-string v21, "DOWNLOAD_STATUS"

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 1106
    const/16 v21, 0x1

    goto/16 :goto_3

    .line 1078
    :catch_4
    move-exception v8

    .line 1080
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1078
    .end local v6    # "buffer":[B
    .end local v7    # "conn":Ljava/net/HttpURLConnection;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v13    # "len1":I
    .end local v16    # "sizeDownload":J
    .end local v20    # "url":Ljava/net/URL;
    :catch_5
    move-exception v8

    .line 1080
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1110
    .end local v4    # "Fout":Ljava/io/FileOutputStream;
    .end local v5    # "InStream":Ljava/io/InputStream;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "file":Ljava/io/File;
    .end local v10    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    .end local v11    # "httpParams":Lorg/apache/http/params/HttpParams;
    .end local v12    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v15    # "timeout":I
    .end local v18    # "sizeTotal":D
    :cond_b
    const/16 v21, 0x0

    goto/16 :goto_3
.end method

.method private getCSC()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1473
    const-string v0, ""

    .line 1475
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1481
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->isCSCExistFile()Z

    move-result v2

    if-eq v2, v3, :cond_0

    .line 1519
    :goto_0
    return-object v0

    .line 1489
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 1491
    if-nez v1, :cond_1

    .line 1493
    const-string v2, "StubAppActivation"

    const-string v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1501
    :cond_1
    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 1503
    const-string v2, "StubAppActivation"

    const-string v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1511
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1527
    const/4 v6, 0x0

    .line 1529
    .local v6, "s":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    const-string v8, "/system/csc/sales_code.dat"

    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1531
    .local v5, "mFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1533
    const/16 v8, 0x14

    new-array v0, v8, [B

    .line 1535
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 1537
    .local v2, "in":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 1542
    .local v4, "length":I
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1544
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 1546
    if-lez v4, :cond_1

    .line 1548
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v6    # "s":Ljava/lang/String;
    .local v7, "s":Ljava/lang/String;
    move-object v6, v7

    .line 1566
    .end local v7    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    :goto_0
    if-eqz v3, :cond_0

    .line 1570
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1572
    const/4 v2, 0x0

    .line 1588
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v4    # "length":I
    :cond_0
    :goto_1
    return-object v6

    .line 1552
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "length":I
    :cond_1
    :try_start_3
    new-instance v7, Ljava/lang/String;

    const-string v8, "FAIL"

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .end local v6    # "s":Ljava/lang/String;
    .restart local v7    # "s":Ljava/lang/String;
    move-object v6, v7

    .end local v7    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    goto :goto_0

    .line 1574
    :catch_0
    move-exception v1

    .line 1576
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1556
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    .line 1558
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1566
    if-eqz v2, :cond_0

    .line 1570
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 1572
    const/4 v2, 0x0

    goto :goto_1

    .line 1574
    :catch_2
    move-exception v1

    .line 1576
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1560
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1562
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1566
    if-eqz v2, :cond_0

    .line 1570
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 1572
    const/4 v2, 0x0

    goto :goto_1

    .line 1574
    :catch_4
    move-exception v1

    .line 1576
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1566
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_4
    if-eqz v2, :cond_2

    .line 1570
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 1572
    const/4 v2, 0x0

    .line 1578
    :cond_2
    :goto_5
    throw v8

    .line 1574
    :catch_5
    move-exception v1

    .line 1576
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1566
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_4

    .line 1560
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 1556
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_2
.end method

.method private getIMEI()Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x10

    .line 1628
    const-string v3, ""

    .line 1630
    .local v3, "imei":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    const-string v7, "phone"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 1634
    .local v5, "telMgr":Landroid/telephony/TelephonyManager;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 1638
    .local v4, "md5":Ljava/lang/StringBuffer;
    if-eqz v5, :cond_1

    .line 1640
    const/4 v0, 0x0

    .line 1644
    .local v0, "digest":[B
    :try_start_0
    const-string v6, "MD5"

    invoke-static {v6}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    const-string v7, "000000000000000"

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/security/MessageDigest;->digest([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1654
    :goto_0
    if-nez v0, :cond_0

    .line 1656
    const/4 v6, 0x0

    .line 1674
    .end local v0    # "digest":[B
    :goto_1
    return-object v6

    .line 1646
    .restart local v0    # "digest":[B
    :catch_0
    move-exception v1

    .line 1650
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 1660
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    array-length v6, v0

    if-ge v2, v6, :cond_1

    .line 1662
    aget-byte v6, v0, v2

    and-int/lit16 v6, v6, 0xf0

    shr-int/lit8 v6, v6, 0x4

    invoke-static {v6, v8}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1664
    aget-byte v6, v0, v2

    and-int/lit8 v6, v6, 0xf

    shr-int/lit8 v6, v6, 0x0

    invoke-static {v6, v8}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1660
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1672
    .end local v0    # "digest":[B
    .end local v2    # "i":I
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    .line 1674
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method private getMCC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 1415
    const-string v0, ""

    .line 1417
    .local v0, "mcc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 1423
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 1425
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 1429
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v3, v5, :cond_0

    .line 1431
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1437
    .end local v1    # "networkOperator":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getMNC()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 1445
    const-string v0, "00"

    .line 1447
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 1453
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-eqz v2, :cond_0

    .line 1455
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 1457
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v5, :cond_0

    .line 1459
    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1465
    .end local v1    # "networkOperator":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getPD()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1682
    const-string v2, "0"

    .line 1684
    .local v2, "rtn_str":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1688
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    sget-object v3, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->PD_TEST_PATH:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1694
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 1696
    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    .line 1698
    const-string v2, "1"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1706
    :cond_0
    :goto_0
    return-object v2

    .line 1702
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private getUrlForCTC(Ljava/lang/String;)Z
    .locals 17
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 1234
    const-string v14, "StubAppActivation"

    const-string v15, "getUrlForCTC()"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1240
    const/4 v1, 0x0

    .line 1244
    .local v1, "bReturn":Z
    new-instance v5, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v5}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 1246
    .local v5, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v6, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1248
    .local v6, "httpget":Lorg/apache/http/client/methods/HttpGet;
    const/4 v10, 0x0

    .line 1254
    .local v10, "response":Lorg/apache/http/HttpResponse;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 1256
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 1262
    .local v8, "parser":Lorg/xmlpull/v1/XmlPullParser;
    :try_start_1
    invoke-interface {v5, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v10

    .line 1266
    const-string v14, "StubAppActivation"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " checkDownload() response : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v10}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1274
    invoke-interface {v10}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 1276
    .local v3, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v3, :cond_0

    .line 1280
    :try_start_2
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v7

    .line 1282
    .local v7, "instream":Ljava/io/InputStream;
    const/4 v14, 0x0

    invoke-interface {v8, v7, v14}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1304
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v7    # "instream":Ljava/io/InputStream;
    :cond_0
    :goto_0
    :try_start_3
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v9

    .line 1308
    .local v9, "parserEvent":I
    const-string v11, ""

    .line 1312
    .local v11, "serverUri":Ljava/lang/String;
    :goto_1
    const/4 v14, 0x1

    if-eq v9, v14, :cond_4

    .line 1314
    const/4 v14, 0x2

    if-ne v9, v14, :cond_1

    .line 1316
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 1318
    .local v12, "tag":Ljava/lang/String;
    const-string v14, "serverURL"

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 1320
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v13

    .line 1322
    .local v13, "type":I
    const/4 v14, 0x4

    if-ne v13, v14, :cond_1

    .line 1324
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v11

    .line 1332
    .end local v12    # "tag":Ljava/lang/String;
    .end local v13    # "type":I
    :cond_1
    const/4 v14, 0x3

    if-ne v9, v14, :cond_2

    .line 1334
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 1336
    .restart local v12    # "tag":Ljava/lang/String;
    const-string v14, "serverURL"

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1338
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "http://"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/stub/stubUpdateCheck.as"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mUpdateUriCTC:Ljava/lang/String;

    .line 1340
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "https://"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/stub/stubDownload.as"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUriCTC:Ljava/lang/String;

    .line 1342
    const-string v14, "StubAppActivation"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " mUpdateUri : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mUpdateUriCTC:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1344
    const-string v14, "StubAppActivation"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " mDownloadUri : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUriCTC:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1352
    .end local v12    # "tag":Ljava/lang/String;
    :cond_2
    invoke-interface {v8}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v9

    goto/16 :goto_1

    .line 1284
    .end local v9    # "parserEvent":I
    .end local v11    # "serverUri":Ljava/lang/String;
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    :catch_0
    move-exception v2

    .line 1286
    .local v2, "e":Ljava/lang/IllegalStateException;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1296
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    :catch_1
    move-exception v2

    .line 1298
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 1362
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_2
    move-exception v2

    .line 1364
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_6
    const-string v14, "StubAppActivation"

    const-string v15, "xml parsing error"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1366
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1368
    const/4 v1, 0x0

    .line 1396
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 1398
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1400
    const-string v14, "StubAppActivation"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1408
    .end local v2    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_3
    :goto_2
    return v1

    .line 1288
    .restart local v3    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_3
    move-exception v2

    .line 1290
    .local v2, "e":Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 1370
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_4
    move-exception v2

    .line 1372
    .local v2, "e":Ljava/net/SocketException;
    :try_start_8
    invoke-virtual {v2}, Ljava/net/SocketException;->printStackTrace()V

    .line 1374
    const-string v14, "StubAppActivation"

    const-string v15, "network is unavailable"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1376
    const/4 v1, 0x0

    .line 1396
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 1398
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1400
    const-string v14, "StubAppActivation"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1358
    .end local v2    # "e":Ljava/net/SocketException;
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v9    # "parserEvent":I
    .restart local v11    # "serverUri":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x1

    .line 1396
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 1398
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1400
    const-string v14, "StubAppActivation"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1378
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v9    # "parserEvent":I
    .end local v11    # "serverUri":Ljava/lang/String;
    :catch_5
    move-exception v2

    .line 1380
    .local v2, "e":Ljava/net/UnknownHostException;
    :try_start_9
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 1382
    const-string v14, "StubAppActivation"

    const-string v15, "server is not response"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1384
    const/4 v1, 0x0

    .line 1396
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 1398
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1400
    const-string v14, "StubAppActivation"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1386
    .end local v2    # "e":Ljava/net/UnknownHostException;
    :catch_6
    move-exception v2

    .line 1388
    .local v2, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 1390
    const-string v14, "StubAppActivation"

    const-string v15, "network error"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1392
    const/4 v1, 0x0

    .line 1396
    if-eqz v5, :cond_3

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    if-eqz v14, :cond_3

    .line 1398
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v14

    invoke-interface {v14}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1400
    const-string v14, "StubAppActivation"

    const-string v15, "shutdown"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 1396
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v14

    if-eqz v5, :cond_5

    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v15

    if-eqz v15, :cond_5

    .line 1398
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v15

    invoke-interface {v15}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 1400
    const-string v15, "StubAppActivation"

    const-string v16, "shutdown"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    throw v14
.end method

.method private installApk()Z
    .locals 8

    .prologue
    const/4 v7, 0x5

    .line 1118
    const-string v5, "StubAppActivation"

    const-string v6, "installApk()"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    new-instance v3, Ljava/io/File;

    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1122
    .local v3, "folder":Ljava/io/File;
    const-string v5, "DOWNLOAD_STATUS"

    const/4 v6, 0x2

    invoke-direct {p0, v5, v6}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 1124
    const/4 v0, 0x0

    .line 1126
    .local v0, "bIsSuccess":Z
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1128
    const-string v5, "DOWNLOAD_STATUS"

    invoke-direct {p0, v5, v7}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    move v1, v0

    .line 1215
    .end local v0    # "bIsSuccess":Z
    .local v1, "bIsSuccess":I
    :goto_0
    return v1

    .line 1136
    .end local v1    # "bIsSuccess":I
    .restart local v0    # "bIsSuccess":Z
    :cond_0
    :try_start_0
    new-instance v4, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;

    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;-><init>(Landroid/content/Context;)V

    .line 1138
    .local v4, "mgr":Lcom/sec/android/stub/paywithpaypal/ApplicationManager;
    new-instance v5, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$1;

    invoke-direct {v5, p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$1;-><init>(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->setOnInstalledPackaged(Lcom/sec/android/stub/paywithpaypal/OnInstalledPackaged;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1168
    :try_start_1
    sget-boolean v5, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mRequestInstallation:Z

    if-eqz v5, :cond_2

    .line 1170
    const/4 v5, 0x0

    sput-boolean v5, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mRequestInstallation:Z

    .line 1172
    const-string v5, "temp"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    invoke-virtual {v6}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1174
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->checkApkSignature(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1176
    const-string v5, "DOWNLOAD_STATUS"

    const/4 v6, 0x5

    invoke-direct {p0, v5, v6}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    move v1, v0

    .line 1178
    .restart local v1    # "bIsSuccess":I
    goto :goto_0

    .line 1181
    .end local v1    # "bIsSuccess":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->installPackage(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1203
    :cond_2
    :goto_1
    const/4 v0, 0x1

    .end local v4    # "mgr":Lcom/sec/android/stub/paywithpaypal/ApplicationManager;
    :goto_2
    move v1, v0

    .line 1215
    .restart local v1    # "bIsSuccess":I
    goto :goto_0

    .line 1185
    .end local v1    # "bIsSuccess":I
    .restart local v4    # "mgr":Lcom/sec/android/stub/paywithpaypal/ApplicationManager;
    :catch_0
    move-exception v2

    .line 1187
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 1205
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    .end local v4    # "mgr":Lcom/sec/android/stub/paywithpaypal/ApplicationManager;
    :catch_1
    move-exception v2

    .line 1207
    .local v2, "e":Ljava/lang/SecurityException;
    invoke-virtual {v2}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_2

    .line 1189
    .end local v2    # "e":Ljava/lang/SecurityException;
    .restart local v4    # "mgr":Lcom/sec/android/stub/paywithpaypal/ApplicationManager;
    :catch_2
    move-exception v2

    .line 1191
    .local v2, "e":Ljava/lang/IllegalAccessException;
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    .line 1209
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    .end local v4    # "mgr":Lcom/sec/android/stub/paywithpaypal/ApplicationManager;
    :catch_3
    move-exception v2

    .line 1211
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_2

    .line 1193
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v4    # "mgr":Lcom/sec/android/stub/paywithpaypal/ApplicationManager;
    :catch_4
    move-exception v2

    .line 1195
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    .line 1197
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_5
    move-exception v2

    .line 1199
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1
.end method

.method private isCSCExistFile()Z
    .locals 4

    .prologue
    .line 1596
    const/4 v1, 0x0

    .line 1598
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string v2, "/system/csc/sales_code.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1604
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 1606
    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 1608
    const-string v2, "StubAppActivation"

    const-string v3, "CSC is not exist"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1620
    :cond_0
    :goto_0
    return v1

    .line 1612
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private sendMessage(Ljava/lang/String;D)V
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "arg"    # D

    .prologue
    .line 252
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 254
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 256
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 258
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v2, "APK_SIZE"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 263
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->MESSAGE_ID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mID:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v2, "APK_SIZE"

    invoke-virtual {v0, v2, p2, p3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 269
    :cond_0
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 271
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 273
    return-void
.end method

.method private sendMessage(Ljava/lang/String;I)V
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "arg"    # I

    .prologue
    .line 204
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 248
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 210
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    .line 212
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 214
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->MESSAGE_ID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mID:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 220
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 224
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->MESSAGE_RESULT:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mResult:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->MESSAGE_DATA_URI:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUri:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_1
    :goto_1
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 246
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 228
    :cond_2
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 232
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 234
    :cond_3
    const-string v2, "RETURNCODE_ERROR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 238
    const-string v2, "RETURNCODE_ERROR"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mFlagCancel:Z

    .line 198
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 65
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "arg0"    # [Ljava/lang/String;

    .prologue
    .line 1724
    const-string v2, "StubAppActivation"

    const-string v3, "doInBackground()"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1726
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->checkDownload()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1736
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mResult:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mDownloadUri:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->downloadApk(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mRequestInstallation:Z

    .line 1740
    const-string v2, "StubAppActivation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mRequestInstallation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mRequestInstallation:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1760
    :goto_0
    const/4 v2, 0x0

    return-object v2

    .line 1744
    :cond_0
    const-string v2, "StubAppActivation"

    const-string v3, "Check download Failed : "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1746
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1748
    .local v1, "msg":Landroid/os/Message;
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1750
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1752
    .local v0, "b":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->MESSAGE_ID:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mID:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1754
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1756
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public getDownloadPkgName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 65
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 1768
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1771
    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1773
    sget-boolean v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->mRequestInstallation:Z

    if-eqz v0, :cond_0

    .line 1775
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_Handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$2;

    invoke-direct {v1, p0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$2;-><init>(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1791
    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 1715
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1717
    const-string v0, "DOWNLOAD_STATUS"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V

    .line 1718
    return-void
.end method
