.class public Lcom/sec/android/stub/paywithpaypal/HFileUtil;
.super Ljava/lang/Object;
.source "HFileUtil.java"


# instance fields
.field private _AbsolutePath:Ljava/lang/String;

.field private _Context:Landroid/content/Context;

.field private _File:Ljava/io/File;

.field private _FileName:Ljava/lang/String;

.field private _bPrepared:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_bPrepared:Z

    .line 36
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_Context:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_FileName:Ljava/lang/String;

    .line 40
    return-void
.end method

.method private createDir(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 86
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 88
    .local v0, "root":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v1

    .line 96
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 104
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_Context:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    .local v1, "ret":Ljava/io/File;
    return-object v1
.end method


# virtual methods
.method public delete()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exists()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 184
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_AbsolutePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_File:Ljava/io/File;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_FileName:Ljava/lang/String;

    return-object v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 202
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public prepare()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 126
    iget-boolean v1, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_bPrepared:Z

    if-eqz v1, :cond_0

    .line 158
    :goto_0
    return v0

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->createDir(Ljava/lang/String;)Z

    .line 138
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_FileName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_File:Ljava/io/File;

    .line 142
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_File:Ljava/io/File;

    if-nez v1, :cond_1

    .line 144
    const/4 v0, 0x0

    goto :goto_0

    .line 150
    :cond_1
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_AbsolutePath:Ljava/lang/String;

    .line 154
    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_bPrepared:Z

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_Context:Landroid/content/Context;

    .line 56
    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_File:Ljava/io/File;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->_bPrepared:Z

    .line 60
    return-void
.end method
