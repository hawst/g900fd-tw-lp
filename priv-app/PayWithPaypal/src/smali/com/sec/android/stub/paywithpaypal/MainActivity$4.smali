.class Lcom/sec/android/stub/paywithpaypal/MainActivity$4;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/stub/paywithpaypal/MainActivity;->setLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V
    .locals 0

    .prologue
    .line 499
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$4;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 503
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$4;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_SAMSUNGAPPS:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3100()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 504
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 505
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "samsungapps://ProductDetail/"

    .line 506
    .local v1, "string_of_uri":Ljava/lang/String;
    const v2, 0x14000020

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 508
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f090007

    if-ne v2, v3, :cond_2

    .line 509
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->getInstalledInfo()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 510
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 511
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 512
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$4;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 526
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "string_of_uri":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 514
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "string_of_uri":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$4;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->performClick()Z

    goto :goto_0

    .line 516
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f090011

    if-ne v2, v3, :cond_0

    .line 517
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->getInstalledInfo()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 518
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 519
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 520
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$4;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 522
    :cond_3
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$4;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->performClick()Z

    goto :goto_0
.end method
