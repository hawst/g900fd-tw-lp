.class Lcom/sec/android/stub/paywithpaypal/MainActivity$5;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/stub/paywithpaypal/MainActivity;->setLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V
    .locals 0

    .prologue
    .line 613
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 616
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 618
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 619
    .local v10, "intent":Landroid/content/Intent;
    const-string v0, "com.paypal.android.p2pmobile"

    const-string v1, "com.paypal.android.lib.authenticator.mini.PaypalLinkDialogFragment"

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 622
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v10, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v9

    .line 623
    .local v9, "info":Landroid/content/pm/ResolveInfo;
    if-nez v9, :cond_0

    .line 624
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.paypal.android.p2pmobile"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 627
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 628
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->LOGGING_APP_ID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3400()Ljava/lang/String;

    move-result-object v1

    const-string v2, "FPLP"

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 703
    .end local v9    # "info":Landroid/content/pm/ResolveInfo;
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 629
    .restart local v9    # "info":Landroid/content/pm/ResolveInfo;
    .restart local v10    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v7

    .line 630
    .local v7, "ex":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v7}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 631
    .end local v7    # "ex":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v6

    .line 632
    .local v6, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v6}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 635
    .end local v6    # "e":Ljava/lang/NullPointerException;
    .end local v9    # "info":Landroid/content/pm/ResolveInfo;
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_2
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-nez v0, :cond_3

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-eqz v0, :cond_6

    .line 638
    :cond_3
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    .line 640
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-eqz v0, :cond_4

    .line 641
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iput-boolean v3, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 643
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->cancelDownload()V

    .line 645
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 648
    :cond_4
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-eqz v0, :cond_5

    .line 649
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iput-boolean v3, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 651
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->cancelDownload()V

    .line 653
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 656
    :cond_5
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateCheckBoxEnableState()V

    .line 658
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateBtnState()V

    goto/16 :goto_0

    .line 661
    :cond_6
    const-string v0, "VERSION_CODE"

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 662
    const-string v0, "VERSION_CODE"

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 663
    const-string v0, "VERSION_CODE"

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 664
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v0, v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    .line 668
    :cond_7
    :goto_1
    const-string v0, "IS_DATA_USING_DIALOG_SHOW"

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 669
    const-string v0, "IS_DATA_USING_DIALOG_SHOW"

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 670
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isDataUsingPopupDone:Z
    invoke-static {v0, v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3602(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    .line 676
    :goto_2
    const-string v0, "IS_ROAMING_USING_DIALOG_SHOW"

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 677
    const-string v0, "IS_ROAMING_USING_DIALOG_SHOW"

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 678
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v0, v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    .line 685
    :goto_3
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isNetworkConnected(Landroid/content/Context;)Z
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1300(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isWifiNetworkConnected()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingConnected()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 688
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->showDataUsingDialog()V
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    goto/16 :goto_0

    .line 666
    :cond_8
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    goto :goto_1

    .line 672
    :cond_9
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isDataUsingPopupDone:Z
    invoke-static {v0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3602(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    goto :goto_2

    .line 674
    :cond_a
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isDataUsingPopupDone:Z
    invoke-static {v0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3602(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    goto :goto_2

    .line 680
    :cond_b
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    goto :goto_3

    .line 682
    :cond_c
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingUsingPopupDone:Z
    invoke-static {v0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    goto :goto_3

    .line 689
    :cond_d
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isNetworkConnected(Landroid/content/Context;)Z
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1300(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isWifiNetworkConnected()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingConnected()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isDataUsingPopupDone:Z
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3600(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 690
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->showDataUsingDialog()V
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    goto/16 :goto_0

    .line 692
    :cond_e
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 694
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 695
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 697
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_4
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_1

    .line 698
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$4000(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Z

    move-result v0

    if-eqz v0, :cond_f

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->getInstalledInfo()Z

    move-result v0

    if-nez v0, :cond_f

    .line 699
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->startDownload()V

    .line 697
    :cond_f
    add-int/lit8 v8, v8, 0x1

    goto :goto_4
.end method
