.class public final Lcom/sec/android/stub/paywithpaypal/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/stub/paywithpaypal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final additional_charges_msg:I = 0x7f070010

.field public static final alert:I = 0x7f070012

.field public static final app_label:I = 0x7f070000

.field public static final attention:I = 0x7f070019

.field public static final btn_text_cancel:I = 0x7f070007

.field public static final btn_text_install:I = 0x7f070006

.field public static final connect:I = 0x7f07000d

.field public static final connect_via_roaming_network:I = 0x7f07000f

.field public static final data_connection_title:I = 0x7f07000e

.field public static final do_not_show_again:I = 0x7f07001a

.field public static final download_will_be_cancelled:I = 0x7f07001b

.field public static final downloading:I = 0x7f07000b

.field public static final error_failed_to_download_msg:I = 0x7f070016

.field public static final error_failed_to_download_title:I = 0x7f070013

.field public static final error_failed_to_install_msg:I = 0x7f070017

.field public static final install_mfac:I = 0x7f070002

.field public static final install_paypal:I = 0x7f070004

.field public static final installation:I = 0x7f070008

.field public static final installing:I = 0x7f07000c

.field public static final link_my_account:I = 0x7f07001c

.field public static final mb:I = 0x7f07000a

.field public static final message_roaming_extra_charges:I = 0x7f070011

.field public static final mfac_description:I = 0x7f070003

.field public static final paypal_description:I = 0x7f070005

.field public static final pkg_installed:I = 0x7f070014

.field public static final pkg_not_installed:I = 0x7f070015

.field public static final size:I = 0x7f070009

.field public static final top_information:I = 0x7f070001

.field public static final try_again_later:I = 0x7f070018


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
