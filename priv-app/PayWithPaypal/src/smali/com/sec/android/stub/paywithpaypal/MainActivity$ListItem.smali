.class Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
.super Ljava/lang/Object;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/stub/paywithpaypal/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ListItem"
.end annotation


# instance fields
.field private mAppID:Ljava/lang/String;

.field private mDownCheck:Z

.field private mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

.field private mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

.field private mLabel:Ljava/lang/String;

.field private mNeedUpdate:Z

.field private mTimer:Ljava/util/Timer;

.field final synthetic this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)V
    .locals 2
    .param p2, "ID"    # Ljava/lang/String;

    .prologue
    .line 1163
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z

    .line 1161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z

    .line 1164
    iput-object p2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    .line 1165
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mLabel:Ljava/lang/String;

    .line 1167
    new-instance v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    invoke-direct {v0, p2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    .line 1168
    invoke-virtual {p1, p2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z

    .line 1169
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    invoke-virtual {p1, p2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalled:Z

    .line 1170
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2, "ID"    # Ljava/lang/String;
    .param p3, "LABEL"    # Ljava/lang/String;

    .prologue
    .line 1171
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z

    .line 1161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z

    .line 1172
    iput-object p2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    .line 1173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mLabel:Ljava/lang/String;

    .line 1175
    new-instance v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    invoke-direct {v0, p2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    .line 1176
    invoke-virtual {p1, p2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z

    .line 1177
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    invoke-virtual {p1, p2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalled:Z

    .line 1178
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    .prologue
    .line 1152
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    .prologue
    .line 1152
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 1152
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    .prologue
    .line 1152
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    .param p1, "x1"    # Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    .prologue
    .line 1152
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    return-object p1
.end method

.method static synthetic access$4000(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    .prologue
    .line 1152
    iget-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z

    return v0
.end method

.method static synthetic access$4002(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    .param p1, "x1"    # Z

    .prologue
    .line 1152
    iput-boolean p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z

    return p1
.end method

.method static synthetic access$4300(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    .prologue
    .line 1152
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    .param p1, "x1"    # Z

    .prologue
    .line 1152
    iput-boolean p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z

    return p1
.end method


# virtual methods
.method cancelDownload()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1241
    const-string v0, "StubAppActivation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : cancelDownload"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 1243
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    if-eqz v0, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->cancel()V

    .line 1245
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    invoke-virtual {v0, v3}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->cancel(Z)Z

    .line 1246
    iput-object v4, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    .line 1247
    iput-boolean v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z

    .line 1250
    :cond_0
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 1251
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1252
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 1253
    iput-object v4, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    .line 1256
    :cond_1
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1257
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1900()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1258
    const-string v0, "DOWNLOAD_RATIO_MFAC"

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1900()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 1265
    :cond_2
    :goto_0
    return-void

    .line 1260
    :cond_3
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1261
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2000()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1262
    const-string v0, "DOWNLOAD_RATIO_PAYPAL"

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2000()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public getInstalledInfo()Z
    .locals 1

    .prologue
    .line 1202
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalled:Z

    return v0
.end method

.method public setDownInfo(D)V
    .locals 1
    .param p1, "size"    # D

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iput-wide p1, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mSize:D

    .line 1195
    return-void
.end method

.method public setDownInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;
    .param p2, "downURL"    # Ljava/lang/String;

    .prologue
    .line 1187
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iput-object p1, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mResult:Ljava/lang/String;

    .line 1188
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iput-object p2, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mURL:Ljava/lang/String;

    .line 1190
    return-void
.end method

.method public setDownInfo(Ljava/lang/String;Ljava/lang/String;D)V
    .locals 1
    .param p1, "result"    # Ljava/lang/String;
    .param p2, "downURL"    # Ljava/lang/String;
    .param p3, "size"    # D

    .prologue
    .line 1181
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iput-object p1, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mResult:Ljava/lang/String;

    .line 1182
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iput-object p2, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mURL:Ljava/lang/String;

    .line 1183
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iput-wide p3, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mSize:D

    .line 1185
    return-void
.end method

.method public setInstalledInfo()V
    .locals 3

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalled:Z

    .line 1199
    return-void
.end method

.method startDownload()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1206
    const-string v0, "StubAppActivation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : startDownload"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1207
    new-instance v0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$4200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iget-object v4, v4, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mResult:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    iget-object v5, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mURL:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mLabel:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    .line 1209
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v8, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1210
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v0, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    .line 1212
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 1213
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    .line 1214
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 1238
    :cond_0
    return-void
.end method
