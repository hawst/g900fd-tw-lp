.class Lcom/sec/android/stub/paywithpaypal/MainActivity$1;
.super Landroid/os/Handler;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/stub/paywithpaypal/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "MESSAGE_CMD"

    const-string v7, "DOWNLOAD_STATUS"

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "cmd":Ljava/lang/String;
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 139
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->MESSAGE_ID:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 140
    .local v4, "mPkgName":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getItem(Ljava/lang/String;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    move-result-object v2

    .line 141
    .local v2, "item":Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    if-eqz v2, :cond_0

    .line 145
    const-string v5, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 146
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "DOWNLOAD_STATUS"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_1

    .line 282
    :cond_1
    :goto_1
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateCheckBoxEnableState()V

    .line 283
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateBtnState()V

    goto :goto_0

    .line 148
    :pswitch_2
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 149
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 150
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x1

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    goto :goto_1

    .line 151
    :cond_2
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 152
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x1

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    goto :goto_1

    .line 156
    :pswitch_3
    const/4 v5, 0x0

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    goto :goto_1

    .line 159
    :pswitch_4
    const-string v5, "DOWNLOAD_RATIO_MFAC"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 160
    const-string v5, "DOWNLOAD_RATIO_PAYPAL"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 162
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    .line 163
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 165
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 166
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 168
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_mfac:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07000c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 169
    :cond_3
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 170
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 172
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_paypal:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07000c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 176
    :pswitch_5
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 177
    const/4 v5, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    .line 178
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1200(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Ljava/util/Timer;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 179
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1200(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Ljava/util/Timer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Timer;->cancel()V

    .line 180
    const/4 v5, 0x0

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1202(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Ljava/util/Timer;)Ljava/util/Timer;

    .line 182
    :cond_4
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 183
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 187
    :cond_5
    :goto_2
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v5

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->isNetworkConnected(Landroid/content/Context;)Z
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1300(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 188
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->getNetworkErrorState()I
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1400(Lcom/sec/android/stub/paywithpaypal/MainActivity;)I

    move-result v6

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->sendBroadcastForNetworkErrorPopup(I)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1500(Lcom/sec/android/stub/paywithpaypal/MainActivity;I)V

    .line 190
    :cond_6
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 191
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->cancel()V

    .line 192
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->cancel(Z)Z

    .line 193
    const/4 v5, 0x0

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    goto/16 :goto_1

    .line 184
    :cond_7
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 185
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    goto :goto_2

    .line 197
    :pswitch_6
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 198
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->cancel()V

    .line 199
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->cancel(Z)Z

    .line 200
    const/4 v5, 0x0

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    .line 202
    :cond_8
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    .line 203
    const/4 v5, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    .line 204
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    .line 205
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 207
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 208
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 210
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_mfac:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07000b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    goto/16 :goto_1

    .line 212
    :cond_9
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 213
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 215
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_paypal:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07000b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    goto/16 :goto_1

    .line 220
    :pswitch_7
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    .line 221
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalled:Z

    .line 223
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1200(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Ljava/util/Timer;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 224
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1200(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Ljava/util/Timer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Timer;->cancel()V

    .line 225
    const/4 v5, 0x0

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mTimer:Ljava/util/Timer;
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1202(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Ljava/util/Timer;)Ljava/util/Timer;

    .line 227
    :cond_a
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 228
    const-string v5, "DOWNLOAD_RATIO_MFAC"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 229
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 237
    :cond_b
    :goto_3
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setLayout()V
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    goto/16 :goto_1

    .line 231
    :cond_c
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 232
    const-string v5, "DOWNLOAD_RATIO_PAYPAL"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 233
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    goto :goto_3

    .line 275
    :pswitch_8
    const/4 v5, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    .line 276
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 277
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 278
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->getNetworkErrorState()I
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1400(Lcom/sec/android/stub/paywithpaypal/MainActivity;)I

    move-result v6

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->sendBroadcastForNetworkErrorPopup(I)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1500(Lcom/sec/android/stub/paywithpaypal/MainActivity;I)V

    goto/16 :goto_1

    .line 284
    :cond_d
    const-string v5, "DOWNLOA_RATIO"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 285
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 286
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "DOWNLOA_RATIO"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1902(I)I

    goto/16 :goto_0

    .line 287
    :cond_e
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 288
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "DOWNLOA_RATIO"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2002(I)I

    goto/16 :goto_0

    .line 290
    :cond_f
    const-string v5, "DOWNLODING_PROGRESS_UPDATE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 291
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 292
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1900()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 293
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1900()I

    move-result v5

    if-eqz v5, :cond_10

    .line 294
    const-string v5, "DOWNLOAD_RATIO_MFAC"

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1900()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 295
    :cond_10
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_mfac:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%.1f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D
    invoke-static {v10}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v10

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v10, v12

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1900()I

    move-result v12

    int-to-double v12, v12

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v8, 0x7f07000a

    invoke-virtual {v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 296
    :cond_11
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 297
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2000()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 298
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2000()I

    move-result v5

    if-eqz v5, :cond_12

    .line 299
    const-string v5, "DOWNLOAD_RATIO_PAYPAL"

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2000()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 300
    :cond_12
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_paypal:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2400(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%.1f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D
    invoke-static {v10}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v10

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v10, v12

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2000()I

    move-result v12

    int-to-double v12, v12

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v8, 0x7f07000a

    invoke-virtual {v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 302
    :cond_13
    const-string v5, "RETURNCODE_ERROR"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 303
    const/4 v1, 0x0

    .line 304
    .local v1, "err_msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    .line 305
    const/4 v5, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    .line 307
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 308
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    .line 317
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    if-eqz v5, :cond_14

    .line 318
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->cancel()V

    .line 319
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1600(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->cancel(Z)Z

    .line 320
    const/4 v5, 0x0

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadTask:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;
    invoke-static {v2, v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$1602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    .line 322
    :cond_14
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 323
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 324
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 325
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_mfac:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07000b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v7, 0x7f070017

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v11, 0x7f070002

    invoke-virtual {v10, v11}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v7, 0x7f070018

    invoke-virtual {v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 336
    :cond_15
    :goto_4
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateCheckBoxEnableState()V

    .line 337
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateBtnState()V

    .line 338
    if-eqz v1, :cond_0

    .line 339
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->showErrorDialog(Ljava/lang/String;)V
    invoke-static {v5, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 329
    :cond_16
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 330
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 331
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 332
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_paypal:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07000b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v7, 0x7f070017

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v11, 0x7f070004

    invoke-virtual {v10, v11}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v7, 0x7f070018

    invoke-virtual {v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 340
    .end local v1    # "err_msg":Ljava/lang/String;
    :cond_17
    const-string v5, "APK_SIZE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 341
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "APK_SIZE"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->apkSize:D
    invoke-static {v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2602(D)D

    .line 342
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->apkSize:D
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2600()D

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->setDownInfo(D)V

    .line 343
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 344
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->apkSize:D
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2600()D

    move-result-wide v6

    const-wide/high16 v8, 0x4130000000000000L    # 1048576.0

    div-double/2addr v6, v8

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D
    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2102(Lcom/sec/android/stub/paywithpaypal/MainActivity;D)D

    .line 345
    const-string v5, "APK_SIZE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_19

    .line 346
    const-string v5, "APK_SIZE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v8

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_18

    .line 347
    const-string v5, "APK_SIZE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/Double;Landroid/content/Context;)V

    .line 352
    :cond_18
    :goto_5
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_mfac:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%.1f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D
    invoke-static {v10}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v10

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v10, v12

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1900()I

    move-result v12

    int-to-double v12, v12

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v8, 0x7f07000a

    invoke-virtual {v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSize_mfac:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " / %.1f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D
    invoke-static {v10}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v8, 0x7f07000a

    invoke-virtual {v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 350
    :cond_19
    const-string v5, "APK_SIZE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/Double;Landroid/content/Context;)V

    goto/16 :goto_5

    .line 354
    :cond_1a
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 355
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->apkSize:D
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2600()D

    move-result-wide v6

    const-wide/high16 v8, 0x4130000000000000L    # 1048576.0

    div-double/2addr v6, v8

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D
    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2302(Lcom/sec/android/stub/paywithpaypal/MainActivity;D)D

    .line 356
    const-string v5, "APK_SIZE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1c

    .line 357
    const-string v5, "APK_SIZE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v8

    cmpl-double v5, v6, v8

    if-eqz v5, :cond_1b

    .line 358
    const-string v5, "APK_SIZE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/Double;Landroid/content/Context;)V

    .line 365
    :cond_1b
    :goto_6
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_paypal:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2400(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%.1f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D
    invoke-static {v10}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v10

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v10, v12

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2000()I

    move-result v12

    int-to-double v12, v12

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v8, 0x7f07000a

    invoke-virtual {v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSize_paypal:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " / %.1f"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D
    invoke-static {v10}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v8, 0x7f07000a

    invoke-virtual {v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 361
    :cond_1c
    const-string v5, "APK_SIZE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/Double;Landroid/content/Context;)V

    goto/16 :goto_6

    .line 368
    :cond_1d
    const-string v5, "VERSION_CODE"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 369
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "VERSION_CODE"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2902(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 370
    const-string v5, "VERSION_CODE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1f

    .line 371
    const-string v5, "VERSION_CODE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 372
    const-string v5, "VERSION_CODE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 373
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    goto/16 :goto_0

    .line 375
    :cond_1e
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    goto/16 :goto_0

    .line 378
    :cond_1f
    const-string v5, "VERSION_CODE"

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 379
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z

    goto/16 :goto_0

    .line 384
    .end local v2    # "item":Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    .end local v4    # "mPkgName":Ljava/lang/String;
    :pswitch_9
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->getNetworkErrorState()I
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1400(Lcom/sec/android/stub/paywithpaypal/MainActivity;)I

    move-result v6

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->sendBroadcastForNetworkErrorPopup(I)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$1500(Lcom/sec/android/stub/paywithpaypal/MainActivity;I)V

    .line 385
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 386
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    goto/16 :goto_0

    .line 391
    :pswitch_a
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->MESSAGE_ID:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 392
    .local v3, "mPkg":Ljava/lang/String;
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 393
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v7, 0x7f070016

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v11, 0x7f070002

    invoke-virtual {v10, v11}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->showErrorDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)V

    .line 394
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 395
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    .line 402
    :cond_20
    :goto_7
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateCheckBoxEnableState()V

    .line 403
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateBtnState()V

    goto/16 :goto_0

    .line 396
    :cond_21
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$400()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_20

    .line 397
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    iget-object v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v7, 0x7f070016

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const v11, 0x7f070004

    invoke-virtual {v10, v11}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->showErrorDialog(Ljava/lang/String;)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$2500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)V

    .line 398
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v5}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v5

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 399
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    const/4 v6, 0x0

    # invokes: Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V
    invoke-static {v5, v6}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V

    goto :goto_7

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_a
    .end packed-switch

    .line 146
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method
