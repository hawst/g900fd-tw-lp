.class Lcom/sec/android/stub/paywithpaypal/MainActivity$9;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/stub/paywithpaypal/MainActivity;->showDataUsingDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V
    .locals 0

    .prologue
    .line 856
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$9;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 859
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$9;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$4100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 860
    const-string v1, "IS_ROAMING_USING_DIALOG_SHOW"

    const-string v2, "true"

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$9;->this$0:Lcom/sec/android/stub/paywithpaypal/MainActivity;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 861
    const-string v1, "StubAppActivation"

    const-string v2, "putPref: Roaming"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 865
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 866
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$4000(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->getInstalledInfo()Z

    move-result v1

    if-nez v1, :cond_1

    .line 867
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->access$3000()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->startDownload()V

    .line 865
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 869
    :cond_2
    return-void
.end method
