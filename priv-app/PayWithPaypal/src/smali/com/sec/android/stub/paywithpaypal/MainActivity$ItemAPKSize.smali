.class Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
.super Ljava/lang/Object;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/stub/paywithpaypal/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ItemAPKSize"
.end annotation


# static fields
.field static final NOTI_STATE_FAILED:I = -0x1

.field static final NOTI_STATE_INIT:I = 0x1

.field static final NOTI_STATE_INSTALLED:I = 0x4

.field static final NOTI_STATE_INSTALLING:I = 0x3

.field static final NOTI_STATE_NONE:I = 0x0

.field static final NOTI_STATE_PROGRESS:I = 0x2


# instance fields
.field isDownloading:Z

.field isInstallLayout:Z

.field isInstalled:Z

.field isInstalling:Z

.field public mAppID:Ljava/lang/String;

.field public mRatio:I

.field public mResult:Ljava/lang/String;

.field public mSize:D

.field public mURL:Ljava/lang/String;

.field updateState:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "ID"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1270
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mSize:D

    .line 1271
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mResult:Ljava/lang/String;

    .line 1273
    iput v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mRatio:I

    .line 1275
    iput-boolean v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 1276
    iput-boolean v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    .line 1277
    iput-boolean v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalled:Z

    .line 1290
    iput v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->updateState:I

    .line 1293
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mAppID:Ljava/lang/String;

    .line 1294
    return-void
.end method
