.class public final Lcom/sec/android/stub/paywithpaypal/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/stub/paywithpaypal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final button_cancel_min_height:I = 0x7f06001a

.field public static final button_cancel_min_width:I = 0x7f06001b

.field public static final button_text_size:I = 0x7f060008

.field public static final check_box_charges_warning_do_not_show_again_margin_bottom:I = 0x7f060020

.field public static final check_box_charges_warning_do_not_show_again_margin_left:I = 0x7f060021

.field public static final check_box_charges_warning_do_not_show_again_padding_left:I = 0x7f060022

.field public static final install_downloading_margin_start:I = 0x7f060013

.field public static final install_downloading_size_margin_right:I = 0x7f060014

.field public static final install_layout_margin_top:I = 0x7f060009

.field public static final install_layout_progressing_margin_bottom:I = 0x7f06000d

.field public static final install_layout_progressing_margin_top:I = 0x7f06000c

.field public static final install_progressbar_margin_bottom:I = 0x7f060011

.field public static final install_progressbar_margin_left:I = 0x7f06000e

.field public static final install_progressbar_margin_right:I = 0x7f06000f

.field public static final install_progressbar_margin_top:I = 0x7f060010

.field public static final install_progressbar_min_height:I = 0x7f060012

.field public static final install_text_margin_start:I = 0x7f06000a

.field public static final install_title_height:I = 0x7f06000b

.field public static final layout_install_button_height:I = 0x7f060016

.field public static final layout_install_button_margin_top:I = 0x7f060019

.field public static final layout_install_button_min_height:I = 0x7f060018

.field public static final layout_install_button_min_width:I = 0x7f060017

.field public static final layout_install_button_width:I = 0x7f060015

.field public static final main_activity_padding_bottom:I = 0x7f060001

.field public static final main_activity_padding_left:I = 0x7f060002

.field public static final main_activity_padding_right:I = 0x7f060003

.field public static final main_activity_padding_top:I = 0x7f060004

.field public static final main_text_size:I = 0x7f060005

.field public static final popup_dialog_text_size:I = 0x7f060000

.field public static final state_text_size:I = 0x7f060007

.field public static final sub_text_size:I = 0x7f060006

.field public static final text_view_charges_warning_message_padding_bottom:I = 0x7f06001c

.field public static final text_view_charges_warning_message_padding_left:I = 0x7f06001d

.field public static final text_view_charges_warning_message_padding_right:I = 0x7f06001e

.field public static final text_view_charges_warning_message_padding_top:I = 0x7f06001f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
