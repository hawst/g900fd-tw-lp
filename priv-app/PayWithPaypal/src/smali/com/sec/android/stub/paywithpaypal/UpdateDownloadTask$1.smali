.class Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$1;
.super Ljava/lang/Object;
.source "UpdateDownloadTask.java"

# interfaces
.implements Lcom/sec/android/stub/paywithpaypal/OnInstalledPackaged;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->installApk()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;


# direct methods
.method constructor <init>(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)V
    .locals 0

    .prologue
    .line 1138
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$1;->this$0:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 3
    .param p1, "pkgname"    # Ljava/lang/String;
    .param p2, "returncode"    # I

    .prologue
    .line 1146
    const-string v0, "StubAppActivation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "returnCode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$1;->this$0:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    const-string v1, "RETURNCODE_ERROR"

    # invokes: Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    invoke-static {v0, v1, p2}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->access$000(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;Ljava/lang/String;I)V

    .line 1162
    :goto_0
    return-void

    .line 1156
    :cond_0
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$1;->this$0:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    # getter for: Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->_HFileUtil:Lcom/sec/android/stub/paywithpaypal/HFileUtil;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->access$100(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;)Lcom/sec/android/stub/paywithpaypal/HFileUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/HFileUtil;->delete()Z

    .line 1158
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask$1;->this$0:Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;

    const-string v1, "DOWNLOAD_STATUS"

    const/16 v2, 0x8

    # invokes: Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->sendMessage(Ljava/lang/String;I)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;->access$000(Lcom/sec/android/stub/paywithpaypal/UpdateDownloadTask;Ljava/lang/String;I)V

    goto :goto_0
.end method
