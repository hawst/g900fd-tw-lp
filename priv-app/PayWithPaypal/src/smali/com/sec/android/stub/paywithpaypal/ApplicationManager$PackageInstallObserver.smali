.class Lcom/sec/android/stub/paywithpaypal/ApplicationManager$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/stub/paywithpaypal/ApplicationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/stub/paywithpaypal/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/stub/paywithpaypal/ApplicationManager;)V
    .locals 0

    .prologue
    .line 810
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/stub/paywithpaypal/ApplicationManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 818
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/stub/paywithpaypal/ApplicationManager;

    # getter for: Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->onInstalledPackaged:Lcom/sec/android/stub/paywithpaypal/OnInstalledPackaged;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->access$000(Lcom/sec/android/stub/paywithpaypal/ApplicationManager;)Lcom/sec/android/stub/paywithpaypal/OnInstalledPackaged;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 826
    const/4 v1, 0x1

    if-eq p2, v1, :cond_0

    .line 846
    :cond_0
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/stub/paywithpaypal/ApplicationManager;

    iput-object p1, v1, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 850
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/stub/paywithpaypal/ApplicationManager;

    iput p2, v1, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->returncode:I

    .line 854
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/stub/paywithpaypal/ApplicationManager;

    # getter for: Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->access$100(Lcom/sec/android/stub/paywithpaypal/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 858
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/ApplicationManager$PackageInstallObserver;->this$0:Lcom/sec/android/stub/paywithpaypal/ApplicationManager;

    # getter for: Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/ApplicationManager;->access$100(Lcom/sec/android/stub/paywithpaypal/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 866
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    return-void
.end method
