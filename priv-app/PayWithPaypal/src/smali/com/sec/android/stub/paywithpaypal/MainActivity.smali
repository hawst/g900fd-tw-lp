.class public Lcom/sec/android/stub/paywithpaypal/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;,
        Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;,
        Lcom/sec/android/stub/paywithpaypal/MainActivity$ProgressTimeTask;
    }
.end annotation


# static fields
.field private static LOGGING_APP_ID:Ljava/lang/String; = null

.field private static final MFAC_APP_INDEX:I = 0x0

.field private static final NETWORKERR_DATAROAMING_OFF:I = 0x2

.field private static final NETWORKERR_FLIGHTMODE_ON:I = 0x0

.field private static final NETWORKERR_MOBILEDATA_OFF:I = 0x1

.field private static final NETWORKERR_NO_SIGNAL:I = 0x4

.field private static final NETWORKERR_REACHED_DATALIMIT:I = 0x3

.field private static final PAYPAL_APP_INDEX:I = 0x1

.field private static PKG_NAME_MFAC:Ljava/lang/String; = null

.field private static PKG_NAME_PAYPAL:Ljava/lang/String; = null

.field private static PKG_NAME_SAMSUNGAPPS:Ljava/lang/String; = null

.field public static final TAG:Ljava/lang/String; = "StubAppActivation"

.field private static apkSize:D

.field private static mListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;",
            ">;"
        }
    .end annotation
.end field

.field private static ratio_mfac:I

.field private static ratio_paypal:I


# instance fields
.field private MESSAGE_ID:Ljava/lang/String;

.field private chkButton_mfac:Landroid/widget/CheckBox;

.field private chkButton_mfac_text:Landroid/widget/TextView;

.field private chkButton_paypal:Landroid/widget/CheckBox;

.field private chkButton_paypal_text:Landroid/widget/TextView;

.field private isDataUsingPopupDone:Z

.field private isRoamingUsingPopupDone:Z

.field private mAppSize_mfac:D

.field private mAppSize_paypal:D

.field private mBtnCancel_mfac:Landroid/widget/Button;

.field private mBtnCancel_paypal:Landroid/widget/Button;

.field private mBtnUpdate:Landroid/widget/Button;

.field private mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mLayoutProgressing_mfac:Landroid/widget/RelativeLayout;

.field private mLayoutProgressing_paypal:Landroid/widget/RelativeLayout;

.field private mProgressBarInstall_mfac:Landroid/widget/ProgressBar;

.field private mProgressBarInstall_paypal:Landroid/widget/ProgressBar;

.field private mShowErrDialog:Landroid/app/AlertDialog;

.field private mStartFromFirst:Z

.field private mTextViewDownlodState_mfac:Landroid/widget/TextView;

.field private mTextViewDownlodState_paypal:Landroid/widget/TextView;

.field private mTextViewDownlodingSizeCurrent_mfac:Landroid/widget/TextView;

.field private mTextViewDownlodingSizeCurrent_paypal:Landroid/widget/TextView;

.field private mTextViewDownlodingSize_mfac:Landroid/widget/TextView;

.field private mTextViewDownlodingSize_paypal:Landroid/widget/TextView;

.field private mTvDownloading_mfac:Landroid/widget/TextView;

.field private mTvDownloading_paypal:Landroid/widget/TextView;

.field private versionCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    const-string v0, "com.noknok.android.framework.service"

    sput-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;

    .line 70
    const-string v0, "com.paypal.android.p2pmobile"

    sput-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;

    .line 71
    const-string v0, "com.sec.android.app.samsungapps"

    sput-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_SAMSUNGAPPS:Ljava/lang/String;

    .line 72
    const-string v0, "com.samsung.android.fingerprint.service"

    sput-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->LOGGING_APP_ID:Ljava/lang/String;

    .line 125
    sput v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I

    .line 126
    sput v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 83
    iput-boolean v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z

    .line 84
    iput-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;

    .line 88
    const-string v0, "id of the message"

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->MESSAGE_ID:Ljava/lang/String;

    .line 130
    iput-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mShowErrDialog:Landroid/app/AlertDialog;

    .line 131
    iput-boolean v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isDataUsingPopupDone:Z

    .line 132
    iput-boolean v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingUsingPopupDone:Z

    .line 134
    new-instance v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$1;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    iput-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mHandler:Landroid/os/Handler;

    .line 1268
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->MESSAGE_ID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_paypal:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-static {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/stub/paywithpaypal/MainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getNetworkErrorState()I

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/stub/paywithpaypal/MainActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->sendBroadcastForNetworkErrorPopup(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mStartFromFirst:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setLayout()V

    return-void
.end method

.method static synthetic access$1900()I
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I

    return v0
.end method

.method static synthetic access$1902(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 65
    sput p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I

    return p0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000()I
    .locals 1

    .prologue
    .line 65
    sget v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I

    return v0
.end method

.method static synthetic access$2002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 65
    sput p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I

    return p0
.end method

.method static synthetic access$2100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D

    return-wide v0
.end method

.method static synthetic access$2102(Lcom/sec/android/stub/paywithpaypal/MainActivity;D)D
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # D

    .prologue
    .line 65
    iput-wide p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D

    return-wide p1
.end method

.method static synthetic access$2200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_mfac:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D

    return-wide v0
.end method

.method static synthetic access$2302(Lcom/sec/android/stub/paywithpaypal/MainActivity;D)D
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # D

    .prologue
    .line 65
    iput-wide p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D

    return-wide p1
.end method

.method static synthetic access$2400(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_paypal:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->showErrorDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2600()D
    .locals 2

    .prologue
    .line 65
    sget-wide v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->apkSize:D

    return-wide v0
.end method

.method static synthetic access$2602(D)D
    .locals 0
    .param p0, "x0"    # D

    .prologue
    .line 65
    sput-wide p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->apkSize:D

    return-wide p0
.end method

.method static synthetic access$2700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSize_mfac:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSize_paypal:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->versionCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V

    return-void
.end method

.method static synthetic access$3000()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_SAMSUNGAPPS:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$3400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->LOGGING_APP_ID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # J

    .prologue
    .line 65
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isDataUsingPopupDone:Z

    return v0
.end method

.method static synthetic access$3602(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isDataUsingPopupDone:Z

    return p1
.end method

.method static synthetic access$3700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingUsingPopupDone:Z

    return v0
.end method

.method static synthetic access$3702(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingUsingPopupDone:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->showDataUsingDialog()V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/stub/paywithpaypal/MainActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/stub/paywithpaypal/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/stub/paywithpaypal/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_mfac:Landroid/widget/TextView;

    return-object v0
.end method

.method public static getItem(Ljava/lang/String;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    .locals 3
    .param p0, "appID"    # Ljava/lang/String;

    .prologue
    .line 1305
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    .line 1306
    .local v1, "item":Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mAppID:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$4300(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1311
    .end local v1    # "item":Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getNetworkErrorState()I
    .locals 2

    .prologue
    .line 939
    const/4 v0, -0x1

    .line 940
    .local v0, "networkStatus":I
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isWifiOnlyModel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 941
    const/4 v0, 0x4

    .line 954
    :goto_0
    return v0

    .line 942
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isFligtMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 943
    const/4 v0, 0x0

    goto :goto_0

    .line 944
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isMobileDataOff()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 945
    const/4 v0, 0x1

    goto :goto_0

    .line 946
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingOff()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 947
    const/4 v0, 0x2

    goto :goto_0

    .line 948
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isReachToDataLimit()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 949
    const/4 v0, 0x3

    goto :goto_0

    .line 951
    :cond_4
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public static getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1095
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1096
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private init()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 450
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    .line 451
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;

    const-string v3, "NOKNOK"

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;

    const-string v3, "PAYPAL"

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    const-string v0, "DOWNLOAD_RATIO_MFAC"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 455
    const-string v0, "DOWNLOAD_RATIO_PAYPAL"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 457
    return-void
.end method

.method private insertLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 4
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "value"    # J

    .prologue
    .line 1345
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v3, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1346
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1347
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348
    const-string v2, "feature"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1349
    const-string v2, "extra"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    const-string v2, "value"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1352
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1354
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1355
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1356
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1358
    invoke-virtual {p0, v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1360
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private isFligtMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1036
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isMobileDataOff()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1041
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1043
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 1047
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static isNetworkConnected(Landroid/content/Context;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 977
    const/4 v0, 0x0

    .line 979
    .local v0, "bIsConnected":Z
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 981
    .local v3, "manager":Landroid/net/ConnectivityManager;
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 982
    .local v4, "mobile":Landroid/net/NetworkInfo;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v5

    .line 983
    .local v5, "wifi":Landroid/net/NetworkInfo;
    const/4 v6, 0x7

    invoke-virtual {v3, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 984
    .local v1, "bluetooth":Landroid/net/NetworkInfo;
    const/16 v6, 0x9

    invoke-virtual {v3, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 986
    .local v2, "ethernet":Landroid/net/NetworkInfo;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-nez v6, :cond_3

    :cond_0
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-nez v6, :cond_3

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 988
    :cond_3
    const/4 v0, 0x1

    .line 993
    :goto_0
    return v0

    .line 990
    :cond_4
    const-string v6, "StubAppActivation"

    const-string v7, "isNetworkConnected : network error"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isReachToDataLimit()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1059
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1060
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-nez v0, :cond_1

    .line 1063
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->isMobilePolicyDataEnable()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isRoamingOff()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1051
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1052
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_1

    .line 1055
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataRoamingEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isWifiOnlyModel()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1026
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1027
    .local v0, "cm":Landroid/net/ConnectivityManager;
    if-eqz v0, :cond_0

    .line 1028
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1032
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static putPref(Ljava/lang/String;Ljava/lang/Double;Landroid/content/Context;)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Double;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1088
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1089
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1090
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1091
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1092
    return-void
.end method

.method public static putPref(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1081
    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1082
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1083
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1084
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1085
    return-void
.end method

.method private sendBroadcastForNetworkErrorPopup(I)V
    .locals 5
    .param p1, "status"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 958
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z
    invoke-static {v2, v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    .line 959
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mNeedUpdate:Z
    invoke-static {v2, v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$602(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    .line 961
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v2

    iput-boolean v3, v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 962
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v2

    iput-boolean v3, v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 964
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 965
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.popupuireceiver"

    const-string v3, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 967
    const-string v2, "network_err_type"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 970
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 974
    :goto_0
    return-void

    .line 971
    :catch_0
    move-exception v0

    .line 972
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "StubAppActivation"

    const-string v3, "ActivityNotFoundException, No Activity found for - com.sec.android.app.popupuireceiver"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setInstallLayout_mfac(Z)V
    .locals 3
    .param p1, "bIsInstalling"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1067
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mLayoutProgressing_mfac:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1068
    if-nez p1, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 1071
    :cond_0
    return-void

    .line 1067
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private setInstallLayout_paypal(Z)V
    .locals 3
    .param p1, "bIsInstalling"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1074
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mLayoutProgressing_paypal:Landroid/widget/RelativeLayout;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1075
    if-nez p1, :cond_0

    .line 1076
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 1078
    :cond_0
    return-void

    .line 1074
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private setLayout()V
    .locals 10

    .prologue
    .line 461
    const v1, 0x7f040001

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setContentView(I)V

    .line 462
    iput-object p0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    .line 464
    const v1, 0x7f090019

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    .line 469
    const v1, 0x7f090006

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    .line 470
    const v1, 0x7f090010

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    .line 472
    const v1, 0x7f090007

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac_text:Landroid/widget/TextView;

    .line 473
    const v1, 0x7f090011

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal_text:Landroid/widget/TextView;

    .line 475
    const v1, 0x7f09000c

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_mfac:Landroid/widget/TextView;

    .line 476
    const v1, 0x7f090016

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_paypal:Landroid/widget/TextView;

    .line 481
    const v1, 0x7f09000d

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSize_mfac:Landroid/widget/TextView;

    .line 482
    const v1, 0x7f090017

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSize_paypal:Landroid/widget/TextView;

    .line 484
    const v1, 0x7f09000e

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_mfac:Landroid/widget/TextView;

    .line 485
    const v1, 0x7f090018

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_paypal:Landroid/widget/TextView;

    .line 487
    const v1, 0x7f09000b

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;

    .line 488
    const v1, 0x7f090015

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;

    .line 490
    const v1, 0x7f09000a

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mLayoutProgressing_mfac:Landroid/widget/RelativeLayout;

    .line 491
    const v1, 0x7f090014

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mLayoutProgressing_paypal:Landroid/widget/RelativeLayout;

    .line 493
    const v1, 0x7f090009

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodState_mfac:Landroid/widget/TextView;

    .line 494
    const v1, 0x7f090013

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodState_paypal:Landroid/widget/TextView;

    .line 496
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->setInstalledInfo()V

    .line 497
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->setInstalledInfo()V

    .line 499
    new-instance v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$4;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    .line 529
    .local v0, "chkButtonClick":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateCheckBoxCheckedState()V

    .line 530
    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateCheckBoxEnableState()V

    .line 531
    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->updateBtnState()V

    .line 533
    const-string v1, "DOWNLOAD_RATIO_MFAC"

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 534
    const-string v1, "DOWNLOAD_RATIO_MFAC"

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I

    .line 536
    :cond_0
    const-string v1, "DOWNLOAD_RATIO_PAYPAL"

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 537
    const-string v1, "DOWNLOAD_RATIO_PAYPAL"

    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPref(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I

    .line 540
    :cond_1
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v1

    iget-wide v2, v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mSize:D

    iput-wide v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D

    .line 541
    iget-wide v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D

    .line 542
    iget-wide v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_6

    .line 544
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_mfac:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    div-double/2addr v6, v8

    sget v8, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I

    int-to-double v8, v8

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f07000a

    invoke-virtual {p0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 545
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSize_mfac:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " / %.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_mfac:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f07000a

    invoke-virtual {p0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 546
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;

    sget v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 553
    :goto_0
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v1

    iget-wide v2, v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->mSize:D

    iput-wide v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D

    .line 554
    iget-wide v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D

    .line 555
    iget-wide v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_7

    .line 558
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSizeCurrent_paypal:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    div-double/2addr v6, v8

    sget v8, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I

    int-to-double v8, v8

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f07000a

    invoke-virtual {p0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 559
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodingSize_paypal:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " / %.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mAppSize_paypal:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f07000a

    invoke-virtual {p0, v3}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 560
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;

    sget v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 567
    :goto_1
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    if-eqz v1, :cond_2

    .line 568
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 570
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_mfac:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 572
    :cond_2
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    if-eqz v1, :cond_3

    .line 573
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 575
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTvDownloading_paypal:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 578
    :cond_3
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->getInstalledInfo()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 580
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac_text:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<u>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac_text:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</u>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 581
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodState_mfac:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    :goto_2
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->getInstalledInfo()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 589
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal_text:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<u>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal_text:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</u>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 590
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodState_paypal:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 595
    :goto_3
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac_text:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal_text:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 598
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac_text:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 599
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal_text:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 601
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-nez v1, :cond_4

    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    if-eqz v1, :cond_a

    .line 602
    :cond_4
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V

    .line 606
    :goto_4
    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-nez v1, :cond_5

    sget-object v1, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v1

    iget-boolean v1, v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    if-eqz v1, :cond_b

    .line 607
    :cond_5
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V

    .line 613
    :goto_5
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    new-instance v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$5;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 706
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$6;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 714
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$7;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 749
    return-void

    .line 549
    :cond_6
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;

    sget v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_mfac:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 550
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_mfac:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    goto/16 :goto_0

    .line 563
    :cond_7
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;

    sget v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->ratio_paypal:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 564
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mProgressBarInstall_paypal:Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    goto/16 :goto_1

    .line 583
    :cond_8
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac_text:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodState_mfac:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 592
    :cond_9
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal_text:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 593
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mTextViewDownlodState_paypal:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 604
    :cond_a
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_mfac(Z)V

    goto/16 :goto_4

    .line 609
    :cond_b
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setInstallLayout_paypal(Z)V

    goto/16 :goto_5
.end method

.method private showDataUsingDialog()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    .line 822
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 823
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 824
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/high16 v4, 0x7f040000

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 825
    .local v3, "view":Landroid/view/View;
    const/high16 v4, 0x7f090000

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 826
    .local v2, "tvChargesWarning":Landroid/widget/TextView;
    const v4, 0x7f090001

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mCheckBoxDoNotShowAgain:Landroid/widget/CheckBox;

    .line 827
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 829
    iget-object v4, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 830
    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isRoamingConnected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 831
    const-string v4, "StubAppActivation"

    const-string v5, "usingMobileData"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    const v4, 0x7f07000e

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 833
    const v4, 0x7f070010

    invoke-virtual {p0, v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 834
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 835
    const v4, 0x104000a

    new-instance v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$8;

    invoke-direct {v5, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$8;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 873
    :goto_0
    const/high16 v4, 0x1040000

    new-instance v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$10;

    invoke-direct {v5, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$10;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 881
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 882
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 884
    :cond_0
    return-void

    .line 851
    :cond_1
    const-string v4, "StubAppActivation"

    const-string v5, "usingRoamingData"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    const v4, 0x7f07000f

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 853
    const v4, 0x7f070011

    invoke-virtual {p0, v4}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 854
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 855
    const v4, 0x7f07000d

    new-instance v5, Lcom/sec/android/stub/paywithpaypal/MainActivity$9;

    invoke-direct {v5, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$9;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private showErrorDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "stringId"    # Ljava/lang/String;

    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 438
    :goto_0
    return-void

    .line 421
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 422
    .local v0, "build":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f070013

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/android/stub/paywithpaypal/MainActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$2;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 428
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mShowErrDialog:Landroid/app/AlertDialog;

    .line 429
    iget-object v1, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mShowErrDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 431
    new-instance v1, Lcom/sec/android/stub/paywithpaypal/MainActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$3;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private showExitConfirmDialog()V
    .locals 3

    .prologue
    .line 1100
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070019

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07001b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$12;

    invoke-direct {v2, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$12;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$11;

    invoke-direct {v2, p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$11;-><init>(Lcom/sec/android/stub/paywithpaypal/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1117
    return-void
.end method


# virtual methods
.method public getItem(I)Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1298
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 1299
    const/4 v0, 0x0

    .line 1301
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    goto :goto_0
.end method

.method public isInstalledPkg(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1332
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/16 v3, 0x4000

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 1333
    .local v0, "info":Landroid/content/pm/PackageInfo;
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "1.0"

    iget-object v3, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 1340
    .end local v0    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return v1

    .line 1335
    .restart local v0    # "info":Landroid/content/pm/PackageInfo;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 1336
    .end local v0    # "info":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected isRoamingConnected()Z
    .locals 5

    .prologue
    .line 1010
    const/4 v0, 0x0

    .line 1011
    .local v0, "bIsConnected":Z
    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1013
    .local v1, "manager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1015
    .local v2, "mobile":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1016
    const/4 v0, 0x1

    .line 1019
    :cond_0
    return v0
.end method

.method protected isWifiNetworkConnected()Z
    .locals 5

    .prologue
    .line 997
    const/4 v2, 0x0

    .line 998
    .local v2, "ret":Z
    iget-object v3, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1001
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1002
    .local v1, "info":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1003
    const/4 v2, 0x1

    .line 1006
    :cond_0
    return v2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1121
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1122
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setLayout()V

    .line 1125
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 443
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 444
    iput-object p0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    .line 445
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->init()V

    .line 446
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setLayout()V

    .line 447
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 918
    const-string v0, "StubAppActivation"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iput-boolean v2, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 920
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->cancelDownload()V

    .line 921
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iput-boolean v2, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    .line 922
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->cancelDownload()V

    .line 923
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 924
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 1142
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-eqz v0, :cond_1

    .line 1145
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->showExitConfirmDialog()V

    move v0, v1

    .line 1149
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 3
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 1130
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x102002c

    if-ne v0, v2, :cond_1

    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-eqz v0, :cond_1

    .line 1133
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->showExitConfirmDialog()V

    move v0, v1

    .line 1136
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 929
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 934
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 931
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 932
    const/4 v0, 0x1

    goto :goto_0

    .line 929
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 907
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 908
    invoke-direct {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->setLayout()V

    .line 914
    return-void
.end method

.method public setDownloadCheckValue(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 752
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$4000(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z
    invoke-static {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$4002(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    .line 759
    :goto_0
    return-void

    .line 756
    :cond_0
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z
    invoke-static {v0, v1}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$4002(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;Z)Z

    goto :goto_0
.end method

.method public updateBtnState()V
    .locals 5

    .prologue
    const v2, 0x7f070006

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 762
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 763
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 764
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 790
    :cond_0
    :goto_0
    return-void

    .line 768
    :cond_1
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 769
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 771
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 773
    :cond_2
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 777
    :cond_3
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 778
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    if-eqz v0, :cond_0

    .line 780
    :cond_4
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 786
    :cond_5
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 787
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mBtnUpdate:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateCheckBoxCheckedState()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 793
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$4000(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 795
    :cond_0
    sget-object v0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownCheck:Z
    invoke-static {v0}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$4000(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 796
    iget-object v0, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 797
    :cond_1
    return-void
.end method

.method public updateCheckBoxEnableState()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 800
    const/4 v0, 0x0

    .line 801
    .local v0, "isInstalledMfac":Z
    const/4 v1, 0x0

    .line 803
    .local v1, "isInstalledPaypal":Z
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 804
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 806
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isDownloading:Z

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->mListItem:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;

    # getter for: Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->mDownloadInfo:Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;
    invoke-static {v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;->access$100(Lcom/sec/android/stub/paywithpaypal/MainActivity$ListItem;)Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/android/stub/paywithpaypal/MainActivity$ItemAPKSize;->isInstalling:Z

    if-eqz v2, :cond_1

    .line 808
    :cond_0
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 809
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 812
    :cond_1
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_MFAC:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v0

    .line 813
    sget-object v2, Lcom/sec/android/stub/paywithpaypal/MainActivity;->PKG_NAME_PAYPAL:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/sec/android/stub/paywithpaypal/MainActivity;->isInstalledPkg(Ljava/lang/String;)Z

    move-result v1

    .line 815
    if-eqz v0, :cond_2

    .line 816
    iget-object v5, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_mfac:Landroid/widget/CheckBox;

    if-nez v0, :cond_4

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 817
    :cond_2
    if-eqz v1, :cond_3

    .line 818
    iget-object v2, p0, Lcom/sec/android/stub/paywithpaypal/MainActivity;->chkButton_paypal:Landroid/widget/CheckBox;

    if-nez v1, :cond_5

    :goto_1
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 819
    :cond_3
    return-void

    :cond_4
    move v2, v4

    .line 816
    goto :goto_0

    :cond_5
    move v3, v4

    .line 818
    goto :goto_1
.end method
