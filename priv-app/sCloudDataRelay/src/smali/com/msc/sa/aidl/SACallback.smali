.class public Lcom/msc/sa/aidl/SACallback;
.super Lcom/msc/sa/aidl/ISACallback$Stub;
.source "SACallback.java"


# instance fields
.field context:Landroid/content/Context;

.field mCountrtyCode:Ljava/lang/String;

.field mErrorCode:Ljava/lang/String;

.field mResultAccessTokenValue:Ljava/lang/String;

.field mUserId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/msc/sa/aidl/ISACallback$Stub;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/msc/sa/aidl/SACallback;->context:Landroid/content/Context;

    .line 22
    return-void
.end method


# virtual methods
.method public onReceiveAccessToken(IZLandroid/os/Bundle;)V
    .locals 5
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 28
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 29
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 31
    const-string v2, "access_token"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/msc/sa/aidl/SACallback;->mResultAccessTokenValue:Ljava/lang/String;

    .line 32
    const-string v2, "user_id"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/msc/sa/aidl/SACallback;->mUserId:Ljava/lang/String;

    .line 33
    const-string v2, "cc"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/msc/sa/aidl/SACallback;->mCountrtyCode:Ljava/lang/String;

    .line 35
    const-string v2, "access_token"

    iget-object v3, p0, Lcom/msc/sa/aidl/SACallback;->mResultAccessTokenValue:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    const-string v2, "user_id"

    iget-object v3, p0, Lcom/msc/sa/aidl/SACallback;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    const-string v2, "country_code"

    iget-object v3, p0, Lcom/msc/sa/aidl/SACallback;->mCountrtyCode:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    const-string v2, "result_code"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 39
    invoke-static {v4}, Lcom/samsung/android/scloud/auth/task/TokenTask;->getInstance(I)Lcom/samsung/android/scloud/auth/task/TokenTask;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/aidl/SACallback;->context:Landroid/content/Context;

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/scloud/auth/task/TokenTask;->onTrigger(Landroid/content/Context;Landroid/content/Intent;)V

    .line 52
    :goto_0
    return-void

    .line 43
    :cond_0
    const-string v2, "error_code"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/msc/sa/aidl/SACallback;->mErrorCode:Ljava/lang/String;

    .line 45
    iget-object v2, p0, Lcom/msc/sa/aidl/SACallback;->mErrorCode:Ljava/lang/String;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 46
    .local v1, "rCode":I
    const-string v2, "result_code"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 48
    invoke-static {v4}, Lcom/samsung/android/scloud/auth/task/TokenTask;->getInstance(I)Lcom/samsung/android/scloud/auth/task/TokenTask;

    move-result-object v2

    iget-object v3, p0, Lcom/msc/sa/aidl/SACallback;->context:Landroid/content/Context;

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/scloud/auth/task/TokenTask;->onTrigger(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onReceiveAuthCode(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 73
    return-void
.end method

.method public onReceiveChecklistValidation(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 59
    return-void
.end method

.method public onReceiveDisclaimerAgreement(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 66
    return-void
.end method

.method public onReceiveSCloudAccessToken(IZLandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 80
    return-void
.end method
