.class Lcom/samsung/android/scloud/auth/task/TokenTask$2;
.super Ljava/lang/Object;
.source "TokenTask.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/task/TokenTask;->bindToSA(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/auth/task/TokenTask;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$mapId:I

.field final synthetic val$trigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/auth/task/TokenTask;Ljava/lang/String;ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/TokenTask;

    iput-object p2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$ctid:Ljava/lang/String;

    iput p3, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$mapId:I

    iput-object p4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$trigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    iput-object p5, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$context:Landroid/content/Context;

    iput-object p6, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/TokenTask;

    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    .line 137
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/TokenTask;

    iget-object v0, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    if-nez v0, :cond_0

    .line 138
    # getter for: Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/auth/task/TokenTask;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=======CANNOT BIND TO SAMSUNG ACCOUNT==========, CTID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$ctid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :goto_0
    return-void

    .line 141
    :cond_0
    # getter for: Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/auth/task/TokenTask;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=======SUCCESS BIND TO SAMSUNG ACCOUNT==========, CTID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$ctid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/TokenTask;

    iget v1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$mapId:I

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$trigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/samsung/android/scloud/auth/task/TokenTask;->prepareWithBindedServie(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/auth/task/TokenTask;->access$200(Lcom/samsung/android/scloud/auth/task/TokenTask;ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 130
    # getter for: Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/auth/task/TokenTask;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=======onServiceDisconnected==========, CTID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->val$ctid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/TokenTask;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    .line 132
    return-void
.end method
