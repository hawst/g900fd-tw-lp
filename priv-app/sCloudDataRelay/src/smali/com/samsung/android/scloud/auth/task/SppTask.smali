.class public Lcom/samsung/android/scloud/auth/task/SppTask;
.super Ljava/lang/Object;
.source "SppTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/ITaskHandler;


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/auth/task/SppTask; = null

.field private static final TAG:Ljava/lang/String; = "SppTask"

.field private static maxTimeOut:J

.field public static sppTask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCtid:I

.field private mIntent:Landroid/content/Intent;

.field private mService:Lcom/sec/spp/push/IPushClientService;

.field private mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

.field private pushClientConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-wide/32 v0, 0xc350

    sput-wide v0, Lcom/samsung/android/scloud/auth/task/SppTask;->maxTimeOut:J

    .line 39
    new-instance v0, Lcom/samsung/android/scloud/auth/task/SppTask$1;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/task/SppTask$1;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/task/SppTask;->sppTask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 19
    sget-wide v0, Lcom/samsung/android/scloud/auth/task/SppTask;->maxTimeOut:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/auth/task/SppTask;)Lcom/sec/spp/push/IPushClientService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/auth/task/SppTask;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/scloud/auth/task/SppTask;Lcom/sec/spp/push/IPushClientService;)Lcom/sec/spp/push/IPushClientService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/auth/task/SppTask;
    .param p1, "x1"    # Lcom/sec/spp/push/IPushClientService;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/auth/task/SppTask;ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/auth/task/SppTask;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "x3"    # Landroid/content/Context;
    .param p4, "x4"    # Landroid/content/Intent;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/scloud/auth/task/SppTask;->prepareWithBindedServie(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private bindToSpp(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 8
    .param p1, "mapId"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 131
    const-string v0, "SppTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bindToSpp - "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 134
    .local v7, "mintent":Landroid/content/Intent;
    const-string v0, "ctid"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "ctid":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/scloud/auth/task/SppTask$2;

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/scloud/auth/task/SppTask$2;-><init>(Lcom/samsung/android/scloud/auth/task/SppTask;Ljava/lang/String;ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->pushClientConnection:Landroid/content/ServiceConnection;

    .line 160
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->pushClientConnection:Landroid/content/ServiceConnection;

    const/4 v1, 0x1

    invoke-virtual {p3, v7, v0, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    return v0
.end method

.method private clear()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 113
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->pushClientConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v1, :cond_0

    .line 115
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->pushClientConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    .line 121
    iput-object v3, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->pushClientConnection:Landroid/content/ServiceConnection;

    .line 123
    iput-object v3, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    .line 124
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mCtid:I

    .line 125
    iput-object v3, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mContext:Landroid/content/Context;

    .line 126
    iput-object v3, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mIntent:Landroid/content/Intent;

    .line 127
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "SppTask"

    const-string v2, "Illegal Argument Exception when unbinding service"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(I)Lcom/samsung/android/scloud/auth/task/SppTask;
    .locals 2
    .param p0, "cmd"    # I

    .prologue
    .line 52
    const-class v1, Lcom/samsung/android/scloud/auth/task/SppTask;

    monitor-enter v1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_1

    .line 53
    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/auth/task/SppTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/SppTask;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/samsung/android/scloud/auth/task/SppTask;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/task/SppTask;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/task/SppTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/SppTask;

    .line 56
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/auth/task/SppTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/SppTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/samsung/android/scloud/auth/task/SppTask;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/task/SppTask;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private prepareWithBindedServie(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "mapId"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 167
    const-string v3, "command"

    const/4 v4, -0x1

    invoke-virtual {p4, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 168
    .local v0, "cmd":I
    const-string v3, "ctid"

    invoke-virtual {p4, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 169
    .local v1, "ctid":Ljava/lang/String;
    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 170
    const-string v3, "SppTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "=======REGISTER TO SPP=======, CTID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    const-string v4, "tj9u972o46"

    invoke-virtual {p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/sec/spp/push/IPushClientService;->registration(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v2

    .line 176
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 179
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-interface {p2, p1, p3, p4}, Lcom/samsung/android/scloud/auth/core/ITaskTrigger;->prepared(ILandroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private registrationResult(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 245
    new-instance v4, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct {v4}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 246
    .local v4, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    const-string v5, "ctid"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "ctid":Ljava/lang/String;
    const-string v5, "com.sec.spp.Status"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 250
    .local v3, "registrationState":I
    const-string v5, "SppTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "PUSH_REGISTRATION STATUS : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    packed-switch v3, :pswitch_data_0

    .line 281
    const-string v5, "SppTask"

    const-string v6, "======= UnKnown PUSH_MESSAGE TO SPP ======="

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :goto_0
    return-object v4

    .line 254
    :pswitch_0
    const-string v5, "SppTask"

    const-string v6, "======= PUSH_REGISTRATION_SUCCESS TO SPP ======="

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    const-string v6, "tj9u972o46"

    invoke-interface {v5, v6}, Lcom/sec/spp/push/IPushClientService;->getRegId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 257
    .local v2, "regId":Ljava/lang/String;
    const-string v5, "reg_id"

    invoke-virtual {v4, v5, v2}, Lcom/samsung/android/scloud/auth/core/TaskResult;->putStringData(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 259
    .end local v2    # "regId":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 260
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 266
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_1
    const/16 v5, 0x3e9

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V

    .line 267
    const-string v5, "SppTask"

    const-string v6, "======= PUSH_REGISTRATION_FAIL TO SPP ======="

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :pswitch_2
    const-string v5, "SppTask"

    const-string v6, "======= PUSH_DE-REGISTRATION_FAIL TO SPP ======="

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :pswitch_3
    const/16 v5, 0x3ea

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V

    .line 277
    const-string v5, "SppTask"

    const-string v6, "======= PUSH_DEREGISTRATION_FAIL TO SPP ======="

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 252
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private sppTask(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    .line 184
    new-instance v7, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct {v7}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 185
    .local v7, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    const/4 v1, -0x1

    .line 188
    .local v1, "cmd":I
    :try_start_0
    const-string v8, "command"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 189
    sparse-switch v1, :sswitch_data_0

    .line 240
    :cond_0
    :goto_0
    return-object v7

    .line 191
    :sswitch_0
    iget-object v8, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v8, :cond_1

    .line 192
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/auth/task/SppTask;->registrationResult(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v7

    goto :goto_0

    .line 194
    :cond_1
    const-string v8, "SppTask"

    const-string v9, "sppTask: COMMAND: 2, but mService is Null, this request will be return false~!"

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 234
    :catch_0
    move-exception v2

    .line 235
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 236
    invoke-virtual {v7, v11}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    .line 237
    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setResultMsg(Ljava/lang/String;)V

    goto :goto_0

    .line 201
    .end local v2    # "e":Landroid/os/RemoteException;
    :sswitch_1
    :try_start_1
    const-string v8, "SppTask"

    const-string v9, "=======DEREGIST SPP======="

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v8, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v8, :cond_0

    .line 203
    iget-object v8, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    const-string v9, "tj9u972o46"

    invoke-interface {v8, v9}, Lcom/sec/spp/push/IPushClientService;->deregistration(Ljava/lang/String;)V

    .line 204
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    goto :goto_0

    .line 208
    :sswitch_2
    iget-object v8, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v8, :cond_0

    .line 209
    const-string v8, "SppTask"

    const-string v9, "=======NOTIFY ACK SPP======="

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v8, "appId"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 212
    .local v3, "mAppId":Ljava/lang/String;
    const-string v8, "msg"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 214
    .local v5, "mMessage":Ljava/lang/String;
    const-string v8, "ack"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 216
    .local v0, "bAck":Z
    const-string v8, "appData"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 218
    .local v4, "mData":Ljava/lang/String;
    const-string v8, "notificationId"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 220
    .local v6, "mNoti":Ljava/lang/String;
    const-string v8, "SppTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Received MSG = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v8, "SppTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Received APP_ID = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v8, "SppTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Received APP Data = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v8, "SppTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Required ackowldgement? = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v8, "SppTask"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Received Notification ID = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v8, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ";"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/sec/spp/push/IPushClientService;->ackNotification(Ljava/lang/String;)V

    .line 227
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 189
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x9 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public doJob(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/auth/task/SppTask;->sppTask(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v0

    .line 96
    .local v0, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-direct {p0}, Lcom/samsung/android/scloud/auth/task/SppTask;->clear()V

    .line 98
    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "SppTask"

    return-object v0
.end method

.method public hasPrepare()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    return v0
.end method

.method public onTimeout(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/samsung/android/scloud/auth/task/SppTask;->clear()V

    .line 69
    return-void
.end method

.method public declared-synchronized onTrigger(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    const-string v0, "SppTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrigger mTrigger = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;

    if-eqz v0, :cond_0

    .line 106
    iput-object p2, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mIntent:Landroid/content/Intent;

    .line 107
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    iget v1, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mCtid:I

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mIntent:Landroid/content/Intent;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/auth/core/ITaskTrigger;->prepared(ILandroid/content/Context;Landroid/content/Intent;)V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :cond_0
    monitor-exit p0

    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3
    .param p1, "ctid"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 79
    iput-object p3, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mContext:Landroid/content/Context;

    .line 80
    const-string v1, "command"

    const/4 v2, -0x1

    invoke-virtual {p4, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 82
    .local v0, "cmd":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 83
    iput-object p2, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    .line 84
    iput p1, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mCtid:I

    .line 85
    iput-object p4, p0, Lcom/samsung/android/scloud/auth/task/SppTask;->mIntent:Landroid/content/Intent;

    .line 88
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/scloud/auth/task/SppTask;->bindToSpp(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method
