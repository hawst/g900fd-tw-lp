.class final Lcom/samsung/android/scloud/auth/core/TaskHelper$3;
.super Ljava/lang/Object;
.source "TaskHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/core/TaskHelper;->doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$listener:Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

.field final synthetic val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

.field final synthetic val$taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/auth/core/TaskObject;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iput-object p2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    iput-object p3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$intent:Landroid/content/Intent;

    iput-object p5, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    iput-object p6, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$listener:Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 122
    const-string v1, "TaskHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doTask-excute prepare - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v3}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->DEFAULT_TASK_TRIGGER:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$200()Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$context:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$intent:Landroid/content/Intent;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    .line 124
    .local v0, "isSuc":Z
    if-nez v0, :cond_1

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    invoke-interface {v1}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getMaxRetryCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v1, v1, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    invoke-interface {v2}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getMaxRetryCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 126
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v2, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    .line 127
    const-string v1, "TaskHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doTask-retry prepare - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v3}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->DEFAULT_TASK_TRIGGER:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$200()Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$context:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$intent:Landroid/content/Intent;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    :cond_1
    if-nez v0, :cond_2

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135
    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const-string v1, "TaskHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Remove from TaskMap - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v3}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", retryCnt : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$listener:Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_PREPARE_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$300()Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;->val$intent:Landroid/content/Intent;

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;->onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V

    .line 139
    :cond_2
    return-void
.end method
