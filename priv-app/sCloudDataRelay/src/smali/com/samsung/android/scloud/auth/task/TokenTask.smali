.class public Lcom/samsung/android/scloud/auth/task/TokenTask;
.super Ljava/lang/Object;
.source "TokenTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/ITaskHandler;


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/auth/task/TokenTask;

.field public static RequestTokenTask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

.field private static TAG:Ljava/lang/String;

.field private static maxTimeOut:J


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCtid:I

.field private mIntent:Landroid/content/Intent;

.field private mRegistrationCode:Ljava/lang/String;

.field private mSACallback:Lcom/msc/sa/aidl/ISACallback;

.field mService:Lcom/msc/sa/aidl/ISAService;

.field private mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

.field private tokenClientConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    const-string v0, "TokenTask"

    sput-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    .line 42
    const-wide/32 v0, 0xea60

    sput-wide v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->maxTimeOut:J

    .line 43
    new-instance v0, Lcom/samsung/android/scloud/auth/task/TokenTask$1;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/task/TokenTask$1;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->RequestTokenTask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 26
    sget-wide v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->maxTimeOut:J

    return-wide v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/auth/task/TokenTask;ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/auth/task/TokenTask;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "x3"    # Landroid/content/Context;
    .param p4, "x4"    # Landroid/content/Intent;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/scloud/auth/task/TokenTask;->prepareWithBindedServie(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private bindToSA(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 8
    .param p1, "mapId"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 121
    sget-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bindToSA - mapId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v0, "ctid"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "ctid":Ljava/lang/String;
    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    .local v7, "mintent":Landroid/content/Intent;
    new-instance v0, Lcom/samsung/android/scloud/auth/task/TokenTask$2;

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/scloud/auth/task/TokenTask$2;-><init>(Lcom/samsung/android/scloud/auth/task/TokenTask;Ljava/lang/String;ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->tokenClientConnection:Landroid/content/ServiceConnection;

    .line 147
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->tokenClientConnection:Landroid/content/ServiceConnection;

    const/4 v1, 0x1

    invoke-virtual {p3, v7, v0, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    return v0
.end method

.method private clear(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 220
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->tokenClientConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v1, :cond_0

    .line 222
    :try_start_0
    sget-object v1, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "=======UNREGISTER CALLBACK, REGISTRAGIONCODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mRegistrationCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 224
    sget-object v1, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    const-string v2, "=======UNREGISTER CALLBACK SUCCESS======="

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 230
    sget-object v1, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    const-string v2, "=======UNBIND SERVICE TO SA======="

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->tokenClientConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 240
    :cond_0
    :goto_1
    iput-object v4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    .line 241
    iput-object v4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    .line 242
    iput-object v4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->tokenClientConnection:Landroid/content/ServiceConnection;

    .line 243
    iput-object v4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mRegistrationCode:Ljava/lang/String;

    .line 244
    iput-object v4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    .line 245
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mCtid:I

    .line 246
    iput-object v4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mContext:Landroid/content/Context;

    .line 247
    iput-object v4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mIntent:Landroid/content/Intent;

    .line 248
    return-void

    .line 226
    :cond_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    const-string v2, "=======UNREGISTER CALLBACK FAIL======="

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    const-string v2, "Illegal Argument Exception when unbinding service"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 235
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 236
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    const-string v2, "Illegal Remote Exception when unregistering Callback"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static declared-synchronized getInstance(I)Lcom/samsung/android/scloud/auth/task/TokenTask;
    .locals 2
    .param p0, "cmd"    # I

    .prologue
    .line 59
    const-class v1, Lcom/samsung/android/scloud/auth/task/TokenTask;

    monitor-enter v1

    if-nez p0, :cond_1

    .line 60
    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/TokenTask;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/samsung/android/scloud/auth/task/TokenTask;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/task/TokenTask;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/TokenTask;

    .line 63
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/TokenTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/samsung/android/scloud/auth/task/TokenTask;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/task/TokenTask;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private prepareWithBindedServie(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 18
    .param p1, "mapId"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 155
    const-string v15, "command"

    const/16 v16, -0x1

    move-object/from16 v0, p4

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 156
    .local v8, "cmd":I
    const-string v15, "ctid"

    move-object/from16 v0, p4

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 159
    .local v9, "ctid":Ljava/lang/String;
    const-string v6, "tj9u972o46"

    .line 160
    .local v6, "appId":Ljava/lang/String;
    const-string v7, "D234AE3C42F092D4334433173AE9E264"

    .line 161
    .local v7, "appSecret":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v14

    .line 163
    .local v14, "packageName":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v13

    .line 164
    .local v13, "manager":Landroid/accounts/AccountManager;
    const/4 v4, 0x0

    .line 166
    .local v4, "accountArr":[Landroid/accounts/Account;
    if-eqz v13, :cond_0

    .line 167
    const-string v15, "com.osp.app.signin"

    invoke-virtual {v13, v15}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    .line 169
    :cond_0
    if-nez v8, :cond_5

    .line 171
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v15, :cond_1

    .line 173
    sget-object v15, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "=======REGISTER_CALLBACK TO SAMSUNG_ACCOUNT=======, CTID: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :try_start_0
    new-instance v15, Lcom/msc/sa/aidl/SACallback;

    move-object/from16 v0, p3

    invoke-direct {v15, v0}, Lcom/msc/sa/aidl/SACallback;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    .line 177
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mSACallback:Lcom/msc/sa/aidl/ISACallback;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v15, v6, v7, v14, v0}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mRegistrationCode:Ljava/lang/String;

    .line 180
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mRegistrationCode:Ljava/lang/String;

    if-nez v15, :cond_2

    .line 181
    sget-object v15, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "=======REGISTER_CALLBACK TO SAMSUNG_ACCOUNT FAIL=======, CTID: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/auth/task/TokenTask;->clear(Landroid/content/Intent;)V

    .line 216
    :cond_1
    :goto_0
    return-void

    .line 185
    :cond_2
    if-eqz v4, :cond_4

    array-length v15, v4

    if-lez v15, :cond_4

    .line 186
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 187
    .local v10, "data":Landroid/os/Bundle;
    const/4 v15, 0x2

    new-array v5, v15, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "user_id"

    aput-object v16, v5, v15

    const/4 v15, 0x1

    const-string v16, "cc"

    aput-object v16, v5, v15

    .line 188
    .local v5, "additionalData":[Ljava/lang/String;
    const-string v15, "request_new_token"

    const/16 v16, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 190
    const-string v15, "expired_access_token"

    move-object/from16 v0, p4

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 192
    .local v12, "expired_token":Ljava/lang/String;
    sget-object v15, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "=======REQUEST NEW ACCESS TOKEN=======, CTID: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v15, "expired_access_token"

    invoke-virtual {v10, v15, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    sget-object v15, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "EXPIRED_TOKEN INFO :"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .end local v12    # "expired_token":Ljava/lang/String;
    :goto_1
    const-string v15, "additional"

    invoke-virtual {v10, v15, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mService:Lcom/msc/sa/aidl/ISAService;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mRegistrationCode:Ljava/lang/String;

    move-object/from16 v16, v0

    move/from16 v0, p1

    move-object/from16 v1, v16

    invoke-interface {v15, v0, v1, v10}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 208
    .end local v5    # "additionalData":[Ljava/lang/String;
    .end local v10    # "data":Landroid/os/Bundle;
    :catch_0
    move-exception v11

    .line 210
    .local v11, "e":Landroid/os/RemoteException;
    invoke-virtual {v11}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 198
    .end local v11    # "e":Landroid/os/RemoteException;
    .restart local v5    # "additionalData":[Ljava/lang/String;
    .restart local v10    # "data":Landroid/os/Bundle;
    :cond_3
    :try_start_1
    sget-object v15, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "=======REQUEST ACCESS TOKEN=======, CTID: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 204
    .end local v5    # "additionalData":[Ljava/lang/String;
    .end local v10    # "data":Landroid/os/Bundle;
    :cond_4
    sget-object v15, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "NOT SIGNIN TO SAMSUNG ACCOUNT, CTID: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/auth/task/TokenTask;->clear(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 214
    :cond_5
    move-object/from16 v0, p2

    move/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/auth/core/ITaskTrigger;->prepared(ILandroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private tokenTask(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    .line 251
    new-instance v2, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct {v2}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 254
    .local v2, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    const-string v5, "result_code"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 257
    .local v3, "resultCode":I
    packed-switch v3, :pswitch_data_0

    .line 273
    sget-object v5, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "=======RECEIVED ACCESSTOKEN FAIL, RCODE: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    .line 275
    invoke-virtual {v2, v3}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V

    .line 279
    :goto_0
    return-object v2

    .line 259
    :pswitch_0
    sget-object v5, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    const-string v6, "=======RECEIVED ACCESSTOKEN, USERID, COUNTRY CODE======="

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v5, "access_token"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "accessToken":Ljava/lang/String;
    const-string v5, "user_id"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 264
    .local v4, "userId":Ljava/lang/String;
    const-string v5, "country_code"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 265
    .local v1, "countryCode":Ljava/lang/String;
    sget-object v5, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "=======TOKEN INFO : ACCESSTOKEN: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " USERID: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " COUNTRYCODE: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v5, "access_token"

    invoke-virtual {v2, v5, v0}, Lcom/samsung/android/scloud/auth/core/TaskResult;->putStringData(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v5, "user_id"

    invoke-virtual {v2, v5, v4}, Lcom/samsung/android/scloud/auth/core/TaskResult;->putStringData(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v5, "country_code"

    invoke-virtual {v2, v5, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->putStringData(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-virtual {v2, v8}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    goto :goto_0

    .line 257
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public doJob(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/auth/task/TokenTask;->tokenTask(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v0

    .line 106
    .local v0, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-direct {p0, p2}, Lcom/samsung/android/scloud/auth/task/TokenTask;->clear(Landroid/content/Intent;)V

    .line 107
    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public hasPrepare()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public onTimeout(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 77
    invoke-direct {p0, p2}, Lcom/samsung/android/scloud/auth/task/TokenTask;->clear(Landroid/content/Intent;)V

    .line 78
    return-void
.end method

.method public onTrigger(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 111
    sget-object v0, Lcom/samsung/android/scloud/auth/task/TokenTask;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTrigger mTrigger = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    if-eqz v0, :cond_0

    .line 114
    iput-object p2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mIntent:Landroid/content/Intent;

    .line 115
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    iget v1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mCtid:I

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mIntent:Landroid/content/Intent;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/auth/core/ITaskTrigger;->prepared(ILandroid/content/Context;Landroid/content/Intent;)V

    .line 117
    :cond_0
    return-void
.end method

.method public prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 3
    .param p1, "ctid"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 90
    iput-object p3, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mContext:Landroid/content/Context;

    .line 91
    const-string v1, "command"

    const/4 v2, -0x1

    invoke-virtual {p4, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 93
    .local v0, "cmd":I
    if-nez v0, :cond_0

    .line 94
    iput-object p2, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mTrigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    .line 95
    iput p1, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mCtid:I

    .line 96
    iput-object p4, p0, Lcom/samsung/android/scloud/auth/task/TokenTask;->mIntent:Landroid/content/Intent;

    .line 99
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/scloud/auth/task/TokenTask;->bindToSA(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method
