.class public Lcom/samsung/android/scloud/auth/task/APITask;
.super Ljava/lang/Object;
.source "APITask.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/ITaskHandler;


# static fields
.field public static APITask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption; = null

.field private static final TAG:Ljava/lang/String; = "APITask"

.field private static maxTimeOut:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const-wide/32 v0, 0x9c40

    sput-wide v0, Lcom/samsung/android/scloud/auth/task/APITask;->maxTimeOut:J

    .line 41
    new-instance v0, Lcom/samsung/android/scloud/auth/task/APITask$1;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/task/APITask$1;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/task/APITask;->APITask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()J
    .locals 2

    .prologue
    .line 37
    sget-wide v0, Lcom/samsung/android/scloud/auth/task/APITask;->maxTimeOut:J

    return-wide v0
.end method

.method private deRegisterProxyServer(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 374
    const-string v20, "APITask"

    const-string v21, "======= REQUEST_DE-REGISTER_PROXYSERVER ======="

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    new-instance v16, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 377
    .local v16, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v5

    .line 379
    .local v5, "dataMgr":Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getDid(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 380
    .local v7, "did":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    .line 381
    .local v19, "userId":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getBaseUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 383
    .local v4, "baseUrl":Ljava/lang/String;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 384
    .local v6, "deReg_proxyjson":Lorg/json/JSONObject;
    const-string v10, ""

    .line 386
    .local v10, "jsonstring":Ljava/lang/String;
    :try_start_0
    const-string v20, "uid"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 387
    const-string v20, "appid"

    const-string v21, "tj9u972o46"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 388
    const-string v20, "did"

    move-object/from16 v0, v20

    invoke-virtual {v6, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 390
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 395
    :goto_0
    const-string v20, "APITask"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "deregisterProxyServer() Base URL = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/service/deactivate"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 398
    .local v18, "url":Ljava/lang/String;
    const-string v20, "APITask"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Deactivate URL = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const/16 v20, 0x0

    const-string v21, "application/json;Charset=utf-8"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-static {v0, v10, v1, v2}, Lcom/samsung/android/scloud/auth/util/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v14

    .line 402
    .local v14, "response":Lorg/apache/http/HttpResponse;
    if-eqz v14, :cond_1

    .line 403
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v17

    .line 404
    .local v17, "status":I
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    .line 407
    .local v9, "entity":Lorg/apache/http/HttpEntity;
    :try_start_1
    invoke-static {v9}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v15

    .line 408
    .local v15, "responseStr":Ljava/lang/String;
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11, v15}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 409
    .local v11, "myJSON":Lorg/json/JSONObject;
    const-string v20, "rcode"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 411
    .local v12, "rCode":J
    const-string v20, "APITask"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "response ="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string v20, "APITask"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "======= DE-REGISTER_PROXYSERVER RESPONSE STATUS: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " RCODE: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "status ="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setResultMsg(Ljava/lang/String;)V

    .line 414
    if-eqz v9, :cond_0

    .line 415
    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 417
    :cond_0
    const/16 v20, 0xc8

    move/from16 v0, v17

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 418
    const-string v20, "APITask"

    const-string v21, "======= DE-REGISTER_PROXYSERVER SUCCESS ======="

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    const/16 v20, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V
    :try_end_1
    .catch Lorg/apache/http/ParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3

    .line 434
    .end local v9    # "entity":Lorg/apache/http/HttpEntity;
    .end local v11    # "myJSON":Lorg/json/JSONObject;
    .end local v12    # "rCode":J
    .end local v15    # "responseStr":Ljava/lang/String;
    .end local v17    # "status":I
    :cond_1
    :goto_1
    return-object v16

    .line 391
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    .end local v18    # "url":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 392
    .local v8, "e":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0

    .line 421
    .end local v8    # "e":Lorg/json/JSONException;
    .restart local v9    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v11    # "myJSON":Lorg/json/JSONObject;
    .restart local v12    # "rCode":J
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    .restart local v15    # "responseStr":Ljava/lang/String;
    .restart local v17    # "status":I
    .restart local v18    # "url":Ljava/lang/String;
    :cond_2
    const/16 v20, 0x190

    move/from16 v0, v17

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    .line 422
    :try_start_2
    const-string v20, "APITask"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "There was a problem on the Authentication Server. RESULT CODE: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    :cond_3
    const-string v20, "APITask"

    const-string v21, "======= DE-REGISTER_PROXYSERVER FAIL ======="

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    long-to-int v0, v12

    move/from16 v20, v0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V
    :try_end_2
    .catch Lorg/apache/http/ParseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 426
    .end local v11    # "myJSON":Lorg/json/JSONObject;
    .end local v12    # "rCode":J
    .end local v15    # "responseStr":Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 427
    .local v8, "e":Lorg/apache/http/ParseException;
    invoke-virtual {v8}, Lorg/apache/http/ParseException;->printStackTrace()V

    goto :goto_1

    .line 428
    .end local v8    # "e":Lorg/apache/http/ParseException;
    :catch_2
    move-exception v8

    .line 429
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 430
    .end local v8    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v8

    .line 431
    .local v8, "e":Lorg/json/JSONException;
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method

.method private getDeviceInformation(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/task/DeviceInfo;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 438
    const-string v10, "APITask"

    const-string v11, "getDeviceInformation"

    invoke-static {v10, v11}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const-string v10, "phone"

    invoke-virtual {p1, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 440
    .local v5, "mTelephony":Landroid/telephony/TelephonyManager;
    const-string v10, "wifi"

    invoke-virtual {p1, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/WifiManager;

    .line 441
    .local v8, "wifi":Landroid/net/wifi/WifiManager;
    const/4 v9, 0x0

    .line 443
    .local v9, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v8, :cond_0

    .line 445
    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v9

    .line 447
    :cond_0
    new-instance v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;

    invoke-direct {v3}, Lcom/samsung/android/scloud/auth/task/DeviceInfo;-><init>()V

    .line 449
    .local v3, "device":Lcom/samsung/android/scloud/auth/task/DeviceInfo;
    if-eqz v9, :cond_1

    .line 451
    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->macAddress:Ljava/lang/String;

    .line 453
    :cond_1
    const-string v10, "Samsung"

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->manufacturer:Ljava/lang/String;

    .line 455
    sget-object v10, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->model:Ljava/lang/String;

    .line 457
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->IMEI:Ljava/lang/String;

    .line 460
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 461
    .local v1, "am":Landroid/accounts/AccountManager;
    const/4 v0, 0x0

    .line 463
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v1, :cond_2

    .line 465
    const-string v10, "com.osp.app.signin"

    invoke-virtual {v1, v10}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 467
    :cond_2
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->model:Ljava/lang/String;

    if-eqz v10, :cond_a

    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->model:Ljava/lang/String;

    const-string v11, "-"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 468
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->model:Ljava/lang/String;

    const-string v11, "-"

    invoke-virtual {v10, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 469
    .local v2, "curPos":I
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->model:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual {v10, v11, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 470
    .local v7, "tmp":Ljava/lang/String;
    const-string v10, "s"

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 471
    const-string v10, "Phone"

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceType:Ljava/lang/String;

    .line 487
    .end local v2    # "curPos":I
    .end local v7    # "tmp":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_3

    .line 491
    array-length v10, v0

    const/4 v11, 0x1

    if-ge v10, v11, :cond_c

    .line 492
    const/4 v10, 0x0

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->account:Ljava/lang/String;

    .line 499
    :cond_3
    :goto_1
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_4

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v10

    const/4 v11, 0x5

    if-ne v10, v11, :cond_4

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_4

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x3

    if-ge v10, v11, :cond_d

    .line 501
    :cond_4
    const-string v10, ""

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceCC:Ljava/lang/String;

    .line 502
    const-string v10, ""

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceMNC:Ljava/lang/String;

    .line 516
    :cond_5
    :goto_2
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_6

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_6

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x3

    if-ge v10, v11, :cond_f

    .line 518
    :cond_6
    const-string v10, ""

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredCC:Ljava/lang/String;

    .line 519
    const-string v10, ""

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredMNC:Ljava/lang/String;

    .line 532
    :cond_7
    :goto_3
    return-object v3

    .line 473
    .restart local v2    # "curPos":I
    .restart local v7    # "tmp":Ljava/lang/String;
    :cond_8
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->IMEI:Ljava/lang/String;

    if-eqz v10, :cond_9

    .line 474
    const-string v10, "Phone"

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceType:Ljava/lang/String;

    goto :goto_0

    .line 476
    :cond_9
    const-string v10, "Wifi"

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceType:Ljava/lang/String;

    goto :goto_0

    .line 481
    .end local v2    # "curPos":I
    .end local v7    # "tmp":Ljava/lang/String;
    :cond_a
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->IMEI:Ljava/lang/String;

    if-eqz v10, :cond_b

    .line 482
    const-string v10, "Phone"

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceType:Ljava/lang/String;

    goto :goto_0

    .line 484
    :cond_b
    const-string v10, "Wifi"

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceType:Ljava/lang/String;

    goto :goto_0

    .line 494
    :cond_c
    const/4 v10, 0x0

    aget-object v10, v0, v10

    iget-object v10, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->account:Ljava/lang/String;

    goto :goto_1

    .line 505
    :cond_d
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v4

    .line 506
    .local v4, "imsi":Ljava/lang/String;
    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-virtual {v4, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceCC:Ljava/lang/String;

    .line 507
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceCC:Ljava/lang/String;

    if-nez v10, :cond_e

    .line 508
    const-string v10, ""

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceCC:Ljava/lang/String;

    .line 509
    :cond_e
    const/4 v10, 0x3

    invoke-virtual {v4, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceMNC:Ljava/lang/String;

    .line 510
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceMNC:Ljava/lang/String;

    if-nez v10, :cond_5

    .line 511
    const-string v10, ""

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceMNC:Ljava/lang/String;

    goto :goto_2

    .line 522
    .end local v4    # "imsi":Ljava/lang/String;
    :cond_f
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v6

    .line 523
    .local v6, "netopp":Ljava/lang/String;
    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-virtual {v6, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredCC:Ljava/lang/String;

    .line 524
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredCC:Ljava/lang/String;

    if-nez v10, :cond_10

    .line 525
    const-string v10, ""

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredCC:Ljava/lang/String;

    .line 526
    :cond_10
    const/4 v10, 0x3

    invoke-virtual {v6, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredMNC:Ljava/lang/String;

    .line 527
    iget-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredMNC:Ljava/lang/String;

    if-nez v10, :cond_7

    .line 528
    const-string v10, ""

    iput-object v10, v3, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredMNC:Ljava/lang/String;

    goto :goto_3
.end method

.method public static getUserSerial(Landroid/content/Context;)J
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 539
    const-string v4, "APITask"

    const-string v5, "getUserSerial"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-ge v4, v5, :cond_0

    .line 542
    const-wide/16 v0, 0x0

    .line 549
    :goto_0
    return-wide v0

    .line 544
    :cond_0
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v2

    .line 545
    .local v2, "uh":Landroid/os/UserHandle;
    const-string v4, "user"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 546
    .local v3, "um":Landroid/os/UserManager;
    invoke-virtual {v3, v2}, Landroid/os/UserManager;->getSerialNumberForUser(Landroid/os/UserHandle;)J

    move-result-wide v0

    .line 548
    .local v0, "sn":J
    const-string v4, "APITask"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getUserSerial : Serial = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private registerProxyServer(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 32
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 228
    const-string v27, "ctid"

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 229
    .local v6, "ctid":Ljava/lang/String;
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "======= REQUEST_REGISTER_PROXYSERVER =======, CTID: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    new-instance v21, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct/range {v21 .. v21}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 233
    .local v21, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v7

    .line 235
    .local v7, "dataMgr":Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getRegId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v17

    .line 236
    .local v17, "regId":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v26

    .line 237
    .local v26, "userId":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getBaseUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 238
    .local v5, "baseUrl":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getAccessToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 239
    .local v4, "accessToken":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/scloud/auth/task/APITask;->getDeviceInformation(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/task/DeviceInfo;

    move-result-object v8

    .line 240
    .local v8, "device":Lcom/samsung/android/scloud/auth/task/DeviceInfo;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/task/APITask;->getUserSerial(Landroid/content/Context;)J

    move-result-wide v22

    .line 242
    .local v22, "serionNo":J
    new-instance v18, Lorg/json/JSONObject;

    invoke-direct/range {v18 .. v18}, Lorg/json/JSONObject;-><init>()V

    .line 243
    .local v18, "reg_proxyjson":Lorg/json/JSONObject;
    new-instance v16, Lorg/json/JSONObject;

    invoke-direct/range {v16 .. v16}, Lorg/json/JSONObject;-><init>()V

    .line 244
    .local v16, "pushInfo":Lorg/json/JSONObject;
    const-string v13, ""

    .line 246
    .local v13, "jsonstring":Ljava/lang/String;
    :try_start_0
    const-string v27, "uid"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 247
    const-string v27, "account"

    iget-object v0, v8, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->account:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 248
    const-string v27, "appid"

    const-string v28, "tj9u972o46"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 249
    const-string v27, "manufacturer"

    iget-object v0, v8, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->manufacturer:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 250
    const-string v27, "model"

    iget-object v0, v8, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->model:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 252
    iget-object v0, v8, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->IMEI:Ljava/lang/String;

    move-object/from16 v27, v0

    if-eqz v27, :cond_4

    .line 253
    iget-object v12, v8, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->IMEI:Ljava/lang/String;

    .line 254
    .local v12, "imei":Ljava/lang/String;
    const-wide/16 v28, 0x0

    cmp-long v27, v22, v28

    if-eqz v27, :cond_0

    .line 255
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "_"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 256
    :cond_0
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "IMEI = "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const-string v27, "imei"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 262
    .end local v12    # "imei":Ljava/lang/String;
    :goto_0
    iget-object v14, v8, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->macAddress:Ljava/lang/String;

    .line 263
    .local v14, "mac":Ljava/lang/String;
    const-wide/16 v28, 0x0

    cmp-long v27, v22, v28

    if-eqz v27, :cond_1

    .line 264
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "_"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 265
    :cond_1
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "MAC = "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v27, "deviceType"

    iget-object v0, v8, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->deviceType:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 268
    const-string v27, "macAddress"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 269
    const-string v27, "push_type"

    const-string v28, "SPP"

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 270
    const-string v27, "push_appid"

    const-string v28, "tj9u972o46"

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 271
    const-string v27, "push_token"

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 272
    const-string v27, "pushInfo"

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 274
    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 279
    .end local v14    # "mac":Ljava/lang/String;
    :goto_1
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "registerProxyServer() Base URL = "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "/service/activate"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "?access_token="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 283
    .local v25, "url":Ljava/lang/String;
    const/16 v27, 0x0

    const-string v28, "application/json;Charset=utf-8"

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-static {v0, v13, v1, v2}, Lcom/samsung/android/scloud/auth/util/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v19

    .line 285
    .local v19, "response":Lorg/apache/http/HttpResponse;
    if-eqz v19, :cond_3

    .line 286
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v24

    .line 288
    .local v24, "status":I
    invoke-interface/range {v19 .. v19}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v11

    .line 290
    .local v11, "entity":Lorg/apache/http/HttpEntity;
    :try_start_1
    invoke-static {v11}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v20

    .line 291
    .local v20, "responseStr":Ljava/lang/String;
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "responseStr ="

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    new-instance v15, Lorg/json/JSONObject;

    move-object/from16 v0, v20

    invoke-direct {v15, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 293
    .local v15, "myJSON":Lorg/json/JSONObject;
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "======= REGISTER_PROXYSERVER RESPONSE STATUS: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " RCODE: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "rcode"

    move-object/from16 v0, v29

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v30

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ", CTID: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    if-eqz v11, :cond_2

    .line 295
    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 297
    :cond_2
    const/16 v27, 0xc8

    move/from16 v0, v24

    move/from16 v1, v27

    if-ne v0, v1, :cond_5

    .line 298
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "======= REGISTER_PROXYSERVER SUCCESS =======, CTID: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string v27, "did"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 300
    .local v9, "did":Ljava/lang/String;
    const-string v27, "did"

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v9}, Lcom/samsung/android/scloud/auth/core/TaskResult;->putStringData(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "registerProxyServer : Did ="

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const/16 v27, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V
    :try_end_1
    .catch Lorg/apache/http/ParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3

    .line 317
    .end local v9    # "did":Ljava/lang/String;
    .end local v11    # "entity":Lorg/apache/http/HttpEntity;
    .end local v15    # "myJSON":Lorg/json/JSONObject;
    .end local v20    # "responseStr":Ljava/lang/String;
    .end local v24    # "status":I
    :cond_3
    :goto_2
    return-object v21

    .line 259
    .end local v19    # "response":Lorg/apache/http/HttpResponse;
    .end local v25    # "url":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v27, "imei"

    const-string v28, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 275
    :catch_0
    move-exception v10

    .line 276
    .local v10, "e":Lorg/json/JSONException;
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 304
    .end local v10    # "e":Lorg/json/JSONException;
    .restart local v11    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v15    # "myJSON":Lorg/json/JSONObject;
    .restart local v19    # "response":Lorg/apache/http/HttpResponse;
    .restart local v20    # "responseStr":Ljava/lang/String;
    .restart local v24    # "status":I
    .restart local v25    # "url":Ljava/lang/String;
    :cond_5
    const/16 v27, 0x190

    move/from16 v0, v24

    move/from16 v1, v27

    if-eq v0, v1, :cond_6

    .line 305
    :try_start_3
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "There was a problem on the Authentication Server. RESULT CODE: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_6
    const-string v27, "APITask"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "======= REGISTER_PROXYSERVER FAIL =======, CTID: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v27, "rcode"

    move-object/from16 v0, v27

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v21

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V
    :try_end_3
    .catch Lorg/apache/http/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_2

    .line 309
    .end local v15    # "myJSON":Lorg/json/JSONObject;
    .end local v20    # "responseStr":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 310
    .local v10, "e":Lorg/apache/http/ParseException;
    invoke-virtual {v10}, Lorg/apache/http/ParseException;->printStackTrace()V

    goto :goto_2

    .line 311
    .end local v10    # "e":Lorg/apache/http/ParseException;
    :catch_2
    move-exception v10

    .line 312
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 313
    .end local v10    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v10

    .line 314
    .local v10, "e":Lorg/json/JSONException;
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method

.method private registerToken(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 157
    const-string v21, "ctid"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 158
    .local v7, "ctid":Ljava/lang/String;
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "======= REQUEST_REGISTERTOKEN =======, CTID: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    new-instance v17, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 162
    .local v17, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v8

    .line 163
    .local v8, "dataMgr":Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 164
    .local v11, "header":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 166
    .local v13, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getAccessToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 167
    .local v4, "accessToken":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v20

    .line 168
    .local v20, "userId":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getRegId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    .line 169
    .local v14, "regId":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getAuthKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 170
    .local v5, "authKey":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getBaseUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 172
    .local v6, "baseUrl":Ljava/lang/String;
    const-string v21, "x-sc-authkey"

    move-object/from16 v0, v21

    invoke-interface {v11, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    const-string v21, "access_token"

    move-object/from16 v0, v21

    invoke-interface {v13, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    const-string v21, "uid"

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-interface {v13, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const-string v21, "app_id"

    const-string v22, "tj9u972o46"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v13, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const-string v21, "app_secret"

    const-string v22, "D234AE3C42F092D4334433173AE9E264"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v13, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    const-string v21, "did"

    move-object/from16 v0, v21

    invoke-interface {v13, v0, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "registerToken() Base URL = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "registerToken() header = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "registerToken() params = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/auth/v2/token/register"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 185
    .local v19, "url":Ljava/lang/String;
    const/16 v21, 0x0

    const-string v22, "application/x-www-form-urlencoded"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v11, v1, v13, v2}, Lcom/samsung/android/scloud/auth/util/NetworkUtil;->post(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v15

    .line 187
    .local v15, "response":Lorg/apache/http/HttpResponse;
    if-eqz v15, :cond_1

    .line 188
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v21

    invoke-interface/range {v21 .. v21}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v18

    .line 189
    .local v18, "status":I
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v10

    .line 191
    .local v10, "entity":Lorg/apache/http/HttpEntity;
    :try_start_0
    invoke-static {v10}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v16

    .line 192
    .local v16, "responseStr":Ljava/lang/String;
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "responseStr ="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    if-eqz v10, :cond_0

    .line 195
    invoke-interface {v10}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 197
    :cond_0
    new-instance v12, Lorg/json/JSONObject;

    move-object/from16 v0, v16

    invoke-direct {v12, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 198
    .local v12, "json":Lorg/json/JSONObject;
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "======= REGISTER_TOKEN RESPONSE STATUS: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " RCODE: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "rcode"

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v24

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", CTID: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const/16 v21, 0xc8

    move/from16 v0, v18

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 200
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "======= REGISTER_TOKEN SUCCESS =======, CTID: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const/16 v21, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    .line 223
    .end local v10    # "entity":Lorg/apache/http/HttpEntity;
    .end local v12    # "json":Lorg/json/JSONObject;
    .end local v16    # "responseStr":Ljava/lang/String;
    .end local v18    # "status":I
    :cond_1
    :goto_0
    return-object v17

    .line 202
    .restart local v10    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v12    # "json":Lorg/json/JSONObject;
    .restart local v16    # "responseStr":Ljava/lang/String;
    .restart local v18    # "status":I
    :cond_2
    const/16 v21, 0x190

    move/from16 v0, v18

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    .line 203
    const-string v21, "rcode"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v22

    const-wide/32 v24, 0xc030

    cmp-long v21, v22, v24

    if-nez v21, :cond_3

    .line 204
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "=======REGISTER_TOKEN : RE_SERVERINFO_REQUIRED=======, CTID: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v21, "rcode"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V
    :try_end_0
    .catch Lorg/apache/http/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 214
    .end local v12    # "json":Lorg/json/JSONObject;
    .end local v16    # "responseStr":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 215
    .local v9, "e":Lorg/apache/http/ParseException;
    invoke-virtual {v9}, Lorg/apache/http/ParseException;->printStackTrace()V

    goto :goto_0

    .line 207
    .end local v9    # "e":Lorg/apache/http/ParseException;
    .restart local v12    # "json":Lorg/json/JSONObject;
    .restart local v16    # "responseStr":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v21, "rcode"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V
    :try_end_1
    .catch Lorg/apache/http/ParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 216
    .end local v12    # "json":Lorg/json/JSONObject;
    .end local v16    # "responseStr":Ljava/lang/String;
    :catch_1
    move-exception v9

    .line 217
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 210
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v12    # "json":Lorg/json/JSONObject;
    .restart local v16    # "responseStr":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "There was a problem on the Authentication Server. RESULT CODE: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v21, "rcode"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v21, v0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V

    .line 212
    const-string v21, "APITask"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "======= REGISTER_TOKEN FAIL =======, CTID: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/apache/http/ParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 218
    .end local v12    # "json":Lorg/json/JSONObject;
    .end local v16    # "responseStr":Ljava/lang/String;
    :catch_2
    move-exception v9

    .line 219
    .local v9, "e":Lorg/json/JSONException;
    invoke-virtual {v9}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private requestServerInfo(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 28
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 90
    const-string v23, "ctid"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 92
    .local v8, "ctid":Ljava/lang/String;
    const-string v23, "APITask"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "======= REQUEST_SERVERINFO =======, CTID: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v17, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 95
    .local v17, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getAccessToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v20

    .line 96
    .local v20, "token":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v21

    .line 97
    .local v21, "uid":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getCountryCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 98
    .local v7, "cc":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/scloud/auth/task/APITask;->getDeviceInformation(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/task/DeviceInfo;

    move-result-object v9

    .line 99
    .local v9, "device":Lcom/samsung/android/scloud/auth/task/DeviceInfo;
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getRegId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    .line 100
    .local v15, "regId":Ljava/lang/String;
    const-string v23, "APITask"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "requestServerInfo Token : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v22, "https://sc-auth.samsungosp.com/gld/v2/serverinfo"

    .line 104
    .local v22, "url":Ljava/lang/String;
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 106
    .local v14, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v23, "uid"

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v23, "access_token"

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    const-string v23, "app_id"

    const-string v24, "tj9u972o46"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    const-string v23, "app_secret"

    const-string v24, "D234AE3C42F092D4334433173AE9E264"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    const-string v23, "did"

    move-object/from16 v0, v23

    invoke-interface {v14, v0, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    const-string v23, "mcc"

    iget-object v0, v9, Lcom/samsung/android/scloud/auth/task/DeviceInfo;->registeredCC:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-interface {v14, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    const-string v23, "cc"

    move-object/from16 v0, v23

    invoke-interface {v14, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    move-object/from16 v0, v22

    invoke-static {v0, v14}, Lcom/samsung/android/scloud/auth/util/NetworkUtil;->get(Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 116
    .local v12, "httpResponse":Lorg/apache/http/HttpResponse;
    if-eqz v12, :cond_0

    .line 117
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v19

    .line 119
    .local v19, "status":I
    :try_start_0
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v11

    .line 120
    .local v11, "entity":Lorg/apache/http/HttpEntity;
    invoke-static {v11}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v16

    .line 121
    .local v16, "response":Ljava/lang/String;
    new-instance v13, Lorg/json/JSONObject;

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 123
    .local v13, "myJSON":Lorg/json/JSONObject;
    const-string v23, "APITask"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "response ="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v23, "APITask"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "======= SERVERINFO RESPONSE STATUS: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " RCODE: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "rcode"

    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v26

    move-object/from16 v0, v24

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", CTID: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const/16 v23, 0xc8

    move/from16 v0, v19

    move/from16 v1, v23

    if-ne v0, v1, :cond_1

    .line 126
    const-string v23, "APITask"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "======= SERVERINFO SUCCESS =======, CTID: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v23, "address"

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 128
    .local v4, "address":Ljava/lang/String;
    const-string v23, "scheme"

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 129
    .local v18, "scheme":Ljava/lang/String;
    const-string v23, "authkey"

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 130
    .local v5, "authKey":Ljava/lang/String;
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "://"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 131
    .local v6, "baseUrl":Ljava/lang/String;
    const-string v23, "APITask"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "requestServerInfo : scheme="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", address="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", authKey="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v23, "base_url"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v6}, Lcom/samsung/android/scloud/auth/core/TaskResult;->putStringData(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v23, "authKey"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v5}, Lcom/samsung/android/scloud/auth/core/TaskResult;->putStringData(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const/16 v23, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    .line 135
    if-eqz v11, :cond_0

    .line 136
    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 153
    .end local v4    # "address":Ljava/lang/String;
    .end local v5    # "authKey":Ljava/lang/String;
    .end local v6    # "baseUrl":Ljava/lang/String;
    .end local v11    # "entity":Lorg/apache/http/HttpEntity;
    .end local v13    # "myJSON":Lorg/json/JSONObject;
    .end local v16    # "response":Ljava/lang/String;
    .end local v18    # "scheme":Ljava/lang/String;
    .end local v19    # "status":I
    :cond_0
    :goto_0
    return-object v17

    .line 140
    .restart local v11    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v13    # "myJSON":Lorg/json/JSONObject;
    .restart local v16    # "response":Ljava/lang/String;
    .restart local v19    # "status":I
    :cond_1
    const/16 v23, 0x190

    move/from16 v0, v19

    move/from16 v1, v23

    if-eq v0, v1, :cond_2

    .line 141
    const-string v23, "APITask"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "There was a problem on the Authentication Server. RESULT CODE: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_2
    const-string v23, "APITask"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "======= SERVERINFO FAIL =======, CTID: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v23, "rcode"

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v24

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V
    :try_end_0
    .catch Lorg/apache/http/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 145
    .end local v11    # "entity":Lorg/apache/http/HttpEntity;
    .end local v13    # "myJSON":Lorg/json/JSONObject;
    .end local v16    # "response":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 146
    .local v10, "e":Lorg/apache/http/ParseException;
    invoke-virtual {v10}, Lorg/apache/http/ParseException;->printStackTrace()V

    goto :goto_0

    .line 147
    .end local v10    # "e":Lorg/apache/http/ParseException;
    :catch_1
    move-exception v10

    .line 148
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 149
    .end local v10    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v10

    .line 150
    .local v10, "e":Lorg/json/JSONException;
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public doJob(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 71
    const-string v1, "command"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 72
    .local v0, "cmd":I
    packed-switch v0, :pswitch_data_0

    .line 85
    :pswitch_0
    new-instance v1, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct {v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    :goto_0
    return-object v1

    .line 74
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/auth/task/APITask;->registerProxyServer(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v1

    goto :goto_0

    .line 76
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/auth/task/APITask;->requestServerInfo(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v1

    goto :goto_0

    .line 80
    :pswitch_3
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/auth/task/APITask;->deRegisterProxyServer(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v1

    goto :goto_0

    .line 82
    :pswitch_4
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/auth/task/APITask;->registerToken(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v1

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    const-string v0, "APITask"

    return-object v0
.end method

.method public hasPrepare()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public onTimeout(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 57
    return-void
.end method

.method public prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1
    .param p1, "ctid"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method
