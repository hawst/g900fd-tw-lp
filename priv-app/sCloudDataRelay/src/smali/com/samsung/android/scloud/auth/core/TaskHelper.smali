.class public Lcom/samsung/android/scloud/auth/core/TaskHelper;
.super Ljava/lang/Object;
.source "TaskHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;
    }
.end annotation


# static fields
.field private static COMMON_PREPARE_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult; = null

.field private static COMMON_TIMEOUT_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult; = null

.field private static DEFAULT_TASK_TRIGGER:Lcom/samsung/android/scloud/auth/core/ITaskTrigger; = null

.field private static DEFAULT_TIMEOUT_HANDLER:Landroid/os/Handler; = null

.field private static final TAG:Ljava/lang/String; = "TaskHelper"

.field private static TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/scloud/auth/core/TaskObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;

    .line 31
    new-instance v0, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_TIMEOUT_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;

    .line 32
    sget-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_TIMEOUT_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-virtual {v0, v2}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    .line 33
    sget-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_TIMEOUT_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V

    .line 35
    new-instance v0, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_PREPARE_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;

    .line 36
    sget-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_PREPARE_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-virtual {v0, v2}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    .line 37
    sget-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_PREPARE_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setRcode(I)V

    .line 39
    new-instance v0, Lcom/samsung/android/scloud/auth/core/TaskHelper$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskHelper$1;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->DEFAULT_TIMEOUT_HANDLER:Landroid/os/Handler;

    .line 65
    new-instance v0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/core/TaskHelper$2;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->DEFAULT_TASK_TRIGGER:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    return-void
.end method

.method static synthetic access$000()Ljava/util/concurrent/ConcurrentMap;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method static synthetic access$100()Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_TIMEOUT_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;

    return-object v0
.end method

.method static synthetic access$200()Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->DEFAULT_TASK_TRIGGER:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    return-object v0
.end method

.method static synthetic access$300()Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_PREPARE_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;

    return-object v0
.end method

.method public static doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "handler"    # Lcom/samsung/android/scloud/auth/core/ITaskHandler;
    .param p3, "taskOption"    # Lcom/samsung/android/scloud/auth/core/ITaskOption;
    .param p4, "listener"    # Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

    .prologue
    .line 115
    new-instance v0, Lcom/samsung/android/scloud/auth/core/TaskObject;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/scloud/auth/core/TaskObject;-><init>(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 117
    .local v0, "task":Lcom/samsung/android/scloud/auth/core/TaskObject;
    invoke-interface {p2}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->hasPrepare()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 119
    new-instance v1, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;

    move-object v2, v0

    move-object v3, p2

    move-object v4, p0

    move-object v5, p1

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/samsung/android/scloud/auth/core/TaskHelper$3;-><init>(Lcom/samsung/android/scloud/auth/core/TaskObject;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 141
    .local v1, "runPrepare":Ljava/lang/Runnable;
    new-instance v10, Ljava/lang/Thread;

    const-string v3, "PREPARED_THREAD"

    invoke-direct {v10, v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 143
    .local v10, "threadPrepare":Ljava/lang/Thread;
    sget-object v3, Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    invoke-interface {p3}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getTimeout()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 145
    const-string v3, "TaskHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doTask-setTimeout - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v5}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", after "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getTimeout()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    sget-object v3, Lcom/samsung/android/scloud/auth/core/TaskHelper;->DEFAULT_TIMEOUT_HANDLER:Landroid/os/Handler;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getTimeout()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 148
    :cond_0
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 181
    .end local v1    # "runPrepare":Ljava/lang/Runnable;
    .end local v10    # "threadPrepare":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 151
    :cond_1
    new-instance v2, Lcom/samsung/android/scloud/auth/core/TaskHelper$4;

    move-object v3, v0

    move-object v4, p2

    move-object v5, p0

    move-object v6, p1

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/scloud/auth/core/TaskHelper$4;-><init>(Lcom/samsung/android/scloud/auth/core/TaskObject;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 173
    .local v2, "run":Ljava/lang/Runnable;
    new-instance v9, Ljava/lang/Thread;

    const-string v3, "EXCUTE_THREAD"

    invoke-direct {v9, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 174
    .local v9, "thread":Ljava/lang/Thread;
    sget-object v3, Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    invoke-interface {p3}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getTimeout()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 176
    const-string v3, "TaskHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doTask-setTimeout - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v5}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", after "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getTimeout()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    sget-object v3, Lcom/samsung/android/scloud/auth/core/TaskHelper;->DEFAULT_TIMEOUT_HANDLER:Landroid/os/Handler;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getTimeout()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 179
    :cond_2
    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
