.class final Lcom/samsung/android/scloud/auth/RelayTask$5;
.super Ljava/lang/Object;
.source "RelayTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/RelayTask;->requestServerInfo(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$AuthKey_Key:I

.field final synthetic val$baseUrlKey:I

.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

.field final synthetic val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

.field final synthetic val$tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;Lcom/samsung/android/scloud/auth/data/AuthDataManager;II)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$tag:Ljava/lang/String;

    iput-object p2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$ctid:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

    iput-object p4, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iput p5, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$baseUrlKey:I

    iput p6, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$AuthKey_Key:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "result"    # Lcom/samsung/android/scloud/auth/core/TaskResult;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 242
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCompleted - REQUEST_SERVERINFO : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$ctid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "REQUEST_SERVERINFO - result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getRcode()I

    move-result v3

    iput v3, v2, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    .line 245
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$tag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "REQUEST_SERVERINFO - isSucceeded() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->isSucceeded()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->isSucceeded()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 247
    const-string v2, "base_url"

    invoke-virtual {p1, v2}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getStringData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "baseUrl":Ljava/lang/String;
    const-string v2, "authKey"

    invoke-virtual {p1, v2}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getStringData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    .local v0, "authKey":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$baseUrlKey:I

    invoke-virtual {v2, p2, v3, v1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->setBaseUrl(Landroid/content/Context;ILjava/lang/String;)V

    .line 250
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$AuthKey_Key:I

    invoke-virtual {v2, p2, v3, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->setAuthKey(Landroid/content/Context;ILjava/lang/String;)V

    .line 256
    .end local v0    # "authKey":Ljava/lang/String;
    .end local v1    # "baseUrl":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$baseUrlKey:I

    invoke-virtual {v2, p2, v3}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->releaseBaseUrl(Landroid/content/Context;I)V

    .line 257
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$AuthKey_Key:I

    invoke-virtual {v2, p2, v3}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->releaseAuthKey(Landroid/content/Context;I)V

    .line 258
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$ctid:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->notify(Ljava/lang/String;)V

    .line 259
    return-void

    .line 251
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getRcode()I

    move-result v2

    const/16 v3, 0x4a40

    if-eq v2, v3, :cond_0

    .line 252
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$baseUrlKey:I

    const-string v4, ""

    invoke-virtual {v2, p2, v3, v4}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->setBaseUrl(Landroid/content/Context;ILjava/lang/String;)V

    .line 253
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$5;->val$AuthKey_Key:I

    const-string v4, ""

    invoke-virtual {v2, p2, v3, v4}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->setAuthKey(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method
