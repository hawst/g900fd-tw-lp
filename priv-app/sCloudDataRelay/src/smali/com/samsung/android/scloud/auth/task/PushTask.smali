.class public Lcom/samsung/android/scloud/auth/task/PushTask;
.super Ljava/lang/Object;
.source "PushTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/ITaskHandler;


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/auth/task/PushTask; = null

.field private static final TAG:Ljava/lang/String; = "PushTask"


# instance fields
.field private authorityMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    .line 29
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "8kLTKS0V1y"

    const-string v2, "com.android.calendar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "LzftMiEuh2"

    const-string v2, "com.samsung.sec.android"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "KEqLhXhtEP"

    const-string v2, "com.android.contacts"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v2, "4OuNBe4y9z"

    const-string v0, "com.sec.android.app.sbrowser"

    invoke-static {p1, v0}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->checkPackageByName(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BOOKMARK"

    invoke-static {p1, v0}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->getSBrowserAuthority(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "E2PW6NSgIO"

    const-string v2, "com.sec.android.backup.providers.BackupProvider"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "X08g96bD1C"

    const-string v2, "com.sec.android.widgetapp.q1_penmemo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v2, "a1QGNqwu27"

    const-string v0, "com.sec.android.provider.snote"

    invoke-static {p1, v0}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->checkPackageByName(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.infraware.provider.SNoteProvider"

    :goto_1
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "PM3HWwUYhP"

    const-string v2, "com.samsung.android.snoteprovider"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "P56GWW8N4r"

    const-string v2, "TAB"

    invoke-static {p1, v2}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->getSBrowserAuthority(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "QUVql3tKM8"

    const-string v2, "com.sec.android.app.sbrowser"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    const-string v1, "w8wcqZo4Uk"

    const-string v2, "com.samsung.android.memo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void

    .line 32
    :cond_0
    const-string v0, "com.android.browser"

    goto :goto_0

    .line 37
    :cond_1
    const-string v0, "com.samsung.android.provider.snote2"

    goto :goto_1
.end method

.method private appIdInfoReceiver(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 28
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 77
    new-instance v23, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 79
    .local v23, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    const-string v25, "PushTask"

    const-string v26, "AppIdInfoReceiver(Intent intent, Context context)"

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v25, "PushTask"

    const-string v26, "=======PUSH MSG RECEIVED======="

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v25, "appId"

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 83
    .local v13, "mAppId":Ljava/lang/String;
    const-string v25, "msg"

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 84
    .local v20, "mMessage":Ljava/lang/String;
    const-string v25, "ack"

    const/16 v26, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 85
    .local v5, "bAck":Z
    const-string v25, "appData"

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 86
    .local v16, "mData":Ljava/lang/String;
    const-string v25, "notificationId"

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 87
    .local v21, "mNoti":Ljava/lang/String;
    const/4 v4, 0x0

    .line 88
    .local v4, "authority":Ljava/lang/String;
    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    .line 89
    .local v17, "mDataMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v25, "PushTask"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Received MSG = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v25, "PushTask"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Received APP_ID = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v25, "PushTask"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Received APP Data = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v25, "PushTask"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Required ackowldgement? = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v25, "PushTask"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Received Notification ID = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    if-eqz v5, :cond_0

    .line 96
    const-class v25, Lcom/samsung/android/scloud/auth/RelayService;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 97
    const-string v25, "NOTIFY_ACK_SPP"

    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    invoke-virtual/range {p1 .. p2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 102
    :cond_0
    const-string v25, "&"

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 103
    .local v18, "mDataTemp":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 104
    .local v9, "i":I
    :goto_0
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v25, v0

    move/from16 v0, v25

    if-ge v9, v0, :cond_2

    .line 105
    aget-object v25, v18, v9

    const-string v26, "="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    .line 106
    .local v24, "splitTemp":[Ljava/lang/String;
    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v25, v0

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_1

    .line 107
    const/16 v25, 0x0

    aget-object v25, v24, v25

    const/16 v26, 0x1

    aget-object v26, v24, v26

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    :goto_1
    add-int/lit8 v9, v9, 0x1

    .line 113
    goto :goto_0

    .line 110
    :cond_1
    const/16 v25, 0x0

    aget-object v25, v24, v25

    const-string v26, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 114
    .end local v24    # "splitTemp":[Ljava/lang/String;
    :cond_2
    const-string v25, "cid"

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 115
    .local v14, "mCID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/auth/task/PushTask;->authorityMap:Ljava/util/HashMap;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "authority":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 120
    .restart local v4    # "authority":Ljava/lang/String;
    const-string v25, "commerce="

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 121
    .local v7, "commercePos":I
    const-string v25, "extra="

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 122
    .local v8, "extraPos":I
    if-lez v7, :cond_4

    .line 124
    const-string v25, "PushTask"

    const-string v26, "==========PURCHASE REQUESTED =================="

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v25, "commerce"

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 126
    .local v15, "mCommerce":Ljava/lang/String;
    const-string v25, "PushTask"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "value of mCommerce : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    new-instance v12, Landroid/content/Intent;

    const-string v25, "com.sec.android.scloud.commerce"

    move-object/from16 v0, v25

    invoke-direct {v12, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 128
    .local v12, "intentCommerce":Landroid/content/Intent;
    const-string v25, "CODE"

    move-object/from16 v0, v25

    invoke-virtual {v12, v0, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    if-lez v8, :cond_3

    .line 131
    const-string v25, "extra"

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 132
    .local v19, "mExtra":Ljava/lang/String;
    const-string v25, "PushTask"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "value of mExtra : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v25, "EXTRA"

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    .end local v19    # "mExtra":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 137
    .end local v12    # "intentCommerce":Landroid/content/Intent;
    .end local v15    # "mCommerce":Ljava/lang/String;
    :cond_4
    const-string v25, "cid"

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 140
    const-string v25, "BNRCLEAR"

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 141
    const-string v25, "PushTask"

    const-string v26, "==========BACKUP DELETED=================="

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v11, Landroid/content/Intent;

    const-string v25, "com.sec.android.sCloudBackup.BACKUP_DELETED"

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 143
    .local v11, "intentBNRClear":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 145
    .end local v11    # "intentBNRClear":Landroid/content/Intent;
    :cond_5
    const-string v25, "100MWARN"

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 146
    const-string v25, "PushTask"

    const-string v26, "==========QUOTA WARNING=================="

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v10, Landroid/content/Intent;

    const-string v25, "com.sec.android.sCloudQuota.QUOTA_WARNING"

    move-object/from16 v0, v25

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 148
    .local v10, "intent100MWarn":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 150
    .end local v10    # "intent100MWarn":Landroid/content/Intent;
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/scloud/auth/task/PushTask;->getUserName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v22

    .line 152
    .local v22, "mUserName":Ljava/lang/String;
    if-eqz v22, :cond_9

    .line 153
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 154
    .local v6, "bundle":Landroid/os/Bundle;
    new-instance v3, Landroid/accounts/Account;

    const-string v25, "com.osp.app.signin"

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-direct {v3, v0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .local v3, "account":Landroid/accounts/Account;
    if-nez v4, :cond_8

    .line 156
    const-string v25, "PushTask"

    const-string v26, "==========CID REQUESTED UNKNOWN=================="

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .end local v3    # "account":Landroid/accounts/Account;
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v22    # "mUserName":Ljava/lang/String;
    :cond_7
    :goto_2
    const/16 v25, 0x1

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    .line 168
    return-object v23

    .line 158
    .restart local v3    # "account":Landroid/accounts/Account;
    .restart local v6    # "bundle":Landroid/os/Bundle;
    .restart local v22    # "mUserName":Ljava/lang/String;
    :cond_8
    const-string v25, "PushTask"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "== Request Sync to : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v25, "trigger"

    const-string v26, "sync_push"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-static {v3, v4, v6}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    .line 163
    .end local v3    # "account":Landroid/accounts/Account;
    .end local v6    # "bundle":Landroid/os/Bundle;
    :cond_9
    const-string v25, "PushTask"

    const-string v26, "SAMSUNG ACCOUNT REMOVED!!"

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/task/PushTask;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    const-class v1, Lcom/samsung/android/scloud/auth/task/PushTask;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/auth/task/PushTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/PushTask;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/samsung/android/scloud/auth/task/PushTask;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/auth/task/PushTask;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/scloud/auth/task/PushTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/PushTask;

    .line 49
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/auth/task/PushTask;->INSTANCE:Lcom/samsung/android/scloud/auth/task/PushTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getUserName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 175
    const-string v3, "PushTask"

    const-string v4, "getUserName(Context context)"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    if-nez p1, :cond_1

    .line 189
    :cond_0
    :goto_0
    return-object v2

    .line 180
    :cond_1
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 181
    .local v1, "am":Landroid/accounts/AccountManager;
    const/4 v0, 0x0

    .line 183
    .local v0, "accounts":[Landroid/accounts/Account;
    if-eqz v1, :cond_2

    .line 184
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 186
    :cond_2
    if-eqz v0, :cond_0

    array-length v3, v0

    const/4 v4, 0x1

    if-lt v3, v4, :cond_0

    .line 189
    const/4 v2, 0x0

    aget-object v2, v0, v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public doJob(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/auth/task/PushTask;->appIdInfoReceiver(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v0

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "PushTask"

    return-object v0
.end method

.method public hasPrepare()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public onTimeout(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 59
    return-void
.end method

.method public prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1
    .param p1, "ctid"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method
