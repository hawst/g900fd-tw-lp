.class public interface abstract Lcom/samsung/android/scloud/auth/common/RelayConstants$API;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/common/RelayConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "API"
.end annotation


# static fields
.field public static final ACTIVATE:Ljava/lang/String; = "/service/activate"

.field public static final CHECK_ACCESSTOKEN:Ljava/lang/String; = "/auth/v2/token/check"

.field public static final CLOUD_PREPARE:Ljava/lang/String; = "/cloud/prepare"

.field public static final DEACTIVATE:Ljava/lang/String; = "/service/deactivate"

.field public static final REGISTERTOKEN:Ljava/lang/String; = "/auth/v2/token/register"

.field public static final SERVERINFO:Ljava/lang/String; = "/gld/v2/serverinfo"
