.class public interface abstract Lcom/samsung/android/scloud/auth/common/RelayConstants$Commands;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/common/RelayConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Commands"
.end annotation


# static fields
.field public static final ACTIVATE:I = 0x5

.field public static final CHECK_TOKEN:I = 0x1

.field public static final CLOUD_PREPARE:I = 0x8

.field public static final DEACTIVATE:I = 0xa

.field public static final DEREGIST_SA:I = 0xd

.field public static final DEREGIST_SPP:I = 0x3

.field public static final NOTIFY_ACK_SPP:I = 0x9

.field public static final REGISTER_TOKEN:I = 0xb

.field public static final REGIST_SA:I = 0xc

.field public static final REGIST_SPP:I = 0x2

.field public static final REQUEST_PROVISIONING:I = 0x6

.field public static final REQUEST_SERVERINFO:I = 0x7

.field public static final REQUEST_TOKEN:I = 0x0

.field public static final SYNC_PUSH:I = 0x4
