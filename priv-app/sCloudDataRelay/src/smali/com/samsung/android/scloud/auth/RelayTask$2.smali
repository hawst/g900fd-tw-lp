.class final Lcom/samsung/android/scloud/auth/RelayTask$2;
.super Ljava/lang/Object;
.source "RelayTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/RelayTask;->prepareAuthInformation(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

.field final synthetic val$didKey:I

.field final synthetic val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

.field final synthetic val$tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;Lcom/samsung/android/scloud/auth/data/AuthDataManager;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$tag:Ljava/lang/String;

    iput-object p2, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

    iput-object p3, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iput p4, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$didKey:I

    iput-object p5, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$ctid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "result"    # Lcom/samsung/android/scloud/auth/core/TaskResult;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 133
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$tag:Ljava/lang/String;

    const-string v2, "onCompleted - ACTIVATE"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$tag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTIVATE - result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getRcode()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    .line 136
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->isSucceeded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    const-string v1, "did"

    invoke-virtual {p1, v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getStringData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "did":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$didKey:I

    invoke-virtual {v1, p2, v2, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->setDid(Landroid/content/Context;ILjava/lang/String;)V

    .line 140
    .end local v0    # "did":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v2, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$didKey:I

    invoke-virtual {v1, p2, v2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->releaseDid(Landroid/content/Context;I)V

    .line 141
    iget-object v1, p0, Lcom/samsung/android/scloud/auth/RelayTask$2;->val$ctid:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->notify(Ljava/lang/String;)V

    .line 142
    return-void
.end method
