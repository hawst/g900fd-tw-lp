.class public interface abstract Lcom/samsung/android/scloud/auth/common/RelayConstants$API_RESPONSE_CODE;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/common/RelayConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "API_RESPONSE_CODE"
.end annotation


# static fields
.field public static final BAD_ACCESS_TOKEN:I = 0x4a40

.field public static final INTERNAL_SERVER_ERROR:I = 0x1f4

.field public static final REQUEST_TIME_OUT:I = 0x198

.field public static final RE_SERVERINFO_REQUIRED:I = 0xc030

.field public static final SUCCESS:I
