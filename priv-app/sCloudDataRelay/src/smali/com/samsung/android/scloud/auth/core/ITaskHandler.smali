.class public interface abstract Lcom/samsung/android/scloud/auth/core/ITaskHandler;
.super Ljava/lang/Object;
.source "ITaskHandler.java"


# virtual methods
.method public abstract doJob(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
.end method

.method public abstract getTag()Ljava/lang/String;
.end method

.method public abstract hasPrepare()Z
.end method

.method public abstract onTimeout(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method public abstract prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
.end method
