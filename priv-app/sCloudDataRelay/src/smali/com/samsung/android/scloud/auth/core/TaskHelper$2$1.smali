.class Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;
.super Ljava/lang/Object;
.source "TaskHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/core/TaskHelper$2;->prepared(ILandroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/auth/core/TaskHelper$2;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/auth/core/TaskHelper$2;Lcom/samsung/android/scloud/auth/core/TaskObject;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->this$0:Lcom/samsung/android/scloud/auth/core/TaskHelper$2;

    iput-object p2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iput-object p3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 78
    const-string v2, "TaskHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doTask-excute doJob - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v4, v4, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v4}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v2, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v4, v4, Lcom/samsung/android/scloud/auth/core/TaskObject;->intent:Landroid/content/Intent;

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->doJob(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v1

    .line 80
    .local v1, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->isSucceeded()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v2, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    invoke-interface {v3}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getMaxRetryCount()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 82
    :cond_0
    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    const-string v2, "TaskHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remove from TaskMap - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v4, v4, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v4}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", retryCnt : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v4, v4, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v2, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->listener:Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$intent:Landroid/content/Intent;

    invoke-interface {v2, v1, v3, v4}, Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;->onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V

    .line 104
    :cond_1
    :goto_0
    return-void

    .line 86
    :cond_2
    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->isSucceeded()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v2, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    invoke-interface {v2}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getMaxRetryCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 88
    const/4 v0, 0x0

    .line 89
    .local v0, "isSuc":Z
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v2, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    invoke-interface {v3}, Lcom/samsung/android/scloud/auth/core/ITaskOption;->getMaxRetryCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 90
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v3, v3, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    .line 91
    const-string v2, "TaskHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doTask-retry prepare - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v4, v4, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v4}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v4, v4, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v2, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->DEFAULT_TASK_TRIGGER:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$200()Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$context:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$intent:Landroid/content/Intent;

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_4

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 97
    :cond_4
    if-nez v0, :cond_1

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 98
    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    const-string v2, "TaskHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remove from TaskMap - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v4, v4, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v4}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", retryCnt : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget v4, v4, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$task:Lcom/samsung/android/scloud/auth/core/TaskObject;

    iget-object v2, v2, Lcom/samsung/android/scloud/auth/core/TaskObject;->listener:Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_PREPARE_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$300()Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$context:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;->val$intent:Landroid/content/Intent;

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;->onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
