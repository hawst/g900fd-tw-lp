.class public Lcom/samsung/android/scloud/auth/task/MUMTask;
.super Ljava/lang/Object;
.source "MUMTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/ITaskHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "MUMTask"


# instance fields
.field private mUserReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lcom/samsung/android/scloud/auth/task/MUMTask$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/auth/task/MUMTask$1;-><init>(Lcom/samsung/android/scloud/auth/task/MUMTask;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/auth/task/MUMTask;->mUserReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public doJob(Landroid/content/Context;Landroid/content/Intent;)Lcom/samsung/android/scloud/auth/core/TaskResult;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 52
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 54
    .local v1, "userFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.USER_STOPPED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 55
    const-string v2, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 56
    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/MUMTask;->mUserReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v2, v1, v3, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 58
    new-instance v0, Lcom/samsung/android/scloud/auth/core/TaskResult;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/core/TaskResult;-><init>()V

    .line 59
    .local v0, "result":Lcom/samsung/android/scloud/auth/core/TaskResult;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/scloud/auth/core/TaskResult;->setSucceeded(Z)V

    .line 60
    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "MUMTask"

    return-object v0
.end method

.method public hasPrepare()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public onTimeout(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 37
    return-void
.end method

.method public prepare(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1
    .param p1, "ctid"    # I
    .param p2, "trigger"    # Lcom/samsung/android/scloud/auth/core/ITaskTrigger;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method
