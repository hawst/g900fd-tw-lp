.class public interface abstract Lcom/samsung/android/scloud/auth/common/RelayConstants$Key;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/common/RelayConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Key"
.end annotation


# static fields
.field public static final ACCESS_TOKEN:Ljava/lang/String; = "access_token"

.field public static final AUTHKEY:Ljava/lang/String; = "authKey"

.field public static final BASE_URL:Ljava/lang/String; = "base_url"

.field public static final COUNTRYCODE:Ljava/lang/String; = "country_code"

.field public static final DEVICEID:Ljava/lang/String; = "did"

.field public static final EXPIRED_ACCESS_TOKEN:Ljava/lang/String; = "expired_access_token"

.field public static final REG_ID:Ljava/lang/String; = "reg_id"

.field public static final USER_ID:Ljava/lang/String; = "user_id"
