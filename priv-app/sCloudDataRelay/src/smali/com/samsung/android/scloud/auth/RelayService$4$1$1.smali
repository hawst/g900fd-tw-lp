.class Lcom/samsung/android/scloud/auth/RelayService$4$1$1;
.super Ljava/lang/Object;
.source "RelayService.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/RelayService$4$1;->onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/scloud/auth/RelayService$4$1;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/auth/RelayService$4$1;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/RelayService$4$1$1;->this$1:Lcom/samsung/android/scloud/auth/RelayService$4$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "result"    # Lcom/samsung/android/scloud/auth/core/TaskResult;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 137
    const-string v0, "RelayService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCompleted - DEACTIVATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-static {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->clear(Landroid/content/Context;)V

    .line 139
    const-class v0, Lcom/samsung/android/scloud/auth/RelayService;

    invoke-virtual {p3, p2, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 140
    invoke-virtual {p2, p3}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 141
    return-void
.end method
