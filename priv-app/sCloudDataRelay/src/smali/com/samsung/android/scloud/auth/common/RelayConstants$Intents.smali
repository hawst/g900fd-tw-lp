.class public interface abstract Lcom/samsung/android/scloud/auth/common/RelayConstants$Intents;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/common/RelayConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Intents"
.end annotation


# static fields
.field public static final MUM_USER_STOPPED:Ljava/lang/String; = "android.intent.action.USER_STOPPED"

.field public static final MUM_USER_SWITCHED:Ljava/lang/String; = "android.intent.action.USER_SWITCHED"

.field public static final NOTIFY_ACK_SPP:Ljava/lang/String; = "NOTIFY_ACK_SPP"

.field public static final PUSH_INTENT:Ljava/lang/String; = "tj9u972o46"

.field public static final RECEIVE_V02_ACCESS_TOKEN:Ljava/lang/String; = "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

.field public static final REGISTER_MUM_HANDLER:Ljava/lang/String; = "REGISTER_MUM_HANDLER"

.field public static final REGISTRATION_RESULT:Ljava/lang/String; = "com.sec.spp.RegistrationChangedAction"

.field public static final REQUEST_V02_ACCESS_TOKEN:Ljava/lang/String; = "com.msc.action.ACCESSTOKEN_V02_REQUEST"

.field public static final SIGN_IN_COMPLETED:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

.field public static final SIGN_OUT_COMPLETE:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

.field public static final SPP_ACK_RESULT:Ljava/lang/String; = "com.sec.spp.NotificationAckResultAction"
