.class Lcom/samsung/android/scloud/auth/core/TaskHelper$1$1;
.super Ljava/lang/Object;
.source "TaskHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/core/TaskHelper$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/auth/core/TaskHelper$1;

.field final synthetic val$what:I


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/auth/core/TaskHelper$1;I)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$1$1;->this$0:Lcom/samsung/android/scloud/auth/core/TaskHelper$1;

    iput p2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$1$1;->val$what:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 47
    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$1$1;->val$what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/auth/core/TaskObject;

    .line 48
    .local v0, "task":Lcom/samsung/android/scloud/auth/core/TaskObject;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    if-eqz v1, :cond_0

    .line 49
    const-string v1, "TaskHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doTask-timeout - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v3}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", retryCnt : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v1, "TaskHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DO TASK - TIMEOUT : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v3}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", CTID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->intent:Landroid/content/Intent;

    const-string v4, "ctid"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v1, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    iget-object v2, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->context:Landroid/content/Context;

    iget-object v3, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->intent:Landroid/content/Intent;

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->onTimeout(Landroid/content/Context;Landroid/content/Intent;)V

    .line 52
    iget-object v1, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->listener:Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->COMMON_TIMEOUT_FAILED_RESULT:Lcom/samsung/android/scloud/auth/core/TaskResult;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$100()Lcom/samsung/android/scloud/auth/core/TaskResult;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->context:Landroid/content/Context;

    iget-object v4, v0, Lcom/samsung/android/scloud/auth/core/TaskObject;->intent:Landroid/content/Intent;

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;->onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    const-string v1, "TaskHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Already removed from TaskMap : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/scloud/auth/core/TaskHelper$1$1;->val$what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
