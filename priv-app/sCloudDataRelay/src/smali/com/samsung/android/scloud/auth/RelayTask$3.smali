.class final Lcom/samsung/android/scloud/auth/RelayTask$3;
.super Ljava/lang/Object;
.source "RelayTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/RelayTask;->requestToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$countryCodeKey:I

.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

.field final synthetic val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

.field final synthetic val$tag:Ljava/lang/String;

.field final synthetic val$tokenKey:I

.field final synthetic val$userIdKey:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;Lcom/samsung/android/scloud/auth/data/AuthDataManager;III)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$tag:Ljava/lang/String;

    iput-object p2, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$ctid:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

    iput-object p4, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iput p5, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$tokenKey:I

    iput p6, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$userIdKey:I

    iput p7, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$countryCodeKey:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "result"    # Lcom/samsung/android/scloud/auth/core/TaskResult;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 168
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$tag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCompleted - TokenTask : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$ctid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$tag:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TokenTask - result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$mResult:Lcom/samsung/android/scloud/auth/ResultCode;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getRcode()I

    move-result v4

    iput v4, v3, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    .line 172
    invoke-virtual {p1}, Lcom/samsung/android/scloud/auth/core/TaskResult;->isSucceeded()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 173
    const-string v3, "access_token"

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getStringData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "accessToken":Ljava/lang/String;
    const-string v3, "user_id"

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getStringData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 175
    .local v2, "userId":Ljava/lang/String;
    const-string v3, "country_code"

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/auth/core/TaskResult;->getStringData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, "cc":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v4, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$tokenKey:I

    invoke-virtual {v3, p2, v4, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->setAccessToken(Landroid/content/Context;ILjava/lang/String;)V

    .line 177
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v4, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$userIdKey:I

    invoke-virtual {v3, p2, v4, v2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->setUserId(Landroid/content/Context;ILjava/lang/String;)V

    .line 178
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v4, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$countryCodeKey:I

    invoke-virtual {v3, p2, v4, v1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->setCountryCode(Landroid/content/Context;ILjava/lang/String;)V

    .line 180
    .end local v0    # "accessToken":Ljava/lang/String;
    .end local v1    # "cc":Ljava/lang/String;
    .end local v2    # "userId":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v4, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$tokenKey:I

    invoke-virtual {v3, p2, v4}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->releaseAccessToken(Landroid/content/Context;I)V

    .line 181
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v4, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$userIdKey:I

    invoke-virtual {v3, p2, v4}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->releaseUserId(Landroid/content/Context;I)V

    .line 182
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$dataMgr:Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    iget v4, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$countryCodeKey:I

    invoke-virtual {v3, p2, v4}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->releaseCountryCode(Landroid/content/Context;I)V

    .line 183
    iget-object v3, p0, Lcom/samsung/android/scloud/auth/RelayTask$3;->val$ctid:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->notify(Ljava/lang/String;)V

    .line 184
    return-void
.end method
