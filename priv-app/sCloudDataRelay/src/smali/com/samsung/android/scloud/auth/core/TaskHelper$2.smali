.class final Lcom/samsung/android/scloud/auth/core/TaskHelper$2;
.super Ljava/lang/Object;
.source "TaskHelper.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/ITaskTrigger;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/core/TaskHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public prepared(ILandroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "ctid"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 69
    # getter for: Lcom/samsung/android/scloud/auth/core/TaskHelper;->TASK_MAP:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->access$000()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/scloud/auth/core/TaskObject;

    .line 70
    .local v1, "task":Lcom/samsung/android/scloud/auth/core/TaskObject;
    if-eqz v1, :cond_0

    .line 71
    iput-object p2, v1, Lcom/samsung/android/scloud/auth/core/TaskObject;->context:Landroid/content/Context;

    .line 72
    iput-object p3, v1, Lcom/samsung/android/scloud/auth/core/TaskObject;->intent:Landroid/content/Intent;

    .line 73
    const-string v3, "TaskHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doTask-after prepare - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    invoke-interface {v5}, Lcom/samsung/android/scloud/auth/core/ITaskHandler;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;

    invoke-direct {v0, p0, v1, p2, p3}, Lcom/samsung/android/scloud/auth/core/TaskHelper$2$1;-><init>(Lcom/samsung/android/scloud/auth/core/TaskHelper$2;Lcom/samsung/android/scloud/auth/core/TaskObject;Landroid/content/Context;Landroid/content/Intent;)V

    .line 106
    .local v0, "run":Ljava/lang/Runnable;
    new-instance v2, Ljava/lang/Thread;

    const-string v3, "TRIGGER_THREAD"

    invoke-direct {v2, v0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 107
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 109
    .end local v0    # "run":Ljava/lang/Runnable;
    .end local v2    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method
