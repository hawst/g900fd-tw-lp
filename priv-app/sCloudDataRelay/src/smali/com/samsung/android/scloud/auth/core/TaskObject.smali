.class Lcom/samsung/android/scloud/auth/core/TaskObject;
.super Ljava/lang/Object;
.source "TaskHelper.java"


# instance fields
.field context:Landroid/content/Context;

.field handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

.field intent:Landroid/content/Intent;

.field listener:Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

.field retryCnt:I

.field taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "handler"    # Lcom/samsung/android/scloud/auth/core/ITaskHandler;
    .param p4, "taskOption"    # Lcom/samsung/android/scloud/auth/core/ITaskOption;
    .param p5, "listener"    # Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/scloud/auth/core/TaskObject;->retryCnt:I

    .line 201
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/core/TaskObject;->context:Landroid/content/Context;

    .line 202
    iput-object p2, p0, Lcom/samsung/android/scloud/auth/core/TaskObject;->intent:Landroid/content/Intent;

    .line 203
    iput-object p3, p0, Lcom/samsung/android/scloud/auth/core/TaskObject;->handler:Lcom/samsung/android/scloud/auth/core/ITaskHandler;

    .line 204
    iput-object p4, p0, Lcom/samsung/android/scloud/auth/core/TaskObject;->taskOption:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    .line 205
    iput-object p5, p0, Lcom/samsung/android/scloud/auth/core/TaskObject;->listener:Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;

    .line 206
    return-void
.end method
