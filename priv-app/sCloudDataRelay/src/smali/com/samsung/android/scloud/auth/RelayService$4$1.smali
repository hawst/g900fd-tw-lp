.class Lcom/samsung/android/scloud/auth/RelayService$4$1;
.super Ljava/lang/Object;
.source "RelayService.java"

# interfaces
.implements Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/RelayService$4;->handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/auth/RelayService$4;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/auth/RelayService$4;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/RelayService$4$1;->this$0:Lcom/samsung/android/scloud/auth/RelayService$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/samsung/android/scloud/auth/core/TaskResult;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "result"    # Lcom/samsung/android/scloud/auth/core/TaskResult;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 125
    const-string v2, "RelayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCompleted - DEREGIST_SPP : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-static {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v0

    .line 129
    .local v0, "dataMgr":Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    invoke-virtual {v0, p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidBaseUrl(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130
    new-instance v1, Lcom/samsung/android/scloud/auth/task/APITask;

    invoke-direct {v1}, Lcom/samsung/android/scloud/auth/task/APITask;-><init>()V

    .line 131
    .local v1, "task":Lcom/samsung/android/scloud/auth/task/APITask;
    const-string v2, "command"

    const/16 v3, 0xa

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 133
    sget-object v2, Lcom/samsung/android/scloud/auth/RelayService;->DEFAULT_TASK_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    new-instance v3, Lcom/samsung/android/scloud/auth/RelayService$4$1$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/scloud/auth/RelayService$4$1$1;-><init>(Lcom/samsung/android/scloud/auth/RelayService$4$1;)V

    invoke-static {p2, p3, v1, v2, v3}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 148
    .end local v1    # "task":Lcom/samsung/android/scloud/auth/task/APITask;
    :goto_0
    return-void

    .line 144
    :cond_0
    invoke-static {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->clear(Landroid/content/Context;)V

    .line 145
    const-class v2, Lcom/samsung/android/scloud/auth/RelayService;

    invoke-virtual {p3, p2, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 146
    invoke-virtual {p2, p3}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method
