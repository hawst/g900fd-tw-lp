.class public interface abstract Lcom/samsung/android/scloud/auth/common/RelayConstants$RELAY_STATUS_CODE;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/common/RelayConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RELAY_STATUS_CODE"
.end annotation


# static fields
.field public static final ACCESS_TOKEN_CANCELLED:I = 0x3eb

.field public static final ACCESS_TOKEN_FAILED:I = 0x3ec

.field public static final ACCESS_TOKEN_NETWORK_ERROR:I = 0x3ed

.field public static final AUTH_FAILED_MSG:Ljava/lang/String; = "e"

.field public static final AUTH_SUCCEEDED:I = 0x0

.field public static final AUTH_SUCCEEDED_MSG:Ljava/lang/String; = "r"

.field public static final BASEURL_FAILED:I = 0x7d2

.field public static final CLOUD_PREPARE_FAILED:I = 0xbb9

.field public static final COUNTRY_CODE_FAILED:I = 0x3ee

.field public static final FAILED_ACTIVATION:I = 0x3ed

.field public static final HTTP_NETWORK_RESPONSE:I = 0x190

.field public static final HTTP_REQUEST_GET_ERROR:I = 0x191

.field public static final HTTP_REQUEST_POST_ERROR:I = 0x192

.field public static final PREPARE_FAILED:I = 0x2

.field public static final PROCESS_TIMEOUT:I = 0x1

.field public static final PUSHDEREG_FAILED:I = 0x3ea

.field public static final PUSHREG_FAILED:I = 0x3e9

.field public static final PUSH_BIND_FAILED:I = 0x3eb

.field public static final REGID_FAILED:I = 0x7d3

.field public static final TOKEN_FAILED:I = 0x7d4

.field public static final USERID_FAILED:I = 0x7d1
