.class public interface abstract Lcom/samsung/android/scloud/auth/common/RelayConstants$PUSH_RESULT;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/common/RelayConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PUSH_RESULT"
.end annotation


# static fields
.field public static final PUSH_DEREGISTRATION_FAIL:I = 0x3

.field public static final PUSH_DEREGISTRATION_SUCCESS:I = 0x2

.field public static final PUSH_IGNORED:I = 0x4

.field public static final PUSH_REGISTRATION_FAIL:I = 0x1

.field public static final PUSH_REGISTRATION_SUCCESS:I = 0x0

.field public static final PUSH_UNKNOWN:I = -0x1
