.class public Lcom/samsung/android/scloud/auth/common/RelayConstants;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/auth/common/RelayConstants$PUSH_RESULT;,
        Lcom/samsung/android/scloud/auth/common/RelayConstants$CID;,
        Lcom/samsung/android/scloud/auth/common/RelayConstants$RELAY_STATUS_CODE;,
        Lcom/samsung/android/scloud/auth/common/RelayConstants$API_RESPONSE_CODE;,
        Lcom/samsung/android/scloud/auth/common/RelayConstants$TOKEN_INFO;,
        Lcom/samsung/android/scloud/auth/common/RelayConstants$API;,
        Lcom/samsung/android/scloud/auth/common/RelayConstants$Commands;,
        Lcom/samsung/android/scloud/auth/common/RelayConstants$Intents;,
        Lcom/samsung/android/scloud/auth/common/RelayConstants$Key;
    }
.end annotation


# static fields
.field public static final APPID:Ljava/lang/String; = "tj9u972o46"

.field public static final APP_KEY:Ljava/lang/String; = "D234AE3C42F092D4334433173AE9E264"

.field public static final AUTH_STATIC_URL:Ljava/lang/String; = "https://sc-auth.samsungosp.com"

.field private static final AUTH_STATIC_URL_P:Ljava/lang/String; = "https://sc-auth.samsungosp.com"

.field private static final AUTH_STATIC_URL_STG:Ljava/lang/String; = "https://stga.sc-proxy.samsungosp.com"

.field public static final BAD_ACCESS_TOKEN:Ljava/lang/String; = "BAD_ACCESS_TOKEN"

.field public static final COMMAND:Ljava/lang/String; = "command"

.field public static final CTID:Ljava/lang/String; = "ctid"

.field public static final INTERNAL_SERVER_ERROR:Ljava/lang/String; = "INTERNAL_SERVER_ERROR"

.field public static final MAX_RETRY_COUNT:I = 0x2

.field public static final MAX_TIME_OUT:J = 0x3a98L

.field public static final REQUEST_NEW_TOKEN:Ljava/lang/String; = "request_new_token"

.field public static final REQUEST_TIME_OUT:Ljava/lang/String; = "REQUEST_TIME_OUT"

.field public static final RESULT:Ljava/lang/String; = "result"

.field public static final RE_PROVISIONING_REQUIRED:Ljava/lang/String; = "RE_PROVISIONING_REQUIRED"

.field public static final TRIGGER:Ljava/lang/String; = "trigger"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    return-void
.end method
