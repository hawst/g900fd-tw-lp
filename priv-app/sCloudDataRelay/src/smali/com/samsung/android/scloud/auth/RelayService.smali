.class public Lcom/samsung/android/scloud/auth/RelayService;
.super Landroid/app/Service;
.source "RelayService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/auth/RelayService$RelayServiceHandler;
    }
.end annotation


# static fields
.field public static DEFAULT_TASK_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption; = null

.field private static final HANDLER_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/auth/RelayService$RelayServiceHandler;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "RelayService"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Lcom/samsung/android/scloud/auth/RelayService$1;

    invoke-direct {v0}, Lcom/samsung/android/scloud/auth/RelayService$1;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/RelayService;->DEFAULT_TASK_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/RelayService;->HANDLER_MAP:Ljava/util/Map;

    .line 94
    sget-object v0, Lcom/samsung/android/scloud/auth/RelayService;->HANDLER_MAP:Ljava/util/Map;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    new-instance v2, Lcom/samsung/android/scloud/auth/RelayService$3;

    invoke-direct {v2}, Lcom/samsung/android/scloud/auth/RelayService$3;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/samsung/android/scloud/auth/RelayService;->HANDLER_MAP:Ljava/util/Map;

    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    new-instance v2, Lcom/samsung/android/scloud/auth/RelayService$4;

    invoke-direct {v2}, Lcom/samsung/android/scloud/auth/RelayService$4;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/samsung/android/scloud/auth/RelayService;->HANDLER_MAP:Ljava/util/Map;

    const-string v1, "NOTIFY_ACK_SPP"

    new-instance v2, Lcom/samsung/android/scloud/auth/RelayService$5;

    invoke-direct {v2}, Lcom/samsung/android/scloud/auth/RelayService$5;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/samsung/android/scloud/auth/RelayService;->HANDLER_MAP:Ljava/util/Map;

    const-string v1, "com.sec.spp.RegistrationChangedAction"

    new-instance v2, Lcom/samsung/android/scloud/auth/RelayService$6;

    invoke-direct {v2}, Lcom/samsung/android/scloud/auth/RelayService$6;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/samsung/android/scloud/auth/RelayService;->HANDLER_MAP:Ljava/util/Map;

    const-string v1, "tj9u972o46"

    new-instance v2, Lcom/samsung/android/scloud/auth/RelayService$7;

    invoke-direct {v2}, Lcom/samsung/android/scloud/auth/RelayService$7;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 87
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 43
    const-string v0, "RelayService"

    const-string v1, "onBind(Intent arg0)"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 50
    const-string v0, "RelayService"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 56
    const-string v0, "RelayService"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 58
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 63
    if-eqz p1, :cond_0

    .line 65
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 67
    const-string v3, "RelayService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RelayService Started!! : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    sget-object v3, Lcom/samsung/android/scloud/auth/RelayService;->HANDLER_MAP:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/auth/RelayService$RelayServiceHandler;

    .line 70
    .local v0, "handler":Lcom/samsung/android/scloud/auth/RelayService$RelayServiceHandler;
    new-instance v1, Lcom/samsung/android/scloud/auth/RelayService$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/samsung/android/scloud/auth/RelayService$2;-><init>(Lcom/samsung/android/scloud/auth/RelayService;Lcom/samsung/android/scloud/auth/RelayService$RelayServiceHandler;Landroid/content/Intent;)V

    .line 78
    .local v1, "serviceTask":Ljava/lang/Runnable;
    new-instance v2, Ljava/lang/Thread;

    const-string v3, "SERVICE_THREAD"

    invoke-direct {v2, v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 79
    .local v2, "serviceThread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 81
    const/4 v3, 0x2

    .line 84
    .end local v0    # "handler":Lcom/samsung/android/scloud/auth/RelayService$RelayServiceHandler;
    .end local v1    # "serviceTask":Ljava/lang/Runnable;
    .end local v2    # "serviceThread":Ljava/lang/Thread;
    :goto_0
    return v3

    :cond_0
    const/4 v3, -0x1

    goto :goto_0
.end method
