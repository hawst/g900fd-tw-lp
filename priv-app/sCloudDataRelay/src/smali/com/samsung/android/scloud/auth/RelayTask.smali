.class public Lcom/samsung/android/scloud/auth/RelayTask;
.super Ljava/lang/Object;
.source "RelayTask.java"


# static fields
.field public static final EMPTY:Ljava/lang/String; = ""

.field private static final SUCCESS:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static prepareAuthInformation(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)I
    .locals 18
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ctid"    # Ljava/lang/String;

    .prologue
    .line 32
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DATA RELAY VERSION : 3.06.000\nprepareAuthInformation - Start : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Thread : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v7

    .line 36
    .local v7, "dataMgr":Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    new-instance v6, Lcom/samsung/android/scloud/auth/ResultCode;

    invoke-direct {v6}, Lcom/samsung/android/scloud/auth/ResultCode;-><init>()V

    .line 38
    .local v6, "mResult":Lcom/samsung/android/scloud/auth/ResultCode;
    new-instance v16, Landroid/content/Intent;

    invoke-direct/range {v16 .. v16}, Landroid/content/Intent;-><init>()V

    .line 39
    .local v16, "intent":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidAccessToken(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidUserId(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 42
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v7, v2, v6}, Lcom/samsung/android/scloud/auth/RelayTask;->requestToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V

    .line 43
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const/16 v5, 0x3ee

    if-ne v4, v5, :cond_1

    .line 44
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v7, v2, v6}, Lcom/samsung/android/scloud/auth/RelayTask;->requestNewToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V

    .line 48
    :cond_1
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidAccessToken(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidUserId(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidCountryCode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 50
    :cond_2
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    .line 151
    :goto_0
    return v4

    .line 53
    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidAccessToken(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidUserId(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidBaseUrl(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 54
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v7, v2, v6}, Lcom/samsung/android/scloud/auth/RelayTask;->requestRegisterToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V

    .line 56
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    .line 57
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const/16 v5, 0x4a40

    if-ne v4, v5, :cond_4

    .line 58
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v7, v2, v6}, Lcom/samsung/android/scloud/auth/RelayTask;->requestNewToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V

    .line 59
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_5

    .line 60
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v7, v2, v6}, Lcom/samsung/android/scloud/auth/RelayTask;->requestRegisterToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V

    .line 61
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    .line 62
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    goto :goto_0

    .line 65
    :cond_4
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const v5, 0xc030

    if-ne v4, v5, :cond_6

    .line 66
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->resetBaseUrl(Landroid/content/Context;)V

    .line 67
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->resetAuthKey(Landroid/content/Context;)V

    .line 68
    const/4 v4, -0x1

    iput v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    .line 77
    :cond_5
    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidAccessToken(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 78
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    goto :goto_0

    .line 69
    :cond_6
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 71
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    goto :goto_0

    .line 73
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->resetAccessToken(Landroid/content/Context;)V

    goto :goto_1

    .line 81
    :cond_8
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidRegId(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 82
    invoke-virtual {v7}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchRegId()I

    move-result v8

    .line 83
    .local v8, "key":I
    const/4 v4, 0x2

    invoke-static {v4}, Lcom/samsung/android/scloud/auth/task/SppTask;->getInstance(I)Lcom/samsung/android/scloud/auth/task/SppTask;

    move-result-object v17

    .line 84
    .local v17, "sppTask":Lcom/samsung/android/scloud/auth/task/SppTask;
    const-string v4, "command"

    const/4 v5, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 85
    const-string v4, "ctid"

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    sget-object v10, Lcom/samsung/android/scloud/auth/task/SppTask;->sppTask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    new-instance v4, Lcom/samsung/android/scloud/auth/RelayTask$1;

    move-object/from16 v5, p0

    move-object/from16 v9, p2

    invoke-direct/range {v4 .. v9}, Lcom/samsung/android/scloud/auth/RelayTask$1;-><init>(Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;Lcom/samsung/android/scloud/auth/data/AuthDataManager;ILjava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2, v10, v4}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 100
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->pause(Ljava/lang/String;)V

    .line 103
    .end local v8    # "key":I
    .end local v17    # "sppTask":Lcom/samsung/android/scloud/auth/task/SppTask;
    :cond_9
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidRegId(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 104
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    goto/16 :goto_0

    .line 107
    :cond_a
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidBaseUrl(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidAuthKey(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 108
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v7, v2, v6}, Lcom/samsung/android/scloud/auth/RelayTask;->requestServerInfo(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V

    .line 110
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const/16 v5, 0x4a40

    if-ne v4, v5, :cond_c

    .line 111
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v7, v2, v6}, Lcom/samsung/android/scloud/auth/RelayTask;->requestNewToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V

    .line 112
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_c

    .line 113
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v7, v2, v6}, Lcom/samsung/android/scloud/auth/RelayTask;->requestServerInfo(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V

    .line 119
    :cond_c
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidBaseUrl(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_d

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidAuthKey(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_d

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidAccessToken(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 121
    :cond_d
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    goto/16 :goto_0

    .line 124
    :cond_e
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidDid(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_f

    .line 125
    invoke-virtual {v7}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchDid()I

    move-result v13

    .line 126
    .local v13, "didKey":I
    new-instance v15, Lcom/samsung/android/scloud/auth/task/APITask;

    invoke-direct {v15}, Lcom/samsung/android/scloud/auth/task/APITask;-><init>()V

    .line 127
    .local v15, "activateTask":Lcom/samsung/android/scloud/auth/task/APITask;
    const-string v4, "command"

    const/4 v5, 0x5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 128
    const-string v4, "ctid"

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    sget-object v4, Lcom/samsung/android/scloud/auth/task/APITask;->APITask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    new-instance v9, Lcom/samsung/android/scloud/auth/RelayTask$2;

    move-object/from16 v10, p0

    move-object v11, v6

    move-object v12, v7

    move-object/from16 v14, p2

    invoke-direct/range {v9 .. v14}, Lcom/samsung/android/scloud/auth/RelayTask$2;-><init>(Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;Lcom/samsung/android/scloud/auth/data/AuthDataManager;ILjava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1, v15, v4, v9}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 144
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->pause(Ljava/lang/String;)V

    .line 147
    .end local v13    # "didKey":I
    .end local v15    # "activateTask":Lcom/samsung/android/scloud/auth/task/APITask;
    :cond_f
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isValidAuthInformation(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 148
    const-string v4, "VALID AUTH INFO SUCCESS"

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 151
    :cond_10
    iget v4, v6, Lcom/samsung/android/scloud/auth/ResultCode;->rCode:I

    goto/16 :goto_0
.end method

.method private static requestNewToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V
    .locals 12
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dataMgr"    # Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "mResult"    # Lcom/samsung/android/scloud/auth/ResultCode;

    .prologue
    .line 191
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 192
    .local v9, "intent":Landroid/content/Intent;
    invoke-virtual {p2, p1}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getAccessToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 193
    .local v8, "accessToken":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchUserId()I

    move-result v6

    .line 194
    .local v6, "userIdKey":I
    invoke-virtual {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchCountryCode()I

    move-result v7

    .line 195
    .local v7, "countryCodeKey":I
    invoke-virtual {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchAccessToken()I

    move-result v5

    .line 197
    .local v5, "tokenKey":I
    const-string v0, "expired_access_token"

    invoke-virtual {v9, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    const-string v0, "request_new_token"

    const/4 v1, 0x1

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 199
    const-string v0, "command"

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 200
    const-string v0, "ctid"

    invoke-virtual {v9, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/scloud/auth/task/TokenTask;->getInstance(I)Lcom/samsung/android/scloud/auth/task/TokenTask;

    move-result-object v10

    .line 203
    .local v10, "reqTknTask":Lcom/samsung/android/scloud/auth/task/TokenTask;
    sget-object v11, Lcom/samsung/android/scloud/auth/task/TokenTask;->RequestTokenTask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    new-instance v0, Lcom/samsung/android/scloud/auth/RelayTask$4;

    move-object v1, p0

    move-object v2, p3

    move-object/from16 v3, p4

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/scloud/auth/RelayTask$4;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;Lcom/samsung/android/scloud/auth/data/AuthDataManager;III)V

    invoke-static {p1, v9, v10, v11, v0}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 228
    invoke-static {p3}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->pause(Ljava/lang/String;)V

    .line 229
    return-void
.end method

.method private static requestRegisterToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dataMgr"    # Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "mResult"    # Lcom/samsung/android/scloud/auth/ResultCode;

    .prologue
    .line 265
    new-instance v1, Lcom/samsung/android/scloud/auth/task/APITask;

    invoke-direct {v1}, Lcom/samsung/android/scloud/auth/task/APITask;-><init>()V

    .line 266
    .local v1, "task":Lcom/samsung/android/scloud/auth/task/APITask;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 267
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "command"

    const/16 v3, 0xb

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 268
    const-string v2, "ctid"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    sget-object v2, Lcom/samsung/android/scloud/auth/task/APITask;->APITask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    new-instance v3, Lcom/samsung/android/scloud/auth/RelayTask$6;

    invoke-direct {v3, p4, p0, p3}, Lcom/samsung/android/scloud/auth/RelayTask$6;-><init>(Lcom/samsung/android/scloud/auth/ResultCode;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v0, v1, v2, v3}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 278
    invoke-static {p3}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->pause(Ljava/lang/String;)V

    .line 279
    return-void
.end method

.method private static requestServerInfo(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V
    .locals 10
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dataMgr"    # Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "mResult"    # Lcom/samsung/android/scloud/auth/ResultCode;

    .prologue
    .line 233
    invoke-virtual {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchBaseUrl()I

    move-result v5

    .line 234
    .local v5, "baseUrlKey":I
    invoke-virtual {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchAuthKey()I

    move-result v6

    .line 235
    .local v6, "AuthKey_Key":I
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 236
    .local v7, "intent":Landroid/content/Intent;
    new-instance v8, Lcom/samsung/android/scloud/auth/task/APITask;

    invoke-direct {v8}, Lcom/samsung/android/scloud/auth/task/APITask;-><init>()V

    .line 237
    .local v8, "provisioningTask":Lcom/samsung/android/scloud/auth/task/APITask;
    const-string v0, "command"

    const/4 v1, 0x7

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 238
    const-string v0, "ctid"

    invoke-virtual {v7, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    sget-object v9, Lcom/samsung/android/scloud/auth/task/APITask;->APITask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    new-instance v0, Lcom/samsung/android/scloud/auth/RelayTask$5;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/scloud/auth/RelayTask$5;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;Lcom/samsung/android/scloud/auth/data/AuthDataManager;II)V

    invoke-static {p1, v7, v8, v9, v0}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 261
    invoke-static {p3}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->pause(Ljava/lang/String;)V

    .line 262
    return-void
.end method

.method private static requestToken(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/scloud/auth/data/AuthDataManager;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;)V
    .locals 11
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dataMgr"    # Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "mResult"    # Lcom/samsung/android/scloud/auth/ResultCode;

    .prologue
    const/4 v1, 0x0

    .line 156
    invoke-virtual {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchAccessToken()I

    move-result v5

    .line 157
    .local v5, "tokenKey":I
    invoke-virtual {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchUserId()I

    move-result v6

    .line 158
    .local v6, "userIdKey":I
    invoke-virtual {p2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->catchCountryCode()I

    move-result v7

    .line 160
    .local v7, "countryCodeKey":I
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 161
    .local v8, "intent":Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/android/scloud/auth/task/TokenTask;->getInstance(I)Lcom/samsung/android/scloud/auth/task/TokenTask;

    move-result-object v9

    .line 162
    .local v9, "reqTknTask":Lcom/samsung/android/scloud/auth/task/TokenTask;
    const-string v0, "command"

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 163
    const-string v0, "ctid"

    invoke-virtual {v8, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    sget-object v10, Lcom/samsung/android/scloud/auth/task/TokenTask;->RequestTokenTask_OPTION:Lcom/samsung/android/scloud/auth/core/ITaskOption;

    new-instance v0, Lcom/samsung/android/scloud/auth/RelayTask$3;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/scloud/auth/RelayTask$3;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/auth/ResultCode;Lcom/samsung/android/scloud/auth/data/AuthDataManager;III)V

    invoke-static {p1, v8, v9, v10, v0}, Lcom/samsung/android/scloud/auth/core/TaskHelper;->doTask(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/scloud/auth/core/ITaskHandler;Lcom/samsung/android/scloud/auth/core/ITaskOption;Lcom/samsung/android/scloud/auth/core/TaskHelper$TaskResultListener;)V

    .line 186
    invoke-static {p3}, Lcom/samsung/android/scloud/auth/util/RelayUtil;->pause(Ljava/lang/String;)V

    .line 187
    return-void
.end method
