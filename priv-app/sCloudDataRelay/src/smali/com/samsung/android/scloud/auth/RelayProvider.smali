.class public Lcom/samsung/android/scloud/auth/RelayProvider;
.super Landroid/content/ContentProvider;
.source "RelayProvider.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field public static final lock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-string v0, "RelayProvider"

    sput-object v0, Lcom/samsung/android/scloud/auth/RelayProvider;->TAG:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/auth/RelayProvider;->lock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 57
    const-string v0, "getAuthInformation"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0, p2}, Lcom/samsung/android/scloud/auth/RelayProvider;->getAuthInformation(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 61
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/auth/RelayProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid call(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public getAuthInformation(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 11
    .param p1, "ctid"    # Ljava/lang/String;

    .prologue
    .line 68
    sget-object v8, Lcom/samsung/android/scloud/auth/RelayProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getAuthInformation CTID : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/android/scloud/auth/RelayProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 70
    .local v3, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/auth/RelayProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/auth/data/AuthDataManager;

    move-result-object v4

    .line 71
    .local v4, "dataMgr":Lcom/samsung/android/scloud/auth/data/AuthDataManager;
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 72
    .local v5, "mBundle":Landroid/os/Bundle;
    sget-object v8, Lcom/samsung/android/scloud/auth/RelayProvider;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/auth/RelayProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v8, v9, p1}, Lcom/samsung/android/scloud/auth/RelayTask;->prepareAuthInformation(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 73
    .local v1, "auth_Result":I
    const-string v8, "status"

    invoke-virtual {v5, v8, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 98
    invoke-virtual {v4, v3}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getUserId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 99
    .local v7, "userId":Ljava/lang/String;
    invoke-virtual {v4, v3}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getRegId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 100
    .local v6, "regId":Ljava/lang/String;
    invoke-virtual {v4, v3}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getAccessToken(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "accessToken":Ljava/lang/String;
    invoke-virtual {v4, v3}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->getBaseUrl(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 103
    .local v2, "baseUrl":Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/auth/data/AuthDataManager;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 104
    const/4 v2, 0x0

    .line 116
    :cond_0
    const-string v8, "userId"

    invoke-virtual {v5, v8, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v8, "regId"

    invoke-virtual {v5, v8, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v8, "accessToken"

    invoke-virtual {v5, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v8, "baseUrl"

    invoke-virtual {v5, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    sget-object v8, Lcom/samsung/android/scloud/auth/RelayProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getAuthInformation Finished : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\nstatus : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "status"

    invoke-virtual {v5, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    sget-object v8, Lcom/samsung/android/scloud/auth/RelayProvider;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "\nuserId : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\nregId : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\naccessToken : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\nbaseUrl : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/auth/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-object v5
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 31
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/scloud/auth/RelayProvider;->TAG:Ljava/lang/String;

    const-string v1, "===onCreate===="

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 47
    const/4 v0, 0x0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method
