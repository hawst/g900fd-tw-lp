.class public interface abstract Lcom/samsung/android/scloud/auth/common/RelayConstants$TOKEN_INFO;
.super Ljava/lang/Object;
.source "RelayConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/auth/common/RelayConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TOKEN_INFO"
.end annotation


# static fields
.field public static final ACCESS_TOKEN_CANCELLED:I = 0x0

.field public static final ACCESS_TOKEN_FAILED:I = 0x1

.field public static final ACCESS_TOKEN_NETWORK_ERROR:I = 0x3

.field public static final ACCESS_TOKEN_NOT_RECEIVED:I = 0x64

.field public static final ACCESS_TOKEN_RECEIVED:I = -0x1

.field public static final FAILED_ACTIVATION:I = 0xd

.field public static final KEY_BG_RESULT:Ljava/lang/String; = "bg_result"

.field public static final RESULT_CODE:Ljava/lang/String; = "result_code"
