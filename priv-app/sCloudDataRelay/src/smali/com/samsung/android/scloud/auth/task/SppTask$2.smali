.class Lcom/samsung/android/scloud/auth/task/SppTask$2;
.super Ljava/lang/Object;
.source "SppTask.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/auth/task/SppTask;->bindToSpp(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/auth/task/SppTask;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$mapId:I

.field final synthetic val$trigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/auth/task/SppTask;Ljava/lang/String;ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/SppTask;

    iput-object p2, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$ctid:Ljava/lang/String;

    iput p3, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$mapId:I

    iput-object p4, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$trigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    iput-object p5, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$context:Landroid/content/Context;

    iput-object p6, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/SppTask;

    invoke-static {p2}, Lcom/sec/spp/push/IPushClientService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/spp/push/IPushClientService;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/task/SppTask;->access$102(Lcom/samsung/android/scloud/auth/task/SppTask;Lcom/sec/spp/push/IPushClientService;)Lcom/sec/spp/push/IPushClientService;

    .line 147
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/SppTask;

    # getter for: Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;
    invoke-static {v0}, Lcom/samsung/android/scloud/auth/task/SppTask;->access$100(Lcom/samsung/android/scloud/auth/task/SppTask;)Lcom/sec/spp/push/IPushClientService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 148
    const-string v0, "SppTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=======CANNOT BIND TO SPP==========, CTID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$ctid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :goto_0
    return-void

    .line 154
    :cond_0
    const-string v0, "SppTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=======SUCCESS BIND TO SPP==========, CTID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$ctid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/SppTask;

    iget v1, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$mapId:I

    iget-object v2, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$trigger:Lcom/samsung/android/scloud/auth/core/ITaskTrigger;

    iget-object v3, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/samsung/android/scloud/auth/task/SppTask;->prepareWithBindedServie(ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/auth/task/SppTask;->access$200(Lcom/samsung/android/scloud/auth/task/SppTask;ILcom/samsung/android/scloud/auth/core/ITaskTrigger;Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 140
    const-string v0, "SppTask"

    const-string v1, "=======onServiceDisconnected=========="

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/samsung/android/scloud/auth/task/SppTask$2;->this$0:Lcom/samsung/android/scloud/auth/task/SppTask;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/scloud/auth/task/SppTask;->mService:Lcom/sec/spp/push/IPushClientService;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/auth/task/SppTask;->access$102(Lcom/samsung/android/scloud/auth/task/SppTask;Lcom/sec/spp/push/IPushClientService;)Lcom/sec/spp/push/IPushClientService;

    .line 142
    return-void
.end method
