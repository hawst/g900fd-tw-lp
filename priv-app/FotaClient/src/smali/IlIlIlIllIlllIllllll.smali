.class public LIlIlIlIllIlllIllllll;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public static IIIIllIlIIlIIIIlllIl(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 150
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 151
    if-nez v0, :cond_0

    .line 152
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "AlarmManager is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 164
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    .line 157
    invoke-static {p0, v2, v3}, LlIlIlIIIIlIIlIlIIlII;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)Z

    .line 159
    new-instance v1, Landroid/content/Intent;

    const-string v4, "sec.fota.action.DISCLAIMER_TIME"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x8000000

    invoke-static {p0, v6, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 163
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start retry disclaimer timer after "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 116
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 117
    if-nez v0, :cond_0

    .line 118
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "AlarmManager is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 125
    :goto_0
    return-void

    .line 121
    :cond_0
    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "sec.fota.action.DISCLAIMER_TIME"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x10000000

    invoke-static {p0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 124
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Stop disclaimer timer"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 171
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd/HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 172
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 173
    return-object v0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 40
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Calculate auto register time"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 42
    sget-wide v2, LllIIIIlllllIIllllllI;->IlIlllIIlIlIIIlIlIll:J

    .line 43
    add-long/2addr v2, v0

    .line 45
    sget-object v4, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Current time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0, v1}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 46
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Next disclaimer time:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 47
    invoke-static {p0, v2, v3}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 48
    return-void
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;J)V
    .locals 7

    .prologue
    .line 99
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 100
    if-nez v0, :cond_0

    .line 101
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "AlarmManager is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 110
    :goto_0
    return-void

    .line 104
    :cond_0
    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p1

    const/4 v4, 0x0

    new-instance v5, Landroid/content/Intent;

    const-string v6, "sec.fota.action.DISCLAIMER_TIME"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v6, 0x8000000

    invoke-static {p0, v4, v5, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 109
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start disclaimer timer after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIllllIIlllIIIIlll(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 132
    invoke-static {p0}, LlIlIlIIIIlIIlIlIIlII;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)J

    move-result-wide v4

    .line 133
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    .line 134
    sget-object v1, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Disclaimer time is zero"

    invoke-virtual {v1, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 142
    :cond_0
    :goto_0
    return v0

    .line 136
    :cond_1
    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    .line 137
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Current time:"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 138
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Next retry disclaimer time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v4, v5}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 139
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Disclaimer time is vaild"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 140
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 81
    invoke-static {p0}, LIIIIlllIIIIIlllIIlll;->IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)J

    move-result-wide v4

    .line 82
    sget-object v1, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Current time:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2, v3}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 83
    sget-object v1, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Next term time:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4, v5}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 85
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    .line 86
    sget-object v1, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Disclaimer time is zero"

    invoke-virtual {v1, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 88
    :cond_1
    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    .line 89
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Disclaimer time is vaild"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 90
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static lllIlIlIIIllIIlIllIl(Landroid/content/Context;)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 69
    invoke-static {p0}, LIIIIlllIIIIIlllIIlll;->IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)J

    move-result-wide v4

    .line 70
    sub-long v2, v4, v2

    .line 71
    cmp-long v4, v2, v0

    if-gez v4, :cond_0

    .line 73
    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 54
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Calculate next disclaimer time"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 56
    sget v2, LllIIIIlllllIIllllllI;->IlIIIllllIIlIIIIIlII:I

    int-to-long v2, v2

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v2, v4

    .line 57
    add-long/2addr v2, v0

    .line 59
    sget-object v4, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Current time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0, v1}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 60
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Next disclaimer time:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 61
    invoke-static {p0, v2, v3}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 62
    return-void
.end method
