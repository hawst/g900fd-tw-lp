.class public LIlIllIIIIllllllIlIIl;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private llIIIIlllllIIllIIllI(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 58
    const-string v0, ""

    .line 59
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    sget-object v1, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Decoding source is null: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 82
    :goto_0
    return-object v0

    .line 65
    :cond_0
    invoke-static {p1}, LllllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)[B

    move-result-object v1

    .line 67
    if-nez v1, :cond_1

    .line 68
    sget-object v1, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Base 64 Decoding output is null: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_1
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 76
    const-string v0, ""

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 79
    const-string v0, ""

    goto :goto_0
.end method

.method private llllIIIllIlIIIIllllI(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x33

    const/16 v7, 0x32

    const/16 v6, 0x31

    const/16 v5, 0x30

    const/16 v4, 0x2d

    .line 87
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "decrypt string is empty: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 89
    const-string v0, ""

    .line 247
    :goto_0
    return-object v0

    .line 92
    :cond_0
    const/16 v0, 0x100

    new-array v2, v0, [C

    .line 93
    const/16 v0, 0x100

    new-array v3, v0, [C

    .line 94
    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x100

    if-ge v0, v1, :cond_1

    .line 95
    int-to-char v1, v0

    aput-char v1, v2, v0

    .line 96
    int-to-char v1, v0

    aput-char v1, v3, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 98
    :cond_1
    const/16 v0, 0x62

    aput-char v0, v2, v5

    .line 99
    const/16 v0, 0x38

    aput-char v0, v2, v6

    .line 100
    const/16 v0, 0x70

    aput-char v0, v2, v7

    .line 101
    const/16 v0, 0x6a

    aput-char v0, v2, v8

    .line 102
    const/16 v0, 0x34

    const/16 v1, 0x41

    aput-char v1, v2, v0

    .line 103
    const/16 v0, 0x35

    const/16 v1, 0x66

    aput-char v1, v2, v0

    .line 104
    const/16 v0, 0x36

    const/16 v1, 0x48

    aput-char v1, v2, v0

    .line 105
    const/16 v0, 0x37

    const/16 v1, 0x78

    aput-char v1, v2, v0

    .line 106
    const/16 v0, 0x38

    const/16 v1, 0x59

    aput-char v1, v2, v0

    .line 107
    const/16 v0, 0x39

    const/16 v1, 0x68

    aput-char v1, v2, v0

    .line 108
    const/16 v0, 0x61

    const/16 v1, 0x47

    aput-char v1, v2, v0

    .line 109
    const/16 v0, 0x62

    const/16 v1, 0x6e

    aput-char v1, v2, v0

    .line 110
    const/16 v0, 0x63

    const/16 v1, 0x6c

    aput-char v1, v2, v0

    .line 111
    const/16 v0, 0x64

    const/16 v1, 0x54

    aput-char v1, v2, v0

    .line 112
    const/16 v0, 0x65

    const/16 v1, 0x74

    aput-char v1, v2, v0

    .line 113
    const/16 v0, 0x66

    aput-char v6, v2, v0

    .line 114
    const/16 v0, 0x67

    const/16 v1, 0x76

    aput-char v1, v2, v0

    .line 115
    const/16 v0, 0x68

    const/16 v1, 0x55

    aput-char v1, v2, v0

    .line 116
    const/16 v0, 0x69

    const/16 v1, 0x64

    aput-char v1, v2, v0

    .line 117
    const/16 v0, 0x6a

    const/16 v1, 0x4d

    aput-char v1, v2, v0

    .line 118
    const/16 v0, 0x6b

    const/16 v1, 0x35

    aput-char v1, v2, v0

    .line 119
    const/16 v0, 0x6c

    const/16 v1, 0x72

    aput-char v1, v2, v0

    .line 120
    const/16 v0, 0x6d

    const/16 v1, 0x5f

    aput-char v1, v2, v0

    .line 121
    const/16 v0, 0x6e

    const/16 v1, 0x45

    aput-char v1, v2, v0

    .line 122
    const/16 v0, 0x6f

    const/16 v1, 0x4a

    aput-char v1, v2, v0

    .line 123
    const/16 v0, 0x70

    const/16 v1, 0x43

    aput-char v1, v2, v0

    .line 124
    const/16 v0, 0x71

    const/16 v1, 0x5a

    aput-char v1, v2, v0

    .line 125
    const/16 v0, 0x72

    aput-char v8, v2, v0

    .line 126
    const/16 v0, 0x73

    const/16 v1, 0x52

    aput-char v1, v2, v0

    .line 127
    const/16 v0, 0x74

    const/16 v1, 0x57

    aput-char v1, v2, v0

    .line 128
    const/16 v0, 0x75

    const/16 v1, 0x61

    aput-char v1, v2, v0

    .line 129
    const/16 v0, 0x76

    const/16 v1, 0x49

    aput-char v1, v2, v0

    .line 130
    const/16 v0, 0x77

    const/16 v1, 0x37

    aput-char v1, v2, v0

    .line 131
    const/16 v0, 0x78

    const/16 v1, 0x77

    aput-char v1, v2, v0

    .line 132
    const/16 v0, 0x79

    const/16 v1, 0x50

    aput-char v1, v2, v0

    .line 133
    const/16 v0, 0x7a

    const/16 v1, 0x6d

    aput-char v1, v2, v0

    .line 134
    const/16 v0, 0x41

    const/16 v1, 0x58

    aput-char v1, v2, v0

    .line 135
    const/16 v0, 0x42

    const/16 v1, 0x69

    aput-char v1, v2, v0

    .line 136
    const/16 v0, 0x43

    const/16 v1, 0x7a

    aput-char v1, v2, v0

    .line 137
    const/16 v0, 0x44

    aput-char v5, v2, v0

    .line 138
    const/16 v0, 0x45

    const/16 v1, 0x4b

    aput-char v1, v2, v0

    .line 139
    const/16 v0, 0x46

    const/16 v1, 0x6f

    aput-char v1, v2, v0

    .line 140
    const/16 v0, 0x47

    const/16 v1, 0x56

    aput-char v1, v2, v0

    .line 141
    const/16 v0, 0x48

    const/16 v1, 0x39

    aput-char v1, v2, v0

    .line 142
    const/16 v0, 0x49

    const/16 v1, 0x51

    aput-char v1, v2, v0

    .line 143
    const/16 v0, 0x4a

    const/16 v1, 0x65

    aput-char v1, v2, v0

    .line 144
    const/16 v0, 0x4b

    const/16 v1, 0x53

    aput-char v1, v2, v0

    .line 145
    const/16 v0, 0x4c

    const/16 v1, 0x34

    aput-char v1, v2, v0

    .line 146
    const/16 v0, 0x4d

    const/16 v1, 0x4c

    aput-char v1, v2, v0

    .line 147
    const/16 v0, 0x4e

    const/16 v1, 0x73

    aput-char v1, v2, v0

    .line 148
    const/16 v0, 0x4f

    const/16 v1, 0x44

    aput-char v1, v2, v0

    .line 149
    const/16 v0, 0x50

    const/16 v1, 0x63

    aput-char v1, v2, v0

    .line 150
    const/16 v0, 0x51

    const/16 v1, 0x42

    aput-char v1, v2, v0

    .line 151
    const/16 v0, 0x52

    const/16 v1, 0x79

    aput-char v1, v2, v0

    .line 152
    const/16 v0, 0x53

    aput-char v7, v2, v0

    .line 153
    const/16 v0, 0x54

    const/16 v1, 0x4f

    aput-char v1, v2, v0

    .line 154
    const/16 v0, 0x55

    const/16 v1, 0x71

    aput-char v1, v2, v0

    .line 155
    const/16 v0, 0x56

    const/16 v1, 0x6b

    aput-char v1, v2, v0

    .line 156
    const/16 v0, 0x57

    const/16 v1, 0x4e

    aput-char v1, v2, v0

    .line 157
    const/16 v0, 0x58

    const/16 v1, 0x36

    aput-char v1, v2, v0

    .line 158
    const/16 v0, 0x59

    const/16 v1, 0x46

    aput-char v1, v2, v0

    .line 159
    const/16 v0, 0x5a

    const/16 v1, 0x75

    aput-char v1, v2, v0

    .line 160
    const/16 v0, 0x5f

    const/16 v1, 0x67

    aput-char v1, v2, v0

    .line 161
    const/16 v0, 0x2e

    aput-char v4, v2, v0

    .line 162
    const/16 v0, 0x27

    const/16 v1, 0x3d

    aput-char v1, v2, v0

    .line 163
    const/16 v0, 0x7e

    const/16 v1, 0x2b

    aput-char v1, v2, v0

    .line 164
    const/16 v0, 0x2f

    aput-char v0, v2, v4

    .line 166
    const/16 v0, 0x77

    aput-char v0, v3, v5

    .line 167
    const/16 v0, 0x35

    aput-char v0, v3, v6

    .line 168
    const/16 v0, 0x72

    aput-char v0, v3, v7

    .line 169
    const/16 v0, 0x68

    aput-char v0, v3, v8

    .line 170
    const/16 v0, 0x34

    const/16 v1, 0x41

    aput-char v1, v3, v0

    .line 171
    const/16 v0, 0x35

    const/16 v1, 0x63

    aput-char v1, v3, v0

    .line 172
    const/16 v0, 0x36

    const/16 v1, 0x4c

    aput-char v1, v3, v0

    .line 173
    const/16 v0, 0x37

    const/16 v1, 0x6e

    aput-char v1, v3, v0

    .line 174
    const/16 v0, 0x38

    const/16 v1, 0x51

    aput-char v1, v3, v0

    .line 175
    const/16 v0, 0x39

    const/16 v1, 0x66

    aput-char v1, v3, v0

    .line 176
    const/16 v0, 0x61

    const/16 v1, 0x71

    aput-char v1, v3, v0

    .line 177
    const/16 v0, 0x62

    const/16 v1, 0x49

    aput-char v1, v3, v0

    .line 178
    const/16 v0, 0x63

    const/16 v1, 0x6b

    aput-char v1, v3, v0

    .line 179
    const/16 v0, 0x64

    const/16 v1, 0x45

    aput-char v1, v3, v0

    .line 180
    const/16 v0, 0x65

    aput-char v7, v3, v0

    .line 181
    const/16 v0, 0x66

    const/16 v1, 0x7a

    aput-char v1, v3, v0

    .line 182
    const/16 v0, 0x67

    const/16 v1, 0x56

    aput-char v1, v3, v0

    .line 183
    const/16 v0, 0x68

    const/16 v1, 0x65

    aput-char v1, v3, v0

    .line 184
    const/16 v0, 0x69

    const/16 v1, 0x4f

    aput-char v1, v3, v0

    .line 185
    const/16 v0, 0x6a

    const/16 v1, 0x61

    aput-char v1, v3, v0

    .line 186
    const/16 v0, 0x6b

    const/16 v1, 0x48

    aput-char v1, v3, v0

    .line 187
    const/16 v0, 0x6c

    const/16 v1, 0x6c

    aput-char v1, v3, v0

    .line 188
    const/16 v0, 0x6d

    const/16 v1, 0x43

    aput-char v1, v3, v0

    .line 189
    const/16 v0, 0x6e

    aput-char v8, v3, v0

    .line 190
    const/16 v0, 0x6f

    const/16 v1, 0x58

    aput-char v1, v3, v0

    .line 191
    const/16 v0, 0x70

    const/16 v1, 0x46

    aput-char v1, v3, v0

    .line 192
    const/16 v0, 0x71

    const/16 v1, 0x6f

    aput-char v1, v3, v0

    .line 193
    const/16 v0, 0x72

    const/16 v1, 0x53

    aput-char v1, v3, v0

    .line 194
    const/16 v0, 0x73

    const/16 v1, 0x38

    aput-char v1, v3, v0

    .line 195
    const/16 v0, 0x74

    const/16 v1, 0x57

    aput-char v1, v3, v0

    .line 196
    const/16 v0, 0x75

    const/16 v1, 0x6a

    aput-char v1, v3, v0

    .line 197
    const/16 v0, 0x76

    const/16 v1, 0x79

    aput-char v1, v3, v0

    .line 198
    const/16 v0, 0x77

    const/16 v1, 0x73

    aput-char v1, v3, v0

    .line 199
    const/16 v0, 0x78

    aput-char v6, v3, v0

    .line 200
    const/16 v0, 0x79

    const/16 v1, 0x4a

    aput-char v1, v3, v0

    .line 201
    const/16 v0, 0x7a

    const/16 v1, 0x59

    aput-char v1, v3, v0

    .line 202
    const/16 v0, 0x41

    const/16 v1, 0x67

    aput-char v1, v3, v0

    .line 203
    const/16 v0, 0x42

    const/16 v1, 0x5a

    aput-char v1, v3, v0

    .line 204
    const/16 v0, 0x43

    const/16 v1, 0x52

    aput-char v1, v3, v0

    .line 205
    const/16 v0, 0x44

    const/16 v1, 0x37

    aput-char v1, v3, v0

    .line 206
    const/16 v0, 0x45

    const/16 v1, 0x4e

    aput-char v1, v3, v0

    .line 207
    const/16 v0, 0x46

    const/16 v1, 0x76

    aput-char v1, v3, v0

    .line 208
    const/16 v0, 0x47

    const/16 v1, 0x50

    aput-char v1, v3, v0

    .line 209
    const/16 v0, 0x48

    const/16 v1, 0x39

    aput-char v1, v3, v0

    .line 210
    const/16 v0, 0x49

    const/16 v1, 0x5f

    aput-char v1, v3, v0

    .line 211
    const/16 v0, 0x4a

    const/16 v1, 0x74

    aput-char v1, v3, v0

    .line 212
    const/16 v0, 0x4b

    const/16 v1, 0x44

    aput-char v1, v3, v0

    .line 213
    const/16 v0, 0x4c

    const/16 v1, 0x34

    aput-char v1, v3, v0

    .line 214
    const/16 v0, 0x4d

    const/16 v1, 0x4b

    aput-char v1, v3, v0

    .line 215
    const/16 v0, 0x4e

    const/16 v1, 0x75

    aput-char v1, v3, v0

    .line 216
    const/16 v0, 0x4f

    const/16 v1, 0x62

    aput-char v1, v3, v0

    .line 217
    const/16 v0, 0x50

    const/16 v1, 0x42

    aput-char v1, v3, v0

    .line 218
    const/16 v0, 0x51

    const/16 v1, 0x6d

    aput-char v1, v3, v0

    .line 219
    const/16 v0, 0x52

    const/16 v1, 0x54

    aput-char v1, v3, v0

    .line 220
    const/16 v0, 0x53

    const/16 v1, 0x36

    aput-char v1, v3, v0

    .line 221
    const/16 v0, 0x54

    const/16 v1, 0x47

    aput-char v1, v3, v0

    .line 222
    const/16 v0, 0x55

    const/16 v1, 0x69

    aput-char v1, v3, v0

    .line 223
    const/16 v0, 0x56

    const/16 v1, 0x55

    aput-char v1, v3, v0

    .line 224
    const/16 v0, 0x57

    const/16 v1, 0x64

    aput-char v1, v3, v0

    .line 225
    const/16 v0, 0x58

    const/16 v1, 0x4d

    aput-char v1, v3, v0

    .line 226
    const/16 v0, 0x59

    const/16 v1, 0x70

    aput-char v1, v3, v0

    .line 227
    const/16 v0, 0x5a

    const/16 v1, 0x78

    aput-char v1, v3, v0

    .line 228
    const/16 v0, 0x5f

    aput-char v5, v3, v0

    .line 229
    const/16 v0, 0x7e

    aput-char v4, v3, v0

    .line 230
    const/16 v0, 0x3d

    aput-char v0, v3, v4

    .line 231
    const/16 v0, 0x2e

    const/16 v1, 0x2b

    aput-char v1, v3, v0

    .line 232
    const/16 v0, 0x27

    const/16 v1, 0x2f

    aput-char v1, v3, v0

    .line 234
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 235
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 236
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 237
    const/4 v1, 0x0

    .line 238
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v5, :cond_3

    .line 239
    rem-int/lit8 v7, v1, 0x2

    if-nez v7, :cond_2

    .line 240
    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aget-char v7, v2, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 245
    :goto_3
    add-int/lit8 v1, v1, 0x1

    .line 238
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 242
    :cond_2
    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v7

    aget-char v7, v3, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 247
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const-string v0, ""

    goto/16 :goto_0
.end method


# virtual methods
.method public llIIIIlllllIIllIIllI([Landroid/os/Parcelable;LllllIIlIlIIIIllIlIIl;)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 18
    if-eqz p1, :cond_3

    .line 19
    array-length v0, p1

    new-array v5, v0, [Landroid/nfc/NdefMessage;

    move v1, v2

    .line 20
    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_3

    .line 21
    aget-object v0, p1, v1

    check-cast v0, Landroid/nfc/NdefMessage;

    aput-object v0, v5, v1

    .line 22
    aget-object v0, v5, v1

    invoke-virtual {v0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v6

    move v0, v2

    .line 23
    :goto_1
    array-length v3, v6

    if-ge v0, v3, :cond_0

    .line 24
    aget-object v3, v6, v1

    invoke-virtual {v3}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v3

    .line 26
    :try_start_0
    new-instance v4, Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-direct {v4, v3, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 27
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NFC tagging: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 28
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 29
    const-string v3, "v1"

    invoke-virtual {v7, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, LIlIllIIIIllllllIlIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 30
    const-string v3, "v2"

    invoke-virtual {v7, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, LIlIllIIIIllllllIlIIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 31
    const-string v8, "v3"

    invoke-virtual {v7, v8}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 32
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 33
    sget-object v3, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v4, "SSID is empty"

    invoke-virtual {v3, v4}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 20
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 37
    :cond_1
    const-string v8, "2"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 38
    invoke-direct {p0, v4}, LIlIllIIIIllllllIlIIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 39
    invoke-direct {p0, v3}, LIlIllIIIIllllllIlIIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 41
    :cond_2
    invoke-virtual {p2, v4, v3}, LllllIIlIlIIIIllIlIIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    .line 42
    const/4 v2, 0x1

    .line 54
    :cond_3
    return v2

    .line 43
    :catch_0
    move-exception v3

    .line 44
    invoke-static {v3}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 23
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 45
    :catch_1
    move-exception v3

    .line 46
    invoke-static {v3}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 47
    :catch_2
    move-exception v3

    .line 48
    invoke-static {v3}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_2
.end method
