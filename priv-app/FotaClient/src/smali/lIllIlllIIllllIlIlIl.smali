.class public abstract LlIllIlllIIllllIlIlIl;
.super Landroid/content/ContentProvider;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field protected llIIIIlllllIIllIIllI:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private IIIIllIlIIlIIIIlllIl()Ljava/util/List;
    .locals 4

    .prologue
    .line 64
    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "log"

    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LIlIlllIIlIlIIIlIlIll;->llIIIIlllllIIllIIllI(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, LllIllIIllIIlIIlIIIII;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private llIIIIlllllIIllIIllI(Ljava/util/List;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 71
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 72
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 74
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 77
    :goto_1
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "content"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->lllIlIlIIIllIIlIllIl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 82
    :cond_0
    return-object v1

    .line 75
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private llllllIllIlIlllIIlIl()V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->IllIlIIIIlIIlIIIllIl()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    return-void

    .line 97
    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Permission Denial"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method protected abstract IllIlIIIIlIIlIIIllIl()Z
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 102
    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->llIIllllIIlllIIIIlll()V

    .line 104
    const-string v1, "get"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "agreed"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 106
    const-string v1, "result"

    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->IllIlIIIIlIIlIIIllIl()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 124
    :goto_0
    return-object v0

    .line 110
    :cond_0
    const-string v1, "set"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "registered"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    if-eqz p3, :cond_1

    const-string v1, "value"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_1
    invoke-virtual {p0, v0}, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI(Z)V

    .line 112
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_0

    .line 114
    :cond_2
    const-string v0, "get"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "registered"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 115
    invoke-direct {p0}, LlIllIlllIIllllIlIlIl;->llllllIllIlIlllIIlIl()V

    .line 116
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 117
    const-string v1, "result"

    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->llIlIIIIlIIIIIlIlIII()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 121
    :cond_3
    const-string v0, "get"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 122
    iget-object v0, p0, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI:Landroid/os/Bundle;

    invoke-virtual {v0, p2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 124
    :cond_4
    invoke-super {p0, p1, p2, p3}, Landroid/content/ContentProvider;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation not supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->llIIllllIIlllIIIIlll()V

    .line 152
    const-string v0, "text/plain"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 157
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation not supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 2

    .prologue
    .line 141
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method protected abstract llIIIIlllllIIllIIllI()Ljava/util/List;
.end method

.method protected abstract llIIIIlllllIIllIIllI(Z)V
.end method

.method protected llIIllllIIlllIIIIlll()V
    .locals 2

    .prologue
    .line 86
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 88
    :cond_0
    return-void

    .line 90
    :cond_1
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Permission Denial"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract llIlIIIIlIIIIIlIlIII()Z
.end method

.method protected abstract lllIlIlIIIllIIlIllIl()Ljava/lang/String;
.end method

.method protected abstract llllIIIllIlIIIIllllI()Ljava/util/List;
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 49
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI:Landroid/os/Bundle;

    .line 51
    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI()Ljava/util/List;

    move-result-object v0

    .line 52
    invoke-direct {p0, v0}, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI(Ljava/util/List;)Landroid/os/Bundle;

    move-result-object v0

    .line 53
    invoke-direct {p0}, LlIllIlllIIllllIlIlIl;->IIIIllIlIIlIIIIlllIl()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI(Ljava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 54
    iget-object v1, p0, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI:Landroid/os/Bundle;

    const-string v2, "logList"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->llllIIIllIlIIIIllllI()Ljava/util/List;

    move-result-object v0

    .line 57
    invoke-direct {p0, v0}, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI(Ljava/util/List;)Landroid/os/Bundle;

    move-result-object v0

    .line 58
    iget-object v1, p0, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI:Landroid/os/Bundle;

    const-string v2, "plainLogList"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    .line 129
    invoke-virtual {p0}, LlIllIlllIIllllIlIlIl;->llIIllllIIlllIIIIlll()V

    .line 130
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 131
    iget-object v1, p0, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI:Landroid/os/Bundle;

    const-string v2, "logList"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI:Landroid/os/Bundle;

    const-string v2, "plainLogList"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    .line 132
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Data is corrupted"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_1
    iget-object v1, p0, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI:Landroid/os/Bundle;

    const-string v2, "logList"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI:Landroid/os/Bundle;

    const-string v2, "plainLogList"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 135
    :cond_2
    invoke-virtual {p0, v0}, LlIllIlllIIllllIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0

    .line 136
    :cond_3
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 162
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation not supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 167
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Operation not supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
