.class public final enum Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;
.super Ljava/lang/Enum;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static final synthetic IllIlIIIIlIIlIIIllIl:[Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

.field public static final enum llIIIIlllllIIllIIllI:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

.field public static final enum llIlIIIIlIIIIIlIlIII:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

.field public static final enum lllIlIlIIIllIIlIllIl:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

.field public static final enum llllIIIllIlIIIIllllI:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    const-string v1, "POST"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->llIIIIlllllIIllIIllI:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    new-instance v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    const-string v1, "GET"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->llllIIIllIlIIIIllllI:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    new-instance v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    new-instance v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->llIlIIIIlIIIIIlIlIII:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    .line 56
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    sget-object v1, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->llIIIIlllllIIllIIllI:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->llllIIIllIlIIIIllllI:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->lllIlIlIIIllIIlIllIl:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->llIlIIIIlIIIIIlIlIII:Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->IllIlIIIIlIIlIIIllIl:[Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->IllIlIIIIlIIlIIIllIl:[Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    invoke-virtual {v0}, [Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/fota/osp/http/RestClient$HttpMethod;

    return-object v0
.end method
