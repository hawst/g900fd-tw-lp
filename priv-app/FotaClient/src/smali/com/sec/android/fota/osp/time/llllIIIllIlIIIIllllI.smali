.class public Lcom/sec/android/fota/osp/time/llllIIIllIlIIIIllllI;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;)J
    .locals 5

    .prologue
    .line 25
    const-wide/16 v0, 0x0

    .line 26
    if-nez p0, :cond_1

    .line 40
    :cond_0
    :goto_0
    return-wide v0

    .line 29
    :cond_1
    const-string v2, "FOTA_SERVER_TIME"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 31
    if-eqz v2, :cond_0

    .line 33
    :try_start_0
    const-string v3, "ServerTime.SettingTime"

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 35
    :catch_0
    move-exception v2

    .line 36
    invoke-static {v2}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;J)Z
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 72
    .line 74
    if-nez p0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    const-string v1, "FOTA_SERVER_TIME"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 80
    cmp-long v4, p1, v2

    if-eqz v4, :cond_2

    .line 81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v4, v2, v4

    .line 82
    sub-long v2, p1, v4

    .line 87
    :goto_1
    if-eqz v1, :cond_0

    .line 88
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 89
    const-string v1, "ServerTime.SettingTime"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 90
    const-string v1, "ServerTime.OffsetTime"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 91
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    goto :goto_0

    :cond_2
    move-wide v4, v2

    .line 85
    goto :goto_1
.end method

.method public static lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 101
    if-nez p0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    const-string v1, "FOTA_SERVER_TIME"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 106
    if-eqz v1, :cond_0

    .line 107
    const-string v1, "FOTA_SERVER_TIME"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 109
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 110
    const-string v2, "ServerTime.SettingTime"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 111
    const-string v2, "ServerTime.OffsetTime"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 112
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Landroid/content/Context;)J
    .locals 5

    .prologue
    .line 50
    const-wide/16 v0, 0x0

    .line 51
    if-nez p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-wide v0

    .line 54
    :cond_1
    const-string v2, "FOTA_SERVER_TIME"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 56
    if-eqz v2, :cond_0

    .line 58
    :try_start_0
    const-string v3, "ServerTime.OffsetTime"

    const-string v4, "0"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v2

    .line 60
    invoke-static {v2}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
