.class public Lcom/sec/android/fotaclient/FotaInitUpdateService;
.super Landroid/app/Service;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field private volatile llIIIIlllllIIllIIllI:Landroid/os/Looper;

.field private llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

.field private lllIlIlIIIllIIlIllIl:I

.field private volatile llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl:I

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    .line 93
    return-void
.end method

.method static synthetic IllIlIIIIlIIlIIIllIl(Lcom/sec/android/fotaclient/FotaInitUpdateService;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIlIIIIlIIIIIlIlIII()V

    return-void
.end method

.method private llIIIIlllllIIllIIllI()V
    .locals 4

    .prologue
    .line 247
    const-string v0, "request Pull"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "init Type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 249
    new-instance v1, Landroid/content/Intent;

    const-string v0, "sec.fota.pull.intent.RECEIVE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 250
    const-string v2, "NFC_MODE"

    iget v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl:I

    const/4 v3, 0x4

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 251
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 252
    invoke-virtual {p0, v1}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 253
    return-void

    .line 250
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private llIIIIlllllIIllIIllI(I)V
    .locals 2

    .prologue
    .line 217
    invoke-static {p0}, LllIIllllIIlllIIIIlll;->IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, LllIIIIlllllIIllllllI;->lllIlIlIIIllIIlIllIl:Z

    if-eqz v0, :cond_1

    .line 218
    iget v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 219
    :cond_0
    const-string v0, "Push & Polling update does not allow on Roaming"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    const/16 v1, 0xc8

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    .line 237
    :goto_0
    return-void

    .line 225
    :cond_1
    iget v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 228
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    const/16 v1, 0xa

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    goto :goto_0

    .line 231
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    const/16 v1, 0xb

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    goto :goto_0

    .line 234
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    const/16 v1, 0xc

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    goto :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI()V

    return-void
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI(I)V

    return-void
.end method

.method private llIlIIIIlIIIIIlIlIII()V
    .locals 2

    .prologue
    .line 369
    const-string v0, "update Last Checked Date"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 370
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, LIIIllIIllIllIlIIlIIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 372
    new-instance v0, Landroid/content/Intent;

    const-string v1, "sec.fota.intent.CHECKED_DATE_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 373
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 374
    invoke-virtual {p0, v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 375
    return-void
.end method

.method static synthetic llIlIIIIlIIIIIlIlIII(Lcom/sec/android/fotaclient/FotaInitUpdateService;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl()V

    return-void
.end method

.method private lllIlIlIIIllIIlIllIl()V
    .locals 2

    .prologue
    .line 359
    const-string v0, "request Polling"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 360
    new-instance v0, Landroid/content/Intent;

    const-string v1, "sec.fota.polling.intent.RECEIVE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 361
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 362
    invoke-virtual {p0, v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    .line 363
    return-void
.end method

.method static synthetic lllIlIlIIIllIIlIllIl(Lcom/sec/android/fotaclient/FotaInitUpdateService;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI()V

    return-void
.end method

.method static synthetic llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;)Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    return-object v0
.end method

.method private llllIIIllIlIIIIllllI()V
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x22

    if-ge v0, v1, :cond_0

    .line 264
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Push Message format is wrong!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 273
    :goto_0
    return-void

    .line 268
    :cond_0
    const-string v0, "request Push"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 269
    new-instance v0, Landroid/content/Intent;

    const-string v1, "sec.fota.push.intent.RECEIVE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 270
    const-string v1, "pdus"

    iget-object v2, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    invoke-static {v2}, LlIIllIIIIIlllIIlllIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 271
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 272
    invoke-virtual {p0, v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private llllIIIllIlIIIIllllI(I)V
    .locals 3

    .prologue
    .line 285
    new-instance v0, LIlIIIlIIllllIIIIIIII;

    invoke-direct {v0}, LIlIIIlIIllllIIIIIIII;-><init>()V

    .line 286
    sget-object v1, LllIIIIlllllIIllllllI;->llllIIIllIlIIIIllllI:Ljava/lang/String;

    invoke-virtual {v0, v1}, LIlIIIlIIllllIIIIIIII;->IllIlIIIIlIIlIIIllIl(Ljava/lang/String;)V

    .line 287
    invoke-static {}, LlllIlIlIIIllIIlIllIl;->IllIlIIIIlIIlIIIllIl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIlIIIlIIllllIIIIIIII;->IIIIllIlIIlIIIIlllIl(Ljava/lang/String;)V

    .line 289
    invoke-static {p0}, LlIlllIlIIIlIlIlllIll;->llIIIIlllllIIllIIllI(Landroid/content/Context;)LllIIllIllIIIIllIIIlI;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_0

    .line 291
    const-string v0, "Network is disconnected"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    const/16 v2, 0x23

    invoke-virtual {v0, p1, v2, v1}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(IILjava/lang/Object;)V

    .line 353
    :goto_0
    return-void

    .line 296
    :cond_0
    new-instance v1, LIllIIIIIIlIIlIIIlIII;

    invoke-static {p0}, LlllllllllllIlllllIIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;)LIIllIlIIlIlllIIllIII;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, LIllIIIIIIlIIlIIIlIII;-><init>(Landroid/content/Context;LIIllIlIIlIlllIIllIII;LIlIIIlIIllllIIIIIIII;)V

    .line 298
    new-instance v0, Lcom/sec/android/fotaclient/llIIIIlllllIIllIIllI;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/fotaclient/llIIIIlllllIIllIIllI;-><init>(Lcom/sec/android/fotaclient/FotaInitUpdateService;I)V

    invoke-virtual {v1, v0}, LIllIIIIIIlIIlIIIlIII;->llIIIIlllllIIllIIllI(LlIIIllIIIlIlIIIlllII;)V

    .line 348
    new-instance v0, LlIlIIlIllllIIIIIIlIl;

    invoke-direct {v0}, LlIlIIlIllllIIIIIIlIl;-><init>()V

    .line 349
    invoke-virtual {v0, p0, v1}, LlIlIIlIllllIIIIIIlIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;LlIllIIllIlIlIIIlIIII;)V

    .line 350
    invoke-static {p0}, LlIlIlIIIIlIIlIlIIlII;->IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p0, v0}, LlIlIlIIIIlIIlIlIIlII;->llllIIIllIlIIIIllllI(Landroid/content/Context;I)Z

    .line 352
    const-string v0, "Request NetActionGetPolling"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI(I)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 171
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Service[FotaInitUpdateService]"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 174
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI:Landroid/os/Looper;

    .line 175
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 176
    new-instance v0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    iget-object v1, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;-><init>(Lcom/sec/android/fotaclient/FotaInitUpdateService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    .line 180
    :goto_0
    return-void

    .line 178
    :cond_0
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Thread Looper is null"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 210
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 211
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 185
    if-nez p1, :cond_0

    .line 186
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Intent is null"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->stopSelf()V

    .line 202
    :goto_0
    return v2

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    if-nez v0, :cond_1

    .line 192
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Handler is null"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_1
    const-string v0, "updateType"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl:I

    .line 197
    iget v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl:I

    if-ne v0, v2, :cond_2

    .line 198
    const-string v0, "msg"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIlIIIIlIIIIIlIlIII:Ljava/lang/String;

    .line 201
    :cond_2
    iget-object v0, p0, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI:Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    invoke-virtual {v0, p3, v1}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    goto :goto_0
.end method
