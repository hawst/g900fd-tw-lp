.class Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;
.super Landroid/os/Handler;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field final synthetic llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;


# direct methods
.method public constructor <init>(Lcom/sec/android/fotaclient/FotaRegisterService;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    .line 133
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 134
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/16 v2, 0xc8

    .line 139
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 197
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 198
    return-void

    .line 141
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Register State: Check condition to decide next state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/sec/android/fotaclient/FotaRegisterService;->llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaRegisterService;I)V

    goto :goto_0

    .line 146
    :sswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Register State: Registering device: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/sec/android/fotaclient/FotaRegisterService;->llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaRegisterService;I)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaRegisterService;->llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaRegisterService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    invoke-static {}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI()Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(I)V

    goto :goto_0

    .line 155
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Register State: Fail to register device: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 156
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI(II)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaRegisterService;->llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaRegisterService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-static {}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI()Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    move-result-object v1

    const/16 v2, 0xa

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LllIIllIllIIIIllIIIlI;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 164
    :sswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Register State: Success to register device: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/fotaclient/FotaRegisterService;->llIIIIlllllIIllIIllI(I)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaRegisterService;->llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaRegisterService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    invoke-static {}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI()Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;->llIIIIlllllIIllIIllI(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    invoke-virtual {v0}, Lcom/sec/android/fotaclient/FotaRegisterService;->llllIIIllIlIIIIllllI()V

    goto/16 :goto_0

    .line 174
    :sswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Register State: Registering polling: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/fotaclient/FotaRegisterService;->llllIIIllIlIIIIllllI(I)V

    goto/16 :goto_0

    .line 179
    :sswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Register State: Registering push: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/sec/android/fotaclient/FotaRegisterService;->lllIlIlIIIllIIlIllIl(Lcom/sec/android/fotaclient/FotaRegisterService;I)V

    goto/16 :goto_0

    .line 184
    :sswitch_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Register State: Fail to register push: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 185
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI(II)V

    goto/16 :goto_0

    .line 189
    :sswitch_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Register State: Finish registration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaRegisterService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/fotaclient/FotaRegisterService;->stopSelf(I)V

    goto/16 :goto_0

    .line 139
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x14 -> :sswitch_1
        0x19 -> :sswitch_2
        0x1e -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
        0x37 -> :sswitch_6
        0xc8 -> :sswitch_7
    .end sparse-switch
.end method

.method public llIIIIlllllIIllIIllI(II)V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->llIIIIlllllIIllIIllI(IILjava/lang/Object;)V

    .line 202
    return-void
.end method

.method public llIIIIlllllIIllIIllI(IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 205
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 206
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 207
    iput p2, v0, Landroid/os/Message;->what:I

    .line 208
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 209
    invoke-virtual {p0, v0}, Lcom/sec/android/fotaclient/llllllIllIlIlllIIlIl;->sendMessage(Landroid/os/Message;)Z

    .line 210
    return-void
.end method
