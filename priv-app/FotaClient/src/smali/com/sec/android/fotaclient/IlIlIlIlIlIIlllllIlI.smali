.class Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;
.super Landroid/os/Handler;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field final synthetic llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaReportHeartBeatService;


# direct methods
.method public constructor <init>(Lcom/sec/android/fotaclient/FotaReportHeartBeatService;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaReportHeartBeatService;

    .line 56
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 57
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 61
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 90
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 91
    return-void

    .line 63
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HeartBeat State: Check condition to decide next state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaReportHeartBeatService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/sec/android/fotaclient/FotaReportHeartBeatService;->llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaReportHeartBeatService;I)V

    goto :goto_0

    .line 68
    :sswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HeartBeat State: Report push HeartBeat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaReportHeartBeatService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/sec/android/fotaclient/FotaReportHeartBeatService;->llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaReportHeartBeatService;I)V

    goto :goto_0

    .line 73
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HeartBeat State: Report polling HeartBeat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaReportHeartBeatService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/sec/android/fotaclient/FotaReportHeartBeatService;->lllIlIlIIIllIIlIllIl(Lcom/sec/android/fotaclient/FotaReportHeartBeatService;I)V

    goto :goto_0

    .line 78
    :sswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HeartBeat State: Fail to report HeartBeat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaReportHeartBeatService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaReportHeartBeatService;->llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaReportHeartBeatService;)Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI(II)V

    goto/16 :goto_0

    .line 83
    :sswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HeartBeat State: Finish HeartBeat: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaReportHeartBeatService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/fotaclient/FotaReportHeartBeatService;->stopSelf(I)V

    goto/16 :goto_0

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x14 -> :sswitch_2
        0x19 -> :sswitch_3
        0xc8 -> :sswitch_4
    .end sparse-switch
.end method

.method public llIIIIlllllIIllIIllI(II)V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->llIIIIlllllIIllIIllI(IILjava/lang/Object;)V

    .line 95
    return-void
.end method

.method public llIIIIlllllIIllIIllI(IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 99
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 100
    iput p2, v0, Landroid/os/Message;->what:I

    .line 101
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 102
    invoke-virtual {p0, v0}, Lcom/sec/android/fotaclient/IlIlIlIlIlIIlllllIlI;->sendMessage(Landroid/os/Message;)Z

    .line 103
    return-void
.end method
