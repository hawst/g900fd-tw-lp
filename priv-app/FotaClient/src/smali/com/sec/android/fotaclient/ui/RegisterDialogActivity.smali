.class public Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;
.super Landroid/app/Activity;
.source "llIIIIlllllIIllIIllI"


# static fields
.field private static llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;


# instance fields
.field private llIlIIIIlIIIIIlIlIII:Landroid/widget/Toast;

.field private lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

.field private llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    .line 40
    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    .line 42
    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIlIIIIlIIIIIlIlIII:Landroid/widget/Toast;

    .line 214
    return-void
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    return-object p1
.end method

.method public static llIIIIlllllIIllIIllI()Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    return-object v0
.end method

.method private llIIIIlllllIIllIIllI(I)V
    .locals 2

    .prologue
    .line 155
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    const v1, 0x7f06002f

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {p0, p1}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/android/fotaclient/ui/llIIlIlIlIlIIIIIIlIl;

    invoke-direct {v1, p0}, Lcom/sec/android/fotaclient/ui/llIIlIlIlIlIIIIIIlIl;-><init>(Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 169
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->finish()V

    goto :goto_0
.end method

.method private llllIIIllIlIIIIllllI()V
    .locals 1

    .prologue
    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 184
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private llllIIIllIlIIIIllllI(I)V
    .locals 3

    .prologue
    .line 196
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "fota_register_dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 198
    if-eqz v1, :cond_0

    .line 199
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 201
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 203
    invoke-static {p1}, Lcom/sec/android/fotaclient/ui/llIllIIllIIlIIlIIIII;->llIIIIlllllIIllIIllI(I)Lcom/sec/android/fotaclient/ui/llIllIIllIIlIIlIIIII;

    move-result-object v1

    .line 204
    const-string v2, "fota_register_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_0
    return-void

    .line 205
    :catch_0
    move-exception v0

    .line 206
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public llIIIIlllllIIllIIllI(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIlIIIIlIIIIIlIlIII:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 306
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIlIIIIlIIIIIlIlIII:Landroid/widget/Toast;

    .line 310
    :goto_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIlIIIIlIIIIIlIlIII:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 311
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIlIIIIlIIIIIlIlIII:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method public llIIIIlllllIIllIIllI(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 73
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p0, v0}, LIIIIllIlIIlIIIIlllIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->finish()V

    .line 117
    :goto_0
    return-void

    .line 79
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 81
    :sswitch_0
    const-string v0, "UI State: registering device progress dialog"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 82
    const v0, 0x7f06002a

    invoke-direct {p0, v0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI(I)V

    goto :goto_0

    .line 85
    :sswitch_1
    const-string v0, "UI State: registering fail alert dialog"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI()V

    .line 87
    const v1, 0x7f060025

    .line 88
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LllIIllIllIIIIllIIIlI;

    .line 90
    invoke-virtual {v0}, LllIIllIllIIIIllIIIlI;->llIIIIlllllIIllIIllI()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    :cond_1
    move v0, v1

    .line 108
    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI(I)V

    goto :goto_0

    .line 95
    :sswitch_2
    const v0, 0x7f060024

    .line 96
    goto :goto_1

    .line 98
    :sswitch_3
    const v0, 0x7f06001c

    .line 99
    goto :goto_1

    .line 101
    :sswitch_4
    const v0, 0x7f06002b

    .line 102
    goto :goto_1

    .line 104
    :sswitch_5
    const v0, 0x7f06002d

    goto :goto_1

    .line 111
    :sswitch_6
    const-string v0, "UI State: registering success toast"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 112
    invoke-direct {p0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI()V

    .line 113
    const v0, 0x7f060029

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->finish()V

    goto :goto_0

    .line 79
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x14 -> :sswitch_6
    .end sparse-switch

    .line 90
    :sswitch_data_1
    .sparse-switch
        0x64 -> :sswitch_2
        0x190 -> :sswitch_3
        0x19a -> :sswitch_5
        0x1a4 -> :sswitch_4
        0x1ae -> :sswitch_2
        0x1b8 -> :sswitch_2
        0x1f4 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 122
    new-instance v0, Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    invoke-direct {v0, p0}, Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;-><init>(Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;)V

    sput-object v0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    .line 123
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :cond_1
    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    .line 138
    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    .line 139
    sput-object v1, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    .line 141
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 142
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    :try_start_1
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    .line 138
    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    .line 139
    sput-object v1, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    goto :goto_0

    .line 137
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    .line 138
    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/app/AlertDialog;

    .line 139
    sput-object v1, Lcom/sec/android/fotaclient/ui/RegisterDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllIIIIllIlIlllllllI;

    throw v0
.end method
