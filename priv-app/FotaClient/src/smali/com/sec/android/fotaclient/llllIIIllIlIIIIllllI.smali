.class Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;
.super Landroid/os/Handler;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field final synthetic llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;


# direct methods
.method public constructor <init>(Lcom/sec/android/fotaclient/FotaInitUpdateService;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    .line 96
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 97
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    const/16 v2, 0xc8

    .line 102
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 145
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 146
    return-void

    .line 104
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update State: Check condition to decide next state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;I)V

    goto :goto_0

    .line 109
    :sswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update State: Initialize pull update: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;)Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    goto :goto_0

    .line 115
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update State: Initialize push update: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->lllIlIlIIIllIIlIllIl(Lcom/sec/android/fotaclient/FotaInitUpdateService;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;)Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    goto :goto_0

    .line 121
    :sswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update State: Initialize polling update: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;I)V

    goto/16 :goto_0

    .line 126
    :sswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update State: Fail to get polling info: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;)Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    goto/16 :goto_0

    .line 131
    :sswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update State: Need to polling update: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llIlIIIIlIIIIIlIlIII(Lcom/sec/android/fotaclient/FotaInitUpdateService;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-static {v0}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/FotaInitUpdateService;)Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(II)V

    goto/16 :goto_0

    .line 137
    :sswitch_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update State: Finish update init: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/FotaInitUpdateService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/fotaclient/FotaInitUpdateService;->stopSelf(I)V

    goto/16 :goto_0

    .line 102
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0xb -> :sswitch_2
        0xc -> :sswitch_3
        0x23 -> :sswitch_4
        0x28 -> :sswitch_5
        0xc8 -> :sswitch_6
    .end sparse-switch
.end method

.method public llIIIIlllllIIllIIllI(II)V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->llIIIIlllllIIllIIllI(IILjava/lang/Object;)V

    .line 150
    return-void
.end method

.method public llIIIIlllllIIllIIllI(IILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 153
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 154
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 155
    iput p2, v0, Landroid/os/Message;->what:I

    .line 156
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 157
    invoke-virtual {p0, v0}, Lcom/sec/android/fotaclient/llllIIIllIlIIIIllllI;->sendMessage(Landroid/os/Message;)Z

    .line 158
    return-void
.end method
