.class public Lcom/sec/android/fotaclient/FotaUpdaterReceiver;
.super Landroid/content/BroadcastReceiver;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field private llIIIIlllllIIllIIllI:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 50
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI:I

    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 179
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/fotaclient/FotaReportHeartBeatService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 180
    const-string v1, "heartbeatType"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 181
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 182
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 183
    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 169
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 170
    const-string v1, "updateType"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 171
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 172
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 173
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/16 v4, 0x14

    .line 55
    if-nez p1, :cond_1

    .line 56
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Content is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    if-nez p2, :cond_2

    .line 61
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Intent is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 65
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 67
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Action is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_3
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receive broadcast meassage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 73
    invoke-static {}, LIIIIllIlIIlIIIIlllIl;->llIIIIlllllIIllIIllI()Z

    move-result v0

    if-nez v0, :cond_4

    .line 74
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "No action is available when User is not owner"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 75
    const-string v0, "com.sec.android.fotaclient"

    invoke-static {p1, v0}, LIIIIllIlIIlIIIIlllIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :cond_4
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)I

    move-result v0

    if-eq v0, v5, :cond_5

    .line 80
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "No action is available before device registered"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_5
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 85
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 86
    if-eqz v0, :cond_7

    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 88
    iput v4, p0, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI:I

    .line 95
    :cond_6
    :goto_1
    const-string v0, "sec.fota.action.SOFTWARE_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 96
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Press update button. Start user init"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 97
    invoke-direct {p0, p1, v5}, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 90
    :cond_7
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI:I

    goto :goto_1

    .line 102
    :cond_8
    invoke-static {p1}, LIIIllIIllIllIlIIlIIl;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 103
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Auto update settings is activated"

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 105
    invoke-static {p1}, LlllllllllllIlllllIIl;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 106
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 107
    invoke-static {p1}, LlllllllllllIlllllIIl;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)V

    .line 125
    :cond_9
    :goto_2
    invoke-static {p1}, LllIlIIIIlllIlllllIll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 126
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Heartbeat settings is activated."

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 128
    invoke-static {p1}, LllIlIIIIlllIlllllIll;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 129
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 130
    invoke-static {p1}, LllIlIIIIlllIlllllIll;->IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)V

    .line 147
    :cond_a
    :goto_3
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->llIIllllIIlllIIIIlll(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    new-instance v0, LllIllllIlllIIIllIIIl;

    invoke-direct {v0, p1}, LllIllllIlllIIIllIIIl;-><init>(Landroid/content/Context;)V

    .line 150
    invoke-virtual {v0}, LllIllllIlllIIIllIIIl;->llIIIIlllllIIllIIllI()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Campaign message is available"

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 153
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 154
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Show Campaign Notification"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 155
    invoke-static {p1}, LIIlIlIllIlIIIIIIllll;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 110
    :cond_b
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 111
    iget v0, p0, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI:I

    if-ne v0, v4, :cond_9

    .line 112
    const-string v0, "Polling time is already passed. Start device init Immediately"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 113
    invoke-direct {p0, p1, v6}, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto :goto_2

    .line 116
    :cond_c
    const-string v0, "sec.fota.action.POLLING_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 117
    const-string v0, "Polling time is done. Start device init"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 118
    invoke-direct {p0, p1, v6}, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 133
    :cond_d
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 134
    iget v0, p0, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI:I

    if-ne v0, v4, :cond_a

    .line 135
    const-string v0, "Heartbeat time is already passed. Start Heartbeat report Immediately"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto :goto_3

    .line 138
    :cond_e
    const-string v0, "sec.fota.action.HEARTBEAT_TIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 139
    const-string v0, "Heartbeat time is done. Start Heartbeat report"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 140
    invoke-direct {p0, p1}, Lcom/sec/android/fotaclient/FotaUpdaterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 156
    :cond_f
    const-string v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Change Campaign Notification"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 158
    invoke-static {p1}, LIIlIlIllIlIIIIIIllll;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
