.class public Lcom/sec/android/fotaclient/GCMIntentService;
.super Lcom/google/android/gcm/llIIIIlllllIIllIIllI;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 22
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "GCMIntentService"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gcm/llIIIIlllllIIllIIllI;-><init>([Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected llIIIIlllllIIllIIllI(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/16 v5, 0x20

    .line 36
    const-string v0, "GCM message received"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 37
    const-string v0, "msg"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 38
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39
    const-string v0, "Push message is null!!"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    invoke-static {v0}, LlIIllIIIIIlllIIlllIl;->llllIIIllIlIIIIllllI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 44
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "message:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 45
    const-string v2, "DM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 46
    const-string v1, "Start server init"

    invoke-static {v1}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 47
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    const-string v2, "updateType"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 49
    const-string v2, "msg"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 51
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 52
    :cond_2
    const-string v2, "CM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 53
    const-string v1, "Receive campaign message"

    invoke-static {v1}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 54
    new-instance v1, LllIllllIlllIIIllIIIl;

    invoke-direct {v1, p1}, LllIllllIlllIIIllIIIl;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-virtual {v1, v0}, LllIllllIlllIIIllIIIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-static {p1}, LIIlIlIllIlIIIIIIllll;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto :goto_0

    .line 58
    :cond_3
    const-string v2, "AD"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 59
    const-string v1, "Report activation result"

    invoke-static {v1}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 60
    invoke-static {v0}, LlIIllIIIIIlllIIlllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/fotaclient/FotaReportHeartBeatService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 62
    const-string v1, "heartbeatType"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 65
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 67
    :cond_4
    const-string v2, "EX"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 68
    const-string v1, "Receive extension message"

    invoke-static {v1}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 69
    invoke-static {v0}, LlIIllIIIIIlllIIlllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-static {v0}, LlIIlllllIllIIIllIlII;->llIIIIlllllIIllIIllI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 71
    const-string v2, "sec.fota.intent.%s.PUSH"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 73
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 75
    const-string v2, "msg"

    invoke-static {v0}, LlIIlllllIllIIIllIlII;->llllIIIllIlIIIIllllI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 77
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 80
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Do not support push type:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected llIlIIIIlIIIIIlIlIII(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 101
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "GCM unregistered"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 102
    const/4 v0, 0x0

    invoke-static {p1, v0}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;Z)V

    .line 103
    return-void
.end method

.method protected lllIlIlIIIllIIlIllIl(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 87
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "GCM registered"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 88
    invoke-static {}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI()LllIllIIllIlIIlIlIlIl;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_1

    .line 90
    const-string v1, ""

    invoke-virtual {v0, p2, v1}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->llIIllllIIlllIIIIlll(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "GCM needs re-register"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x0

    invoke-static {p1, v0}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method protected llllIIIllIlIIIIllllI(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 27
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GCM error:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 28
    invoke-static {}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI()LllIllIIllIlIIlIlIlIl;

    move-result-object v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    const-string v1, ""

    invoke-virtual {v0, v1, p2}, LllIllIIllIlIIlIlIlIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    :cond_0
    return-void
.end method
