.class public Lcom/sec/android/fotaclient/log/MasterLogProvider;
.super LIllIIlllIllIIIIIllII;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, LIllIIlllIllIIIIIllII;-><init>()V

    return-void
.end method


# virtual methods
.method protected IIIIllIlIIlIIIIlllIl()Ljava/util/List;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 23
    const-string v1, "com.sec.android.log.x6g1q14r75"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    const-string v1, "com.sec.android.log.x6g1q14r75.wssyncmldm"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    const-string v1, "com.sec.android.log.diagmonagent"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    return-object v0
.end method

.method protected IllIlIIIIlIIlIIIllIl()Z
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->llIlIllllllllllllllI()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->llllllIllIlIlllIIlIl()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->llIIllllIIlllIIIIlll()V

    .line 64
    const-string v0, "get"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "testPollingMode"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 66
    const-string v1, "result"

    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->llllllIllIlIlllIIlIl()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 69
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, LIllIIlllIllIIIIIllII;->call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method protected llIIIIlllllIIllIIllI()Ljava/util/List;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 31
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 33
    :goto_0
    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    .line 34
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "log"

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fotaclient"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".log"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_0
    return-object v2
.end method

.method protected llIIIIlllllIIllIIllI(Z)V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_0

    sget v0, LllllIIIIlllIIIlIllIl;->llllIIIllIlIIIIllllI:I

    :goto_0
    invoke-static {v1, v0}, LllllIIIIlllIIIlIllIl;->llllIIIllIlIIIIllllI(Landroid/content/Context;I)Z

    .line 53
    return-void

    .line 51
    :cond_0
    sget v0, LllllIIIIlllIIIlIllIl;->llIIIIlllllIIllIIllI:I

    goto :goto_0
.end method

.method protected llIlIIIIlIIIIIlIlIII()Z
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LllllIIIIlllIIIlIllIl;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)I

    move-result v0

    sget v1, LllllIIIIlllIIIlIllIl;->llIIIIlllllIIllIIllI:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected llIlIllllllllllllllI()Z
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LllllIIIIlllIIIlIllIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method protected lllIlIlIIIllIIlIllIl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const-string v0, "com.sec.android.log.x6g1q14r75"

    return-object v0
.end method

.method protected llllIIIllIlIIIIllllI()Ljava/util/List;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method protected llllllIllIlIlllIIlIl()Z
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/log/MasterLogProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LIIIIlllIIIIIlllIIlll;->IlIlIlIlIlIIlllllIlI(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
