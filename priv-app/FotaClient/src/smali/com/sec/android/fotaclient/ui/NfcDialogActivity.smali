.class public Lcom/sec/android/fotaclient/ui/NfcDialogActivity;
.super Landroid/app/Activity;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field private llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;

.field private lllIlIlIIIllIIlIllIl:Landroid/widget/Toast;

.field private llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;

    .line 38
    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    .line 40
    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/widget/Toast;

    .line 42
    return-void
.end method

.method private llIIIIlllllIIllIIllI()V
    .locals 4

    .prologue
    const/16 v3, 0x20

    const/4 v2, 0x4

    .line 153
    const/4 v0, 0x1

    invoke-static {p0}, LIIIIlllIIIIIlllIIlll;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 154
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Request Init"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 155
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/fotaclient/FotaInitUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 156
    const-string v1, "updateType"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 157
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 158
    invoke-virtual {p0, v0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 166
    :goto_0
    return-void

    .line 160
    :cond_0
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Request Register"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 161
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/fotaclient/FotaRegisterService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 163
    const-string v1, "registerType"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 164
    invoke-virtual {p0, v0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private llIIIIlllllIIllIIllI(I)V
    .locals 2

    .prologue
    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 176
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    const v1, 0x7f06002f

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {p0, p1}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/android/fotaclient/ui/IIllIlIIIIlIIIllIllI;

    invoke-direct {v1, p0}, Lcom/sec/android/fotaclient/ui/IIllIlIIIIlIIIllIllI;-><init>(Lcom/sec/android/fotaclient/ui/NfcDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_0
    return-void

    .line 188
    :catch_0
    move-exception v0

    .line 189
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->finish()V

    goto :goto_0
.end method

.method private llIIIIlllllIIllIIllI(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p0, v0}, LIIIIllIlIIlIIIIlllIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->finish()V

    .line 90
    :goto_0
    return-void

    .line 71
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 73
    :sswitch_0
    const-string v0, "UI State: Connecting to target wifi network"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 74
    const v0, 0x7f060032

    invoke-direct {p0, v0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI(I)V

    goto :goto_0

    .line 78
    :sswitch_1
    const-string v0, "UI State: Fail to connect target wifi network"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI()V

    .line 80
    const v0, 0x7f060033

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->finish()V

    goto :goto_0

    .line 85
    :sswitch_2
    const-string v0, "UI State: Success to connect target wifi network"

    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI()V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->finish()V

    goto :goto_0

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x96 -> :sswitch_1
        0xc8 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/ui/NfcDialogActivity;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI()V

    return-void
.end method

.method static synthetic llIIIIlllllIIllIIllI(Lcom/sec/android/fotaclient/ui/NfcDialogActivity;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic llllIIIllIlIIIIllllI(Lcom/sec/android/fotaclient/ui/NfcDialogActivity;)Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;

    return-object v0
.end method

.method private llllIIIllIlIIIIllllI()V
    .locals 1

    .prologue
    .line 199
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 201
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public llIIIIlllllIIllIIllI(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 213
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/widget/Toast;

    .line 217
    :goto_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 218
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->lllIlIlIIIllIIlIllIl:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 97
    if-nez v0, :cond_0

    .line 98
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Intent is null"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->finish()V

    .line 136
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 104
    const-string v2, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 105
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "action is not android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->finish()V

    goto :goto_0

    .line 110
    :cond_1
    const-string v1, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 111
    if-nez v0, :cond_2

    .line 112
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "android.nfc.extra.NDEF_MESSAGES is null"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->finish()V

    goto :goto_0

    .line 117
    :cond_2
    iget-object v1, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;

    if-nez v1, :cond_3

    .line 118
    new-instance v1, Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;

    invoke-direct {v1, p0}, Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;-><init>(Lcom/sec/android/fotaclient/ui/NfcDialogActivity;)V

    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;

    .line 120
    :cond_3
    iget-object v1, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llIIIIlllllIIllIIllI:Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lcom/sec/android/fotaclient/ui/lllllllIlllIIlllIlIl;->llIIIIlllllIIllIIllI(I)V

    .line 121
    new-instance v1, Lcom/sec/android/fotaclient/ui/llllIllIIIIIlIIllIII;

    invoke-direct {v1, p0}, Lcom/sec/android/fotaclient/ui/llllIllIIIIIlIIllIII;-><init>(Lcom/sec/android/fotaclient/ui/NfcDialogActivity;)V

    .line 134
    new-instance v2, LIIIIlIllIlllIlIIIIlI;

    invoke-direct {v2}, LIIIIlIllIlllIlIIIIlI;-><init>()V

    .line 135
    invoke-virtual {v2, p0, v0, v1}, LIIIIlIllIlllIlIIIIlI;->llIIIIlllllIIllIIllI(Landroid/content/Context;[Landroid/os/Parcelable;LIlIlllIlllllIlIllllI;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :cond_0
    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    .line 149
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 150
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 145
    :try_start_1
    invoke-static {v0}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/sec/android/fotaclient/ui/NfcDialogActivity;->llllIIIllIlIIIIllllI:Landroid/app/ProgressDialog;

    throw v0
.end method
