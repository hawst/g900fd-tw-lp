.class public Lcom/sec/android/fotaclient/FotaRegisterReceiver;
.super Landroid/content/BroadcastReceiver;
.source "llIIIIlllllIIllIIllI"


# instance fields
.field private llIIIIlllllIIllIIllI:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 56
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI:I

    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 302
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/fotaclient/FotaRegisterService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 303
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 304
    const-string v1, "registerType"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 305
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 306
    return-void
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0xa

    const/4 v2, 0x0

    .line 202
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 203
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    invoke-static {p1}, LIlIlIlIllIlllIllllll;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    invoke-static {p1}, LIlIlIlIllIlllIllllll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Already passed disclaimer timer"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 208
    invoke-static {p1, v2}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    .line 209
    invoke-static {p1, v3}, LlIlIlIIIIlIIlIlIIlII;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)Z

    .line 210
    invoke-static {p1}, LlIlIlIIIIIlIlIIlllII;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto :goto_0

    .line 212
    :cond_2
    const-string v0, "sec.fota.action.DISCLAIMER_TIME"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 213
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Receive disclaimer timer"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 214
    invoke-static {p1, v2}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    .line 215
    invoke-static {p1, v3}, LlIlIlIIIIlIIlIlIIlII;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)Z

    .line 216
    invoke-static {p1}, LlIlIlIIIIIlIlIIlllII;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto :goto_0

    .line 217
    :cond_3
    const-string v0, "sec.fota.action.SOFTWARE_UPDATE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "disclaimer timer finish. Need to agree FOTA disclaimer first"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 219
    invoke-static {p1, v2}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    .line 220
    const-wide/16 v0, 0x0

    invoke-static {p1, v0, v1}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 221
    invoke-static {p1}, LIlIlIlIllIlllIllllll;->IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)V

    .line 223
    const/16 v0, 0x1e

    invoke-static {p1, v0}, LlIlIlIIIIlIIlIlIIlII;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)Z

    .line 224
    invoke-static {p1}, LlIlIlIIIIIlIlIIlllII;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    .line 225
    invoke-direct {p0, p1}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llllIIIllIlIIIIllllI(Landroid/content/Context;)V

    goto :goto_0

    .line 228
    :cond_4
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Recovery wrong Terms state.."

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 229
    invoke-static {p1, v2}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/16 v3, 0x1e

    const/4 v2, 0x1

    .line 129
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 130
    const-string v0, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 131
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Show Disclaimer Notification after Setup wizard"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 132
    invoke-static {p1}, LlIlIlIIIIlIIlIlIIlII;->llllIIIllIlIIIIllllI(Landroid/content/Context;)I

    move-result v0

    if-eq v3, v0, :cond_0

    .line 133
    invoke-static {p1, p3}, LlIlIlIIIIlIIlIlIIlII;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)Z

    .line 134
    :cond_0
    invoke-static {p1}, LlIlIlIIIIIlIlIIlllII;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    .line 135
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LIIIllIIllIllIlIIlIIl;->llllIIIllIlIIIIllllI(Landroid/content/Context;J)Z

    .line 141
    :cond_1
    :goto_0
    const-string v0, "com.sec.android.app.SecSetupWizard"

    invoke-static {p1, v0}, LIIIIllIlIIlIIIIlllIl;->llllIIIllIlIIIIllllI(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, LIIIllIIllIllIlIIlIIl;->IIIIllIlIIlIIIIlllIl(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 143
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Setup Wizard is not completed"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 199
    :cond_2
    :goto_1
    return-void

    .line 136
    :cond_3
    const-string v0, "sec.fota.action.SOFTWARE_UPDATE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Need to agree FOTA disclaimer by User action"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 138
    invoke-direct {p0, p1}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llllIIIllIlIIIIllllI(Landroid/content/Context;)V

    goto :goto_0

    .line 147
    :cond_4
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 148
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Show Disclaimer Notification"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 149
    invoke-static {p1}, LlIlIlIIIIlIIlIlIIlII;->llllIIIllIlIIIIllllI(Landroid/content/Context;)I

    move-result v0

    if-eq v3, v0, :cond_5

    .line 150
    invoke-static {p1, p3}, LlIlIlIIIIlIIlIlIIlII;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)Z

    .line 151
    :cond_5
    invoke-static {p1}, LlIlIlIIIIIlIlIIlllII;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto :goto_1

    .line 152
    :cond_6
    const-string v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 153
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Change Disclaimer Notification"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 154
    invoke-static {p1}, LlIlIlIIIIIlIlIIlllII;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto :goto_1

    .line 158
    :cond_7
    invoke-static {p1}, LlIlIlIIIIlIIlIlIIlII;->llllIIIllIlIIIIllllI(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_8

    .line 159
    const/4 v0, 0x0

    invoke-static {p1, v0}, LlIlIlIIIIlIIlIlIIlII;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)Z

    .line 160
    invoke-static {p1}, LlIlIlIIIIIlIlIIlllII;->llllIIIllIlIIIIllllI(Landroid/content/Context;)V

    .line 163
    :cond_8
    const-string v0, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 164
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "EULA is agreed now"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 165
    invoke-static {p1}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    .line 166
    invoke-static {p1}, LIlIlIlIllIlllIllllll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    .line 167
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LIIIllIIllIllIlIIlIIl;->llllIIIllIlIIIIllllI(Landroid/content/Context;J)Z

    .line 179
    :cond_9
    :goto_2
    invoke-static {p1}, LIIIllIIllIllIlIIlIIl;->IIIIllIlIIlIIIIlllIl(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 180
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Setup Wizard is not completed"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto :goto_1

    .line 168
    :cond_a
    const-string v0, "android.intent.action.TIME_SET"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 169
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Reset disclaimer time."

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 170
    invoke-static {p1}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    goto :goto_2

    .line 171
    :cond_b
    const-string v0, "sec.fota.action.DISCLAIMER_TIME"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 172
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Receive disclaimer timer. Start to register Device"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 173
    invoke-direct {p0, p1, v2}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto :goto_2

    .line 174
    :cond_c
    const-string v0, "sec.fota.action.SOFTWARE_UPDATE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 175
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "EULA already agreed. Start to register Device by User action"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 176
    const/4 v0, 0x6

    invoke-direct {p0, p1, v0}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto :goto_2

    .line 184
    :cond_d
    invoke-static {p1}, LIlIlIlIllIlllIllllll;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 185
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 186
    invoke-static {p1}, LIlIlIlIllIlllIllllll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LIlIlIlIllIlllIllllll;->llIIIIlllllIIllIIllI(Landroid/content/Context;J)V

    goto/16 :goto_1

    .line 188
    :cond_e
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Waiting until disclaimer timer..."

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 191
    :cond_f
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    iget v0, p0, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    .line 193
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "EULA already agreed. Start to register Device Immediately"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 194
    invoke-direct {p0, p1, v2}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_1
.end method

.method private lllIlIlIIIllIIlIllIl(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 265
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    invoke-static {p1, v1}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    const-string v0, "sec.fota.action.SOFTWARE_UPDATE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    invoke-static {p1, v1}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    .line 269
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 270
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Need to agree FOTA disclaimer by User action"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 271
    invoke-direct {p0, p1}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llllIIIllIlIIIIllllI(Landroid/content/Context;)V

    goto :goto_0

    .line 273
    :cond_2
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Start to register Device by User action"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 274
    const/4 v0, 0x6

    invoke-direct {p0, p1, v0}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private llllIIIllIlIIIIllllI(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 292
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/fotaclient/ui/DisclaimerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 293
    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 295
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 296
    return-void
.end method

.method private llllIIIllIlIIIIllllI(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 236
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 237
    iget v0, p0, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    .line 238
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->llIIllllIIlllIIIIlll(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->IIIIllIlIIlIIIIlllIl(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    const-string v0, "GCM"

    sget-object v1, LllIIIIlllllIIllllllI;->IIIIIIlIIIllllllIlII:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "com.google.android.gsf"

    invoke-static {p1, v0}, LIIIIllIlIIlIIIIlllIl;->llllIIIllIlIIIIllllI(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 245
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Not supported to register push"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 250
    :cond_2
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Need to register push"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 251
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto :goto_0

    .line 258
    :cond_3
    const-string v0, "sec.fota.intent.AUTOUPDATE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto :goto_0
.end method


# virtual methods
.method protected llIIIIlllllIIllIIllI(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 284
    invoke-static {p1}, LIIIllIIllIllIlIIlIIl;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)V

    .line 289
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/16 v5, 0x14

    const/16 v4, 0xa

    .line 61
    if-nez p1, :cond_0

    .line 62
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Content is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 126
    :goto_0
    return-void

    .line 66
    :cond_0
    if-nez p2, :cond_1

    .line 67
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Intent is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Action is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_2
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receive broadcast meassage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    .line 79
    invoke-static {}, LIIIIllIlIIlIIIIlllIl;->llIIIIlllllIIllIIllI()Z

    move-result v0

    if-nez v0, :cond_3

    .line 80
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "No action is available when User is not owner"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 81
    const-string v0, "com.sec.android.fotaclient"

    invoke-static {p1, v0}, LIIIIllIlIIlIIIIlllIl;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_3
    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 87
    if-eqz v0, :cond_6

    sget-object v2, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 89
    iput v5, p0, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI:I

    .line 95
    :cond_4
    :goto_1
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 96
    invoke-virtual {p0, p1}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;)V

    .line 99
    :cond_5
    invoke-static {p1}, LIIIIlllIIIIIlllIIlll;->llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 121
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Recovery wrong Terms state.."

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 122
    const/4 v0, 0x0

    invoke-static {p1, v0}, LIIIIlllIIIIIlllIIlll;->llIIIIlllllIIllIIllI(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 91
    :cond_6
    iput v4, p0, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI:I

    goto :goto_1

    .line 101
    :pswitch_0
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Need to device registering..."

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 102
    invoke-direct {p0, p1, v1, v4}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 105
    :pswitch_1
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Need to device updating..."

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 106
    invoke-direct {p0, p1, v1, v5}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 109
    :pswitch_2
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Waiting to device register..."

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 110
    invoke-direct {p0, p1, v1}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 113
    :pswitch_3
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Already device registered..."

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 114
    invoke-direct {p0, p1, v1}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->llllIIIllIlIIIIllllI(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 117
    :pswitch_4
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v2, "Don\'t register device anymore..."

    invoke-virtual {v0, v2}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 118
    invoke-direct {p0, p1, v1}, Lcom/sec/android/fotaclient/FotaRegisterReceiver;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
