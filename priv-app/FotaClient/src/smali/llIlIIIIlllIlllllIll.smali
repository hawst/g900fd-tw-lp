.class public LllIlIIIIlllIlllllIll;
.super Ljava/lang/Object;
.source "llIIIIlllllIIllIIllI"


# direct methods
.method public static IllIlIIIIlIIlIIIllIl(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 136
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 137
    if-nez v0, :cond_0

    .line 138
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "AlarmManager is null!!"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIlIIIIlIIIIIlIlIII(Ljava/lang/String;)V

    .line 146
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-static {p0}, LIIIIlllIIIIIlllIIlll;->lllllIIlIIIlIlIIIllI(Landroid/content/Context;)J

    move-result-wide v2

    .line 142
    const/4 v1, 0x1

    const/4 v4, 0x0

    new-instance v5, Landroid/content/Intent;

    const-string v6, "sec.fota.action.HEARTBEAT_TIME"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v6, 0x8000000

    invoke-static {p0, v4, v5, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 145
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Start heartbeat timer: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llllIIIllIlIIIIllllI(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static llIIIIlllllIIllIIllI(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 168
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd/HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 169
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 170
    return-object v0
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 79
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Calculate immediate heartbeat time"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 81
    const-wide/16 v2, 0x2710

    add-long/2addr v2, v0

    .line 83
    invoke-static {p0, v2, v3}, LIIIIlllIIIIIlllIIlll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;J)Z

    .line 84
    sget-object v4, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Current time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0, v1}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 85
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Next heartbeat time:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public static llIIIIlllllIIllIIllI(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 45
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 46
    new-instance v2, Lorg/xml/sax/InputSource;

    invoke-direct {v2, v1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 47
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 48
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 49
    invoke-virtual {v1, v2}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v1

    .line 51
    if-eqz v1, :cond_0

    .line 52
    invoke-interface {v1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 53
    if-eqz v1, :cond_0

    .line 54
    const-string v2, "ServiceURL"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 55
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-lez v2, :cond_0

    .line 56
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v1

    .line 57
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 58
    invoke-static {p0, v1}, LlIlIlIIIIlIIlIlIIlII;->llllIIIllIlIIIIllllI(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    goto :goto_0

    .line 64
    :catch_0
    move-exception v1

    .line 65
    invoke-static {v1}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 66
    :catch_1
    move-exception v1

    .line 67
    invoke-static {v1}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 68
    :catch_2
    move-exception v1

    .line 69
    invoke-static {v1}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static llIlIIIIlIIIIIlIlIII(Landroid/content/Context;)Z
    .locals 6

    .prologue
    .line 120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 121
    invoke-static {p0}, LIIIIlllIIIIIlllIIlll;->lllllIIlIIIlIlIIIllI(Landroid/content/Context;)J

    move-result-wide v2

    .line 122
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Next heartbeat time:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2, v3}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 124
    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 125
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "heartbeat time is vaild"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 126
    const/4 v0, 0x1

    .line 129
    :goto_0
    return v0

    .line 128
    :cond_0
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "heartbeat time is not vaild"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 129
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static lllIlIlIIIllIIlIllIl(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 112
    invoke-static {p0}, LIIIIlllIIIIIlllIIlll;->IIIIIIlIIIllllllIlII(Landroid/content/Context;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {p0}, LIIIIlllIIIIIlllIIlll;->IlIIIllllIIlIIIIIlII(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static llllIIIllIlIIIIllllI(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/16 v6, 0x3c

    .line 92
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    const-string v1, "Calculate next heartbeat time"

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->lllIlIlIIIllIIlIllIl(Ljava/lang/String;)V

    .line 93
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 96
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 98
    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    .line 99
    invoke-virtual {v3, v0, v1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 100
    const/4 v4, 0x5

    invoke-static {p0}, LIIIIlllIIIIIlllIIlll;->IIIIIIlIIIllllllIlII(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/util/GregorianCalendar;->add(II)V

    .line 101
    const/16 v4, 0xb

    const/16 v5, 0x18

    invoke-virtual {v2, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/util/GregorianCalendar;->set(II)V

    .line 102
    const/16 v4, 0xc

    invoke-virtual {v2, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/util/GregorianCalendar;->set(II)V

    .line 103
    const/16 v4, 0xd

    invoke-virtual {v2, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-virtual {v3, v4, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 105
    invoke-virtual {v3}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    .line 106
    invoke-static {p0, v2, v3}, LIIIIlllIIIIIlllIIlll;->lllIlIlIIIllIIlIllIl(Landroid/content/Context;J)Z

    .line 107
    sget-object v4, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Current time:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0, v1}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 108
    sget-object v0, LIllIlIIIIlIIlIIIllIl;->llIIIIlllllIIllIIllI:LIIlIIIIIIllllIIlIIlI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Next heartbeat time:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, LllIlIIIIlllIlllllIll;->llIIIIlllllIIllIIllI(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LIIlIIIIIIllllIIlIIlI;->llIIIIlllllIIllIIllI(Ljava/lang/String;)V

    .line 109
    return-void
.end method
