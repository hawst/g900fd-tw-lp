.class public interface abstract Lcom/wssnps/c/a;
.super Ljava/lang/Object;
.source "smlBackupRestoreInterface.java"


# static fields
.field public static final cF:[[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 65
    const/16 v0, 0x1d

    new-array v0, v0, [[Ljava/lang/String;

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Calllog"

    aput-object v2, v1, v5

    const-string v2, "com.sec.android.provider.logsprovider"

    aput-object v2, v1, v6

    const-string v2, "1"

    aput-object v2, v1, v7

    const-string v2, "auto"

    aput-object v2, v1, v8

    const-string v2, "Async"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "\\_SamsungBnR_\\ABR\\Calllog.bk"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "call_log.xml"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "File"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "call_log.xml"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "all"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, ""

    aput-object v3, v1, v2

    aput-object v1, v0, v5

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Alarm"

    aput-object v2, v1, v5

    const-string v2, "com.sec.android.app.clockpackage"

    aput-object v2, v1, v6

    const-string v2, "2"

    aput-object v2, v1, v7

    const-string v2, "auto"

    aput-object v2, v1, v8

    const-string v2, "Async"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "\\_SamsungBnR_\\ABR\\Alarm.bk"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "alarm.xml"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "File"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "alarm.xml"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "all"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, ""

    aput-object v3, v1, v2

    aput-object v1, v0, v6

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Wallpaper"

    aput-object v2, v1, v5

    const-string v2, "com.sec.android.app.wallpaperchooser;com.sec.android.app.wallpapers"

    aput-object v2, v1, v6

    const-string v2, "2"

    aput-object v2, v1, v7

    const-string v2, "auto"

    aput-object v2, v1, v8

    const-string v2, "Async"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "\\_SamsungBnR_\\ABR\\Wallpaper.bk"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "wallpaper.xml"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "File"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "all"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, ""

    aput-object v3, v1, v2

    aput-object v1, v0, v7

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Bookmark"

    aput-object v2, v1, v5

    const-string v2, "com.sec.android.app.sbrowser"

    aput-object v2, v1, v6

    const-string v2, "auto"

    aput-object v2, v1, v7

    const-string v2, "auto"

    aput-object v2, v1, v8

    const-string v2, "Async"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "\\_SamsungBnR_\\ABR\\Bookmark.bk"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "bookmark.xml"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "File"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "restoreonly"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "all"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, ""

    aput-object v3, v1, v2

    aput-object v1, v0, v8

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Shealth"

    aput-object v2, v1, v5

    const-string v2, "com.sec.android.app.shealth"

    aput-object v2, v1, v6

    const-string v2, "1"

    aput-object v2, v1, v7

    const-string v2, "auto"

    aput-object v2, v1, v8

    const-string v2, "Async"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "1.0"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "\\_SamsungBnR_\\ABR\\Shealth.bk"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "shealth.xml"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "File"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "seconly"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, ""

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "platform"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, ""

    aput-object v3, v1, v2

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "StoryAlbum"

    aput-object v3, v2, v5

    const-string v3, "com.samsung.android.app.episodes"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\Storyalbum\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "filelist.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "story_album.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "seconly"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "LocationServiceVzw"

    aput-object v3, v2, v5

    const-string v3, "SecProductFeature_GPS.SEC_PRODUCT_FEATURE_GPS_SUPPORT_KIES_VZWLOCATION"

    aput-object v3, v2, v6

    const-string v3, "1"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\Locationservicevzw.bk"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "location_service.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "File"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "seconly"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "WiFi"

    aput-object v3, v2, v5

    const-string v3, "SecProductFeature_WLAN.SEC_PRODUCT_FEATURE_WLAN_SEC_KIES_BACKUP_SUPPORT"

    aput-object v3, v2, v6

    const-string v3, "1"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\Wifiwpaconf.bk"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "wifi_wpaconf.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "File"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "seconly;usbonly"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Ringtone"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.app.ringtoneBR"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\Ringtone\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "filelist.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "ringtone.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "seconly"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "device"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "LockScreen"

    aput-object v3, v2, v5

    const-string v3, "com.android.systemui"

    aput-object v3, v2, v6

    const-string v3, "1"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\Lockscreen.bk"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "lockscreen.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "File"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "VoiceMemo"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.app.voicerecorder;com.sec.android.app.voicenote"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\Sounds\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "restoreonly"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "HomeScreen"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.app.launcher"

    aput-object v3, v2, v6

    const-string v3, "18"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\Homescreen.bk"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "homescreen.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "File"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "seconly"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "homescreen.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Radio"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.app.fm;com.samsung.app.fmradio"

    aput-object v3, v2, v6

    const-string v3, "1"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\Radio.bk"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "radio.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "model"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "SNote3"

    aput-object v3, v2, v5

    const-string v3, "com.samsung.android.snote"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\SnoteData\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "spd;snb"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "LinkedToSplanner"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Snote"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.provider.snote"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\S Note\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "snb"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "LinkedToSplanner"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "SMemo2"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.widgetapp.diotek.smemo"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\SMemo\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "snb"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "LinkedToSplanner"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "TMemo2"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.app.memo"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\TMemo\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "snb"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "LinkedToSplanner"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Music"

    aput-object v3, v2, v5

    const-string v3, "MultimediaContents"

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "mp3;m4a;mp4;wav;amr;awb;wma;ogg;oga;aac;3ga;flac;mpga;imy;mid;qcp;mmf;asf"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Photo"

    aput-object v3, v2, v5

    const-string v3, "MultimediaContents"

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "jpg;jpeg;gif;png;bmp;wbmp;tif;tiff"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Video"

    aput-object v3, v2, v5

    const-string v3, "MultimediaContents"

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "avi;mp4;m4v;3gp;3g2;3gpp;3gpp2;mkv;wmv;asf;flv;divx;mpg;mpeg;rm;rmvb;mov;k3g;skm;svi;swf"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Others"

    aput-object v3, v2, v5

    const-string v3, "MultimediaContents"

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "Internal:/Android/data;Internal:/_SamsungBnR_"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "*"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Playlist"

    aput-object v3, v2, v5

    const-string v3, "MultimediaContents"

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\Playlists"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "pla"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Configuration"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\BR\\Configuration.bk"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "File"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Wallpaper;Ringtone;ETC;noAirplain"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "model"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Email"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\BR\\Email.bk"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "File"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "Account;Settings"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "device"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Application"

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    const-string v3, "2"

    aput-object v3, v2, v7

    const-string v3, "preload"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\restore\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "VerificationType1"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "device"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "SMemo"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.widgetapp.diotek.smemo"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Sync"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "Memo"

    aput-object v3, v2, v5

    const-string v3, "com.sec.android.app.memo"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "2.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "LifeTimes"

    aput-object v3, v2, v5

    const-string v3, "com.samsung.android.app.lifetimes"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\LifeTimes\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "filelist.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, "lifetimes.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, "lifetimes.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "platform"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, ""

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const/16 v2, 0x11

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "NMemo"

    aput-object v3, v2, v5

    const-string v3, "com.samsung.android.app.memo"

    aput-object v3, v2, v6

    const-string v3, "auto"

    aput-object v3, v2, v7

    const-string v3, "auto"

    aput-object v3, v2, v8

    const-string v3, "Async"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "1.0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "\\_SamsungBnR_\\ABR\\NMemo\\"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string v4, "filelist.xml"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string v4, "bk;snb"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string v4, "Folder"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string v4, "NMemoBnR;hasTitle;hasCategory"

    aput-object v4, v2, v3

    const/16 v3, 0xe

    const-string v4, ""

    aput-object v4, v2, v3

    const/16 v3, 0xf

    const-string v4, "all"

    aput-object v4, v2, v3

    const/16 v3, 0x10

    const-string v4, "TMemo1;TMemo2"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    sput-object v0, Lcom/wssnps/c/a;->cF:[[Ljava/lang/String;

    return-void
.end method
