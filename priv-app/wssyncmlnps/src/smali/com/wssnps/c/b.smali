.class public Lcom/wssnps/c/b;
.super Ljava/lang/Object;
.source "smlMakeDeviceInfo.java"


# instance fields
.field private A:[Ljava/lang/String;

.field private B:I

.field private C:J

.field private D:Ljava/lang/String;

.field private E:I

.field private F:I

.field private G:Ljava/lang/String;

.field private H:Z

.field private a:Lorg/xmlpull/v1/XmlSerializer;

.field private b:Ljava/lang/String;

.field private c:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:[Ljava/io/File;

.field private z:[Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v5, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->b:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->d:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->e:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->f:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->g:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->h:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->i:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->j:Ljava/lang/String;

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->k:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->l:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->m:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->n:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->o:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->p:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->q:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->r:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->s:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->t:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->u:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->v:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->w:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->x:Ljava/lang/String;

    .line 61
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "hk.xml"

    aput-object v1, v0, v5

    const-string v1, "BackupRestore.xml"

    aput-object v1, v0, v7

    const-string v1, "ServiceInfo.xml"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/wssnps/c/b;->A:[Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->D:Ljava/lang/String;

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->G:Ljava/lang/String;

    .line 76
    const-string v0, "smlMakeDeviceInfo"

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 78
    iput-object p2, p0, Lcom/wssnps/c/b;->b:Ljava/lang/String;

    .line 79
    iput-object p1, p0, Lcom/wssnps/c/b;->c:Landroid/content/Context;

    .line 81
    iget-object v0, p0, Lcom/wssnps/c/b;->b:Ljava/lang/String;

    const-string v1, "DeviceInfo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/wssnps/c/b;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/wssnps/c/b;->b:Ljava/lang/String;

    const-string v2, "DeviceInfo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->x:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/wssnps/c/b;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/wssnps/c/b;->x:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->y:[Ljava/io/File;

    .line 89
    :cond_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->d:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/wssnps/c/b;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 92
    iget-object v0, p0, Lcom/wssnps/c/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 93
    const-string v1, "03"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 94
    const-string v0, "3.0"

    iput-object v0, p0, Lcom/wssnps/c/b;->d:Ljava/lang/String;

    .line 103
    :goto_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->e:Ljava/lang/String;

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->f:Ljava/lang/String;

    .line 105
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->g:Ljava/lang/String;

    .line 107
    invoke-static {}, Lcom/wssnps/smlModelDefine;->j()Ljava/lang/String;

    move-result-object v0

    .line 108
    const-string v1, "TRUE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 109
    const-string v0, "SM-N900"

    iput-object v0, p0, Lcom/wssnps/c/b;->h:Ljava/lang/String;

    .line 113
    :goto_1
    const-string v0, "Android"

    iput-object v0, p0, Lcom/wssnps/c/b;->i:Ljava/lang/String;

    .line 114
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/c/b;->j:Ljava/lang/String;

    .line 115
    iget-object v0, p0, Lcom/wssnps/c/b;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->j:Ljava/lang/String;

    .line 117
    :cond_1
    const-string v0, "encryption"

    iput-object v0, p0, Lcom/wssnps/c/b;->k:Ljava/lang/String;

    .line 119
    invoke-static {}, Lcom/wssnps/smlModelDefine;->l()Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-static {}, Lcom/wssnps/smlModelDefine;->m()Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 123
    :cond_2
    invoke-static {}, Lcom/wssnps/smlModelDefine;->n()Ljava/lang/String;

    move-result-object v2

    .line 124
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 126
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/wssnps/c/b;->l:Ljava/lang/String;

    .line 127
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->m:Ljava/lang/String;

    .line 135
    :goto_2
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 136
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 138
    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/wssnps/c/b;->o:Ljava/lang/String;

    .line 139
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->n:Ljava/lang/String;

    .line 142
    :cond_3
    iget-object v0, p0, Lcom/wssnps/c/b;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 143
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->o:Ljava/lang/String;

    .line 145
    :cond_4
    iget-object v0, p0, Lcom/wssnps/c/b;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 146
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->n:Ljava/lang/String;

    .line 148
    :cond_5
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->p:Ljava/lang/String;

    .line 149
    invoke-static {}, Lcom/wssnps/smlModelDefine;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->q:Ljava/lang/String;

    .line 150
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->r:Ljava/lang/String;

    .line 151
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->s:Ljava/lang/String;

    .line 152
    const-string v0, "SEC_FLOATING_FEATURE_WSSNPS_SUPPORT_MULTISIM"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 153
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->t:Ljava/lang/String;

    .line 154
    :cond_6
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/b;->u:Ljava/lang/String;

    .line 155
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->v:Ljava/lang/String;

    .line 156
    invoke-static {}, Lcom/wssnps/smlModelDefine;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->w:Ljava/lang/String;

    .line 158
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/etc/kies/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->z:[Ljava/io/File;

    .line 160
    invoke-static {}, Lcom/wssnps/smlModelDefine;->s()I

    move-result v0

    iput v0, p0, Lcom/wssnps/c/b;->B:I

    .line 161
    iget v0, p0, Lcom/wssnps/c/b;->B:I

    if-eqz v0, :cond_b

    .line 162
    iput-boolean v7, p0, Lcom/wssnps/c/b;->H:Z

    .line 166
    :goto_3
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->n(Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/wssnps/c/b;->C:J

    .line 167
    iget v0, p0, Lcom/wssnps/c/b;->B:I

    invoke-static {p1, v0}, Lcom/wssnps/smlModelDefine;->b(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->D:Ljava/lang/String;

    .line 169
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->o(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 170
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/wssnps/c/b;->E:I

    .line 171
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/wssnps/c/b;->F:I

    .line 173
    const-string v0, "ReadFromMTP"

    iput-object v0, p0, Lcom/wssnps/c/b;->G:Ljava/lang/String;

    .line 174
    return-void

    .line 96
    :cond_7
    const-string v0, "2.0"

    iput-object v0, p0, Lcom/wssnps/c/b;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 100
    :cond_8
    const-string v0, "2.0"

    iput-object v0, p0, Lcom/wssnps/c/b;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 111
    :cond_9
    const-string v0, "GT-I9300"

    iput-object v0, p0, Lcom/wssnps/c/b;->h:Ljava/lang/String;

    goto/16 :goto_1

    .line 131
    :cond_a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/wssnps/c/b;->l:Ljava/lang/String;

    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->m:Ljava/lang/String;

    goto/16 :goto_2

    .line 164
    :cond_b
    iput-boolean v5, p0, Lcom/wssnps/c/b;->H:Z

    goto/16 :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 379
    iget-object v0, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, ""

    invoke-interface {v0, v1, p2, p3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 380
    iget-object v0, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 381
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 351
    iget-object v0, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 352
    if-eqz p3, :cond_0

    .line 354
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 355
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 359
    :try_start_0
    iget-object v2, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const/4 v3, 0x1

    invoke-interface {v2, v1, v0, v3}, Lorg/xmlpull/v1/XmlSerializer;->text([CII)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 361
    :catch_0
    move-exception v2

    .line 363
    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "skip invalid character "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-char v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " !!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 364
    iget-object v2, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, "_"

    invoke-interface {v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_1

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 374
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 178
    const/4 v0, 0x2

    const-string v1, "smlDeviceInfo"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 180
    new-instance v4, Ljava/io/StringWriter;

    invoke-direct {v4}, Ljava/io/StringWriter;-><init>()V

    .line 181
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    .line 182
    const-string v0, ""

    .line 186
    :try_start_0
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v1, v4}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    .line 189
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, "UTF-8"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v1, v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 190
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, "http://xmlpull.org/v1/doc/features.html#indent-output"

    const/4 v5, 0x1

    invoke-interface {v1, v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 192
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v5, "Deviceinfo"

    invoke-interface {v1, v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 194
    const-string v1, "VersionCode"

    const-string v3, "2.0"

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 195
    const-string v1, "USBVersion"

    iget-object v3, p0, Lcom/wssnps/c/b;->d:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 196
    const-string v1, "ModelName"

    iget-object v3, p0, Lcom/wssnps/c/b;->e:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 197
    const-string v1, "DisplayName"

    iget-object v3, p0, Lcom/wssnps/c/b;->f:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 199
    iget-object v1, p0, Lcom/wssnps/c/b;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 200
    const-string v1, "UserFriendlyDisplayName"

    iget-object v3, p0, Lcom/wssnps/c/b;->g:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 202
    :cond_0
    const-string v1, "BaseModel"

    iget-object v3, p0, Lcom/wssnps/c/b;->h:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 203
    const-string v1, "Platform"

    iget-object v3, p0, Lcom/wssnps/c/b;->i:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 204
    const-string v1, "PlatformVersion"

    iget-object v3, p0, Lcom/wssnps/c/b;->j:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 206
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v5, "Support"

    invoke-interface {v1, v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 207
    const-string v1, "AsyncPims"

    iget-object v3, p0, Lcom/wssnps/c/b;->k:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 208
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v5, "Support"

    invoke-interface {v1, v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 210
    const-string v1, "Version"

    iget-object v3, p0, Lcom/wssnps/c/b;->l:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 211
    const-string v1, "HiddenVersion"

    iget-object v3, p0, Lcom/wssnps/c/b;->m:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 212
    const-string v1, "MNC"

    iget-object v3, p0, Lcom/wssnps/c/b;->n:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 213
    const-string v1, "MCC"

    iget-object v3, p0, Lcom/wssnps/c/b;->o:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 214
    const-string v1, "ProductCode"

    iget-object v3, p0, Lcom/wssnps/c/b;->p:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 215
    const-string v1, "SerialNumber"

    iget-object v3, p0, Lcom/wssnps/c/b;->q:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 216
    const-string v1, "IMEI"

    iget-object v3, p0, Lcom/wssnps/c/b;->r:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 217
    const-string v1, "PhoneNumber"

    iget-object v3, p0, Lcom/wssnps/c/b;->s:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 218
    const-string v1, "SEC_FLOATING_FEATURE_WSSNPS_SUPPORT_MULTISIM"

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    const-string v1, "PhoneNumber"

    iget-object v3, p0, Lcom/wssnps/c/b;->t:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 220
    :cond_1
    const-string v1, "Security"

    iget-object v3, p0, Lcom/wssnps/c/b;->u:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 221
    const-string v1, "Limit"

    iget-object v3, p0, Lcom/wssnps/c/b;->v:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 222
    const-string v1, "UniqueNumber"

    iget-object v3, p0, Lcom/wssnps/c/b;->w:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v1, v3, v5}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 224
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v5, "ProfileList"

    invoke-interface {v1, v3, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 225
    iget-object v1, p0, Lcom/wssnps/c/b;->z:[Ljava/io/File;

    if-eqz v1, :cond_3

    move v1, v2

    .line 227
    :goto_0
    iget-object v3, p0, Lcom/wssnps/c/b;->z:[Ljava/io/File;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 229
    const-string v3, "Kies_Default_Feature.xml"

    iget-object v5, p0, Lcom/wssnps/c/b;->z:[Ljava/io/File;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 227
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 235
    :cond_2
    const-string v3, "Profile"

    iget-object v5, p0, Lcom/wssnps/c/b;->z:[Ljava/io/File;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {p0, v3, v5, v6}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 333
    :catch_0
    move-exception v1

    .line 335
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 346
    :goto_2
    return-object v0

    .line 240
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/wssnps/c/b;->y:[Ljava/io/File;

    if-eqz v1, :cond_6

    move v3, v2

    .line 242
    :goto_3
    iget-object v1, p0, Lcom/wssnps/c/b;->y:[Ljava/io/File;

    array-length v1, v1

    if-ge v3, v1, :cond_6

    move v1, v2

    .line 244
    :goto_4
    iget-object v5, p0, Lcom/wssnps/c/b;->A:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_4

    .line 246
    iget-object v5, p0, Lcom/wssnps/c/b;->A:[Ljava/lang/String;

    aget-object v5, v5, v1

    iget-object v6, p0, Lcom/wssnps/c/b;->y:[Ljava/io/File;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 248
    const-string v1, "Profile"

    iget-object v5, p0, Lcom/wssnps/c/b;->y:[Ljava/io/File;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {p0, v1, v5, v6}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 242
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 244
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 255
    :cond_6
    const-string v1, "Profile"

    const-string v2, "DeviceInfo.xml"

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 256
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, ""

    const-string v3, "ProfileList"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 258
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, ""

    const-string v3, "User"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 259
    iget-boolean v1, p0, Lcom/wssnps/c/b;->H:Z

    if-nez v1, :cond_d

    .line 260
    const-string v1, "Desc"

    const-string v2, "Owner"

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 264
    :goto_5
    const-string v1, "ID"

    iget-wide v2, p0, Lcom/wssnps/c/b;->C:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 265
    const-string v1, "Name"

    iget-object v2, p0, Lcom/wssnps/c/b;->D:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 266
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, ""

    const-string v3, "User"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 268
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, ""

    const-string v3, "Display"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 269
    const-string v1, "Width"

    iget v2, p0, Lcom/wssnps/c/b;->E:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 270
    const-string v1, "Height"

    iget v2, p0, Lcom/wssnps/c/b;->F:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 271
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, ""

    const-string v3, "Display"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 273
    const-string v1, "ProductLineup"

    iget-object v2, p0, Lcom/wssnps/c/b;->G:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 275
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, ""

    const-string v3, "ConnectModes"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 276
    iget-object v1, p0, Lcom/wssnps/c/b;->G:Ljava/lang/String;

    const-string v2, "HomeSync"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 278
    const-string v1, "FileManagement"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v1, "Photo"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const-string v1, "Music"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v1, "Video"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v1, "Phonebook"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v1, "Splanner"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-boolean v1, p0, Lcom/wssnps/c/b;->H:Z

    if-nez v1, :cond_8

    .line 287
    const-string v1, "KDI"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 289
    iget-object v1, p0, Lcom/wssnps/c/b;->c:Landroid/content/Context;

    const-string v2, "com.android.mms"

    invoke-static {v1, v2}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_7

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v1, :cond_7

    .line 291
    const-string v1, "Message"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :cond_7
    iget-object v1, p0, Lcom/wssnps/c/b;->c:Landroid/content/Context;

    const-string v2, "com.samsung.android.app.episodes"

    invoke-static {v1, v2}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 295
    if-eqz v1, :cond_8

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v2, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v2, :cond_8

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_8

    .line 296
    const-string v1, "StoryAlbum"

    const-string v2, "value"

    const-string v3, "USB"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_8
    iget-object v1, p0, Lcom/wssnps/c/b;->c:Landroid/content/Context;

    const-string v2, "com.sec.android.provider.snote"

    invoke-static {v1, v2}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 300
    iget-object v2, p0, Lcom/wssnps/c/b;->c:Landroid/content/Context;

    const-string v3, "com.sec.android.widgetapp.diotek.smemo"

    invoke-static {v2, v3}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 301
    iget-object v3, p0, Lcom/wssnps/c/b;->c:Landroid/content/Context;

    const-string v5, "com.sec.android.app.memo"

    invoke-static {v3, v5}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 302
    if-eqz v1, :cond_e

    if-eqz v2, :cond_e

    .line 303
    const-string v1, "SMemo2"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_9
    :goto_6
    iget-object v1, p0, Lcom/wssnps/c/b;->c:Landroid/content/Context;

    const-string v2, "com.samsung.android.snote"

    invoke-static {v1, v2}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 314
    if-eqz v1, :cond_a

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v1, :cond_a

    .line 315
    const-string v1, "SNote3"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    :cond_a
    iget-boolean v1, p0, Lcom/wssnps/c/b;->H:Z

    if-nez v1, :cond_b

    .line 319
    const-string v1, "BackupRestore"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v1, "Playlist"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_b
    iget-boolean v1, p0, Lcom/wssnps/c/b;->H:Z

    if-nez v1, :cond_c

    .line 325
    const-string v1, "FirmwareUpdate"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    :cond_c
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, ""

    const-string v3, "ConnectModes"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 328
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, ""

    const-string v3, "Deviceinfo"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 329
    iget-object v1, p0, Lcom/wssnps/c/b;->a:Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v1}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 331
    invoke-virtual {v4}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 262
    :cond_d
    const-string v1, "Desc"

    const-string v2, "Guest"

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_5

    .line 337
    :catch_1
    move-exception v1

    .line 339
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_2

    .line 304
    :cond_e
    if-eqz v1, :cond_f

    if-eqz v3, :cond_f

    .line 305
    :try_start_2
    const-string v1, "TMemo2"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_6

    .line 341
    :catch_2
    move-exception v1

    .line 343
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 306
    :cond_f
    if-eqz v2, :cond_10

    .line 307
    :try_start_3
    const-string v1, "SMemo"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 308
    :cond_10
    if-eqz v3, :cond_11

    .line 309
    const-string v1, "Memo"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 310
    :cond_11
    if-eqz v1, :cond_9

    .line 311
    const-string v1, "SNote"

    const-string v2, "value"

    const-string v3, "USB;WIFI"

    invoke-direct {p0, v1, v2, v3}, Lcom/wssnps/c/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_6
.end method
