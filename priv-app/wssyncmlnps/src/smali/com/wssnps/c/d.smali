.class public Lcom/wssnps/c/d;
.super Ljava/lang/Object;
.source "smlMakeManifest.java"

# interfaces
.implements Lcom/wssnps/c/a;


# instance fields
.field private a:Lorg/xmlpull/v1/XmlSerializer;

.field private b:Landroid/content/Context;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;IJ)V
    .locals 5

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/wssnps/c/d;->b:Landroid/content/Context;

    .line 37
    iput p2, p0, Lcom/wssnps/c/d;->c:I

    .line 39
    const-string v0, "1.0"

    iput-object v0, p0, Lcom/wssnps/c/d;->d:Ljava/lang/String;

    .line 40
    invoke-static {}, Lcom/wssnps/smlModelDefine;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/d;->e:Ljava/lang/String;

    .line 41
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/d;->g:Ljava/lang/String;

    .line 42
    invoke-static {}, Lcom/wssnps/smlModelDefine;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/d;->h:Ljava/lang/String;

    .line 43
    invoke-static {}, Lcom/wssnps/smlModelDefine;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/d;->i:Ljava/lang/String;

    .line 44
    invoke-static {}, Lcom/wssnps/smlModelDefine;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/d;->j:Ljava/lang/String;

    .line 46
    if-nez p2, :cond_0

    .line 47
    const-string v0, "2"

    iput-object v0, p0, Lcom/wssnps/c/d;->k:Ljava/lang/String;

    .line 53
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 54
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 55
    new-instance v3, Ljava/sql/Date;

    invoke-direct {v3, v0, v1}, Ljava/sql/Date;-><init>(J)V

    .line 56
    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/d;->f:Ljava/lang/String;

    .line 57
    sget-object v0, Lcom/wssnps/c/d;->cF:[[Ljava/lang/String;

    aget-object v0, v0, p2

    const/16 v1, 0xb

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/wssnps/c/d;->l:Ljava/lang/String;

    .line 58
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 49
    const-string v0, "3"

    iput-object v0, p0, Lcom/wssnps/c/d;->k:Ljava/lang/String;

    goto :goto_0

    .line 51
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/wssnps/c/d;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 132
    iget-object v0, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 133
    if-eqz p3, :cond_0

    .line 135
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 136
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 140
    :try_start_0
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const/4 v3, 0x1

    invoke-interface {v2, v1, v0, v3}, Lorg/xmlpull/v1/XmlSerializer;->text([CII)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :catch_0
    move-exception v2

    .line 144
    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "skip invalid character "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-char v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " !!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 145
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, "_"

    invoke-interface {v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_1

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, ""

    invoke-interface {v0, v1, p1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 155
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 63
    const/4 v0, 0x2

    const-string v1, "makeManifest"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 64
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 65
    invoke-static {}, Landroid/util/Xml;->newSerializer()Lorg/xmlpull/v1/XmlSerializer;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    .line 66
    const-string v0, ""

    .line 70
    :try_start_0
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v2, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/Writer;)V

    .line 71
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, "UTF-8"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 72
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, "http://xmlpull.org/v1/doc/features.html#indent-output"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 75
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "SamsungBnR_Manifest"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 77
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "Creation_Info"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 79
    const-string v2, "xml_VER"

    iget-object v3, p0, Lcom/wssnps/c/d;->d:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/wssnps/c/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 81
    const-string v2, "date"

    iget-object v3, p0, Lcom/wssnps/c/d;->f:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/wssnps/c/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 83
    const-string v2, "size"

    iget-object v3, p0, Lcom/wssnps/c/d;->g:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/wssnps/c/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 84
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "Creation_Info"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 87
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "Device_Info"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 89
    const-string v2, "name"

    iget-object v3, p0, Lcom/wssnps/c/d;->e:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/wssnps/c/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 91
    const-string v2, "platform_VER"

    iget-object v3, p0, Lcom/wssnps/c/d;->h:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/wssnps/c/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 93
    const-string v2, "build_VER"

    iget-object v3, p0, Lcom/wssnps/c/d;->i:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/wssnps/c/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 94
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "Device_Info"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 96
    iget-object v2, p0, Lcom/wssnps/c/d;->k:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/wssnps/c/d;->b:Landroid/content/Context;

    sget-object v3, Lcom/wssnps/c/d;->cF:[[Ljava/lang/String;

    iget v4, p0, Lcom/wssnps/c/d;->c:I

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-static {v2, v3}, Lcom/wssnps/smlModelDefine;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/wssnps/c/d;->k:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 101
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "Enc_Info"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 103
    const-string v2, "hk"

    iget-object v3, p0, Lcom/wssnps/c/d;->j:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/wssnps/c/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 105
    const-string v2, "target"

    iget-object v3, p0, Lcom/wssnps/c/d;->l:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/wssnps/c/d;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 106
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "Enc_Info"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 109
    :cond_0
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    const-string v3, ""

    const-string v4, "SamsungBnR_Manifest"

    invoke-interface {v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 110
    iget-object v2, p0, Lcom/wssnps/c/d;->a:Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 112
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    .line 114
    :catch_0
    move-exception v1

    .line 116
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 118
    :catch_1
    move-exception v1

    .line 120
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 122
    :catch_2
    move-exception v1

    .line 124
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
