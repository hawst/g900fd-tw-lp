.class public Lcom/wssnps/b;
.super Ljava/lang/Object;
.source "smlNpsHandler.java"

# interfaces
.implements Lcom/wssnps/a/b;
.implements Ljava/lang/Runnable;


# static fields
.field public static a:Z

.field public static b:I

.field public static c:I

.field public static d:I

.field public static e:I

.field public static f:I

.field public static g:J

.field public static h:I

.field public static i:I

.field public static j:Z

.field public static k:Ljava/lang/StringBuffer;

.field public static l:Ljava/lang/StringBuffer;

.field public static m:I

.field public static n:J

.field public static final o:Landroid/content/BroadcastReceiver;

.field static p:Ljava/lang/String;

.field public static q:I

.field public static r:Ljava/lang/String;

.field private static t:I

.field private static u:I

.field private static v:Z

.field private static w:I


# instance fields
.field private s:Landroid/net/LocalSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 77
    sput-boolean v1, Lcom/wssnps/b;->a:Z

    .line 86
    sput v1, Lcom/wssnps/b;->t:I

    .line 87
    sput v1, Lcom/wssnps/b;->u:I

    .line 91
    const/4 v0, 0x5

    sput v0, Lcom/wssnps/b;->b:I

    .line 92
    const/4 v0, 0x4

    sput v0, Lcom/wssnps/b;->c:I

    .line 93
    const/4 v0, 0x2

    sput v0, Lcom/wssnps/b;->d:I

    .line 94
    const/4 v0, 0x1

    sput v0, Lcom/wssnps/b;->e:I

    .line 95
    sput v1, Lcom/wssnps/b;->f:I

    .line 97
    sput-wide v4, Lcom/wssnps/b;->g:J

    .line 98
    sput v1, Lcom/wssnps/b;->h:I

    .line 99
    sput v1, Lcom/wssnps/b;->i:I

    .line 100
    sput-boolean v1, Lcom/wssnps/b;->j:Z

    .line 104
    sput-object v2, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    .line 105
    sput-object v2, Lcom/wssnps/b;->l:Ljava/lang/StringBuffer;

    .line 107
    sput v1, Lcom/wssnps/b;->m:I

    .line 4086
    sput-wide v4, Lcom/wssnps/b;->n:J

    .line 4843
    new-instance v0, Lcom/wssnps/d;

    invoke-direct {v0}, Lcom/wssnps/d;-><init>()V

    sput-object v0, Lcom/wssnps/b;->o:Landroid/content/BroadcastReceiver;

    .line 8143
    const/4 v0, -0x1

    sput v0, Lcom/wssnps/b;->w:I

    .line 8144
    sput-object v2, Lcom/wssnps/b;->p:Ljava/lang/String;

    .line 8464
    sput v1, Lcom/wssnps/b;->q:I

    return-void
.end method

.method public constructor <init>(Landroid/net/LocalSocket;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    .line 112
    return-void
.end method

.method public static D(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 7651
    .line 7652
    const/4 v0, 0x1

    const-string v1, "insertTask"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7653
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 7654
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7658
    :goto_0
    return-object v0

    .line 7656
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7657
    invoke-static {p0}, Lcom/wssnps/b/ag;->a(Ljava/lang/String;)Lcom/wssnps/b/ag;

    move-result-object v1

    .line 7658
    invoke-static {v0, v1, v2}, Lcom/wssnps/b/ag;->a(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static J(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2127
    const-string v0, "\""

    const-string v1, "_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2128
    const-string v1, "/"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2129
    const-string v1, ":"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2130
    const-string v1, "*"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2131
    const-string v1, "<"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2132
    const-string v1, ">"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2133
    const-string v1, "|"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2134
    const-string v1, "?"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2135
    const-string v1, ";"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2136
    const-string v1, "\\"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2138
    return-object v0
.end method

.method private static K(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2143
    const-string v0, ";"

    const-string v1, "_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2144
    return-object v0
.end method

.method private static L(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 2149
    const-string v0, "&amp;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_0

    const-string v0, "&apos;"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 2151
    :cond_0
    const-string v0, "&amp;"

    const-string v1, "||"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2152
    const-string v1, "&apos;"

    const-string v2, "//"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2154
    const-string v1, ";"

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2156
    const-string v1, "||"

    const-string v2, "&amp;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2157
    const-string v1, "//"

    const-string v2, "&apos;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2164
    :goto_0
    return-object v0

    .line 2161
    :cond_1
    const-string v0, ";"

    const-string v1, "_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static M(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2169
    const-string v1, ""

    .line 2171
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2172
    const-string v0, ""

    .line 2185
    :goto_0
    return-object v0

    .line 2174
    :cond_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 2175
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 2177
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 2178
    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2179
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 2175
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2182
    :cond_2
    if-eqz v2, :cond_3

    .line 2183
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private static N(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2192
    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2193
    const/4 v0, 0x1

    .line 2197
    :goto_0
    return v0

    .line 2195
    :catch_0
    move-exception v0

    .line 2197
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static O(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2203
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2205
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2207
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2208
    const/4 v3, 0x1

    if-ne v3, v1, :cond_1

    .line 2210
    const/16 v1, 0x2c

    .line 2216
    :cond_0
    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2212
    :cond_1
    const/4 v3, 0x2

    if-ne v3, v1, :cond_0

    .line 2214
    const/16 v1, 0xa

    goto :goto_1

    .line 2218
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private P(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2705
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2707
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2709
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2710
    const/16 v3, 0x2c

    if-ne v3, v1, :cond_1

    .line 2712
    const/4 v1, 0x1

    .line 2718
    :cond_0
    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2707
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2714
    :cond_1
    const/16 v3, 0xa

    if-ne v3, v1, :cond_0

    .line 2716
    const/4 v1, 0x2

    goto :goto_1

    .line 2720
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private Q(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2725
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2727
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2729
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 2730
    const/16 v3, 0x2c

    if-ne v3, v1, :cond_1

    .line 2732
    const/4 v1, 0x1

    .line 2738
    :cond_0
    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2727
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2734
    :cond_1
    const/16 v3, 0xd

    if-ne v3, v1, :cond_0

    .line 2736
    const/4 v1, 0x2

    goto :goto_1

    .line 2740
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static U()Ljava/lang/String;
    .locals 3

    .prologue
    .line 8553
    const-string v1, "0\n"

    .line 8555
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 8556
    if-nez v0, :cond_0

    .line 8557
    const-string v0, "1\n"

    .line 8560
    :goto_0
    return-object v0

    .line 8559
    :cond_0
    const-string v2, "download"

    invoke-virtual {v0, v2}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    move-object v0, v1

    .line 8560
    goto :goto_0
.end method

.method private static a(J)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x400

    .line 2801
    cmp-long v0, p0, v2

    if-ltz v0, :cond_0

    .line 2802
    div-long/2addr p0, v2

    .line 2803
    :cond_0
    return-wide p0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 2223
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 2224
    const-string v2, ""

    .line 2225
    const-string v3, "content://sms/"

    .line 2226
    const-string v4, ""

    .line 2227
    const-string v5, ""

    .line 2228
    const/4 v2, 0x0

    .line 2230
    const-string v6, "VZW"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {}, Lcom/wssnps/smlModelDefine;->C()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2231
    const-string v2, "0\n"

    .line 2408
    :goto_0
    return-object v2

    .line 2233
    :cond_0
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2234
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 2235
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 2236
    const/4 v3, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CreateSmsItem "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2245
    sparse-switch p0, :sswitch_data_0

    .line 2270
    :goto_1
    :sswitch_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 2274
    :try_start_0
    new-instance v3, Ljava/lang/String;

    const-string v8, "ISO-8859-1"

    invoke-virtual {p3, v8}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2280
    :goto_2
    const-string v4, "body"

    invoke-virtual {v7, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2283
    :cond_1
    const-string v3, "0"

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2285
    const-string v3, "read"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2286
    const-string v3, "locked"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2304
    :cond_2
    :goto_3
    const/16 v3, 0x1a7

    if-ne p0, v3, :cond_a

    .line 2306
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_b

    .line 2310
    :try_start_1
    new-instance v3, Ljava/lang/String;

    const-string v4, "ISO-8859-1"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2333
    :goto_4
    const-string v4, "address"

    invoke-virtual {v7, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2335
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v4, v3}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2336
    const-string v4, "thread_id"

    invoke-virtual {v7, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2338
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2340
    invoke-static/range {p7 .. p7}, Lcom/wssnps/b;->M(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2341
    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    .line 2342
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 2343
    const-string v4, "date"

    invoke-virtual {v7, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346
    :cond_3
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2348
    invoke-static/range {p8 .. p8}, Lcom/wssnps/b;->M(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2349
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2350
    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    .line 2352
    const/16 v3, 0x1a7

    if-ne p0, v3, :cond_c

    .line 2354
    const-string v3, "reserved"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2378
    :cond_4
    :goto_5
    const-string v3, "type"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2380
    invoke-virtual/range {p10 .. p10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2382
    invoke-virtual/range {p10 .. p10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2383
    const-string v3, "sim_slot"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2386
    :cond_5
    invoke-static/range {p11 .. p11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2388
    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2389
    const-string v3, "seen"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2392
    :cond_6
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_f

    .line 2393
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2248
    :sswitch_1
    const/4 v2, 0x2

    .line 2249
    goto/16 :goto_1

    .line 2252
    :sswitch_2
    const/4 v2, 0x1

    .line 2253
    goto/16 :goto_1

    .line 2256
    :sswitch_3
    const/4 v2, 0x4

    .line 2257
    goto/16 :goto_1

    .line 2260
    :sswitch_4
    const/4 v2, 0x6

    .line 2261
    goto/16 :goto_1

    .line 2276
    :catch_0
    move-exception v3

    .line 2278
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v3, v4

    goto/16 :goto_2

    .line 2288
    :cond_7
    const-string v3, "1"

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2290
    const-string v3, "read"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2291
    const-string v3, "locked"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    .line 2293
    :cond_8
    const-string v3, "4"

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2295
    const-string v3, "read"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2296
    const-string v3, "locked"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    .line 2298
    :cond_9
    const-string v3, "5"

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2300
    const-string v3, "read"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2301
    const-string v3, "locked"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    .line 2312
    :catch_1
    move-exception v3

    .line 2314
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v3, v5

    .line 2315
    goto/16 :goto_4

    .line 2320
    :cond_a
    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_b

    .line 2324
    :try_start_2
    new-instance v3, Ljava/lang/String;

    const-string v4, "ISO-8859-1"

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_4

    .line 2326
    :catch_2
    move-exception v3

    .line 2328
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :cond_b
    move-object v3, v5

    goto/16 :goto_4

    .line 2358
    :cond_c
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 2360
    cmp-long v3, v8, v4

    if-gtz v3, :cond_d

    .line 2362
    const-string v2, "reserved"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2363
    const/4 v2, 0x2

    goto/16 :goto_5

    .line 2367
    :cond_d
    const-string v3, "reserved"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    .line 2372
    :cond_e
    const-string v2, "reserved"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2373
    const/4 v2, 0x2

    goto/16 :goto_5

    .line 2395
    :cond_f
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2396
    invoke-virtual {v2, v6, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 2397
    if-eqz v2, :cond_10

    .line 2399
    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2400
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2404
    :cond_10
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2245
    nop

    :sswitch_data_0
    .sparse-switch
        0x1a4 -> :sswitch_1
        0x1a7 -> :sswitch_2
        0x1aa -> :sswitch_3
        0x1ad -> :sswitch_4
        0x1b0 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(I[Ljava/lang/String;)Ljava/lang/String;
    .locals 38

    .prologue
    .line 4090
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "2\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 4091
    const-string v34, ""

    .line 4092
    const-string v35, ""

    .line 4093
    const-string v33, ""

    .line 4094
    const-string v32, ""

    .line 4095
    const/16 v36, 0x0

    .line 4096
    const/4 v4, 0x0

    .line 4099
    const/16 v31, 0x0

    .line 4100
    const/16 v30, 0x0

    .line 4101
    const/16 v29, 0x0

    .line 4102
    const/16 v28, 0x0

    .line 4103
    const/16 v27, 0x0

    .line 4104
    const/16 v26, 0x0

    .line 4105
    const/16 v25, 0x0

    .line 4106
    const/16 v24, 0x0

    .line 4107
    const/16 v23, 0x0

    .line 4108
    const/16 v22, 0x0

    .line 4109
    const/16 v21, 0x0

    .line 4110
    const/16 v20, 0x0

    .line 4111
    const/16 v19, 0x0

    .line 4112
    const/16 v18, 0x0

    .line 4113
    const/16 v17, 0x0

    .line 4114
    const/16 v16, 0x0

    .line 4115
    const/4 v15, 0x0

    .line 4116
    const/4 v14, 0x0

    .line 4117
    const/4 v13, 0x0

    .line 4118
    const/4 v12, 0x0

    .line 4119
    const/4 v11, 0x0

    .line 4121
    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CreateMmsItem "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 4123
    const-string v5, "VZW"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/wssnps/a/c;->G:Lcom/wssnps/a/c;

    invoke-virtual {v5}, Lcom/wssnps/a/c;->ordinal()I

    move-result v5

    aget-object v5, p1, v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {}, Lcom/wssnps/smlModelDefine;->C()Z

    move-result v5

    if-nez v5, :cond_0

    .line 4125
    const-string v5, "0"

    sget-object v6, Lcom/wssnps/a/c;->G:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4126
    const-string v4, "0\n"

    .line 4619
    :goto_0
    return-object v4

    .line 4130
    :cond_0
    const-string v5, "content://mms/"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 4132
    packed-switch p0, :pswitch_data_0

    move v10, v4

    .line 4150
    :goto_1
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v4, :cond_1

    .line 4151
    const-string v4, ""

    goto :goto_0

    .line 4135
    :pswitch_0
    const/4 v4, 0x2

    move v10, v4

    .line 4136
    goto :goto_1

    .line 4138
    :pswitch_1
    const/4 v4, 0x1

    move v10, v4

    .line 4139
    goto :goto_1

    .line 4141
    :pswitch_2
    const/4 v4, 0x3

    move v10, v4

    .line 4142
    goto :goto_1

    .line 4144
    :pswitch_3
    const/4 v4, 0x4

    move v10, v4

    .line 4145
    goto :goto_1

    .line 4153
    :cond_1
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 4154
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v37

    .line 4155
    if-eqz v37, :cond_2f

    .line 4157
    const-string v6, "msg_box"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 4158
    const-string v6, "m_id"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 4159
    const-string v6, "sub"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 4160
    const-string v6, "ct_t"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 4161
    const-string v6, "tr_id"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 4162
    const-string v6, "date"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 4163
    const-string v6, "read"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 4164
    const-string v6, "date_sent"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 4165
    const-string v6, "exp"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 4166
    const-string v6, "locked"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 4167
    const-string v6, "m_type"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 4168
    const-string v6, "seen"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 4169
    const-string v6, "thread_id"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 4170
    const-string v6, "rr"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 4171
    const-string v6, "d_rpt"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 4172
    const-string v6, "resp_st"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 4173
    const-string v6, "retr_st"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 4174
    const-string v6, "m_cls"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 4175
    const-string v6, "pri"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 4176
    const-string v6, "sim_slot"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 4177
    const-string v6, "reserved"

    move-object/from16 v0, v37

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 4178
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->close()V

    .line 4182
    :goto_2
    new-instance v29, Ljava/util/HashSet;

    invoke-direct/range {v29 .. v29}, Ljava/util/HashSet;-><init>()V

    .line 4183
    sget-object v28, Lcom/wssnps/a/c;->e:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v28

    if-nez v28, :cond_10

    sget-object v28, Lcom/wssnps/a/c;->d:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v28

    if-nez v28, :cond_10

    .line 4185
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v10, v0, :cond_2

    .line 4187
    sget-object v28, Lcom/wssnps/a/c;->d:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    const-string v30, ","

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 4188
    if-eqz v30, :cond_3

    .line 4190
    const/16 v28, 0x0

    :goto_3
    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v28

    move/from16 v1, v31

    if-ge v0, v1, :cond_3

    .line 4192
    aget-object v31, v30, v28

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4190
    add-int/lit8 v28, v28, 0x1

    goto :goto_3

    .line 4198
    :cond_2
    sget-object v28, Lcom/wssnps/a/c;->e:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    const-string v30, ","

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 4199
    if-eqz v30, :cond_3

    .line 4201
    const/16 v28, 0x0

    :goto_4
    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v28

    move/from16 v1, v31

    if-ge v0, v1, :cond_3

    .line 4203
    aget-object v31, v30, v28

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4201
    add-int/lit8 v28, v28, 0x1

    goto :goto_4

    .line 4208
    :cond_3
    sget-object v28, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static/range {v28 .. v29}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v28

    sput-wide v28, Lcom/wssnps/b;->n:J

    .line 4238
    :cond_4
    :goto_5
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 4239
    invoke-virtual/range {v29 .. v29}, Landroid/content/ContentValues;->clear()V

    .line 4242
    sget-object v28, Lcom/wssnps/a/c;->H:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v28

    if-nez v28, :cond_5

    .line 4246
    :try_start_0
    new-instance v28, Ljava/lang/String;

    sget-object v30, Lcom/wssnps/a/c;->H:Lcom/wssnps/a/c;

    invoke-virtual/range {v30 .. v30}, Lcom/wssnps/a/c;->ordinal()I

    move-result v30

    aget-object v30, p1, v30

    const-string v31, "ISO-8859-1"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4253
    :goto_6
    const/16 v30, -0x1

    move/from16 v0, v26

    move/from16 v1, v30

    if-eq v0, v1, :cond_5

    .line 4254
    const-string v26, "m_id"

    move-object/from16 v0, v29

    move-object/from16 v1, v26

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4258
    :cond_5
    sget-object v26, Lcom/wssnps/a/c;->c:Lcom/wssnps/a/c;

    invoke-virtual/range {v26 .. v26}, Lcom/wssnps/a/c;->ordinal()I

    move-result v26

    aget-object v26, p1, v26

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_6

    .line 4262
    :try_start_1
    new-instance v26, Ljava/lang/String;

    sget-object v28, Lcom/wssnps/a/c;->c:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    const-string v30, "ISO-8859-1"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v28

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 4269
    :goto_7
    const/16 v28, -0x1

    move/from16 v0, v25

    move/from16 v1, v28

    if-eq v0, v1, :cond_6

    .line 4270
    const-string v25, "sub"

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4274
    :cond_6
    sget-object v25, Lcom/wssnps/a/c;->r:Lcom/wssnps/a/c;

    invoke-virtual/range {v25 .. v25}, Lcom/wssnps/a/c;->ordinal()I

    move-result v25

    aget-object v25, p1, v25

    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_7

    .line 4278
    :try_start_2
    new-instance v25, Ljava/lang/String;

    sget-object v26, Lcom/wssnps/a/c;->r:Lcom/wssnps/a/c;

    invoke-virtual/range {v26 .. v26}, Lcom/wssnps/a/c;->ordinal()I

    move-result v26

    aget-object v26, p1, v26

    const-string v28, "ISO-8859-1"

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 4285
    :goto_8
    const/16 v26, -0x1

    move/from16 v0, v24

    move/from16 v1, v26

    if-eq v0, v1, :cond_7

    .line 4286
    const-string v24, "ct_t"

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v29

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4290
    :cond_7
    sget-object v24, Lcom/wssnps/a/c;->I:Lcom/wssnps/a/c;

    invoke-virtual/range {v24 .. v24}, Lcom/wssnps/a/c;->ordinal()I

    move-result v24

    aget-object v24, p1, v24

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_8

    .line 4294
    :try_start_3
    new-instance v24, Ljava/lang/String;

    sget-object v25, Lcom/wssnps/a/c;->I:Lcom/wssnps/a/c;

    invoke-virtual/range {v25 .. v25}, Lcom/wssnps/a/c;->ordinal()I

    move-result v25

    aget-object v25, p1, v25

    const-string v26, "ISO-8859-1"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/String;-><init>([B)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_3

    .line 4301
    :goto_9
    const/16 v25, -0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    .line 4302
    const-string v23, "tr_id"

    move-object/from16 v0, v29

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4306
    :cond_8
    sget-object v23, Lcom/wssnps/a/c;->f:Lcom/wssnps/a/c;

    invoke-virtual/range {v23 .. v23}, Lcom/wssnps/a/c;->ordinal()I

    move-result v23

    aget-object v23, p1, v23

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_9

    .line 4308
    sget-object v23, Lcom/wssnps/a/c;->f:Lcom/wssnps/a/c;

    invoke-virtual/range {v23 .. v23}, Lcom/wssnps/a/c;->ordinal()I

    move-result v23

    aget-object v23, p1, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Integer;->intValue()I

    move-result v23

    .line 4309
    const/16 v24, -0x1

    move/from16 v0, v22

    move/from16 v1, v24

    if-eq v0, v1, :cond_9

    .line 4310
    const-string v22, "date"

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4314
    :cond_9
    sget-object v22, Lcom/wssnps/a/c;->G:Lcom/wssnps/a/c;

    invoke-virtual/range {v22 .. v22}, Lcom/wssnps/a/c;->ordinal()I

    move-result v22

    aget-object v22, p1, v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_a

    .line 4316
    sget-object v22, Lcom/wssnps/a/c;->G:Lcom/wssnps/a/c;

    invoke-virtual/range {v22 .. v22}, Lcom/wssnps/a/c;->ordinal()I

    move-result v22

    aget-object v22, p1, v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 4317
    const/16 v23, -0x1

    move/from16 v0, v23

    if-eq v6, v0, :cond_a

    .line 4319
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 4320
    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    const-wide/16 v30, 0x3e8

    mul-long v22, v22, v30

    .line 4322
    const/16 v6, 0x1e0

    move/from16 v0, p0

    if-ne v0, v6, :cond_14

    .line 4324
    const-string v6, "reserved"

    const/16 v22, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4342
    :cond_a
    :goto_a
    const/4 v6, -0x1

    move/from16 v0, v27

    if-eq v0, v6, :cond_b

    .line 4343
    const-string v6, "msg_box"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4346
    :cond_b
    sget-object v6, Lcom/wssnps/a/c;->g:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_c

    .line 4348
    sget-object v6, Lcom/wssnps/a/c;->g:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4349
    if-ltz v6, :cond_c

    const/4 v10, -0x1

    move/from16 v0, v21

    if-eq v0, v10, :cond_c

    .line 4350
    const-string v10, "read"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4354
    :cond_c
    sget-object v6, Lcom/wssnps/a/c;->h:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_d

    .line 4356
    sget-object v6, Lcom/wssnps/a/c;->h:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4357
    const/4 v10, -0x1

    move/from16 v0, v20

    if-eq v0, v10, :cond_d

    .line 4358
    const-string v10, "date_sent"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4362
    :cond_d
    sget-object v6, Lcom/wssnps/a/c;->s:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e

    .line 4364
    sget-object v6, Lcom/wssnps/a/c;->s:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4365
    const/4 v10, -0x1

    move/from16 v0, v19

    if-eq v0, v10, :cond_e

    .line 4366
    const-string v10, "exp"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4370
    :cond_e
    sget-object v6, Lcom/wssnps/a/c;->t:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_f

    .line 4372
    sget-object v6, Lcom/wssnps/a/c;->t:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4373
    const/4 v10, -0x1

    move/from16 v0, v18

    if-eq v0, v10, :cond_f

    .line 4374
    const-string v10, "locked"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4378
    :cond_f
    sget-object v6, Lcom/wssnps/a/c;->u:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_17

    .line 4380
    sget-object v6, Lcom/wssnps/a/c;->u:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4381
    const/16 v10, 0x86

    if-ne v6, v10, :cond_16

    .line 4382
    const-string v4, ""

    goto/16 :goto_0

    .line 4210
    :cond_10
    sget-object v28, Lcom/wssnps/a/c;->d:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v28

    if-nez v28, :cond_12

    .line 4212
    sget-object v28, Lcom/wssnps/a/c;->d:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    const-string v30, ","

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 4213
    if-eqz v30, :cond_11

    .line 4215
    const/16 v28, 0x0

    :goto_b
    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v28

    move/from16 v1, v31

    if-ge v0, v1, :cond_11

    .line 4217
    aget-object v31, v30, v28

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4215
    add-int/lit8 v28, v28, 0x1

    goto :goto_b

    .line 4221
    :cond_11
    sget-object v28, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static/range {v28 .. v29}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v28

    sput-wide v28, Lcom/wssnps/b;->n:J

    goto/16 :goto_5

    .line 4223
    :cond_12
    sget-object v28, Lcom/wssnps/a/c;->e:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v28

    if-nez v28, :cond_4

    .line 4225
    sget-object v28, Lcom/wssnps/a/c;->e:Lcom/wssnps/a/c;

    invoke-virtual/range {v28 .. v28}, Lcom/wssnps/a/c;->ordinal()I

    move-result v28

    aget-object v28, p1, v28

    const-string v30, ","

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 4226
    if-eqz v30, :cond_13

    .line 4228
    const/16 v28, 0x0

    :goto_c
    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v31, v0

    move/from16 v0, v28

    move/from16 v1, v31

    if-ge v0, v1, :cond_13

    .line 4230
    aget-object v31, v30, v28

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 4228
    add-int/lit8 v28, v28, 0x1

    goto :goto_c

    .line 4234
    :cond_13
    sget-object v28, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static/range {v28 .. v29}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v28

    sput-wide v28, Lcom/wssnps/b;->n:J

    goto/16 :goto_5

    .line 4248
    :catch_0
    move-exception v28

    .line 4250
    invoke-virtual/range {v28 .. v28}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object/from16 v28, v35

    goto/16 :goto_6

    .line 4264
    :catch_1
    move-exception v26

    .line 4266
    invoke-virtual/range {v26 .. v26}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object/from16 v26, v34

    goto/16 :goto_7

    .line 4280
    :catch_2
    move-exception v25

    .line 4282
    invoke-virtual/range {v25 .. v25}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object/from16 v25, v33

    goto/16 :goto_8

    .line 4296
    :catch_3
    move-exception v24

    .line 4298
    invoke-virtual/range {v24 .. v24}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object/from16 v24, v32

    goto/16 :goto_9

    .line 4328
    :cond_14
    cmp-long v6, v22, v24

    if-gtz v6, :cond_15

    .line 4330
    const-string v6, "reserved"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4331
    const/4 v10, 0x2

    goto/16 :goto_a

    .line 4335
    :cond_15
    const-string v6, "reserved"

    const/16 v22, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_a

    .line 4384
    :cond_16
    const/4 v10, -0x1

    move/from16 v0, v17

    if-eq v0, v10, :cond_17

    .line 4386
    const/16 v10, 0x80

    if-ne v6, v10, :cond_21

    .line 4387
    const-string v6, "m_type"

    const/16 v10, 0x80

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4396
    :cond_17
    :goto_d
    sget-object v6, Lcom/wssnps/a/c;->x:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_18

    .line 4398
    sget-object v6, Lcom/wssnps/a/c;->x:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4399
    const/4 v10, -0x1

    move/from16 v0, v16

    if-eq v0, v10, :cond_18

    .line 4400
    const-string v10, "seen"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4404
    :cond_18
    sget-wide v16, Lcom/wssnps/b;->n:J

    const-wide/16 v18, 0x0

    cmp-long v6, v16, v18

    if-lez v6, :cond_19

    const/4 v6, -0x1

    if-eq v15, v6, :cond_19

    .line 4406
    const-string v6, "thread_id"

    sget-wide v16, Lcom/wssnps/b;->n:J

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4410
    :cond_19
    sget-object v6, Lcom/wssnps/a/c;->z:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1a

    const/4 v6, -0x1

    if-eq v7, v6, :cond_1a

    .line 4412
    sget-object v6, Lcom/wssnps/a/c;->z:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4413
    const-string v7, "sim_slot"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v0, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4417
    :cond_1a
    sget-object v6, Lcom/wssnps/a/c;->A:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1b

    .line 4419
    sget-object v6, Lcom/wssnps/a/c;->A:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4420
    const/4 v7, -0x1

    if-eq v14, v7, :cond_1b

    .line 4422
    if-nez v6, :cond_23

    .line 4423
    const-string v6, "rr"

    const/16 v7, 0x80

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4430
    :cond_1b
    :goto_e
    sget-object v6, Lcom/wssnps/a/c;->B:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1c

    .line 4432
    sget-object v6, Lcom/wssnps/a/c;->B:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4433
    const/4 v7, -0x1

    if-eq v13, v7, :cond_1c

    .line 4435
    if-nez v6, :cond_24

    .line 4436
    const-string v6, "d_rpt"

    const/16 v7, 0x80

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4445
    :cond_1c
    :goto_f
    sget-object v6, Lcom/wssnps/a/c;->v:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1d

    .line 4447
    sget-object v6, Lcom/wssnps/a/c;->v:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4448
    const/4 v7, -0x1

    if-eq v12, v7, :cond_1d

    .line 4450
    sparse-switch v6, :sswitch_data_0

    .line 4538
    :cond_1d
    :goto_10
    sget-object v6, Lcom/wssnps/a/c;->w:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1e

    .line 4540
    sget-object v6, Lcom/wssnps/a/c;->w:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4541
    const/4 v7, -0x1

    if-eq v11, v7, :cond_1e

    .line 4543
    sparse-switch v6, :sswitch_data_1

    .line 4562
    :cond_1e
    :goto_11
    sget-object v6, Lcom/wssnps/a/c;->C:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1f

    .line 4564
    sget-object v6, Lcom/wssnps/a/c;->C:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4565
    const/4 v7, -0x1

    if-eq v9, v7, :cond_1f

    .line 4567
    const/4 v7, 0x1

    if-ne v6, v7, :cond_26

    .line 4568
    const-string v6, "m_cls"

    const-string v7, "personal"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4581
    :cond_1f
    :goto_12
    sget-object v6, Lcom/wssnps/a/c;->D:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_20

    const-string v6, "VZW"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_20

    .line 4583
    sget-object v6, Lcom/wssnps/a/c;->D:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 4584
    const/4 v7, -0x1

    if-eq v8, v7, :cond_20

    .line 4586
    const/4 v7, 0x1

    if-ne v6, v7, :cond_2a

    .line 4587
    const-string v6, "pri"

    const/16 v7, 0x80

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4597
    :cond_20
    :goto_13
    move-object/from16 v0, v29

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v5

    .line 4598
    if-eqz v5, :cond_2e

    .line 4600
    invoke-virtual {v5}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    .line 4601
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2d

    .line 4603
    move-object/from16 v0, p1

    invoke-static {v4, v5, v0}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Ljava/lang/String;[Ljava/lang/String;)V

    .line 4604
    sget-object v6, Lcom/wssnps/a/c;->d:Lcom/wssnps/a/c;

    invoke-virtual {v6}, Lcom/wssnps/a/c;->ordinal()I

    move-result v6

    aget-object v6, p1, v6

    sget-object v7, Lcom/wssnps/a/c;->e:Lcom/wssnps/a/c;

    invoke-virtual {v7}, Lcom/wssnps/a/c;->ordinal()I

    move-result v7

    aget-object v7, p1, v7

    invoke-static {v4, v5, v6, v7}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4606
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 4388
    :cond_21
    const/16 v10, 0x84

    if-ne v6, v10, :cond_22

    .line 4389
    const-string v6, "m_type"

    const/16 v10, 0x84

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_d

    .line 4391
    :cond_22
    const-string v6, "m_type"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_d

    .line 4425
    :cond_23
    const-string v6, "rr"

    const/16 v7, 0x81

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_e

    .line 4437
    :cond_24
    const/4 v7, 0x1

    if-ne v6, v7, :cond_25

    .line 4438
    const-string v6, "d_rpt"

    const/16 v7, 0x81

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_f

    .line 4440
    :cond_25
    const-string v6, "d_rpt"

    move-object/from16 v0, v29

    move-object/from16 v1, v36

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    .line 4453
    :sswitch_0
    const-string v6, "resp_st"

    const/16 v7, 0x80

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4456
    :sswitch_1
    const-string v6, "resp_st"

    const/16 v7, 0x81

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4459
    :sswitch_2
    const-string v6, "resp_st"

    const/16 v7, 0x82

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4462
    :sswitch_3
    const-string v6, "resp_st"

    const/16 v7, 0x83

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4465
    :sswitch_4
    const-string v6, "resp_st"

    const/16 v7, 0x84

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4468
    :sswitch_5
    const-string v6, "resp_st"

    const/16 v7, 0x85

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4471
    :sswitch_6
    const-string v6, "resp_st"

    const/16 v7, 0x86

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4474
    :sswitch_7
    const-string v6, "resp_st"

    const/16 v7, 0x87

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4477
    :sswitch_8
    const-string v6, "resp_st"

    const/16 v7, 0x88

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4480
    :sswitch_9
    const-string v6, "resp_st"

    const/16 v7, 0xc0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4483
    :sswitch_a
    const-string v6, "resp_st"

    const/16 v7, 0xc1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4486
    :sswitch_b
    const-string v6, "resp_st"

    const/16 v7, 0xc2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4489
    :sswitch_c
    const-string v6, "resp_st"

    const/16 v7, 0xc3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4492
    :sswitch_d
    const-string v6, "resp_st"

    const/16 v7, 0xc4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4495
    :sswitch_e
    const-string v6, "resp_st"

    const/16 v7, 0xe0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4498
    :sswitch_f
    const-string v6, "resp_st"

    const/16 v7, 0xe1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4501
    :sswitch_10
    const-string v6, "resp_st"

    const/16 v7, 0xe2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4504
    :sswitch_11
    const-string v6, "resp_st"

    const/16 v7, 0xe3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4507
    :sswitch_12
    const-string v6, "resp_st"

    const/16 v7, 0xe4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4510
    :sswitch_13
    const-string v6, "resp_st"

    const/16 v7, 0xe5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4513
    :sswitch_14
    const-string v6, "resp_st"

    const/16 v7, 0xe6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4516
    :sswitch_15
    const-string v6, "resp_st"

    const/16 v7, 0xe7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4519
    :sswitch_16
    const-string v6, "resp_st"

    const/16 v7, 0xe8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4522
    :sswitch_17
    const-string v6, "resp_st"

    const/16 v7, 0xe9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4525
    :sswitch_18
    const-string v6, "resp_st"

    const/16 v7, 0xea

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4528
    :sswitch_19
    const-string v6, "resp_st"

    const/16 v7, 0xeb

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4531
    :sswitch_1a
    const-string v6, "resp_st"

    const/16 v7, 0xff

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_10

    .line 4546
    :sswitch_1b
    const-string v6, "retr_st"

    const/16 v7, 0x80

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_11

    .line 4549
    :sswitch_1c
    const-string v6, "retr_st"

    const/16 v7, 0xc0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_11

    .line 4552
    :sswitch_1d
    const-string v6, "retr_st"

    const/16 v7, 0x82

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_11

    .line 4555
    :sswitch_1e
    const-string v6, "retr_st"

    const/16 v7, 0x83

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_11

    .line 4569
    :cond_26
    const/4 v7, 0x2

    if-ne v6, v7, :cond_27

    .line 4570
    const-string v6, "m_cls"

    const-string v7, "advertisement"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 4571
    :cond_27
    const/4 v7, 0x3

    if-ne v6, v7, :cond_28

    .line 4572
    const-string v6, "m_cls"

    const-string v7, "informational"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 4573
    :cond_28
    const/4 v7, 0x4

    if-ne v6, v7, :cond_29

    .line 4574
    const-string v6, "m_cls"

    const-string v7, "auto"

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 4576
    :cond_29
    const-string v6, "m_cls"

    move-object/from16 v0, v29

    move-object/from16 v1, v36

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 4588
    :cond_2a
    const/4 v7, 0x2

    if-ne v6, v7, :cond_2b

    .line 4589
    const-string v6, "pri"

    const/16 v7, 0x81

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_13

    .line 4590
    :cond_2b
    const/4 v7, 0x3

    if-ne v6, v7, :cond_2c

    .line 4591
    const-string v6, "pri"

    const/16 v7, 0x82

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_13

    .line 4593
    :cond_2c
    const-string v6, "pri"

    move-object/from16 v0, v29

    move-object/from16 v1, v36

    invoke-virtual {v0, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_13

    .line 4610
    :cond_2d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "2\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 4615
    :cond_2e
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "2\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    :cond_2f
    move v6, v11

    move v7, v12

    move v8, v13

    move v9, v14

    move v11, v15

    move/from16 v12, v16

    move/from16 v13, v17

    move/from16 v14, v18

    move/from16 v15, v19

    move/from16 v16, v20

    move/from16 v17, v21

    move/from16 v18, v22

    move/from16 v19, v23

    move/from16 v20, v24

    move/from16 v21, v25

    move/from16 v22, v26

    move/from16 v23, v27

    move/from16 v24, v28

    move/from16 v25, v29

    move/from16 v26, v30

    move/from16 v27, v31

    goto/16 :goto_2

    .line 4132
    nop

    :pswitch_data_0
    .packed-switch 0x1df
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 4450
    :sswitch_data_0
    .sparse-switch
        0x80 -> :sswitch_0
        0x81 -> :sswitch_1
        0x82 -> :sswitch_2
        0x83 -> :sswitch_3
        0x84 -> :sswitch_4
        0x85 -> :sswitch_5
        0x86 -> :sswitch_6
        0x87 -> :sswitch_7
        0x88 -> :sswitch_8
        0xc0 -> :sswitch_9
        0xc1 -> :sswitch_a
        0xc2 -> :sswitch_b
        0xc3 -> :sswitch_c
        0xc4 -> :sswitch_d
        0xe0 -> :sswitch_e
        0xe1 -> :sswitch_f
        0xe2 -> :sswitch_10
        0xe3 -> :sswitch_11
        0xe4 -> :sswitch_12
        0xe5 -> :sswitch_13
        0xe6 -> :sswitch_14
        0xe7 -> :sswitch_15
        0xe8 -> :sswitch_16
        0xe9 -> :sswitch_17
        0xea -> :sswitch_18
        0xeb -> :sswitch_19
        0xff -> :sswitch_1a
    .end sparse-switch

    .line 4543
    :sswitch_data_1
    .sparse-switch
        0x80 -> :sswitch_1b
        0xc0 -> :sswitch_1c
        0xc1 -> :sswitch_1d
        0xc2 -> :sswitch_1e
    .end sparse-switch
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 4858
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 4859
    return-void
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const v0, 0xfa000

    const/4 v4, 0x1

    .line 4730
    .line 4735
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_uri "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 4738
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 4740
    :try_start_1
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4741
    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 4743
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->available()I

    move-result v2

    .line 4744
    if-le v2, v0, :cond_c

    .line 4747
    :goto_0
    new-array v2, v0, [B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4750
    :cond_0
    const/4 v4, 0x0

    :try_start_3
    invoke-virtual {v3, v2, v4, v0}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 4752
    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5, v4}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 4753
    if-nez v4, :cond_0

    .line 4757
    :cond_1
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    .line 4758
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 4759
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4769
    if-eqz v3, :cond_2

    .line 4770
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 4772
    :cond_2
    if-eqz v1, :cond_3

    .line 4773
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 4783
    :cond_3
    :goto_1
    if-eqz v3, :cond_4

    .line 4787
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 4796
    :cond_4
    :goto_2
    if-eqz v1, :cond_5

    .line 4800
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 4810
    :cond_5
    :goto_3
    invoke-static {p0, p1}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 4811
    return-void

    .line 4762
    :catch_0
    move-exception v0

    .line 4765
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 4769
    if-eqz v3, :cond_6

    .line 4770
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 4772
    :cond_6
    if-eqz v1, :cond_3

    .line 4773
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    .line 4776
    :catch_1
    move-exception v0

    move-object v2, v3

    .line 4778
    :goto_4
    :try_start_9
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    .line 4779
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ATTACH_FILE_DATA "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 4783
    if-eqz v2, :cond_7

    .line 4787
    :try_start_a
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 4796
    :cond_7
    :goto_5
    if-eqz v1, :cond_5

    .line 4800
    :try_start_b
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    goto :goto_3

    .line 4802
    :catch_2
    move-exception v0

    .line 4805
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 4769
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_8

    .line 4770
    :try_start_c
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 4772
    :cond_8
    if-eqz v1, :cond_9

    .line 4773
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    :cond_9
    throw v0
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 4783
    :catchall_1
    move-exception v0

    move-object v2, v3

    :goto_6
    if-eqz v2, :cond_a

    .line 4787
    :try_start_d
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    .line 4796
    :cond_a
    :goto_7
    if-eqz v1, :cond_b

    .line 4800
    :try_start_e
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    .line 4806
    :cond_b
    :goto_8
    throw v0

    .line 4789
    :catch_3
    move-exception v0

    .line 4792
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 4802
    :catch_4
    move-exception v0

    .line 4805
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 4789
    :catch_5
    move-exception v0

    .line 4792
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 4789
    :catch_6
    move-exception v2

    .line 4792
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 4802
    :catch_7
    move-exception v1

    .line 4805
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 4783
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :catchall_3
    move-exception v0

    goto :goto_6

    .line 4776
    :catch_8
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :catch_9
    move-exception v0

    goto :goto_4

    :cond_c
    move v0, v2

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15

    .prologue
    .line 3971
    const/16 v11, 0x89

    .line 3972
    const/16 v12, 0x97

    .line 3974
    const-string v13, "insert-address-token"

    .line 3977
    const/4 v10, 0x0

    .line 3978
    const/4 v9, 0x0

    .line 3979
    const/4 v8, 0x0

    .line 3980
    const/4 v7, 0x0

    .line 3983
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://mms/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/addr"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3985
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3987
    if-eqz v6, :cond_16

    .line 3989
    const-string v1, "msg_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 3990
    const-string v1, "address"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 3991
    const-string v1, "type"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 3992
    const-string v1, "charset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 3994
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3997
    :goto_0
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 3998
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_8

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_8

    .line 4000
    const-string v6, ","

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 4001
    array-length v9, v8

    .line 4003
    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 4004
    const-string v6, "msg_id"

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4005
    :cond_0
    const/4 v6, -0x1

    if-eq v4, v6, :cond_1

    .line 4006
    const-string v6, "address"

    move-object/from16 v0, p2

    invoke-virtual {v7, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4007
    :cond_1
    const/4 v6, -0x1

    if-eq v3, v6, :cond_2

    .line 4008
    const-string v6, "type"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4009
    :cond_2
    const/4 v6, -0x1

    if-eq v1, v6, :cond_3

    .line 4010
    const-string v6, "charset"

    const/16 v10, 0x6a

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4012
    :cond_3
    invoke-virtual {p0, v2, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v6

    .line 4014
    invoke-static {p0, v6}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 4016
    const/4 v6, 0x0

    :goto_1
    if-ge v6, v9, :cond_d

    .line 4018
    const/4 v10, -0x1

    if-eq v5, v10, :cond_4

    .line 4019
    const-string v10, "msg_id"

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4020
    :cond_4
    const/4 v10, -0x1

    if-eq v4, v10, :cond_5

    .line 4021
    const-string v10, "address"

    aget-object v11, v8, v6

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4022
    :cond_5
    const/4 v10, -0x1

    if-eq v3, v10, :cond_6

    .line 4023
    const-string v10, "type"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4024
    :cond_6
    const/4 v10, -0x1

    if-eq v1, v10, :cond_7

    .line 4025
    const-string v10, "charset"

    const/16 v11, 0x6a

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4027
    :cond_7
    invoke-virtual {p0, v2, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v10

    .line 4029
    invoke-static {p0, v10}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 4016
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 4032
    :cond_8
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_e

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_e

    .line 4035
    const/4 v6, -0x1

    if-eq v5, v6, :cond_9

    .line 4036
    const-string v5, "msg_id"

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v7, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4037
    :cond_9
    const/4 v5, -0x1

    if-eq v4, v5, :cond_a

    .line 4038
    const-string v4, "address"

    move-object/from16 v0, p2

    invoke-virtual {v7, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4039
    :cond_a
    const/4 v4, -0x1

    if-eq v3, v4, :cond_b

    .line 4040
    const-string v3, "type"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v7, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4041
    :cond_b
    const/4 v3, -0x1

    if-eq v1, v3, :cond_c

    .line 4042
    const-string v1, "charset"

    const/16 v3, 0x24

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4044
    :cond_c
    invoke-virtual {p0, v2, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 4046
    invoke-static {p0, v1}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 4084
    :cond_d
    return-void

    .line 4049
    :cond_e
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_d

    .line 4051
    const-string v6, ","

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 4052
    array-length v9, v8

    .line 4054
    const/4 v6, 0x0

    :goto_2
    add-int/lit8 v10, v9, 0x1

    if-ge v6, v10, :cond_d

    .line 4057
    const/4 v10, -0x1

    if-eq v5, v10, :cond_f

    .line 4058
    const-string v10, "msg_id"

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v7, v10, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4060
    :cond_f
    if-nez v6, :cond_13

    .line 4062
    const/4 v10, -0x1

    if-eq v4, v10, :cond_10

    .line 4063
    const-string v10, "address"

    invoke-virtual {v7, v10, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4064
    :cond_10
    const/4 v10, -0x1

    if-eq v3, v10, :cond_11

    .line 4065
    const-string v10, "type"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v7, v10, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4066
    :cond_11
    const/4 v10, -0x1

    if-eq v1, v10, :cond_12

    .line 4067
    const-string v10, "charset"

    const/16 v14, 0x6a

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v7, v10, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4079
    :cond_12
    :goto_3
    invoke-virtual {p0, v2, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v10

    .line 4081
    invoke-static {p0, v10}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 4054
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 4071
    :cond_13
    const/4 v10, -0x1

    if-eq v4, v10, :cond_14

    .line 4072
    const-string v10, "address"

    add-int/lit8 v14, v6, -0x1

    aget-object v14, v8, v14

    invoke-virtual {v7, v10, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4073
    :cond_14
    const/4 v10, -0x1

    if-eq v3, v10, :cond_15

    .line 4074
    const-string v10, "type"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v7, v10, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4075
    :cond_15
    const/4 v10, -0x1

    if-eq v1, v10, :cond_12

    .line 4076
    const-string v10, "charset"

    const/16 v14, 0x6a

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v7, v10, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_3

    :cond_16
    move v1, v7

    move v3, v8

    move v4, v9

    move v5, v10

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/ContentResolver;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 29

    .prologue
    .line 3652
    const/4 v11, 0x0

    .line 3653
    const/16 v26, 0x0

    .line 3654
    const/16 v25, 0x0

    .line 3656
    const-string v2, ""

    .line 3657
    const-string v18, ""

    .line 3659
    const/4 v12, 0x0

    .line 3660
    const/4 v13, 0x0

    .line 3661
    const/4 v14, 0x0

    .line 3662
    const/4 v15, 0x0

    .line 3663
    const/16 v16, 0x0

    .line 3664
    const/16 v17, 0x0

    .line 3665
    const/16 v24, 0x0

    .line 3667
    const/16 v23, 0x0

    .line 3668
    const/16 v22, 0x0

    .line 3669
    const/16 v21, 0x0

    .line 3670
    const/16 v20, 0x0

    .line 3671
    const/16 v19, 0x0

    .line 3672
    const/4 v10, 0x0

    .line 3673
    const/4 v9, 0x0

    .line 3674
    const/4 v8, 0x0

    .line 3677
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mms/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/part"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3680
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    :try_start_0
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v23

    .line 3681
    if-eqz v23, :cond_22

    .line 3683
    :try_start_1
    const-string v2, "mid"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 3684
    const-string v2, "ct"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 3685
    const-string v2, "cid"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    .line 3686
    :try_start_2
    const-string v2, "cl"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    .line 3687
    :try_start_3
    const-string v2, "text"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_b
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v5

    .line 3688
    :try_start_4
    const-string v2, "chset"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_c
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v4

    .line 3689
    :try_start_5
    const-string v2, "name"

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_d
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v2

    move/from16 v8, v21

    move/from16 v9, v22

    .line 3698
    :goto_0
    if-eqz v23, :cond_21

    .line 3699
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    .line 3702
    :goto_1
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 3704
    sget-object v2, Lcom/wssnps/a/c;->i:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3705
    sget-object v2, Lcom/wssnps/a/c;->i:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v11, v2

    .line 3707
    :cond_0
    sget-object v2, Lcom/wssnps/a/c;->n:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3708
    sget-object v2, Lcom/wssnps/a/c;->n:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    const-string v12, ";"

    invoke-virtual {v2, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object v12, v2

    .line 3710
    :cond_1
    sget-object v2, Lcom/wssnps/a/c;->o:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3711
    sget-object v2, Lcom/wssnps/a/c;->o:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    const-string v13, ";"

    invoke-virtual {v2, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object v13, v2

    .line 3713
    :cond_2
    sget-object v2, Lcom/wssnps/a/c;->p:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3714
    sget-object v2, Lcom/wssnps/a/c;->p:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    const-string v14, ";"

    invoke-virtual {v2, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object v14, v2

    .line 3716
    :cond_3
    sget-object v2, Lcom/wssnps/a/c;->m:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 3717
    sget-object v2, Lcom/wssnps/a/c;->m:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    const-string v15, ";"

    invoke-virtual {v2, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object v15, v2

    .line 3719
    :cond_4
    sget-object v2, Lcom/wssnps/a/c;->q:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 3720
    sget-object v2, Lcom/wssnps/a/c;->q:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    const-string v16, ";"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object/from16 v16, v2

    .line 3722
    :cond_5
    sget-object v2, Lcom/wssnps/a/c;->k:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 3723
    sget-object v2, Lcom/wssnps/a/c;->k:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    const-string v17, ";"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object/from16 v17, v2

    .line 3725
    :cond_6
    sget-object v2, Lcom/wssnps/a/c;->l:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 3726
    sget-object v2, Lcom/wssnps/a/c;->l:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    move-object/from16 v18, v2

    .line 3728
    :cond_7
    const/4 v2, 0x0

    move/from16 v23, v2

    move-object/from16 v22, v24

    move/from16 v20, v25

    move/from16 v21, v26

    :goto_2
    move/from16 v0, v23

    if-ge v0, v11, :cond_1c

    .line 3730
    const-string v19, ""

    .line 3733
    const/4 v2, -0x1

    if-eq v10, v2, :cond_8

    .line 3734
    const-string v2, "mid"

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3737
    :cond_8
    const/4 v2, -0x1

    if-eq v9, v2, :cond_9

    .line 3739
    if-eqz v12, :cond_9

    array-length v2, v12

    move/from16 v0, v23

    if-le v2, v0, :cond_9

    .line 3741
    aget-object v2, v12, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 3742
    const-string v2, "ct"

    aget-object v24, v12, v23

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3747
    :cond_9
    const/4 v2, -0x1

    if-eq v8, v2, :cond_a

    .line 3749
    const-string v2, ""

    .line 3750
    if-eqz v13, :cond_a

    array-length v2, v13

    move/from16 v0, v23

    if-le v2, v0, :cond_a

    .line 3752
    aget-object v2, v13, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 3756
    :try_start_6
    new-instance v2, Ljava/lang/String;

    aget-object v24, v13, v23

    const-string v25, "ISO-8859-1"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    .line 3757
    const-string v24, "cid"

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_6 .. :try_end_6} :catch_1

    .line 3768
    :cond_a
    :goto_3
    const/4 v2, -0x1

    if-eq v7, v2, :cond_b

    .line 3770
    const-string v2, ""

    .line 3771
    if-eqz v14, :cond_b

    array-length v2, v14

    move/from16 v0, v23

    if-le v2, v0, :cond_b

    .line 3773
    aget-object v2, v14, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 3777
    :try_start_7
    new-instance v2, Ljava/lang/String;

    aget-object v24, v14, v23

    const-string v25, "ISO-8859-1"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    .line 3778
    const-string v24, "cl"

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7 .. :try_end_7} :catch_2

    .line 3789
    :cond_b
    :goto_4
    if-eqz v12, :cond_1f

    array-length v2, v12

    move/from16 v0, v23

    if-le v2, v0, :cond_1f

    .line 3791
    aget-object v2, v12, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1f

    .line 3792
    aget-object v2, v12, v23

    const-string v22, "/"

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 3795
    :goto_5
    if-eqz v2, :cond_1e

    .line 3797
    const/16 v22, 0x0

    aget-object v22, v2, v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_1a

    .line 3799
    const-string v22, "text"

    const/16 v24, 0x0

    aget-object v24, v2, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_c

    const-string v22, "image"

    const/16 v24, 0x0

    aget-object v24, v2, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_c

    const-string v22, "video"

    const/16 v24, 0x0

    aget-object v24, v2, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_c

    const-string v22, "audio"

    const/16 v24, 0x0

    aget-object v24, v2, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_15

    .line 3801
    :cond_c
    const/16 v22, 0x1

    aget-object v22, v2, v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_1a

    .line 3803
    const-string v22, "plain"

    const/16 v24, 0x1

    aget-object v2, v2, v24

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 3805
    const-string v2, ""

    .line 3806
    const-string v2, ""

    .line 3807
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1d

    .line 3809
    if-eqz v17, :cond_f

    .line 3811
    aget-object v2, v17, v21

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 3815
    :try_start_8
    aget-object v2, v17, v21

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 3816
    add-int v2, v2, v20

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3817
    new-instance v22, Ljava/lang/String;

    const-string v24, "ISO-8859-1"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    .line 3819
    const/4 v2, -0x1

    if-eq v6, v2, :cond_d

    .line 3820
    const-string v2, "text"

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3822
    :cond_d
    const/4 v2, -0x1

    if-eq v5, v2, :cond_e

    .line 3823
    const-string v2, "chset"

    const/16 v22, 0x6a

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_8
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_8 .. :try_end_8} :catch_3

    .line 3830
    :cond_e
    :goto_6
    aget-object v2, v17, v21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int v20, v20, v2

    .line 3834
    :cond_f
    add-int/lit8 v21, v21, 0x1

    move/from16 v2, v20

    move/from16 v20, v21

    :goto_7
    move-object/from16 v28, v19

    move/from16 v19, v2

    move-object/from16 v2, v28

    .line 3924
    :goto_8
    const/16 v22, 0x0

    move/from16 v21, v20

    move/from16 v20, v19

    move-object/from16 v19, v2

    .line 3928
    :goto_9
    if-eqz v16, :cond_1b

    move-object/from16 v0, v16

    array-length v2, v0

    move/from16 v0, v23

    if-le v2, v0, :cond_1b

    .line 3930
    const-string v2, ""

    .line 3931
    aget-object v2, v16, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 3935
    :try_start_9
    new-instance v2, Ljava/lang/String;

    aget-object v24, v16, v23

    const-string v25, "ISO-8859-1"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    .line 3936
    const/16 v24, -0x1

    move/from16 v0, v24

    if-eq v4, v0, :cond_10

    .line 3937
    const-string v24, "name"

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_9 .. :try_end_9} :catch_8

    .line 3959
    :cond_10
    :goto_a
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 3960
    invoke-virtual/range {v27 .. v27}, Landroid/content/ContentValues;->clear()V

    .line 3962
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_11

    .line 3963
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/_SamsungBnR_/ABR/MMS/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-static {v0, v2, v1}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)V

    .line 3965
    :cond_11
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/wssnps/b;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 3728
    add-int/lit8 v2, v23, 0x1

    move/from16 v23, v2

    goto/16 :goto_2

    .line 3692
    :catch_0
    move-exception v2

    move v4, v10

    move/from16 v5, v19

    move/from16 v6, v20

    move/from16 v7, v21

    move-object v10, v2

    move-object/from16 v19, v23

    move v2, v9

    move/from16 v9, v22

    .line 3694
    :goto_b
    :try_start_a
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 3698
    if-eqz v19, :cond_20

    .line 3699
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    move v10, v9

    move v9, v7

    move v7, v5

    move v5, v2

    move/from16 v28, v6

    move v6, v4

    move v4, v8

    move/from16 v8, v28

    goto/16 :goto_1

    .line 3698
    :catchall_0
    move-exception v2

    :goto_c
    if-eqz v23, :cond_12

    .line 3699
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_12
    throw v2

    .line 3759
    :catch_1
    move-exception v2

    .line 3761
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_3

    .line 3780
    :catch_2
    move-exception v2

    .line 3783
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_4

    .line 3825
    :catch_3
    move-exception v2

    .line 3827
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_6

    .line 3839
    :cond_13
    if-eqz v15, :cond_1a

    array-length v2, v15

    move/from16 v0, v23

    if-le v2, v0, :cond_1a

    .line 3841
    const-string v2, ""

    .line 3842
    aget-object v2, v15, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_14

    .line 3846
    :try_start_b
    new-instance v2, Ljava/lang/String;

    aget-object v22, v15, v23

    const-string v24, "ISO-8859-1"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_b .. :try_end_b} :catch_4

    :goto_d
    move/from16 v19, v20

    move/from16 v20, v21

    .line 3854
    goto/16 :goto_8

    .line 3849
    :catch_4
    move-exception v2

    .line 3851
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :cond_14
    move-object/from16 v2, v19

    goto :goto_d

    .line 3858
    :cond_15
    const-string v22, "application"

    const/16 v24, 0x0

    aget-object v24, v2, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_19

    .line 3860
    const/16 v22, 0x1

    aget-object v22, v2, v22

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_1a

    .line 3862
    const-string v22, "smil"

    const/16 v24, 0x1

    aget-object v2, v2, v24

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 3864
    const-string v2, ""

    .line 3866
    sget-object v2, Lcom/wssnps/a/c;->j:Lcom/wssnps/a/c;

    invoke-virtual {v2}, Lcom/wssnps/a/c;->ordinal()I

    move-result v2

    aget-object v2, p2, v2

    .line 3868
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_16

    .line 3872
    :try_start_c
    new-instance v22, Ljava/lang/String;

    const-string v24, "ISO-8859-1"

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    .line 3874
    const/4 v2, -0x1

    if-eq v6, v2, :cond_16

    .line 3875
    const-string v2, "text"

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_c .. :try_end_c} :catch_5

    :cond_16
    :goto_e
    move-object/from16 v2, v19

    move/from16 v19, v20

    move/from16 v20, v21

    .line 3882
    goto/16 :goto_8

    .line 3877
    :catch_5
    move-exception v2

    .line 3879
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_e

    .line 3885
    :cond_17
    if-eqz v15, :cond_1a

    array-length v2, v15

    move/from16 v0, v23

    if-le v2, v0, :cond_1a

    .line 3887
    const-string v2, ""

    .line 3888
    aget-object v2, v15, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 3892
    :try_start_d
    new-instance v2, Ljava/lang/String;

    aget-object v22, v15, v23

    const-string v24, "ISO-8859-1"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_d .. :try_end_d} :catch_6

    :goto_f
    move/from16 v19, v20

    move/from16 v20, v21

    .line 3900
    goto/16 :goto_8

    .line 3895
    :catch_6
    move-exception v2

    .line 3897
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :cond_18
    move-object/from16 v2, v19

    goto :goto_f

    .line 3906
    :cond_19
    if-eqz v15, :cond_1a

    array-length v2, v15

    move/from16 v0, v23

    if-le v2, v0, :cond_1a

    .line 3908
    const-string v2, ""

    .line 3909
    aget-object v2, v15, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 3913
    :try_start_e
    new-instance v2, Ljava/lang/String;

    aget-object v22, v15, v23

    const-string v24, "ISO-8859-1"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_e .. :try_end_e} :catch_7

    move/from16 v19, v20

    move/from16 v20, v21

    .line 3919
    goto/16 :goto_8

    .line 3916
    :catch_7
    move-exception v2

    .line 3918
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    :cond_1a
    move-object/from16 v2, v19

    move/from16 v19, v20

    move/from16 v20, v21

    goto/16 :goto_8

    .line 3939
    :catch_8
    move-exception v2

    .line 3941
    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_a

    .line 3947
    :cond_1b
    if-eqz v12, :cond_10

    array-length v2, v12

    move/from16 v0, v23

    if-le v2, v0, :cond_10

    .line 3949
    aget-object v2, v12, v23

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 3951
    const-string v2, "text/plain"

    aget-object v24, v12, v23

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 3953
    const-string v2, "name"

    const-string v24, "null"

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 3967
    :cond_1c
    return-void

    .line 3698
    :catchall_1
    move-exception v2

    move-object/from16 v23, v19

    goto/16 :goto_c

    .line 3692
    :catch_9
    move-exception v2

    move v4, v10

    move/from16 v5, v19

    move/from16 v6, v20

    move/from16 v7, v21

    move-object v10, v2

    move-object/from16 v19, v23

    move v2, v9

    move/from16 v9, v22

    goto/16 :goto_b

    :catch_a
    move-exception v2

    move v4, v10

    move/from16 v5, v19

    move v6, v7

    move-object v10, v2

    move/from16 v7, v21

    move-object/from16 v19, v23

    move v2, v9

    move/from16 v9, v22

    goto/16 :goto_b

    :catch_b
    move-exception v2

    move v4, v10

    move v5, v6

    move-object/from16 v19, v23

    move v6, v7

    move-object v10, v2

    move/from16 v7, v21

    move v2, v9

    move/from16 v9, v22

    goto/16 :goto_b

    :catch_c
    move-exception v2

    move-object v10, v2

    move v4, v5

    move-object/from16 v19, v23

    move v5, v6

    move v2, v9

    move/from16 v9, v22

    move v6, v7

    move/from16 v7, v21

    goto/16 :goto_b

    :catch_d
    move-exception v2

    move-object v10, v2

    move/from16 v9, v22

    move-object/from16 v19, v23

    move v2, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move/from16 v7, v21

    goto/16 :goto_b

    :cond_1d
    move/from16 v2, v20

    move/from16 v20, v21

    goto/16 :goto_7

    :cond_1e
    move-object/from16 v22, v2

    goto/16 :goto_9

    :cond_1f
    move-object/from16 v2, v22

    goto/16 :goto_5

    :cond_20
    move v10, v9

    move v9, v7

    move v7, v5

    move v5, v2

    move/from16 v28, v6

    move v6, v4

    move v4, v8

    move/from16 v8, v28

    goto/16 :goto_1

    :cond_21
    move v10, v9

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v2

    goto/16 :goto_1

    :cond_22
    move v2, v8

    move v4, v9

    move v5, v10

    move/from16 v6, v19

    move/from16 v7, v20

    move/from16 v8, v21

    move/from16 v9, v22

    goto/16 :goto_0
.end method

.method public static a(I)[I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1409
    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 1413
    :try_start_0
    sget-object v2, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    if-ne p0, v2, :cond_2

    .line 1415
    const-string v2, "simphonebook"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v2

    .line 1416
    if-nez v2, :cond_1

    move-object v0, v1

    .line 1441
    :cond_0
    :goto_0
    return-object v0

    .line 1418
    :cond_1
    const/16 v1, 0x6f3a

    invoke-interface {v2, v1}, Lcom/android/internal/telephony/IIccPhoneBook;->getAdnLikesInfo(I)[I

    move-result-object v0

    goto :goto_0

    .line 1420
    :cond_2
    sget-object v2, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    if-ne p0, v2, :cond_0

    .line 1422
    const-string v2, "true"

    const-string v3, "persist.dsds.enabled"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 1424
    goto :goto_0

    .line 1428
    :cond_3
    const-string v2, "simphonebook2"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v2

    .line 1429
    if-nez v2, :cond_4

    move-object v0, v1

    .line 1430
    goto :goto_0

    .line 1431
    :cond_4
    const/16 v1, 0x6f3a

    invoke-interface {v2, v1}, Lcom/android/internal/telephony/IIccPhoneBook;->getAdnLikesInfo(I)[I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1436
    :catch_0
    move-exception v1

    .line 1438
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(II)[I
    .locals 14

    .prologue
    .line 1446
    const/4 v0, 0x3

    new-array v0, v0, [I

    .line 1447
    const/4 v1, 0x6

    new-array v1, v1, [I

    .line 1448
    const/16 v2, 0x9

    new-array v2, v2, [I

    .line 1452
    :try_start_0
    new-instance v3, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;

    invoke-direct {v3}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;-><init>()V

    .line 1454
    sget-object v4, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v4}, Lcom/wssnps/b/aa;->a()I

    move-result v4

    if-ne p1, v4, :cond_3

    .line 1456
    const-string v3, "simphonebook"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v3

    .line 1457
    if-nez v3, :cond_0

    .line 1458
    const/4 v0, 0x0

    .line 1534
    :goto_0
    return-object v0

    .line 1459
    :cond_0
    invoke-interface {v3}, Lcom/android/internal/telephony/IIccPhoneBook;->getUsimPBCapaInfo()Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;

    move-result-object v3

    .line 1476
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    .line 1478
    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v4

    .line 1479
    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v5

    .line 1480
    const/4 v6, 0x1

    const/4 v7, 0x3

    invoke-virtual {v3, v6, v7}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v6

    .line 1481
    const/4 v7, 0x2

    const/4 v8, 0x2

    invoke-virtual {v3, v7, v8}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v7

    .line 1482
    const/4 v8, 0x2

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v8

    .line 1483
    const/4 v9, 0x2

    const/4 v10, 0x3

    invoke-virtual {v3, v9, v10}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v9

    .line 1484
    const/4 v10, 0x4

    const/4 v11, 0x2

    invoke-virtual {v3, v10, v11}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v10

    .line 1485
    const/4 v11, 0x4

    const/4 v12, 0x1

    invoke-virtual {v3, v11, v12}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v11

    .line 1486
    const/4 v12, 0x4

    const/4 v13, 0x3

    invoke-virtual {v3, v12, v13}, Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;->getFieldInfo(II)I

    move-result v3

    .line 1488
    const/4 v12, 0x1

    if-ne p0, v12, :cond_6

    .line 1490
    const/4 v1, 0x0

    aput v4, v0, v1

    .line 1491
    const/4 v1, 0x1

    aput v7, v0, v1

    .line 1492
    const/4 v1, 0x2

    aput v10, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1529
    :catch_0
    move-exception v0

    .line 1531
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1534
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1461
    :cond_3
    :try_start_1
    sget-object v4, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v4}, Lcom/wssnps/b/aa;->a()I

    move-result v4

    if-ne p1, v4, :cond_1

    .line 1463
    const-string v3, "true"

    const-string v4, "persist.dsds.enabled"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1465
    const/4 v0, 0x0

    goto :goto_0

    .line 1469
    :cond_4
    const-string v3, "simphonebook2"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/IIccPhoneBook$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/IIccPhoneBook;

    move-result-object v3

    .line 1470
    if-nez v3, :cond_5

    .line 1471
    const/4 v0, 0x0

    goto :goto_0

    .line 1472
    :cond_5
    invoke-interface {v3}, Lcom/android/internal/telephony/IIccPhoneBook;->getUsimPBCapaInfo()Lcom/android/internal/telephony/uicc/UsimPhonebookCapaInfo;

    move-result-object v3

    goto :goto_1

    .line 1496
    :cond_6
    const/4 v0, 0x2

    if-ne p0, v0, :cond_9

    .line 1498
    const/4 v0, 0x0

    aput v6, v1, v0

    .line 1499
    const/4 v0, 0x1

    aput v9, v1, v0

    .line 1500
    const/4 v0, 0x2

    aput v3, v1, v0

    .line 1502
    const/4 v0, 0x3

    aput v5, v1, v0

    .line 1503
    const/4 v0, 0x4

    aput v8, v1, v0

    .line 1504
    const/4 v0, 0x5

    aput v11, v1, v0

    .line 1506
    sget-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_8

    .line 1507
    sput v11, Lcom/wssnps/b;->t:I

    :cond_7
    :goto_2
    move-object v0, v1

    .line 1511
    goto/16 :goto_0

    .line 1508
    :cond_8
    sget-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_7

    .line 1509
    sput v11, Lcom/wssnps/b;->u:I

    goto :goto_2

    .line 1515
    :cond_9
    const/4 v0, 0x0

    aput v4, v2, v0

    .line 1516
    const/4 v0, 0x1

    aput v5, v2, v0

    .line 1517
    const/4 v0, 0x2

    aput v6, v2, v0

    .line 1518
    const/4 v0, 0x3

    aput v7, v2, v0

    .line 1519
    const/4 v0, 0x4

    aput v8, v2, v0

    .line 1520
    const/4 v0, 0x5

    aput v9, v2, v0

    .line 1521
    const/4 v0, 0x6

    aput v10, v2, v0

    .line 1522
    const/4 v0, 0x7

    aput v11, v2, v0

    .line 1523
    const/16 v0, 0x8

    aput v3, v2, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v2

    .line 1525
    goto/16 :goto_0
.end method

.method public static b(I)I
    .locals 2

    .prologue
    .line 1539
    .line 1540
    const-string v0, ""

    .line 1542
    sget-object v1, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p0, v1, :cond_1

    .line 1544
    const-string v0, "ril.ICC_TYPE"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1554
    :cond_0
    :goto_0
    const-string v1, "2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1555
    const/4 v0, 0x2

    .line 1559
    :goto_1
    return v0

    .line 1546
    :cond_1
    sget-object v1, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p0, v1, :cond_0

    .line 1548
    const-string v0, "true"

    const-string v1, "persist.dsds.enabled"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1549
    const-string v0, "ril.ICC_TYPE_1"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1551
    :cond_2
    const-string v0, "ril.ICC2_TYPE"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1557
    :cond_3
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public static d(ILjava/lang/String;)Ljava/lang/String;
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v13, 0x0

    .line 2417
    invoke-static {p1}, Lcom/wssnps/b;->O(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2418
    const-string v1, "\r\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2420
    invoke-static {}, Lcom/wssnps/b/d;->g()Landroid/app/AppOpsManager;

    move-result-object v12

    .line 2421
    invoke-static {v12}, Lcom/wssnps/b/d;->a(Landroid/app/AppOpsManager;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 2422
    invoke-static {v12, v2}, Lcom/wssnps/b/d;->a(Landroid/app/AppOpsManager;Z)V

    .line 2424
    :cond_0
    aget-object v1, v0, v13

    aget-object v2, v0, v2

    const/4 v3, 0x2

    aget-object v3, v0, v3

    const/4 v4, 0x3

    aget-object v4, v0, v4

    const/4 v5, 0x4

    aget-object v5, v0, v5

    const/4 v6, 0x5

    aget-object v6, v0, v6

    const/4 v7, 0x6

    aget-object v7, v0, v7

    const/16 v8, 0x8

    aget-object v8, v0, v8

    const/4 v9, 0x7

    aget-object v9, v0, v9

    const/16 v10, 0x9

    aget-object v10, v0, v10

    const/16 v11, 0xa

    aget-object v11, v0, v11

    move v0, p0

    invoke-static/range {v0 .. v11}, Lcom/wssnps/b;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2425
    if-nez v0, :cond_1

    .line 2426
    const-string v0, "-1\n"

    .line 2429
    :goto_0
    return-object v0

    .line 2428
    :cond_1
    invoke-static {v12, v13}, Lcom/wssnps/b/d;->a(Landroid/app/AppOpsManager;Z)V

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 5747
    .line 5750
    const-string v0, ""

    .line 5752
    const/4 v0, 0x0

    .line 5760
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateCalendar "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v8, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5763
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v3, :cond_9

    .line 5765
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v0

    .line 5768
    :goto_0
    if-nez v3, :cond_0

    .line 5855
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 5775
    :cond_0
    invoke-static {p1}, Lcom/wssnps/b/x;->a(Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v4

    .line 5776
    if-nez v4, :cond_1

    .line 5779
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 5782
    :cond_1
    iget-object v0, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-wide v6, v4, Lcom/wssnps/b/x;->w:J

    cmp-long v0, v6, v10

    if-eqz v0, :cond_3

    .line 5785
    iget-object v0, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v0}, Lcom/wssnps/b/d;->H(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_2

    .line 5787
    iput v0, v4, Lcom/wssnps/b/x;->x:I

    .line 5790
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CalUidTable.get parLuid : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move v0, v1

    .line 5802
    :goto_2
    invoke-static {p0}, Lcom/wssnps/b;->N(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 5804
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 5794
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CalUidTable.get Return NULL : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    .line 5799
    goto :goto_2

    .line 5807
    :cond_4
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 5808
    invoke-static {v3, v4, v5}, Lcom/wssnps/b/x;->d(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I

    move-result v6

    .line 5810
    if-eq v0, v1, :cond_5

    .line 5812
    iget-object v0, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-wide v0, v4, Lcom/wssnps/b/x;->w:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_6

    .line 5815
    iget-object v0, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/wssnps/b/d;->a(Ljava/lang/String;I)V

    .line 5818
    new-instance v0, Lcom/wssnps/b/x;

    invoke-direct {v0}, Lcom/wssnps/b/x;-><init>()V

    .line 5819
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v0

    .line 5821
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5822
    const-string v4, "deleted"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5823
    const-string v4, "original_sync_id"

    const-string v7, ""

    invoke-virtual {v1, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5824
    const-string v4, "original_id"

    const-string v7, ""

    invoke-virtual {v1, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5826
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "original_sync_id=\""

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v0, Lcom/wssnps/b/x;->r:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\""

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5829
    invoke-static {v3, v1, v0}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Ljava/lang/String;)Z

    .line 5839
    :cond_5
    :goto_3
    if-nez v6, :cond_7

    .line 5841
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 5834
    :cond_6
    iput v2, v4, Lcom/wssnps/b/x;->x:I

    goto :goto_3

    .line 5845
    :cond_7
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 5846
    sget-boolean v1, Lcom/wssnps/b/d;->n:Z

    if-nez v1, :cond_8

    .line 5848
    sget-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    .line 5849
    sget-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    .line 5851
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    move-object v3, v0

    goto/16 :goto_0
.end method

.method public static f(I)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2808
    .line 2812
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/_SamsungBnR_/ABR/.MMS/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2814
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2815
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2817
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    move v0, v1

    .line 2818
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 2819
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v3, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2818
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2822
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v2

    .line 2825
    invoke-static {v2, v3}, Lcom/wssnps/b;->a(J)J

    move-result-wide v2

    const-wide/32 v4, 0x19000

    sub-long/2addr v2, v4

    int-to-long v4, p0

    invoke-static {v4, v5}, Lcom/wssnps/b;->a(J)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    .line 2826
    const/4 v1, 0x1

    .line 2830
    :cond_1
    return v1
.end method

.method public static f(ILjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 4626
    invoke-static {p1}, Lcom/wssnps/b;->O(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4627
    const-string v1, "\r\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 4629
    invoke-static {p0, v0}, Lcom/wssnps/b;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4631
    if-nez v0, :cond_0

    .line 4632
    const-string v0, "-1\n"

    .line 4634
    :cond_0
    return-object v0
.end method

.method public static h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 7663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7666
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateTask "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7668
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 7685
    :cond_0
    :goto_0
    return-object v0

    .line 7671
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 7672
    if-eqz v1, :cond_0

    .line 7674
    invoke-static {p1}, Lcom/wssnps/b/ag;->a(Ljava/lang/String;)Lcom/wssnps/b/ag;

    move-result-object v2

    .line 7675
    if-eqz v2, :cond_0

    .line 7677
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 7678
    invoke-static {v1, v2, v0}, Lcom/wssnps/b/ag;->b(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)I

    move-result v1

    .line 7679
    if-nez v1, :cond_2

    .line 7680
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 7682
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static h()Z
    .locals 5

    .prologue
    .line 4817
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 4819
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 4820
    const-string v2, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 4821
    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 4822
    sget-object v2, Lcom/wssnps/b;->o:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 4824
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 4826
    :goto_0
    sget-boolean v0, Lcom/wssnps/b;->j:Z

    if-nez v0, :cond_0

    .line 4830
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4832
    :catch_0
    move-exception v0

    .line 4834
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 4838
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssnps/b;->j:Z

    .line 4839
    const/4 v0, 0x1

    return v0
.end method

.method private j(Ljava/lang/String;Ljava/lang/String;)I
    .locals 12

    .prologue
    const/4 v2, 0x0

    const v3, 0xfa000

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 4640
    .line 4644
    const-string v4, ""

    .line 4645
    const-string v4, ""

    .line 4647
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://mms/part/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 4649
    const/4 v6, 0x0

    .line 4652
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 4654
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "/_SamsungBnR_/ABR/.MMS/"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 4656
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 4657
    invoke-static {v4}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4659
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v9, Lcom/wssnps/smlModelDefine;->c:I

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/_SamsungBnR_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v9, v10}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I

    .line 4660
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v9, Lcom/wssnps/smlModelDefine;->c:I

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/_SamsungBnR_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/ABR"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v9, v10}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I

    .line 4661
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v9, Lcom/wssnps/smlModelDefine;->c:I

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/_SamsungBnR_/ABR/.MMS/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v9, v10}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I

    .line 4666
    :cond_0
    :try_start_0
    new-instance v4, Ljava/io/BufferedInputStream;

    invoke-virtual {v7, v5}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4667
    :try_start_1
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4668
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->available()I

    move-result v5

    .line 4669
    invoke-static {v5}, Lcom/wssnps/b;->f(I)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 4671
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/_SamsungBnR_/ABR/.MMS/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4672
    const/4 v0, 0x5

    .line 4697
    if-eqz v2, :cond_1

    .line 4701
    :try_start_2
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 4710
    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    .line 4714
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 4724
    :cond_2
    :goto_1
    return v0

    .line 4703
    :catch_0
    move-exception v1

    .line 4706
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 4716
    :catch_1
    move-exception v1

    .line 4719
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 4675
    :cond_3
    if-le v5, v3, :cond_4

    move v5, v3

    .line 4678
    :cond_4
    :try_start_4
    new-array v6, v5, [B

    .line 4679
    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v8}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 4681
    :cond_5
    const/4 v2, 0x0

    :try_start_5
    invoke-virtual {v4, v6, v2, v5}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v2

    if-eq v2, v1, :cond_6

    .line 4683
    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7, v2}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 4684
    if-nez v2, :cond_5

    .line 4687
    :cond_6
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 4697
    if-eqz v3, :cond_7

    .line 4701
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 4710
    :cond_7
    :goto_2
    if-eqz v4, :cond_2

    .line 4714
    :try_start_7
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    .line 4716
    :catch_2
    move-exception v1

    .line 4719
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 4703
    :catch_3
    move-exception v1

    .line 4706
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 4689
    :catch_4
    move-exception v0

    move-object v3, v2

    .line 4692
    :goto_3
    :try_start_8
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 4697
    if-eqz v2, :cond_8

    .line 4701
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 4710
    :cond_8
    :goto_4
    if-eqz v3, :cond_b

    .line 4714
    :try_start_a
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    move v0, v1

    .line 4720
    goto :goto_1

    .line 4703
    :catch_5
    move-exception v0

    .line 4706
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 4716
    :catch_6
    move-exception v0

    .line 4719
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v1

    .line 4720
    goto :goto_1

    .line 4697
    :catchall_0
    move-exception v0

    move-object v4, v2

    :goto_5
    if-eqz v2, :cond_9

    .line 4701
    :try_start_b
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 4710
    :cond_9
    :goto_6
    if-eqz v4, :cond_a

    .line 4714
    :try_start_c
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 4720
    :cond_a
    :goto_7
    throw v0

    .line 4703
    :catch_7
    move-exception v1

    .line 4706
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 4716
    :catch_8
    move-exception v1

    .line 4719
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 4697
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_5

    :catchall_3
    move-exception v0

    move-object v4, v3

    goto :goto_5

    .line 4689
    :catch_9
    move-exception v0

    move-object v3, v4

    goto :goto_3

    :catch_a
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public static j(Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 5652
    .line 5654
    const/4 v0, 0x0

    .line 5656
    const-string v3, "-1"

    .line 5665
    const-string v3, "insertCalendar"

    invoke-static {v8, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5668
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v3, :cond_8

    .line 5670
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v0

    .line 5673
    :goto_0
    if-nez v3, :cond_0

    .line 5742
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 5680
    :cond_0
    invoke-static {p0}, Lcom/wssnps/b/x;->a(Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v4

    .line 5681
    if-nez v4, :cond_1

    .line 5684
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 5687
    :cond_1
    iget-object v0, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-wide v6, v4, Lcom/wssnps/b/x;->w:J

    cmp-long v0, v6, v10

    if-eqz v0, :cond_4

    .line 5690
    iget-object v0, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v0}, Lcom/wssnps/b/d;->H(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_3

    .line 5692
    iput v0, v4, Lcom/wssnps/b/x;->x:I

    .line 5695
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CalUidTable.get parLuid : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move v0, v1

    .line 5707
    :goto_2
    invoke-static {v3, v4, v2}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I

    move-result v3

    .line 5709
    if-eq v0, v1, :cond_2

    .line 5711
    iget-object v0, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-wide v0, v4, Lcom/wssnps/b/x;->w:J

    cmp-long v0, v0, v10

    if-nez v0, :cond_5

    .line 5714
    iget-object v0, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/wssnps/b/d;->a(Ljava/lang/String;I)V

    .line 5726
    :cond_2
    :goto_3
    if-nez v3, :cond_6

    .line 5728
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 5699
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CalUidTable.get Return NULL : "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, v4, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    .line 5704
    goto :goto_2

    .line 5720
    :cond_5
    iput v2, v4, Lcom/wssnps/b/x;->x:I

    goto :goto_3

    .line 5732
    :cond_6
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 5733
    sget-boolean v1, Lcom/wssnps/b/d;->n:Z

    if-nez v1, :cond_7

    .line 5735
    sget-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    .line 5736
    sget-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    .line 5738
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    move-object v3, v0

    goto/16 :goto_0
.end method

.method static synthetic q(I)I
    .locals 0

    .prologue
    .line 75
    sput p0, Lcom/wssnps/b;->w:I

    return p0
.end method

.method public static y(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 6738
    const-string v0, "calendar_sync_id"

    .line 6740
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 6741
    const-string v2, "content://settings/system"

    .line 6742
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6745
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SetCalendarSyncID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6746
    invoke-static {}, Lcom/wssnps/b/d;->h()V

    .line 6748
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 6749
    const-string v0, "-1\ncontext null\n"

    .line 6767
    :goto_0
    return-object v0

    .line 6751
    :cond_0
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 6752
    if-nez v3, :cond_1

    .line 6753
    const-string v0, "-1\nresolver null\n"

    goto :goto_0

    .line 6755
    :cond_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 6756
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 6758
    const-string v5, "value"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6765
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "name=\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6766
    const/4 v1, 0x0

    invoke-virtual {v3, v2, v4, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 6767
    const-string v0, "0\n"

    goto :goto_0

    .line 6762
    :cond_2
    const-string v1, "value"

    const-string v5, "0"

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 7079
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 7081
    const-string v2, "vnd.sec.contact.phone"

    .line 7082
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleted=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "account_type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 7083
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 7086
    const-string v3, "setDeleteContactsSyncIndexArray"

    invoke-static {v5, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7088
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 7090
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7091
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7111
    :goto_0
    return-object v0

    .line 7094
    :cond_0
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 7095
    if-eqz v3, :cond_2

    .line 7097
    invoke-static {v3, v2}, Lcom/wssnps/b/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 7098
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 7100
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7102
    :goto_1
    if-ge v0, v3, :cond_2

    .line 7104
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7106
    sget-object v4, Lcom/wssnps/b;->l:Ljava/lang/StringBuffer;

    if-eqz v4, :cond_1

    .line 7107
    sget-object v4, Lcom/wssnps/b;->l:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7102
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7110
    :cond_2
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7111
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public A(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 7494
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7498
    const-string v2, "insertMemo"

    invoke-static {v4, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7500
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 7528
    :cond_0
    :goto_0
    return-object v0

    .line 7503
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7504
    if-eqz v2, :cond_0

    .line 7506
    invoke-static {p1}, Lcom/wssnps/b/ae;->a(Ljava/lang/String;)Lcom/wssnps/b/ae;

    move-result-object v3

    .line 7507
    if-eqz v3, :cond_0

    .line 7509
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 7510
    if-ne v0, v4, :cond_2

    .line 7511
    invoke-static {v2, v3, v1}, Lcom/wssnps/b/ae;->a(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    .line 7521
    :goto_1
    if-nez v0, :cond_6

    .line 7522
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "6\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 7512
    :cond_2
    const/4 v4, 0x3

    if-ne v0, v4, :cond_3

    .line 7513
    invoke-static {v2, v3, v1}, Lcom/wssnps/b/ae;->c(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    goto :goto_1

    .line 7514
    :cond_3
    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    .line 7515
    invoke-static {v2, v3, v1}, Lcom/wssnps/b/ae;->e(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    goto :goto_1

    .line 7516
    :cond_4
    const/4 v4, 0x4

    if-ne v0, v4, :cond_5

    .line 7517
    invoke-static {v2, v3, v1}, Lcom/wssnps/b/ae;->g(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    goto :goto_1

    .line 7518
    :cond_5
    const/4 v4, 0x6

    if-ne v0, v4, :cond_7

    .line 7519
    invoke-static {v2, v3, v1}, Lcom/wssnps/b/ae;->i(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    goto :goto_1

    .line 7524
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public B()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 7116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 7123
    const-string v0, "setCalendarInvalidSyncInit"

    invoke-static {v8, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7125
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 7159
    :goto_0
    return-object v6

    .line 7128
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7130
    if-eqz v0, :cond_5

    .line 7132
    const-string v3, "account_name=\"My calendar\""

    .line 7133
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 7134
    if-eqz v1, :cond_4

    .line 7136
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 7140
    :cond_1
    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 7142
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleted=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "calendar_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND contact_account_type is null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 7145
    invoke-static {v0, v2}, Lcom/wssnps/b/x;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v2

    .line 7146
    if-nez v2, :cond_2

    .line 7148
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 7152
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 7154
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7156
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v6, v0

    .line 7159
    goto/16 :goto_0

    :cond_5
    move-object v0, v6

    goto :goto_1
.end method

.method public B(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 7573
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7578
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteMemo "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7580
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 7605
    :cond_0
    :goto_0
    return-object v0

    .line 7583
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7584
    if-eqz v2, :cond_0

    .line 7586
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 7587
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 7588
    if-ne v0, v4, :cond_2

    .line 7589
    invoke-static {v2, v3}, Lcom/wssnps/b/ae;->b(Landroid/content/ContentResolver;I)Z

    move-result v0

    .line 7599
    :goto_1
    if-nez v0, :cond_6

    .line 7600
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "6\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 7590
    :cond_2
    const/4 v4, 0x3

    if-ne v0, v4, :cond_3

    .line 7591
    invoke-static {v2, v3}, Lcom/wssnps/b/ae;->d(Landroid/content/ContentResolver;I)Z

    move-result v0

    goto :goto_1

    .line 7592
    :cond_3
    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    .line 7593
    invoke-static {v2, v3}, Lcom/wssnps/b/ae;->f(Landroid/content/ContentResolver;I)Z

    move-result v0

    goto :goto_1

    .line 7594
    :cond_4
    const/4 v4, 0x4

    if-ne v0, v4, :cond_5

    .line 7595
    invoke-static {v2, v3}, Lcom/wssnps/b/ae;->h(Landroid/content/ContentResolver;I)Z

    move-result v0

    goto :goto_1

    .line 7596
    :cond_5
    const/4 v2, 0x6

    if-ne v0, v2, :cond_7

    .line 7597
    invoke-static {}, Lcom/wssnps/b/ae;->c()Z

    move-result v0

    goto :goto_1

    .line 7602
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public C()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 7164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 7167
    const-string v0, ""

    .line 7168
    const-string v0, ""

    .line 7169
    const-string v8, ""

    .line 7175
    const-string v0, "setCalendarInitSyncInfo"

    invoke-static {v11, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7177
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    move-object v0, v7

    .line 7264
    :goto_0
    return-object v0

    .line 7180
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7181
    if-eqz v0, :cond_4

    .line 7183
    const-string v3, "account_name=\"My calendar\""

    .line 7184
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 7185
    if-eqz v3, :cond_4

    .line 7187
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    new-array v2, v1, [I

    .line 7188
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v6

    .line 7192
    :cond_1
    const-string v4, "_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aput v4, v2, v1

    .line 7193
    const-string v4, "_sync_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 7196
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 7197
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "My calendar"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 7199
    aget v5, v2, v1

    invoke-static {v0, v4, v5}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    move-result v4

    .line 7200
    if-nez v4, :cond_2

    .line 7202
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move-object v0, v7

    .line 7203
    goto :goto_0

    .line 7206
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 7207
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 7209
    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 7213
    :cond_4
    sget-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 7215
    sget-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    sget-object v3, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 7216
    const-string v3, ""

    sput-object v3, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    move-object v4, v1

    .line 7219
    :goto_1
    if-eqz v2, :cond_9

    move v3, v6

    .line 7221
    :goto_2
    array-length v1, v2

    if-ge v3, v1, :cond_9

    .line 7223
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 7224
    const-string v1, "dirty"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7225
    const-string v1, "calendar_id"

    aget v8, v2, v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7227
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 7229
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id in("

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ") AND "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "calendar_id"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "=\""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v8, v2, v3

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "\""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " AND contact_account_type is null"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7238
    :goto_3
    invoke-static {v0, v5, v1}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v1

    .line 7239
    if-nez v1, :cond_6

    move-object v0, v7

    .line 7241
    goto/16 :goto_0

    .line 7234
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dirty=\""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "\""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " AND "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "calendar_id"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "=\""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v8, v2, v3

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, "\""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " AND contact_account_type is null"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 7244
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleted=\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " AND "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "calendar_id"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "=\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v5, v2, v3

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " AND contact_account_type is null"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7246
    invoke-static {v0, v1}, Lcom/wssnps/b/x;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    .line 7247
    if-nez v1, :cond_7

    move-object v0, v7

    .line 7249
    goto/16 :goto_0

    .line 7252
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleted=\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " AND "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "_id"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "=\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v5, v2, v3

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7254
    invoke-static {v0, v1}, Lcom/wssnps/b/x;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    .line 7255
    if-nez v1, :cond_8

    move-object v0, v7

    .line 7257
    goto/16 :goto_0

    .line 7221
    :cond_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_2

    .line 7262
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7263
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "strResult : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    move-object v4, v8

    goto/16 :goto_1
.end method

.method public C(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 7636
    .line 7639
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTaskItem "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7641
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 7642
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7646
    :goto_0
    return-object v0

    .line 7644
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 7645
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 7646
    invoke-static {v1, v0}, Lcom/wssnps/b/ag;->a(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public D()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 7269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7271
    const-string v0, ""

    .line 7272
    const-string v0, ""

    .line 7277
    const-string v0, "setAddCalendarSyncCount"

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7279
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 7308
    :goto_0
    return-object v1

    .line 7282
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7284
    if-eqz v0, :cond_4

    .line 7286
    const-string v3, "account_name=\"My calendar\""

    .line 7287
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 7288
    if-eqz v2, :cond_3

    .line 7290
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v6

    .line 7295
    :cond_1
    const-string v3, "_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 7297
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_id=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "dirty"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND contact_account_type is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 7301
    invoke-static {v0, v3}, Lcom/wssnps/b/x;->d(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 7302
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 7304
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 7306
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    move-object v1, v0

    .line 7308
    goto/16 :goto_0

    :cond_2
    move v1, v6

    goto :goto_1

    :cond_3
    move v1, v6

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public E()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 7313
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7315
    const-string v0, ""

    .line 7316
    const-string v0, ""

    .line 7321
    const-string v0, "setDeleteCalendarSyncCount"

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7323
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 7352
    :goto_0
    return-object v1

    .line 7326
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7327
    if-eqz v0, :cond_4

    .line 7329
    const-string v3, "account_name=\"My calendar\""

    .line 7330
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 7332
    if-eqz v2, :cond_3

    .line 7334
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v6

    .line 7339
    :cond_1
    const-string v3, "_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 7341
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_id=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "dirty"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND contact_account_type is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 7345
    invoke-static {v0, v3}, Lcom/wssnps/b/x;->d(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    .line 7346
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 7348
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 7350
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    move-object v1, v0

    .line 7352
    goto/16 :goto_0

    :cond_2
    move v1, v6

    goto :goto_1

    :cond_3
    move v1, v6

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public E(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 7690
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7694
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteTask "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7696
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 7709
    :cond_0
    :goto_0
    return-object v0

    .line 7699
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 7700
    if-eqz v1, :cond_0

    .line 7702
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 7703
    invoke-static {v1, v0}, Lcom/wssnps/b/ag;->b(Landroid/content/ContentResolver;I)Z

    move-result v1

    .line 7704
    if-nez v1, :cond_2

    .line 7705
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 7707
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public F()Ljava/lang/String;
    .locals 3

    .prologue
    .line 7357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7360
    const/4 v1, 0x1

    const-string v2, "setAddCalendarSyncIndexArray"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7362
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 7369
    :cond_0
    :goto_0
    return-object v0

    .line 7365
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 7366
    if-eqz v1, :cond_0

    .line 7367
    const/4 v0, 0x2

    invoke-static {v1, v0}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public F(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 8296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8298
    const-string v1, ""

    .line 8301
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSmemoItem "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8303
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 8325
    :cond_0
    :goto_0
    return-object v0

    .line 8306
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v1

    .line 8307
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 8309
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 8310
    if-eqz v1, :cond_0

    .line 8312
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 8313
    invoke-static {v1, v0}, Lcom/wssnps/b/af;->a(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    .line 8314
    if-nez v0, :cond_2

    .line 8315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 8317
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 8320
    :cond_3
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 8322
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public G()Ljava/lang/String;
    .locals 3

    .prologue
    .line 7374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7377
    const/4 v1, 0x1

    const-string v2, "setDeleteCalendarSyncIndexArray"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7379
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 7386
    :cond_0
    :goto_0
    return-object v0

    .line 7382
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 7383
    if-eqz v1, :cond_0

    .line 7384
    const/4 v0, 0x3

    invoke-static {v1, v0}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public G(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 8330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8331
    const-string v1, ""

    .line 8335
    const-string v1, "insertSmemo"

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8337
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 8381
    :cond_0
    :goto_0
    return-object v0

    .line 8340
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 8341
    if-eqz v1, :cond_0

    .line 8343
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 8345
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v2

    .line 8346
    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    .line 8348
    invoke-static {v1, p1, v5}, Lcom/wssnps/b/af;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 8349
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 8350
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 8352
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 8354
    :cond_3
    const/4 v1, 0x4

    if-ne v2, v1, :cond_0

    .line 8356
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Application/SMemo/smemo1.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8357
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    .line 8359
    const-string v0, "File dose not exist!! SMEMO_TO_SMEMO2_RESTORE_FINISH set true"

    invoke-static {v4, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8360
    sput-boolean v4, Lcom/wssnps/b/af;->ao:Z

    .line 8363
    :cond_4
    sget-boolean v0, Lcom/wssnps/b/af;->ao:Z

    if-nez v0, :cond_5

    .line 8365
    const-string v0, "SMEMO_TO_SMEMO2_RESTORE_FINISH = false"

    invoke-static {v4, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8366
    const-string v0, "9\n"

    goto/16 :goto_0

    .line 8370
    :cond_5
    const-string v0, "addSMemotoSMemo2"

    invoke-static {v4, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8371
    invoke-static {p1}, Lcom/wssnps/b/af;->e(Ljava/lang/String;)Z

    move-result v0

    .line 8372
    if-eqz v0, :cond_6

    .line 8373
    const-string v0, "0\n1\n"

    goto/16 :goto_0

    .line 8375
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public H()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 7391
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 7394
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7397
    const-string v3, "getMemoIndexArray"

    invoke-static {v5, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7399
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 7401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7402
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7422
    :goto_0
    return-object v0

    .line 7405
    :cond_0
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 7406
    if-eqz v3, :cond_3

    .line 7408
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v4

    .line 7409
    if-ne v4, v5, :cond_2

    .line 7410
    invoke-static {v3}, Lcom/wssnps/b/ae;->b(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v0

    .line 7414
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 7415
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7416
    :goto_2
    if-ge v1, v3, :cond_3

    .line 7418
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7416
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 7411
    :cond_2
    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 7412
    invoke-static {v3}, Lcom/wssnps/b/ae;->d(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    .line 7421
    :cond_3
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7422
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public H(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 8413
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8418
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteSmemo "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8420
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 8434
    :cond_0
    :goto_0
    return-object v0

    .line 8423
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 8424
    if-eqz v1, :cond_0

    .line 8426
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 8427
    invoke-static {v1, v0}, Lcom/wssnps/b/af;->b(Landroid/content/ContentResolver;I)Z

    move-result v1

    .line 8428
    if-nez v1, :cond_2

    .line 8429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 8431
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public I()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 7468
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7472
    const-string v2, "getMemoSize"

    invoke-static {v3, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7474
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 7489
    :goto_0
    return-object v1

    .line 7477
    :cond_0
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7478
    if-eqz v2, :cond_3

    .line 7480
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v1

    .line 7481
    if-ne v1, v3, :cond_2

    .line 7482
    invoke-static {v2}, Lcom/wssnps/b/ae;->a(Landroid/content/ContentResolver;)I

    move-result v0

    .line 7486
    :cond_1
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v1, v0

    .line 7489
    goto :goto_0

    .line 7483
    :cond_2
    const/4 v3, 0x3

    if-ne v1, v3, :cond_1

    .line 7484
    invoke-static {v2}, Lcom/wssnps/b/ae;->c(Landroid/content/ContentResolver;)I

    move-result v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method public I(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 8565
    const-string v1, "0\n"

    .line 8566
    const-string v0, ""

    .line 8568
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8570
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 8571
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 8575
    :goto_0
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updated : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8576
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/16 v0, 0x20

    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 8577
    return-object v1

    .line 8573
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public J()Ljava/lang/String;
    .locals 2

    .prologue
    .line 7610
    .line 7612
    const/4 v0, 0x1

    const-string v1, "getTaskSize"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7614
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 7615
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7618
    :goto_0
    return-object v0

    .line 7617
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7618
    invoke-static {v0}, Lcom/wssnps/b/ag;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public K()Ljava/lang/String;
    .locals 2

    .prologue
    .line 7623
    .line 7625
    const/4 v0, 0x1

    const-string v1, "getTaskIndexArray"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7627
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 7628
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7631
    :goto_0
    return-object v0

    .line 7630
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7631
    invoke-static {v0}, Lcom/wssnps/b/ag;->b(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public L()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7714
    .line 7718
    const/4 v2, 0x0

    .line 7720
    const-string v3, "ResponseKiseStatus"

    invoke-static {v0, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7722
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 7724
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7727
    :cond_0
    if-nez v2, :cond_1

    .line 7729
    const/4 v0, 0x2

    .line 7750
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 7733
    :cond_1
    invoke-static {}, Lcom/wssnps/smlNpsReceiver;->a()I

    move-result v2

    .line 7734
    if-gez v2, :cond_2

    move v0, v1

    .line 7739
    :cond_2
    if-nez v0, :cond_3

    .line 7741
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "6\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 7745
    :cond_3
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 7746
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public M()Ljava/lang/String;
    .locals 5

    .prologue
    .line 8148
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 8150
    sget v1, Lcom/wssnps/b;->w:I

    if-eqz v1, :cond_1

    .line 8163
    :cond_0
    sget v0, Lcom/wssnps/b;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/wssnps/b;->p:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 8164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/wssnps/b;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8166
    :goto_1
    return-object v0

    .line 8154
    :cond_1
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8148
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8157
    :catch_0
    move-exception v1

    .line 8159
    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 8166
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-1\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/wssnps/b;->w:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public N()Ljava/lang/String;
    .locals 3

    .prologue
    .line 8234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8238
    const/4 v1, 0x1

    const-string v2, "getSmemoSize"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8240
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 8258
    :cond_0
    :goto_0
    return-object v0

    .line 8243
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v1

    .line 8244
    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 8246
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 8247
    if-eqz v1, :cond_0

    .line 8249
    invoke-static {v1}, Lcom/wssnps/b/af;->a(Landroid/content/ContentResolver;)I

    move-result v0

    .line 8250
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 8253
    :cond_2
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 8255
    const-string v0, "0\n0\n"

    goto :goto_0
.end method

.method public O()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 8263
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 8266
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 8269
    const/4 v2, 0x1

    const-string v3, "getSmemoIndexArray"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8271
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 8273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 8274
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8291
    :goto_0
    return-object v0

    .line 8277
    :cond_0
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 8278
    if-eqz v2, :cond_1

    .line 8280
    invoke-static {v2}, Lcom/wssnps/b/af;->b(Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v2

    .line 8281
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 8282
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 8284
    :goto_1
    if-ge v0, v3, :cond_1

    .line 8286
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 8284
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 8290
    :cond_1
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 8291
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public P()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 8439
    const-string v0, "1\n"

    .line 8441
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Application/SMemo/smemo1.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 8443
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 8445
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.REQUEST_RESTORE_SMEMO1"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 8446
    if-eqz v1, :cond_0

    .line 8448
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 8449
    const-string v0, "SAVE_PATH"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Application/SMemo/smemo1.xml"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 8450
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 8451
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssnps/b/af;->ao:Z

    .line 8452
    const-string v0, "0\n"

    .line 8461
    :cond_0
    :goto_0
    return-object v0

    .line 8457
    :cond_1
    const-string v1, "file.exists() is false!!"

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8458
    const-string v1, "Smemo Intent send fail!!!"

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public Q()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8469
    const/4 v0, 0x1

    const-string v1, "AutoConnectionStatus"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8471
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.intent.action.KIES_AUTOCONNETION_STATUS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 8472
    sput v2, Lcom/wssnps/b;->q:I

    .line 8473
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 8474
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 8476
    :goto_0
    sget v0, Lcom/wssnps/b;->q:I

    if-nez v0, :cond_0

    .line 8480
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 8482
    :catch_0
    move-exception v0

    .line 8484
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 8487
    :cond_0
    sput v2, Lcom/wssnps/b;->q:I

    .line 8488
    sget-object v0, Lcom/wssnps/b;->r:Ljava/lang/String;

    return-object v0
.end method

.method public R()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8493
    const/4 v0, 0x1

    const-string v1, "AutoConnectionEnable"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8495
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.intent.action.KIES_AUTOCONNETION_ENABLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 8496
    sput v2, Lcom/wssnps/b;->q:I

    .line 8497
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 8498
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 8500
    :goto_0
    sget v0, Lcom/wssnps/b;->q:I

    if-nez v0, :cond_0

    .line 8504
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 8506
    :catch_0
    move-exception v0

    .line 8508
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 8511
    :cond_0
    sput v2, Lcom/wssnps/b;->q:I

    .line 8512
    sget-object v0, Lcom/wssnps/b;->r:Ljava/lang/String;

    return-object v0
.end method

.method public S()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8517
    const/4 v0, 0x1

    const-string v1, "AutoConnectionDisable"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8519
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.intent.action.KIES_AUTOCONNETION_DISABLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 8520
    sput v2, Lcom/wssnps/b;->q:I

    .line 8521
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 8522
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 8524
    :goto_0
    sget v0, Lcom/wssnps/b;->q:I

    if-nez v0, :cond_0

    .line 8528
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 8530
    :catch_0
    move-exception v0

    .line 8532
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 8535
    :cond_0
    sput v2, Lcom/wssnps/b;->q:I

    .line 8536
    sget-object v0, Lcom/wssnps/b;->r:Ljava/lang/String;

    return-object v0
.end method

.method public T()Ljava/lang/String;
    .locals 2

    .prologue
    .line 8541
    const-string v0, "1\n"

    .line 8543
    sget-object v0, Lcom/wssnps/smlNpsService;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 8544
    const-string v0, "1\nnull\n"

    .line 8548
    :goto_0
    return-object v0

    .line 8546
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/wssnps/smlNpsService;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public V()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 8582
    const/4 v0, 0x0

    .line 8583
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v2

    .line 8584
    if-eq v2, v1, :cond_0

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    .line 8587
    :cond_1
    return v0
.end method

.method public a(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1351
    const-string v0, "0"

    .line 1392
    return-object v0
.end method

.method protected a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 209
    .line 210
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 214
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 216
    if-gtz v1, :cond_0

    .line 217
    const/4 v0, 0x0

    .line 238
    :goto_1
    return-object v0

    .line 219
    :cond_0
    if-ne v1, v2, :cond_2

    .line 221
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 222
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 223
    if-ne v1, v2, :cond_1

    .line 238
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 229
    :cond_1
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 234
    :cond_2
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2642
    invoke-virtual {p0, p1}, Lcom/wssnps/b;->c(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2643
    if-nez v1, :cond_0

    .line 2645
    const-string v0, "-1\n"

    .line 2655
    :goto_0
    return-object v0

    .line 2647
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2648
    const-string v0, "0\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2649
    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 2651
    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2652
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2649
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2655
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5331
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5335
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateGroup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5337
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 5354
    :cond_0
    :goto_0
    return-object v0

    .line 5340
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5341
    if-eqz v1, :cond_0

    .line 5343
    invoke-static {p2}, Lcom/wssnps/b/y;->b(Ljava/lang/String;)Lcom/wssnps/b/y;

    move-result-object v2

    .line 5344
    if-eqz v2, :cond_0

    .line 5346
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v2, v0}, Lcom/wssnps/b/y;->d(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I

    move-result v0

    .line 5347
    if-nez v0, :cond_2

    .line 5348
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "5\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5350
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v4, 0x0

    const/4 v5, -0x1

    const/4 v3, 0x0

    .line 7999
    .line 8001
    const-string v0, ""

    .line 8003
    sget-object v1, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p3, v1, :cond_1

    .line 8004
    const-string v0, "content://com.android.contacts/raw_contacts/adn"

    .line 8013
    :cond_0
    :goto_0
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateSimContact "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8015
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_f

    .line 8017
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object v6, v1

    .line 8020
    :goto_1
    if-nez v6, :cond_2

    .line 8023
    const-string v3, "ERROR:resolver null."

    .line 8128
    :goto_2
    if-eqz v3, :cond_b

    .line 8129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8131
    :goto_3
    return-object v0

    .line 8005
    :cond_1
    sget-object v1, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p3, v1, :cond_0

    .line 8006
    const-string v0, "content://com.android.contacts/raw_contacts/adn2"

    goto :goto_0

    .line 8027
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 8036
    invoke-static {p2}, Lcom/wssnps/a/s;->b(Ljava/lang/String;)Lcom/wssnps/a/ac;

    move-result-object v8

    .line 8037
    if-nez v8, :cond_3

    .line 8040
    const-string v0, "Error insertSimContact newContactUri"

    .line 8041
    const-string v0, "-1\n"

    goto :goto_3

    .line 8044
    :cond_3
    iget-object v0, v8, Lcom/wssnps/a/ac;->b:Ljava/lang/String;

    .line 8045
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 8047
    iget-object v0, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    if-eqz v0, :cond_9

    .line 8049
    iget-object v0, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 8051
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 8072
    :goto_4
    iget-object v0, v8, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_d

    .line 8074
    iget-object v0, v8, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    move-object v2, v0

    .line 8077
    :goto_5
    invoke-static {p3}, Lcom/wssnps/b;->b(I)I

    move-result v0

    if-ne v0, v10, :cond_c

    .line 8079
    iget-object v0, v8, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_c

    .line 8081
    iget-object v0, v8, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 8085
    :goto_6
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 8086
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    .line 8091
    const-string v9, "newTag"

    invoke-virtual {v8, v9, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8093
    if-eqz v2, :cond_4

    .line 8095
    const-string v1, "newNumber"

    iget-object v2, v2, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-virtual {v8, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8098
    :cond_4
    invoke-static {p3}, Lcom/wssnps/b;->b(I)I

    move-result v1

    if-ne v1, v10, :cond_5

    .line 8100
    if-eqz v0, :cond_5

    .line 8101
    const-string v1, "newEmails"

    iget-object v0, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 8104
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 8109
    int-to-long v0, v0

    :try_start_0
    invoke-static {v7, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v8, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-lez v0, :cond_a

    move v0, v4

    :goto_7
    move v5, v0

    .line 8124
    goto/16 :goto_2

    .line 8053
    :cond_6
    iget-object v0, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 8055
    iget-object v0, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    move-object v1, v0

    goto :goto_4

    .line 8057
    :cond_7
    iget-object v0, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 8059
    iget-object v0, v8, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    move-object v1, v0

    goto/16 :goto_4

    .line 8063
    :cond_8
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_4

    .line 8068
    :cond_9
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_4

    .line 8116
    :cond_a
    :try_start_1
    const-string v3, "ERROR:update fail"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v5

    goto :goto_7

    .line 8119
    :catch_0
    move-exception v0

    .line 8122
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 8123
    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ERROR:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 8131
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    :cond_c
    move-object v0, v3

    goto/16 :goto_6

    :cond_d
    move-object v2, v3

    goto/16 :goto_5

    :cond_e
    move-object v1, v0

    goto/16 :goto_4

    :cond_f
    move-object v6, v3

    goto/16 :goto_1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 8172
    const-string v0, "SMS_SENT"

    .line 8173
    const-string v1, "SMS_DELIVERED"

    .line 8174
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 8175
    sput v5, Lcom/wssnps/b;->w:I

    .line 8176
    sput-object p1, Lcom/wssnps/b;->p:Ljava/lang/String;

    .line 8178
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v5, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 8179
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v5, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 8182
    new-instance v3, Lcom/wssnps/e;

    invoke-direct {v3, p0}, Lcom/wssnps/e;-><init>(Lcom/wssnps/b;)V

    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 8210
    new-instance v0, Lcom/wssnps/f;

    invoke-direct {v0, p0}, Lcom/wssnps/f;-><init>(Lcom/wssnps/b;)V

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 8227
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 8228
    const/4 v2, 0x0

    move-object v1, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 8229
    const-string v0, "0\n"

    return-object v0
.end method

.method public a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5099
    .line 5100
    const/4 v0, 0x0

    .line 5101
    const-string v1, "-1"

    .line 5103
    const-string v1, "insertContactBatch"

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5106
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 5108
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5111
    :cond_0
    if-nez v0, :cond_1

    .line 5114
    const/4 v0, 0x2

    .line 5150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5118
    :cond_1
    invoke-static {p1}, Lcom/wssnps/b/y;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 5119
    if-nez v1, :cond_2

    .line 5122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5125
    :cond_2
    sget-boolean v2, Lcom/wssnps/b;->a:Z

    if-eqz v2, :cond_3

    .line 5127
    const-string v0, "park  applyBatch DB FULL"

    invoke-static {v4, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5128
    sput-boolean v3, Lcom/wssnps/b;->a:Z

    .line 5129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "3\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5132
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v0, v1, v3, v2}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;II)Ljava/lang/String;

    move-result-object v0

    .line 5133
    if-nez v0, :cond_4

    .line 5135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5137
    :cond_4
    const-string v1, "-10"

    if-ne v0, v1, :cond_5

    .line 5139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "7\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 5143
    :cond_5
    sget-object v1, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    if-eqz v1, :cond_6

    .line 5144
    sget-object v1, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 5145
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insert id= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5146
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    if-eqz v0, :cond_0

    .line 255
    const/4 v0, 0x1

    const-string v1, "WsNpsHandler close"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V

    .line 257
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    .line 259
    :cond_0
    return-void
.end method

.method protected a(Ljava/io/InputStream;I)[B
    .locals 2

    .prologue
    .line 243
    new-array v0, p2, [B

    .line 245
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .line 246
    if-eq v1, p2, :cond_0

    .line 247
    const/4 v0, 0x0

    .line 248
    :cond_0
    return-object v0
.end method

.method a(Ljava/lang/String;I)[Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 5025
    const/4 v0, 0x3

    new-array v6, v0, [Ljava/lang/String;

    .line 5026
    const-string v0, ""

    .line 5028
    sget-object v1, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p2, v1, :cond_2

    .line 5029
    const-string v0, "content://com.android.contacts/raw_contacts/adn"

    .line 5033
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 5035
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_3

    .line 5074
    :cond_1
    :goto_1
    return-object v2

    .line 5030
    :cond_2
    sget-object v1, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p2, v1, :cond_0

    .line 5031
    const-string v0, "content://com.android.contacts/raw_contacts/adn2"

    goto :goto_0

    .line 5038
    :cond_3
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5039
    if-eqz v0, :cond_1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 5042
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 5043
    if-eqz v0, :cond_1

    .line 5046
    invoke-static {p2}, Lcom/wssnps/b;->b(I)I

    move-result v1

    .line 5047
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 5048
    const-string v3, "emails"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 5049
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 5053
    :cond_4
    const-string v4, "adn_index"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_7

    .line 5055
    const/4 v2, 0x0

    const-string v4, "name"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v2

    .line 5056
    const/4 v2, 0x1

    const-string v4, "number"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v2

    .line 5058
    if-ne v1, v7, :cond_6

    .line 5060
    const/4 v1, -0x1

    if-eq v3, v1, :cond_5

    .line 5062
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v7

    .line 5073
    :cond_5
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v2, v6

    .line 5074
    goto :goto_1

    .line 5067
    :cond_6
    const-string v1, ""

    aput-object v1, v6, v7

    goto :goto_2

    .line 5071
    :cond_7
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_4

    goto :goto_2
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2660
    .line 2662
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://sms/sim/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2664
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v2, :cond_3

    .line 2666
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2669
    :goto_0
    if-nez v2, :cond_1

    .line 2672
    const-string v1, "ERROR:resolver null."

    .line 2697
    :cond_0
    :goto_1
    if-eqz v1, :cond_2

    .line 2698
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2700
    :goto_2
    return-object v0

    .line 2676
    :cond_1
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2680
    :try_start_0
    const-string v4, ""

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-lez v2, :cond_0

    .line 2682
    const/4 v0, 0x0

    goto :goto_1

    .line 2689
    :catch_0
    move-exception v1

    .line 2692
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ERROR:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2693
    const/4 v3, 0x3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteSimSms ERROR: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move-object v1, v2

    goto :goto_1

    .line 2700
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v2, v1

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 5079
    invoke-virtual {p0, p1, p2}, Lcom/wssnps/b;->a(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    .line 5080
    if-nez v2, :cond_0

    .line 5082
    const-string v0, "-1\n"

    .line 5094
    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    .line 5085
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 5087
    aget-object v3, v2, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5088
    const-string v3, ""

    aput-object v3, v2, v0

    .line 5085
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5091
    :cond_2
    aget-object v0, v2, v5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5092
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v2, v1

    invoke-direct {p0, v1}, Lcom/wssnps/b;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v2, v4

    invoke-direct {p0, v1}, Lcom/wssnps/b;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5094
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v2, v1

    invoke-direct {p0, v1}, Lcom/wssnps/b;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v2, v4

    invoke-direct {p0, v1}, Lcom/wssnps/b;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, v2, v5

    invoke-direct {p0, v1}, Lcom/wssnps/b;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5481
    .line 5486
    const/4 v0, 0x0

    .line 5488
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateContact "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5491
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 5493
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5496
    :cond_0
    if-nez v0, :cond_1

    .line 5499
    const/4 v0, 0x2

    .line 5523
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5503
    :cond_1
    invoke-static {p2}, Lcom/wssnps/b/y;->b(Ljava/lang/String;)Lcom/wssnps/b/y;

    move-result-object v1

    .line 5504
    if-nez v1, :cond_2

    .line 5507
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5509
    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 5510
    invoke-static {v0, v1, v2}, Lcom/wssnps/b/y;->e(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I

    move-result v0

    .line 5512
    if-nez v0, :cond_3

    .line 5514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5518
    :cond_3
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 5519
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected b()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v4, 0x1

    .line 264
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    if-nez v0, :cond_1

    .line 1303
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 269
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    .line 275
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/wssnps/b;->a(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 282
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 284
    invoke-virtual {p0}, Lcom/wssnps/b;->a()V

    .line 285
    invoke-virtual {p0}, Lcom/wssnps/b;->c()V

    .line 286
    if-eqz v5, :cond_2

    .line 287
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 288
    :cond_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 279
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    goto :goto_1

    .line 298
    :cond_3
    new-array v6, v10, [Ljava/lang/String;

    .line 299
    const-string v7, "BEGIN:VC"

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 300
    invoke-virtual {v0, v3, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v3

    .line 302
    if-le v7, v10, :cond_9

    .line 304
    invoke-virtual {v0, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v4

    .line 305
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v9

    .line 312
    :goto_2
    const-string v0, ""

    .line 313
    aget-object v0, v6, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 317
    sparse-switch v7, :sswitch_data_0

    .line 1226
    :try_start_1
    const-string v0, "-1\nERROR:none cmd.\n"

    .line 1227
    const/4 v2, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "cmd not found: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    move v2, v3

    .line 1239
    :goto_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1240
    const-string v0, "-1\nERROR:arg check me.\n"

    .line 1242
    :cond_4
    if-nez v1, :cond_18

    .line 1244
    const/16 v1, 0x84

    if-eq v7, v1, :cond_5

    const/16 v1, 0x83

    if-eq v7, v1, :cond_5

    const/16 v1, 0x85

    if-eq v7, v1, :cond_5

    const/16 v1, 0x86

    if-eq v7, v1, :cond_5

    const/16 v1, 0x87

    if-eq v7, v1, :cond_5

    const/16 v1, 0x8a

    if-eq v7, v1, :cond_5

    const/16 v1, 0x1d8

    if-eq v7, v1, :cond_5

    const/16 v1, 0x1d7

    if-eq v7, v1, :cond_5

    const/16 v1, 0x1d9

    if-eq v7, v1, :cond_5

    const/16 v1, 0x1da

    if-eq v7, v1, :cond_5

    const/16 v1, 0x1de

    if-eq v7, v1, :cond_5

    const/16 v1, 0x1e5

    if-eq v7, v1, :cond_5

    .line 1256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n.\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1258
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/io/OutputStream;->write([B)V

    .line 1271
    :cond_6
    :goto_5
    const/16 v0, 0xae

    if-ne v7, v0, :cond_7

    sget v0, Lcom/wssnps/b;->m:I

    if-ne v0, v4, :cond_7

    .line 1273
    const-string v0, "Restore Reset"

    invoke-static {v4, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1274
    sput v3, Lcom/wssnps/b;->m:I

    .line 1275
    new-instance v0, Lcom/wssnps/c;

    invoke-direct {v0, p0}, Lcom/wssnps/c;-><init>(Lcom/wssnps/b;)V

    .line 1284
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 1285
    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v0, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1288
    :cond_7
    if-eqz v2, :cond_0

    .line 1290
    sget-object v0, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    if-eqz v0, :cond_8

    .line 1294
    :try_start_2
    sget-object v0, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1301
    :cond_8
    :goto_6
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V

    goto/16 :goto_0

    .line 309
    :cond_9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v0, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v4

    goto/16 :goto_2

    :sswitch_0
    move-object v0, v1

    move v2, v3

    .line 321
    goto/16 :goto_4

    .line 324
    :sswitch_1
    :try_start_3
    sget-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->c(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 325
    goto/16 :goto_4

    .line 328
    :sswitch_2
    sget-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->j(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 329
    goto/16 :goto_4

    .line 332
    :sswitch_3
    sget-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->k(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 333
    goto/16 :goto_4

    .line 336
    :sswitch_4
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 337
    const/4 v0, 0x1

    aget-object v0, v6, v0

    sget-object v2, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 341
    :sswitch_5
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 342
    const/4 v0, 0x1

    aget-object v0, v6, v0

    sget-object v2, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->e(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 346
    :sswitch_6
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 347
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    sget-object v6, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v6}, Lcom/wssnps/b/aa;->a()I

    move-result v6

    invoke-virtual {p0, v0, v2, v6}, Lcom/wssnps/b;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 351
    :sswitch_7
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 352
    const/4 v0, 0x1

    aget-object v0, v6, v0

    sget-object v2, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->d(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 356
    :sswitch_8
    sget-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->c(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 357
    goto/16 :goto_4

    .line 360
    :sswitch_9
    sget-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->j(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 361
    goto/16 :goto_4

    .line 364
    :sswitch_a
    sget-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->k(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 365
    goto/16 :goto_4

    .line 368
    :sswitch_b
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 369
    const/4 v0, 0x1

    aget-object v0, v6, v0

    sget-object v2, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->b(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 373
    :sswitch_c
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 374
    const/4 v0, 0x1

    aget-object v0, v6, v0

    sget-object v2, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->e(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 378
    :sswitch_d
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 379
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    sget-object v6, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v6}, Lcom/wssnps/b/aa;->a()I

    move-result v6

    invoke-virtual {p0, v0, v2, v6}, Lcom/wssnps/b;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 383
    :sswitch_e
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 384
    const/4 v0, 0x1

    aget-object v0, v6, v0

    sget-object v2, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->d(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 388
    :sswitch_f
    invoke-virtual {p0}, Lcom/wssnps/b;->d()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 389
    goto/16 :goto_4

    .line 397
    :sswitch_10
    invoke-virtual {p0, v7}, Lcom/wssnps/b;->d(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 398
    goto/16 :goto_4

    .line 406
    :sswitch_11
    invoke-virtual {p0, v7}, Lcom/wssnps/b;->e(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 407
    goto/16 :goto_4

    .line 415
    :sswitch_12
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 416
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 417
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v7, v0}, Lcom/wssnps/b;->c(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 425
    :sswitch_13
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 426
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v7, v0}, Lcom/wssnps/b;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    :sswitch_14
    move-object v0, v1

    move v2, v3

    .line 434
    goto/16 :goto_4

    :sswitch_15
    move-object v0, v1

    move v2, v3

    .line 441
    goto/16 :goto_4

    .line 444
    :sswitch_16
    invoke-virtual {p0}, Lcom/wssnps/b;->e()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 445
    goto/16 :goto_4

    .line 448
    :sswitch_17
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 449
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 453
    :sswitch_18
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 454
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 458
    :sswitch_19
    array-length v0, v6

    const/4 v2, 0x4

    if-lt v0, v2, :cond_19

    .line 459
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    const/4 v8, 0x3

    aget-object v6, v6, v8

    invoke-virtual {p0, v0, v2, v6}, Lcom/wssnps/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 463
    :sswitch_1a
    invoke-virtual {p0}, Lcom/wssnps/b;->M()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 464
    goto/16 :goto_4

    .line 472
    :sswitch_1b
    invoke-virtual {p0, v7}, Lcom/wssnps/b;->g(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 473
    goto/16 :goto_4

    .line 481
    :sswitch_1c
    invoke-virtual {p0, v7}, Lcom/wssnps/b;->h(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 482
    goto/16 :goto_4

    .line 491
    :sswitch_1d
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 492
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 493
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v7, v0}, Lcom/wssnps/b;->e(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 501
    :sswitch_1e
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 502
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v7, v0}, Lcom/wssnps/b;->f(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 506
    :sswitch_1f
    invoke-virtual {p0}, Lcom/wssnps/b;->f()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 507
    goto/16 :goto_4

    .line 510
    :sswitch_20
    invoke-virtual {p0}, Lcom/wssnps/b;->g()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 511
    goto/16 :goto_4

    .line 514
    :sswitch_21
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->l(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 515
    goto/16 :goto_4

    .line 518
    :sswitch_22
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->m(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 519
    goto/16 :goto_4

    .line 522
    :sswitch_23
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 523
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 524
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 528
    :sswitch_24
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->l(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 529
    goto/16 :goto_4

    .line 532
    :sswitch_25
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->m(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 533
    goto/16 :goto_4

    .line 536
    :sswitch_26
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 537
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 538
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 542
    :sswitch_27
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 543
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 544
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 548
    :sswitch_28
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 549
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 550
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 554
    :sswitch_29
    sget-object v0, Lcom/wssnps/b/aa;->b:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->n(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 555
    goto/16 :goto_4

    .line 558
    :sswitch_2a
    sget-object v0, Lcom/wssnps/b/aa;->b:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->o(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 559
    goto/16 :goto_4

    .line 562
    :sswitch_2b
    sget-object v0, Lcom/wssnps/b/aa;->h:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->n(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 563
    goto/16 :goto_4

    .line 566
    :sswitch_2c
    sget-object v0, Lcom/wssnps/b/aa;->h:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->o(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 567
    goto/16 :goto_4

    .line 572
    :sswitch_2d
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 573
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 574
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 578
    :sswitch_2e
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 579
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 580
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->c(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 584
    :sswitch_2f
    sget-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->o(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 585
    goto/16 :goto_4

    .line 588
    :sswitch_30
    sget-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->o(I)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 589
    goto/16 :goto_4

    .line 592
    :sswitch_31
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 593
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 594
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 598
    :sswitch_32
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 599
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 600
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 604
    :sswitch_33
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 605
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 606
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 610
    :sswitch_34
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 611
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 612
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 616
    :sswitch_35
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 617
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 618
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 622
    :sswitch_36
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 623
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 624
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 628
    :sswitch_37
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 629
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 633
    :sswitch_38
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 634
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-static {v0, v2}, Lcom/wssnps/b;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 638
    :sswitch_39
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 639
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 643
    :sswitch_3a
    invoke-virtual {p0}, Lcom/wssnps/b;->L()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 644
    goto/16 :goto_4

    .line 647
    :sswitch_3b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {p0, v7, v2}, Lcom/wssnps/b;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 648
    goto/16 :goto_4

    .line 651
    :sswitch_3c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x1

    aget-object v2, v6, v2

    invoke-virtual {p0, v7, v2}, Lcom/wssnps/b;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 652
    goto/16 :goto_4

    .line 655
    :sswitch_3d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {p0, v7, v2}, Lcom/wssnps/b;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 656
    goto/16 :goto_4

    .line 659
    :sswitch_3e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x1

    aget-object v2, v6, v2

    invoke-virtual {p0, v7, v2}, Lcom/wssnps/b;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 660
    goto/16 :goto_4

    .line 663
    :sswitch_3f
    invoke-virtual {p0}, Lcom/wssnps/b;->H()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 664
    goto/16 :goto_4

    .line 667
    :sswitch_40
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 668
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 672
    :sswitch_41
    invoke-virtual {p0}, Lcom/wssnps/b;->I()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 673
    goto/16 :goto_4

    .line 676
    :sswitch_42
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 677
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->A(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 681
    :sswitch_43
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 682
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 686
    :sswitch_44
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 687
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->B(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 691
    :sswitch_45
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 692
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 696
    :sswitch_46
    invoke-virtual {p0}, Lcom/wssnps/b;->J()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 697
    goto/16 :goto_4

    .line 700
    :sswitch_47
    invoke-virtual {p0}, Lcom/wssnps/b;->K()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 701
    goto/16 :goto_4

    .line 704
    :sswitch_48
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 705
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->C(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 709
    :sswitch_49
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 710
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b;->D(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 714
    :sswitch_4a
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 715
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-static {v0, v2}, Lcom/wssnps/b;->h(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 719
    :sswitch_4b
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 720
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->E(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 724
    :sswitch_4c
    invoke-virtual {p0}, Lcom/wssnps/b;->i()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 725
    goto/16 :goto_4

    .line 728
    :sswitch_4d
    invoke-virtual {p0}, Lcom/wssnps/b;->j()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 729
    goto/16 :goto_4

    .line 732
    :sswitch_4e
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 733
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 737
    :sswitch_4f
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 738
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->m(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 742
    :sswitch_50
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 743
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 747
    :sswitch_51
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 748
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 752
    :sswitch_52
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 753
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->h(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 757
    :sswitch_53
    invoke-virtual {p0}, Lcom/wssnps/b;->k()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 758
    goto/16 :goto_4

    .line 761
    :sswitch_54
    invoke-virtual {p0}, Lcom/wssnps/b;->l()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 762
    goto/16 :goto_4

    .line 765
    :sswitch_55
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 766
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 770
    :sswitch_56
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 771
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 775
    :sswitch_57
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 776
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 780
    :sswitch_58
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 781
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 785
    :sswitch_59
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 786
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->i(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 790
    :sswitch_5a
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 791
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 795
    :sswitch_5b
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 796
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 800
    :sswitch_5c
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 801
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 805
    :sswitch_5d
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 806
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b;->y(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 810
    :sswitch_5e
    invoke-virtual {p0}, Lcom/wssnps/b;->r()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 811
    goto/16 :goto_4

    .line 814
    :sswitch_5f
    invoke-virtual {p0}, Lcom/wssnps/b;->s()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 815
    goto/16 :goto_4

    .line 818
    :sswitch_60
    invoke-virtual {p0}, Lcom/wssnps/b;->t()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 819
    goto/16 :goto_4

    .line 822
    :sswitch_61
    invoke-virtual {p0}, Lcom/wssnps/b;->u()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 823
    goto/16 :goto_4

    .line 826
    :sswitch_62
    invoke-virtual {p0}, Lcom/wssnps/b;->v()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 827
    goto/16 :goto_4

    .line 830
    :sswitch_63
    invoke-virtual {p0}, Lcom/wssnps/b;->w()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 831
    goto/16 :goto_4

    .line 834
    :sswitch_64
    invoke-virtual {p0}, Lcom/wssnps/b;->x()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 835
    goto/16 :goto_4

    .line 838
    :sswitch_65
    invoke-virtual {p0}, Lcom/wssnps/b;->y()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 839
    goto/16 :goto_4

    .line 842
    :sswitch_66
    invoke-virtual {p0}, Lcom/wssnps/b;->z()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 843
    goto/16 :goto_4

    .line 846
    :sswitch_67
    invoke-virtual {p0}, Lcom/wssnps/b;->A()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 847
    goto/16 :goto_4

    .line 850
    :sswitch_68
    invoke-virtual {p0}, Lcom/wssnps/b;->B()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 851
    goto/16 :goto_4

    .line 854
    :sswitch_69
    invoke-virtual {p0}, Lcom/wssnps/b;->C()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 855
    goto/16 :goto_4

    .line 858
    :sswitch_6a
    invoke-virtual {p0}, Lcom/wssnps/b;->D()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 859
    goto/16 :goto_4

    .line 862
    :sswitch_6b
    invoke-virtual {p0}, Lcom/wssnps/b;->E()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 863
    goto/16 :goto_4

    .line 866
    :sswitch_6c
    invoke-virtual {p0}, Lcom/wssnps/b;->F()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 867
    goto/16 :goto_4

    .line 870
    :sswitch_6d
    invoke-virtual {p0}, Lcom/wssnps/b;->G()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 871
    goto/16 :goto_4

    .line 874
    :sswitch_6e
    const-string v0, "0\nok:exit.\n"

    move v2, v4

    .line 876
    goto/16 :goto_4

    .line 879
    :sswitch_6f
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 880
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/l;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 884
    :sswitch_70
    invoke-static {}, Lcom/wssnps/b/l;->b()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 885
    goto/16 :goto_4

    .line 888
    :sswitch_71
    invoke-static {}, Lcom/wssnps/b/l;->a()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 889
    goto/16 :goto_4

    .line 892
    :sswitch_72
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 893
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/l;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 897
    :sswitch_73
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 898
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 899
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->g(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 903
    :sswitch_74
    const-string v0, "0\n"

    move v2, v3

    .line 904
    goto/16 :goto_4

    .line 907
    :sswitch_75
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 908
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 909
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 913
    :sswitch_76
    const-string v0, "0\n"

    move v2, v3

    .line 914
    goto/16 :goto_4

    .line 917
    :sswitch_77
    invoke-static {}, Lcom/wssnps/b/l;->c()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 918
    goto/16 :goto_4

    .line 921
    :sswitch_78
    invoke-static {}, Lcom/wssnps/b/l;->d()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 922
    goto/16 :goto_4

    .line 925
    :sswitch_79
    invoke-static {}, Lcom/wssnps/b/l;->b()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 926
    goto/16 :goto_4

    .line 929
    :sswitch_7a
    invoke-static {}, Lcom/wssnps/b/l;->e()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 930
    goto/16 :goto_4

    .line 933
    :sswitch_7b
    invoke-static {}, Lcom/wssnps/b/a;->a()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 934
    goto/16 :goto_4

    .line 937
    :sswitch_7c
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 938
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 942
    :sswitch_7d
    invoke-virtual {p0}, Lcom/wssnps/b;->m()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 943
    goto/16 :goto_4

    .line 946
    :sswitch_7e
    invoke-virtual {p0}, Lcom/wssnps/b;->n()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 947
    goto/16 :goto_4

    .line 950
    :sswitch_7f
    invoke-virtual {p0}, Lcom/wssnps/b;->o()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 951
    goto/16 :goto_4

    .line 954
    :sswitch_80
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 955
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 959
    :sswitch_81
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 960
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 964
    :sswitch_82
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 965
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 969
    :sswitch_83
    invoke-virtual {p0}, Lcom/wssnps/b;->p()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 970
    goto/16 :goto_4

    .line 973
    :sswitch_84
    invoke-virtual {p0}, Lcom/wssnps/b;->q()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 974
    goto/16 :goto_4

    .line 977
    :sswitch_85
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 978
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->j(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 982
    :sswitch_86
    invoke-virtual {p0}, Lcom/wssnps/b;->N()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 983
    goto/16 :goto_4

    .line 986
    :sswitch_87
    invoke-virtual {p0}, Lcom/wssnps/b;->O()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 987
    goto/16 :goto_4

    .line 990
    :sswitch_88
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 991
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 992
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->F(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 996
    :sswitch_89
    const-string v0, "UP"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;)V

    .line 997
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 998
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->G(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1002
    :sswitch_8a
    array-length v0, v6

    if-lt v0, v10, :cond_19

    .line 1003
    const/4 v0, 0x1

    aget-object v0, v6, v0

    const/4 v2, 0x2

    aget-object v2, v6, v2

    invoke-virtual {p0, v0, v2}, Lcom/wssnps/b;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1007
    :sswitch_8b
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1008
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->H(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1012
    :sswitch_8c
    invoke-virtual {p0}, Lcom/wssnps/b;->P()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1013
    goto/16 :goto_4

    .line 1016
    :sswitch_8d
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1018
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1019
    if-lez v0, :cond_c

    .line 1021
    invoke-virtual {p0, v2, v0}, Lcom/wssnps/b;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 1022
    if-nez v0, :cond_a

    .line 1024
    const-string v0, "-1\nERROR:read bytes.\n"

    move v2, v3

    goto/16 :goto_4

    .line 1028
    :cond_a
    invoke-static {v0}, Lcom/wssnps/b/l;->a([B)[B

    move-result-object v1

    .line 1029
    if-nez v1, :cond_b

    .line 1030
    const-string v0, "-1\nERROR:outBuf is null\n"

    move v2, v3

    goto/16 :goto_4

    .line 1032
    :cond_b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    array-length v2, v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1037
    :cond_c
    const-string v0, "-1\nERROR:length is 0\n"

    move v2, v3

    goto/16 :goto_4

    .line 1043
    :sswitch_8e
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1045
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1046
    if-lez v0, :cond_f

    .line 1048
    invoke-virtual {p0, v2, v0}, Lcom/wssnps/b;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 1049
    if-nez v0, :cond_d

    .line 1051
    const-string v0, "-1\nERROR:read bytes.\n"

    move v2, v3

    goto/16 :goto_4

    .line 1055
    :cond_d
    invoke-static {v0}, Lcom/wssnps/b/l;->b([B)[B

    move-result-object v1

    .line 1056
    if-nez v1, :cond_e

    .line 1057
    const-string v0, "-1\nERROR:outBuf is null\n"

    move v2, v3

    goto/16 :goto_4

    .line 1059
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    array-length v2, v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1064
    :cond_f
    const-string v0, "-1\nERROR:length is 0\n"

    move v2, v3

    goto/16 :goto_4

    .line 1070
    :sswitch_8f
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1072
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1073
    if-lez v0, :cond_12

    .line 1075
    invoke-virtual {p0, v2, v0}, Lcom/wssnps/b;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 1076
    if-nez v0, :cond_10

    .line 1077
    const-string v0, "-1\nERROR:read bytes.\n"

    move v2, v3

    goto/16 :goto_4

    .line 1080
    :cond_10
    invoke-static {v0}, Lcom/wssnps/b/l;->c([B)[B

    move-result-object v1

    .line 1081
    if-nez v1, :cond_11

    .line 1082
    const-string v0, "-1\nERROR:outBuf is null\n"

    move v2, v3

    goto/16 :goto_4

    .line 1084
    :cond_11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    array-length v2, v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1089
    :cond_12
    const-string v0, "-1\nERROR:length is 0\n"

    move v2, v3

    goto/16 :goto_4

    .line 1095
    :sswitch_90
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1097
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1098
    if-lez v0, :cond_15

    .line 1100
    invoke-virtual {p0, v2, v0}, Lcom/wssnps/b;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    .line 1101
    if-nez v0, :cond_13

    .line 1102
    const-string v0, "-1\nERROR:read bytes.\n"

    move v2, v3

    goto/16 :goto_4

    .line 1105
    :cond_13
    invoke-static {v0}, Lcom/wssnps/b/l;->d([B)[B

    move-result-object v1

    .line 1106
    if-nez v1, :cond_14

    .line 1107
    const-string v0, "-1\nERROR:outBuf is null\n"

    move v2, v3

    goto/16 :goto_4

    .line 1109
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    array-length v2, v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1114
    :cond_15
    const-string v0, "-1\nERROR:length is 0\n"

    move v2, v3

    goto/16 :goto_4

    .line 1120
    :sswitch_91
    invoke-virtual {p0}, Lcom/wssnps/b;->Q()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1121
    goto/16 :goto_4

    .line 1124
    :sswitch_92
    invoke-virtual {p0}, Lcom/wssnps/b;->R()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1125
    goto/16 :goto_4

    .line 1128
    :sswitch_93
    invoke-virtual {p0}, Lcom/wssnps/b;->S()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1129
    goto/16 :goto_4

    .line 1132
    :sswitch_94
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1133
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/d;->F(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1137
    :sswitch_95
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1138
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/d;->G(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1142
    :sswitch_96
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1143
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1147
    :sswitch_97
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1148
    invoke-static {}, Lcom/wssnps/b/b;->b()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1152
    :sswitch_98
    array-length v0, v6

    if-lt v0, v9, :cond_19

    .line 1153
    invoke-static {}, Lcom/wssnps/b/b;->c()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1158
    :sswitch_99
    invoke-virtual {p0}, Lcom/wssnps/b;->T()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1159
    goto/16 :goto_4

    .line 1163
    :sswitch_9a
    invoke-static {}, Lcom/wssnps/b;->U()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1164
    goto/16 :goto_4

    .line 1168
    :sswitch_9b
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {p0, v0}, Lcom/wssnps/b;->I(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1169
    goto/16 :goto_4

    .line 1173
    :sswitch_9c
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1174
    goto/16 :goto_4

    .line 1177
    :sswitch_9d
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1178
    goto/16 :goto_4

    .line 1181
    :sswitch_9e
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/b;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1182
    goto/16 :goto_4

    .line 1185
    :sswitch_9f
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1186
    goto/16 :goto_4

    .line 1189
    :sswitch_a0
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1190
    goto/16 :goto_4

    .line 1193
    :sswitch_a1
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lcom/wssnps/b/b;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1194
    goto/16 :goto_4

    .line 1197
    :sswitch_a2
    invoke-static {}, Lcom/wssnps/b/b;->r()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1198
    goto/16 :goto_4

    .line 1201
    :sswitch_a3
    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1202
    const/4 v2, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMultiUser: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1203
    if-nez v0, :cond_16

    .line 1205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/wssnps/smlModelDefine;->s()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1207
    :cond_16
    if-ne v0, v4, :cond_19

    .line 1209
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    goto/16 :goto_4

    .line 1214
    :sswitch_a4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/wssnps/b;->V()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v2, v3

    .line 1215
    goto/16 :goto_4

    .line 1218
    :sswitch_a5
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->s(Landroid/content/Context;)Z

    move-result v0

    .line 1219
    if-eqz v0, :cond_17

    .line 1220
    const-string v0, "0\n"

    move v2, v3

    goto/16 :goto_4

    .line 1222
    :cond_17
    const-string v0, "-1\nERROR:file create exception.\n"
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move v2, v3

    .line 1223
    goto/16 :goto_4

    .line 1232
    :catch_1
    move-exception v0

    move-object v2, v0

    .line 1234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "-1\n ERROR:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1235
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 1236
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "work ERROR:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v10, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 1263
    :cond_18
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/io/OutputStream;->write([B)V

    .line 1264
    array-length v0, v1

    if-lez v0, :cond_6

    .line 1265
    invoke-virtual {v5, v1}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_5

    .line 1296
    :catch_2
    move-exception v0

    .line 1298
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    :cond_19
    move-object v0, v1

    move v2, v3

    goto/16 :goto_4

    .line 317
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0xb -> :sswitch_1
        0xc -> :sswitch_2
        0xd -> :sswitch_3
        0xe -> :sswitch_4
        0xf -> :sswitch_5
        0x10 -> :sswitch_6
        0x11 -> :sswitch_7
        0x15 -> :sswitch_f
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x1a -> :sswitch_18
        0x1b -> :sswitch_19
        0x1c -> :sswitch_1a
        0x28 -> :sswitch_31
        0x29 -> :sswitch_32
        0x2a -> :sswitch_33
        0x2b -> :sswitch_37
        0x2c -> :sswitch_38
        0x2d -> :sswitch_39
        0x2e -> :sswitch_49
        0x2f -> :sswitch_4a
        0x30 -> :sswitch_4b
        0x31 -> :sswitch_42
        0x32 -> :sswitch_43
        0x33 -> :sswitch_44
        0x34 -> :sswitch_45
        0x35 -> :sswitch_3a
        0x36 -> :sswitch_3b
        0x37 -> :sswitch_3c
        0x38 -> :sswitch_3d
        0x39 -> :sswitch_3e
        0x3a -> :sswitch_34
        0x3b -> :sswitch_36
        0x3c -> :sswitch_26
        0x3d -> :sswitch_27
        0x3e -> :sswitch_28
        0x3f -> :sswitch_5a
        0x40 -> :sswitch_35
        0x46 -> :sswitch_2d
        0x47 -> :sswitch_29
        0x48 -> :sswitch_2a
        0x49 -> :sswitch_2b
        0x4a -> :sswitch_2c
        0x4b -> :sswitch_2e
        0x50 -> :sswitch_2d
        0x52 -> :sswitch_2f
        0x53 -> :sswitch_2d
        0x55 -> :sswitch_30
        0x5a -> :sswitch_80
        0x5b -> :sswitch_7e
        0x5c -> :sswitch_7f
        0x5d -> :sswitch_7d
        0x64 -> :sswitch_40
        0x65 -> :sswitch_3f
        0x66 -> :sswitch_41
        0x6e -> :sswitch_48
        0x6f -> :sswitch_47
        0x70 -> :sswitch_46
        0x78 -> :sswitch_10
        0x79 -> :sswitch_10
        0x7a -> :sswitch_10
        0x7b -> :sswitch_10
        0x7c -> :sswitch_10
        0x7e -> :sswitch_11
        0x7f -> :sswitch_11
        0x80 -> :sswitch_11
        0x81 -> :sswitch_11
        0x82 -> :sswitch_11
        0x83 -> :sswitch_12
        0x84 -> :sswitch_12
        0x85 -> :sswitch_12
        0x86 -> :sswitch_12
        0x87 -> :sswitch_12
        0x88 -> :sswitch_10
        0x89 -> :sswitch_11
        0x8a -> :sswitch_12
        0x8c -> :sswitch_7c
        0x8d -> :sswitch_7b
        0x96 -> :sswitch_23
        0x97 -> :sswitch_21
        0x98 -> :sswitch_22
        0x99 -> :sswitch_24
        0x9a -> :sswitch_25
        0x9b -> :sswitch_4e
        0x9c -> :sswitch_4c
        0x9d -> :sswitch_4d
        0x9e -> :sswitch_4f
        0x9f -> :sswitch_50
        0xa0 -> :sswitch_51
        0xa1 -> :sswitch_52
        0xa2 -> :sswitch_55
        0xa3 -> :sswitch_53
        0xa4 -> :sswitch_54
        0xa5 -> :sswitch_56
        0xa6 -> :sswitch_57
        0xa7 -> :sswitch_58
        0xa8 -> :sswitch_59
        0xaa -> :sswitch_6f
        0xab -> :sswitch_71
        0xac -> :sswitch_70
        0xad -> :sswitch_72
        0xae -> :sswitch_7a
        0xaf -> :sswitch_79
        0xb0 -> :sswitch_77
        0xb1 -> :sswitch_78
        0xb2 -> :sswitch_73
        0xb3 -> :sswitch_74
        0xb4 -> :sswitch_75
        0xb5 -> :sswitch_76
        0xb6 -> :sswitch_8
        0xb7 -> :sswitch_9
        0xb8 -> :sswitch_a
        0xb9 -> :sswitch_b
        0xba -> :sswitch_c
        0xbb -> :sswitch_d
        0xbc -> :sswitch_e
        0xc8 -> :sswitch_81
        0xc9 -> :sswitch_82
        0xca -> :sswitch_83
        0xcb -> :sswitch_84
        0xcc -> :sswitch_85
        0x190 -> :sswitch_5b
        0x191 -> :sswitch_5c
        0x192 -> :sswitch_5d
        0x193 -> :sswitch_5e
        0x194 -> :sswitch_5f
        0x195 -> :sswitch_60
        0x196 -> :sswitch_61
        0x197 -> :sswitch_62
        0x198 -> :sswitch_63
        0x199 -> :sswitch_64
        0x19a -> :sswitch_65
        0x19b -> :sswitch_66
        0x19c -> :sswitch_67
        0x19d -> :sswitch_68
        0x19e -> :sswitch_69
        0x19f -> :sswitch_6a
        0x1a0 -> :sswitch_6b
        0x1a1 -> :sswitch_6c
        0x1a2 -> :sswitch_6d
        0x1a4 -> :sswitch_13
        0x1a5 -> :sswitch_14
        0x1a6 -> :sswitch_15
        0x1a7 -> :sswitch_13
        0x1a8 -> :sswitch_14
        0x1a9 -> :sswitch_15
        0x1aa -> :sswitch_13
        0x1ab -> :sswitch_14
        0x1ac -> :sswitch_15
        0x1ad -> :sswitch_13
        0x1ae -> :sswitch_14
        0x1af -> :sswitch_15
        0x1b0 -> :sswitch_13
        0x1b1 -> :sswitch_14
        0x1b2 -> :sswitch_15
        0x1b3 -> :sswitch_86
        0x1b4 -> :sswitch_87
        0x1b5 -> :sswitch_88
        0x1b6 -> :sswitch_89
        0x1b7 -> :sswitch_8a
        0x1b8 -> :sswitch_8b
        0x1b9 -> :sswitch_8c
        0x1c2 -> :sswitch_8d
        0x1c3 -> :sswitch_8e
        0x1c4 -> :sswitch_8f
        0x1c5 -> :sswitch_90
        0x1c7 -> :sswitch_91
        0x1c8 -> :sswitch_92
        0x1c9 -> :sswitch_93
        0x1cc -> :sswitch_1b
        0x1cd -> :sswitch_1b
        0x1ce -> :sswitch_1b
        0x1cf -> :sswitch_1b
        0x1d0 -> :sswitch_1b
        0x1d2 -> :sswitch_1c
        0x1d3 -> :sswitch_1c
        0x1d4 -> :sswitch_1c
        0x1d5 -> :sswitch_1c
        0x1d6 -> :sswitch_1c
        0x1d7 -> :sswitch_1d
        0x1d8 -> :sswitch_1d
        0x1d9 -> :sswitch_1d
        0x1da -> :sswitch_1d
        0x1db -> :sswitch_1d
        0x1dc -> :sswitch_1b
        0x1dd -> :sswitch_1c
        0x1de -> :sswitch_1d
        0x1df -> :sswitch_1e
        0x1e0 -> :sswitch_1e
        0x1e1 -> :sswitch_1e
        0x1e2 -> :sswitch_1e
        0x1e3 -> :sswitch_1e
        0x1e4 -> :sswitch_20
        0x1e5 -> :sswitch_1d
        0x1e6 -> :sswitch_1f
        0x1ea -> :sswitch_94
        0x1eb -> :sswitch_95
        0x1ec -> :sswitch_96
        0x1ed -> :sswitch_97
        0x1ee -> :sswitch_98
        0x1ef -> :sswitch_99
        0x1f0 -> :sswitch_9a
        0x1f4 -> :sswitch_9b
        0x1fe -> :sswitch_9c
        0x1ff -> :sswitch_9d
        0x200 -> :sswitch_9e
        0x201 -> :sswitch_9f
        0x202 -> :sswitch_a0
        0x204 -> :sswitch_a1
        0x205 -> :sswitch_a2
        0x207 -> :sswitch_a3
        0x208 -> :sswitch_a4
        0x209 -> :sswitch_a5
        0x258 -> :sswitch_6e
    .end sparse-switch
.end method

.method public b(ILjava/lang/String;)[Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v6, -0x1

    const/4 v12, 0x6

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 1890
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 1892
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://sms/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1896
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1897
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 2003
    :cond_0
    :goto_0
    return-object v2

    .line 1900
    :cond_1
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1901
    if-eqz v0, :cond_0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 1904
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1905
    if-eqz v1, :cond_0

    .line 1908
    if-eqz v8, :cond_6

    .line 1912
    :try_start_0
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1920
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1924
    :cond_2
    const-string v3, "_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v0, v3, :cond_e

    .line 1928
    const/16 v0, 0x9

    new-array v2, v0, [Ljava/lang/String;

    .line 1929
    const-string v0, "body"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    .line 1930
    aget-object v0, v2, v7

    if-nez v0, :cond_7

    .line 1931
    const-string v0, ""

    aput-object v0, v2, v7

    .line 1934
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v2, v7

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\r"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    .line 1936
    const-string v0, "read"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1937
    const-string v3, "locked"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1939
    if-nez v0, :cond_8

    if-nez v3, :cond_8

    .line 1940
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/wssnps/b;->f:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\r"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v9

    .line 1949
    :cond_3
    :goto_3
    const/4 v0, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "address"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1950
    const/4 v0, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "date"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v10, 0x3e8

    div-long/2addr v4, v10

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1951
    const/4 v0, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reserved"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1953
    const-string v0, "type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v9, :cond_b

    .line 1955
    const/4 v0, 0x5

    const-string v3, "1\r"

    aput-object v3, v2, v0

    .line 1956
    const-string v0, "0\r"

    aput-object v0, v2, v12

    .line 1975
    :cond_4
    :goto_4
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v3, "SEC_FLOATING_FEATURE_COMMON_USE_GED_MULTISIM"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1977
    const-string v0, "sim_slot"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1978
    if-eq v0, v6, :cond_10

    .line 1979
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1981
    :goto_5
    const/4 v3, 0x7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\r"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 1984
    const-string v0, "seen"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1985
    if-eq v0, v6, :cond_f

    .line 1986
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1987
    :goto_6
    const/16 v3, 0x8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\r"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 1989
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStringSmsGetItem "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2002
    :cond_5
    :goto_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1914
    :catch_0
    move-exception v0

    .line 1916
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_6
    move v0, v6

    goto/16 :goto_1

    .line 1933
    :cond_7
    aget-object v0, v2, v7

    invoke-direct {p0, v0}, Lcom/wssnps/b;->Q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v7

    goto/16 :goto_2

    .line 1941
    :cond_8
    if-ne v0, v9, :cond_9

    if-nez v3, :cond_9

    .line 1942
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/wssnps/b;->e:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\r"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v9

    goto/16 :goto_3

    .line 1943
    :cond_9
    if-nez v0, :cond_a

    if-ne v3, v9, :cond_a

    .line 1944
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/wssnps/b;->c:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\r"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v9

    goto/16 :goto_3

    .line 1945
    :cond_a
    if-ne v0, v9, :cond_3

    if-ne v3, v9, :cond_3

    .line 1946
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/wssnps/b;->b:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\r"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v9

    goto/16 :goto_3

    .line 1958
    :cond_b
    const-string v0, "type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_c

    .line 1960
    const/4 v0, 0x5

    const-string v3, "2\r"

    aput-object v3, v2, v0

    .line 1961
    const-string v0, "0\r"

    aput-object v0, v2, v12

    goto/16 :goto_4

    .line 1963
    :cond_c
    const-string v0, "type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_d

    .line 1965
    const/4 v0, 0x5

    const-string v3, "3\r"

    aput-object v3, v2, v0

    .line 1966
    const-string v0, "0\r"

    aput-object v0, v2, v12

    goto/16 :goto_4

    .line 1968
    :cond_d
    const-string v0, "type"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v12, :cond_4

    .line 1970
    const/4 v0, 0x5

    const-string v3, "4\r"

    aput-object v3, v2, v0

    .line 1971
    const-string v0, "0\r"

    aput-object v0, v2, v12

    goto/16 :goto_4

    .line 2000
    :cond_e
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    goto/16 :goto_7

    :cond_f
    move v0, v7

    goto/16 :goto_6

    :cond_10
    move v0, v6

    goto/16 :goto_5
.end method

.method public c(I)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1564
    const-string v0, ""

    .line 1576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSimContactInfo "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1578
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;I)I

    move-result v4

    .line 1579
    const/4 v0, -0x1

    if-ne v4, v0, :cond_0

    .line 1580
    const-string v0, "-1\nsim is absent\n"

    .line 1608
    :goto_0
    return-object v0

    .line 1584
    :cond_0
    invoke-static {p1}, Lcom/wssnps/b;->b(I)I

    move-result v5

    .line 1585
    if-ne v5, v7, :cond_1

    .line 1587
    invoke-static {v6, p1}, Lcom/wssnps/b;->a(II)[I

    move-result-object v0

    .line 1588
    if-eqz v0, :cond_2

    .line 1590
    aget v3, v0, v1

    .line 1591
    aget v2, v0, v6

    .line 1592
    aget v0, v0, v7

    .line 1605
    :goto_1
    const-string v6, "0\n"

    .line 1606
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1597
    :cond_1
    invoke-static {p1}, Lcom/wssnps/b;->a(I)[I

    move-result-object v0

    .line 1598
    if-eqz v0, :cond_2

    .line 1600
    const/4 v2, 0x3

    aget v2, v0, v2

    .line 1601
    const/4 v3, 0x4

    aget v0, v0, v3

    move v3, v2

    move v2, v0

    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public c(ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2091
    invoke-virtual {p0, p1, p2}, Lcom/wssnps/b;->b(ILjava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2092
    if-nez v1, :cond_0

    .line 2094
    const-string v0, "-1\n"

    .line 2104
    :goto_0
    return-object v0

    .line 2096
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2097
    const-string v0, "0\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2098
    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 2100
    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2101
    const-string v3, "\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2098
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2104
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 5412
    .line 5414
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5416
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GetContact "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5418
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 5420
    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5421
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5427
    :goto_0
    return-object v0

    .line 5424
    :cond_0
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5425
    invoke-static {v1, p1, p2}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)Lcom/wssnps/b/y;

    move-result-object v1

    .line 5426
    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/wssnps/b/y;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5427
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5528
    .line 5529
    const-string v0, "-1"

    .line 5530
    const/4 v0, 0x0

    .line 5532
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateContactBatch "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5534
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 5536
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5539
    :cond_0
    if-nez v0, :cond_1

    .line 5541
    const/4 v0, 0x2

    .line 5570
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5545
    :cond_1
    invoke-static {p2}, Lcom/wssnps/b/y;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 5546
    if-nez v1, :cond_2

    .line 5549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5551
    :cond_2
    invoke-static {v0, v1, p1}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5553
    if-nez v0, :cond_3

    .line 5555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5557
    :cond_3
    const-string v1, "-10"

    if-ne v0, v1, :cond_4

    .line 5559
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "7\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5563
    :cond_4
    sget-object v1, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    if-eqz v1, :cond_5

    .line 5564
    sget-object v1, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 5565
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5566
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1397
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssnps/b;->v:Z

    .line 1399
    return-void
.end method

.method public c(Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2745
    const-string v0, "content://sms/sim"

    .line 2747
    const/4 v6, -0x1

    .line 2749
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2750
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 2796
    :cond_0
    :goto_0
    return-object v2

    .line 2753
    :cond_1
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2754
    if-eqz v0, :cond_0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 2757
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2758
    if-eqz v3, :cond_0

    .line 2761
    const-string v0, "index_on_sim"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 2764
    if-eqz p1, :cond_7

    .line 2765
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2767
    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2771
    :cond_2
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v0, v4, :cond_4

    .line 2773
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2774
    const/4 v0, 0x0

    :try_start_1
    const-string v1, "address"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 2775
    const/4 v0, 0x1

    const-string v1, "body"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 2776
    const/4 v0, 0x2

    const-string v1, "date"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 2778
    const/4 v0, 0x0

    const/4 v1, 0x0

    aget-object v1, v2, v1

    invoke-direct {p0, v1}, Lcom/wssnps/b;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 2779
    const/4 v0, 0x1

    const/4 v1, 0x1

    aget-object v1, v2, v1

    invoke-direct {p0, v1}, Lcom/wssnps/b;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 2793
    :goto_2
    if-eqz v3, :cond_3

    .line 2794
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_3
    move-object v2, v0

    .line 2796
    goto :goto_0

    .line 2784
    :cond_4
    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-nez v4, :cond_2

    :cond_5
    move-object v0, v2

    goto :goto_2

    .line 2787
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    .line 2789
    :goto_4
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2793
    if-eqz v3, :cond_3

    .line 2794
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 2793
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_6

    .line 2794
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 2787
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    goto :goto_4

    :cond_7
    move v0, v6

    goto :goto_1
.end method

.method public d()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2580
    const-string v0, "content://sms/sim"

    .line 2581
    const-string v1, ""

    .line 2584
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2586
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 2587
    const-string v0, "-1\ncontext null\n"

    .line 2603
    :goto_0
    return-object v0

    .line 2588
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2589
    if-nez v0, :cond_1

    .line 2590
    const-string v0, "-1\nresolver null\n"

    goto :goto_0

    :cond_1
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 2592
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2593
    if-nez v0, :cond_2

    .line 2594
    const-string v0, "-1\nquery null\n"

    goto :goto_0

    .line 2596
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 2597
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2599
    const/16 v0, 0x1f4

    .line 2600
    const-string v2, "0\n"

    .line 2601
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d(I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1613
    const-string v0, ""

    .line 1614
    const-string v3, ""

    .line 1616
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    .line 1651
    :goto_0
    const-string v4, "VZW"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/wssnps/smlModelDefine;->C()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1652
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND reserved=\'0\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1654
    :cond_0
    const-string v4, "content://sms/"

    .line 1656
    if-nez v0, :cond_4

    .line 1658
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1660
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 1661
    const-string v0, "-1\ncontext null\n"

    .line 1676
    :goto_1
    return-object v0

    .line 1620
    :sswitch_0
    const-string v0, "(type=\'2\')"

    move-object v3, v0

    move v0, v1

    .line 1621
    goto :goto_0

    .line 1625
    :sswitch_1
    const-string v0, "(type=\'1\')"

    move-object v3, v0

    move v0, v1

    .line 1626
    goto :goto_0

    .line 1630
    :sswitch_2
    const-string v0, "(type=\'3\')"

    move-object v3, v0

    move v0, v1

    .line 1631
    goto :goto_0

    .line 1635
    :sswitch_3
    const-string v0, "(type=\'6\')"

    move-object v3, v0

    move v0, v1

    .line 1636
    goto :goto_0

    .line 1639
    :sswitch_4
    const/4 v0, 0x1

    .line 1640
    goto :goto_0

    .line 1644
    :sswitch_5
    const-string v0, "(type=\'2\' OR type=\'1\' OR type=\'3\' OR type=\'6\')"

    move-object v3, v0

    move v0, v1

    .line 1645
    goto :goto_0

    .line 1662
    :cond_1
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1663
    if-nez v0, :cond_2

    .line 1664
    const-string v0, "-1\nresolver null\n"

    goto :goto_1

    :cond_2
    move-object v4, v2

    move-object v5, v2

    .line 1666
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1667
    if-nez v0, :cond_3

    .line 1668
    const-string v0, "-1\nquery null\n"

    goto :goto_1

    .line 1670
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 1671
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1674
    :cond_4
    const-string v0, "0\n"

    .line 1675
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1616
    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0x79 -> :sswitch_1
        0x7a -> :sswitch_2
        0x7b -> :sswitch_3
        0x7c -> :sswitch_4
        0x88 -> :sswitch_5
    .end sparse-switch
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5243
    .line 5244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5247
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGroupItem "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5249
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 5267
    :cond_0
    :goto_0
    return-object v0

    .line 5252
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5253
    if-eqz v1, :cond_0

    .line 5255
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;I)Lcom/wssnps/b/y;

    move-result-object v0

    .line 5256
    if-nez v0, :cond_2

    .line 5258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "5\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5262
    :cond_2
    invoke-virtual {v0}, Lcom/wssnps/b/y;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5263
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 7755
    .line 7757
    const-string v0, ""

    .line 7759
    sget-object v3, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v3}, Lcom/wssnps/b/aa;->a()I

    move-result v3

    if-ne p2, v3, :cond_1

    .line 7760
    const-string v0, "content://com.android.contacts/raw_contacts/adn"

    .line 7766
    :cond_0
    :goto_0
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteSimContact "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7768
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v3, :cond_5

    .line 7770
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 7773
    :goto_1
    if-nez v3, :cond_2

    .line 7776
    const-string v0, "ERROR:resolver null."

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 7822
    :goto_2
    if-eqz v1, :cond_4

    .line 7824
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7826
    :goto_3
    return-object v0

    .line 7761
    :cond_1
    sget-object v3, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v3}, Lcom/wssnps/b/aa;->a()I

    move-result v3

    if-ne p2, v3, :cond_0

    .line 7762
    const-string v0, "content://com.android.contacts/raw_contacts/adn2"

    goto :goto_0

    .line 7780
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 7781
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 7783
    int-to-long v4, v4

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    const-string v4, ""

    invoke-virtual {v3, v0, v4, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 7784
    if-lez v0, :cond_3

    .line 7786
    const/4 v0, 0x0

    move-object v1, v2

    goto :goto_2

    .line 7791
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move v6, v1

    move-object v1, v0

    move v0, v6

    goto :goto_2

    .line 7826
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_5
    move-object v3, v2

    goto/16 :goto_1
.end method

.method public e()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2608
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2609
    const-string v0, "content://sms/sim"

    .line 2611
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2613
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 2614
    const-string v0, "-1\ncontext null\n"

    .line 2637
    :goto_0
    return-object v0

    .line 2615
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2616
    if-nez v0, :cond_1

    .line 2617
    const-string v0, "-1\nresolver null\n"

    goto :goto_0

    :cond_1
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 2619
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2621
    if-nez v0, :cond_2

    .line 2622
    const-string v0, "-1\nquery null\n"

    goto :goto_0

    .line 2624
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 2625
    const-string v2, "index_on_sim"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 2627
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2631
    :cond_3
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2632
    const-string v3, "\n"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2633
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2636
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2637
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public e(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1753
    const-string v0, ""

    .line 1754
    const-string v0, ""

    .line 1755
    sparse-switch p1, :sswitch_data_0

    .line 1781
    :goto_0
    const-string v1, "VZW"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/wssnps/smlModelDefine;->C()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1782
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND reserved=\'0\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1784
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1786
    const-string v0, "content://sms/"

    .line 1788
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1790
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1791
    const-string v0, "-1\ncontext null\n"

    .line 1813
    :goto_2
    return-object v0

    .line 1758
    :sswitch_0
    const-string v0, "(type=\'2\')"

    goto :goto_0

    .line 1762
    :sswitch_1
    const-string v0, "(type=\'1\')"

    goto :goto_0

    .line 1766
    :sswitch_2
    const-string v0, "(type=\'3\')"

    goto :goto_0

    .line 1770
    :sswitch_3
    const-string v0, "(type=\'6\')"

    goto :goto_0

    .line 1774
    :sswitch_4
    const-string v0, "(type=\'2\' OR type=\'1\' OR type=\'3\' OR type=\'6\')"

    goto :goto_0

    .line 1792
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1793
    if-nez v0, :cond_1

    .line 1794
    const-string v0, "-1\nresolver null\n"

    goto :goto_2

    :cond_1
    move-object v4, v2

    move-object v5, v2

    .line 1796
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1797
    if-nez v0, :cond_2

    .line 1798
    const-string v0, "-1\nquery null\n"

    goto :goto_2

    .line 1800
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 1801
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1805
    :cond_3
    const-string v2, "_id"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1806
    const-string v2, ","

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1807
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1809
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1811
    const-string v0, "0\n"

    .line 1812
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v3, v0

    goto/16 :goto_1

    .line 1755
    nop

    :sswitch_data_0
    .sparse-switch
        0x7e -> :sswitch_0
        0x7f -> :sswitch_1
        0x80 -> :sswitch_2
        0x81 -> :sswitch_3
        0x89 -> :sswitch_4
    .end sparse-switch
.end method

.method public e(ILjava/lang/String;)Ljava/lang/String;
    .locals 56

    .prologue
    .line 2986
    const-string v31, ""

    .line 2993
    const/16 v35, 0x0

    .line 2994
    const/16 v30, 0x0

    .line 2995
    const/16 v29, 0x0

    .line 2996
    const/16 v28, 0x0

    .line 2997
    const/16 v27, 0x0

    .line 2998
    const/16 v26, 0x0

    .line 2999
    const-string v25, ""

    .line 3000
    const-string v24, ""

    .line 3001
    const-string v23, ""

    .line 3002
    const-string v22, ""

    .line 3003
    const/16 v21, 0x0

    .line 3004
    const/16 v20, 0x0

    .line 3005
    const/16 v19, 0x0

    .line 3006
    const/16 v18, 0x0

    .line 3007
    const/16 v17, 0x0

    .line 3008
    const/16 v16, 0x0

    .line 3009
    const/4 v15, 0x0

    .line 3010
    const/4 v14, 0x0

    .line 3011
    const/4 v13, 0x0

    .line 3012
    const/4 v12, 0x0

    .line 3013
    const/4 v11, 0x0

    .line 3014
    const/16 v36, 0x8

    .line 3015
    const/16 v34, 0x0

    .line 3016
    const/16 v33, 0x0

    .line 3018
    const/4 v10, -0x1

    .line 3019
    const/4 v9, 0x0

    .line 3021
    const-string v32, ""

    .line 3022
    const-string v37, "insert-address-token"

    .line 3023
    new-instance v38, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v38

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3024
    new-instance v39, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v39

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3025
    new-instance v40, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v40

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3026
    new-instance v41, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v41

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3027
    new-instance v42, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v42

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3028
    new-instance v43, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v43

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3029
    new-instance v44, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v44

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3030
    new-instance v45, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v45

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3031
    new-instance v46, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v46

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3032
    new-instance v47, Ljava/lang/StringBuffer;

    const-string v3, ""

    move-object/from16 v0, v47

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3037
    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    .line 3039
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 3041
    const-string v3, "-1\ncontext null\n"

    .line 3578
    :goto_0
    return-object v3

    .line 3046
    :cond_0
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 3048
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 3049
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mms/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 3051
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v49

    .line 3052
    if-nez v49, :cond_1

    .line 3054
    const/4 v3, 0x1

    const-string v4, "pducursor null\n"

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3055
    const-string v3, "-1\npducursor null\n"

    goto :goto_0

    .line 3058
    :cond_1
    invoke-interface/range {v49 .. v49}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    move v6, v9

    move v7, v10

    move v8, v11

    move v5, v12

    move-object/from16 v4, v31

    move v10, v14

    move v11, v15

    move/from16 v12, v16

    move v9, v13

    move/from16 v14, v18

    move/from16 v15, v19

    move/from16 v16, v20

    move/from16 v13, v17

    move-object/from16 v18, v22

    move-object/from16 v19, v23

    move-object/from16 v20, v24

    move/from16 v17, v21

    move/from16 v22, v26

    move/from16 v23, v27

    move/from16 v24, v28

    move-object/from16 v21, v25

    move/from16 v26, v30

    move/from16 v25, v29

    .line 3062
    :goto_1
    const-string v27, "m_type"

    move-object/from16 v0, v49

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    move-object/from16 v0, v49

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    .line 3063
    const/16 v28, 0x86

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_3

    move/from16 v53, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v13

    move v13, v14

    move v14, v15

    move/from16 v15, v16

    move/from16 v16, v17

    move-object/from16 v17, v18

    move-object/from16 v18, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v21, v22

    move/from16 v22, v23

    move/from16 v23, v24

    move/from16 v24, v25

    move/from16 v25, v26

    move-object/from16 v26, v4

    move/from16 v4, v53

    move/from16 v54, v5

    move v5, v7

    move/from16 v7, v54

    .line 3272
    :goto_2
    invoke-interface/range {v49 .. v49}, Landroid/database/Cursor;->moveToNext()Z

    move-result v27

    if-nez v27, :cond_40

    move/from16 v26, v21

    move/from16 v27, v22

    move/from16 v28, v23

    move/from16 v29, v24

    move/from16 v30, v25

    move/from16 v21, v16

    move-object/from16 v22, v17

    move-object/from16 v23, v18

    move-object/from16 v24, v19

    move-object/from16 v25, v20

    move/from16 v19, v14

    move/from16 v20, v15

    move/from16 v16, v11

    move/from16 v17, v12

    move/from16 v18, v13

    move v15, v10

    move v14, v9

    move v11, v6

    move v12, v7

    move v13, v8

    move v10, v5

    move v9, v4

    .line 3274
    :cond_2
    invoke-interface/range {v49 .. v49}, Landroid/database/Cursor;->close()V

    .line 3276
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://mms/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/addr"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 3277
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 3278
    if-nez v4, :cond_16

    .line 3280
    const/4 v3, 0x1

    const-string v4, "addrcursor null\n"

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3281
    const-string v3, "-1\naddrcursor null\n"

    goto/16 :goto_0

    .line 3065
    :cond_3
    const/16 v6, 0x80

    move/from16 v0, v27

    if-ne v0, v6, :cond_a

    .line 3066
    const/16 v14, 0x80

    .line 3070
    :cond_4
    :goto_3
    const-string v6, "_id"

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    .line 3072
    const-string v6, "m_id"

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 3073
    const-string v6, "m_id"

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 3075
    :cond_5
    const-string v6, "sub"

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 3076
    const-string v6, "sub"

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 3078
    :cond_6
    const-string v6, "date"

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 3080
    const-string v6, "reserved"

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 3082
    const-string v8, "read"

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 3083
    const-string v8, "date_sent"

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 3085
    const-string v8, "ct_t"

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 3086
    const-string v8, "ct_t"

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 3088
    :cond_7
    const-string v8, "exp"

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 3089
    const-string v8, "locked"

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 3091
    const-string v8, "seen"

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v49

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 3092
    const-string v9, "thread_id"

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 3094
    const-string v9, "rr"

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 3095
    const/16 v10, 0x80

    if-ne v9, v10, :cond_b

    .line 3096
    const/4 v12, 0x0

    .line 3102
    :goto_4
    const-string v9, "d_rpt"

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 3103
    const/16 v10, 0x80

    if-ne v9, v10, :cond_d

    .line 3104
    const/4 v11, 0x0

    .line 3110
    :goto_5
    const-string v9, "tr_id"

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3f

    .line 3111
    const-string v9, "tr_id"

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 3113
    :goto_6
    const-string v9, "resp_st"

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 3114
    sparse-switch v9, :sswitch_data_0

    .line 3198
    const/4 v10, 0x0

    .line 3202
    :goto_7
    const-string v9, "retr_st"

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    move-object/from16 v0, v49

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 3203
    sparse-switch v9, :sswitch_data_1

    .line 3233
    const/4 v9, 0x0

    .line 3237
    :goto_8
    const-string v18, "m_cls"

    move-object/from16 v0, v49

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, v49

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_8

    .line 3238
    const-string v4, "m_cls"

    move-object/from16 v0, v49

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v49

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 3240
    :cond_8
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v26

    .line 3242
    const-string v4, "personal"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 3243
    const/4 v4, 0x1

    .line 3253
    :goto_9
    const-string v18, "VZW"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_9

    .line 3255
    const-string v5, "pri"

    move-object/from16 v0, v49

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v49

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 3256
    const/16 v18, 0x80

    move/from16 v0, v18

    if-ne v5, v0, :cond_13

    .line 3257
    const/4 v5, 0x1

    .line 3266
    :cond_9
    :goto_a
    const-string v18, "msg_box"

    move-object/from16 v0, v49

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, v49

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 3268
    const-string v27, "sim_slot"

    move-object/from16 v0, v49

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 3269
    const/16 v28, -0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_41

    .line 3270
    const-string v7, "sim_slot"

    move-object/from16 v0, v49

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v49

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    move/from16 v53, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v13

    move v13, v14

    move v14, v4

    move/from16 v4, v53

    move/from16 v54, v5

    move v5, v7

    move/from16 v7, v54

    move-object/from16 v55, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v21, v18

    move-object/from16 v18, v55

    goto/16 :goto_2

    .line 3067
    :cond_a
    const/16 v6, 0x84

    move/from16 v0, v27

    if-ne v0, v6, :cond_4

    .line 3068
    const/16 v14, 0x84

    goto/16 :goto_3

    .line 3097
    :cond_b
    const/16 v10, 0x81

    if-ne v9, v10, :cond_c

    .line 3098
    const/4 v12, 0x1

    goto/16 :goto_4

    .line 3100
    :cond_c
    const/4 v12, 0x2

    goto/16 :goto_4

    .line 3105
    :cond_d
    const/16 v10, 0x81

    if-ne v9, v10, :cond_e

    .line 3106
    const/4 v11, 0x1

    goto/16 :goto_5

    .line 3108
    :cond_e
    const/4 v11, 0x2

    goto/16 :goto_5

    .line 3117
    :sswitch_0
    const/16 v10, 0x80

    .line 3118
    goto/16 :goto_7

    .line 3120
    :sswitch_1
    const/16 v10, 0x81

    .line 3121
    goto/16 :goto_7

    .line 3123
    :sswitch_2
    const/16 v10, 0x82

    .line 3124
    goto/16 :goto_7

    .line 3126
    :sswitch_3
    const/16 v10, 0x83

    .line 3127
    goto/16 :goto_7

    .line 3129
    :sswitch_4
    const/16 v10, 0x84

    .line 3130
    goto/16 :goto_7

    .line 3132
    :sswitch_5
    const/16 v10, 0x85

    .line 3133
    goto/16 :goto_7

    .line 3135
    :sswitch_6
    const/16 v10, 0x86

    .line 3136
    goto/16 :goto_7

    .line 3138
    :sswitch_7
    const/16 v10, 0x87

    .line 3139
    goto/16 :goto_7

    .line 3141
    :sswitch_8
    const/16 v10, 0x88

    .line 3142
    goto/16 :goto_7

    .line 3144
    :sswitch_9
    const/16 v10, 0xc0

    .line 3145
    goto/16 :goto_7

    .line 3147
    :sswitch_a
    const/16 v10, 0xc1

    .line 3148
    goto/16 :goto_7

    .line 3150
    :sswitch_b
    const/16 v10, 0xc2

    .line 3151
    goto/16 :goto_7

    .line 3153
    :sswitch_c
    const/16 v10, 0xc3

    .line 3154
    goto/16 :goto_7

    .line 3156
    :sswitch_d
    const/16 v10, 0xc4

    .line 3157
    goto/16 :goto_7

    .line 3159
    :sswitch_e
    const/16 v10, 0xe0

    .line 3160
    goto/16 :goto_7

    .line 3162
    :sswitch_f
    const/16 v10, 0xe1

    .line 3163
    goto/16 :goto_7

    .line 3165
    :sswitch_10
    const/16 v10, 0xe2

    .line 3166
    goto/16 :goto_7

    .line 3168
    :sswitch_11
    const/16 v10, 0xe3

    .line 3169
    goto/16 :goto_7

    .line 3171
    :sswitch_12
    const/16 v10, 0xe4

    .line 3172
    goto/16 :goto_7

    .line 3174
    :sswitch_13
    const/16 v10, 0xe5

    .line 3175
    goto/16 :goto_7

    .line 3177
    :sswitch_14
    const/16 v10, 0xe6

    .line 3178
    goto/16 :goto_7

    .line 3180
    :sswitch_15
    const/16 v10, 0xe7

    .line 3181
    goto/16 :goto_7

    .line 3183
    :sswitch_16
    const/16 v10, 0xe8

    .line 3184
    goto/16 :goto_7

    .line 3186
    :sswitch_17
    const/16 v10, 0xe9

    .line 3187
    goto/16 :goto_7

    .line 3189
    :sswitch_18
    const/16 v10, 0xea

    .line 3190
    goto/16 :goto_7

    .line 3192
    :sswitch_19
    const/16 v10, 0xeb

    .line 3193
    goto/16 :goto_7

    .line 3195
    :sswitch_1a
    const/16 v10, 0xff

    .line 3196
    goto/16 :goto_7

    .line 3206
    :sswitch_1b
    const/16 v9, 0x80

    .line 3207
    goto/16 :goto_8

    .line 3209
    :sswitch_1c
    const/16 v9, 0xc0

    .line 3210
    goto/16 :goto_8

    .line 3212
    :sswitch_1d
    const/16 v9, 0xc1

    .line 3213
    goto/16 :goto_8

    .line 3215
    :sswitch_1e
    const/16 v9, 0xc2

    .line 3216
    goto/16 :goto_8

    .line 3218
    :sswitch_1f
    const/16 v9, 0xe0

    .line 3219
    goto/16 :goto_8

    .line 3221
    :sswitch_20
    const/16 v9, 0xe1

    .line 3222
    goto/16 :goto_8

    .line 3224
    :sswitch_21
    const/16 v9, 0xe2

    .line 3225
    goto/16 :goto_8

    .line 3227
    :sswitch_22
    const/16 v9, 0xe3

    .line 3228
    goto/16 :goto_8

    .line 3230
    :sswitch_23
    const/16 v9, 0xff

    .line 3231
    goto/16 :goto_8

    .line 3244
    :cond_f
    const-string v4, "advertisement"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 3245
    const/4 v4, 0x2

    goto/16 :goto_9

    .line 3246
    :cond_10
    const-string v4, "informational"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 3247
    const/4 v4, 0x3

    goto/16 :goto_9

    .line 3248
    :cond_11
    const-string v4, "auto"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 3249
    const/4 v4, 0x4

    goto/16 :goto_9

    .line 3251
    :cond_12
    const/4 v4, 0x0

    goto/16 :goto_9

    .line 3258
    :cond_13
    const/16 v18, 0x81

    move/from16 v0, v18

    if-ne v5, v0, :cond_14

    .line 3259
    const/4 v5, 0x2

    goto/16 :goto_a

    .line 3260
    :cond_14
    const/16 v18, 0x82

    move/from16 v0, v18

    if-ne v5, v0, :cond_15

    .line 3261
    const/4 v5, 0x3

    goto/16 :goto_a

    .line 3263
    :cond_15
    const/4 v5, 0x0

    goto/16 :goto_a

    .line 3284
    :cond_16
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 3290
    :cond_17
    const-string v5, "type"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 3291
    const-string v6, "address"

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 3293
    const/16 v7, 0x97

    if-ne v5, v7, :cond_1b

    .line 3295
    move-object/from16 v0, v37

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_19

    .line 3297
    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-eqz v5, :cond_18

    .line 3298
    const-string v5, ","

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3299
    :cond_18
    move-object/from16 v0, v39

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3319
    :cond_19
    :goto_b
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_17

    .line 3321
    :cond_1a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 3323
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mid = \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 3324
    const-string v4, "content://mms/part"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 3325
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v37

    .line 3326
    if-nez v37, :cond_1f

    .line 3328
    const/4 v3, 0x1

    const-string v4, "partcursor null\n"

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3329
    const-string v3, "-1\npartcursor null\n"

    goto/16 :goto_0

    .line 3302
    :cond_1b
    const/16 v7, 0x89

    if-ne v5, v7, :cond_1d

    .line 3304
    move-object/from16 v0, v37

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_19

    .line 3306
    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-eqz v5, :cond_1c

    .line 3307
    const-string v5, ","

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3308
    :cond_1c
    move-object/from16 v0, v38

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_b

    .line 3313
    :cond_1d
    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-eqz v5, :cond_1e

    .line 3314
    const-string v5, ","

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3316
    :cond_1e
    move-object/from16 v0, v39

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_b

    .line 3332
    :cond_1f
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3e

    .line 3334
    const/4 v8, 0x0

    .line 3335
    const/4 v7, 0x0

    .line 3336
    const/4 v6, 0x0

    .line 3337
    const/4 v5, 0x0

    .line 3338
    const/4 v4, 0x0

    .line 3339
    const-string v3, ""

    move/from16 v30, v5

    move/from16 v31, v6

    move/from16 v5, v33

    move/from16 v6, v34

    move/from16 v33, v35

    move/from16 v53, v8

    move v8, v4

    move-object/from16 v4, v32

    move/from16 v32, v53

    move-object/from16 v54, v3

    move v3, v7

    move-object/from16 v7, v54

    .line 3348
    :cond_20
    const-string v34, "_id"

    move-object/from16 v0, v37

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move-object/from16 v0, v37

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v49

    .line 3349
    const-string v34, "_data"

    move-object/from16 v0, v37

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move-object/from16 v0, v37

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 3350
    const-string v35, "text"

    move-object/from16 v0, v37

    move-object/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    move-object/from16 v0, v37

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v50

    .line 3353
    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_29

    .line 3355
    add-int/lit8 v32, v32, 0x1

    .line 3356
    const/16 v34, 0x2

    move/from16 v0, v32

    move/from16 v1, v34

    if-lt v0, v1, :cond_21

    .line 3357
    const-string v34, ";"

    move-object/from16 v0, v43

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3358
    :cond_21
    const-string v34, ""

    .line 3359
    move-object/from16 v0, v43

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3415
    :cond_22
    :goto_c
    const-string v34, "cid"

    move-object/from16 v0, v37

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move-object/from16 v0, v37

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 3416
    add-int/lit8 v31, v31, 0x1

    .line 3417
    const/16 v35, 0x2

    move/from16 v0, v31

    move/from16 v1, v35

    if-lt v0, v1, :cond_23

    .line 3418
    const-string v35, ";"

    move-object/from16 v0, v45

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3420
    :cond_23
    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_2e

    .line 3421
    const-string v34, ""

    .line 3425
    :goto_d
    move-object/from16 v0, v45

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3428
    const-string v34, "cl"

    move-object/from16 v0, v37

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move-object/from16 v0, v37

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 3429
    add-int/lit8 v30, v30, 0x1

    .line 3430
    const/16 v35, 0x2

    move/from16 v0, v30

    move/from16 v1, v35

    if-lt v0, v1, :cond_24

    .line 3431
    const-string v35, ";"

    move-object/from16 v0, v46

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3433
    :cond_24
    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_2f

    .line 3434
    const-string v34, ""

    .line 3438
    :goto_e
    move-object/from16 v0, v46

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3441
    const-string v34, "name"

    move-object/from16 v0, v37

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move-object/from16 v0, v37

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 3442
    add-int/lit8 v8, v8, 0x1

    .line 3443
    const/16 v35, 0x2

    move/from16 v0, v35

    if-lt v8, v0, :cond_25

    .line 3444
    const-string v35, ";"

    move-object/from16 v0, v47

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3446
    :cond_25
    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-eqz v35, :cond_30

    .line 3447
    const-string v34, ""

    .line 3451
    :goto_f
    move-object/from16 v0, v47

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3454
    const-string v34, "ct"

    move-object/from16 v0, v37

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    move-object/from16 v0, v37

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 3455
    invoke-static/range {v34 .. v34}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-nez v35, :cond_28

    .line 3457
    const-string v35, "/"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v35

    .line 3458
    const-string v49, "image"

    const/16 v51, 0x0

    aget-object v51, v35, v51

    move-object/from16 v0, v49

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_26

    const-string v49, "video"

    const/16 v51, 0x0

    aget-object v51, v35, v51

    move-object/from16 v0, v49

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-nez v49, :cond_26

    const-string v49, "audio"

    const/16 v51, 0x0

    aget-object v51, v35, v51

    move-object/from16 v0, v49

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_31

    .line 3460
    :cond_26
    add-int/lit8 v3, v3, 0x1

    .line 3461
    const/16 v35, 0x2

    move/from16 v0, v35

    if-lt v3, v0, :cond_27

    .line 3462
    const-string v35, ";"

    move-object/from16 v0, v44

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3464
    :cond_27
    add-int/lit8 v6, v6, 0x1

    .line 3465
    move-object/from16 v0, v44

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3553
    :cond_28
    :goto_10
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->moveToNext()Z

    move-result v34

    if-nez v34, :cond_20

    .line 3556
    :goto_11
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->close()V

    .line 3558
    const-string v3, "0\n"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3559
    move-object/from16 v0, v48

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3560
    move-object/from16 v0, v48

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3561
    move-object/from16 v0, v48

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3562
    move-object/from16 v0, v48

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3563
    move-object/from16 v0, v48

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3564
    move-object/from16 v0, v48

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3565
    move-object/from16 v0, v48

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3566
    move-object/from16 v0, v48

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3567
    move-object/from16 v0, v48

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3568
    move-object/from16 v0, v48

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3569
    move-object/from16 v0, v48

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3570
    move-object/from16 v0, v48

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3571
    move-object/from16 v0, v48

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3572
    move-object/from16 v0, v48

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "\r\r\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3573
    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3575
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v36

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3576
    move-object/from16 v0, v48

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3578
    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 3364
    :cond_29
    const-string v35, ""

    .line 3366
    const-string v35, "/"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v34

    .line 3367
    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v35, v0

    add-int/lit8 v35, v35, -0x1

    aget-object v35, v34, v35

    const-string v51, "\\."

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v35

    .line 3368
    move-object/from16 v0, v35

    array-length v0, v0

    move/from16 v51, v0

    const/16 v52, 0x1

    move/from16 v0, v51

    move/from16 v1, v52

    if-le v0, v1, :cond_2b

    .line 3371
    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v51, v0

    add-int/lit8 v51, v51, -0x1

    aget-object v51, v34, v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/String;->length()I

    move-result v51

    move-object/from16 v0, v35

    array-length v0, v0

    move/from16 v52, v0

    add-int/lit8 v52, v52, -0x1

    aget-object v35, v35, v52

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->length()I

    move-result v35

    sub-int v35, v51, v35

    .line 3372
    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v51, v0

    add-int/lit8 v51, v51, -0x1

    aget-object v34, v34, v51

    const/16 v51, 0x0

    add-int/lit8 v35, v35, -0x1

    move-object/from16 v0, v34

    move/from16 v1, v51

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v35, v34

    .line 3381
    :goto_12
    :try_start_0
    new-instance v34, Ljava/lang/String;

    const-string v51, "ISO-8859-1"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v35

    invoke-direct/range {v34 .. v35}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v7, v34

    .line 3389
    :goto_13
    invoke-static {v7}, Lcom/wssnps/b;->J(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3392
    const/16 v34, 0x1e5

    move/from16 v0, p1

    move/from16 v1, v34

    if-eq v0, v1, :cond_2c

    .line 3394
    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-direct {v0, v1, v7}, Lcom/wssnps/b;->j(Ljava/lang/String;Ljava/lang/String;)I

    move-result v33

    .line 3395
    const/16 v34, 0x5

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_2c

    .line 3397
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v33

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v48

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3399
    if-eqz v37, :cond_2a

    .line 3400
    invoke-interface/range {v37 .. v37}, Landroid/database/Cursor;->close()V

    .line 3402
    :cond_2a
    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 3376
    :cond_2b
    move-object/from16 v0, v34

    array-length v0, v0

    move/from16 v35, v0

    add-int/lit8 v35, v35, -0x1

    aget-object v34, v34, v35

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v35, v34

    goto :goto_12

    .line 3383
    :catch_0
    move-exception v34

    .line 3386
    invoke-virtual/range {v34 .. v34}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_13

    .line 3406
    :cond_2c
    add-int/lit8 v32, v32, 0x1

    .line 3407
    const/16 v34, 0x2

    move/from16 v0, v32

    move/from16 v1, v34

    if-lt v0, v1, :cond_2d

    .line 3408
    const-string v34, ";"

    move-object/from16 v0, v43

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3410
    :cond_2d
    if-ltz v33, :cond_22

    .line 3411
    move-object/from16 v0, v43

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_c

    .line 3423
    :cond_2e
    invoke-static/range {v34 .. v34}, Lcom/wssnps/b;->K(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    goto/16 :goto_d

    .line 3436
    :cond_2f
    invoke-static/range {v34 .. v34}, Lcom/wssnps/b;->K(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    goto/16 :goto_e

    .line 3449
    :cond_30
    invoke-static/range {v34 .. v34}, Lcom/wssnps/b;->K(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    goto/16 :goto_f

    .line 3467
    :cond_31
    const-string v49, "text"

    const/16 v51, 0x0

    aget-object v51, v35, v51

    move-object/from16 v0, v49

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_37

    .line 3469
    const-string v49, "plain"

    const/16 v51, 0x1

    aget-object v51, v35, v51

    move-object/from16 v0, v49

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_35

    .line 3471
    invoke-static/range {v50 .. v50}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-nez v35, :cond_34

    .line 3473
    add-int/lit8 v5, v5, 0x1

    .line 3474
    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/wssnps/b;->Q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 3475
    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->getBytes()[B

    move-result-object v49

    move-object/from16 v0, v49

    array-length v0, v0

    move/from16 v49, v0

    .line 3476
    const/16 v50, 0x2

    move/from16 v0, v50

    if-lt v5, v0, :cond_32

    .line 3478
    const-string v50, ";"

    move-object/from16 v0, v40

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3479
    const-string v50, ";"

    move-object/from16 v0, v41

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3482
    :cond_32
    move-object/from16 v0, v40

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 3483
    move-object/from16 v0, v41

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 3484
    move-object/from16 v0, v42

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3486
    add-int/lit8 v3, v3, 0x1

    .line 3487
    const/16 v35, 0x2

    move/from16 v0, v35

    if-lt v3, v0, :cond_33

    .line 3488
    const-string v35, ";"

    move-object/from16 v0, v44

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3490
    :cond_33
    add-int/lit8 v6, v6, 0x1

    .line 3491
    move-object/from16 v0, v44

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_10

    .line 3495
    :cond_34
    const-string v34, ""

    goto/16 :goto_10

    .line 3499
    :cond_35
    const/16 v49, 0x1

    aget-object v35, v35, v49

    invoke-static/range {v35 .. v35}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-nez v35, :cond_28

    .line 3501
    add-int/lit8 v3, v3, 0x1

    .line 3502
    const/16 v35, 0x2

    move/from16 v0, v35

    if-lt v3, v0, :cond_36

    .line 3503
    const-string v35, ";"

    move-object/from16 v0, v44

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3505
    :cond_36
    add-int/lit8 v6, v6, 0x1

    .line 3506
    move-object/from16 v0, v44

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_10

    .line 3509
    :cond_37
    const-string v49, "application"

    const/16 v50, 0x0

    aget-object v50, v35, v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v49

    if-eqz v49, :cond_3c

    .line 3511
    const-string v49, "smil"

    const/16 v50, 0x1

    aget-object v35, v35, v50

    move-object/from16 v0, v49

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v35

    if-eqz v35, :cond_3a

    .line 3513
    const-string v4, "text"

    move-object/from16 v0, v37

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v37

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 3515
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-nez v35, :cond_39

    .line 3517
    invoke-static {v4}, Lcom/wssnps/b;->L(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3518
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/wssnps/b;->Q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3520
    add-int/lit8 v3, v3, 0x1

    .line 3521
    const/16 v35, 0x2

    move/from16 v0, v35

    if-lt v3, v0, :cond_38

    .line 3522
    const-string v35, ";"

    move-object/from16 v0, v44

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3524
    :cond_38
    move-object/from16 v0, v44

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3526
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_10

    .line 3530
    :cond_39
    const-string v4, ""

    goto/16 :goto_10

    .line 3535
    :cond_3a
    add-int/lit8 v3, v3, 0x1

    .line 3536
    const/16 v35, 0x2

    move/from16 v0, v35

    if-lt v3, v0, :cond_3b

    .line 3537
    const-string v35, ";"

    move-object/from16 v0, v44

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3539
    :cond_3b
    add-int/lit8 v6, v6, 0x1

    .line 3540
    move-object/from16 v0, v44

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_10

    .line 3545
    :cond_3c
    add-int/lit8 v3, v3, 0x1

    .line 3546
    const/16 v35, 0x2

    move/from16 v0, v35

    if-lt v3, v0, :cond_3d

    .line 3547
    const-string v35, ";"

    move-object/from16 v0, v44

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3549
    :cond_3d
    add-int/lit8 v6, v6, 0x1

    .line 3550
    move-object/from16 v0, v44

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_10

    :cond_3e
    move-object/from16 v4, v32

    move/from16 v5, v33

    move/from16 v6, v34

    goto/16 :goto_11

    :cond_3f
    move-object/from16 v17, v18

    goto/16 :goto_6

    :cond_40
    move/from16 v53, v4

    move-object/from16 v4, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v22, v21

    move-object/from16 v21, v20

    move-object/from16 v20, v19

    move-object/from16 v19, v18

    move-object/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v6

    move/from16 v6, v53

    move/from16 v54, v5

    move v5, v7

    move/from16 v7, v54

    goto/16 :goto_1

    :cond_41
    move/from16 v53, v6

    move v6, v8

    move v8, v9

    move v9, v10

    move v10, v11

    move v11, v12

    move v12, v13

    move v13, v14

    move v14, v4

    move/from16 v4, v53

    move/from16 v54, v5

    move v5, v7

    move/from16 v7, v54

    move-object/from16 v55, v19

    move-object/from16 v19, v20

    move-object/from16 v20, v21

    move/from16 v21, v18

    move-object/from16 v18, v55

    goto/16 :goto_2

    .line 3114
    nop

    :sswitch_data_0
    .sparse-switch
        0x80 -> :sswitch_0
        0x81 -> :sswitch_1
        0x82 -> :sswitch_2
        0x83 -> :sswitch_3
        0x84 -> :sswitch_4
        0x85 -> :sswitch_5
        0x86 -> :sswitch_6
        0x87 -> :sswitch_7
        0x88 -> :sswitch_8
        0xc0 -> :sswitch_9
        0xc1 -> :sswitch_a
        0xc2 -> :sswitch_b
        0xc3 -> :sswitch_c
        0xc4 -> :sswitch_d
        0xe0 -> :sswitch_e
        0xe1 -> :sswitch_f
        0xe2 -> :sswitch_10
        0xe3 -> :sswitch_11
        0xe4 -> :sswitch_12
        0xe5 -> :sswitch_13
        0xe6 -> :sswitch_14
        0xe7 -> :sswitch_15
        0xe8 -> :sswitch_16
        0xe9 -> :sswitch_17
        0xea -> :sswitch_18
        0xeb -> :sswitch_19
        0xff -> :sswitch_1a
    .end sparse-switch

    .line 3203
    :sswitch_data_1
    .sparse-switch
        0x80 -> :sswitch_1b
        0xc0 -> :sswitch_1c
        0xc1 -> :sswitch_1d
        0xc2 -> :sswitch_1e
        0xe0 -> :sswitch_1f
        0xe1 -> :sswitch_20
        0xe2 -> :sswitch_21
        0xe3 -> :sswitch_22
        0xff -> :sswitch_23
    .end sparse-switch
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5276
    const/4 v1, 0x1

    const-string v2, "insertGroup"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5278
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 5297
    :cond_0
    :goto_0
    return-object v0

    .line 5281
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5282
    if-eqz v1, :cond_0

    .line 5284
    invoke-static {p1}, Lcom/wssnps/b/y;->b(Ljava/lang/String;)Lcom/wssnps/b/y;

    move-result-object v2

    .line 5285
    if-eqz v2, :cond_0

    .line 5287
    invoke-static {v1, v2, v3}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I

    move-result v0

    .line 5288
    if-nez v0, :cond_2

    .line 5289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5290
    :cond_2
    const/16 v1, -0xa

    if-ne v0, v1, :cond_3

    .line 5291
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "7\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5293
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public e(Ljava/lang/String;I)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x2

    .line 7870
    .line 7872
    const-string v0, ""

    .line 7874
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertSimContact "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7876
    sget-object v1, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p2, v1, :cond_1

    .line 7877
    const-string v0, "content://com.android.contacts/raw_contacts/adn"

    .line 7882
    :cond_0
    :goto_0
    const-string v1, "-1"

    .line 7887
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_10

    .line 7889
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object v4, v1

    .line 7892
    :goto_1
    if-nez v4, :cond_2

    .line 7894
    const/4 v0, -0x1

    .line 7895
    const-string v1, "ERROR:resolver null."

    .line 7991
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 7992
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7994
    :goto_2
    return-object v0

    .line 7878
    :cond_1
    sget-object v1, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p2, v1, :cond_0

    .line 7879
    const-string v0, "content://com.android.contacts/raw_contacts/adn2"

    goto :goto_0

    .line 7899
    :cond_2
    invoke-static {p1}, Lcom/wssnps/a/s;->b(Ljava/lang/String;)Lcom/wssnps/a/ac;

    move-result-object v5

    .line 7900
    if-nez v5, :cond_3

    .line 7903
    const-string v0, "Error insertSimContact newContactUri"

    .line 7904
    const-string v0, "-1\n"

    goto :goto_2

    .line 7907
    :cond_3
    iget-object v1, v5, Lcom/wssnps/a/ac;->b:Ljava/lang/String;

    .line 7908
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 7910
    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    if-eqz v1, :cond_b

    .line 7912
    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 7914
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v2, v2, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v2, v2, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7933
    :cond_4
    :goto_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 7934
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 7935
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 7937
    iget-object v0, v5, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_f

    .line 7939
    iget-object v0, v5, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    move-object v2, v0

    .line 7942
    :goto_4
    invoke-static {p2}, Lcom/wssnps/b;->b(I)I

    move-result v0

    if-ne v0, v8, :cond_5

    .line 7944
    iget-object v0, v5, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_5

    .line 7946
    iget-object v0, v5, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    move-object v3, v0

    .line 7950
    :cond_5
    const-string v0, "tag"

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7952
    if-eqz v2, :cond_6

    .line 7954
    const-string v0, "number"

    iget-object v1, v2, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7957
    :cond_6
    invoke-static {p2}, Lcom/wssnps/b;->b(I)I

    move-result v0

    if-ne v0, v8, :cond_7

    .line 7959
    if-eqz v3, :cond_7

    .line 7961
    invoke-virtual {p0, p2}, Lcom/wssnps/b;->p(I)I

    move-result v0

    .line 7962
    sget-object v1, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p2, v1, :cond_c

    .line 7964
    sget v1, Lcom/wssnps/b;->t:I

    if-ge v0, v1, :cond_7

    .line 7965
    const-string v0, "emails"

    iget-object v1, v3, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 7975
    :cond_7
    :goto_5
    invoke-virtual {v4, v6, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 7977
    if-eqz v0, :cond_d

    .line 7980
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 7981
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 7916
    :cond_8
    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 7918
    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    goto/16 :goto_3

    .line 7920
    :cond_9
    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 7922
    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    goto/16 :goto_3

    .line 7926
    :cond_a
    const-string v1, ""

    goto/16 :goto_3

    .line 7930
    :cond_b
    const-string v1, ""

    goto/16 :goto_3

    .line 7967
    :cond_c
    sget-object v1, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p2, v1, :cond_7

    .line 7969
    sget v1, Lcom/wssnps/b;->u:I

    if-ge v0, v1, :cond_7

    .line 7970
    const-string v0, "emails"

    iget-object v1, v3, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 7986
    :cond_d
    const-string v0, "Error insertSimContact newContactUri"

    .line 7987
    const-string v0, "-1\n"

    goto/16 :goto_2

    .line 7994
    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_f
    move-object v2, v3

    goto/16 :goto_4

    :cond_10
    move-object v4, v3

    goto/16 :goto_1
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 6064
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6068
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateCalGroup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6070
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 6087
    :cond_0
    :goto_0
    return-object v0

    .line 6073
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6074
    if-eqz v1, :cond_0

    .line 6076
    invoke-static {p2}, Lcom/wssnps/b/x;->a(Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v2

    .line 6077
    if-eqz v2, :cond_0

    .line 6079
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, v2, v0}, Lcom/wssnps/b/x;->e(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I

    move-result v0

    .line 6080
    if-nez v0, :cond_2

    .line 6081
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "5\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6083
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v3, 0x1

    .line 3584
    const-string v0, "getMmsGetItemFinish"

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3586
    const-string v0, ""

    .line 3587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/ABR/.MMS/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3588
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/ABR/MMS/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3590
    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3592
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3593
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3594
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v4, Lcom/wssnps/smlModelDefine;->e:I

    invoke-static {v3, v4, v1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I

    .line 3595
    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3597
    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3599
    const-string v0, "renameTo fail : MMS folder is exist."

    invoke-static {v5, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3600
    const-string v0, "0\n0\n"

    .line 3619
    :goto_0
    return-object v0

    .line 3604
    :cond_0
    const-string v0, "renameTo fail : MMS folder is not exist."

    invoke-static {v5, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3605
    const-string v0, "-1\n0\n"

    goto :goto_0

    .line 3610
    :cond_1
    const-string v0, "0\n0\n"

    goto :goto_0

    .line 3615
    :cond_2
    const-string v0, "MMS hidden folder is not exist"

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3616
    const-string v0, "0\n0\n"

    goto :goto_0
.end method

.method public f(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5364
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteGroup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5366
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 5380
    :cond_0
    :goto_0
    return-object v0

    .line 5369
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5370
    if-eqz v1, :cond_0

    .line 5372
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 5373
    invoke-static {v1, v0}, Lcom/wssnps/b/y;->d(Landroid/content/ContentResolver;I)Z

    move-result v1

    .line 5374
    if-nez v1, :cond_2

    .line 5375
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "5\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5377
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 6215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6218
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateTaskGroup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6221
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 6239
    :cond_0
    :goto_0
    return-object v0

    .line 6224
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6225
    if-eqz v1, :cond_0

    .line 6227
    invoke-static {p2}, Lcom/wssnps/b/ag;->a(Ljava/lang/String;)Lcom/wssnps/b/ag;

    move-result-object v2

    .line 6229
    if-eqz v2, :cond_0

    .line 6231
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 6232
    invoke-static {v1, v2, v0}, Lcom/wssnps/b/ag;->d(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)I

    move-result v1

    .line 6233
    if-nez v1, :cond_2

    .line 6234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6236
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3624
    .line 3625
    const-string v0, "0\n"

    .line 3629
    const-wide/16 v2, 0x1770

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3636
    :goto_0
    sget-boolean v1, Lcom/wssnps/b/d;->p:Z

    if-nez v1, :cond_0

    .line 3638
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/ABR/MMS/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 3639
    invoke-static {}, Lcom/wssnps/b;->h()Z

    move-result v1

    .line 3640
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 3642
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget-object v2, Lcom/wssnps/b;->o:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3646
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3647
    return-object v0

    .line 3631
    :catch_0
    move-exception v1

    .line 3633
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public g(I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2835
    const-string v0, ""

    .line 2836
    const-string v0, ""

    .line 2838
    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMmsAllCount nBoxID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2839
    sparse-switch p1, :sswitch_data_0

    .line 2868
    :goto_0
    :sswitch_0
    const-string v1, "VZW"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/wssnps/smlModelDefine;->C()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2869
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND reserved=\'0\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2871
    :goto_1
    const-string v0, "content://mms/"

    .line 2873
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2876
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 2877
    const-string v0, "-1\ncontext null\n"

    .line 2907
    :goto_2
    return-object v0

    .line 2842
    :sswitch_1
    const-string v0, "(msg_box=\'2\')"

    goto :goto_0

    .line 2846
    :sswitch_2
    const-string v0, "(msg_box=\'1\')"

    goto :goto_0

    .line 2850
    :sswitch_3
    const-string v0, "(msg_box=\'3\')"

    goto :goto_0

    .line 2854
    :sswitch_4
    const-string v0, "(msg_box=\'4\')"

    goto :goto_0

    .line 2861
    :sswitch_5
    const-string v0, "(msg_box=\'2\' OR msg_box=\'1\' OR msg_box=\'3\' OR msg_box=\'4\')"

    goto :goto_0

    .line 2879
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2880
    if-nez v0, :cond_1

    .line 2881
    const-string v0, "-1\nresolver null\n"

    goto :goto_2

    :cond_1
    move-object v4, v2

    move-object v5, v2

    .line 2883
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2884
    if-nez v1, :cond_2

    .line 2885
    const-string v0, "-1\nquery null\n"

    goto :goto_2

    .line 2887
    :cond_2
    const/4 v0, 0x0

    .line 2888
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2892
    :cond_3
    const-string v2, "m_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 2893
    const/16 v3, 0x86

    if-ne v2, v3, :cond_5

    .line 2897
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2900
    :cond_4
    sput v0, Lcom/wssnps/b;->h:I

    .line 2901
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2903
    const-string v0, "0\n"

    .line 2904
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/wssnps/b;->h:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2896
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    move-object v3, v0

    goto :goto_1

    .line 2839
    :sswitch_data_0
    .sparse-switch
        0x1cc -> :sswitch_1
        0x1cd -> :sswitch_2
        0x1ce -> :sswitch_3
        0x1cf -> :sswitch_4
        0x1d0 -> :sswitch_0
        0x1dc -> :sswitch_5
    .end sparse-switch
.end method

.method public g(ILjava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5302
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5305
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RestoreGroup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5307
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 5326
    :cond_0
    :goto_0
    return-object v0

    .line 5310
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5311
    if-eqz v1, :cond_0

    .line 5313
    invoke-static {p2}, Lcom/wssnps/b/y;->b(Ljava/lang/String;)Lcom/wssnps/b/y;

    move-result-object v2

    .line 5314
    if-eqz v2, :cond_0

    .line 5316
    invoke-static {v1, v2, p1}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I

    move-result v0

    .line 5317
    if-nez v0, :cond_2

    .line 5318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "5\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5319
    :cond_2
    const/16 v1, -0xa

    if-ne v0, v1, :cond_3

    .line 5320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "7\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5322
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public g(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5432
    .line 5433
    const/4 v0, 0x0

    .line 5435
    const-string v1, "-1"

    .line 5438
    const/4 v1, 0x1

    const-string v2, "insertContact"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5441
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 5443
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5445
    :cond_0
    if-nez v0, :cond_1

    .line 5448
    const/4 v0, 0x2

    .line 5476
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5452
    :cond_1
    invoke-static {p1}, Lcom/wssnps/b/y;->b(Ljava/lang/String;)Lcom/wssnps/b/y;

    move-result-object v1

    .line 5453
    if-nez v1, :cond_2

    .line 5456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5459
    :cond_2
    invoke-static {v0, v1, v3}, Lcom/wssnps/b/y;->c(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I

    move-result v0

    .line 5461
    if-nez v0, :cond_3

    .line 5463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5465
    :cond_3
    const/16 v1, -0xa

    if-ne v0, v1, :cond_4

    .line 5467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "7\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5471
    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 5472
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 7533
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7537
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateMemo "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7539
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 7568
    :cond_0
    :goto_0
    return-object v0

    .line 7542
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7543
    if-eqz v2, :cond_0

    .line 7545
    invoke-static {p2}, Lcom/wssnps/b/ae;->a(Ljava/lang/String;)Lcom/wssnps/b/ae;

    move-result-object v3

    .line 7546
    if-eqz v3, :cond_0

    .line 7548
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 7549
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 7550
    if-ne v0, v5, :cond_2

    .line 7551
    invoke-static {v2, v3, v4}, Lcom/wssnps/b/ae;->b(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    .line 7561
    :goto_1
    if-nez v0, :cond_6

    .line 7562
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "6\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 7552
    :cond_2
    const/4 v5, 0x3

    if-ne v0, v5, :cond_3

    .line 7553
    invoke-static {v2, v3, v4}, Lcom/wssnps/b/ae;->d(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    goto :goto_1

    .line 7554
    :cond_3
    const/4 v5, 0x2

    if-ne v0, v5, :cond_4

    .line 7555
    invoke-static {v2, v3, v4}, Lcom/wssnps/b/ae;->f(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    goto :goto_1

    .line 7556
    :cond_4
    const/4 v5, 0x4

    if-ne v0, v5, :cond_5

    .line 7557
    invoke-static {v2, v3, v4}, Lcom/wssnps/b/ae;->h(Landroid/content/ContentResolver;Lcom/wssnps/b/ae;I)I

    move-result v0

    goto :goto_1

    .line 7558
    :cond_5
    const/4 v2, 0x6

    if-ne v0, v2, :cond_7

    .line 7559
    invoke-static {v4}, Lcom/wssnps/b/ae;->a(I)I

    move-result v0

    goto :goto_1

    .line 7564
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public h(I)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2912
    const-string v0, ""

    .line 2913
    const-string v0, ""

    .line 2915
    sparse-switch p1, :sswitch_data_0

    .line 2941
    :goto_0
    const-string v1, "VZW"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/wssnps/smlModelDefine;->C()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2942
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND reserved=\'0\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2944
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 2946
    const-string v0, "content://mms/"

    .line 2947
    const/4 v6, 0x0

    .line 2948
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2951
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 2952
    const-string v0, "-1\ncontext null\n"

    .line 2981
    :goto_2
    return-object v0

    .line 2918
    :sswitch_0
    const-string v0, "(msg_box=\'2\')"

    goto :goto_0

    .line 2922
    :sswitch_1
    const-string v0, "(msg_box=\'1\')"

    goto :goto_0

    .line 2926
    :sswitch_2
    const-string v0, "(msg_box=\'3\')"

    goto :goto_0

    .line 2930
    :sswitch_3
    const-string v0, "(msg_box=\'4\')"

    goto :goto_0

    .line 2934
    :sswitch_4
    const-string v0, "(msg_box=\'2\' OR msg_box=\'1\' OR msg_box=\'3\' OR msg_box=\'4\')"

    goto :goto_0

    .line 2954
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2956
    if-nez v0, :cond_1

    .line 2957
    const-string v0, "-1\nresolver null\n"

    goto :goto_2

    :cond_1
    move-object v4, v2

    move-object v5, v2

    .line 2959
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2961
    if-nez v1, :cond_2

    .line 2962
    const-string v0, "-1\nquery null\n"

    goto :goto_2

    .line 2964
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v6

    .line 2968
    :cond_3
    const-string v2, "m_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 2969
    const/16 v3, 0x86

    if-ne v2, v3, :cond_4

    .line 2975
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2977
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2979
    const-string v1, "0\n"

    .line 2980
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2972
    :cond_4
    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2973
    const-string v2, ","

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2974
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move v0, v6

    goto :goto_4

    :cond_6
    move-object v3, v0

    goto/16 :goto_1

    .line 2915
    :sswitch_data_0
    .sparse-switch
        0x1d2 -> :sswitch_0
        0x1d3 -> :sswitch_1
        0x1d4 -> :sswitch_2
        0x1d5 -> :sswitch_3
        0x1dd -> :sswitch_4
    .end sparse-switch
.end method

.method public h(ILjava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 6035
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6038
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RestoreCalGroup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6040
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 6059
    :cond_0
    :goto_0
    return-object v0

    .line 6043
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6044
    if-eqz v1, :cond_0

    .line 6046
    invoke-static {p2}, Lcom/wssnps/b/x;->a(Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v2

    .line 6047
    if-eqz v2, :cond_0

    .line 6049
    invoke-static {v1, v2, p1}, Lcom/wssnps/b/x;->c(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I

    move-result v0

    .line 6050
    if-nez v0, :cond_2

    .line 6051
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "5\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6052
    :cond_2
    const/16 v1, -0xa

    if-ne v0, v1, :cond_3

    .line 6053
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "7\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6055
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public h(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 5575
    .line 5579
    const/4 v0, 0x0

    .line 5581
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteContactBatch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5583
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 5585
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5588
    :cond_0
    if-nez v0, :cond_1

    .line 5591
    const/4 v0, 0x2

    .line 5607
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5595
    :cond_1
    invoke-static {v0, p1}, Lcom/wssnps/b/y;->h(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    .line 5596
    if-nez v0, :cond_2

    .line 5598
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5602
    :cond_2
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 5603
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method i(I)I
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 4865
    const-string v3, ""

    .line 4866
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    .line 4868
    sget-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 4869
    const-string v3, "deleted = 0 AND (account_type = \'vnd.sec.contact.sim\')"

    .line 4873
    :cond_0
    :goto_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_2

    move v0, v6

    .line 4886
    :goto_1
    return v0

    .line 4870
    :cond_1
    sget-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 4871
    const-string v3, "deleted = 0 AND (account_type = \'vnd.sec.contact.sim2\')"

    goto :goto_0

    .line 4876
    :cond_2
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4877
    if-nez v0, :cond_3

    move v0, v6

    .line 4878
    goto :goto_1

    .line 4880
    :cond_3
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4881
    if-nez v1, :cond_4

    move v0, v6

    .line 4882
    goto :goto_1

    .line 4884
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 4885
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method public i()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 5907
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5910
    const-string v0, ""

    .line 5912
    const-string v0, "getCalGroupSize"

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5914
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 5916
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5919
    :goto_0
    if-eqz v0, :cond_1

    .line 5921
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "account_name=\"My calendar\" AND deleted=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5922
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 5923
    if-eqz v1, :cond_0

    .line 5925
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 5926
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 5928
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCalGroupSize : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5929
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5932
    :goto_2
    return-object v0

    :cond_0
    move v0, v6

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method public i(ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6244
    .line 6245
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RestoreTaskGroup "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6247
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 6248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6252
    :goto_0
    return-object v0

    .line 6250
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6251
    invoke-static {p2}, Lcom/wssnps/b/ag;->a(Ljava/lang/String;)Lcom/wssnps/b/ag;

    move-result-object v1

    .line 6252
    invoke-static {v0, v1, v3}, Lcom/wssnps/b/ag;->c(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public i(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5612
    .line 5616
    const/4 v0, 0x0

    .line 5618
    const/4 v1, 0x1

    const-string v2, "deleteContact"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5620
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 5622
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5625
    :cond_0
    if-nez v0, :cond_1

    .line 5628
    const/4 v0, 0x2

    .line 5646
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5632
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 5633
    invoke-static {v0, v1}, Lcom/wssnps/b/y;->e(Landroid/content/ContentResolver;I)Z

    move-result v0

    .line 5635
    if-nez v0, :cond_2

    .line 5637
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5641
    :cond_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 5642
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 8386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 8390
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateSmemo "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 8392
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 8408
    :cond_0
    :goto_0
    return-object v0

    .line 8395
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 8396
    if-eqz v1, :cond_0

    .line 8398
    if-eqz p2, :cond_0

    .line 8400
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v1, p2, v0}, Lcom/wssnps/b/af;->b(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 8401
    if-nez v0, :cond_2

    .line 8402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 8404
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5937
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5940
    const-string v1, "getCalGroupIndexArray"

    invoke-static {v2, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5942
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 5955
    :goto_0
    return-object v0

    .line 5945
    :cond_0
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5946
    if-eqz v1, :cond_1

    .line 5948
    const-string v0, ""

    .line 5949
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "account_name=\"My calendar\" AND deleted=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5951
    invoke-static {v1, v0}, Lcom/wssnps/b/x;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5954
    :cond_1
    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method j(I)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v8, 0x5

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4891
    const-string v0, ""

    .line 4907
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getSimContactCountInfo "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 4909
    sget-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 4911
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->k(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 4912
    if-nez v0, :cond_0

    .line 4913
    const-string v0, "-1\nsim is absent\n"

    .line 4982
    :goto_0
    return-object v0

    .line 4914
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    .line 4931
    :goto_1
    if-eq v0, v3, :cond_1

    if-nez v0, :cond_5

    .line 4933
    :cond_1
    const-string v0, "-1\nsim is absent\n"

    goto :goto_0

    .line 4916
    :cond_2
    sget-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_b

    .line 4918
    const-string v0, "true"

    const-string v1, "persist.dsds.enabled"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4920
    const-string v0, "-1\nsim is absent\n"

    goto :goto_0

    .line 4924
    :cond_3
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->l(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v0

    .line 4925
    if-nez v0, :cond_4

    .line 4926
    const-string v0, "-1\nsim is absent\n"

    goto :goto_0

    .line 4927
    :cond_4
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    goto :goto_1

    .line 4936
    :cond_5
    if-ne v0, v8, :cond_6

    .line 4943
    :cond_6
    invoke-static {p1}, Lcom/wssnps/b;->b(I)I

    move-result v0

    .line 4945
    if-ne v0, v5, :cond_7

    .line 4947
    invoke-static {v5, p1}, Lcom/wssnps/b;->a(II)[I

    move-result-object v7

    .line 4948
    if-eqz v7, :cond_9

    .line 4950
    aget v1, v7, v2

    .line 4953
    invoke-virtual {p0, p1}, Lcom/wssnps/b;->i(I)I

    move-result v0

    .line 4954
    if-eq v0, v1, :cond_a

    .line 4957
    :goto_2
    aget v4, v7, v3

    .line 4958
    aget v1, v7, v5

    .line 4959
    const/4 v3, 0x3

    aget v6, v7, v3

    .line 4960
    const/4 v3, 0x4

    aget v5, v7, v3

    .line 4961
    aget v3, v7, v8

    move v9, v1

    move v1, v3

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v0

    move v0, v9

    .line 4980
    :goto_3
    const-string v7, "0\n"

    .line 4981
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\n"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 4966
    :cond_7
    invoke-static {p1}, Lcom/wssnps/b;->a(I)[I

    move-result-object v1

    .line 4967
    if-eqz v1, :cond_9

    .line 4969
    aget v0, v1, v2

    .line 4970
    aget v3, v1, v3

    .line 4973
    invoke-virtual {p0, p1}, Lcom/wssnps/b;->i(I)I

    move-result v1

    .line 4974
    if-eq v1, v3, :cond_8

    move v3, v2

    move v4, v2

    move v5, v0

    move v6, v1

    move v0, v2

    move v1, v2

    .line 4975
    goto/16 :goto_3

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v0

    move v6, v3

    move v3, v2

    move v0, v2

    goto/16 :goto_3

    :cond_9
    move v0, v2

    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_2

    :cond_b
    move v0, v3

    goto/16 :goto_1
.end method

.method public j(ILjava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 6667
    const-string v0, "-1\n"

    .line 6672
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetMediaResfresh "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6676
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 6678
    const-string v0, "-1\ncontext null\n"

    .line 6701
    :goto_0
    return-object v0

    .line 6682
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 6684
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 6685
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.samsung.ebook.reader"

    const-string v3, "com.samsung.elibrary.KiesReceiver"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 6686
    const-string v0, "com.sec.android.intent.action.EBOOK_IMPORT_NEWBOOKS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 6687
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 6688
    const-string v0, "</MediaItem>"

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 6689
    const/4 v0, 0x0

    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    .line 6691
    aget-object v4, v3, v0

    aget-object v5, v3, v0

    const-string v6, "<Filename>"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0xa

    aget-object v6, v3, v0

    const-string v7, "</Filename>"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 6692
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6689
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6695
    :cond_1
    const-string v0, "filenames"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 6696
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 6697
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 6699
    :cond_2
    const-string v0, "0\n0\n"

    goto :goto_0
.end method

.method public k()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 6138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6141
    const-string v0, ""

    .line 6143
    const-string v0, "getTaskGroupSize"

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6145
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 6147
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6150
    :goto_0
    if-eqz v0, :cond_1

    .line 6152
    const-string v1, "content://com.android.calendar/taskGroup"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 6153
    const-string v3, "_id!=1"

    move-object v4, v2

    move-object v5, v2

    .line 6154
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 6155
    if-eqz v1, :cond_0

    .line 6157
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 6158
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6160
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTaskGroupSize : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6164
    :goto_2
    return-object v0

    :cond_0
    move v0, v6

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method k(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 4987
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 4988
    const-string v0, ""

    .line 4990
    sget-object v1, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 4991
    const-string v0, "content://com.android.contacts/raw_contacts/adn"

    .line 4996
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 4998
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 4999
    const-string v0, "-1\ncontext null\n"

    .line 5019
    :goto_1
    return-object v0

    .line 4992
    :cond_1
    sget-object v1, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 4993
    const-string v0, "content://com.android.contacts/raw_contacts/adn2"

    goto :goto_0

    .line 5001
    :cond_2
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5002
    if-nez v0, :cond_3

    .line 5003
    const-string v0, "-1\nresolver null\n"

    goto :goto_1

    :cond_3
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 5005
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 5006
    if-nez v0, :cond_4

    .line 5007
    const-string v0, "-1\nquery null\n"

    goto :goto_1

    .line 5009
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 5010
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 5014
    :cond_5
    const-string v2, "adn_index"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5015
    const-string v2, ","

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5016
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 5018
    :cond_6
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 5019
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public k(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 5860
    .line 5864
    const/4 v0, 0x0

    .line 5866
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteCalendar "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5868
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 5870
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5873
    :cond_0
    if-nez v0, :cond_1

    .line 5876
    const/4 v0, 0x2

    .line 5901
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5881
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 5883
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 5885
    new-instance v2, Lcom/wssnps/b/x;

    invoke-direct {v2}, Lcom/wssnps/b/x;-><init>()V

    .line 5886
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v2

    .line 5887
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "original_sync_id=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v2, Lcom/wssnps/b/x;->r:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5888
    invoke-static {v0, v2}, Lcom/wssnps/b/x;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    .line 5890
    invoke-static {v0, v1}, Lcom/wssnps/b/x;->b(Landroid/content/ContentResolver;I)Z

    move-result v0

    .line 5892
    if-nez v0, :cond_2

    .line 5893
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5896
    :cond_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 5897
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6172
    const-string v1, "getTaskGroupIndexArray"

    invoke-static {v2, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6174
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 6184
    :goto_0
    return-object v0

    .line 6177
    :cond_0
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6178
    if-eqz v1, :cond_1

    .line 6180
    invoke-static {v1}, Lcom/wssnps/b/ag;->c(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6183
    :cond_1
    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public l(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 5155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5159
    const/4 v1, 0x1

    const-string v2, "getGroupSize"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5161
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 5194
    :cond_0
    :goto_0
    return-object v0

    .line 5164
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5165
    if-eqz v1, :cond_0

    .line 5167
    const-string v0, ""

    .line 5168
    if-nez p1, :cond_3

    .line 5170
    const-string v0, "CHM"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5172
    const-string v0, "deleted = 0 AND (account_type=\"vnd.sec.contact.phone\" OR account_type=\"com.google\" OR account_type=\"com.android.exchange\")"

    .line 5185
    :goto_1
    invoke-static {v1, v0}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 5192
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5179
    :cond_2
    const-string v0, "deleted = 0 AND (account_type=\"vnd.sec.contact.phone\" OR account_type=\"com.google\" OR account_type=\"com.android.exchange\" OR account_type=\"com.osp.app.signin\")"

    goto :goto_1

    .line 5189
    :cond_3
    const-string v0, "account_type = \"vnd.sec.contact.phone\" AND deleted = 0"

    .line 5190
    invoke-static {v1, v0}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    goto :goto_2
.end method

.method public l(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 5960
    .line 5961
    const/4 v0, 0x0

    .line 5963
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 5965
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCalGroupItem "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5967
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 5969
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5972
    :cond_0
    if-nez v0, :cond_1

    .line 5974
    const/4 v0, 0x2

    .line 5989
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 5978
    :cond_1
    invoke-static {v0, v1}, Lcom/wssnps/b/x;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v1

    .line 5979
    invoke-virtual {v1, v0}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    .line 5980
    if-nez v0, :cond_2

    .line 5982
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5986
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public m()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6327
    .line 6332
    const/4 v0, 0x1

    const-string v1, "GetGoogleSign"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6334
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6369
    :goto_0
    return-object v0

    .line 6339
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    .line 6340
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6341
    if-nez v0, :cond_1

    .line 6343
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 6369
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6364
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public m(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 5199
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5202
    const/4 v1, 0x1

    const-string v2, "getGroupIndexArray"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5204
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 5238
    :cond_0
    :goto_0
    return-object v0

    .line 5207
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 5208
    if-eqz v1, :cond_0

    .line 5210
    const-string v0, ""

    .line 5211
    if-nez p1, :cond_3

    .line 5213
    const-string v0, "CHM"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5215
    const-string v0, "deleted = 0 AND (account_type=\"vnd.sec.contact.phone\" OR account_type=\"com.google\" OR account_type=\"com.android.exchange\")"

    .line 5228
    :goto_1
    invoke-static {v1, v0}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 5222
    :cond_2
    const-string v0, "deleted = 0 AND (account_type=\"vnd.sec.contact.phone\" OR account_type=\"com.google\" OR account_type=\"com.android.exchange\" OR account_type=\"com.osp.app.signin\")"

    goto :goto_1

    .line 5232
    :cond_3
    const-string v0, "account_type = \"vnd.sec.contact.phone\" AND deleted = 0"

    .line 5233
    invoke-static {v1, v0}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public m(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5994
    .line 5995
    const/4 v0, 0x0

    .line 5996
    const-string v1, "-1"

    .line 5999
    const/4 v1, 0x1

    const-string v2, "insertCalGroup"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6001
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 6003
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6006
    :cond_0
    if-nez v0, :cond_1

    .line 6008
    const/4 v0, 0x2

    .line 6030
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 6012
    :cond_1
    invoke-static {p1}, Lcom/wssnps/b/x;->a(Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v1

    .line 6013
    if-nez v1, :cond_2

    .line 6015
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6018
    :cond_2
    invoke-static {v0, v1, v3}, Lcom/wssnps/b/x;->b(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I

    move-result v0

    .line 6019
    if-nez v0, :cond_3

    .line 6021
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6025
    :cond_3
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 6026
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public n()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 6374
    .line 6377
    const-string v0, ""

    .line 6379
    const/4 v0, 0x1

    const-string v1, "GetCalendarCount"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6381
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v0, :cond_6

    .line 6383
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6386
    :goto_0
    if-nez v0, :cond_0

    .line 6388
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 6420
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 6394
    :cond_0
    const-string v3, "account_name=\"My calendar\""

    .line 6395
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 6396
    if-eqz v9, :cond_5

    .line 6398
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    move v6, v8

    move-object v7, v2

    .line 6402
    :goto_2
    const-string v1, "_id"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 6403
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calendar_id=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND contact_account_type is null AND deleted = 0"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 6404
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 6405
    if-eqz v4, :cond_2

    .line 6407
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int v3, v6, v1

    .line 6408
    if-ltz v3, :cond_1

    .line 6409
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 6412
    :goto_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    move v10, v3

    move-object v3, v1

    move v1, v10

    .line 6414
    :goto_4
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 6416
    :goto_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move v8, v1

    goto/16 :goto_1

    .line 6411
    :cond_1
    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_2
    move v1, v6

    move-object v3, v7

    goto :goto_4

    :cond_3
    move v6, v1

    move-object v7, v3

    goto :goto_2

    :cond_4
    move v1, v8

    move-object v3, v2

    goto :goto_5

    :cond_5
    move-object v3, v2

    goto/16 :goto_1

    :cond_6
    move-object v0, v2

    goto/16 :goto_0
.end method

.method public n(I)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5385
    .line 5389
    const/4 v0, 0x1

    const-string v1, "GetContactsSize"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 5391
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 5392
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5396
    :goto_0
    return-object v0

    .line 5394
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5395
    invoke-static {v0, p1}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;I)I

    move-result v0

    .line 5396
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public n(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 6092
    .line 6096
    const/4 v0, 0x0

    .line 6098
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteCalendaGroupr "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6100
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 6102
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6105
    :cond_0
    if-nez v0, :cond_1

    .line 6108
    const/4 v0, 0x2

    .line 6133
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 6113
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 6115
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 6117
    new-instance v2, Lcom/wssnps/b/x;

    invoke-direct {v2}, Lcom/wssnps/b/x;-><init>()V

    .line 6118
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/wssnps/b/x;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v2

    .line 6119
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v2, v2, Lcom/wssnps/b/x;->a:I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 6120
    invoke-static {v0, v2}, Lcom/wssnps/b/x;->f(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    .line 6122
    invoke-static {v0, v1}, Lcom/wssnps/b/x;->c(Landroid/content/ContentResolver;I)Z

    move-result v0

    .line 6124
    if-nez v0, :cond_2

    .line 6125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6128
    :cond_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 6129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public o()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6429
    const-string v1, "getCalendarIndexArray"

    invoke-static {v2, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6431
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 6433
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 6436
    :cond_0
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_2

    .line 6444
    :cond_1
    :goto_0
    return-object v0

    .line 6439
    :cond_2
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6440
    if-eqz v1, :cond_1

    .line 6442
    invoke-static {v1, v2}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public o(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 5401
    .line 5403
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 5404
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5407
    :goto_0
    return-object v0

    .line 5406
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5407
    invoke-static {v0, p1}, Lcom/wssnps/b/y;->c(Landroid/content/ContentResolver;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public o(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 6189
    .line 6191
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTaskGroupItem "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6193
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 6194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6197
    :goto_0
    return-object v0

    .line 6196
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6197
    invoke-static {v0, p1}, Lcom/wssnps/b/ag;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public p(I)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 7831
    const/4 v6, 0x0

    .line 7834
    const-string v0, ""

    .line 7835
    const-string v3, ""

    .line 7836
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 7838
    sget-object v1, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p1, v1, :cond_3

    .line 7839
    const-string v3, "account_type = \'vnd.sec.contact.sim\'AND mimetype=\"vnd.android.cursor.item/email_v2\""

    .line 7843
    :cond_0
    :goto_0
    const/4 v1, 0x1

    const-string v4, "getSimEmailCount"

    invoke-static {v1, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7845
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 7846
    if-eqz v1, :cond_5

    .line 7848
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v6

    .line 7852
    :cond_1
    const-string v2, "data1"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 7853
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 7855
    add-int/lit8 v0, v0, 0x1

    .line 7858
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 7863
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 7865
    :goto_2
    return v0

    .line 7840
    :cond_3
    sget-object v1, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v1}, Lcom/wssnps/b/aa;->a()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 7841
    const-string v3, "account_type = \'vnd.sec.contact.sim2\'AND mimetype=\"vnd.android.cursor.item/email_v2\""

    goto :goto_0

    :cond_4
    move v0, v6

    goto :goto_1

    :cond_5
    move v0, v6

    goto :goto_2
.end method

.method public p()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6623
    const-string v0, "-1\n"

    .line 6625
    const-string v0, "MediaScan "

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6627
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 6629
    const-string v0, "-1\ncontext null\n"

    .line 6641
    :goto_0
    return-object v0

    .line 6633
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.intent.action.EBOOK_SCAN_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6634
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 6635
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 6637
    sput-boolean v2, Lcom/wssnps/smlNpsReceiver;->a:Z

    .line 6639
    const-string v0, "0\nok\n"

    goto :goto_0
.end method

.method public p(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6202
    .line 6203
    const/4 v0, 0x1

    const-string v1, "insertTaskGroup"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6205
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 6206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6210
    :goto_0
    return-object v0

    .line 6208
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6209
    invoke-static {p1}, Lcom/wssnps/b/ag;->a(Ljava/lang/String;)Lcom/wssnps/b/ag;

    move-result-object v1

    .line 6210
    invoke-static {v0, v1, v2}, Lcom/wssnps/b/ag;->c(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6646
    const-string v0, "-1\n"

    .line 6648
    const/4 v0, 0x1

    const-string v1, "GetMediaScaning "

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6650
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 6652
    const-string v0, "-1\ncontext null\n"

    .line 6662
    :goto_0
    return-object v0

    .line 6656
    :cond_0
    sget-boolean v0, Lcom/wssnps/smlNpsReceiver;->a:Z

    if-eqz v0, :cond_1

    .line 6657
    const-string v0, "0\n1\n"

    goto :goto_0

    .line 6659
    :cond_1
    const-string v0, "0\n0\n"

    goto :goto_0
.end method

.method public q(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 6257
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6261
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteTaskGroup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6263
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 6276
    :cond_0
    :goto_0
    return-object v0

    .line 6266
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6267
    if-eqz v1, :cond_0

    .line 6269
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 6270
    invoke-static {v1, v0}, Lcom/wssnps/b/ag;->c(Landroid/content/ContentResolver;I)Z

    move-result v1

    .line 6271
    if-nez v1, :cond_2

    .line 6272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6274
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public r()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6772
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6776
    const/4 v1, 0x1

    const-string v2, "setContactsInvalidSyncInit"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6778
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 6795
    :cond_0
    :goto_0
    return-object v0

    .line 6781
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6782
    if-eqz v1, :cond_0

    .line 6784
    invoke-static {v1}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;)Z

    move-result v2

    .line 6785
    if-eqz v2, :cond_0

    .line 6788
    invoke-static {v1}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;)Z

    move-result v1

    .line 6789
    if-eqz v1, :cond_0

    .line 6792
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public r(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 6281
    .line 6288
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v0, :cond_4

    .line 6290
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6293
    :goto_0
    if-nez v0, :cond_0

    .line 6295
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    move-object v0, v4

    move-object v4, v7

    .line 6322
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0

    .line 6300
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 6302
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mimetype=\"vnd.android.cursor.item/photo\"AND raw_contact_id=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 6303
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v5, "data15"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 6305
    if-eqz v1, :cond_3

    .line 6307
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 6309
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 6310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "6\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 6313
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6315
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 6316
    new-instance v0, Ljava/lang/String;

    invoke-static {v2}, Lcom/wssnps/a/a;->a([B)[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    .line 6318
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_2
    move-object v0, v4

    goto :goto_3

    :cond_3
    move-object v0, v4

    goto/16 :goto_1

    :cond_4
    move-object v0, v4

    goto/16 :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 121
    const/4 v0, 0x1

    sput-boolean v0, Lcom/wssnps/b;->v:Z

    .line 122
    const-string v0, "run  WsNpsHandler"

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 124
    :goto_0
    sget-boolean v0, Lcom/wssnps/b;->v:Z

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    if-nez v0, :cond_1

    .line 153
    :cond_0
    :goto_1
    return-void

    .line 131
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/wssnps/b;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    .line 135
    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException  WsNpsHandler.run:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 136
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 141
    :cond_2
    const-string v0, "WsNpsHandler destroy."

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    if-eqz v0, :cond_0

    .line 146
    :try_start_1
    iget-object v0, p0, Lcom/wssnps/b;->s:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 148
    :catch_1
    move-exception v0

    .line 150
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public s()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 6800
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6803
    const-string v2, "vnd.sec.contact.phone"

    .line 6808
    const-string v3, "setContactsInitSyncInfo"

    invoke-static {v1, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6810
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 6884
    :cond_0
    :goto_0
    return-object v0

    .line 6813
    :cond_1
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 6814
    if-eqz v3, :cond_0

    .line 6816
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 6817
    const-string v5, "dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6819
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dirty=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "account_type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 6821
    invoke-static {v3, v4, v5}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v4

    .line 6822
    if-eqz v4, :cond_0

    .line 6825
    invoke-static {v3}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;)Z

    move-result v4

    .line 6826
    if-eqz v4, :cond_0

    .line 6829
    sget-object v4, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    if-eqz v4, :cond_3

    .line 6831
    sget-object v4, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 6833
    sput-object v9, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    .line 6834
    const-string v5, ""

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v1

    .line 6848
    :goto_1
    if-eqz v2, :cond_0

    .line 6857
    sget-object v2, Lcom/wssnps/b;->l:Ljava/lang/StringBuffer;

    if-eqz v2, :cond_5

    .line 6859
    sget-object v2, Lcom/wssnps/b;->l:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 6861
    sput-object v9, Lcom/wssnps/b;->l:Ljava/lang/StringBuffer;

    .line 6862
    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 6872
    :goto_2
    if-eqz v1, :cond_0

    .line 6881
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 6840
    :cond_2
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 6841
    const-string v6, "dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6842
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 6843
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_id in("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " AND "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "account_type"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "=\""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 6846
    invoke-static {v3, v5, v2}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v2

    goto/16 :goto_1

    .line 6853
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 6868
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 6869
    invoke-static {v3, v1}, Lcom/wssnps/b/y;->e(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v1

    goto/16 :goto_2

    .line 6877
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public s(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 6449
    .line 6450
    const/4 v0, 0x0

    .line 6452
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 6454
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GetCalendar "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6456
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 6458
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6461
    :cond_0
    if-nez v0, :cond_1

    .line 6463
    const/4 v0, 0x2

    .line 6480
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 6467
    :cond_1
    invoke-static {v0, v1}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v1

    .line 6469
    invoke-virtual {v1, v0}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    .line 6471
    if-nez v0, :cond_2

    .line 6473
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6477
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public t()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6889
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6891
    const-string v1, "vnd.sec.contact.phone"

    .line 6892
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dirty=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "deleted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6897
    const-string v2, "setAddContactsSyncCount"

    invoke-static {v5, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6899
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 6910
    :cond_0
    :goto_0
    return-object v0

    .line 6902
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 6903
    if-eqz v2, :cond_0

    .line 6905
    invoke-static {v2, v1}, Lcom/wssnps/b/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 6907
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public t(Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide v10, 0x40ac200000000000L    # 3600.0

    const-wide v6, 0x408f400000000000L    # 1000.0

    const/4 v8, 0x1

    .line 6485
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 6486
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 6489
    iget-object v0, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 6490
    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v2

    .line 6492
    int-to-double v4, v2

    div-double/2addr v4, v6

    div-double/2addr v4, v10

    .line 6493
    int-to-double v2, v2

    div-double/2addr v2, v10

    rem-double/2addr v2, v6

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v2, v6

    .line 6494
    add-double/2addr v2, v4

    .line 6496
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 6497
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v8, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 6499
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 6500
    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 6501
    invoke-virtual {v0, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 6502
    if-ne v4, v8, :cond_0

    .line 6504
    const-string v0, "GMT"

    invoke-virtual {v1, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 6505
    invoke-virtual {v1, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 6506
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6515
    :goto_0
    return-object v0

    .line 6508
    :cond_0
    if-nez v4, :cond_1

    .line 6510
    int-to-long v4, v0

    sub-long v4, v6, v4

    .line 6511
    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 6512
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 6515
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public u()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 6915
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6917
    const-string v1, "vnd.sec.contact.phone"

    .line 6918
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleted=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6922
    const-string v2, "setDeleteContactsSyncCount"

    invoke-static {v4, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6924
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 6935
    :cond_0
    :goto_0
    return-object v0

    .line 6927
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 6928
    if-eqz v2, :cond_0

    .line 6930
    invoke-static {v2, v1}, Lcom/wssnps/b/y;->f(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 6932
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public u(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 6521
    const-string v7, "0"

    .line 6522
    const-string v3, ""

    .line 6523
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 6524
    const-string v0, "content://settings/system"

    .line 6525
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 6528
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GetSyncID "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6530
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 6531
    const-string v0, "-1\ncontext null\n"

    .line 6575
    :goto_0
    return-object v0

    .line 6533
    :cond_0
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6534
    if-nez v0, :cond_1

    .line 6535
    const-string v0, "-1\nresolver null\n"

    goto :goto_0

    .line 6537
    :cond_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v6, :cond_2

    .line 6539
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    sput-object v3, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    .line 6540
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    sput-object v3, Lcom/wssnps/b;->l:Ljava/lang/StringBuffer;

    .line 6541
    const-string v3, "contact_sync_id"

    move-object v6, v3

    .line 6550
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "name=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    .line 6551
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 6552
    if-nez v2, :cond_3

    .line 6553
    const-string v0, "-1\ncursor null\n"

    goto :goto_0

    .line 6543
    :cond_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    .line 6545
    invoke-static {}, Lcom/wssnps/b/d;->h()V

    .line 6546
    const-string v3, ""

    sput-object v3, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    .line 6547
    const-string v3, "calendar_sync_id"

    move-object v6, v3

    goto :goto_1

    .line 6555
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_5

    .line 6557
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 6558
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 6560
    const-string v4, "name"

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6561
    const-string v4, "value"

    const-string v5, "0"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6563
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 6565
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 6566
    const-string v0, "0\n0\n"

    goto/16 :goto_0

    .line 6570
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 6572
    const-string v0, "value"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 6574
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 6575
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move-object v0, v7

    goto :goto_2

    :cond_7
    move-object v6, v3

    goto/16 :goto_1
.end method

.method public v()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6940
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6942
    const-string v1, "vnd.sec.contact.phone"

    .line 6943
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dirty=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "deleted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6948
    const-string v2, "setAddGroupSyncCount"

    invoke-static {v5, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6950
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 6961
    :cond_0
    :goto_0
    return-object v0

    .line 6953
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 6954
    if-eqz v2, :cond_0

    .line 6956
    invoke-static {v2, v1}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 6958
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public v(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6581
    .line 6583
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetMedia "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6585
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 6586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6594
    :goto_0
    return-object v0

    .line 6588
    :cond_0
    const-string v0, "\r\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 6589
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 6590
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 6592
    :cond_1
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6594
    invoke-static {v0, p1}, Lcom/wssnps/b/ad;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public w()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 6966
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6968
    const-string v1, "vnd.sec.contact.phone"

    .line 6969
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleted=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6973
    const-string v2, "setDeleteGroupSyncCount"

    invoke-static {v4, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6975
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 6986
    :cond_0
    :goto_0
    return-object v0

    .line 6978
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 6979
    if-eqz v2, :cond_0

    .line 6981
    invoke-static {v2, v1}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    .line 6983
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public w(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 6599
    .line 6600
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6602
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GetMedia "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6604
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 6606
    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6607
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6618
    :goto_0
    return-object v0

    .line 6610
    :cond_0
    const-string v1, "\r\n"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 6611
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 6612
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 6614
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6616
    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1, p1}, Lcom/wssnps/b/ad;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6618
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public x()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6991
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6993
    const-string v1, "vnd.sec.contact.phone"

    .line 6994
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dirty=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "deleted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 6998
    const-string v2, "setAddGroupSyncIndexArray"

    invoke-static {v5, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7000
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 7009
    :cond_0
    :goto_0
    return-object v0

    .line 7003
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7004
    if-eqz v2, :cond_0

    .line 7006
    invoke-static {v2, v1}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public x(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 6706
    const-string v0, "contact_sync_id"

    .line 6707
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 6708
    const-string v2, "content://settings/system"

    .line 6709
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6712
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SetContactsSyncID  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 6714
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 6715
    const-string v0, "-1\ncontext null\n"

    .line 6733
    :goto_0
    return-object v0

    .line 6717
    :cond_0
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 6718
    if-nez v3, :cond_1

    .line 6719
    const-string v0, "-1\nresolver null\n"

    goto :goto_0

    .line 6721
    :cond_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 6722
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 6724
    const-string v5, "value"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 6731
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "name=\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 6732
    const/4 v1, 0x0

    invoke-virtual {v3, v2, v4, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 6733
    const-string v0, "0\n"

    goto :goto_0

    .line 6728
    :cond_2
    const-string v1, "value"

    const-string v5, "0"

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public y()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 7014
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7016
    const-string v1, "vnd.sec.contact.phone"

    .line 7017
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleted=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account_type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 7020
    const-string v2, "setDeleteGroupSyncIndexArray"

    invoke-static {v4, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7022
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 7031
    :cond_0
    :goto_0
    return-object v0

    .line 7025
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7026
    if-eqz v2, :cond_0

    .line 7028
    invoke-static {v2, v1}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public z()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 7037
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 7039
    const-string v2, "vnd.sec.contact.phone"

    .line 7040
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dirty=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "deleted"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "account_type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 7043
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 7046
    const-string v3, "setAddContactsSyncIndexArray"

    invoke-static {v5, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7048
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v3, :cond_0

    .line 7050
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "2\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7051
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7073
    :goto_0
    return-object v0

    .line 7054
    :cond_0
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 7055
    if-eqz v3, :cond_2

    .line 7057
    invoke-static {v3, v2}, Lcom/wssnps/b/y;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 7058
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 7060
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7063
    :goto_1
    if-ge v0, v3, :cond_2

    .line 7065
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7067
    sget-object v4, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    if-eqz v4, :cond_1

    .line 7068
    sget-object v4, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7063
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7072
    :cond_2
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 7073
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public z(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 7427
    .line 7428
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7430
    const/4 v1, 0x0

    .line 7433
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMemoItem "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 7435
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 7463
    :cond_0
    :goto_0
    return-object v0

    .line 7438
    :cond_1
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 7439
    if-eqz v2, :cond_0

    .line 7441
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 7442
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v3

    .line 7443
    if-ne v3, v4, :cond_2

    .line 7444
    invoke-static {v2, v0}, Lcom/wssnps/b/ae;->a(Landroid/content/ContentResolver;I)Lcom/wssnps/b/ae;

    move-result-object v0

    .line 7452
    :goto_1
    if-nez v0, :cond_5

    .line 7454
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 7445
    :cond_2
    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 7446
    invoke-static {v2, v0}, Lcom/wssnps/b/ae;->c(Landroid/content/ContentResolver;I)Lcom/wssnps/b/ae;

    move-result-object v0

    goto :goto_1

    .line 7447
    :cond_3
    const/4 v4, 0x2

    if-ne v3, v4, :cond_4

    .line 7448
    invoke-static {v2, v0}, Lcom/wssnps/b/ae;->e(Landroid/content/ContentResolver;I)Lcom/wssnps/b/ae;

    move-result-object v0

    goto :goto_1

    .line 7449
    :cond_4
    const/4 v4, 0x4

    if-ne v3, v4, :cond_6

    .line 7450
    invoke-static {v2, v0}, Lcom/wssnps/b/ae;->g(Landroid/content/ContentResolver;I)Lcom/wssnps/b/ae;

    move-result-object v0

    goto :goto_1

    .line 7458
    :cond_5
    invoke-virtual {v0}, Lcom/wssnps/b/ae;->toString()Ljava/lang/String;

    move-result-object v0

    .line 7459
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method
