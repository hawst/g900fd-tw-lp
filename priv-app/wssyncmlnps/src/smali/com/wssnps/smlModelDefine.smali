.class public Lcom/wssnps/smlModelDefine;
.super Ljava/lang/Object;
.source "smlModelDefine.java"


# static fields
.field public static a:Z

.field public static b:Landroid/os/PowerManager$WakeLock;

.field public static c:I

.field public static d:I

.field public static e:I

.field private static f:Ljavax/crypto/Cipher;

.field private static g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 122
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssnps/smlModelDefine;->a:Z

    .line 129
    sput-boolean v1, Lcom/wssnps/smlModelDefine;->g:Z

    .line 131
    const/4 v0, 0x0

    sput-object v0, Lcom/wssnps/smlModelDefine;->b:Landroid/os/PowerManager$WakeLock;

    .line 1149
    sput v1, Lcom/wssnps/smlModelDefine;->c:I

    .line 1150
    const/4 v0, 0x2

    sput v0, Lcom/wssnps/smlModelDefine;->d:I

    .line 1151
    const/4 v0, 0x3

    sput v0, Lcom/wssnps/smlModelDefine;->e:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static A()Z
    .locals 2

    .prologue
    .line 2200
    const-string v0, "ro.csc.country_code"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2202
    const-string v1, "KOREA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2203
    const/4 v0, 0x1

    .line 2205
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static B()Z
    .locals 2

    .prologue
    .line 2210
    const-string v0, "ro.csc.country_code"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2212
    const-string v1, "JP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2213
    const/4 v0, 0x1

    .line 2215
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static C()Z
    .locals 2

    .prologue
    .line 2267
    const-string v0, "dev.message.reserved"

    const-string v1, "TRUE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2268
    const-string v1, "TRUE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2269
    const/4 v0, 0x1

    .line 2271
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static D()Z
    .locals 2

    .prologue
    .line 2284
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    sget-object v0, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v1, "L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)I
    .locals 5

    .prologue
    .line 203
    const/4 v0, 0x0

    .line 204
    const-string v1, "com.sec.android.provider.snote"

    invoke-static {p0, v1}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 205
    const-string v2, "com.sec.android.widgetapp.diotek.smemo"

    invoke-static {p0, v2}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 206
    const-string v3, "com.sec.android.app.memo"

    invoke-static {p0, v3}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 207
    const-string v4, "com.samsung.android.app.memo"

    invoke-static {p0, v4}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 209
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 211
    const/4 v0, 0x4

    .line 234
    :cond_0
    :goto_0
    return v0

    .line 213
    :cond_1
    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    .line 215
    const/4 v0, 0x2

    goto :goto_0

    .line 217
    :cond_2
    if-eqz v2, :cond_3

    .line 219
    const/4 v0, 0x3

    goto :goto_0

    .line 221
    :cond_3
    if-eqz v3, :cond_4

    .line 223
    const/4 v0, 0x1

    goto :goto_0

    .line 225
    :cond_4
    if-eqz v1, :cond_5

    .line 227
    const/4 v0, 0x5

    goto :goto_0

    .line 229
    :cond_5
    if-eqz v4, :cond_0

    .line 231
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1553
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSimState SIMType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1556
    const/4 v1, -0x1

    .line 1559
    sget-object v2, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    if-ne p1, v2, :cond_3

    .line 1562
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->k(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    .line 1563
    if-nez v2, :cond_1

    move v0, v1

    .line 1605
    :cond_0
    :goto_0
    return v0

    .line 1566
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    .line 1595
    :goto_1
    if-eq v2, v0, :cond_2

    if-nez v2, :cond_7

    :cond_2
    move v0, v1

    .line 1597
    goto :goto_0

    .line 1568
    :cond_3
    sget-object v2, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v2}, Lcom/wssnps/b/aa;->a()I

    move-result v2

    if-ne p1, v2, :cond_6

    .line 1571
    const-string v2, "true"

    const-string v3, "persist.dsds.enabled"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 1577
    goto :goto_0

    .line 1583
    :cond_4
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->l(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    .line 1584
    if-nez v2, :cond_5

    move v0, v1

    .line 1585
    goto :goto_0

    .line 1587
    :cond_5
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    goto :goto_1

    :cond_6
    move v0, v1

    .line 1592
    goto :goto_0

    .line 1600
    :cond_7
    const/4 v1, 0x5

    if-eq v2, v1, :cond_0

    .line 1603
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1155
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1158
    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1159
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1161
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    .line 1164
    :cond_0
    sget v0, Lcom/wssnps/smlModelDefine;->c:I

    if-ne p1, v0, :cond_1

    .line 1165
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.MTP.OBJECT_ADDED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1171
    :goto_0
    if-nez p0, :cond_3

    .line 1177
    :goto_1
    return v3

    .line 1166
    :cond_1
    sget v0, Lcom/wssnps/smlModelDefine;->d:I

    if-ne p1, v0, :cond_2

    .line 1167
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.MTP.OBJECT_REMOVED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1169
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.MTP.OBJECT_PROP_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1174
    :cond_3
    const-string v2, "Path"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1175
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public static a(Ljava/io/File;)J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 493
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 496
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 497
    const/4 v0, 0x0

    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 500
    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 502
    aget-object v4, v1, v0

    invoke-static {v4}, Lcom/wssnps/smlModelDefine;->a(Ljava/io/File;)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 497
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 507
    :cond_0
    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_1

    .line 512
    :cond_1
    return-wide v2
.end method

.method public static a(Ljava/io/InputStream;Ljavax/crypto/spec/SecretKeySpec;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 424
    sget-object v0, Lcom/wssnps/smlModelDefine;->f:Ljavax/crypto/Cipher;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v0

    new-array v0, v0, [B

    .line 426
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    .line 428
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v1, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 431
    sget-object v0, Lcom/wssnps/smlModelDefine;->f:Ljavax/crypto/Cipher;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, p1, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 433
    new-instance v0, Ljavax/crypto/CipherInputStream;

    sget-object v1, Lcom/wssnps/smlModelDefine;->f:Ljavax/crypto/Cipher;

    invoke-direct {v0, p0, v1}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    return-object v0
.end method

.method public static a(Ljava/io/OutputStream;Ljavax/crypto/spec/SecretKeySpec;)Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 408
    sget-object v0, Lcom/wssnps/smlModelDefine;->f:Ljavax/crypto/Cipher;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v0

    new-array v0, v0, [B

    .line 409
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 410
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v1, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 413
    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 416
    sget-object v0, Lcom/wssnps/smlModelDefine;->f:Ljavax/crypto/Cipher;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p1, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 418
    new-instance v0, Ljavax/crypto/CipherOutputStream;

    sget-object v1, Lcom/wssnps/smlModelDefine;->f:Ljavax/crypto/Cipher;

    invoke-direct {v0, p0, v1}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 438
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 439
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    :goto_0
    return-void

    .line 442
    :cond_0
    new-instance v0, Lcom/wssnps/c/b;

    invoke-direct {v0, p0, p1}, Lcom/wssnps/c/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 443
    invoke-virtual {v0}, Lcom/wssnps/c/b;->a()Ljava/lang/String;

    move-result-object v0

    .line 444
    invoke-static {p1, v0}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 470
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Manifest.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 472
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 473
    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->a(Ljava/io/File;)J

    move-result-wide v2

    .line 474
    new-instance v1, Lcom/wssnps/c/d;

    invoke-direct {v1, p0, p2, v2, v3}, Lcom/wssnps/c/d;-><init>(Landroid/content/Context;IJ)V

    .line 475
    invoke-virtual {v1}, Lcom/wssnps/c/d;->a()Ljava/lang/String;

    move-result-object v1

    .line 476
    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;Ljava/lang/String;)Z

    .line 477
    return-void
.end method

.method public static a(Ljava/io/File;Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 318
    .line 323
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s%08x"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {}, Lcom/wssnps/smlModelDefine;->w()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 326
    :try_start_0
    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->d(Ljava/lang/String;)Ljavax/crypto/spec/SecretKeySpec;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 333
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 334
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 335
    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 336
    invoke-virtual {v3, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 337
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ".exml"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 341
    :try_start_1
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342
    :try_start_2
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 343
    :try_start_3
    invoke-static {v2, v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/io/OutputStream;Ljavax/crypto/spec/SecretKeySpec;)Ljava/io/OutputStream;

    move-result-object v1

    .line 345
    const/16 v0, 0x400

    new-array v0, v0, [B

    .line 346
    :goto_1
    const/4 v4, 0x0

    const/16 v5, 0x400

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 348
    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    .line 351
    :catch_0
    move-exception v0

    .line 353
    :goto_2
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 359
    if-eqz v1, :cond_0

    .line 360
    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 369
    :cond_0
    :goto_3
    if-eqz v2, :cond_1

    .line 370
    :try_start_6
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 379
    :cond_1
    :goto_4
    if-eqz v3, :cond_2

    .line 380
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    .line 387
    :cond_2
    :goto_5
    return-void

    .line 328
    :catch_1
    move-exception v0

    .line 330
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 359
    :cond_3
    if-eqz v1, :cond_4

    .line 360
    :try_start_8
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 369
    :cond_4
    :goto_6
    if-eqz v2, :cond_5

    .line 370
    :try_start_9
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 379
    :cond_5
    :goto_7
    if-eqz v3, :cond_2

    .line 380
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_5

    .line 382
    :catch_2
    move-exception v0

    .line 384
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 362
    :catch_3
    move-exception v0

    .line 364
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 372
    :catch_4
    move-exception v0

    .line 374
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 362
    :catch_5
    move-exception v0

    .line 364
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 372
    :catch_6
    move-exception v0

    .line 374
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 382
    :catch_7
    move-exception v0

    .line 384
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 357
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    .line 359
    :goto_8
    if-eqz v1, :cond_6

    .line 360
    :try_start_b
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 369
    :cond_6
    :goto_9
    if-eqz v2, :cond_7

    .line 370
    :try_start_c
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 379
    :cond_7
    :goto_a
    if-eqz v3, :cond_8

    .line 380
    :try_start_d
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 385
    :cond_8
    :goto_b
    throw v0

    .line 362
    :catch_8
    move-exception v1

    .line 364
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 372
    :catch_9
    move-exception v1

    .line 374
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 382
    :catch_a
    move-exception v1

    .line 384
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 357
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_8

    :catchall_2
    move-exception v0

    goto :goto_8

    .line 351
    :catch_b
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_2

    :catch_c
    move-exception v0

    move-object v2, v1

    goto :goto_2
.end method

.method private static a(Ljava/io/File;Ljava/lang/String;Ljava/util/zip/ZipOutputStream;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 923
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 925
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".metadata"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 992
    :cond_0
    :goto_0
    return-void

    .line 930
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 931
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 933
    aget-object v2, v1, v0

    invoke-static {v2, p1, p2}, Lcom/wssnps/smlModelDefine;->a(Ljava/io/File;Ljava/lang/String;Ljava/util/zip/ZipOutputStream;)V

    .line 931
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 943
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 944
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 946
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 947
    :try_start_1
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 948
    :try_start_2
    new-instance v2, Ljava/util/zip/ZipEntry;

    invoke-direct {v2, v0}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 949
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/zip/ZipEntry;->setTime(J)V

    .line 950
    invoke-virtual {p2, v2}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 952
    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 954
    :goto_2
    const/4 v2, 0x0

    const/16 v4, 0x1000

    invoke-virtual {v1, v0, v2, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_4

    .line 956
    const/4 v4, 0x0

    invoke-virtual {p2, v0, v4, v2}, Ljava/util/zip/ZipOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_2

    .line 961
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 963
    :goto_3
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 967
    if-eqz v1, :cond_3

    .line 971
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 979
    :cond_3
    :goto_4
    if-eqz v2, :cond_0

    .line 983
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 985
    :catch_1
    move-exception v0

    .line 987
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 958
    :cond_4
    :try_start_6
    invoke-virtual {p2}, Ljava/util/zip/ZipOutputStream;->closeEntry()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 967
    if-eqz v1, :cond_5

    .line 971
    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 979
    :cond_5
    :goto_5
    if-eqz v3, :cond_0

    .line 983
    :try_start_8
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_0

    .line 985
    :catch_2
    move-exception v0

    .line 987
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 973
    :catch_3
    move-exception v0

    .line 975
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 973
    :catch_4
    move-exception v0

    .line 975
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 967
    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_6
    if-eqz v1, :cond_6

    .line 971
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 979
    :cond_6
    :goto_7
    if-eqz v3, :cond_7

    .line 983
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 988
    :cond_7
    :goto_8
    throw v0

    .line 973
    :catch_5
    move-exception v1

    .line 975
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 985
    :catch_6
    move-exception v1

    .line 987
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 967
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_6

    .line 961
    :catch_7
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_8
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_3
.end method

.method public static a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 249
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 251
    sget-boolean v1, Lcom/wssnps/smlModelDefine;->a:Z

    if-nez v1, :cond_0

    .line 253
    const-string v1, "CpuBoostSet :: cpuBooster is UP"

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 254
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 255
    const-string v2, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 256
    const-string v2, "SSRM_STATUS_NAME"

    const-string v3, "OBEX_dataTransfer"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string v2, "SSRM_STATUS_VALUE"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 258
    const-string v2, "PackageName"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 260
    sput-boolean v4, Lcom/wssnps/smlModelDefine;->a:Z

    .line 262
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v2, 0x0

    .line 836
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 837
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 839
    const/4 v0, 0x1

    const-string v1, "smlFileZip() - sourcePath is not file or not directory"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 919
    :cond_0
    :goto_0
    return-void

    .line 843
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 844
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 852
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/zip/ZipException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 853
    :try_start_1
    new-instance v3, Ljava/io/BufferedOutputStream;

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/util/zip/ZipException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 854
    :try_start_2
    new-instance v1, Ljava/util/zip/ZipOutputStream;

    invoke-direct {v1, v3}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/util/zip/ZipException; {:try_start_2 .. :try_end_2} :catch_12
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 856
    const/16 v2, 0x8

    :try_start_3
    invoke-virtual {v1, v2}, Ljava/util/zip/ZipOutputStream;->setLevel(I)V

    .line 857
    invoke-static {v0, p0, v1}, Lcom/wssnps/smlModelDefine;->a(Ljava/io/File;Ljava/lang/String;Ljava/util/zip/ZipOutputStream;)V

    .line 858
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->finish()V
    :try_end_3
    .catch Ljava/util/zip/ZipException; {:try_start_3 .. :try_end_3} :catch_13
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_10
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 871
    if-eqz v1, :cond_2

    .line 875
    :try_start_4
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 883
    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    .line 887
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 895
    :cond_3
    :goto_2
    if-eqz v4, :cond_4

    .line 899
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 908
    :cond_4
    :goto_3
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 910
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 911
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 912
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v3, Lcom/wssnps/smlModelDefine;->e:I

    invoke-static {v2, v3, v6}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I

    .line 914
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 916
    const-string v0, "renameTo fail"

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 877
    :catch_0
    move-exception v0

    .line 879
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 889
    :catch_1
    move-exception v0

    .line 891
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 901
    :catch_2
    move-exception v0

    .line 903
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 860
    :catch_3
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    .line 862
    :goto_4
    const/4 v4, 0x3

    :try_start_7
    invoke-virtual {v0}, Ljava/util/zip/ZipException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 871
    if-eqz v1, :cond_5

    .line 875
    :try_start_8
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 883
    :cond_5
    :goto_5
    if-eqz v2, :cond_6

    .line 887
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 895
    :cond_6
    :goto_6
    if-eqz v3, :cond_0

    .line 899
    :try_start_a
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    goto/16 :goto_0

    .line 901
    :catch_4
    move-exception v0

    .line 903
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 877
    :catch_5
    move-exception v0

    .line 879
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 889
    :catch_6
    move-exception v0

    .line 891
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 865
    :catch_7
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    .line 867
    :goto_7
    :try_start_b
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 871
    if-eqz v2, :cond_7

    .line 875
    :try_start_c
    invoke-virtual {v2}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 883
    :cond_7
    :goto_8
    if-eqz v3, :cond_8

    .line 887
    :try_start_d
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 895
    :cond_8
    :goto_9
    if-eqz v4, :cond_4

    .line 899
    :try_start_e
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_8

    goto :goto_3

    .line 901
    :catch_8
    move-exception v0

    .line 903
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 877
    :catch_9
    move-exception v0

    .line 879
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 889
    :catch_a
    move-exception v0

    .line 891
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 871
    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    :goto_a
    if-eqz v2, :cond_9

    .line 875
    :try_start_f
    invoke-virtual {v2}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    .line 883
    :cond_9
    :goto_b
    if-eqz v3, :cond_a

    .line 887
    :try_start_10
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_c

    .line 895
    :cond_a
    :goto_c
    if-eqz v4, :cond_b

    .line 899
    :try_start_11
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_d

    .line 904
    :cond_b
    :goto_d
    throw v0

    .line 877
    :catch_b
    move-exception v1

    .line 879
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 889
    :catch_c
    move-exception v1

    .line 891
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 901
    :catch_d
    move-exception v1

    .line 903
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 871
    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_a

    :catchall_2
    move-exception v0

    goto :goto_a

    :catchall_3
    move-exception v0

    move-object v2, v1

    goto :goto_a

    :catchall_4
    move-exception v0

    move-object v4, v3

    move-object v3, v2

    move-object v2, v1

    goto :goto_a

    .line 865
    :catch_e
    move-exception v0

    move-object v3, v2

    goto :goto_7

    :catch_f
    move-exception v0

    goto :goto_7

    :catch_10
    move-exception v0

    move-object v2, v1

    goto :goto_7

    .line 860
    :catch_11
    move-exception v0

    move-object v1, v2

    move-object v3, v4

    goto/16 :goto_4

    :catch_12
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_4

    :catch_13
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_4
.end method

.method public static a()Z
    .locals 3

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 137
    if-nez v1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v0

    .line 140
    :cond_1
    invoke-static {}, Lcom/wssnps/smlModelDefine;->A()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->c(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 517
    const/4 v2, 0x0

    .line 520
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 521
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/FileWriter;

    const/4 v5, 0x1

    invoke-direct {v4, v3, v5}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct {v1, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 523
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 532
    if-eqz v1, :cond_0

    .line 536
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 544
    :cond_0
    :goto_0
    return v0

    .line 538
    :catch_0
    move-exception v1

    .line 540
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 526
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 528
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 532
    if-eqz v1, :cond_1

    .line 536
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 544
    :cond_1
    :goto_2
    const/4 v0, 0x0

    goto :goto_0

    .line 538
    :catch_2
    move-exception v0

    .line 540
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 532
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v2, :cond_2

    .line 536
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 541
    :cond_2
    :goto_4
    throw v0

    .line 538
    :catch_3
    move-exception v1

    .line 540
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 532
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 526
    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method protected static a(Ljava/util/zip/ZipInputStream;Ljava/io/File;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1083
    const/4 v2, 0x0

    .line 1088
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1089
    const/16 v2, 0x1000

    :try_start_1
    new-array v3, v2, [B

    move v2, v0

    .line 1091
    :goto_0
    invoke-virtual {p0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    .line 1093
    if-nez v4, :cond_1

    .line 1095
    add-int/lit8 v2, v2, 0x1

    .line 1096
    const/16 v5, 0x32

    if-ne v2, v5, :cond_2

    .line 1113
    if-eqz v1, :cond_0

    .line 1117
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1126
    :cond_0
    :goto_1
    return v0

    .line 1119
    :catch_0
    move-exception v1

    .line 1121
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_1
    move v2, v0

    .line 1104
    :cond_2
    const/4 v5, 0x0

    :try_start_3
    invoke-virtual {v1, v3, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 1107
    :catch_1
    move-exception v0

    .line 1109
    :goto_2
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1113
    if-eqz v1, :cond_3

    .line 1117
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1126
    :cond_3
    :goto_3
    const/4 v0, 0x1

    goto :goto_1

    .line 1113
    :cond_4
    if-eqz v1, :cond_3

    .line 1117
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3

    .line 1119
    :catch_2
    move-exception v0

    .line 1121
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1119
    :catch_3
    move-exception v0

    .line 1121
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1113
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    if-eqz v1, :cond_5

    .line 1117
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 1122
    :cond_5
    :goto_5
    throw v0

    .line 1119
    :catch_4
    move-exception v1

    .line 1121
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1113
    :catchall_1
    move-exception v0

    goto :goto_4

    .line 1107
    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1953
    const-string v0, ""

    .line 1954
    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 1955
    invoke-virtual {v0}, Landroid/os/UserManager;->getUserName()Ljava/lang/String;

    move-result-object v0

    .line 1957
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1958
    const-string v0, ""

    .line 1959
    :cond_0
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 282
    .line 285
    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Internal:"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 286
    const-string v1, "/storage/sdcard0"

    const-string v2, "Internal:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 287
    const-string v1, "/mnt/sdcard"

    const-string v2, "Internal:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-static {}, Lcom/wssnps/smlModelDefine;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "External:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 291
    const-string v1, "/mnt/extSdCard"

    const-string v2, "External:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 292
    const-string v1, "/mnt/sdcard/external_sd"

    const-string v2, "External:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 293
    const-string v1, "/mnt/sdcard/extStorages/SdCard"

    const-string v2, "External:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 294
    const-string v1, "/mnt/sdcard/Storages/sd"

    const-string v2, "External:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 298
    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1131
    const/4 v0, 0x1

    const-string v1, "Media_Scan"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1133
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1134
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1135
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1136
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1140
    const-wide/16 v0, 0xbb8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1146
    :goto_0
    const/4 v0, 0x2

    const-string v1, "Media_Scan Finish"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1147
    return-void

    .line 1142
    :catch_0
    move-exception v0

    .line 1144
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 449
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 450
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    :goto_0
    return-void

    .line 453
    :cond_0
    new-instance v0, Lcom/wssnps/c/e;

    invoke-direct {v0, p0}, Lcom/wssnps/c/e;-><init>(Landroid/content/Context;)V

    .line 454
    invoke-virtual {v0}, Lcom/wssnps/c/e;->a()Ljava/lang/String;

    move-result-object v0

    .line 455
    invoke-static {p1, v0}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x0

    .line 149
    invoke-static {}, Lcom/wssnps/smlModelDefine;->A()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/wssnps/smlModelDefine;->y()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/wssnps/smlModelDefine;->z()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    :cond_0
    const/4 v0, 0x1

    .line 152
    :cond_1
    return v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 549
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 550
    const/4 v3, 0x0

    .line 553
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 569
    if-eqz v2, :cond_0

    .line 573
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 578
    :cond_0
    :goto_0
    return v0

    .line 575
    :catch_0
    move-exception v1

    .line 577
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 557
    :catch_1
    move-exception v0

    move-object v0, v3

    .line 559
    :goto_1
    const/4 v2, 0x1

    :try_start_3
    const-string v3, "file write fail!!"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 569
    if-eqz v0, :cond_1

    .line 573
    :try_start_4
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_1
    :goto_2
    move v0, v1

    .line 578
    goto :goto_0

    .line 575
    :catch_2
    move-exception v0

    .line 577
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 562
    :catch_3
    move-exception v0

    move-object v2, v3

    .line 564
    :goto_3
    const/4 v0, 0x1

    :try_start_5
    const-string v3, "file write fail!!"

    invoke-static {v0, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 569
    if-eqz v2, :cond_2

    .line 573
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_2
    :goto_4
    move v0, v1

    .line 578
    goto :goto_0

    .line 575
    :catch_4
    move-exception v0

    .line 577
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 569
    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_5
    if-eqz v2, :cond_3

    .line 573
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 578
    :cond_3
    :goto_6
    throw v0

    .line 575
    :catch_5
    move-exception v1

    .line 577
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 569
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    .line 562
    :catch_6
    move-exception v0

    goto :goto_3

    .line 557
    :catch_7
    move-exception v0

    move-object v0, v2

    goto :goto_1
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1201
    invoke-static {p0, p1}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 1202
    if-eqz v0, :cond_0

    .line 1203
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 1205
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 303
    .line 306
    const-string v0, "Internal:"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 309
    const-string v1, "External:"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 313
    return-object v0
.end method

.method public static c()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 157
    const/4 v1, 0x0

    .line 158
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 159
    if-nez v3, :cond_0

    .line 178
    :goto_0
    return v1

    .line 162
    :cond_0
    const-string v2, "com.sec.android.app.fm"

    invoke-static {v3, v2}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 163
    if-nez v2, :cond_1

    .line 165
    const-string v2, "first FM Radio not Support"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 166
    const-string v2, "com.samsung.app.fmradio"

    invoke-static {v3, v2}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 169
    :cond_1
    if-eqz v2, :cond_2

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v2, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v2, :cond_2

    :goto_1
    move v1, v0

    .line 178
    goto :goto_0

    .line 175
    :cond_2
    const-string v2, "second FM Radio not Support"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move v0, v1

    goto :goto_1
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1390
    .line 1392
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1393
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->isNetworkSupported(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1394
    const/4 v0, 0x1

    .line 1398
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 1396
    goto :goto_0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 997
    .line 1004
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1005
    :try_start_1
    new-instance v3, Ljava/util/zip/ZipInputStream;

    invoke-direct {v3, v4}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1007
    :try_start_2
    invoke-static {p1}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_a
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move v0, v1

    .line 1009
    :cond_0
    :goto_0
    :try_start_3
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1011
    invoke-virtual {v2}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1012
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    invoke-virtual {v2}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1015
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_0

    .line 1026
    :catch_0
    move-exception v2

    move-object v7, v2

    move-object v2, v3

    move-object v3, v4

    move v4, v0

    move-object v0, v7

    .line 1028
    :goto_1
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1063
    if-eqz v3, :cond_9

    .line 1067
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    move v0, v1

    .line 1077
    :cond_1
    :goto_2
    return v0

    .line 1019
    :cond_2
    :try_start_6
    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 1020
    invoke-static {v3, v6}, Lcom/wssnps/smlModelDefine;->a(Ljava/util/zip/ZipInputStream;Ljava/io/File;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result v0

    .line 1021
    if-nez v0, :cond_0

    .line 1033
    :cond_3
    if-eqz v0, :cond_5

    .line 1035
    if-eqz v3, :cond_4

    .line 1039
    :try_start_7
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 1047
    :cond_4
    :goto_3
    if-eqz v4, :cond_1

    .line 1051
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_2

    .line 1053
    :catch_1
    move-exception v1

    .line 1055
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1041
    :catch_2
    move-exception v1

    .line 1043
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1063
    :cond_5
    if-eqz v4, :cond_1

    .line 1067
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_2

    .line 1069
    :catch_3
    move-exception v1

    .line 1071
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1069
    :catch_4
    move-exception v0

    .line 1071
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v1

    .line 1072
    goto :goto_2

    .line 1033
    :catchall_0
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    :goto_4
    if-eqz v1, :cond_8

    .line 1035
    if-eqz v3, :cond_6

    .line 1039
    :try_start_a
    invoke-virtual {v3}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 1047
    :cond_6
    :goto_5
    if-eqz v4, :cond_7

    .line 1051
    :try_start_b
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 1072
    :cond_7
    :goto_6
    throw v0

    .line 1041
    :catch_5
    move-exception v1

    .line 1043
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1053
    :catch_6
    move-exception v1

    .line 1055
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1063
    :cond_8
    if-eqz v4, :cond_7

    .line 1067
    :try_start_c
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    goto :goto_6

    .line 1069
    :catch_7
    move-exception v1

    .line 1071
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1033
    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    :catchall_3
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    goto :goto_4

    :catchall_4
    move-exception v0

    move v1, v4

    move-object v4, v3

    move-object v3, v2

    goto :goto_4

    .line 1026
    :catch_8
    move-exception v0

    move-object v3, v2

    move v4, v1

    goto :goto_1

    :catch_9
    move-exception v0

    move-object v3, v4

    move v4, v1

    goto :goto_1

    :catch_a
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    move v4, v1

    goto :goto_1

    :cond_9
    move v0, v1

    goto :goto_2
.end method

.method public static d(Ljava/lang/String;)Ljavax/crypto/spec/SecretKeySpec;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 394
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 395
    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 396
    const/16 v1, 0x10

    new-array v1, v1, [B

    .line 397
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    array-length v2, v1

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 399
    const-string v0, "AES/CBC/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    sput-object v0, Lcom/wssnps/smlModelDefine;->f:Ljavax/crypto/Cipher;

    .line 400
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 402
    return-object v0
.end method

.method public static d()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 183
    const/4 v1, 0x0

    .line 184
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 185
    if-nez v2, :cond_0

    .line 198
    :goto_0
    return v1

    .line 188
    :cond_0
    const-string v3, "com.android.browser"

    invoke-static {v2, v3}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 189
    if-eqz v2, :cond_1

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v2, v2, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v2, :cond_1

    :goto_1
    move v1, v0

    .line 198
    goto :goto_0

    .line 195
    :cond_1
    const-string v2, "browser not Support"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move v0, v1

    goto :goto_1
.end method

.method public static d(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 1403
    .line 1405
    invoke-static {p0}, Landroid/util/GeneralUtil;->isVoiceCapable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1406
    const/4 v0, 0x1

    .line 1410
    :goto_0
    return v0

    .line 1408
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1415
    .line 1416
    const-string v0, ""

    .line 1417
    const-string v1, ""

    .line 1418
    const-string v0, ""

    .line 1419
    const-string v0, "getPhoneLockStatus"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1421
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.KIES_GET_EAS_STATUS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1422
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1424
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/com.sec.android.Kies/kies_limit_status.dat"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "path : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1426
    const/4 v0, 0x0

    .line 1429
    :cond_0
    invoke-static {v2}, Lcom/wssnps/b/d;->C(Ljava/lang/String;)[C

    move-result-object v3

    .line 1430
    if-nez v3, :cond_1

    .line 1432
    const-string v4, "FileLoad Fail!!"

    invoke-static {v6, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1435
    const-wide/16 v4, 0x12c

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1450
    :goto_0
    add-int/lit8 v0, v0, 0x1

    const/4 v4, 0x5

    if-le v0, v4, :cond_0

    move-object v0, v1

    .line 1456
    :goto_1
    if-nez v3, :cond_2

    .line 1458
    const-string v0, "Can\'t open IT Policy file PATH"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1459
    const-string v0, "FALSE"

    .line 1478
    :goto_2
    return-object v0

    .line 1437
    :catch_0
    move-exception v4

    .line 1439
    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 1445
    :cond_1
    const-string v0, "FileLoad OK!!"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1446
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([C)V

    goto :goto_1

    .line 1463
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File Data = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1464
    const-string v1, "True"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1466
    const-string v0, "TRUE"

    goto :goto_2

    .line 1468
    :cond_3
    const-string v1, "False"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1470
    const-string v0, "FALSE"

    goto :goto_2

    .line 1474
    :cond_4
    const-string v0, ""

    goto :goto_2
.end method

.method public static e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 460
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 461
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    :goto_0
    return-void

    .line 464
    :cond_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->v()Ljava/lang/String;

    move-result-object v0

    .line 465
    invoke-static {p0, v0}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    const-string v0, "/storage/extSdCard"

    return-object v0
.end method

.method public static f(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1622
    const-string v0, ""

    .line 1623
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "device_name"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1624
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1625
    const-string v0, ""

    .line 1626
    :cond_0
    return-object v0
.end method

.method public static f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 481
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/filelist.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 482
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 483
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 489
    :goto_0
    return-void

    .line 486
    :cond_0
    new-instance v1, Lcom/wssnps/c/c;

    invoke-direct {v1, p0}, Lcom/wssnps/c/c;-><init>(Ljava/lang/String;)V

    .line 487
    invoke-virtual {v1}, Lcom/wssnps/c/c;->a()Ljava/lang/String;

    move-result-object v1

    .line 488
    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1694
    const-string v0, ""

    .line 1695
    invoke-static {}, Lcom/wssnps/smlModelDefine;->A()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1697
    const-string v0, "ro.csc.sales_code"

    const-string v1, "XXX"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1698
    const-string v1, "KTT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1699
    const-string v0, "KTF"

    .line 1775
    :cond_0
    :goto_0
    return-object v0

    .line 1703
    :cond_1
    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v1

    .line 1704
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1706
    const-string v2, "KDI"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "DCM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "XJP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1708
    :cond_2
    const-string v0, "ro.csc.sales_code"

    const-string v1, "XXXXXXXXXXXXXX"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1712
    :cond_3
    const-string v1, "ro.csc.sales_code"

    const-string v2, "XXXXXXXXXXXXXX"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1713
    invoke-static {}, Lcom/wssnps/smlModelDefine;->k()Ljava/lang/String;

    move-result-object v2

    .line 1714
    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1715
    const-string v2, "XXX"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1716
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1721
    :cond_4
    const-string v0, "VZW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "SPR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "XAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "BST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "VMU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "KDI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "DCM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "USC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "MTR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "XAR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "TFN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "TMB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "CSP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "CRI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "LRA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ACG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "XJP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1738
    :cond_5
    const-string v0, "ro.csc.sales_code"

    const-string v2, "XXXXXXXXXXXXXX"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1744
    :goto_1
    const-string v2, "Not Active"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1746
    const-string v0, "CTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1747
    const-string v0, "XXXXXXXXXXXCTC"

    goto/16 :goto_0

    .line 1739
    :cond_6
    const-string v0, "CTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1740
    const-string v0, "ril.product_code"

    const-string v2, "XXXXXXXXXXXCTC"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1742
    :cond_7
    const-string v0, "ril.product_code"

    const-string v2, "XXXXXXXXXXXXXX"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1749
    :cond_8
    const-string v0, "XXXXXXXXXXXXXX"

    goto/16 :goto_0

    .line 1753
    :cond_9
    const-string v2, "VZW"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "SPR"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "XAS"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "BST"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "VMU"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "USC"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "MTR"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "XAR"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "TFN"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "TMB"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "CSP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "CRI"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "LRA"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "ACG"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1768
    :cond_a
    const-string v0, "XXXXXXXXXXXXXX"

    const/4 v2, 0x0

    const/16 v3, 0xb

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1769
    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static g()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 266
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cpuBooster release : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/wssnps/smlModelDefine;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 267
    sget-boolean v0, Lcom/wssnps/smlModelDefine;->a:Z

    if-eqz v0, :cond_0

    .line 269
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 270
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 271
    const-string v2, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    const-string v2, "SSRM_STATUS_NAME"

    const-string v3, "OBEX_dataTransfer"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    const-string v2, "SSRM_STATUS_VALUE"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 274
    const-string v2, "PackageName"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 276
    sput-boolean v4, Lcom/wssnps/smlModelDefine;->a:Z

    .line 278
    :cond_0
    return-void
.end method

.method public static g(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 586
    .line 587
    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    .line 592
    :goto_0
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 593
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 595
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 596
    if-eqz v2, :cond_0

    .line 597
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v3, Lcom/wssnps/smlModelDefine;->d:I

    invoke-static {v2, v3, p0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 606
    :cond_0
    :goto_1
    return v0

    .line 602
    :catch_0
    move-exception v2

    .line 604
    const-string v3, "file delete fail!!"

    invoke-static {v0, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 605
    invoke-virtual {v2}, Ljava/lang/SecurityException;->printStackTrace()V

    move v0, v1

    .line 606
    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_0
.end method

.method public static native getLibCryptionkey()Ljava/lang/String;
.end method

.method public static h()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1483
    const-string v0, ""

    .line 1484
    const-string v1, "/sys/class/android_usb/android0/bcdUSB"

    invoke-static {v1}, Lcom/wssnps/b/d;->C(Ljava/lang/String;)[C

    move-result-object v1

    .line 1485
    if-nez v1, :cond_0

    .line 1487
    const-string v1, "bcdUSB File Open Fail"

    invoke-static {v2, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1495
    :goto_0
    return-object v0

    .line 1491
    :cond_0
    const-string v0, "bcdUSB File Open Success"

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1492
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method

.method public static h(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1789
    const-string v0, ""

    .line 1791
    const-string v1, "SEC_FLOATING_FEATURE_KIES_SHOW_SECONDARY_IMEI"

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1792
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/android/telephony/MultiSimManager;->getImei(I)Ljava/lang/String;

    move-result-object v0

    .line 1799
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1800
    const-string v0, ""

    .line 1801
    :cond_1
    return-object v0

    .line 1795
    :cond_2
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->k(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 1796
    if-eqz v1, :cond_0

    .line 1797
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 793
    const/4 v0, 0x0

    .line 794
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 797
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 798
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    .line 800
    :cond_0
    return v0
.end method

.method public static i()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1503
    const-string v0, ""

    .line 1504
    const-string v0, ""

    .line 1505
    const-string v0, ""

    .line 1506
    const-string v0, ""

    .line 1508
    const-string v1, ""

    .line 1510
    const-string v2, "/sys/block/mmcblk0/device/cid"

    invoke-static {v2}, Lcom/wssnps/b/d;->C(Ljava/lang/String;)[C

    move-result-object v2

    .line 1511
    const-string v3, "/sys/block/mmcblk0/device/name"

    invoke-static {v3}, Lcom/wssnps/b/d;->C(Ljava/lang/String;)[C

    move-result-object v3

    .line 1513
    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 1515
    :cond_0
    const-string v0, "UN File Open Fail"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move-object v0, v1

    .line 1539
    :goto_0
    return-object v0

    .line 1519
    :cond_1
    const-string v1, "UN File Open Success"

    invoke-static {v6, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1521
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    .line 1522
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([C)V

    .line 1524
    const-string v3, "C"

    .line 1525
    invoke-virtual {v1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1527
    const-string v5, "15"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1528
    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1534
    :cond_2
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1535
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x12

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1536
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1529
    :cond_3
    const-string v5, "02"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "45"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1530
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {v2, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1531
    :cond_5
    const-string v5, "11"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1532
    invoke-virtual {v2, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static i(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 1806
    const-string v0, ""

    .line 1807
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->k(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 1808
    if-eqz v1, :cond_0

    .line 1810
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-eq v2, v3, :cond_2

    .line 1811
    const-string v0, ""

    .line 1815
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1816
    const-string v0, ""

    .line 1817
    :cond_1
    return-object v0

    .line 1812
    :cond_2
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 1813
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static i(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 806
    .line 807
    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 808
    const/4 v0, 0x1

    .line 810
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 811
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 830
    :cond_0
    :goto_1
    return-void

    .line 814
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 815
    :goto_2
    array-length v4, v3

    if-ge v1, v4, :cond_3

    .line 817
    aget-object v4, v3, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 819
    aget-object v4, v3, v1

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 815
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 823
    :cond_2
    aget-object v4, v3, v1

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    goto :goto_3

    .line 827
    :cond_3
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 828
    if-eqz v0, :cond_0

    .line 829
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v1, Lcom/wssnps/smlModelDefine;->d:I

    invoke-static {v0, v1, p0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static j()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1544
    const-string v0, ""

    .line 1545
    const-string v0, "dev.kies.sommode"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1546
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1547
    const-string v0, ""

    .line 1548
    :cond_0
    return-object v0
.end method

.method public static j(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 1822
    const-string v0, ""

    .line 1823
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->l(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 1825
    if-eqz v1, :cond_0

    .line 1827
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-eq v2, v3, :cond_2

    .line 1828
    const-string v0, ""

    .line 1833
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1834
    const-string v0, ""

    .line 1835
    :cond_1
    return-object v0

    .line 1829
    :cond_2
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 1830
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static j(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1210
    .line 1211
    const/4 v0, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szFullFilePath : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1215
    :try_start_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->d(Ljava/lang/String;)Ljavax/crypto/spec/SecretKeySpec;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1223
    :goto_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1225
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v4

    if-nez v4, :cond_0

    move v0, v2

    .line 1289
    :goto_1
    return v0

    .line 1217
    :catch_0
    move-exception v0

    .line 1219
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 1228
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1229
    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    .line 1231
    const-string v6, "."

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 1232
    invoke-virtual {v4, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1234
    new-instance v6, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".xml"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    :try_start_1
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1242
    :try_start_2
    invoke-static {v2, v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/io/InputStream;Ljavax/crypto/spec/SecretKeySpec;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 1243
    :try_start_3
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_d
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 1245
    const/16 v0, 0x400

    :try_start_4
    new-array v0, v0, [B

    .line 1246
    :goto_2
    const/4 v1, 0x0

    const/16 v5, 0x400

    invoke-virtual {v4, v0, v1, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    const/4 v5, -0x1

    if-eq v1, v5, :cond_4

    .line 1248
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_2

    .line 1251
    :catch_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    .line 1253
    :goto_3
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 1259
    if-eqz v1, :cond_1

    .line 1260
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 1269
    :cond_1
    :goto_4
    if-eqz v3, :cond_2

    .line 1270
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 1279
    :cond_2
    :goto_5
    if-eqz v2, :cond_3

    .line 1280
    :try_start_8
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 1288
    :cond_3
    :goto_6
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 1289
    const/4 v0, 0x1

    goto :goto_1

    .line 1259
    :cond_4
    if-eqz v2, :cond_5

    .line 1260
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 1269
    :cond_5
    :goto_7
    if-eqz v4, :cond_6

    .line 1270
    :try_start_a
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 1279
    :cond_6
    :goto_8
    if-eqz v3, :cond_3

    .line 1280
    :try_start_b
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    goto :goto_6

    .line 1282
    :catch_2
    move-exception v0

    .line 1284
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1262
    :catch_3
    move-exception v0

    .line 1264
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 1272
    :catch_4
    move-exception v0

    .line 1274
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 1262
    :catch_5
    move-exception v0

    .line 1264
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1272
    :catch_6
    move-exception v0

    .line 1274
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1282
    :catch_7
    move-exception v0

    .line 1284
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1257
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v4, v1

    .line 1259
    :goto_9
    if-eqz v2, :cond_7

    .line 1260
    :try_start_c
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 1269
    :cond_7
    :goto_a
    if-eqz v4, :cond_8

    .line 1270
    :try_start_d
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    .line 1279
    :cond_8
    :goto_b
    if-eqz v1, :cond_9

    .line 1280
    :try_start_e
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    .line 1285
    :cond_9
    :goto_c
    throw v0

    .line 1262
    :catch_8
    move-exception v2

    .line 1264
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 1272
    :catch_9
    move-exception v2

    .line 1274
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 1282
    :catch_a
    move-exception v1

    .line 1284
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 1257
    :catchall_1
    move-exception v0

    move-object v4, v1

    goto :goto_9

    :catchall_2
    move-exception v0

    goto :goto_9

    :catchall_3
    move-exception v0

    move-object v1, v3

    goto :goto_9

    :catchall_4
    move-exception v0

    move-object v4, v3

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    goto :goto_9

    .line 1251
    :catch_b
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_3

    :catch_c
    move-exception v0

    move-object v3, v1

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_3

    :catch_d
    move-exception v0

    move-object v3, v4

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto/16 :goto_3
.end method

.method public static k(Landroid/content/Context;)Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 1840
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1841
    if-nez v0, :cond_0

    .line 1845
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1852
    :goto_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1855
    :cond_0
    return-object v0

    .line 1847
    :catch_0
    move-exception v0

    .line 1849
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public static k()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1610
    const-string v0, "ro.product.model"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1611
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1612
    const-string v0, ""

    .line 1614
    :cond_0
    const-string v1, "SAMSUNG-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1615
    const-string v1, "SAMSUNG-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1617
    :cond_1
    return-object v0
.end method

.method public static k(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1295
    .line 1297
    const/4 v0, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "szFullFilePath : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1301
    :try_start_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->d(Ljava/lang/String;)Ljavax/crypto/spec/SecretKeySpec;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1309
    :goto_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1310
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v2

    .line 1373
    :goto_1
    return v0

    .line 1303
    :catch_0
    move-exception v0

    .line 1305
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 1313
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1314
    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    .line 1316
    const-string v6, "."

    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 1317
    invoke-virtual {v3, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1319
    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ".exml"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1326
    :try_start_2
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1327
    :try_start_3
    invoke-static {v2, v0}, Lcom/wssnps/smlModelDefine;->a(Ljava/io/OutputStream;Ljavax/crypto/spec/SecretKeySpec;)Ljava/io/OutputStream;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_d
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    .line 1329
    const/16 v0, 0x400

    :try_start_4
    new-array v0, v0, [B

    .line 1330
    :goto_2
    const/4 v4, 0x0

    const/16 v5, 0x400

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/FileInputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    .line 1332
    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_2

    .line 1335
    :catch_1
    move-exception v0

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    .line 1337
    :goto_3
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 1343
    if-eqz v3, :cond_1

    .line 1344
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 1353
    :cond_1
    :goto_4
    if-eqz v2, :cond_2

    .line 1354
    :try_start_7
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 1363
    :cond_2
    :goto_5
    if-eqz v1, :cond_3

    .line 1364
    :try_start_8
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 1372
    :cond_3
    :goto_6
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 1373
    const/4 v0, 0x1

    goto :goto_1

    .line 1343
    :cond_4
    if-eqz v3, :cond_5

    .line 1344
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 1353
    :cond_5
    :goto_7
    if-eqz v1, :cond_6

    .line 1354
    :try_start_a
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 1363
    :cond_6
    :goto_8
    if-eqz v2, :cond_3

    .line 1364
    :try_start_b
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    goto :goto_6

    .line 1366
    :catch_2
    move-exception v0

    .line 1368
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1346
    :catch_3
    move-exception v0

    .line 1348
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 1356
    :catch_4
    move-exception v0

    .line 1358
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 1346
    :catch_5
    move-exception v0

    .line 1348
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1356
    :catch_6
    move-exception v0

    .line 1358
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 1366
    :catch_7
    move-exception v0

    .line 1368
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 1341
    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    .line 1343
    :goto_9
    if-eqz v3, :cond_7

    .line 1344
    :try_start_c
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 1353
    :cond_7
    :goto_a
    if-eqz v1, :cond_8

    .line 1354
    :try_start_d
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    .line 1363
    :cond_8
    :goto_b
    if-eqz v2, :cond_9

    .line 1364
    :try_start_e
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    .line 1369
    :cond_9
    :goto_c
    throw v0

    .line 1346
    :catch_8
    move-exception v3

    .line 1348
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 1356
    :catch_9
    move-exception v1

    .line 1358
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 1366
    :catch_a
    move-exception v1

    .line 1368
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 1341
    :catchall_1
    move-exception v0

    move-object v3, v1

    goto :goto_9

    :catchall_2
    move-exception v0

    goto :goto_9

    :catchall_3
    move-exception v0

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_9

    .line 1335
    :catch_b
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    goto :goto_3

    :catch_c
    move-exception v0

    move-object v3, v1

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_3

    :catch_d
    move-exception v0

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    goto :goto_3
.end method

.method public static l(Landroid/content/Context;)Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 1860
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1861
    if-nez v0, :cond_0

    .line 1865
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1872
    :goto_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1875
    :cond_0
    return-object v0

    .line 1867
    :catch_0
    move-exception v0

    .line 1869
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public static l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1631
    const-string v0, ""

    .line 1632
    const-string v0, "ro.build.PDA"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1633
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1634
    const-string v0, ""

    .line 1635
    :cond_0
    return-object v0
.end method

.method public static l(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1378
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1385
    :cond_0
    :goto_0
    return v0

    .line 1381
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1382
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1383
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1640
    const-string v0, ""

    .line 1641
    const-string v0, "ril.sw_ver"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1642
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1643
    const-string v0, ""

    .line 1644
    :cond_0
    return-object v0
.end method

.method public static m(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1880
    const-string v0, ""

    .line 1881
    const-string v1, ""

    .line 1883
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->k(Landroid/content/Context;)Landroid/telephony/TelephonyManager;

    move-result-object v1

    .line 1917
    const-string v2, "ril.servicestate"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1918
    const-string v3, "0"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "2"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1921
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 1931
    :cond_1
    :goto_0
    return-object v0

    .line 1926
    :cond_2
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_1

    .line 1927
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static m(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2276
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2277
    const/4 v0, 0x0

    .line 2279
    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static n(Landroid/content/Context;)J
    .locals 2

    .prologue
    .line 1946
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    .line 1947
    const-string v0, "user"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 1948
    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getSerialNumberForUser(Landroid/os/UserHandle;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static n()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1649
    const-string v0, ""

    .line 1650
    const-string v0, "ril.official_cscver"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1651
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1652
    const-string v0, ""

    .line 1653
    :cond_0
    return-object v0
.end method

.method public static o(Landroid/content/Context;)Landroid/util/DisplayMetrics;
    .locals 2

    .prologue
    .line 1964
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1965
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1966
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1967
    return-object v1
.end method

.method public static o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1658
    const-string v0, ""

    .line 1659
    const-string v0, "ro.build.version.release"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1660
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1661
    const-string v0, ""

    .line 1662
    :cond_0
    return-object v0
.end method

.method public static p()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1667
    invoke-static {}, Lcom/wssnps/smlModelDefine;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1669
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1670
    const-string v1, "KTT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1671
    const-string v0, "KTF"

    .line 1689
    :cond_0
    :goto_0
    return-object v0

    .line 1675
    :cond_1
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1676
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1677
    const-string v0, "ril.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1679
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1681
    const-string v0, "ril.product_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1682
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1683
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1686
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1687
    const-string v0, ""

    goto :goto_0
.end method

.method public static p(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2026
    const-string v0, ""

    .line 2027
    if-eqz p0, :cond_0

    .line 2029
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2030
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    .line 2033
    :cond_0
    return-object v0
.end method

.method public static q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1780
    const-string v0, ""

    .line 1781
    const-string v0, "ril.serialnumber"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1782
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1783
    const-string v0, "ro.serialno"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1784
    :cond_0
    return-object v0
.end method

.method public static q(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2038
    const-string v0, ""

    .line 2039
    if-eqz p0, :cond_0

    .line 2040
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/profile/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2042
    :cond_0
    return-object v0
.end method

.method public static r()I
    .locals 1

    .prologue
    .line 1936
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    return v0
.end method

.method public static r(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2047
    const-string v0, ""

    .line 2048
    if-eqz p0, :cond_0

    .line 2049
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/libCryptionkey.so"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2051
    :cond_0
    return-object v0
.end method

.method public static s()I
    .locals 1

    .prologue
    .line 1941
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    return v0
.end method

.method public static s(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 2056
    const/4 v0, 0x0

    .line 2059
    :try_start_0
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 2060
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ServiceInfo.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/wssnps/smlModelDefine;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 2061
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hk.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->e(Ljava/lang/String;)V

    .line 2062
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "DeviceInfo.xml"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2064
    const/4 v0, 0x1

    .line 2071
    :goto_0
    return v0

    .line 2066
    :catch_0
    move-exception v1

    .line 2068
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static t()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1972
    const-string v0, ""

    .line 1973
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1974
    if-eqz v0, :cond_0

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976
    const-string v0, "Tablet"

    .line 1983
    :goto_0
    return-object v0

    .line 1980
    :cond_0
    const-string v0, "Phone"

    goto :goto_0
.end method

.method public static t(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x3

    .line 2108
    const-string v0, "loadCryptionkeyLibrary"

    invoke-static {v5, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2111
    invoke-static {p0}, Lcom/wssnps/smlModelDefine;->r(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2112
    const-string v3, "libCryptionkey.so"

    .line 2114
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2115
    sget-boolean v4, Lcom/wssnps/smlModelDefine;->g:Z

    if-nez v4, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2117
    :cond_0
    const-string v4, "FirstLoad Or Not Exist"

    invoke-static {v5, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2118
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2119
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 2123
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2124
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 2125
    new-array v4, v4, [B

    .line 2126
    invoke-virtual {v2, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 2128
    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 2129
    invoke-virtual {v1, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 2131
    :cond_2
    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2141
    if-eqz v1, :cond_3

    .line 2142
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2144
    :cond_3
    if-eqz v2, :cond_4

    .line 2145
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2152
    :cond_4
    :goto_0
    sput-boolean v8, Lcom/wssnps/smlModelDefine;->g:Z

    .line 2154
    :cond_5
    return-void

    .line 2147
    :catch_0
    move-exception v0

    .line 2149
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 2133
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 2135
    :goto_1
    const/4 v3, 0x3

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2141
    if-eqz v1, :cond_6

    .line 2142
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2144
    :cond_6
    if-eqz v2, :cond_4

    .line 2145
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 2147
    :catch_2
    move-exception v0

    .line 2149
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 2139
    :catchall_0
    move-exception v0

    move-object v2, v1

    .line 2141
    :goto_2
    if-eqz v1, :cond_7

    .line 2142
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2144
    :cond_7
    if-eqz v2, :cond_8

    .line 2145
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 2150
    :cond_8
    :goto_3
    throw v0

    .line 2147
    :catch_3
    move-exception v1

    .line 2149
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_3

    .line 2139
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 2133
    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method public static u()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1988
    const-string v0, ""

    .line 1989
    const-string v2, "content://com.android.calendar/syncTasks"

    .line 1990
    const-string v3, "content://tasks/tasks"

    .line 1991
    const/4 v1, 0x0

    .line 1995
    :try_start_0
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2002
    :goto_0
    if-eqz v1, :cond_1

    .line 2004
    const-string v0, "splanner"

    .line 2021
    :cond_0
    :goto_1
    return-object v0

    .line 1997
    :catch_0
    move-exception v2

    .line 1999
    invoke-virtual {v2}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0

    .line 2010
    :cond_1
    :try_start_1
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 2017
    :goto_2
    if-eqz v1, :cond_0

    .line 2018
    const-string v0, "normal"

    goto :goto_1

    .line 2012
    :catch_1
    move-exception v2

    .line 2014
    invoke-virtual {v2}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_2
.end method

.method public static u(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 2220
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 2221
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 2222
    const/16 v3, 0x3d

    iput v3, v2, Landroid/os/Message;->what:I

    .line 2227
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2234
    :goto_0
    if-eq v0, v1, :cond_0

    .line 2235
    const/4 v0, 0x1

    .line 2237
    :goto_1
    return v0

    .line 2229
    :catch_0
    move-exception v0

    .line 2231
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v1

    goto :goto_0

    .line 2237
    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static v()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2076
    const-string v0, ""

    .line 2081
    :try_start_0
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 2082
    invoke-static {}, Lcom/wssnps/smlModelDefine;->w()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update([B)V

    .line 2083
    const/16 v2, 0x10

    new-array v2, v2, [B

    .line 2084
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    array-length v5, v2

    invoke-static {v1, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2085
    const/4 v1, 0x0

    invoke-static {v2, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 2100
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2101
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2103
    :cond_0
    return-object v0

    .line 2087
    :catch_0
    move-exception v1

    .line 2089
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 2091
    :catch_1
    move-exception v1

    .line 2093
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 2095
    :catch_2
    move-exception v1

    .line 2097
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static v(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    .line 2242
    .line 2243
    const-string v1, ""

    .line 2244
    const-string v1, "com.android.calendar"

    invoke-static {p0, v1}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 2245
    if-eqz v1, :cond_1

    .line 2247
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 2248
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 2249
    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Calendar APK PATH = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2251
    const-string v2, "/system/app/SPlanner.apk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/system/app/SPlanner_2013.apk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/system/app/SPlanner_ESS.apk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2253
    :cond_0
    const-string v1, "Calendar AttachFile not Support"

    invoke-static {v5, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2262
    :cond_1
    :goto_0
    return v0

    .line 2256
    :cond_2
    const-string v2, "/system/app/SecCalendar.apk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2258
    const-string v0, "Calendar AttachFile Support"

    invoke-static {v5, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2259
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2160
    const/4 v0, 0x0

    .line 2163
    :try_start_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->getLibCryptionkey()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2170
    :goto_0
    return-object v0

    .line 2165
    :catch_0
    move-exception v1

    .line 2167
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public static w(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2289
    sget-object v0, Lcom/wssnps/smlModelDefine;->b:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 2291
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 2292
    if-nez v0, :cond_1

    .line 2294
    const-string v0, "PowerManager is null!!"

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2312
    :cond_0
    :goto_0
    return-void

    .line 2298
    :cond_1
    const v1, 0x30000006

    const-string v2, "wssyncmlnps"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/wssnps/smlModelDefine;->b:Landroid/os/PowerManager$WakeLock;

    .line 2299
    sget-object v0, Lcom/wssnps/smlModelDefine;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 2301
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 2302
    if-eqz v0, :cond_0

    .line 2304
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2306
    const-string v1, "in lockscreen"

    invoke-static {v3, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2307
    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/app/KeyguardManager;->newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;

    move-result-object v0

    .line 2308
    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->disableKeyguard()V

    goto :goto_0
.end method

.method public static x()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2175
    const-string v0, "ro.product_ship"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static x(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2318
    :try_start_0
    sget-object v0, Lcom/wssnps/smlModelDefine;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 2320
    const/4 v0, 0x1

    const-string v1, "mWakeLock is release"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2321
    sget-object v0, Lcom/wssnps/smlModelDefine;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2322
    const/4 v0, 0x0

    sput-object v0, Lcom/wssnps/smlModelDefine;->b:Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2329
    :cond_0
    :goto_0
    return-void

    .line 2325
    :catch_0
    move-exception v0

    .line 2327
    const/4 v1, 0x3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static y()Z
    .locals 2

    .prologue
    .line 2180
    const-string v0, "ro.csc.country_code"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2182
    const-string v1, "CHINA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2183
    const/4 v0, 0x1

    .line 2185
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static z()Z
    .locals 2

    .prologue
    .line 2190
    const-string v0, "ro.csc.country_code"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2192
    const-string v1, "TAIWAN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "HONG KONG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2193
    :cond_0
    const/4 v0, 0x1

    .line 2195
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
