.class public final enum Lcom/wssnps/a/q;
.super Ljava/lang/Enum;
.source "smlvCal.java"


# static fields
.field public static final enum a:Lcom/wssnps/a/q;

.field public static final enum b:Lcom/wssnps/a/q;

.field private static final synthetic c:[Lcom/wssnps/a/q;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/wssnps/a/q;

    const-string v1, "VCAL_V10"

    invoke-direct {v0, v1, v2}, Lcom/wssnps/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssnps/a/q;->a:Lcom/wssnps/a/q;

    .line 16
    new-instance v0, Lcom/wssnps/a/q;

    const-string v1, "VCAL_V20"

    invoke-direct {v0, v1, v3}, Lcom/wssnps/a/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/wssnps/a/q;->b:Lcom/wssnps/a/q;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/wssnps/a/q;

    sget-object v1, Lcom/wssnps/a/q;->a:Lcom/wssnps/a/q;

    aput-object v1, v0, v2

    sget-object v1, Lcom/wssnps/a/q;->b:Lcom/wssnps/a/q;

    aput-object v1, v0, v3

    sput-object v0, Lcom/wssnps/a/q;->c:[Lcom/wssnps/a/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/wssnps/a/q;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/wssnps/a/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/q;

    return-object v0
.end method

.method public static values()[Lcom/wssnps/a/q;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/wssnps/a/q;->c:[Lcom/wssnps/a/q;

    array-length v1, v0

    new-array v2, v1, [Lcom/wssnps/a/q;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
