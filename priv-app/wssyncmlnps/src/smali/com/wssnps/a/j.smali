.class public Lcom/wssnps/a/j;
.super Ljava/lang/Object;
.source "smlvCal.java"

# interfaces
.implements Lcom/wssnps/a/b;


# direct methods
.method public static a(Ljava/lang/String;)Lcom/wssnps/a/i;
    .locals 2

    .prologue
    .line 191
    new-instance v0, Lcom/wssnps/a/i;

    invoke-direct {v0}, Lcom/wssnps/a/i;-><init>()V

    .line 192
    sget-object v1, Lcom/wssnps/a/g;->c:Lcom/wssnps/a/h;

    invoke-virtual {v0, p0, v1}, Lcom/wssnps/a/i;->a(Ljava/lang/String;Lcom/wssnps/a/h;)Z

    .line 193
    return-object v0
.end method

.method public static a(Lcom/wssnps/a/r;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 198
    .line 201
    if-nez p0, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 387
    :goto_0
    return-object v0

    .line 206
    :cond_0
    invoke-static {v2}, Lcom/wssnps/a/ad;->a(I)Lcom/wssnps/a/ag;

    move-result-object v4

    .line 207
    const-string v0, "1.0"

    invoke-static {v0}, Lcom/wssnps/a/ad;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 210
    iget v1, p0, Lcom/wssnps/a/r;->c:I

    if-ne v1, v7, :cond_23

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/wssnps/a/ad;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    :goto_1
    iget v1, p0, Lcom/wssnps/a/r;->m:I

    if-eqz v1, :cond_1

    .line 217
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x8

    iget v3, p0, Lcom/wssnps/a/r;->m:I

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 220
    :cond_1
    iget-object v1, p0, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 221
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    invoke-static {v7, v0, v4}, Lcom/wssnps/a/ad;->c(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 224
    :cond_2
    iget-object v1, p0, Lcom/wssnps/a/r;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 225
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x9

    iget-object v3, p0, Lcom/wssnps/a/r;->d:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/wssnps/a/ad;->c(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    :cond_3
    iget-object v1, p0, Lcom/wssnps/a/r;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 229
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0xd

    iget-object v3, p0, Lcom/wssnps/a/r;->f:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/wssnps/a/ad;->c(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    :cond_4
    iget-object v1, p0, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-eqz v1, :cond_24

    .line 233
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x3

    iget-object v3, p0, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget v5, p0, Lcom/wssnps/a/r;->t:I

    invoke-static {v0, v3, v5}, Lcom/wssnps/a/ad;->a(ILandroid/text/format/Time;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 238
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    if-eqz v1, :cond_6

    .line 239
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x4

    iget-object v3, p0, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    iget v5, p0, Lcom/wssnps/a/r;->t:I

    invoke-static {v0, v3, v5}, Lcom/wssnps/a/ad;->a(ILandroid/text/format/Time;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 242
    :cond_6
    iget v1, p0, Lcom/wssnps/a/r;->c:I

    if-eq v1, v7, :cond_7

    .line 244
    iget v1, p0, Lcom/wssnps/a/r;->t:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_7

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x15

    iget v3, p0, Lcom/wssnps/a/r;->t:I

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->b(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 248
    :cond_7
    iget v1, p0, Lcom/wssnps/a/r;->v:I

    if-eqz v1, :cond_8

    .line 249
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x19

    iget v3, p0, Lcom/wssnps/a/r;->v:I

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    :cond_8
    iget-object v1, p0, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    if-eqz v1, :cond_25

    .line 253
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0xc

    iget-object v3, p0, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    iget v5, p0, Lcom/wssnps/a/r;->t:I

    invoke-static {v0, v3, v5}, Lcom/wssnps/a/ad;->a(ILandroid/text/format/Time;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 258
    :cond_9
    :goto_3
    iget v1, p0, Lcom/wssnps/a/r;->l:I

    if-eqz v1, :cond_a

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x7

    iget v3, p0, Lcom/wssnps/a/r;->l:I

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 262
    :cond_a
    iget v1, p0, Lcom/wssnps/a/r;->n:I

    if-eqz v1, :cond_b

    .line 263
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0xb

    iget v3, p0, Lcom/wssnps/a/r;->n:I

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->d(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 266
    :cond_b
    iget v1, p0, Lcom/wssnps/a/r;->k:I

    if-eqz v1, :cond_c

    .line 267
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, p0, Lcom/wssnps/a/r;->k:I

    invoke-static {v0}, Lcom/wssnps/a/ad;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 270
    :cond_c
    iget-object v1, p0, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    move v1, v2

    .line 272
    :goto_4
    iget-object v3, p0, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_26

    .line 279
    :cond_d
    iget-object v1, p0, Lcom/wssnps/a/r;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    move v1, v2

    .line 281
    :goto_5
    iget-object v3, p0, Lcom/wssnps/a/r;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_27

    .line 287
    :cond_e
    iget-object v1, p0, Lcom/wssnps/a/r;->u:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 288
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x28

    iget-object v3, p0, Lcom/wssnps/a/r;->u:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/wssnps/a/ad;->c(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 291
    :cond_f
    iget-object v1, p0, Lcom/wssnps/a/r;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/wssnps/a/r;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_10

    .line 292
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x29

    const-string v3, "1.0"

    invoke-static {v0, v3, v4}, Lcom/wssnps/a/ad;->c(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 295
    :cond_10
    iget-object v1, p0, Lcom/wssnps/a/r;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_11

    iget-object v1, p0, Lcom/wssnps/a/r;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_11

    .line 296
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/wssnps/a/r;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget v5, p0, Lcom/wssnps/a/r;->t:I

    iget-object v6, p0, Lcom/wssnps/a/r;->u:Ljava/lang/String;

    invoke-static {v0, v3, v5, v6}, Lcom/wssnps/a/ad;->a(Ljava/lang/String;Landroid/text/format/Time;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 299
    :cond_11
    iget v1, p0, Lcom/wssnps/a/r;->z:I

    if-eqz v1, :cond_12

    .line 300
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x23

    iget v3, p0, Lcom/wssnps/a/r;->z:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 302
    :cond_12
    iget-object v1, p0, Lcom/wssnps/a/r;->A:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 303
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x22

    iget-object v3, p0, Lcom/wssnps/a/r;->A:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 306
    :cond_13
    iget-object v1, p0, Lcom/wssnps/a/r;->B:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 307
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x1e

    iget-object v3, p0, Lcom/wssnps/a/r;->B:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 310
    :cond_14
    iget-object v1, p0, Lcom/wssnps/a/r;->C:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 311
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x1f

    iget-object v3, p0, Lcom/wssnps/a/r;->C:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 314
    :cond_15
    iget-object v1, p0, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 315
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x20

    iget-object v3, p0, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/wssnps/a/ad;->c(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 318
    :cond_16
    iget-object v1, p0, Lcom/wssnps/a/r;->E:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 319
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x21

    iget-object v3, p0, Lcom/wssnps/a/r;->E:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 322
    :cond_17
    iget-object v1, p0, Lcom/wssnps/a/r;->F:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_18

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x24

    iget-object v3, p0, Lcom/wssnps/a/r;->F:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 326
    :cond_18
    iget-object v1, p0, Lcom/wssnps/a/r;->G:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 327
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x25

    iget-object v3, p0, Lcom/wssnps/a/r;->G:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 330
    :cond_19
    iget-object v1, p0, Lcom/wssnps/a/r;->H:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1a

    .line 331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x26

    iget-object v3, p0, Lcom/wssnps/a/r;->H:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 334
    :cond_1a
    iget-object v1, p0, Lcom/wssnps/a/r;->w:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 335
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x16

    iget-object v3, p0, Lcom/wssnps/a/r;->w:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/wssnps/a/ad;->c(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 338
    :cond_1b
    iget v1, p0, Lcom/wssnps/a/r;->x:I

    if-eqz v1, :cond_1c

    .line 339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, p0, Lcom/wssnps/a/r;->x:I

    invoke-static {v0}, Lcom/wssnps/a/ad;->h(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 342
    :cond_1c
    iget-object v1, p0, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    if-eqz v1, :cond_1d

    .line 343
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x18

    iget-object v3, p0, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    iget v5, p0, Lcom/wssnps/a/r;->t:I

    invoke-static {v0, v3, v5}, Lcom/wssnps/a/ad;->a(ILandroid/text/format/Time;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 345
    :cond_1d
    iget-object v1, p0, Lcom/wssnps/a/r;->I:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 346
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x27

    iget-object v3, p0, Lcom/wssnps/a/r;->I:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/wssnps/a/ad;->c(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 348
    :cond_1e
    iget-object v1, p0, Lcom/wssnps/a/r;->J:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1f

    move v1, v2

    .line 350
    :goto_6
    iget-object v3, p0, Lcom/wssnps/a/r;->J:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_28

    .line 356
    :cond_1f
    iget-object v1, p0, Lcom/wssnps/a/r;->K:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_20

    move v1, v2

    .line 358
    :goto_7
    iget-object v3, p0, Lcom/wssnps/a/r;->K:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_29

    .line 364
    :cond_20
    iget-object v1, p0, Lcom/wssnps/a/r;->L:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_21

    move v1, v2

    .line 367
    :goto_8
    iget-object v3, p0, Lcom/wssnps/a/r;->L:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_2a

    .line 373
    :cond_21
    iget-object v1, p0, Lcom/wssnps/a/r;->M:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_22

    .line 375
    :goto_9
    iget-object v1, p0, Lcom/wssnps/a/r;->M:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v2, v1, :cond_2b

    .line 381
    :cond_22
    iget v1, p0, Lcom/wssnps/a/r;->c:I

    if-ne v1, v7, :cond_2c

    .line 382
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/wssnps/a/ad;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 386
    :goto_a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/wssnps/a/ad;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 213
    :cond_23
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/wssnps/a/ad;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 234
    :cond_24
    iget v1, p0, Lcom/wssnps/a/r;->c:I

    if-ne v1, v7, :cond_5

    iget-object v1, p0, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 235
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "DTSTART:20370101T090000\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 254
    :cond_25
    iget v1, p0, Lcom/wssnps/a/r;->c:I

    if-ne v1, v7, :cond_9

    iget-object v1, p0, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 255
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "DUE:20370101T090000\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 274
    :cond_26
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0xe

    iget-object v0, p0, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget v6, p0, Lcom/wssnps/a/r;->t:I

    invoke-static {v5, v0, v4, v6}, Lcom/wssnps/a/ad;->a(ILcom/wssnps/a/k;Lcom/wssnps/a/ag;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 272
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto/16 :goto_4

    .line 283
    :cond_27
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x6

    iget-object v0, p0, Lcom/wssnps/a/r;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget v6, p0, Lcom/wssnps/a/r;->t:I

    invoke-static {v5, v0, v4, v6}, Lcom/wssnps/a/ad;->a(ILcom/wssnps/a/k;Lcom/wssnps/a/ag;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 281
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto/16 :goto_5

    .line 352
    :cond_28
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x1a

    iget-object v0, p0, Lcom/wssnps/a/r;->J:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/l;

    invoke-static {v5, v0, v4}, Lcom/wssnps/a/ad;->a(ILcom/wssnps/a/l;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 350
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto/16 :goto_6

    .line 360
    :cond_29
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x1b

    iget-object v0, p0, Lcom/wssnps/a/r;->K:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/n;

    invoke-static {v5, v0}, Lcom/wssnps/a/ad;->a(ILcom/wssnps/a/n;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 358
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto/16 :goto_7

    .line 369
    :cond_2a
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x1c

    iget-object v0, p0, Lcom/wssnps/a/r;->L:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    invoke-static {v5, v0, v4}, Lcom/wssnps/a/ad;->a(ILcom/wssnps/a/m;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 367
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto/16 :goto_8

    .line 377
    :cond_2b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x1d

    iget-object v0, p0, Lcom/wssnps/a/r;->M:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    invoke-static {v3, v0, v4}, Lcom/wssnps/a/ad;->a(ILcom/wssnps/a/o;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 375
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_9

    .line 384
    :cond_2c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/wssnps/a/ad;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_a
.end method

.method public static b(Ljava/lang/String;)Lcom/wssnps/a/r;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 392
    .line 396
    const-string v1, ""

    .line 398
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 399
    const/4 v0, 0x0

    .line 563
    :goto_0
    return-object v0

    .line 401
    :cond_0
    new-instance v3, Lcom/wssnps/a/r;

    invoke-direct {v3}, Lcom/wssnps/a/r;-><init>()V

    .line 403
    invoke-static {v0}, Lcom/wssnps/a/ad;->a(I)Lcom/wssnps/a/ag;

    move-result-object v4

    .line 404
    invoke-static {p0}, Lcom/wssnps/a/j;->a(Ljava/lang/String;)Lcom/wssnps/a/i;

    move-result-object v5

    move v2, v0

    .line 406
    :goto_1
    iget-object v0, v5, Lcom/wssnps/a/i;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v2, v0, :cond_1

    move-object v0, v3

    .line 563
    goto :goto_0

    .line 408
    :cond_1
    iget-object v0, v5, Lcom/wssnps/a/i;->a:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/f;

    .line 409
    iget v6, v0, Lcom/wssnps/a/f;->a:I

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    .line 560
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1

    :pswitch_1
    move-object v0, v1

    .line 412
    goto :goto_2

    :pswitch_2
    move-object v0, v1

    .line 415
    goto :goto_2

    .line 418
    :pswitch_3
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    move-object v0, v1

    .line 419
    goto :goto_2

    .line 422
    :pswitch_4
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->d:Ljava/lang/String;

    move-object v0, v1

    .line 423
    goto :goto_2

    .line 426
    :pswitch_5
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->f:Ljava/lang/String;

    move-object v0, v1

    .line 427
    goto :goto_2

    .line 430
    :pswitch_6
    iget-object v1, v0, Lcom/wssnps/a/f;->c:Ljava/lang/String;

    .line 431
    invoke-static {v0}, Lcom/wssnps/a/ad;->g(Lcom/wssnps/a/f;)Landroid/text/format/Time;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    move-object v0, v1

    .line 432
    goto :goto_2

    .line 435
    :pswitch_7
    invoke-static {v0}, Lcom/wssnps/a/ad;->g(Lcom/wssnps/a/f;)Landroid/text/format/Time;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    move-object v0, v1

    .line 436
    goto :goto_2

    .line 439
    :pswitch_8
    invoke-static {v0}, Lcom/wssnps/a/ad;->g(Lcom/wssnps/a/f;)Landroid/text/format/Time;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    move-object v0, v1

    .line 440
    goto :goto_2

    .line 443
    :pswitch_9
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->u:Ljava/lang/String;

    move-object v0, v1

    .line 444
    goto :goto_2

    .line 447
    :pswitch_a
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->h:Ljava/lang/String;

    move-object v0, v1

    .line 448
    goto :goto_2

    .line 451
    :pswitch_b
    iget v6, v3, Lcom/wssnps/a/r;->t:I

    iget-object v7, v3, Lcom/wssnps/a/r;->u:Ljava/lang/String;

    iget-object v8, v3, Lcom/wssnps/a/r;->h:Ljava/lang/String;

    invoke-static {v0, v1, v6, v7, v8}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->g:Ljava/lang/String;

    move-object v0, v1

    .line 452
    goto :goto_2

    .line 455
    :pswitch_c
    iget-object v6, v3, Lcom/wssnps/a/r;->j:Ljava/util/List;

    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->t(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/k;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 456
    goto :goto_2

    .line 459
    :pswitch_d
    iget-object v6, v3, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->t(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/k;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 460
    goto :goto_2

    .line 463
    :pswitch_e
    invoke-static {v0}, Lcom/wssnps/a/ad;->k(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v3, Lcom/wssnps/a/r;->k:I

    move-object v0, v1

    .line 464
    goto :goto_2

    .line 467
    :pswitch_f
    invoke-static {v0}, Lcom/wssnps/a/ad;->h(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v3, Lcom/wssnps/a/r;->l:I

    move-object v0, v1

    .line 468
    goto :goto_2

    .line 471
    :pswitch_10
    invoke-static {v0}, Lcom/wssnps/a/ad;->h(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v3, Lcom/wssnps/a/r;->n:I

    move-object v0, v1

    .line 472
    goto/16 :goto_2

    .line 475
    :pswitch_11
    invoke-static {v0}, Lcom/wssnps/a/ad;->h(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v3, Lcom/wssnps/a/r;->m:I

    move-object v0, v1

    .line 476
    goto/16 :goto_2

    .line 479
    :pswitch_12
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->u(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->r:Ljava/util/List;

    move-object v0, v1

    .line 480
    goto/16 :goto_2

    .line 483
    :pswitch_13
    invoke-static {v0}, Lcom/wssnps/a/ad;->i(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v3, Lcom/wssnps/a/r;->t:I

    move-object v0, v1

    .line 484
    goto/16 :goto_2

    .line 487
    :pswitch_14
    invoke-static {v0}, Lcom/wssnps/a/ad;->l(Lcom/wssnps/a/f;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->w:Ljava/lang/String;

    move-object v0, v1

    .line 488
    goto/16 :goto_2

    .line 491
    :pswitch_15
    invoke-static {v0}, Lcom/wssnps/a/ad;->m(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v3, Lcom/wssnps/a/r;->x:I

    move-object v0, v1

    .line 492
    goto/16 :goto_2

    .line 495
    :pswitch_16
    invoke-static {v0}, Lcom/wssnps/a/ad;->g(Lcom/wssnps/a/f;)Landroid/text/format/Time;

    move-result-object v6

    iput-object v6, v3, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    .line 498
    :pswitch_17
    invoke-static {v0}, Lcom/wssnps/a/ad;->j(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v3, Lcom/wssnps/a/r;->v:I

    move-object v0, v1

    .line 499
    goto/16 :goto_2

    .line 502
    :pswitch_18
    iget-object v6, v3, Lcom/wssnps/a/r;->J:Ljava/util/List;

    sget-object v7, Lcom/wssnps/a/g;->c:Lcom/wssnps/a/h;

    invoke-static {v0, v7, v4}, Lcom/wssnps/a/ad;->b(Lcom/wssnps/a/f;Lcom/wssnps/a/h;Lcom/wssnps/a/ag;)Lcom/wssnps/a/l;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 503
    goto/16 :goto_2

    .line 506
    :pswitch_19
    iget-object v6, v3, Lcom/wssnps/a/r;->K:Ljava/util/List;

    invoke-static {v0}, Lcom/wssnps/a/ad;->n(Lcom/wssnps/a/f;)Lcom/wssnps/a/n;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 507
    goto/16 :goto_2

    .line 510
    :pswitch_1a
    iget-object v6, v3, Lcom/wssnps/a/r;->L:Ljava/util/List;

    sget-object v7, Lcom/wssnps/a/g;->c:Lcom/wssnps/a/h;

    invoke-static {v0, v7, v4}, Lcom/wssnps/a/ad;->c(Lcom/wssnps/a/f;Lcom/wssnps/a/h;Lcom/wssnps/a/ag;)Lcom/wssnps/a/m;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 511
    goto/16 :goto_2

    .line 514
    :pswitch_1b
    iget-object v6, v3, Lcom/wssnps/a/r;->M:Ljava/util/List;

    sget-object v7, Lcom/wssnps/a/g;->c:Lcom/wssnps/a/h;

    invoke-static {v0, v7, v4}, Lcom/wssnps/a/ad;->d(Lcom/wssnps/a/f;Lcom/wssnps/a/h;Lcom/wssnps/a/ag;)Lcom/wssnps/a/o;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    .line 515
    goto/16 :goto_2

    .line 518
    :pswitch_1c
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v3, Lcom/wssnps/a/r;->z:I

    move-object v0, v1

    .line 519
    goto/16 :goto_2

    .line 522
    :pswitch_1d
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->A:Ljava/lang/String;

    move-object v0, v1

    .line 523
    goto/16 :goto_2

    .line 526
    :pswitch_1e
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->B:Ljava/lang/String;

    move-object v0, v1

    .line 527
    goto/16 :goto_2

    .line 530
    :pswitch_1f
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->C:Ljava/lang/String;

    move-object v0, v1

    .line 531
    goto/16 :goto_2

    .line 534
    :pswitch_20
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    move-object v0, v1

    .line 535
    goto/16 :goto_2

    .line 538
    :pswitch_21
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->E:Ljava/lang/String;

    move-object v0, v1

    .line 539
    goto/16 :goto_2

    .line 542
    :pswitch_22
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->F:Ljava/lang/String;

    move-object v0, v1

    .line 543
    goto/16 :goto_2

    .line 546
    :pswitch_23
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->G:Ljava/lang/String;

    move-object v0, v1

    .line 547
    goto/16 :goto_2

    .line 550
    :pswitch_24
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->H:Ljava/lang/String;

    move-object v0, v1

    .line 551
    goto/16 :goto_2

    .line 554
    :pswitch_25
    invoke-static {v0, v4}, Lcom/wssnps/a/ad;->s(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/wssnps/a/r;->I:Ljava/lang/String;

    move-object v0, v1

    .line 555
    goto/16 :goto_2

    .line 409
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_b
        :pswitch_c
        :pswitch_f
        :pswitch_11
        :pswitch_4
        :pswitch_e
        :pswitch_10
        :pswitch_8
        :pswitch_5
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_1d
        :pswitch_1c
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
