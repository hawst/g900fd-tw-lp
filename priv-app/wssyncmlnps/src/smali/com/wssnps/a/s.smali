.class public Lcom/wssnps/a/s;
.super Ljava/lang/Object;
.source "smlvCard.java"

# interfaces
.implements Lcom/wssnps/a/b;


# direct methods
.method public static a(Lcom/wssnps/a/ac;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/16 v9, 0x16

    const/4 v2, 0x0

    .line 149
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 152
    if-nez p0, :cond_0

    move-object v0, v3

    .line 452
    :goto_0
    return-object v0

    .line 155
    :cond_0
    invoke-static {v2}, Lcom/wssnps/a/ad;->a(I)Lcom/wssnps/a/ag;

    move-result-object v6

    .line 158
    const-string v0, "2.1"

    invoke-static {v0}, Lcom/wssnps/a/ad;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 161
    iget-object v0, p0, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-boolean v0, v0, Lcom/wssnps/a/y;->j:Z

    if-eqz v0, :cond_1

    .line 162
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    invoke-static {v0, v1, v6}, Lcom/wssnps/a/ad;->a(ILcom/wssnps/a/y;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v1, v2

    .line 171
    :goto_1
    iget-object v0, p0, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1a

    .line 181
    :cond_2
    iget-object v0, p0, Lcom/wssnps/a/ac;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    move v1, v2

    .line 185
    :goto_2
    iget-object v0, p0, Lcom/wssnps/a/ac;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1c

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    move v1, v2

    .line 198
    :goto_3
    iget-object v0, p0, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1e

    .line 207
    :cond_4
    iget-object v0, p0, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 210
    const-string v1, ""

    .line 211
    iget-object v0, p0, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move-object v4, v1

    move-object v0, v3

    move v3, v2

    move v1, v2

    .line 213
    :goto_4
    if-lt v3, v7, :cond_20

    .line 230
    if-eqz v0, :cond_5

    .line 232
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    if-eqz v1, :cond_5

    .line 233
    const/16 v0, 0x17

    invoke-static {v0, v4, v6}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 238
    :cond_5
    iget-object v0, p0, Lcom/wssnps/a/ac;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 239
    const/16 v0, 0x21

    iget-object v1, p0, Lcom/wssnps/a/ac;->z:Ljava/lang/String;

    invoke-static {v0, v1, v6}, Lcom/wssnps/a/ad;->b(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 242
    :cond_6
    iget-object v0, p0, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    move v1, v2

    .line 246
    :goto_5
    iget-object v0, p0, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_23

    .line 255
    :cond_7
    iget-object v0, p0, Lcom/wssnps/a/ac;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8

    move v1, v2

    .line 259
    :goto_6
    iget-object v0, p0, Lcom/wssnps/a/ac;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_25

    .line 268
    :cond_8
    iget-object v0, p0, Lcom/wssnps/a/ac;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9

    move v1, v2

    .line 272
    :goto_7
    iget-object v0, p0, Lcom/wssnps/a/ac;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_27

    .line 281
    :cond_9
    iget-object v0, p0, Lcom/wssnps/a/ac;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_a

    move v1, v2

    .line 285
    :goto_8
    iget-object v0, p0, Lcom/wssnps/a/ac;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_29

    .line 294
    :cond_a
    iget-object v0, p0, Lcom/wssnps/a/ac;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    move v1, v2

    .line 298
    :goto_9
    iget-object v0, p0, Lcom/wssnps/a/ac;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2b

    .line 307
    :cond_b
    iget-object v0, p0, Lcom/wssnps/a/ac;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 308
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/wssnps/a/ac;->m:Ljava/lang/String;

    invoke-static {v0, v1, v6}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 311
    :cond_c
    iget-object v0, p0, Lcom/wssnps/a/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_d

    move v1, v2

    .line 314
    :goto_a
    iget-object v0, p0, Lcom/wssnps/a/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2d

    .line 326
    :cond_d
    iget-object v0, p0, Lcom/wssnps/a/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_e

    move v1, v2

    .line 329
    :goto_b
    iget-object v0, p0, Lcom/wssnps/a/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_30

    .line 338
    :cond_e
    iget-object v0, p0, Lcom/wssnps/a/ac;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    move v1, v2

    .line 341
    :goto_c
    iget-object v0, p0, Lcom/wssnps/a/ac;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_32

    .line 350
    :cond_f
    iget-object v0, p0, Lcom/wssnps/a/ac;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_10

    move v1, v2

    .line 353
    :goto_d
    iget-object v0, p0, Lcom/wssnps/a/ac;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_34

    .line 362
    :cond_10
    iget-object v0, p0, Lcom/wssnps/a/ac;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_11

    .line 366
    :goto_e
    iget-object v0, p0, Lcom/wssnps/a/ac;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_36

    .line 375
    :cond_11
    iget-object v0, p0, Lcom/wssnps/a/ac;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_12

    .line 376
    iget-object v0, p0, Lcom/wssnps/a/ac;->r:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/wssnps/a/ad;->a(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 379
    :cond_12
    iget v0, p0, Lcom/wssnps/a/ac;->A:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_13

    .line 380
    iget v0, p0, Lcom/wssnps/a/ac;->A:I

    invoke-static {v0}, Lcom/wssnps/a/ad;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 383
    :cond_13
    iget-object v0, p0, Lcom/wssnps/a/ac;->w:Lcom/wssnps/a/ab;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/wssnps/a/ac;->w:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/wssnps/a/ac;->w:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_14

    .line 384
    iget-object v0, p0, Lcom/wssnps/a/ac;->w:Lcom/wssnps/a/ab;

    invoke-static {v0}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/ab;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 386
    :cond_14
    iget-object v0, p0, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 388
    const-string v0, "X-ACCOUNT-N:"

    .line 389
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 390
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 392
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 395
    :cond_15
    iget-object v0, p0, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 397
    const-string v0, "X-ACCOUNT-TYPE:"

    .line 398
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 399
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 401
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 404
    :cond_16
    iget-object v0, p0, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    if-eqz v0, :cond_17

    .line 406
    iget-object v0, p0, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v0, v0, Lcom/wssnps/a/x;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 408
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v1, v1, Lcom/wssnps/a/x;->a:Ljava/lang/String;

    invoke-static {v0, v1, v6}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 410
    iget-object v0, p0, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget v0, v0, Lcom/wssnps/a/x;->b:I

    packed-switch v0, :pswitch_data_0

    .line 423
    const-string v0, "Normal"

    invoke-static {v9, v0, v6}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 427
    :goto_f
    iget-object v0, p0, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v0, v0, Lcom/wssnps/a/x;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 429
    const-string v0, "X-GDISPN :"

    .line 430
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v0, v0, Lcom/wssnps/a/x;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 431
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 433
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 438
    :cond_17
    iget-object v0, p0, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 440
    const-string v0, "X-FAVORITE:"

    .line 441
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 442
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 444
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 448
    :cond_18
    iget-object v0, p0, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    iget-object v0, v0, Lcom/wssnps/a/v;->b:Ljava/lang/String;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    iget-object v0, v0, Lcom/wssnps/a/v;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_19

    .line 449
    iget-object v0, p0, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    iget-object v0, v0, Lcom/wssnps/a/v;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/wssnps/a/ad;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 451
    :cond_19
    invoke-static {}, Lcom/wssnps/a/ad;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 452
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 173
    :cond_1a
    iget-object v0, p0, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    iget-object v0, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 174
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1b

    .line 175
    const/4 v4, 0x4

    invoke-static {v4, v0, v6}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 171
    :cond_1b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 187
    :cond_1c
    iget-object v0, p0, Lcom/wssnps/a/ac;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    iget-object v0, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 188
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1d

    .line 189
    const/16 v4, 0xe

    invoke-static {v4, v0, v6}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    :cond_1d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    .line 200
    :cond_1e
    iget-object v0, p0, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 201
    iget-object v4, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    if-eqz v4, :cond_1f

    .line 202
    invoke-static {v0, v6}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/w;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 198
    :cond_1f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    .line 215
    :cond_20
    iget-object v0, p0, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 216
    const-string v8, "-1"

    invoke-virtual {v0, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-nez v8, :cond_22

    .line 218
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move v10, v1

    move-object v1, v4

    move v4, v10

    .line 226
    :goto_10
    add-int/lit8 v8, v7, -0x1

    if-ge v3, v8, :cond_21

    .line 227
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ","

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 213
    :cond_21
    add-int/lit8 v3, v3, 0x1

    move v10, v4

    move-object v4, v1

    move v1, v10

    goto/16 :goto_4

    .line 222
    :cond_22
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 223
    const/4 v4, 0x1

    goto :goto_10

    .line 248
    :cond_23
    iget-object v0, p0, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 249
    iget-object v3, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    if-eqz v3, :cond_24

    .line 250
    invoke-static {v0, v6}, Lcom/wssnps/a/ad;->b(Lcom/wssnps/a/w;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 246
    :cond_24
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_5

    .line 261
    :cond_25
    iget-object v0, p0, Lcom/wssnps/a/ac;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 262
    iget-object v3, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    if-eqz v3, :cond_26

    .line 263
    invoke-static {v0, v6}, Lcom/wssnps/a/ad;->c(Lcom/wssnps/a/w;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 259
    :cond_26
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_6

    .line 274
    :cond_27
    iget-object v0, p0, Lcom/wssnps/a/ac;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/u;

    .line 275
    iget-boolean v3, v0, Lcom/wssnps/a/u;->i:Z

    if-eqz v3, :cond_28

    .line 276
    invoke-static {v0, v6}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/u;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 272
    :cond_28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_7

    .line 287
    :cond_29
    iget-object v0, p0, Lcom/wssnps/a/ac;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 288
    iget-object v3, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    if-eqz v3, :cond_2a

    .line 289
    iget-object v3, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iget v0, v0, Lcom/wssnps/a/w;->b:I

    invoke-static {v3, v0, v6}, Lcom/wssnps/a/ad;->a(Ljava/lang/String;ILcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 285
    :cond_2a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_8

    .line 300
    :cond_2b
    iget-object v0, p0, Lcom/wssnps/a/ac;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/z;

    .line 301
    iget-boolean v3, v0, Lcom/wssnps/a/z;->f:Z

    if-eqz v3, :cond_2c

    .line 302
    invoke-static {v0, v6}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/z;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 298
    :cond_2c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_9

    .line 316
    :cond_2d
    iget-object v0, p0, Lcom/wssnps/a/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/t;

    .line 317
    iget v3, v0, Lcom/wssnps/a/t;->a:I

    if-lez v3, :cond_2e

    .line 318
    invoke-static {v0}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/t;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 320
    :cond_2e
    iget v3, v0, Lcom/wssnps/a/t;->b:I

    if-ltz v3, :cond_2f

    .line 321
    const/16 v3, 0x25

    iget v0, v0, Lcom/wssnps/a/t;->b:I

    invoke-static {v3, v0}, Lcom/wssnps/a/ad;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 314
    :cond_2f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_a

    .line 331
    :cond_30
    iget-object v0, p0, Lcom/wssnps/a/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/t;

    .line 332
    iget v3, v0, Lcom/wssnps/a/t;->a:I

    if-lez v3, :cond_31

    .line 333
    invoke-static {v0}, Lcom/wssnps/a/ad;->b(Lcom/wssnps/a/t;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 329
    :cond_31
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_b

    .line 343
    :cond_32
    iget-object v0, p0, Lcom/wssnps/a/ac;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/t;

    .line 344
    iget v3, v0, Lcom/wssnps/a/t;->a:I

    if-lez v3, :cond_33

    .line 345
    invoke-static {v0}, Lcom/wssnps/a/ad;->c(Lcom/wssnps/a/t;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 341
    :cond_33
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_c

    .line 355
    :cond_34
    iget-object v0, p0, Lcom/wssnps/a/ac;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/t;

    .line 356
    iget v3, v0, Lcom/wssnps/a/t;->a:I

    if-lez v3, :cond_35

    .line 357
    invoke-static {v0, v6}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/t;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 353
    :cond_35
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_d

    .line 368
    :cond_36
    iget-object v0, p0, Lcom/wssnps/a/ac;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 369
    iget-object v1, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    if-eqz v1, :cond_37

    .line 370
    invoke-static {v0, v6}, Lcom/wssnps/a/ad;->d(Lcom/wssnps/a/w;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 366
    :cond_37
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_e

    .line 413
    :pswitch_0
    const-string v0, ""

    .line 414
    iget-object v0, p0, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v0, v0, Lcom/wssnps/a/x;->a:Ljava/lang/String;

    const-string v1, "Contacts"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 415
    const-string v0, "System;DEFAULT"

    .line 419
    :goto_11
    invoke-static {v9, v0, v6}, Lcom/wssnps/a/ad;->a(ILjava/lang/String;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_f

    .line 417
    :cond_38
    const-string v0, "System"

    goto :goto_11

    .line 410
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 457
    .line 464
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    const/4 v0, 0x0

    .line 629
    :goto_0
    return-object v0

    .line 467
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 469
    invoke-static {v2}, Lcom/wssnps/a/ad;->a(I)Lcom/wssnps/a/ag;

    move-result-object v5

    .line 472
    const-string v0, "BEGIN:VCARD"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    move v3, v2

    .line 473
    :goto_1
    array-length v0, v6

    if-lt v3, v0, :cond_1

    move-object v0, v4

    .line 629
    goto :goto_0

    .line 475
    :cond_1
    aget-object v0, v6, v3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 477
    new-instance v7, Lcom/wssnps/a/ac;

    invoke-direct {v7}, Lcom/wssnps/a/ac;-><init>()V

    .line 478
    aget-object v0, v6, v3

    invoke-static {v0}, Lcom/wssnps/a/s;->c(Ljava/lang/String;)Lcom/wssnps/a/i;

    move-result-object v8

    move v1, v2

    .line 480
    :goto_2
    iget-object v0, v8, Lcom/wssnps/a/i;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_3

    .line 624
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 482
    :cond_3
    iget-object v0, v8, Lcom/wssnps/a/i;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/f;

    .line 484
    iget v9, v0, Lcom/wssnps/a/f;->a:I

    packed-switch v9, :pswitch_data_0

    .line 621
    :cond_4
    :goto_3
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 494
    :pswitch_1
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->b:Ljava/lang/String;

    goto :goto_3

    .line 498
    :pswitch_2
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->b(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/y;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    goto :goto_3

    .line 502
    :pswitch_3
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->c(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 503
    if-eqz v0, :cond_4

    .line 504
    iget-object v9, v7, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 508
    :pswitch_4
    invoke-static {v0}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/f;)Lcom/wssnps/a/t;

    move-result-object v0

    .line 509
    if-eqz v0, :cond_4

    .line 510
    iget-object v9, v7, Lcom/wssnps/a/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 514
    :pswitch_5
    invoke-static {v0}, Lcom/wssnps/a/ad;->b(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v7, Lcom/wssnps/a/ac;->B:I

    goto :goto_3

    .line 518
    :pswitch_6
    invoke-static {v0}, Lcom/wssnps/a/ad;->c(Lcom/wssnps/a/f;)Lcom/wssnps/a/t;

    move-result-object v0

    .line 519
    if-eqz v0, :cond_4

    .line 520
    iget-object v9, v7, Lcom/wssnps/a/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 524
    :pswitch_7
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->d(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/t;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_4

    .line 527
    iget-object v9, v0, Lcom/wssnps/a/t;->g:Ljava/lang/String;

    if-nez v9, :cond_5

    .line 528
    iget-object v9, v7, Lcom/wssnps/a/ac;->g:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 530
    :cond_5
    iget-object v9, v7, Lcom/wssnps/a/ac;->h:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 535
    :pswitch_8
    invoke-static {v0}, Lcom/wssnps/a/ad;->d(Lcom/wssnps/a/f;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 536
    if-eqz v0, :cond_4

    .line 537
    iget-object v9, v7, Lcom/wssnps/a/ac;->i:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 541
    :pswitch_9
    invoke-static {v0}, Lcom/wssnps/a/ad;->f(Lcom/wssnps/a/f;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 542
    if-eqz v0, :cond_4

    .line 543
    iget-object v9, v7, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 547
    :pswitch_a
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->e(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_4

    .line 549
    iget-object v9, v7, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 554
    :pswitch_b
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->i(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->m:Ljava/lang/String;

    goto :goto_3

    .line 558
    :pswitch_c
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->j(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->n:Ljava/lang/String;

    goto/16 :goto_3

    .line 563
    :pswitch_d
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->h(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/u;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_4

    .line 565
    iget-object v9, v7, Lcom/wssnps/a/ac;->p:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 569
    :pswitch_e
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->k(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/z;

    move-result-object v0

    .line 570
    if-eqz v0, :cond_4

    .line 571
    iget-object v9, v7, Lcom/wssnps/a/ac;->q:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 576
    :pswitch_f
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->l(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_4

    .line 578
    iget-object v9, v7, Lcom/wssnps/a/ac;->o:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 582
    :pswitch_10
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->f(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 583
    if-eqz v0, :cond_4

    .line 584
    iget-object v9, v7, Lcom/wssnps/a/ac;->l:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 588
    :pswitch_11
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->o(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->r:Ljava/util/ArrayList;

    goto/16 :goto_3

    .line 592
    :pswitch_12
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->m(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/v;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    goto/16 :goto_3

    .line 597
    :pswitch_13
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->n(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    goto/16 :goto_3

    .line 601
    :pswitch_14
    invoke-static {v0}, Lcom/wssnps/a/ad;->e(Lcom/wssnps/a/f;)Lcom/wssnps/a/aa;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->u:Lcom/wssnps/a/aa;

    goto/16 :goto_3

    .line 605
    :pswitch_15
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->q(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/ab;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    goto/16 :goto_3

    .line 609
    :pswitch_16
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->g(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 610
    if-eqz v0, :cond_4

    .line 611
    iget-object v9, v7, Lcom/wssnps/a/ac;->x:Ljava/util/ArrayList;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 615
    :pswitch_17
    invoke-static {v0, v5}, Lcom/wssnps/a/ad;->r(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    goto/16 :goto_3

    .line 484
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_d
        :pswitch_4
        :pswitch_12
        :pswitch_10
        :pswitch_f
        :pswitch_14
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_5
        :pswitch_17
    .end packed-switch
.end method

.method public static b(Ljava/lang/String;)Lcom/wssnps/a/ac;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 634
    .line 641
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 642
    const/4 v0, 0x0

    .line 794
    :goto_0
    return-object v0

    .line 644
    :cond_0
    new-instance v2, Lcom/wssnps/a/ac;

    invoke-direct {v2}, Lcom/wssnps/a/ac;-><init>()V

    .line 645
    invoke-static {v0}, Lcom/wssnps/a/ad;->a(I)Lcom/wssnps/a/ag;

    move-result-object v3

    .line 646
    invoke-static {p0}, Lcom/wssnps/a/s;->c(Ljava/lang/String;)Lcom/wssnps/a/i;

    move-result-object v4

    move v1, v0

    .line 647
    :goto_1
    iget-object v0, v4, Lcom/wssnps/a/i;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    move-object v0, v2

    .line 794
    goto :goto_0

    .line 649
    :cond_1
    iget-object v0, v4, Lcom/wssnps/a/i;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/f;

    .line 651
    iget v5, v0, Lcom/wssnps/a/f;->a:I

    packed-switch v5, :pswitch_data_0

    .line 791
    :cond_2
    :goto_2
    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 661
    :pswitch_1
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->b:Ljava/lang/String;

    goto :goto_2

    .line 665
    :pswitch_2
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->b(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/y;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    goto :goto_2

    .line 669
    :pswitch_3
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->c(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 670
    if-eqz v0, :cond_2

    .line 671
    iget-object v5, v2, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 675
    :pswitch_4
    invoke-static {v0}, Lcom/wssnps/a/ad;->a(Lcom/wssnps/a/f;)Lcom/wssnps/a/t;

    move-result-object v0

    .line 676
    if-eqz v0, :cond_2

    .line 677
    iget-object v5, v2, Lcom/wssnps/a/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 681
    :pswitch_5
    invoke-static {v0}, Lcom/wssnps/a/ad;->b(Lcom/wssnps/a/f;)I

    move-result v0

    iput v0, v2, Lcom/wssnps/a/ac;->B:I

    goto :goto_2

    .line 685
    :pswitch_6
    invoke-static {v0}, Lcom/wssnps/a/ad;->c(Lcom/wssnps/a/f;)Lcom/wssnps/a/t;

    move-result-object v0

    .line 686
    if-eqz v0, :cond_2

    .line 687
    iget-object v5, v2, Lcom/wssnps/a/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 691
    :pswitch_7
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->d(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/t;

    move-result-object v0

    .line 692
    if-eqz v0, :cond_2

    .line 694
    iget-object v5, v0, Lcom/wssnps/a/t;->g:Ljava/lang/String;

    if-nez v5, :cond_3

    .line 695
    iget-object v5, v2, Lcom/wssnps/a/ac;->g:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 697
    :cond_3
    iget-object v5, v2, Lcom/wssnps/a/ac;->h:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 702
    :pswitch_8
    invoke-static {v0}, Lcom/wssnps/a/ad;->d(Lcom/wssnps/a/f;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 703
    if-eqz v0, :cond_2

    .line 704
    iget-object v5, v2, Lcom/wssnps/a/ac;->i:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 708
    :pswitch_9
    invoke-static {v0}, Lcom/wssnps/a/ad;->f(Lcom/wssnps/a/f;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 709
    if-eqz v0, :cond_2

    .line 710
    iget-object v5, v2, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 714
    :pswitch_a
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->e(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 715
    if-eqz v0, :cond_2

    .line 716
    iget-object v5, v2, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 721
    :pswitch_b
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->i(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->m:Ljava/lang/String;

    goto :goto_2

    .line 725
    :pswitch_c
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->j(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->n:Ljava/lang/String;

    goto/16 :goto_2

    .line 730
    :pswitch_d
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->h(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/u;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_2

    .line 732
    iget-object v5, v2, Lcom/wssnps/a/ac;->p:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 736
    :pswitch_e
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->k(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/z;

    move-result-object v0

    .line 737
    if-eqz v0, :cond_2

    .line 738
    iget-object v5, v2, Lcom/wssnps/a/ac;->q:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 743
    :pswitch_f
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->l(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 744
    if-eqz v0, :cond_2

    .line 745
    iget-object v5, v2, Lcom/wssnps/a/ac;->o:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 749
    :pswitch_10
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->f(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 750
    if-eqz v0, :cond_2

    .line 751
    iget-object v5, v2, Lcom/wssnps/a/ac;->l:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 755
    :pswitch_11
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->o(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->r:Ljava/util/ArrayList;

    goto/16 :goto_2

    .line 759
    :pswitch_12
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->m(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/v;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    goto/16 :goto_2

    .line 763
    :pswitch_13
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->n(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    goto/16 :goto_2

    .line 767
    :pswitch_14
    invoke-static {v0}, Lcom/wssnps/a/ad;->e(Lcom/wssnps/a/f;)Lcom/wssnps/a/aa;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->u:Lcom/wssnps/a/aa;

    goto/16 :goto_2

    .line 771
    :pswitch_15
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->q(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/ab;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    goto/16 :goto_2

    .line 775
    :pswitch_16
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->g(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/w;

    move-result-object v0

    .line 776
    if-eqz v0, :cond_2

    .line 777
    iget-object v5, v2, Lcom/wssnps/a/ac;->x:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 781
    :pswitch_17
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->p(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Lcom/wssnps/a/x;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    goto/16 :goto_2

    .line 785
    :pswitch_18
    invoke-static {v0, v3}, Lcom/wssnps/a/ad;->r(Lcom/wssnps/a/f;Lcom/wssnps/a/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    goto/16 :goto_2

    .line 651
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_e
        :pswitch_d
        :pswitch_4
        :pswitch_12
        :pswitch_10
        :pswitch_f
        :pswitch_14
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_16
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_5
        :pswitch_18
    .end packed-switch
.end method

.method public static c(Ljava/lang/String;)Lcom/wssnps/a/i;
    .locals 2

    .prologue
    .line 799
    new-instance v0, Lcom/wssnps/a/i;

    invoke-direct {v0}, Lcom/wssnps/a/i;-><init>()V

    .line 800
    sget-object v1, Lcom/wssnps/a/g;->a:Lcom/wssnps/a/h;

    invoke-virtual {v0, p0, v1}, Lcom/wssnps/a/i;->a(Ljava/lang/String;Lcom/wssnps/a/h;)Z

    .line 801
    return-object v0
.end method
