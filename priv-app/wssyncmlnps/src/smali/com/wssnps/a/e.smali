.class public Lcom/wssnps/a/e;
.super Ljava/lang/Object;
.source "smlQP.java"


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/wssnps/a/e;->a:[B

    .line 53
    const-string v0, "=\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/wssnps/a/e;->b:[B

    .line 55
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/wssnps/a/e;->c:[B

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
    .end array-data
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)J
    .locals 11

    .prologue
    .line 72
    const/4 v4, 0x0

    .line 73
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 74
    const/16 v5, 0x50

    new-array v7, v5, [B

    .line 78
    :goto_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 80
    const/4 v6, -0x1

    if-ne v5, v6, :cond_1

    .line 82
    if-lez v3, :cond_0

    .line 84
    const/4 v0, 0x0

    invoke-virtual {p1, v7, v0, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 89
    :cond_0
    int-to-long v0, v2

    return-wide v0

    .line 92
    :cond_1
    const/16 v6, 0x4a

    if-le v3, v6, :cond_2

    .line 94
    const/4 v4, 0x0

    invoke-virtual {p1, v7, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 97
    const/4 v3, 0x0

    .line 98
    const/4 v4, 0x0

    .line 100
    :cond_2
    int-to-byte v5, v5

    .line 102
    if-nez v5, :cond_3

    .line 104
    add-int/lit8 v0, v0, 0x1

    .line 106
    const/4 v1, 0x0

    move v4, v5

    .line 107
    goto :goto_0

    .line 109
    :cond_3
    if-lez v0, :cond_12

    .line 112
    const/4 v4, 0x1

    move v6, v4

    move v4, v3

    move v3, v2

    :goto_1
    if-le v6, v0, :cond_4

    .line 131
    const/4 v2, 0x0

    .line 132
    const/4 v0, 0x0

    move v10, v3

    move v3, v2

    move v2, v10

    .line 135
    :goto_2
    const/16 v6, 0x20

    if-le v5, v6, :cond_6

    const/16 v6, 0x7f

    if-ge v5, v6, :cond_6

    const/16 v6, 0x3d

    if-eq v5, v6, :cond_6

    const/16 v6, 0x28

    if-eq v5, v6, :cond_6

    const/16 v6, 0x29

    if-eq v5, v6, :cond_6

    const/16 v6, 0x3b

    if-eq v5, v6, :cond_6

    const/16 v6, 0x3a

    if-eq v5, v6, :cond_6

    .line 139
    add-int/lit8 v3, v4, 0x1

    aput-byte v5, v7, v4

    .line 141
    add-int/lit8 v2, v2, 0x1

    .line 142
    const/4 v1, 0x0

    move v4, v5

    .line 144
    goto :goto_0

    .line 115
    :cond_4
    add-int/lit8 v2, v4, 0x1

    const/16 v8, 0x3d

    aput-byte v8, v7, v4

    .line 116
    add-int/lit8 v4, v2, 0x1

    sget-object v8, Lcom/wssnps/a/e;->c:[B

    const/4 v9, 0x0

    aget-byte v8, v8, v9

    aput-byte v8, v7, v2

    .line 117
    add-int/lit8 v2, v4, 0x1

    sget-object v8, Lcom/wssnps/a/e;->c:[B

    const/4 v9, 0x0

    aget-byte v8, v8, v9

    aput-byte v8, v7, v4

    .line 120
    add-int/lit8 v4, v3, 0x3

    .line 122
    const/16 v3, 0x4a

    if-le v2, v3, :cond_5

    .line 124
    const/4 v3, 0x0

    invoke-virtual {p1, v7, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 127
    const/4 v2, 0x0

    .line 112
    :cond_5
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v4

    move v4, v2

    goto :goto_1

    .line 145
    :cond_6
    const/16 v6, 0x20

    if-eq v5, v6, :cond_7

    const/16 v6, 0x9

    if-ne v5, v6, :cond_8

    .line 148
    :cond_7
    add-int/lit8 v3, v4, 0x1

    aput-byte v5, v7, v4

    .line 149
    add-int/lit8 v2, v2, 0x1

    .line 150
    const/4 v1, 0x1

    move v4, v5

    .line 152
    goto/16 :goto_0

    .line 153
    :cond_8
    const/16 v6, 0xa

    if-ne v5, v6, :cond_9

    const/16 v6, 0xd

    if-ne v3, v6, :cond_9

    .line 156
    const/4 v3, 0x0

    move v10, v4

    move v4, v3

    move v3, v10

    .line 157
    goto/16 :goto_0

    .line 158
    :cond_9
    const/16 v6, 0xa

    if-ne v5, v6, :cond_c

    .line 160
    const/4 v5, 0x1

    if-eq v1, v5, :cond_a

    const/16 v1, 0x2e

    if-ne v3, v1, :cond_b

    const/4 v1, 0x1

    if-ne v4, v1, :cond_b

    .line 162
    :cond_a
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    .line 163
    add-int/lit8 v3, v1, 0x1

    const/16 v4, 0xd

    aput-byte v4, v7, v1

    .line 164
    add-int/lit8 v4, v3, 0x1

    const/16 v1, 0xa

    aput-byte v1, v7, v3

    .line 165
    add-int/lit8 v2, v2, 0x3

    .line 168
    :cond_b
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    .line 169
    add-int/lit8 v3, v1, 0x1

    const/16 v4, 0x30

    aput-byte v4, v7, v1

    .line 170
    add-int/lit8 v4, v3, 0x1

    const/16 v1, 0x41

    aput-byte v1, v7, v3

    .line 172
    const/4 v1, 0x0

    .line 173
    add-int/lit8 v2, v2, 0x3

    .line 175
    const/4 v3, 0x0

    invoke-virtual {p1, v7, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 176
    const/4 v4, 0x0

    .line 177
    const/4 v3, 0x0

    .line 178
    goto/16 :goto_0

    .line 179
    :cond_c
    const/16 v6, 0xd

    if-ne v5, v6, :cond_f

    .line 183
    const/4 v5, 0x1

    if-eq v1, v5, :cond_d

    const/16 v1, 0x2e

    if-ne v3, v1, :cond_e

    const/4 v1, 0x1

    if-ne v4, v1, :cond_e

    .line 185
    :cond_d
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    .line 186
    add-int/lit8 v3, v1, 0x1

    const/16 v4, 0xd

    aput-byte v4, v7, v1

    .line 187
    add-int/lit8 v4, v3, 0x1

    const/16 v1, 0xa

    aput-byte v1, v7, v3

    .line 188
    add-int/lit8 v2, v2, 0x3

    .line 194
    :cond_e
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    .line 195
    add-int/lit8 v3, v1, 0x1

    const/16 v4, 0x30

    aput-byte v4, v7, v1

    .line 196
    add-int/lit8 v4, v3, 0x1

    const/16 v1, 0x44

    aput-byte v1, v7, v3

    .line 198
    const/4 v1, 0x0

    .line 200
    add-int/lit8 v2, v2, 0x3

    .line 201
    const/4 v3, 0x0

    invoke-virtual {p1, v7, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 202
    const/4 v4, 0x0

    .line 203
    const/4 v3, 0x0

    .line 206
    goto/16 :goto_0

    .line 207
    :cond_f
    const/16 v1, 0x20

    if-lt v5, v1, :cond_10

    const/16 v1, 0x3d

    if-eq v5, v1, :cond_10

    const/16 v1, 0x7f

    if-ge v5, v1, :cond_10

    const/16 v1, 0x28

    if-eq v5, v1, :cond_10

    const/16 v1, 0x29

    if-eq v5, v1, :cond_10

    const/16 v1, 0x3b

    if-eq v5, v1, :cond_10

    const/16 v1, 0x3a

    if-ne v5, v1, :cond_11

    .line 213
    :cond_10
    add-int/lit8 v1, v4, 0x1

    const/16 v3, 0x3d

    aput-byte v3, v7, v4

    .line 215
    add-int/lit8 v4, v1, 0x1

    sget-object v3, Lcom/wssnps/a/e;->c:[B

    ushr-int/lit8 v6, v5, 0x4

    and-int/lit8 v6, v6, 0xf

    aget-byte v3, v3, v6

    aput-byte v3, v7, v1

    .line 216
    add-int/lit8 v3, v4, 0x1

    sget-object v1, Lcom/wssnps/a/e;->c:[B

    and-int/lit8 v6, v5, 0xf

    aget-byte v1, v1, v6

    aput-byte v1, v7, v4

    .line 217
    const/4 v1, 0x0

    .line 219
    add-int/lit8 v2, v2, 0x3

    move v4, v5

    .line 221
    goto/16 :goto_0

    .line 225
    :cond_11
    add-int/lit8 v3, v4, 0x1

    aput-byte v5, v7, v4

    .line 226
    const/4 v1, 0x0

    .line 228
    add-int/lit8 v2, v2, 0x1

    move v4, v5

    .line 76
    goto/16 :goto_0

    :cond_12
    move v10, v3

    move v3, v4

    move v4, v10

    goto/16 :goto_2
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 237
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 238
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 241
    :try_start_0
    invoke-static {v0, v1}, Lcom/wssnps/a/e;->a(Ljava/io/InputStream;Ljava/io/ByteArrayOutputStream;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :goto_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    .line 248
    return-object v0

    .line 243
    :catch_0
    move-exception v0

    .line 245
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected static a([B)[B
    .locals 1

    .prologue
    .line 318
    array-length v0, p0

    invoke-static {p0, v0}, Lcom/wssnps/a/e;->a([BI)[B

    move-result-object v0

    return-object v0
.end method

.method protected static a([BI)[B
    .locals 11

    .prologue
    const/16 v10, 0x10

    const/16 v9, 0xd

    const/16 v8, 0xa

    const/4 v3, 0x0

    .line 351
    if-nez p0, :cond_0

    .line 352
    new-instance v0, Lcom/wssnps/a/d;

    const-string v1, "Error: Bad parameter"

    invoke-direct {v0, v1}, Lcom/wssnps/a/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_0
    add-int/lit8 v0, p1, 0x1

    new-array v5, v0, [B

    .line 356
    const-string v0, "\r\n"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    move v4, v3

    move v2, v3

    move v1, v3

    .line 360
    :goto_0
    if-lt v4, p1, :cond_1

    .line 414
    new-array v0, v2, [B

    .line 416
    :goto_1
    if-lt v3, v2, :cond_a

    .line 420
    return-object v0

    .line 362
    :cond_1
    add-int/lit8 v0, v4, 0x1

    aget-byte v7, p0, v4

    .line 363
    const/16 v4, 0x3d

    if-ne v7, v4, :cond_5

    .line 365
    aget-byte v1, p0, v0

    if-eq v1, v8, :cond_2

    aget-byte v1, p0, v0

    if-ne v1, v9, :cond_3

    .line 367
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 369
    add-int/lit8 v1, v0, -0x1

    aget-byte v1, p0, v1

    if-ne v1, v9, :cond_c

    aget-byte v1, p0, v0

    if-ne v1, v8, :cond_c

    .line 370
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    :goto_2
    move v4, v0

    move v2, v1

    .line 393
    goto :goto_0

    .line 376
    :cond_3
    aget-byte v1, p0, v0

    int-to-char v1, v1

    invoke-static {v1, v10}, Ljava/lang/Character;->digit(CI)I

    move-result v1

    .line 377
    add-int/lit8 v4, v0, 0x1

    aget-byte v4, p0, v4

    int-to-char v4, v4

    invoke-static {v4, v10}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    .line 379
    or-int v7, v1, v4

    if-gez v7, :cond_4

    .line 381
    new-instance v1, Lcom/wssnps/a/d;

    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/String;

    add-int/lit8 v0, v0, -0x1

    const/4 v4, 0x3

    invoke-direct {v3, p0, v0, v4}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "is an invalid code"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/wssnps/a/d;-><init>(Ljava/lang/String;)V

    throw v1

    .line 385
    :cond_4
    shl-int/lit8 v1, v1, 0x4

    or-int/2addr v1, v4

    int-to-byte v4, v1

    .line 386
    add-int/lit8 v0, v0, 0x2

    .line 389
    add-int/lit8 v1, v2, 0x1

    aput-byte v4, v5, v2

    goto :goto_2

    .line 394
    :cond_5
    if-eq v7, v8, :cond_6

    if-ne v7, v9, :cond_9

    .line 396
    :cond_6
    add-int/lit8 v2, v0, -0x1

    aget-byte v2, p0, v2

    if-ne v2, v9, :cond_7

    aget-byte v2, p0, v0

    if-ne v2, v8, :cond_7

    .line 397
    add-int/lit8 v0, v0, 0x1

    :cond_7
    move v2, v1

    move v1, v3

    .line 399
    :goto_3
    array-length v4, v6

    if-lt v1, v4, :cond_8

    move v4, v0

    move v1, v2

    .line 403
    goto :goto_0

    .line 400
    :cond_8
    add-int/lit8 v4, v2, 0x1

    aget-byte v7, v6, v1

    aput-byte v7, v5, v2

    .line 399
    add-int/lit8 v1, v1, 0x1

    move v2, v4

    goto :goto_3

    .line 407
    :cond_9
    add-int/lit8 v4, v2, 0x1

    aput-byte v7, v5, v2

    .line 409
    const/16 v2, 0x20

    if-eq v7, v2, :cond_b

    const/16 v2, 0x9

    if-eq v7, v2, :cond_b

    move v2, v4

    move v1, v4

    move v4, v0

    .line 410
    goto/16 :goto_0

    .line 418
    :cond_a
    aget-byte v1, v5, v3

    aput-byte v1, v0, v3

    .line 416
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_b
    move v2, v4

    move v4, v0

    goto/16 :goto_0

    :cond_c
    move v1, v2

    goto/16 :goto_2
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 294
    const-string v0, ""

    .line 295
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 306
    :goto_0
    return-object v0

    .line 300
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/wssnps/a/e;->a([B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 302
    :catch_0
    move-exception v0

    .line 304
    new-instance v1, Lcom/wssnps/a/d;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/wssnps/a/d;-><init>(Ljava/lang/String;)V

    throw v1
.end method
