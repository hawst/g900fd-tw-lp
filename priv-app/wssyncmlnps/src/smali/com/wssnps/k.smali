.class public Lcom/wssnps/k;
.super Ljava/lang/Object;
.source "smlNpsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 186
    sput-object v1, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    .line 190
    const/4 v0, 0x2

    :try_start_0
    const-string v2, "NpsServiceTask Start"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 193
    new-instance v0, Landroid/net/LocalServerSocket;

    const-string v2, "/data/.wssnps_stream"

    invoke-direct {v0, v2}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    move-object v2, v1

    .line 194
    :goto_0
    invoke-static {}, Lcom/wssnps/smlNpsService;->d()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    :try_start_1
    sget-object v0, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    if-nez v0, :cond_1

    .line 200
    const/4 v0, 0x2

    const-string v3, "dsServerSocket is NULL!!!!!"

    invoke-static {v0, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 231
    if-eqz v2, :cond_0

    .line 233
    :try_start_2
    invoke-virtual {v2}, Landroid/net/LocalSocket;->close()V

    .line 238
    :cond_0
    const/4 v0, 0x2

    const-string v1, "dsServerSocket m_NpsServiceState OUT!!!!! "

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 246
    invoke-static {v6}, Lcom/wssnps/smlNpsService;->a(Z)Z

    .line 248
    :goto_1
    return-void

    .line 205
    :cond_1
    :try_start_3
    sget-object v0, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v0

    .line 207
    const/4 v2, 0x2

    :try_start_4
    const-string v3, "dsServerSocket accept"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 210
    if-eqz v0, :cond_2

    .line 212
    const/4 v2, 0x2

    const-string v3, "localSocket connect"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 213
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/wssnps/b;

    invoke-direct {v3, v0}, Lcom/wssnps/b;-><init>(Landroid/net/LocalSocket;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v2, Lcom/wssnps/smlNpsService;->c:Ljava/lang/Thread;

    .line 214
    sget-object v2, Lcom/wssnps/smlNpsService;->c:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 217
    :cond_2
    sget-object v2, Lcom/wssnps/smlNpsService;->c:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V

    .line 218
    const/4 v2, 0x2

    const-string v3, "runServer m_thread1.join() End !!!!!"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 231
    if-eqz v0, :cond_3

    .line 233
    :try_start_5
    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v0, v1

    :cond_3
    :goto_2
    move-object v2, v0

    .line 234
    goto :goto_0

    .line 221
    :catch_0
    move-exception v2

    .line 223
    :goto_3
    const/4 v3, 0x3

    :try_start_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 231
    if-eqz v0, :cond_3

    .line 233
    :try_start_7
    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v0, v1

    .line 234
    goto :goto_2

    .line 225
    :catch_1
    move-exception v2

    .line 227
    :goto_4
    const/4 v3, 0x3

    :try_start_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 231
    if-eqz v0, :cond_3

    .line 233
    :try_start_9
    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V

    move-object v0, v1

    .line 234
    goto :goto_2

    .line 231
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_5
    if-eqz v2, :cond_4

    .line 233
    invoke-virtual {v2}, Landroid/net/LocalSocket;->close()V

    .line 234
    :cond_4
    throw v0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 240
    :catch_2
    move-exception v0

    .line 242
    :try_start_a
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 246
    invoke-static {v6}, Lcom/wssnps/smlNpsService;->a(Z)Z

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    invoke-static {v6}, Lcom/wssnps/smlNpsService;->a(Z)Z

    throw v0

    .line 231
    :catchall_2
    move-exception v0

    goto :goto_5

    .line 225
    :catch_3
    move-exception v0

    move-object v7, v0

    move-object v0, v2

    move-object v2, v7

    goto :goto_4

    .line 221
    :catch_4
    move-exception v0

    move-object v7, v0

    move-object v0, v2

    move-object v2, v7

    goto :goto_3
.end method
