.class public Lcom/wssnps/b/a;
.super Ljava/lang/Object;
.source "smlAcccountItem.java"

# interfaces
.implements Lcom/wssnps/a/b;


# static fields
.field private static a:I

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13
    sput v0, Lcom/wssnps/b/a;->a:I

    .line 14
    sput v0, Lcom/wssnps/b/a;->b:I

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 23
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 27
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v2, :cond_0

    move-object v0, v1

    .line 57
    :goto_0
    return-object v0

    .line 30
    :cond_0
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 31
    goto :goto_0

    .line 33
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 34
    array-length v1, v2

    .line 36
    const-string v3, "CHM"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 38
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_5

    .line 40
    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "vnd.sec.contact.phone"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "com.android.exchange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    .line 42
    add-int/lit8 v1, v1, -0x1

    .line 38
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 47
    :cond_3
    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_5

    .line 49
    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "vnd.sec.contact.phone"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_4

    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_4

    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "com.android.exchange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_4

    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v4, "com.osp.app.signin"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_4

    .line 51
    add-int/lit8 v1, v1, -0x1

    .line 47
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 55
    :cond_5
    sput v1, Lcom/wssnps/b/a;->b:I

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-object v0

    .line 70
    :cond_1
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 73
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 75
    new-instance v2, Lcom/wssnps/b/y;

    invoke-direct {v2}, Lcom/wssnps/b/y;-><init>()V

    .line 76
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 77
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 79
    add-int/lit8 v3, v3, -0x1

    .line 81
    if-nez v3, :cond_3

    .line 83
    iget-object v0, v2, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    const-string v1, "vnd.sec.contact.phone"

    iput-object v1, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    .line 84
    iget-object v0, v2, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    const-string v1, "vnd.sec.contact.phone"

    iput-object v1, v0, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    .line 108
    :goto_1
    sget v0, Lcom/wssnps/b/a;->b:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/wssnps/b/a;->b:I

    .line 109
    sget v0, Lcom/wssnps/b/a;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 110
    sput v5, Lcom/wssnps/b/a;->a:I

    .line 111
    :cond_2
    invoke-virtual {v2}, Lcom/wssnps/b/y;->toString()Ljava/lang/String;

    move-result-object v0

    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_3
    if-eqz v1, :cond_0

    array-length v4, v1

    if-lez v4, :cond_0

    .line 91
    add-int/lit8 v0, v3, -0x1

    .line 92
    const-string v0, "CHM"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95
    :goto_2
    sget v0, Lcom/wssnps/b/a;->a:I

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v3, "vnd.sec.contact.phone"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/wssnps/b/a;->a:I

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/wssnps/b/a;->a:I

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v3, "com.android.exchange"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    .line 96
    sget v0, Lcom/wssnps/b/a;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/wssnps/b/a;->a:I

    goto :goto_2

    .line 101
    :cond_4
    :goto_3
    sget v0, Lcom/wssnps/b/a;->a:I

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v3, "vnd.sec.contact.phone"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/wssnps/b/a;->a:I

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/wssnps/b/a;->a:I

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v3, "com.android.exchange"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/wssnps/b/a;->a:I

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v3, "com.osp.app.signin"

    invoke-virtual {v0, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    .line 102
    sget v0, Lcom/wssnps/b/a;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/wssnps/b/a;->a:I

    goto :goto_3

    .line 104
    :cond_5
    iget-object v0, v2, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    sget v3, Lcom/wssnps/b/a;->a:I

    aget-object v3, v1, v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v3, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    .line 105
    iget-object v0, v2, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    sget v3, Lcom/wssnps/b/a;->a:I

    add-int/lit8 v4, v3, 0x1

    sput v4, Lcom/wssnps/b/a;->a:I

    aget-object v1, v1, v3

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    goto/16 :goto_1
.end method
