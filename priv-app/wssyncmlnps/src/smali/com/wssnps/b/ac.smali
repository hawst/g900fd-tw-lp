.class public Lcom/wssnps/b/ac;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "smlDBHelper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 13
    const-string v0, "wssnpsdb.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 14
    const/4 v0, 0x2

    const-string v1, "smlDBHelper"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 37
    const/4 v0, 0x2

    const-string v1, "smlDBHelper smlDBCreateAllTable"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 38
    invoke-static {p1}, Lcom/wssnps/b/k;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    return-void
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 20
    const/4 v0, 0x2

    const-string v1, "smlDBHelper onCreate"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 21
    invoke-virtual {p0, p1}, Lcom/wssnps/b/ac;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 22
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "smlDBHelper onUpgrade oldVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "smlDBHelper onUpgrade newVersion : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 29
    if-ne p2, v2, :cond_0

    const/4 v0, 0x3

    if-ne p3, v0, :cond_0

    .line 31
    invoke-virtual {p0, p1}, Lcom/wssnps/b/ac;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 33
    :cond_0
    return-void
.end method
