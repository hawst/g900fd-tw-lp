.class public Lcom/wssnps/b/ag;
.super Ljava/lang/Object;
.source "smlTaskItem.java"

# interfaces
.implements Lcom/wssnps/a/b;


# static fields
.field public static a:Lcom/wssnps/smlModelDefine;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;


# instance fields
.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:J

.field public j:I

.field public k:I

.field public l:J

.field public m:J

.field public n:I

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Ljava/util/ArrayList;

.field public t:Ljava/util/ArrayList;

.field public u:Ljava/util/ArrayList;

.field public v:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/wssnps/smlModelDefine;

    invoke-direct {v0}, Lcom/wssnps/smlModelDefine;-><init>()V

    sput-object v0, Lcom/wssnps/b/ag;->a:Lcom/wssnps/smlModelDefine;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/wssnps/a/r;)V
    .locals 10

    .prologue
    const/4 v7, 0x3

    const-wide/16 v8, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    .line 104
    if-nez p1, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p1, Lcom/wssnps/a/r;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    .line 108
    iget-object v0, p1, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/ag;->e:Ljava/lang/String;

    .line 110
    iget-object v0, p1, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 112
    iget-object v0, p1, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    .line 113
    iget-object v0, p1, Lcom/wssnps/a/r;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    .line 114
    iget-object v0, p1, Lcom/wssnps/a/r;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/ag;->q:Ljava/lang/String;

    .line 117
    :cond_2
    iget v0, p1, Lcom/wssnps/a/r;->n:I

    const/16 v1, 0x16

    if-ne v0, v1, :cond_a

    .line 118
    iput v5, p0, Lcom/wssnps/b/ag;->j:I

    .line 123
    :goto_1
    iget v0, p1, Lcom/wssnps/a/r;->k:I

    if-ne v0, v5, :cond_b

    .line 124
    iput v6, p0, Lcom/wssnps/b/ag;->k:I

    .line 131
    :cond_3
    :goto_2
    iget-object v0, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-eqz v0, :cond_4

    .line 133
    iget-object v0, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/wssnps/b/ag;->m:J

    .line 134
    iget-wide v0, p0, Lcom/wssnps/b/ag;->m:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/32 v2, 0x7e076290

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    .line 135
    iput-wide v8, p0, Lcom/wssnps/b/ag;->m:J

    .line 139
    :cond_4
    iget-object v0, p1, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    if-eqz v0, :cond_5

    .line 141
    iget-object v0, p1, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/wssnps/b/ag;->l:J

    .line 142
    iget-wide v0, p0, Lcom/wssnps/b/ag;->l:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/32 v2, 0x7e076290

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 143
    iput-wide v8, p0, Lcom/wssnps/b/ag;->l:J

    .line 147
    :cond_5
    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 149
    iput v5, p0, Lcom/wssnps/b/ag;->h:I

    .line 150
    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget-object v0, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/wssnps/b/ag;->i:J

    .line 152
    iget-object v0, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-eqz v0, :cond_d

    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget-object v0, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget-object v1, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-ne v0, v1, :cond_d

    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget-object v0, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->month:I

    iget-object v1, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    if-ne v0, v1, :cond_d

    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget-object v0, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    iget-object v1, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    if-ne v0, v1, :cond_d

    .line 156
    iput v5, p0, Lcom/wssnps/b/ag;->g:I

    .line 176
    :goto_3
    iget-object v0, p1, Lcom/wssnps/a/r;->F:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 177
    iget-object v0, p1, Lcom/wssnps/a/r;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    .line 179
    :cond_6
    iget-object v0, p1, Lcom/wssnps/a/r;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 180
    iget-object v0, p1, Lcom/wssnps/a/r;->J:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    .line 182
    :cond_7
    iget-object v0, p1, Lcom/wssnps/a/r;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 183
    iget-object v0, p1, Lcom/wssnps/a/r;->K:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    .line 185
    :cond_8
    iget-object v0, p1, Lcom/wssnps/a/r;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 186
    iget-object v0, p1, Lcom/wssnps/a/r;->L:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    .line 188
    :cond_9
    iget-object v0, p1, Lcom/wssnps/a/r;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p1, Lcom/wssnps/a/r;->M:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 120
    :cond_a
    iput v4, p0, Lcom/wssnps/b/ag;->j:I

    goto/16 :goto_1

    .line 125
    :cond_b
    iget v0, p1, Lcom/wssnps/a/r;->k:I

    if-ne v0, v6, :cond_c

    .line 126
    iput v5, p0, Lcom/wssnps/b/ag;->k:I

    goto/16 :goto_2

    .line 127
    :cond_c
    iget v0, p1, Lcom/wssnps/a/r;->k:I

    if-ne v0, v7, :cond_3

    .line 128
    iput v4, p0, Lcom/wssnps/b/ag;->k:I

    goto/16 :goto_2

    .line 158
    :cond_d
    iget-object v0, p1, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget-object v0, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget-object v1, p1, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-ne v0, v1, :cond_e

    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget-object v0, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->month:I

    iget-object v1, p1, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    if-ne v0, v1, :cond_e

    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget-object v0, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    iget-object v1, p1, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    if-ne v0, v1, :cond_e

    .line 162
    iput v6, p0, Lcom/wssnps/b/ag;->g:I

    goto/16 :goto_3

    .line 166
    :cond_e
    iput v7, p0, Lcom/wssnps/b/ag;->g:I

    goto/16 :goto_3

    .line 171
    :cond_f
    iput-wide v8, p0, Lcom/wssnps/b/ag;->i:J

    .line 172
    iput v4, p0, Lcom/wssnps/b/ag;->h:I

    .line 173
    iput v4, p0, Lcom/wssnps/b/ag;->g:I

    goto/16 :goto_3
.end method

.method public static a(Ljava/lang/String;)Lcom/wssnps/b/ag;
    .locals 2

    .prologue
    .line 308
    invoke-static {p0}, Lcom/wssnps/a/j;->b(Ljava/lang/String;)Lcom/wssnps/a/r;

    move-result-object v0

    .line 309
    new-instance v1, Lcom/wssnps/b/ag;

    invoke-direct {v1, v0}, Lcom/wssnps/b/ag;-><init>(Lcom/wssnps/a/r;)V

    .line 310
    return-object v1
.end method

.method public static a(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 315
    const/4 v6, 0x0

    .line 316
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 317
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    const-string v0, "content://com.android.calendar/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    .line 322
    :goto_0
    const-string v3, "deleted=\"0\" AND accountName=\"My task\""

    .line 323
    sget-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 324
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 325
    if-eqz v1, :cond_1

    .line 327
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 328
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 331
    :goto_1
    const-string v1, "0"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 320
    :cond_0
    const-string v0, "content://tasks/tasks"

    sput-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1
.end method

.method public static a(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 368
    const-string v0, ""

    .line 369
    new-instance v6, Lcom/wssnps/b/ag;

    invoke-direct {v6}, Lcom/wssnps/b/ag;-><init>()V

    .line 370
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 372
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 373
    const-string v0, "content://com.android.calendar/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    .line 377
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 378
    sget-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 379
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 380
    if-eqz v0, :cond_1

    .line 382
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v6, Lcom/wssnps/b/ag;->d:I

    .line 385
    const-string v1, "due_date"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v6, Lcom/wssnps/b/ag;->l:J

    .line 386
    const-string v1, "importance"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v6, Lcom/wssnps/b/ag;->k:I

    .line 387
    const-string v1, "complete"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v6, Lcom/wssnps/b/ag;->j:I

    .line 388
    const-string v1, "start_date"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v6, Lcom/wssnps/b/ag;->m:J

    .line 389
    const-string v1, "subject"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/ag;->e:Ljava/lang/String;

    .line 390
    const-string v1, "body"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    .line 391
    const-string v1, "reminder_time"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v6, Lcom/wssnps/b/ag;->i:J

    .line 392
    const-string v1, "reminder_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v6, Lcom/wssnps/b/ag;->g:I

    .line 393
    const-string v1, "groupId"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    .line 395
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 398
    :cond_1
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v6, Lcom/wssnps/b/ag;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 403
    const-string v0, "content://com.android.calendar/images"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 404
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 405
    if-eqz v0, :cond_5

    .line 407
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 412
    :cond_2
    const-string v1, "event_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v12, :cond_3

    .line 414
    new-instance v1, Lcom/wssnps/a/l;

    invoke-direct {v1}, Lcom/wssnps/a/l;-><init>()V

    .line 415
    const-string v3, "filepath"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 416
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 418
    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/wssnps/a/l;->b:Ljava/lang/String;

    .line 419
    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 420
    iget-object v3, v6, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    :cond_3
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 427
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 431
    :cond_5
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 432
    const/4 v1, 0x3

    if-ne v0, v1, :cond_d

    .line 434
    sget-boolean v0, Lcom/wssnps/b/l;->aO:Z

    if-eqz v0, :cond_9

    .line 437
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v6, Lcom/wssnps/b/ag;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 438
    const-string v0, "content://com.android.calendar/memos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 439
    const-string v0, "content://com.sec.android.widgetapp.q1_penmemo/PenMemo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 440
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 441
    if-eqz v9, :cond_9

    .line 443
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 447
    :cond_6
    const-string v0, "event_type"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v12, :cond_7

    .line 449
    new-instance v10, Lcom/wssnps/a/n;

    invoke-direct {v10}, Lcom/wssnps/a/n;-><init>()V

    .line 450
    const-string v0, "memo_id"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/wssnps/a/n;->a:I

    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v10, Lcom/wssnps/a/n;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, v8

    move-object v4, v2

    move-object v5, v2

    .line 452
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 453
    if-eqz v0, :cond_7

    .line 455
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_c

    .line 456
    iget-object v1, v6, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 463
    :cond_7
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_6

    .line 465
    :cond_8
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 551
    :cond_9
    :goto_3
    if-nez v6, :cond_1d

    .line 553
    const-string v0, "2"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 562
    :goto_4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 375
    :cond_a
    const-string v0, "content://tasks/tasks"

    sput-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 422
    :cond_b
    const-string v1, "not found file"

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 458
    :cond_c
    const-string v1, "not found file"

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 469
    :cond_d
    const/4 v1, 0x4

    if-ne v0, v1, :cond_14

    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v6, Lcom/wssnps/b/ag;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 473
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 474
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 475
    if-eqz v0, :cond_9

    .line 477
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 481
    :cond_e
    const-string v1, "event_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v12, :cond_11

    .line 483
    new-instance v1, Lcom/wssnps/a/m;

    invoke-direct {v1}, Lcom/wssnps/a/m;-><init>()V

    .line 484
    const-string v2, "filepath"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 485
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 486
    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/wssnps/a/m;->a:Ljava/lang/String;

    .line 488
    :cond_f
    const-string v3, "imagepath"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 489
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 490
    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/wssnps/a/m;->b:Ljava/lang/String;

    .line 492
    :cond_10
    const-string v4, "page_no"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/wssnps/a/m;->c:J

    .line 493
    const-string v4, "locked"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v1, Lcom/wssnps/a/m;->d:I

    .line 494
    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 495
    iget-object v2, v6, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    :cond_11
    :goto_5
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_e

    .line 501
    :cond_12
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 497
    :cond_13
    const-string v1, "not found file"

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_5

    .line 504
    :cond_14
    const/4 v1, 0x5

    if-ne v0, v1, :cond_9

    .line 507
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v6, Lcom/wssnps/b/ag;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 508
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 509
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 510
    if-eqz v0, :cond_9

    .line 512
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 516
    :cond_15
    const-string v1, "event_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v12, :cond_18

    .line 518
    new-instance v1, Lcom/wssnps/a/o;

    invoke-direct {v1}, Lcom/wssnps/a/o;-><init>()V

    .line 519
    const-string v2, "filepath"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 520
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_16

    .line 521
    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/wssnps/a/o;->a:Ljava/lang/String;

    .line 523
    :cond_16
    const-string v3, "imagepath"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 524
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_17

    .line 525
    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/wssnps/a/o;->b:Ljava/lang/String;

    .line 527
    :cond_17
    const-string v4, "page_no"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/wssnps/a/o;->c:J

    .line 528
    const-string v4, "locked"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v1, Lcom/wssnps/a/o;->d:I

    .line 530
    const-string v4, "date"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v8, :cond_1a

    .line 531
    const-string v4, "date"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v1, Lcom/wssnps/a/o;->e:I

    .line 534
    :goto_6
    const-string v4, "memo_id "

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v8, :cond_1b

    .line 535
    const-string v4, "memo_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v1, Lcom/wssnps/a/o;->f:I

    .line 539
    :goto_7
    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 540
    iget-object v2, v6, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    :cond_18
    :goto_8
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_15

    .line 546
    :cond_19
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 533
    :cond_1a
    iput v9, v1, Lcom/wssnps/a/o;->e:I

    goto :goto_6

    .line 537
    :cond_1b
    iput v9, v1, Lcom/wssnps/a/o;->f:I

    goto :goto_7

    .line 542
    :cond_1c
    const-string v1, "not found file"

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_8

    .line 557
    :cond_1d
    invoke-virtual {v6}, Lcom/wssnps/b/ag;->b()Ljava/lang/String;

    move-result-object v0

    .line 559
    const-string v1, "0"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)Ljava/lang/String;
    .locals 10

    .prologue
    .line 567
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 568
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 571
    iget-object v0, p1, Lcom/wssnps/b/ag;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 573
    const-string v0, "subject"

    iget-object v1, p1, Lcom/wssnps/b/ag;->e:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    :goto_0
    iget-object v0, p1, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 583
    const-string v0, "body"

    iget-object v1, p1, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    iget-object v0, p1, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p1, Lcom/wssnps/b/ag;->n:I

    .line 591
    :goto_1
    const-string v0, "body_size"

    iget v1, p1, Lcom/wssnps/b/ag;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 594
    const-string v0, "importance"

    iget v1, p1, Lcom/wssnps/b/ag;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 597
    const-string v0, "complete"

    iget v1, p1, Lcom/wssnps/b/ag;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 600
    iget-wide v0, p1, Lcom/wssnps/b/ag;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 602
    const-string v0, "start_date"

    iget-wide v2, p1, Lcom/wssnps/b/ag;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 603
    const-string v0, "utc_start_date"

    iget-wide v2, p1, Lcom/wssnps/b/ag;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 607
    :cond_0
    iget-wide v0, p1, Lcom/wssnps/b/ag;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 609
    const-string v0, "due_date"

    iget-wide v2, p1, Lcom/wssnps/b/ag;->l:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 610
    const-string v0, "utc_due_date"

    iget-wide v2, p1, Lcom/wssnps/b/ag;->l:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 614
    :cond_1
    iget-wide v0, p1, Lcom/wssnps/b/ag;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 616
    const-string v0, "reminder_time"

    iget-wide v2, p1, Lcom/wssnps/b/ag;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 624
    :goto_2
    const-string v0, "reminder_set"

    iget v1, p1, Lcom/wssnps/b/ag;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 625
    const-string v0, "reminder_type"

    iget v1, p1, Lcom/wssnps/b/ag;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 628
    const-string v0, "bodyType"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 629
    const-string v0, "accountKey"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 630
    const-string v0, "_sync_dirty"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 631
    const-string v0, "clientId"

    const-string v1, "new_task_13044200773"

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    const-string v0, "accountName"

    const-string v1, "My task"

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    iget-object v0, p1, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 637
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 638
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 639
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 640
    if-eqz v0, :cond_2

    .line 642
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_7

    .line 643
    const-string v1, "groupId"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 647
    :goto_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 651
    :cond_2
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 652
    const-string v0, "content://com.android.calendar/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    .line 656
    :goto_4
    sget-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 657
    invoke-virtual {p0, v0, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 659
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 662
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 666
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 667
    const-string v0, "content://com.android.calendar/images"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 668
    iget-object v0, p1, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_9

    .line 670
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    iget-object v0, p1, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 672
    const-string v0, "event_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 673
    iget-object v0, p1, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/l;

    iget-object v0, v0, Lcom/wssnps/a/l;->b:Ljava/lang/String;

    .line 674
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 675
    const-string v5, "filepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    :cond_3
    const-string v0, "event_type"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 677
    invoke-virtual {p0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 670
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 577
    :cond_4
    const-string v0, "subject"

    const-string v1, ""

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 588
    :cond_5
    const-string v0, "body"

    const-string v1, ""

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const/4 v0, 0x0

    iput v0, p1, Lcom/wssnps/b/ag;->n:I

    goto/16 :goto_1

    .line 620
    :cond_6
    const/4 v0, 0x0

    .line 621
    const-string v1, "reminder_time"

    invoke-virtual {v7, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 645
    :cond_7
    const-string v1, "groupId"

    iget-object v2, p1, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 654
    :cond_8
    const-string v0, "content://tasks/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    goto/16 :goto_4

    .line 682
    :cond_9
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 683
    const/4 v1, 0x3

    if-ne v0, v1, :cond_a

    .line 685
    sget-boolean v0, Lcom/wssnps/b/l;->aO:Z

    if-eqz v0, :cond_12

    .line 688
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 689
    const-string v0, "content://com.android.calendar/memos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 690
    iget-object v0, p1, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_12

    iget-object v0, p1, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_12

    .line 692
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    iget-object v0, p1, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 694
    const-string v0, "event_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 695
    const-string v5, "memo_id"

    iget-object v0, p1, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/n;

    iget v0, v0, Lcom/wssnps/a/n;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 696
    const-string v0, "event_type"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 697
    invoke-virtual {p0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 692
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 702
    :cond_a
    const/4 v1, 0x4

    if-ne v0, v1, :cond_d

    .line 705
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 706
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 707
    iget-object v0, p1, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    if-eqz v0, :cond_12

    iget-object v0, p1, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_12

    .line 709
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p1, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 711
    const-string v0, "event_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 712
    iget-object v0, p1, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    iget-object v0, v0, Lcom/wssnps/a/m;->a:Ljava/lang/String;

    .line 713
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 714
    const-string v5, "filepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    :cond_b
    iget-object v0, p1, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    iget-object v0, v0, Lcom/wssnps/a/m;->b:Ljava/lang/String;

    .line 717
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 718
    const-string v5, "imagepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    :cond_c
    const-string v5, "page_no"

    iget-object v0, p1, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    iget-wide v8, v0, Lcom/wssnps/a/m;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 721
    const-string v5, "locked"

    iget-object v0, p1, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    iget v0, v0, Lcom/wssnps/a/m;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 722
    const-string v0, "event_type"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 723
    invoke-virtual {p0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 709
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 727
    :cond_d
    const/4 v1, 0x5

    if-ne v0, v1, :cond_12

    .line 730
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 731
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 732
    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    if-eqz v0, :cond_12

    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_12

    .line 734
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_12

    .line 736
    const-string v0, "event_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 737
    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget-object v0, v0, Lcom/wssnps/a/o;->a:Ljava/lang/String;

    .line 738
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 739
    const-string v5, "filepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    :cond_e
    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget-object v0, v0, Lcom/wssnps/a/o;->b:Ljava/lang/String;

    .line 742
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 743
    const-string v5, "imagepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    :cond_f
    const-string v5, "page_no"

    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget-wide v8, v0, Lcom/wssnps/a/o;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 746
    const-string v5, "locked"

    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 748
    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->e:I

    if-eqz v0, :cond_10

    .line 749
    const-string v5, "date"

    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 750
    :cond_10
    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->f:I

    if-eqz v0, :cond_11

    .line 751
    const-string v5, "memo_id"

    iget-object v0, p1, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 753
    :cond_11
    const-string v0, "event_type"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 754
    invoke-virtual {p0, v4, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 734
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_8

    .line 760
    :cond_12
    if-nez v2, :cond_13

    .line 761
    const-string v0, "6"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 765
    :goto_9
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 763
    :cond_13
    const-string v0, "0"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 963
    new-instance v6, Lcom/wssnps/b/ag;

    invoke-direct {v6}, Lcom/wssnps/b/ag;-><init>()V

    .line 964
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 965
    const-string v0, ""

    .line 966
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 968
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 969
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 970
    if-eqz v0, :cond_1

    .line 972
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 974
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    .line 975
    const-string v1, "groupName"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    .line 976
    const-string v1, "selected"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/ag;->q:Ljava/lang/String;

    .line 977
    const-string v1, "group_order"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    .line 980
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 983
    :cond_1
    iget-object v0, v6, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 985
    const-string v0, "2"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 994
    :goto_0
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 989
    :cond_2
    invoke-virtual {v6}, Lcom/wssnps/b/ag;->b()Ljava/lang/String;

    move-result-object v0

    .line 990
    const-string v1, "0"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static b(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)I
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 770
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 773
    iget v0, p1, Lcom/wssnps/b/ag;->d:I

    if-eqz v0, :cond_0

    .line 775
    const-string v0, "_id"

    iget v1, p1, Lcom/wssnps/b/ag;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 779
    :cond_0
    iget-object v0, p1, Lcom/wssnps/b/ag;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 781
    const-string v0, "subject"

    iget-object v1, p1, Lcom/wssnps/b/ag;->e:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    :goto_0
    iget-object v0, p1, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 791
    const-string v0, "body"

    iget-object v1, p1, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    iget-object v0, p1, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p1, Lcom/wssnps/b/ag;->n:I

    .line 799
    :goto_1
    const-string v0, "bodyType"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 800
    const-string v0, "body_size"

    iget v1, p1, Lcom/wssnps/b/ag;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 803
    const-string v0, "importance"

    iget v1, p1, Lcom/wssnps/b/ag;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 806
    const-string v0, "complete"

    iget v1, p1, Lcom/wssnps/b/ag;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 809
    iget-wide v0, p1, Lcom/wssnps/b/ag;->m:J

    cmp-long v0, v0, v10

    if-eqz v0, :cond_4

    .line 811
    const-string v0, "start_date"

    iget-wide v4, p1, Lcom/wssnps/b/ag;->m:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 812
    const-string v0, "utc_start_date"

    iget-wide v4, p1, Lcom/wssnps/b/ag;->m:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 821
    :goto_2
    iget-wide v0, p1, Lcom/wssnps/b/ag;->l:J

    cmp-long v0, v0, v10

    if-eqz v0, :cond_5

    .line 823
    const-string v0, "due_date"

    iget-wide v4, p1, Lcom/wssnps/b/ag;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 824
    const-string v0, "utc_due_date"

    iget-wide v4, p1, Lcom/wssnps/b/ag;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 833
    :goto_3
    iget-wide v0, p1, Lcom/wssnps/b/ag;->i:J

    cmp-long v0, v0, v10

    if-eqz v0, :cond_6

    .line 835
    const-string v0, "reminder_time"

    iget-wide v4, p1, Lcom/wssnps/b/ag;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 842
    :goto_4
    const-string v0, "reminder_set"

    iget v1, p1, Lcom/wssnps/b/ag;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 843
    const-string v0, "reminder_type"

    iget v1, p1, Lcom/wssnps/b/ag;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 846
    const-string v0, "accountKey"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 847
    const-string v0, "_sync_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 848
    const-string v0, "clientId"

    const-string v1, "new_task_13044200773"

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const-string v0, "accountName"

    const-string v1, "My task"

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    iget-object v0, p1, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 854
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 855
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 856
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 858
    if-eqz v0, :cond_1

    .line 860
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_7

    .line 861
    const-string v1, "groupId"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 865
    :goto_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 869
    :cond_1
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 870
    const-string v0, "content://com.android.calendar/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    .line 874
    :goto_6
    sget-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 875
    int-to-long v4, p2

    invoke-static {v0, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 876
    invoke-virtual {p0, v0, v7, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_9

    .line 879
    :goto_7
    return p2

    .line 785
    :cond_2
    const-string v0, "subject"

    const-string v1, ""

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 796
    :cond_3
    const-string v0, "body"

    const-string v1, ""

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    iput v6, p1, Lcom/wssnps/b/ag;->n:I

    goto/16 :goto_1

    .line 816
    :cond_4
    const-string v0, "start_date"

    invoke-virtual {v7, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 817
    const-string v0, "utc_start_date"

    invoke-virtual {v7, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 828
    :cond_5
    const-string v0, "due_date"

    invoke-virtual {v7, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 829
    const-string v0, "utc_due_date"

    invoke-virtual {v7, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 840
    :cond_6
    const-string v0, "reminder_time"

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 863
    :cond_7
    const-string v1, "groupId"

    iget-object v3, p1, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    invoke-virtual {v7, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 872
    :cond_8
    const-string v0, "content://tasks/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    goto :goto_6

    :cond_9
    move p2, v6

    .line 879
    goto :goto_7
.end method

.method public static b(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 337
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 338
    const/4 v7, 0x0

    .line 340
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 341
    const-string v0, "content://com.android.calendar/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    .line 345
    :goto_0
    sget-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 346
    const-string v3, "deleted=\"0\" AND accountName =\"My task\""

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 347
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 348
    if-eqz v0, :cond_2

    .line 350
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 351
    const-string v2, "0"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356
    :cond_0
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 357
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 359
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 362
    :cond_2
    const-string v0, "\n"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 343
    :cond_3
    const-string v0, "content://tasks/tasks"

    sput-object v0, Lcom/wssnps/b/ag;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static b(Landroid/content/ContentResolver;I)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 884
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 885
    const-string v0, "content://com.android.calendar/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    .line 889
    :goto_0
    sget-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 890
    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    const-string v2, "My task"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "local"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 895
    invoke-virtual {p0, v0, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_3

    .line 897
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 901
    const-string v0, "content://com.android.calendar/images"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 902
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 903
    invoke-virtual {p0, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 906
    sget-boolean v0, Lcom/wssnps/b/l;->aO:Z

    if-eqz v0, :cond_0

    .line 909
    const-string v0, "content://com.android.calendar/memos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 910
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 911
    invoke-virtual {p0, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 915
    :cond_0
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 916
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 917
    invoke-virtual {p0, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 920
    :cond_1
    const/4 v0, 0x1

    .line 924
    :goto_1
    return v0

    .line 887
    :cond_2
    const-string v0, "content://tasks/syncTasks"

    sput-object v0, Lcom/wssnps/b/ag;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 924
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static c(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 933
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 934
    const/4 v6, 0x0

    .line 935
    const-string v0, ""

    .line 936
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 938
    const-string v3, "_id!=1"

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 940
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 941
    if-eqz v1, :cond_2

    .line 943
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 944
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v6

    .line 948
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 949
    add-int/lit8 v0, v0, 0x1

    .line 950
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 952
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 955
    :goto_1
    const-string v1, "\n"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 956
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 957
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 958
    return-object v0

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1
.end method

.method public static c(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 999
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1000
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1003
    iget-object v0, p1, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1005
    const-string v0, "groupName"

    iget-object v1, p1, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    :cond_0
    iget-object v0, p1, Lcom/wssnps/b/ag;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1011
    const-string v0, "selected"

    iget-object v1, p1, Lcom/wssnps/b/ag;->q:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    :cond_1
    iget-object v0, p1, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1018
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1019
    iget-object v0, p1, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object v7, v2

    .line 1023
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "group_order = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1027
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v2

    .line 1028
    if-eqz v2, :cond_7

    .line 1030
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_3

    .line 1032
    add-int/lit8 v0, v6, 0x1

    .line 1048
    :goto_1
    if-eqz v2, :cond_2

    .line 1049
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_2
    move v6, v0

    move-object v7, v2

    .line 1051
    goto :goto_0

    .line 1036
    :cond_3
    :try_start_2
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    .line 1037
    const-string v0, "group_order"

    iget-object v3, p1, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    invoke-virtual {v9, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1048
    if-eqz v2, :cond_4

    .line 1049
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1054
    :cond_4
    const-string v0, "_accountId"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1056
    const-string v0, "content://com.android.calendar/taskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1057
    invoke-virtual {p0, v0, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 1059
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1060
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1062
    if-nez v0, :cond_6

    .line 1063
    const-string v0, "6"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1067
    :goto_3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1042
    :catch_0
    move-exception v0

    move-object v11, v0

    move-object v0, v2

    move-object v2, v11

    .line 1044
    :goto_4
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1048
    if-eqz v0, :cond_8

    .line 1049
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v2, v0

    move v0, v6

    goto :goto_2

    .line 1048
    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1049
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 1065
    :cond_6
    const-string v1, "0"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1048
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v2, v7

    goto :goto_5

    .line 1042
    :catch_1
    move-exception v0

    move-object v2, v0

    move-object v0, v7

    goto :goto_4

    :cond_7
    move v0, v6

    goto/16 :goto_1

    :cond_8
    move-object v2, v0

    move v0, v6

    goto/16 :goto_2
.end method

.method public static c(Landroid/content/ContentResolver;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1112
    const-string v0, "content://com.android.calendar/taskGroup"

    .line 1113
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1115
    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, v1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 1116
    const/4 v0, 0x1

    .line 1118
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/ContentResolver;Lcom/wssnps/b/ag;I)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1072
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1075
    iget-object v1, p1, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1077
    const-string v1, "_id"

    iget-object v2, p1, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1081
    :cond_0
    iget-object v1, p1, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1083
    const-string v1, "groupName"

    iget-object v2, p1, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    :cond_1
    iget-object v1, p1, Lcom/wssnps/b/ag;->q:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1089
    const-string v1, "selected"

    iget-object v2, p1, Lcom/wssnps/b/ag;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    :cond_2
    iget-object v1, p1, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1095
    const-string v1, "group_order"

    iget-object v2, p1, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    :cond_3
    const-string v1, "content://com.android.calendar/taskGroup"

    .line 1100
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1101
    int-to-long v2, p2

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1103
    invoke-virtual {p0, v1, v0, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_4

    .line 1106
    :goto_0
    return p2

    :cond_4
    const/4 p2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/wssnps/a/r;
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x3

    const-wide/16 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 194
    new-instance v0, Lcom/wssnps/a/r;

    invoke-direct {v0}, Lcom/wssnps/a/r;-><init>()V

    .line 195
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 196
    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 198
    iput v9, v0, Lcom/wssnps/a/r;->c:I

    .line 199
    iget v2, p0, Lcom/wssnps/b/ag;->d:I

    iput v2, v0, Lcom/wssnps/a/r;->a:I

    .line 200
    sget-object v2, Lcom/wssnps/a/q;->a:Lcom/wssnps/a/q;

    iput-object v2, v0, Lcom/wssnps/a/r;->b:Lcom/wssnps/a/q;

    .line 201
    iget-object v2, p0, Lcom/wssnps/b/ag;->f:Ljava/lang/String;

    iput-object v2, v0, Lcom/wssnps/a/r;->d:Ljava/lang/String;

    .line 202
    iget-object v2, p0, Lcom/wssnps/b/ag;->e:Ljava/lang/String;

    iput-object v2, v0, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    .line 203
    const/4 v2, 0x0

    iput v2, v0, Lcom/wssnps/a/r;->t:I

    .line 205
    iget-object v2, p0, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 208
    iget-object v1, p0, Lcom/wssnps/b/ag;->q:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/r;->G:Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/wssnps/b/ag;->r:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/r;->H:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/wssnps/b/ag;->p:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    .line 294
    :cond_0
    :goto_0
    return-object v0

    .line 215
    :cond_1
    iget-wide v2, p0, Lcom/wssnps/b/ag;->m:J

    cmp-long v2, v2, v10

    if-nez v2, :cond_4

    .line 217
    iput-object v13, v0, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    .line 231
    :goto_1
    iget-wide v2, p0, Lcom/wssnps/b/ag;->l:J

    cmp-long v2, v2, v10

    if-nez v2, :cond_6

    .line 233
    iput-object v13, v0, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    .line 247
    :goto_2
    iget-wide v2, p0, Lcom/wssnps/b/ag;->i:J

    cmp-long v2, v2, v10

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/wssnps/b/ag;->i:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 249
    new-instance v2, Lcom/wssnps/a/k;

    invoke-direct {v2}, Lcom/wssnps/a/k;-><init>()V

    .line 250
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 252
    iget-wide v4, p0, Lcom/wssnps/b/ag;->i:J

    invoke-virtual {v1, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v1

    .line 253
    iget-wide v4, p0, Lcom/wssnps/b/ag;->i:J

    int-to-long v6, v1

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 254
    iput-object v3, v2, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    .line 255
    iget v1, p0, Lcom/wssnps/b/ag;->g:I

    iput v1, v2, Lcom/wssnps/a/k;->c:I

    .line 256
    iget-object v1, v0, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    :cond_2
    iget v1, p0, Lcom/wssnps/b/ag;->j:I

    if-ne v1, v8, :cond_3

    .line 261
    const/16 v1, 0x16

    iput v1, v0, Lcom/wssnps/a/r;->n:I

    .line 264
    :cond_3
    iget v1, p0, Lcom/wssnps/b/ag;->k:I

    if-ne v1, v9, :cond_8

    .line 265
    iput v8, v0, Lcom/wssnps/a/r;->k:I

    .line 271
    :goto_3
    iget-object v1, p0, Lcom/wssnps/b/ag;->o:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/r;->F:Ljava/lang/String;

    .line 273
    const-string v1, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 275
    iget-object v1, p0, Lcom/wssnps/b/ag;->s:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/wssnps/a/r;->J:Ljava/util/List;

    .line 277
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v1

    .line 278
    if-ne v1, v12, :cond_a

    .line 280
    sget-boolean v1, Lcom/wssnps/b/l;->aO:Z

    if-eqz v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/wssnps/b/ag;->t:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/wssnps/a/r;->K:Ljava/util/List;

    goto :goto_0

    .line 221
    :cond_4
    iget-object v2, v0, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-nez v2, :cond_5

    .line 222
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    iput-object v2, v0, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    .line 224
    :cond_5
    iget-wide v2, p0, Lcom/wssnps/b/ag;->m:J

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    .line 225
    iget-object v3, v0, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget-wide v4, p0, Lcom/wssnps/b/ag;->m:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_1

    .line 237
    :cond_6
    iget-object v2, v0, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    if-nez v2, :cond_7

    .line 238
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    iput-object v2, v0, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    .line 240
    :cond_7
    iget-wide v2, p0, Lcom/wssnps/b/ag;->l:J

    invoke-virtual {v1, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    .line 241
    iget-object v3, v0, Lcom/wssnps/a/r;->q:Landroid/text/format/Time;

    iget-wide v4, p0, Lcom/wssnps/b/ag;->l:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_2

    .line 266
    :cond_8
    iget v1, p0, Lcom/wssnps/b/ag;->k:I

    if-ne v1, v8, :cond_9

    .line 267
    iput v9, v0, Lcom/wssnps/a/r;->k:I

    goto :goto_3

    .line 269
    :cond_9
    iput v12, v0, Lcom/wssnps/a/r;->k:I

    goto :goto_3

    .line 283
    :cond_a
    const/4 v2, 0x4

    if-ne v1, v2, :cond_b

    .line 285
    iget-object v1, p0, Lcom/wssnps/b/ag;->u:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/wssnps/a/r;->L:Ljava/util/List;

    goto/16 :goto_0

    .line 287
    :cond_b
    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 289
    iget-object v1, p0, Lcom/wssnps/b/ag;->v:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/wssnps/a/r;->M:Ljava/util/List;

    goto/16 :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/wssnps/b/ag;->a()Lcom/wssnps/a/r;

    move-result-object v0

    .line 300
    invoke-static {v0}, Lcom/wssnps/a/j;->a(Lcom/wssnps/a/r;)Ljava/lang/String;

    move-result-object v0

    .line 302
    return-object v0
.end method
