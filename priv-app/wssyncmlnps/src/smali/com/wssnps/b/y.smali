.class public Lcom/wssnps/b/y;
.super Ljava/lang/Object;
.source "smlContactItem.java"

# interfaces
.implements Lcom/wssnps/a/b;


# static fields
.field public static B:Ljava/util/ArrayList;

.field public static C:Ljava/util/ArrayList;

.field public static D:Ljava/util/HashMap;

.field public static E:Lcom/wssnps/smlModelDefine;

.field public static F:Lcom/wssnps/b/ab;

.field public static G:Lcom/wssnps/b/ab;

.field public static H:Lcom/wssnps/b/ab;

.field public static I:Lcom/wssnps/b/ab;

.field public static J:Lcom/wssnps/b/ab;

.field private static K:Landroid/content/Context;

.field private static L:Landroid/accounts/AccountManager;

.field public static final a:Ljava/util/Hashtable;


# instance fields
.field public A:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/util/ArrayList;

.field public e:Ljava/util/ArrayList;

.field public f:Ljava/util/ArrayList;

.field public g:Ljava/util/ArrayList;

.field public h:Ljava/util/ArrayList;

.field public i:Ljava/util/ArrayList;

.field public j:Ljava/util/ArrayList;

.field public k:Ljava/util/ArrayList;

.field public l:Ljava/util/ArrayList;

.field public m:Ljava/util/ArrayList;

.field public n:Ljava/util/ArrayList;

.field public o:Ljava/util/ArrayList;

.field public p:Ljava/util/ArrayList;

.field public q:Ljava/util/ArrayList;

.field public r:Ljava/util/ArrayList;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/util/ArrayList;

.field public v:Lcom/wssnps/a/ab;

.field public w:Lcom/wssnps/a/ab;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x4

    const/high16 v4, 0x800000

    const/16 v3, 0x100

    .line 138
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->a:Ljava/util/Hashtable;

    .line 236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    .line 240
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->D:Ljava/util/HashMap;

    .line 244
    new-instance v0, Lcom/wssnps/smlModelDefine;

    invoke-direct {v0}, Lcom/wssnps/smlModelDefine;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->E:Lcom/wssnps/smlModelDefine;

    .line 293
    new-instance v0, Lcom/wssnps/b/ab;

    invoke-direct {v0}, Lcom/wssnps/b/ab;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    .line 294
    new-instance v0, Lcom/wssnps/b/ab;

    invoke-direct {v0}, Lcom/wssnps/b/ab;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    .line 295
    new-instance v0, Lcom/wssnps/b/ab;

    invoke-direct {v0}, Lcom/wssnps/b/ab;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    .line 296
    new-instance v0, Lcom/wssnps/b/ab;

    invoke-direct {v0}, Lcom/wssnps/b/ab;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    .line 297
    new-instance v0, Lcom/wssnps/b/ab;

    invoke-direct {v0}, Lcom/wssnps/b/ab;-><init>()V

    sput-object v0, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    .line 305
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "1"

    invoke-virtual {v0, v1, v6}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 306
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "2"

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 307
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "3"

    invoke-virtual {v0, v1, v5}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 308
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "4"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 309
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "5"

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 310
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "6"

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 311
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "7"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 312
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "8"

    const/high16 v2, 0x1000000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 313
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "9"

    const/16 v2, 0x400

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 314
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "10"

    const v2, 0x2000004

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 315
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "11"

    const/16 v2, 0x800

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 316
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "12"

    const/high16 v2, 0x2000000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 317
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "13"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 318
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "14"

    const/high16 v2, 0x4000000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 319
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "15"

    const/high16 v2, 0x8000000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 320
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "16"

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 321
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "17"

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 322
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "18"

    const/16 v2, 0x84

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 323
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "19"

    const/high16 v2, 0x20000000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 324
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "20"

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 325
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "0"

    invoke-virtual {v0, v1, v4}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 326
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "21"

    const/16 v2, 0x4000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 327
    sget-object v0, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    const-string v1, "7"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->b(Ljava/lang/String;I)V

    .line 330
    sget-object v0, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    const-string v1, "1"

    invoke-virtual {v0, v1, v6}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 331
    sget-object v0, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    const-string v1, "2"

    invoke-virtual {v0, v1, v5}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 332
    sget-object v0, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    const-string v1, "3"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 333
    sget-object v0, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    const-string v1, "4"

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 334
    sget-object v0, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    const-string v1, "0"

    invoke-virtual {v0, v1, v4}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 335
    sget-object v0, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    const-string v1, "3"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->b(Ljava/lang/String;I)V

    .line 337
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "0"

    const v2, 0x8000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 338
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "1"

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 339
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "2"

    const/high16 v2, 0x20000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 340
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "3"

    const/high16 v2, 0x40000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 341
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "4"

    const/high16 v2, 0x80000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 342
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "5"

    const/high16 v2, 0x100000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 343
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "6"

    const/high16 v2, 0x200000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 344
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "7"

    const/high16 v2, 0x400000

    invoke-virtual {v0, v1, v2}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 345
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "-1"

    invoke-virtual {v0, v1, v4}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 346
    sget-object v0, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    const-string v1, "0"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->b(Ljava/lang/String;I)V

    .line 348
    sget-object v0, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    const-string v1, "1"

    invoke-virtual {v0, v1, v6}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 349
    sget-object v0, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    const-string v1, "2"

    invoke-virtual {v0, v1, v5}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 350
    sget-object v0, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    const-string v1, "3"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 351
    sget-object v0, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    const-string v1, "0"

    invoke-virtual {v0, v1, v4}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 352
    sget-object v0, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    const-string v1, "3"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->b(Ljava/lang/String;I)V

    .line 354
    sget-object v0, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    const-string v1, "1"

    invoke-virtual {v0, v1, v5}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 355
    sget-object v0, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    const-string v1, "2"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 356
    sget-object v0, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    const-string v1, "0"

    invoke-virtual {v0, v1, v4}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;I)V

    .line 357
    sget-object v0, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    const-string v1, "2"

    invoke-virtual {v0, v1, v3}, Lcom/wssnps/b/ab;->b(Ljava/lang/String;I)V

    .line 359
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    .line 215
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    .line 216
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    .line 218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    .line 226
    iput-object v1, p0, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    .line 227
    iput-object v1, p0, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    .line 228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    .line 229
    new-instance v0, Lcom/wssnps/a/ab;

    invoke-direct {v0}, Lcom/wssnps/a/ab;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    .line 230
    new-instance v0, Lcom/wssnps/a/ab;

    invoke-direct {v0}, Lcom/wssnps/a/ab;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    .line 231
    iput-object v1, p0, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    .line 232
    iput-object v1, p0, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    .line 233
    iput-object v1, p0, Lcom/wssnps/b/y;->z:Ljava/lang/String;

    .line 234
    iput-object v1, p0, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    .line 423
    return-void
.end method

.method public constructor <init>(Lcom/wssnps/a/ac;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    .line 212
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    .line 215
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    .line 216
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    .line 217
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    .line 218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    .line 220
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    .line 222
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    .line 226
    iput-object v2, p0, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    .line 227
    iput-object v2, p0, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    .line 228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    .line 229
    new-instance v0, Lcom/wssnps/a/ab;

    invoke-direct {v0}, Lcom/wssnps/a/ab;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    .line 230
    new-instance v0, Lcom/wssnps/a/ab;

    invoke-direct {v0}, Lcom/wssnps/a/ab;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    .line 231
    iput-object v2, p0, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    .line 232
    iput-object v2, p0, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    .line 233
    iput-object v2, p0, Lcom/wssnps/b/y;->z:Ljava/lang/String;

    .line 234
    iput-object v2, p0, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    .line 429
    if-nez p1, :cond_1

    .line 1139
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    iget v0, p1, Lcom/wssnps/a/ac;->a:I

    iput v0, p0, Lcom/wssnps/b/y;->b:I

    .line 436
    iget-object v0, p1, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    if-eqz v0, :cond_3

    .line 438
    iget-object v0, p1, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 439
    iget-object v0, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v1, p1, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    iget-object v1, v1, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    .line 440
    :cond_2
    iget-object v0, p1, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 441
    iget-object v0, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v1, p1, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    iget-object v1, v1, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    .line 445
    :cond_3
    iget-object v0, p1, Lcom/wssnps/a/ac;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    .line 454
    iget-object v0, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 456
    iget-object v0, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    if-eqz v0, :cond_11

    .line 458
    iget-object v0, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 460
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    .line 477
    :cond_4
    :goto_1
    new-instance v0, Lcom/wssnps/b/z;

    invoke-direct {v0}, Lcom/wssnps/b/z;-><init>()V

    .line 478
    iget-object v1, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 481
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    if-eqz v1, :cond_d

    .line 484
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 485
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 487
    :cond_5
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 488
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 490
    :cond_6
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 491
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    .line 493
    :cond_7
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 494
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    .line 496
    :cond_8
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 497
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    .line 499
    :cond_9
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 500
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    .line 502
    :cond_a
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 503
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    .line 505
    :cond_b
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 506
    iget-object v1, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, v1, Lcom/wssnps/a/y;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    .line 508
    :cond_c
    iget-object v1, p0, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_d
    move-object v1, v0

    move v2, v3

    .line 513
    :goto_2
    iget-object v0, p1, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_13

    .line 515
    iget-object v0, p1, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 516
    iget-object v4, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_12

    move-object v0, v1

    .line 513
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_2

    .line 462
    :cond_e
    iget-object v0, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 464
    iget-object v0, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 466
    :cond_f
    iget-object v0, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 468
    iget-object v0, p1, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v0, v0, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 471
    :cond_10
    iput-object v2, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 474
    :cond_11
    iput-object v2, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    goto/16 :goto_1

    .line 518
    :cond_12
    iget-object v4, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    if-eqz v4, :cond_6b

    .line 520
    new-instance v1, Lcom/wssnps/b/z;

    invoke-direct {v1}, Lcom/wssnps/b/z;-><init>()V

    .line 521
    iget-object v0, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iput-object v0, v1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 522
    iget-object v0, p0, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_3

    :cond_13
    move v2, v3

    .line 528
    :goto_4
    iget-object v0, p1, Lcom/wssnps/a/ac;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_16

    .line 530
    iget-object v0, p1, Lcom/wssnps/a/ac;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 531
    iget-object v4, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 528
    :cond_14
    :goto_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 533
    :cond_15
    iget-object v4, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    if-eqz v4, :cond_14

    .line 535
    new-instance v1, Lcom/wssnps/b/z;

    invoke-direct {v1}, Lcom/wssnps/b/z;-><init>()V

    .line 536
    iget-object v0, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iput-object v0, v1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 537
    iget-object v0, p0, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_16
    move v2, v3

    .line 543
    :goto_6
    iget-object v0, p1, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1c

    .line 545
    iget-object v0, p1, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 546
    iget-object v4, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 543
    :cond_17
    :goto_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 549
    :cond_18
    sget-object v4, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    iget v6, v0, Lcom/wssnps/a/w;->b:I

    invoke-virtual {v4, v6}, Lcom/wssnps/b/ab;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 551
    if-nez v4, :cond_19

    .line 552
    sget-object v4, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    invoke-virtual {v4}, Lcom/wssnps/b/ab;->a()Ljava/lang/String;

    move-result-object v4

    .line 554
    :cond_19
    if-eqz v4, :cond_17

    .line 556
    const-string v6, "21"

    if-ne v4, v6, :cond_1a

    .line 558
    iget-object v0, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    goto :goto_7

    .line 562
    :cond_1a
    new-instance v1, Lcom/wssnps/b/z;

    invoke-direct {v1}, Lcom/wssnps/b/z;-><init>()V

    .line 563
    iget-object v6, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iput-object v6, v1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 564
    iput-object v4, v1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 565
    const-string v6, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    .line 566
    iget-object v0, v0, Lcom/wssnps/a/w;->c:Ljava/lang/String;

    iput-object v0, v1, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 567
    :cond_1b
    iget-object v0, p0, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 574
    :cond_1c
    iget-object v0, p1, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_1d

    iget-object v0, p1, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1d

    move v2, v3

    .line 576
    :goto_8
    iget-object v0, p1, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1d

    .line 578
    iget-object v0, p1, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 579
    iget-object v4, p0, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    :cond_1d
    move v2, v3

    .line 585
    :goto_9
    iget-object v0, p1, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_22

    .line 587
    iget-object v0, p1, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 588
    iget-object v4, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 585
    :cond_1e
    :goto_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 591
    :cond_1f
    sget-object v4, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    iget v6, v0, Lcom/wssnps/a/w;->b:I

    invoke-virtual {v4, v6}, Lcom/wssnps/b/ab;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 593
    if-nez v4, :cond_20

    .line 594
    sget-object v4, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    invoke-virtual {v4}, Lcom/wssnps/b/ab;->a()Ljava/lang/String;

    move-result-object v4

    .line 596
    :cond_20
    if-eqz v4, :cond_1e

    .line 598
    new-instance v1, Lcom/wssnps/b/z;

    invoke-direct {v1}, Lcom/wssnps/b/z;-><init>()V

    .line 599
    iget-object v6, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iput-object v6, v1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 600
    iput-object v4, v1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 601
    const-string v6, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 602
    iget-object v0, v0, Lcom/wssnps/a/w;->c:Ljava/lang/String;

    iput-object v0, v1, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 603
    :cond_21
    iget-object v0, p0, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_22
    move v2, v3

    .line 610
    :goto_b
    iget-object v0, p1, Lcom/wssnps/a/ac;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2e

    .line 613
    iget-object v0, p1, Lcom/wssnps/a/ac;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/u;

    .line 614
    sget-object v4, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    iget v6, v0, Lcom/wssnps/a/u;->h:I

    invoke-virtual {v4, v6}, Lcom/wssnps/b/ab;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 616
    if-nez v4, :cond_23

    .line 617
    sget-object v4, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    invoke-virtual {v4}, Lcom/wssnps/b/ab;->a()Ljava/lang/String;

    move-result-object v4

    .line 619
    :cond_23
    if-eqz v4, :cond_2c

    .line 621
    new-instance v1, Lcom/wssnps/b/z;

    invoke-direct {v1}, Lcom/wssnps/b/z;-><init>()V

    .line 622
    iput-object v4, v1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 623
    const-string v6, "0"

    invoke-virtual {v4, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_24

    .line 624
    iget-object v4, v0, Lcom/wssnps/a/u;->j:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 626
    :cond_24
    iget-object v4, v0, Lcom/wssnps/a/u;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, v0, Lcom/wssnps/a/u;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, v0, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, v0, Lcom/wssnps/a/u;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, v0, Lcom/wssnps/a/u;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, v0, Lcom/wssnps/a/u;->f:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, v0, Lcom/wssnps/a/u;->g:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2d

    .line 634
    :cond_25
    iget-object v4, v0, Lcom/wssnps/a/u;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 636
    iget-object v4, v0, Lcom/wssnps/a/u;->c:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    .line 638
    :cond_26
    iget-object v4, v0, Lcom/wssnps/a/u;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_27

    .line 640
    iget-object v4, v0, Lcom/wssnps/a/u;->a:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    .line 642
    :cond_27
    iget-object v4, v0, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_28

    .line 644
    iget-object v4, v0, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    .line 646
    :cond_28
    iget-object v4, v0, Lcom/wssnps/a/u;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_29

    .line 648
    iget-object v4, v0, Lcom/wssnps/a/u;->d:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    .line 650
    :cond_29
    iget-object v4, v0, Lcom/wssnps/a/u;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    .line 652
    iget-object v4, v0, Lcom/wssnps/a/u;->e:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    .line 654
    :cond_2a
    iget-object v4, v0, Lcom/wssnps/a/u;->f:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2b

    .line 656
    iget-object v4, v0, Lcom/wssnps/a/u;->f:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    .line 658
    :cond_2b
    iget-object v4, v0, Lcom/wssnps/a/u;->g:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2c

    .line 660
    iget-object v0, v0, Lcom/wssnps/a/u;->g:Ljava/lang/String;

    iput-object v0, v1, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    .line 672
    :cond_2c
    :goto_c
    iget-object v0, p0, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_b

    .line 665
    :cond_2d
    iget-object v4, v0, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2c

    .line 667
    iget-object v0, v0, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    iput-object v0, v1, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    goto :goto_c

    :cond_2e
    move v1, v3

    .line 678
    :goto_d
    iget-object v0, p1, Lcom/wssnps/a/ac;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_34

    .line 680
    iget-object v0, p1, Lcom/wssnps/a/ac;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/z;

    .line 682
    sget-object v2, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    iget v4, v0, Lcom/wssnps/a/z;->e:I

    invoke-virtual {v2, v4}, Lcom/wssnps/b/ab;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 684
    if-nez v2, :cond_2f

    .line 685
    sget-object v2, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    invoke-virtual {v2}, Lcom/wssnps/b/ab;->a()Ljava/lang/String;

    move-result-object v2

    .line 687
    :cond_2f
    if-eqz v2, :cond_33

    .line 689
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 691
    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 692
    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_30

    .line 693
    iget-object v2, v0, Lcom/wssnps/a/z;->g:Ljava/lang/String;

    iput-object v2, v4, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 694
    :cond_30
    iget-object v2, v0, Lcom/wssnps/a/z;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_31

    .line 696
    iget-object v2, v0, Lcom/wssnps/a/z;->a:Ljava/lang/String;

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 699
    :cond_31
    iget-object v2, v0, Lcom/wssnps/a/z;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_32

    .line 701
    iget-object v0, v0, Lcom/wssnps/a/z;->c:Ljava/lang/String;

    iput-object v0, v4, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    .line 704
    :cond_32
    iget-object v0, p0, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 678
    :cond_33
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    :cond_34
    move v1, v3

    .line 710
    :goto_e
    iget-object v0, p1, Lcom/wssnps/a/ac;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_36

    .line 712
    iget-object v0, p1, Lcom/wssnps/a/ac;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 713
    iget-object v2, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 710
    :goto_f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    .line 716
    :cond_35
    new-instance v2, Lcom/wssnps/b/z;

    invoke-direct {v2}, Lcom/wssnps/b/z;-><init>()V

    .line 717
    iget-object v0, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iput-object v0, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 719
    iget-object v0, p0, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    :cond_36
    move v1, v3

    .line 725
    :goto_10
    iget-object v0, p1, Lcom/wssnps/a/ac;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3b

    .line 727
    iget-object v0, p1, Lcom/wssnps/a/ac;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 728
    iget-object v2, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_38

    .line 725
    :cond_37
    :goto_11
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 731
    :cond_38
    sget-object v2, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    iget v4, v0, Lcom/wssnps/a/w;->b:I

    invoke-virtual {v2, v4}, Lcom/wssnps/b/ab;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 733
    if-nez v2, :cond_39

    .line 734
    sget-object v2, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    invoke-virtual {v2}, Lcom/wssnps/b/ab;->a()Ljava/lang/String;

    move-result-object v2

    .line 736
    :cond_39
    if-eqz v2, :cond_37

    .line 738
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 739
    iget-object v6, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iput-object v6, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 740
    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 741
    iput-object v2, v4, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    .line 743
    const-string v6, "-1"

    invoke-virtual {v2, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3a

    .line 746
    iget-object v0, v0, Lcom/wssnps/a/w;->c:Ljava/lang/String;

    iput-object v0, v4, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    .line 749
    :cond_3a
    iget-object v0, p0, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11

    :cond_3b
    move v1, v3

    .line 756
    :goto_12
    iget-object v0, p1, Lcom/wssnps/a/ac;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3d

    .line 758
    iget-object v0, p1, Lcom/wssnps/a/ac;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 759
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3c

    .line 761
    new-instance v2, Lcom/wssnps/b/z;

    invoke-direct {v2}, Lcom/wssnps/b/z;-><init>()V

    .line 762
    iput-object v0, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 763
    iget-object v0, p0, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 756
    :cond_3c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    .line 769
    :cond_3d
    iget-object v0, p1, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    if-eqz v0, :cond_3e

    .line 772
    iget-object v0, p1, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    .line 773
    iget-object v0, v0, Lcom/wssnps/a/v;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    :cond_3e
    move v1, v3

    .line 784
    :goto_13
    iget-object v0, p1, Lcom/wssnps/a/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_46

    .line 786
    iget-object v0, p1, Lcom/wssnps/a/ac;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/t;

    .line 788
    const-string v2, ""

    .line 789
    const-string v7, ""

    .line 790
    const-string v4, ""

    .line 793
    iget v6, p1, Lcom/wssnps/a/ac;->B:I

    if-ne v6, v5, :cond_41

    .line 794
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 798
    :goto_14
    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_42

    .line 800
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 801
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 802
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 810
    :goto_15
    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_43

    .line 812
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 813
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 814
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 822
    :goto_16
    invoke-static {}, Lcom/wssnps/smlModelDefine;->b()Z

    move-result v6

    if-eqz v6, :cond_6a

    .line 824
    new-instance v8, Lcom/a/a/a/a;

    invoke-direct {v8}, Lcom/a/a/a/a;-><init>()V

    .line 825
    iget v4, v0, Lcom/wssnps/a/t;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 826
    const-string v4, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_69

    .line 830
    const-string v4, "2"

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_68

    move v4, v5

    .line 833
    :goto_17
    iget v7, v0, Lcom/wssnps/a/t;->a:I

    iget v9, v0, Lcom/wssnps/a/t;->c:I

    add-int/lit8 v9, v9, -0x1

    iget v0, v0, Lcom/wssnps/a/t;->d:I

    invoke-virtual {v8, v7, v9, v0, v4}, Lcom/a/a/a/a;->a(IIIZ)V

    .line 834
    invoke-virtual {v8}, Lcom/a/a/a/a;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 835
    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    if-gt v4, v5, :cond_44

    .line 837
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 838
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 839
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 847
    :goto_18
    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v4, v5, :cond_45

    .line 849
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 850
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 851
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move-object v0, v6

    .line 861
    :goto_19
    new-instance v6, Lcom/wssnps/b/z;

    invoke-direct {v6}, Lcom/wssnps/b/z;-><init>()V

    .line 862
    iput-object v2, v6, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 863
    const-string v2, "3"

    iput-object v2, v6, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 864
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3f

    .line 865
    iput-object v4, v6, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    .line 866
    :cond_3f
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_40

    .line 867
    iput-object v0, v6, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 868
    :cond_40
    iget-object v0, p0, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 784
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_13

    .line 796
    :cond_41
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_14

    .line 806
    :cond_42
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 807
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_15

    .line 818
    :cond_43
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 819
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_16

    .line 843
    :cond_44
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 844
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_18

    .line 855
    :cond_45
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 856
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move-object v0, v6

    goto/16 :goto_19

    :cond_46
    move v1, v3

    .line 872
    :goto_1a
    iget-object v0, p1, Lcom/wssnps/a/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4d

    .line 874
    iget-object v0, p1, Lcom/wssnps/a/ac;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/t;

    .line 876
    const-string v2, ""

    .line 877
    const-string v7, ""

    .line 878
    const-string v4, ""

    .line 879
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 880
    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_49

    .line 882
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 883
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 884
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 892
    :goto_1b
    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_4a

    .line 894
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 895
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 896
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 904
    :goto_1c
    invoke-static {}, Lcom/wssnps/smlModelDefine;->b()Z

    move-result v6

    if-eqz v6, :cond_67

    .line 906
    new-instance v8, Lcom/a/a/a/a;

    invoke-direct {v8}, Lcom/a/a/a/a;-><init>()V

    .line 907
    iget v4, v0, Lcom/wssnps/a/t;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 908
    const-string v4, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_66

    .line 912
    const-string v4, "2"

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_65

    move v4, v5

    .line 915
    :goto_1d
    iget v7, v0, Lcom/wssnps/a/t;->a:I

    iget v9, v0, Lcom/wssnps/a/t;->c:I

    add-int/lit8 v9, v9, -0x1

    iget v0, v0, Lcom/wssnps/a/t;->d:I

    invoke-virtual {v8, v7, v9, v0, v4}, Lcom/a/a/a/a;->a(IIIZ)V

    .line 916
    invoke-virtual {v8}, Lcom/a/a/a/a;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 917
    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    if-gt v4, v5, :cond_4b

    .line 919
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 920
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 921
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 929
    :goto_1e
    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v4, v5, :cond_4c

    .line 931
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 932
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 933
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move-object v0, v6

    .line 942
    :goto_1f
    new-instance v6, Lcom/wssnps/b/z;

    invoke-direct {v6}, Lcom/wssnps/b/z;-><init>()V

    .line 943
    iput-object v2, v6, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 944
    const-string v2, "1"

    iput-object v2, v6, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 945
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_47

    .line 946
    iput-object v4, v6, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    .line 947
    :cond_47
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_48

    .line 948
    iput-object v0, v6, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 949
    :cond_48
    iget-object v0, p0, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 872
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1a

    .line 888
    :cond_49
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 889
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1b

    .line 900
    :cond_4a
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 901
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1c

    .line 925
    :cond_4b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 926
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1e

    .line 937
    :cond_4c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 938
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move-object v0, v6

    goto/16 :goto_1f

    :cond_4d
    move v1, v3

    .line 953
    :goto_20
    iget-object v0, p1, Lcom/wssnps/a/ac;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_54

    .line 955
    iget-object v0, p1, Lcom/wssnps/a/ac;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/t;

    .line 957
    const-string v2, ""

    .line 958
    const-string v7, ""

    .line 959
    const-string v4, ""

    .line 960
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 961
    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_50

    .line 963
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 964
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 965
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 973
    :goto_21
    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_51

    .line 975
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 976
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 977
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 985
    :goto_22
    invoke-static {}, Lcom/wssnps/smlModelDefine;->b()Z

    move-result v6

    if-eqz v6, :cond_64

    .line 987
    new-instance v8, Lcom/a/a/a/a;

    invoke-direct {v8}, Lcom/a/a/a/a;-><init>()V

    .line 988
    iget v4, v0, Lcom/wssnps/a/t;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 989
    const-string v4, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_63

    .line 993
    const-string v4, "2"

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_62

    move v4, v5

    .line 996
    :goto_23
    iget v7, v0, Lcom/wssnps/a/t;->a:I

    iget v9, v0, Lcom/wssnps/a/t;->c:I

    add-int/lit8 v9, v9, -0x1

    iget v0, v0, Lcom/wssnps/a/t;->d:I

    invoke-virtual {v8, v7, v9, v0, v4}, Lcom/a/a/a/a;->a(IIIZ)V

    .line 997
    invoke-virtual {v8}, Lcom/a/a/a/a;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 998
    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    if-gt v4, v5, :cond_52

    .line 1000
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1001
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1002
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1010
    :goto_24
    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v4, v5, :cond_53

    .line 1012
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1013
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "0"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1014
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move-object v0, v6

    .line 1023
    :goto_25
    new-instance v6, Lcom/wssnps/b/z;

    invoke-direct {v6}, Lcom/wssnps/b/z;-><init>()V

    .line 1024
    iput-object v2, v6, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 1025
    const-string v2, "2"

    iput-object v2, v6, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 1026
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4e

    .line 1027
    iput-object v4, v6, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    .line 1028
    :cond_4e
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4f

    .line 1029
    iput-object v0, v6, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 1030
    :cond_4f
    iget-object v0, p0, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 953
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_20

    .line 969
    :cond_50
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 970
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_21

    .line 981
    :cond_51
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 982
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_22

    .line 1006
    :cond_52
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1007
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_24

    .line 1018
    :cond_53
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1019
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    move-object v0, v6

    goto/16 :goto_25

    :cond_54
    move v1, v3

    .line 1034
    :goto_26
    iget-object v0, p1, Lcom/wssnps/a/ac;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5b

    .line 1036
    iget-object v0, p1, Lcom/wssnps/a/ac;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/t;

    .line 1038
    const-string v2, ""

    .line 1039
    const-string v7, ""

    .line 1040
    const-string v4, ""

    .line 1041
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1042
    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_57

    .line 1044
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1045
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1046
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1054
    :goto_27
    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-gt v6, v5, :cond_58

    .line 1056
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1057
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1058
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1066
    :goto_28
    invoke-static {}, Lcom/wssnps/smlModelDefine;->b()Z

    move-result v6

    if-eqz v6, :cond_61

    .line 1068
    new-instance v8, Lcom/a/a/a/a;

    invoke-direct {v8}, Lcom/a/a/a/a;-><init>()V

    .line 1069
    iget v4, v0, Lcom/wssnps/a/t;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 1070
    const-string v4, "0"

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_60

    .line 1074
    const-string v4, "2"

    invoke-virtual {v6, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_5f

    move v4, v5

    .line 1077
    :goto_29
    iget v7, v0, Lcom/wssnps/a/t;->a:I

    iget v9, v0, Lcom/wssnps/a/t;->c:I

    add-int/lit8 v9, v9, -0x1

    iget v10, v0, Lcom/wssnps/a/t;->d:I

    invoke-virtual {v8, v7, v9, v10, v4}, Lcom/a/a/a/a;->a(IIIZ)V

    .line 1078
    invoke-virtual {v8}, Lcom/a/a/a/a;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 1079
    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    if-gt v7, v5, :cond_59

    .line 1081
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "."

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1082
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "0"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1083
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1091
    :goto_2a
    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-gt v7, v5, :cond_5a

    .line 1093
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "."

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1094
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "0"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1095
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v11, v6

    move-object v6, v4

    move-object v4, v11

    .line 1104
    :goto_2b
    new-instance v7, Lcom/wssnps/b/z;

    invoke-direct {v7}, Lcom/wssnps/b/z;-><init>()V

    .line 1105
    iput-object v2, v7, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 1106
    const-string v2, "0"

    iput-object v2, v7, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 1107
    iget-object v0, v0, Lcom/wssnps/a/t;->g:Ljava/lang/String;

    iput-object v0, v7, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 1108
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_55

    .line 1109
    iput-object v6, v7, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    .line 1110
    :cond_55
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_56

    .line 1111
    iput-object v4, v7, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 1112
    :cond_56
    iget-object v0, p0, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1034
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_26

    .line 1050
    :cond_57
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1051
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_27

    .line 1062
    :cond_58
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "-"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1063
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, v0, Lcom/wssnps/a/t;->d:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_28

    .line 1087
    :cond_59
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "."

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1088
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/a/a/a/a;->b()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2a

    .line 1099
    :cond_5a
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "."

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1100
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/a/a/a/a;->c()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v11, v6

    move-object v6, v4

    move-object v4, v11

    goto/16 :goto_2b

    .line 1115
    :cond_5b
    :goto_2c
    iget-object v0, p1, Lcom/wssnps/a/ac;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_5c

    .line 1117
    iget-object v0, p1, Lcom/wssnps/a/ac;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/w;

    .line 1118
    new-instance v1, Lcom/wssnps/b/z;

    invoke-direct {v1}, Lcom/wssnps/b/z;-><init>()V

    .line 1119
    iget-object v2, v0, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    iput-object v2, v1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 1120
    iget v2, v0, Lcom/wssnps/a/w;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 1121
    iget-object v0, v0, Lcom/wssnps/a/w;->c:Ljava/lang/String;

    iput-object v0, v1, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 1122
    iget-object v0, p0, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1115
    add-int/lit8 v3, v3, 0x1

    goto :goto_2c

    .line 1125
    :cond_5c
    iget-object v0, p1, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    if-eqz v0, :cond_5d

    .line 1127
    iget-object v0, p1, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v0, v0, Lcom/wssnps/a/x;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5d

    .line 1128
    iget-object v0, p1, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v0, v0, Lcom/wssnps/a/x;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    .line 1131
    :cond_5d
    iget-object v0, p1, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1133
    iget-object v0, p1, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    const-string v1, "UNSET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5e

    .line 1134
    const-string v0, "0"

    iput-object v0, p0, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 1135
    :cond_5e
    iget-object v0, p1, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    const-string v1, "SET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1136
    const-string v0, "1"

    iput-object v0, p0, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5f
    move v4, v3

    goto/16 :goto_29

    :cond_60
    move-object v4, v6

    move-object v6, v7

    goto/16 :goto_2b

    :cond_61
    move-object v6, v7

    goto/16 :goto_2b

    :cond_62
    move v4, v3

    goto/16 :goto_23

    :cond_63
    move-object v0, v6

    move-object v4, v7

    goto/16 :goto_25

    :cond_64
    move-object v0, v4

    move-object v4, v7

    goto/16 :goto_25

    :cond_65
    move v4, v3

    goto/16 :goto_1d

    :cond_66
    move-object v0, v6

    move-object v4, v7

    goto/16 :goto_1f

    :cond_67
    move-object v0, v4

    move-object v4, v7

    goto/16 :goto_1f

    :cond_68
    move v4, v3

    goto/16 :goto_17

    :cond_69
    move-object v0, v6

    move-object v4, v7

    goto/16 :goto_19

    :cond_6a
    move-object v0, v4

    move-object v4, v7

    goto/16 :goto_19

    :cond_6b
    move-object v0, v1

    goto/16 :goto_3
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 4009
    if-eqz p1, :cond_0

    .line 4010
    const/4 v0, 0x0

    invoke-virtual {p0, p2, p1, p3, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 4012
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2620
    .line 2621
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2622
    iget-object v1, p1, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2624
    const-string v1, "title"

    iget-object v2, p1, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625
    const-string v1, "group_visible"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2626
    const-string v1, "account_name"

    iget-object v2, p1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2627
    const-string v1, "account_type"

    iget-object v2, p1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2631
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v1}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 2633
    if-nez v0, :cond_1

    .line 2635
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error add contact. luid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2636
    const/4 v0, 0x0

    .line 2641
    :goto_0
    return v0

    .line 2639
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2640
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2487
    const/4 v6, 0x0

    .line 2489
    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    .line 2490
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2491
    if-eqz v1, :cond_0

    .line 2493
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 2494
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2496
    :goto_0
    return v0

    :cond_0
    move v0, v6

    goto :goto_0
.end method

.method private static a(Landroid/content/ContentResolver;[Landroid/content/ContentValues;Landroid/net/Uri;)I
    .locals 1

    .prologue
    .line 4000
    if-eqz p1, :cond_0

    .line 4002
    invoke-virtual {p0, p2, p1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    .line 4004
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 3989
    if-eqz p1, :cond_0

    .line 3992
    invoke-virtual {p0, p2, p1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 3995
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/wssnps/a/t;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 1716
    new-instance v0, Lcom/wssnps/a/t;

    invoke-direct {v0}, Lcom/wssnps/a/t;-><init>()V

    .line 1721
    const-string v1, "--"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1723
    const-string v1, "-"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1724
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "1902-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v1, v1, v7

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1725
    iput v5, v0, Lcom/wssnps/a/t;->b:I

    .line 1728
    :cond_0
    const-string v1, "-"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1730
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1733
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1746
    const-string v1, "-"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1747
    aget-object v2, v1, v6

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/wssnps/a/t;->a:I

    .line 1748
    aget-object v2, v1, v5

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/wssnps/a/t;->c:I

    .line 1749
    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 1750
    aget-object v2, v1, v4

    invoke-virtual {v2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_3

    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v5, :cond_3

    .line 1751
    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/wssnps/a/t;->d:I

    .line 1757
    :cond_1
    :goto_0
    const-string v1, "1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 1758
    iput v5, v0, Lcom/wssnps/a/t;->e:I

    .line 1802
    :cond_2
    :goto_1
    return-object v0

    .line 1735
    :catch_0
    move-exception v1

    .line 1737
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "String2SMLDate ParseException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 1740
    :catch_1
    move-exception v1

    .line 1742
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "String2SMLDate IllegalArgumentException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 1752
    :cond_3
    aget-object v2, v1, v4

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1753
    aget-object v1, v1, v4

    invoke-virtual {v1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/wssnps/a/t;->d:I

    goto :goto_0

    .line 1754
    :cond_4
    aget-object v2, v1, v4

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLetter(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1755
    aget-object v1, v1, v4

    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/wssnps/a/t;->d:I

    goto :goto_0

    .line 1759
    :cond_5
    const-string v1, "2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_6

    .line 1760
    iput v4, v0, Lcom/wssnps/a/t;->e:I

    goto :goto_1

    .line 1761
    :cond_6
    const-string v1, "3"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 1762
    iput v7, v0, Lcom/wssnps/a/t;->e:I

    goto :goto_1

    .line 1765
    :cond_7
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1767
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy.MM.dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1770
    :try_start_1
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1783
    const-string v1, "\\."

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1784
    aget-object v2, v1, v6

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/wssnps/a/t;->a:I

    .line 1785
    aget-object v2, v1, v5

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/wssnps/a/t;->c:I

    .line 1786
    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 1787
    aget-object v2, v1, v4

    invoke-virtual {v2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_9

    aget-object v2, v1, v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v5, :cond_9

    .line 1788
    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/wssnps/a/t;->d:I

    .line 1794
    :cond_8
    :goto_2
    const-string v1, "1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_b

    .line 1795
    iput v5, v0, Lcom/wssnps/a/t;->e:I

    goto/16 :goto_1

    .line 1772
    :catch_2
    move-exception v1

    .line 1774
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "String2SMLDate ParseException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 1777
    :catch_3
    move-exception v1

    .line 1779
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "String2SMLDate IllegalArgumentException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 1789
    :cond_9
    aget-object v2, v1, v4

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1790
    aget-object v1, v1, v4

    invoke-virtual {v1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/wssnps/a/t;->d:I

    goto :goto_2

    .line 1791
    :cond_a
    aget-object v2, v1, v4

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLetter(C)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1792
    aget-object v1, v1, v4

    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/wssnps/a/t;->d:I

    goto :goto_2

    .line 1796
    :cond_b
    const-string v1, "2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_c

    .line 1797
    iput v4, v0, Lcom/wssnps/a/t;->e:I

    goto/16 :goto_1

    .line 1798
    :cond_c
    const-string v1, "3"

    invoke-virtual {p1, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 1799
    iput v7, v0, Lcom/wssnps/a/t;->e:I

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/ContentResolver;I)Lcom/wssnps/b/y;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2557
    .line 2558
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/y;->K:Landroid/content/Context;

    .line 2559
    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    .line 2560
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id=\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 2565
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2566
    if-eqz v0, :cond_2

    .line 2568
    const-string v1, "title"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 2569
    const-string v3, "account_name"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 2570
    const-string v4, "account_type"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 2571
    const-string v5, "group_is_read_only"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 2572
    const-string v6, "system_id"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 2574
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2576
    new-instance v2, Lcom/wssnps/b/y;

    invoke-direct {v2}, Lcom/wssnps/b/y;-><init>()V

    .line 2578
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-nez v5, :cond_3

    .line 2580
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    .line 2581
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    .line 2609
    :cond_0
    :goto_0
    iget-object v1, v2, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    .line 2610
    iget-object v1, v2, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    .line 2612
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2614
    :cond_2
    return-object v2

    .line 2585
    :cond_3
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    .line 2587
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 2589
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "Contacts"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2591
    sget-object v1, Lcom/wssnps/b/y;->K:Landroid/content/Context;

    const v5, 0x7f040016

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    .line 2593
    :cond_4
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "Friends"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2595
    sget-object v1, Lcom/wssnps/b/y;->K:Landroid/content/Context;

    const v5, 0x7f040015

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    .line 2597
    :cond_5
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "Family"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2599
    sget-object v1, Lcom/wssnps/b/y;->K:Landroid/content/Context;

    const v5, 0x7f040014

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    .line 2601
    :cond_6
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "Coworkers"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2603
    sget-object v1, Lcom/wssnps/b/y;->K:Landroid/content/Context;

    const v5, 0x7f040013

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    goto :goto_0

    .line 2607
    :cond_7
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;I)Lcom/wssnps/b/y;
    .locals 17

    .prologue
    .line 2796
    const/16 v2, 0x12

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v8, v2

    const/4 v2, 0x1

    const-string v3, "account_type"

    aput-object v3, v8, v2

    const/4 v2, 0x2

    const-string v3, "dirty"

    aput-object v3, v8, v2

    const/4 v2, 0x3

    const-string v3, "starred"

    aput-object v3, v8, v2

    const/4 v2, 0x4

    const-string v3, "mimetype"

    aput-object v3, v8, v2

    const/4 v2, 0x5

    const-string v3, "data1"

    aput-object v3, v8, v2

    const/4 v2, 0x6

    const-string v3, "data2"

    aput-object v3, v8, v2

    const/4 v2, 0x7

    const-string v3, "data3"

    aput-object v3, v8, v2

    const/16 v2, 0x8

    const-string v3, "data4"

    aput-object v3, v8, v2

    const/16 v2, 0x9

    const-string v3, "data5"

    aput-object v3, v8, v2

    const/16 v2, 0xa

    const-string v3, "data6"

    aput-object v3, v8, v2

    const/16 v2, 0xb

    const-string v3, "data7"

    aput-object v3, v8, v2

    const/16 v2, 0xc

    const-string v3, "data8"

    aput-object v3, v8, v2

    const/16 v2, 0xd

    const-string v3, "data9"

    aput-object v3, v8, v2

    const/16 v2, 0xe

    const-string v3, "data10"

    aput-object v3, v8, v2

    const/16 v2, 0xf

    const-string v3, "data14"

    aput-object v3, v8, v2

    const/16 v2, 0x10

    const-string v3, "data15"

    aput-object v3, v8, v2

    const/16 v2, 0x11

    const-string v3, "_id"

    aput-object v3, v8, v2

    .line 2819
    sget-object v2, Lcom/wssnps/b/y;->a:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V

    .line 2820
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2824
    const-string v2, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2826
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2827
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2831
    :cond_0
    const-string v3, "key_number"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 2832
    const-string v4, "speed_dial_data_id"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 2833
    sget-object v5, Lcom/wssnps/b/y;->a:Ljava/util/Hashtable;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v4, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2834
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2836
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2839
    :cond_2
    new-instance v9, Ljava/util/Hashtable;

    invoke-direct {v9}, Ljava/util/Hashtable;-><init>()V

    .line 2840
    const/4 v2, 0x0

    :goto_0
    array-length v3, v8

    if-ge v2, v3, :cond_3

    .line 2842
    aget-object v3, v8, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2840
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2845
    :cond_3
    new-instance v10, Lcom/wssnps/b/y;

    invoke-direct {v10}, Lcom/wssnps/b/y;-><init>()V

    .line 2849
    const-string v5, "raw_contact_id=?"

    .line 2850
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v6, v2

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object v4, v8

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2851
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 2853
    const-string v2, "mimetype"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 2856
    :cond_4
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2858
    iget-object v4, v10, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    const-string v2, "account_name"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    .line 2859
    iget-object v4, v10, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    const-string v2, "account_type"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    .line 2860
    const-string v2, "dirty"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/wssnps/b/y;->z:Ljava/lang/String;

    .line 2861
    const-string v2, "starred"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    .line 2864
    const-string v2, "vnd.android.cursor.item/name"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_5

    .line 2866
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2867
    const-string v2, "data3"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 2868
    const-string v2, "data2"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 2869
    const-string v2, "data5"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    .line 2870
    const-string v2, "data4"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    .line 2871
    const-string v2, "data6"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    .line 2872
    const-string v2, "data9"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    .line 2873
    const-string v2, "data7"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    .line 2874
    const-string v2, "data8"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    .line 2875
    iget-object v2, v10, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2879
    :cond_5
    const-string v2, "vnd.android.cursor.item/note"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_6

    .line 2881
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2882
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2883
    iget-object v2, v10, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2887
    :cond_6
    const-string v2, "vnd.android.cursor.item/nickname"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_7

    .line 2889
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2890
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2891
    iget-object v2, v10, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2895
    :cond_7
    const-string v2, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_9

    .line 2897
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2899
    const-string v2, "_id"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 2900
    sget-object v4, Lcom/wssnps/b/y;->a:Ljava/util/Hashtable;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_18

    .line 2901
    iget-object v4, v10, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    sget-object v5, Lcom/wssnps/b/y;->a:Ljava/util/Hashtable;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2906
    :cond_8
    :goto_1
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2907
    const-string v2, "data2"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 2908
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2909
    const-string v2, "data3"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 2910
    iget-object v2, v10, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2914
    :cond_9
    const-string v2, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_a

    .line 2916
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2917
    const-string v2, "data2"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 2918
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2919
    const-string v2, "data3"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 2920
    iget-object v2, v10, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2924
    :cond_a
    const-string v2, "vnd.android.cursor.item/im"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_b

    .line 2926
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2927
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2928
    const-string v2, "data2"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 2929
    const-string v2, "data5"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    .line 2930
    const-string v2, "data6"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    .line 2932
    iget-object v2, v10, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2936
    :cond_b
    const-string v2, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_c

    .line 2938
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2939
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2940
    const-string v2, "data2"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 2941
    const-string v2, "data4"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    .line 2942
    const-string v2, "data5"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    .line 2943
    const-string v2, "data6"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    .line 2944
    const-string v2, "data7"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    .line 2945
    const-string v2, "data8"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    .line 2946
    const-string v2, "data9"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    .line 2947
    const-string v2, "data10"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    .line 2948
    const-string v2, "data3"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 2950
    iget-object v2, v10, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2954
    :cond_c
    const-string v2, "vnd.android.cursor.item/website"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_d

    .line 2956
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2957
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2958
    iget-object v2, v10, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2962
    :cond_d
    const-string v2, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_11

    .line 2964
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2965
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2966
    const-string v2, "data2"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 2967
    const-string v2, "data3"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 2969
    const-string v2, "data15"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 2971
    const-string v2, "3"

    iget-object v5, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2972
    iget-object v2, v10, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2973
    :cond_e
    const-string v2, "1"

    iget-object v5, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2974
    iget-object v2, v10, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2975
    :cond_f
    const-string v2, "2"

    iget-object v5, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 2976
    iget-object v2, v10, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2977
    :cond_10
    const-string v2, "0"

    iget-object v5, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2978
    iget-object v2, v10, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2981
    :cond_11
    const-string v2, "vnd.android.cursor.item/relation"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_12

    .line 2983
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 2984
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 2985
    const-string v2, "data2"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 2986
    const-string v2, "data3"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 2987
    iget-object v2, v10, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2990
    :cond_12
    const-string v2, "vnd.android.cursor.item/sip_address"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_13

    .line 2992
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    iput-object v2, v10, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    .line 2994
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 2997
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    .line 3002
    :cond_13
    const-string v2, "vnd.android.cursor.item/organization"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_14

    .line 3004
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 3005
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 3006
    const-string v2, "data2"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    .line 3007
    const-string v2, "data4"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    .line 3008
    const-string v2, "data3"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    .line 3009
    iget-object v2, v10, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3013
    :cond_14
    const-string v2, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_15

    .line 3015
    new-instance v4, Lcom/wssnps/b/z;

    invoke-direct {v4}, Lcom/wssnps/b/z;-><init>()V

    .line 3016
    const-string v2, "data1"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 3017
    iget-object v2, v10, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3021
    :cond_15
    const-string v2, "vnd.android.cursor.item/photo"

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1a

    .line 3023
    const-string v2, ""

    .line 3025
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    .line 3027
    const/16 v2, 0x4000

    new-array v13, v2, [B

    .line 3028
    const/4 v6, 0x0

    .line 3029
    const/4 v5, 0x0

    .line 3030
    const/4 v3, 0x0

    .line 3031
    const/4 v4, 0x0

    .line 3033
    const-string v2, "data14"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3034
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_16

    if-nez p2, :cond_1e

    .line 3038
    :cond_16
    const-string v2, "data15"

    invoke-virtual {v9, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 3039
    if-eqz v2, :cond_1a

    array-length v3, v2

    if-eqz v3, :cond_1a

    .line 3044
    new-instance v3, Ljava/lang/StringBuffer;

    new-instance v4, Ljava/lang/String;

    invoke-static {v2}, Lcom/wssnps/a/a;->a([B)[B

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3046
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-ge v2, v4, :cond_19

    .line 3048
    rem-int/lit8 v4, v2, 0x4c

    if-nez v4, :cond_17

    .line 3049
    const-string v4, "\r\n "

    invoke-virtual {v12, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3050
    :cond_17
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v4

    invoke-virtual {v12, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3046
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2903
    :cond_18
    iget-object v2, v10, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 3052
    :cond_19
    const-string v2, "\r\n"

    invoke-virtual {v12, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3053
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    .line 3113
    :cond_1a
    :goto_3
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_4

    .line 3115
    :cond_1b
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 3117
    iget-object v2, v10, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    if-eqz v2, :cond_1c

    iget-object v2, v10, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1d

    .line 3119
    :cond_1c
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "account_name"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "account_type"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "dirty"

    aput-object v3, v4, v2

    .line 3121
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3122
    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 3123
    if-eqz v2, :cond_1d

    .line 3125
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 3126
    iget-object v3, v10, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    .line 3127
    iget-object v3, v10, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    .line 3128
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 3132
    :cond_1d
    return-object v10

    .line 3060
    :cond_1e
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 3061
    sget-object v7, Landroid/provider/ContactsContract$DisplayPhoto;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v14, v2

    invoke-static {v7, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 3063
    const-string v7, "r"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 3064
    if-eqz v7, :cond_2c

    .line 3066
    :try_start_1
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 3067
    :try_start_2
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-direct {v5, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 3068
    :try_start_3
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 3071
    :goto_4
    :try_start_4
    invoke-virtual {v5, v13}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_22

    .line 3073
    const/4 v4, 0x0

    invoke-virtual {v3, v13, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto :goto_4

    .line 3086
    :catch_0
    move-exception v2

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    .line 3088
    :goto_5
    const/4 v7, 0x1

    :try_start_5
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Load Full Size Contact Photo Exception"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 3094
    if-eqz v3, :cond_1f

    .line 3095
    :try_start_6
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 3097
    :cond_1f
    if-eqz v4, :cond_20

    .line 3098
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V

    .line 3100
    :cond_20
    if-eqz v5, :cond_21

    .line 3101
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 3103
    :cond_21
    if-eqz v6, :cond_1a

    .line 3104
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_3

    .line 3106
    :catch_1
    move-exception v2

    .line 3108
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Load Full Size Contact Photo Close Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 3075
    :cond_22
    :try_start_7
    new-instance v4, Ljava/lang/StringBuffer;

    new-instance v2, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v13

    invoke-static {v13}, Lcom/wssnps/a/a;->a([B)[B

    move-result-object v13

    invoke-direct {v2, v13}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v4, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3076
    const/4 v2, 0x0

    :goto_6
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v13

    if-ge v2, v13, :cond_24

    .line 3078
    rem-int/lit8 v13, v2, 0x4c

    if-nez v13, :cond_23

    .line 3079
    const-string v13, "\r\n "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3080
    :cond_23
    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 3076
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 3082
    :cond_24
    const-string v2, "\r\n"

    invoke-virtual {v12, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3083
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/wssnps/b/y;->s:Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 3094
    :goto_7
    if-eqz v3, :cond_25

    .line 3095
    :try_start_8
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 3097
    :cond_25
    if-eqz v5, :cond_26

    .line 3098
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 3100
    :cond_26
    if-eqz v6, :cond_27

    .line 3101
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 3103
    :cond_27
    if-eqz v7, :cond_1a

    .line 3104
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_3

    .line 3106
    :catch_2
    move-exception v2

    .line 3108
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Load Full Size Contact Photo Close Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_3

    .line 3092
    :catchall_0
    move-exception v2

    move-object v7, v6

    move-object v6, v5

    move-object v5, v3

    move-object v3, v4

    .line 3094
    :goto_8
    if-eqz v3, :cond_28

    .line 3095
    :try_start_9
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 3097
    :cond_28
    if-eqz v5, :cond_29

    .line 3098
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 3100
    :cond_29
    if-eqz v6, :cond_2a

    .line 3101
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 3103
    :cond_2a
    if-eqz v7, :cond_2b

    .line 3104
    invoke-virtual {v7}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    .line 3109
    :cond_2b
    :goto_9
    throw v2

    .line 3106
    :catch_3
    move-exception v3

    .line 3108
    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Load Full Size Contact Photo Close Exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_9

    .line 3092
    :catchall_1
    move-exception v2

    move-object v6, v5

    move-object v5, v3

    move-object v3, v4

    goto :goto_8

    :catchall_2
    move-exception v2

    move-object v5, v3

    move-object v3, v4

    goto :goto_8

    :catchall_3
    move-exception v2

    move-object v3, v4

    goto :goto_8

    :catchall_4
    move-exception v2

    goto :goto_8

    :catchall_5
    move-exception v2

    move-object v7, v6

    move-object v6, v5

    move-object v5, v4

    goto :goto_8

    .line 3086
    :catch_4
    move-exception v2

    move-object/from16 v16, v4

    move-object v4, v3

    move-object/from16 v3, v16

    goto/16 :goto_5

    :catch_5
    move-exception v2

    move-object v6, v7

    move-object/from16 v16, v3

    move-object v3, v4

    move-object/from16 v4, v16

    goto/16 :goto_5

    :catch_6
    move-exception v2

    move-object v5, v6

    move-object v6, v7

    move-object/from16 v16, v4

    move-object v4, v3

    move-object/from16 v3, v16

    goto/16 :goto_5

    :catch_7
    move-exception v2

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    goto/16 :goto_5

    :cond_2c
    move-object v6, v5

    move-object v5, v3

    move-object v3, v4

    goto/16 :goto_7
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/util/ArrayList;II)Ljava/lang/String;
    .locals 15

    .prologue
    .line 1807
    sget-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 1808
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    .line 1813
    :cond_0
    const-string v4, ""

    .line 1817
    const/4 v1, 0x0

    .line 1819
    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 1820
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    .line 1822
    :cond_1
    const-string v2, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 1823
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1825
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 1826
    new-array v10, v9, [I

    .line 1828
    const v2, 0x7a120

    move/from16 v0, p3

    if-ge v0, v2, :cond_4

    .line 1829
    const/4 v2, 0x0

    .line 1833
    :goto_0
    packed-switch v2, :pswitch_data_0

    move-object v3, v4

    .line 2475
    :cond_2
    :goto_1
    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "applybatch sid = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2476
    sget-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2478
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2479
    sget-object v1, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2481
    :cond_3
    return-object v3

    .line 1831
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    .line 1836
    :pswitch_0
    const/4 v1, 0x1

    const-string v2, "applybatch BATCH_METHOD_ALL"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1837
    const/4 v1, 0x0

    move v5, v1

    :goto_2
    if-ge v5, v9, :cond_1a

    .line 1839
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/wssnps/b/y;

    .line 1840
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1841
    aput v6, v10, v5

    .line 1843
    iget-object v2, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1845
    iget-object v2, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    const-string v3, "vnd.sec.contact.phone"

    iput-object v3, v2, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    .line 1847
    :cond_5
    iget-object v2, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1849
    iget-object v2, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    const-string v3, "vnd.sec.contact.phone"

    iput-object v3, v2, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    .line 1851
    :cond_6
    iget-object v2, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1853
    const-string v2, "0"

    iput-object v2, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    .line 1856
    :cond_7
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v11, "account_type"

    iget-object v12, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v12, v12, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-virtual {v3, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v11, "account_name"

    iget-object v12, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v12, v12, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v3, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v11, "starred"

    iget-object v12, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-virtual {v3, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const/4 v11, 0x1

    invoke-virtual {v3, v11}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1863
    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_8

    .line 1865
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v11, "raw_contact_id"

    invoke-virtual {v2, v11, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v11, "mimetype"

    const-string v12, "vnd.android.cursor.item/name"

    invoke-virtual {v2, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data9"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data7"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data8"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1881
    :cond_8
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    iget-object v2, v1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_9

    .line 1883
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/nickname"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1881
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 1892
    :cond_9
    iget-object v2, v1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 1895
    iget-object v2, v1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lcom/wssnps/a/a;->b([B)[B

    move-result-object v2

    .line 1903
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v11, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "raw_contact_id"

    invoke-virtual {v11, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/photo"

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v11

    const-string v12, "data15"

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1911
    :cond_a
    const/4 v2, 0x0

    move v3, v2

    :goto_4
    iget-object v2, v1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_b

    .line 1913
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/note"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1911
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_4

    .line 1921
    :cond_b
    const/4 v2, 0x0

    move v3, v2

    :goto_5
    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_d

    .line 1923
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1925
    const-string v2, ""

    .line 1926
    iget-object v2, v1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    if-eqz v2, :cond_c

    iget-object v2, v1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    .line 1928
    iget-object v2, v1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1929
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_c

    const-string v11, " "

    invoke-virtual {v2, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v11

    if-eqz v11, :cond_c

    .line 1931
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1932
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1937
    :cond_c
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1921
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_5

    .line 1948
    :cond_d
    iget-object v2, v1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 1950
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v11, "raw_contact_id"

    invoke-virtual {v3, v11, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v11, "mimetype"

    const-string v12, "vnd.android.cursor.item/sip_address"

    invoke-virtual {v3, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v11, "data1"

    iget-object v12, v1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    invoke-virtual {v3, v11, v12}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const/4 v11, 0x1

    invoke-virtual {v3, v11}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1958
    :cond_e
    const/4 v2, 0x0

    move v3, v2

    :goto_6
    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_f

    .line 1960
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1958
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 1970
    :cond_f
    const/4 v2, 0x0

    move v3, v2

    :goto_7
    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_10

    .line 1972
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/im"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1970
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_7

    .line 1983
    :cond_10
    const/4 v2, 0x0

    move v3, v2

    :goto_8
    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_11

    .line 1985
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data7"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data8"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data9"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data10"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1983
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_8

    .line 2002
    :cond_11
    const/4 v2, 0x0

    move v3, v2

    :goto_9
    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_12

    .line 2004
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/organization"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2002
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_9

    .line 2015
    :cond_12
    const/4 v2, 0x0

    move v3, v2

    :goto_a
    iget-object v2, v1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_13

    .line 2017
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/website"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2015
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_a

    .line 2025
    :cond_13
    const/4 v2, 0x0

    move v3, v2

    :goto_b
    iget-object v2, v1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_14

    .line 2027
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2025
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_b

    .line 2035
    :cond_14
    const/4 v2, 0x0

    move v3, v2

    :goto_c
    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_15

    .line 2037
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2035
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_c

    .line 2048
    :cond_15
    const/4 v2, 0x0

    move v3, v2

    :goto_d
    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_16

    .line 2050
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2048
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_d

    .line 2061
    :cond_16
    const/4 v2, 0x0

    move v3, v2

    :goto_e
    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_17

    .line 2063
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2061
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_e

    .line 2074
    :cond_17
    const/4 v2, 0x0

    move v3, v2

    :goto_f
    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_18

    .line 2076
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2074
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_f

    .line 2088
    :cond_18
    const/4 v2, 0x0

    move v3, v2

    :goto_10
    iget-object v2, v1, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_19

    .line 2090
    sget-object v11, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/relation"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2088
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_10

    .line 1837
    :cond_19
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_2

    .line 2103
    :cond_1a
    const/4 v1, 0x1

    :try_start_0
    const-string v2, "applybatch all start"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2104
    const-string v1, "com.android.contacts"

    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v5

    .line 2105
    const-string v3, ""
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 2107
    const/4 v1, 0x0

    move v2, v1

    :goto_11
    if-ge v2, v9, :cond_1b

    .line 2109
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v1, v10, v2

    aget-object v1, v5, v1

    iget-object v1, v1, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v6, 0x1

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2107
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_11

    .line 2113
    :cond_1b
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2115
    const/4 v1, 0x0

    :goto_12
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1c

    .line 2117
    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://com.android.contacts/contacts/speeddial/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v6, v1, 0x1

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2115
    add-int/lit8 v1, v6, 0x1

    goto :goto_12

    .line 2122
    :cond_1c
    const/4 v1, 0x0

    :goto_13
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1d

    .line 2124
    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "key_number"

    add-int/lit8 v9, v1, 0x1

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v6, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v6, "speed_dial_data_id"

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, v5, v1

    iget-object v1, v1, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v10, 0x1

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v6, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2122
    add-int/lit8 v1, v9, 0x1

    goto :goto_13

    .line 2131
    :cond_1d
    const-string v1, "com.android.contacts"

    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8

    goto/16 :goto_1

    .line 2135
    :catch_0
    move-exception v1

    .line 2137
    :goto_14
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 2140
    :catch_1
    move-exception v1

    move-object v3, v4

    .line 2142
    :goto_15
    invoke-virtual {v1}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto/16 :goto_1

    .line 2145
    :catch_2
    move-exception v1

    move-object v3, v4

    .line 2147
    :goto_16
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDiskIOException;->printStackTrace()V

    goto/16 :goto_1

    .line 2150
    :catch_3
    move-exception v1

    move-object v3, v4

    .line 2152
    :goto_17
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 2159
    :pswitch_1
    const/4 v2, 0x1

    const-string v3, "applybatch BATCH_METHOD_PART"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2160
    const/4 v2, 0x0

    move v5, v1

    move-object v3, v4

    move v6, v2

    :goto_18
    if-ge v6, v9, :cond_2

    .line 2162
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/wssnps/b/y;

    .line 2163
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 2164
    aput v11, v10, v5

    .line 2166
    iget-object v2, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 2168
    iget-object v2, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    const-string v4, "vnd.sec.contact.phone"

    iput-object v4, v2, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    .line 2170
    :cond_1e
    iget-object v2, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 2172
    iget-object v2, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    const-string v4, "vnd.sec.contact.phone"

    iput-object v4, v2, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    .line 2174
    :cond_1f
    iget-object v2, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 2176
    const-string v2, "0"

    iput-object v2, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    .line 2179
    :cond_20
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "account_type"

    iget-object v13, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v13, v13, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "account_name"

    iget-object v13, v1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v13, v13, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "starred"

    iget-object v13, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2186
    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_21

    .line 2188
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-virtual {v2, v12, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/name"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data9"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data7"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data8"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2204
    :cond_21
    const/4 v2, 0x0

    move v4, v2

    :goto_19
    iget-object v2, v1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_22

    .line 2206
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/nickname"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2204
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_19

    .line 2214
    :cond_22
    const/4 v2, 0x0

    move v4, v2

    :goto_1a
    iget-object v2, v1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_23

    .line 2216
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/note"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2214
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1a

    .line 2224
    :cond_23
    const/4 v2, 0x0

    move v4, v2

    :goto_1b
    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_25

    .line 2226
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 2228
    const-string v2, ""

    .line 2229
    iget-object v2, v1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2230
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_24

    const-string v12, " "

    invoke-virtual {v2, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_24

    .line 2232
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2233
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2237
    :cond_24
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2224
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_1b

    .line 2247
    :cond_25
    iget-object v2, v1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    if-eqz v2, :cond_26

    .line 2249
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "raw_contact_id"

    invoke-virtual {v4, v12, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/sip_address"

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "data1"

    iget-object v13, v1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2257
    :cond_26
    const/4 v2, 0x0

    move v4, v2

    :goto_1c
    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_27

    .line 2259
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2257
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1c

    .line 2269
    :cond_27
    const/4 v2, 0x0

    move v4, v2

    :goto_1d
    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_28

    .line 2271
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/im"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2269
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1d

    .line 2282
    :cond_28
    const/4 v2, 0x0

    move v4, v2

    :goto_1e
    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_29

    .line 2284
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data7"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data8"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data9"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data10"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2282
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_1e

    .line 2301
    :cond_29
    const/4 v2, 0x0

    move v4, v2

    :goto_1f
    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_2a

    .line 2303
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/organization"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2301
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1f

    .line 2314
    :cond_2a
    const/4 v2, 0x0

    move v4, v2

    :goto_20
    iget-object v2, v1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_2b

    .line 2316
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/website"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2314
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_20

    .line 2324
    :cond_2b
    const/4 v2, 0x0

    move v4, v2

    :goto_21
    iget-object v2, v1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_2c

    .line 2326
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2324
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_21

    .line 2335
    :cond_2c
    iget-object v2, v1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    if-eqz v2, :cond_2d

    .line 2338
    iget-object v2, v1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lcom/wssnps/a/a;->b([B)[B

    move-result-object v2

    .line 2346
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v12, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v12}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "raw_contact_id"

    invoke-virtual {v12, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/photo"

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data15"

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2354
    :cond_2d
    const/4 v2, 0x0

    move v4, v2

    :goto_22
    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_2e

    .line 2356
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2354
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_22

    .line 2367
    :cond_2e
    const/4 v2, 0x0

    move v4, v2

    :goto_23
    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_2f

    .line 2369
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2367
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_23

    .line 2380
    :cond_2f
    const/4 v2, 0x0

    move v4, v2

    :goto_24
    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_30

    .line 2382
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2380
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_24

    .line 2393
    :cond_30
    const/4 v2, 0x0

    move v4, v2

    :goto_25
    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_31

    .line 2395
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-virtual {v2, v13, v11}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2393
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_25

    .line 2407
    :cond_31
    add-int/lit8 v2, v5, 0x1

    .line 2408
    if-eqz v6, :cond_32

    add-int/lit8 v1, v6, 0x1

    rem-int/lit8 v1, v1, 0x5

    if-eqz v1, :cond_33

    :cond_32
    add-int/lit8 v1, v9, -0x1

    if-ne v6, v1, :cond_38

    .line 2412
    :cond_33
    const/4 v1, 0x1

    :try_start_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "applybatch part start "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v6, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2413
    const-string v1, "com.android.contacts"

    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v5

    .line 2415
    const/4 v1, 0x0

    move v4, v1

    :goto_26
    if-ge v4, v2, :cond_34

    .line 2417
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    aget v1, v10, v4

    aget-object v1, v5, v1

    iget-object v1, v1, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v12, 0x1

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2418
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, ","

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2415
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_26

    .line 2421
    :cond_34
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 2423
    const/4 v1, 0x0

    :goto_27
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_35

    .line 2425
    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "content://com.android.contacts/contacts/speeddial/"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v11, v1, 0x1

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2423
    add-int/lit8 v1, v11, 0x1

    goto :goto_27

    .line 2430
    :cond_35
    const/4 v1, 0x0

    :goto_28
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_36

    .line 2432
    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v11, "key_number"

    add-int/lit8 v12, v1, 0x1

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v11, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v11, "speed_dial_data_id"

    invoke-interface {v8, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, v5, v1

    iget-object v1, v1, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v13, 0x1

    invoke-interface {v1, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v11, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2430
    add-int/lit8 v1, v12, 0x1

    goto :goto_28

    .line 2439
    :cond_36
    const-string v1, "com.android.contacts"

    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 2440
    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 2441
    sget-object v1, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2444
    :cond_37
    const/4 v1, 0x0

    .line 2445
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_7

    move-object v2, v3

    .line 2160
    :goto_29
    add-int/lit8 v3, v6, 0x1

    move v5, v1

    move v6, v3

    move-object v3, v2

    goto/16 :goto_18

    .line 2447
    :catch_4
    move-exception v1

    .line 2449
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 2452
    :catch_5
    move-exception v1

    .line 2454
    invoke-virtual {v1}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto/16 :goto_1

    .line 2457
    :catch_6
    move-exception v1

    .line 2459
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDiskIOException;->printStackTrace()V

    goto/16 :goto_1

    .line 2462
    :catch_7
    move-exception v1

    .line 2464
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 2150
    :catch_8
    move-exception v1

    goto/16 :goto_17

    .line 2145
    :catch_9
    move-exception v1

    goto/16 :goto_16

    .line 2140
    :catch_a
    move-exception v1

    goto/16 :goto_15

    .line 2135
    :catch_b
    move-exception v1

    move-object v3, v4

    goto/16 :goto_14

    :cond_38
    move v1, v2

    move-object v2, v3

    goto :goto_29

    .line 1833
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/lang/String;
    .locals 17

    .prologue
    .line 3470
    .line 3474
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 3476
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 3477
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 3478
    const-string v5, ""

    .line 3481
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 3482
    new-array v9, v8, [I

    .line 3483
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 3484
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    .line 3486
    :cond_0
    const-string v2, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 3487
    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 3488
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    .line 3489
    :cond_1
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3490
    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/wssnps/b/y;->i(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 3492
    :cond_2
    const/4 v1, 0x0

    move v3, v1

    :goto_0
    array-length v1, v7

    if-ge v3, v1, :cond_16

    .line 3494
    aget-object v1, v7, v3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 3496
    aget-object v1, v7, v3

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 3497
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/wssnps/b/y;

    .line 3498
    aput v11, v9, v3

    .line 3500
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "raw_contact_id=?"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3505
    iget-object v2, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3507
    const-string v2, "0"

    iput-object v2, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    .line 3510
    :cond_3
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 3511
    const-string v4, "starred"

    iget-object v12, v1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-virtual {v2, v4, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3513
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "_id = "

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v12, v9, v3

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 3514
    sget-object v12, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v12, v4}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;Ljava/lang/String;)I

    .line 3516
    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_4

    .line 3518
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/name"

    invoke-virtual {v2, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data9"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data7"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data8"

    iget-object v2, v1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    const/4 v14, 0x0

    invoke-virtual {v2, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3535
    :cond_4
    const/4 v2, 0x0

    move v4, v2

    :goto_1
    iget-object v2, v1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_5

    .line 3537
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/nickname"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3535
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 3547
    :cond_5
    iget-object v2, v1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 3550
    iget-object v2, v1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lcom/wssnps/a/a;->b([B)[B

    move-result-object v2

    .line 3558
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v12, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v12}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/photo"

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    const-string v13, "data15"

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3577
    :goto_2
    const/4 v2, 0x0

    move v4, v2

    :goto_3
    iget-object v2, v1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_7

    .line 3579
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/note"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3577
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    .line 3568
    :cond_6
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/photo"

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "data15"

    const/4 v13, 0x0

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3588
    :cond_7
    const/4 v2, 0x0

    move v4, v2

    :goto_4
    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_9

    .line 3590
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 3592
    const-string v2, ""

    .line 3594
    iget-object v2, v1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    if-eqz v2, :cond_8

    iget-object v2, v1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    .line 3596
    iget-object v2, v1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 3597
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_8

    const-string v12, " "

    invoke-virtual {v2, v12}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v12

    if-eqz v12, :cond_8

    .line 3599
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3600
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3605
    :cond_8
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3588
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_4

    .line 3616
    :cond_9
    iget-object v2, v1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 3618
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "mimetype"

    const-string v13, "vnd.android.cursor.item/sip_address"

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v12, "data1"

    iget-object v13, v1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const/4 v12, 0x1

    invoke-virtual {v4, v12}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3627
    :cond_a
    const/4 v2, 0x0

    move v4, v2

    :goto_5
    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_b

    .line 3629
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3627
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    .line 3640
    :cond_b
    const/4 v2, 0x0

    move v4, v2

    :goto_6
    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_c

    .line 3642
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/im"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3640
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_6

    .line 3654
    :cond_c
    const/4 v2, 0x0

    move v4, v2

    :goto_7
    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_d

    .line 3656
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data5"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data6"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data7"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data8"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data9"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data10"

    iget-object v2, v1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3654
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_7

    .line 3674
    :cond_d
    const/4 v2, 0x0

    move v4, v2

    :goto_8
    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_e

    .line 3676
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/organization"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data4"

    iget-object v2, v1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3674
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_8

    .line 3688
    :cond_e
    const/4 v2, 0x0

    move v4, v2

    :goto_9
    iget-object v2, v1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_f

    .line 3690
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/website"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3688
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_9

    .line 3699
    :cond_f
    const/4 v2, 0x0

    move v4, v2

    :goto_a
    iget-object v2, v1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_10

    .line 3701
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3699
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_a

    .line 3710
    :cond_10
    const/4 v2, 0x0

    move v4, v2

    :goto_b
    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_11

    .line 3712
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3710
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_b

    .line 3724
    :cond_11
    const/4 v2, 0x0

    move v4, v2

    :goto_c
    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_12

    .line 3726
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3724
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_c

    .line 3738
    :cond_12
    const/4 v2, 0x0

    move v4, v2

    :goto_d
    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_13

    .line 3740
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3738
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_d

    .line 3752
    :cond_13
    const/4 v2, 0x0

    move v4, v2

    :goto_e
    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_14

    .line 3754
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data14"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->k:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data15"

    iget-object v2, v1, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3752
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_e

    .line 3767
    :cond_14
    const/4 v2, 0x0

    move v4, v2

    :goto_f
    iget-object v2, v1, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_15

    .line 3769
    sget-object v12, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "raw_contact_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v13, "mimetype"

    const-string v14, "vnd.android.cursor.item/relation"

    invoke-virtual {v2, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data1"

    iget-object v2, v1, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data2"

    iget-object v2, v1, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    const-string v14, "data3"

    iget-object v2, v1, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/wssnps/b/z;

    iget-object v2, v2, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v13, v14, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3767
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_f

    .line 3492
    :cond_15
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_0

    .line 3784
    :cond_16
    const/4 v1, 0x1

    :try_start_0
    const-string v2, "updateContactBatchstart"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3785
    const-string v1, "com.android.contacts"

    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v3

    .line 3786
    const-string v2, ""
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3787
    const/4 v1, 0x0

    :goto_10
    if-ge v1, v8, :cond_17

    .line 3789
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v9, v1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3790
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3787
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 3793
    :cond_17
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 3795
    const/4 v1, 0x0

    :goto_11
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_18

    .line 3796
    sget-object v4, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "content://com.android.contacts/contacts/speeddial/"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v7, v1, 0x1

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3795
    add-int/lit8 v1, v7, 0x1

    goto :goto_11

    .line 3799
    :cond_18
    const/4 v1, 0x0

    :goto_12
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_19

    .line 3800
    sget-object v4, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v7, "key_number"

    add-int/lit8 v8, v1, 0x1

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v7, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v7, "speed_dial_data_id"

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, v3, v1

    iget-object v1, v1, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v9, 0x1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v7, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3799
    add-int/lit8 v1, v8, 0x1

    goto :goto_12

    .line 3806
    :cond_19
    const-string v1, "com.android.contacts"

    sget-object v3, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1a
    move-object v1, v2

    .line 3818
    :goto_13
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateContactBatchstart sid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3819
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 3821
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 3822
    sget-object v2, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 3824
    :cond_1b
    return-object v1

    .line 3810
    :catch_0
    move-exception v1

    move-object v2, v1

    move-object v1, v5

    .line 3812
    :goto_14
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_13

    .line 3814
    :catch_1
    move-exception v1

    move-object v2, v1

    move-object v1, v5

    .line 3816
    :goto_15
    invoke-virtual {v2}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_13

    .line 3814
    :catch_2
    move-exception v1

    move-object/from16 v16, v1

    move-object v1, v2

    move-object/from16 v2, v16

    goto :goto_15

    .line 3810
    :catch_3
    move-exception v1

    move-object/from16 v16, v1

    move-object v1, v2

    move-object/from16 v2, v16

    goto :goto_14
.end method

.method public static a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 1150
    .line 1151
    invoke-static {p0}, Lcom/wssnps/a/s;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1152
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1154
    if-eqz v2, :cond_0

    .line 1156
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1160
    new-instance v4, Lcom/wssnps/b/y;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/ac;

    invoke-direct {v4, v0}, Lcom/wssnps/b/y;-><init>(Lcom/wssnps/a/ac;)V

    .line 1161
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1156
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1165
    :cond_0
    return-object v3
.end method

.method private static a(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 4199
    .line 4202
    iget v6, p1, Lcom/wssnps/b/y;->b:I

    .line 4204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/name"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4206
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4207
    if-nez v1, :cond_1

    .line 4261
    :cond_0
    :goto_0
    return-void

    .line 4212
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 4213
    if-lez v0, :cond_4

    .line 4215
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4217
    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 4219
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v3, "_id=?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "_id"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data5"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data4"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data6"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data9"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data7"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data8"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4259
    :cond_2
    :goto_1
    if-eqz v1, :cond_0

    .line 4260
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 4234
    :cond_3
    sget-object v0, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "_id=?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "_id"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 4242
    :cond_4
    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 4244
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v3, "raw_contact_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v3, "mimetype"

    const-string v4, "vnd.android.cursor.item/name"

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data5"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data4"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data6"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data9"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data7"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data8"

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/ContentResolver;)Z
    .locals 3

    .prologue
    .line 3845
    const-string v0, "vnd.sec.contact.phone"

    .line 3846
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleted=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3849
    invoke-static {p0, v0}, Lcom/wssnps/b/y;->d(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 3854
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    .line 3856
    if-nez p1, :cond_0

    .line 3857
    const/4 v0, 0x0

    .line 3860
    :goto_0
    return v0

    .line 3859
    :cond_0
    invoke-static {p0, p1, v0, p2}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;Ljava/lang/String;)I

    .line 3860
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/content/ContentResolver;I)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2721
    const/4 v6, 0x0

    .line 2723
    const-string v0, ""

    .line 2724
    sget-object v0, Lcom/wssnps/b/aa;->h:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2726
    const-string v3, "contact_id is NOT NULL AND deleted=0 AND account_type == \'vnd.sec.contact.phone\'"

    .line 2736
    :goto_0
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2737
    if-eqz v1, :cond_2

    .line 2739
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 2740
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2743
    :goto_1
    return v0

    .line 2730
    :cond_0
    const-string v0, "CHM"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2731
    const-string v3, "contact_id is NOT NULL AND deleted=0 AND ((account_type = \'vnd.sec.contact.phone\') OR (account_type = \'com.google\') OR (account_type = \'com.android.exchange\'))"

    goto :goto_0

    .line 2733
    :cond_1
    const-string v3, "contact_id is NOT NULL AND deleted=0 AND ((account_type = \'vnd.sec.contact.phone\') OR (account_type = \'com.google\') OR (account_type = \'com.android.exchange\') OR (account_type = \'com.osp.app.signin\'))"

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1
.end method

.method public static b(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v8, 0x1

    .line 2647
    .line 2648
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 2653
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2654
    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2656
    if-eqz v1, :cond_0

    .line 2659
    const-string v0, "system_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2660
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2662
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2665
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2716
    :cond_0
    :goto_0
    return p2

    .line 2672
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 2676
    invoke-static {p0, p1, v6}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I

    move-result v0

    .line 2677
    sget-object v2, Lcom/wssnps/b/y;->D:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2679
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move p2, v0

    .line 2680
    goto :goto_0

    .line 2683
    :cond_2
    iget-object v0, p1, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2685
    const-string v0, "title"

    iget-object v2, p1, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2686
    const-string v0, "group_visible"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2687
    const-string v0, "_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2691
    :cond_3
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v7, v0}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 2693
    if-nez v0, :cond_4

    .line 2695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error add contact. luid: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2696
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move p2, v6

    .line 2697
    goto :goto_0

    .line 2700
    :cond_4
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2704
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result p2

    .line 2712
    if-eqz v1, :cond_0

    .line 2713
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2706
    :catch_0
    move-exception v0

    .line 2708
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2712
    if-eqz v1, :cond_0

    .line 2713
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 2712
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    .line 2713
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method public static b(Ljava/lang/String;)Lcom/wssnps/b/y;
    .locals 2

    .prologue
    .line 1171
    invoke-static {p0}, Lcom/wssnps/a/s;->b(Ljava/lang/String;)Lcom/wssnps/a/ac;

    move-result-object v0

    .line 1172
    new-instance v1, Lcom/wssnps/b/y;

    invoke-direct {v1, v0}, Lcom/wssnps/b/y;-><init>(Lcom/wssnps/a/ac;)V

    .line 1174
    return-object v1
.end method

.method public static b(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2502
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2503
    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    .line 2507
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2508
    if-eqz v0, :cond_2

    .line 2510
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 2512
    const-string v2, "0\n"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2514
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 2516
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2520
    :cond_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2521
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2523
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2526
    :cond_2
    const-string v0, "\n"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2528
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 4265
    .line 4269
    iget v7, p1, Lcom/wssnps/b/y;->b:I

    .line 4271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/nickname"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4273
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4274
    if-nez v2, :cond_1

    .line 4335
    :cond_0
    :goto_0
    return-void

    .line 4279
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 4281
    iget-object v0, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    .line 4283
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4289
    :goto_1
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 4291
    iget-object v3, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 4293
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4297
    add-int/lit8 v0, v1, 0x1

    .line 4305
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4333
    :cond_2
    if-eqz v2, :cond_0

    .line 4334
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4301
    :cond_3
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 4310
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 4312
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4314
    if-ge v1, v3, :cond_5

    .line 4316
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    const-string v9, "_id"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4321
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 4312
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4325
    :cond_5
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v8, "vnd.android.cursor.item/nickname"

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method public static b(Landroid/content/ContentResolver;)Z
    .locals 3

    .prologue
    .line 3872
    const-string v0, "vnd.sec.contact.phone"

    .line 3873
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleted=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3876
    invoke-static {p0, v0}, Lcom/wssnps/b/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/ContentResolver;Landroid/content/ContentValues;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 3905
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 3908
    if-nez p1, :cond_0

    .line 3909
    const/4 v0, 0x0

    .line 3912
    :goto_0
    return v0

    .line 3911
    :cond_0
    invoke-static {p0, p1, v0, p2}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;Ljava/lang/String;)I

    .line 3912
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3137
    iput p2, p1, Lcom/wssnps/b/y;->b:I

    .line 3138
    const/16 v0, 0x64

    new-array v6, v0, [Landroid/content/ContentValues;

    .line 3142
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/y;->K:Landroid/content/Context;

    .line 3143
    sget-object v0, Lcom/wssnps/b/y;->K:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/y;->L:Landroid/accounts/AccountManager;

    .line 3144
    sget-object v0, Lcom/wssnps/b/y;->L:Landroid/accounts/AccountManager;

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    .line 3146
    iget-object v3, p1, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v3, v3, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    array-length v0, v0

    if-lt v0, v1, :cond_0

    .line 3149
    const/16 v2, -0xa

    .line 3285
    :goto_0
    return v2

    .line 3151
    :cond_0
    invoke-virtual {p1, p0}, Lcom/wssnps/b/y;->c(Landroid/content/ContentResolver;)Landroid/content/ContentValues;

    move-result-object v0

    sget-object v3, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v3}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 3153
    if-nez v0, :cond_1

    .line 3155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error add contact. luid: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 3159
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3160
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 3161
    iput v5, p1, Lcom/wssnps/b/y;->b:I

    .line 3165
    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_19

    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_19

    .line 3168
    iget-object v0, p1, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v3, "vnd.android.cursor.item/name"

    invoke-virtual {p1, v0, v3}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v2

    move v0, v1

    .line 3173
    :goto_1
    iget-object v1, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v0

    .line 3175
    :goto_2
    iget-object v0, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 3177
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/nickname"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3175
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_2

    :cond_2
    move v0, v3

    .line 3183
    :cond_3
    iget-object v1, p1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3185
    add-int/lit8 v1, v0, 0x1

    iget-object v3, p1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    const-string v4, "vnd.android.cursor.item/photo"

    invoke-virtual {p1, v3, v4}, Lcom/wssnps/b/y;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v3

    aput-object v3, v6, v0

    move v0, v1

    .line 3190
    :cond_4
    iget-object v1, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    iget-object v1, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v0

    .line 3192
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 3194
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/note"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3192
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_3

    :cond_5
    move v0, v3

    .line 3200
    :cond_6
    iget-object v1, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    if-eqz v1, :cond_8

    iget-object v1, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v0

    .line 3202
    :goto_4
    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 3204
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3202
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_4

    :cond_7
    move v0, v3

    .line 3210
    :cond_8
    iget-object v1, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    if-eqz v1, :cond_a

    iget-object v1, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_a

    move v1, v2

    move v3, v0

    .line 3212
    :goto_5
    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 3214
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/email_v2"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3212
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_5

    :cond_9
    move v0, v3

    .line 3219
    :cond_a
    iget-object v1, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    if-eqz v1, :cond_c

    iget-object v1, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_c

    move v1, v2

    move v3, v0

    .line 3221
    :goto_6
    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 3223
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/im"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3221
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_6

    :cond_b
    move v0, v3

    .line 3229
    :cond_c
    iget-object v1, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    if-eqz v1, :cond_e

    iget-object v1, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_e

    move v1, v2

    move v3, v0

    .line 3231
    :goto_7
    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 3233
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3231
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_7

    :cond_d
    move v0, v3

    .line 3239
    :cond_e
    iget-object v1, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    if-eqz v1, :cond_10

    iget-object v1, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_10

    move v1, v2

    move v3, v0

    .line 3241
    :goto_8
    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 3243
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/organization"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3241
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_8

    :cond_f
    move v0, v3

    .line 3249
    :cond_10
    iget-object v1, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    if-eqz v1, :cond_12

    iget-object v1, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_12

    move v1, v2

    move v3, v0

    .line 3251
    :goto_9
    iget-object v0, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_11

    .line 3253
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/website"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3251
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_9

    :cond_11
    move v0, v3

    .line 3258
    :cond_12
    iget-object v1, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    if-eqz v1, :cond_14

    iget-object v1, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_14

    move v1, v2

    move v3, v0

    .line 3260
    :goto_a
    iget-object v0, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_13

    .line 3262
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    const-string v7, "vnd.android.cursor.item/group_membership"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3260
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_a

    :cond_13
    move v0, v3

    .line 3267
    :cond_14
    iget-object v1, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    if-eqz v1, :cond_16

    iget-object v1, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_16

    move v1, v2

    move v3, v0

    .line 3269
    :goto_b
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_15

    .line 3271
    add-int/lit8 v4, v3, 0x1

    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v7, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {p1, v0, v7}, Lcom/wssnps/b/y;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v6, v3

    .line 3269
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v3, v4

    goto :goto_b

    :cond_15
    move v0, v3

    .line 3275
    :cond_16
    new-array v1, v0, [Landroid/content/ContentValues;

    .line 3276
    :goto_c
    if-ge v2, v0, :cond_17

    .line 3278
    aget-object v3, v6, v2

    aput-object v3, v1, v2

    .line 3276
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 3281
    :cond_17
    if-lez v0, :cond_18

    .line 3283
    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v1, v0}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;[Landroid/content/ContentValues;Landroid/net/Uri;)I

    :cond_18
    move v2, v5

    .line 3285
    goto/16 :goto_0

    :cond_19
    move v0, v2

    goto/16 :goto_1
.end method

.method public static c(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 2749
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v7

    .line 2751
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2754
    sget-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_4

    .line 2756
    const-string v3, "deleted = 0 AND (account_type = \'vnd.sec.contact.sim\')"

    .line 2773
    :goto_0
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2774
    if-eqz v0, :cond_3

    .line 2776
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 2777
    const-string v2, "0\n"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2778
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2782
    :cond_0
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2783
    sget-object v1, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    if-eqz v1, :cond_1

    .line 2784
    sget-object v1, Lcom/wssnps/b;->k:Ljava/lang/StringBuffer;

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2786
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2788
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2790
    :cond_3
    const-string v0, "\n"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2791
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2758
    :cond_4
    sget-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 2760
    const-string v3, "deleted = 0 AND (account_type = \'vnd.sec.contact.sim2\')"

    goto :goto_0

    .line 2762
    :cond_5
    sget-object v0, Lcom/wssnps/b/aa;->h:Lcom/wssnps/b/aa;

    invoke-virtual {v0}, Lcom/wssnps/b/aa;->a()I

    move-result v0

    if-ne p1, v0, :cond_6

    .line 2764
    const-string v3, "contact_id is NOT NULL AND deleted=0 AND account_type == \'vnd.sec.contact.phone\'"

    goto :goto_0

    .line 2768
    :cond_6
    const-string v0, "CHM"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2769
    const-string v3, "contact_id is NOT NULL AND deleted = 0 AND ((account_type = \'vnd.sec.contact.phone\') OR (account_type = \'com.google\') OR (account_type = \'com.android.exchange\'))"

    goto :goto_0

    .line 2771
    :cond_7
    const-string v3, "contact_id is NOT NULL AND deleted = 0 AND ((account_type = \'vnd.sec.contact.phone\') OR (account_type = \'com.google\') OR (account_type = \'com.android.exchange\') OR (account_type = \'com.osp.app.signin\'))"

    goto :goto_0
.end method

.method public static c(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2533
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2534
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    .line 2537
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2538
    if-eqz v0, :cond_2

    .line 2540
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 2542
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2546
    :cond_0
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2547
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2549
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2552
    :cond_2
    return-object v6
.end method

.method private static c(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 4339
    .line 4343
    iget v7, p1, Lcom/wssnps/b/y;->b:I

    .line 4345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/note"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4347
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4348
    if-nez v2, :cond_1

    .line 4409
    :cond_0
    :goto_0
    return-void

    .line 4353
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 4355
    iget-object v0, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    .line 4357
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4363
    :goto_1
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 4365
    iget-object v3, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 4367
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4371
    add-int/lit8 v0, v1, 0x1

    .line 4379
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4407
    :cond_2
    if-eqz v2, :cond_0

    .line 4408
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4375
    :cond_3
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 4384
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 4386
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4388
    if-ge v1, v3, :cond_5

    .line 4390
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    const-string v9, "_id"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4395
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 4386
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4399
    :cond_5
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v8, "vnd.android.cursor.item/note"

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method public static d(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I
    .locals 4

    .prologue
    .line 3290
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3291
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3293
    iget-object v2, p1, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3295
    const-string v2, "title"

    iget-object v3, p1, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3296
    const-string v2, "group_visible"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3299
    :cond_0
    sget-object v2, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v2, v1}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    .line 3301
    return v0
.end method

.method private static d(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)Ljava/util/List;
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 4413
    .line 4417
    iget v8, p1, Lcom/wssnps/b/y;->b:I

    .line 4418
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 4419
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4420
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 4421
    if-nez v3, :cond_0

    move-object v0, v7

    .line 4541
    :goto_0
    return-object v0

    .line 4426
    :cond_0
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    .line 4428
    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v4, v0, :cond_5

    .line 4430
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4436
    :goto_1
    const-string v0, "_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 4437
    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 4439
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4441
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v2, v6

    .line 4443
    :goto_2
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 4447
    :try_start_0
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4453
    :goto_3
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4443
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 4449
    :catch_0
    move-exception v0

    .line 4451
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 4458
    :cond_1
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v11, [Ljava/lang/String;

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4464
    add-int/lit8 v0, v1, 0x1

    .line 4472
    :goto_4
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_9

    .line 4539
    :cond_2
    if-eqz v3, :cond_3

    .line 4540
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v7

    .line 4541
    goto/16 :goto_0

    .line 4468
    :cond_4
    sget-object v0, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v5, "_id=?"

    new-array v8, v11, [Ljava/lang/String;

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v6

    invoke-virtual {v2, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_4

    .line 4477
    :cond_5
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 4479
    :goto_5
    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4481
    if-ge v1, v4, :cond_7

    .line 4483
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4485
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 4489
    :try_start_1
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 4495
    :goto_6
    const-string v0, "_id"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4499
    :cond_6
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v9, v11, [Ljava/lang/String;

    const-string v10, "_id"

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-virtual {v0, v5, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v9, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v9, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v9, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v9, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v9, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v5, v9, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4505
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 4479
    :goto_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_5

    .line 4491
    :catch_1
    move-exception v0

    .line 4493
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 4509
    :cond_7
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 4511
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    move v2, v6

    .line 4513
    :goto_8
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 4517
    :try_start_2
    iget-object v0, p1, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 4523
    :goto_9
    sget-object v0, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4513
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 4519
    :catch_2
    move-exception v0

    .line 4521
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_9

    .line 4528
    :cond_8
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v5, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v9, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v0, v5, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v9, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v9, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v9, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v9, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v9, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v5, v9, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    :cond_9
    move v1, v0

    goto/16 :goto_1
.end method

.method public static d(Landroid/content/ContentResolver;I)Z
    .locals 3

    .prologue
    .line 3829
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 3830
    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 3831
    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3833
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 3835
    const/4 v0, 0x1

    .line 3839
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 3865
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    .line 3866
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3867
    const/4 v0, 0x1

    return v0
.end method

.method public static e(Landroid/content/ContentResolver;Lcom/wssnps/b/y;I)I
    .locals 10

    .prologue
    const/4 v6, -0x1

    const/4 v0, 0x0

    .line 3306
    iput p2, p1, Lcom/wssnps/b/y;->b:I

    .line 3308
    sget-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 3309
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    .line 3312
    :cond_0
    const-string v1, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3313
    sget-object v1, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 3314
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    .line 3315
    :cond_1
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3316
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->m(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3318
    :cond_2
    iget-object v1, p1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3320
    const-string v1, "0"

    iput-object v1, p1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    .line 3323
    :cond_3
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3324
    const-string v3, "starred"

    iget-object v4, p1, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3326
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3327
    sget-object v4, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v1, v4, v3}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;Ljava/lang/String;)I

    .line 3331
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->a(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3336
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->b(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3340
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->k(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3344
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->c(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3348
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->d(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)Ljava/util/List;

    move-result-object v3

    .line 3352
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->l(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3356
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->e(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3360
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->f(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3364
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->g(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3368
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->h(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3372
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->i(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3376
    invoke-static {p0, p1}, Lcom/wssnps/b/y;->j(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V

    .line 3392
    :try_start_0
    const-string v1, "com.android.contacts"

    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v4

    .line 3394
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 3396
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 3397
    if-ne v5, v6, :cond_5

    .line 3399
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    .line 3400
    :goto_0
    if-ge v1, v4, :cond_4

    .line 3401
    sget-object v5, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "content://com.android.contacts/contacts/speeddial/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v1, 0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3400
    add-int/lit8 v1, v7, 0x1

    goto :goto_0

    .line 3404
    :cond_4
    :goto_1
    if-ge v0, v4, :cond_9

    .line 3407
    sget-object v1, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v6, "key_number"

    add-int/lit8 v7, v0, 0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "speed_dial_data_id"

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3404
    add-int/lit8 v0, v7, 0x1

    goto :goto_1

    .line 3416
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    move v1, v0

    .line 3417
    :goto_2
    if-ge v1, v5, :cond_6

    .line 3418
    sget-object v7, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "content://com.android.contacts/contacts/speeddial/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v1, 0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3417
    add-int/lit8 v1, v9, 0x1

    goto :goto_2

    .line 3421
    :cond_6
    add-int/lit8 v1, v5, 0x1

    :goto_3
    if-ge v1, v6, :cond_7

    .line 3422
    sget-object v7, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "content://com.android.contacts/contacts/speeddial/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v1, 0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3421
    add-int/lit8 v1, v9, 0x1

    goto :goto_3

    .line 3425
    :cond_7
    :goto_4
    if-ge v0, v5, :cond_8

    .line 3428
    sget-object v1, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    const-string v8, "key_number"

    add-int/lit8 v9, v0, 0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v7, "speed_dial_data_id"

    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3425
    add-int/lit8 v0, v9, 0x1

    goto :goto_4

    .line 3434
    :cond_8
    add-int/lit8 v0, v5, 0x1

    :goto_5
    if-ge v0, v6, :cond_9

    .line 3437
    sget-object v1, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v7, "key_number"

    add-int/lit8 v8, v0, 0x1

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v7, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v7, "speed_dial_data_id"

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v0, v4, v0

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v9, 0x1

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v7, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3434
    add-int/lit8 v0, v8, 0x1

    goto :goto_5

    .line 3444
    :cond_9
    const-string v0, "com.android.contacts"

    sget-object v1, Lcom/wssnps/b/y;->C:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 3464
    :cond_a
    :goto_6
    sget-object v0, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3465
    return p2

    .line 3447
    :catch_0
    move-exception v0

    .line 3449
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_6

    .line 3451
    :catch_1
    move-exception v0

    .line 3453
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_6

    .line 3455
    :catch_2
    move-exception v0

    .line 3457
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDiskIOException;->printStackTrace()V

    goto :goto_6

    .line 3459
    :catch_3
    move-exception v0

    .line 3461
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6
.end method

.method private static e(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 4546
    .line 4550
    iget v7, p1, Lcom/wssnps/b/y;->b:I

    .line 4552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4554
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4555
    if-nez v2, :cond_1

    .line 4622
    :cond_0
    :goto_0
    return-void

    .line 4560
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 4562
    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    .line 4564
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4570
    :goto_1
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 4572
    iget-object v3, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 4574
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4580
    add-int/lit8 v0, v1, 0x1

    .line 4588
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4620
    :cond_2
    if-eqz v2, :cond_0

    .line 4621
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4584
    :cond_3
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 4593
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 4595
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4597
    if-ge v1, v3, :cond_5

    .line 4599
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    const-string v9, "_id"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4606
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 4595
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4610
    :cond_5
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v8, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method public static e(Landroid/content/ContentResolver;I)Z
    .locals 3

    .prologue
    .line 3927
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 3928
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 3929
    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3931
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 3933
    const/4 v0, 0x1

    .line 3937
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 3881
    const-string v0, "vnd.sec.contact.phone"

    .line 3882
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id in("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "deleted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "account_type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3886
    invoke-static {p0, v0}, Lcom/wssnps/b/y;->g(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static f(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3891
    const/4 v6, 0x0

    .line 3893
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    .line 3894
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 3895
    if-eqz v1, :cond_0

    .line 3897
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 3898
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 3900
    :goto_0
    return v0

    :cond_0
    move v0, v6

    goto :goto_0
.end method

.method private static f(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 4626
    .line 4630
    iget v7, p1, Lcom/wssnps/b/y;->b:I

    .line 4632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/im"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4634
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4635
    if-nez v2, :cond_1

    .line 4705
    :cond_0
    :goto_0
    return-void

    .line 4640
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 4642
    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    .line 4644
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4650
    :goto_1
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 4652
    iget-object v3, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 4654
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data5"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data6"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4661
    add-int/lit8 v0, v1, 0x1

    .line 4669
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4703
    :cond_2
    if-eqz v2, :cond_0

    .line 4704
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 4665
    :cond_3
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 4674
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 4676
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4678
    if-ge v1, v3, :cond_5

    .line 4680
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    const-string v9, "_id"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data5"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data6"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4688
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 4676
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4692
    :cond_5
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v8, "vnd.android.cursor.item/im"

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data5"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data6"

    iget-object v0, p1, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method private static g(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 4709
    .line 4713
    iget v7, p1, Lcom/wssnps/b/y;->b:I

    .line 4715
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4717
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4718
    if-nez v2, :cond_1

    .line 4807
    :cond_0
    :goto_0
    return-void

    .line 4723
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 4725
    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    .line 4727
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4733
    :goto_1
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 4735
    iget-object v3, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 4737
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data4"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data5"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data6"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data7"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data8"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data9"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data10"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4750
    add-int/lit8 v0, v1, 0x1

    .line 4758
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4805
    :cond_2
    if-eqz v2, :cond_0

    .line 4806
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 4754
    :cond_3
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 4763
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 4765
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4767
    if-ge v1, v3, :cond_5

    .line 4769
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    const-string v9, "_id"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data4"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data5"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data6"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data7"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data8"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data9"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data10"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4783
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 4765
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    .line 4787
    :cond_5
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v8, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data4"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data5"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data6"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data7"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data8"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data9"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data10"

    iget-object v0, p1, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method public static g(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 3917
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 3920
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3921
    const/4 v0, 0x1

    return v0
.end method

.method private static h(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 4811
    .line 4815
    iget v7, p1, Lcom/wssnps/b/y;->b:I

    .line 4817
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/organization"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4819
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4820
    if-nez v2, :cond_1

    .line 4888
    :cond_0
    :goto_0
    return-void

    .line 4825
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 4827
    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    .line 4829
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4835
    :goto_1
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 4837
    iget-object v3, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 4839
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data3"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data4"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4846
    add-int/lit8 v0, v1, 0x1

    .line 4854
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4886
    :cond_2
    if-eqz v2, :cond_0

    .line 4887
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 4850
    :cond_3
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 4859
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 4861
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4863
    if-ge v1, v3, :cond_5

    .line 4865
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    const-string v9, "_id"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data4"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4872
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 4861
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4876
    :cond_5
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v8, "vnd.android.cursor.item/organization"

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data2"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data4"

    iget-object v0, p1, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method public static h(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/16 v3, 0x32

    const/4 v0, 0x0

    .line 3943
    .line 3947
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 3948
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 3949
    array-length v6, v5

    .line 3951
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move v4, v0

    .line 3955
    :goto_0
    if-ge v4, v6, :cond_4

    .line 3957
    sub-int v1, v6, v4

    if-le v1, v3, :cond_1

    move v2, v3

    .line 3958
    :goto_1
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    move v1, v0

    .line 3959
    :goto_2
    if-ge v1, v2, :cond_3

    .line 3961
    add-int v8, v4, v1

    aget-object v8, v5, v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 3963
    if-nez v1, :cond_2

    .line 3965
    const-string v8, " ( "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3971
    :goto_3
    add-int v8, v4, v1

    aget-object v8, v5, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3959
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3957
    :cond_1
    sub-int v1, v6, v4

    move v2, v1

    goto :goto_1

    .line 3969
    :cond_2
    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 3978
    :catch_0
    move-exception v1

    .line 3980
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 3984
    :goto_4
    return v0

    .line 3974
    :cond_3
    :try_start_1
    const-string v1, " ) "

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3975
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id in "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x0

    invoke-virtual {p0, v1, v2, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 3955
    add-int/lit8 v1, v4, 0x32

    move v4, v1

    goto :goto_0

    .line 3984
    :cond_4
    const/4 v0, 0x1

    goto :goto_4
.end method

.method private static i(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 4892
    .line 4896
    iget v7, p1, Lcom/wssnps/b/y;->b:I

    .line 4898
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/website"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4900
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4901
    if-nez v2, :cond_1

    .line 4962
    :cond_0
    :goto_0
    return-void

    .line 4906
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 4908
    iget-object v0, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    .line 4910
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4916
    :goto_1
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 4918
    iget-object v3, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 4920
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4924
    add-int/lit8 v0, v1, 0x1

    .line 4932
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 4960
    :cond_2
    if-eqz v2, :cond_0

    .line 4961
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4928
    :cond_3
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 4937
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 4939
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 4941
    if-ge v1, v3, :cond_5

    .line 4943
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    const-string v9, "_id"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4948
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 4939
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 4952
    :cond_5
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v8, "vnd.android.cursor.item/website"

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method private static i(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 5211
    .line 5214
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 5215
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 5217
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "raw_contact_id IN ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5218
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 5219
    if-nez v7, :cond_1

    .line 5258
    :cond_0
    :goto_0
    return-void

    .line 5223
    :cond_1
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 5224
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 5228
    :cond_2
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5229
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 5233
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5234
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 5236
    const-string v1, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "speed_dial_data_id IN ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 5237
    if-nez v2, :cond_4

    .line 5239
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 5242
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5246
    :cond_5
    sget-object v0, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://com.android.contacts/contacts/speeddial/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "key_number"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5249
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5253
    :cond_6
    if-eqz v2, :cond_7

    .line 5254
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 5256
    :cond_7
    if-eqz v7, :cond_0

    .line 5257
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method private static j(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 4966
    .line 4970
    iget v7, p1, Lcom/wssnps/b/y;->b:I

    .line 4972
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4974
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4975
    if-nez v2, :cond_1

    .line 5036
    :cond_0
    :goto_0
    return-void

    .line 4980
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 4982
    iget-object v0, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_4

    .line 4984
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v6

    .line 4990
    :goto_1
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 4992
    iget-object v3, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 4994
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4998
    add-int/lit8 v0, v1, 0x1

    .line 5006
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_6

    .line 5034
    :cond_2
    if-eqz v2, :cond_0

    .line 5035
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 5002
    :cond_3
    sget-object v3, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v7, v10, [Ljava/lang/String;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v6

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 5011
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v1, v6

    .line 5013
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 5015
    if-ge v1, v3, :cond_5

    .line 5017
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "_id=?"

    new-array v8, v10, [Ljava/lang/String;

    const-string v9, "_id"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5022
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 5013
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 5026
    :cond_5
    sget-object v4, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "raw_contact_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "mimetype"

    const-string v8, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v0, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v8, "data1"

    iget-object v0, p1, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v5, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    move v1, v0

    goto/16 :goto_1
.end method

.method private static k(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 5040
    .line 5043
    iget v6, p1, Lcom/wssnps/b/y;->b:I

    .line 5045
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/photo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5047
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 5048
    if-nez v0, :cond_1

    .line 5107
    :cond_0
    :goto_0
    return-void

    .line 5053
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 5054
    if-lez v1, :cond_4

    .line 5056
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5058
    iget-object v1, p1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5061
    iget-object v1, p1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/wssnps/a/a;->b([B)[B

    move-result-object v1

    .line 5069
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    const-string v6, "_id"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data15"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5105
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 5106
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 5077
    :cond_3
    sget-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    const-string v6, "_id"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data15"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 5087
    :cond_4
    iget-object v1, p1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5090
    iget-object v1, p1, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/wssnps/a/a;->b([B)[B

    move-result-object v1

    .line 5098
    sget-object v2, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "raw_contact_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "mimetype"

    const-string v5, "vnd.android.cursor.item/photo"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "data15"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static l(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 5156
    .line 5159
    iget v6, p1, Lcom/wssnps/b/y;->b:I

    .line 5161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw_contact_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/sip_address"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5163
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 5166
    if-nez v0, :cond_1

    .line 5207
    :cond_0
    :goto_0
    return-void

    .line 5171
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 5172
    if-lez v1, :cond_4

    .line 5174
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5176
    iget-object v1, p1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5178
    sget-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "_id=?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data1"

    iget-object v4, p1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5205
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 5206
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 5186
    :cond_3
    sget-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "_id=?"

    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 5195
    :cond_4
    iget-object v1, p1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5197
    sget-object v1, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "raw_contact_id"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype"

    const-string v4, "vnd.android.cursor.item/sip_address"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "data1"

    iget-object v4, p1, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static m(Landroid/content/ContentResolver;Lcom/wssnps/b/y;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 5262
    .line 5265
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 5267
    iget v0, p1, Lcom/wssnps/b/y;->b:I

    .line 5269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "raw_contact_id=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5270
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 5271
    if-nez v7, :cond_1

    .line 5311
    :cond_0
    :goto_0
    return-void

    .line 5275
    :cond_1
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 5276
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 5280
    :cond_2
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5281
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 5284
    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5285
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 5287
    const-string v0, "content://com.android.contacts/contacts/speeddial"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "speed_dial_data_id IN ("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 5289
    if-nez v2, :cond_4

    .line 5291
    if-eqz v7, :cond_0

    .line 5292
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 5295
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 5299
    :cond_5
    sget-object v0, Lcom/wssnps/b/y;->B:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://com.android.contacts/contacts/speeddial/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "key_number"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5302
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_5

    .line 5306
    :cond_6
    if-eqz v2, :cond_7

    .line 5307
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 5309
    :cond_7
    if-eqz v7, :cond_0

    .line 5310
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Lcom/wssnps/b/z;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4105
    if-nez p1, :cond_0

    .line 4194
    :goto_0
    return-object v1

    .line 4108
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4110
    const-string v2, "raw_contact_id"

    iget v3, p0, Lcom/wssnps/b/y;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4111
    const-string v2, "vnd.android.cursor.item/nickname"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4113
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/nickname"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4114
    const-string v1, "data1"

    iget-object v2, p1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move-object v1, v0

    .line 4194
    goto :goto_0

    .line 4116
    :cond_1
    const-string v2, "vnd.android.cursor.item/note"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4119
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/note"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4120
    const-string v1, "data1"

    iget-object v2, p1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 4122
    :cond_2
    const-string v2, "vnd.android.cursor.item/name"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 4124
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/name"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4126
    const-string v1, "data2"

    iget-object v2, p1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4127
    const-string v1, "data3"

    iget-object v2, p1, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4128
    const-string v1, "data4"

    iget-object v2, p1, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4129
    const-string v1, "data5"

    iget-object v2, p1, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4130
    const-string v1, "data6"

    iget-object v2, p1, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4131
    const-string v1, "data7"

    iget-object v2, p1, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4132
    const-string v1, "data9"

    iget-object v2, p1, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4133
    const-string v1, "data8"

    iget-object v2, p1, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 4135
    :cond_3
    const-string v2, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4137
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/phone_v2"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4138
    const-string v1, "data1"

    iget-object v2, p1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4139
    const-string v1, "data2"

    iget-object v2, p1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4140
    const-string v1, "data3"

    iget-object v2, p1, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4142
    :cond_4
    const-string v2, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 4144
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/email_v2"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4145
    const-string v1, "data1"

    iget-object v2, p1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4146
    const-string v1, "data2"

    iget-object v2, p1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4147
    const-string v1, "data3"

    iget-object v2, p1, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4149
    :cond_5
    const-string v2, "vnd.android.cursor.item/im"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4151
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/im"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4152
    const-string v1, "data1"

    iget-object v2, p1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4153
    const-string v1, "data2"

    iget-object v2, p1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4154
    const-string v1, "data5"

    iget-object v2, p1, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4155
    const-string v1, "data6"

    iget-object v2, p1, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4157
    :cond_6
    const-string v2, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 4159
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4161
    const-string v1, "data2"

    iget-object v2, p1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4162
    const-string v1, "data4"

    iget-object v2, p1, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4163
    const-string v1, "data5"

    iget-object v2, p1, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4164
    const-string v1, "data6"

    iget-object v2, p1, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4165
    const-string v1, "data7"

    iget-object v2, p1, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4166
    const-string v1, "data8"

    iget-object v2, p1, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4167
    const-string v1, "data9"

    iget-object v2, p1, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4168
    const-string v1, "data10"

    iget-object v2, p1, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4169
    const-string v1, "data3"

    iget-object v2, p1, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4171
    :cond_7
    const-string v2, "vnd.android.cursor.item/organization"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 4173
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/organization"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4174
    const-string v1, "data1"

    iget-object v2, p1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4175
    const-string v1, "data2"

    iget-object v2, p1, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4176
    const-string v1, "data4"

    iget-object v2, p1, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4178
    :cond_8
    const-string v2, "vnd.android.cursor.item/website"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 4180
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/website"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4181
    const-string v1, "data1"

    iget-object v2, p1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4183
    :cond_9
    const-string v2, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 4185
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4186
    const-string v1, "data1"

    iget-object v2, p1, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4191
    :cond_a
    const/4 v0, 0x1

    const-string v2, "minetype not match"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1143
    invoke-virtual {p0}, Lcom/wssnps/b/y;->b()Lcom/wssnps/a/ac;

    move-result-object v0

    .line 1144
    invoke-static {v0}, Lcom/wssnps/a/s;->a(Lcom/wssnps/a/ac;)Ljava/lang/String;

    move-result-object v0

    .line 1145
    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 4037
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4038
    const-string v1, "raw_contact_id"

    iget v2, p0, Lcom/wssnps/b/y;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4040
    const-string v1, "vnd.android.cursor.item/note"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4042
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/note"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4043
    const-string v1, "data1"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4079
    :goto_0
    return-object v0

    .line 4045
    :cond_0
    const-string v1, "vnd.android.cursor.item/nickname"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4047
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/nickname"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4048
    const-string v1, "data1"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4050
    :cond_1
    const-string v1, "vnd.android.cursor.item/photo"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4052
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/photo"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4054
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-static {v1}, Lcom/wssnps/a/a;->b([B)[B

    move-result-object v1

    .line 4061
    const-string v2, "data15"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_0

    .line 4063
    :cond_2
    const-string v1, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4065
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4067
    :cond_3
    const-string v1, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 4069
    const-string v1, "mimetype"

    const-string v2, "vnd.android.cursor.item/contact_event"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4070
    const-string v1, "data1"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4071
    const-string v1, "data2"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 4075
    :cond_4
    const/4 v0, 0x0

    .line 4076
    const/4 v1, 0x1

    const-string v2, "minetype not match"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public b()Lcom/wssnps/a/ac;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1180
    new-instance v5, Lcom/wssnps/a/ac;

    invoke-direct {v5}, Lcom/wssnps/a/ac;-><init>()V

    .line 1181
    iget v0, p0, Lcom/wssnps/b/y;->b:I

    iput v0, v5, Lcom/wssnps/a/ac;->a:I

    .line 1182
    iget-object v0, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    iput-object v0, v5, Lcom/wssnps/a/ac;->b:Ljava/lang/String;

    .line 1185
    sget-object v0, Lcom/wssnps/a/aa;->a:Lcom/wssnps/a/aa;

    iput-object v0, v5, Lcom/wssnps/a/ac;->u:Lcom/wssnps/a/aa;

    .line 1187
    new-instance v0, Lcom/wssnps/a/y;

    invoke-direct {v0}, Lcom/wssnps/a/y;-><init>()V

    iput-object v0, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    .line 1188
    iget-object v0, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    const-string v1, ""

    iput-object v1, v0, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    .line 1189
    iget-object v0, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    const-string v1, ""

    iput-object v1, v0, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    .line 1191
    iget-object v0, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1193
    iget-object v0, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1194
    const/4 v1, -0x1

    if-ne v0, v1, :cond_d

    .line 1196
    iget-object v0, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v1, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    .line 1197
    iget-object v0, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    const-string v1, ""

    iput-object v1, v0, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    .line 1206
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_b

    .line 1210
    iget-object v0, p0, Lcom/wssnps/b/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1212
    if-eqz v0, :cond_5c

    .line 1214
    new-instance v1, Lcom/wssnps/a/y;

    invoke-direct {v1}, Lcom/wssnps/a/y;-><init>()V

    .line 1216
    iget-object v4, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1217
    iget-object v4, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    .line 1219
    :cond_1
    iget-object v4, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1220
    iget-object v4, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    .line 1222
    :cond_2
    iget-object v4, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1223
    iget-object v4, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/a/y;->d:Ljava/lang/String;

    .line 1225
    :cond_3
    iget-object v4, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1226
    iget-object v4, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/a/y;->c:Ljava/lang/String;

    .line 1228
    :cond_4
    iget-object v4, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1229
    iget-object v4, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/a/y;->e:Ljava/lang/String;

    .line 1231
    :cond_5
    iget-object v4, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1232
    iget-object v4, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/a/y;->g:Ljava/lang/String;

    .line 1234
    :cond_6
    iget-object v4, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1235
    iget-object v4, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    iput-object v4, v1, Lcom/wssnps/a/y;->h:Ljava/lang/String;

    .line 1237
    :cond_7
    iget-object v4, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1238
    iget-object v0, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    iput-object v0, v1, Lcom/wssnps/a/y;->i:Ljava/lang/String;

    .line 1240
    :cond_8
    iget-object v0, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/wssnps/a/y;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/wssnps/a/y;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/wssnps/a/y;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/wssnps/a/y;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/wssnps/a/y;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/wssnps/a/y;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1249
    :cond_9
    iput-boolean v8, v1, Lcom/wssnps/a/y;->j:Z

    :cond_a
    move-object v0, v1

    .line 1252
    :goto_1
    iput-object v0, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    .line 1258
    :cond_b
    iget-object v0, p0, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    move v1, v3

    .line 1260
    :goto_2
    iget-object v0, p0, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 1262
    iget-object v0, p0, Lcom/wssnps/b/y;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1263
    if-eqz v0, :cond_c

    .line 1265
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 1267
    if-eqz v4, :cond_c

    .line 1269
    new-instance v4, Lcom/wssnps/a/w;

    invoke-direct {v4}, Lcom/wssnps/a/w;-><init>()V

    .line 1270
    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v0, v4, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 1272
    iget-object v0, v5, Lcom/wssnps/a/ac;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1260
    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1201
    :cond_d
    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v4, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    invoke-virtual {v4, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/wssnps/a/y;->a:Ljava/lang/String;

    .line 1202
    iget-object v1, v5, Lcom/wssnps/a/ac;->c:Lcom/wssnps/a/y;

    iget-object v4, p0, Lcom/wssnps/b/y;->c:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/wssnps/a/y;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 1278
    :cond_e
    iget-object v0, p0, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_10

    move v1, v3

    .line 1280
    :goto_3
    iget-object v0, p0, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 1282
    iget-object v0, p0, Lcom/wssnps/b/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1283
    if-eqz v0, :cond_f

    .line 1285
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    .line 1287
    if-eqz v4, :cond_f

    .line 1289
    new-instance v4, Lcom/wssnps/a/w;

    invoke-direct {v4}, Lcom/wssnps/a/w;-><init>()V

    .line 1290
    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v0, v4, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 1292
    iget-object v0, v5, Lcom/wssnps/a/ac;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1280
    :cond_f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1298
    :cond_10
    iget-object v0, p0, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_14

    move v1, v3

    .line 1300
    :goto_4
    iget-object v0, p0, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_14

    .line 1302
    iget-object v0, p0, Lcom/wssnps/b/y;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1303
    if-eqz v0, :cond_12

    .line 1307
    iget-object v4, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5b

    .line 1308
    sget-object v4, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    iget-object v6, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 1310
    :goto_5
    if-nez v4, :cond_11

    .line 1311
    sget-object v4, Lcom/wssnps/b/y;->F:Lcom/wssnps/b/ab;

    invoke-virtual {v4}, Lcom/wssnps/b/ab;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1313
    :cond_11
    if-eqz v4, :cond_12

    .line 1315
    new-instance v6, Lcom/wssnps/a/w;

    invoke-direct {v6}, Lcom/wssnps/a/w;-><init>()V

    .line 1316
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v6, Lcom/wssnps/a/w;->b:I

    .line 1317
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 1318
    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v0, v6, Lcom/wssnps/a/w;->c:Ljava/lang/String;

    .line 1320
    iget-object v0, v5, Lcom/wssnps/a/ac;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1322
    invoke-static {}, Lcom/wssnps/smlModelDefine;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1324
    iget-object v0, p0, Lcom/wssnps/b/y;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1325
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_13

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_13

    .line 1326
    iget-object v4, v5, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1300
    :cond_12
    :goto_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1328
    :cond_13
    iget-object v0, v5, Lcom/wssnps/a/ac;->t:Ljava/util/ArrayList;

    const-string v4, "-1"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1335
    :cond_14
    iget-object v0, p0, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_17

    move v1, v3

    .line 1337
    :goto_7
    iget-object v0, p0, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_17

    .line 1339
    iget-object v0, p0, Lcom/wssnps/b/y;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1340
    if-eqz v0, :cond_16

    .line 1344
    iget-object v4, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5a

    .line 1345
    sget-object v4, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    iget-object v6, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 1347
    :goto_8
    if-nez v4, :cond_15

    .line 1348
    sget-object v4, Lcom/wssnps/b/y;->G:Lcom/wssnps/b/ab;

    invoke-virtual {v4}, Lcom/wssnps/b/ab;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1350
    :cond_15
    if-eqz v4, :cond_16

    .line 1352
    new-instance v6, Lcom/wssnps/a/w;

    invoke-direct {v6}, Lcom/wssnps/a/w;-><init>()V

    .line 1353
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v6, Lcom/wssnps/a/w;->b:I

    .line 1354
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 1355
    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v0, v6, Lcom/wssnps/a/w;->c:Ljava/lang/String;

    .line 1357
    iget-object v0, v5, Lcom/wssnps/a/ac;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1337
    :cond_16
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1363
    :cond_17
    iget-object v0, p0, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_1a

    move v1, v3

    .line 1365
    :goto_9
    iget-object v0, p0, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1a

    .line 1367
    iget-object v0, p0, Lcom/wssnps/b/y;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1368
    if-eqz v0, :cond_19

    .line 1372
    iget-object v4, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_59

    .line 1373
    sget-object v4, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    iget-object v6, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 1375
    :goto_a
    if-nez v4, :cond_18

    .line 1376
    sget-object v4, Lcom/wssnps/b/y;->H:Lcom/wssnps/b/ab;

    invoke-virtual {v4}, Lcom/wssnps/b/ab;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1378
    :cond_18
    if-eqz v4, :cond_19

    .line 1380
    new-instance v6, Lcom/wssnps/a/w;

    invoke-direct {v6}, Lcom/wssnps/a/w;-><init>()V

    .line 1381
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v6, Lcom/wssnps/a/w;->b:I

    .line 1382
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 1383
    iget-object v0, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    iput-object v0, v6, Lcom/wssnps/a/w;->c:Ljava/lang/String;

    .line 1385
    iget-object v0, v5, Lcom/wssnps/a/ac;->x:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1365
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 1391
    :cond_1a
    iget-object v0, p0, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 1393
    iget-object v0, p0, Lcom/wssnps/b/y;->t:Ljava/lang/String;

    iput-object v0, v5, Lcom/wssnps/a/ac;->z:Ljava/lang/String;

    .line 1396
    :cond_1b
    iget-object v0, p0, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_33

    move v1, v3

    .line 1399
    :goto_b
    iget-object v0, p0, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_33

    .line 1401
    iget-object v0, p0, Lcom/wssnps/b/y;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1402
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 1399
    :cond_1c
    :goto_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1405
    :cond_1d
    if-eqz v0, :cond_1c

    .line 1409
    iget-object v4, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_58

    .line 1410
    sget-object v4, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    iget-object v6, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 1412
    :goto_d
    new-instance v6, Lcom/wssnps/a/u;

    invoke-direct {v6}, Lcom/wssnps/a/u;-><init>()V

    .line 1414
    if-nez v4, :cond_1e

    .line 1415
    sget-object v4, Lcom/wssnps/b/y;->I:Lcom/wssnps/b/ab;

    invoke-virtual {v4}, Lcom/wssnps/b/ab;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1417
    :cond_1e
    if-eqz v4, :cond_1c

    .line 1419
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v6, Lcom/wssnps/a/u;->h:I

    .line 1421
    if-eqz v0, :cond_28

    .line 1423
    iget-object v4, p0, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    iget-object v4, v4, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    const-string v7, "com.android.exchange"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 1425
    iget-object v4, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1f

    .line 1426
    iget-object v4, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->c:Ljava/lang/String;

    .line 1428
    :cond_1f
    iget-object v4, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_20

    .line 1429
    iget-object v4, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->a:Ljava/lang/String;

    .line 1431
    :cond_20
    iget-object v4, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_21

    .line 1432
    iget-object v4, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    .line 1434
    :cond_21
    iget-object v4, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_22

    .line 1435
    iget-object v4, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->d:Ljava/lang/String;

    .line 1437
    :cond_22
    iget-object v4, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_23

    .line 1438
    iget-object v4, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->e:Ljava/lang/String;

    .line 1440
    :cond_23
    iget-object v4, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_24

    .line 1441
    iget-object v4, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->f:Ljava/lang/String;

    .line 1443
    :cond_24
    iget-object v4, v0, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_25

    .line 1444
    iget-object v4, v0, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->g:Ljava/lang/String;

    .line 1446
    :cond_25
    iget-object v4, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 1447
    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v0, v6, Lcom/wssnps/a/u;->j:Ljava/lang/String;

    .line 1487
    :cond_26
    :goto_e
    iget-object v0, v6, Lcom/wssnps/a/u;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, v6, Lcom/wssnps/a/u;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, v6, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, v6, Lcom/wssnps/a/u;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, v6, Lcom/wssnps/a/u;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, v6, Lcom/wssnps/a/u;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, v6, Lcom/wssnps/a/u;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 1495
    :cond_27
    iput-boolean v8, v6, Lcom/wssnps/a/u;->i:Z

    .line 1499
    :cond_28
    iget-object v0, v5, Lcom/wssnps/a/ac;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c

    .line 1451
    :cond_29
    invoke-static {}, Lcom/wssnps/smlModelDefine;->A()Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 1453
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    .line 1454
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    .line 1456
    :cond_2a
    iget-object v4, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 1457
    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v0, v6, Lcom/wssnps/a/u;->j:Ljava/lang/String;

    goto :goto_e

    .line 1461
    :cond_2b
    iget-object v4, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2c

    .line 1462
    iget-object v4, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->c:Ljava/lang/String;

    .line 1464
    :cond_2c
    iget-object v4, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2d

    .line 1465
    iget-object v4, v0, Lcom/wssnps/b/z;->e:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->a:Ljava/lang/String;

    .line 1467
    :cond_2d
    iget-object v4, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2e

    .line 1468
    iget-object v4, v0, Lcom/wssnps/b/z;->f:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->b:Ljava/lang/String;

    .line 1470
    :cond_2e
    iget-object v4, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2f

    .line 1471
    iget-object v4, v0, Lcom/wssnps/b/z;->g:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->d:Ljava/lang/String;

    .line 1473
    :cond_2f
    iget-object v4, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_30

    .line 1474
    iget-object v4, v0, Lcom/wssnps/b/z;->h:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->e:Ljava/lang/String;

    .line 1476
    :cond_30
    iget-object v4, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_31

    .line 1477
    iget-object v4, v0, Lcom/wssnps/b/z;->i:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->f:Ljava/lang/String;

    .line 1479
    :cond_31
    iget-object v4, v0, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_32

    .line 1480
    iget-object v4, v0, Lcom/wssnps/b/z;->j:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/u;->g:Ljava/lang/String;

    .line 1482
    :cond_32
    iget-object v4, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_26

    .line 1483
    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v0, v6, Lcom/wssnps/a/u;->j:Ljava/lang/String;

    goto/16 :goto_e

    .line 1505
    :cond_33
    iget-object v0, p0, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_35

    move v1, v3

    .line 1508
    :goto_f
    iget-object v0, p0, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_35

    .line 1510
    iget-object v0, p0, Lcom/wssnps/b/y;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1511
    if-eqz v0, :cond_34

    .line 1513
    new-instance v4, Lcom/wssnps/a/w;

    invoke-direct {v4}, Lcom/wssnps/a/w;-><init>()V

    .line 1514
    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v0, v4, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 1516
    iget-object v0, v5, Lcom/wssnps/a/ac;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1508
    :cond_34
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 1521
    :cond_35
    iget-object v0, p0, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_3e

    move v1, v3

    .line 1524
    :goto_10
    iget-object v0, p0, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3e

    .line 1526
    iget-object v0, p0, Lcom/wssnps/b/y;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1531
    if-eqz v0, :cond_3d

    .line 1535
    iget-object v4, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_57

    .line 1536
    sget-object v4, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    iget-object v6, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/wssnps/b/ab;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 1538
    :goto_11
    new-instance v6, Lcom/wssnps/a/z;

    invoke-direct {v6}, Lcom/wssnps/a/z;-><init>()V

    .line 1540
    if-nez v4, :cond_36

    .line 1541
    sget-object v4, Lcom/wssnps/b/y;->J:Lcom/wssnps/b/ab;

    invoke-virtual {v4}, Lcom/wssnps/b/ab;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 1543
    :cond_36
    if-eqz v4, :cond_3d

    .line 1545
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v6, Lcom/wssnps/a/z;->e:I

    .line 1547
    if-eqz v0, :cond_3c

    .line 1549
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_37

    .line 1550
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/z;->a:Ljava/lang/String;

    .line 1552
    :cond_37
    iget-object v4, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_38

    .line 1553
    iget-object v4, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/z;->b:Ljava/lang/String;

    .line 1555
    :cond_38
    iget-object v4, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_39

    .line 1556
    iget-object v4, v0, Lcom/wssnps/b/z;->d:Ljava/lang/String;

    iput-object v4, v6, Lcom/wssnps/a/z;->c:Ljava/lang/String;

    .line 1558
    :cond_39
    iget-object v4, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3a

    .line 1559
    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v0, v6, Lcom/wssnps/a/z;->g:Ljava/lang/String;

    .line 1561
    :cond_3a
    iget-object v0, v6, Lcom/wssnps/a/z;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3b

    iget-object v0, v6, Lcom/wssnps/a/z;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3b

    iget-object v0, v6, Lcom/wssnps/a/z;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3c

    .line 1563
    :cond_3b
    iput-boolean v8, v6, Lcom/wssnps/a/z;->f:Z

    .line 1566
    :cond_3c
    iget-object v0, v5, Lcom/wssnps/a/ac;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1524
    :cond_3d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_10

    .line 1572
    :cond_3e
    iget-object v0, p0, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    if-eqz v0, :cond_40

    move v1, v3

    .line 1574
    :goto_12
    iget-object v0, p0, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_40

    .line 1576
    iget-object v0, p0, Lcom/wssnps/b/y;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1577
    if-eqz v0, :cond_3f

    .line 1579
    iget-object v4, v5, Lcom/wssnps/a/ac;->r:Ljava/util/ArrayList;

    iget-object v0, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1574
    :cond_3f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    .line 1584
    :cond_40
    iget-object v0, p0, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_43

    move v1, v3

    .line 1586
    :goto_13
    iget-object v0, p0, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_43

    .line 1588
    iget-object v0, p0, Lcom/wssnps/b/y;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1589
    if-eqz v0, :cond_42

    .line 1591
    iget-object v4, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    if-nez v4, :cond_41

    .line 1592
    const-string v4, "0"

    iput-object v4, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 1593
    :cond_41
    iget-object v4, v5, Lcom/wssnps/a/ac;->e:Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-static {v6, v0}, Lcom/wssnps/b/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/wssnps/a/t;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1586
    :cond_42
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_13

    .line 1598
    :cond_43
    iget-object v0, p0, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_46

    move v1, v3

    .line 1600
    :goto_14
    iget-object v0, p0, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_46

    .line 1602
    iget-object v0, p0, Lcom/wssnps/b/y;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1603
    if-eqz v0, :cond_45

    .line 1605
    iget-object v4, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    if-nez v4, :cond_44

    .line 1606
    const-string v4, "0"

    iput-object v4, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 1607
    :cond_44
    iget-object v4, v5, Lcom/wssnps/a/ac;->f:Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-static {v6, v0}, Lcom/wssnps/b/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/wssnps/a/t;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1600
    :cond_45
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_14

    .line 1612
    :cond_46
    iget-object v0, p0, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_49

    move v1, v3

    .line 1614
    :goto_15
    iget-object v0, p0, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_49

    .line 1616
    iget-object v0, p0, Lcom/wssnps/b/y;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1617
    if-eqz v0, :cond_48

    .line 1619
    iget-object v4, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    if-nez v4, :cond_47

    .line 1620
    const-string v4, "0"

    iput-object v4, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 1621
    :cond_47
    iget-object v4, v5, Lcom/wssnps/a/ac;->g:Ljava/util/ArrayList;

    iget-object v6, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-static {v6, v0}, Lcom/wssnps/b/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/wssnps/a/t;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1614
    :cond_48
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_15

    .line 1626
    :cond_49
    iget-object v0, p0, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    if-eqz v0, :cond_4c

    move v1, v3

    .line 1629
    :goto_16
    iget-object v0, p0, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4c

    .line 1631
    iget-object v0, p0, Lcom/wssnps/b/y;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1632
    if-eqz v0, :cond_4b

    .line 1634
    iget-object v4, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    if-nez v4, :cond_4a

    .line 1635
    const-string v4, "0"

    iput-object v4, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    .line 1636
    :cond_4a
    iget-object v4, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iget-object v6, v0, Lcom/wssnps/b/z;->l:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/wssnps/b/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/wssnps/a/t;

    move-result-object v4

    .line 1637
    const/high16 v6, 0x800000

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 1638
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lcom/wssnps/a/t;->f:I

    .line 1639
    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v0, v4, Lcom/wssnps/a/t;->g:Ljava/lang/String;

    .line 1640
    iget-object v0, v5, Lcom/wssnps/a/ac;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1629
    :cond_4b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    .line 1645
    :cond_4c
    iget-object v0, p0, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_4e

    move v1, v3

    .line 1647
    :goto_17
    iget-object v0, p0, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4e

    .line 1649
    iget-object v0, p0, Lcom/wssnps/b/y;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/z;

    .line 1650
    if-eqz v0, :cond_4d

    .line 1652
    new-instance v4, Lcom/wssnps/a/w;

    invoke-direct {v4}, Lcom/wssnps/a/w;-><init>()V

    .line 1653
    iget-object v6, v0, Lcom/wssnps/b/z;->b:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iput v6, v4, Lcom/wssnps/a/w;->b:I

    .line 1654
    iget-object v6, v0, Lcom/wssnps/b/z;->a:Ljava/lang/String;

    iput-object v6, v4, Lcom/wssnps/a/w;->a:Ljava/lang/String;

    .line 1655
    iget-object v0, v0, Lcom/wssnps/b/z;->c:Ljava/lang/String;

    iput-object v0, v4, Lcom/wssnps/a/w;->c:Ljava/lang/String;

    .line 1656
    iget-object v0, v5, Lcom/wssnps/a/ac;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1647
    :cond_4d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    .line 1661
    :cond_4e
    iget-object v0, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4f

    .line 1663
    iget-object v0, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iput-object v0, v5, Lcom/wssnps/a/ac;->v:Lcom/wssnps/a/ab;

    .line 1664
    iput-object v2, p0, Lcom/wssnps/b/y;->z:Ljava/lang/String;

    .line 1665
    iput-object v2, p0, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    .line 1668
    :cond_4f
    iget-object v0, p0, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    if-eqz v0, :cond_50

    iget-object v0, p0, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    if-eqz v0, :cond_50

    iget-object v0, p0, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    iget-object v0, v0, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_50

    .line 1670
    iget-object v0, p0, Lcom/wssnps/b/y;->w:Lcom/wssnps/a/ab;

    iput-object v0, v5, Lcom/wssnps/a/ac;->w:Lcom/wssnps/a/ab;

    .line 1673
    :cond_50
    iget-object v0, p0, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_51

    .line 1675
    iget-object v0, v5, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v1, p0, Lcom/wssnps/b/y;->x:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/x;->a:Ljava/lang/String;

    .line 1677
    iget-object v0, p0, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    if-eqz v0, :cond_55

    .line 1680
    iget-object v0, v5, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iput v8, v0, Lcom/wssnps/a/x;->b:I

    .line 1689
    :goto_18
    iget-object v0, v5, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iget-object v1, p0, Lcom/wssnps/b/y;->y:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/x;->c:Ljava/lang/String;

    .line 1690
    iput-object v2, p0, Lcom/wssnps/b/y;->z:Ljava/lang/String;

    .line 1693
    :cond_51
    iget-object v0, p0, Lcom/wssnps/b/y;->z:Ljava/lang/String;

    if-eqz v0, :cond_52

    .line 1695
    iget-object v0, p0, Lcom/wssnps/b/y;->z:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v5, Lcom/wssnps/a/ac;->A:I

    .line 1698
    :cond_52
    iget-object v0, p0, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    if-eqz v0, :cond_53

    .line 1700
    iget-object v0, v5, Lcom/wssnps/a/ac;->s:Lcom/wssnps/a/v;

    iget-object v1, p0, Lcom/wssnps/b/y;->s:Ljava/lang/String;

    iput-object v1, v0, Lcom/wssnps/a/v;->b:Ljava/lang/String;

    .line 1703
    :cond_53
    iget-object v0, p0, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    if-eqz v0, :cond_54

    .line 1705
    iget-object v0, p0, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 1706
    const-string v0, "SET"

    iput-object v0, v5, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    .line 1711
    :cond_54
    :goto_19
    return-object v5

    .line 1683
    :cond_55
    iget-object v0, v5, Lcom/wssnps/a/ac;->y:Lcom/wssnps/a/x;

    iput v3, v0, Lcom/wssnps/a/x;->b:I

    goto :goto_18

    .line 1708
    :cond_56
    const-string v0, "UNSET"

    iput-object v0, v5, Lcom/wssnps/a/ac;->C:Ljava/lang/String;

    goto :goto_19

    :cond_57
    move-object v4, v2

    goto/16 :goto_11

    :cond_58
    move-object v4, v2

    goto/16 :goto_d

    :cond_59
    move-object v4, v2

    goto/16 :goto_a

    :cond_5a
    move-object v4, v2

    goto/16 :goto_8

    :cond_5b
    move-object v4, v2

    goto/16 :goto_5

    :cond_5c
    move-object v0, v2

    goto/16 :goto_1
.end method

.method public c(Landroid/content/ContentResolver;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 4017
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4018
    iget-object v1, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v1, v1, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4020
    const-string v1, "account_name"

    iget-object v2, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4021
    const-string v1, "account_type"

    iget-object v2, p0, Lcom/wssnps/b/y;->v:Lcom/wssnps/a/ab;

    iget-object v2, v2, Lcom/wssnps/a/ab;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4023
    :cond_0
    iget-object v1, p0, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4025
    const-string v1, "starred"

    iget-object v2, p0, Lcom/wssnps/b/y;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4028
    :cond_1
    iget v1, p0, Lcom/wssnps/b/y;->b:I

    if-eqz v1, :cond_2

    .line 4029
    const-string v1, "_id"

    iget v2, p0, Lcom/wssnps/b/y;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4031
    :cond_2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/wssnps/b/y;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
