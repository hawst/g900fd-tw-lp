.class public final enum Lcom/wssnps/b/aa;
.super Ljava/lang/Enum;
.source "smlContactItem.java"


# static fields
.field public static final enum a:Lcom/wssnps/b/aa;

.field public static final enum b:Lcom/wssnps/b/aa;

.field public static final enum c:Lcom/wssnps/b/aa;

.field public static final enum d:Lcom/wssnps/b/aa;

.field public static final enum e:Lcom/wssnps/b/aa;

.field public static final enum f:Lcom/wssnps/b/aa;

.field public static final enum g:Lcom/wssnps/b/aa;

.field public static final enum h:Lcom/wssnps/b/aa;

.field public static final enum i:Lcom/wssnps/b/aa;

.field private static final synthetic k:[Lcom/wssnps/b/aa;


# instance fields
.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 142
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_NONE"

    invoke-direct {v0, v1, v4, v4}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->a:Lcom/wssnps/b/aa;

    .line 143
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_PHONE"

    invoke-direct {v0, v1, v5, v5}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->b:Lcom/wssnps/b/aa;

    .line 144
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_SIM_ADN"

    invoke-direct {v0, v1, v6, v6}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    .line 145
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_SIM_FDN"

    invoke-direct {v0, v1, v7, v7}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->d:Lcom/wssnps/b/aa;

    .line 146
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_SIM_SDN"

    invoke-direct {v0, v1, v8, v8}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->e:Lcom/wssnps/b/aa;

    .line 147
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_SIM_MSISDN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->f:Lcom/wssnps/b/aa;

    .line 148
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_EXTERNAL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->g:Lcom/wssnps/b/aa;

    .line 149
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_BR"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->h:Lcom/wssnps/b/aa;

    .line 150
    new-instance v0, Lcom/wssnps/b/aa;

    const-string v1, "SMLDS_PIM_ADAPTER_CONTACT_SIM_ADN2"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/wssnps/b/aa;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    .line 140
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/wssnps/b/aa;

    sget-object v1, Lcom/wssnps/b/aa;->a:Lcom/wssnps/b/aa;

    aput-object v1, v0, v4

    sget-object v1, Lcom/wssnps/b/aa;->b:Lcom/wssnps/b/aa;

    aput-object v1, v0, v5

    sget-object v1, Lcom/wssnps/b/aa;->c:Lcom/wssnps/b/aa;

    aput-object v1, v0, v6

    sget-object v1, Lcom/wssnps/b/aa;->d:Lcom/wssnps/b/aa;

    aput-object v1, v0, v7

    sget-object v1, Lcom/wssnps/b/aa;->e:Lcom/wssnps/b/aa;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/wssnps/b/aa;->f:Lcom/wssnps/b/aa;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/wssnps/b/aa;->g:Lcom/wssnps/b/aa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/wssnps/b/aa;->h:Lcom/wssnps/b/aa;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/wssnps/b/aa;->i:Lcom/wssnps/b/aa;

    aput-object v2, v0, v1

    sput-object v0, Lcom/wssnps/b/aa;->k:[Lcom/wssnps/b/aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 156
    iput p3, p0, Lcom/wssnps/b/aa;->j:I

    .line 157
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/wssnps/b/aa;
    .locals 1

    .prologue
    .line 140
    const-class v0, Lcom/wssnps/b/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/aa;

    return-object v0
.end method

.method public static values()[Lcom/wssnps/b/aa;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/wssnps/b/aa;->k:[Lcom/wssnps/b/aa;

    invoke-virtual {v0}, [Lcom/wssnps/b/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/wssnps/b/aa;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/wssnps/b/aa;->j:I

    return v0
.end method
