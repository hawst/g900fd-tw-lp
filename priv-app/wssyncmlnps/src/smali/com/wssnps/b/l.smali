.class public Lcom/wssnps/b/l;
.super Ljava/lang/Object;
.source "smlBackupRestoreItem.java"

# interfaces
.implements Lcom/wssnps/c/a;


# static fields
.field public static final G:Landroid/net/Uri;

.field public static final H:Landroid/net/Uri;

.field public static final I:Landroid/net/Uri;

.field public static final J:Landroid/net/Uri;

.field public static final K:Landroid/net/Uri;

.field public static final a:Landroid/net/Uri;

.field public static aA:Z

.field public static aB:Z

.field public static aC:Z

.field public static aD:Z

.field public static aE:Z

.field public static aF:Z

.field public static aG:Z

.field public static aH:Z

.field public static aI:Z

.field public static aJ:Z

.field public static aK:I

.field public static aL:[I

.field public static aM:[I

.field public static aN:Z

.field public static aO:Z

.field public static aP:[B

.field public static aQ:I

.field static aR:I

.field public static aS:[B

.field static aT:[B

.field private static aU:Landroid/database/sqlite/SQLiteDatabase;

.field private static aV:Landroid/database/sqlite/SQLiteDatabase;

.field private static aW:I

.field private static aX:J

.field public static an:Landroid/content/Context;

.field public static ao:J

.field public static ap:Ljava/util/ArrayList;

.field static aq:I

.field public static ar:Lcom/wssnps/smlModelDefine;

.field public static as:I

.field static at:I

.field public static au:Z

.field public static av:Z

.field public static aw:Z

.field public static ax:Z

.field public static ay:Z

.field public static az:Z

.field public static final b:Landroid/net/Uri;

.field public static c:[Ljava/lang/String;

.field public static d:[Ljava/lang/String;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;

.field public static i:Ljava/lang/String;

.field public static final j:Landroid/net/Uri;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field public F:I

.field public L:I

.field public M:Ljava/lang/String;

.field public N:Ljava/lang/String;

.field public O:Ljava/lang/String;

.field public P:Ljava/lang/String;

.field public Q:Ljava/lang/String;

.field public R:Ljava/lang/String;

.field public S:Ljava/lang/String;

.field public T:Ljava/lang/String;

.field public U:Ljava/lang/String;

.field public V:Ljava/lang/String;

.field public W:Ljava/lang/String;

.field public X:Ljava/lang/String;

.field public Y:Ljava/lang/String;

.field public Z:Ljava/lang/String;

.field public aa:Ljava/lang/String;

.field public ab:Ljava/lang/String;

.field public ac:Ljava/lang/String;

.field public ad:Ljava/lang/String;

.field public ae:Ljava/lang/String;

.field public af:Ljava/lang/String;

.field public ag:Ljava/lang/String;

.field public ah:Ljava/lang/String;

.field public ai:Ljava/lang/String;

.field public aj:Ljava/lang/String;

.field public ak:Ljava/lang/String;

.field public al:Ljava/lang/String;

.field public am:Ljava/lang/String;

.field public k:I

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field public z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 46
    const-string v0, "content://settings/system"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->a:Landroid/net/Uri;

    .line 47
    const-string v0, "content://settings/global"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->b:Landroid/net/Uri;

    .line 53
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "volume_system%"

    aput-object v1, v0, v4

    const-string v1, "volume_ring%"

    aput-object v1, v0, v3

    const-string v1, "volume_notification%"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "volume_music%"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "volume_alarm%"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "volume_voice%"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "volume_bluetooth_sco%"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "volume_enforced%"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "video_call_ringtone"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "screen_off_timeout"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "screen_brightness"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "screen_brightness_mode"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "auto_brightness_detail"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "ringtone"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ringtone_2"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "notification_sound"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "notification_sound_2"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "lockscreen_sounds_enabled"

    aput-object v2, v0, v1

    sput-object v0, Lcom/wssnps/b/l;->c:[Ljava/lang/String;

    .line 75
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "mode_ringer"

    aput-object v1, v0, v4

    sput-object v0, Lcom/wssnps/b/l;->d:[Ljava/lang/String;

    .line 82
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/wssnps/b/l;->e:Landroid/net/Uri;

    .line 83
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/wssnps/b/l;->f:Landroid/net/Uri;

    .line 86
    const-string v0, "content://com.samsung.sec.android/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->g:Landroid/net/Uri;

    .line 87
    const-string v0, "content://com.sec.android.app/memo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->h:Landroid/net/Uri;

    .line 123
    const-string v0, "content://com.android.browser/bookmarks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->j:Landroid/net/Uri;

    .line 159
    const-string v0, "content://logs/call"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->G:Landroid/net/Uri;

    .line 160
    const-string v0, "content://logs/call"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->H:Landroid/net/Uri;

    .line 161
    const-string v0, "content://logs/video_call"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->I:Landroid/net/Uri;

    .line 162
    const-string v0, "content://logs/sms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->J:Landroid/net/Uri;

    .line 163
    const-string v0, "content://logs/mms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->K:Landroid/net/Uri;

    .line 233
    const/4 v0, 0x0

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    .line 240
    const/16 v0, 0x1e

    sput v0, Lcom/wssnps/b/l;->aq:I

    .line 273
    new-instance v0, Lcom/wssnps/smlModelDefine;

    invoke-direct {v0}, Lcom/wssnps/smlModelDefine;-><init>()V

    sput-object v0, Lcom/wssnps/b/l;->ar:Lcom/wssnps/smlModelDefine;

    .line 380
    sput v4, Lcom/wssnps/b/l;->as:I

    .line 382
    sput v4, Lcom/wssnps/b/l;->at:I

    .line 384
    sput-boolean v3, Lcom/wssnps/b/l;->au:Z

    .line 385
    sput-boolean v3, Lcom/wssnps/b/l;->av:Z

    .line 386
    sput-boolean v3, Lcom/wssnps/b/l;->aw:Z

    .line 387
    sput-boolean v3, Lcom/wssnps/b/l;->ax:Z

    .line 388
    sput-boolean v3, Lcom/wssnps/b/l;->ay:Z

    .line 389
    sput-boolean v3, Lcom/wssnps/b/l;->az:Z

    .line 390
    sput-boolean v3, Lcom/wssnps/b/l;->aA:Z

    .line 391
    sput-boolean v3, Lcom/wssnps/b/l;->aB:Z

    .line 392
    sput-boolean v3, Lcom/wssnps/b/l;->aC:Z

    .line 393
    sput-boolean v3, Lcom/wssnps/b/l;->aD:Z

    .line 394
    sput-boolean v3, Lcom/wssnps/b/l;->aE:Z

    .line 395
    sput-boolean v3, Lcom/wssnps/b/l;->aF:Z

    .line 396
    sput-boolean v3, Lcom/wssnps/b/l;->aG:Z

    .line 397
    sput-boolean v3, Lcom/wssnps/b/l;->aH:Z

    .line 399
    sput-boolean v3, Lcom/wssnps/b/l;->aI:Z

    .line 400
    sput-boolean v3, Lcom/wssnps/b/l;->aJ:Z

    .line 402
    sput v4, Lcom/wssnps/b/l;->aK:I

    .line 406
    sput-boolean v4, Lcom/wssnps/b/l;->aN:Z

    .line 407
    sput-boolean v4, Lcom/wssnps/b/l;->aO:Z

    .line 3723
    sput v4, Lcom/wssnps/b/l;->aQ:I

    .line 3724
    sput v4, Lcom/wssnps/b/l;->aR:I

    .line 3726
    new-array v0, v5, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/wssnps/b/l;->aS:[B

    .line 3727
    new-array v0, v5, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/wssnps/b/l;->aT:[B

    return-void

    .line 3726
    :array_0
    .array-data 1
        0x0t
        0x1t
    .end array-data

    .line 3727
    nop

    :array_1
    .array-data 1
        0x0t
        0x2t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    return-void
.end method

.method public static A()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2485
    const/4 v0, 0x1

    const-string v1, "smlBREmailXMLBackup"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2486
    sput-boolean v2, Lcom/wssnps/b/l;->ay:Z

    .line 2487
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 2488
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.email.service.BROADCAST_DETECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2489
    const-string v1, "backup_Or_Restore"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2490
    const-string v1, "backup_Path"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/Email"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2491
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2492
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    const-string v2, "com.sec.android.email.service.BROADCAST_DETECT"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2493
    return-void
.end method

.method public static B()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2497
    const-string v0, "smlBREmailXMLRestore"

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2498
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssnps/b/l;->az:Z

    .line 2499
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 2500
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.email.service.BROADCAST_DETECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2501
    const-string v1, "backup_Or_Restore"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2502
    const-string v1, "backup_Path"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/Email"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2503
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2504
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    const-string v2, "com.sec.android.email.service.BROADCAST_DETECT"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 2505
    return-void
.end method

.method public static C()V
    .locals 3

    .prologue
    .line 2509
    const/4 v0, 0x1

    const-string v1, "smlBREmailCompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2510
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Email"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Email.bk"

    invoke-static {v0, v1, v2}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2511
    return-void
.end method

.method public static D()V
    .locals 3

    .prologue
    .line 2515
    const/4 v0, 0x1

    const-string v1, "smlBREmailDecompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Email.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/Email"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2517
    return-void
.end method

.method public static E()V
    .locals 2

    .prologue
    .line 2521
    const/4 v0, 0x1

    const-string v1, "smlBREmailDelete"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2522
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Email"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 2523
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Email.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 2524
    return-void
.end method

.method public static F()V
    .locals 2

    .prologue
    .line 2528
    const/4 v0, 0x1

    const-string v1, "smlBRNetworkBackup"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2529
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Network"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 2531
    invoke-static {}, Lcom/wssnps/smlModelDefine;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2533
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Network/Network_DB.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    .line 2534
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "create table if not exists bookmarks (_id integer primary key autoincrement, title text, url text, folder integer, parent integer, position integer, insert_after integer, deleted integer, account_name text, account_type text, sourceid text, version integer, created integer, modified integer, dirty integer, sync1 text, sync2 text, sync3 text, sync4 text, sync5 text, parent_title text );"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2536
    invoke-static {}, Lcom/wssnps/b/l;->H()Ljava/lang/Object;

    .line 2537
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 2540
    :cond_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2542
    invoke-static {}, Lcom/wssnps/b/l;->J()V

    .line 2555
    :goto_0
    return-void

    .line 2548
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->L()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2550
    :catch_0
    move-exception v0

    .line 2552
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static G()V
    .locals 4

    .prologue
    .line 2559
    const/4 v0, 0x1

    const-string v1, "smlBRNetworkRestore"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2563
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->M()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2570
    :goto_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2571
    invoke-static {}, Lcom/wssnps/b/l;->I()Ljava/lang/Object;

    .line 2573
    :cond_0
    invoke-static {}, Lcom/wssnps/smlModelDefine;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2574
    invoke-static {}, Lcom/wssnps/b/l;->K()V

    .line 2575
    :cond_1
    return-void

    .line 2565
    :catch_0
    move-exception v0

    .line 2567
    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "smlBRNetworkDecompress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static H()Ljava/lang/Object;
    .locals 30

    .prologue
    .line 2583
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2584
    const/4 v7, 0x0

    .line 2586
    sget-object v2, Lcom/wssnps/b/l;->j:Landroid/net/Uri;

    .line 2588
    sget-object v3, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2589
    const-string v6, "folder desc"

    .line 2590
    const/4 v3, 0x0

    const-string v4, "_id>1 "

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2591
    if-eqz v8, :cond_18

    .line 2593
    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 2594
    const-string v3, "title"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 2595
    const-string v3, "url"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 2596
    const-string v3, "folder"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 2597
    const-string v3, "parent"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 2598
    const-string v3, "position"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 2599
    const-string v3, "insert_after"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 2600
    const-string v3, "deleted"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 2601
    const-string v3, "account_name"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 2602
    const-string v3, "account_type"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 2603
    const-string v3, "sourceid"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 2604
    const-string v3, "version"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 2605
    const-string v3, "created"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 2606
    const-string v3, "modified"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 2607
    const-string v3, "dirty"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 2608
    const-string v3, "sync1"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 2609
    const-string v3, "sync2"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 2610
    const-string v3, "sync3"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 2611
    const-string v3, "sync4"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 2612
    const-string v3, "sync5"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 2613
    const-string v3, "expanded"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 2617
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-gtz v3, :cond_0

    .line 2619
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2620
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2695
    :goto_0
    return-object v1

    .line 2623
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 2627
    :goto_1
    new-instance v29, Lcom/wssnps/b/l;

    invoke-direct/range {v29 .. v29}, Lcom/wssnps/b/l;-><init>()V

    .line 2628
    const/4 v3, -0x1

    if-eq v9, v3, :cond_1

    .line 2629
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->k:I

    .line 2630
    :cond_1
    const/4 v3, -0x1

    if-eq v10, v3, :cond_2

    .line 2631
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->l:Ljava/lang/String;

    .line 2632
    :cond_2
    const/4 v3, -0x1

    if-eq v11, v3, :cond_3

    .line 2633
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->m:Ljava/lang/String;

    .line 2634
    :cond_3
    const/4 v3, -0x1

    if-eq v12, v3, :cond_4

    .line 2635
    invoke-interface {v8, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->n:I

    .line 2636
    :cond_4
    const/4 v3, -0x1

    if-eq v13, v3, :cond_5

    .line 2637
    invoke-interface {v8, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->o:I

    .line 2638
    :cond_5
    const/4 v3, -0x1

    if-eq v14, v3, :cond_6

    .line 2639
    invoke-interface {v8, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->p:I

    .line 2640
    :cond_6
    const/4 v3, -0x1

    if-eq v15, v3, :cond_7

    .line 2641
    invoke-interface {v8, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->q:I

    .line 2642
    :cond_7
    const/4 v3, -0x1

    move/from16 v0, v16

    if-eq v0, v3, :cond_8

    .line 2643
    move/from16 v0, v16

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->r:I

    .line 2644
    :cond_8
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_9

    .line 2645
    move/from16 v0, v17

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->s:Ljava/lang/String;

    .line 2646
    :cond_9
    const/4 v3, -0x1

    move/from16 v0, v18

    if-eq v0, v3, :cond_a

    .line 2647
    move/from16 v0, v18

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->t:Ljava/lang/String;

    .line 2648
    :cond_a
    const/4 v3, -0x1

    move/from16 v0, v19

    if-eq v0, v3, :cond_b

    .line 2649
    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->u:Ljava/lang/String;

    .line 2650
    :cond_b
    const/4 v3, -0x1

    move/from16 v0, v20

    if-eq v0, v3, :cond_c

    .line 2651
    move/from16 v0, v20

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->v:I

    .line 2652
    :cond_c
    const/4 v3, -0x1

    move/from16 v0, v21

    if-eq v0, v3, :cond_d

    .line 2653
    move/from16 v0, v21

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->w:I

    .line 2654
    :cond_d
    const/4 v3, -0x1

    move/from16 v0, v22

    if-eq v0, v3, :cond_e

    .line 2655
    move/from16 v0, v22

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->x:I

    .line 2656
    :cond_e
    const/4 v3, -0x1

    move/from16 v0, v23

    if-eq v0, v3, :cond_f

    .line 2657
    move/from16 v0, v23

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    iput v3, v0, Lcom/wssnps/b/l;->y:I

    .line 2658
    :cond_f
    const/4 v3, -0x1

    move/from16 v0, v24

    if-eq v0, v3, :cond_10

    .line 2659
    move/from16 v0, v24

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->z:Ljava/lang/String;

    .line 2660
    :cond_10
    const/4 v3, -0x1

    move/from16 v0, v25

    if-eq v0, v3, :cond_11

    .line 2661
    move/from16 v0, v25

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->A:Ljava/lang/String;

    .line 2662
    :cond_11
    const/4 v3, -0x1

    move/from16 v0, v26

    if-eq v0, v3, :cond_12

    .line 2663
    move/from16 v0, v26

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->B:Ljava/lang/String;

    .line 2664
    :cond_12
    const/4 v3, -0x1

    move/from16 v0, v27

    if-eq v0, v3, :cond_13

    .line 2665
    move/from16 v0, v27

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->C:Ljava/lang/String;

    .line 2666
    :cond_13
    const/4 v3, -0x1

    move/from16 v0, v28

    if-eq v0, v3, :cond_14

    .line 2667
    move/from16 v0, v28

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    iput-object v3, v0, Lcom/wssnps/b/l;->D:Ljava/lang/String;

    .line 2668
    :cond_14
    const/4 v3, -0x1

    if-eq v13, v3, :cond_16

    move-object/from16 v0, v29

    iget v3, v0, Lcom/wssnps/b/l;->o:I

    if-eqz v3, :cond_16

    move-object/from16 v0, v29

    iget v3, v0, Lcom/wssnps/b/l;->o:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_16

    .line 2670
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v29

    iget v4, v0, Lcom/wssnps/b/l;->o:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2671
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2673
    if-eqz v3, :cond_16

    .line 2675
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_15

    .line 2677
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 2679
    invoke-interface {v3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v29

    iput-object v4, v0, Lcom/wssnps/b/l;->E:Ljava/lang/String;

    .line 2682
    :cond_15
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2686
    :cond_16
    sget-object v3, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    move-object/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2687
    add-int/lit8 v3, v7, 0x1

    .line 2689
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_19

    .line 2691
    :cond_17
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2693
    sget-object v1, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/wssnps/b/l;->a(Ljava/util/ArrayList;)V

    .line 2695
    :cond_18
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_19
    move v7, v3

    goto/16 :goto_1
.end method

.method public static I()Ljava/lang/Object;
    .locals 27

    .prologue
    .line 2746
    const/4 v12, 0x0

    .line 2770
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 2771
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/Network/Network_DB.bk"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sput-object v2, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    .line 2773
    sget-object v2, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2774
    const-string v10, "folder desc"

    .line 2775
    sget-object v2, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "bookmarks"

    const/4 v5, 0x0

    const-string v6, "_id>0"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2777
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "smlBRRestoreBookmarkSql count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2779
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-gtz v3, :cond_0

    .line 2781
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2782
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2967
    :goto_0
    return-object v2

    .line 2785
    :cond_0
    const-string v3, "_id"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 2786
    const-string v3, "title"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 2787
    const-string v3, "url"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 2788
    const-string v3, "folder"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 2789
    const-string v3, "parent"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 2790
    const-string v4, "position"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 2791
    const-string v5, "insert_after"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 2792
    const-string v6, "deleted"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 2793
    const-string v7, "account_name"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 2794
    const-string v8, "account_type"

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 2795
    const-string v9, "sourceid"

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 2796
    const-string v10, "version"

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 2797
    const-string v11, "created"

    invoke-interface {v2, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 2798
    const-string v17, "modified"

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 2799
    const-string v18, "dirty"

    move-object/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 2800
    const-string v19, "sync1"

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 2801
    const-string v20, "sync2"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 2802
    const-string v21, "sync3"

    move-object/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 2803
    const-string v22, "sync4"

    move-object/from16 v0, v22

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 2804
    const-string v23, "sync5"

    move-object/from16 v0, v23

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 2805
    const-string v24, "expanded"

    move-object/from16 v0, v24

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 2806
    const-string v24, "parent_title"

    move-object/from16 v0, v24

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 2809
    const-string v25, "bookmark"

    move-object/from16 v0, v25

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 2810
    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_12

    .line 2812
    const/4 v3, 0x1

    const-string v4, "smlBRRestoreBookmarkSql GB DATA"

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2813
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2816
    sget-object v2, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "bookmarks"

    const/4 v5, 0x0

    const-string v6, "bookmark=2"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 2817
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2821
    :goto_1
    new-instance v18, Lcom/wssnps/b/l;

    invoke-direct/range {v18 .. v18}, Lcom/wssnps/b/l;-><init>()V

    .line 2822
    const/4 v2, -0x1

    if-eq v13, v2, :cond_1

    .line 2823
    move-object/from16 v0, v17

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->k:I

    .line 2824
    :cond_1
    const/4 v2, -0x1

    if-eq v14, v2, :cond_2

    .line 2825
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/wssnps/b/l;->l:Ljava/lang/String;

    .line 2826
    :cond_2
    const/4 v2, -0x1

    if-eq v15, v2, :cond_3

    .line 2827
    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/wssnps/b/l;->m:Ljava/lang/String;

    .line 2828
    :cond_3
    const/4 v2, -0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_4

    .line 2829
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->n:I

    .line 2830
    :cond_4
    const/4 v2, -0x1

    move/from16 v0, v25

    if-eq v0, v2, :cond_5

    .line 2831
    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->F:I

    .line 2833
    :cond_5
    move-object/from16 v0, v18

    iget v2, v0, Lcom/wssnps/b/l;->n:I

    if-nez v2, :cond_e

    .line 2835
    const/4 v2, 0x1

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->n:I

    .line 2836
    const/4 v2, 0x1

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->o:I

    .line 2856
    :goto_2
    sget-object v2, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2857
    add-int/lit8 v2, v12, 0x1

    .line 2858
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2a

    move v12, v2

    .line 2860
    :cond_6
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 2863
    sget-object v2, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "bookmarks"

    const/4 v5, 0x0

    const-string v6, "bookmark=1"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 2864
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2868
    :cond_7
    new-instance v18, Lcom/wssnps/b/l;

    invoke-direct/range {v18 .. v18}, Lcom/wssnps/b/l;-><init>()V

    .line 2869
    const/4 v2, -0x1

    if-eq v13, v2, :cond_8

    .line 2870
    move-object/from16 v0, v17

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->k:I

    .line 2871
    :cond_8
    const/4 v2, -0x1

    if-eq v14, v2, :cond_9

    .line 2872
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/wssnps/b/l;->l:Ljava/lang/String;

    .line 2873
    :cond_9
    const/4 v2, -0x1

    if-eq v15, v2, :cond_a

    .line 2874
    move-object/from16 v0, v17

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/wssnps/b/l;->m:Ljava/lang/String;

    .line 2875
    :cond_a
    const/4 v2, -0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_b

    .line 2876
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->n:I

    .line 2877
    :cond_b
    const/4 v2, -0x1

    move/from16 v0, v25

    if-eq v0, v2, :cond_c

    .line 2878
    move-object/from16 v0, v17

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->F:I

    .line 2880
    :cond_c
    move-object/from16 v0, v18

    iget v2, v0, Lcom/wssnps/b/l;->n:I

    if-nez v2, :cond_10

    .line 2882
    const/4 v2, 0x0

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->n:I

    .line 2883
    const/4 v2, 0x1

    move-object/from16 v0, v18

    iput v2, v0, Lcom/wssnps/b/l;->o:I

    .line 2903
    :goto_3
    sget-object v2, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2904
    add-int/lit8 v12, v12, 0x1

    .line 2905
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2907
    :cond_d
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 2908
    sget-object v2, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 2965
    :goto_4
    sget-object v2, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/wssnps/b/l;->b(Ljava/util/ArrayList;)Ljava/lang/Object;

    .line 2966
    sget-object v2, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 2967
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0

    .line 2841
    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/Network/Network_DB.bk"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v19

    .line 2842
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    iget v3, v0, Lcom/wssnps/b/l;->n:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2843
    sget-object v2, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "bookmarks"

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2844
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_f

    .line 2846
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 2848
    invoke-interface {v2, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/wssnps/b/l;->E:Ljava/lang/String;

    .line 2849
    const/4 v3, 0x1

    move-object/from16 v0, v18

    iput v3, v0, Lcom/wssnps/b/l;->n:I

    .line 2850
    const/4 v3, 0x2

    move-object/from16 v0, v18

    iput v3, v0, Lcom/wssnps/b/l;->o:I

    .line 2853
    :cond_f
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2854
    invoke-virtual/range {v19 .. v19}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto/16 :goto_2

    .line 2888
    :cond_10
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/Network/Network_DB.bk"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v19

    .line 2889
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    iget v3, v0, Lcom/wssnps/b/l;->n:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2890
    sget-object v2, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "bookmarks"

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2891
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_11

    .line 2893
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 2895
    invoke-interface {v2, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    iput-object v3, v0, Lcom/wssnps/b/l;->E:Ljava/lang/String;

    .line 2896
    const/4 v3, 0x0

    move-object/from16 v0, v18

    iput v3, v0, Lcom/wssnps/b/l;->n:I

    .line 2897
    const/4 v3, 0x2

    move-object/from16 v0, v18

    iput v3, v0, Lcom/wssnps/b/l;->o:I

    .line 2900
    :cond_11
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2901
    invoke-virtual/range {v19 .. v19}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto/16 :goto_3

    .line 2912
    :cond_12
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v25

    if-eqz v25, :cond_29

    .line 2916
    :cond_13
    new-instance v25, Lcom/wssnps/b/l;

    invoke-direct/range {v25 .. v25}, Lcom/wssnps/b/l;-><init>()V

    .line 2917
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v13, v0, :cond_14

    .line 2918
    invoke-interface {v2, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->k:I

    .line 2919
    :cond_14
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v14, v0, :cond_15

    .line 2920
    invoke-interface {v2, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->l:Ljava/lang/String;

    .line 2921
    :cond_15
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v15, v0, :cond_16

    .line 2922
    invoke-interface {v2, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->m:Ljava/lang/String;

    .line 2923
    :cond_16
    const/16 v26, -0x1

    move/from16 v0, v16

    move/from16 v1, v26

    if-eq v0, v1, :cond_17

    .line 2924
    move/from16 v0, v16

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->n:I

    .line 2925
    :cond_17
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v3, v0, :cond_18

    .line 2926
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->o:I

    .line 2927
    :cond_18
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v4, v0, :cond_19

    .line 2928
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->p:I

    .line 2929
    :cond_19
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v5, v0, :cond_1a

    .line 2930
    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->q:I

    .line 2931
    :cond_1a
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v6, v0, :cond_1b

    .line 2932
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->r:I

    .line 2933
    :cond_1b
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v7, v0, :cond_1c

    .line 2934
    invoke-interface {v2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->s:Ljava/lang/String;

    .line 2935
    :cond_1c
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v8, v0, :cond_1d

    .line 2936
    invoke-interface {v2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->t:Ljava/lang/String;

    .line 2937
    :cond_1d
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v9, v0, :cond_1e

    .line 2938
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->u:Ljava/lang/String;

    .line 2939
    :cond_1e
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v10, v0, :cond_1f

    .line 2940
    invoke-interface {v2, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->v:I

    .line 2941
    :cond_1f
    const/16 v26, -0x1

    move/from16 v0, v26

    if-eq v11, v0, :cond_20

    .line 2942
    invoke-interface {v2, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->w:I

    .line 2943
    :cond_20
    const/16 v26, -0x1

    move/from16 v0, v17

    move/from16 v1, v26

    if-eq v0, v1, :cond_21

    .line 2944
    move/from16 v0, v17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->x:I

    .line 2945
    :cond_21
    const/16 v26, -0x1

    move/from16 v0, v18

    move/from16 v1, v26

    if-eq v0, v1, :cond_22

    .line 2946
    move/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/wssnps/b/l;->y:I

    .line 2947
    :cond_22
    const/16 v26, -0x1

    move/from16 v0, v19

    move/from16 v1, v26

    if-eq v0, v1, :cond_23

    .line 2948
    move/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->z:Ljava/lang/String;

    .line 2949
    :cond_23
    const/16 v26, -0x1

    move/from16 v0, v20

    move/from16 v1, v26

    if-eq v0, v1, :cond_24

    .line 2950
    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->A:Ljava/lang/String;

    .line 2951
    :cond_24
    const/16 v26, -0x1

    move/from16 v0, v21

    move/from16 v1, v26

    if-eq v0, v1, :cond_25

    .line 2952
    move/from16 v0, v21

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->B:Ljava/lang/String;

    .line 2953
    :cond_25
    const/16 v26, -0x1

    move/from16 v0, v22

    move/from16 v1, v26

    if-eq v0, v1, :cond_26

    .line 2954
    move/from16 v0, v22

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->C:Ljava/lang/String;

    .line 2955
    :cond_26
    const/16 v26, -0x1

    move/from16 v0, v23

    move/from16 v1, v26

    if-eq v0, v1, :cond_27

    .line 2956
    move/from16 v0, v23

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->D:Ljava/lang/String;

    .line 2957
    :cond_27
    const/16 v26, -0x1

    move/from16 v0, v24

    move/from16 v1, v26

    if-eq v0, v1, :cond_28

    .line 2958
    move/from16 v0, v24

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/wssnps/b/l;->E:Ljava/lang/String;

    .line 2959
    :cond_28
    sget-object v26, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2960
    add-int/lit8 v12, v12, 0x1

    .line 2961
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v25

    if-nez v25, :cond_13

    .line 2963
    :cond_29
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_4

    :cond_2a
    move v12, v2

    goto/16 :goto_1
.end method

.method public static J()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3029
    const/4 v0, 0x1

    const-string v1, "smlBRRadioBackup"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3030
    sput-boolean v2, Lcom/wssnps/b/l;->aA:Z

    .line 3031
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3032
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.fm.BROADCAST_DETECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3033
    const-string v1, "backup_Or_Restore"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3034
    const-string v1, "backup_Path"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/Network"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3035
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3036
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3037
    return-void
.end method

.method public static K()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 3041
    const-string v0, "smlBRRadioRestore"

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3042
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssnps/b/l;->aB:Z

    .line 3043
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3044
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.fm.BROADCAST_DETECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3045
    const-string v1, "backup_Or_Restore"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3046
    const-string v1, "backup_Path"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/Network"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3047
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3048
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3049
    return-void
.end method

.method public static L()V
    .locals 3

    .prologue
    .line 3053
    const/4 v0, 0x1

    const-string v1, "smlBRNetworkCompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3054
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Network"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Network.bk"

    invoke-static {v0, v1, v2}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3055
    return-void
.end method

.method public static M()V
    .locals 3

    .prologue
    .line 3059
    const/4 v0, 0x1

    const-string v1, "smlBRNetworkDecompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3060
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Network.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/Network"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3061
    return-void
.end method

.method public static N()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 3065
    sget-object v0, Lcom/wssnps/b/l;->j:Landroid/net/Uri;

    .line 3066
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 3068
    const/4 v2, 0x1

    const-string v3, "smlBRBookmarkDelete"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3069
    const-string v2, "_id>1"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3071
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static O()V
    .locals 2

    .prologue
    .line 3076
    const/4 v0, 0x1

    const-string v1, "smlBRNetworkDelete"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3077
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Network"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 3078
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Network.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 3079
    return-void
.end method

.method public static P()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3083
    const-string v0, "smlBRCalllogBackup"

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3084
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/CallLog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 3086
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/CallLog/CallLog_DB.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->aV:Landroid/database/sqlite/SQLiteDatabase;

    .line 3087
    sget-object v0, Lcom/wssnps/b/l;->aV:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "create table if not exists calllog (_id integer primary key autoincrement, number text, sim_id text, simnum text, address text, date text, duration text, type text, new text, name text, numbertype text, numberlabel text, messageid text, logtype text, frequent text, contactid text, raw_contact_id text, m_subject text, m_content text, sns_tid text, sns_pkey text, account_name text, account_id text, sns_receiver_count text, sp_type text, cnap_name text, cdnip_number text, service_type text);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3089
    invoke-static {}, Lcom/wssnps/b/l;->R()Ljava/lang/Object;

    .line 3092
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->T()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3098
    :goto_0
    sput-boolean v2, Lcom/wssnps/b/l;->aw:Z

    .line 3099
    sget-object v0, Lcom/wssnps/b/l;->aV:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 3100
    return-void

    .line 3094
    :catch_0
    move-exception v0

    .line 3096
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static Q()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3104
    const-string v0, "smlBRCalllogRestore "

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3107
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->U()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3113
    :goto_0
    invoke-static {}, Lcom/wssnps/b/l;->S()Ljava/lang/Object;

    .line 3114
    sput-boolean v1, Lcom/wssnps/b/l;->ax:Z

    .line 3115
    return-void

    .line 3109
    :catch_0
    move-exception v0

    .line 3111
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static R()Ljava/lang/Object;
    .locals 34

    .prologue
    .line 3121
    const/4 v8, 0x0

    .line 3123
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 3124
    const/4 v3, 0x1

    const-string v4, "smlBRCalllogGet"

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3126
    sget-object v3, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 3127
    sget-object v3, Lcom/wssnps/b/l;->G:Landroid/net/Uri;

    .line 3129
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 3130
    if-eqz v3, :cond_1f

    .line 3132
    const-string v2, "_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 3133
    const-string v2, "number"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 3134
    const-string v2, "sim_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 3135
    const-string v2, "simnum"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 3136
    const-string v2, "address"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 3137
    const-string v2, "date"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 3138
    const-string v2, "duration"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 3139
    const-string v2, "type"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 3140
    const-string v2, "new"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 3141
    const-string v2, "name"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 3142
    const-string v2, "numbertype"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 3143
    const-string v2, "numberlabel"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 3144
    const-string v2, "messageid"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 3145
    const-string v2, "logtype"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 3146
    const-string v2, "frequent"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 3147
    const-string v2, "contactid"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 3148
    const-string v2, "raw_contact_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 3149
    const-string v2, "m_subject"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 3150
    const-string v2, "m_content"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 3151
    const-string v2, "sns_tid"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 3152
    const-string v2, "sns_pkey"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 3153
    const-string v2, "account_name"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 3154
    const-string v2, "account_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 3155
    const-string v2, "sns_receiver_count"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 3156
    const-string v2, "sp_type"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 3157
    const-string v2, "cnap_name"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 3158
    const-string v2, "cdnip_number"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 3159
    const-string v2, "service_type"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 3163
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_0

    .line 3165
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 3166
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3240
    :goto_0
    return-object v2

    .line 3169
    :cond_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1e

    move v2, v8

    .line 3173
    :cond_1
    new-instance v8, Lcom/wssnps/b/l;

    invoke-direct {v8}, Lcom/wssnps/b/l;-><init>()V

    .line 3174
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v4, v0, :cond_2

    .line 3175
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    move/from16 v0, v33

    iput v0, v8, Lcom/wssnps/b/l;->L:I

    .line 3176
    :cond_2
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v5, v0, :cond_3

    .line 3177
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->M:Ljava/lang/String;

    .line 3178
    :cond_3
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v6, v0, :cond_4

    .line 3179
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->N:Ljava/lang/String;

    .line 3180
    :cond_4
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v7, v0, :cond_5

    .line 3181
    invoke-interface {v3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->O:Ljava/lang/String;

    .line 3182
    :cond_5
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v9, v0, :cond_6

    .line 3183
    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->P:Ljava/lang/String;

    .line 3184
    :cond_6
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v10, v0, :cond_7

    .line 3185
    invoke-interface {v3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->Q:Ljava/lang/String;

    .line 3186
    :cond_7
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v11, v0, :cond_8

    .line 3187
    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->R:Ljava/lang/String;

    .line 3188
    :cond_8
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v12, v0, :cond_9

    .line 3189
    invoke-interface {v3, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->S:Ljava/lang/String;

    .line 3190
    :cond_9
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v13, v0, :cond_a

    .line 3191
    invoke-interface {v3, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->T:Ljava/lang/String;

    .line 3192
    :cond_a
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v14, v0, :cond_b

    .line 3193
    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->U:Ljava/lang/String;

    .line 3194
    :cond_b
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v15, v0, :cond_c

    .line 3195
    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->V:Ljava/lang/String;

    .line 3196
    :cond_c
    const/16 v33, -0x1

    move/from16 v0, v16

    move/from16 v1, v33

    if-eq v0, v1, :cond_d

    .line 3197
    move/from16 v0, v16

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->W:Ljava/lang/String;

    .line 3198
    :cond_d
    const/16 v33, -0x1

    move/from16 v0, v17

    move/from16 v1, v33

    if-eq v0, v1, :cond_e

    .line 3199
    move/from16 v0, v17

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->X:Ljava/lang/String;

    .line 3200
    :cond_e
    const/16 v33, -0x1

    move/from16 v0, v18

    move/from16 v1, v33

    if-eq v0, v1, :cond_f

    .line 3201
    move/from16 v0, v18

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->Y:Ljava/lang/String;

    .line 3202
    :cond_f
    const/16 v33, -0x1

    move/from16 v0, v19

    move/from16 v1, v33

    if-eq v0, v1, :cond_10

    .line 3203
    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->Z:Ljava/lang/String;

    .line 3204
    :cond_10
    const/16 v33, -0x1

    move/from16 v0, v20

    move/from16 v1, v33

    if-eq v0, v1, :cond_11

    .line 3205
    move/from16 v0, v20

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->aa:Ljava/lang/String;

    .line 3206
    :cond_11
    const/16 v33, -0x1

    move/from16 v0, v21

    move/from16 v1, v33

    if-eq v0, v1, :cond_12

    .line 3207
    move/from16 v0, v21

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->ab:Ljava/lang/String;

    .line 3208
    :cond_12
    const/16 v33, -0x1

    move/from16 v0, v22

    move/from16 v1, v33

    if-eq v0, v1, :cond_13

    .line 3209
    move/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->ac:Ljava/lang/String;

    .line 3210
    :cond_13
    const/16 v33, -0x1

    move/from16 v0, v23

    move/from16 v1, v33

    if-eq v0, v1, :cond_14

    .line 3211
    move/from16 v0, v23

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->ad:Ljava/lang/String;

    .line 3212
    :cond_14
    const/16 v33, -0x1

    move/from16 v0, v24

    move/from16 v1, v33

    if-eq v0, v1, :cond_15

    .line 3213
    move/from16 v0, v24

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->ae:Ljava/lang/String;

    .line 3214
    :cond_15
    const/16 v33, -0x1

    move/from16 v0, v25

    move/from16 v1, v33

    if-eq v0, v1, :cond_16

    .line 3215
    move/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->af:Ljava/lang/String;

    .line 3216
    :cond_16
    const/16 v33, -0x1

    move/from16 v0, v26

    move/from16 v1, v33

    if-eq v0, v1, :cond_17

    .line 3217
    move/from16 v0, v26

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->ag:Ljava/lang/String;

    .line 3218
    :cond_17
    const/16 v33, -0x1

    move/from16 v0, v27

    move/from16 v1, v33

    if-eq v0, v1, :cond_18

    .line 3219
    move/from16 v0, v27

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->ah:Ljava/lang/String;

    .line 3220
    :cond_18
    const/16 v33, -0x1

    move/from16 v0, v28

    move/from16 v1, v33

    if-eq v0, v1, :cond_19

    .line 3221
    move/from16 v0, v28

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->ai:Ljava/lang/String;

    .line 3222
    :cond_19
    const/16 v33, -0x1

    move/from16 v0, v29

    move/from16 v1, v33

    if-eq v0, v1, :cond_1a

    .line 3223
    move/from16 v0, v29

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->aj:Ljava/lang/String;

    .line 3224
    :cond_1a
    const/16 v33, -0x1

    move/from16 v0, v30

    move/from16 v1, v33

    if-eq v0, v1, :cond_1b

    .line 3225
    move/from16 v0, v30

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->ak:Ljava/lang/String;

    .line 3226
    :cond_1b
    const/16 v33, -0x1

    move/from16 v0, v31

    move/from16 v1, v33

    if-eq v0, v1, :cond_1c

    .line 3227
    move/from16 v0, v31

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->al:Ljava/lang/String;

    .line 3228
    :cond_1c
    const/16 v33, -0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_1d

    .line 3229
    move/from16 v0, v32

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v8, Lcom/wssnps/b/l;->am:Ljava/lang/String;

    .line 3231
    :cond_1d
    sget-object v33, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3233
    add-int/lit8 v2, v2, 0x1

    .line 3234
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_1

    .line 3236
    :cond_1e
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 3237
    sget-object v2, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/wssnps/b/l;->c(Ljava/util/ArrayList;)V

    .line 3240
    :cond_1f
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public static S()Ljava/lang/Object;
    .locals 34

    .prologue
    .line 3296
    const/4 v12, 0x0

    .line 3300
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3301
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/CallLog/CallLog_DB.bk"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sput-object v2, Lcom/wssnps/b/l;->aV:Landroid/database/sqlite/SQLiteDatabase;

    .line 3302
    sget-object v2, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 3304
    sget-object v2, Lcom/wssnps/b/l;->aV:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x1

    const-string v4, "calllog"

    const/4 v5, 0x0

    const-string v6, "_id>0"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 3305
    if-eqz v3, :cond_1f

    .line 3309
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_0

    .line 3311
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 3312
    sget-object v2, Lcom/wssnps/b/l;->aV:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 3313
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 3416
    :goto_0
    return-object v2

    .line 3316
    :cond_0
    const-string v2, "_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 3317
    const-string v2, "number"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 3318
    const-string v2, "sim_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 3319
    const-string v2, "simnum"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 3320
    const-string v2, "address"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 3321
    const-string v2, "date"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 3322
    const-string v2, "duration"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 3323
    const-string v2, "type"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 3324
    const-string v2, "new"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 3325
    const-string v2, "name"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 3326
    const-string v2, "numbertype"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 3327
    const-string v2, "numberlabel"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 3328
    const-string v2, "messageid"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 3329
    const-string v2, "logtype"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 3330
    const-string v2, "frequent"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 3331
    const-string v2, "contactid"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 3332
    const-string v2, "raw_contact_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 3333
    const-string v2, "m_subject"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 3334
    const-string v2, "m_content"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 3335
    const-string v2, "sns_tid"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 3336
    const-string v2, "sns_pkey"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 3337
    const-string v2, "account_name"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 3338
    const-string v2, "account_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 3339
    const-string v2, "sns_receiver_count"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 3340
    const-string v2, "sp_type"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 3341
    const-string v2, "cnap_name"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 3342
    const-string v2, "cdnip_number"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 3343
    const-string v2, "service_type"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 3345
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1e

    move v2, v12

    .line 3349
    :cond_1
    new-instance v12, Lcom/wssnps/b/l;

    invoke-direct {v12}, Lcom/wssnps/b/l;-><init>()V

    .line 3350
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v4, v0, :cond_2

    .line 3351
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    move/from16 v0, v33

    iput v0, v12, Lcom/wssnps/b/l;->L:I

    .line 3352
    :cond_2
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v5, v0, :cond_3

    .line 3353
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->M:Ljava/lang/String;

    .line 3354
    :cond_3
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v6, v0, :cond_4

    .line 3355
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->N:Ljava/lang/String;

    .line 3356
    :cond_4
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v7, v0, :cond_5

    .line 3357
    invoke-interface {v3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->O:Ljava/lang/String;

    .line 3358
    :cond_5
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v8, v0, :cond_6

    .line 3359
    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->P:Ljava/lang/String;

    .line 3360
    :cond_6
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v9, v0, :cond_7

    .line 3361
    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->Q:Ljava/lang/String;

    .line 3362
    :cond_7
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v10, v0, :cond_8

    .line 3363
    invoke-interface {v3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->R:Ljava/lang/String;

    .line 3364
    :cond_8
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v11, v0, :cond_9

    .line 3365
    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->S:Ljava/lang/String;

    .line 3366
    :cond_9
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v13, v0, :cond_a

    .line 3367
    invoke-interface {v3, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->T:Ljava/lang/String;

    .line 3368
    :cond_a
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v14, v0, :cond_b

    .line 3369
    invoke-interface {v3, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->U:Ljava/lang/String;

    .line 3370
    :cond_b
    const/16 v33, -0x1

    move/from16 v0, v33

    if-eq v15, v0, :cond_c

    .line 3371
    invoke-interface {v3, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->V:Ljava/lang/String;

    .line 3372
    :cond_c
    const/16 v33, -0x1

    move/from16 v0, v16

    move/from16 v1, v33

    if-eq v0, v1, :cond_d

    .line 3373
    move/from16 v0, v16

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->W:Ljava/lang/String;

    .line 3374
    :cond_d
    const/16 v33, -0x1

    move/from16 v0, v17

    move/from16 v1, v33

    if-eq v0, v1, :cond_e

    .line 3375
    move/from16 v0, v17

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->X:Ljava/lang/String;

    .line 3376
    :cond_e
    const/16 v33, -0x1

    move/from16 v0, v18

    move/from16 v1, v33

    if-eq v0, v1, :cond_f

    .line 3377
    move/from16 v0, v18

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->Y:Ljava/lang/String;

    .line 3378
    :cond_f
    const/16 v33, -0x1

    move/from16 v0, v19

    move/from16 v1, v33

    if-eq v0, v1, :cond_10

    .line 3379
    move/from16 v0, v19

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->Z:Ljava/lang/String;

    .line 3380
    :cond_10
    const/16 v33, -0x1

    move/from16 v0, v20

    move/from16 v1, v33

    if-eq v0, v1, :cond_11

    .line 3381
    move/from16 v0, v20

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->aa:Ljava/lang/String;

    .line 3382
    :cond_11
    const/16 v33, -0x1

    move/from16 v0, v21

    move/from16 v1, v33

    if-eq v0, v1, :cond_12

    .line 3383
    move/from16 v0, v21

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->ab:Ljava/lang/String;

    .line 3384
    :cond_12
    const/16 v33, -0x1

    move/from16 v0, v22

    move/from16 v1, v33

    if-eq v0, v1, :cond_13

    .line 3385
    move/from16 v0, v22

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->ac:Ljava/lang/String;

    .line 3386
    :cond_13
    const/16 v33, -0x1

    move/from16 v0, v23

    move/from16 v1, v33

    if-eq v0, v1, :cond_14

    .line 3387
    move/from16 v0, v23

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->ad:Ljava/lang/String;

    .line 3388
    :cond_14
    const/16 v33, -0x1

    move/from16 v0, v24

    move/from16 v1, v33

    if-eq v0, v1, :cond_15

    .line 3389
    move/from16 v0, v24

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->ae:Ljava/lang/String;

    .line 3390
    :cond_15
    const/16 v33, -0x1

    move/from16 v0, v25

    move/from16 v1, v33

    if-eq v0, v1, :cond_16

    .line 3391
    move/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->af:Ljava/lang/String;

    .line 3392
    :cond_16
    const/16 v33, -0x1

    move/from16 v0, v26

    move/from16 v1, v33

    if-eq v0, v1, :cond_17

    .line 3393
    move/from16 v0, v26

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->ag:Ljava/lang/String;

    .line 3394
    :cond_17
    const/16 v33, -0x1

    move/from16 v0, v27

    move/from16 v1, v33

    if-eq v0, v1, :cond_18

    .line 3395
    move/from16 v0, v27

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->ah:Ljava/lang/String;

    .line 3396
    :cond_18
    const/16 v33, -0x1

    move/from16 v0, v28

    move/from16 v1, v33

    if-eq v0, v1, :cond_19

    .line 3397
    move/from16 v0, v28

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->ai:Ljava/lang/String;

    .line 3398
    :cond_19
    const/16 v33, -0x1

    move/from16 v0, v29

    move/from16 v1, v33

    if-eq v0, v1, :cond_1a

    .line 3399
    move/from16 v0, v29

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->aj:Ljava/lang/String;

    .line 3400
    :cond_1a
    const/16 v33, -0x1

    move/from16 v0, v30

    move/from16 v1, v33

    if-eq v0, v1, :cond_1b

    .line 3401
    move/from16 v0, v30

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->ak:Ljava/lang/String;

    .line 3402
    :cond_1b
    const/16 v33, -0x1

    move/from16 v0, v31

    move/from16 v1, v33

    if-eq v0, v1, :cond_1c

    .line 3403
    move/from16 v0, v31

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->al:Ljava/lang/String;

    .line 3404
    :cond_1c
    const/16 v33, -0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-eq v0, v1, :cond_1d

    .line 3405
    move/from16 v0, v32

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    iput-object v0, v12, Lcom/wssnps/b/l;->am:Ljava/lang/String;

    .line 3407
    :cond_1d
    sget-object v33, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3408
    add-int/lit8 v2, v2, 0x1

    .line 3409
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v12

    if-nez v12, :cond_1

    .line 3411
    :cond_1e
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 3413
    sget-object v2, Lcom/wssnps/b/l;->ap:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/wssnps/b/l;->d(Ljava/util/ArrayList;)Ljava/lang/Object;

    .line 3414
    sget-object v2, Lcom/wssnps/b/l;->aV:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 3416
    :cond_1f
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public static T()V
    .locals 3

    .prologue
    .line 3485
    const/4 v0, 0x1

    const-string v1, "smlBRCallLogCompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3486
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/CallLog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CallLog.bk"

    invoke-static {v0, v1, v2}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3487
    return-void
.end method

.method public static U()V
    .locals 3

    .prologue
    .line 3491
    const/4 v0, 0x1

    const-string v1, "smlBRCallLogDecompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3492
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/CallLog.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/CallLog"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3493
    return-void
.end method

.method public static V()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 3498
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3499
    sget-object v1, Lcom/wssnps/b/l;->G:Landroid/net/Uri;

    .line 3501
    const/4 v3, 0x1

    const-string v4, "smlBRCallLogDBDelete"

    invoke-static {v3, v4}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 3502
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 3503
    const-string v4, ""

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3504
    if-eqz v3, :cond_0

    .line 3505
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 3506
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static W()V
    .locals 2

    .prologue
    .line 3511
    const/4 v0, 0x1

    const-string v1, "smlBRCallLogDelete"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3512
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/CallLog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 3513
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/CallLog.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 3514
    return-void
.end method

.method public static X()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 3518
    const/4 v0, 0x1

    const-string v1, "smlBRMiniDiaryBackup"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/MiniDiary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 3521
    sput-boolean v2, Lcom/wssnps/b/l;->aC:Z

    .line 3522
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3523
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.minidiary.service.BROADCAST_DETECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3524
    const-string v1, "backup_Or_Restore"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3525
    const-string v1, "backup_Path"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/MiniDiary"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3526
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3527
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3528
    return-void
.end method

.method public static Y()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 3532
    const-string v0, "smlBRMiniDiaryRestore"

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3535
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->ab()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3542
    :goto_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssnps/b/l;->aD:Z

    .line 3543
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3544
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.minidiary.service.BROADCAST_DETECT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3545
    const-string v1, "backup_Or_Restore"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3546
    const-string v1, "backup_Path"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/MiniDiary"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3547
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3548
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3549
    return-void

    .line 3537
    :catch_0
    move-exception v0

    .line 3539
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static Z()V
    .locals 2

    .prologue
    .line 3553
    const/4 v0, 0x1

    const-string v1, "smlBRMiniDiaryDelete"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3554
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/MiniDiary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 3555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/MiniDiary.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 3556
    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 757
    sget-wide v0, Lcom/wssnps/b/l;->ao:J

    .line 758
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 759
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "smlBRBackupFinish "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 761
    cmp-long v2, v0, v4

    if-eqz v2, :cond_12

    .line 763
    sput v7, Lcom/wssnps/b/l;->as:I

    .line 764
    sput-boolean v6, Lcom/wssnps/b/l;->au:Z

    .line 765
    sput-boolean v6, Lcom/wssnps/b/l;->aw:Z

    .line 766
    sput-boolean v6, Lcom/wssnps/b/l;->ay:Z

    .line 767
    sput-boolean v6, Lcom/wssnps/b/l;->aA:Z

    .line 768
    sput-boolean v6, Lcom/wssnps/b/l;->aC:Z

    .line 769
    sput-boolean v6, Lcom/wssnps/b/l;->aE:Z

    .line 770
    sput-boolean v6, Lcom/wssnps/b/l;->aG:Z

    .line 772
    sput-boolean v6, Lcom/wssnps/b/l;->aI:Z

    .line 774
    const-wide/16 v2, 0x1

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 778
    :cond_0
    const-wide/16 v2, 0x4

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 782
    :cond_1
    const-wide/16 v2, 0x8

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 784
    sput-boolean v7, Lcom/wssnps/b/l;->aN:Z

    .line 787
    :cond_2
    const-wide/16 v2, 0x10

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 791
    :cond_3
    const-wide/16 v2, 0x20

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 793
    sput-boolean v7, Lcom/wssnps/b/l;->aO:Z

    .line 796
    :cond_4
    const-wide/16 v2, 0x100

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 800
    :cond_5
    const-wide/16 v2, 0x200

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 804
    :cond_6
    const-wide/16 v2, 0x400

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    .line 808
    :cond_7
    const-wide/32 v2, 0x40000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 812
    :cond_8
    const-wide/16 v2, 0x800

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 816
    :cond_9
    const-wide/16 v2, 0x1000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 820
    :cond_a
    const-wide/16 v2, 0x4000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 824
    :cond_b
    const-wide/32 v2, 0x10000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 828
    :cond_c
    const-wide/32 v2, 0x8000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 832
    :cond_d
    const-wide/16 v2, 0x40

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    .line 836
    :cond_e
    const-wide/32 v2, 0x1000000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    .line 840
    :cond_f
    const-wide/32 v2, 0x2000000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    .line 844
    :cond_10
    const-wide/32 v2, 0x40000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_11

    .line 854
    :cond_11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 851
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(I)V
    .locals 0

    .prologue
    .line 40
    invoke-static {p0}, Lcom/wssnps/b/l;->b(I)V

    return-void
.end method

.method static synthetic a(J)V
    .locals 0

    .prologue
    .line 40
    invoke-static {p0, p1}, Lcom/wssnps/b/l;->b(J)V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 277
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 279
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 283
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "getPackageSizeInfo"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Landroid/content/pm/IPackageStatsObserver;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 296
    if-nez v1, :cond_0

    .line 330
    :goto_0
    return-void

    .line 285
    :catch_0
    move-exception v0

    .line 287
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 290
    :catch_1
    move-exception v0

    .line 292
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 303
    :cond_0
    invoke-static {v6}, Lcom/wssnps/b/l;->b(I)V

    .line 306
    const/4 v2, 0x2

    :try_start_1
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/wssnps/b/m;

    invoke-direct {v4}, Lcom/wssnps/b/m;-><init>()V

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    .line 318
    :catch_2
    move-exception v0

    .line 320
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 322
    :catch_3
    move-exception v0

    .line 324
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 326
    :catch_4
    move-exception v0

    .line 328
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    .line 2700
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2704
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2705
    const/4 v0, 0x1

    const-string v1, "smlBRBookmarkInsertRow "

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2709
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 2711
    :try_start_0
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/l;

    .line 2712
    const-string v4, "_id"

    iget v5, v0, Lcom/wssnps/b/l;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2713
    const-string v4, "title"

    iget-object v5, v0, Lcom/wssnps/b/l;->l:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2714
    const-string v4, "url"

    iget-object v5, v0, Lcom/wssnps/b/l;->m:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2715
    const-string v4, "folder"

    iget v5, v0, Lcom/wssnps/b/l;->n:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2716
    const-string v4, "parent"

    iget v5, v0, Lcom/wssnps/b/l;->o:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2717
    const-string v4, "position"

    iget v5, v0, Lcom/wssnps/b/l;->p:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2718
    const-string v4, "insert_after"

    iget v5, v0, Lcom/wssnps/b/l;->q:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2719
    const-string v4, "deleted"

    iget v5, v0, Lcom/wssnps/b/l;->r:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2720
    const-string v4, "account_name"

    iget-object v5, v0, Lcom/wssnps/b/l;->s:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2721
    const-string v4, "account_type"

    iget-object v5, v0, Lcom/wssnps/b/l;->t:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722
    const-string v4, "sourceid"

    iget-object v5, v0, Lcom/wssnps/b/l;->u:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2723
    const-string v4, "version"

    iget v5, v0, Lcom/wssnps/b/l;->v:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2724
    const-string v4, "created"

    iget v5, v0, Lcom/wssnps/b/l;->w:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2725
    const-string v4, "modified"

    iget v5, v0, Lcom/wssnps/b/l;->x:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2726
    const-string v4, "dirty"

    iget v5, v0, Lcom/wssnps/b/l;->y:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2727
    const-string v4, "sync1"

    iget-object v5, v0, Lcom/wssnps/b/l;->z:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2728
    const-string v4, "sync2"

    iget-object v5, v0, Lcom/wssnps/b/l;->A:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2729
    const-string v4, "sync3"

    iget-object v5, v0, Lcom/wssnps/b/l;->B:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2730
    const-string v4, "sync4"

    iget-object v5, v0, Lcom/wssnps/b/l;->C:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2731
    const-string v4, "sync5"

    iget-object v5, v0, Lcom/wssnps/b/l;->D:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2732
    const-string v4, "parent_title"

    iget-object v0, v0, Lcom/wssnps/b/l;->E:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2733
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "bookmarks"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2709
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2736
    :catch_0
    move-exception v0

    .line 2738
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2740
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1871
    const/4 v0, 0x1

    const-string v2, "smlDbSqlConfigurationInsertRow"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1873
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1874
    new-instance v0, Lcom/wssnps/b/w;

    invoke-direct {v0, v4}, Lcom/wssnps/b/w;-><init>(Lcom/wssnps/b/m;)V

    .line 1875
    new-instance v0, Lcom/wssnps/b/w;

    invoke-direct {v0, v4}, Lcom/wssnps/b/w;-><init>(Lcom/wssnps/b/m;)V

    move v2, v1

    .line 1879
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1881
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/w;

    .line 1882
    const-string v4, "_id"

    iget v5, v0, Lcom/wssnps/b/w;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1883
    const-string v4, "name"

    iget-object v5, v0, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1884
    const-string v4, "value"

    iget-object v0, v0, Lcom/wssnps/b/w;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1885
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "configuration"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1879
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1888
    :catch_0
    move-exception v0

    .line 1890
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "smlDbSqlConfigrationInsertRow system : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1895
    :cond_0
    :goto_1
    :try_start_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1897
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/w;

    .line 1898
    const-string v2, "_id"

    iget v4, v0, Lcom/wssnps/b/w;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1899
    const-string v2, "name"

    iget-object v4, v0, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1900
    const-string v2, "value"

    iget-object v0, v0, Lcom/wssnps/b/w;->c:Ljava/lang/String;

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1901
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "global"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1895
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1904
    :catch_1
    move-exception v0

    .line 1906
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "smlDbSqlConfigrationInsertRow global: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1908
    :cond_1
    return-void
.end method

.method public static a([B)[B
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x0

    .line 3731
    .line 3734
    const/4 v0, 0x1

    const-string v1, "BackupApplicationInfo"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3736
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3737
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_MAKE_BACKUP_LIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3739
    if-eqz p0, :cond_0

    .line 3741
    array-length v1, p0

    if-le v1, v5, :cond_1

    .line 3743
    new-array v1, v5, [B

    .line 3744
    array-length v2, p0

    add-int/lit8 v2, v2, -0xc

    new-array v2, v2, [B

    .line 3745
    invoke-static {p0, v4, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3746
    array-length v3, p0

    add-int/lit8 v3, v3, -0xc

    invoke-static {p0, v5, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3747
    const-string v3, "head"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3748
    const-string v1, "body"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3756
    :goto_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3759
    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/wssnps/b/l;->aP:[B

    .line 3760
    sput v4, Lcom/wssnps/b/l;->aQ:I

    .line 3761
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3763
    :goto_1
    sget v0, Lcom/wssnps/b/l;->aQ:I

    if-nez v0, :cond_2

    .line 3767
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3769
    :catch_0
    move-exception v0

    .line 3771
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 3752
    :cond_1
    new-array v1, v5, [B

    .line 3753
    invoke-static {p0, v4, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3754
    const-string v2, "head"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    goto :goto_0

    .line 3775
    :cond_2
    sput v4, Lcom/wssnps/b/l;->aQ:I

    .line 3776
    sget-object v0, Lcom/wssnps/b/l;->aP:[B

    return-object v0
.end method

.method public static aa()V
    .locals 3

    .prologue
    .line 3560
    const/4 v0, 0x1

    const-string v1, "smlBRMiniDiaryCompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3561
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/MiniDiary"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiniDiary.bk"

    invoke-static {v0, v1, v2}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3562
    return-void
.end method

.method public static ab()V
    .locals 3

    .prologue
    .line 3566
    const/4 v0, 0x1

    const-string v1, "smlBRMiniDiaryDecompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3567
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/MiniDiary.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/MiniDiary"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3568
    return-void
.end method

.method public static ac()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 3573
    const-string v0, "smlBRPenMemoBackup"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3574
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/PenMemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 3576
    const-string v1, "content://com.diotek.penmemo.util.PenMemoProvider/DB_Backup"

    .line 3577
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/_SamsungBnR_/BR/PenMemo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 3580
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->af()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3586
    :goto_0
    sput-boolean v6, Lcom/wssnps/b/l;->aE:Z

    .line 3588
    if-eqz v0, :cond_0

    .line 3589
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 3590
    :cond_0
    return-void

    .line 3582
    :catch_0
    move-exception v1

    .line 3584
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static ad()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 3595
    const-string v0, "smlBRPenMemoRestore"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3598
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->ag()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3605
    :goto_0
    const-string v1, "content://com.diotek.penmemo.util.PenMemoProvider/DB_Restore"

    .line 3606
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/_SamsungBnR_/BR/PenMemo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 3607
    sput-boolean v6, Lcom/wssnps/b/l;->aF:Z

    .line 3609
    if-eqz v0, :cond_0

    .line 3610
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 3611
    :cond_0
    return-void

    .line 3600
    :catch_0
    move-exception v0

    .line 3602
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static ae()V
    .locals 2

    .prologue
    .line 3615
    const/4 v0, 0x1

    const-string v1, "smlBRPenMemoMDelete"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3616
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/PenMemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 3617
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/PenMemo.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 3618
    return-void
.end method

.method public static af()V
    .locals 3

    .prologue
    .line 3622
    const/4 v0, 0x1

    const-string v1, "smlBRPenMemoCompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3623
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/PenMemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PenMemo.bk"

    invoke-static {v0, v1, v2}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3624
    return-void
.end method

.method public static ag()V
    .locals 3

    .prologue
    .line 3628
    const/4 v0, 0x1

    const-string v1, "smlBRPenMemoDecompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3629
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/PenMemo.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/PenMemo"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3630
    return-void
.end method

.method public static ah()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 3635
    const-string v0, "smlBRSMemoBackup"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3636
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Smemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 3639
    const-string v1, "content://com.sec.android.widgetapp.q1_penmemo/DB_Backup"

    .line 3641
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/_SamsungBnR_/BR/Smemo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 3644
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->al()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3650
    :goto_0
    sput-boolean v6, Lcom/wssnps/b/l;->aG:Z

    .line 3652
    if-eqz v0, :cond_0

    .line 3653
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 3654
    :cond_0
    return-void

    .line 3646
    :catch_0
    move-exception v1

    .line 3648
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static ai()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 3659
    const-string v0, "smlBRSMemoRestore"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3662
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->am()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3670
    :goto_0
    const-string v1, "content://com.sec.android.widgetapp.q1_penmemo/DB_Restore"

    .line 3672
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/_SamsungBnR_/BR/Smemo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 3673
    sput-boolean v6, Lcom/wssnps/b/l;->aH:Z

    .line 3675
    if-eqz v0, :cond_0

    .line 3676
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 3677
    :cond_0
    return-void

    .line 3664
    :catch_0
    move-exception v0

    .line 3666
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static aj()V
    .locals 4

    .prologue
    .line 3681
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3682
    const/4 v0, 0x1

    const-string v1, "smlBRSMemo2Restore"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3685
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->am()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3692
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.memo.KIES_RESTORES_MEMODB"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3693
    if-eqz v0, :cond_0

    .line 3695
    const-string v1, "db "

    const-string v2, "pen_memo.db"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3696
    const-string v1, "files "

    const-string v2, "SMemoFiles.zip"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3697
    const-string v1, "Path"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/_SamsungBnR_/BR/Smemo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3699
    :cond_0
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3700
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3701
    return-void

    .line 3687
    :catch_0
    move-exception v0

    .line 3689
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static ak()V
    .locals 2

    .prologue
    .line 3705
    const/4 v0, 0x1

    const-string v1, "smlBRSMemoDelete"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3706
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Smemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 3707
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Smemo.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 3708
    return-void
.end method

.method public static al()V
    .locals 3

    .prologue
    .line 3712
    const/4 v0, 0x1

    const-string v1, "smlBRSMemoCompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3713
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Smemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Smemo.bk"

    invoke-static {v0, v1, v2}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3714
    return-void
.end method

.method public static am()V
    .locals 3

    .prologue
    .line 3718
    const/4 v0, 0x1

    const-string v1, "smlBRSMemoDecompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Smemo.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/Smemo"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3720
    return-void
.end method

.method public static an()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3909
    const-string v2, "RestoreApplicationReady"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3911
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3912
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.intent.action.KIES_SET_RESTORE_STATUS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3913
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3915
    const/4 v3, 0x0

    sput-object v3, Lcom/wssnps/b/l;->aP:[B

    .line 3916
    sput v0, Lcom/wssnps/b/l;->aQ:I

    .line 3917
    sget-object v3, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3919
    :goto_0
    sget v2, Lcom/wssnps/b/l;->aQ:I

    if-nez v2, :cond_0

    .line 3923
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3925
    :catch_0
    move-exception v2

    .line 3927
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 3931
    :cond_0
    sput v0, Lcom/wssnps/b/l;->aQ:I

    .line 3932
    sget-object v2, Lcom/wssnps/b/l;->aP:[B

    aget-byte v2, v2, v1

    if-nez v2, :cond_1

    .line 3935
    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static ao()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3940
    const-string v2, "RestoreApplicationFinish"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3942
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3943
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.intent.action.KIES_REQUEST_RESTORE_FINISH"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3944
    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3946
    const/4 v3, 0x0

    sput-object v3, Lcom/wssnps/b/l;->aP:[B

    .line 3947
    sput v0, Lcom/wssnps/b/l;->aQ:I

    .line 3948
    sget-object v3, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3950
    :goto_0
    sget v2, Lcom/wssnps/b/l;->aQ:I

    if-nez v2, :cond_0

    .line 3954
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3956
    :catch_0
    move-exception v2

    .line 3958
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 3962
    :cond_0
    sget-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->x(Landroid/content/Context;)V

    .line 3963
    sput v0, Lcom/wssnps/b/l;->aQ:I

    .line 3964
    sget-object v2, Lcom/wssnps/b/l;->aP:[B

    aget-byte v2, v2, v1

    if-nez v2, :cond_1

    .line 3967
    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static ap()V
    .locals 1

    .prologue
    .line 3972
    invoke-static {}, Lcom/wssnps/b/l;->l()V

    .line 3973
    invoke-static {}, Lcom/wssnps/b/l;->x()V

    .line 3974
    invoke-static {}, Lcom/wssnps/b/l;->E()V

    .line 3975
    invoke-static {}, Lcom/wssnps/b/l;->O()V

    .line 3976
    invoke-static {}, Lcom/wssnps/b/l;->W()V

    .line 3977
    invoke-static {}, Lcom/wssnps/b/l;->Z()V

    .line 3978
    invoke-static {}, Lcom/wssnps/b/l;->ae()V

    .line 3979
    invoke-static {}, Lcom/wssnps/b/l;->ak()V

    .line 3980
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->x(Landroid/content/Context;)V

    .line 3981
    return-void
.end method

.method public static aq()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3985
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 3986
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3988
    :cond_0
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_enable"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ar()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3993
    invoke-static {}, Lcom/wssnps/smlModelDefine;->p()Ljava/lang/String;

    move-result-object v2

    .line 3994
    const-string v3, "CHC"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CHM"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CHN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CHU"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CTC"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4009
    :cond_0
    :goto_0
    return v1

    .line 3998
    :cond_1
    invoke-static {}, Lcom/wssnps/smlModelDefine;->D()Z

    move-result v2

    if-nez v2, :cond_0

    .line 4001
    sget-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    if-nez v2, :cond_2

    .line 4002
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 4004
    :cond_2
    sget-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 4005
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.PACKAGE_NEEDS_VERIFICATION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4006
    const-string v4, "application/vnd.android.package-archive"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 4007
    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 4008
    invoke-virtual {v2, v3, v1}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 4009
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private static as()I
    .locals 1

    .prologue
    .line 258
    sget v0, Lcom/wssnps/b/l;->aW:I

    return v0
.end method

.method private static at()J
    .locals 2

    .prologue
    .line 270
    sget-wide v0, Lcom/wssnps/b/l;->aX:J

    return-wide v0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 334
    .line 338
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/_SamsungBnR_/BR/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 340
    invoke-static {p0}, Lcom/wssnps/b/l;->a(Ljava/lang/String;)V

    .line 344
    :goto_0
    invoke-static {}, Lcom/wssnps/b/l;->as()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 357
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 358
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    move v0, v1

    .line 361
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_1

    .line 362
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v7, v0, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 361
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 349
    :cond_0
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 351
    :catch_0
    move-exception v3

    .line 353
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 365
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->getTotalSpace()J

    .line 366
    invoke-virtual {v3}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v4

    .line 372
    invoke-static {}, Lcom/wssnps/b/l;->at()J

    move-result-wide v6

    const-wide/16 v8, 0x2

    mul-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-gez v0, :cond_2

    move v1, v2

    .line 377
    :cond_2
    return v1
.end method

.method public static b(Ljava/util/ArrayList;)Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 2972
    .line 2974
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2975
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 2976
    sget-object v1, Lcom/wssnps/b/l;->j:Landroid/net/Uri;

    .line 2978
    const-string v3, "smlBRBookmarkPut"

    invoke-static {v11, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2980
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v7, v8

    .line 2982
    :goto_0
    if-ge v7, v10, :cond_2

    .line 2984
    invoke-virtual {p0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/wssnps/b/l;

    .line 2986
    const-string v3, "title"

    iget-object v4, v6, Lcom/wssnps/b/l;->l:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2987
    const-string v3, "url"

    iget-object v4, v6, Lcom/wssnps/b/l;->m:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2988
    const-string v3, "folder"

    iget v4, v6, Lcom/wssnps/b/l;->n:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2989
    const-string v3, "position"

    iget v4, v6, Lcom/wssnps/b/l;->p:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2990
    const-string v3, "insert_after"

    iget v4, v6, Lcom/wssnps/b/l;->q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2991
    const-string v3, "deleted"

    iget v4, v6, Lcom/wssnps/b/l;->r:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2992
    const-string v3, "account_name"

    iget-object v4, v6, Lcom/wssnps/b/l;->s:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2993
    const-string v3, "account_type"

    iget-object v4, v6, Lcom/wssnps/b/l;->t:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2994
    const-string v3, "sourceid"

    iget-object v4, v6, Lcom/wssnps/b/l;->u:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2995
    const-string v3, "version"

    iget v4, v6, Lcom/wssnps/b/l;->v:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2996
    const-string v3, "created"

    iget v4, v6, Lcom/wssnps/b/l;->w:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2997
    const-string v3, "modified"

    iget v4, v6, Lcom/wssnps/b/l;->x:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2998
    const-string v3, "dirty"

    iget v4, v6, Lcom/wssnps/b/l;->y:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2999
    const-string v3, "sync1"

    iget-object v4, v6, Lcom/wssnps/b/l;->z:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3000
    const-string v3, "sync2"

    iget-object v4, v6, Lcom/wssnps/b/l;->A:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3001
    const-string v3, "sync3"

    iget-object v4, v6, Lcom/wssnps/b/l;->B:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3002
    const-string v3, "sync4"

    iget-object v4, v6, Lcom/wssnps/b/l;->C:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3003
    const-string v3, "sync5"

    iget-object v4, v6, Lcom/wssnps/b/l;->D:Ljava/lang/String;

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3005
    iget v3, v6, Lcom/wssnps/b/l;->o:I

    if-eqz v3, :cond_1

    iget v3, v6, Lcom/wssnps/b/l;->o:I

    if-eq v3, v11, :cond_1

    .line 3007
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "folder = 1 AND title=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v6, Lcom/wssnps/b/l;->E:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    .line 3008
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 3009
    if-eqz v3, :cond_1

    .line 3011
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 3013
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3015
    const-string v4, "_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v6, Lcom/wssnps/b/l;->o:I

    .line 3018
    :cond_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 3021
    :cond_1
    const-string v3, "parent"

    iget v4, v6, Lcom/wssnps/b/l;->o:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3022
    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2982
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto/16 :goto_0

    .line 3024
    :cond_2
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2005
    const-string v0, "smlBRConfigurationPut"

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2008
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 2010
    sget-object v4, Lcom/wssnps/b/l;->a:Landroid/net/Uri;

    .line 2011
    sget-object v5, Lcom/wssnps/b/l;->b:Landroid/net/Uri;

    .line 2013
    const-string v0, "smlBRConfigurationPut"

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move v1, v2

    .line 2015
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2017
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 2018
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/w;

    .line 2020
    const-string v7, "value"

    iget-object v8, v0, Lcom/wssnps/b/w;->c:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2021
    const-string v7, "name"

    iget-object v0, v0, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2025
    :try_start_0
    invoke-virtual {v3, v4, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2015
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2027
    :catch_0
    move-exception v0

    .line 2029
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_0
    move v1, v2

    .line 2033
    :goto_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2035
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2036
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/w;

    .line 2038
    const-string v6, "value"

    iget-object v7, v0, Lcom/wssnps/b/w;->c:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039
    const-string v6, "name"

    iget-object v0, v0, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    invoke-virtual {v4, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2043
    :try_start_1
    invoke-virtual {v3, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2033
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2045
    :catch_1
    move-exception v0

    .line 2047
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 2051
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 859
    sget-wide v0, Lcom/wssnps/b/l;->ao:J

    .line 860
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 861
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "smlBRBackupRestoreCancel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 863
    cmp-long v2, v0, v4

    if-eqz v2, :cond_12

    .line 865
    const-wide/16 v2, 0x1

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 867
    invoke-static {}, Lcom/wssnps/b/l;->l()V

    .line 870
    :cond_0
    const-wide/16 v2, 0x4

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 874
    :cond_1
    const-wide/16 v2, 0x8

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 876
    sput-boolean v7, Lcom/wssnps/b/l;->aN:Z

    .line 879
    :cond_2
    const-wide/16 v2, 0x10

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 883
    :cond_3
    const-wide/16 v2, 0x20

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 885
    sput-boolean v7, Lcom/wssnps/b/l;->aO:Z

    .line 888
    :cond_4
    const-wide/16 v2, 0x100

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 892
    :cond_5
    const-wide/16 v2, 0x200

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 896
    :cond_6
    const-wide/16 v2, 0x400

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    .line 900
    :cond_7
    const-wide/32 v2, 0x40000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 904
    :cond_8
    const-wide/16 v2, 0x800

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 908
    :cond_9
    const-wide/16 v2, 0x1000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 910
    sget-boolean v2, Lcom/wssnps/b/l;->ay:Z

    if-eqz v2, :cond_a

    sget-boolean v2, Lcom/wssnps/b/l;->az:Z

    if-eqz v2, :cond_a

    .line 911
    invoke-static {}, Lcom/wssnps/b/l;->E()V

    .line 914
    :cond_a
    const-wide/16 v2, 0x4000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 916
    sget-boolean v2, Lcom/wssnps/b/l;->aA:Z

    if-eqz v2, :cond_b

    sget-boolean v2, Lcom/wssnps/b/l;->aB:Z

    if-eqz v2, :cond_b

    .line 917
    invoke-static {}, Lcom/wssnps/b/l;->O()V

    .line 920
    :cond_b
    const-wide/32 v2, 0x10000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 922
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 923
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.intent.action.KIES_BNR_CANCEL"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 924
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 925
    sget-object v3, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 926
    sput v7, Lcom/wssnps/b/l;->aR:I

    .line 927
    sput v7, Lcom/wssnps/b/l;->aQ:I

    .line 928
    sget-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->x(Landroid/content/Context;)V

    .line 931
    :cond_c
    const-wide/32 v2, 0x8000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 933
    sget-boolean v2, Lcom/wssnps/b/l;->aw:Z

    if-eqz v2, :cond_d

    sget-boolean v2, Lcom/wssnps/b/l;->ax:Z

    if-eqz v2, :cond_d

    .line 934
    invoke-static {}, Lcom/wssnps/b/l;->W()V

    .line 937
    :cond_d
    const-wide/16 v2, 0x40

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    .line 939
    sget-boolean v2, Lcom/wssnps/b/l;->au:Z

    if-eqz v2, :cond_e

    sget-boolean v2, Lcom/wssnps/b/l;->av:Z

    if-eqz v2, :cond_e

    .line 940
    invoke-static {}, Lcom/wssnps/b/l;->x()V

    .line 943
    :cond_e
    const-wide/32 v2, 0x1000000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    .line 945
    sget-boolean v2, Lcom/wssnps/b/l;->aC:Z

    if-eqz v2, :cond_f

    sget-boolean v2, Lcom/wssnps/b/l;->aD:Z

    if-eqz v2, :cond_f

    .line 946
    invoke-static {}, Lcom/wssnps/b/l;->Z()V

    .line 949
    :cond_f
    const-wide/32 v2, 0x2000000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    .line 951
    sget-boolean v2, Lcom/wssnps/b/l;->aE:Z

    if-eqz v2, :cond_10

    sget-boolean v2, Lcom/wssnps/b/l;->aF:Z

    if-eqz v2, :cond_10

    .line 952
    invoke-static {}, Lcom/wssnps/b/l;->ae()V

    .line 955
    :cond_10
    const-wide/32 v2, 0x40000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_11

    .line 957
    sget-boolean v0, Lcom/wssnps/b/l;->aG:Z

    if-eqz v0, :cond_11

    sget-boolean v0, Lcom/wssnps/b/l;->aH:Z

    if-eqz v0, :cond_11

    .line 959
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_11

    .line 960
    invoke-static {}, Lcom/wssnps/b/l;->ak()V

    .line 964
    :cond_11
    sput v7, Lcom/wssnps/b/l;->as:I

    .line 965
    sput-boolean v6, Lcom/wssnps/b/l;->au:Z

    .line 966
    sput-boolean v6, Lcom/wssnps/b/l;->av:Z

    .line 967
    sput-boolean v6, Lcom/wssnps/b/l;->aw:Z

    .line 968
    sput-boolean v6, Lcom/wssnps/b/l;->ax:Z

    .line 969
    sput-boolean v6, Lcom/wssnps/b/l;->ay:Z

    .line 970
    sput-boolean v6, Lcom/wssnps/b/l;->az:Z

    .line 971
    sput-boolean v6, Lcom/wssnps/b/l;->aA:Z

    .line 972
    sput-boolean v6, Lcom/wssnps/b/l;->aB:Z

    .line 973
    sput-boolean v6, Lcom/wssnps/b/l;->aC:Z

    .line 974
    sput-boolean v6, Lcom/wssnps/b/l;->aD:Z

    .line 975
    sput-boolean v6, Lcom/wssnps/b/l;->aE:Z

    .line 976
    sput-boolean v6, Lcom/wssnps/b/l;->aF:Z

    .line 977
    sput-boolean v6, Lcom/wssnps/b/l;->aG:Z

    .line 978
    sput-boolean v6, Lcom/wssnps/b/l;->aH:Z

    .line 980
    sput-boolean v6, Lcom/wssnps/b/l;->aI:Z

    .line 981
    sput-boolean v6, Lcom/wssnps/b/l;->aJ:Z

    .line 989
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 986
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(I)V
    .locals 0

    .prologue
    .line 253
    sput p0, Lcom/wssnps/b/l;->aW:I

    .line 254
    return-void
.end method

.method private static b(J)V
    .locals 0

    .prologue
    .line 265
    sput-wide p0, Lcom/wssnps/b/l;->aX:J

    .line 266
    return-void
.end method

.method public static b([B)[B
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 3781
    .line 3784
    const/4 v0, 0x1

    const-string v1, "BackupApplicationReady"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3786
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3787
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_REQUEST_BACKUP_SPACE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3789
    if-eqz p0, :cond_0

    .line 3791
    new-array v1, v5, [B

    .line 3792
    array-length v2, p0

    add-int/lit8 v2, v2, -0x8

    new-array v2, v2, [B

    .line 3793
    invoke-static {p0, v4, v1, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3794
    array-length v3, p0

    add-int/lit8 v3, v3, -0x8

    invoke-static {p0, v5, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3795
    const-string v3, "head"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3796
    const-string v1, "body"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3797
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3800
    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/wssnps/b/l;->aP:[B

    .line 3801
    sput v4, Lcom/wssnps/b/l;->aQ:I

    .line 3802
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3804
    :goto_0
    sget v0, Lcom/wssnps/b/l;->aQ:I

    if-nez v0, :cond_1

    .line 3808
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3810
    :catch_0
    move-exception v0

    .line 3812
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 3816
    :cond_1
    sput v4, Lcom/wssnps/b/l;->aQ:I

    .line 3817
    sget-object v0, Lcom/wssnps/b/l;->aP:[B

    return-object v0
.end method

.method public static c()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 1215
    sget-wide v0, Lcom/wssnps/b/l;->ao:J

    .line 1216
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 1217
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "smlBRRestoreStart "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1219
    cmp-long v2, v0, v4

    if-eqz v2, :cond_11

    .line 1221
    sput-boolean v6, Lcom/wssnps/b/l;->av:Z

    .line 1222
    sput-boolean v6, Lcom/wssnps/b/l;->ax:Z

    .line 1223
    sput-boolean v6, Lcom/wssnps/b/l;->az:Z

    .line 1224
    sput-boolean v6, Lcom/wssnps/b/l;->aB:Z

    .line 1225
    sput-boolean v6, Lcom/wssnps/b/l;->aD:Z

    .line 1226
    sput-boolean v6, Lcom/wssnps/b/l;->aF:Z

    .line 1227
    sput-boolean v6, Lcom/wssnps/b/l;->aH:Z

    .line 1229
    sput-boolean v6, Lcom/wssnps/b/l;->aJ:Z

    .line 1230
    sput v7, Lcom/wssnps/b/l;->at:I

    .line 1232
    const-wide/16 v2, 0x1

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1234
    invoke-static {}, Lcom/wssnps/b/l;->g()V

    .line 1237
    :cond_0
    const-wide/16 v2, 0x4

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1241
    :cond_1
    const-wide/16 v2, 0x8

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1245
    :cond_2
    const-wide/16 v2, 0x10

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 1249
    :cond_3
    const-wide/16 v2, 0x20

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 1253
    :cond_4
    const-wide/16 v2, 0x100

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 1257
    :cond_5
    const-wide/16 v2, 0x200

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 1261
    :cond_6
    const-wide/16 v2, 0x400

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    .line 1265
    :cond_7
    const-wide/32 v2, 0x40000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 1269
    :cond_8
    const-wide/16 v2, 0x800

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 1273
    :cond_9
    const-wide/16 v2, 0x1000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 1275
    invoke-static {}, Lcom/wssnps/b/l;->z()V

    .line 1278
    :cond_a
    const-wide/16 v2, 0x4000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 1280
    invoke-static {}, Lcom/wssnps/b/l;->G()V

    .line 1283
    :cond_b
    const-wide/32 v2, 0x10000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 1287
    :cond_c
    const-wide/32 v2, 0x8000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 1289
    sput-boolean v7, Lcom/wssnps/b/l;->ax:Z

    .line 1290
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/wssnps/b/s;

    invoke-direct {v3}, Lcom/wssnps/b/s;-><init>()V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 1299
    :cond_d
    const-wide/16 v2, 0x40

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    .line 1301
    sput-boolean v7, Lcom/wssnps/b/l;->av:Z

    .line 1302
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/wssnps/b/t;

    invoke-direct {v3}, Lcom/wssnps/b/t;-><init>()V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 1311
    :cond_e
    const-wide/32 v2, 0x1000000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    .line 1313
    invoke-static {}, Lcom/wssnps/b/l;->Y()V

    .line 1316
    :cond_f
    const-wide/32 v2, 0x2000000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    .line 1318
    sput-boolean v7, Lcom/wssnps/b/l;->aF:Z

    .line 1319
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/wssnps/b/u;

    invoke-direct {v3}, Lcom/wssnps/b/u;-><init>()V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 1328
    :cond_10
    const-wide/32 v2, 0x40000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_11

    .line 1330
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 1331
    const/4 v1, 0x3

    if-ne v0, v1, :cond_12

    .line 1333
    sput-boolean v7, Lcom/wssnps/b/l;->aH:Z

    .line 1334
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/wssnps/b/v;

    invoke-direct {v1}, Lcom/wssnps/b/v;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1360
    :cond_11
    :goto_0
    sget-boolean v0, Lcom/wssnps/b/l;->av:Z

    if-eqz v0, :cond_13

    sget-boolean v0, Lcom/wssnps/b/l;->ax:Z

    if-eqz v0, :cond_13

    sget-boolean v0, Lcom/wssnps/b/l;->az:Z

    if-eqz v0, :cond_13

    sget-boolean v0, Lcom/wssnps/b/l;->aB:Z

    if-eqz v0, :cond_13

    sget-boolean v0, Lcom/wssnps/b/l;->aD:Z

    if-eqz v0, :cond_13

    sget-boolean v0, Lcom/wssnps/b/l;->aF:Z

    if-eqz v0, :cond_13

    sget-boolean v0, Lcom/wssnps/b/l;->aH:Z

    if-eqz v0, :cond_13

    .line 1368
    const-string v0, "smlBRRestoreStart COMPLEATE"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1377
    :goto_1
    return-object v0

    .line 1342
    :cond_12
    const/4 v1, 0x4

    if-ne v0, v1, :cond_11

    .line 1344
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/Smemo.bk"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1345
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1347
    sput-boolean v7, Lcom/wssnps/b/l;->aH:Z

    .line 1348
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/wssnps/b/n;

    invoke-direct {v1}, Lcom/wssnps/b/n;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 1373
    :cond_13
    const-string v0, "smlBRRestoreStart BUSY"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "9\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x6

    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    .line 412
    .line 413
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 415
    const/4 v1, 0x0

    sput-object v1, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    .line 416
    sget-object v1, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v1, :cond_1

    .line 417
    const-string v1, "2\nERROR:DB null\n"

    .line 752
    :cond_0
    :goto_0
    return-object v1

    .line 419
    :cond_1
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 420
    sput-wide v2, Lcom/wssnps/b/l;->ao:J

    .line 422
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 424
    sget v1, Lcom/wssnps/b/l;->as:I

    if-nez v1, :cond_14

    .line 426
    sget v1, Lcom/wssnps/b/l;->aq:I

    new-array v1, v1, [I

    sput-object v1, Lcom/wssnps/b/l;->aL:[I

    .line 427
    sget v1, Lcom/wssnps/b/l;->aq:I

    new-array v1, v1, [I

    sput-object v1, Lcom/wssnps/b/l;->aM:[I

    .line 428
    sput v0, Lcom/wssnps/b/l;->aK:I

    .line 430
    sput-boolean v8, Lcom/wssnps/b/l;->au:Z

    .line 431
    sput-boolean v8, Lcom/wssnps/b/l;->aw:Z

    .line 432
    sput-boolean v8, Lcom/wssnps/b/l;->ay:Z

    .line 433
    sput-boolean v8, Lcom/wssnps/b/l;->aA:Z

    .line 434
    sput-boolean v8, Lcom/wssnps/b/l;->aC:Z

    .line 435
    sput-boolean v8, Lcom/wssnps/b/l;->aE:Z

    .line 436
    sput-boolean v8, Lcom/wssnps/b/l;->aG:Z

    .line 438
    sput-boolean v8, Lcom/wssnps/b/l;->aI:Z

    .line 439
    sput v0, Lcom/wssnps/b/l;->at:I

    .line 441
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "smlBRBackupReady "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 443
    cmp-long v1, v2, v6

    if-eqz v1, :cond_1f

    .line 445
    const-wide/16 v4, 0x1

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    .line 447
    invoke-static {}, Lcom/wssnps/b/l;->l()V

    .line 448
    const-string v1, "com.android.providers.settings"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_15

    .line 450
    invoke-static {}, Lcom/wssnps/b/l;->f()V

    .line 451
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 458
    :goto_1
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v8, v1, v4

    .line 459
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 462
    :cond_2
    const-wide/16 v4, 0x4

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_3

    .line 464
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 465
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v10, v1, v4

    .line 466
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 469
    :cond_3
    const-wide/16 v4, 0x8

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_4

    .line 471
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 472
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x8

    aput v5, v1, v4

    .line 473
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 474
    sput-boolean v8, Lcom/wssnps/b/l;->aN:Z

    .line 477
    :cond_4
    const-wide/16 v4, 0x10

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_5

    .line 479
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 480
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x10

    aput v5, v1, v4

    .line 481
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 484
    :cond_5
    const-wide/16 v4, 0x20

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    .line 486
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 487
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x20

    aput v5, v1, v4

    .line 488
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 489
    sput-boolean v8, Lcom/wssnps/b/l;->aO:Z

    .line 492
    :cond_6
    const-wide/16 v4, 0x100

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_7

    .line 494
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 495
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x100

    aput v5, v1, v4

    .line 496
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 499
    :cond_7
    const-wide/16 v4, 0x200

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_8

    .line 501
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 502
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x200

    aput v5, v1, v4

    .line 503
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 506
    :cond_8
    const-wide/16 v4, 0x400

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_9

    .line 508
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 509
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x400

    aput v5, v1, v4

    .line 510
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 513
    :cond_9
    const-wide/16 v4, 0x800

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_a

    .line 515
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 516
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x800

    aput v5, v1, v4

    .line 517
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 520
    :cond_a
    const-wide/32 v4, 0x40000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_b

    .line 522
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 523
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/high16 v5, 0x40000

    aput v5, v1, v4

    .line 524
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 527
    :cond_b
    const-wide/16 v4, 0x1000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_c

    .line 529
    invoke-static {}, Lcom/wssnps/b/l;->E()V

    .line 530
    const-string v1, "com.android.email"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_16

    .line 532
    invoke-static {}, Lcom/wssnps/b/l;->y()V

    .line 533
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 540
    :goto_2
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x1000

    aput v5, v1, v4

    .line 541
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 544
    :cond_c
    const-wide/16 v4, 0x4000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_e

    .line 548
    invoke-static {}, Lcom/wssnps/b/l;->O()V

    .line 550
    invoke-static {}, Lcom/wssnps/smlModelDefine;->d()Z

    move-result v1

    if-nez v1, :cond_d

    invoke-static {}, Lcom/wssnps/smlModelDefine;->c()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 552
    :cond_d
    const-string v1, "com.android.browser"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v4

    .line 553
    invoke-static {}, Lcom/wssnps/smlModelDefine;->c()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 554
    const-string v1, "com.sec.android.app.fm"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v1

    .line 556
    :goto_3
    if-nez v4, :cond_17

    if-nez v1, :cond_17

    .line 558
    invoke-static {}, Lcom/wssnps/b/l;->F()V

    .line 559
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 571
    :goto_4
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x4000

    aput v5, v1, v4

    .line 572
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 575
    :cond_e
    const-wide/32 v4, 0x10000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_f

    .line 577
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 578
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/high16 v5, 0x10000

    aput v5, v1, v4

    .line 579
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 580
    sput v0, Lcom/wssnps/b/l;->aR:I

    .line 581
    sput v0, Lcom/wssnps/b/l;->aQ:I

    .line 584
    :cond_f
    const-wide/32 v4, 0x8000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_10

    .line 586
    invoke-static {}, Lcom/wssnps/b/l;->W()V

    .line 587
    const-string v1, "com.sec.android.provider.logsprovider"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_19

    .line 589
    sput-boolean v0, Lcom/wssnps/b/l;->aw:Z

    .line 590
    new-instance v1, Ljava/lang/Thread;

    new-instance v4, Lcom/wssnps/b/o;

    invoke-direct {v4}, Lcom/wssnps/b/o;-><init>()V

    invoke-direct {v1, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 597
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 604
    :goto_5
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const v5, 0x8000

    aput v5, v1, v4

    .line 605
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 608
    :cond_10
    const-wide/16 v4, 0x40

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_11

    .line 611
    invoke-static {}, Lcom/wssnps/b/l;->x()V

    .line 612
    const-string v1, "com.android.providers.telephony"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v1

    .line 613
    if-nez v1, :cond_1a

    .line 615
    sput-boolean v0, Lcom/wssnps/b/l;->au:Z

    .line 616
    new-instance v1, Ljava/lang/Thread;

    new-instance v4, Lcom/wssnps/b/p;

    invoke-direct {v4}, Lcom/wssnps/b/p;-><init>()V

    invoke-direct {v1, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 623
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 630
    :goto_6
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/16 v5, 0x40

    aput v5, v1, v4

    .line 631
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 634
    :cond_11
    const-wide/32 v4, 0x1000000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_12

    .line 636
    invoke-static {}, Lcom/wssnps/b/l;->Z()V

    .line 637
    const-string v1, "com.sec.android.app.minidiary"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1b

    .line 639
    invoke-static {}, Lcom/wssnps/b/l;->X()V

    .line 640
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 647
    :goto_7
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/high16 v5, 0x1000000

    aput v5, v1, v4

    .line 648
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 651
    :cond_12
    const-wide/32 v4, 0x2000000

    and-long/2addr v4, v2

    cmp-long v1, v4, v6

    if-eqz v1, :cond_13

    .line 653
    invoke-static {}, Lcom/wssnps/b/l;->ae()V

    .line 654
    const-string v1, "com.diotek.penmemo"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1c

    .line 656
    sput-boolean v0, Lcom/wssnps/b/l;->aE:Z

    .line 657
    new-instance v1, Ljava/lang/Thread;

    new-instance v4, Lcom/wssnps/b/q;

    invoke-direct {v4}, Lcom/wssnps/b/q;-><init>()V

    invoke-direct {v1, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 664
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v4

    .line 671
    :goto_8
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    const/high16 v5, 0x2000000

    aput v5, v1, v4

    .line 672
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 675
    :cond_13
    const-wide/32 v4, 0x40000000

    and-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-eqz v1, :cond_14

    .line 677
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v1

    .line 678
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1e

    .line 680
    invoke-static {}, Lcom/wssnps/b/l;->ak()V

    .line 681
    const-string v1, "com.sec.android.widgetapp.diotek.smemo"

    invoke-static {v1}, Lcom/wssnps/b/l;->b(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1d

    .line 683
    sput-boolean v0, Lcom/wssnps/b/l;->aG:Z

    .line 684
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/wssnps/b/r;

    invoke-direct {v2}, Lcom/wssnps/b/r;-><init>()V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 691
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v2, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v2

    .line 698
    :goto_9
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v2, Lcom/wssnps/b/l;->aK:I

    const/high16 v3, 0x40000000    # 2.0f

    aput v3, v1, v2

    .line 699
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    .line 721
    :cond_14
    :goto_a
    sget-boolean v1, Lcom/wssnps/b/l;->au:Z

    if-eqz v1, :cond_21

    sget-boolean v1, Lcom/wssnps/b/l;->aw:Z

    if-eqz v1, :cond_21

    sget-boolean v1, Lcom/wssnps/b/l;->ay:Z

    if-eqz v1, :cond_21

    sget-boolean v1, Lcom/wssnps/b/l;->aA:Z

    if-eqz v1, :cond_21

    sget-boolean v1, Lcom/wssnps/b/l;->aC:Z

    if-eqz v1, :cond_21

    sget-boolean v1, Lcom/wssnps/b/l;->aE:Z

    if-eqz v1, :cond_21

    sget-boolean v1, Lcom/wssnps/b/l;->aG:Z

    if-eqz v1, :cond_21

    .line 729
    const-string v1, "smlBRBackupReady COMPLEATE"

    invoke-static {v8, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 730
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wssnps/b/l;->aK:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    .line 731
    :goto_b
    sget v3, Lcom/wssnps/b/l;->aK:I

    if-ge v1, v3, :cond_20

    .line 733
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/wssnps/b/l;->aM:[I

    aget v4, v4, v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 734
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/wssnps/b/l;->aL:[I

    aget v4, v4, v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 731
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 455
    :cond_15
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v9, v1, v4

    goto/16 :goto_1

    .line 537
    :cond_16
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v9, v1, v4

    goto/16 :goto_2

    .line 563
    :cond_17
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v9, v1, v4

    goto/16 :goto_4

    .line 568
    :cond_18
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v10, v1, v4

    goto/16 :goto_4

    .line 601
    :cond_19
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v9, v1, v4

    goto/16 :goto_5

    .line 627
    :cond_1a
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v9, v1, v4

    goto/16 :goto_6

    .line 644
    :cond_1b
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v9, v1, v4

    goto/16 :goto_7

    .line 668
    :cond_1c
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v4, Lcom/wssnps/b/l;->aK:I

    aput v9, v1, v4

    goto/16 :goto_8

    .line 695
    :cond_1d
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v2, Lcom/wssnps/b/l;->aK:I

    aput v9, v1, v2

    goto/16 :goto_9

    .line 701
    :cond_1e
    if-ne v1, v10, :cond_14

    .line 703
    sget-object v1, Lcom/wssnps/b/l;->aL:[I

    sget v2, Lcom/wssnps/b/l;->aK:I

    aput v0, v1, v2

    .line 704
    sget-object v1, Lcom/wssnps/b/l;->aM:[I

    sget v2, Lcom/wssnps/b/l;->aK:I

    const/high16 v3, 0x40000000    # 2.0f

    aput v3, v1, v2

    .line 705
    sget v1, Lcom/wssnps/b/l;->aK:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/wssnps/b/l;->aK:I

    goto/16 :goto_a

    .line 711
    :cond_1f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wssnps/b/l;->aK:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 712
    :goto_c
    sget v2, Lcom/wssnps/b/l;->aK:I

    if-ge v0, v2, :cond_0

    .line 714
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/wssnps/b/l;->aM:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 715
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/wssnps/b/l;->aL:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 712
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 737
    :cond_20
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v3, Lcom/wssnps/smlModelDefine;->e:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/_SamsungBnR_/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I

    .line 738
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget v3, Lcom/wssnps/smlModelDefine;->e:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/_SamsungBnR_/BR/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;ILjava/lang/String;)I

    .line 739
    sput v0, Lcom/wssnps/b/l;->as:I

    :goto_d
    move-object v1, v2

    .line 752
    goto/16 :goto_0

    .line 743
    :cond_21
    const-string v1, "smlBRBackupReady BUSY"

    invoke-static {v8, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 744
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "9\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/wssnps/b/l;->aK:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 745
    :goto_e
    sget v2, Lcom/wssnps/b/l;->aK:I

    if-ge v0, v2, :cond_22

    .line 747
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/wssnps/b/l;->aM:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 748
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/wssnps/b/l;->aL:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 745
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 750
    :cond_22
    sput v8, Lcom/wssnps/b/l;->as:I

    move-object v2, v1

    goto :goto_d

    :cond_23
    move v1, v0

    goto/16 :goto_3
.end method

.method public static c(Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    .line 3245
    .line 3246
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 3249
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 3250
    const/4 v0, 0x1

    const-string v1, "smlBRCalllogInsertRow "

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3253
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 3255
    :try_start_0
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/l;

    .line 3256
    const-string v4, "_id"

    iget v5, v0, Lcom/wssnps/b/l;->L:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3257
    const-string v4, "number"

    iget-object v5, v0, Lcom/wssnps/b/l;->M:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3258
    const-string v4, "sim_id"

    iget-object v5, v0, Lcom/wssnps/b/l;->N:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3259
    const-string v4, "simnum"

    iget-object v5, v0, Lcom/wssnps/b/l;->O:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3260
    const-string v4, "address"

    iget-object v5, v0, Lcom/wssnps/b/l;->P:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3261
    const-string v4, "date"

    iget-object v5, v0, Lcom/wssnps/b/l;->Q:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3262
    const-string v4, "duration"

    iget-object v5, v0, Lcom/wssnps/b/l;->R:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3263
    const-string v4, "type"

    iget-object v5, v0, Lcom/wssnps/b/l;->S:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3264
    const-string v4, "new"

    iget-object v5, v0, Lcom/wssnps/b/l;->T:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3265
    const-string v4, "name"

    iget-object v5, v0, Lcom/wssnps/b/l;->U:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3266
    const-string v4, "numbertype"

    iget-object v5, v0, Lcom/wssnps/b/l;->V:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3267
    const-string v4, "numberlabel"

    iget-object v5, v0, Lcom/wssnps/b/l;->W:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3268
    const-string v4, "messageid"

    iget-object v5, v0, Lcom/wssnps/b/l;->X:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3269
    const-string v4, "logtype"

    iget-object v5, v0, Lcom/wssnps/b/l;->Y:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3270
    const-string v4, "frequent"

    iget-object v5, v0, Lcom/wssnps/b/l;->Z:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3271
    const-string v4, "contactid"

    iget-object v5, v0, Lcom/wssnps/b/l;->aa:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3272
    const-string v4, "raw_contact_id"

    iget-object v5, v0, Lcom/wssnps/b/l;->ab:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3273
    const-string v4, "m_subject"

    iget-object v5, v0, Lcom/wssnps/b/l;->ac:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3274
    const-string v4, "m_content"

    iget-object v5, v0, Lcom/wssnps/b/l;->ad:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3275
    const-string v4, "sns_tid"

    iget-object v5, v0, Lcom/wssnps/b/l;->ae:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3276
    const-string v4, "sns_pkey"

    iget-object v5, v0, Lcom/wssnps/b/l;->af:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3277
    const-string v4, "account_name"

    iget-object v5, v0, Lcom/wssnps/b/l;->ag:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3278
    const-string v4, "account_id"

    iget-object v5, v0, Lcom/wssnps/b/l;->ah:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3279
    const-string v4, "sns_receiver_count"

    iget-object v5, v0, Lcom/wssnps/b/l;->ai:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3280
    const-string v4, "sp_type"

    iget-object v5, v0, Lcom/wssnps/b/l;->aj:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3281
    const-string v4, "cnap_name"

    iget-object v5, v0, Lcom/wssnps/b/l;->ak:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3282
    const-string v4, "cdnip_number"

    iget-object v5, v0, Lcom/wssnps/b/l;->al:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3283
    const-string v4, "service_type"

    iget-object v0, v0, Lcom/wssnps/b/l;->am:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3285
    sget-object v0, Lcom/wssnps/b/l;->aV:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "calllog"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3253
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 3288
    :catch_0
    move-exception v0

    .line 3290
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 3292
    :cond_0
    return-void
.end method

.method public static c([B)[B
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3822
    .line 3825
    const-string v0, "BackupApplicationStart"

    invoke-static {v5, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3827
    sget v0, Lcom/wssnps/b/l;->aR:I

    if-nez v0, :cond_1

    .line 3829
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3830
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_MAKE_BACKUP_APK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3832
    if-eqz p0, :cond_0

    .line 3834
    new-array v1, v6, [B

    .line 3835
    array-length v2, p0

    add-int/lit8 v2, v2, -0x6

    new-array v2, v2, [B

    .line 3836
    invoke-static {p0, v4, v1, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3837
    array-length v3, p0

    add-int/lit8 v3, v3, -0x6

    invoke-static {p0, v6, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3838
    const-string v3, "head"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3839
    const-string v1, "body"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3840
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3843
    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/wssnps/b/l;->aP:[B

    .line 3844
    sput v4, Lcom/wssnps/b/l;->aQ:I

    .line 3845
    sput v5, Lcom/wssnps/b/l;->aR:I

    .line 3846
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3848
    sget-object v0, Lcom/wssnps/b/l;->aT:[B

    sput-object v0, Lcom/wssnps/b/l;->aP:[B

    .line 3860
    :goto_0
    sget-object v0, Lcom/wssnps/b/l;->aP:[B

    return-object v0

    .line 3850
    :cond_1
    sget v0, Lcom/wssnps/b/l;->aR:I

    if-ne v0, v5, :cond_2

    sget v0, Lcom/wssnps/b/l;->aQ:I

    if-nez v0, :cond_2

    .line 3852
    sget-object v0, Lcom/wssnps/b/l;->aT:[B

    sput-object v0, Lcom/wssnps/b/l;->aP:[B

    goto :goto_0

    .line 3856
    :cond_2
    sput v4, Lcom/wssnps/b/l;->aR:I

    .line 3857
    sput v4, Lcom/wssnps/b/l;->aQ:I

    goto :goto_0
.end method

.method public static d(Ljava/util/ArrayList;)Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 3421
    .line 3423
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 3424
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 3425
    sget-object v5, Lcom/wssnps/b/l;->G:Landroid/net/Uri;

    .line 3426
    sget-object v6, Lcom/wssnps/b/l;->I:Landroid/net/Uri;

    .line 3427
    sget-object v7, Lcom/wssnps/b/l;->J:Landroid/net/Uri;

    .line 3428
    sget-object v8, Lcom/wssnps/b/l;->K:Landroid/net/Uri;

    .line 3430
    const/4 v0, 0x1

    const-string v1, "smlBRCalllogPut"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3432
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v1, v2

    .line 3434
    :goto_0
    if-ge v1, v9, :cond_4

    .line 3436
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/b/l;

    .line 3437
    const-string v10, "_id"

    iget v11, v0, Lcom/wssnps/b/l;->L:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3438
    const-string v10, "number"

    iget-object v11, v0, Lcom/wssnps/b/l;->M:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3439
    const-string v10, "sim_id"

    iget-object v11, v0, Lcom/wssnps/b/l;->N:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3440
    const-string v10, "simnum"

    iget-object v11, v0, Lcom/wssnps/b/l;->O:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3441
    const-string v10, "address"

    iget-object v11, v0, Lcom/wssnps/b/l;->P:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3442
    const-string v10, "date"

    iget-object v11, v0, Lcom/wssnps/b/l;->Q:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3443
    const-string v10, "duration"

    iget-object v11, v0, Lcom/wssnps/b/l;->R:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3444
    const-string v10, "type"

    iget-object v11, v0, Lcom/wssnps/b/l;->S:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3445
    const-string v10, "new"

    iget-object v11, v0, Lcom/wssnps/b/l;->T:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3446
    const-string v10, "name"

    iget-object v11, v0, Lcom/wssnps/b/l;->U:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3447
    const-string v10, "numbertype"

    iget-object v11, v0, Lcom/wssnps/b/l;->V:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3448
    const-string v10, "numberlabel"

    iget-object v11, v0, Lcom/wssnps/b/l;->W:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3449
    const-string v10, "messageid"

    iget-object v11, v0, Lcom/wssnps/b/l;->X:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3450
    const-string v10, "logtype"

    iget-object v11, v0, Lcom/wssnps/b/l;->Y:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3451
    const-string v10, "frequent"

    iget-object v11, v0, Lcom/wssnps/b/l;->Z:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3452
    const-string v10, "contactid"

    iget-object v11, v0, Lcom/wssnps/b/l;->aa:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3453
    const-string v10, "raw_contact_id"

    iget-object v11, v0, Lcom/wssnps/b/l;->ab:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3454
    const-string v10, "m_subject"

    iget-object v11, v0, Lcom/wssnps/b/l;->ac:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3455
    const-string v10, "m_content"

    iget-object v11, v0, Lcom/wssnps/b/l;->ad:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3456
    const-string v10, "sns_tid"

    iget-object v11, v0, Lcom/wssnps/b/l;->ae:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3457
    const-string v10, "sns_pkey"

    iget-object v11, v0, Lcom/wssnps/b/l;->af:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3458
    const-string v10, "account_name"

    iget-object v11, v0, Lcom/wssnps/b/l;->ag:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3459
    const-string v10, "account_id"

    iget-object v11, v0, Lcom/wssnps/b/l;->ah:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3460
    const-string v10, "sns_receiver_count"

    iget-object v11, v0, Lcom/wssnps/b/l;->ai:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3461
    const-string v10, "sp_type"

    iget-object v11, v0, Lcom/wssnps/b/l;->aj:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3462
    const-string v10, "cnap_name"

    iget-object v11, v0, Lcom/wssnps/b/l;->ak:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3463
    const-string v10, "cdnip_number"

    iget-object v11, v0, Lcom/wssnps/b/l;->al:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3464
    const-string v10, "service_type"

    iget-object v11, v0, Lcom/wssnps/b/l;->am:Ljava/lang/String;

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3466
    iget-object v10, v0, Lcom/wssnps/b/l;->Y:Ljava/lang/String;

    const-string v11, "100"

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_0

    .line 3467
    invoke-virtual {v3, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 3469
    :cond_0
    iget-object v10, v0, Lcom/wssnps/b/l;->Y:Ljava/lang/String;

    const-string v11, "500"

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_1

    .line 3470
    invoke-virtual {v3, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 3472
    :cond_1
    iget-object v10, v0, Lcom/wssnps/b/l;->Y:Ljava/lang/String;

    const-string v11, "300"

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_2

    .line 3473
    invoke-virtual {v3, v7, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 3475
    :cond_2
    iget-object v0, v0, Lcom/wssnps/b/l;->Y:Ljava/lang/String;

    const-string v10, "200"

    invoke-virtual {v0, v10}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 3476
    invoke-virtual {v3, v8, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 3434
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 3480
    :cond_4
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static d()Ljava/lang/String;
    .locals 14

    .prologue
    const/16 v12, 0x18

    const/4 v2, 0x1

    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 1386
    sget-wide v4, Lcom/wssnps/b/l;->ao:J

    .line 1387
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 1389
    sget v0, Lcom/wssnps/b/l;->aq:I

    new-array v6, v0, [I

    .line 1390
    sget v0, Lcom/wssnps/b/l;->aq:I

    new-array v7, v0, [I

    .line 1392
    cmp-long v0, v4, v10

    if-eqz v0, :cond_11

    .line 1394
    const-wide/16 v8, 0x1

    and-long/2addr v8, v4

    cmp-long v0, v8, v10

    if-eqz v0, :cond_15

    .line 1396
    aput v1, v6, v1

    .line 1397
    aput v2, v7, v1

    move v0, v2

    .line 1401
    :goto_0
    const-wide/16 v8, 0x4

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_0

    .line 1403
    aput v1, v6, v0

    .line 1404
    const/4 v3, 0x4

    aput v3, v7, v0

    .line 1405
    add-int/lit8 v0, v0, 0x1

    .line 1408
    :cond_0
    const-wide/16 v8, 0x8

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_1

    .line 1410
    aput v1, v6, v0

    .line 1411
    const/16 v3, 0x8

    aput v3, v7, v0

    .line 1412
    add-int/lit8 v0, v0, 0x1

    .line 1415
    :cond_1
    const-wide/16 v8, 0x10

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_2

    .line 1418
    aput v1, v6, v0

    .line 1419
    const/16 v3, 0x10

    aput v3, v7, v0

    .line 1420
    add-int/lit8 v0, v0, 0x1

    .line 1423
    :cond_2
    const-wide/16 v8, 0x20

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_3

    .line 1425
    aput v1, v6, v0

    .line 1426
    const/16 v3, 0x20

    aput v3, v7, v0

    .line 1427
    add-int/lit8 v0, v0, 0x1

    .line 1430
    :cond_3
    const-wide/16 v8, 0x100

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_4

    .line 1432
    aput v1, v6, v0

    .line 1433
    const/16 v3, 0x100

    aput v3, v7, v0

    .line 1434
    add-int/lit8 v0, v0, 0x1

    .line 1437
    :cond_4
    const-wide/16 v8, 0x200

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_5

    .line 1439
    aput v1, v6, v0

    .line 1440
    const/16 v3, 0x200

    aput v3, v7, v0

    .line 1441
    add-int/lit8 v0, v0, 0x1

    .line 1444
    :cond_5
    const-wide/16 v8, 0x400

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_6

    .line 1446
    aput v1, v6, v0

    .line 1447
    const/16 v3, 0x400

    aput v3, v7, v0

    .line 1448
    add-int/lit8 v0, v0, 0x1

    .line 1451
    :cond_6
    const-wide/32 v8, 0x40000

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_7

    .line 1453
    aput v1, v6, v0

    .line 1454
    const/high16 v3, 0x40000

    aput v3, v7, v0

    .line 1455
    add-int/lit8 v0, v0, 0x1

    .line 1458
    :cond_7
    const-wide/16 v8, 0x800

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_8

    .line 1460
    aput v1, v6, v0

    .line 1461
    const/16 v3, 0x800

    aput v3, v7, v0

    .line 1462
    add-int/lit8 v0, v0, 0x1

    .line 1465
    :cond_8
    const-wide/16 v8, 0x1000

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_9

    .line 1467
    aput v1, v6, v0

    .line 1468
    const/16 v3, 0x1000

    aput v3, v7, v0

    .line 1469
    add-int/lit8 v0, v0, 0x1

    .line 1472
    :cond_9
    const-wide/16 v8, 0x4000

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_a

    .line 1474
    aput v1, v6, v0

    .line 1475
    const/16 v3, 0x4000

    aput v3, v7, v0

    .line 1476
    add-int/lit8 v0, v0, 0x1

    .line 1479
    :cond_a
    const-wide/32 v8, 0x10000

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_b

    .line 1481
    aput v1, v6, v0

    .line 1482
    const/high16 v3, 0x10000

    aput v3, v7, v0

    .line 1483
    add-int/lit8 v0, v0, 0x1

    .line 1486
    :cond_b
    const-wide/32 v8, 0x8000

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_c

    .line 1488
    aput v1, v6, v0

    .line 1489
    const v3, 0x8000

    aput v3, v7, v0

    .line 1490
    add-int/lit8 v0, v0, 0x1

    .line 1493
    :cond_c
    const-wide/16 v8, 0x40

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_d

    .line 1495
    aput v1, v6, v0

    .line 1496
    const/16 v3, 0x40

    aput v3, v7, v0

    .line 1497
    add-int/lit8 v0, v0, 0x1

    .line 1500
    :cond_d
    const-wide/32 v8, 0x1000000

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_e

    .line 1502
    aput v1, v6, v0

    .line 1503
    const/high16 v3, 0x1000000

    aput v3, v7, v0

    .line 1504
    add-int/lit8 v0, v0, 0x1

    .line 1507
    :cond_e
    const-wide/32 v8, 0x2000000

    and-long/2addr v8, v4

    cmp-long v3, v8, v10

    if-eqz v3, :cond_f

    .line 1509
    aput v1, v6, v0

    .line 1510
    const/high16 v3, 0x2000000

    aput v3, v7, v0

    .line 1511
    add-int/lit8 v0, v0, 0x1

    .line 1514
    :cond_f
    const-wide/32 v8, 0x40000000

    and-long/2addr v4, v8

    cmp-long v3, v4, v10

    if-eqz v3, :cond_10

    .line 1516
    aput v1, v6, v0

    .line 1517
    const/high16 v3, 0x40000000    # 2.0f

    aput v3, v7, v0

    .line 1518
    add-int/lit8 v0, v0, 0x1

    .line 1533
    :cond_10
    sget-boolean v3, Lcom/wssnps/b/l;->av:Z

    if-eqz v3, :cond_13

    sget-boolean v3, Lcom/wssnps/b/l;->ax:Z

    if-eqz v3, :cond_13

    sget-boolean v3, Lcom/wssnps/b/l;->az:Z

    if-eqz v3, :cond_13

    sget-boolean v3, Lcom/wssnps/b/l;->aB:Z

    if-eqz v3, :cond_13

    sget-boolean v3, Lcom/wssnps/b/l;->aD:Z

    if-eqz v3, :cond_13

    sget-boolean v3, Lcom/wssnps/b/l;->aF:Z

    if-eqz v3, :cond_13

    sget-boolean v3, Lcom/wssnps/b/l;->aH:Z

    if-eqz v3, :cond_13

    .line 1541
    const-string v3, "smlBRRestoreCheck COMPLEATE"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1542
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move v13, v1

    move-object v1, v2

    move v2, v13

    .line 1543
    :goto_1
    if-ge v2, v0, :cond_12

    .line 1545
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget v4, v7, v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1546
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget v4, v6, v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1543
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v3

    goto :goto_1

    .line 1524
    :cond_11
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1560
    :goto_2
    return-object v0

    :cond_12
    move-object v0, v1

    .line 1543
    goto :goto_2

    .line 1551
    :cond_13
    const-string v3, "smlBRRestoreCheck BUSY"

    invoke-static {v2, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1552
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "9\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move v13, v1

    move-object v1, v2

    move v2, v13

    .line 1553
    :goto_3
    if-ge v2, v0, :cond_14

    .line 1555
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget v4, v7, v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1556
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget v4, v6, v2

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1553
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v3

    goto :goto_3

    :cond_14
    move-object v0, v1

    goto :goto_2

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    .prologue
    .line 994
    .line 999
    const/4 v1, 0x0

    .line 1001
    const/4 v0, 0x0

    const/16 v2, 0xa

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1002
    sput-wide v2, Lcom/wssnps/b/l;->ao:J

    .line 1004
    sget v0, Lcom/wssnps/b/l;->aq:I

    new-array v4, v0, [I

    .line 1005
    sget v0, Lcom/wssnps/b/l;->aq:I

    new-array v5, v0, [I

    .line 1007
    const/4 v0, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "smlBRRestoreReady "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1009
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v6

    .line 1010
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "/_SamsungBnR_/BR/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 1011
    const-wide/16 v8, 0x0

    cmp-long v0, v2, v8

    if-eqz v0, :cond_1c

    .line 1013
    const-wide/16 v8, 0x1

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_0

    .line 1015
    invoke-static {}, Lcom/wssnps/b/l;->l()V

    .line 1016
    const/4 v0, 0x0

    aput v0, v4, v1

    .line 1017
    const/4 v0, 0x1

    aput v0, v5, v1

    .line 1018
    const/4 v1, 0x1

    .line 1021
    :cond_0
    const-wide/16 v8, 0x4

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_2

    .line 1023
    invoke-static {}, Lcom/wssnps/b/l;->m()Z

    move-result v0

    .line 1024
    const/4 v7, 0x1

    if-ne v0, v7, :cond_1

    .line 1026
    invoke-static {}, Lcom/wssnps/b/l;->n()Z

    move-result v0

    .line 1028
    :cond_1
    if-eqz v0, :cond_e

    const/4 v0, 0x0

    :goto_0
    aput v0, v4, v1

    .line 1029
    const/4 v0, 0x4

    aput v0, v5, v1

    .line 1030
    add-int/lit8 v1, v1, 0x1

    .line 1033
    :cond_2
    const-wide/16 v8, 0x8

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_4

    .line 1035
    invoke-static {}, Lcom/wssnps/b/l;->o()Z

    move-result v0

    .line 1036
    const/4 v7, 0x1

    if-ne v0, v7, :cond_3

    .line 1038
    invoke-static {}, Lcom/wssnps/b/l;->p()Z

    move-result v0

    .line 1040
    :cond_3
    if-eqz v0, :cond_f

    const/4 v0, 0x0

    :goto_1
    aput v0, v4, v1

    .line 1041
    const/16 v0, 0x8

    aput v0, v5, v1

    .line 1042
    add-int/lit8 v1, v1, 0x1

    .line 1043
    const/4 v0, 0x1

    sput-boolean v0, Lcom/wssnps/b/l;->aN:Z

    .line 1046
    :cond_4
    const-wide/16 v8, 0x10

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_5

    .line 1048
    const/4 v0, 0x1

    if-ne v6, v0, :cond_11

    .line 1050
    invoke-static {}, Lcom/wssnps/b/l;->s()Z

    move-result v0

    .line 1051
    if-eqz v0, :cond_10

    const/4 v0, 0x0

    :goto_2
    aput v0, v4, v1

    .line 1052
    const/16 v0, 0x10

    aput v0, v5, v1

    .line 1053
    add-int/lit8 v1, v1, 0x1

    .line 1063
    :cond_5
    :goto_3
    const-wide/16 v8, 0x20

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v0, v8, v10

    if-eqz v0, :cond_1e

    .line 1065
    invoke-static {}, Lcom/wssnps/b/l;->q()Z

    move-result v0

    .line 1066
    const/4 v7, 0x1

    if-ne v0, v7, :cond_6

    .line 1068
    invoke-static {}, Lcom/wssnps/b/l;->r()Z

    move-result v0

    .line 1070
    :cond_6
    if-eqz v0, :cond_12

    const/4 v0, 0x0

    :goto_4
    aput v0, v4, v1

    .line 1071
    const/16 v0, 0x20

    aput v0, v5, v1

    .line 1072
    add-int/lit8 v0, v1, 0x1

    .line 1073
    const/4 v1, 0x1

    sput-boolean v1, Lcom/wssnps/b/l;->aO:Z

    .line 1076
    :goto_5
    const-wide/16 v8, 0x100

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_7

    .line 1078
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1079
    const/16 v1, 0x100

    aput v1, v5, v0

    .line 1080
    add-int/lit8 v0, v0, 0x1

    .line 1083
    :cond_7
    const-wide/16 v8, 0x200

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_8

    .line 1085
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1086
    const/16 v1, 0x200

    aput v1, v5, v0

    .line 1087
    add-int/lit8 v0, v0, 0x1

    .line 1090
    :cond_8
    const-wide/16 v8, 0x400

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_9

    .line 1092
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1093
    const/16 v1, 0x400

    aput v1, v5, v0

    .line 1094
    add-int/lit8 v0, v0, 0x1

    .line 1097
    :cond_9
    const-wide/32 v8, 0x40000

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_a

    .line 1099
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1100
    const/high16 v1, 0x40000

    aput v1, v5, v0

    .line 1101
    add-int/lit8 v0, v0, 0x1

    .line 1104
    :cond_a
    const-wide/16 v8, 0x800

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_b

    .line 1106
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1107
    const/16 v1, 0x800

    aput v1, v5, v0

    .line 1108
    add-int/lit8 v0, v0, 0x1

    .line 1111
    :cond_b
    const-wide/16 v8, 0x1000

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_c

    .line 1113
    invoke-static {}, Lcom/wssnps/b/l;->E()V

    .line 1114
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1115
    const/16 v1, 0x1000

    aput v1, v5, v0

    .line 1116
    add-int/lit8 v0, v0, 0x1

    .line 1119
    :cond_c
    const-wide/16 v8, 0x4000

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_d

    .line 1121
    invoke-static {}, Lcom/wssnps/b/l;->O()V

    .line 1122
    invoke-static {}, Lcom/wssnps/b/l;->N()Ljava/lang/Object;

    .line 1123
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1124
    const/16 v1, 0x4000

    aput v1, v5, v0

    .line 1125
    add-int/lit8 v0, v0, 0x1

    .line 1128
    :cond_d
    const-wide/32 v8, 0x10000

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_14

    .line 1130
    invoke-static {}, Lcom/wssnps/b/l;->ar()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-static {}, Lcom/wssnps/b/l;->aq()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1132
    const/4 v0, 0x1

    const-string v1, "VerifierEnabled!!"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1133
    const-string v0, "4\n"

    .line 1209
    :goto_6
    return-object v0

    .line 1028
    :cond_e
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1040
    :cond_f
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 1051
    :cond_10
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 1055
    :cond_11
    const/4 v0, 0x2

    if-ne v6, v0, :cond_5

    .line 1057
    const/4 v0, 0x0

    aput v0, v4, v1

    .line 1058
    const/16 v0, 0x10

    aput v0, v5, v1

    .line 1059
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    .line 1070
    :cond_12
    const/4 v0, 0x1

    goto/16 :goto_4

    .line 1136
    :cond_13
    invoke-static {}, Lcom/wssnps/b/l;->an()I

    move-result v1

    aput v1, v4, v0

    .line 1137
    const/high16 v1, 0x10000

    aput v1, v5, v0

    .line 1138
    add-int/lit8 v0, v0, 0x1

    .line 1139
    const/4 v1, 0x0

    sput v1, Lcom/wssnps/b/l;->aR:I

    .line 1140
    const/4 v1, 0x0

    sput v1, Lcom/wssnps/b/l;->aQ:I

    .line 1143
    :cond_14
    const-wide/32 v8, 0x8000

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_15

    .line 1145
    invoke-static {}, Lcom/wssnps/b/l;->W()V

    .line 1146
    invoke-static {}, Lcom/wssnps/b/l;->V()Ljava/lang/Object;

    .line 1147
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1148
    const v1, 0x8000

    aput v1, v5, v0

    .line 1149
    add-int/lit8 v0, v0, 0x1

    .line 1152
    :cond_15
    const-wide/16 v8, 0x40

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_16

    .line 1154
    invoke-static {}, Lcom/wssnps/b/l;->x()V

    .line 1155
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1156
    const/16 v1, 0x40

    aput v1, v5, v0

    .line 1157
    add-int/lit8 v0, v0, 0x1

    .line 1160
    :cond_16
    const-wide/32 v8, 0x1000000

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_17

    .line 1162
    invoke-static {}, Lcom/wssnps/b/l;->Z()V

    .line 1163
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1164
    const/high16 v1, 0x1000000

    aput v1, v5, v0

    .line 1165
    add-int/lit8 v0, v0, 0x1

    .line 1168
    :cond_17
    const-wide/32 v8, 0x2000000

    and-long/2addr v8, v2

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-eqz v1, :cond_18

    .line 1170
    invoke-static {}, Lcom/wssnps/b/l;->ae()V

    .line 1171
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1172
    const/high16 v1, 0x2000000

    aput v1, v5, v0

    .line 1173
    add-int/lit8 v0, v0, 0x1

    .line 1176
    :cond_18
    const-wide/32 v8, 0x40000000

    and-long/2addr v2, v8

    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-eqz v1, :cond_1a

    .line 1178
    const/4 v1, 0x3

    if-ne v6, v1, :cond_19

    .line 1179
    invoke-static {}, Lcom/wssnps/b/l;->ak()V

    .line 1180
    :cond_19
    const/4 v1, 0x0

    aput v1, v4, v0

    .line 1181
    const/high16 v1, 0x40000000    # 2.0f

    aput v1, v5, v0

    .line 1182
    add-int/lit8 v0, v0, 0x1

    .line 1198
    :cond_1a
    sget-object v1, Lcom/wssnps/b/y;->D:Ljava/util/HashMap;

    if-eqz v1, :cond_1b

    .line 1200
    sget-object v1, Lcom/wssnps/b/y;->D:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 1203
    :cond_1b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1204
    const/4 v1, 0x0

    move v12, v1

    move-object v1, v2

    move v2, v12

    :goto_7
    if-ge v2, v0, :cond_1d

    .line 1206
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget v6, v5, v2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1207
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget v6, v4, v2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "\n"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1204
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v3

    goto :goto_7

    .line 1188
    :cond_1c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "2\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x18

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    :cond_1d
    move-object v0, v1

    .line 1209
    goto/16 :goto_6

    :cond_1e
    move v0, v1

    goto/16 :goto_5
.end method

.method public static d([B)[B
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3865
    .line 3868
    const-string v0, "RestoreApplicationStart"

    invoke-static {v5, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 3870
    sget v0, Lcom/wssnps/b/l;->aR:I

    if-nez v0, :cond_1

    .line 3872
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 3873
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.KIES_START_RESTORE_APK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3875
    if-eqz p0, :cond_0

    .line 3877
    new-array v1, v6, [B

    .line 3878
    array-length v2, p0

    add-int/lit8 v2, v2, -0x6

    new-array v2, v2, [B

    .line 3879
    invoke-static {p0, v4, v1, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3880
    array-length v3, p0

    add-int/lit8 v3, v3, -0x6

    invoke-static {p0, v6, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3881
    const-string v3, "head"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3882
    const-string v1, "body"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 3883
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3886
    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/wssnps/b/l;->aP:[B

    .line 3887
    sput v4, Lcom/wssnps/b/l;->aQ:I

    .line 3888
    sput v5, Lcom/wssnps/b/l;->aR:I

    .line 3889
    sget-object v1, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 3891
    sget-object v0, Lcom/wssnps/b/l;->aT:[B

    sput-object v0, Lcom/wssnps/b/l;->aP:[B

    .line 3892
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->w(Landroid/content/Context;)V

    .line 3904
    :goto_0
    sget-object v0, Lcom/wssnps/b/l;->aP:[B

    return-object v0

    .line 3894
    :cond_1
    sget v0, Lcom/wssnps/b/l;->aR:I

    if-ne v0, v5, :cond_2

    sget v0, Lcom/wssnps/b/l;->aQ:I

    if-nez v0, :cond_2

    .line 3896
    sget-object v0, Lcom/wssnps/b/l;->aT:[B

    sput-object v0, Lcom/wssnps/b/l;->aP:[B

    goto :goto_0

    .line 3900
    :cond_2
    sput v4, Lcom/wssnps/b/l;->aR:I

    .line 3901
    sput v4, Lcom/wssnps/b/l;->aQ:I

    goto :goto_0
.end method

.method public static e()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 1565
    sget-wide v0, Lcom/wssnps/b/l;->ao:J

    .line 1566
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sput-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    .line 1567
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "smlBRRestoreFinish "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1569
    cmp-long v2, v0, v4

    if-eqz v2, :cond_14

    .line 1571
    sput-boolean v6, Lcom/wssnps/b/l;->av:Z

    .line 1572
    sput-boolean v6, Lcom/wssnps/b/l;->ax:Z

    .line 1573
    sput-boolean v6, Lcom/wssnps/b/l;->az:Z

    .line 1574
    sput-boolean v6, Lcom/wssnps/b/l;->aB:Z

    .line 1575
    sput-boolean v6, Lcom/wssnps/b/l;->aD:Z

    .line 1576
    sput-boolean v6, Lcom/wssnps/b/l;->aF:Z

    .line 1577
    sput-boolean v6, Lcom/wssnps/b/l;->aH:Z

    .line 1579
    sput-boolean v6, Lcom/wssnps/b/l;->aJ:Z

    .line 1581
    const-wide/16 v2, 0x1

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 1583
    sput v6, Lcom/wssnps/b;->m:I

    .line 1584
    invoke-static {}, Lcom/wssnps/b/l;->l()V

    .line 1587
    :cond_0
    const-wide/16 v2, 0x4

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 1591
    :cond_1
    const-wide/16 v2, 0x8

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 1593
    const-string v2, "0"

    invoke-static {v2}, Lcom/wssnps/b;->y(Ljava/lang/String;)Ljava/lang/String;

    .line 1594
    sput-boolean v7, Lcom/wssnps/b/l;->aN:Z

    .line 1597
    :cond_2
    const-wide/16 v2, 0x10

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 1601
    :cond_3
    const-wide/16 v2, 0x20

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 1603
    sput-boolean v7, Lcom/wssnps/b/l;->aO:Z

    .line 1606
    :cond_4
    const-wide/16 v2, 0x100

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 1610
    :cond_5
    const-wide/16 v2, 0x200

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 1614
    :cond_6
    const-wide/16 v2, 0x400

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    .line 1618
    :cond_7
    const-wide/32 v2, 0x40000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 1622
    :cond_8
    const-wide/16 v2, 0x800

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 1626
    :cond_9
    const-wide/16 v2, 0x1000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 1628
    const-string v2, "com.android.email"

    invoke-static {v2}, Lcom/wssnps/b/l;->e(Ljava/lang/String;)V

    .line 1629
    invoke-static {}, Lcom/wssnps/b/l;->E()V

    .line 1632
    :cond_a
    const-wide/16 v2, 0x2000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 1636
    :cond_b
    const-wide/16 v2, 0x4000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 1638
    invoke-static {}, Lcom/wssnps/smlModelDefine;->c()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1639
    const-string v2, "com.sec.android.app.fm"

    invoke-static {v2}, Lcom/wssnps/b/l;->e(Ljava/lang/String;)V

    .line 1640
    :cond_c
    const-string v2, "com.android.browser"

    invoke-static {v2}, Lcom/wssnps/b/l;->e(Ljava/lang/String;)V

    .line 1641
    invoke-static {}, Lcom/wssnps/b/l;->O()V

    .line 1644
    :cond_d
    const-wide/32 v2, 0x10000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    .line 1646
    invoke-static {}, Lcom/wssnps/b/l;->ao()I

    .line 1649
    :cond_e
    const-wide/32 v2, 0x8000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    .line 1651
    invoke-static {}, Lcom/wssnps/b/l;->W()V

    .line 1654
    :cond_f
    const-wide/16 v2, 0x40

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    .line 1656
    sput v6, Lcom/wssnps/b;->m:I

    .line 1657
    const-string v2, "com.android.providers.telephony"

    invoke-static {v2}, Lcom/wssnps/b/l;->e(Ljava/lang/String;)V

    .line 1658
    invoke-static {}, Lcom/wssnps/b/l;->x()V

    .line 1661
    :cond_10
    const-wide/32 v2, 0x1000000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_11

    .line 1663
    sput v6, Lcom/wssnps/b;->m:I

    .line 1664
    const-string v2, "com.sec.android.app.minidiary"

    invoke-static {v2}, Lcom/wssnps/b/l;->e(Ljava/lang/String;)V

    .line 1665
    invoke-static {}, Lcom/wssnps/b/l;->Z()V

    .line 1668
    :cond_11
    const-wide/32 v2, 0x2000000

    and-long/2addr v2, v0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    .line 1670
    const-string v2, "com.diotek.penmemo"

    invoke-static {v2}, Lcom/wssnps/b/l;->e(Ljava/lang/String;)V

    .line 1671
    invoke-static {}, Lcom/wssnps/b/l;->ae()V

    .line 1674
    :cond_12
    const-wide/32 v2, 0x40000000

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_13

    .line 1676
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_13

    .line 1678
    const-string v0, "com.sec.android.widgetapp.diotek.smemo"

    invoke-static {v0}, Lcom/wssnps/b/l;->e(Ljava/lang/String;)V

    .line 1679
    invoke-static {}, Lcom/wssnps/b/l;->ak()V

    .line 1689
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1686
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "2\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1695
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1696
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 1697
    if-eqz v1, :cond_2

    .line 1699
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1701
    iget-object v1, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1703
    const-string v1, "com.android.email"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1705
    invoke-virtual {v0, p0}, Landroid/app/ActivityManager;->killBackgroundProcesses(Ljava/lang/String;)V

    goto :goto_0

    .line 1707
    :cond_1
    invoke-virtual {v0, p0}, Landroid/app/ActivityManager;->killBackgroundProcesses(Ljava/lang/String;)V

    goto :goto_0

    .line 1711
    :cond_2
    return-void
.end method

.method public static f()V
    .locals 4

    .prologue
    .line 1715
    const/4 v0, 0x1

    const-string v1, "smlBRConfigurationBackup"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1717
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Configuration"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 1719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Configuration/Configuration_DB.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    .line 1720
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "create table if not exists configuration (_id integer primary key autoincrement, name text, value text);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1721
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "create table if not exists global (_id integer primary key autoincrement, name text, value text);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1723
    invoke-static {}, Lcom/wssnps/b/l;->h()Ljava/lang/Object;

    .line 1726
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->j()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1734
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1736
    :goto_0
    return-void

    .line 1728
    :catch_0
    move-exception v0

    .line 1730
    const/4 v1, 0x3

    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "smlBRConfigurationCompress"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1734
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v0
.end method

.method public static g()V
    .locals 4

    .prologue
    .line 1740
    const/4 v0, 0x1

    const-string v1, "smlBRConfigurationRestore"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1743
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->k()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1749
    :goto_0
    invoke-static {}, Lcom/wssnps/b/l;->i()Ljava/lang/Object;

    .line 1750
    return-void

    .line 1745
    :catch_0
    move-exception v0

    .line 1747
    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "smlBRConfigurationDecompress"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static h()Ljava/lang/Object;
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 1754
    const-string v0, "smlBRConfigurationGet"

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1756
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1757
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1760
    const-string v3, ""

    .line 1766
    sget-object v1, Lcom/wssnps/b/l;->a:Landroid/net/Uri;

    .line 1767
    sget-object v10, Lcom/wssnps/b/l;->b:Landroid/net/Uri;

    .line 1769
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1770
    sget-object v4, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget-object v5, Lcom/wssnps/b/l;->cF:[[Ljava/lang/String;

    const/16 v11, 0x8

    aget-object v5, v5, v11

    aget-object v5, v5, v2

    invoke-static {v4, v5}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 1771
    if-eqz v4, :cond_d

    :goto_0
    move v4, v6

    .line 1774
    :goto_1
    sget-object v5, Lcom/wssnps/b/l;->c:[Ljava/lang/String;

    array-length v5, v5

    if-ge v4, v5, :cond_2

    .line 1776
    if-eqz v2, :cond_1

    .line 1778
    const-string v5, "ringtone"

    sget-object v11, Lcom/wssnps/b/l;->c:[Ljava/lang/String;

    aget-object v11, v11, v4

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "ringtone_2"

    sget-object v11, Lcom/wssnps/b/l;->c:[Ljava/lang/String;

    aget-object v11, v11, v4

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1774
    :cond_0
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1782
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "name LIKE \'"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v11, Lcom/wssnps/b/l;->c:[Ljava/lang/String;

    aget-object v11, v11, v4

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, "\'"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1783
    sget-object v5, Lcom/wssnps/b/l;->c:[Ljava/lang/String;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_0

    .line 1784
    const-string v5, " or "

    invoke-virtual {v3, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 1789
    :cond_2
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1790
    if-eqz v1, :cond_4

    .line 1792
    :try_start_1
    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1793
    const-string v3, "name"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 1794
    const-string v4, "value"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 1796
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_4

    .line 1798
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1802
    :cond_3
    new-instance v5, Lcom/wssnps/b/w;

    const/4 v7, 0x0

    invoke-direct {v5, v7}, Lcom/wssnps/b/w;-><init>(Lcom/wssnps/b/m;)V

    .line 1803
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/wssnps/b/w;->a:I

    .line 1804
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    .line 1805
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/wssnps/b/w;->c:Ljava/lang/String;

    .line 1806
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1807
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    if-nez v5, :cond_3

    .line 1818
    :cond_4
    if-eqz v1, :cond_c

    .line 1819
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v7, v1

    .line 1822
    :goto_3
    const-string v3, ""

    move v2, v6

    .line 1823
    :goto_4
    sget-object v1, Lcom/wssnps/b/l;->d:[Ljava/lang/String;

    array-length v1, v1

    if-ge v2, v1, :cond_7

    .line 1825
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "name LIKE \'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, Lcom/wssnps/b/l;->d:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1826
    sget-object v3, Lcom/wssnps/b/l;->d:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_5

    .line 1827
    const-string v3, " or "

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1823
    :cond_5
    add-int/lit8 v2, v2, 0x1

    move-object v3, v1

    goto :goto_4

    .line 1812
    :catch_0
    move-exception v1

    move-object v2, v1

    move-object v1, v7

    .line 1814
    :goto_5
    const/4 v3, 0x3

    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1818
    if-eqz v1, :cond_c

    .line 1819
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v7, v1

    goto :goto_3

    .line 1818
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_6
    if-eqz v1, :cond_6

    .line 1819
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 1832
    :cond_7
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v10

    :try_start_3
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1833
    if-eqz v7, :cond_9

    .line 1835
    const-string v0, "_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1836
    const-string v1, "name"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 1837
    const-string v2, "value"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1839
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_9

    .line 1841
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1845
    :cond_8
    new-instance v3, Lcom/wssnps/b/w;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/wssnps/b/w;-><init>(Lcom/wssnps/b/m;)V

    .line 1846
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/wssnps/b/w;->a:I

    .line 1847
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    .line 1848
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/wssnps/b/w;->c:Ljava/lang/String;

    .line 1849
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1850
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v3

    if-nez v3, :cond_8

    .line 1861
    :cond_9
    if-eqz v7, :cond_a

    .line 1862
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1865
    :cond_a
    :goto_7
    invoke-static {v8, v9}, Lcom/wssnps/b/l;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 1866
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 1855
    :catch_1
    move-exception v0

    .line 1857
    const/4 v1, 0x3

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1861
    if-eqz v7, :cond_a

    .line 1862
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_7

    .line 1861
    :catchall_1
    move-exception v0

    if-eqz v7, :cond_b

    .line 1862
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v0

    .line 1818
    :catchall_2
    move-exception v0

    goto :goto_6

    .line 1812
    :catch_2
    move-exception v2

    goto/16 :goto_5

    :cond_c
    move-object v7, v1

    goto/16 :goto_3

    :cond_d
    move v2, v6

    goto/16 :goto_0
.end method

.method public static i()Ljava/lang/Object;
    .locals 15

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v11, 0x0

    const/4 v0, 0x1

    const/4 v12, 0x0

    .line 1912
    const-string v1, "smlBRRestoreConfigurationSql"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1914
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1915
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1921
    new-array v3, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v3, v11

    const-string v1, "name"

    aput-object v1, v3, v0

    const-string v1, "value"

    aput-object v1, v3, v2

    .line 1923
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    sget-object v2, Lcom/wssnps/b/l;->cF:[[Ljava/lang/String;

    const/16 v4, 0x8

    aget-object v2, v2, v4

    aget-object v2, v2, v0

    invoke-static {v1, v2}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1924
    if-eqz v1, :cond_6

    move v10, v0

    .line 1927
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Configuration/Configuration_DB.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/database/sqlite/SQLiteDatabase;->openOrCreateDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    .line 1931
    :try_start_0
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "configuration"

    const-string v4, "_id>0"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1932
    :try_start_1
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1933
    const-string v2, "name"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1934
    const-string v4, "value"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 1936
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_2

    .line 1938
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1942
    :cond_0
    new-instance v5, Lcom/wssnps/b/w;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/wssnps/b/w;-><init>(Lcom/wssnps/b/m;)V

    .line 1943
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    iput v6, v5, Lcom/wssnps/b/w;->a:I

    .line 1944
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    .line 1945
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/wssnps/b/w;->c:Ljava/lang/String;

    .line 1947
    if-eqz v10, :cond_5

    .line 1949
    const-string v6, "ringtone"

    iget-object v7, v5, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "ringtone_2"

    iget-object v7, v5, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1954
    :cond_1
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result v5

    if-nez v5, :cond_0

    .line 1964
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v10, v1

    .line 1969
    :goto_2
    :try_start_2
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "global"

    const-string v4, "_id>0"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v1

    .line 1970
    :try_start_3
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 1971
    const-string v2, "name"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1972
    const-string v3, "value"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 1974
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_4

    .line 1976
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1980
    :cond_3
    new-instance v4, Lcom/wssnps/b/w;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/wssnps/b/w;-><init>(Lcom/wssnps/b/m;)V

    .line 1981
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v4, Lcom/wssnps/b/w;->a:I

    .line 1982
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/wssnps/b/w;->b:Ljava/lang/String;

    .line 1983
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/wssnps/b/w;->c:Ljava/lang/String;

    .line 1984
    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1985
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v4

    if-nez v4, :cond_3

    .line 1995
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1996
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 1999
    :goto_3
    invoke-static {v13, v14}, Lcom/wssnps/b/l;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/Object;

    .line 2000
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 1953
    :cond_5
    :try_start_4
    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_1

    .line 1958
    :catch_0
    move-exception v0

    .line 1960
    :goto_4
    const/4 v2, 0x3

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 1964
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v10, v1

    .line 1965
    goto :goto_2

    .line 1964
    :catchall_0
    move-exception v0

    move-object v1, v12

    :goto_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1989
    :catch_1
    move-exception v0

    move-object v1, v10

    .line 1991
    :goto_6
    const/4 v2, 0x2

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1995
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1996
    sget-object v0, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_3

    .line 1995
    :catchall_1
    move-exception v0

    move-object v1, v10

    :goto_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1996
    sget-object v1, Lcom/wssnps/b/l;->aU:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v0

    .line 1995
    :catchall_2
    move-exception v0

    goto :goto_7

    .line 1989
    :catch_2
    move-exception v0

    goto :goto_6

    .line 1964
    :catchall_3
    move-exception v0

    goto :goto_5

    .line 1958
    :catch_3
    move-exception v0

    move-object v1, v12

    goto :goto_4

    :cond_6
    move v10, v11

    goto/16 :goto_0
.end method

.method public static j()V
    .locals 3

    .prologue
    .line 2056
    const/4 v0, 0x1

    const-string v1, "smlBRConfigurationCompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2057
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Configuration"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Configuration.bk"

    invoke-static {v0, v1, v2}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2058
    return-void
.end method

.method public static k()V
    .locals 3

    .prologue
    .line 2062
    const/4 v0, 0x1

    const-string v1, "smlBRConfigurationDecompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2063
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Configuration.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/Configuration"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2064
    return-void
.end method

.method public static l()V
    .locals 2

    .prologue
    .line 2068
    const/4 v0, 0x1

    const-string v1, "smlBRConfigurationDelete"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2069
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Configuration"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 2070
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Configuration.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 2071
    return-void
.end method

.method public static m()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2077
    const-string v2, "smlBRContactsDeleteGroup"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2079
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v2, :cond_2

    .line 2081
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2084
    :goto_0
    if-nez v2, :cond_0

    move v2, v1

    .line 2095
    :goto_1
    if-ltz v2, :cond_1

    .line 2098
    :goto_2
    return v0

    .line 2090
    :cond_0
    const-string v4, "account_type=\"vnd.sec.contact.phone\""

    .line 2091
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND SYSTEM_ID is null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2092
    sget-object v5, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v5, v4, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    :cond_1
    move v0, v1

    .line 2098
    goto :goto_2

    :cond_2
    move-object v2, v3

    goto :goto_0
.end method

.method public static n()Z
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2106
    const-string v0, "smlBRContactsDelete"

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2108
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v0, :cond_6

    .line 2110
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2113
    :goto_0
    if-nez v0, :cond_1

    .line 2199
    :goto_1
    if-ltz v6, :cond_0

    move v6, v7

    .line 2202
    :cond_0
    :goto_2
    return v6

    .line 2119
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2122
    const-string v3, "account_type=\"vnd.sec.contact.phone\" AND deleted=0"

    .line 2123
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v2, v6

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2125
    if-eqz v2, :cond_5

    .line 2127
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v6

    .line 2133
    :cond_2
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 2134
    sget-object v4, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v9, v7, [Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2138
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "raw_contact_id=?"

    new-array v9, v7, [Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v6

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2143
    add-int/lit8 v1, v1, 0x1

    const/16 v3, 0x1f4

    if-lt v1, v3, :cond_3

    .line 2147
    :try_start_0
    const-string v1, "com.android.contacts"

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 2148
    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    :goto_3
    move v1, v6

    .line 2168
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2170
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2175
    :cond_5
    :try_start_1
    const-string v1, "com.android.contacts"

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7

    goto :goto_1

    .line 2177
    :catch_0
    move-exception v0

    .line 2179
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2

    .line 2150
    :catch_1
    move-exception v1

    .line 2152
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 2154
    :catch_2
    move-exception v1

    .line 2156
    invoke-virtual {v1}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_3

    .line 2158
    :catch_3
    move-exception v1

    .line 2160
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDiskIOException;->printStackTrace()V

    goto :goto_3

    .line 2162
    :catch_4
    move-exception v1

    .line 2164
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 2182
    :catch_5
    move-exception v0

    .line 2184
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto/16 :goto_2

    .line 2187
    :catch_6
    move-exception v0

    .line 2189
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDiskIOException;->printStackTrace()V

    goto/16 :goto_2

    .line 2192
    :catch_7
    move-exception v0

    .line 2194
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    :cond_6
    move-object v0, v4

    goto/16 :goto_0
.end method

.method public static o()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2209
    const-string v2, "smlBRCalendarGroupDelete"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2211
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v2, :cond_2

    .line 2213
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2216
    :goto_0
    if-nez v2, :cond_0

    .line 2226
    :goto_1
    if-ltz v1, :cond_1

    .line 2229
    :goto_2
    return v0

    .line 2222
    :cond_0
    const-string v4, "_id!=\"1\" AND account_name=\"My calendar\""

    .line 2223
    sget-object v5, Lcom/wssnps/b/l;->f:Landroid/net/Uri;

    invoke-virtual {v2, v5, v4, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    move v0, v1

    .line 2229
    goto :goto_2

    :cond_2
    move-object v2, v3

    goto :goto_0
.end method

.method public static p()Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 2237
    const-string v0, "smlBRCalendarDelete"

    invoke-static {v6, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2239
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v0, :cond_5

    .line 2241
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2244
    :goto_0
    if-nez v0, :cond_1

    .line 2267
    :cond_0
    :goto_1
    if-ltz v7, :cond_4

    move v0, v6

    .line 2270
    :goto_2
    return v0

    .line 2250
    :cond_1
    const-string v3, "account_name=\"My calendar\""

    .line 2251
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2252
    if-eqz v1, :cond_0

    .line 2254
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2258
    :cond_2
    const-string v3, "_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 2259
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2260
    sget-object v4, Lcom/wssnps/b/l;->e:Landroid/net/Uri;

    invoke-virtual {v0, v4, v3, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2261
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2263
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_4
    move v0, v7

    .line 2270
    goto :goto_2

    :cond_5
    move-object v0, v2

    goto :goto_0
.end method

.method public static q()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2278
    const-string v2, "smlBRTaskGroupDelete"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2280
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v2, :cond_2

    .line 2282
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2285
    :goto_0
    if-nez v2, :cond_0

    .line 2298
    :goto_1
    if-ltz v1, :cond_1

    .line 2301
    :goto_2
    return v0

    .line 2291
    :cond_0
    const-string v4, "content://com.android.calendar/taskGroup"

    .line 2293
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2294
    const-string v5, "_id != 1"

    .line 2295
    invoke-virtual {v2, v4, v5, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    move v0, v1

    .line 2301
    goto :goto_2

    :cond_2
    move-object v2, v3

    goto :goto_0
.end method

.method public static r()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2308
    const-string v2, "smlBRTaskDelete"

    invoke-static {v0, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2310
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v2, :cond_3

    .line 2312
    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2315
    :goto_0
    if-nez v2, :cond_0

    .line 2330
    :goto_1
    if-ltz v1, :cond_2

    .line 2333
    :goto_2
    return v0

    .line 2321
    :cond_0
    const-string v4, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2322
    const-string v4, "content://com.android.calendar/syncTasks"

    sput-object v4, Lcom/wssnps/b/l;->i:Ljava/lang/String;

    .line 2325
    :goto_3
    sget-object v4, Lcom/wssnps/b/l;->i:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2326
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "caller_is_syncadapter"

    const-string v7, "true"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2327
    invoke-virtual {v2, v4, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 2324
    :cond_1
    const-string v4, "content://tasks/tasks"

    sput-object v4, Lcom/wssnps/b/l;->i:Ljava/lang/String;

    goto :goto_3

    :cond_2
    move v0, v1

    .line 2333
    goto :goto_2

    :cond_3
    move-object v2, v3

    goto :goto_0
.end method

.method public static s()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2341
    const-string v0, "Tablet"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2342
    sget-object v0, Lcom/wssnps/b/l;->h:Landroid/net/Uri;

    .line 2346
    :goto_0
    const-string v3, "smlBRMemoDelete"

    invoke-static {v1, v3}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2348
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v3, :cond_3

    .line 2350
    sget-object v3, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 2353
    :goto_1
    if-nez v3, :cond_1

    .line 2362
    :goto_2
    if-ltz v2, :cond_2

    move v0, v1

    .line 2365
    :goto_3
    return v0

    .line 2344
    :cond_0
    sget-object v0, Lcom/wssnps/b/l;->g:Landroid/net/Uri;

    goto :goto_0

    .line 2359
    :cond_1
    const-string v5, ""

    invoke-virtual {v3, v0, v5, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    move v0, v2

    .line 2365
    goto :goto_3

    :cond_3
    move-object v3, v4

    goto :goto_1
.end method

.method public static t()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 2372
    const-string v0, "smlBRMessageBackup"

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2374
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    .line 2376
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 2383
    :goto_0
    if-nez v1, :cond_2

    .line 2385
    sput-boolean v7, Lcom/wssnps/b/l;->au:Z

    .line 2404
    :cond_0
    :goto_1
    return-void

    .line 2380
    :cond_1
    const-string v0, "smlBRMessageBackup Fail!!!"

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    move-object v1, v3

    goto :goto_0

    .line 2389
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/_SamsungBnR_/BR/Message"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 2390
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "msgbackup"

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "target_directory_path"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/_SamsungBnR_/BR/Message"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 2391
    sget-object v0, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-static/range {v0 .. v6}, Landroid/database/sqlite/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2394
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->v()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2400
    :goto_2
    sput-boolean v7, Lcom/wssnps/b/l;->au:Z

    .line 2402
    if-eqz v0, :cond_0

    .line 2403
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 2396
    :catch_0
    move-exception v1

    .line 2398
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public static u()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2408
    const/4 v0, 0x0

    .line 2410
    const-string v1, "smlBRMessageRestore"

    invoke-static {v5, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2412
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 2414
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2421
    :goto_0
    if-nez v0, :cond_1

    .line 2423
    sput-boolean v5, Lcom/wssnps/b/l;->av:Z

    .line 2439
    :goto_1
    return-void

    .line 2418
    :cond_0
    const-string v1, "smlBRMessageRestore Fail!!!"

    invoke-static {v5, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 2429
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->w()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2436
    :goto_2
    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "msgrestore"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "source_directory_path"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/_SamsungBnR_/BR/Message"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2437
    sget-object v2, Lcom/wssnps/b/l;->an:Landroid/content/Context;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v2, v0, v1, v3}, Landroid/database/sqlite/SqliteWrapper;->insert(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2438
    sput-boolean v5, Lcom/wssnps/b/l;->av:Z

    goto :goto_1

    .line 2431
    :catch_0
    move-exception v1

    .line 2433
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public static v()V
    .locals 3

    .prologue
    .line 2443
    const/4 v0, 0x1

    const-string v1, "smlBRMessageCompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2444
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Message.bk"

    invoke-static {v0, v1, v2}, Lcom/wssnps/smlModelDefine;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2445
    return-void
.end method

.method public static w()V
    .locals 3

    .prologue
    .line 2449
    const/4 v0, 0x1

    const-string v1, "smlBRMessageDecompress"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Message.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/_SamsungBnR_/BR/Message"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2451
    return-void
.end method

.method public static x()V
    .locals 2

    .prologue
    .line 2455
    const/4 v0, 0x1

    const-string v1, "smlBRMessageDelete"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 2457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Message.bk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 2458
    return-void
.end method

.method public static y()V
    .locals 2

    .prologue
    .line 2462
    const/4 v0, 0x1

    const-string v1, "smlBREmailBackup"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_/BR/Email"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->h(Ljava/lang/String;)Z

    .line 2466
    invoke-static {}, Lcom/wssnps/b/l;->A()V

    .line 2467
    return-void
.end method

.method public static z()V
    .locals 2

    .prologue
    .line 2471
    const/4 v0, 0x1

    const-string v1, "smlBREmailRestore"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 2474
    :try_start_0
    invoke-static {}, Lcom/wssnps/b/l;->D()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2480
    :goto_0
    invoke-static {}, Lcom/wssnps/b/l;->B()V

    .line 2481
    return-void

    .line 2476
    :catch_0
    move-exception v0

    .line 2478
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
