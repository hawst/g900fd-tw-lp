.class public Lcom/wssnps/b/x;
.super Ljava/lang/Object;
.source "smlCalendarItem.java"

# interfaces
.implements Lcom/wssnps/a/b;


# static fields
.field public static H:Ljava/lang/String;

.field public static J:Lcom/wssnps/smlModelDefine;


# instance fields
.field public A:Ljava/util/ArrayList;

.field public B:Ljava/util/ArrayList;

.field public C:Ljava/util/ArrayList;

.field public D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field public F:Ljava/lang/String;

.field public G:I

.field public I:I

.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public j:I

.field public k:I

.field public l:Ljava/lang/String;

.field public m:Ljava/util/List;

.field public n:Ljava/util/List;

.field public o:Ljava/lang/String;

.field public p:[J

.field public q:Ljava/util/ArrayList;

.field public r:Ljava/lang/String;

.field public s:I

.field public t:Ljava/lang/String;

.field public u:J

.field public v:Ljava/lang/String;

.field public w:J

.field public x:I

.field public y:I

.field public z:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-string v0, ""

    sput-object v0, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    .line 87
    new-instance v0, Lcom/wssnps/smlModelDefine;

    invoke-direct {v0}, Lcom/wssnps/smlModelDefine;-><init>()V

    sput-object v0, Lcom/wssnps/b/x;->J:Lcom/wssnps/smlModelDefine;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->m:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->n:Ljava/util/List;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Lcom/wssnps/b/x;->I:I

    .line 149
    return-void
.end method

.method public constructor <init>(Lcom/wssnps/a/r;)V
    .locals 12

    .prologue
    const-wide/32 v10, 0xea60

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->m:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->n:Ljava/util/List;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    .line 85
    iput v2, p0, Lcom/wssnps/b/x;->I:I

    .line 153
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 154
    if-nez p1, :cond_1

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    iget v1, p1, Lcom/wssnps/a/r;->a:I

    iput v1, p0, Lcom/wssnps/b/x;->a:I

    .line 159
    iget-object v1, p1, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 160
    iget-object v1, p1, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    .line 162
    :cond_2
    iget-object v1, p1, Lcom/wssnps/a/r;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->c:Ljava/lang/String;

    .line 163
    iget-object v1, p1, Lcom/wssnps/a/r;->f:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->d:Ljava/lang/String;

    .line 164
    iget-object v1, p1, Lcom/wssnps/a/r;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->o:Ljava/lang/String;

    .line 166
    iget-object v1, p1, Lcom/wssnps/a/r;->A:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 167
    iget-object v1, p1, Lcom/wssnps/a/r;->A:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->l:Ljava/lang/String;

    .line 168
    :cond_3
    iget-object v1, p1, Lcom/wssnps/a/r;->B:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 169
    iget-object v1, p1, Lcom/wssnps/a/r;->B:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->D:Ljava/lang/String;

    .line 170
    :cond_4
    iget-object v1, p1, Lcom/wssnps/a/r;->C:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 171
    iget-object v1, p1, Lcom/wssnps/a/r;->C:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->E:Ljava/lang/String;

    .line 172
    :cond_5
    iget-object v1, p1, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 173
    iget-object v1, p1, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    .line 174
    :cond_6
    iget-object v1, p1, Lcom/wssnps/a/r;->E:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 176
    iget-object v1, p1, Lcom/wssnps/a/r;->E:Ljava/lang/String;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/wssnps/b/x;->G:I

    .line 181
    :cond_7
    iget-object v1, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-eqz v1, :cond_8

    .line 182
    iget-object v1, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/wssnps/b/x;->e:J

    .line 184
    :cond_8
    iget-object v1, p1, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    if-eqz v1, :cond_9

    .line 185
    iget-object v1, p1, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/wssnps/b/x;->f:J

    .line 187
    :cond_9
    iget v1, p1, Lcom/wssnps/a/r;->z:I

    iput v1, p0, Lcom/wssnps/b/x;->y:I

    .line 189
    iget v1, p1, Lcom/wssnps/a/r;->t:I

    iput v1, p0, Lcom/wssnps/b/x;->i:I

    .line 191
    invoke-static {}, Lcom/wssnps/smlModelDefine;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 192
    iget v1, p1, Lcom/wssnps/a/r;->v:I

    iput v1, p0, Lcom/wssnps/b/x;->j:I

    .line 194
    :cond_a
    iget v1, p0, Lcom/wssnps/b/x;->i:I

    if-ne v1, v8, :cond_d

    .line 196
    const-string v1, "UTC"

    iput-object v1, p0, Lcom/wssnps/b/x;->g:Ljava/lang/String;

    .line 197
    iget-wide v4, p0, Lcom/wssnps/b/x;->f:J

    add-long/2addr v4, v10

    iput-wide v4, p0, Lcom/wssnps/b/x;->f:J

    .line 207
    :cond_b
    :goto_1
    iget-object v1, p0, Lcom/wssnps/b/x;->o:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 209
    iget-wide v4, p0, Lcom/wssnps/b/x;->f:J

    iget-wide v6, p0, Lcom/wssnps/b/x;->e:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 210
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "P"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "S"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    .line 213
    :cond_c
    iget-wide v4, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 214
    iget-object v1, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/wssnps/b/x;->a(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 216
    iget-wide v4, p0, Lcom/wssnps/b/x;->e:J

    .line 217
    iput v8, p0, Lcom/wssnps/b/x;->k:I

    .line 218
    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/wssnps/b/x;->p:[J

    move v1, v2

    .line 219
    :goto_2
    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_10

    .line 221
    iget-object v0, p1, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/k;

    iget-object v0, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 224
    sub-long v6, v4, v6

    div-long/2addr v6, v10

    long-to-int v0, v6

    .line 225
    iget-object v3, p0, Lcom/wssnps/b/x;->p:[J

    int-to-long v6, v0

    aput-wide v6, v3, v1

    .line 219
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 201
    :cond_d
    iget-object v1, p1, Lcom/wssnps/a/r;->u:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 202
    iget-object v1, p1, Lcom/wssnps/a/r;->u:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->g:Ljava/lang/String;

    goto :goto_1

    .line 203
    :cond_e
    iget-object v1, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-eqz v1, :cond_b

    .line 204
    iget-object v1, p1, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget-object v1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iput-object v1, p0, Lcom/wssnps/b/x;->g:Ljava/lang/String;

    goto/16 :goto_1

    .line 230
    :cond_f
    iput v2, p0, Lcom/wssnps/b/x;->k:I

    .line 233
    :cond_10
    iget-object v0, p1, Lcom/wssnps/a/r;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    .line 235
    iget-object v0, p1, Lcom/wssnps/a/r;->r:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    .line 238
    :cond_11
    iget-object v0, p1, Lcom/wssnps/a/r;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    .line 240
    iget v0, p1, Lcom/wssnps/a/r;->x:I

    if-ne v0, v8, :cond_16

    .line 241
    iput v9, p0, Lcom/wssnps/b/x;->s:I

    .line 245
    :cond_12
    :goto_3
    iget-object v0, p1, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    if-eqz v0, :cond_17

    .line 247
    iget-object v0, p1, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/wssnps/b/x;->w:J

    .line 254
    :goto_4
    iget-object v0, p1, Lcom/wssnps/a/r;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 256
    iget-object v0, p1, Lcom/wssnps/a/r;->J:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    .line 259
    :cond_13
    iget-object v0, p1, Lcom/wssnps/a/r;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    .line 261
    iget-object v0, p1, Lcom/wssnps/a/r;->K:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    .line 264
    :cond_14
    iget-object v0, p1, Lcom/wssnps/a/r;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    .line 266
    iget-object v0, p1, Lcom/wssnps/a/r;->L:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    .line 269
    :cond_15
    iget-object v0, p1, Lcom/wssnps/a/r;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    iget-object v0, p1, Lcom/wssnps/a/r;->M:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 242
    :cond_16
    iget v0, p1, Lcom/wssnps/a/r;->x:I

    if-ne v0, v9, :cond_12

    .line 243
    iput v2, p0, Lcom/wssnps/b/x;->s:I

    goto :goto_3

    .line 251
    :cond_17
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/wssnps/b/x;->w:J

    goto :goto_4
.end method

.method public static a(JJ)I
    .locals 4

    .prologue
    .line 91
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 93
    const-wide/32 v0, 0x5265bff

    sub-long/2addr p0, v0

    .line 95
    :cond_0
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p2

    .line 96
    add-long/2addr v0, p0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    .line 97
    long-to-int v0, v0

    const v1, 0x253d8c    # 3.419992E-39f

    add-int/2addr v0, v1

    return v0
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1414
    if-eqz p1, :cond_0

    .line 1415
    const/4 v0, 0x0

    invoke-virtual {p0, p2, p1, p3, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1416
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 880
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 881
    invoke-virtual {p1, p0}, Lcom/wssnps/b/x;->d(Landroid/content/ContentResolver;)Landroid/content/ContentValues;

    move-result-object v0

    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v2}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 882
    if-nez v0, :cond_0

    .line 884
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error add calendar. luid: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1015
    :goto_0
    return v1

    .line 888
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 889
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 890
    iput v3, p1, Lcom/wssnps/b/x;->a:I

    .line 893
    iget v0, p1, Lcom/wssnps/b/x;->k:I

    if-lez v0, :cond_2

    move v0, v1

    .line 895
    :goto_1
    iget-object v2, p1, Lcom/wssnps/b/x;->p:[J

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 897
    invoke-virtual {p1, v0}, Lcom/wssnps/b/x;->a(I)Landroid/content/ContentValues;

    move-result-object v2

    .line 898
    if-eqz v2, :cond_1

    .line 900
    sget-object v4, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v2, v4}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    .line 895
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 905
    :cond_2
    iget-object v0, p1, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    move v2, v1

    .line 907
    :goto_2
    iget-object v0, p1, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 909
    iget-object v0, p1, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/p;

    invoke-virtual {p1, v0, v3}, Lcom/wssnps/b/x;->a(Lcom/wssnps/a/p;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 910
    if-eqz v0, :cond_3

    .line 912
    sget-object v4, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v4}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    .line 907
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 917
    :cond_4
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 921
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 922
    const-string v0, "content://com.android.calendar/images"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 923
    iget-object v0, p1, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    move v2, v1

    .line 925
    :goto_3
    iget-object v0, p1, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 927
    const-string v0, "event_id"

    iget v6, p1, Lcom/wssnps/b/x;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 928
    iget-object v0, p1, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/l;

    iget-object v0, v0, Lcom/wssnps/a/l;->b:Ljava/lang/String;

    .line 929
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 930
    const-string v6, "filepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    :cond_5
    const-string v0, "event_type"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 932
    invoke-virtual {p0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 925
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 937
    :cond_6
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 938
    const/4 v2, 0x3

    if-ne v0, v2, :cond_7

    .line 940
    sget-boolean v0, Lcom/wssnps/b/l;->aN:Z

    if-eqz v0, :cond_f

    .line 943
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 944
    const-string v0, "content://com.android.calendar/memos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 945
    iget-object v0, p1, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    iget-object v0, p1, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    .line 947
    :goto_4
    iget-object v0, p1, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 949
    const-string v0, "event_id"

    iget v5, p1, Lcom/wssnps/b/x;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 950
    const-string v5, "memo_id"

    iget-object v0, p1, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/n;

    iget v0, v0, Lcom/wssnps/a/n;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 951
    const-string v0, "event_type"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 952
    invoke-virtual {p0, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 947
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 957
    :cond_7
    const/4 v2, 0x4

    if-ne v0, v2, :cond_a

    .line 960
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 961
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 962
    iget-object v0, p1, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    iget-object v0, p1, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    .line 964
    :goto_5
    iget-object v0, p1, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 966
    const-string v0, "event_id"

    iget v5, p1, Lcom/wssnps/b/x;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 967
    iget-object v0, p1, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    iget-object v0, v0, Lcom/wssnps/a/m;->a:Ljava/lang/String;

    .line 968
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 969
    const-string v5, "filepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    :cond_8
    iget-object v0, p1, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    iget-object v0, v0, Lcom/wssnps/a/m;->b:Ljava/lang/String;

    .line 972
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 973
    const-string v5, "imagepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    :cond_9
    const-string v5, "page_no"

    iget-object v0, p1, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    iget-wide v6, v0, Lcom/wssnps/a/m;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 976
    const-string v5, "locked"

    iget-object v0, p1, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/m;

    iget v0, v0, Lcom/wssnps/a/m;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 977
    const-string v0, "event_type"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 978
    invoke-virtual {p0, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 964
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 982
    :cond_a
    const/4 v2, 0x5

    if-ne v0, v2, :cond_f

    .line 985
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 986
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 987
    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    .line 989
    :goto_6
    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    .line 991
    const-string v0, "event_id"

    iget v5, p1, Lcom/wssnps/b/x;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 992
    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget-object v0, v0, Lcom/wssnps/a/o;->a:Ljava/lang/String;

    .line 993
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 994
    const-string v5, "filepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    :cond_b
    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget-object v0, v0, Lcom/wssnps/a/o;->b:Ljava/lang/String;

    .line 997
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 998
    const-string v5, "imagepath"

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    :cond_c
    const-string v5, "page_no"

    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget-wide v6, v0, Lcom/wssnps/a/o;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1001
    const-string v5, "locked"

    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1003
    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->e:I

    if-eqz v0, :cond_d

    .line 1004
    const-string v5, "date"

    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1005
    :cond_d
    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->f:I

    if-eqz v0, :cond_e

    .line 1006
    const-string v5, "memo_id"

    iget-object v0, p1, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/o;

    iget v0, v0, Lcom/wssnps/a/o;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1008
    :cond_e
    const-string v0, "event_type"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1009
    invoke-virtual {p0, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 989
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_6

    :cond_f
    move v1, v3

    .line 1015
    goto/16 :goto_0
.end method

.method private static a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1400
    if-eqz p1, :cond_0

    .line 1401
    invoke-virtual {p0, p2, p1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 1402
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/wssnps/b/x;
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v11, 0x1

    const/4 v2, 0x0

    const/4 v10, -0x1

    .line 291
    new-instance v7, Lcom/wssnps/b/x;

    invoke-direct {v7}, Lcom/wssnps/b/x;-><init>()V

    .line 292
    const-string v0, "_id"

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 294
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 295
    if-eqz v0, :cond_12

    .line 297
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 299
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 300
    if-eq v1, v10, :cond_0

    .line 301
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/wssnps/b/x;->a:I

    .line 303
    :cond_0
    const-string v1, "title"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 304
    if-eq v1, v10, :cond_1

    .line 305
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    .line 307
    :cond_1
    const-string v1, "eventLocation"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 308
    if-eq v1, v10, :cond_2

    .line 309
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->d:Ljava/lang/String;

    .line 311
    :cond_2
    const-string v1, "description"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 312
    if-eq v1, v10, :cond_3

    .line 313
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->c:Ljava/lang/String;

    .line 315
    :cond_3
    const-string v1, "dtstart"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 316
    if-eq v1, v10, :cond_4

    .line 317
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v7, Lcom/wssnps/b/x;->e:J

    .line 319
    :cond_4
    const-string v1, "dtend"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 320
    if-eq v1, v10, :cond_5

    .line 321
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v7, Lcom/wssnps/b/x;->f:J

    .line 323
    :cond_5
    const-string v1, "duration"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 324
    if-eq v1, v10, :cond_6

    .line 325
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    .line 327
    :cond_6
    const-string v1, "allDay"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 328
    if-eq v1, v10, :cond_7

    .line 329
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/wssnps/b/x;->i:I

    .line 331
    :cond_7
    const-string v1, "hasAlarm"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 332
    if-eq v1, v10, :cond_8

    .line 333
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/wssnps/b/x;->k:I

    .line 335
    :cond_8
    const-string v1, "rrule"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 336
    if-eq v1, v10, :cond_9

    .line 337
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->o:Ljava/lang/String;

    .line 339
    :cond_9
    const-string v1, "eventTimezone"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 340
    if-eq v1, v10, :cond_a

    .line 341
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->g:Ljava/lang/String;

    .line 343
    :cond_a
    const-string v1, "calendar_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 344
    if-eq v1, v10, :cond_b

    .line 345
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->l:Ljava/lang/String;

    .line 347
    :cond_b
    invoke-static {}, Lcom/wssnps/smlModelDefine;->b()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 349
    const-string v1, "setLunar"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 350
    if-eq v1, v10, :cond_c

    .line 351
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/wssnps/b/x;->j:I

    .line 354
    :cond_c
    const-string v1, "_sync_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 355
    if-eq v1, v10, :cond_d

    .line 356
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->r:Ljava/lang/String;

    .line 358
    :cond_d
    const-string v1, "eventStatus"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 359
    if-eq v1, v10, :cond_e

    .line 360
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/wssnps/b/x;->s:I

    .line 362
    :cond_e
    const-string v1, "original_sync_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 363
    if-eq v1, v10, :cond_f

    .line 364
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/wssnps/b/x;->t:Ljava/lang/String;

    .line 366
    :cond_f
    const-string v1, "originalInstanceTime"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 367
    if-eq v1, v10, :cond_10

    .line 368
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v7, Lcom/wssnps/b/x;->u:J

    .line 370
    :cond_10
    const-string v1, "sticker_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 371
    if-eq v1, v10, :cond_11

    .line 372
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/wssnps/b/x;->y:I

    .line 374
    :cond_11
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 377
    :cond_12
    iget v0, v7, Lcom/wssnps/b/x;->k:I

    if-ne v0, v11, :cond_15

    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 381
    const-string v5, "minutes desc"

    .line 382
    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 383
    if-eqz v1, :cond_15

    .line 385
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, v7, Lcom/wssnps/b/x;->p:[J

    .line 386
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_14

    move v0, v6

    .line 390
    :cond_13
    const-string v3, "minutes"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 391
    iget-object v4, v7, Lcom/wssnps/b/x;->p:[J

    int-to-long v8, v3

    aput-wide v8, v4, v0

    .line 392
    add-int/lit8 v0, v0, 0x1

    .line 393
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_13

    .line 395
    :cond_14
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 399
    :cond_15
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 403
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v7, Lcom/wssnps/b/x;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 404
    const-string v0, "content://com.android.calendar/images"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 405
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 406
    if-eqz v0, :cond_19

    .line 408
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 412
    :cond_16
    const-string v1, "event_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v11, :cond_17

    .line 414
    new-instance v1, Lcom/wssnps/a/l;

    invoke-direct {v1}, Lcom/wssnps/a/l;-><init>()V

    .line 415
    const-string v3, "filepath"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 416
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_17

    .line 418
    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/wssnps/a/l;->b:Ljava/lang/String;

    .line 419
    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 420
    iget-object v3, v7, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    :cond_17
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_16

    .line 427
    :cond_18
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 431
    :cond_19
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 432
    const/4 v1, 0x3

    if-ne v0, v1, :cond_20

    .line 434
    sget-boolean v0, Lcom/wssnps/b/l;->aN:Z

    if-eqz v0, :cond_1d

    .line 437
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v7, Lcom/wssnps/b/x;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 438
    const-string v0, "content://com.android.calendar/memos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 439
    const-string v0, "content://com.sec.android.widgetapp.q1_penmemo/PenMemo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 441
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 442
    if-eqz v8, :cond_1d

    .line 444
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 448
    :cond_1a
    const-string v0, "event_type"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v11, :cond_1b

    .line 450
    new-instance v9, Lcom/wssnps/a/n;

    invoke-direct {v9}, Lcom/wssnps/a/n;-><init>()V

    .line 451
    const-string v0, "memo_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v9, Lcom/wssnps/a/n;->a:I

    .line 452
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v9, Lcom/wssnps/a/n;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, v6

    move-object v4, v2

    move-object v5, v2

    .line 453
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_1b

    .line 456
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1f

    .line 457
    iget-object v1, v7, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 464
    :cond_1b
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 466
    :cond_1c
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 553
    :cond_1d
    :goto_2
    return-object v7

    .line 422
    :cond_1e
    const-string v1, "not found file"

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 459
    :cond_1f
    const-string v1, "not found file"

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 470
    :cond_20
    const/4 v1, 0x4

    if-ne v0, v1, :cond_27

    .line 473
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v7, Lcom/wssnps/b/x;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 474
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 475
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 476
    if-eqz v0, :cond_1d

    .line 478
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 482
    :cond_21
    const-string v1, "event_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v11, :cond_24

    .line 484
    new-instance v1, Lcom/wssnps/a/m;

    invoke-direct {v1}, Lcom/wssnps/a/m;-><init>()V

    .line 485
    const-string v2, "filepath"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 486
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_22

    .line 487
    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/wssnps/a/m;->a:Ljava/lang/String;

    .line 489
    :cond_22
    const-string v3, "imagepath"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 490
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_23

    .line 491
    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/wssnps/a/m;->b:Ljava/lang/String;

    .line 493
    :cond_23
    const-string v4, "page_no"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/wssnps/a/m;->c:J

    .line 494
    const-string v4, "locked"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v1, Lcom/wssnps/a/m;->d:I

    .line 495
    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 496
    iget-object v2, v7, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 500
    :cond_24
    :goto_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_21

    .line 502
    :cond_25
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 498
    :cond_26
    const-string v1, "not found file"

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_3

    .line 505
    :cond_27
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1d

    .line 508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "event_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v7, Lcom/wssnps/b/x;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 509
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    .line 510
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 511
    if-eqz v0, :cond_1d

    .line 513
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 517
    :cond_28
    const-string v1, "event_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v11, :cond_2b

    .line 519
    new-instance v1, Lcom/wssnps/a/o;

    invoke-direct {v1}, Lcom/wssnps/a/o;-><init>()V

    .line 520
    const-string v2, "filepath"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 521
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_29

    .line 522
    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/wssnps/a/o;->a:Ljava/lang/String;

    .line 524
    :cond_29
    const-string v3, "imagepath"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 525
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2a

    .line 526
    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Lcom/wssnps/a/o;->b:Ljava/lang/String;

    .line 528
    :cond_2a
    const-string v4, "page_no"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/wssnps/a/o;->c:J

    .line 529
    const-string v4, "locked"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v1, Lcom/wssnps/a/o;->d:I

    .line 531
    const-string v4, "date"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v10, :cond_2d

    .line 532
    const-string v4, "date"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v1, Lcom/wssnps/a/o;->e:I

    .line 536
    :goto_4
    const-string v4, "memo_id "

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v10, :cond_2e

    .line 537
    const-string v4, "memo_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v1, Lcom/wssnps/a/o;->f:I

    .line 541
    :goto_5
    invoke-static {v2}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2f

    invoke-static {v3}, Lcom/wssnps/smlModelDefine;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 542
    iget-object v2, v7, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 546
    :cond_2b
    :goto_6
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_28

    .line 548
    :cond_2c
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 534
    :cond_2d
    iput v6, v1, Lcom/wssnps/a/o;->e:I

    goto :goto_4

    .line 539
    :cond_2e
    iput v6, v1, Lcom/wssnps/a/o;->f:I

    goto :goto_5

    .line 544
    :cond_2f
    const-string v1, "not found file"

    invoke-static {v11, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_6
.end method

.method public static a(Ljava/lang/String;)Lcom/wssnps/b/x;
    .locals 2

    .prologue
    .line 284
    invoke-static {p0}, Lcom/wssnps/a/j;->b(Ljava/lang/String;)Lcom/wssnps/a/r;

    move-result-object v0

    .line 285
    new-instance v1, Lcom/wssnps/b/x;

    invoke-direct {v1, v0}, Lcom/wssnps/b/x;-><init>(Lcom/wssnps/a/r;)V

    .line 286
    return-object v1
.end method

.method public static a(Landroid/content/ContentResolver;I)Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1192
    const-string v6, ""

    .line 1194
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1196
    const-string v0, ""

    .line 1197
    sget-object v9, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    .line 1199
    const-string v3, "account_name=\"My calendar\""

    .line 1200
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1201
    if-eqz v10, :cond_5

    .line 1203
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v3, v6

    move v6, v7

    .line 1207
    :goto_0
    const-string v0, "_id"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1208
    packed-switch p1, :pswitch_data_0

    :goto_1
    move-object v0, p0

    move-object v1, v9

    move-object v4, v2

    move-object v5, v2

    .line 1226
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1227
    if-eqz v1, :cond_3

    .line 1229
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 1230
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v6

    .line 1234
    :cond_0
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1235
    add-int/lit8 v0, v0, 0x1

    .line 1236
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1238
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1240
    :goto_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1242
    :goto_4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1245
    :goto_5
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/wssnps/b/x;->H:Ljava/lang/String;

    .line 1246
    const-string v1, "\n"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1249
    return-object v0

    .line 1211
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calendar_id=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND contact_account_type is null AND deleted = 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1215
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dirty=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "deleted"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "calendar_id"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND contact_account_type is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1220
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dirty=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "deleted"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "calendar_id"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND contact_account_type is null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_1
    move v6, v0

    goto/16 :goto_0

    :cond_2
    move v0, v6

    goto/16 :goto_2

    :cond_3
    move v0, v6

    goto/16 :goto_3

    :cond_4
    move v0, v7

    goto/16 :goto_4

    :cond_5
    move v0, v7

    goto/16 :goto_5

    .line 1208
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1317
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    const-string v2, "My calendar"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "LOCAL"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1322
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, p2, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1323
    const/4 v0, 0x1

    return v0
.end method

.method public static a(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1046
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1048
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1050
    if-eqz p1, :cond_0

    .line 1051
    const-string v3, "_sync_id"

    invoke-virtual {v1, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    :cond_0
    sget-object v3, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "account_name"

    const-string v5, "My calendar"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "account_type"

    const-string v5, "LOCAL"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1058
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v1, v2, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1059
    if-nez v1, :cond_1

    .line 1061
    const-string v1, "Error add calendar group sync Id."

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1062
    const/4 v0, 0x0

    .line 1064
    :cond_1
    return v0
.end method

.method public static a(Landroid/text/format/Time;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 102
    invoke-virtual {p0, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 103
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v4, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/wssnps/b/x;->a(JJ)I

    move-result v1

    const v4, 0x24dc86

    if-lt v1, v4, :cond_0

    iget-wide v4, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/wssnps/b/x;->a(JJ)I

    move-result v1

    const v2, 0x259e91

    if-le v1, v2, :cond_1

    .line 104
    :cond_0
    const/4 v0, 0x0

    .line 105
    :cond_1
    return v0
.end method

.method private static b(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1407
    if-eqz p1, :cond_0

    .line 1408
    invoke-virtual {p0, p2, p1, v0, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1409
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1021
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1024
    iget-object v1, p1, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1026
    const-string v1, "account_name"

    iget-object v2, p1, Lcom/wssnps/b/x;->D:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    const-string v1, "account_type"

    iget-object v2, p1, Lcom/wssnps/b/x;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    const-string v1, "name"

    iget-object v2, p1, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1029
    const-string v1, "calendar_color"

    iget v2, p1, Lcom/wssnps/b/x;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1032
    :cond_0
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v1}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1033
    if-nez v0, :cond_1

    .line 1035
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error add calendar group. luid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1036
    const/4 v0, 0x0

    .line 1041
    :goto_0
    return v0

    .line 1039
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1040
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static b(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/wssnps/b/x;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 558
    new-instance v6, Lcom/wssnps/b/x;

    invoke-direct {v6}, Lcom/wssnps/b/x;-><init>()V

    .line 559
    const-string v0, "_id"

    .line 560
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 561
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 562
    if-eqz v0, :cond_1

    .line 564
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 566
    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/x;->l:Ljava/lang/String;

    .line 567
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/x;->D:Ljava/lang/String;

    .line 568
    const-string v1, "account_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/x;->E:Ljava/lang/String;

    .line 569
    const-string v1, "name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    .line 570
    const-string v1, "calendar_color"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v6, Lcom/wssnps/b/x;->G:I

    .line 572
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 575
    :cond_1
    return-object v6
.end method

.method public static b(Landroid/content/ContentResolver;I)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1341
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1343
    const-string v1, ""

    invoke-virtual {p0, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_2

    .line 1345
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1349
    const-string v0, "content://com.android.calendar/images"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1350
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1351
    invoke-virtual {p0, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1354
    sget-boolean v0, Lcom/wssnps/b/l;->aN:Z

    if-eqz v0, :cond_0

    .line 1357
    const-string v0, "content://com.android.calendar/memos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1358
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1359
    invoke-virtual {p0, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1363
    :cond_0
    const-string v0, "content://com.android.calendar/notes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1364
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1365
    invoke-virtual {p0, v0, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1368
    :cond_1
    const/4 v0, 0x1

    .line 1372
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 1070
    .line 1071
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1073
    if-ne p2, v7, :cond_1

    .line 1109
    :cond_0
    :goto_0
    return p2

    .line 1077
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1078
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1079
    if-eqz v1, :cond_0

    .line 1081
    iget-object v0, p1, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1083
    const-string v0, "account_name"

    iget-object v2, p1, Lcom/wssnps/b/x;->D:Ljava/lang/String;

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    const-string v0, "account_type"

    iget-object v2, p1, Lcom/wssnps/b/x;->E:Ljava/lang/String;

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1085
    const-string v0, "name"

    iget-object v2, p1, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    const-string v0, "calendar_color"

    iget v2, p1, Lcom/wssnps/b/x;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1089
    :cond_2
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v6, v0}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 1090
    if-nez v0, :cond_3

    .line 1092
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error add contact. luid: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1093
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1094
    const/4 p2, 0x0

    goto :goto_0

    .line 1097
    :cond_3
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1100
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 1107
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1102
    :catch_0
    move-exception v0

    .line 1104
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static c(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1257
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1258
    const/4 v6, 0x0

    .line 1260
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    .line 1263
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1264
    if-eqz v1, :cond_2

    .line 1266
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 1267
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v6

    .line 1271
    :cond_0
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1272
    add-int/lit8 v0, v0, 0x1

    .line 1273
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1275
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1278
    :goto_1
    const-string v1, "\n"

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1280
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1281
    return-object v0

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1
.end method

.method public static c(Landroid/content/ContentResolver;I)Z
    .locals 4

    .prologue
    .line 1390
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v2, p1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1392
    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    .line 1393
    const/4 v0, 0x1

    .line 1395
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1114
    .line 1115
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1116
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v2, p2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1117
    invoke-virtual {p1, p0}, Lcom/wssnps/b/x;->d(Landroid/content/ContentResolver;)Landroid/content/ContentValues;

    move-result-object v2

    .line 1118
    if-eqz v2, :cond_0

    .line 1120
    invoke-static {p0, v2, v0}, Lcom/wssnps/b/x;->b(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)I

    .line 1123
    :cond_0
    iput p2, p1, Lcom/wssnps/b/x;->a:I

    .line 1126
    iget v0, p1, Lcom/wssnps/b/x;->k:I

    if-lez v0, :cond_2

    .line 1128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1129
    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move v0, v1

    .line 1131
    :goto_0
    iget-object v2, p1, Lcom/wssnps/b/x;->p:[J

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1133
    invoke-virtual {p1, v0}, Lcom/wssnps/b/x;->a(I)Landroid/content/ContentValues;

    move-result-object v2

    .line 1134
    if-eqz v2, :cond_1

    .line 1136
    sget-object v3, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v2, v3}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    .line 1131
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1142
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1143
    sget-object v2, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1146
    :cond_3
    iget-object v0, p1, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "event_id=\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1149
    sget-object v2, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v2, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1151
    :goto_1
    iget-object v0, p1, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1153
    iget-object v0, p1, Lcom/wssnps/b/x;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/wssnps/a/p;

    invoke-virtual {p1, v0, p2}, Lcom/wssnps/b/x;->a(Lcom/wssnps/a/p;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 1154
    if-eqz v0, :cond_4

    .line 1156
    sget-object v2, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v0, v2}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;)Landroid/net/Uri;

    .line 1151
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1160
    :cond_5
    return p2
.end method

.method public static d(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1301
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    .line 1302
    const/4 v6, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    .line 1304
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1305
    if-eqz v1, :cond_0

    .line 1307
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 1308
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1310
    :goto_0
    return v0

    :cond_0
    move v0, v6

    goto :goto_0
.end method

.method public static e(Landroid/content/ContentResolver;Lcom/wssnps/b/x;I)I
    .locals 5

    .prologue
    .line 1166
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1168
    sget-object v2, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "caller_is_syncadapter"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_name"

    const-string v4, "My calendar"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "account_type"

    const-string v4, "LOCAL"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 1176
    iget-object v3, p1, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 1178
    const-string v3, "account_name"

    iget-object v4, p1, Lcom/wssnps/b/x;->D:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179
    const-string v3, "account_type"

    iget-object v4, p1, Lcom/wssnps/b/x;->E:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    const-string v3, "calendar_color"

    iget v4, p1, Lcom/wssnps/b/x;->G:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1181
    const-string v3, "name"

    iget-object v4, p1, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    const-string v3, "calendar_displayName"

    iget-object v4, p1, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    :cond_0
    invoke-static {p0, v0, v2, v1}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Landroid/content/ContentValues;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    .line 1186
    return v0
.end method

.method public static e(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 1330
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    const-string v2, "My calendar"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "LOCAL"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1335
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1336
    const/4 v0, 0x1

    return v0
.end method

.method public static f(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1378
    sget-object v0, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    const-string v2, "My calendar"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "LOCAL"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1383
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleteCalendarGroup uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1384
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1385
    return v3
.end method


# virtual methods
.method public a(I)Landroid/content/ContentValues;
    .locals 6

    .prologue
    .line 1671
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1673
    iget-object v1, p0, Lcom/wssnps/b/x;->p:[J

    aget-wide v2, v1, p1

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1674
    const/4 v0, 0x0

    .line 1680
    :goto_0
    return-object v0

    .line 1676
    :cond_0
    const-string v1, "event_id"

    iget v2, p0, Lcom/wssnps/b/x;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1677
    const-string v1, "minutes"

    iget-object v2, p0, Lcom/wssnps/b/x;->p:[J

    aget-wide v2, v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1678
    const-string v1, "method"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public a(Lcom/wssnps/a/p;I)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 1685
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1686
    const-string v1, "event_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1687
    iget-object v1, p1, Lcom/wssnps/a/p;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1690
    const-string v1, "attendeeEmail"

    iget-object v2, p1, Lcom/wssnps/a/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1692
    :cond_0
    iget-object v1, p1, Lcom/wssnps/a/p;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1695
    const-string v1, "attendeeName"

    iget-object v2, p1, Lcom/wssnps/a/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698
    :cond_1
    return-object v0
.end method

.method public a(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/wssnps/b/x;->b(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    invoke-virtual {p0, p1}, Lcom/wssnps/b/x;->c(Landroid/content/ContentResolver;)Lcom/wssnps/a/r;

    move-result-object v0

    .line 278
    invoke-static {v0}, Lcom/wssnps/a/j;->a(Lcom/wssnps/a/r;)Ljava/lang/String;

    move-result-object v0

    .line 279
    return-object v0
.end method

.method public c(Landroid/content/ContentResolver;)Lcom/wssnps/a/r;
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v12, 0x2

    const v11, 0x24dc86

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 580
    new-instance v8, Lcom/wssnps/a/r;

    invoke-direct {v8}, Lcom/wssnps/a/r;-><init>()V

    .line 581
    iget v0, p0, Lcom/wssnps/b/x;->a:I

    iput v0, v8, Lcom/wssnps/a/r;->a:I

    .line 582
    sget-object v0, Lcom/wssnps/a/q;->a:Lcom/wssnps/a/q;

    iput-object v0, v8, Lcom/wssnps/a/r;->b:Lcom/wssnps/a/q;

    .line 585
    iget-object v0, p0, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/wssnps/b/x;->D:Ljava/lang/String;

    iput-object v0, v8, Lcom/wssnps/a/r;->B:Ljava/lang/String;

    .line 589
    iget-object v0, p0, Lcom/wssnps/b/x;->E:Ljava/lang/String;

    iput-object v0, v8, Lcom/wssnps/a/r;->C:Ljava/lang/String;

    .line 590
    iget-object v0, p0, Lcom/wssnps/b/x;->F:Ljava/lang/String;

    iput-object v0, v8, Lcom/wssnps/a/r;->D:Ljava/lang/String;

    .line 593
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "#%06X"

    new-array v2, v7, [Ljava/lang/Object;

    const v3, 0xffffff

    iget v4, p0, Lcom/wssnps/b/x;->G:I

    and-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/wssnps/a/r;->E:Ljava/lang/String;

    .line 594
    iget-object v0, v8, Lcom/wssnps/a/r;->E:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/wssnps/a/r;->E:Ljava/lang/String;

    .line 875
    :cond_0
    return-object v8

    .line 598
    :cond_1
    iget-object v0, p0, Lcom/wssnps/b/x;->t:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 600
    iget v0, p0, Lcom/wssnps/b/x;->s:I

    if-eq v0, v12, :cond_2

    iget-object v0, p0, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 601
    :cond_2
    iput-object v2, v8, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    .line 610
    :goto_0
    iget v0, p0, Lcom/wssnps/b/x;->s:I

    if-ne v0, v12, :cond_d

    .line 612
    invoke-virtual {p0, p1}, Lcom/wssnps/b/x;->e(Landroid/content/ContentResolver;)I

    move-result v0

    .line 619
    :goto_1
    iget-object v1, p0, Lcom/wssnps/b/x;->c:Ljava/lang/String;

    iput-object v1, v8, Lcom/wssnps/a/r;->d:Ljava/lang/String;

    .line 620
    iget-object v1, p0, Lcom/wssnps/b/x;->d:Ljava/lang/String;

    iput-object v1, v8, Lcom/wssnps/a/r;->f:Ljava/lang/String;

    .line 621
    iget-object v1, p0, Lcom/wssnps/b/x;->o:Ljava/lang/String;

    iput-object v1, v8, Lcom/wssnps/a/r;->g:Ljava/lang/String;

    .line 622
    iput v0, v8, Lcom/wssnps/a/r;->t:I

    .line 623
    iget v0, p0, Lcom/wssnps/b/x;->j:I

    iput v0, v8, Lcom/wssnps/a/r;->v:I

    .line 624
    iget-object v0, p0, Lcom/wssnps/b/x;->g:Ljava/lang/String;

    iput-object v0, v8, Lcom/wssnps/a/r;->u:Ljava/lang/String;

    .line 625
    iget-object v0, p0, Lcom/wssnps/b/x;->l:Ljava/lang/String;

    iput-object v0, v8, Lcom/wssnps/a/r;->A:Ljava/lang/String;

    .line 626
    iget v0, p0, Lcom/wssnps/b/x;->y:I

    iput v0, v8, Lcom/wssnps/a/r;->z:I

    .line 628
    const-string v0, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 630
    iget-object v0, p0, Lcom/wssnps/b/x;->z:Ljava/util/ArrayList;

    iput-object v0, v8, Lcom/wssnps/a/r;->J:Ljava/util/List;

    .line 632
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v0

    .line 633
    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    .line 635
    sget-boolean v0, Lcom/wssnps/b/l;->aN:Z

    if-eqz v0, :cond_3

    .line 636
    iget-object v0, p0, Lcom/wssnps/b/x;->A:Ljava/util/ArrayList;

    iput-object v0, v8, Lcom/wssnps/a/r;->K:Ljava/util/List;

    .line 648
    :cond_3
    :goto_2
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    .line 649
    iget-object v0, v9, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v10

    .line 651
    iget-object v0, p0, Lcom/wssnps/b/x;->t:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 654
    const-string v0, "_sync_id"

    .line 655
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/wssnps/b/x;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 656
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 657
    if-eqz v1, :cond_26

    .line 659
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 661
    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 663
    :goto_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 666
    :goto_4
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%048d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/wssnps/a/r;->w:Ljava/lang/String;

    .line 668
    iget v0, p0, Lcom/wssnps/b/x;->s:I

    if-ne v0, v12, :cond_10

    .line 669
    iput v7, v8, Lcom/wssnps/a/r;->x:I

    .line 673
    :goto_5
    iget-wide v0, p0, Lcom/wssnps/b/x;->e:J

    iget-wide v2, v9, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/wssnps/b/x;->a(JJ)I

    move-result v0

    if-lt v0, v11, :cond_5

    .line 675
    iget-object v0, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-nez v0, :cond_4

    .line 676
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    .line 678
    :cond_4
    iget v0, v8, Lcom/wssnps/a/r;->t:I

    if-ne v0, v7, :cond_11

    .line 680
    iget-object v0, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    const-string v1, "GMT"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 681
    iget-object v0, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 695
    :cond_5
    :goto_6
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    iget-wide v2, v9, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/wssnps/b/x;->a(JJ)I

    move-result v0

    if-lt v0, v11, :cond_7

    .line 697
    iget-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    if-nez v0, :cond_6

    .line 698
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    .line 700
    :cond_6
    iget v0, v8, Lcom/wssnps/a/r;->t:I

    if-ne v0, v7, :cond_12

    .line 702
    iget-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    const-string v1, "GMT"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 703
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    const-wide/32 v2, 0xea60

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/wssnps/b/x;->f:J

    .line 704
    iget-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->f:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 717
    :cond_7
    :goto_7
    iget v0, v8, Lcom/wssnps/a/r;->x:I

    if-eq v0, v7, :cond_8

    iget v0, v8, Lcom/wssnps/a/r;->x:I

    if-ne v0, v12, :cond_14

    .line 719
    :cond_8
    iget-wide v0, p0, Lcom/wssnps/b/x;->u:J

    iget-wide v2, v9, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/wssnps/b/x;->a(JJ)I

    move-result v0

    if-lt v0, v11, :cond_a

    .line 721
    iget-object v0, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    if-nez v0, :cond_9

    .line 722
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    .line 724
    :cond_9
    iget v0, v8, Lcom/wssnps/a/r;->t:I

    if-ne v0, v7, :cond_13

    .line 726
    iget-object v0, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    const-string v1, "GMT"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 727
    iget-object v0, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->u:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 850
    :cond_a
    :goto_8
    iget v0, p0, Lcom/wssnps/b/x;->k:I

    if-lez v0, :cond_0

    .line 854
    :goto_9
    iget-object v0, p0, Lcom/wssnps/b/x;->p:[J

    array-length v0, v0

    if-ge v6, v0, :cond_0

    .line 856
    new-instance v0, Lcom/wssnps/a/k;

    invoke-direct {v0}, Lcom/wssnps/a/k;-><init>()V

    .line 857
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 858
    iget-wide v2, p0, Lcom/wssnps/b/x;->e:J

    iget-object v4, p0, Lcom/wssnps/b/x;->p:[J

    aget-wide v4, v4, v6

    const-wide/16 v12, 0x3c

    mul-long/2addr v4, v12

    const-wide/16 v12, 0x3e8

    mul-long/2addr v4, v12

    sub-long/2addr v2, v4

    .line 859
    iget v4, v8, Lcom/wssnps/a/r;->t:I

    if-ne v4, v7, :cond_23

    .line 861
    const-string v4, "GMT"

    invoke-virtual {v1, v4}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 862
    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 870
    :goto_a
    iput-object v1, v0, Lcom/wssnps/a/k;->a:Landroid/text/format/Time;

    .line 871
    iget-object v1, v8, Lcom/wssnps/a/r;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 854
    add-int/lit8 v6, v6, 0x1

    goto :goto_9

    .line 603
    :cond_b
    iget-object v0, p0, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    iput-object v0, v8, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 607
    :cond_c
    iget-object v0, p0, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    iput-object v0, v8, Lcom/wssnps/a/r;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 616
    :cond_d
    iget v0, p0, Lcom/wssnps/b/x;->i:I

    goto/16 :goto_1

    .line 638
    :cond_e
    const/4 v1, 0x4

    if-ne v0, v1, :cond_f

    .line 640
    iget-object v0, p0, Lcom/wssnps/b/x;->B:Ljava/util/ArrayList;

    iput-object v0, v8, Lcom/wssnps/a/r;->L:Ljava/util/List;

    goto/16 :goto_2

    .line 642
    :cond_f
    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 644
    iget-object v0, p0, Lcom/wssnps/b/x;->C:Ljava/util/ArrayList;

    iput-object v0, v8, Lcom/wssnps/a/r;->M:Ljava/util/List;

    goto/16 :goto_2

    .line 671
    :cond_10
    iput v12, v8, Lcom/wssnps/a/r;->x:I

    goto/16 :goto_5

    .line 685
    :cond_11
    iget-wide v0, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v10, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 686
    iget-object v1, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->e:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_6

    .line 708
    :cond_12
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    invoke-virtual {v10, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 709
    iget-object v1, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->f:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_7

    .line 731
    :cond_13
    iget-wide v0, p0, Lcom/wssnps/b/x;->u:J

    invoke-virtual {v10, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 732
    iget-object v1, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->u:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_8

    .line 742
    :cond_14
    iget-wide v0, p0, Lcom/wssnps/b/x;->e:J

    iget-wide v2, v9, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/wssnps/b/x;->a(JJ)I

    move-result v0

    if-lt v0, v11, :cond_a

    .line 744
    iget-object v0, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    if-nez v0, :cond_15

    .line 745
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    .line 747
    :cond_15
    iget v0, v8, Lcom/wssnps/a/r;->t:I

    if-ne v0, v7, :cond_16

    .line 749
    iget-object v0, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    const-string v1, "GMT"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 750
    iget-object v0, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_8

    .line 754
    :cond_16
    iget-wide v0, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v10, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 755
    iget-object v1, v8, Lcom/wssnps/a/r;->y:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->e:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_8

    .line 766
    :cond_17
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%048d"

    new-array v2, v7, [Ljava/lang/Object;

    iget v3, p0, Lcom/wssnps/b/x;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/wssnps/a/r;->w:Ljava/lang/String;

    .line 768
    iget-wide v0, p0, Lcom/wssnps/b/x;->e:J

    iget-wide v2, v9, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/wssnps/b/x;->a(JJ)I

    move-result v0

    if-lt v0, v11, :cond_19

    .line 770
    iget-object v0, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    if-nez v0, :cond_18

    .line 771
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    .line 772
    :cond_18
    iget v0, v8, Lcom/wssnps/a/r;->t:I

    if-ne v0, v7, :cond_1c

    .line 774
    iget-object v0, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    const-string v1, "GMT"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 775
    iget-object v0, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 788
    :cond_19
    :goto_b
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    iget-wide v2, v9, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/wssnps/b/x;->a(JJ)I

    move-result v0

    if-lt v0, v11, :cond_1e

    iget-object v0, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    if-nez v0, :cond_1e

    .line 790
    :cond_1a
    iget-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    if-nez v0, :cond_1b

    .line 791
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    .line 793
    :cond_1b
    iget v0, v8, Lcom/wssnps/a/r;->t:I

    if-ne v0, v7, :cond_1d

    .line 795
    iget-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    const-string v1, "GMT"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 796
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    const-wide/32 v2, 0xea60

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/wssnps/b/x;->f:J

    .line 797
    iget-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->f:J

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_8

    .line 779
    :cond_1c
    iget-wide v0, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v10, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 780
    iget-object v1, v8, Lcom/wssnps/a/r;->o:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->e:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto :goto_b

    .line 801
    :cond_1d
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    invoke-virtual {v10, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 802
    iget-object v1, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->f:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_8

    .line 807
    :cond_1e
    iget-object v0, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 811
    const-string v0, ""

    .line 813
    iget-object v2, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    .line 815
    const/16 v0, 0x53

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_21

    move v1, v0

    move v0, v7

    .line 824
    :goto_c
    if-lez v1, :cond_a

    .line 826
    invoke-virtual {v2, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 827
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 829
    if-eqz v0, :cond_22

    .line 831
    iget-wide v0, p0, Lcom/wssnps/b/x;->e:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/wssnps/b/x;->f:J

    .line 832
    iget v0, v8, Lcom/wssnps/a/r;->t:I

    if-ne v0, v7, :cond_1f

    .line 833
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    const-wide/32 v2, 0xea60

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/wssnps/b/x;->f:J

    .line 841
    :cond_1f
    :goto_d
    iget-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    if-nez v0, :cond_20

    .line 842
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    .line 843
    :cond_20
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    invoke-virtual {v10, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 844
    iget-object v1, v8, Lcom/wssnps/a/r;->p:Landroid/text/format/Time;

    iget-wide v2, p0, Lcom/wssnps/b/x;->f:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_8

    .line 819
    :cond_21
    const/16 v0, 0x44

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_24

    move v1, v0

    move v0, v6

    .line 821
    goto :goto_c

    .line 837
    :cond_22
    iget-wide v0, p0, Lcom/wssnps/b/x;->e:J

    const-wide/16 v4, 0x18

    mul-long/2addr v2, v4

    const-wide/16 v4, 0xe10

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/wssnps/b/x;->f:J

    .line 838
    iget v0, v8, Lcom/wssnps/a/r;->t:I

    if-ne v0, v7, :cond_1f

    .line 839
    iget-wide v0, p0, Lcom/wssnps/b/x;->f:J

    const-wide/32 v2, 0xea60

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/wssnps/b/x;->f:J

    goto :goto_d

    .line 866
    :cond_23
    invoke-virtual {v10, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    .line 867
    int-to-long v4, v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    goto/16 :goto_a

    :cond_24
    move v1, v0

    move v0, v6

    goto :goto_c

    :cond_25
    move v0, v6

    goto/16 :goto_3

    :cond_26
    move v0, v6

    goto/16 :goto_4
.end method

.method public d(Landroid/content/ContentResolver;)Landroid/content/ContentValues;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1421
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    .line 1422
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1428
    new-instance v0, Lcom/wssnps/b/x;

    invoke-direct {v0}, Lcom/wssnps/b/x;-><init>()V

    .line 1432
    iget-object v1, p0, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-wide v10, p0, Lcom/wssnps/b/x;->w:J

    cmp-long v1, v10, v12

    if-nez v1, :cond_8

    move v1, v2

    .line 1454
    :goto_0
    iget-object v5, p0, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 1456
    const-string v5, "title"

    iget-object v9, p0, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1473
    :goto_1
    iget-object v5, p0, Lcom/wssnps/b/x;->l:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 1474
    const-string v5, "calendar_id"

    iget-object v9, p0, Lcom/wssnps/b/x;->l:Ljava/lang/String;

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    :goto_2
    iget v5, p0, Lcom/wssnps/b/x;->a:I

    if-eqz v5, :cond_0

    .line 1480
    const-string v5, "Event id is not used"

    invoke-static {v2, v5}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 1481
    const-string v5, "_id"

    iget v9, p0, Lcom/wssnps/b/x;->a:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1484
    :cond_0
    iget-object v5, p0, Lcom/wssnps/b/x;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1485
    const-string v5, "description"

    iget-object v9, p0, Lcom/wssnps/b/x;->c:Ljava/lang/String;

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    :goto_3
    iget-object v5, p0, Lcom/wssnps/b/x;->d:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 1490
    const-string v5, "eventLocation"

    iget-object v9, p0, Lcom/wssnps/b/x;->d:Ljava/lang/String;

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1495
    :goto_4
    if-ne v1, v3, :cond_18

    .line 1497
    iget v5, p0, Lcom/wssnps/b/x;->s:I

    if-ne v5, v3, :cond_14

    .line 1499
    iget-object v5, v8, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-static {v5}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v9

    .line 1500
    iget-wide v10, p0, Lcom/wssnps/b/x;->w:J

    invoke-virtual {v8, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 1501
    invoke-static {v8}, Lcom/wssnps/b/x;->a(Landroid/text/format/Time;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1503
    const-string v5, "dtstart"

    iget-wide v10, p0, Lcom/wssnps/b/x;->w:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v7, v5, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1504
    const-string v5, "originalInstanceTime"

    iget-wide v10, p0, Lcom/wssnps/b/x;->w:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v7, v5, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1507
    :cond_1
    iget-object v5, v0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_13

    .line 1511
    const-string v5, ""

    .line 1514
    iget-object v10, v0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    .line 1516
    const/16 v5, 0x53

    invoke-virtual {v10, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-lez v5, :cond_10

    move v4, v2

    .line 1521
    :cond_2
    :goto_5
    if-lez v5, :cond_4

    .line 1523
    invoke-virtual {v10, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1524
    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1526
    if-eqz v4, :cond_11

    .line 1528
    iget-wide v4, p0, Lcom/wssnps/b/x;->w:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v4, v10

    .line 1529
    iget v10, v0, Lcom/wssnps/b/x;->i:I

    if-ne v10, v2, :cond_3

    .line 1530
    const-wide/32 v10, 0xea60

    sub-long/2addr v4, v10

    .line 1539
    :cond_3
    :goto_6
    invoke-virtual {v8, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1540
    invoke-static {v8}, Lcom/wssnps/b/x;->a(Landroid/text/format/Time;)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 1542
    invoke-virtual {v9, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v8

    .line 1543
    const-string v9, "dtend"

    int-to-long v10, v8

    sub-long/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v7, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1544
    const-string v4, "duration"

    invoke-virtual {v7, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1610
    :cond_4
    :goto_7
    iget-object v4, p0, Lcom/wssnps/b/x;->g:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1611
    const-string v4, "eventTimezone"

    iget-object v5, p0, Lcom/wssnps/b/x;->g:Ljava/lang/String;

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1617
    :cond_5
    const-string v4, "allDay"

    iget v5, p0, Lcom/wssnps/b/x;->i:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1619
    const-string v4, "sticker_type"

    iget v5, p0, Lcom/wssnps/b/x;->y:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1622
    const-string v4, "hasAlarm"

    iget v5, p0, Lcom/wssnps/b/x;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1624
    iget-object v4, p0, Lcom/wssnps/b/x;->o:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1b

    .line 1626
    const-string v4, "rrule"

    iget-object v5, p0, Lcom/wssnps/b/x;->o:Ljava/lang/String;

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1634
    :goto_8
    invoke-static {}, Lcom/wssnps/smlModelDefine;->b()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1635
    const-string v4, "setLunar"

    iget v5, p0, Lcom/wssnps/b/x;->j:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1637
    :cond_6
    const-string v4, "hasAttendeeData"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1639
    const-string v2, "splanner"

    invoke-static {}, Lcom/wssnps/smlModelDefine;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1641
    if-ne v1, v3, :cond_1d

    .line 1643
    iget v1, p0, Lcom/wssnps/b/x;->s:I

    if-ne v1, v3, :cond_1c

    .line 1644
    const-string v1, "eventStatus"

    const-string v2, "2"

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    :goto_9
    const-string v1, "original_id"

    iget v0, v0, Lcom/wssnps/b/x;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_7
    :goto_a
    move-object v0, v7

    .line 1666
    :goto_b
    return-object v0

    .line 1437
    :cond_8
    iget-object v1, p0, Lcom/wssnps/b/x;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    iget-wide v10, p0, Lcom/wssnps/b/x;->w:J

    cmp-long v1, v10, v12

    if-eqz v1, :cond_9

    .line 1441
    iget v0, p0, Lcom/wssnps/b/x;->x:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/wssnps/b/x;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/wssnps/b/x;

    move-result-object v0

    move v1, v3

    goto/16 :goto_0

    .line 1447
    :cond_9
    iput v4, p0, Lcom/wssnps/b/x;->x:I

    move v1, v4

    goto/16 :goto_0

    .line 1460
    :cond_a
    if-ne v1, v3, :cond_c

    .line 1462
    iget-object v5, v0, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 1463
    const-string v5, "title"

    iget-object v9, v0, Lcom/wssnps/b/x;->b:Ljava/lang/String;

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1465
    :cond_b
    const-string v5, "title"

    const-string v9, ""

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1469
    :cond_c
    const-string v5, "title"

    const-string v9, ""

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1476
    :cond_d
    const-string v5, "calendar_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_2

    .line 1487
    :cond_e
    const-string v5, "description"

    const-string v9, ""

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1492
    :cond_f
    const-string v5, "eventLocation"

    const-string v9, ""

    invoke-virtual {v7, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1518
    :cond_10
    const/16 v5, 0x44

    invoke-virtual {v10, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    if-lez v5, :cond_2

    goto/16 :goto_5

    .line 1534
    :cond_11
    iget-wide v4, p0, Lcom/wssnps/b/x;->w:J

    const-wide/16 v12, 0x18

    mul-long/2addr v10, v12

    const-wide/16 v12, 0xe10

    mul-long/2addr v10, v12

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v4, v10

    .line 1535
    iget v10, v0, Lcom/wssnps/b/x;->i:I

    if-ne v10, v2, :cond_3

    .line 1536
    const-wide/32 v10, 0xea60

    sub-long/2addr v4, v10

    goto/16 :goto_6

    .line 1548
    :cond_12
    iget-object v4, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1550
    const-string v4, "duration"

    iget-object v5, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    const-string v4, "dtend"

    invoke-virtual {v7, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1558
    :cond_13
    iput v2, p0, Lcom/wssnps/b/x;->I:I

    move-object v0, v6

    .line 1559
    goto/16 :goto_b

    .line 1564
    :cond_14
    iget-wide v4, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v8, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1565
    invoke-static {v8}, Lcom/wssnps/b/x;->a(Landroid/text/format/Time;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1566
    const-string v4, "dtstart"

    iget-wide v10, p0, Lcom/wssnps/b/x;->e:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1568
    :cond_15
    iget-wide v4, p0, Lcom/wssnps/b/x;->w:J

    invoke-virtual {v8, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1569
    invoke-static {v8}, Lcom/wssnps/b/x;->a(Landroid/text/format/Time;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 1570
    const-string v4, "originalInstanceTime"

    iget-wide v10, p0, Lcom/wssnps/b/x;->w:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1572
    :cond_16
    iget-wide v4, p0, Lcom/wssnps/b/x;->f:J

    invoke-virtual {v8, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1573
    invoke-static {v8}, Lcom/wssnps/b/x;->a(Landroid/text/format/Time;)Z

    move-result v4

    if-eqz v4, :cond_17

    iget-object v4, p0, Lcom/wssnps/b/x;->o:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 1575
    const-string v4, "dtend"

    iget-wide v8, p0, Lcom/wssnps/b/x;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1576
    const-string v4, "duration"

    invoke-virtual {v7, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1580
    :cond_17
    iget-object v4, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1582
    const-string v4, "duration"

    iget-object v5, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1583
    const-string v4, "dtend"

    invoke-virtual {v7, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1590
    :cond_18
    iget-wide v4, p0, Lcom/wssnps/b/x;->e:J

    invoke-virtual {v8, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1591
    invoke-static {v8}, Lcom/wssnps/b/x;->a(Landroid/text/format/Time;)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 1592
    const-string v4, "dtstart"

    iget-wide v10, p0, Lcom/wssnps/b/x;->e:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1594
    :cond_19
    iget-wide v4, p0, Lcom/wssnps/b/x;->f:J

    invoke-virtual {v8, v4, v5}, Landroid/text/format/Time;->set(J)V

    .line 1595
    invoke-static {v8}, Lcom/wssnps/b/x;->a(Landroid/text/format/Time;)Z

    move-result v4

    if-eqz v4, :cond_1a

    iget-object v4, p0, Lcom/wssnps/b/x;->o:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1a

    .line 1597
    const-string v4, "dtend"

    iget-wide v8, p0, Lcom/wssnps/b/x;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1598
    const-string v4, "duration"

    invoke-virtual {v7, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1602
    :cond_1a
    iget-object v4, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1604
    const-string v4, "duration"

    iget-object v5, p0, Lcom/wssnps/b/x;->h:Ljava/lang/String;

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    const-string v4, "dtend"

    invoke-virtual {v7, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1631
    :cond_1b
    const-string v4, "rrule"

    invoke-virtual {v7, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1646
    :cond_1c
    const-string v1, "eventStatus"

    const-string v2, "0"

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 1652
    :cond_1d
    const-string v0, "eventStatus"

    const-string v1, "0"

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1657
    :cond_1e
    if-ne v1, v3, :cond_7

    .line 1659
    iget v1, p0, Lcom/wssnps/b/x;->s:I

    if-ne v1, v3, :cond_1f

    .line 1660
    const-string v1, "eventStatus"

    const-string v2, "2"

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1662
    :cond_1f
    const-string v1, "original_id"

    iget v0, v0, Lcom/wssnps/b/x;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_a
.end method

.method public e(Landroid/content/ContentResolver;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1716
    const/4 v6, 0x0

    .line 1717
    const-string v0, "_sync_id"

    .line 1718
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/wssnps/b/x;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1719
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object v0, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1720
    if-eqz v1, :cond_1

    .line 1722
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1724
    const-string v0, "allDay"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1726
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1729
    :goto_1
    return v0

    :cond_0
    move v0, v6

    goto :goto_0

    :cond_1
    move v0, v6

    goto :goto_1
.end method
