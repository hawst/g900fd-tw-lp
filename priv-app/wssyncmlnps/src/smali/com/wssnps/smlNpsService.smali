.class public Lcom/wssnps/smlNpsService;
.super Landroid/app/Service;
.source "smlNpsService.java"


# static fields
.field public static a:Landroid/content/Context;

.field static c:Ljava/lang/Thread;

.field public static d:Landroid/net/LocalServerSocket;

.field public static f:Lcom/wssnps/smlModelDefine;

.field public static g:Ljava/lang/String;

.field private static i:Ljava/lang/String;

.field private static j:Z


# instance fields
.field b:Ljava/lang/Thread;

.field e:Landroid/os/Binder;

.field public h:Landroid/content/BroadcastReceiver;

.field private final k:Landroid/os/UEventObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    sput-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 45
    sput-object v0, Lcom/wssnps/smlNpsService;->c:Ljava/lang/Thread;

    .line 46
    sput-object v0, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    .line 49
    new-instance v0, Lcom/wssnps/smlModelDefine;

    invoke-direct {v0}, Lcom/wssnps/smlModelDefine;-><init>()V

    sput-object v0, Lcom/wssnps/smlNpsService;->f:Lcom/wssnps/smlModelDefine;

    .line 58
    const-string v0, "2"

    sput-object v0, Lcom/wssnps/smlNpsService;->g:Ljava/lang/String;

    .line 60
    const-string v0, ""

    sput-object v0, Lcom/wssnps/smlNpsService;->i:Ljava/lang/String;

    .line 62
    const/4 v0, 0x1

    sput-boolean v0, Lcom/wssnps/smlNpsService;->j:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wssnps/smlNpsService;->b:Ljava/lang/Thread;

    .line 48
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/wssnps/smlNpsService;->e:Landroid/os/Binder;

    .line 151
    new-instance v0, Lcom/wssnps/i;

    invoke-direct {v0, p0}, Lcom/wssnps/i;-><init>(Lcom/wssnps/smlNpsService;)V

    iput-object v0, p0, Lcom/wssnps/smlNpsService;->k:Landroid/os/UEventObserver;

    .line 365
    new-instance v0, Lcom/wssnps/j;

    invoke-direct {v0, p0}, Lcom/wssnps/j;-><init>(Lcom/wssnps/smlNpsService;)V

    iput-object v0, p0, Lcom/wssnps/smlNpsService;->h:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 434
    const/4 v0, 0x0

    .line 438
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 439
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v2

    .line 440
    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    if-ne v2, v5, :cond_1

    .line 441
    :cond_0
    const/4 v1, 0x1

    const-string v2, "package is disabled"

    invoke-static {v1, v2}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 458
    :goto_0
    return-object v0

    .line 443
    :cond_1
    const/16 v2, 0x9

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    .line 445
    :catch_0
    move-exception v1

    .line 447
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 449
    :catch_1
    move-exception v1

    .line 451
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown package : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 453
    :catch_2
    move-exception v1

    .line 455
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 391
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 393
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 395
    const-wide/16 v0, 0x1

    .line 398
    :cond_0
    const/4 v2, -0x1

    if-ne p1, v2, :cond_1

    const-wide/16 v2, 0x78

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 400
    const-string v0, "3"

    sput-object v0, Lcom/wssnps/smlNpsService;->g:Ljava/lang/String;

    .line 417
    :goto_0
    return-void

    .line 404
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 406
    const-string v0, "1"

    sput-object v0, Lcom/wssnps/smlNpsService;->g:Ljava/lang/String;

    goto :goto_0

    .line 408
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 410
    const-string v0, "3"

    sput-object v0, Lcom/wssnps/smlNpsService;->g:Ljava/lang/String;

    goto :goto_0

    .line 414
    :cond_3
    const-string v0, "2"

    sput-object v0, Lcom/wssnps/smlNpsService;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/app/NotificationManager;Landroid/support/v4/app/af;III)V
    .locals 4

    .prologue
    .line 265
    int-to-double v0, p3

    int-to-double v2, p4

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    .line 266
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    double-to-int v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/app/af;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p4, p3, v1}, Landroid/support/v4/app/af;->a(IIZ)Landroid/support/v4/app/af;

    .line 267
    invoke-virtual {p1}, Landroid/support/v4/app/af;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 268
    return-void
.end method

.method public static a(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 259
    invoke-virtual {p1, p3}, Landroid/support/v4/app/af;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->a(I)Landroid/support/v4/app/af;

    move-result-object v0

    const-string v1, "0%"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v2, v2}, Landroid/support/v4/app/af;->a(IIZ)Landroid/support/v4/app/af;

    .line 260
    invoke-virtual {p1}, Landroid/support/v4/app/af;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 261
    return-void
.end method

.method public static a(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 352
    new-instance v0, Landroid/support/v4/app/ae;

    invoke-direct {v0}, Landroid/support/v4/app/ae;-><init>()V

    invoke-virtual {v0, p4}, Landroid/support/v4/app/ae;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/ae;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/ae;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/ae;

    move-result-object v0

    .line 353
    invoke-virtual {p1, p3}, Landroid/support/v4/app/af;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v1

    const v2, 0x7f020001

    invoke-virtual {v1, v2}, Landroid/support/v4/app/af;->a(I)Landroid/support/v4/app/af;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/support/v4/app/af;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/support/v4/app/af;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/af;->a(Landroid/support/v4/app/am;)Landroid/support/v4/app/af;

    move-result-object v0

    invoke-virtual {v0, v3, v3, v3}, Landroid/support/v4/app/af;->a(IIZ)Landroid/support/v4/app/af;

    .line 358
    invoke-virtual {p1}, Landroid/support/v4/app/af;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 359
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 422
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 423
    if-eqz v0, :cond_0

    .line 424
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v0, Lcom/wssnps/smlNpsService;->i:Ljava/lang/String;

    .line 425
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/wssnps/smlNpsService;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/wssnps/smlNpsService;->e()V

    return-void
.end method

.method static synthetic a(Lcom/wssnps/smlNpsService;I)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/wssnps/smlNpsService;->a(I)V

    return-void
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 38
    sput-boolean p0, Lcom/wssnps/smlNpsService;->j:Z

    return p0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 429
    sget-object v0, Lcom/wssnps/smlNpsService;->i:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;)V
    .locals 7

    .prologue
    const v6, 0x7f04000b

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/high16 v3, 0x10000000

    .line 272
    invoke-virtual {p1, v4, v4, v4}, Landroid/support/v4/app/af;->a(IIZ)Landroid/support/v4/app/af;

    .line 273
    invoke-virtual {p1}, Landroid/support/v4/app/af;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 274
    invoke-virtual {p0, p2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 276
    const/4 v0, 0x0

    .line 278
    const v1, -0xffff

    if-ne p2, v1, :cond_1

    .line 280
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "content://contacts/people"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 281
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 324
    :cond_0
    :goto_0
    if-eqz v0, :cond_9

    .line 326
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1, v4, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 328
    invoke-virtual {p1, p3}, Landroid/support/v4/app/af;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/support/v4/app/af;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v1

    sget-object v2, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/af;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/support/v4/app/af;->a(Z)Landroid/support/v4/app/af;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/af;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/af;

    .line 336
    :goto_1
    invoke-virtual {p1}, Landroid/support/v4/app/af;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 337
    return-void

    .line 284
    :cond_1
    const v1, -0xfffe

    if-eq p2, v1, :cond_2

    const v1, -0xfff8

    if-ne p2, v1, :cond_3

    .line 286
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "content://com.android.calendar/time"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 287
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 290
    :cond_3
    const v1, -0xfffc

    if-ne p2, v1, :cond_4

    .line 292
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const-string v2, "content://mms-sms/all"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 293
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 296
    :cond_4
    const v1, -0xfff0

    if-ne p2, v1, :cond_0

    .line 298
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->a(Landroid/content/Context;)I

    move-result v1

    .line 299
    if-ne v1, v5, :cond_5

    .line 301
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 302
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 303
    const-string v1, "com.sec.android.app.memo"

    const-string v2, "com.sec.android.app.memo.Memo"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 305
    :cond_5
    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 307
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 308
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 309
    const-string v1, "com.sec.android.app.memo"

    const-string v2, "com.sec.android.app.memo.MemoRoot"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 311
    :cond_6
    const/4 v2, 0x3

    if-eq v1, v2, :cond_7

    const/4 v2, 0x4

    if-ne v1, v2, :cond_8

    .line 313
    :cond_7
    new-instance v0, Landroid/content/Intent;

    const-string v1, "smemo.intent.action.suggest"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 314
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto/16 :goto_0

    .line 316
    :cond_8
    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    .line 318
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 319
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 320
    const-string v1, "com.samsung.android.app.memo"

    const-string v2, "com.samsung.android.app.memo.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 333
    :cond_9
    invoke-virtual {p1, p3}, Landroid/support/v4/app/af;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    goto/16 :goto_1
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 522
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/wssnps/smlNpsService;->e:Landroid/os/Binder;

    if-eqz v1, :cond_0

    .line 527
    iget-object v1, p0, Lcom/wssnps/smlNpsService;->e:Landroid/os/Binder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-interface {v0, v1, v2, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 530
    :catch_0
    move-exception v0

    .line 532
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static c()V
    .locals 14

    .prologue
    const v13, 0x7f04000d

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 463
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 464
    new-instance v3, Landroid/support/v4/app/af;

    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-direct {v3, v1}, Landroid/support/v4/app/af;-><init>(Landroid/content/Context;)V

    .line 466
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/b/k;->b(Landroid/content/Context;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 467
    invoke-static {v4}, Lcom/wssnps/b/k;->c(Landroid/database/sqlite/SQLiteDatabase;)[I

    move-result-object v5

    .line 469
    array-length v1, v5

    new-array v6, v1, [Lcom/wssnps/b/c;

    move v1, v2

    .line 471
    :goto_0
    array-length v7, v5

    if-ge v1, v7, :cond_5

    .line 473
    aget v7, v5, v1

    invoke-static {v7, v4}, Lcom/wssnps/b/k;->a(ILandroid/database/sqlite/SQLiteDatabase;)Lcom/wssnps/b/c;

    move-result-object v7

    aput-object v7, v6, v1

    .line 474
    aget-object v7, v6, v1

    iget v7, v7, Lcom/wssnps/b/c;->a:I

    .line 475
    if-ne v7, v11, :cond_0

    .line 477
    aget-object v7, v6, v1

    iget v7, v7, Lcom/wssnps/b/c;->b:I

    const/16 v8, 0xb

    if-ne v7, v8, :cond_1

    .line 479
    const-string v7, "SML_IMPORT_CONTACT"

    invoke-static {v12, v7}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 480
    sget-object v7, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v7, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    sget-object v9, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v10, 0x7f040002

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 481
    sget-object v8, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v9, 0x7f04000f

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 482
    const v9, -0xffff

    invoke-static {v0, v3, v9, v8, v7}, Lcom/wssnps/smlNpsService;->a(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_0
    :goto_1
    aget v7, v5, v1

    int-to-long v8, v7

    invoke-static {v8, v9, v4}, Lcom/wssnps/b/k;->a(JLandroid/database/sqlite/SQLiteDatabase;)V

    .line 471
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 484
    :cond_1
    aget-object v7, v6, v1

    iget v7, v7, Lcom/wssnps/b/c;->b:I

    const/16 v8, 0xc

    if-ne v7, v8, :cond_2

    .line 486
    const-string v7, "SML_IMPORT_CALENDAR"

    invoke-static {v12, v7}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 487
    sget-object v7, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v7, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    sget-object v9, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v10, 0x7f040001

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 488
    sget-object v8, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v9, 0x7f04000e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 489
    const v9, -0xfffe

    invoke-static {v0, v3, v9, v8, v7}, Lcom/wssnps/smlNpsService;->a(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 491
    :cond_2
    aget-object v7, v6, v1

    iget v7, v7, Lcom/wssnps/b/c;->b:I

    const/16 v8, 0xf

    if-ne v7, v8, :cond_3

    .line 493
    const-string v7, "SML_IMPORT_MEMO"

    invoke-static {v12, v7}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 494
    sget-object v7, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v7, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    sget-object v9, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v10, 0x7f040003

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 495
    sget-object v8, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v9, 0x7f040010

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 496
    const v9, -0xfff0

    invoke-static {v0, v3, v9, v8, v7}, Lcom/wssnps/smlNpsService;->a(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 498
    :cond_3
    aget-object v7, v6, v1

    iget v7, v7, Lcom/wssnps/b/c;->b:I

    const/16 v8, 0xd

    if-ne v7, v8, :cond_4

    .line 500
    const-string v7, "SML_IMPORT_MESSAGE"

    invoke-static {v12, v7}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 501
    sget-object v7, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v7, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    sget-object v9, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v10, 0x7f040004

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 502
    sget-object v8, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v9, 0x7f040011

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 503
    const v9, -0xfffc

    invoke-static {v0, v3, v9, v8, v7}, Lcom/wssnps/smlNpsService;->a(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 505
    :cond_4
    aget-object v7, v6, v1

    iget v7, v7, Lcom/wssnps/b/c;->b:I

    const/16 v8, 0xe

    if-ne v7, v8, :cond_0

    .line 507
    const-string v7, "SML_IMPORT_TASK"

    invoke-static {v12, v7}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 508
    sget-object v7, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-virtual {v7, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    sget-object v9, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v10, 0x7f040005

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 509
    sget-object v8, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v9, 0x7f040012

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 510
    const v9, -0xfff8

    invoke-static {v0, v3, v9, v8, v7}, Lcom/wssnps/smlNpsService;->a(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 517
    :cond_5
    invoke-static {v4}, Lcom/wssnps/b/k;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 518
    return-void
.end method

.method public static c(Landroid/app/NotificationManager;Landroid/support/v4/app/af;ILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 341
    invoke-virtual {p1, v0, v0, v0}, Landroid/support/v4/app/af;->a(IIZ)Landroid/support/v4/app/af;

    .line 342
    invoke-virtual {p1}, Landroid/support/v4/app/af;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 343
    invoke-virtual {p0, p2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 344
    invoke-virtual {p1, p3}, Landroid/support/v4/app/af;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const v2, 0x7f04000c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/af;

    move-result-object v0

    const v1, 0x7f020001

    invoke-virtual {v0, v1}, Landroid/support/v4/app/af;->a(I)Landroid/support/v4/app/af;

    .line 347
    invoke-virtual {p1}, Landroid/support/v4/app/af;->a()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 348
    return-void
.end method

.method static synthetic d()Z
    .locals 1

    .prologue
    .line 38
    sget-boolean v0, Lcom/wssnps/smlNpsService;->j:Z

    return v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 167
    const/4 v0, 0x2

    const-string v1, "usbDisconnected"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ServiceInfo.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "hk.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/wssnps/smlModelDefine;->q(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "DeviceInfo.xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->g(Ljava/lang/String;)Z

    .line 172
    sput-boolean v2, Lcom/wssnps/smlNpsReceiver;->c:Z

    .line 173
    sput-boolean v2, Lcom/wssnps/smlNpsReceiver;->d:Z

    .line 175
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->x(Landroid/content/Context;)V

    .line 176
    sput v2, Lcom/wssnps/b/l;->as:I

    .line 177
    sput v2, Lcom/wssnps/b/b;->c:I

    .line 178
    invoke-static {}, Lcom/wssnps/b/d;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    const-class v2, Lcom/wssnps/smlNpsService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/wssnps/smlNpsService;->stopService(Landroid/content/Intent;)Z

    .line 180
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 383
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 385
    const-string v1, "com.sec.intent.action.SYSSCOPESTATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 386
    sget-object v1, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/wssnps/smlNpsService;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 387
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/wssnps/smlNpsService;->e:Landroid/os/Binder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 77
    const/4 v0, 0x2

    const-string v1, "Service onCreate!!"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 78
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 79
    sput-object p0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    .line 80
    invoke-direct {p0, v2}, Lcom/wssnps/smlNpsService;->b(Z)V

    .line 82
    sput-boolean v2, Lcom/wssnps/smlNpsService;->j:Z

    .line 84
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/wssnps/k;

    invoke-direct {v1}, Lcom/wssnps/k;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/wssnps/smlNpsService;->b:Ljava/lang/Thread;

    .line 85
    iget-object v0, p0, Lcom/wssnps/smlNpsService;->b:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 87
    iget-object v0, p0, Lcom/wssnps/smlNpsService;->k:Landroid/os/UEventObserver;

    const-string v1, "USB_STATE"

    invoke-virtual {v0, v1}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 88
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlNpsService;->a(Landroid/content/Context;)V

    .line 90
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->t(Landroid/content/Context;)V

    .line 92
    invoke-virtual {p0}, Lcom/wssnps/smlNpsService;->a()V

    .line 93
    invoke-static {}, Lcom/wssnps/smlNpsService;->c()V

    .line 95
    sget-boolean v0, Lcom/wssnps/smlNpsReceiver;->e:Z

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/wssnps/b/d;->a(Landroid/content/Context;)V

    .line 98
    invoke-static {}, Lcom/wssnps/b/d;->d()Z

    .line 100
    new-instance v0, Lcom/wssnps/h;

    invoke-direct {v0, p0}, Lcom/wssnps/h;-><init>(Lcom/wssnps/smlNpsService;)V

    .line 114
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 115
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 117
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 121
    const-string v0, "Service onDestroy"

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 123
    invoke-static {}, Lcom/wssnps/b/l;->ap()V

    .line 124
    sput v2, Lcom/wssnps/b/l;->as:I

    .line 125
    sput v2, Lcom/wssnps/b/b;->c:I

    .line 126
    invoke-static {}, Lcom/wssnps/smlModelDefine;->g()V

    .line 128
    sget-object v0, Lcom/wssnps/smlNpsService;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/wssnps/smlNpsService;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 129
    invoke-direct {p0, v2}, Lcom/wssnps/smlNpsService;->b(Z)V

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/wssnps/smlModelDefine;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/_SamsungBnR_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/wssnps/smlModelDefine;->i(Ljava/lang/String;)V

    .line 134
    sget-object v0, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x2

    :try_start_0
    const-string v1, "dsServerSocket close"

    invoke-static {v0, v1}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    .line 139
    const/4 v0, 0x0

    sput-boolean v0, Lcom/wssnps/smlNpsService;->j:Z

    .line 141
    sget-object v0, Lcom/wssnps/smlNpsService;->d:Landroid/net/LocalServerSocket;

    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 149
    return-void

    .line 143
    :catch_0
    move-exception v0

    .line 145
    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/wssnps/a;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 73
    return-void
.end method
