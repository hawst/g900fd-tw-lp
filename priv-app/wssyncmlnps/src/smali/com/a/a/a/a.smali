.class public final Lcom/a/a/a/a;
.super Ljava/lang/Object;
.source "SolarLunarConverter.java"


# static fields
.field static a:[I

.field static b:[I


# instance fields
.field private c:I

.field private d:I

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xd

    .line 15
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/a/a/a/a;->a:[I

    .line 17
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/a/a/a/a;->b:[I

    return-void

    .line 15
    nop

    :array_0
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb5
        0xd4
        0xf3
        0x111
        0x130
        0x14e
        0x16d
    .end array-data

    .line 17
    :array_1
    .array-data 4
        0x0
        0x1f
        0x3c
        0x5b
        0x79
        0x98
        0xb6
        0xd5
        0xf4
        0x112
        0x131
        0x14f
        0x16e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 88
    rem-int/lit8 v1, p1, 0x4

    if-gtz v1, :cond_0

    rem-int/lit8 v1, p1, 0x64

    if-ge v1, v0, :cond_1

    rem-int/lit16 v1, p1, 0x190

    if-lez v1, :cond_1

    .line 89
    :cond_0
    const/4 v0, 0x0

    .line 91
    :cond_1
    return v0
.end method

.method private b(I)[I
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/a/a/a/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/a/a/a/a;->b:[I

    .line 99
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/a/a/a/a;->a:[I

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/a/a/a/a;->c:I

    return v0
.end method

.method public a(IIIZ)V
    .locals 11

    .prologue
    const/16 v1, 0x16d

    const/16 v10, 0x1e

    const/16 v4, 0xc

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 167
    const-string v0, "CHN"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CHU"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CMCC"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CTC"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/a/a/a/a;->b(IIIZ)V

    .line 279
    :goto_0
    return-void

    .line 173
    :cond_1
    const/16 v0, 0x759

    if-lt p1, v0, :cond_2

    const/16 v0, 0x834

    if-gt p1, v0, :cond_2

    if-ltz p2, :cond_2

    const/16 v0, 0xb

    if-gt p2, v0, :cond_2

    if-lt p3, v9, :cond_2

    if-le p3, v10, :cond_3

    .line 175
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The date "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of range."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_3
    invoke-direct {p0, p1}, Lcom/a/a/a/a;->b(I)[I

    move-result-object v5

    .line 180
    add-int/lit16 v0, p1, -0x759

    .line 181
    mul-int/lit8 v6, v0, 0xe

    .line 182
    sget-object v2, Lcom/a/a/a/b;->b:[I

    aget v0, v2, v0

    .line 184
    sget-object v2, Lcom/a/a/a/b;->a:[B

    add-int/lit8 v7, v6, 0xd

    aget-byte v7, v2, v7

    .line 185
    const/16 v2, 0x7f

    if-ne v7, v2, :cond_4

    move v2, v4

    .line 187
    :goto_1
    if-ne v2, v4, :cond_5

    move v2, v3

    .line 189
    :goto_2
    if-ge v2, p2, :cond_8

    .line 191
    sget-object v4, Lcom/a/a/a/b;->a:[B

    add-int v7, v6, v2

    aget-byte v4, v4, v7

    add-int/2addr v4, v0

    .line 189
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_2

    .line 185
    :cond_4
    const/16 v2, 0xd

    goto :goto_1

    .line 196
    :cond_5
    if-eqz p4, :cond_6

    add-int/lit8 v2, p2, 0x1

    if-ne v2, v7, :cond_6

    move v2, v3

    .line 198
    :goto_3
    if-ge v2, v7, :cond_8

    .line 200
    sget-object v4, Lcom/a/a/a/b;->a:[B

    add-int v8, v6, v2

    aget-byte v4, v4, v8

    add-int/2addr v4, v0

    .line 198
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_3

    .line 206
    :cond_6
    add-int/lit8 v2, p2, 0x1

    if-le v2, v7, :cond_7

    .line 207
    add-int/lit8 p2, p2, 0x1

    :cond_7
    move v2, v3

    .line 211
    :goto_4
    if-ge v2, p2, :cond_8

    .line 213
    sget-object v4, Lcom/a/a/a/b;->a:[B

    add-int v7, v6, v2

    aget-byte v4, v4, v7

    add-int/2addr v4, v0

    .line 211
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_4

    .line 219
    :cond_8
    add-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    .line 222
    const/16 v2, 0x759

    iput v2, p0, Lcom/a/a/a/a;->c:I

    .line 223
    iput v3, p0, Lcom/a/a/a/a;->d:I

    .line 224
    iput v10, p0, Lcom/a/a/a/a;->e:I

    .line 226
    const/16 v2, 0x14f

    if-le v0, v2, :cond_c

    .line 229
    const/16 v2, 0x75a

    iput v2, p0, Lcom/a/a/a/a;->c:I

    .line 230
    iput v3, p0, Lcom/a/a/a/a;->d:I

    .line 231
    iput v9, p0, Lcom/a/a/a/a;->e:I

    .line 232
    add-int/lit16 v0, v0, -0x150

    move v2, v0

    move v0, v1

    .line 236
    :goto_5
    if-lt v2, v0, :cond_a

    .line 238
    sub-int/2addr v2, v0

    .line 240
    iget v0, p0, Lcom/a/a/a/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/a/a/a;->c:I

    .line 241
    iget v0, p0, Lcom/a/a/a/a;->c:I

    invoke-direct {p0, v0}, Lcom/a/a/a/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 242
    const/16 v0, 0x16e

    goto :goto_5

    :cond_9
    move v0, v1

    .line 244
    goto :goto_5

    .line 248
    :cond_a
    :goto_6
    iget v0, p0, Lcom/a/a/a/a;->d:I

    add-int/lit8 v0, v0, 0x1

    aget v0, v5, v0

    if-lt v2, v0, :cond_b

    .line 250
    iget v0, p0, Lcom/a/a/a/a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/a/a/a;->d:I

    goto :goto_6

    .line 252
    :cond_b
    iget v0, p0, Lcom/a/a/a/a;->d:I

    aget v0, v5, v0

    sub-int v0, v2, v0

    .line 255
    iget v1, p0, Lcom/a/a/a/a;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/a/a/a;->e:I

    goto/16 :goto_0

    .line 259
    :cond_c
    if-le v0, v9, :cond_e

    .line 261
    iput v9, p0, Lcom/a/a/a/a;->d:I

    .line 262
    iput v9, p0, Lcom/a/a/a/a;->e:I

    .line 263
    add-int/lit8 v0, v0, -0x2

    .line 265
    :goto_7
    iget v1, p0, Lcom/a/a/a/a;->d:I

    add-int/lit8 v1, v1, 0x1

    aget v1, v5, v1

    if-lt v0, v1, :cond_d

    .line 267
    iget v1, p0, Lcom/a/a/a/a;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/a/a/a;->d:I

    goto :goto_7

    .line 269
    :cond_d
    iget v1, p0, Lcom/a/a/a/a;->d:I

    aget v1, v5, v1

    sub-int/2addr v0, v1

    .line 272
    iget v1, p0, Lcom/a/a/a/a;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/a/a/a;->e:I

    goto/16 :goto_0

    .line 276
    :cond_e
    iget v1, p0, Lcom/a/a/a/a;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/a/a/a;->e:I

    goto/16 :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/a/a/a/a;->d:I

    return v0
.end method

.method public b(IIIZ)V
    .locals 11

    .prologue
    const/16 v1, 0x16d

    const/16 v10, 0x1e

    const/16 v4, 0xc

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 360
    const/16 v0, 0x759

    if-lt p1, v0, :cond_0

    const/16 v0, 0x834

    if-gt p1, v0, :cond_0

    if-ltz p2, :cond_0

    const/16 v0, 0xb

    if-gt p2, v0, :cond_0

    if-lt p3, v9, :cond_0

    if-le p3, v10, :cond_1

    if-nez p4, :cond_1

    .line 364
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The date "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of range."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_1
    invoke-direct {p0, p1}, Lcom/a/a/a/a;->b(I)[I

    move-result-object v5

    .line 368
    add-int/lit16 v0, p1, -0x759

    .line 369
    mul-int/lit8 v6, v0, 0xe

    .line 371
    sget-object v2, Lcom/a/a/a/b;->d:[I

    aget v0, v2, v0

    .line 372
    sget-object v2, Lcom/a/a/a/b;->c:[B

    add-int/lit8 v7, v6, 0xd

    aget-byte v7, v2, v7

    .line 374
    const/16 v2, 0x7f

    if-ne v7, v2, :cond_2

    move v2, v4

    .line 375
    :goto_0
    if-ne v2, v4, :cond_3

    move v2, v3

    .line 377
    :goto_1
    if-ge v2, p2, :cond_6

    .line 380
    sget-object v4, Lcom/a/a/a/b;->c:[B

    add-int v7, v6, v2

    aget-byte v4, v4, v7

    add-int/2addr v4, v0

    .line 377
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_1

    .line 374
    :cond_2
    const/16 v2, 0xd

    goto :goto_0

    .line 386
    :cond_3
    if-eqz p4, :cond_4

    add-int/lit8 v2, p2, 0x1

    if-ne v2, v7, :cond_4

    move v2, v3

    .line 388
    :goto_2
    if-ge v2, v7, :cond_6

    .line 391
    sget-object v4, Lcom/a/a/a/b;->c:[B

    add-int v8, v6, v2

    aget-byte v4, v4, v8

    add-int/2addr v4, v0

    .line 388
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_2

    .line 398
    :cond_4
    add-int/lit8 v2, p2, 0x1

    if-le v2, v7, :cond_5

    .line 399
    add-int/lit8 p2, p2, 0x1

    :cond_5
    move v2, v3

    .line 403
    :goto_3
    if-ge v2, p2, :cond_6

    .line 406
    sget-object v4, Lcom/a/a/a/b;->c:[B

    add-int v7, v6, v2

    aget-byte v4, v4, v7

    add-int/2addr v4, v0

    .line 403
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_3

    .line 413
    :cond_6
    add-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    .line 415
    const/16 v2, 0x759

    iput v2, p0, Lcom/a/a/a/a;->c:I

    .line 416
    iput v3, p0, Lcom/a/a/a/a;->d:I

    .line 417
    iput v10, p0, Lcom/a/a/a/a;->e:I

    .line 418
    const/16 v2, 0x14f

    if-le v0, v2, :cond_a

    .line 421
    const/16 v2, 0x75a

    iput v2, p0, Lcom/a/a/a/a;->c:I

    .line 422
    iput v3, p0, Lcom/a/a/a/a;->d:I

    .line 423
    iput v9, p0, Lcom/a/a/a/a;->e:I

    .line 425
    add-int/lit16 v0, v0, -0x150

    move v2, v0

    move v0, v1

    .line 429
    :goto_4
    if-lt v2, v0, :cond_8

    .line 431
    sub-int/2addr v2, v0

    .line 432
    iget v0, p0, Lcom/a/a/a/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/a/a/a;->c:I

    .line 433
    iget v0, p0, Lcom/a/a/a/a;->c:I

    invoke-direct {p0, v0}, Lcom/a/a/a/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 434
    const/16 v0, 0x16e

    goto :goto_4

    :cond_7
    move v0, v1

    .line 436
    goto :goto_4

    .line 440
    :cond_8
    :goto_5
    iget v0, p0, Lcom/a/a/a/a;->d:I

    add-int/lit8 v0, v0, 0x1

    aget v0, v5, v0

    if-lt v2, v0, :cond_9

    .line 442
    iget v0, p0, Lcom/a/a/a/a;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/a/a/a/a;->d:I

    goto :goto_5

    .line 444
    :cond_9
    iget v0, p0, Lcom/a/a/a/a;->d:I

    aget v0, v5, v0

    sub-int v0, v2, v0

    .line 447
    iget v1, p0, Lcom/a/a/a/a;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/a/a/a;->e:I

    .line 470
    :goto_6
    return-void

    .line 451
    :cond_a
    if-le v0, v9, :cond_c

    .line 453
    iput v9, p0, Lcom/a/a/a/a;->d:I

    .line 454
    iput v9, p0, Lcom/a/a/a/a;->e:I

    .line 455
    add-int/lit8 v0, v0, -0x2

    .line 456
    :goto_7
    iget v1, p0, Lcom/a/a/a/a;->d:I

    add-int/lit8 v1, v1, 0x1

    aget v1, v5, v1

    if-lt v0, v1, :cond_b

    .line 458
    iget v1, p0, Lcom/a/a/a/a;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/a/a/a;->d:I

    goto :goto_7

    .line 460
    :cond_b
    iget v1, p0, Lcom/a/a/a/a;->d:I

    aget v1, v5, v1

    sub-int/2addr v0, v1

    .line 463
    iget v1, p0, Lcom/a/a/a/a;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/a/a/a;->e:I

    goto :goto_6

    .line 467
    :cond_c
    iget v1, p0, Lcom/a/a/a/a;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/a/a/a/a;->e:I

    goto :goto_6
.end method

.method public c()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/a/a/a/a;->e:I

    return v0
.end method
