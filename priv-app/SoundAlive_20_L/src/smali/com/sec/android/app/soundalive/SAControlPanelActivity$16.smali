.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;
.super Ljava/lang/Object;
.source "SAControlPanelActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initLayout(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0

    .prologue
    .line 1827
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1830
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1831
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isEnableAdaptSoundPath()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1832
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->warningAdaptSound(Landroid/content/Context;)V

    .line 1833
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAdapt(Z)V
    invoke-static {v2, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 1859
    :goto_0
    return-void

    .line 1836
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAdapt(Z)V
    invoke-static {v2, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 1837
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hearing_diagnosis"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1838
    .local v0, "DBdata":I
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_1

    .line 1839
    const-string v2, "SAControlPanelActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "db data: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1840
    :cond_1
    if-nez v0, :cond_2

    .line 1842
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1843
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.sec.hearingadjust.launch"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1844
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1848
    .end local v1    # "i":Landroid/content/Intent;
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "internal_SA_Adapt_changed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1849
    .restart local v1    # "i":Landroid/content/Intent;
    const-string v2, "SA_AdaptSound_State"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1850
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 1854
    .end local v0    # "DBdata":I
    .end local v1    # "i":Landroid/content/Intent;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAdapt(Z)V
    invoke-static {v2, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 1855
    new-instance v1, Landroid/content/Intent;

    const-string v2, "internal_SA_Adapt_changed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1856
    .restart local v1    # "i":Landroid/content/Intent;
    const-string v2, "SA_AdaptSound_State"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1857
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
