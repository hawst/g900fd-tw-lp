.class public Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;
.super Landroid/content/BroadcastReceiver;
.source "HelperGooglePlayMusic.java"


# static fields
.field public static final SUPPORT_PACKAGE_NAME:Ljava/lang/String; = "com.google.android.music"

.field private static final TAG:Ljava/lang/String; = "HelperGooglePlayMusic"

.field private static isNextTrackRequest:Z

.field private static isPlayStateChanged:Z

.field private static prevAudioSession:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    const/4 v0, -0x4

    sput v0, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->prevAudioSession:I

    .line 17
    sput-boolean v1, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->isPlayStateChanged:Z

    .line 18
    sput-boolean v1, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->isNextTrackRequest:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 24
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 25
    :cond_0
    sget-boolean v7, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v7, :cond_1

    .line 26
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "Context or intent is null. Do nothing."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_1
    :goto_0
    return-void

    .line 30
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "action":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 33
    sget-boolean v7, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v7, :cond_1

    .line 34
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "action is null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 38
    :cond_3
    sget-boolean v7, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v7, :cond_4

    .line 39
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "*****onReceive()*****"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    const-string v7, "HelperGooglePlayMusic"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Action: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_4
    const-string v7, "com.android.music.playstatechanged"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 44
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->isPlayStateChanged:Z

    .line 45
    const-string v7, "HelperGooglePlayMusic"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACTION_PLAYSTATE_CHANGED : Action: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 50
    :cond_5
    const-string v7, "android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 52
    const-string v7, "android.media.extra.PACKAGE_NAME"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, "PackageName":Ljava/lang/String;
    const-string v7, "android.media.extra.AUDIO_SESSION"

    const/4 v8, -0x4

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 56
    .local v2, "currentAudioSession":I
    if-eqz v0, :cond_1

    .line 59
    const-string v7, "com.google.android.music"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 62
    const/4 v7, -0x4

    if-eq v2, v7, :cond_6

    if-gez v2, :cond_7

    .line 63
    :cond_6
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "Invalid or missing audio session or pakagename "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 67
    :cond_7
    invoke-static {p1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v6

    .line 69
    .local v6, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const/4 v5, 0x0

    .line 70
    .local v5, "isThisRCV_Got_Control_FromOthers":Z
    const-string v7, "WHICH_RCV_HAS_CONTRL"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 72
    .local v4, "idx":I
    const/16 v7, 0x8a

    if-eq v4, v7, :cond_8

    .line 73
    const/4 v5, 0x1

    .line 76
    :cond_8
    const-string v7, "AUDIOSESSION_ID"

    invoke-virtual {v6, v7, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 77
    const-string v7, "WHICH_RCV_HAS_CONTRL"

    const/16 v8, 0x8a

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 78
    const-string v7, "CURRENT_PACKAGENAME"

    invoke-virtual {v6, v7, v0}, Lcom/sec/android/app/soundalive/PreferencesManager;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    sget-boolean v7, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v7, :cond_9

    .line 81
    const-string v7, "HelperGooglePlayMusic"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OPEN_SESSION PackageName :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " currentAudioSession : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  prevAudioSession : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->prevAudioSession:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_9
    new-instance v3, Landroid/content/Intent;

    const-class v7, Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-direct {v3, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    .local v3, "i":Landroid/content/Intent;
    sget v7, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->prevAudioSession:I

    if-ne v2, v7, :cond_a

    if-eqz v5, :cond_d

    .line 88
    :cond_a
    sget-boolean v7, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v7, :cond_b

    .line 89
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "start SAControlPanelService - SERVICE_STATE_OPEN"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_b
    const-string v7, "SA_SERVICE_STATE"

    const/16 v8, 0x9

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 92
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 110
    :cond_c
    :goto_1
    sput v2, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->prevAudioSession:I

    goto/16 :goto_0

    .line 94
    :cond_d
    sget-boolean v7, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->isPlayStateChanged:Z

    if-eqz v7, :cond_e

    .line 95
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "start SAControlPanelService - SERVICE_STATE_RESUME"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v7, "SA_SERVICE_STATE"

    const/16 v8, 0xa

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 98
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 99
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->isPlayStateChanged:Z

    goto :goto_1

    .line 101
    :cond_e
    sget-boolean v7, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->isNextTrackRequest:Z

    if-eqz v7, :cond_c

    .line 102
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "start SAControlPanelService - SERVICE_STATE_OPEN for next track"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const-string v7, "SA_SERVICE_STATE"

    const/16 v8, 0x9

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 105
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 106
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->isNextTrackRequest:Z

    goto :goto_1

    .line 114
    .end local v0    # "PackageName":Ljava/lang/String;
    .end local v2    # "currentAudioSession":I
    .end local v3    # "i":Landroid/content/Intent;
    .end local v4    # "idx":I
    .end local v5    # "isThisRCV_Got_Control_FromOthers":Z
    .end local v6    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_f
    const-string v7, "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 116
    const-string v7, "android.media.extra.PACKAGE_NAME"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    .restart local v0    # "PackageName":Ljava/lang/String;
    const-string v7, "android.media.extra.AUDIO_SESSION"

    const/4 v8, -0x4

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 120
    .restart local v2    # "currentAudioSession":I
    if-eqz v0, :cond_1

    .line 123
    const-string v7, "com.google.android.music"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 125
    invoke-static {p1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v6

    .line 126
    .restart local v6    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v7, "WHICH_RCV_HAS_CONTRL"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 128
    .restart local v4    # "idx":I
    const/16 v7, 0x8a

    if-eq v4, v7, :cond_10

    .line 129
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "PlayMusic sending CLOSE SESSION Intent with too much delay, so we need to skip SA service pause"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 133
    :cond_10
    sget-boolean v7, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v7, :cond_11

    .line 134
    const-string v7, "HelperGooglePlayMusic"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CLOSE_SESSION PackageName :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " currentAudioSession : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  prevAudioSession : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget v9, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->prevAudioSession:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    const-string v7, "HelperGooglePlayMusic"

    const-string v8, "start SAControlPanelService - SERVICE_STATE_PAUSE"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_11
    new-instance v3, Landroid/content/Intent;

    const-class v7, Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-direct {v3, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    .restart local v3    # "i":Landroid/content/Intent;
    const-string v7, "SA_SERVICE_STATE"

    const/16 v8, 0xb

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 141
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 142
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->isPlayStateChanged:Z

    .line 143
    sput v2, Lcom/sec/android/app/soundalive/compatibility/HelperGooglePlayMusic;->prevAudioSession:I

    goto/16 :goto_0
.end method
