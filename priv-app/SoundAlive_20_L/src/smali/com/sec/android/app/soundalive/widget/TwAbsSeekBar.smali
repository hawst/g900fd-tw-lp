.class public abstract Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;
.super Lcom/sec/android/app/soundalive/widget/TwProgressBar;
.source "TwAbsSeekBar.java"


# static fields
.field private static final NO_ALPHA:I = 0xff


# instance fields
.field private TWSEEKTHUMB_DEFAULT_FONT_SIZE:F

.field private final debug:Z

.field private mDisabledAlpha:F

.field private mHoverUIEnabled:I

.field private mHoveringLevel:I

.field private mIsDisableCompensationTouchArea:Z

.field private mIsTouch:Z

.field mIsUserSeekable:Z

.field private mKeyProgressIncrement:I

.field private mScale:F

.field private mSeekThumbFontBoldStyle:Z

.field private mSeekThumbFontColor:I

.field private mSeekThumbFontEnable:Z

.field private mSeekThumbFontPainter:Landroid/graphics/Paint;

.field private mSeekThumbFontSize:F

.field private mThumb:Landroid/graphics/drawable/Drawable;

.field private mThumbOffset:I

.field private mThumbPosX:I

.field private mThumbPosY:I

.field mTouchProgressOffset:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 110
    const v0, 0x7f01001c

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 125
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    iput-boolean v7, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->debug:Z

    .line 51
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mScale:F

    .line 57
    iput v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mTouchProgressOffset:F

    .line 62
    iput-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsUserSeekable:Z

    .line 68
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    .line 73
    iput-boolean v7, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsTouch:Z

    .line 77
    iput-boolean v7, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontEnable:Z

    .line 78
    iput-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontBoldStyle:Z

    .line 80
    const/high16 v4, 0x41700000    # 15.0f

    iput v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->TWSEEKTHUMB_DEFAULT_FONT_SIZE:F

    .line 84
    iput-boolean v7, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsDisableCompensationTouchArea:Z

    .line 87
    iput v7, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoverUIEnabled:I

    .line 88
    iput v7, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoveringLevel:I

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    iput v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mScale:F

    .line 128
    sget-object v4, Lcom/sec/android/app/soundalive/R$styleable;->TwSeekBar:[I

    invoke-virtual {p1, p2, v4, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 129
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v7}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 131
    .local v2, "thumb":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_0

    .line 132
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getThumbOffset()I

    move-result v4

    invoke-virtual {v0, v8, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    .line 140
    .local v3, "thumbOffset":I
    invoke-virtual {p0, v3}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setThumbOffset(I)V

    .line 141
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v9}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mDisabledAlpha:F

    .line 142
    const/4 v4, 0x5

    const v5, 0x7f040018

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontColor:I

    .line 143
    const/4 v4, 0x6

    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->TWSEEKTHUMB_DEFAULT_FONT_SIZE:F

    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mScale:F

    mul-float/2addr v5, v6

    add-float/2addr v5, v9

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontSize:F

    .line 144
    const/4 v4, 0x7

    invoke-virtual {v0, v4, v8}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontBoldStyle:Z

    .line 145
    const/16 v4, 0x8

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontEnable:Z

    .line 146
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 148
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontPainter:Landroid/graphics/Paint;

    .line 149
    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontPainter:Landroid/graphics/Paint;

    invoke-virtual {v4, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 150
    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontPainter:Landroid/graphics/Paint;

    iget-boolean v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontBoldStyle:Z

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 156
    return-void

    .line 134
    .end local v3    # "thumbOffset":I
    :catch_0
    move-exception v1

    .line 135
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private attemptClaimDrag()V
    .locals 0

    .prologue
    .line 662
    return-void
.end method

.method private setThumbPos(ILandroid/graphics/drawable/Drawable;FI)V
    .locals 11
    .param p1, "w"    # I
    .param p2, "thumb"    # Landroid/graphics/drawable/Drawable;
    .param p3, "scale"    # F
    .param p4, "gap"    # I

    .prologue
    .line 428
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 429
    .local v3, "progress":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    div-int/lit8 v4, v9, 0x2

    .line 431
    .local v4, "progressSpacing":I
    if-lez v4, :cond_0

    instance-of v9, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarSplit;

    if-eqz v9, :cond_1

    .line 432
    :cond_0
    const/4 v4, 0x0

    .line 434
    :cond_1
    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    sub-int v9, p1, v9

    iget v10, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingRight:I

    sub-int/2addr v9, v10

    sub-int v0, v9, v4

    .line 435
    .local v0, "available":I
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 436
    .local v7, "thumbWidth":I
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 437
    .local v5, "thumbHeight":I
    sub-int/2addr v0, v7

    .line 440
    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbOffset:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v0, v9

    .line 442
    int-to-float v9, v0

    mul-float/2addr v9, p3

    float-to-int v9, v9

    add-int v6, v9, v4

    .line 445
    .local v6, "thumbPos":I
    const/high16 v9, -0x80000000

    if-ne p4, v9, :cond_2

    .line 446
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 447
    .local v2, "oldBounds":Landroid/graphics/Rect;
    iget v8, v2, Landroid/graphics/Rect;->top:I

    .line 448
    .local v8, "topBound":I
    iget v1, v2, Landroid/graphics/Rect;->bottom:I

    .line 455
    .end local v2    # "oldBounds":Landroid/graphics/Rect;
    .local v1, "bottomBound":I
    :goto_0
    add-int v9, v6, v7

    invoke-virtual {p2, v6, v8, v9, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 456
    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosX:I

    .line 457
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosY:I

    .line 458
    return-void

    .line 450
    .end local v1    # "bottomBound":I
    .end local v8    # "topBound":I
    :cond_2
    move v8, p4

    .line 451
    .restart local v8    # "topBound":I
    add-int v1, p4, v5

    .restart local v1    # "bottomBound":I
    goto :goto_0
.end method

.method private setThumbVerticalPos(ILandroid/graphics/drawable/Drawable;FI)V
    .locals 9
    .param p1, "h"    # I
    .param p2, "thumb"    # Landroid/graphics/drawable/Drawable;
    .param p3, "scale"    # F
    .param p4, "gap"    # I

    .prologue
    .line 465
    iget v7, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    sub-int v7, p1, v7

    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingBottom:I

    sub-int v0, v7, v8

    .line 466
    .local v0, "available":I
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 467
    .local v6, "thumbWidth":I
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    .line 469
    .local v4, "thumbHeight":I
    const/high16 v7, 0x3f800000    # 1.0f

    sub-float/2addr v7, p3

    int-to-float v8, v0

    mul-float/2addr v7, v8

    float-to-int v7, v7

    div-int/lit8 v8, v4, 0x2

    sub-int v5, v7, v8

    .line 472
    .local v5, "thumbPos":I
    const/high16 v7, -0x80000000

    if-ne p4, v7, :cond_0

    .line 473
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    .line 474
    .local v2, "oldBounds":Landroid/graphics/Rect;
    iget v1, v2, Landroid/graphics/Rect;->left:I

    .line 475
    .local v1, "leftBound":I
    iget v3, v2, Landroid/graphics/Rect;->right:I

    .line 482
    .end local v2    # "oldBounds":Landroid/graphics/Rect;
    .local v3, "rightBound":I
    :goto_0
    add-int v7, v5, v4

    invoke-virtual {p2, v1, v5, v3, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 484
    iput v1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosX:I

    .line 485
    iput v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosY:I

    .line 486
    return-void

    .line 477
    .end local v1    # "leftBound":I
    .end local v3    # "rightBound":I
    :cond_0
    move v1, p4

    .line 478
    .restart local v1    # "leftBound":I
    add-int v3, p4, v6

    .restart local v3    # "rightBound":I
    goto :goto_0
.end method

.method private trackHoverEvent(Landroid/view/MotionEvent;)V
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x0

    .line 773
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getWidth()I

    move-result v5

    .line 774
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHeight()I

    move-result v1

    .line 776
    .local v1, "height":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v6, v8

    .line 777
    .local v6, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v7, v8

    .line 779
    .local v7, "y":I
    const/4 v3, 0x0

    .line 780
    .local v3, "progress":F
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getMax()I

    move-result v2

    .line 782
    .local v2, "max":I
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v8

    if-nez v8, :cond_6

    .line 783
    iget-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsDisableCompensationTouchArea:Z

    if-eqz v8, :cond_1

    .line 785
    if-ltz v7, :cond_0

    if-le v7, v1, :cond_1

    .line 825
    :cond_0
    :goto_0
    return-void

    .line 790
    :cond_1
    if-ltz v7, :cond_2

    if-le v7, v1, :cond_3

    .line 791
    :cond_2
    invoke-virtual {p0, v9}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setPressed(Z)V

    .line 792
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsTouch:Z

    goto :goto_0

    .line 796
    :cond_3
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    sub-int v8, v5, v8

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingRight:I

    sub-int v0, v8, v9

    .line 797
    .local v0, "available":I
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    if-ge v6, v8, :cond_4

    .line 798
    const/4 v4, 0x0

    .line 823
    .local v4, "scale":F
    :goto_1
    int-to-float v8, v2

    mul-float/2addr v8, v4

    add-float/2addr v3, v8

    .line 824
    float-to-int v8, v3

    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoveringLevel:I

    goto :goto_0

    .line 799
    .end local v4    # "scale":F
    :cond_4
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingRight:I

    sub-int v8, v5, v8

    if-le v6, v8, :cond_5

    .line 800
    const/high16 v4, 0x3f800000    # 1.0f

    .restart local v4    # "scale":F
    goto :goto_1

    .line 802
    .end local v4    # "scale":F
    :cond_5
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    sub-int v8, v6, v8

    int-to-float v8, v8

    int-to-float v9, v0

    div-float v4, v8, v9

    .line 803
    .restart local v4    # "scale":F
    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mTouchProgressOffset:F

    goto :goto_1

    .line 806
    .end local v0    # "available":I
    .end local v4    # "scale":F
    :cond_6
    iget-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsDisableCompensationTouchArea:Z

    if-eqz v8, :cond_7

    .line 808
    if-ltz v6, :cond_0

    if-gt v6, v5, :cond_0

    .line 813
    :cond_7
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    sub-int v8, v1, v8

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingBottom:I

    sub-int v0, v8, v9

    .line 814
    .restart local v0    # "available":I
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    if-ge v7, v8, :cond_8

    .line 815
    const/high16 v4, 0x3f800000    # 1.0f

    .restart local v4    # "scale":F
    goto :goto_1

    .line 816
    .end local v4    # "scale":F
    :cond_8
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingBottom:I

    sub-int v8, v1, v8

    if-le v7, v8, :cond_9

    .line 817
    const/4 v4, 0x0

    .restart local v4    # "scale":F
    goto :goto_1

    .line 819
    .end local v4    # "scale":F
    :cond_9
    const/high16 v8, 0x3f800000    # 1.0f

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    sub-int v9, v7, v9

    int-to-float v9, v9

    int-to-float v10, v0

    div-float/2addr v9, v10

    sub-float v4, v8, v9

    .line 820
    .restart local v4    # "scale":F
    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mTouchProgressOffset:F

    goto :goto_1
.end method

.method private trackTouchEvent(Landroid/view/MotionEvent;)V
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 595
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getWidth()I

    move-result v5

    .line 596
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHeight()I

    move-result v1

    .line 598
    .local v1, "height":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v6, v8

    .line 599
    .local v6, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v7, v8

    .line 601
    .local v7, "y":I
    const/4 v3, 0x0

    .line 602
    .local v3, "progress":F
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getMax()I

    move-result v2

    .line 604
    .local v2, "max":I
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v8

    if-nez v8, :cond_4

    .line 605
    iget-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsDisableCompensationTouchArea:Z

    if-eqz v8, :cond_1

    .line 607
    if-ltz v7, :cond_0

    if-le v7, v1, :cond_1

    .line 651
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    sub-int v8, v5, v8

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingRight:I

    sub-int v0, v8, v9

    .line 619
    .local v0, "available":I
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    if-ge v6, v8, :cond_2

    .line 620
    const/4 v4, 0x0

    .line 645
    .local v4, "scale":F
    :goto_1
    int-to-float v8, v2

    mul-float/2addr v8, v4

    add-float/2addr v3, v8

    .line 650
    float-to-int v8, v3

    const/4 v9, 0x1

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setProgress(IZ)V

    goto :goto_0

    .line 621
    .end local v4    # "scale":F
    :cond_2
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingRight:I

    sub-int v8, v5, v8

    if-le v6, v8, :cond_3

    .line 622
    const/high16 v4, 0x3f800000    # 1.0f

    .restart local v4    # "scale":F
    goto :goto_1

    .line 624
    .end local v4    # "scale":F
    :cond_3
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    sub-int v8, v6, v8

    int-to-float v8, v8

    int-to-float v9, v0

    div-float v4, v8, v9

    .line 625
    .restart local v4    # "scale":F
    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mTouchProgressOffset:F

    goto :goto_1

    .line 628
    .end local v0    # "available":I
    .end local v4    # "scale":F
    :cond_4
    iget-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsDisableCompensationTouchArea:Z

    if-eqz v8, :cond_5

    .line 630
    if-ltz v6, :cond_0

    if-gt v6, v5, :cond_0

    .line 635
    :cond_5
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    sub-int v8, v1, v8

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingBottom:I

    sub-int v0, v8, v9

    .line 636
    .restart local v0    # "available":I
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    if-ge v7, v8, :cond_6

    .line 637
    const/high16 v4, 0x3f800000    # 1.0f

    .restart local v4    # "scale":F
    goto :goto_1

    .line 638
    .end local v4    # "scale":F
    :cond_6
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingBottom:I

    sub-int v8, v1, v8

    if-le v7, v8, :cond_7

    .line 639
    const/4 v4, 0x0

    .restart local v4    # "scale":F
    goto :goto_1

    .line 641
    .end local v4    # "scale":F
    :cond_7
    const/high16 v8, 0x3f800000    # 1.0f

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    sub-int v9, v7, v9

    int-to-float v9, v9

    int-to-float v10, v0

    div-float/2addr v9, v10

    sub-float v4, v8, v9

    .line 642
    .restart local v4    # "scale":F
    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mTouchProgressOffset:F

    goto :goto_1
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 5

    .prologue
    .line 315
    invoke-super {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->drawableStateChanged()V

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 318
    .local v1, "progressDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0xff

    :goto_0
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 322
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 323
    .local v0, "bgDrawble":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_1

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getDrawableState()[I

    move-result-object v2

    .line 325
    .local v2, "state":[I
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 328
    .end local v2    # "state":[I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getDrawableState()[I

    move-result-object v2

    .line 330
    .restart local v2    # "state":[I
    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 332
    .end local v2    # "state":[I
    :cond_2
    return-void

    .line 319
    .end local v0    # "bgDrawble":Landroid/graphics/drawable/Drawable;
    :cond_3
    const/high16 v3, 0x437f0000    # 255.0f

    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mDisabledAlpha:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    goto :goto_0
.end method

.method public getKeyProgressIncrement()I
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    return v0
.end method

.method public getSeekThumbFontColor()I
    .locals 1

    .prologue
    .line 282
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontColor:I

    return v0
.end method

.method public getSeekThumbFontSize()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontSize:F

    float-to-int v0, v0

    return v0
.end method

.method public getThumbCenterPosX()I
    .locals 3

    .prologue
    .line 381
    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosX:I

    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int v0, v1, v2

    .line 383
    .local v0, "thumbCenterPosX":I
    return v0
.end method

.method public getThumbCenterPosY()I
    .locals 3

    .prologue
    .line 393
    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosY:I

    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int v0, v1, v2

    .line 395
    .local v0, "thumbCenterPosY":I
    return v0
.end method

.method public getThumbHeight()I
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public getThumbOffset()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbOffset:I

    return v0
.end method

.method public getThumbWidth()I
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "dr"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 298
    invoke-super {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->invalidate()V

    .line 300
    return-void
.end method

.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 493
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->onDraw(Landroid/graphics/Canvas;)V

    .line 495
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHeight()I

    move-result v0

    .line 496
    .local v0, "height":I
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-eqz v8, :cond_1

    .line 497
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 500
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbOffset:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 501
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 503
    iget-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsTouch:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontEnable:Z

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 505
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgress()I

    move-result v1

    .line 506
    .local v1, "progress":I
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    .line 507
    .local v7, "thumbWidth":I
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    .line 508
    .local v5, "thumbHeight":I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 509
    .local v6, "thumbText":Ljava/lang/CharSequence;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 511
    .local v2, "rectText":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontPainter:Landroid/graphics/Paint;

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontSize:F

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 512
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontPainter:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontColor:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 513
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontPainter:Landroid/graphics/Paint;

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v11

    invoke-virtual {v8, v9, v10, v11, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 515
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v8

    if-nez v8, :cond_2

    .line 516
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosX:I

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v9, v7, v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, v2, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    sub-float v3, v8, v9

    .line 517
    .local v3, "textX":F
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosY:I

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int v9, v0, v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, v2, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    sub-float v4, v8, v9

    .line 523
    .local v4, "textY":F
    :goto_0
    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontPainter:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v3, v4, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 526
    .end local v1    # "progress":I
    .end local v2    # "rectText":Landroid/graphics/Rect;
    .end local v3    # "textX":F
    .end local v4    # "textY":F
    .end local v5    # "thumbHeight":I
    .end local v6    # "thumbText":Ljava/lang/CharSequence;
    .end local v7    # "thumbWidth":I
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528
    :cond_1
    monitor-exit p0

    return-void

    .line 519
    .restart local v1    # "progress":I
    .restart local v2    # "rectText":Landroid/graphics/Rect;
    .restart local v5    # "thumbHeight":I
    .restart local v6    # "thumbText":Ljava/lang/CharSequence;
    .restart local v7    # "thumbWidth":I
    :cond_2
    :try_start_1
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosX:I

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v9, v7, v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, v2, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    sub-float v3, v8, v9

    .line 520
    .restart local v3    # "textX":F
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbPosY:I

    int-to-float v8, v8

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int v9, v5, v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    add-float/2addr v8, v9

    iget v9, v2, Landroid/graphics/Rect;->top:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-float v9, v9

    sub-float v4, v8, v9

    .restart local v4    # "textY":F
    goto :goto_0

    .line 493
    .end local v0    # "height":I
    .end local v1    # "progress":I
    .end local v2    # "rectText":Landroid/graphics/Rect;
    .end local v3    # "textX":F
    .end local v4    # "textY":F
    .end local v5    # "thumbHeight":I
    .end local v6    # "thumbText":Ljava/lang/CharSequence;
    .end local v7    # "thumbWidth":I
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8
.end method

.method onHoverChanged(III)V
    .locals 0
    .param p1, "hoverLevel"    # I
    .param p2, "posX"    # I
    .param p3, "posY"    # I

    .prologue
    .line 770
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 725
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    move v1, v4

    .line 728
    .local v1, "isPossibleTooltype":Z
    :cond_0
    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoverUIEnabled:I

    if-nez v5, :cond_1

    .line 729
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.feature.hovering_ui"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 730
    iput v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoverUIEnabled:I

    .line 737
    :cond_1
    :goto_0
    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoverUIEnabled:I

    if-ne v5, v4, :cond_2

    if-eqz v1, :cond_2

    .line 738
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 739
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 740
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 741
    .local v3, "y":I
    const/16 v4, 0x9

    if-ne v0, v4, :cond_4

    .line 744
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->trackHoverEvent(Landroid/view/MotionEvent;)V

    .line 745
    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoveringLevel:I

    invoke-virtual {p0, v4, v2, v3}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onStartTrackingHover(III)V

    .line 760
    .end local v0    # "action":I
    .end local v2    # "x":I
    .end local v3    # "y":I
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    return v4

    .line 732
    :cond_3
    const/4 v5, -0x1

    iput v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoverUIEnabled:I

    goto :goto_0

    .line 747
    .restart local v0    # "action":I
    .restart local v2    # "x":I
    .restart local v3    # "y":I
    :cond_4
    const/4 v4, 0x7

    if-ne v0, v4, :cond_5

    .line 748
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->trackHoverEvent(Landroid/view/MotionEvent;)V

    .line 749
    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoveringLevel:I

    invoke-virtual {p0, v4, v2, v3}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onHoverChanged(III)V

    .line 752
    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mHoverPopupType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 753
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/HoverPopupWindow;->setHoveringPoint(II)V

    .line 754
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/HoverPopupWindow;->updateHoverPopup()V

    goto :goto_1

    .line 756
    :cond_5
    const/16 v4, 0xa

    if-ne v0, v4, :cond_2

    .line 757
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onStopTrackingHover()V

    goto :goto_1
.end method

.method onKeyChange()V
    .locals 0

    .prologue
    .line 681
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 685
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgress()I

    move-result v0

    .line 687
    .local v0, "progress":I
    packed-switch p1, :pswitch_data_0

    .line 719
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_0
    return v1

    .line 689
    :pswitch_0
    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 691
    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    sub-int v2, v0, v2

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setProgress(IZ)V

    .line 692
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onKeyChange()V

    goto :goto_0

    .line 696
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getMax()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 698
    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setProgress(IZ)V

    .line 699
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onKeyChange()V

    goto :goto_0

    .line 703
    :pswitch_2
    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v2

    if-eqz v2, :cond_0

    .line 705
    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    sub-int v2, v0, v2

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setProgress(IZ)V

    .line 706
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onKeyChange()V

    goto :goto_0

    .line 710
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getMax()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v2

    if-eqz v2, :cond_0

    .line 712
    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setProgress(IZ)V

    .line 713
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onKeyChange()V

    goto :goto_0

    .line 687
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected declared-synchronized onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 535
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getCurrentDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 537
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_1

    const/4 v3, 0x0

    .line 538
    .local v3, "thumbHeight":I
    :goto_0
    const/4 v2, 0x0

    .line 539
    .local v2, "dw":I
    const/4 v1, 0x0

    .line 540
    .local v1, "dh":I
    if-eqz v0, :cond_0

    .line 541
    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mMinWidth:I

    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mMaxWidth:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 542
    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mMinHeight:I

    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mMaxHeight:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 543
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 545
    :cond_0
    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingRight:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 546
    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingBottom:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 548
    invoke-static {v2, p1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->resolveSize(II)I

    move-result v4

    invoke-static {v1, p2}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->resolveSize(II)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    monitor-exit p0

    return-void

    .line 537
    .end local v1    # "dh":I
    .end local v2    # "dw":I
    .end local v3    # "thumbHeight":I
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    goto :goto_0

    .line 535
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method onProgressRefresh(FZ)V
    .locals 3
    .param p1, "scale"    # F
    .param p2, "fromUser"    # Z

    .prologue
    const/high16 v2, -0x80000000

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    .line 337
    .local v0, "thumb":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v1

    if-nez v1, :cond_1

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressWidth()I

    move-result v1

    invoke-direct {p0, v1, v0, p1, v2}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setThumbPos(ILandroid/graphics/drawable/Drawable;FI)V

    .line 348
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->invalidate()V

    .line 350
    :cond_0
    return-void

    .line 341
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHeight()I

    move-result v1

    invoke-direct {p0, v1, v0, p1, v2}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setThumbVerticalPos(ILandroid/graphics/drawable/Drawable;FI)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 357
    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    .line 358
    .local v3, "thumb":Landroid/graphics/drawable/Drawable;
    if-nez v3, :cond_1

    const/4 v4, 0x0

    .line 359
    .local v4, "thumbHeight":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getMax()I

    move-result v1

    .line 360
    .local v1, "max":I
    if-lez v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgress()I

    move-result v5

    int-to-float v5, v5

    int-to-float v6, v1

    div-float v2, v5, v6

    .line 361
    .local v2, "scale":F
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressWidth()I

    move-result p1

    .line 363
    if-eqz v3, :cond_0

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getProgressBarMode()I

    move-result v5

    if-nez v5, :cond_3

    .line 365
    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingTop:I

    sub-int v5, p2, v5

    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingBottom:I

    sub-int/2addr v5, v6

    sub-int/2addr v5, v4

    div-int/lit8 v0, v5, 0x2

    .line 366
    .local v0, "gap":I
    invoke-direct {p0, p1, v3, v2, v0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setThumbPos(ILandroid/graphics/drawable/Drawable;FI)V

    .line 372
    .end local v0    # "gap":I
    :cond_0
    :goto_2
    return-void

    .line 358
    .end local v1    # "max":I
    .end local v2    # "scale":F
    .end local v4    # "thumbHeight":I
    :cond_1
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    goto :goto_0

    .line 360
    .restart local v1    # "max":I
    .restart local v4    # "thumbHeight":I
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 368
    .restart local v2    # "scale":F
    :cond_3
    iget v5, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingLeft:I

    sub-int v5, p1, v5

    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mPaddingRight:I

    sub-int/2addr v5, v6

    div-int/lit8 v0, v5, 0x2

    .line 369
    .restart local v0    # "gap":I
    invoke-direct {p0, p2, v3, v2, v0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setThumbVerticalPos(ILandroid/graphics/drawable/Drawable;FI)V

    goto :goto_2
.end method

.method onStartTrackingHover(III)V
    .locals 0
    .param p1, "hoverLevel"    # I
    .param p2, "posX"    # I
    .param p3, "posY"    # I

    .prologue
    .line 764
    return-void
.end method

.method onStartTrackingTouch()V
    .locals 0

    .prologue
    .line 668
    return-void
.end method

.method onStopTrackingHover()V
    .locals 0

    .prologue
    .line 767
    return-void
.end method

.method onStopTrackingTouch()V
    .locals 0

    .prologue
    .line 675
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 553
    iget-boolean v2, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsUserSeekable:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 591
    :goto_0
    return v0

    .line 557
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 559
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setPressed(Z)V

    .line 560
    iput-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsTouch:Z

    .line 561
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onStartTrackingTouch()V

    .line 562
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    .line 563
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->invalidate()V

    goto :goto_0

    .line 567
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    .line 568
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->attemptClaimDrag()V

    goto :goto_0

    .line 572
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->trackTouchEvent(Landroid/view/MotionEvent;)V

    .line 573
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onStopTrackingTouch()V

    .line 574
    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setPressed(Z)V

    .line 575
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsTouch:Z

    .line 579
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->invalidate()V

    goto :goto_0

    .line 583
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onStopTrackingTouch()V

    .line 584
    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setPressed(Z)V

    .line 585
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mIsTouch:Z

    .line 586
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->invalidate()V

    goto :goto_0

    .line 557
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setHoverPopupType(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 829
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->isHoveringUIEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 840
    :goto_0
    return-void

    .line 832
    :cond_0
    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    .line 833
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    const/16 v2, 0x3231

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 834
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getMeasuredHeight()I

    move-result v0

    .line 835
    .local v0, "contentHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    const/4 v2, 0x0

    div-int/lit8 v3, v0, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 836
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    const/16 v2, 0xc8

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 839
    .end local v0    # "contentHeight":I
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setHoverPopupType(I)V

    goto :goto_0
.end method

.method public setKeyProgressIncrement(I)V
    .locals 0
    .param p1, "increment"    # I

    .prologue
    .line 212
    if-gez p1, :cond_0

    neg-int p1, p1

    .end local p1    # "increment":I
    :cond_0
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    .line 213
    return-void
.end method

.method public declared-synchronized setMax(I)V
    .locals 3
    .param p1, "max"    # I

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setMax(I)V

    .line 234
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getMax()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mKeyProgressIncrement:I

    div-int/2addr v0, v1

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    .line 237
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->getMax()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x41a00000    # 20.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->setKeyProgressIncrement(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    :cond_1
    monitor-exit p0

    return-void

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSeekThumbFontColor(I)V
    .locals 0
    .param p1, "resourceID"    # I

    .prologue
    .line 272
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontColor:I

    .line 273
    return-void
.end method

.method public setSeekThumbFontEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 293
    iput-boolean p1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontEnable:Z

    .line 294
    return-void
.end method

.method public setSeekThumbFontSize(I)V
    .locals 2
    .param p1, "fontSize"    # I

    .prologue
    .line 248
    if-gez p1, :cond_0

    .line 249
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->TWSEEKTHUMB_DEFAULT_FONT_SIZE:F

    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontSize:F

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_0
    int-to-float v0, p1

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mSeekThumbFontSize:F

    goto :goto_0
.end method

.method public setThumb(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "thumb"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 169
    if-eqz p1, :cond_0

    .line 170
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 175
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbOffset:I

    .line 177
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->invalidate()V

    .line 179
    return-void
.end method

.method public setThumbOffset(I)V
    .locals 0
    .param p1, "thumbOffset"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumbOffset:I

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->invalidate()V

    .line 200
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "who"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->mThumb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
