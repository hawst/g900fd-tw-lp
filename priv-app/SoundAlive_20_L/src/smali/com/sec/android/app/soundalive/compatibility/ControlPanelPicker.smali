.class public Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;
.super Lcom/android/internal/app/AlertActivity;
.source "ControlPanelPicker.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "ControlPanelPicker"


# instance fields
.field private c:Landroid/database/MatrixCursor;

.field private mItemClickListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    .line 81
    new-instance v0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker$1;-><init>(Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;)V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->mItemClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;)Lcom/android/internal/app/AlertController$AlertParams;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    .line 94
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 96
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Service;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 97
    .local v1, "updateIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iget-object v0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    .line 98
    .local v0, "c":Landroid/database/Cursor;
    iget-object v2, p0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    iget v2, v2, Lcom/android/internal/app/AlertController$AlertParams;->mCheckedItem:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 99
    const-string v2, "defPackage"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    const-string v2, "defName"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 102
    const-string v2, "ControlPanelPicker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onClick() defPackage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  defName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "updateIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 18
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super/range {p0 .. p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const-string v14, "ControlPanelPicker"

    const-string v15, "onCreate()"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const/4 v14, 0x4

    new-array v2, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "_id"

    aput-object v15, v2, v14

    const/4 v14, 0x1

    const-string v15, "title"

    aput-object v15, v2, v14

    const/4 v14, 0x2

    const-string v15, "package"

    aput-object v15, v2, v14

    const/4 v14, 0x3

    const-string v15, "name"

    aput-object v15, v2, v14

    .line 35
    .local v2, "cols":[Ljava/lang/String;
    new-instance v14, Landroid/database/MatrixCursor;

    invoke-direct {v14, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->c:Landroid/database/MatrixCursor;

    .line 37
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 38
    .local v9, "pmgr":Landroid/content/pm/PackageManager;
    if-nez v9, :cond_0

    .line 39
    const-string v14, "ControlPanelPicker"

    const-string v15, "onCreate() failed to get getPackageManager()"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :goto_0
    return-void

    .line 42
    :cond_0
    new-instance v5, Landroid/content/Intent;

    const-string v14, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v5, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 43
    .local v5, "i":Landroid/content/Intent;
    const/16 v14, 0x200

    invoke-virtual {v9, v5, v14}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v10

    .line 44
    .local v10, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v13

    .line 46
    .local v13, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v14, "defaultpanelpackage"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 47
    .local v12, "savedDefPackage":Ljava/lang/String;
    const-string v14, "defaultpanelname"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 48
    .local v11, "savedDefName":Ljava/lang/String;
    const/4 v1, -0x1

    .line 49
    .local v1, "cnt":I
    const/4 v3, 0x0

    .line 51
    .local v3, "defpanelidx":I
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 52
    .local v4, "foo":Landroid/content/pm/ResolveInfo;
    iget-object v14, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-class v15, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Redirector;

    invoke-virtual {v15}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 55
    iget-object v14, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v9, v14}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 56
    .local v7, "name":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->c:Landroid/database/MatrixCursor;

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v7, v15, v16

    const/16 v16, 0x2

    iget-object v0, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x3

    iget-object v0, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 57
    add-int/lit8 v1, v1, 0x1

    .line 58
    iget-object v14, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    iget-object v14, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    iget-object v14, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v14, v14, Landroid/content/pm/ActivityInfo;->enabled:Z

    if-eqz v14, :cond_1

    .line 62
    move v3, v1

    .line 63
    const-string v14, "ControlPanelPicker"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onCreate() savedDefPackage : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " savedDefName : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "  defpanelidx : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 67
    .end local v4    # "foo":Landroid/content/pm/ResolveInfo;
    .end local v7    # "name":Ljava/lang/CharSequence;
    :cond_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    .line 68
    .local v8, "p":Lcom/android/internal/app/AlertController$AlertParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->c:Landroid/database/MatrixCursor;

    iput-object v14, v8, Lcom/android/internal/app/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    .line 69
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->mItemClickListener:Landroid/content/DialogInterface$OnClickListener;

    iput-object v14, v8, Lcom/android/internal/app/AlertController$AlertParams;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 70
    const-string v14, "title"

    iput-object v14, v8, Lcom/android/internal/app/AlertController$AlertParams;->mLabelColumn:Ljava/lang/String;

    .line 71
    const/4 v14, 0x1

    iput-boolean v14, v8, Lcom/android/internal/app/AlertController$AlertParams;->mIsSingleChoice:Z

    .line 72
    const v14, 0x7f070024

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v8, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    .line 73
    move-object/from16 v0, p0

    iput-object v0, v8, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    .line 74
    const v14, 0x7f070005

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v8, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    .line 75
    const v14, 0x7f070025

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v8, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    .line 76
    iput v3, v8, Lcom/android/internal/app/AlertController$AlertParams;->mCheckedItem:I

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/compatibility/ControlPanelPicker;->setupAlert()V

    goto/16 :goto_0
.end method
