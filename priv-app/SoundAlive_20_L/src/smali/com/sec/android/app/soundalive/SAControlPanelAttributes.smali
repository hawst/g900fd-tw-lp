.class public Lcom/sec/android/app/soundalive/SAControlPanelAttributes;
.super Ljava/lang/Object;
.source "SAControlPanelAttributes.java"


# static fields
.field public static final ACTION_3D_CHANGED:Ljava/lang/String; = "internal_SA_3D_changed"

.field public static final ACTION_ADAPT_UI_UPDATE:Ljava/lang/String; = "internal_SA_Adapt_UI_changed"

.field public static final ACTION_ADAPT_UPDATE:Ljava/lang/String; = "internal_SA_Adapt_changed"

.field public static final ACTION_AUDIO_PATH_RECONFIG:Ljava/lang/String; = "internal_SA_AudioPath"

.field public static final ACTION_AUTO_UI_UPDATE:Ljava/lang/String; = "internal_SA_Auto_UI_changed"

.field public static final ACTION_AUTO_UPDATE:Ljava/lang/String; = "internal_SA_Auto_changed"

.field public static final ACTION_BASS_CHANGED:Ljava/lang/String; = "internal_SA_Bass_changed"

.field public static final ACTION_BROADCAST_SA_INFO:Ljava/lang/String; = "com.sec.android.app.SA_GENRE_INFO"

.field public static final ACTION_CLARITY_CHANGED:Ljava/lang/String; = "internal_SA_Clarity_changed"

.field public static final ACTION_CONTEXT_AWARE_MUSIC_INFO:Ljava/lang/String; = "android.intent.action.CONTEXT_AWARE_MUSIC_INFO"

.field public static final ACTION_EQ_CHANGED:Ljava/lang/String; = "internal_SA_EQ_changed"

.field public static final ACTION_PLAYSTATE_CHANGED:Ljava/lang/String; = "com.android.music.playstatechanged"

.field public static final ACTION_PRESET_CHANGED:Ljava/lang/String; = "internal_SA_Preset_changed"

.field public static final ACTION_REQUEST_GENRE:Ljava/lang/String; = "com.sec.android.app.safx.ACTION_REQUEST_GENRE"

.field public static final ACTION_REQUEST_SA_STOP:Ljava/lang/String; = "com.android.bluetooth.ACTION_SOUNDALIVE_CHANGED"

.field public static final ACTION_SA_DUPLICATED_USED:Ljava/lang/String; = "com.sec.android.app.soundalive.ACTION_SA_DUPLICATED_USED"

.field public static final ACTION_SQUARE_CHANGED:Ljava/lang/String; = "internal_SA_Square_changed"

.field public static final ACTION_UI_UPDATE:Ljava/lang/String; = "internal_SA_Advanced_UI_changed"

.field public static final AudioPath:[Ljava/lang/String;

.field public static final EXTRA_SA_3D:Ljava/lang/String; = "SA_Strength_3D"

.field public static final EXTRA_SA_ADAPT_STATE:Ljava/lang/String; = "SA_AdaptSound_State"

.field public static final EXTRA_SA_AUTO_STATE:Ljava/lang/String; = "SA_Auto_State"

.field public static final EXTRA_SA_BASS:Ljava/lang/String; = "SA_Strength_BASS"

.field public static final EXTRA_SA_CLARITY:Ljava/lang/String; = "SA_Strength_Clarity"

.field public static final EXTRA_SA_EQ_ARRAY:Ljava/lang/String; = "SA_EQ_Array"

.field public static final EXTRA_SA_INFO_GENRE:Ljava/lang/String; = "genreINFO"

.field public static final EXTRA_SA_INFO_PRESET:Ljava/lang/String; = "presetINFO"

.field public static final EXTRA_SA_PRESET:Ljava/lang/String; = "SA_Preset"

.field public static final EXTRA_SA_SQUARE:Ljava/lang/String; = "SA_SquarePosition"

.field public static final EXTRA_SA_STRENTH_ARRAY:Ljava/lang/String; = "SA_Strength_Array"

.field public static final INVALID_AUDIOSESSION:I = -0x1

.field public static final PREFERENCES_SA:Ljava/lang/String; = "SoundAlive_Settings"

.field public static final PREFERENCES_SA__ADAPT_STATE:Ljava/lang/String; = "SA_ADAPT_STATE"

.field public static final PREFERENCES_SA__AUDIOSESSION:Ljava/lang/String; = "AUDIOSESSION_ID"

.field public static final PREFERENCES_SA__AUDIO_PATH:Ljava/lang/String; = "SA_CURRENT_PATH_INDEX"

.field public static final PREFERENCES_SA__AUTO_STATE:Ljava/lang/String; = "SA_AUTO_STATE"

.field public static final PREFERENCES_SA__EQ_INDEX:Ljava/lang/String; = "SA_EQ_INDEX_"

.field public static final PREFERENCES_SA__EQ_INDEX_ARRAY:Ljava/lang/String; = "SA_EQ_INDEX_ARRAY"

.field public static final PREFERENCES_SA__GENRE_INDEX:Ljava/lang/String; = "SA_GENRE_INDEX"

.field public static final PREFERENCES_SA__IS_PLAYING:Ljava/lang/String; = "IS_PLAYING"

.field public static final PREFERENCES_SA__PACKAGENAME:Ljava/lang/String; = "CURRENT_PACKAGENAME"

.field public static final PREFERENCES_SA__PRESET_INDEX:Ljava/lang/String; = "SA_PRESET_INDEX"

.field public static final PREFERENCES_SA__RCV_POINT:Ljava/lang/String; = "WHICH_RCV_HAS_CONTRL"

.field public static final PREFERENCES_SA__REQUIRED_RENEW_ATCIVITY:Ljava/lang/String; = "SA_ACTIVITY_RENEW"

.field public static final PREFERENCES_SA__SQUARE_INDEX:Ljava/lang/String; = "SA_SQUARE_INDEX"

.field public static final PREFERENCES_SA__STRENGTH_INDEX:Ljava/lang/String; = "SA_STRENGH_INDEX_"

.field public static final PREFERENCES_SA__STRENGTH_INDEX_ARRAY:Ljava/lang/String; = "SA_STRENGH_INDEX_ARRAY"

.field public static final PREFERENCES_SA__SVC_STATE:Ljava/lang/String; = "SA_SERVICE_STATE"

.field public static final PREFERENCES_SA__TAB_POS:Ljava/lang/String; = "SA_TAB_INDEX"

.field public static final RECEIVER_ID_1:I = 0x89

.field public static final RECEIVER_ID_2:I = 0x8a

.field public static final SA_MUSIC_IS_NOT_PLAYING:I = 0x0

.field public static final SA_MUSIC_IS_PLAYING:I = 0x1

.field public static final SA_NOT_ABAILABLE:I = -0x1

.field public static final SA_PATH_BT:I = 0x2

.field public static final SA_PATH_EAR:I = 0x1

.field public static final SA_PATH_HDMI:I = 0x4

.field public static final SA_PATH_LINE_OUT:I = 0x3

.field public static final SA_PATH_MUM:I = 0x6

.field public static final SA_PATH_REMOTE_SUBMIX:I = 0x5

.field public static final SA_PATH_SPK:I = 0x0

.field public static final SERVICE_STATE_OPEN:I = 0x9

.field public static final SERVICE_STATE_PAUSE:I = 0xb

.field public static final SERVICE_STATE_RESUME:I = 0xa

.field public static final SERVICE_STATE_STOP:I = 0xc

.field public static final SQ_From_Lib_Map_EAR:[[B

.field public static final SQ_From_Lib_Map_MONO_SPK:[[B

.field public static final TAB_POS_ADVANCED:I = 0x1

.field public static final TAB_POS_BASIC:I

.field public static debug_level_high:Z

.field public static debug_level_low:Z

.field public static debug_level_mid:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/16 v3, 0xa

    .line 8
    sput-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    .line 9
    sput-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    .line 10
    sput-boolean v5, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_high:Z

    .line 107
    const/16 v0, 0x19

    new-array v0, v0, [[B

    new-array v1, v3, [B

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    new-array v1, v3, [B

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    new-array v1, v3, [B

    fill-array-data v1, :array_2

    aput-object v1, v0, v6

    new-array v1, v3, [B

    fill-array-data v1, :array_3

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v3, [B

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [B

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [B

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [B

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [B

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [B

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    new-array v1, v3, [B

    fill-array-data v1, :array_a

    aput-object v1, v0, v3

    const/16 v1, 0xb

    new-array v2, v3, [B

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [B

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [B

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [B

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [B

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [B

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [B

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [B

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [B

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [B

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [B

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [B

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [B

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [B

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    .line 139
    const/16 v0, 0x19

    new-array v0, v0, [[B

    new-array v1, v3, [B

    fill-array-data v1, :array_19

    aput-object v1, v0, v5

    new-array v1, v3, [B

    fill-array-data v1, :array_1a

    aput-object v1, v0, v4

    new-array v1, v3, [B

    fill-array-data v1, :array_1b

    aput-object v1, v0, v6

    new-array v1, v3, [B

    fill-array-data v1, :array_1c

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v3, [B

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [B

    fill-array-data v2, :array_1e

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [B

    fill-array-data v2, :array_1f

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [B

    fill-array-data v2, :array_20

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [B

    fill-array-data v2, :array_21

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [B

    fill-array-data v2, :array_22

    aput-object v2, v0, v1

    new-array v1, v3, [B

    fill-array-data v1, :array_23

    aput-object v1, v0, v3

    const/16 v1, 0xb

    new-array v2, v3, [B

    fill-array-data v2, :array_24

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [B

    fill-array-data v2, :array_25

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [B

    fill-array-data v2, :array_26

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [B

    fill-array-data v2, :array_27

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [B

    fill-array-data v2, :array_28

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [B

    fill-array-data v2, :array_29

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [B

    fill-array-data v2, :array_2a

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [B

    fill-array-data v2, :array_2b

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [B

    fill-array-data v2, :array_2c

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [B

    fill-array-data v2, :array_2d

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [B

    fill-array-data v2, :array_2e

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [B

    fill-array-data v2, :array_2f

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [B

    fill-array-data v2, :array_30

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [B

    fill-array-data v2, :array_31

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_MONO_SPK:[[B

    .line 216
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SPK__"

    aput-object v1, v0, v5

    const-string v1, "EAR__"

    aput-object v1, v0, v4

    const-string v1, "BT__"

    aput-object v1, v0, v6

    const-string v1, "LineOut__"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "HDMI__"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "UNKNOWN__"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    return-void

    .line 107
    :array_0
    .array-data 1
        -0x4t
        -0x2t
        -0x3t
        -0x1t
        0x3t
        0x2t
        0x7t
        0x1t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        -0x4t
        -0x2t
        -0x2t
        0x0t
        0x3t
        0x0t
        0x8t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_2
    .array-data 1
        -0x5t
        -0x4t
        0x0t
        0x0t
        0x1t
        0x0t
        0x6t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_3
    .array-data 1
        -0x7t
        -0x6t
        0x0t
        0x3t
        0x4t
        0x0t
        0x2t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_4
    .array-data 1
        -0x7t
        -0x6t
        0x0t
        0x2t
        0x5t
        0x0t
        0x2t
        0x0t
        0x0t
        0x1t
    .end array-data

    nop

    :array_5
    .array-data 1
        -0x1t
        0x0t
        -0x3t
        -0x1t
        0x3t
        0x2t
        0x4t
        0x1t
        0x0t
        0x0t
    .end array-data

    nop

    :array_6
    .array-data 1
        -0x1t
        0x0t
        -0x2t
        0x0t
        0x3t
        0x0t
        0x5t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_7
    .array-data 1
        -0x2t
        -0x2t
        0x0t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_8
    .array-data 1
        -0x4t
        -0x4t
        0x0t
        0x3t
        0x4t
        0x0t
        -0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_9
    .array-data 1
        -0x4t
        -0x4t
        0x0t
        0x2t
        0x5t
        0x0t
        -0x1t
        0x0t
        0x0t
        0x1t
    .end array-data

    nop

    :array_a
    .array-data 1
        0x1t
        0x2t
        -0x3t
        -0x1t
        0x2t
        0x2t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    nop

    :array_b
    .array-data 1
        0x1t
        0x2t
        -0x2t
        0x0t
        0x2t
        0x0t
        0x2t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_c
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_d
    .array-data 1
        -0x2t
        -0x2t
        0x0t
        0x3t
        0x3t
        0x0t
        -0x4t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_e
    .array-data 1
        -0x2t
        -0x2t
        0x0t
        0x2t
        0x4t
        0x0t
        -0x4t
        0x0t
        0x0t
        0x1t
    .end array-data

    nop

    :array_f
    .array-data 1
        0x3t
        0x4t
        -0x3t
        -0x1t
        0x1t
        0x2t
        -0x2t
        0x1t
        0x0t
        0x0t
    .end array-data

    nop

    :array_10
    .array-data 1
        0x3t
        0x4t
        -0x2t
        0x0t
        0x1t
        0x0t
        -0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_11
    .array-data 1
        0x2t
        0x2t
        0x0t
        0x0t
        -0x1t
        0x0t
        -0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_12
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x3t
        0x2t
        0x0t
        -0x7t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_13
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x2t
        0x3t
        0x0t
        -0x7t
        0x0t
        0x0t
        0x1t
    .end array-data

    nop

    :array_14
    .array-data 1
        0x3t
        0x4t
        -0x3t
        -0x1t
        0x1t
        0x2t
        -0x5t
        0x1t
        0x1t
        0x0t
    .end array-data

    nop

    :array_15
    .array-data 1
        0x3t
        0x4t
        -0x2t
        0x0t
        0x1t
        0x0t
        -0x4t
        0x0t
        0x1t
        0x0t
    .end array-data

    nop

    :array_16
    .array-data 1
        0x2t
        0x2t
        0x0t
        0x0t
        -0x1t
        0x0t
        -0x6t
        0x0t
        0x1t
        0x0t
    .end array-data

    nop

    :array_17
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x3t
        0x2t
        0x0t
        -0xat
        0x0t
        0x1t
        0x0t
    .end array-data

    nop

    :array_18
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x2t
        0x3t
        0x0t
        -0xat
        0x0t
        0x1t
        0x1t
    .end array-data

    .line 139
    nop

    :array_19
    .array-data 1
        0x0t
        0x0t
        -0x3t
        -0x9t
        -0x1t
        0x3t
        0x9t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1a
    .array-data 1
        0x0t
        0x0t
        -0x4t
        -0x5t
        0x0t
        0x2t
        0x8t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1b
    .array-data 1
        0x0t
        0x0t
        -0x6t
        0x0t
        0x1t
        0x0t
        0x6t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1c
    .array-data 1
        0x0t
        0x0t
        -0x6t
        0x3t
        0x4t
        -0x2t
        0x2t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1d
    .array-data 1
        0x0t
        0x0t
        -0x6t
        0x3t
        0x5t
        -0x3t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    nop

    :array_1e
    .array-data 1
        0x0t
        0x0t
        0x0t
        -0x9t
        -0x1t
        0x3t
        0x6t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1f
    .array-data 1
        0x0t
        0x0t
        -0x1t
        -0x5t
        0x0t
        0x2t
        0x5t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_20
    .array-data 1
        0x0t
        0x0t
        -0x3t
        0x0t
        0x1t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_21
    .array-data 1
        0x0t
        0x0t
        -0x3t
        0x2t
        0x4t
        -0x2t
        -0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_22
    .array-data 1
        0x0t
        0x0t
        -0x3t
        0x3t
        0x5t
        -0x3t
        -0x3t
        0x0t
        0x0t
        0x1t
    .end array-data

    nop

    :array_23
    .array-data 1
        0x0t
        0x0t
        0x3t
        -0x9t
        -0x2t
        0x3t
        0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_24
    .array-data 1
        0x0t
        0x0t
        0x2t
        -0x5t
        -0x1t
        0x2t
        0x2t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_25
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_26
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x2t
        0x3t
        -0x2t
        -0x4t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_27
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x3t
        0x4t
        -0x3t
        -0x6t
        0x0t
        0x0t
        0x1t
    .end array-data

    nop

    :array_28
    .array-data 1
        0x0t
        0x0t
        0x6t
        -0x9t
        -0x3t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_29
    .array-data 1
        0x0t
        0x0t
        0x5t
        -0x5t
        -0x2t
        0x2t
        -0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_2a
    .array-data 1
        0x0t
        0x0t
        0x3t
        0x0t
        -0x1t
        0x0t
        -0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_2b
    .array-data 1
        0x0t
        0x0t
        0x3t
        0x2t
        0x2t
        -0x2t
        -0x7t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_2c
    .array-data 1
        0x0t
        0x0t
        0x3t
        0x3t
        0x3t
        -0x3t
        -0x9t
        0x0t
        0x0t
        0x1t
    .end array-data

    nop

    :array_2d
    .array-data 1
        0x0t
        0x0t
        0x9t
        -0x9t
        -0x3t
        0x3t
        -0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_2e
    .array-data 1
        0x0t
        0x0t
        0x8t
        -0x5t
        -0x2t
        0x2t
        -0x4t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_2f
    .array-data 1
        0x0t
        0x0t
        0x8t
        0x0t
        -0x1t
        0x0t
        -0x6t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_30
    .array-data 1
        0x0t
        0x0t
        0x6t
        0x2t
        0x2t
        -0x2t
        -0xat
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_31
    .array-data 1
        0x0t
        0x0t
        0x6t
        0x3t
        0x3t
        -0x3t
        -0xct
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
