.class public Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
.super Landroid/widget/FrameLayout;
.source "TwSeekBarBubble.java"


# static fields
.field public static final BUBBLE_ALIGN_BOTTOM:I = 0x3

.field public static final BUBBLE_ALIGN_CENTER:I = 0x4

.field public static final BUBBLE_ALIGN_LEFT:I = 0x0

.field public static final BUBBLE_ALIGN_RIGHT:I = 0x1

.field public static final BUBBLE_ALIGN_TOP:I = 0x2

.field private static final TAG:Ljava/lang/String; = "TwSeekbarBubble"


# instance fields
.field private TWSEEKBARBUBBLE_DEFAULT_TEXT_SIZE:F

.field private final debug:Z

.field private mBubbleAlign:I

.field private mBubbleBackground:Landroid/graphics/drawable/Drawable;

.field private mBubbleDisplay:Z

.field private mBubbleDrawnRect:Landroid/graphics/Rect;

.field private mBubbleFontColor:I

.field private mBubbleFontSize:F

.field private mBubblePosLeft:I

.field private mBubblePosTop:I

.field private mBubbleText:Ljava/lang/CharSequence;

.field private mScale:F

.field private mTextPainter:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 106
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->debug:Z

    .line 82
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleAlign:I

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDisplay:Z

    .line 92
    const v0, 0x421551ec    # 37.33f

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->TWSEEKBARBUBBLE_DEFAULT_TEXT_SIZE:F

    .line 95
    const v0, 0x7f040018

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontColor:I

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mScale:F

    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->init()V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->debug:Z

    .line 82
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleAlign:I

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDisplay:Z

    .line 92
    const v0, 0x421551ec    # 37.33f

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->TWSEEKBARBUBBLE_DEFAULT_TEXT_SIZE:F

    .line 95
    const v0, 0x7f040018

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontColor:I

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mScale:F

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->init()V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    .line 136
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->debug:Z

    .line 82
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleAlign:I

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDisplay:Z

    .line 92
    const v0, 0x421551ec    # 37.33f

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->TWSEEKBARBUBBLE_DEFAULT_TEXT_SIZE:F

    .line 95
    const v0, 0x7f040018

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontColor:I

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mScale:F

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->init()V

    .line 138
    return-void
.end method

.method private calculateBubblePosition(Landroid/graphics/Point;)V
    .locals 10
    .param p1, "bubblePosition"    # Landroid/graphics/Point;

    .prologue
    .line 467
    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    .line 468
    .local v3, "dBubbleBg":Landroid/graphics/drawable/Drawable;
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 470
    .local v5, "rectText":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 471
    .local v4, "rectBubblePadding":Landroid/graphics/Rect;
    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 473
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 474
    .local v2, "bubbleWidth":I
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 476
    .local v1, "bubbleHeight":I
    iget v6, v4, Landroid/graphics/Rect;->left:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    sub-int v0, v2, v6

    .line 478
    .local v0, "bubbleContentWidth":I
    iget-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mTextPainter:Landroid/graphics/Paint;

    iget-object v7, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleText:Ljava/lang/CharSequence;

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleText:Ljava/lang/CharSequence;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    invoke-virtual {v6, v7, v8, v9, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 481
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 482
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 484
    :cond_0
    iget v6, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v0

    iget v7, v4, Landroid/graphics/Rect;->right:I

    add-int v2, v6, v7

    .line 486
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleAlign:I

    packed-switch v6, :pswitch_data_0

    .line 509
    :goto_0
    return-void

    .line 488
    :pswitch_0
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosLeft:I

    iput v6, p1, Landroid/graphics/Point;->x:I

    .line 489
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosTop:I

    div-int/lit8 v7, v1, 0x2

    sub-int/2addr v6, v7

    iput v6, p1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 492
    :pswitch_1
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosLeft:I

    sub-int/2addr v6, v2

    iput v6, p1, Landroid/graphics/Point;->x:I

    .line 493
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosTop:I

    div-int/lit8 v7, v1, 0x2

    sub-int/2addr v6, v7

    iput v6, p1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 496
    :pswitch_2
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosLeft:I

    div-int/lit8 v7, v2, 0x2

    sub-int/2addr v6, v7

    iput v6, p1, Landroid/graphics/Point;->x:I

    .line 497
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosTop:I

    iput v6, p1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 500
    :pswitch_3
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosLeft:I

    div-int/lit8 v7, v2, 0x2

    sub-int/2addr v6, v7

    iput v6, p1, Landroid/graphics/Point;->x:I

    .line 501
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosTop:I

    sub-int/2addr v6, v1

    iput v6, p1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 504
    :pswitch_4
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosLeft:I

    div-int/lit8 v7, v2, 0x2

    sub-int/2addr v6, v7

    iput v6, p1, Landroid/graphics/Point;->x:I

    .line 505
    iget v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosTop:I

    div-int/lit8 v7, v1, 0x2

    sub-int/2addr v6, v7

    iput v6, p1, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 486
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mScale:F

    .line 145
    invoke-virtual {p0, v2}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setClickable(Z)V

    .line 146
    invoke-virtual {p0, v2}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setFocusable(Z)V

    .line 148
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleText:Ljava/lang/CharSequence;

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    .line 152
    iput v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosLeft:I

    .line 153
    iput v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosTop:I

    .line 155
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    .line 157
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mTextPainter:Landroid/graphics/Paint;

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mTextPainter:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 160
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->TWSEEKBARBUBBLE_DEFAULT_TEXT_SIZE:F

    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontSize:F

    .line 161
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 313
    invoke-super/range {p0 .. p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 315
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDisplay:Z

    if-eqz v12, :cond_2

    .line 317
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    .line 318
    .local v6, "dBubbleBg":Landroid/graphics/drawable/Drawable;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 319
    .local v7, "rectBg":Landroid/graphics/Rect;
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    .line 324
    .local v9, "rectText":Landroid/graphics/Rect;
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 325
    .local v8, "rectBubblePadding":Landroid/graphics/Rect;
    invoke-virtual {v6, v8}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 327
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 328
    .local v5, "bubbleWidth":I
    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 330
    .local v3, "bubbleHeight":I
    iget v12, v8, Landroid/graphics/Rect;->left:I

    iget v13, v8, Landroid/graphics/Rect;->right:I

    add-int/2addr v12, v13

    sub-int v2, v5, v12

    .line 331
    .local v2, "bubbleContentWidth":I
    iget v12, v8, Landroid/graphics/Rect;->top:I

    iget v13, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v12, v13

    sub-int v1, v3, v12

    .line 333
    .local v1, "bubbleContentHeight":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mTextPainter:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontColor:I

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 334
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mTextPainter:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontSize:F

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 335
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mTextPainter:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleText:Ljava/lang/CharSequence;

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleText:Ljava/lang/CharSequence;

    invoke-interface {v15}, Ljava/lang/CharSequence;->length()I

    move-result v15

    invoke-virtual {v12, v13, v14, v15, v9}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 337
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v12

    if-ge v2, v12, :cond_0

    .line 338
    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 341
    :cond_0
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v12

    if-ge v1, v12, :cond_1

    .line 342
    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 345
    :cond_1
    iget v12, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v12, v2

    iget v13, v8, Landroid/graphics/Rect;->right:I

    add-int v5, v12, v13

    .line 349
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 350
    .local v4, "bubblePoint":Landroid/graphics/Point;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->calculateBubblePosition(Landroid/graphics/Point;)V

    .line 352
    iget v12, v4, Landroid/graphics/Point;->x:I

    iget v13, v4, Landroid/graphics/Point;->y:I

    iget v14, v4, Landroid/graphics/Point;->x:I

    add-int/2addr v14, v5

    iget v15, v4, Landroid/graphics/Point;->y:I

    add-int/2addr v15, v3

    invoke-virtual {v7, v12, v13, v14, v15}, Landroid/graphics/Rect;->set(IIII)V

    .line 354
    invoke-virtual {v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 355
    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 357
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v13, v4, Landroid/graphics/Point;->x:I

    iput v13, v12, Landroid/graphics/Rect;->left:I

    .line 358
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v13, v4, Landroid/graphics/Point;->x:I

    add-int/2addr v13, v5

    iput v13, v12, Landroid/graphics/Rect;->right:I

    .line 359
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v13, v4, Landroid/graphics/Point;->y:I

    iput v13, v12, Landroid/graphics/Rect;->top:I

    .line 360
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v13, v4, Landroid/graphics/Point;->y:I

    add-int/2addr v13, v3

    iput v13, v12, Landroid/graphics/Rect;->bottom:I

    .line 364
    iget v12, v4, Landroid/graphics/Point;->x:I

    iget v13, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v12, v13

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v13

    sub-int v13, v2, v13

    div-int/lit8 v13, v13, 0x2

    add-int/2addr v12, v13

    iget v13, v9, Landroid/graphics/Rect;->left:I

    sub-int v10, v12, v13

    .line 365
    .local v10, "textX":I
    iget v12, v4, Landroid/graphics/Point;->y:I

    iget v13, v8, Landroid/graphics/Rect;->top:I

    add-int/2addr v12, v13

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v13

    sub-int v13, v1, v13

    div-int/lit8 v13, v13, 0x2

    add-int/2addr v12, v13

    iget v13, v9, Landroid/graphics/Rect;->top:I

    sub-int v11, v12, v13

    .line 368
    .local v11, "textY":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleText:Ljava/lang/CharSequence;

    invoke-interface {v12}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    int-to-float v13, v10

    int-to-float v14, v11

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mTextPainter:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13, v14, v15}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 369
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 372
    .end local v1    # "bubbleContentHeight":I
    .end local v2    # "bubbleContentWidth":I
    .end local v3    # "bubbleHeight":I
    .end local v4    # "bubblePoint":Landroid/graphics/Point;
    .end local v5    # "bubbleWidth":I
    .end local v6    # "dBubbleBg":Landroid/graphics/drawable/Drawable;
    .end local v7    # "rectBg":Landroid/graphics/Rect;
    .end local v8    # "rectBubblePadding":Landroid/graphics/Rect;
    .end local v9    # "rectText":Landroid/graphics/Rect;
    .end local v10    # "textX":I
    .end local v11    # "textY":I
    :cond_2
    return-void
.end method

.method public getBubbleFontSize()F
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontSize:F

    return v0
.end method

.method public getBubbleHeight()I
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    return v0
.end method

.method public getBubbleWidth()I
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public hideBubble()V
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDisplay:Z

    .line 264
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 376
    const/4 v0, 0x0

    .line 378
    .local v0, "bResult":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 379
    .local v1, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 381
    .local v2, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 399
    :cond_0
    :goto_0
    return v0

    .line 383
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    float-to-int v4, v1

    float-to-int v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 386
    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    sget-object v4, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->PRESSED_STATE_SET:[I

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 388
    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->invalidate(IIII)V

    .line 392
    const/4 v0, 0x1

    goto :goto_0

    .line 381
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 404
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 405
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 407
    .local v1, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 446
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v2, 0x1

    return v2

    .line 409
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    float-to-int v3, v0

    float-to-int v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 412
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    sget-object v3, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->PRESSED_STATE_SET:[I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 414
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->invalidate(IIII)V

    goto :goto_0

    .line 433
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    sget-object v3, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->EMPTY_STATE_SET:[I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->invalidate(IIII)V

    goto :goto_0

    .line 439
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    sget-object v3, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->EMPTY_STATE_SET:[I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 441
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDrawnRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->invalidate(IIII)V

    goto :goto_0

    .line 407
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setBubbleAlign(I)V
    .locals 0
    .param p1, "bubbleAlign"    # I

    .prologue
    .line 249
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleAlign:I

    .line 250
    return-void
.end method

.method public setBubbleBackgroundDrawable(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubbleBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 186
    return-void
.end method

.method public setBubbleBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 173
    if-eqz p1, :cond_0

    .line 174
    iput-object p1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleBackground:Landroid/graphics/drawable/Drawable;

    .line 176
    :cond_0
    return-void
.end method

.method public setBubbleFontColor(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 308
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontColor:I

    .line 309
    return-void
.end method

.method public setBubbleFontSize(F)V
    .locals 2
    .param p1, "fontSize"    # F

    .prologue
    .line 286
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 287
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->TWSEEKBARBUBBLE_DEFAULT_TEXT_SIZE:F

    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mScale:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontSize:F

    .line 291
    :goto_0
    return-void

    .line 289
    :cond_0
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleFontSize:F

    goto :goto_0
.end method

.method public setBubblePosition(Landroid/view/View;II)V
    .locals 2
    .param p1, "offsetView"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I

    .prologue
    .line 225
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    .line 235
    :goto_0
    return-void

    .line 228
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 229
    .local v0, "rectOffset":Landroid/graphics/Rect;
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 230
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p2

    iput v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosLeft:I

    .line 231
    iget v1, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, p3

    iput v1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubblePosTop:I

    goto :goto_0
.end method

.method public setBubbleText(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubbleText(Ljava/lang/CharSequence;)V

    .line 206
    return-void
.end method

.method public setBubbleText(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleText:Ljava/lang/CharSequence;

    .line 196
    return-void
.end method

.method public showBubble()V
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->mBubbleDisplay:Z

    .line 257
    return-void
.end method
