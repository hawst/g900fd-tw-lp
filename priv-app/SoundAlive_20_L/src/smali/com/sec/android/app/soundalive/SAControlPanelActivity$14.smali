.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$14;
.super Ljava/lang/Object;
.source "SAControlPanelActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initLayout(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0

    .prologue
    .line 1782
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$14;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 1785
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$14;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 1786
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 1787
    const-string v0, "SAControlPanelActivity"

    const-string v1, "***** mAdvancedButton.setOnClickListener*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1788
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$14;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateBandLevel()V

    .line 1789
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$14;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateStrength()V
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    .line 1791
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$14;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSelectedTab(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V

    .line 1792
    return-void
.end method
