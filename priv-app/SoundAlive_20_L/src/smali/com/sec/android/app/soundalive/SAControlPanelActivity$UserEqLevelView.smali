.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;
.super Landroid/view/View;
.source "SAControlPanelActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserEqLevelView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2464
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .line 2465
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2466
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2471
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2473
    const/high16 v8, 0x42540000    # 53.0f

    .line 2476
    .local v8, "linePosX":F
    const/high16 v12, 0x42a40000    # 82.0f

    .line 2477
    .local v12, "paddingTopPortrait":F
    const/high16 v11, 0x42960000    # 75.0f

    .line 2479
    .local v11, "paddingTopHorizontal":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2480
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mDbLinePaddingTop:I
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2482
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2485
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getScreenRawWidth()I
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v13

    .line 2486
    .local v13, "screenRawidth":I
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isPortrait()Z
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v7

    .line 2487
    .local v7, "isPortrait":Z
    if-eqz v7, :cond_0

    const/16 v0, 0x5a0

    if-ge v13, v0, :cond_1

    :cond_0
    if-nez v7, :cond_2

    const/16 v0, 0xa00

    if-lt v13, v0, :cond_2

    .line 2489
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x42180000    # 38.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2490
    const/high16 v8, 0x43b40000    # 360.0f

    .line 2491
    const/high16 v10, 0x40400000    # 3.0f

    .line 2492
    .local v10, "offsetLinePosY":F
    const/high16 v12, 0x42e20000    # 113.0f

    .line 2493
    const/high16 v11, 0x42d40000    # 106.0f

    .line 2494
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea76c8b4395810L    # 0.827

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2495
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea666666666666L    # 0.825

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2557
    :goto_0
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/16 v0, 0xa

    if-gt v6, v0, :cond_18

    .line 2558
    if-eqz v7, :cond_17

    .line 2559
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSeekBarHeight:I
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v0

    mul-int/2addr v0, v6

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    add-float v9, v0, v12

    .line 2561
    .local v9, "linePosY":F
    const/high16 v1, 0x41200000    # 10.0f

    add-float v2, v9, v10

    const v0, 0x44598000    # 870.0f

    add-float v3, v8, v0

    add-float v4, v9, v10

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2557
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2496
    .end local v6    # "i":I
    .end local v9    # "linePosY":F
    .end local v10    # "offsetLinePosY":F
    :cond_2
    if-eqz v7, :cond_3

    const/16 v0, 0x438

    if-ge v13, v0, :cond_4

    :cond_3
    if-nez v7, :cond_5

    const/16 v0, 0x780

    if-lt v13, v0, :cond_5

    .line 2497
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x42180000    # 38.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2498
    const/high16 v8, 0x42860000    # 67.0f

    .line 2499
    const/high16 v10, 0x40400000    # 3.0f

    .line 2500
    .restart local v10    # "offsetLinePosY":F
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2501
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea6e978d4fdf3bL    # 0.826

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto :goto_0

    .line 2502
    .end local v10    # "offsetLinePosY":F
    :cond_5
    if-eqz v7, :cond_6

    const/16 v0, 0x2d0

    if-ge v13, v0, :cond_7

    :cond_6
    if-nez v7, :cond_9

    const/16 v0, 0x500

    if-lt v13, v0, :cond_9

    .line 2503
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2504
    const/high16 v8, -0x3c8b0000    # -245.0f

    .line 2505
    const/high16 v10, 0x40400000    # 3.0f

    .line 2506
    .restart local v10    # "offsetLinePosY":F
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getDensity()I
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v0

    const/16 v1, 0x140

    if-ne v0, v1, :cond_8

    .line 2508
    const/high16 v12, 0x42540000    # 53.0f

    .line 2509
    const/high16 v11, 0x42480000    # 50.0f

    .line 2510
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2511
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea5604189374bcL    # 0.823

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2513
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea2d0e56041893L    # 0.818

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2514
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fe62d0e56041893L    # 0.693

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2516
    .end local v10    # "offsetLinePosY":F
    :cond_9
    if-eqz v7, :cond_a

    const/16 v0, 0x21c

    if-gt v13, v0, :cond_b

    :cond_a
    if-nez v7, :cond_d

    const/16 v0, 0x3c0

    if-le v13, v0, :cond_d

    .line 2517
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2518
    const/high16 v8, 0x42200000    # 40.0f

    .line 2519
    const/high16 v10, 0x40400000    # 3.0f

    .line 2520
    .restart local v10    # "offsetLinePosY":F
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getDensity()I
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v0

    const/16 v1, 0xf0

    if-ne v0, v1, :cond_c

    .line 2522
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3febc6a7ef9db22dL    # 0.868

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2523
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fe9b22d0e560419L    # 0.803

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2525
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea2d0e56041893L    # 0.818

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2526
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fe62d0e56041893L    # 0.693

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2529
    .end local v10    # "offsetLinePosY":F
    :cond_d
    if-eqz v7, :cond_e

    const/16 v0, 0x1e0

    if-gt v13, v0, :cond_f

    :cond_e
    if-nez v7, :cond_10

    const/16 v0, 0x320

    if-le v13, v0, :cond_10

    .line 2530
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2531
    const/high16 v8, 0x42200000    # 40.0f

    .line 2532
    const/high16 v12, 0x42240000    # 41.0f

    .line 2533
    const/high16 v11, 0x42140000    # 37.0f

    .line 2534
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2535
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fea45a1cac08312L    # 0.821

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2554
    :goto_3
    const/high16 v10, 0x40000000    # 2.0f

    .restart local v10    # "offsetLinePosY":F
    goto/16 :goto_0

    .line 2536
    .end local v10    # "offsetLinePosY":F
    :cond_10
    if-eqz v7, :cond_11

    const/16 v0, 0x140

    if-gt v13, v0, :cond_12

    :cond_11
    if-nez v7, :cond_13

    const/16 v0, 0x1e0

    if-le v13, v0, :cond_13

    .line 2537
    :cond_12
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2538
    const/high16 v8, 0x42200000    # 40.0f

    .line 2539
    const/high16 v12, 0x41f00000    # 30.0f

    .line 2540
    const/high16 v11, 0x41f00000    # 30.0f

    .line 2541
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3feab851eb851eb8L    # 0.835

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2542
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto :goto_3

    .line 2543
    :cond_13
    if-eqz v7, :cond_14

    const/16 v0, 0xf0

    if-gt v13, v0, :cond_15

    :cond_14
    if-nez v7, :cond_16

    const/16 v0, 0x140

    if-le v13, v0, :cond_16

    .line 2544
    :cond_15
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2545
    const/high16 v8, 0x424c0000    # 51.0f

    .line 2546
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2547
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fe5c28f5c28f5c3L    # 0.68

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto :goto_3

    .line 2549
    :cond_16
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x41300000    # 11.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2550
    const/high16 v8, 0x42200000    # 40.0f

    .line 2551
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2552
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v2, 0x3fe5c28f5c28f5c3L    # 0.68

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_3

    .line 2564
    .restart local v6    # "i":I
    .restart local v10    # "offsetLinePosY":F
    :cond_17
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSeekBarHeight:I
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v0

    mul-int/2addr v0, v6

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    add-float v9, v0, v11

    .line 2566
    .restart local v9    # "linePosY":F
    const/4 v1, 0x0

    add-float v2, v9, v10

    const/high16 v0, 0x44480000    # 800.0f

    add-float v3, v8, v0

    add-float v4, v9, v10

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 2570
    .end local v9    # "linePosY":F
    :cond_18
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2571
    return-void
.end method
