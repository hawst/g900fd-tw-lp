.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;
.super Landroid/content/BroadcastReceiver;
.source "SAControlPanelActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;->registerReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0

    .prologue
    .line 1159
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1163
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 1164
    :cond_0
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v12, :cond_1

    .line 1165
    const-string v12, "SAControlPanelActivity"

    const-string v13, "Context or intent is null. Do nothing."

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1284
    :cond_1
    :goto_0
    return-void

    .line 1169
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 1171
    .local v2, "action":Ljava/lang/String;
    if-nez v2, :cond_3

    .line 1172
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v12, :cond_1

    .line 1173
    const-string v12, "SAControlPanelActivity"

    const-string v13, "action is null"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1177
    :cond_3
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v12, :cond_4

    .line 1178
    const-string v12, "SAControlPanelActivity"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "mStateReceiver action = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1181
    :cond_4
    const-string v12, "internal_SA_Advanced_UI_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_16

    .line 1184
    const-string v12, "SA_Preset"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 1185
    .local v10, "updatedPreset":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v12

    if-eq v12, v10, :cond_5

    .line 1186
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const/4 v13, 0x1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setUsePresetEffect(IZ)V
    invoke-static {v12, v10, v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;IZ)V

    .line 1190
    :cond_5
    const-string v12, "SA_SquarePosition"

    const/16 v13, 0xc

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 1191
    .local v11, "updatedSquarePos":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v12

    if-eq v12, v11, :cond_6

    .line 1192
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v13

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v12, v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 1193
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v13

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v12, v13, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V

    .line 1194
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v12, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1202(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)I

    .line 1198
    :cond_6
    const/4 v12, 0x7

    new-array v7, v12, [I

    .line 1199
    .local v7, "currentUserEq":[I
    const/4 v12, 0x3

    new-array v6, v12, [I

    .line 1201
    .local v6, "currentStrength":[I
    if-ltz v11, :cond_c

    const/16 v12, 0x18

    if-gt v11, v12, :cond_c

    .line 1202
    const/16 v12, 0xa

    new-array v1, v12, [B

    .line 1203
    .local v1, "Array_EQ_Strengh":[B
    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_8

    .line 1204
    sget-object v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v13

    aget-object v1, v12, v13

    .line 1221
    :cond_7
    :goto_1
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    const/4 v12, 0x7

    if-ge v8, v12, :cond_b

    .line 1222
    aget-byte v12, v1, v8

    aput v12, v7, v8

    .line 1221
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 1205
    .end local v8    # "i":I
    :cond_8
    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-nez v12, :cond_9

    .line 1206
    sget-object v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_MONO_SPK:[[B

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v13

    aget-object v1, v12, v13

    .line 1208
    invoke-static {}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isStereoSpeakerDevice()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1209
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v12

    rem-int/lit8 v12, v12, 0x5

    if-nez v12, :cond_7

    .line 1210
    const/4 v12, 0x7

    const/4 v13, 0x1

    aput-byte v13, v1, v12

    goto :goto_1

    .line 1213
    :cond_9
    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_a

    .line 1214
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_3
    const/16 v12, 0xa

    if-ge v8, v12, :cond_7

    .line 1215
    const/4 v12, 0x0

    aput-byte v12, v1, v8

    .line 1214
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1218
    .end local v8    # "i":I
    :cond_a
    sget-object v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v13

    aget-object v1, v12, v13

    goto :goto_1

    .line 1225
    .restart local v8    # "i":I
    :cond_b
    const/4 v12, 0x0

    const/4 v13, 0x7

    aget-byte v13, v1, v13

    aput v13, v6, v12

    .line 1226
    const/4 v12, 0x1

    const/16 v13, 0x8

    aget-byte v13, v1, v13

    aput v13, v6, v12

    .line 1227
    const/4 v12, 0x2

    const/16 v13, 0x9

    aget-byte v13, v1, v13

    aput v13, v6, v12

    .line 1234
    .end local v1    # "Array_EQ_Strengh":[B
    .end local v8    # "i":I
    :goto_4
    if-eqz v7, :cond_d

    .line 1236
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_5
    const/4 v12, 0x7

    if-ge v8, v12, :cond_e

    .line 1237
    aget v5, v7, v8

    .line 1238
    .local v5, "bandLevel":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const/4 v13, 0x1

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z
    invoke-static {v12, v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)Z

    .line 1239
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    move-result-object v12

    aget-object v12, v12, v8

    add-int/lit8 v13, v5, 0xa

    invoke-virtual {v12, v13}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setProgress(I)V

    .line 1240
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    move-result-object v12

    aget-object v12, v12, v8

    const-string v13, "%s %s %d db %s"

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    move-object/from16 v16, v0

    const v17, 0x7f070035

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I
    invoke-static {}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1700()[I

    move-result-object v17

    aget v17, v17, v8

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    move-object/from16 v16, v0

    const v17, 0x7f070038

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1236
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 1229
    .end local v5    # "bandLevel":I
    .end local v8    # "i":I
    :cond_c
    const-string v12, "SA_EQ_Array"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v7

    .line 1230
    const-string v12, "SA_Strength_Array"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v6

    goto/16 :goto_4

    .line 1244
    :cond_d
    const-string v12, "SAControlPanelActivity"

    const-string v13, "currentUserEq is null"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    :cond_e
    if-eqz v6, :cond_15

    .line 1249
    const/4 v8, 0x0

    .restart local v8    # "i":I
    :goto_6
    const/4 v12, 0x3

    if-ge v8, v12, :cond_1

    .line 1250
    if-nez v8, :cond_11

    .line 1251
    aget v12, v6, v8

    if-nez v12, :cond_10

    .line 1252
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1249
    :cond_f
    :goto_7
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 1254
    :cond_10
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_7

    .line 1255
    :cond_11
    const/4 v12, 0x1

    if-ne v8, v12, :cond_13

    .line 1256
    aget v12, v6, v8

    if-nez v12, :cond_12

    .line 1257
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_7

    .line 1259
    :cond_12
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_7

    .line 1260
    :cond_13
    const/4 v12, 0x2

    if-ne v8, v12, :cond_f

    .line 1261
    aget v12, v6, v8

    if-nez v12, :cond_14

    .line 1262
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_7

    .line 1264
    :cond_14
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_7

    .line 1268
    .end local v8    # "i":I
    :cond_15
    const-string v12, "SAControlPanelActivity"

    const-string v13, "currentStrength is null"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1271
    .end local v6    # "currentStrength":[I
    .end local v7    # "currentUserEq":[I
    .end local v10    # "updatedPreset":I
    .end local v11    # "updatedSquarePos":I
    :cond_16
    const-string v12, "internal_SA_Auto_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_17

    .line 1272
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v9

    .line 1273
    .local v9, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v12, "SA_AUTO_STATE"

    const/4 v13, 0x0

    invoke-virtual {v9, v12, v13}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 1274
    .local v4, "autoState":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v12, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 1275
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const/4 v13, 0x0

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateAutoPlay(Z)V
    invoke-static {v12, v13}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    goto/16 :goto_0

    .line 1276
    .end local v4    # "autoState":Z
    .end local v9    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_17
    const-string v12, "internal_SA_Auto_UI_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_18

    .line 1277
    const-string v12, "SA_Auto_State"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 1278
    .restart local v4    # "autoState":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v12, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    goto/16 :goto_0

    .line 1280
    .end local v4    # "autoState":Z
    :cond_18
    const-string v12, "internal_SA_Adapt_UI_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1281
    const-string v12, "SA_AdaptSound_State"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 1282
    .local v3, "adaptstate":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAdapt(Z)V
    invoke-static {v12, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    goto/16 :goto_0
.end method
