.class public Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;
.super Landroid/widget/BaseAdapter;
.source "SAControlPanelActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SoundAliveAdapter"
.end annotation


# static fields
.field private static final CLASSIC:I = 0x2

.field private static final JAZZ:I = 0x10

.field private static final NORMAL:I = 0xc

.field private static final POP:I = 0xd

.field private static final ROCK:I = 0xa


# instance fields
.field private saContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 2624
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2625
    iput-object p2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->saContext:Landroid/content/Context;

    .line 2626
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 2629
    const/16 v0, 0x19

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 2633
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 2637
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 2642
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->saContext:Landroid/content/Context;

    const v4, 0x7f030005

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2644
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f09002f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2645
    .local v1, "text":Landroid/widget/TextView;
    const v3, 0x7f09002e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2648
    .local v0, "imageView":Landroid/widget/ImageView;
    if-nez p2, :cond_1

    .line 2649
    sparse-switch p1, :sswitch_data_0

    .line 2667
    const-string v3, " "

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2674
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v3

    if-ne v3, p1, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mLastControlledTab:Z
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2675
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f040011

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2678
    :cond_0
    return-object v2

    .line 2651
    :sswitch_0
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2654
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070028

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2657
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070023

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2660
    :sswitch_3
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070026

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2663
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070013

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2671
    :cond_1
    move-object v2, p2

    goto :goto_0

    .line 2649
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xa -> :sswitch_1
        0xc -> :sswitch_2
        0xd -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method
