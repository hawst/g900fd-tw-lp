.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;
.super Ljava/lang/Object;
.source "SAControlPanelActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initAdvancedEffectButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0

    .prologue
    .line 710
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 713
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 714
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****mNewSoundAliveButtonBass.setOnClickListener*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const/16 v2, 0x8

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isSoundAliveSupportCurrentAudioPath(IZ)Z
    invoke-static {v1, v2, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 716
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 717
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 718
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 722
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setStrength(II)V
    invoke-static {v1, v5, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;II)V

    .line 723
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v2

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v0

    .line 725
    .local v0, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v1, "SA_TAB_INDEX"

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 727
    .end local v0    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_2
    return-void
.end method
