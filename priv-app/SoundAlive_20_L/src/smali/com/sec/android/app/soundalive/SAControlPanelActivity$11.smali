.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;
.super Ljava/lang/Object;
.source "SAControlPanelActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initSoundAliveGridView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0

    .prologue
    .line 985
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v9, 0x18

    const/16 v8, 0xc

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 989
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v4

    if-nez v4, :cond_0

    move v4, v5

    .line 1068
    :goto_0
    return v4

    .line 993
    :cond_0
    instance-of v4, p1, Landroid/widget/GridView;

    if-eqz v4, :cond_1

    move-object v4, p1

    check-cast v4, Landroid/widget/GridView;

    move-object v0, v4

    .line 994
    .local v0, "gView":Landroid/widget/GridView;
    :goto_1
    if-nez v0, :cond_2

    move v4, v5

    goto :goto_0

    .line 993
    .end local v0    # "gView":Landroid/widget/GridView;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 996
    .restart local v0    # "gView":Landroid/widget/GridView;
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v0, v4, v7}, Landroid/widget/GridView;->pointToPosition(II)I

    move-result v2

    .line 998
    .local v2, "position":I
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 999
    if-ltz v2, :cond_3

    if-gt v2, v9, :cond_3

    if-eq v2, v8, :cond_3

    .line 1000
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v7, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v7

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v4, v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 1001
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v7, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v7

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v4, v7, v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V

    .line 1002
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V
    invoke-static {v4, v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V

    .line 1003
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v4, v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1202(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)I

    .line 1004
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f07002d

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1005
    .local v1, "msg":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v4

    if-nez v4, :cond_4

    .line 1006
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v7, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v4, v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1302(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 1010
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .end local v1    # "msg":Ljava/lang/String;
    :cond_3
    move v4, v5

    .line 1013
    goto :goto_0

    .line 1008
    .restart local v1    # "msg":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1016
    .end local v1    # "msg":Ljava/lang/String;
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1066
    const-string v4, "SAControlPanelActivity"

    const-string v5, "undefined touch event!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v6

    .line 1068
    goto/16 :goto_0

    .line 1018
    :pswitch_0
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_6

    .line 1019
    const-string v4, "SAControlPanelActivity"

    const-string v7, "*****MotionEvent.ACTION_DOWN*****"

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    :cond_6
    instance-of v4, p1, Landroid/widget/GridView;

    if-eqz v4, :cond_8

    .line 1022
    if-ltz v2, :cond_8

    if-gt v2, v9, :cond_8

    .line 1023
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v7, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v7

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v4, v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 1024
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v7, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v7

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v4, v7, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V

    .line 1025
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V
    invoke-static {v4, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V

    .line 1026
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1028
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v4, v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 1029
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/content/Context;

    move-result-object v4

    iget-object v7, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070014

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1031
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v4, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1202(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)I

    .line 1036
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 1037
    .local v3, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v4, "SA_TAB_INDEX"

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    move v4, v5

    .line 1039
    goto/16 :goto_0

    .line 1041
    .end local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :pswitch_1
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_9

    .line 1042
    const-string v4, "SAControlPanelActivity"

    const-string v6, "*****MotionEvent.ACTION_MOVE*****"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v4

    if-eq v4, v2, :cond_a

    if-ltz v2, :cond_a

    if-gt v2, v9, :cond_a

    .line 1045
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v6

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v4, v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 1046
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v6

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v4, v6, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V

    .line 1047
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V
    invoke-static {v4, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V

    .line 1048
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v4, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1202(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)I

    :cond_a
    move v4, v5

    .line 1051
    goto/16 :goto_0

    .line 1053
    :pswitch_2
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_b

    .line 1054
    const-string v4, "SAControlPanelActivity"

    const-string v6, "*****MotionEvent.ACTION_UP*****"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v4

    if-eq v4, v2, :cond_c

    if-ltz v2, :cond_c

    if-gt v2, v9, :cond_c

    .line 1057
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v6

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v4, v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 1058
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v6

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v4, v6, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V

    .line 1059
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V
    invoke-static {v4, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V

    .line 1060
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I
    invoke-static {v4, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1202(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)I

    .line 1062
    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    move v4, v5

    .line 1064
    goto/16 :goto_0

    .line 1016
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
