.class public Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;
.super Lcom/sec/android/app/soundalive/widget/TwSeekBar;
.source "UserEQSeekBar.java"


# instance fields
.field private mBaseValue:I

.field private mTitleRes:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;-><init>(Landroid/content/Context;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method


# virtual methods
.method public getValue()I
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->getProgress()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->mBaseValue:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 54
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->onSaveInstanceState()Landroid/os/Parcelable;

    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public setBaseValue(I)V
    .locals 0
    .param p1, "base"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->mBaseValue:I

    .line 28
    return-void
.end method

.method public setContentDescription()V
    .locals 6

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 45
    .local v0, "context":Landroid/content/Context;
    const-string v2, "%s %s, %ddb. %s"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0x7f070035

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->mTitleRes:I

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const v5, 0x7f070038

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "description":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 49
    return-void
.end method

.method public setTitle(I)V
    .locals 0
    .param p1, "title"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->mTitleRes:I

    .line 38
    return-void
.end method
