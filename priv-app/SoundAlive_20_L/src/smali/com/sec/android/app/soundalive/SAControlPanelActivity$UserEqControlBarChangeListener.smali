.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;
.super Ljava/lang/Object;
.source "SAControlPanelActivity.java"

# interfaces
.implements Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserEqControlBarChangeListener"
.end annotation


# instance fields
.field private final mHideBubble:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 1

    .prologue
    .line 2077
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2166
    new-instance v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener$1;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;)V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->mHideBubble:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Lcom/sec/android/app/soundalive/SAControlPanelActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p2, "x1"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity$1;

    .prologue
    .line 2077
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Lcom/sec/android/app/soundalive/widget/TwSeekBar;IZ)V
    .locals 9
    .param p1, "controlBar"    # Lcom/sec/android/app/soundalive/widget/TwSeekBar;
    .param p2, "progress"    # I
    .param p3, "fromTouch"    # Z

    .prologue
    const/4 v8, 0x1

    .line 2081
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2124
    :cond_0
    :goto_0
    return-void

    .line 2084
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v3

    if-ne v3, v8, :cond_2

    .line 2085
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    add-int/lit8 v4, p2, -0xa

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubbleText(Ljava/lang/CharSequence;)V

    .line 2087
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbCenterPosX()I

    move-result v0

    .line 2088
    .local v0, "bubbleX":I
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbCenterPosY()I

    move-result v1

    .line 2090
    .local v1, "bubbleY":I
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 2091
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isPortrait()Z
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v3

    if-nez v3, :cond_3

    add-int/lit8 v3, p2, -0xa

    const/4 v4, 0x7

    if-gt v3, v4, :cond_4

    :cond_3
    add-int/lit8 v3, p2, -0xa

    const/16 v4, 0x9

    if-le v3, v4, :cond_7

    .line 2092
    :cond_4
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0xf

    add-int/2addr v1, v3

    .line 2097
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v4

    monitor-enter v4

    .line 2098
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3, p1, v0, v1}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubblePosition(Landroid/view/View;II)V

    .line 2099
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2100
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v3

    if-ne v3, v8, :cond_5

    .line 2101
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->showBubble()V

    .line 2102
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->invalidate()V

    .line 2104
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)Z

    .line 2106
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsBubbleRunable:Z
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2107
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->mHideBubble:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2109
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->mHideBubble:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v4

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsBubbleRunable:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2902(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)Z

    .line 2111
    const-string v3, "SAControlPanelActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Eq control changed fromTouch : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2112
    if-eqz p3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v3

    if-ne v3, v8, :cond_0

    .line 2116
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setAdvancedBandLevel()V

    .line 2120
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v2

    .line 2121
    .local v2, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v3, "SA_TAB_INDEX"

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 2094
    .end local v2    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0xf

    sub-int/2addr v1, v3

    goto/16 :goto_1

    .line 2099
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public onStartTrackingTouch(Lcom/sec/android/app/soundalive/widget/TwSeekBar;)V
    .locals 5
    .param p1, "controlBar"    # Lcom/sec/android/app/soundalive/widget/TwSeekBar;

    .prologue
    .line 2129
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbCenterPosX()I

    move-result v0

    .line 2130
    .local v0, "bubbleX":I
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbCenterPosY()I

    move-result v1

    .line 2131
    .local v1, "bubbleY":I
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getProgress()I

    move-result v2

    .line 2133
    .local v2, "progress":I
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 2134
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isPortrait()Z
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v3, v2, -0xa

    const/4 v4, 0x7

    if-gt v3, v4, :cond_1

    :cond_0
    add-int/lit8 v3, v2, -0xa

    const/16 v4, 0x9

    if-le v3, v4, :cond_3

    .line 2135
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0xf

    add-int/2addr v1, v3

    .line 2140
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 2141
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    add-int/lit8 v4, v2, -0xa

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubbleText(Ljava/lang/CharSequence;)V

    .line 2143
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3, p1, v0, v1}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubblePosition(Landroid/view/View;II)V

    .line 2144
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->showBubble()V

    .line 2145
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->invalidate()V

    .line 2146
    return-void

    .line 2137
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getThumbHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0xf

    sub-int/2addr v1, v3

    goto :goto_0
.end method

.method public onStopTrackingTouch(Lcom/sec/android/app/soundalive/widget/TwSeekBar;)V
    .locals 4
    .param p1, "controlBar"    # Lcom/sec/android/app/soundalive/widget/TwSeekBar;

    .prologue
    const/16 v2, 0xc

    const/4 v3, 0x0

    .line 2150
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->hideBubble()V

    .line 2151
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->invalidate()V

    .line 2153
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mLastControlledTab:Z
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2154
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 2155
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2156
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V

    .line 2157
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V

    .line 2159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2160
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 2161
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2164
    :cond_1
    return-void
.end method
