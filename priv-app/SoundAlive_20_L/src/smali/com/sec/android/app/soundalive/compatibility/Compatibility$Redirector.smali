.class public Lcom/sec/android/app/soundalive/compatibility/Compatibility$Redirector;
.super Landroid/app/Activity;
.source "Compatibility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/compatibility/Compatibility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Redirector"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const-string v7, "Compatibility"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onCreate() : Compatibility Activity called from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Redirector;->getCallingPackage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Redirector;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 48
    .local v4, "i":Landroid/content/Intent;
    const/high16 v7, 0x2000000

    invoke-virtual {v4, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Redirector;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v5

    .line 52
    .local v5, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v7, "defaultpanelpackage"

    invoke-virtual {v5, v7, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 53
    .local v3, "defPackage":Ljava/lang/String;
    const-string v7, "defaultpanelname"

    invoke-virtual {v5, v7, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "defName":Ljava/lang/String;
    const-string v7, "Compatibility"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onCreate() : previous saved setting :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " as default"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 57
    :cond_0
    const-string v7, "Compatibility"

    const-string v8, "onCreate() : no default set! so setting default Effect panel as SoundAlive"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const-string v1, "com.sec.android.app.soundalive"

    .line 60
    .local v1, "SAPackageName":Ljava/lang/String;
    const-string v0, "com.sec.android.app.soundalive.SAControlPanelActivity"

    .line 61
    .local v0, "SAClassName":Ljava/lang/String;
    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Service;

    invoke-direct {v6, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .local v6, "updateIntent":Landroid/content/Intent;
    const-string v7, "defPackage"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string v7, "defName"

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p0, v6}, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Redirector;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 70
    .end local v0    # "SAClassName":Ljava/lang/String;
    .end local v1    # "SAPackageName":Ljava/lang/String;
    .end local v6    # "updateIntent":Landroid/content/Intent;
    :goto_0
    const-string v7, "Compatibility"

    const-string v8, "onCreate() : call startActivity"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-virtual {p0, v4}, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Redirector;->startActivity(Landroid/content/Intent;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/compatibility/Compatibility$Redirector;->finish()V

    .line 74
    return-void

    .line 68
    :cond_1
    new-instance v7, Landroid/content/ComponentName;

    invoke-direct {v7, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method
