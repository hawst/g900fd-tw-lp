.class public Lcom/sec/android/app/soundalive/SAControlPanelReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SAControlPanelReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SAControlPanelReceiver"

.field private static prevAudioSession:I

.field private static prevPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, -0x4

    sput v0, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevAudioSession:I

    .line 17
    const-string v0, "__SoundAlivePlayer"

    sput-object v0, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevPackageName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v14, :cond_0

    .line 23
    const-string v14, "SAControlPanelReceiver"

    const-string v15, "*****onReceive()*****"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_3

    .line 26
    :cond_1
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_2

    .line 27
    const-string v14, "SAControlPanelReceiver"

    const-string v15, "Context or intent is null. Do nothing."

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_2
    :goto_0
    return-void

    .line 31
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "action":Ljava/lang/String;
    if-nez v1, :cond_4

    .line 34
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_2

    .line 35
    const-string v14, "SAControlPanelReceiver"

    const-string v15, "action is null"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 39
    :cond_4
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_5

    .line 40
    const-string v14, "SAControlPanelReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Action: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_5
    const-string v14, "com.android.bluetooth.ACTION_SOUNDALIVE_CHANGED"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 45
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v11

    .line 46
    .local v11, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v14, "SA_SERVICE_STATE"

    const/16 v15, 0xc

    invoke-virtual {v11, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v13

    .line 48
    .local v13, "svc_state":I
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_6

    .line 49
    const-string v14, "SAControlPanelReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ACTION_SOUNDALIVE_CHANGED    svc_state : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :cond_6
    const/16 v14, 0xc

    if-eq v13, v14, :cond_7

    .line 53
    new-instance v7, Landroid/content/Intent;

    const-string v14, "com.sec.android.app.soundalive.ACTION_SA_DUPLICATED_USED"

    invoke-direct {v7, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 54
    .local v7, "i":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 57
    .end local v7    # "i":Landroid/content/Intent;
    :cond_7
    const/4 v14, 0x1

    sput-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBT_App_Active:Z

    goto :goto_0

    .line 61
    .end local v11    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    .end local v13    # "svc_state":I
    :cond_8
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBT_App_Active:Z

    if-nez v14, :cond_2

    .line 65
    const-string v14, "android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10

    .line 67
    const-string v14, "android.media.extra.PACKAGE_NAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 68
    .local v5, "currentPackageName":Ljava/lang/String;
    const-string v14, "android.media.extra.AUDIO_SESSION"

    const/4 v15, -0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 71
    .local v4, "currentAudioSession":I
    if-eqz v5, :cond_2

    .line 74
    const-string v14, "com.google.android.music"

    invoke-virtual {v14, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 77
    const/4 v14, -0x4

    if-eq v4, v14, :cond_9

    if-gez v4, :cond_a

    .line 78
    :cond_9
    const-string v14, "SAControlPanelReceiver"

    const-string v15, "Invalid or missing audio session or pakagename "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 82
    :cond_a
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v11

    .line 84
    .restart local v11    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const/4 v9, 0x0

    .line 85
    .local v9, "isThisRCV_Got_Control_FromOthers":Z
    const-string v14, "WHICH_RCV_HAS_CONTRL"

    const/4 v15, 0x0

    invoke-virtual {v11, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 87
    .local v8, "idx":I
    const/16 v14, 0x89

    if-eq v8, v14, :cond_b

    .line 88
    const/4 v9, 0x1

    .line 91
    :cond_b
    const-string v14, "AUDIOSESSION_ID"

    invoke-virtual {v11, v14, v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 92
    const-string v14, "WHICH_RCV_HAS_CONTRL"

    const/16 v15, 0x89

    invoke-virtual {v11, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 93
    const-string v14, "CURRENT_PACKAGENAME"

    invoke-virtual {v11, v14, v5}, Lcom/sec/android/app/soundalive/PreferencesManager;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v14, "com.sec.android.app.music"

    invoke-virtual {v14, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 97
    const-string v14, "com.sec.android.app.Auto"

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 98
    .local v2, "auto":I
    const-string v14, "SA_GENRE_INDEX"

    invoke-virtual {v11, v14, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 101
    .end local v2    # "auto":I
    :cond_c
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_d

    .line 102
    const-string v14, "SAControlPanelReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "OPEN_SESSION currentPackageName :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "  prevPackageName : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevPackageName:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " currentAudioSession : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "  prevAudioSession : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevAudioSession:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_d
    new-instance v7, Landroid/content/Intent;

    const-class v14, Lcom/sec/android/app/soundalive/SAControlPanelService;

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 108
    .restart local v7    # "i":Landroid/content/Intent;
    sget v14, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevAudioSession:I

    if-ne v4, v14, :cond_e

    if-nez v9, :cond_e

    sget-object v14, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevPackageName:Ljava/lang/String;

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_12

    .line 109
    :cond_e
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_f

    .line 110
    const-string v14, "SAControlPanelReceiver"

    const-string v15, "start SAControlPanelService - SERVICE_STATE_OPEN"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_f
    const-string v14, "SA_SERVICE_STATE"

    const/16 v15, 0x9

    invoke-virtual {v7, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 113
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 122
    :goto_1
    sput-object v5, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevPackageName:Ljava/lang/String;

    .line 123
    sput v4, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevAudioSession:I

    .line 127
    .end local v4    # "currentAudioSession":I
    .end local v5    # "currentPackageName":Ljava/lang/String;
    .end local v7    # "i":Landroid/content/Intent;
    .end local v8    # "idx":I
    .end local v9    # "isThisRCV_Got_Control_FromOthers":Z
    .end local v11    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_10
    const-string v14, "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_17

    .line 129
    const-string v14, "android.media.extra.PACKAGE_NAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 130
    .restart local v5    # "currentPackageName":Ljava/lang/String;
    const-string v14, "android.media.extra.AUDIO_SESSION"

    const/4 v15, -0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 133
    .restart local v4    # "currentAudioSession":I
    if-eqz v5, :cond_2

    .line 136
    const-string v14, "com.google.android.music"

    invoke-virtual {v14, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 138
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v11

    .line 139
    .restart local v11    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v14, "AUDIOSESSION_ID"

    invoke-virtual {v11, v14, v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 140
    const-string v14, "SA_SERVICE_STATE"

    const/16 v15, 0xc

    invoke-virtual {v11, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v13

    .line 142
    .restart local v13    # "svc_state":I
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_11

    .line 143
    const-string v14, "SAControlPanelReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "CLOSE_SESSION currentPackageName :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "  prevPackageName : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevPackageName:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " currentAudioSession : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "  prevAudioSession : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevAudioSession:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_11
    const-string v14, "WHICH_RCV_HAS_CONTRL"

    const/4 v15, 0x0

    invoke-virtual {v11, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 149
    .restart local v8    # "idx":I
    const/16 v14, 0x89

    if-eq v8, v14, :cond_14

    .line 150
    const-string v14, "SAControlPanelReceiver"

    const-string v15, "Player sending CLOSE SESSION Intent with too much delay, so we need to skip SA service pause"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 115
    .end local v13    # "svc_state":I
    .restart local v7    # "i":Landroid/content/Intent;
    .restart local v9    # "isThisRCV_Got_Control_FromOthers":Z
    :cond_12
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_13

    .line 116
    const-string v14, "SAControlPanelReceiver"

    const-string v15, "start SAControlPanelService - SERVICE_STATE_RESUME"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :cond_13
    const-string v14, "SA_SERVICE_STATE"

    const/16 v15, 0xa

    invoke-virtual {v7, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 119
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_1

    .line 154
    .end local v7    # "i":Landroid/content/Intent;
    .end local v9    # "isThisRCV_Got_Control_FromOthers":Z
    .restart local v13    # "svc_state":I
    :cond_14
    sget-object v14, Lcom/sec/android/app/soundalive/SAControlPanelReceiver;->prevPackageName:Ljava/lang/String;

    invoke-virtual {v5, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_17

    const/16 v14, 0xa

    if-eq v13, v14, :cond_15

    const/16 v14, 0x9

    if-ne v13, v14, :cond_17

    .line 156
    :cond_15
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v14, :cond_16

    .line 157
    const-string v14, "SAControlPanelReceiver"

    const-string v15, "start SAControlPanelService - SERVICE_STATE_PAUSE"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_16
    new-instance v7, Landroid/content/Intent;

    const-class v14, Lcom/sec/android/app/soundalive/SAControlPanelService;

    move-object/from16 v0, p1

    invoke-direct {v7, v0, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    .restart local v7    # "i":Landroid/content/Intent;
    const-string v14, "SA_SERVICE_STATE"

    const/16 v15, 0xb

    invoke-virtual {v7, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 161
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 165
    .end local v4    # "currentAudioSession":I
    .end local v5    # "currentPackageName":Ljava/lang/String;
    .end local v7    # "i":Landroid/content/Intent;
    .end local v8    # "idx":I
    .end local v11    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    .end local v13    # "svc_state":I
    :cond_17
    const-string v14, "com.sec.android.app.safx.ACTION_REQUEST_GENRE"

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 166
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v11

    .line 167
    .restart local v11    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v14, "SA_AUTO_STATE"

    const/4 v15, 0x0

    invoke-virtual {v11, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 168
    .local v3, "autoState":Z
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v15, v15, v16

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "SA_SQUARE_INDEX"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/16 v15, 0xc

    invoke-virtual {v11, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v12

    .line 169
    .local v12, "squarePosition":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v15, v15, v16

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "SA_PRESET_INDEX"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v11, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 171
    .local v10, "preset":I
    if-eqz v3, :cond_18

    .line 172
    const/16 v6, 0x63

    .line 196
    .local v6, "genreINFO":I
    :goto_2
    new-instance v7, Landroid/content/Intent;

    const-string v14, "com.sec.android.app.SA_GENRE_INFO"

    invoke-direct {v7, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 197
    .restart local v7    # "i":Landroid/content/Intent;
    const-string v14, "genreINFO"

    invoke-virtual {v7, v14, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 198
    const-string v14, "presetINFO"

    invoke-virtual {v7, v14, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 199
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 200
    const-string v14, "SAControlPanelReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "received ACTION_REQUEST_GENRE so we sent SA_GENRE_INFO   genreINFO : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 174
    .end local v6    # "genreINFO":I
    .end local v7    # "i":Landroid/content/Intent;
    :cond_18
    sparse-switch v12, :sswitch_data_0

    .line 191
    const/16 v6, 0xd

    .restart local v6    # "genreINFO":I
    goto :goto_2

    .line 176
    .end local v6    # "genreINFO":I
    :sswitch_0
    const/4 v6, 0x0

    .line 177
    .restart local v6    # "genreINFO":I
    goto :goto_2

    .line 179
    .end local v6    # "genreINFO":I
    :sswitch_1
    const/4 v6, 0x1

    .line 180
    .restart local v6    # "genreINFO":I
    goto :goto_2

    .line 182
    .end local v6    # "genreINFO":I
    :sswitch_2
    const/4 v6, 0x2

    .line 183
    .restart local v6    # "genreINFO":I
    goto :goto_2

    .line 185
    .end local v6    # "genreINFO":I
    :sswitch_3
    const/4 v6, 0x4

    .line 186
    .restart local v6    # "genreINFO":I
    goto :goto_2

    .line 188
    .end local v6    # "genreINFO":I
    :sswitch_4
    const/4 v6, 0x5

    .line 189
    .restart local v6    # "genreINFO":I
    goto :goto_2

    .line 174
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0xa -> :sswitch_2
        0xc -> :sswitch_0
        0xd -> :sswitch_1
        0x10 -> :sswitch_3
    .end sparse-switch
.end method
