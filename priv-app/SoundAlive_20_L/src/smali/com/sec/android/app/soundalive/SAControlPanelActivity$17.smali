.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;
.super Ljava/lang/Object;
.source "SAControlPanelActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0

    .prologue
    .line 2269
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0xc

    const/4 v3, 0x0

    .line 2273
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/16 v0, 0x13

    if-eq p2, v0, :cond_0

    const/16 v0, 0x14

    if-ne p2, v0, :cond_2

    .line 2275
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mLastControlledTab:Z
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2276
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 2277
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2278
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V

    .line 2280
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V

    .line 2282
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2283
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 2284
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2290
    :cond_2
    return v3
.end method
