.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;
.super Ljava/lang/Object;
.source "SAControlPanelActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initSoundAliveGridView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0

    .prologue
    .line 972
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 974
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-ltz p3, :cond_0

    const/16 v0, 0x18

    if-gt p3, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 975
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V

    .line 976
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v0, v1, p3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V

    .line 977
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V
    invoke-static {v0, p3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V

    .line 978
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 982
    :cond_0
    return-void
.end method
