.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;
.super Ljava/lang/Object;
.source "SAControlPanelActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initLayout(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0

    .prologue
    .line 1797
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1800
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1801
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1802
    .local v0, "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1803
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1302(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 1807
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1808
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 1824
    .end local v0    # "msg":Ljava/lang/String;
    :goto_1
    return-void

    .line 1805
    .restart local v0    # "msg":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1811
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1812
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateAutoPlay(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    .line 1813
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    goto :goto_1

    .line 1815
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1816
    .restart local v0    # "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1817
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1302(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 1821
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1822
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V

    goto :goto_1

    .line 1819
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
