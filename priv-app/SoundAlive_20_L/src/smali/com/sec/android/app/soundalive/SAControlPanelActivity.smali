.class public Lcom/sec/android/app/soundalive/SAControlPanelActivity;
.super Landroid/app/Activity;
.source "SAControlPanelActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;,
        Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;,
        Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;,
        Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;
    }
.end annotation


# static fields
.field private static final ADVANCED_TAB:Z = true

.field private static final BASE_POSTION:I = 0xa

.field private static final BASIC_TAB:Z = false

.field private static final DB_SCALE_LEVEL_EQ:I = 0xa

.field private static final EQ_SEEKBAR_RES_ID:[I

.field private static final EQ_STRING_RES_ID:[I

.field public static final MENU_CANCEL:I = 0x1

.field public static final MENU_DONE:I = 0x2

.field public static final MENU_RESET:I = 0x0

.field private static final NEWSOUNDALIVE_PRESET_CLUB:I = 0x4

.field private static final NEWSOUNDALIVE_PRESET_CONCERTHALL:I = 0x5

.field private static final NEWSOUNDALIVE_PRESET_NORMAL:I = 0x0

.field private static final NEWSOUNDALIVE_PRESET_STUDIO:I = 0x3

.field private static final NEWSOUNDALIVE_PRESET_TUBE:I = 0x1

.field private static final NEWSOUNDALIVE_PRESET_VIRT71:I = 0x2

.field private static final NEWSOUNDALIVE_STRENGTH_MODE_3D:I = 0x0

.field private static final NEWSOUNDALIVE_STRENGTH_MODE_BASS:I = 0x1

.field private static final NEWSOUNDALIVE_STRENGTH_MODE_CLARITY:I = 0x2

.field private static final SA_EFFECT_OFF:I = 0x0

.field private static final SA_EFFECT_ON:I = 0x1

.field public static final SA_EQ_BAND_LOWER_BOUND:I = 0x0

.field public static final SA_EQ_BAND_UPPER_BOUND:I = 0x6

.field public static final SA_EQ_LEVEL_LOWER_BOUND:I = -0xa

.field public static final SA_EQ_LEVEL_UPPER_BOUND:I = 0xa

.field public static final SA_PRESETS_LOWER_BOUND:I = 0x0

.field public static final SA_PRESETS_UPPER_BOUND:I = 0x5

.field public static final SA_SQUARE_CELL_LOWER_BOUND:I = 0x0

.field public static final SA_SQUARE_CELL_UPPER_BOUND:I = 0x18

.field public static final SA_STRENGTH_ID_LOWER_BOUND:I = 0x0

.field public static final SA_STRENGTH_ID_UPPER_BOUND:I = 0x2

.field private static final SET_STRENGTH_FORCE_OFF:I = 0x2

.field private static final SET_STRENGTH_FORCE_ON:I = 0x1

.field private static final SET_STRENGTH_TOGGLE:I = 0x0

.field private static final SOUND_ALIVE_LOGGING_FEATURE:Ljava/lang/String; = "SDAL"

.field public static final SQUARE_CELL_UNSELECTED:I = -0x1

.field private static final TAB_COUNT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SAControlPanelActivity"

.field public static mAdaptCheckBoxState:Z

.field public static mBT_App_Active:Z

.field public static mCheckBoxState:Z

.field public static mIsActivityVisible:Z

.field public static sEntrance:Z


# instance fields
.field private mAdaptEnable:Z

.field private mAdvancedButton:Landroid/widget/TextView;

.field private mAdvancedLayout:Landroid/view/ViewGroup;

.field private mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

.field private mAutoEnable:Z

.field private mBasicButton:Landroid/widget/TextView;

.field private mBasicLayout:Landroid/view/ViewGroup;

.field private mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

.field private mCheckBoxAdapt:Landroid/widget/CheckBox;

.field private mCheckBoxAuto:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mCurrentTab:I

.field private mDbLinePaddingTop:I

.field private mEntranceUsePreset:I

.field private final mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

.field private final mEqControlBarKeyListener:Landroid/view/View$OnKeyListener;

.field private mImageViewEffctClub:Landroid/widget/ImageView;

.field private mImageViewEffctConcertHall:Landroid/widget/ImageView;

.field private mImageViewEffctNone:Landroid/widget/ImageView;

.field private mImageViewEffctStudio:Landroid/widget/ImageView;

.field private mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

.field private mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

.field private mImageViewTabAdvancedBottomLine:Landroid/view/View;

.field private mImageViewTabBasicBottomLine:Landroid/view/View;

.field private mIsBubbleRunable:Z

.field private mIsCancel:Z

.field private mIsReset:Z

.field private mLastControlledTab:Z

.field private mNewSoundAliveButton3D:Landroid/widget/TextView;

.field private mNewSoundAliveButtonBass:Landroid/widget/TextView;

.field private mNewSoundAliveButtonClarity:Landroid/widget/TextView;

.field private mPaint:Landroid/graphics/Paint;

.field private mRateLandlinePosY:D

.field private mRatePortlinePosY:D

.field private mRenderingMode:Z

.field private mSavedInstance:Landroid/os/Bundle;

.field private mSeekBarHeight:I

.field private mSelectedUsePreset:I

.field private mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

.field private mSoundAlivePresetGridview:Landroid/widget/GridView;

.field private mSquarePosition:I

.field private mStateReceiver:Landroid/content/BroadcastReceiver;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x7

    const/4 v1, 0x0

    .line 60
    sput-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBT_App_Active:Z

    .line 171
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_SEEKBAR_RES_ID:[I

    .line 174
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    .line 201
    sput-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sEntrance:Z

    .line 203
    sput-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsActivityVisible:Z

    .line 205
    sput-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxState:Z

    .line 207
    sput-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptCheckBoxState:Z

    return-void

    .line 171
    nop

    :array_0
    .array-data 4
        0x7f090010
        0x7f090011
        0x7f090012
        0x7f090013
        0x7f090014
        0x7f090015
        0x7f090016
    .end array-data

    .line 174
    :array_1
    .array-data 4
        0x7f07000c
        0x7f07000d
        0x7f07000e
        0x7f07000f
        0x7f070010
        0x7f070011
        0x7f070012
    .end array-data
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    iput-boolean v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRenderingMode:Z

    .line 157
    iput v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    .line 163
    iput-wide v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D

    .line 165
    iput-wide v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D

    .line 167
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    .line 169
    iput v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEntranceUsePreset:I

    .line 193
    iput v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    .line 197
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z

    .line 209
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsCancel:Z

    .line 215
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mLastControlledTab:Z

    .line 217
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    .line 219
    iput-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSavedInstance:Landroid/os/Bundle;

    .line 221
    iput-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;

    .line 223
    iput-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    .line 225
    iput-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    .line 227
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    .line 229
    iput-boolean v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptEnable:Z

    .line 231
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;

    .line 233
    iput-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    .line 2269
    new-instance v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$17;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 2611
    return-void
.end method

.method private EffectReconfigByPreviousSettings(Z)V
    .locals 13
    .param p1, "isCancel"    # Z

    .prologue
    .line 447
    sget-boolean v9, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBT_App_Active:Z

    if-eqz v9, :cond_1

    .line 553
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v8

    .line 451
    .local v8, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v9, "SA_AUTO_STATE"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 454
    .local v3, "autoState":Z
    if-eqz v3, :cond_7

    .line 455
    const-string v9, "SA_GENRE_INDEX"

    const/16 v10, 0xc

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 458
    .local v6, "genreIdx":I
    packed-switch v6, :pswitch_data_0

    .line 475
    :pswitch_0
    const/16 v1, 0xc

    .line 479
    .local v1, "Position":I
    :goto_1
    const/16 v9, 0xa

    new-array v0, v9, [B

    .line 480
    .local v0, "Array_EQ_Strengh":[B
    sget v9, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 481
    sget-object v9, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    aget-object v0, v9, v1

    .line 499
    :cond_2
    :goto_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v11, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "SA_SQUARE_INDEX"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v1}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 501
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    const/4 v9, 0x7

    if-ge v7, v9, :cond_6

    .line 502
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v11, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "SA_EQ_INDEX_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aget-byte v10, v0, v7

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 501
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 460
    .end local v0    # "Array_EQ_Strengh":[B
    .end local v1    # "Position":I
    .end local v7    # "i":I
    :pswitch_1
    const/16 v1, 0xc

    .line 461
    .restart local v1    # "Position":I
    goto :goto_1

    .line 463
    .end local v1    # "Position":I
    :pswitch_2
    const/16 v1, 0xd

    .line 464
    .restart local v1    # "Position":I
    goto :goto_1

    .line 466
    .end local v1    # "Position":I
    :pswitch_3
    const/16 v1, 0xa

    .line 467
    .restart local v1    # "Position":I
    goto :goto_1

    .line 469
    .end local v1    # "Position":I
    :pswitch_4
    const/16 v1, 0x10

    .line 470
    .restart local v1    # "Position":I
    goto :goto_1

    .line 472
    .end local v1    # "Position":I
    :pswitch_5
    const/4 v1, 0x2

    .line 473
    .restart local v1    # "Position":I
    goto :goto_1

    .line 482
    .restart local v0    # "Array_EQ_Strengh":[B
    :cond_3
    sget v9, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-nez v9, :cond_4

    .line 483
    sget-object v9, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_MONO_SPK:[[B

    aget-object v0, v9, v1

    .line 485
    invoke-static {}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isStereoSpeakerDevice()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 486
    rem-int/lit8 v9, v1, 0x5

    if-nez v9, :cond_2

    .line 487
    const/4 v9, 0x7

    const/4 v10, 0x1

    aput-byte v10, v0, v9

    goto :goto_2

    .line 490
    :cond_4
    sget v9, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v10, 0x4

    if-ne v9, v10, :cond_5

    .line 491
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_4
    const/16 v9, 0xa

    if-ge v7, v9, :cond_2

    .line 492
    const/4 v9, 0x0

    aput-byte v9, v0, v7

    .line 491
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 495
    .end local v7    # "i":I
    :cond_5
    sget-object v9, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    aget-object v0, v9, v1

    .line 496
    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/4 v12, 0x0

    aput-byte v12, v0, v11

    aput-byte v12, v0, v10

    aput-byte v12, v0, v9

    goto/16 :goto_2

    .line 505
    .restart local v7    # "i":I
    :cond_6
    const/4 v7, 0x0

    :goto_5
    const/4 v9, 0x3

    if-ge v7, v9, :cond_7

    .line 506
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v11, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "SA_STRENGH_INDEX_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v10, v7, 0x7

    aget-byte v10, v0, v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 505
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 511
    .end local v0    # "Array_EQ_Strengh":[B
    .end local v1    # "Position":I
    .end local v6    # "genreIdx":I
    .end local v7    # "i":I
    :cond_7
    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct {p0, v9, v10, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadPrevPreferences(Landroid/os/Bundle;ZZ)V

    .line 513
    iget-boolean v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptEnable:Z

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    if-eqz v9, :cond_8

    .line 514
    const-string v9, "SA_ADAPT_STATE"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 515
    .local v2, "adpatState":Z
    new-instance v7, Landroid/content/Intent;

    const-string v9, "internal_SA_Adapt_changed"

    invoke-direct {v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 516
    .local v7, "i":Landroid/content/Intent;
    const-string v9, "SA_AdaptSound_State"

    invoke-virtual {v7, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 517
    invoke-virtual {p0, v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 520
    .end local v2    # "adpatState":Z
    .end local v7    # "i":Landroid/content/Intent;
    :cond_8
    iget v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    if-ltz v9, :cond_9

    iget v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    const/16 v10, 0x18

    if-gt v9, v10, :cond_9

    .line 522
    iget v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    invoke-direct {p0, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V

    .line 523
    iget v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    invoke-direct {p0, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveUsePreset(I)V

    .line 524
    const/4 v9, 0x0

    iput v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    .line 525
    const-string v9, "SA_TAB_INDEX"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 529
    :cond_9
    const/4 v9, 0x1

    iput v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    .line 530
    const-string v9, "SA_TAB_INDEX"

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 532
    const/4 v9, 0x7

    new-array v5, v9, [I

    .line 534
    .local v5, "currentUserEq":[I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_6
    const/4 v9, 0x7

    if-ge v7, v9, :cond_a

    .line 535
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v11, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "SA_EQ_INDEX_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v9

    aput v9, v5, v7

    .line 534
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 538
    :cond_a
    invoke-direct {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveBandLevel([I)V

    .line 539
    iget v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    invoke-direct {p0, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveUsePreset(I)V

    .line 541
    const/4 v9, 0x3

    new-array v4, v9, [I

    .line 543
    .local v4, "currentStrength":[I
    const/4 v7, 0x0

    :goto_7
    const/4 v9, 0x3

    if-ge v7, v9, :cond_0

    .line 544
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v11, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "SA_STRENGH_INDEX_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v9

    aput v9, v4, v7

    .line 545
    aget v9, v4, v7

    if-eqz v9, :cond_b

    .line 546
    const/4 v9, 0x1

    invoke-direct {p0, v7, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setStrength(II)V

    .line 550
    :goto_8
    iget v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    invoke-direct {p0, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveUsePreset(I)V

    .line 543
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 548
    :cond_b
    const/4 v9, 0x2

    invoke-direct {p0, v7, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setStrength(II)V

    goto :goto_8

    .line 458
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;IZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # Landroid/widget/GridView;
    .param p2, "x2"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    return-object v0
.end method

.method static synthetic access$1700()[I
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateAutoPlay(Z)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAdapt(Z)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSelectedTab(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateStrength()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isPortrait()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsBubbleRunable:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsBubbleRunable:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mLastControlledTab:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mDbLinePaddingTop:I

    return v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getScreenRawWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D

    return-wide v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # D

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D

    return-wide p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)D
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D

    return-wide v0
.end method

.method static synthetic access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # D

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D

    return-wide p1
.end method

.method static synthetic access$3700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getDensity()I

    move-result v0

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSeekBarHeight:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setStrength(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/widget/GridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # Landroid/widget/GridView;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setUsePresetEffect(IZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    return-object v0
.end method

.method private clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    .locals 6
    .param p1, "v"    # Landroid/widget/GridView;

    .prologue
    .line 1074
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_0

    .line 1075
    const-string v4, "SAControlPanelActivity"

    const-string v5, "*****clearAllsoundAlivePresetCell()*****"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1076
    :cond_0
    invoke-virtual {p1}, Landroid/widget/GridView;->getChildCount()I

    move-result v3

    .line 1077
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_2

    .line 1078
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1079
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1080
    const v4, 0x7f09002e

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1082
    .local v2, "image":Landroid/widget/ImageView;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1083
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    .line 1077
    .end local v2    # "image":Landroid/widget/ImageView;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1086
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    .line 1087
    return-void
.end method

.method private clearAllsoundAlivePresetCellNotEffect(Landroid/widget/GridView;)V
    .locals 6
    .param p1, "v"    # Landroid/widget/GridView;

    .prologue
    .line 1090
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_0

    .line 1091
    const-string v4, "SAControlPanelActivity"

    const-string v5, "*****clearAllsoundAlivePresetCell()*****"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    :cond_0
    invoke-virtual {p1}, Landroid/widget/GridView;->getChildCount()I

    move-result v3

    .line 1093
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_2

    .line 1094
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1095
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1096
    const v4, 0x7f09002e

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1098
    .local v2, "image":Landroid/widget/ImageView;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1099
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    .line 1093
    .end local v2    # "image":Landroid/widget/ImageView;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1102
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V
    .locals 3
    .param p1, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 877
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 878
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****clearEffectBottomLineImage()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    :cond_0
    const v1, 0x7f09001c

    invoke-virtual {p1, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 880
    .local v0, "image":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 882
    const v1, 0x7f070036

    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 883
    return-void
.end method

.method private clearEffectImage()V
    .locals 3

    .prologue
    .line 859
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 860
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****clearEffectImage()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 862
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 863
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctNone:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 864
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctStudio:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 865
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctClub:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 866
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctConcertHall:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 868
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 869
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 871
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 872
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 873
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 874
    return-void
.end method

.method private drawDBLine()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2028
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_0

    .line 2029
    const-string v4, "SAControlPanelActivity"

    const-string v5, "*****drawDBLine()*****"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2030
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f05000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSeekBarHeight:I

    .line 2031
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v4, v4, v6

    if-eqz v4, :cond_1

    .line 2032
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v4, v4, v6

    invoke-virtual {v4}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->getPaddingTop()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mDbLinePaddingTop:I

    .line 2035
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedLayout:Landroid/view/ViewGroup;

    const v5, 0x7f09000e

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2036
    .local v0, "lineEqLayout":Landroid/widget/LinearLayout;
    new-instance v3, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;

    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/content/Context;)V

    .line 2037
    .local v3, "lineEqView":Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2039
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedLayout:Landroid/view/ViewGroup;

    const v5, 0x7f09000f

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2040
    .local v1, "lineEqLevelLine":Landroid/widget/LinearLayout;
    new-instance v2, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;

    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/content/Context;)V

    .line 2041
    .local v2, "lineEqLevelView":Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqLevelView;
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2043
    return-void
.end method

.method private getAdaptState(Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 575
    const/4 v0, 0x1

    .line 576
    .local v0, "adpatState":Z
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isEnableAdaptSoundPath()Z

    move-result v3

    if-nez v3, :cond_0

    .line 585
    :goto_0
    return v2

    .line 579
    :cond_0
    if-eqz p1, :cond_1

    .line 580
    const-string v2, "SA_ADAPT_STATE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_1
    move v2, v0

    .line 585
    goto :goto_0

    .line 582
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v1

    .line 583
    .local v1, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v3, "SA_ADAPT_STATE"

    invoke-virtual {v1, v3, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_1
.end method

.method private getAutoState(Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 589
    const/4 v0, 0x1

    .line 590
    .local v0, "autoState":Z
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 599
    :goto_0
    return v2

    .line 593
    :cond_0
    if-eqz p1, :cond_1

    .line 594
    const-string v2, "SA_AUTO_STATE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_1
    move v2, v0

    .line 599
    goto :goto_0

    .line 596
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v1

    .line 597
    .local v1, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v3, "SA_AUTO_STATE"

    invoke-virtual {v1, v3, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_1
.end method

.method private getBandLevelFromUI()[I
    .locals 7

    .prologue
    .line 2197
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_0

    .line 2198
    const-string v2, "SAControlPanelActivity"

    const-string v3, "*****getBandLevelFromUI()*****"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2199
    :cond_0
    const/4 v2, 0x7

    new-array v0, v2, [I

    .line 2201
    .local v0, "eqValue":[I
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 2202
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 2203
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v2, v2, v1

    if-eqz v2, :cond_1

    .line 2204
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->getProgress()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    aput v2, v0, v1

    .line 2205
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v2, v2, v1

    const-string v3, "%s %s %d db %s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const v6, 0x7f070035

    invoke-virtual {p0, v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    aget v6, v6, v1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aget v6, v0, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const v6, 0x7f070038

    invoke-virtual {p0, v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2202
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2210
    .end local v1    # "i":I
    :cond_2
    return-object v0
.end method

.method private getDensity()I
    .locals 1

    .prologue
    .line 2595
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    return v0
.end method

.method private getScreenRawWidth()I
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 2586
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2588
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2589
    .local v1, "point":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2590
    iget v2, v1, Landroid/graphics/Point;->x:I

    return v2
.end method

.method private getStrengthValueFromUI()[I
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2214
    sget-boolean v5, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v5, :cond_0

    .line 2215
    const-string v5, "SAControlPanelActivity"

    const-string v6, "*****getStrengthValue()*****"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2216
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v3

    .line 2217
    .local v0, "strength_3D":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_2

    move v1, v3

    .line 2218
    .local v1, "strength_Bass":I
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v3

    .line 2220
    .local v2, "strength_Clarity":I
    :goto_2
    const/4 v5, 0x3

    new-array v5, v5, [I

    aput v0, v5, v4

    aput v1, v5, v3

    const/4 v3, 0x2

    aput v2, v5, v3

    return-object v5

    .end local v0    # "strength_3D":I
    .end local v1    # "strength_Bass":I
    .end local v2    # "strength_Clarity":I
    :cond_1
    move v0, v4

    .line 2216
    goto :goto_0

    .restart local v0    # "strength_3D":I
    :cond_2
    move v1, v4

    .line 2217
    goto :goto_1

    .restart local v1    # "strength_Bass":I
    :cond_3
    move v2, v4

    .line 2218
    goto :goto_2
.end method

.method private initAdvancedEffectButton()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 679
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 680
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****initAdvancedEffectButton()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :cond_0
    const v0, 0x7f090018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    .line 682
    const v0, 0x7f090019

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    .line 683
    const v0, 0x7f09001a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    .line 685
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 686
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 687
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 689
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$1;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$2;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 730
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$3;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 750
    return-void
.end method

.method private initEffectList()V
    .locals 2

    .prologue
    .line 892
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 893
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****initEffectList()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    :cond_0
    const v0, 0x7f090022

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    .line 895
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$4;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 904
    const v0, 0x7f09001b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    .line 905
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$5;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 915
    const v0, 0x7f090024

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    .line 916
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$6;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 926
    const v0, 0x7f090026

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    .line 927
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$7;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 937
    const v0, 0x7f09001e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    .line 938
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$8;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 950
    const v0, 0x7f090020

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    .line 951
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$9;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 962
    return-void
.end method

.method private initLayout(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f07003b

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1772
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 1773
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****initLayout()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1774
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBasicButton:Landroid/widget/TextView;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1775
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBasicButton:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$13;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1781
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedButton:Landroid/widget/TextView;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1782
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedButton:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$14;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1797
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$15;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1827
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$16;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1862
    if-eqz p1, :cond_1

    .line 1863
    const-string v0, "SA_TAB_INDEX"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSelectedTab(I)V

    .line 1868
    :goto_0
    return-void

    .line 1865
    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSelectedTab(I)V

    goto :goto_0
.end method

.method private initSoundAliveGridView()V
    .locals 2

    .prologue
    .line 966
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 967
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****initSoundAliveGridView()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 968
    :cond_0
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    .line 969
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;

    invoke-direct {v1, p0, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$SoundAliveAdapter;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 970
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->matchAlign()V

    .line 972
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$10;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 985
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$11;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1071
    return-void
.end method

.method private isPortrait()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2575
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 2576
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****isPortrait()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2577
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_1

    .line 2580
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSoundAliveSupportCurrentAudioPath(IZ)Z
    .locals 2
    .param p1, "soundAlive"    # I
    .param p2, "enableToast"    # Z

    .prologue
    .line 886
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 887
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****isSoundAliveSupportCurrentAudioPath()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    :cond_0
    new-instance v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isSoundAliveSupportCurrentAudioPath(Landroid/content/Context;IZ)Z

    move-result v0

    return v0
.end method

.method private loadAudioPath(Landroid/os/Bundle;Z)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z

    .prologue
    .line 603
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 604
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****loadAudioPath()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :cond_0
    if-eqz p1, :cond_1

    .line 606
    const-string v1, "SA_CURRENT_PATH_INDEX"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 612
    :goto_0
    return-void

    .line 608
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v0

    .line 609
    .local v0, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v1, "SA_CURRENT_PATH_INDEX"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    goto :goto_0
.end method

.method private loadBandLevelPref(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v10, 0x7

    .line 2224
    sget-boolean v6, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v6, :cond_0

    .line 2225
    const-string v6, "SAControlPanelActivity"

    const-string v8, "*****loadBandLevelPref()*****"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2226
    :cond_0
    new-array v0, v10, [I

    .line 2227
    .local v0, "currentUserEq":[I
    if-eqz p1, :cond_3

    .line 2228
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v9, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v8, v8, v9

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "SA_EQ_INDEX_ARRAY"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 2229
    if-eqz v0, :cond_1

    .line 2230
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v10, :cond_2

    .line 2231
    const-string v6, "SAControlPanelActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "from savedInstanceState currentUserEq"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2230
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2234
    .end local v3    # "i":I
    :cond_1
    const-string v6, "SAControlPanelActivity"

    const-string v8, "currentUserEq is null"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2244
    :cond_2
    new-array v2, v10, [Landroid/view/View;

    .line 2245
    .local v2, "eqControlView":[Landroid/view/View;
    const/4 v1, 0x0

    .line 2246
    .local v1, "eqControlText":Landroid/widget/TextView;
    new-instance v5, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Lcom/sec/android/app/soundalive/SAControlPanelActivity$1;)V

    .line 2247
    .local v5, "uEqControlbarListener":Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    if-ge v3, v10, :cond_5

    .line 2249
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedLayout:Landroid/view/ViewGroup;

    sget-object v8, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_SEEKBAR_RES_ID:[I

    aget v8, v8, v3

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    aput-object v6, v2, v3

    .line 2250
    aget-object v6, v2, v3

    const v8, 0x7f090031

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "eqControlText":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 2251
    .restart local v1    # "eqControlText":Landroid/widget/TextView;
    sget-object v6, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    aget v6, v6, v3

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 2254
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v6, v2, v3

    const v9, 0x7f090030

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aput-object v6, v8, v3

    .line 2255
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v6, v6, v3

    sget-object v8, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    aget v8, v8, v3

    invoke-virtual {v6, v8}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setTitle(I)V

    .line 2256
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z

    .line 2257
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v8, v6, v3

    if-nez v0, :cond_4

    move v6, v7

    :goto_2
    add-int/lit8 v6, v6, 0xa

    invoke-virtual {v8, v6}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setProgress(I)V

    .line 2259
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v6, v6, v3

    const/16 v8, 0xa

    invoke-virtual {v6, v8}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setBaseValue(I)V

    .line 2260
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v6, v6, v3

    invoke-virtual {v6, v5}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setOnTwSeekBarChangeListener(Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;)V

    .line 2264
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v6, v6, v3

    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2265
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v6, v6, v3

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setContentDescription()V

    .line 2247
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2237
    .end local v1    # "eqControlText":Landroid/widget/TextView;
    .end local v2    # "eqControlView":[Landroid/view/View;
    .end local v3    # "i":I
    .end local v5    # "uEqControlbarListener":Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v4

    .line 2239
    .local v4, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    if-ge v3, v10, :cond_2

    .line 2240
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v9, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v8, v8, v9

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "SA_EQ_INDEX_"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v6

    aput v6, v0, v3

    .line 2241
    const-string v6, "SAControlPanelActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "from SharedPreferences currentUserEq"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ":"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2239
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 2257
    .end local v4    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    .restart local v1    # "eqControlText":Landroid/widget/TextView;
    .restart local v2    # "eqControlView":[Landroid/view/View;
    .restart local v5    # "uEqControlbarListener":Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqControlBarChangeListener;
    :cond_4
    aget v6, v0, v3

    goto :goto_2

    .line 2267
    :cond_5
    return-void
.end method

.method private loadPrevPreferences(Landroid/os/Bundle;ZZ)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z
    .param p3, "isCancel"    # Z

    .prologue
    .line 556
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 557
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****loadPrevPreferences()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getAutoState(Landroid/os/Bundle;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V

    .line 560
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getAdaptState(Landroid/os/Bundle;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAdapt(Z)V

    .line 561
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadAudioPath(Landroid/os/Bundle;Z)V

    .line 563
    if-eqz p2, :cond_1

    .line 564
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadUsePresetPref(Landroid/os/Bundle;Z)V

    .line 565
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadBandLevelPref(Landroid/os/Bundle;)V

    .line 566
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadSquarePositionPref(Landroid/os/Bundle;Z)V

    .line 567
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadStrengthPref(Landroid/os/Bundle;Z)V

    .line 568
    iget v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updatePreset(I)V

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCellNotEffect(Landroid/widget/GridView;)V

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    iget v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    .line 573
    :cond_1
    return-void
.end method

.method private loadSquarePositionPref(Landroid/os/Bundle;Z)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z

    .prologue
    .line 615
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 616
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****loadSquarePositionPref()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    :cond_0
    if-eqz p1, :cond_1

    .line 619
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v3, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SA_SQUARE_INDEX"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    .line 627
    :goto_0
    return-void

    .line 620
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 621
    const-string v1, "SAControlPanelActivity"

    const-string v2, "updateAutoPlay in loadSquarePositionPref()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateAutoPlay(Z)V

    goto :goto_0

    .line 624
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v0

    .line 625
    .local v0, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v3, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SA_SQUARE_INDEX"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    goto :goto_0
.end method

.method private loadStrengthPref(Landroid/os/Bundle;Z)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 642
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v3, :cond_0

    .line 643
    const-string v3, "SAControlPanelActivity"

    const-string v4, "*****loadStrengthPref()*****"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :cond_0
    new-array v0, v8, [I

    .line 647
    .local v0, "currentStrength":[I
    if-eqz p1, :cond_3

    iget-boolean v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsCancel:Z

    if-nez v3, :cond_3

    .line 648
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v5, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SA_STRENGH_INDEX_ARRAY"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 654
    :cond_1
    if-eqz v0, :cond_9

    .line 655
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_a

    .line 656
    if-nez v1, :cond_5

    .line 657
    aget v3, v0, v1

    if-nez v3, :cond_4

    .line 658
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 655
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 650
    .end local v1    # "i":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v2

    .line 651
    .local v2, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, v8, :cond_1

    .line 652
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v5, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SA_STRENGH_INDEX_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v0, v1

    .line 651
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 660
    .end local v2    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1

    .line 661
    :cond_5
    if-ne v1, v7, :cond_7

    .line 662
    aget v3, v0, v1

    if-nez v3, :cond_6

    .line 663
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1

    .line 665
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1

    .line 666
    :cond_7
    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 667
    aget v3, v0, v1

    if-nez v3, :cond_8

    .line 668
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1

    .line 670
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_1

    .line 674
    .end local v1    # "i":I
    :cond_9
    const-string v3, "SAControlPanelActivity"

    const-string v4, "currentStrength is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    :cond_a
    return-void
.end method

.method private loadUsePresetPref(Landroid/os/Bundle;Z)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z

    .prologue
    .line 630
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 631
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****loadUsePresetPref()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    :cond_0
    if-eqz p1, :cond_1

    .line 633
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v3, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SA_PRESET_INDEX"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    .line 639
    :goto_0
    return-void

    .line 635
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v0

    .line 636
    .local v0, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v3, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SA_PRESET_INDEX"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEntranceUsePreset:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    goto :goto_0
.end method

.method private matchAlign()V
    .locals 14
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/high16 v13, 0x40000000    # 2.0f

    .line 1121
    sget-boolean v10, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v10, :cond_0

    .line 1122
    const-string v10, "SAControlPanelActivity"

    const-string v11, "*****matchAlign()*****"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    :cond_0
    const v10, 0x7f09002c

    invoke-virtual {p0, v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1124
    .local v0, "leftTextView":Landroid/widget/TextView;
    const v10, 0x7f09002d

    invoke-virtual {p0, v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1126
    .local v5, "rightTextView":Landroid/widget/TextView;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1127
    .local v3, "rectForLeft":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1129
    .local v4, "rectForRight":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1130
    .local v6, "textForLeft":Ljava/lang/String;
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1132
    .local v7, "textForRight":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 1133
    .local v1, "paintForLeftText":Landroid/graphics/Paint;
    invoke-virtual {v5}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 1135
    .local v2, "paintForRightText":Landroid/graphics/Paint;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    invoke-virtual {v1, v6, v12, v10, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1136
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    invoke-virtual {v2, v7, v12, v10, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1138
    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    .line 1139
    .local v8, "widthForLeftText":F
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v9

    .line 1143
    .local v9, "widthForRightText":F
    iget v10, v4, Landroid/graphics/Rect;->top:I

    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    int-to-float v10, v10

    sub-float v10, v9, v10

    div-float/2addr v10, v13

    sub-float/2addr v10, v13

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 1147
    neg-float v10, v8

    iget v11, v3, Landroid/graphics/Rect;->top:I

    iget v12, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-float v11, v11

    add-float/2addr v10, v11

    div-float/2addr v10, v13

    const/high16 v11, 0x40800000    # 4.0f

    add-float/2addr v10, v11

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 1148
    return-void
.end method

.method private registerReceiver()V
    .locals 2

    .prologue
    .line 1151
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 1153
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1154
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "internal_SA_Advanced_UI_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1155
    const-string v1, "internal_SA_Auto_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1156
    const-string v1, "internal_SA_Auto_UI_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1157
    const-string v1, "internal_SA_Adapt_UI_changed"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1159
    new-instance v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$12;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    .line 1287
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1290
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private resetCurrentValue()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x0

    .line 2296
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_0

    .line 2297
    const-string v2, "SAControlPanelActivity"

    const-string v3, "*****resetCurrentValue()*****"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2299
    :cond_0
    invoke-direct {p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAuto(Z)V

    .line 2300
    iget-boolean v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptEnable:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    if-eqz v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptCheckBoxState:Z

    if-eqz v2, :cond_1

    .line 2301
    invoke-direct {p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAdapt(Z)V

    .line 2302
    new-instance v1, Landroid/content/Intent;

    const-string v2, "internal_SA_Adapt_changed"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2303
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "SA_AdaptSound_State"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2304
    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2307
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_2

    .line 2308
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z

    .line 2309
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v2, v2, v1

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setProgress(I)V

    .line 2307
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2311
    :cond_2
    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x3

    if-ge v1, v2, :cond_3

    .line 2312
    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setStrength(II)V

    .line 2311
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2315
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V

    .line 2316
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    .line 2317
    invoke-direct {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V

    .line 2318
    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setUsePresetEffect(IZ)V

    .line 2319
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBandLevelFromUI()[I

    move-result-object v0

    .line 2320
    .local v0, "eqList":[I
    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveBandLevel([I)V

    .line 2321
    return-void
.end method

.method private saveSoundAlivePreference()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x3

    const/4 v11, 0x7

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1650
    sget-boolean v5, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v5, :cond_0

    .line 1651
    const-string v5, "SAControlPanelActivity"

    const-string v6, "*****saveSoundAlivePreference()*****"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1652
    :cond_0
    iget v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    if-ltz v5, :cond_c

    iget v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    const/16 v6, 0x18

    if-gt v5, v6, :cond_c

    .line 1654
    const/16 v5, 0xa

    new-array v0, v5, [B

    .line 1655
    .local v0, "Array_EQ_Strengh":[B
    sget v5, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-ne v5, v9, :cond_2

    .line 1656
    sget-object v5, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    iget v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    aget-object v0, v5, v6

    .line 1676
    :cond_1
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v11, :cond_5

    .line 1677
    aget-byte v1, v0, v2

    .line 1678
    .local v1, "bandLevel":I
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z

    .line 1679
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v5, v5, v2

    add-int/lit8 v6, v1, 0xa

    invoke-virtual {v5, v6}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setProgress(I)V

    .line 1680
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v5, v5, v2

    const-string v6, "%s %s %d db %s"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const v8, 0x7f070035

    invoke-virtual {p0, v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    sget-object v8, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    aget v8, v8, v2

    invoke-virtual {p0, v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v13

    const v8, 0x7f070038

    invoke-virtual {p0, v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1676
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1657
    .end local v1    # "bandLevel":I
    .end local v2    # "i":I
    :cond_2
    sget v5, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-nez v5, :cond_3

    .line 1658
    sget-object v5, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_MONO_SPK:[[B

    iget v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    aget-object v0, v5, v6

    .line 1660
    invoke-static {}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isStereoSpeakerDevice()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1661
    iget v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    rem-int/lit8 v5, v5, 0x5

    if-nez v5, :cond_1

    .line 1662
    aput-byte v9, v0, v11

    goto :goto_0

    .line 1665
    :cond_3
    sget v5, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_4

    .line 1666
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    const/16 v5, 0xa

    if-ge v2, v5, :cond_1

    .line 1667
    aput-byte v10, v0, v2

    .line 1666
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1670
    .end local v2    # "i":I
    :cond_4
    sget-object v5, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    iget v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    aget-object v0, v5, v6

    .line 1671
    const/16 v5, 0x8

    const/16 v6, 0x9

    aput-byte v10, v0, v6

    aput-byte v10, v0, v5

    aput-byte v10, v0, v11

    goto :goto_0

    .line 1684
    .restart local v2    # "i":I
    :cond_5
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v12, :cond_c

    .line 1685
    if-nez v2, :cond_8

    .line 1686
    add-int/lit8 v5, v2, 0x7

    aget-byte v5, v0, v5

    if-nez v5, :cond_7

    .line 1687
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1684
    :cond_6
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1689
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 1690
    :cond_8
    if-ne v2, v9, :cond_a

    .line 1691
    add-int/lit8 v5, v2, 0x7

    aget-byte v5, v0, v5

    if-nez v5, :cond_9

    .line 1692
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 1694
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 1695
    :cond_a
    if-ne v2, v13, :cond_6

    .line 1696
    add-int/lit8 v5, v2, 0x7

    aget-byte v5, v0, v5

    if-nez v5, :cond_b

    .line 1697
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 1699
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 1705
    .end local v0    # "Array_EQ_Strengh":[B
    .end local v2    # "i":I
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBandLevelFromUI()[I

    move-result-object v1

    .line 1706
    .local v1, "bandLevel":[I
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getStrengthValueFromUI()[I

    move-result-object v4

    .line 1708
    .local v4, "strength":[I
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 1709
    .local v3, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v7, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SA_PRESET_INDEX"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1710
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v7, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SA_SQUARE_INDEX"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1712
    iget-boolean v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    if-eqz v5, :cond_d

    .line 1713
    const-string v5, "SA_AUTO_STATE"

    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    .line 1715
    :cond_d
    iget-boolean v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptEnable:Z

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    if-eqz v5, :cond_e

    .line 1716
    const-string v5, "SA_ADAPT_STATE"

    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    .line 1718
    :cond_e
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    if-ge v2, v12, :cond_f

    .line 1719
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v7, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SA_STRENGH_INDEX_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aget v6, v4, v2

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1718
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1723
    :cond_f
    const/4 v2, 0x0

    :goto_6
    if-ge v2, v11, :cond_10

    .line 1724
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v7, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SA_EQ_INDEX_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aget v6, v1, v2

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1723
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 1727
    :cond_10
    return-void
.end method

.method private selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    .locals 4
    .param p1, "v"    # Landroid/widget/GridView;
    .param p2, "position"    # I

    .prologue
    .line 1105
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_0

    .line 1106
    const-string v2, "SAControlPanelActivity"

    const-string v3, "*****selectSoundAlivePresetCell()*****"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    :cond_0
    if-eqz p1, :cond_1

    .line 1108
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1109
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1110
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 1111
    const v2, 0x7f09002e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1113
    .local v1, "image":Landroid/widget/ImageView;
    const v2, 0x7f040011

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1116
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "image":Landroid/widget/ImageView;
    :cond_1
    return-void
.end method

.method private setButtonStyle(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 10
    .param p1, "btn1"    # Landroid/widget/TextView;
    .param p2, "btn2"    # Landroid/widget/TextView;

    .prologue
    const v9, 0x7f070004

    const v8, 0x7f040015

    const v7, 0x7f040013

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2046
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 2047
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****setButtonStyle()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2049
    :cond_0
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 2050
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2052
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f07003a

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2056
    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 2057
    invoke-virtual {p2, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2059
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f070039

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2061
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2062
    .local v0, "text1":Ljava/lang/String;
    const-string v1, "SAControlPanelActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "btn text1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2063
    const-string v1, "SAControlPanelActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "btn getstring: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2064
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2065
    const-string v1, "SAControlPanelActivity"

    const-string v2, "btn1 select"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2067
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewTabBasicBottomLine:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2068
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewTabAdvancedBottomLine:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2075
    :goto_0
    return-void

    .line 2070
    :cond_1
    const-string v1, "SAControlPanelActivity"

    const-string v2, "btn2 select"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2072
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewTabBasicBottomLine:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2073
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewTabAdvancedBottomLine:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method private setCheckedAdapt(Z)V
    .locals 1
    .param p1, "adapt"    # Z

    .prologue
    .line 2333
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptEnable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 2334
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2335
    sput-boolean p1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptCheckBoxState:Z

    .line 2337
    :cond_0
    return-void
.end method

.method private setCheckedAuto(Z)V
    .locals 2
    .param p1, "auto"    # Z

    .prologue
    .line 2324
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 2325
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****setCheckedAuto()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2326
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 2327
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2328
    sput-boolean p1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxState:Z

    .line 2331
    :cond_1
    return-void
.end method

.method private setNewSoundAliveBandLevel([I)V
    .locals 5
    .param p1, "bandLevel"    # [I

    .prologue
    .line 421
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_0

    .line 422
    const-string v2, "SAControlPanelActivity"

    const-string v3, "*****setNewSoundAliveBandLevel()*****"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :cond_0
    if-eqz p1, :cond_5

    .line 426
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_3

    .line 427
    aget v1, p1, v0

    .line 428
    .local v1, "level":I
    const/16 v2, -0xa

    if-lt v1, v2, :cond_2

    const/16 v2, 0xa

    if-gt v1, v2, :cond_2

    .line 429
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v2, :cond_1

    .line 430
    const-string v2, "SAControlPanelActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "level : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 432
    :cond_2
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v2, :cond_1

    .line 433
    const-string v2, "SAControlPanelActivity"

    const-string v3, "bandLevel is out of bounds"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 437
    .end local v1    # "level":I
    :cond_3
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "i":I
    const-string v2, "internal_SA_EQ_changed"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 438
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "SA_EQ_Array"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 439
    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 444
    .end local v0    # "i":Landroid/content/Intent;
    :cond_4
    :goto_2
    return-void

    .line 441
    :cond_5
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v2, :cond_4

    .line 442
    const-string v2, "SAControlPanelActivity"

    const-string v3, "mSoundAlive or bandLevel is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private setNewSoundAliveStrength(II)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "onoff"    # I

    .prologue
    const/4 v4, 0x2

    .line 392
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 393
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****setNewSoundAliveStrength()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_0
    if-ltz p1, :cond_6

    if-gt p1, v4, :cond_6

    .line 396
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_1

    .line 397
    const-string v1, "SAControlPanelActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_1
    if-nez p1, :cond_4

    .line 399
    new-instance v0, Landroid/content/Intent;

    const-string v1, "internal_SA_3D_changed"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 400
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "SA_Strength_3D"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 401
    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 412
    .end local v0    # "i":Landroid/content/Intent;
    :cond_2
    :goto_0
    const-string v1, "SAControlPanelActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onoff : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    :cond_3
    :goto_1
    return-void

    .line 402
    :cond_4
    const/4 v1, 0x1

    if-ne p1, v1, :cond_5

    .line 403
    new-instance v0, Landroid/content/Intent;

    const-string v1, "internal_SA_Bass_changed"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 404
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v1, "SA_Strength_BASS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 405
    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 406
    .end local v0    # "i":Landroid/content/Intent;
    :cond_5
    if-ne p1, v4, :cond_2

    .line 407
    new-instance v0, Landroid/content/Intent;

    const-string v1, "internal_SA_Clarity_changed"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 408
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v1, "SA_Strength_Clarity"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 409
    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 415
    .end local v0    # "i":Landroid/content/Intent;
    :cond_6
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_3

    .line 416
    const-string v1, "SAControlPanelActivity"

    const-string v2, "mSoundAlive is null or unknown id"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setNewSoundAliveUsePreset(I)V
    .locals 4
    .param p1, "selectedUsePreset"    # I

    .prologue
    .line 373
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 374
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****setNewSoundAliveUsePreset()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_0
    if-ltz p1, :cond_3

    const/4 v1, 0x5

    if-lt v1, p1, :cond_3

    .line 376
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_1

    .line 377
    const-string v1, "SAControlPanelActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectedUsePreset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_1
    iput p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    .line 380
    new-instance v0, Landroid/content/Intent;

    const-string v1, "internal_SA_Preset_changed"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 381
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "SA_Preset"

    iget v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 382
    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 389
    .end local v0    # "i":Landroid/content/Intent;
    :cond_2
    :goto_0
    return-void

    .line 385
    :cond_3
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_2

    .line 386
    const-string v1, "SAControlPanelActivity"

    const-string v2, "selectedUsePreset is out of bounds"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setSelectedTab(I)V
    .locals 13
    .param p1, "tab"    # I

    .prologue
    const/4 v12, 0x2

    const/16 v7, 0x8

    const/4 v11, 0x7

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1949
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_0

    .line 1950
    const-string v4, "SAControlPanelActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*****setSelectedTab()***** : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1951
    :cond_0
    if-nez p1, :cond_2

    .line 1952
    iput v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    .line 1953
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBasicLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1954
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1955
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBasicButton:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedButton:Landroid/widget/TextView;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setButtonStyle(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 1956
    iput-boolean v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mLastControlledTab:Z

    .line 1958
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 1959
    .local v3, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v4, "SA_TAB_INDEX"

    invoke-virtual {v3, v4, v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 2025
    .end local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_1
    return-void

    .line 1960
    :cond_2
    if-ne p1, v9, :cond_1

    .line 1961
    iput v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    .line 1962
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBasicLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1963
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedLayout:Landroid/view/ViewGroup;

    invoke-virtual {v4, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1964
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedButton:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBasicButton:Landroid/widget/TextView;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setButtonStyle(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 1965
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mLastControlledTab:Z

    .line 1967
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 1968
    .restart local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v4, "SA_TAB_INDEX"

    invoke-virtual {v3, v4, v9}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1970
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSavedInstance:Landroid/os/Bundle;

    if-nez v4, :cond_1

    .line 1972
    iget v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    if-ltz v4, :cond_1

    iget v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    const/16 v5, 0x18

    if-gt v4, v5, :cond_1

    .line 1974
    const/16 v4, 0xa

    new-array v0, v4, [B

    .line 1975
    .local v0, "Array_EQ_Strengh":[B
    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-ne v4, v9, :cond_4

    .line 1976
    sget-object v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    iget v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    aget-object v0, v4, v5

    .line 1996
    :cond_3
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v11, :cond_7

    .line 1997
    aget-byte v1, v0, v2

    .line 1998
    .local v1, "bandLevel":I
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z

    .line 1999
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v4, v4, v2

    add-int/lit8 v5, v1, 0xa

    invoke-virtual {v4, v5}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setProgress(I)V

    .line 2000
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v4, v4, v2

    const-string v5, "%s %s %d db %s"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const v7, 0x7f070035

    invoke-virtual {p0, v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    sget-object v7, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    aget v7, v7, v2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v12

    const/4 v7, 0x3

    const v8, 0x7f070038

    invoke-virtual {p0, v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1996
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1977
    .end local v1    # "bandLevel":I
    .end local v2    # "i":I
    :cond_4
    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-nez v4, :cond_5

    .line 1978
    sget-object v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_MONO_SPK:[[B

    iget v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    aget-object v0, v4, v5

    .line 1980
    invoke-static {}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isStereoSpeakerDevice()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1981
    iget v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    rem-int/lit8 v4, v4, 0x5

    if-nez v4, :cond_3

    .line 1982
    aput-byte v9, v0, v11

    goto :goto_0

    .line 1985
    :cond_5
    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_6

    .line 1986
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    const/16 v4, 0xa

    if-ge v2, v4, :cond_3

    .line 1987
    aput-byte v10, v0, v2

    .line 1986
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1990
    .end local v2    # "i":I
    :cond_6
    sget-object v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    iget v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    aget-object v0, v4, v5

    .line 1991
    const/16 v4, 0x9

    aput-byte v10, v0, v4

    aput-byte v10, v0, v7

    aput-byte v10, v0, v11

    goto :goto_0

    .line 2004
    .restart local v2    # "i":I
    :cond_7
    const/4 v2, 0x0

    :goto_3
    const/4 v4, 0x3

    if-ge v2, v4, :cond_1

    .line 2005
    if-nez v2, :cond_a

    .line 2006
    add-int/lit8 v4, v2, 0x7

    aget-byte v4, v0, v4

    if-nez v4, :cond_9

    .line 2007
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setSelected(Z)V

    .line 2004
    :cond_8
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2009
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 2010
    :cond_a
    if-ne v2, v9, :cond_c

    .line 2011
    add-int/lit8 v4, v2, 0x7

    aget-byte v4, v0, v4

    if-nez v4, :cond_b

    .line 2012
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 2014
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 2015
    :cond_c
    if-ne v2, v12, :cond_8

    .line 2016
    add-int/lit8 v4, v2, 0x7

    aget-byte v4, v0, v4

    if-nez v4, :cond_d

    .line 2017
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4

    .line 2019
    :cond_d
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_4
.end method

.method private setSquareEffect(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 353
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 354
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****setSquareEffect()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_0
    if-ltz p1, :cond_3

    const/16 v1, 0x18

    if-gt p1, v1, :cond_3

    .line 356
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_1

    .line 357
    const-string v1, "SAControlPanelActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :cond_1
    iput p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    .line 360
    new-instance v0, Landroid/content/Intent;

    const-string v1, "internal_SA_Square_changed"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 361
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "SA_SquarePosition"

    iget v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 362
    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 370
    .end local v0    # "i":Landroid/content/Intent;
    :cond_2
    :goto_0
    return-void

    .line 366
    :cond_3
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_2

    .line 367
    const-string v1, "SAControlPanelActivity"

    const-string v2, "setSquareEffect() not applied because position value is out of bounds"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setStrength(II)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "mode"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 762
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 763
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****setStrength()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    :cond_0
    const/4 v0, 0x0

    .line 766
    .local v0, "button":Landroid/widget/TextView;
    packed-switch p1, :pswitch_data_0

    .line 807
    :cond_1
    :goto_0
    return-void

    .line 768
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    .line 780
    :goto_1
    if-eqz v0, :cond_1

    .line 781
    if-nez p2, :cond_4

    .line 782
    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 783
    invoke-direct {p0, p1, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveStrength(II)V

    .line 784
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 800
    :cond_2
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f070034

    invoke-virtual {p0, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_8

    const v1, 0x7f070037

    :goto_3
    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 771
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    .line 772
    goto :goto_1

    .line 774
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    .line 775
    goto :goto_1

    .line 786
    :cond_3
    invoke-direct {p0, p1, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveStrength(II)V

    .line 787
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_2

    .line 789
    :cond_4
    if-ne p2, v3, :cond_6

    .line 790
    iget v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    if-ne v1, v3, :cond_5

    .line 791
    invoke-direct {p0, p1, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveStrength(II)V

    .line 793
    :cond_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_2

    .line 794
    :cond_6
    const/4 v1, 0x2

    if-ne p2, v1, :cond_2

    .line 795
    iget v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    if-ne v1, v3, :cond_7

    .line 796
    invoke-direct {p0, p1, v4}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveStrength(II)V

    .line 798
    :cond_7
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_2

    .line 800
    :cond_8
    const v1, 0x7f070036

    goto :goto_3

    .line 766
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setUsePresetEffect(IZ)V
    .locals 3
    .param p1, "selectedUsePreset"    # I
    .param p2, "onlyRefreshUI"    # Z

    .prologue
    .line 810
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 811
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****setUsePresetEffect()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearEffectImage()V

    .line 815
    packed-switch p1, :pswitch_data_0

    .line 843
    :goto_0
    if-nez p2, :cond_1

    .line 844
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveUsePreset(I)V

    .line 846
    :cond_1
    iput p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    .line 847
    return-void

    .line 817
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctClub:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 818
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 821
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctConcertHall:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 825
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctNone:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 829
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctStudio:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 833
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 834
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 837
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 838
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto/16 :goto_0

    .line 815
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private unregisterReceiver()V
    .locals 1

    .prologue
    .line 1293
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1294
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    .line 1297
    :cond_0
    return-void
.end method

.method private updateAutoPlay(Z)V
    .locals 6
    .param p1, "isReload"    # Z

    .prologue
    .line 1730
    iget-boolean v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1731
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v2

    .line 1733
    .local v2, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v3, "SA_GENRE_INDEX"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1735
    .local v0, "genre":I
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v3, :cond_0

    .line 1736
    const-string v3, "SAControlPanelActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "*****updateAutoPlay()***** isReload : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1738
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 1755
    :pswitch_0
    const/16 v1, 0xc

    .line 1759
    .local v1, "position":I
    :goto_0
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v3, :cond_1

    .line 1760
    const-string v3, "SAControlPanelActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateAutoPlay genre : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1762
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V

    .line 1763
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v3, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    .line 1764
    iput v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    .line 1766
    if-eqz p1, :cond_2

    .line 1767
    invoke-direct {p0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setSquareEffect(I)V

    .line 1769
    .end local v0    # "genre":I
    .end local v1    # "position":I
    .end local v2    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_2
    return-void

    .line 1740
    .restart local v0    # "genre":I
    .restart local v2    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :pswitch_1
    const/16 v1, 0xc

    .line 1741
    .restart local v1    # "position":I
    goto :goto_0

    .line 1743
    .end local v1    # "position":I
    :pswitch_2
    const/16 v1, 0xd

    .line 1744
    .restart local v1    # "position":I
    goto :goto_0

    .line 1746
    .end local v1    # "position":I
    :pswitch_3
    const/16 v1, 0xa

    .line 1747
    .restart local v1    # "position":I
    goto :goto_0

    .line 1749
    .end local v1    # "position":I
    :pswitch_4
    const/16 v1, 0x10

    .line 1750
    .restart local v1    # "position":I
    goto :goto_0

    .line 1752
    .end local v1    # "position":I
    :pswitch_5
    const/4 v1, 0x2

    .line 1753
    .restart local v1    # "position":I
    goto :goto_0

    .line 1738
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V
    .locals 3
    .param p1, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 850
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 851
    const-string v1, "SAControlPanelActivity"

    const-string v2, "*****updateEffectBottomLineImage()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :cond_0
    const v1, 0x7f09001c

    invoke-virtual {p1, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 853
    .local v0, "image":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f04000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 855
    const v1, 0x7f070037

    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 856
    return-void
.end method

.method private updateStrength()V
    .locals 4

    .prologue
    .line 1897
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_0

    .line 1898
    const-string v2, "SAControlPanelActivity"

    const-string v3, "*****updateStrength()*****"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1900
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getStrengthValueFromUI()[I

    move-result-object v0

    .line 1902
    .local v0, "currentStrength":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    .line 1904
    aget v2, v0, v1

    if-eqz v2, :cond_1

    .line 1905
    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setStrength(II)V

    .line 1902
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1907
    :cond_1
    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setStrength(II)V

    goto :goto_1

    .line 1910
    :cond_2
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1636
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 1637
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****onBackPressed()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1639
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 1640
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->saveSoundAlivePreference()V

    .line 1641
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->finish()V

    .line 1642
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi",
            "ResourceAsColor"
        }
    .end annotation

    .prologue
    const v12, 0x7f090028

    const/16 v11, 0x8

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 241
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    .line 244
    sget-boolean v5, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v5, :cond_0

    .line 245
    const-string v5, "SAControlPanelActivity"

    const-string v6, "onCreate() intent action is null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 247
    .local v2, "i":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->startActivity(Landroid/content/Intent;)V

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->finish()V

    .line 340
    .end local v2    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 252
    :cond_1
    sget-boolean v5, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v5, :cond_2

    .line 253
    const-string v5, "SAControlPanelActivity"

    const-string v6, "*****onCreate()*****"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, "CallingPackageName":Ljava/lang/String;
    if-eqz v0, :cond_6

    const-string v5, "SAControlPanelActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mCallingPackageName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :goto_1
    const-string v5, "com.sec.android.app.music"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 261
    iput-boolean v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    .line 262
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptEnable:Z

    .line 267
    :goto_2
    if-eqz p1, :cond_3

    .line 268
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSavedInstance:Landroid/os/Bundle;

    .line 271
    :cond_3
    sput-boolean v8, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sEntrance:Z

    .line 272
    iput-object p0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;

    .line 273
    new-instance v5, Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    .line 275
    const-string v5, "TRUE"

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v6

    const-string v7, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v6, v7}, Lcom/samsung/android/feature/FloatingFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 276
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mContext:Landroid/content/Context;

    const-string v6, "SDAL"

    invoke-static {v5, v6, v0}, Lcom/sec/android/app/soundalive/util/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_4
    const/high16 v5, 0x7f030000

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setContentView(I)V

    .line 284
    const v5, 0x7f090017

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBasicLayout:Landroid/view/ViewGroup;

    .line 285
    const v5, 0x7f09000d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedLayout:Landroid/view/ViewGroup;

    .line 287
    const v5, 0x7f090005

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBasicButton:Landroid/widget/TextView;

    .line 288
    const v5, 0x7f090008

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdvancedButton:Landroid/widget/TextView;

    .line 290
    const v5, 0x7f09000c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    .line 291
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    const v6, 0x7f020016

    invoke-virtual {v5, v6}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubbleBackgroundDrawable(I)V

    .line 292
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    const v6, 0x7f040001

    invoke-virtual {v5, v6}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubbleFontColor(I)V

    .line 293
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050004

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubbleFontSize(F)V

    .line 294
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->setBubbleAlign(I)V

    .line 296
    const v5, 0x7f090006

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewTabBasicBottomLine:Landroid/view/View;

    .line 297
    const v5, 0x7f090009

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewTabAdvancedBottomLine:Landroid/view/View;

    .line 298
    const v5, 0x7f09001f

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

    .line 299
    const v5, 0x7f090021

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

    .line 300
    const v5, 0x7f09001d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctNone:Landroid/widget/ImageView;

    .line 301
    const v5, 0x7f090023

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctStudio:Landroid/widget/ImageView;

    .line 302
    const v5, 0x7f090025

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctClub:Landroid/widget/ImageView;

    .line 303
    const v5, 0x7f090027

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctConcertHall:Landroid/widget/ImageView;

    .line 305
    invoke-virtual {p0, v12}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    .line 306
    const v5, 0x7f090029

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    .line 310
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initLayout(Landroid/os/Bundle;)V

    .line 311
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initSoundAliveGridView()V

    .line 312
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initEffectList()V

    .line 313
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->initAdvancedEffectButton()V

    .line 314
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->drawDBLine()V

    .line 316
    invoke-direct {p0, p1, v8, v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadPrevPreferences(Landroid/os/Bundle;ZZ)V

    .line 318
    iget-boolean v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    if-eqz v5, :cond_8

    .line 319
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAdapt:Landroid/widget/CheckBox;

    invoke-virtual {v5, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 323
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v8, :cond_5

    .line 324
    iget-boolean v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    if-eqz v5, :cond_9

    .line 325
    const v5, 0x7f09002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 326
    .local v1, "checkBoxLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 327
    .local v4, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v4, v10, v12}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 336
    .end local v1    # "checkBoxLayout":Landroid/widget/RelativeLayout;
    .end local v4    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 337
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v5, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto/16 :goto_0

    .line 258
    .end local v3    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_6
    const-string v5, "SAControlPanelActivity"

    const-string v6, "mCallingPackageName = null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 264
    :cond_7
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    .line 265
    iput-boolean v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptEnable:Z

    goto/16 :goto_2

    .line 321
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v5, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_3

    .line 330
    :cond_9
    const v5, 0x7f09002a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 331
    .restart local v1    # "checkBoxLayout":Landroid/widget/RelativeLayout;
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 332
    .restart local v4    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    const v5, 0x7f090029

    invoke-virtual {v4, v10, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const v5, 0x7f070005

    const/4 v4, 0x6

    const/4 v3, 0x1

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1572
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isPortrait()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1573
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRenderingMode:Z

    if-ne v0, v3, :cond_0

    .line 1574
    const v0, 0x7f070027

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1575
    invoke-interface {p1, v1, v3, v3, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1576
    const v0, 0x7f07000a

    invoke-interface {p1, v1, v2, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1586
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 1580
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRenderingMode:Z

    if-ne v0, v3, :cond_0

    .line 1581
    const v0, 0x7f070027

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1582
    invoke-interface {p1, v1, v3, v3, v5}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1583
    const v0, 0x7f07000a

    invoke-interface {p1, v1, v2, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1310
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 1311
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****onDestroy()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1313
    :cond_0
    sput-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sEntrance:Z

    .line 1314
    sput-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxState:Z

    .line 1315
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1317
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2600
    const/16 v0, 0x52

    if-ne v0, p1, :cond_0

    .line 2601
    const/4 v0, 0x1

    .line 2603
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 1594
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1622
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 1596
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->resetCurrentValue()V

    goto :goto_0

    .line 1607
    :sswitch_1
    iput-boolean v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsCancel:Z

    .line 1608
    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EffectReconfigByPreviousSettings(Z)V

    .line 1611
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->finish()V

    goto :goto_0

    .line 1618
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->saveSoundAlivePreference()V

    .line 1619
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->finish()V

    goto :goto_0

    .line 1594
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x102002c -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1301
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 1302
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****onPause()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1303
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->unregisterReceiver()V

    .line 1304
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsActivityVisible:Z

    .line 1305
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1306
    return-void
.end method

.method protected onResume()V
    .locals 32

    .prologue
    .line 1322
    invoke-super/range {p0 .. p0}, Landroid/app/Activity;->onResume()V

    .line 1324
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v27

    if-nez v27, :cond_2

    .line 1325
    sget-boolean v27, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v27, :cond_0

    .line 1326
    const-string v27, "SAControlPanelActivity"

    const-string v28, "onResume() intent action is null"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    :cond_0
    new-instance v11, Landroid/content/Intent;

    const-class v27, Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1328
    .local v11, "i":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->startActivity(Landroid/content/Intent;)V

    .line 1329
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->finish()V

    .line 1541
    .end local v11    # "i":Landroid/content/Intent;
    :cond_1
    return-void

    .line 1333
    :cond_2
    const/16 v27, 0x1

    sput-boolean v27, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsActivityVisible:Z

    .line 1334
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v27

    const-string v28, "hearing_diagnosis"

    const/16 v29, 0x0

    invoke-static/range {v27 .. v29}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    .line 1335
    .local v6, "DBdata":I
    if-nez v6, :cond_3

    const/16 v27, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setCheckedAdapt(Z)V

    .line 1336
    :cond_3
    sget-boolean v27, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v27, :cond_4

    .line 1337
    const-string v27, "SAControlPanelActivity"

    const-string v28, "*****onResume()*****"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    :cond_4
    sget-boolean v27, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBT_App_Active:Z

    if-eqz v27, :cond_6

    .line 1340
    sget-boolean v27, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v27, :cond_5

    .line 1341
    const-string v27, "SAControlPanelActivity"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "sEntrance: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    sget-boolean v29, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sEntrance:Z

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1342
    :cond_5
    const/16 v27, 0x0

    const/16 v28, 0x1

    const/16 v29, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadPrevPreferences(Landroid/os/Bundle;ZZ)V

    .line 1345
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->registerReceiver()V

    .line 1347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSavedInstance:Landroid/os/Bundle;

    move-object/from16 v27, v0

    if-eqz v27, :cond_7

    .line 1349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSavedInstance:Landroid/os/Bundle;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->loadBandLevelPref(Landroid/os/Bundle;)V

    .line 1350
    const/16 v27, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSavedInstance:Landroid/os/Bundle;

    .line 1356
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v21

    .line 1357
    .local v21, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v27, "SA_SERVICE_STATE"

    const/16 v28, -0x1

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v24

    .line 1360
    .local v24, "svc_state":I
    const/16 v27, 0xc

    move/from16 v0, v24

    move/from16 v1, v27

    if-eq v0, v1, :cond_8

    const/16 v27, 0xb

    move/from16 v0, v24

    move/from16 v1, v27

    if-ne v0, v1, :cond_9

    .line 1361
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v12

    .line 1362
    .local v12, "intent":Landroid/content/Intent;
    const-string v27, "android.media.extra.AUDIO_SESSION"

    const/16 v28, -0x4

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 1364
    .local v5, "CurrentAudioSession":I
    const/16 v27, -0x4

    move/from16 v0, v27

    if-eq v5, v0, :cond_1

    .line 1366
    const-string v27, "AUDIOSESSION_ID"

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1, v5}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1367
    const-string v27, "SA_SERVICE_STATE"

    const/16 v28, 0x9

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1369
    const-string v27, "SAControlPanelActivity"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "startService from SAControlPanelActivity AudioSession : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1371
    new-instance v11, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    const-class v28, Lcom/sec/android/app/soundalive/SAControlPanelService;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1372
    .restart local v11    # "i":Landroid/content/Intent;
    const-string v27, "SA_SERVICE_STATE"

    const/16 v28, 0x9

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1373
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1377
    .end local v5    # "CurrentAudioSession":I
    .end local v11    # "i":Landroid/content/Intent;
    .end local v12    # "intent":Landroid/content/Intent;
    :cond_9
    const/4 v14, 0x0

    .line 1378
    .local v14, "isServiceRunning":Z
    const/16 v7, 0x64

    .line 1379
    .local v7, "MAX_NUM":I
    const/16 v20, 0x0

    .line 1380
    .local v20, "service_list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    const-string v28, "activity"

    invoke-virtual/range {v27 .. v28}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/app/ActivityManager;

    .line 1381
    .local v17, "manager":Landroid/app/ActivityManager;
    if-eqz v17, :cond_b

    .line 1382
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v20

    .line 1383
    if-eqz v20, :cond_b

    .line 1384
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .line 1385
    .local v15, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v15, :cond_b

    .line 1386
    :cond_a
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_b

    .line 1387
    const-class v27, Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v28

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/app/ActivityManager$RunningServiceInfo;

    move-object/from16 v0, v27

    iget-object v0, v0, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 1388
    const/4 v14, 0x1

    .line 1389
    const-string v27, "SAControlPanelActivity"

    const-string v28, "Now service is running"

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1396
    .end local v15    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_b
    if-nez v14, :cond_10

    .line 1397
    const-string v27, "SAControlPanelActivity"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Service is not running & svc_state : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1399
    const/16 v27, -0x1

    move/from16 v0, v24

    move/from16 v1, v27

    if-ne v0, v1, :cond_f

    .line 1401
    const/16 v22, 0xc

    .line 1402
    .local v22, "squarePosition":I
    const/16 v19, 0x0

    .line 1404
    .local v19, "preset":I
    const/16 v18, 0x0

    .local v18, "path":I
    :goto_1
    const/16 v27, 0x6

    move/from16 v0, v18

    move/from16 v1, v27

    if-ge v0, v1, :cond_e

    .line 1405
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v28, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    aget-object v28, v28, v18

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "SA_PRESET_INDEX"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1406
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v28, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    aget-object v28, v28, v18

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "SA_SQUARE_INDEX"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1408
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    const/16 v27, 0x3

    move/from16 v0, v27

    if-ge v11, v0, :cond_c

    .line 1409
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v28, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    aget-object v28, v28, v18

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "SA_STRENGH_INDEX_"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1408
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1412
    :cond_c
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_3
    const/16 v27, 0x7

    move/from16 v0, v16

    move/from16 v1, v27

    if-ge v0, v1, :cond_d

    .line 1413
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v28, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    aget-object v28, v28, v18

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "SA_EQ_INDEX_"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1412
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 1404
    :cond_d
    add-int/lit8 v18, v18, 0x1

    goto/16 :goto_1

    .line 1416
    .end local v11    # "i":I
    .end local v16    # "j":I
    :cond_e
    const-string v27, "SA_AUTO_STATE"

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    .line 1417
    const-string v27, "SA_ADAPT_STATE"

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    .line 1418
    const-string v27, "SA_SERVICE_STATE"

    const/16 v28, 0xc

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1423
    .end local v18    # "path":I
    .end local v19    # "preset":I
    .end local v22    # "squarePosition":I
    :cond_f
    new-instance v11, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    const-class v28, Lcom/sec/android/app/soundalive/SAControlPanelService;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1424
    .local v11, "i":Landroid/content/Intent;
    const-string v27, "SA_SERVICE_STATE"

    const/16 v28, 0xc

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1425
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1427
    new-instance v11, Landroid/content/Intent;

    .end local v11    # "i":Landroid/content/Intent;
    const-string v27, "internal_SA_AudioPath"

    move-object/from16 v0, v27

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1428
    .restart local v11    # "i":Landroid/content/Intent;
    const/high16 v27, 0x40000000    # 2.0f

    move/from16 v0, v27

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1429
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1433
    .end local v11    # "i":Landroid/content/Intent;
    :cond_10
    const-string v27, "SA_ACTIVITY_RENEW"

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    .line 1435
    .local v13, "isReNewRequired":Z
    if-eqz v13, :cond_1

    .line 1436
    const-string v27, "SA_ACTIVITY_RENEW"

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    .line 1439
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v28, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v29, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v28, v28, v29

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "SA_PRESET_INDEX"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v25

    .line 1440
    .local v25, "updatedPreset":I
    const/16 v27, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setUsePresetEffect(IZ)V

    .line 1443
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v28, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v29, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v28, v28, v29

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "SA_SQUARE_INDEX"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0xc

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v26

    .line 1444
    .local v26, "updatedSquarePos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V

    .line 1445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    .line 1446
    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    .line 1449
    const/16 v27, 0x7

    move/from16 v0, v27

    new-array v10, v0, [I

    .line 1450
    .local v10, "currentUserEq":[I
    const/16 v27, 0x3

    move/from16 v0, v27

    new-array v9, v0, [I

    .line 1452
    .local v9, "currentStrength":[I
    if-ltz v26, :cond_17

    const/16 v27, 0x18

    move/from16 v0, v26

    move/from16 v1, v27

    if-gt v0, v1, :cond_17

    .line 1453
    const/16 v27, 0xa

    move/from16 v0, v27

    new-array v4, v0, [B

    .line 1454
    .local v4, "Array_EQ_Strengh":[B
    sget v27, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_12

    .line 1455
    sget-object v27, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    move/from16 v28, v0

    aget-object v4, v27, v28

    .line 1472
    :cond_11
    :goto_4
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_5
    const/16 v27, 0x7

    move/from16 v0, v27

    if-ge v11, v0, :cond_15

    .line 1473
    aget-byte v27, v4, v11

    aput v27, v10, v11

    .line 1472
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 1456
    .end local v11    # "i":I
    :cond_12
    sget v27, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-nez v27, :cond_13

    .line 1457
    sget-object v27, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_MONO_SPK:[[B

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    move/from16 v28, v0

    aget-object v4, v27, v28

    .line 1459
    invoke-static {}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isStereoSpeakerDevice()Z

    move-result v27

    if-eqz v27, :cond_11

    .line 1460
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    move/from16 v27, v0

    rem-int/lit8 v27, v27, 0x5

    if-nez v27, :cond_11

    .line 1461
    const/16 v27, 0x7

    const/16 v28, 0x1

    aput-byte v28, v4, v27

    goto :goto_4

    .line 1464
    :cond_13
    sget v27, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_14

    .line 1465
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_6
    const/16 v27, 0xa

    move/from16 v0, v27

    if-ge v11, v0, :cond_11

    .line 1466
    const/16 v27, 0x0

    aput-byte v27, v4, v11

    .line 1465
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 1469
    .end local v11    # "i":I
    :cond_14
    sget-object v27, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->SQ_From_Lib_Map_EAR:[[B

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    move/from16 v28, v0

    aget-object v4, v27, v28

    goto :goto_4

    .line 1476
    .restart local v11    # "i":I
    :cond_15
    const/16 v27, 0x0

    const/16 v28, 0x7

    aget-byte v28, v4, v28

    aput v28, v9, v27

    .line 1477
    const/16 v27, 0x1

    const/16 v28, 0x8

    aget-byte v28, v4, v28

    aput v28, v9, v27

    .line 1478
    const/16 v27, 0x2

    const/16 v28, 0x9

    aget-byte v28, v4, v28

    aput v28, v9, v27

    .line 1506
    .end local v4    # "Array_EQ_Strengh":[B
    :cond_16
    if-eqz v10, :cond_20

    .line 1508
    const/4 v11, 0x0

    :goto_7
    const/16 v27, 0x7

    move/from16 v0, v27

    if-ge v11, v0, :cond_20

    .line 1509
    aget v8, v10, v11

    .line 1510
    .local v8, "bandLevel":I
    const-string v27, "SAControlPanelActivity"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "eq bandLevel i "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1511
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsReset:Z

    .line 1512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    move-object/from16 v27, v0

    aget-object v27, v27, v11

    add-int/lit8 v28, v8, 0xa

    invoke-virtual/range {v27 .. v28}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setProgress(I)V

    .line 1513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    move-object/from16 v27, v0

    aget-object v27, v27, v11

    const-string v28, "%s %s %d db %s"

    const/16 v29, 0x4

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    const v31, 0x7f070035

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v31

    aput-object v31, v29, v30

    const/16 v30, 0x1

    sget-object v31, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    aget v31, v31, v11

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v31

    aput-object v31, v29, v30

    const/16 v30, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    aput-object v31, v29, v30

    const/16 v30, 0x3

    const v31, 0x7f070038

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v31

    aput-object v31, v29, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1508
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_7

    .line 1481
    .end local v8    # "bandLevel":I
    .end local v11    # "i":I
    :cond_17
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_8
    const/16 v27, 0x3

    move/from16 v0, v27

    if-ge v11, v0, :cond_1e

    .line 1482
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v28, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v29, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v28, v28, v29

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "SA_STRENGH_INDEX_"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v23

    .line 1483
    .local v23, "strength":I
    if-nez v11, :cond_1a

    .line 1484
    const/16 v27, 0x7

    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v27

    if-eqz v27, :cond_18

    sget v27, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_19

    .line 1485
    :cond_18
    const/16 v23, 0x0

    .line 1494
    :cond_19
    :goto_9
    aput v23, v9, v11

    .line 1481
    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    .line 1486
    :cond_1a
    const/16 v27, 0x1

    move/from16 v0, v27

    if-ne v11, v0, :cond_1c

    .line 1487
    const/16 v27, 0x8

    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v27

    if-eqz v27, :cond_1b

    sget v27, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_19

    .line 1488
    :cond_1b
    const/16 v23, 0x0

    goto :goto_9

    .line 1489
    :cond_1c
    const/16 v27, 0x2

    move/from16 v0, v27

    if-ne v11, v0, :cond_19

    .line 1490
    const/16 v27, 0x9

    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v27

    if-eqz v27, :cond_1d

    sget v27, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_19

    .line 1491
    :cond_1d
    const/16 v23, 0x0

    goto :goto_9

    .line 1498
    .end local v23    # "strength":I
    :cond_1e
    const/4 v11, 0x0

    :goto_a
    const/16 v27, 0x7

    move/from16 v0, v27

    if-ge v11, v0, :cond_16

    .line 1499
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v28, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v29, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v28, v28, v29

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "SA_EQ_INDEX_"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v27

    aput v27, v10, v11

    .line 1500
    sget v27, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1f

    const/16 v27, 0x0

    aput v27, v10, v11

    .line 1498
    :cond_1f
    add-int/lit8 v11, v11, 0x1

    goto :goto_a

    .line 1519
    :cond_20
    if-eqz v9, :cond_1

    .line 1520
    const/4 v11, 0x0

    :goto_b
    const/16 v27, 0x3

    move/from16 v0, v27

    if-ge v11, v0, :cond_1

    .line 1521
    if-nez v11, :cond_23

    .line 1522
    aget v27, v9, v11

    if-nez v27, :cond_22

    .line 1523
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1520
    :cond_21
    :goto_c
    add-int/lit8 v11, v11, 0x1

    goto :goto_b

    .line 1525
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_c

    .line 1526
    :cond_23
    const/16 v27, 0x1

    move/from16 v0, v27

    if-ne v11, v0, :cond_25

    .line 1527
    aget v27, v9, v11

    if-nez v27, :cond_24

    .line 1528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_c

    .line 1530
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_c

    .line 1531
    :cond_25
    const/16 v27, 0x2

    move/from16 v0, v27

    if-ne v11, v0, :cond_21

    .line 1532
    aget v27, v9, v11

    if-nez v27, :cond_26

    .line 1533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_c

    .line 1535
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_c
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1546
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_0

    .line 1547
    const-string v2, "SAControlPanelActivity"

    const-string v3, "*****onSaveInstanceState()*****"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1548
    :cond_0
    const-string v2, "SA_TAB_INDEX"

    iget v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCurrentTab:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1549
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SA_EQ_INDEX_ARRAY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBandLevelFromUI()[I

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1550
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SA_PRESET_INDEX"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1551
    const-string v2, "SA_CURRENT_PATH_INDEX"

    sget v3, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1552
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SA_SQUARE_INDEX"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSquarePosition:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1553
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SA_STRENGH_INDEX_ARRAY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getStrengthValueFromUI()[I

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 1554
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAutoEnable:Z

    if-eqz v2, :cond_1

    .line 1555
    const-string v2, "SA_AUTO_STATE"

    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1558
    :cond_1
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v2, :cond_2

    .line 1559
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBandLevelFromUI()[I

    move-result-object v0

    .line 1560
    .local v0, "band":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_2

    .line 1561
    const-string v2, "SAControlPanelActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "currentUserEq"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1560
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1565
    .end local v0    # "band":[I
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1566
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 345
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 346
    const-string v0, "SAControlPanelActivity"

    const-string v1, "*****onStop()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 350
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 0

    .prologue
    .line 1630
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 1631
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->saveSoundAlivePreference()V

    .line 1632
    return-void
.end method

.method public setAdvancedBandLevel()V
    .locals 4

    .prologue
    .line 2177
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_0

    .line 2178
    const-string v2, "SAControlPanelActivity"

    const-string v3, "*****setAdvancedBandLevel()*****"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2179
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2180
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->resetCurrentValue()V

    .line 2181
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07002d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2182
    .local v1, "msg":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;

    if-nez v2, :cond_1

    .line 2183
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;

    .line 2187
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2194
    .end local v1    # "msg":Ljava/lang/String;
    :goto_1
    return-void

    .line 2185
    .restart local v1    # "msg":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2191
    .end local v1    # "msg":Ljava/lang/String;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBandLevelFromUI()[I

    move-result-object v0

    .line 2192
    .local v0, "eqList":[I
    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->setNewSoundAliveBandLevel([I)V

    goto :goto_1
.end method

.method public updateBandLevel()V
    .locals 8

    .prologue
    .line 1875
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v3, :cond_0

    .line 1876
    const-string v3, "SAControlPanelActivity"

    const-string v4, "*****updateBandLevel()***** +++"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1877
    :cond_0
    const/4 v0, 0x0

    .line 1878
    .local v0, "bandLevel":I
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getBandLevelFromUI()[I

    move-result-object v1

    .line 1880
    .local v1, "currentUserEq":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x7

    if-ge v2, v3, :cond_2

    .line 1882
    aget v0, v1, v2

    .line 1883
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v3, :cond_1

    .line 1884
    const-string v3, "SAControlPanelActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "band ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1885
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v3, v3, v2

    add-int/lit8 v4, v0, 0xa

    invoke-virtual {v3, v4}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setProgress(I)V

    .line 1886
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mEqControlBar:[Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;

    aget-object v3, v3, v2

    const-string v4, "%s %s %d db %s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const v7, 0x7f070035

    invoke-virtual {p0, v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->EQ_STRING_RES_ID:[I

    aget v7, v7, v2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const v7, 0x7f070038

    invoke-virtual {p0, v7}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/soundalive/widget/UserEQSeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1880
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1890
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->isShown()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1891
    iget-object v3, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBubble:Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;

    invoke-virtual {v3}, Lcom/sec/android/app/soundalive/widget/TwSeekBarBubble;->hideBubble()V

    .line 1893
    :cond_3
    return-void
.end method

.method public updatePreset(I)V
    .locals 3
    .param p1, "selectedUsePreset"    # I

    .prologue
    .line 1914
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->clearEffectImage()V

    .line 1916
    packed-switch p1, :pswitch_data_0

    .line 1944
    :goto_0
    iput p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSelectedUsePreset:I

    .line 1946
    return-void

    .line 1918
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctClub:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1919
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 1922
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctConcertHall:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1923
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 1926
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctNone:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1927
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 1930
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctStudio:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1931
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 1934
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1935
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 1938
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1939
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto/16 :goto_0

    .line 1916
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
