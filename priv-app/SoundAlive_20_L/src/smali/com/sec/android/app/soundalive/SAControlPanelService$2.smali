.class Lcom/sec/android/app/soundalive/SAControlPanelService$2;
.super Landroid/content/BroadcastReceiver;
.source "SAControlPanelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/SAControlPanelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelService;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 391
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 392
    :cond_0
    sget-boolean v6, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v6, :cond_1

    .line 393
    const-string v6, "SAControlPanelService"

    const-string v7, "Context or intent is null. Do nothing."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    :cond_1
    :goto_0
    return-void

    .line 397
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_3

    .line 400
    sget-boolean v6, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v6, :cond_1

    .line 401
    const-string v6, "SAControlPanelService"

    const-string v7, "action is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 405
    :cond_3
    sget-boolean v6, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v6, :cond_4

    .line 406
    const-string v6, "SAControlPanelService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mStateReceiver2 action = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_4
    const/4 v1, 0x0

    .line 410
    .local v1, "isPathChanged":Z
    const-string v6, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 412
    const-string v6, "android.bluetooth.profile.extra.STATE"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 413
    .local v4, "sinkState":I
    const-string v6, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 415
    .local v2, "previousSinkState":I
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 417
    .local v3, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const/4 v6, 0x2

    if-ne v4, v6, :cond_7

    const/4 v6, 0x1

    if-eq v2, v6, :cond_5

    if-nez v2, :cond_7

    .line 419
    :cond_5
    const-string v6, "SAControlPanelService"

    const-string v7, "ACTION_CONNECTION_STATE_CHANGED BT connected!!!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathEarjack()Z

    move-result v6

    if-nez v6, :cond_6

    .line 421
    const/4 v1, 0x1

    .line 422
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_BT"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    const/4 v6, 0x2

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 424
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x2

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 543
    .end local v2    # "previousSinkState":I
    .end local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    .end local v4    # "sinkState":I
    :cond_6
    :goto_1
    if-eqz v1, :cond_1

    .line 544
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->PathReConfig()V
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$900(Lcom/sec/android/app/soundalive/SAControlPanelService;)V

    goto/16 :goto_0

    .line 426
    .restart local v2    # "previousSinkState":I
    .restart local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    .restart local v4    # "sinkState":I
    :cond_7
    if-eqz v4, :cond_8

    const/4 v6, 0x3

    if-ne v4, v6, :cond_6

    .line 427
    :cond_8
    const-string v6, "SAControlPanelService"

    const-string v7, "ACTION_CONNECTION_STATE_CHANGED BT disconnected!!!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    const/4 v1, 0x1

    .line 429
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathEarjack()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 430
    const-string v6, "SAControlPanelService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mCurrentAudioPath = SA_PATH_EAR  isAudioPathBT() : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBT()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  isAudioPathBTModeSCO : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBTModeSCO()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    const/4 v6, 0x1

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 432
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 434
    :cond_9
    const-string v6, "SAControlPanelService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mCurrentAudioPath = SA_PATH_SPK  isAudioPathBT() : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBT()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  isAudioPathBTModeSCO : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBTModeSCO()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    const/4 v6, 0x0

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 436
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 439
    .end local v2    # "previousSinkState":I
    .end local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    .end local v4    # "sinkState":I
    :cond_a
    const-string v6, "android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 440
    const-string v6, "android.bluetooth.profile.extra.STATE"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 441
    .restart local v4    # "sinkState":I
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 442
    .restart local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const/16 v6, 0xa

    if-ne v4, v6, :cond_b

    .line 443
    const-string v6, "SAControlPanelService"

    const-string v7, "ACTION_PLAYING_STATE_CHANGED sinkState = BluetoothA2dp.STATE_PLAYING  mCurrentAudioPath = SA_PATH_BT"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const/4 v1, 0x1

    .line 445
    const/4 v6, 0x2

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 446
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x2

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 447
    :cond_b
    const/16 v6, 0xb

    if-ne v4, v6, :cond_6

    .line 448
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathEarjack()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 449
    const/4 v1, 0x1

    .line 450
    const-string v6, "SAControlPanelService"

    const-string v7, "ACTION_PLAYING_STATE_CHANGED sinkState = BluetoothA2dp.STATE_PLAYING  mCurrentAudioPath = SA_PATH_EAR"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    const/4 v6, 0x1

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 452
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 455
    .end local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    .end local v4    # "sinkState":I
    :cond_c
    const-string v6, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_d

    const-string v6, "android.media.action.HDMI_AUDIO_PLUG"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 456
    :cond_d
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 457
    .restart local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathLineOut()Z

    move-result v6

    if-eqz v6, :cond_e

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x3

    if-eq v6, v7, :cond_e

    .line 458
    const/4 v1, 0x1

    .line 459
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_LINE_OUT"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    const/4 v6, 0x3

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 461
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x3

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 462
    :cond_e
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v6

    if-eqz v6, :cond_f

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-eqz v6, :cond_f

    .line 463
    const/4 v1, 0x1

    .line 464
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_SPK"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    const/4 v6, 0x0

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 466
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 467
    :cond_f
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v6

    if-eqz v6, :cond_10

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x4

    if-eq v6, v7, :cond_10

    .line 468
    const/4 v1, 0x1

    .line 469
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_HDMI"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    const/4 v6, 0x4

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 471
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x4

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 472
    :cond_10
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathEarjack()Z

    move-result v6

    if-eqz v6, :cond_11

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_11

    .line 473
    const/4 v1, 0x1

    .line 474
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_EAR"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    const/4 v6, 0x1

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 476
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 477
    :cond_11
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBT()Z

    move-result v6

    if-eqz v6, :cond_6

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_6

    .line 478
    const/4 v1, 0x1

    .line 479
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_BT"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    const/4 v6, 0x2

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 481
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x2

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 484
    .end local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_12
    const-string v6, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 486
    const-string v6, "state"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 487
    .local v5, "state":I
    sput v5, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeviceState_REMOTE_SUBMIX:I

    .line 489
    const-string v6, "SAControlPanelService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ACTION_WIFIDISPLAY_STATE_CHANGED state = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "   mCurrentAudioPath : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    const/4 v6, 0x1

    if-ne v5, v6, :cond_13

    .line 492
    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x5

    if-eq v6, v7, :cond_6

    .line 493
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 494
    .restart local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const/4 v1, 0x1

    .line 495
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_REMOTE_SUBMIX"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    const/4 v6, 0x5

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 497
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x5

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 501
    .end local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_13
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v6, v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mHandler:Landroid/os/Handler;

    new-instance v7, Lcom/sec/android/app/soundalive/SAControlPanelService$2$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/soundalive/SAControlPanelService$2$1;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelService$2;)V

    const-wide/16 v8, 0x3e8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 513
    .end local v5    # "state":I
    :cond_14
    const-string v6, "internal_SA_AudioPath"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 514
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 515
    .restart local v3    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathLineOut()Z

    move-result v6

    if-eqz v6, :cond_15

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x3

    if-eq v6, v7, :cond_15

    .line 516
    const/4 v1, 0x1

    .line 517
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_LINE_OUT"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    const/4 v6, 0x3

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 519
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x3

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 520
    :cond_15
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v6

    if-eqz v6, :cond_16

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-eqz v6, :cond_16

    .line 521
    const/4 v1, 0x1

    .line 522
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_SPK"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    const/4 v6, 0x0

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 524
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 525
    :cond_16
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v6

    if-eqz v6, :cond_17

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x4

    if-eq v6, v7, :cond_17

    .line 526
    const/4 v1, 0x1

    .line 527
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_HDMI"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    const/4 v6, 0x4

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 529
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x4

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 530
    :cond_17
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathEarjack()Z

    move-result v6

    if-eqz v6, :cond_18

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_18

    .line 531
    const/4 v1, 0x1

    .line 532
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_EAR"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    const/4 v6, 0x1

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 534
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 535
    :cond_18
    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    invoke-static {v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBT()Z

    move-result v6

    if-eqz v6, :cond_6

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_6

    .line 536
    const/4 v1, 0x1

    .line 537
    const-string v6, "SAControlPanelService"

    const-string v7, "mCurrentAudioPath = SA_PATH_BT"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    const/4 v6, 0x2

    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 539
    const-string v6, "SA_CURRENT_PATH_INDEX"

    const/4 v7, 0x2

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1
.end method
