.class public Lcom/sec/android/app/soundalive/framework/SecAudioManager;
.super Ljava/lang/Object;
.source "SecAudioManager.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field public static final NEWSOUNDALIVE_ADVANCED_3D:I = 0x7

.field public static final NEWSOUNDALIVE_ADVANCED_BASS:I = 0x8

.field public static final NEWSOUNDALIVE_ADVANCED_CLARITY:I = 0x9

.field public static final NEWSOUNDALIVE_ADVANCED_EQ:I = 0x6

.field public static final NEWSOUNDALIVE_ALL_EFFECT:I = 0x63

.field public static final NEWSOUNDALIVE_BAND_LEVEL_LENGTH:I = 0x7

.field public static final NEWSOUNDALIVE_EQ_EFFECT:I = 0x62

.field public static final NEWSOUNDALIVE_GENRE_CLASSIC:I = 0x2

.field public static final NEWSOUNDALIVE_GENRE_JAZZ:I = 0x10

.field public static final NEWSOUNDALIVE_GENRE_NONE:I = 0xc

.field public static final NEWSOUNDALIVE_GENRE_POP:I = 0xd

.field public static final NEWSOUNDALIVE_GENRE_ROCK:I = 0xa

.field public static final NEWSOUNDALIVE_PRESET_CLUB:I = 0x4

.field public static final NEWSOUNDALIVE_PRESET_CONCERTHALL:I = 0x5

.field public static final NEWSOUNDALIVE_PRESET_EFFECT:I = 0x5f

.field public static final NEWSOUNDALIVE_PRESET_NORMAL:I = 0x0

.field public static final NEWSOUNDALIVE_PRESET_STUDIO:I = 0x3

.field public static final NEWSOUNDALIVE_PRESET_TUBE:I = 0x1

.field public static final NEWSOUNDALIVE_PRESET_VIRT71:I = 0x2

.field public static final NEWSOUNDALIVE_SQUARE_EFFECT:I = 0x60

.field public static final NEWSOUNDALIVE_SQUARE_POSITION_LENGTH:I = 0x2

.field public static final NEWSOUNDALIVE_SQUARE_POSITION_NORMAL:I = 0xc

.field public static final NEWSOUNDALIVE_STRENGTH_LENGTH:I = 0x3

.field public static final NEWSOUNDALIVE_STRENTH_EFFECT:I = 0x61

.field private static sAudioManager:Landroid/media/AudioManager;


# instance fields
.field public final LIBRARY_OVER_API_1:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->LIBRARY_OVER_API_1:Z

    .line 304
    sget-object v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 305
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    sput-object v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    .line 307
    :cond_0
    return-void
.end method

.method public static isStereoSpeakerDevice()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 332
    new-instance v1, Landroid/media/audiofx/SoundAlive$Settings;

    invoke-direct {v1}, Landroid/media/audiofx/SoundAlive$Settings;-><init>()V

    .line 333
    .local v1, "settings":Landroid/media/audiofx/SoundAlive$Settings;
    invoke-virtual {v1}, Landroid/media/audiofx/SoundAlive$Settings;->getNumberOfSpeaker()I

    move-result v0

    .line 334
    .local v0, "numberOfSpeaker":I
    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    .line 340
    :goto_0
    return v2

    .line 336
    :cond_0
    if-ne v0, v2, :cond_1

    move v2, v3

    .line 337
    goto :goto_0

    .line 339
    :cond_1
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "something wrong count number of speaker : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 340
    goto :goto_0
.end method

.method public static isSupportSoundAliveBT(I)Z
    .locals 2
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v0, 0x1

    .line 215
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveHDMI(I)Z
    .locals 1
    .param p0, "soundAlive"    # I

    .prologue
    .line 241
    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveLineOut(I)Z
    .locals 2
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v0, 0x1

    .line 228
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveREMOTE_SUBMIX(I)Z
    .locals 2
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v0, 0x1

    .line 190
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveSpeaker(I)Z
    .locals 4
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 203
    invoke-static {}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isStereoSpeakerDevice()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 204
    if-eq p0, v0, :cond_1

    if-eq p0, v3, :cond_1

    .line 207
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 204
    goto :goto_0

    .line 207
    :cond_2
    if-eq p0, v0, :cond_3

    if-eq p0, v3, :cond_3

    const/4 v2, 0x7

    if-ne p0, v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getParameters(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "keys"    # Ljava/lang/String;

    .prologue
    .line 326
    sget-object v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAudioPathBT()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 84
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    const/16 v2, 0x80

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x200

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 95
    .local v0, "isBt":Z
    :cond_3
    goto :goto_0
.end method

.method public isAudioPathBTHeadphone()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 104
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    :cond_0
    :goto_0
    return v0

    .line 111
    :cond_1
    const/16 v2, 0x80

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v0, 0x1

    .line 114
    .local v0, "isBt":Z
    :cond_3
    goto :goto_0
.end method

.method public isAudioPathBTModeSCO()Z
    .locals 5

    .prologue
    .line 135
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 138
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 139
    :cond_0
    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    .line 142
    :cond_1
    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 144
    .local v0, "isBt":Z
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isAudioPathBTModeSCO() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isAudioPathEarjack()Z
    .locals 4

    .prologue
    .line 71
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;curDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    .local v1, "path":Ljava/lang/String;
    const-string v2, "HPH"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 75
    .local v0, "isEarjack":Z
    return v0
.end method

.method public isAudioPathHDMI()Z
    .locals 4

    .prologue
    .line 165
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 167
    .local v1, "path":Ljava/lang/String;
    const/16 v2, 0x400

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 169
    .local v0, "isHDMI":Z
    return v0
.end method

.method public isAudioPathLineOut()Z
    .locals 4

    .prologue
    .line 156
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "path":Ljava/lang/String;
    const/16 v2, 0x800

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x1000

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 161
    .local v0, "isLineout":Z
    :goto_0
    return v0

    .line 158
    .end local v0    # "isLineout":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAudioPathREMOTE_SUBMIX()Z
    .locals 5

    .prologue
    const/4 v4, 0x5

    .line 174
    invoke-static {}, Lcom/sec/android/app/soundalive/SAControlPanelService;->isDeviceREMOTE_SUBMIX()Z

    move-result v0

    .line 176
    .local v0, "isREMOTE_SUBMIX":Z
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "path":Ljava/lang/String;
    const v2, 0x8000

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 179
    sget v2, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-eq v2, v4, :cond_0

    .line 180
    sput v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 182
    :cond_0
    const/4 v0, 0x1

    .line 185
    :cond_1
    return v0
.end method

.method public isAudioPathSpeaker()Z
    .locals 4

    .prologue
    .line 124
    sget-object v2, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    const-string v3, "audioParam;outDevice"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 126
    .local v1, "path":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 128
    .local v0, "isSpeaker":Z
    return v0
.end method

.method public isBluetoothA2dpOn()Z
    .locals 1

    .prologue
    .line 322
    sget-object v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v0

    return v0
.end method

.method public isEnableAdaptSoundPath()Z
    .locals 1

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBT()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMusicActive()Z
    .locals 1

    .prologue
    .line 310
    sget-object v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 311
    sget-object v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    .line 313
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSoundAliveSupportCurrentAudioPath(Landroid/content/Context;IZ)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "soundAlive"    # I
    .param p3, "enableToast"    # Z

    .prologue
    .line 256
    const/4 v0, 0x1

    .line 257
    .local v0, "isSupportCurrentAudioPath":Z
    const/4 v1, 0x0

    .line 258
    .local v1, "notSupportgMsgId":I
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 259
    invoke-static {p2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isSupportSoundAliveSpeaker(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 260
    const/4 v0, 0x0

    .line 261
    const v1, 0x7f07002f

    .line 293
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    if-eqz p3, :cond_1

    .line 294
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 298
    :cond_1
    return v0

    .line 263
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBT()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBTModeSCO()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 267
    :cond_3
    invoke-static {p2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isSupportSoundAliveBT(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 268
    const/4 v0, 0x0

    .line 269
    const v1, 0x7f07002c

    goto :goto_0

    .line 271
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathLineOut()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 272
    invoke-static {p2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isSupportSoundAliveLineOut(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 273
    const/4 v0, 0x0

    .line 274
    const v1, 0x7f07002e

    goto :goto_0

    .line 276
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 277
    invoke-static {p2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isSupportSoundAliveHDMI(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 278
    const/4 v0, 0x0

    .line 279
    const v1, 0x7f07002d

    goto :goto_0

    .line 281
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathREMOTE_SUBMIX()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    invoke-static {p2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isSupportSoundAliveREMOTE_SUBMIX(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 283
    const/4 v0, 0x0

    .line 284
    const v1, 0x7f07002b

    goto :goto_0
.end method

.method public isWiredHeadsetOn()Z
    .locals 1

    .prologue
    .line 318
    sget-object v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->sAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method public warningAdaptSound(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 350
    const/4 v0, 0x0

    .line 352
    .local v0, "msg":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 357
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 358
    return-void
.end method
