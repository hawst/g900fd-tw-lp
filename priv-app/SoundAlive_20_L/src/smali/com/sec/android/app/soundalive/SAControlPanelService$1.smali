.class Lcom/sec/android/app/soundalive/SAControlPanelService$1;
.super Landroid/content/BroadcastReceiver;
.source "SAControlPanelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/SAControlPanelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelService;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 267
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 268
    :cond_0
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v12, :cond_1

    .line 269
    const-string v12, "SAControlPanelService"

    const-string v13, "Context or intent is null. Do nothing."

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_1
    :goto_0
    return-void

    .line 273
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 275
    .local v2, "action":Ljava/lang/String;
    if-nez v2, :cond_3

    .line 276
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v12, :cond_1

    .line 277
    const-string v12, "SAControlPanelService"

    const-string v13, "action is null"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 281
    :cond_3
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v12, :cond_4

    .line 282
    const-string v12, "SAControlPanelService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "*****onReceive() in Service*****  action : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_4
    const/4 v6, 0x0

    .line 285
    .local v6, "isActiveSAFX":Z
    const-string v12, "internal_SA_Preset_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 287
    const-string v12, "SA_Preset"

    const/4 v13, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 288
    .local v7, "preset":I
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V
    invoke-static {v12, v13, v7}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$100(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;I)V

    .line 290
    const/4 v6, 0x1

    .line 376
    .end local v7    # "preset":I
    :cond_5
    :goto_1
    if-eqz v6, :cond_1

    .line 377
    new-instance v5, Landroid/content/Intent;

    const-string v12, "com.android.music.soundalivechanged"

    invoke-direct {v5, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 378
    .local v5, "i":Landroid/content/Intent;
    const/high16 v12, 0x40000000    # 2.0f

    invoke-virtual {v5, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 379
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual {v12, v5}, Lcom/sec/android/app/soundalive/SAControlPanelService;->sendBroadcast(Landroid/content/Intent;)V

    .line 380
    const/4 v12, 0x0

    sput-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mBT_App_Active:Z

    goto :goto_0

    .line 291
    .end local v5    # "i":Landroid/content/Intent;
    :cond_6
    const-string v12, "internal_SA_Square_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 293
    const-string v12, "SA_SquarePosition"

    const/4 v13, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 294
    .local v11, "squarePosition":I
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V
    invoke-static {v12, v13, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;I)V

    .line 296
    const/4 v6, 0x1

    .line 298
    goto :goto_1

    .end local v11    # "squarePosition":I
    :cond_7
    const-string v12, "internal_SA_EQ_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 300
    const/4 v12, 0x7

    new-array v4, v12, [I

    .line 301
    .local v4, "eqLevel":[I
    const-string v12, "SA_EQ_Array"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v4

    .line 302
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->updateBandLevel(Landroid/media/audiofx/SoundAlive;[I)V
    invoke-static {v12, v13, v4}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$300(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;[I)V

    .line 304
    const/4 v6, 0x1

    .line 306
    goto :goto_1

    .end local v4    # "eqLevel":[I
    :cond_8
    const-string v12, "internal_SA_3D_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 308
    const-string v12, "SA_Strength_3D"

    const/4 v13, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 309
    .local v8, "sa3D":I
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    const/4 v14, 0x0

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->updateStrength(Landroid/media/audiofx/SoundAlive;II)V
    invoke-static {v12, v13, v14, v8}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;II)V

    .line 311
    const/4 v6, 0x1

    .line 313
    goto :goto_1

    .end local v8    # "sa3D":I
    :cond_9
    const-string v12, "internal_SA_Bass_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 315
    const-string v12, "SA_Strength_BASS"

    const/4 v13, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 316
    .local v9, "saBass":I
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    const/4 v14, 0x1

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->updateStrength(Landroid/media/audiofx/SoundAlive;II)V
    invoke-static {v12, v13, v14, v9}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;II)V

    .line 318
    const/4 v6, 0x1

    .line 320
    goto/16 :goto_1

    .end local v9    # "saBass":I
    :cond_a
    const-string v12, "internal_SA_Clarity_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 322
    const-string v12, "SA_Strength_Clarity"

    const/4 v13, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 323
    .local v10, "saClarity":I
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    const/4 v14, 0x2

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->updateStrength(Landroid/media/audiofx/SoundAlive;II)V
    invoke-static {v12, v13, v14, v10}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$400(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;II)V

    .line 325
    const/4 v6, 0x1

    .line 327
    goto/16 :goto_1

    .end local v10    # "saClarity":I
    :cond_b
    const-string v12, "com.sec.android.app.soundalive.ACTION_SA_DUPLICATED_USED"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 330
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v12, :cond_c

    .line 331
    const-string v12, "SAControlPanelService"

    const-string v13, "ACTION_SA_DUPLICATED_USED"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_c
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->saveSoundAlivePreferenceAsBypass()V
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$500(Lcom/sec/android/app/soundalive/SAControlPanelService;)V

    .line 335
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I
    invoke-static {v12, v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;)I

    move-result v1

    .line 336
    .local v1, "SA_STATUS_CHECK":I
    if-gez v1, :cond_d

    const/16 v12, -0x63

    if-eq v1, v12, :cond_d

    .line 337
    const-string v12, "SAControlPanelService"

    const-string v13, "checkStatus(mSA) return bad status in ACTION_SA_DUPLICATED_USED"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 341
    :cond_d
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v12

    if-eqz v12, :cond_5

    .line 342
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v12, :cond_e

    .line 343
    const-string v12, "SAControlPanelService"

    const-string v13, "Now, primium BT HP is using soundalive so soundalive fx going to bypass"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_e
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    const/16 v14, 0xc

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V
    invoke-static {v12, v13, v14}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$200(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;I)V

    .line 345
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    const/4 v14, 0x0

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V
    invoke-static {v12, v13, v14}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$100(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;I)V

    goto/16 :goto_1

    .line 347
    .end local v1    # "SA_STATUS_CHECK":I
    :cond_f
    const-string v12, "internal_SA_Adapt_changed"

    invoke-virtual {v12, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 348
    sget-boolean v12, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v12, :cond_10

    .line 349
    const-string v12, "SAControlPanelService"

    const-string v13, "ACTION_ADAPT_UPDATE"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_10
    const-string v12, "SA_AdaptSound_State"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 351
    .local v3, "adaptState":Z
    const-string v12, "SAControlPanelService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "adaptState: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    iget-object v13, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;
    invoke-static {v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;

    move-result-object v13

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I
    invoke-static {v12, v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$600(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;)I

    move-result v1

    .line 354
    .restart local v1    # "SA_STATUS_CHECK":I
    if-gez v1, :cond_11

    const/16 v12, -0x63

    if-eq v1, v12, :cond_11

    .line 355
    const-string v12, "SAControlPanelService"

    const-string v13, "checkStatus(mSA) return bad status in for AdaptSound ACTION_ADAPT_UPDATE"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 359
    :cond_11
    if-eqz v3, :cond_12

    .line 360
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/AdaptSound;

    move-result-object v12

    if-eqz v12, :cond_5

    .line 361
    const-string v12, "SAControlPanelService"

    const-string v13, "engine on"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/AdaptSound;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->prepare()V

    .line 363
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/AdaptSound;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->activate(Z)V

    .line 364
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    goto/16 :goto_1

    .line 367
    :cond_12
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/AdaptSound;

    move-result-object v12

    if-eqz v12, :cond_5

    .line 368
    const-string v12, "SAControlPanelService"

    const-string v13, "engine off"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;
    invoke-static {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->access$700(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/AdaptSound;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->activate(Z)V

    .line 370
    iget-object v12, p0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelService;

    invoke-virtual {v12}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    goto/16 :goto_1
.end method
