.class public final Lcom/sec/android/app/soundalive/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_tab_max_width:I = 0x7f050000

.field public static final action_bar_tab_text_max_width:I = 0x7f050001

.field public static final custom_seekbubble_height:I = 0x7f050002

.field public static final custom_seekbubble_padding_horizontal:I = 0x7f050003

.field public static final eq_bubble_text:I = 0x7f050004

.field public static final eq_control_bar_padding_bottom:I = 0x7f050005

.field public static final eq_control_bar_padding_top:I = 0x7f050006

.field public static final eq_control_text:I = 0x7f050007

.field public static final eq_dbscale_layout:I = 0x7f050008

.field public static final equalizer_control_height:I = 0x7f050009

.field public static final equalizer_control_margin_left:I = 0x7f05000a

.field public static final equalizer_control_margin_right:I = 0x7f05000b

.field public static final equalizer_seekbar_height:I = 0x7f05000c

.field public static final equalizer_seekbar_padding_top:I = 0x7f05000d

.field public static final new_soundAlive_effect_text2_size:I = 0x7f05000e

.field public static final new_soundalive_advanced_effect_margin_bottom:I = 0x7f05000f

.field public static final new_soundalive_advanced_effect_width:I = 0x7f050025

.field public static final new_soundalive_basic_advanced_height:I = 0x7f050026

.field public static final new_soundalive_basic_advanced_width:I = 0x7f050027

.field public static final new_soundalive_basic_layout_height:I = 0x7f050028

.field public static final new_soundalive_basic_layout_width:I = 0x7f050029

.field public static final new_soundalive_cell_size:I = 0x7f050010

.field public static final new_soundalive_effect_button_divider_height:I = 0x7f050011

.field public static final new_soundalive_effect_layout_height:I = 0x7f050012

.field public static final new_soundalive_effect_subtext_paddingLeft:I = 0x7f050013

.field public static final new_soundalive_effect_subtext_width:I = 0x7f050014

.field public static final new_soundalive_effects_layout_width:I = 0x7f05002a

.field public static final new_soundalive_eq_control_bar_width:I = 0x7f05002b

.field public static final new_soundalive_gridview_margin_left:I = 0x7f050015

.field public static final new_soundalive_gridview_margin_right:I = 0x7f050016

.field public static final new_soundalive_preset_auto_checkbox_margin_left:I = 0x7f050017

.field public static final new_soundalive_preset_auto_checkbox_margin_top:I = 0x7f050018

.field public static final new_soundalive_preset_auto_checkbox_max_width:I = 0x7f05002c

.field public static final new_soundalive_preset_gridview_layout_width:I = 0x7f050019

.field public static final new_soundalive_preset_gridview_margin_left:I = 0x7f05002d

.field public static final new_soundalive_preset_gridview_margin_left2:I = 0x7f05002e

.field public static final new_soundalive_preset_gridview_margin_right:I = 0x7f05002f

.field public static final new_soundalive_preset_gridview_margin_top:I = 0x7f050030

.field public static final new_soundalive_preset_gridview_width:I = 0x7f05001a

.field public static final new_soundalive_preset_left_text_margin_left:I = 0x7f05001b

.field public static final new_soundalive_preset_right_text_margin_right:I = 0x7f05001c

.field public static final new_soundalive_preset_text_margin_bottom:I = 0x7f05001d

.field public static final new_soundalive_preset_text_margin_top:I = 0x7f05001e

.field public static final new_soundalive_square_layout_width:I = 0x7f050031

.field public static final new_soundalive_squarecell_text1_size:I = 0x7f05001f

.field public static final tab_height:I = 0x7f050020

.field public static final tab_text_size:I = 0x7f050021

.field public static final tw_seekbarSplit_height_size:I = 0x7f050022

.field public static final tw_seekbarSplit_verticalbar_height_size:I = 0x7f050023

.field public static final tw_seekbarSplit_verticalbar_width_size:I = 0x7f050024


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
