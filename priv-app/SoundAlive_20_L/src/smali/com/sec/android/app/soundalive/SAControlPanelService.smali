.class public Lcom/sec/android/app/soundalive/SAControlPanelService;
.super Landroid/app/Service;
.source "SAControlPanelService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/soundalive/SAControlPanelService$MyBinder;
    }
.end annotation


# static fields
.field private static final ACTION_WIFIDISPLAY_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.WIFI_DISPLAY"

.field private static final RECONFIG_ADAPTSOUND:I = 0x2

.field private static final RECONFIG_ALL_EFFECT:I = 0x4

.field private static final RECONFIG_AUTO_PRESET:I = 0x3

.field private static final RECONFIG_SOUNDALIVE:I = 0x1

.field private static final SOUNDALIVE_OBJECT_IS_NULL:I = -0x63

.field private static final TAG:Ljava/lang/String; = "SAControlPanelService"

.field public static mCurrentAudioPath:I

.field private static mCurrentEffectState:I

.field private static mDeadAudioSession:I

.field public static mDeviceState_REMOTE_SUBMIX:I


# instance fields
.field private mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

.field private mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

.field private final mBinder:Landroid/os/IBinder;

.field final mHandler:Landroid/os/Handler;

.field private mSA:Landroid/media/audiofx/SoundAlive;

.field private final mStateReceiver:Landroid/content/BroadcastReceiver;

.field private final mStateReceiver2:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    sput v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 25
    sput v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentEffectState:I

    .line 26
    sput v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeadAudioSession:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mHandler:Landroid/os/Handler;

    .line 196
    new-instance v0, Lcom/sec/android/app/soundalive/SAControlPanelService$MyBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/soundalive/SAControlPanelService$MyBinder;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelService;)V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mBinder:Landroid/os/IBinder;

    .line 263
    new-instance v0, Lcom/sec/android/app/soundalive/SAControlPanelService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/soundalive/SAControlPanelService$1;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelService;)V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mStateReceiver:Landroid/content/BroadcastReceiver;

    .line 387
    new-instance v0, Lcom/sec/android/app/soundalive/SAControlPanelService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/soundalive/SAControlPanelService$2;-><init>(Lcom/sec/android/app/soundalive/SAControlPanelService;)V

    iput-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mStateReceiver2:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private PathReConfig()V
    .locals 13

    .prologue
    .line 572
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v10}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v0

    .line 573
    .local v0, "SA_STATUS_CHECK":I
    if-gez v0, :cond_0

    const/16 v10, -0x63

    if-eq v0, v10, :cond_0

    .line 574
    const-string v10, "SAControlPanelService"

    const-string v11, "checkStatus(mSA) return bad status in path changing block"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    :goto_0
    return-void

    .line 578
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v7

    .line 579
    .local v7, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SA_SQUARE_INDEX"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0xc

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 580
    .local v8, "squarePosition":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SA_PRESET_INDEX"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 581
    .local v6, "preset":I
    const/4 v10, 0x3

    new-array v1, v10, [I

    .line 582
    .local v1, "currentStrength":[I
    const/4 v10, 0x7

    new-array v2, v10, [I

    .line 583
    .local v2, "eqLevel":[I
    const-string v10, "CURRENT_PACKAGENAME"

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 585
    .local v5, "pkgName":Ljava/lang/String;
    const-string v10, "com.sec.android.app.music"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 586
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v10}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isEnableAdaptSoundPath()Z

    move-result v10

    if-nez v10, :cond_3

    .line 587
    const-string v10, "SAControlPanelService"

    const-string v11, "engine off"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    if-eqz v10, :cond_1

    .line 589
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->activate(Z)V

    .line 590
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    .line 592
    :cond_1
    sget-boolean v10, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sEntrance:Z

    if-eqz v10, :cond_3

    sget-boolean v10, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mAdaptCheckBoxState:Z

    if-eqz v10, :cond_3

    .line 593
    sget-boolean v10, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsActivityVisible:Z

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->warningAdaptSound(Landroid/content/Context;)V

    .line 594
    :cond_2
    new-instance v4, Landroid/content/Intent;

    const-string v10, "internal_SA_Adapt_UI_changed"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 595
    .local v4, "iUIupdate":Landroid/content/Intent;
    const/high16 v10, 0x40000000    # 2.0f

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 596
    const-string v10, "SA_AdaptSound_State"

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 597
    invoke-virtual {p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelService;->sendBroadcast(Landroid/content/Intent;)V

    .line 602
    .end local v4    # "iUIupdate":Landroid/content/Intent;
    :cond_3
    const-string v10, "com.sec.android.app.music"

    invoke-virtual {v10, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 603
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v10}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v10

    if-eqz v10, :cond_4

    sget-boolean v10, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sEntrance:Z

    if-eqz v10, :cond_4

    sget-boolean v10, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxState:Z

    if-eqz v10, :cond_4

    .line 604
    new-instance v4, Landroid/content/Intent;

    const-string v10, "internal_SA_Auto_UI_changed"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 605
    .restart local v4    # "iUIupdate":Landroid/content/Intent;
    const/high16 v10, 0x40000000    # 2.0f

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 606
    const-string v10, "SA_Auto_State"

    const/4 v11, 0x0

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 607
    invoke-virtual {p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelService;->sendBroadcast(Landroid/content/Intent;)V

    .line 612
    .end local v4    # "iUIupdate":Landroid/content/Intent;
    :cond_4
    const/4 v10, 0x0

    invoke-direct {p0, v6, v10}, Lcom/sec/android/app/soundalive/SAControlPanelService;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v10

    if-eqz v10, :cond_5

    sget v10, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_7

    .line 613
    :cond_5
    sget v10, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-nez v10, :cond_9

    .line 614
    const/4 v10, 0x1

    if-eq v6, v10, :cond_6

    const/4 v10, 0x2

    if-ne v6, v10, :cond_7

    .line 615
    :cond_6
    const/4 v6, 0x0

    .line 622
    :cond_7
    :goto_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SA_PRESET_INDEX"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 623
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SA_SQUARE_INDEX"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10, v8}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 626
    if-ltz v8, :cond_a

    const/16 v10, 0x18

    if-gt v8, v10, :cond_a

    .line 627
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v10, v8}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V

    .line 628
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v10, v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 662
    :cond_8
    new-instance v4, Landroid/content/Intent;

    const-string v10, "internal_SA_Advanced_UI_changed"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 663
    .restart local v4    # "iUIupdate":Landroid/content/Intent;
    const/high16 v10, 0x40000000    # 2.0f

    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 664
    const-string v10, "SA_SquarePosition"

    invoke-virtual {v4, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 665
    const-string v10, "SA_Preset"

    invoke-virtual {v4, v10, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 666
    const-string v10, "SA_EQ_Array"

    invoke-virtual {v4, v10, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 667
    const-string v10, "SA_Strength_Array"

    invoke-virtual {v4, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 668
    invoke-virtual {p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelService;->sendBroadcast(Landroid/content/Intent;)V

    .line 670
    sget-boolean v10, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mIsActivityVisible:Z

    if-nez v10, :cond_14

    .line 671
    const-string v10, "SA_ACTIVITY_RENEW"

    const/4 v11, 0x1

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 618
    .end local v4    # "iUIupdate":Landroid/content/Intent;
    :cond_9
    const/4 v6, 0x0

    goto :goto_1

    .line 631
    :cond_a
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    const/4 v10, 0x3

    if-ge v3, v10, :cond_11

    .line 632
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SA_STRENGH_INDEX_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 633
    .local v9, "strength":I
    if-nez v3, :cond_d

    .line 634
    const/4 v10, 0x7

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v10

    if-eqz v10, :cond_b

    sget v10, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_c

    .line 635
    :cond_b
    const/4 v9, 0x0

    .line 644
    :cond_c
    :goto_3
    aput v9, v1, v3

    .line 645
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SA_STRENGH_INDEX_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aget v11, v1, v3

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 631
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 636
    :cond_d
    const/4 v10, 0x1

    if-ne v3, v10, :cond_f

    .line 637
    const/16 v10, 0x8

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v10

    if-eqz v10, :cond_e

    sget v10, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_c

    .line 638
    :cond_e
    const/4 v9, 0x0

    goto :goto_3

    .line 639
    :cond_f
    const/4 v10, 0x2

    if-ne v3, v10, :cond_c

    .line 640
    const/16 v10, 0x9

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v10

    if-eqz v10, :cond_10

    sget v10, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_c

    .line 641
    :cond_10
    const/4 v9, 0x0

    goto :goto_3

    .line 648
    .end local v9    # "strength":I
    :cond_11
    const/4 v3, 0x0

    :goto_4
    const/4 v10, 0x7

    if-ge v3, v10, :cond_13

    .line 649
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SA_EQ_INDEX_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v10

    aput v10, v2, v3

    .line 650
    sget v10, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_12

    const/4 v10, 0x0

    aput v10, v2, v3

    .line 651
    :cond_12
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v12, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v11, v11, v12

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SA_EQ_INDEX_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aget v11, v2, v3

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 648
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 654
    :cond_13
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v10, v2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateBandLevel(Landroid/media/audiofx/SoundAlive;[I)V

    .line 655
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v10, v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 656
    const/4 v3, 0x0

    :goto_5
    const/4 v10, 0x3

    if-ge v3, v10, :cond_8

    .line 657
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    aget v11, v1, v3

    invoke-direct {p0, v10, v3, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateStrength(Landroid/media/audiofx/SoundAlive;II)V

    .line 658
    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v10, v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 656
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 673
    .end local v3    # "i":I
    .restart local v4    # "iUIupdate":Landroid/content/Intent;
    :cond_14
    const-string v10, "SA_ACTIVITY_RENEW"

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/soundalive/SAControlPanelService;)Landroid/media/audiofx/SoundAlive;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;
    .param p1, "x1"    # Landroid/media/audiofx/SoundAlive;
    .param p2, "x2"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;
    .param p1, "x1"    # Landroid/media/audiofx/SoundAlive;
    .param p2, "x2"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;[I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;
    .param p1, "x1"    # Landroid/media/audiofx/SoundAlive;
    .param p2, "x2"    # [I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateBandLevel(Landroid/media/audiofx/SoundAlive;[I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;
    .param p1, "x1"    # Landroid/media/audiofx/SoundAlive;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateStrength(Landroid/media/audiofx/SoundAlive;II)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/soundalive/SAControlPanelService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->saveSoundAlivePreferenceAsBypass()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/soundalive/SAControlPanelService;Landroid/media/audiofx/SoundAlive;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;
    .param p1, "x1"    # Landroid/media/audiofx/SoundAlive;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/AdaptSound;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/soundalive/SAControlPanelService;)Lcom/sec/android/app/soundalive/framework/SecAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/soundalive/SAControlPanelService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/SAControlPanelService;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->PathReConfig()V

    return-void
.end method

.method private checkAndSetEffect(I)V
    .locals 17
    .param p1, "selection"    # I

    .prologue
    .line 692
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v14, :cond_0

    .line 693
    const-string v14, "SAControlPanelService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, " checkAndSetEffect() selection : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v12

    .line 702
    .local v12, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const/4 v14, 0x1

    move/from16 v0, p1

    if-eq v0, v14, :cond_1

    const/4 v14, 0x4

    move/from16 v0, p1

    if-ne v0, v14, :cond_a

    .line 703
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getCurrentAudioPath()I

    move-result v5

    .line 704
    .local v5, "curPath":I
    sget v14, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-eq v5, v14, :cond_3

    .line 706
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v14, :cond_2

    .line 707
    const-string v14, "SAControlPanelService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "audiopath reconfig mCurrentAudioPath : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " curPath : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    :cond_2
    sput v5, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 711
    :cond_3
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v15, v15, v16

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "SA_SQUARE_INDEX"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, -0x2

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v13

    .line 712
    .local v13, "squarePosition":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v15, v15, v16

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "SA_PRESET_INDEX"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, -0x1

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 713
    .local v11, "preset":I
    const/4 v14, 0x3

    new-array v6, v14, [I

    .line 714
    .local v6, "currentStrength":[I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    const/4 v14, 0x3

    if-ge v9, v14, :cond_4

    .line 715
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v15, v15, v16

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "SA_STRENGH_INDEX_"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v14

    aput v14, v6, v9

    .line 714
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 717
    :cond_4
    const/4 v14, 0x7

    new-array v7, v14, [I

    .line 718
    .local v7, "eqLevel":[I
    const/4 v9, 0x0

    :goto_1
    const/4 v14, 0x7

    if-ge v9, v14, :cond_5

    .line 719
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v15, v15, v16

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "SA_EQ_INDEX_"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v14

    aput v14, v7, v9

    .line 718
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 723
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v14}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v14

    if-nez v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 726
    :cond_6
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v14}, Lcom/sec/android/app/soundalive/SAControlPanelService;->isSoundAliveSupportCurrentAudioPath(IZ)Z

    move-result v14

    if-eqz v14, :cond_7

    sget v14, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    const/4 v15, 0x4

    if-ne v14, v15, :cond_9

    .line 727
    :cond_7
    sget v14, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-nez v14, :cond_d

    .line 728
    const/4 v14, 0x1

    if-eq v11, v14, :cond_8

    const/4 v14, 0x2

    if-ne v11, v14, :cond_9

    .line 729
    :cond_8
    const/4 v11, 0x0

    .line 736
    :cond_9
    :goto_2
    if-ltz v13, :cond_e

    const/16 v14, 0x18

    if-gt v13, v14, :cond_e

    .line 737
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V

    .line 738
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 750
    .end local v5    # "curPath":I
    .end local v6    # "currentStrength":[I
    .end local v7    # "eqLevel":[I
    .end local v9    # "i":I
    .end local v11    # "preset":I
    .end local v13    # "squarePosition":I
    :cond_a
    const/4 v14, 0x2

    move/from16 v0, p1

    if-eq v0, v14, :cond_b

    const/4 v14, 0x4

    move/from16 v0, p1

    if-ne v0, v14, :cond_10

    .line 752
    :cond_b
    const-string v14, "CURRENT_PACKAGENAME"

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 753
    .local v10, "pkgName":Ljava/lang/String;
    const-string v14, "SA_ADAPT_STATE"

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 755
    .local v3, "adpatState":Z
    const-string v14, "com.sec.android.app.music"

    invoke-virtual {v14, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_10

    .line 757
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v1

    .line 758
    .local v1, "SA_STATUS_CHECK_2":I
    if-gez v1, :cond_f

    const/16 v14, -0x63

    if-eq v1, v14, :cond_f

    .line 759
    const-string v14, "SAControlPanelService"

    const-string v15, "checkStatus(mSA) return bad status for AdaptSound in SERVICE_STATE_OPEN"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    .end local v1    # "SA_STATUS_CHECK_2":I
    .end local v3    # "adpatState":Z
    .end local v10    # "pkgName":Ljava/lang/String;
    :cond_c
    :goto_3
    return-void

    .line 732
    .restart local v5    # "curPath":I
    .restart local v6    # "currentStrength":[I
    .restart local v7    # "eqLevel":[I
    .restart local v9    # "i":I
    .restart local v11    # "preset":I
    .restart local v13    # "squarePosition":I
    :cond_d
    const/4 v11, 0x0

    goto :goto_2

    .line 739
    :cond_e
    const/4 v14, -0x1

    if-ne v13, v14, :cond_a

    .line 740
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v7}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateBandLevel(Landroid/media/audiofx/SoundAlive;[I)V

    .line 741
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 743
    const/4 v9, 0x0

    :goto_4
    const/4 v14, 0x3

    if-ge v9, v14, :cond_a

    .line 744
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    aget v15, v6, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v9, v15}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateStrength(Landroid/media/audiofx/SoundAlive;II)V

    .line 745
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 743
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 763
    .end local v5    # "curPath":I
    .end local v6    # "currentStrength":[I
    .end local v7    # "eqLevel":[I
    .end local v9    # "i":I
    .end local v11    # "preset":I
    .end local v13    # "squarePosition":I
    .restart local v1    # "SA_STATUS_CHECK_2":I
    .restart local v3    # "adpatState":Z
    .restart local v10    # "pkgName":Ljava/lang/String;
    :cond_f
    if-eqz v3, :cond_14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v14}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isEnableAdaptSoundPath()Z

    move-result v14

    if-eqz v14, :cond_14

    .line 764
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    if-eqz v14, :cond_10

    .line 765
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    invoke-virtual {v14}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->prepare()V

    .line 766
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->activate(Z)V

    .line 767
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const/4 v15, 0x1

    invoke-static {v14, v15}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    .line 779
    .end local v1    # "SA_STATUS_CHECK_2":I
    .end local v3    # "adpatState":Z
    .end local v10    # "pkgName":Ljava/lang/String;
    :cond_10
    :goto_5
    const/4 v14, 0x3

    move/from16 v0, p1

    if-eq v0, v14, :cond_11

    const/4 v14, 0x4

    move/from16 v0, p1

    if-ne v0, v14, :cond_c

    .line 780
    :cond_11
    const-string v14, "CURRENT_PACKAGENAME"

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 783
    .restart local v10    # "pkgName":Ljava/lang/String;
    const-string v14, "com.sec.android.app.music"

    invoke-virtual {v14, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 784
    const-string v14, "SA_AUTO_STATE"

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 785
    .local v4, "autoState":Z
    if-eqz v4, :cond_12

    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->sEntrance:Z

    if-eqz v14, :cond_13

    :cond_12
    sget-boolean v14, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mCheckBoxState:Z

    if-eqz v14, :cond_c

    .line 786
    :cond_13
    const-string v14, "SA_GENRE_INDEX"

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 789
    .local v8, "genreIndex":I
    packed-switch v8, :pswitch_data_0

    .line 806
    :pswitch_0
    const/16 v2, 0xc

    .line 809
    .local v2, "SquarePosition":I
    :goto_6
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v16, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v15, v15, v16

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "SA_PRESET_INDEX"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 810
    .restart local v11    # "preset":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V

    .line 811
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v11}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 813
    new-instance v9, Landroid/content/Intent;

    const-string v14, "internal_SA_Auto_changed"

    invoke-direct {v9, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 814
    .local v9, "i":Landroid/content/Intent;
    const/high16 v14, 0x40000000    # 2.0f

    invoke-virtual {v9, v14}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 815
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/soundalive/SAControlPanelService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 770
    .end local v2    # "SquarePosition":I
    .end local v4    # "autoState":Z
    .end local v8    # "genreIndex":I
    .end local v9    # "i":Landroid/content/Intent;
    .end local v11    # "preset":I
    .restart local v1    # "SA_STATUS_CHECK_2":I
    .restart local v3    # "adpatState":Z
    :cond_14
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    if-eqz v14, :cond_10

    .line 771
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->activate(Z)V

    .line 772
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    goto/16 :goto_5

    .line 791
    .end local v1    # "SA_STATUS_CHECK_2":I
    .end local v3    # "adpatState":Z
    .restart local v4    # "autoState":Z
    .restart local v8    # "genreIndex":I
    :pswitch_1
    const/16 v2, 0xc

    .line 792
    .restart local v2    # "SquarePosition":I
    goto :goto_6

    .line 794
    .end local v2    # "SquarePosition":I
    :pswitch_2
    const/16 v2, 0xd

    .line 795
    .restart local v2    # "SquarePosition":I
    goto :goto_6

    .line 797
    .end local v2    # "SquarePosition":I
    :pswitch_3
    const/16 v2, 0xa

    .line 798
    .restart local v2    # "SquarePosition":I
    goto :goto_6

    .line 800
    .end local v2    # "SquarePosition":I
    :pswitch_4
    const/16 v2, 0x10

    .line 801
    .restart local v2    # "SquarePosition":I
    goto :goto_6

    .line 803
    .end local v2    # "SquarePosition":I
    :pswitch_5
    const/4 v2, 0x2

    .line 804
    .restart local v2    # "SquarePosition":I
    goto :goto_6

    .line 789
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private checkStatus(Landroid/media/audiofx/SoundAlive;)I
    .locals 6
    .param p1, "object"    # Landroid/media/audiofx/SoundAlive;

    .prologue
    .line 38
    const/4 v3, 0x2

    new-array v1, v3, [B

    .line 39
    .local v1, "dummyValue":[B
    if-nez p1, :cond_1

    .line 40
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_high:Z

    if-eqz v3, :cond_0

    .line 41
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : -99 SoundAlive object is null"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_0
    const/16 v0, -0x63

    .line 85
    :goto_0
    return v0

    .line 45
    :cond_1
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v1}, Landroid/media/audiofx/SoundAlive;->getParameter(I[B)I

    move-result v0

    .line 47
    .local v0, "checkReturn":I
    packed-switch v0, :pswitch_data_0

    .line 75
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_high:Z

    if-eqz v3, :cond_2

    .line 76
    const-string v3, "SAControlPanelService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkStatus() returned  unknown value  : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_2
    :goto_1
    if-gez v0, :cond_3

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v2

    .line 82
    .local v2, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v3, "AUDIOSESSION_ID"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v3

    sput v3, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeadAudioSession:I

    .line 84
    .end local v2    # "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    :cond_3
    sput v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentEffectState:I

    goto :goto_0

    .line 49
    :pswitch_0
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_high:Z

    if-eqz v3, :cond_2

    .line 50
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : AudioEffect.SUCCESS"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 54
    :pswitch_1
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : AudioEffect.ERROR"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 57
    :pswitch_2
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : AudioEffect.ALREADY_EXISTS"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 60
    :pswitch_3
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : AudioEffect.ERROR_NO_INIT"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 63
    :pswitch_4
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : AudioEffect.ERROR_BAD_VALUE"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 66
    :pswitch_5
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : AudioEffect.ERROR_INVALID_OPERATION"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 69
    :pswitch_6
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : AudioEffect.ERROR_NO_MEMORY"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 72
    :pswitch_7
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus() return : AudioEffect.ERROR_DEAD_OBJECT"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 47
    nop

    :pswitch_data_0
    .packed-switch -0x7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getCurrentAudioPath()I
    .locals 3

    .prologue
    .line 550
    const/4 v0, 0x0

    .line 551
    .local v0, "currentAudioPath":I
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathLineOut()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 552
    const-string v1, "SAControlPanelService"

    const-string v2, "getCurrentAudioPath :  currentAudioPath = SA_PATH_LINE_OUT"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    const/4 v0, 0x3

    .line 567
    :cond_0
    :goto_0
    return v0

    .line 554
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 555
    const-string v1, "SAControlPanelService"

    const-string v2, "getCurrentAudioPath :  currentAudioPath = SA_PATH_SPK"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    const/4 v0, 0x0

    goto :goto_0

    .line 557
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathHDMI()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 558
    const-string v1, "SAControlPanelService"

    const-string v2, "getCurrentAudioPath :  currentAudioPath = SA_PATH_HDMI"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    const/4 v0, 0x4

    goto :goto_0

    .line 560
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathEarjack()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 561
    const-string v1, "SAControlPanelService"

    const-string v2, "getCurrentAudioPath :  currentAudioPath = SA_PATH_EAR"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    const/4 v0, 0x1

    goto :goto_0

    .line 563
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-virtual {v1}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isAudioPathBT()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 564
    const-string v1, "SAControlPanelService"

    const-string v2, "getCurrentAudioPath :  currentAudioPath = SA_PATH_BT"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static isDeviceREMOTE_SUBMIX()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 678
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    .line 679
    const-string v1, "SAControlPanelService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isDeviceREMOTE_SUBMIX() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeviceState_REMOTE_SUBMIX:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :cond_0
    sget v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeviceState_REMOTE_SUBMIX:I

    if-ne v1, v0, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSoundAliveSupportCurrentAudioPath(IZ)Z
    .locals 2
    .param p1, "soundAlive"    # I
    .param p2, "enableToast"    # Z

    .prologue
    .line 685
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    .line 686
    const-string v0, "SAControlPanelService"

    const-string v1, "*****isSoundAliveSupportCurrentAudioPath()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_0
    new-instance v0, Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p0, p1, p2}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;->isSoundAliveSupportCurrentAudioPath(Landroid/content/Context;IZ)Z

    move-result v0

    return v0
.end method

.method private saveSoundAlivePreferenceAsBypass()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1104
    sget-boolean v2, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v2, :cond_0

    const-string v2, "SAControlPanelService"

    const-string v3, "*****saveSoundAlivePreference() in Service*****"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1106
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v1

    .line 1108
    .local v1, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SA_PRESET_INDEX"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SA_SQUARE_INDEX"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1110
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_1

    .line 1111
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SA_EQ_INDEX_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1113
    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v2, 0x3

    if-ge v0, v2, :cond_2

    .line 1115
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SA_STRENGH_INDEX_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1113
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1117
    :cond_2
    return-void
.end method

.method private updateBandLevel(Landroid/media/audiofx/SoundAlive;[I)V
    .locals 7
    .param p1, "mSoundAlive"    # Landroid/media/audiofx/SoundAlive;
    .param p2, "bandLevel"    # [I

    .prologue
    const/4 v6, 0x1

    .line 161
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v3, :cond_0

    const-string v3, "SAControlPanelService"

    const-string v4, "*****updateBandLevel()*****"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v0

    .line 164
    .local v0, "SA_STATUS_CHECK":I
    if-gez v0, :cond_2

    const/16 v3, -0x63

    if-eq v0, v3, :cond_2

    .line 165
    const-string v3, "SAControlPanelService"

    const-string v4, "checkStatus(mSoundAlive) return bad status in updateBandLevel"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_1
    :goto_0
    return-void

    .line 169
    :cond_2
    if-eqz p1, :cond_9

    if-eqz p2, :cond_9

    .line 172
    invoke-virtual {p1}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v3

    if-nez v3, :cond_3

    .line 173
    invoke-virtual {p1, v6}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 175
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v3, 0x7

    if-ge v1, v3, :cond_1

    .line 176
    aget v2, p2, v1

    .line 177
    .local v2, "level":I
    const/16 v3, -0xa

    if-lt v2, v3, :cond_8

    const/16 v3, 0xa

    if-gt v2, v3, :cond_8

    .line 180
    sget v3, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-nez v3, :cond_5

    if-eqz v1, :cond_4

    if-ne v1, v6, :cond_5

    .line 181
    :cond_4
    const/4 v2, 0x0

    .line 184
    :cond_5
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v3, :cond_6

    const-string v3, "SAControlPanelService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "band : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  level: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_6
    int-to-short v3, v1

    int-to-short v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/media/audiofx/SoundAlive;->setBandLevel(SS)V

    .line 175
    :cond_7
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 188
    :cond_8
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v3, :cond_7

    const-string v3, "SAControlPanelService"

    const-string v4, "bandLevel is out of bounds"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 192
    .end local v1    # "i":I
    .end local v2    # "level":I
    :cond_9
    sget-boolean v3, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v3, :cond_1

    const-string v3, "SAControlPanelService"

    const-string v4, "mSoundAlive or bandLevel is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V
    .locals 4
    .param p1, "mSoundAlive"    # Landroid/media/audiofx/SoundAlive;
    .param p2, "position"    # I

    .prologue
    .line 90
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    const-string v1, "SAControlPanelService"

    const-string v2, "*****updateSquareUIsetting()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v0

    .line 93
    .local v0, "SA_STATUS_CHECK":I
    if-gez v0, :cond_2

    const/16 v1, -0x63

    if-eq v0, v1, :cond_2

    .line 94
    const-string v1, "SAControlPanelService"

    const-string v2, "checkStatus(mSoundAlive) return bad status in updateSquareUIsetting"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_1
    :goto_0
    return-void

    .line 98
    :cond_2
    if-eqz p1, :cond_5

    if-ltz p2, :cond_5

    const/16 v1, 0x18

    if-gt p2, v1, :cond_5

    .line 100
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_3

    const-string v1, "SAControlPanelService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_3
    invoke-virtual {p1}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 102
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 103
    :cond_4
    div-int/lit8 v1, p2, 0x5

    rem-int/lit8 v2, p2, 0x5

    invoke-virtual {p1, v1, v2}, Landroid/media/audiofx/SoundAlive;->setSquarePostion(II)V

    goto :goto_0

    .line 106
    :cond_5
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_1

    const-string v1, "SAControlPanelService"

    const-string v2, "updateSquareUIsetting() not applied because position value is out of bounds"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateStrength(Landroid/media/audiofx/SoundAlive;II)V
    .locals 4
    .param p1, "mSoundAlive"    # Landroid/media/audiofx/SoundAlive;
    .param p2, "id"    # I
    .param p3, "onoff"    # I

    .prologue
    .line 137
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    const-string v1, "SAControlPanelService"

    const-string v2, "*****updateStrength()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v0

    .line 140
    .local v0, "SA_STATUS_CHECK":I
    if-gez v0, :cond_2

    const/16 v1, -0x63

    if-eq v0, v1, :cond_2

    .line 141
    const-string v1, "SAControlPanelService"

    const-string v2, "checkStatus(mSoundAlive) return bad status in updateStrength"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_1
    :goto_0
    return-void

    .line 145
    :cond_2
    if-eqz p1, :cond_5

    if-ltz p2, :cond_5

    const/4 v1, 0x2

    if-gt p2, v1, :cond_5

    .line 149
    invoke-virtual {p1}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v1

    if-nez v1, :cond_3

    .line 150
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 152
    :cond_3
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_4

    const-string v1, "SAControlPanelService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onoff : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_4
    int-to-short v1, p2

    int-to-short v2, p3

    invoke-virtual {p1, v1, v2}, Landroid/media/audiofx/SoundAlive;->setStrength(SS)V

    goto :goto_0

    .line 156
    :cond_5
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_1

    const-string v1, "SAControlPanelService"

    const-string v2, "mSoundAlive is null or unknown id"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V
    .locals 4
    .param p1, "mSoundAlive"    # Landroid/media/audiofx/SoundAlive;
    .param p2, "selectedUsePreset"    # I

    .prologue
    .line 111
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v1, :cond_0

    const-string v1, "SAControlPanelService"

    const-string v2, "*****updateUsePreset()*****"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v0

    .line 114
    .local v0, "SA_STATUS_CHECK":I
    if-gez v0, :cond_2

    const/16 v1, -0x63

    if-eq v0, v1, :cond_2

    .line 115
    const-string v1, "SAControlPanelService"

    const-string v2, "checkStatus(mSoundAlive) return bad status in updateUsePreset"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_1
    :goto_0
    return-void

    .line 119
    :cond_2
    if-eqz p1, :cond_6

    .line 120
    if-ltz p2, :cond_5

    const/4 v1, 0x5

    if-lt v1, p2, :cond_5

    .line 122
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_3

    const-string v1, "SAControlPanelService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "selectedUsePreset : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_3
    invoke-virtual {p1}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 125
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 127
    :cond_4
    int-to-short v1, p2

    invoke-virtual {p1, v1}, Landroid/media/audiofx/SoundAlive;->usePreset(S)V

    goto :goto_0

    .line 129
    :cond_5
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_1

    const-string v1, "SAControlPanelService"

    const-string v2, "selectedUsePreset is out of bounds"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 132
    :cond_6
    sget-boolean v1, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v1, :cond_1

    const-string v1, "SAControlPanelService"

    const-string v2, "mSoundAlive is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 207
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    const-string v0, "SAControlPanelService"

    const-string v1, "*****onBind()*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 214
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 215
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_0

    const-string v4, "SAControlPanelService"

    const-string v5, "*****onCreate() in Service*****"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_0
    iput-object v7, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 218
    iput-object v7, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 219
    new-instance v4, Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    invoke-direct {v4, p0}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    .line 220
    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentEffectState:I

    .line 221
    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeadAudioSession:I

    .line 222
    sput v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeviceState_REMOTE_SUBMIX:I

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 225
    .local v3, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v4, "SA_CURRENT_PATH_INDEX"

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v4

    sput v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 227
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getCurrentAudioPath()I

    move-result v0

    .line 228
    .local v0, "curPath":I
    sget v4, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    if-eq v0, v4, :cond_2

    .line 230
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_1

    .line 231
    const-string v4, "SAControlPanelService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate() audiopath reconfig mCurrentAudioPath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " curPath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :cond_1
    sput v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    .line 233
    const-string v4, "SA_CURRENT_PATH_INDEX"

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 234
    invoke-direct {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->PathReConfig()V

    .line 237
    :cond_2
    const-string v4, "SA_ACTIVITY_RENEW"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    .line 240
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 241
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v4, "com.sec.android.app.soundalive.ACTION_SA_DUPLICATED_USED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 242
    const-string v4, "internal_SA_Preset_changed"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 243
    const-string v4, "internal_SA_Square_changed"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 244
    const-string v4, "internal_SA_EQ_changed"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 245
    const-string v4, "internal_SA_3D_changed"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 246
    const-string v4, "internal_SA_Bass_changed"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 247
    const-string v4, "internal_SA_Clarity_changed"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 248
    const-string v4, "internal_SA_Adapt_changed"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 252
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 253
    .local v2, "filter2":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 254
    const-string v4, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 255
    const-string v4, "android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 256
    const-string v4, "android.media.action.HDMI_AUDIO_PLUG"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 257
    const-string v4, "android.intent.action.WIFI_DISPLAY"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 258
    const-string v4, "internal_SA_AudioPath"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 259
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mStateReceiver2:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 261
    return-void
.end method

.method public onDestroy()V
    .locals 9

    .prologue
    const/16 v6, 0xc

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 1121
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1122
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mStateReceiver2:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1123
    iput-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    .line 1124
    sget-boolean v4, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v4, :cond_0

    .line 1125
    const-string v4, "SAControlPanelService"

    const-string v5, "*****onDestroy() in Service*****"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    const-string v4, "SAControlPanelService"

    const-string v5, "onDestroy() service_state == SERVICE_STATE_STOP"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v3

    .line 1130
    .local v3, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v4, "SA_SERVICE_STATE"

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1132
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v4}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v0

    .line 1133
    .local v0, "SA_STATUS_CHECK":I
    if-gez v0, :cond_1

    const/16 v4, -0x63

    if-eq v0, v4, :cond_1

    .line 1134
    const-string v4, "SAControlPanelService"

    const-string v5, "checkStatus(mSA) return bad status for AdaptSound in onDestroy"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1135
    iput-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 1136
    iput-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 1139
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    if-eqz v4, :cond_4

    .line 1141
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v4, v6}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V

    .line 1142
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 1143
    const/4 v4, 0x7

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    .line 1144
    .local v1, "bypassEQ":[I
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v4, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateBandLevel(Landroid/media/audiofx/SoundAlive;[I)V

    .line 1145
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 1146
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v4, 0x3

    if-ge v2, v4, :cond_2

    .line 1147
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v4, v2, v7}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateStrength(Landroid/media/audiofx/SoundAlive;II)V

    .line 1148
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-direct {p0, v4, v7}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 1146
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1150
    :cond_2
    const-string v4, "SAControlPanelService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*****onDestroy() mSA.getEnabled() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v6}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v4}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v4, v7}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 1152
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v4}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 1153
    iput-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 1155
    .end local v1    # "bypassEQ":[I
    .end local v2    # "i":I
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    if-eqz v4, :cond_5

    .line 1156
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    .line 1157
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->activate(Z)V

    .line 1158
    iget-object v4, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    invoke-virtual {v4}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->release()V

    .line 1159
    iput-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 1162
    :cond_5
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 1163
    return-void

    .line 1143
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1173
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 1174
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    const-string v0, "SAControlPanelService"

    const-string v1, "*****onRebind() in Service*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1175
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 24
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 824
    sget-boolean v21, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v21, :cond_0

    .line 825
    const-string v21, "SAControlPanelService"

    const-string v22, "*****onStartCommand() in Service*****"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    const-string v21, "SAControlPanelService"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "onStartCommand mCurrentEffectState : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget v23, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentEffectState:I

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 829
    :cond_0
    if-nez p1, :cond_2

    .line 830
    sget-boolean v21, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v21, :cond_1

    const-string v21, "SAControlPanelService"

    const-string v22, "intent is null"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    :cond_1
    const/16 v21, 0x2

    .line 1100
    :goto_0
    return v21

    .line 834
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-object/from16 v21, v0

    if-nez v21, :cond_3

    .line 835
    new-instance v21, Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/framework/SecAudioManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAudioManager:Lcom/sec/android/app/soundalive/framework/SecAudioManager;

    .line 837
    :cond_3
    const-string v21, "SAControlPanelService"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mCurrentAudioPath is "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget v23, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/soundalive/PreferencesManager;

    move-result-object v18

    .line 840
    .local v18, "settings":Lcom/sec/android/app/soundalive/PreferencesManager;
    const-string v21, "AUDIOSESSION_ID"

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 841
    .local v6, "audioSession":I
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v23, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v22, v22, v23

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "SA_SQUARE_INDEX"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, -0x2

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v19

    .line 842
    .local v19, "squarePosition":I
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v23, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v22, v22, v23

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "SA_PRESET_INDEX"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, -0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v16

    .line 843
    .local v16, "preset":I
    const-string v21, "CURRENT_PACKAGENAME"

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 844
    .local v15, "pkgName":Ljava/lang/String;
    const/4 v11, 0x0

    .line 845
    .local v11, "isAudioTrackUse":Z
    const-string v4, "com.maxmpz.audioplayer"

    .line 846
    .local v4, "PowerANP":Ljava/lang/String;
    const-string v3, "com.iloen.melon"

    .line 847
    .local v3, "Melon":Ljava/lang/String;
    if-nez v15, :cond_5

    .line 848
    sget-boolean v21, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v21, :cond_4

    const-string v21, "SAControlPanelService"

    const-string v22, "pkgName is null"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    :cond_4
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 852
    :cond_5
    sget v21, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentEffectState:I

    if-gez v21, :cond_b

    .line 854
    sget v21, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeadAudioSession:I

    move/from16 v0, v21

    if-eq v6, v0, :cond_7

    .line 856
    const-string v21, "SAControlPanelService"

    const-string v22, "new session came out after dead session"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 858
    new-instance v21, Landroid/media/audiofx/SoundAlive;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v6}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 859
    new-instance v21, Lcom/sec/android/app/soundalive/framework/AdaptSound;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/app/soundalive/framework/AdaptSound;-><init>(Landroid/content/Context;II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v5

    .line 862
    .local v5, "SA_STATUS_CHECK":I
    if-gez v5, :cond_6

    const/16 v21, -0x63

    move/from16 v0, v21

    if-eq v5, v0, :cond_6

    .line 863
    const-string v21, "SAControlPanelService"

    const-string v22, "checkStatus(mSA) return bad status again with new session"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 866
    :cond_6
    const-string v21, "SAControlPanelService"

    const-string v22, "SoundAlive rebirth"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    const/16 v21, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 869
    const-string v21, "SA_SERVICE_STATE"

    const/16 v22, 0x9

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 870
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 873
    .end local v5    # "SA_STATUS_CHECK":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v5

    .line 874
    .restart local v5    # "SA_STATUS_CHECK":I
    if-gez v5, :cond_9

    const/16 v21, -0x63

    move/from16 v0, v21

    if-eq v5, v0, :cond_9

    .line 875
    const/16 v21, -0x7

    move/from16 v0, v21

    if-ne v5, v0, :cond_8

    .line 876
    new-instance v21, Landroid/media/audiofx/SoundAlive;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v6}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 877
    new-instance v21, Lcom/sec/android/app/soundalive/framework/AdaptSound;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/app/soundalive/framework/AdaptSound;-><init>(Landroid/content/Context;II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 878
    const/16 v21, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 879
    const-string v21, "SA_SERVICE_STATE"

    const/16 v22, 0x9

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 880
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 882
    :cond_8
    const-string v21, "SAControlPanelService"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SoundAlive native status is still unstable mDeadAudioSession : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget v23, Lcom/sec/android/app/soundalive/SAControlPanelService;->mDeadAudioSession:I

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 886
    :cond_9
    const-string v21, "SAControlPanelService"

    const-string v22, "olleh~ SoundAlive rebirth"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    if-eqz v21, :cond_a

    .line 890
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 891
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 892
    new-instance v21, Landroid/media/audiofx/SoundAlive;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v6}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 898
    :goto_1
    new-instance v21, Lcom/sec/android/app/soundalive/framework/AdaptSound;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/app/soundalive/framework/AdaptSound;-><init>(Landroid/content/Context;II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 900
    const/16 v21, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 902
    const-string v21, "SA_SERVICE_STATE"

    const/16 v22, 0x9

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 903
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 894
    :cond_a
    new-instance v21, Landroid/media/audiofx/SoundAlive;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v6}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 895
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 896
    new-instance v21, Landroid/media/audiofx/SoundAlive;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v6}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    goto :goto_1

    .line 908
    .end local v5    # "SA_STATUS_CHECK":I
    :cond_b
    const-string v21, "com.maxmpz.audioplayer"

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_c

    const-string v21, "com.iloen.melon"

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 909
    :cond_c
    const/4 v11, 0x1

    .line 912
    :cond_d
    const/16 v21, -0x2

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_12

    const/16 v21, -0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-ne v0, v1, :cond_12

    .line 914
    sget-boolean v21, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v21, :cond_e

    const-string v21, "SAControlPanelService"

    const-string v22, "After download & start music first time soundalive service have to have initial value of preset & square UI value"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    :cond_e
    const/16 v19, 0xc

    .line 916
    const/16 v16, 0x0

    .line 918
    const/4 v14, 0x0

    .local v14, "path":I
    :goto_2
    const/16 v21, 0x6

    move/from16 v0, v21

    if-ge v14, v0, :cond_11

    .line 919
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    aget-object v22, v22, v14

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "SA_PRESET_INDEX"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 920
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    aget-object v22, v22, v14

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "SA_SQUARE_INDEX"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 922
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    const/16 v21, 0x3

    move/from16 v0, v21

    if-ge v10, v0, :cond_f

    .line 923
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    aget-object v22, v22, v14

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "SA_STRENGH_INDEX_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 922
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 926
    :cond_f
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_4
    const/16 v21, 0x7

    move/from16 v0, v21

    if-ge v12, v0, :cond_10

    .line 927
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    aget-object v22, v22, v14

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "SA_EQ_INDEX_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 926
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 918
    :cond_10
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    .line 930
    .end local v10    # "i":I
    .end local v12    # "j":I
    :cond_11
    const-string v21, "SA_AUTO_STATE"

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putBoolean(Ljava/lang/String;Z)V

    .line 932
    .end local v14    # "path":I
    :cond_12
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v8, v0, [I

    .line 933
    .local v8, "currentStrength":[I
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_5
    const/16 v21, 0x3

    move/from16 v0, v21

    if-ge v10, v0, :cond_13

    .line 934
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v23, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v22, v22, v23

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "SA_STRENGH_INDEX_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v21

    aput v21, v8, v10

    .line 933
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 938
    :cond_13
    const/16 v21, 0x7

    move/from16 v0, v21

    new-array v9, v0, [I

    .line 939
    .local v9, "eqLevel":[I
    const/4 v10, 0x0

    :goto_6
    const/16 v21, 0x7

    move/from16 v0, v21

    if-ge v10, v0, :cond_14

    .line 940
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v22, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->AudioPath:[Ljava/lang/String;

    sget v23, Lcom/sec/android/app/soundalive/SAControlPanelService;->mCurrentAudioPath:I

    aget-object v22, v22, v23

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "SA_EQ_INDEX_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->getInt(Ljava/lang/String;I)I

    move-result v21

    aput v21, v9, v10

    .line 939
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 944
    :cond_14
    if-gez v6, :cond_15

    const/16 v21, 0x2

    goto/16 :goto_0

    .line 946
    :cond_15
    const-string v21, "SA_SERVICE_STATE"

    const/16 v22, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 948
    .local v17, "service_state":I
    const/16 v21, 0x9

    move/from16 v0, v17

    move/from16 v1, v21

    if-ne v0, v1, :cond_21

    .line 949
    sget-boolean v21, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v21, :cond_16

    .line 950
    const-string v21, "SAControlPanelService"

    const-string v22, "onStartCommand() service_state == SERVICE_STATE_OPEN"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v5

    .line 954
    .restart local v5    # "SA_STATUS_CHECK":I
    if-gez v5, :cond_17

    const/16 v21, -0x63

    move/from16 v0, v21

    if-eq v5, v0, :cond_17

    .line 955
    const-string v21, "SAControlPanelService"

    const-string v22, "checkStatus(mSA) return bad status in SERVICE_STATE_OPEN"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 959
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    if-eqz v21, :cond_18

    .line 960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 961
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 964
    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    const-string v22, "audio"

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/media/AudioManager;

    .line 965
    .local v13, "lAudioManager":Landroid/media/AudioManager;
    const/16 v21, 0x3

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v20

    .line 967
    .local v20, "volume":I
    new-instance v21, Landroid/media/audiofx/SoundAlive;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v6}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 969
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    if-eqz v21, :cond_20

    .line 970
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v21

    if-gez v21, :cond_19

    .line 971
    const-string v21, "SAControlPanelService"

    const-string v22, "checkStatus(mSA) return bad status after SA creat in SERVICE_STATE_OPEN"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 975
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v21

    if-nez v21, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 977
    :cond_1a
    const-string v21, "com.maxmpz.audioplayer"

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1c

    if-eqz v11, :cond_1c

    .line 978
    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_1b

    const/16 v21, 0x3

    add-int/lit8 v22, v20, -0x1

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v13, v0, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 979
    :cond_1b
    const/16 v21, 0x3

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v20

    move/from16 v2, v22

    invoke-virtual {v13, v0, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 981
    :cond_1c
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 987
    :goto_7
    const-string v21, "com.sec.android.app.music"

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_1e

    .line 989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1d

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/soundalive/framework/AdaptSound;->release()V

    .line 991
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 994
    :cond_1d
    new-instance v21, Lcom/sec/android/app/soundalive/framework/AdaptSound;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2, v6}, Lcom/sec/android/app/soundalive/framework/AdaptSound;-><init>(Landroid/content/Context;II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mAdaptSound:Lcom/sec/android/app/soundalive/framework/AdaptSound;

    .line 997
    :cond_1e
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 998
    const/16 v21, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 1000
    const-string v21, "SA_SERVICE_STATE"

    const/16 v22, 0x9

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    .line 1100
    .end local v5    # "SA_STATUS_CHECK":I
    .end local v13    # "lAudioManager":Landroid/media/AudioManager;
    .end local v20    # "volume":I
    :cond_1f
    :goto_8
    invoke-super/range {p0 .. p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v21

    goto/16 :goto_0

    .line 984
    .restart local v5    # "SA_STATUS_CHECK":I
    .restart local v13    # "lAudioManager":Landroid/media/AudioManager;
    .restart local v20    # "volume":I
    :cond_20
    const-string v21, "SAControlPanelService"

    const-string v22, "abnormal case we can not create soundalive"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 1001
    .end local v5    # "SA_STATUS_CHECK":I
    .end local v13    # "lAudioManager":Landroid/media/AudioManager;
    .end local v20    # "volume":I
    :cond_21
    const/16 v21, 0xb

    move/from16 v0, v17

    move/from16 v1, v21

    if-ne v0, v1, :cond_29

    .line 1003
    sget-boolean v21, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_mid:Z

    if-eqz v21, :cond_22

    .line 1004
    const-string v21, "SAControlPanelService"

    const-string v22, "onStartCommand() service_state == SERVICE_STATE_PAUSE"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v5

    .line 1008
    .restart local v5    # "SA_STATUS_CHECK":I
    if-gez v5, :cond_23

    const/16 v21, -0x63

    move/from16 v0, v21

    if-eq v5, v0, :cond_23

    .line 1009
    const-string v21, "SAControlPanelService"

    const-string v22, "checkStatus(mSA) return bad status in SERVICE_STATE_PAUSE"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 1013
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    if-eqz v21, :cond_25

    .line 1014
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v21

    if-eqz v21, :cond_24

    .line 1015
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 1016
    const-string v21, "SAControlPanelService"

    const-string v22, "SoundAlive Disabled successfully"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    :cond_24
    :goto_9
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 1024
    if-eqz v11, :cond_28

    .line 1026
    const-string v21, "com.iloen.melon"

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_26

    .line 1028
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0xc

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateSquareUIsetting(Landroid/media/audiofx/SoundAlive;I)V

    .line 1029
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 1030
    const/16 v21, 0x7

    move/from16 v0, v21

    new-array v7, v0, [I

    fill-array-data v7, :array_0

    .line 1031
    .local v7, "bypassEQ":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateBandLevel(Landroid/media/audiofx/SoundAlive;[I)V

    .line 1032
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 1033
    const/4 v10, 0x0

    :goto_a
    const/16 v21, 0x3

    move/from16 v0, v21

    if-ge v10, v0, :cond_26

    .line 1034
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v10, v2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateStrength(Landroid/media/audiofx/SoundAlive;II)V

    .line 1035
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/soundalive/SAControlPanelService;->updateUsePreset(Landroid/media/audiofx/SoundAlive;I)V

    .line 1033
    add-int/lit8 v10, v10, 0x1

    goto :goto_a

    .line 1019
    .end local v7    # "bypassEQ":[I
    :cond_25
    const-string v21, "SAControlPanelService"

    const-string v22, "abnormal case mSA is null"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9

    .line 1039
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    if-eqz v21, :cond_27

    .line 1040
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 1042
    :cond_27
    const/16 v21, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 1043
    const-string v21, "SAControlPanelService"

    const-string v22, "SoundAlive release need for specific 3rd party application which use AudioTrack instead of MediaPlayer for playback"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    :cond_28
    const-string v21, "SA_SERVICE_STATE"

    const/16 v22, 0xb

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_8

    .line 1048
    .end local v5    # "SA_STATUS_CHECK":I
    :cond_29
    const/16 v21, 0xa

    move/from16 v0, v17

    move/from16 v1, v21

    if-ne v0, v1, :cond_1f

    .line 1049
    sget-boolean v21, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v21, :cond_2a

    .line 1050
    const-string v21, "SAControlPanelService"

    const-string v22, "onStartCommand() service_state == SERVICE_STATE_RESUME"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1053
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v5

    .line 1054
    .restart local v5    # "SA_STATUS_CHECK":I
    if-gez v5, :cond_2b

    const/16 v21, -0x63

    move/from16 v0, v21

    if-eq v5, v0, :cond_2b

    .line 1055
    const-string v21, "SAControlPanelService"

    const-string v22, "checkStatus(mSA) return bad status in SERVICE_STATE_RESUME"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 1059
    :cond_2b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/SAControlPanelService;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    const-string v22, "audio"

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/media/AudioManager;

    .line 1060
    .restart local v13    # "lAudioManager":Landroid/media/AudioManager;
    const/16 v21, 0x3

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v20

    .line 1062
    .restart local v20    # "volume":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    if-nez v21, :cond_32

    .line 1063
    new-instance v21, Landroid/media/audiofx/SoundAlive;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v1, v6}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 1064
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    if-eqz v21, :cond_31

    .line 1066
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkStatus(Landroid/media/audiofx/SoundAlive;)I

    move-result v21

    if-gez v21, :cond_2c

    .line 1067
    const-string v21, "SAControlPanelService"

    const-string v22, "checkStatus(mSA) return bad status after SA creat in SERVICE_STATE_RESUME"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1068
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 1071
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v21

    if-nez v21, :cond_2d

    .line 1072
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 1075
    :cond_2d
    const-string v21, "com.maxmpz.audioplayer"

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2f

    if-eqz v11, :cond_2f

    .line 1076
    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_2e

    const/16 v21, 0x3

    add-int/lit8 v22, v20, -0x1

    const/16 v23, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v13, v0, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1077
    :cond_2e
    const/16 v21, 0x3

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v20

    move/from16 v2, v22

    invoke-virtual {v13, v0, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1080
    :cond_2f
    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 1082
    const-string v21, "SAControlPanelService"

    const-string v22, "abnormal case soundalive was null so re-create & set"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    :cond_30
    :goto_b
    const/16 v21, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 1096
    const/16 v21, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/sec/android/app/soundalive/SAControlPanelService;->checkAndSetEffect(I)V

    .line 1097
    const-string v21, "SA_SERVICE_STATE"

    const/16 v22, 0xa

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/soundalive/PreferencesManager;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_8

    .line 1084
    :cond_31
    const-string v21, "SAControlPanelService"

    const-string v22, "abnormal case we can not create soundalive 2"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1085
    const/16 v21, 0x2

    goto/16 :goto_0

    .line 1088
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/media/audiofx/SoundAlive;->getEnabled()Z

    move-result v21

    if-nez v21, :cond_30

    .line 1089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/SAControlPanelService;->mSA:Landroid/media/audiofx/SoundAlive;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 1090
    sget-boolean v21, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v21, :cond_30

    .line 1091
    const-string v21, "SAControlPanelService"

    const-string v22, "SoundAlive Enabled successfully"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 1030
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1167
    sget-boolean v0, Lcom/sec/android/app/soundalive/SAControlPanelAttributes;->debug_level_low:Z

    if-eqz v0, :cond_0

    const-string v0, "SAControlPanelService"

    const-string v1, "*****onUnbind() in Service*****"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1168
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
