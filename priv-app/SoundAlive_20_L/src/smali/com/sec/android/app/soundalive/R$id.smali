.class public final Lcom/sec/android/app/soundalive/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final advanced_button:I = 0x7f090008

.field public static final advanced_button_bottom_line:I = 0x7f090009

.field public static final advanced_button_layout:I = 0x7f090007

.field public static final advanced_layout:I = 0x7f09000d

.field public static final basic_button:I = 0x7f090005

.field public static final basic_button_bottom_line:I = 0x7f090006

.field public static final basic_button_layout:I = 0x7f090004

.field public static final basic_dbscale_layout:I = 0x7f09000e

.field public static final basic_eq_control0:I = 0x7f090010

.field public static final basic_eq_control1:I = 0x7f090011

.field public static final basic_eq_control2:I = 0x7f090012

.field public static final basic_eq_control3:I = 0x7f090013

.field public static final basic_eq_control4:I = 0x7f090014

.field public static final basic_eq_control5:I = 0x7f090015

.field public static final basic_eq_control6:I = 0x7f090016

.field public static final basic_layout:I = 0x7f090017

.field public static final dark:I = 0x7f090002

.field public static final eq_control_bar:I = 0x7f090030

.field public static final eq_control_bar_level_line:I = 0x7f09000f

.field public static final eq_control_text:I = 0x7f090031

.field public static final horizontal:I = 0x7f090000

.field public static final light:I = 0x7f090003

.field public static final new_basic_soundalive:I = 0x7f09000a

.field public static final new_soundalive_advanced_effect_Button_3d:I = 0x7f090018

.field public static final new_soundalive_advanced_effect_Button_Bass:I = 0x7f090019

.field public static final new_soundalive_advanced_effect_Button_Clarity:I = 0x7f09001a

.field public static final new_soundalive_effect_bottomline:I = 0x7f09001c

.field public static final new_soundalive_effects_club:I = 0x7f090024

.field public static final new_soundalive_effects_concert_hall:I = 0x7f090026

.field public static final new_soundalive_effects_none:I = 0x7f09001b

.field public static final new_soundalive_effects_studio:I = 0x7f090022

.field public static final new_soundalive_effects_tube_amp_effect:I = 0x7f09001e

.field public static final new_soundalive_effects_virtual_71_ch:I = 0x7f090020

.field public static final new_soundalive_gridview_layout:I = 0x7f09002a

.field public static final new_soundalive_imageView_club:I = 0x7f090025

.field public static final new_soundalive_imageView_concert_hall:I = 0x7f090027

.field public static final new_soundalive_imageView_none:I = 0x7f09001d

.field public static final new_soundalive_imageView_studio:I = 0x7f090023

.field public static final new_soundalive_imageView_tube_amp_effect:I = 0x7f09001f

.field public static final new_soundalive_imageView_virtual_71_ch:I = 0x7f090021

.field public static final new_soundalive_preset_adapt_checkbox:I = 0x7f090029

.field public static final new_soundalive_preset_auto_checkbox:I = 0x7f090028

.field public static final new_soundalive_preset_gridview:I = 0x7f09002b

.field public static final new_soundalive_preset_left_text:I = 0x7f09002c

.field public static final new_soundalive_preset_right_text:I = 0x7f09002d

.field public static final new_soundalive_square_cell_image:I = 0x7f09002e

.field public static final new_soundalive_square_cell_text:I = 0x7f09002f

.field public static final seekbubble:I = 0x7f09000c

.field public static final square_layout:I = 0x7f09000b

.field public static final vertical:I = 0x7f090001


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
