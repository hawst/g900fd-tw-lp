.class public final Lcom/sec/android/app/soundalive/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final TwProgressBar:[I

.field public static final TwProgressBar_dualcolorProgress:I = 0xd

.field public static final TwProgressBar_progress:I = 0x5

.field public static final TwProgressBar_secondaryProgress:I = 0x6

.field public static final TwProgressBar_theme:I = 0x12

.field public static final TwProgressBar_twBackgroundColor:I = 0xa

.field public static final TwProgressBar_twBackgroundDrawable:I = 0x7

.field public static final TwProgressBar_twDualColorProgressColor:I = 0xf

.field public static final TwProgressBar_twDualColorProgressDrawable:I = 0xe

.field public static final TwProgressBar_twIndicatorThickness:I = 0x10

.field public static final TwProgressBar_twMax:I = 0x4

.field public static final TwProgressBar_twMaxHeight:I = 0x3

.field public static final TwProgressBar_twMaxWidth:I = 0x1

.field public static final TwProgressBar_twMinHeight:I = 0x2

.field public static final TwProgressBar_twMinWidth:I = 0x0

.field public static final TwProgressBar_twProgressColor:I = 0xb

.field public static final TwProgressBar_twProgressDrawable:I = 0x8

.field public static final TwProgressBar_twProgressOrientation:I = 0x11

.field public static final TwProgressBar_twSecondaryColor:I = 0xc

.field public static final TwProgressBar_twSecondaryDrawable:I = 0x9

.field public static final TwSeekBar:[I

.field public static final TwSeekBar_twSeekBarDisableAlpha:I = 0x3

.field public static final TwSeekBar_twSeekBarIncrement:I = 0x2

.field public static final TwSeekBar_twSeekBarSeekable:I = 0x4

.field public static final TwSeekBar_twSeekBarThumb:I = 0x0

.field public static final TwSeekBar_twSeekBarThumbOffset:I = 0x1

.field public static final TwSeekBar_twSeekThumbFontBoldStyle:I = 0x7

.field public static final TwSeekBar_twSeekThumbFontColor:I = 0x5

.field public static final TwSeekBar_twSeekThumbFontEnable:I = 0x8

.field public static final TwSeekBar_twSeekThumbFontSize:I = 0x6

.field public static final TwTheme:[I

.field public static final TwTheme_twProgressBarStyle:I = 0x1

.field public static final TwTheme_twSeekBarStyle:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 551
    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/soundalive/R$styleable;->TwProgressBar:[I

    .line 834
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/soundalive/R$styleable;->TwSeekBar:[I

    .line 973
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/soundalive/R$styleable;->TwTheme:[I

    return-void

    .line 551
    nop

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
    .end array-data

    .line 834
    :array_1
    .array-data 4
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
    .end array-data

    .line 973
    :array_2
    .array-data 4
        0x7f01001c
        0x7f01001d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
