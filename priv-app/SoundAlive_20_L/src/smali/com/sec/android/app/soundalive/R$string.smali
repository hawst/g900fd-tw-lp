.class public final Lcom/sec/android/app/soundalive/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final adapt_sound:I = 0x7f070000

.field public static final advanced:I = 0x7f070001

.field public static final app_name:I = 0x7f070002

.field public static final auto:I = 0x7f070003

.field public static final basic:I = 0x7f070004

.field public static final cancel:I = 0x7f070005

.field public static final classic:I = 0x7f070006

.field public static final club:I = 0x7f070007

.field public static final concert_hall:I = 0x7f070008

.field public static final dance:I = 0x7f070009

.field public static final done:I = 0x7f07000a

.field public static final effect:I = 0x7f07000b

.field public static final eq_1:I = 0x7f07000c

.field public static final eq_2:I = 0x7f07000d

.field public static final eq_3:I = 0x7f07000e

.field public static final eq_4:I = 0x7f07000f

.field public static final eq_5:I = 0x7f070010

.field public static final eq_6:I = 0x7f070011

.field public static final eq_7:I = 0x7f070012

.field public static final jazz:I = 0x7f070013

.field public static final new_sound_alive_auto_off:I = 0x7f070014

.field public static final new_sound_alive_bass:I = 0x7f070015

.field public static final new_sound_alive_club_explanation:I = 0x7f070016

.field public static final new_sound_alive_concert_hall_explanation:I = 0x7f070017

.field public static final new_sound_alive_instrument:I = 0x7f070018

.field public static final new_sound_alive_none_explanation:I = 0x7f070019

.field public static final new_sound_alive_studio_explanation:I = 0x7f07001a

.field public static final new_sound_alive_treble:I = 0x7f07001b

.field public static final new_sound_alive_tube_amp_effect_explanation:I = 0x7f07001c

.field public static final new_sound_alive_virtual_71_ch_explanation:I = 0x7f07001d

.field public static final new_sound_alive_vocal:I = 0x7f07001e

.field public static final new_soundalive_3D:I = 0x7f07001f

.field public static final new_soundalive_bass:I = 0x7f070020

.field public static final new_soundalive_clarity:I = 0x7f070021

.field public static final none:I = 0x7f070022

.field public static final normal:I = 0x7f070023

.field public static final ok:I = 0x7f070024

.field public static final picker_title:I = 0x7f070025

.field public static final pop:I = 0x7f070026

.field public static final reset:I = 0x7f070027

.field public static final rock:I = 0x7f070028

.field public static final sound_3d_effect_not_supported:I = 0x7f070029

.field public static final sound_alive:I = 0x7f07002a

.field public static final sound_effect_not_supported_by_audiopath:I = 0x7f07002b

.field public static final sound_effect_not_supported_by_bluetooth:I = 0x7f07002c

.field public static final sound_effect_not_supported_by_hdmi:I = 0x7f07002d

.field public static final sound_effect_not_supported_by_line_out:I = 0x7f07002e

.field public static final sound_effect_supported_only_headphones:I = 0x7f07002f

.field public static final sound_effect_works_in_earphone_bt_only:I = 0x7f070030

.field public static final sound_effect_works_in_earphone_only:I = 0x7f070031

.field public static final stms_appgroup:I = 0x7f070032

.field public static final studio:I = 0x7f070033

.field public static final tts_button:I = 0x7f070034

.field public static final tts_control_bar:I = 0x7f070035

.field public static final tts_not_selected:I = 0x7f070036

.field public static final tts_selected:I = 0x7f070037

.field public static final tts_swipe_two_fingers:I = 0x7f070038

.field public static final tts_tab:I = 0x7f070039

.field public static final tts_tab_currently_set:I = 0x7f07003a

.field public static final tts_tab_n_of_n:I = 0x7f07003b

.field public static final tube_amp_effect:I = 0x7f07003c

.field public static final tw_seekbarsplit_seekbar_control:I = 0x7f07003d

.field public static final virtual_71_ch:I = 0x7f07003e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
