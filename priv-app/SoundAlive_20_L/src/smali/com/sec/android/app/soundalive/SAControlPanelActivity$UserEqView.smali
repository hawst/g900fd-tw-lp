.class Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;
.super Landroid/view/View;
.source "SAControlPanelActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/soundalive/SAControlPanelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserEqView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/soundalive/SAControlPanelActivity;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2340
    iput-object p1, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    .line 2341
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2342
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2346
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2349
    const/high16 v7, 0x40a00000    # 5.0f

    .line 2350
    .local v7, "textPosX":F
    const/high16 v5, 0x429c0000    # 78.0f

    .line 2351
    .local v5, "paddingTopPortrait":F
    const/high16 v4, 0x42960000    # 75.0f

    .line 2353
    .local v4, "paddingTopHorizontal":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2354
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mDbLinePaddingTop:I
    invoke-static {v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3200(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2355
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f04001a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 2356
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const-string v9, "sec-roboto-regular"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2357
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getScreenRawWidth()I
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3400(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v6

    .line 2358
    .local v6, "screenRawidth":I
    const/4 v3, 0x0

    .line 2359
    .local v3, "offset":I
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->isPortrait()Z
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$2800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Z

    move-result v1

    .line 2361
    .local v1, "isPortrait":Z
    if-nez v1, :cond_0

    .line 2362
    const/high16 v7, 0x42480000    # 50.0f

    .line 2365
    :cond_0
    if-eqz v1, :cond_1

    const/16 v8, 0x5a0

    if-ge v6, v8, :cond_2

    :cond_1
    if-nez v1, :cond_5

    const/16 v8, 0xa00

    if-lt v6, v8, :cond_5

    .line 2367
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const/high16 v9, 0x42180000    # 38.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2368
    if-eqz v1, :cond_3

    .line 2369
    const/high16 v7, 0x41a00000    # 20.0f

    .line 2371
    :cond_3
    const/high16 v5, 0x42e20000    # 113.0f

    .line 2372
    const/high16 v4, 0x42d40000    # 106.0f

    .line 2373
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea3d70a3d70a3dL    # 0.82

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2374
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea666666666666L    # 0.825

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2438
    :cond_4
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/16 v8, 0xa

    if-gt v0, v8, :cond_1c

    .line 2439
    if-eqz v1, :cond_1b

    .line 2440
    int-to-double v8, v0

    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSeekBarHeight:I
    invoke-static {v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v10

    int-to-double v10, v10

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4024000000000000L    # 10.0

    div-double/2addr v8, v10

    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3500(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)D

    move-result-wide v10

    mul-double/2addr v8, v10

    float-to-double v10, v5

    add-double/2addr v8, v10

    double-to-float v2, v8

    .line 2445
    .local v2, "linePosY":F
    :goto_2
    sparse-switch v0, :sswitch_data_0

    .line 2438
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2375
    .end local v0    # "i":I
    .end local v2    # "linePosY":F
    :cond_5
    if-eqz v1, :cond_6

    const/16 v8, 0x438

    if-ge v6, v8, :cond_7

    :cond_6
    if-nez v1, :cond_8

    const/16 v8, 0x780

    if-lt v6, v8, :cond_8

    .line 2376
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const/high16 v9, 0x42180000    # 38.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2377
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2378
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea6e978d4fdf3bL    # 0.826

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto :goto_0

    .line 2379
    :cond_8
    if-eqz v1, :cond_9

    const/16 v8, 0x2d0

    if-ge v6, v8, :cond_a

    :cond_9
    if-nez v1, :cond_d

    const/16 v8, 0x500

    if-lt v6, v8, :cond_d

    .line 2380
    :cond_a
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const/high16 v9, 0x41c80000    # 25.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2381
    if-eqz v1, :cond_b

    .line 2382
    const/high16 v7, 0x41400000    # 12.0f

    .line 2384
    :cond_b
    const/16 v3, 0x8

    .line 2385
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getDensity()I
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v8

    const/16 v9, 0x140

    if-ne v8, v9, :cond_c

    .line 2387
    const/high16 v5, 0x42540000    # 53.0f

    .line 2388
    const/high16 v4, 0x42480000    # 50.0f

    .line 2389
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2390
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea5604189374bcL    # 0.823

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2392
    :cond_c
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea2d0e56041893L    # 0.818

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2393
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fe62d0e56041893L    # 0.693

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2396
    :cond_d
    if-eqz v1, :cond_e

    const/16 v8, 0x21c

    if-gt v6, v8, :cond_f

    :cond_e
    if-nez v1, :cond_11

    const/16 v8, 0x3c0

    if-le v6, v8, :cond_11

    .line 2397
    :cond_f
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2398
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # invokes: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->getDensity()I
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3700(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v8

    const/16 v9, 0xf0

    if-ne v8, v9, :cond_10

    .line 2400
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3febc6a7ef9db22dL    # 0.868

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2401
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fe9b22d0e560419L    # 0.803

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2403
    :cond_10
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea2d0e56041893L    # 0.818

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2404
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fe62d0e56041893L    # 0.693

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2408
    :cond_11
    if-eqz v1, :cond_12

    const/16 v8, 0x1e0

    if-gt v6, v8, :cond_13

    :cond_12
    if-nez v1, :cond_14

    const/16 v8, 0x320

    if-le v6, v8, :cond_14

    .line 2409
    :cond_13
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2410
    const/high16 v5, 0x42240000    # 41.0f

    .line 2411
    const/high16 v4, 0x42140000    # 37.0f

    .line 2412
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2413
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fea45a1cac08312L    # 0.821

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2414
    if-nez v1, :cond_4

    .line 2415
    const/high16 v7, 0x41a00000    # 20.0f

    goto/16 :goto_0

    .line 2417
    :cond_14
    if-eqz v1, :cond_15

    const/16 v8, 0x140

    if-gt v6, v8, :cond_16

    :cond_15
    if-nez v1, :cond_17

    const/16 v8, 0x1e0

    if-le v6, v8, :cond_17

    .line 2418
    :cond_16
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2419
    const/high16 v5, 0x41f00000    # 30.0f

    .line 2420
    const/high16 v4, 0x41f00000    # 30.0f

    .line 2421
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3feab851eb851eb8L    # 0.835

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2422
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2423
    if-nez v1, :cond_4

    .line 2424
    const/high16 v7, -0x40800000    # -1.0f

    goto/16 :goto_0

    .line 2426
    :cond_17
    if-eqz v1, :cond_18

    const/16 v8, 0xf0

    if-gt v6, v8, :cond_19

    :cond_18
    if-nez v1, :cond_1a

    const/16 v8, 0x140

    if-le v6, v8, :cond_1a

    .line 2427
    :cond_19
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const/high16 v9, 0x41600000    # 14.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2428
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2429
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fe5c28f5c28f5c3L    # 0.68

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2431
    :cond_1a
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v8}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v8

    const/high16 v9, 0x41300000    # 11.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2432
    const/high16 v7, 0x40000000    # 2.0f

    .line 2433
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRatePortlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3502(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    .line 2434
    iget-object v8, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    const-wide v10, 0x3fe5c28f5c28f5c3L    # 0.68

    # setter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v8, v10, v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3602(Lcom/sec/android/app/soundalive/SAControlPanelActivity;D)D

    goto/16 :goto_0

    .line 2442
    .restart local v0    # "i":I
    :cond_1b
    int-to-double v8, v0

    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mSeekBarHeight:I
    invoke-static {v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3800(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)I

    move-result v10

    int-to-double v10, v10

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4024000000000000L    # 10.0

    div-double/2addr v8, v10

    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mRateLandlinePosY:D
    invoke-static {v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3600(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)D

    move-result-wide v10

    mul-double/2addr v8, v10

    float-to-double v10, v4

    add-double/2addr v8, v10

    double-to-float v2, v8

    .restart local v2    # "linePosY":F
    goto/16 :goto_2

    .line 2447
    :sswitch_0
    const-string v8, " 10dB"

    iget-object v9, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v9}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Paint;->ascent()F

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float v9, v2, v9

    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v10

    invoke-virtual {p1, v8, v7, v9, v10}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 2450
    :sswitch_1
    const-string v8, "  0dB"

    int-to-float v9, v3

    add-float/2addr v9, v7

    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Paint;->ascent()F

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float v10, v2, v10

    iget-object v11, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v11

    invoke-virtual {p1, v8, v9, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 2453
    :sswitch_2
    const-string v8, "-10dB"

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float v9, v7, v9

    iget-object v10, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v10}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Paint;->ascent()F

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float v10, v2, v10

    iget-object v11, p0, Lcom/sec/android/app/soundalive/SAControlPanelActivity$UserEqView;->this$0:Lcom/sec/android/app/soundalive/SAControlPanelActivity;

    # getter for: Lcom/sec/android/app/soundalive/SAControlPanelActivity;->mPaint:Landroid/graphics/Paint;
    invoke-static {v11}, Lcom/sec/android/app/soundalive/SAControlPanelActivity;->access$3300(Lcom/sec/android/app/soundalive/SAControlPanelActivity;)Landroid/graphics/Paint;

    move-result-object v11

    invoke-virtual {p1, v8, v9, v10, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 2459
    .end local v2    # "linePosY":F
    :cond_1c
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2460
    return-void

    .line 2445
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method
