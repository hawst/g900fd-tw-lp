.class public Lcom/sec/android/app/soundalive/widget/TwProgressBar;
.super Landroid/view/View;
.source "TwProgressBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/soundalive/widget/TwProgressBar$1;,
        Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;,
        Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;
    }
.end annotation


# static fields
.field private static final CLEAR_ALPHA_MASK:I = 0xffffff

.field private static final CORNERS:[F

.field private static final HIGH_ALPHA:I = 0x32000000

.field protected static final HORIZONTAL:I = 0x0

.field private static final KK_AMERICANO:Z

.field private static final LOW_ALPHA:I = -0x1000000

.field private static final MAX_LEVEL:I = 0x2710

.field private static final MED_ALPHA:I = -0x6a000000

.field protected static final TOUCHWIZ_DARK_THEME:I = 0x0

.field protected static final TOUCHWIZ_LIGHT_THEME:I = 0x1

.field private static final UNDEFINED_COLOR:I = 0x0

.field protected static final VERTICAL:I = 0x1


# instance fields
.field private final DEFAULT_MAX:I

.field private final DEFAULT_MAX_WIDTH:I

.field private final DEFAULT_MIN_WIDTH:I

.field private final DEFAULT_PROGRESS:I

.field private final INVALID_PROGRESS_HEIGHT:I

.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mDrawBackground:Z

.field private mDrawFirstProgress:Z

.field private mDrawSecondaryProgress:Z

.field private mDualColorProgress:I

.field private mDualColorProgressBar:Z

.field private mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

.field private mIndicatorThickness:I

.field private mMax:I

.field mMaxHeight:I

.field mMaxWidth:I

.field mMinHeight:I

.field mMinWidth:I

.field mPaddingBottom:I

.field mPaddingLeft:I

.field mPaddingRight:I

.field mPaddingTop:I

.field private mProgress:I

.field private mProgressBarMode:I

.field private mProgressDrawable:Landroid/graphics/drawable/Drawable;

.field protected mProgressRect:Landroid/graphics/Rect;

.field private mRefreshProgressRunnable:Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;

.field private mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

.field private mSecondaryProgress:I

.field protected mTheme:I

.field private mUiThreadId:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 68
    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->CORNERS:[F

    .line 116
    const-string v0, "americano"

    const-string v1, "ro.build.scafe"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->KK_AMERICANO:Z

    return-void

    .line 68
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 182
    const v0, 0x7f01001d

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 183
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v10, -0x1

    const/16 v6, 0xb

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 186
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->DEFAULT_MIN_WIDTH:I

    .line 58
    const/16 v6, 0x30

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->DEFAULT_MAX_WIDTH:I

    .line 59
    const/16 v6, 0x64

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->DEFAULT_MAX:I

    .line 60
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->DEFAULT_PROGRESS:I

    .line 95
    iput-object v7, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 100
    iput-object v7, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 105
    iput-object v7, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    .line 107
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawBackground:Z

    .line 108
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawFirstProgress:Z

    .line 109
    iput-boolean v9, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawSecondaryProgress:Z

    .line 112
    iput-object v7, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 114
    iput-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    .line 122
    iput-object v7, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    .line 147
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mIndicatorThickness:I

    .line 148
    iput v10, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->INVALID_PROGRESS_HEIGHT:I

    .line 153
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressBarMode:I

    .line 158
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mTheme:I

    .line 159
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mPaddingTop:I

    .line 160
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mPaddingBottom:I

    .line 161
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mPaddingLeft:I

    .line 162
    iput v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mPaddingRight:I

    .line 187
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mUiThreadId:J

    .line 189
    sget-object v6, Lcom/sec/android/app/soundalive/R$styleable;->TwProgressBar:[I

    invoke-virtual {p1, p2, v6, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 192
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v6, 0x0

    const/16 v7, 0xb

    :try_start_0
    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinWidth:I

    .line 193
    const/4 v6, 0x1

    const/16 v7, 0x30

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMaxWidth:I

    .line 194
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050022

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    .line 195
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050022

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMaxHeight:I

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingTop()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mPaddingTop:I

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingBottom()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mPaddingBottom:I

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingLeft()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mPaddingLeft:I

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingRight()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mPaddingRight:I

    .line 202
    const/4 v6, 0x4

    const/16 v7, 0x64

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    .line 204
    const/4 v6, 0x5

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    .line 206
    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryProgress:I

    .line 208
    const/4 v6, 0x7

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 209
    iget-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_0

    .line 210
    const/16 v6, 0xa

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 211
    .local v1, "backColor":I
    if-eqz v1, :cond_4

    .line 212
    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setBackgroundColor(I)V

    .line 218
    .end local v1    # "backColor":I
    :cond_0
    :goto_0
    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 219
    iget-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_1

    .line 220
    const/16 v6, 0xb

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 221
    .local v4, "progressColor":I
    if-eqz v4, :cond_5

    .line 222
    invoke-virtual {p0, v4}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setProgressColor(I)V

    .line 228
    .end local v4    # "progressColor":I
    :cond_1
    :goto_1
    const/16 v6, 0x10

    const/4 v7, -0x1

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mIndicatorThickness:I

    .line 230
    const/16 v6, 0x11

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressBarMode:I

    .line 232
    const/16 v6, 0x12

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mTheme:I

    .line 234
    const/16 v6, 0x9

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    .line 235
    iget-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_2

    .line 236
    const/16 v6, 0xc

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 237
    .local v5, "secondaryColor":I
    if-eqz v5, :cond_6

    .line 238
    invoke-virtual {p0, v5}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setSecondaryProgressColor(I)V

    .line 244
    .end local v5    # "secondaryColor":I
    :cond_2
    :goto_2
    const/16 v6, 0xd

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    .line 245
    const/16 v6, 0xe

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 246
    iget-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_3

    .line 247
    const/16 v6, 0xf

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 248
    .local v2, "dualcolorprogressColor":I
    if-eqz v2, :cond_7

    .line 249
    invoke-virtual {p0, v2}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setDualColorProgressColor(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    .end local v2    # "dualcolorprogressColor":I
    :cond_3
    :goto_3
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 263
    return-void

    .line 214
    .restart local v1    # "backColor":I
    :cond_4
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02001e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 258
    .end local v1    # "backColor":I
    :catch_0
    move-exception v3

    .line 259
    .local v3, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_3

    .line 224
    .end local v3    # "ex":Ljava/lang/Exception;
    .restart local v4    # "progressColor":I
    :cond_5
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02001d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 240
    .end local v4    # "progressColor":I
    .restart local v5    # "secondaryColor":I
    :cond_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020020

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 251
    .end local v5    # "secondaryColor":I
    .restart local v2    # "dualcolorprogressColor":I
    :cond_7
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02001f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isHorizontal"    # Z

    .prologue
    const/4 v1, 0x1

    .line 173
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 174
    if-ne p2, v1, :cond_0

    .line 175
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressBarMode:I

    .line 179
    :goto_0
    return-void

    .line 177
    :cond_0
    iput v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressBarMode:I

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/android/app/soundalive/widget/TwProgressBar;IIZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/widget/TwProgressBar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->doRefreshProgress(IIZ)V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/soundalive/widget/TwProgressBar;Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;)Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/soundalive/widget/TwProgressBar;
    .param p1, "x1"    # Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mRefreshProgressRunnable:Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;

    return-object p1
.end method

.method private declared-synchronized doRefreshProgress(IIZ)V
    .locals 12
    .param p1, "id"    # I
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const v11, 0x461c4000    # 10000.0f

    const/4 v7, 0x0

    .line 811
    monitor-enter p0

    :try_start_0
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-lez v8, :cond_4

    int-to-float v8, p2

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    int-to-float v9, v9

    div-float v6, v8, v9

    .line 812
    .local v6, "scale":F
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 813
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_a

    .line 814
    const/4 v5, 0x0

    .line 816
    .local v5, "progressDrawable":Landroid/graphics/drawable/Drawable;
    instance-of v8, v1, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v8, :cond_0

    .line 817
    move-object v0, v1

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    move-object v8, v0

    invoke-virtual {v8, p1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 820
    :cond_0
    mul-float v8, v6, v11

    float-to-int v3, v8

    .line 821
    .local v3, "level":I
    if-eqz v5, :cond_5

    .end local v5    # "progressDrawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    invoke-virtual {v5, v3}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 824
    iget-boolean v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v8, :cond_2

    .line 825
    const/4 v2, 0x0

    .line 827
    .local v2, "dualDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v8, v8, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v8, :cond_1

    .line 828
    iget-object v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v8, Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v8, p1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 834
    :cond_1
    if-eqz v2, :cond_2

    .line 835
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    if-gt p2, v8, :cond_7

    .line 836
    if-eqz v2, :cond_6

    .end local v2    # "dualDrawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 848
    .end local v3    # "level":I
    :cond_2
    :goto_3
    const v8, 0x102000d

    if-ne p1, v8, :cond_3

    .line 849
    invoke-virtual {p0, v6, p3}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->onProgressRefresh(FZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 851
    :cond_3
    monitor-exit p0

    return-void

    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    .end local v6    # "scale":F
    :cond_4
    move v6, v7

    .line 811
    goto :goto_0

    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "level":I
    .restart local v5    # "progressDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v6    # "scale":F
    :cond_5
    move-object v5, v1

    .line 821
    goto :goto_1

    .line 836
    .end local v5    # "progressDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "dualDrawable":Landroid/graphics/drawable/Drawable;
    :cond_6
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    .line 838
    :cond_7
    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-lez v8, :cond_8

    iget v8, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    sub-int v8, p2, v8

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    iget v10, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    div-float v7, v8, v9

    .line 839
    .local v7, "scale2":F
    :cond_8
    mul-float v8, v7, v11

    float-to-int v4, v8

    .line 840
    .local v4, "level2":I
    if-eqz v2, :cond_9

    .end local v2    # "dualDrawable":Landroid/graphics/drawable/Drawable;
    :goto_4
    invoke-virtual {v2, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 811
    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    .end local v3    # "level":I
    .end local v4    # "level2":I
    .end local v6    # "scale":F
    .end local v7    # "scale2":F
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 840
    .restart local v1    # "d":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "dualDrawable":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "level":I
    .restart local v4    # "level2":I
    .restart local v6    # "scale":F
    .restart local v7    # "scale2":F
    :cond_9
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_4

    .line 845
    .end local v2    # "dualDrawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "level":I
    .end local v4    # "level2":I
    .end local v7    # "scale2":F
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->invalidate()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private makeDrawable(II)Landroid/graphics/drawable/Drawable;
    .locals 7
    .param p1, "color"    # I
    .param p2, "strokeColor"    # I

    .prologue
    const/4 v6, 0x1

    .line 393
    const v5, 0xffffff

    and-int/2addr p1, v5

    .line 394
    const/high16 v5, 0x32000000

    or-int v4, p1, v5

    .line 395
    .local v4, "startColor":I
    const/high16 v5, -0x6a000000

    or-int v3, p1, v5

    .line 396
    .local v3, "middleColor":I
    const/high16 v5, -0x1000000

    or-int v2, p1, v5

    .line 397
    .local v2, "endColor":I
    const/4 v5, 0x3

    new-array v0, v5, [I

    const/4 v5, 0x0

    aput v4, v0, v5

    aput v3, v0, v6

    const/4 v5, 0x2

    aput v2, v0, v5

    .line 398
    .local v0, "colors":[I
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v5, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v1, v5, v0}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 399
    .local v1, "d":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {v1, v6, p2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 400
    sget-object v5, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->CORNERS:[F

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 401
    return-object v1
.end method

.method private declared-synchronized refreshProgress(IIZ)V
    .locals 6
    .param p1, "id"    # I
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 792
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mUiThreadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 793
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->doRefreshProgress(IIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 808
    :goto_0
    monitor-exit p0

    return-void

    .line 796
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mRefreshProgressRunnable:Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;

    if-eqz v1, :cond_1

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mRefreshProgressRunnable:Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;

    .line 800
    .local v0, "r":Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mRefreshProgressRunnable:Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;

    .line 801
    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;->setup(IIZ)V

    .line 806
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 792
    .end local v0    # "r":Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 804
    :cond_1
    :try_start_2
    new-instance v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;-><init>(Lcom/sec/android/app/soundalive/widget/TwProgressBar;IIZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v0    # "r":Lcom/sec/android/app/soundalive/widget/TwProgressBar$RefreshProgressRunnable;
    goto :goto_1
.end method

.method private updateDualColorDrawableBounds(II)V
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 879
    iget-boolean v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 883
    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    if-le v2, v3, :cond_1

    .line 884
    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-lez v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 885
    .local v0, "scale":F
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    if-nez v2, :cond_0

    .line 886
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    .line 887
    :cond_0
    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressBarMode:I

    if-nez v2, :cond_3

    .line 888
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v1, v2

    .line 889
    .local v1, "startDual":I
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 896
    .end local v0    # "scale":F
    .end local v1    # "startDual":I
    :cond_1
    :goto_1
    return-void

    .line 884
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 891
    .restart local v0    # "scale":F
    :cond_3
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v0

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 892
    .restart local v1    # "startDual":I
    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    invoke-virtual {v2, v3, v4, v5, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_1
.end method


# virtual methods
.method public getBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method getCurrentDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method protected getDualColorProgress()I
    .locals 1

    .prologue
    .line 956
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    return v0
.end method

.method public getDualColorProgressDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method protected getDualColorProgressStart()I
    .locals 4

    .prologue
    .line 964
    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 965
    .local v0, "scale":F
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    if-nez v1, :cond_0

    .line 966
    iget-object v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    .line 967
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    iget-object v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    return v1

    .line 964
    .end local v0    # "scale":F
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIndicatorThickness()I
    .locals 1

    .prologue
    .line 369
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mIndicatorThickness:I

    return v0
.end method

.method public declared-synchronized getMax()I
    .locals 1

    .prologue
    .line 551
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getProgress()I
    .locals 1

    .prologue
    .line 525
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getProgressBarMode()I
    .locals 1

    .prologue
    .line 380
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressBarMode:I

    return v0
.end method

.method public getProgressDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method protected getProgressWidth()I
    .locals 1

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getWidth()I

    move-result v0

    return v0
.end method

.method public declared-synchronized getSecondaryProgress()I
    .locals 1

    .prologue
    .line 538
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryProgress:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getUseDualColorProgressBar()Z
    .locals 1

    .prologue
    .line 919
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    return v0
.end method

.method public final declared-synchronized incrementProgressBy(I)V
    .locals 1
    .param p1, "diff"    # I

    .prologue
    .line 498
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setProgress(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    monitor-exit p0

    return-void

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized incrementSecondaryProgressBy(I)V
    .locals 1
    .param p1, "diff"    # I

    .prologue
    .line 512
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryProgress:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setSecondaryProgress(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    monitor-exit p0

    return-void

    .line 512
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 661
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 663
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setProgressBounds()V

    .line 664
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 665
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 667
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawBackground:Z

    if-eqz v0, :cond_0

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 670
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawSecondaryProgress:Z

    if-eqz v0, :cond_1

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 673
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawFirstProgress:Z

    if-eqz v0, :cond_2

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 676
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->KK_AMERICANO:Z

    if-nez v0, :cond_3

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 679
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 680
    monitor-exit p0

    return-void

    .line 661
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 687
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 689
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    .line 690
    .local v2, "dw":I
    const/4 v1, 0x0

    .line 691
    .local v1, "dh":I
    if-eqz v0, :cond_0

    .line 692
    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinWidth:I

    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMaxWidth:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 693
    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    iget v4, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMaxHeight:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 696
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 697
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 699
    invoke-static {v2, p1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->resolveSize(II)I

    move-result v3

    invoke-static {v1, p2}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->resolveSize(II)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 700
    monitor-exit p0

    return-void

    .line 687
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    .end local v1    # "dh":I
    .end local v2    # "dw":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method onProgressRefresh(FZ)V
    .locals 0
    .param p1, "scale"    # F
    .param p2, "fromUser"    # Z

    .prologue
    .line 555
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 758
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;

    .line 760
    .local v0, "ss":Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;
    invoke-virtual {v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 762
    iget v1, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;->progress2:I

    if-lez v1, :cond_0

    iget v1, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;->progress2:I

    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-gt v1, v2, :cond_0

    .line 763
    iget v1, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;->progress2:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setSecondaryProgress(I)V

    .line 766
    :cond_0
    iget v1, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;->progress:I

    if-lez v1, :cond_1

    iget v1, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;->progress:I

    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-gt v1, v2, :cond_1

    .line 767
    iget v1, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;->progress:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setProgress(I)V

    .line 769
    :cond_1
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 752
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 753
    .local v0, "superState":Landroid/os/Parcelable;
    new-instance v1, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;

    iget v2, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    iget v3, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryProgress:I

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/android/app/soundalive/widget/TwProgressBar$SavedState;-><init>(Landroid/os/Parcelable;II)V

    return-object v1
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 325
    const v0, -0x8b8b8c

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->makeDrawable(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 326
    return-void
.end method

.method public setBackgroundDraw(Z)V
    .locals 0
    .param p1, "draw"    # Z

    .prologue
    .line 301
    iput-boolean p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawBackground:Z

    .line 302
    return-void
.end method

.method public setBackgroundDrawable(I)V
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 274
    return-void
.end method

.method public setDualColorProgress(I)Z
    .locals 2
    .param p1, "mDual"    # I

    .prologue
    const/4 v0, 0x0

    .line 940
    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-gt p1, v1, :cond_0

    sget-boolean v1, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->KK_AMERICANO:Z

    if-eqz v1, :cond_1

    .line 941
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    .line 948
    :goto_0
    return v0

    .line 944
    :cond_1
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    .line 947
    invoke-direct {p0, v0, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->updateDualColorDrawableBounds(II)V

    .line 948
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setDualColorProgressColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 355
    const/high16 v0, 0xff0000

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->makeDrawable(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 356
    return-void
.end method

.method public setDualColorProgressDrawable(I)V
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 903
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 904
    return-void
.end method

.method public setIndicatorThickness(I)V
    .locals 0
    .param p1, "thickness"    # I

    .prologue
    .line 362
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mIndicatorThickness:I

    .line 363
    return-void
.end method

.method public declared-synchronized setMax(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 416
    monitor-enter p0

    if-gez p1, :cond_0

    .line 417
    const/4 p1, 0x0

    .line 419
    :cond_0
    :try_start_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-eq p1, v0, :cond_1

    .line 420
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    .line 421
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->postInvalidate()V

    .line 423
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    if-le v0, p1, :cond_1

    .line 424
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    :cond_1
    monitor-exit p0

    return-void

    .line 416
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 440
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setProgress(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 441
    monitor-exit p0

    return-void

    .line 440
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized setProgress(IZ)V
    .locals 2
    .param p1, "progress"    # I
    .param p2, "fromUser"    # Z

    .prologue
    .line 776
    monitor-enter p0

    if-gez p1, :cond_0

    .line 777
    const/4 p1, 0x0

    .line 780
    :cond_0
    :try_start_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-le p1, v0, :cond_1

    .line 781
    iget p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    .line 784
    :cond_1
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    if-eq p1, v0, :cond_2

    .line 785
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    .line 786
    const v0, 0x102000d

    iget v1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    invoke-direct {p0, v0, v1, p2}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->refreshProgress(IIZ)V

    .line 788
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 789
    monitor-exit p0

    return-void

    .line 776
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setProgressBounds()V
    .locals 25

    .prologue
    .line 568
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 569
    .local v6, "mContainer":Landroid/graphics/Rect;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 571
    .local v7, "mOutRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    .line 572
    .local v8, "max":I
    if-lez v8, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    int-to-float v0, v8

    move/from16 v20, v0

    div-float v14, v19, v20

    .line 573
    .local v14, "scale":F
    :goto_0
    if-lez v8, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryProgress:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    int-to-float v0, v8

    move/from16 v20, v0

    div-float v15, v19, v20

    .line 574
    .local v15, "scale2":F
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingLeft()I

    move-result v10

    .line 575
    .local v10, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingRight()I

    move-result v11

    .line 576
    .local v11, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingTop()I

    move-result v12

    .line 577
    .local v12, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getPaddingBottom()I

    move-result v9

    .line 578
    .local v9, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getHeight()I

    move-result v5

    .line 580
    .local v5, "height":I
    const/16 v16, 0x0

    .line 581
    .local v16, "tempHeight":I
    const/16 v17, 0x0

    .line 583
    .local v17, "tempWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getProgressWidth()I

    move-result v19

    sub-int v19, v19, v10

    sub-int v18, v19, v11

    .line 584
    .local v18, "w":I
    sub-int v19, v5, v12

    sub-int v4, v19, v9

    .line 586
    .local v4, "h":I
    const/4 v13, 0x0

    .line 596
    .local v13, "progressSpacing":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressBarMode:I

    move/from16 v19, v0

    if-nez v19, :cond_6

    .line 597
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mIndicatorThickness:I

    move/from16 v19, v0

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    .line 598
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mIndicatorThickness:I

    move/from16 v16, v0

    .line 602
    :goto_2
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 603
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v6, Landroid/graphics/Rect;->top:I

    .line 604
    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    add-int v19, v19, v18

    move/from16 v0, v19

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 605
    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    add-int v19, v19, v4

    move/from16 v0, v19

    iput v0, v6, Landroid/graphics/Rect;->bottom:I

    .line 614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v19

    div-int/lit8 v13, v19, 0x2

    .line 616
    if-lez v13, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgress:I

    move/from16 v19, v0

    if-lez v19, :cond_4

    .line 617
    sub-int v18, v18, v13

    .line 622
    :goto_3
    const/16 v19, 0x10

    move/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v16

    invoke-static {v0, v1, v2, v6, v7}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 623
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMaxHeight:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_5

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v21, v0

    sub-int v21, v4, v21

    div-int/lit8 v21, v21, 0x2

    add-int v22, v18, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v23, v0

    sub-int v23, v4, v23

    div-int/lit8 v23, v23, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v24, v0

    add-int v23, v23, v24

    invoke-virtual/range {v19 .. v23}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v21, v0

    sub-int v21, v4, v21

    div-int/lit8 v21, v21, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v14

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    add-int v22, v22, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v23, v0

    sub-int v23, v4, v23

    div-int/lit8 v23, v23, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v24, v0

    add-int v23, v23, v24

    invoke-virtual/range {v19 .. v23}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 626
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v20, v0

    sub-int v20, v4, v20

    div-int/lit8 v20, v20, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v21, v21, v15

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    add-int v21, v21, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v22, v0

    sub-int v22, v4, v22

    div-int/lit8 v22, v22, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMinHeight:I

    move/from16 v23, v0

    add-int v22, v22, v23

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 649
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressRect:Landroid/graphics/Rect;

    .line 650
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 651
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->updateDualColorDrawableBounds(II)V

    .line 654
    :cond_0
    return-void

    .line 572
    .end local v4    # "h":I
    .end local v5    # "height":I
    .end local v9    # "paddingBottom":I
    .end local v10    # "paddingLeft":I
    .end local v11    # "paddingRight":I
    .end local v12    # "paddingTop":I
    .end local v13    # "progressSpacing":I
    .end local v14    # "scale":F
    .end local v15    # "scale2":F
    .end local v16    # "tempHeight":I
    .end local v17    # "tempWidth":I
    .end local v18    # "w":I
    :cond_1
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 573
    .restart local v14    # "scale":F
    :cond_2
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 600
    .restart local v4    # "h":I
    .restart local v5    # "height":I
    .restart local v9    # "paddingBottom":I
    .restart local v10    # "paddingLeft":I
    .restart local v11    # "paddingRight":I
    .restart local v12    # "paddingTop":I
    .restart local v13    # "progressSpacing":I
    .restart local v15    # "scale2":F
    .restart local v16    # "tempHeight":I
    .restart local v17    # "tempWidth":I
    .restart local v18    # "w":I
    :cond_3
    move/from16 v16, v4

    goto/16 :goto_2

    .line 619
    :cond_4
    const/4 v13, 0x0

    goto/16 :goto_3

    .line 628
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    add-int v22, v18, v13

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v23, v0

    invoke-virtual/range {v19 .. v23}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v22, v22, v14

    move/from16 v0, v22

    float-to-int v0, v0

    move/from16 v22, v0

    add-int v22, v22, v13

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v23, v0

    invoke-virtual/range {v19 .. v23}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v21, v21, v15

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    add-int v21, v21, v13

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/16 :goto_4

    .line 633
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mIndicatorThickness:I

    move/from16 v19, v0

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_7

    .line 634
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mIndicatorThickness:I

    move/from16 v17, v0

    .line 638
    :goto_5
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 639
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v6, Landroid/graphics/Rect;->top:I

    .line 640
    iget v0, v6, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    add-int v19, v19, v18

    move/from16 v0, v19

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 641
    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    add-int v19, v19, v4

    move/from16 v0, v19

    iput v0, v6, Landroid/graphics/Rect;->bottom:I

    .line 643
    const/16 v19, 0x1

    move/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1, v4, v6, v7}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 644
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    iget v0, v7, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    iget v0, v7, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 645
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    sub-float v21, v21, v14

    int-to-float v0, v4

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    iget v0, v7, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v19, v0

    iget v0, v7, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    sub-float v21, v21, v15

    int-to-float v0, v4

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    iget v0, v7, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/16 :goto_4

    .line 636
    :cond_7
    move/from16 v17, v18

    goto/16 :goto_5
.end method

.method public setProgressColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 336
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->makeDrawable(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 337
    return-void
.end method

.method public setProgressDraw(Z)V
    .locals 0
    .param p1, "draw"    # Z

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawFirstProgress:Z

    .line 309
    return-void
.end method

.method public setProgressDrawable(I)V
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 285
    return-void
.end method

.method public declared-synchronized setSecondaryProgress(I)V
    .locals 1
    .param p1, "secondaryProgress"    # I

    .prologue
    .line 474
    monitor-enter p0

    if-gez p1, :cond_0

    .line 475
    const/4 p1, 0x0

    .line 478
    :cond_0
    :try_start_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    if-le p1, v0, :cond_1

    .line 479
    iget p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mMax:I

    .line 482
    :cond_1
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryProgress:I

    if-eq p1, v0, :cond_2

    .line 483
    iput p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryProgress:I

    .line 484
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->invalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 486
    :cond_2
    monitor-exit p0

    return-void

    .line 474
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSecondaryProgressColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 347
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->makeDrawable(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    .line 348
    return-void
.end method

.method public setSecondaryProgressDraw(Z)V
    .locals 0
    .param p1, "draw"    # Z

    .prologue
    .line 315
    iput-boolean p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDrawSecondaryProgress:Z

    .line 316
    return-void
.end method

.method public setSecondaryProgressDrawable(I)V
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mSecondaryDrawable:Landroid/graphics/drawable/Drawable;

    .line 295
    return-void
.end method

.method public setUseDualColorProgressBar(Z)V
    .locals 1
    .param p1, "isDual"    # Z

    .prologue
    .line 927
    sget-boolean v0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->KK_AMERICANO:Z

    if-eqz v0, :cond_0

    .line 928
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    .line 932
    :goto_0
    iget v0, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgress:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->setDualColorProgress(I)Z

    .line 933
    return-void

    .line 930
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/app/soundalive/widget/TwProgressBar;->mDualColorProgressBar:Z

    goto :goto_0
.end method
