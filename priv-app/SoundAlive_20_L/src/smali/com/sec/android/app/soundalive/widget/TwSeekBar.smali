.class public Lcom/sec/android/app/soundalive/widget/TwSeekBar;
.super Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;
.source "TwSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;,
        Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;
    }
.end annotation


# instance fields
.field private mOnSeekBarChangeListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

.field private mOnSeekBarHoverListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 107
    const v0, 0x7f01001c

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 117
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 118
    return-void
.end method


# virtual methods
.method onHoverChanged(III)V
    .locals 2
    .param p1, "hoverLevel"    # I
    .param p2, "posX"    # I
    .param p3, "posY"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarHoverListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarHoverListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, p1, v1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;->onHoverChanged(Lcom/sec/android/app/soundalive/widget/TwSeekBar;IZ)V

    .line 195
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onHoverChanged(III)V

    .line 196
    return-void
.end method

.method onProgressRefresh(FZ)V
    .locals 2
    .param p1, "scale"    # F
    .param p2, "fromUser"    # Z

    .prologue
    .line 122
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onProgressRefresh(FZ)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarChangeListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarChangeListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

    invoke-virtual {p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->getProgress()I

    move-result v1

    invoke-interface {v0, p0, v1, p2}, Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;->onProgressChanged(Lcom/sec/android/app/soundalive/widget/TwSeekBar;IZ)V

    .line 127
    :cond_0
    return-void
.end method

.method onStartTrackingHover(III)V
    .locals 1
    .param p1, "hoverLevel"    # I
    .param p2, "posX"    # I
    .param p3, "posY"    # I

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarHoverListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarHoverListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;

    invoke-interface {v0, p0, p1}, Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;->onStartTrackingHover(Lcom/sec/android/app/soundalive/widget/TwSeekBar;I)V

    .line 179
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onStartTrackingHover(III)V

    .line 180
    return-void
.end method

.method onStartTrackingTouch()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarChangeListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarChangeListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

    invoke-interface {v0, p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;->onStartTrackingTouch(Lcom/sec/android/app/soundalive/widget/TwSeekBar;)V

    .line 146
    :cond_0
    return-void
.end method

.method onStopTrackingHover()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarHoverListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarHoverListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;

    invoke-interface {v0, p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;->onStopTrackingHover(Lcom/sec/android/app/soundalive/widget/TwSeekBar;)V

    .line 187
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/soundalive/widget/TwAbsSeekBar;->onStopTrackingHover()V

    .line 188
    return-void
.end method

.method onStopTrackingTouch()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarChangeListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarChangeListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

    invoke-interface {v0, p0}, Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;->onStopTrackingTouch(Lcom/sec/android/app/soundalive/widget/TwSeekBar;)V

    .line 153
    :cond_0
    return-void
.end method

.method public setOnSeekBarHoverListener(Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarHoverListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnSeekBarHoverListener;

    .line 171
    return-void
.end method

.method public setOnTwSeekBarChangeListener(Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/soundalive/widget/TwSeekBar;->mOnSeekBarChangeListener:Lcom/sec/android/app/soundalive/widget/TwSeekBar$OnTwSeekBarChangeListener;

    .line 139
    return-void
.end method
