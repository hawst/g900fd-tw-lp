.class public interface abstract Lcom/sec/android/sCloudSync/IDataSync/IDataSync;
.super Ljava/lang/Object;
.source "IDataSync.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/sCloudSync/IDataSync/IDataSync$Stub;
    }
.end annotation


# virtual methods
.method public abstract cancelSync()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract performSync(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;)Lcom/sec/android/sCloudSync/IDataSync/DataSyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
