.class public final Ljbw;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dZz:[Ljbw;


# instance fields
.field private aez:I

.field private afW:Ljava/lang/String;

.field private afh:Ljava/lang/String;

.field public aiX:Ljcn;

.field private alB:Ljava/lang/String;

.field private dVg:Ljava/lang/String;

.field public dZA:[Ljava/lang/String;

.field private dZB:Ljava/lang/String;

.field public dZC:[Ljava/lang/String;

.field private dZD:Ljava/lang/String;

.field private dZE:Ljava/lang/String;

.field private dZF:J

.field public dZG:[Ljdq;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54851
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 54852
    const/4 v0, 0x0

    iput v0, p0, Ljbw;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbw;->afh:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljbw;->dZA:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbw;->alB:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbw;->dZB:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljbw;->dZC:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbw;->afW:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbw;->dZD:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbw;->dZE:Ljava/lang/String;

    iput-object v2, p0, Ljbw;->aiX:Ljcn;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljbw;->dZF:J

    invoke-static {}, Ljdq;->bhK()[Ljdq;

    move-result-object v0

    iput-object v0, p0, Ljbw;->dZG:[Ljdq;

    const-string v0, ""

    iput-object v0, p0, Ljbw;->dVg:Ljava/lang/String;

    iput-object v2, p0, Ljbw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbw;->eCz:I

    .line 54853
    return-void
.end method

.method public static bfH()[Ljbw;
    .locals 2

    .prologue
    .line 54653
    sget-object v0, Ljbw;->dZz:[Ljbw;

    if-nez v0, :cond_1

    .line 54654
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 54656
    :try_start_0
    sget-object v0, Ljbw;->dZz:[Ljbw;

    if-nez v0, :cond_0

    .line 54657
    const/4 v0, 0x0

    new-array v0, v0, [Ljbw;

    sput-object v0, Ljbw;->dZz:[Ljbw;

    .line 54659
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54661
    :cond_1
    sget-object v0, Ljbw;->dZz:[Ljbw;

    return-object v0

    .line 54659
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54647
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbw;->afh:Ljava/lang/String;

    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbw;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbw;->dZA:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljbw;->dZA:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljbw;->dZA:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljbw;->dZA:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbw;->alB:Ljava/lang/String;

    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbw;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbw;->dZB:Ljava/lang/String;

    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbw;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbw;->dZC:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljbw;->dZC:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljbw;->dZC:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljbw;->dZC:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbw;->afW:Ljava/lang/String;

    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbw;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbw;->dZD:Ljava/lang/String;

    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbw;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbw;->dZE:Ljava/lang/String;

    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbw;->aez:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljbw;->aiX:Ljcn;

    if-nez v0, :cond_7

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljbw;->aiX:Ljcn;

    :cond_7
    iget-object v0, p0, Ljbw;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljbw;->dZF:J

    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljbw;->aez:I

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbw;->dZG:[Ljdq;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdq;

    if-eqz v0, :cond_8

    iget-object v3, p0, Ljbw;->dZG:[Ljdq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    new-instance v3, Ljdq;

    invoke-direct {v3}, Ljdq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Ljbw;->dZG:[Ljdq;

    array-length v0, v0

    goto :goto_5

    :cond_a
    new-instance v3, Ljdq;

    invoke-direct {v3}, Ljdq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbw;->dZG:[Ljdq;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbw;->dVg:Ljava/lang/String;

    iget v0, p0, Ljbw;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljbw;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54877
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 54878
    const/4 v0, 0x1

    iget-object v2, p0, Ljbw;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 54880
    :cond_0
    iget-object v0, p0, Ljbw;->dZA:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljbw;->dZA:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 54881
    :goto_0
    iget-object v2, p0, Ljbw;->dZA:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 54882
    iget-object v2, p0, Ljbw;->dZA:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 54883
    if-eqz v2, :cond_1

    .line 54884
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 54881
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54888
    :cond_2
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 54889
    const/4 v0, 0x3

    iget-object v2, p0, Ljbw;->alB:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 54891
    :cond_3
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 54892
    const/4 v0, 0x4

    iget-object v2, p0, Ljbw;->dZB:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 54894
    :cond_4
    iget-object v0, p0, Ljbw;->dZC:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ljbw;->dZC:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 54895
    :goto_1
    iget-object v2, p0, Ljbw;->dZC:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 54896
    iget-object v2, p0, Ljbw;->dZC:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 54897
    if-eqz v2, :cond_5

    .line 54898
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 54895
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 54902
    :cond_6
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_7

    .line 54903
    const/4 v0, 0x6

    iget-object v2, p0, Ljbw;->afW:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 54905
    :cond_7
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    .line 54906
    const/4 v0, 0x7

    iget-object v2, p0, Ljbw;->dZD:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 54908
    :cond_8
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_9

    .line 54909
    const/16 v0, 0x8

    iget-object v2, p0, Ljbw;->dZE:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 54911
    :cond_9
    iget-object v0, p0, Ljbw;->aiX:Ljcn;

    if-eqz v0, :cond_a

    .line 54912
    const/16 v0, 0x9

    iget-object v2, p0, Ljbw;->aiX:Ljcn;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 54914
    :cond_a
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_b

    .line 54915
    const/16 v0, 0xa

    iget-wide v2, p0, Ljbw;->dZF:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 54917
    :cond_b
    iget-object v0, p0, Ljbw;->dZG:[Ljdq;

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljbw;->dZG:[Ljdq;

    array-length v0, v0

    if-lez v0, :cond_d

    .line 54918
    :goto_2
    iget-object v0, p0, Ljbw;->dZG:[Ljdq;

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 54919
    iget-object v0, p0, Ljbw;->dZG:[Ljdq;

    aget-object v0, v0, v1

    .line 54920
    if-eqz v0, :cond_c

    .line 54921
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 54918
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 54925
    :cond_d
    iget v0, p0, Ljbw;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_e

    .line 54926
    const/16 v0, 0xc

    iget-object v1, p0, Ljbw;->dVg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 54928
    :cond_e
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 54929
    return-void
.end method

.method public final bdu()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54832
    iget-object v0, p0, Ljbw;->dVg:Ljava/lang/String;

    return-object v0
.end method

.method public final bfI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54694
    iget-object v0, p0, Ljbw;->alB:Ljava/lang/String;

    return-object v0
.end method

.method public final bfJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54716
    iget-object v0, p0, Ljbw;->dZB:Ljava/lang/String;

    return-object v0
.end method

.method public final bfK()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54763
    iget-object v0, p0, Ljbw;->dZD:Ljava/lang/String;

    return-object v0
.end method

.method public final bfL()Z
    .locals 1

    .prologue
    .line 54774
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfM()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54785
    iget-object v0, p0, Ljbw;->dZE:Ljava/lang/String;

    return-object v0
.end method

.method public final bfN()Z
    .locals 1

    .prologue
    .line 54796
    iget v0, p0, Ljbw;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54669
    iget-object v0, p0, Ljbw;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 54933
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 54934
    iget v1, p0, Ljbw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 54935
    const/4 v1, 0x1

    iget-object v3, p0, Ljbw;->afh:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54938
    :cond_0
    iget-object v1, p0, Ljbw;->dZA:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljbw;->dZA:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    move v4, v2

    .line 54941
    :goto_0
    iget-object v5, p0, Ljbw;->dZA:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 54942
    iget-object v5, p0, Ljbw;->dZA:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 54943
    if-eqz v5, :cond_1

    .line 54944
    add-int/lit8 v4, v4, 0x1

    .line 54945
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 54941
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54949
    :cond_2
    add-int/2addr v0, v3

    .line 54950
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 54952
    :cond_3
    iget v1, p0, Ljbw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 54953
    const/4 v1, 0x3

    iget-object v3, p0, Ljbw;->alB:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54956
    :cond_4
    iget v1, p0, Ljbw;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 54957
    const/4 v1, 0x4

    iget-object v3, p0, Ljbw;->dZB:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54960
    :cond_5
    iget-object v1, p0, Ljbw;->dZC:[Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Ljbw;->dZC:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    move v4, v2

    .line 54963
    :goto_1
    iget-object v5, p0, Ljbw;->dZC:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_7

    .line 54964
    iget-object v5, p0, Ljbw;->dZC:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 54965
    if-eqz v5, :cond_6

    .line 54966
    add-int/lit8 v4, v4, 0x1

    .line 54967
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 54963
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 54971
    :cond_7
    add-int/2addr v0, v3

    .line 54972
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 54974
    :cond_8
    iget v1, p0, Ljbw;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_9

    .line 54975
    const/4 v1, 0x6

    iget-object v3, p0, Ljbw;->afW:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54978
    :cond_9
    iget v1, p0, Ljbw;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 54979
    const/4 v1, 0x7

    iget-object v3, p0, Ljbw;->dZD:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54982
    :cond_a
    iget v1, p0, Ljbw;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_b

    .line 54983
    const/16 v1, 0x8

    iget-object v3, p0, Ljbw;->dZE:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54986
    :cond_b
    iget-object v1, p0, Ljbw;->aiX:Ljcn;

    if-eqz v1, :cond_c

    .line 54987
    const/16 v1, 0x9

    iget-object v3, p0, Ljbw;->aiX:Ljcn;

    invoke-static {v1, v3}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54990
    :cond_c
    iget v1, p0, Ljbw;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_d

    .line 54991
    const/16 v1, 0xa

    iget-wide v4, p0, Ljbw;->dZF:J

    invoke-static {v1, v4, v5}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 54994
    :cond_d
    iget-object v1, p0, Ljbw;->dZG:[Ljdq;

    if-eqz v1, :cond_f

    iget-object v1, p0, Ljbw;->dZG:[Ljdq;

    array-length v1, v1

    if-lez v1, :cond_f

    .line 54995
    :goto_2
    iget-object v1, p0, Ljbw;->dZG:[Ljdq;

    array-length v1, v1

    if-ge v2, v1, :cond_f

    .line 54996
    iget-object v1, p0, Ljbw;->dZG:[Ljdq;

    aget-object v1, v1, v2

    .line 54997
    if-eqz v1, :cond_e

    .line 54998
    const/16 v3, 0xb

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54995
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 55003
    :cond_f
    iget v1, p0, Ljbw;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_10

    .line 55004
    const/16 v1, 0xc

    iget-object v2, p0, Ljbw;->dVg:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55007
    :cond_10
    return v0
.end method

.method public final sF(Ljava/lang/String;)Ljbw;
    .locals 1

    .prologue
    .line 54672
    if-nez p1, :cond_0

    .line 54673
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54675
    :cond_0
    iput-object p1, p0, Ljbw;->afh:Ljava/lang/String;

    .line 54676
    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbw;->aez:I

    .line 54677
    return-object p0
.end method

.method public final sG(Ljava/lang/String;)Ljbw;
    .locals 1

    .prologue
    .line 54697
    if-nez p1, :cond_0

    .line 54698
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54700
    :cond_0
    iput-object p1, p0, Ljbw;->alB:Ljava/lang/String;

    .line 54701
    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbw;->aez:I

    .line 54702
    return-object p0
.end method

.method public final sH(Ljava/lang/String;)Ljbw;
    .locals 1

    .prologue
    .line 54719
    if-nez p1, :cond_0

    .line 54720
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54722
    :cond_0
    iput-object p1, p0, Ljbw;->dZB:Ljava/lang/String;

    .line 54723
    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbw;->aez:I

    .line 54724
    return-object p0
.end method

.method public final sI(Ljava/lang/String;)Ljbw;
    .locals 1

    .prologue
    .line 54766
    if-nez p1, :cond_0

    .line 54767
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54769
    :cond_0
    iput-object p1, p0, Ljbw;->dZD:Ljava/lang/String;

    .line 54770
    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbw;->aez:I

    .line 54771
    return-object p0
.end method

.method public final sJ(Ljava/lang/String;)Ljbw;
    .locals 1

    .prologue
    .line 54788
    if-nez p1, :cond_0

    .line 54789
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54791
    :cond_0
    iput-object p1, p0, Ljbw;->dZE:Ljava/lang/String;

    .line 54792
    iget v0, p0, Ljbw;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbw;->aez:I

    .line 54793
    return-object p0
.end method
