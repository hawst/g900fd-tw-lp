.class public abstract Lduc;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Ldua;


# static fields
.field private static final bLM:[I

.field private static final bLN:[I


# instance fields
.field private apJ:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bLO:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bLP:Ldub;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bLQ:Landroid/app/DialogFragment;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

.field private bLS:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-array v0, v3, [I

    const v1, 0x7f0100c7

    aput v1, v0, v2

    sput-object v0, Lduc;->bLM:[I

    .line 32
    new-array v0, v3, [I

    const v1, 0x7f0100c8

    aput v1, v0, v2

    sput-object v0, Lduc;->bLN:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lduc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lduc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    .line 44
    return-void
.end method

.method protected static varargs a(I[Landroid/view/View;)V
    .locals 3

    .prologue
    .line 148
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 149
    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    return-void
.end method

.method private ahh()V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 100
    iget-object v0, p0, Lduc;->apJ:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lduc;->bLO:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 133
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 100
    goto :goto_0

    .line 104
    :cond_2
    invoke-virtual {p0}, Lduc;->ahk()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lduc;->ahj()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 105
    :cond_3
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    .line 106
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {p0, v2}, Lduc;->c(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 109
    iget-object v2, p0, Lduc;->apJ:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    iget-object v2, p0, Lduc;->apJ:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    :goto_2
    iget-object v0, p0, Lduc;->bLO:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    invoke-virtual {p0}, Lduc;->ahm()[Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v0}, Lduc;->a(I[Landroid/view/View;)V

    goto :goto_1

    .line 114
    :cond_4
    iget-object v0, p0, Lduc;->apJ:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 118
    :cond_5
    invoke-virtual {p0}, Lduc;->ahk()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    .line 121
    iget-object v0, p0, Lduc;->apJ:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lduc;->bLO:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lduc;->bLO:Landroid/widget/TextView;

    iget-object v1, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual {p0}, Lduc;->ahm()[Landroid/view/View;

    move-result-object v0

    invoke-static {v3, v0}, Lduc;->a(I[Landroid/view/View;)V

    goto :goto_1

    .line 125
    :cond_6
    invoke-virtual {p0}, Lduc;->ahk()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lduc;->apJ:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lduc;->bLO:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lduc;->bLO:Landroid/widget/TextView;

    iget-object v1, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getLabel()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {p0}, Lduc;->ahm()[Landroid/view/View;

    move-result-object v0

    invoke-static {v3, v0}, Lduc;->a(I[Landroid/view/View;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Landroid/view/View;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    if-ne v0, p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method protected final a(Landroid/app/DialogFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lduc;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_0

    .line 237
    iput-object p1, p0, Lduc;->bLQ:Landroid/app/DialogFragment;

    .line 238
    invoke-virtual {p1, v0, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 240
    :cond_0
    return-void
.end method

.method public final a(Ldub;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lduc;->bLP:Ldub;

    .line 176
    return-void
.end method

.method public final ahf()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    return-object v0
.end method

.method protected final ahi()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lduc;->bLP:Ldub;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lduc;->bLP:Ldub;

    invoke-interface {v0}, Ldub;->ahg()V

    .line 182
    :cond_0
    return-void
.end method

.method protected ahj()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method public ahk()Z
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v0

    return v0
.end method

.method public final ahl()[Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 270
    invoke-virtual {p0}, Lduc;->ahm()[Landroid/view/View;

    move-result-object v0

    .line 271
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    .line 272
    array-length v2, v0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 273
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, Lduc;->bLO:Landroid/widget/TextView;

    aput-object v2, v1, v0

    .line 275
    return-object v1
.end method

.method protected abstract ahm()[Landroid/view/View;
.end method

.method public final b(ILjava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->getId()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 161
    :goto_0
    iget-object v0, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    iput-object p2, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    .line 163
    invoke-virtual {p0}, Lduc;->refreshDrawableState()V

    .line 164
    invoke-direct {p0}, Lduc;->ahh()V

    .line 166
    :cond_0
    return-void

    .line 159
    :cond_1
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public b(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 71
    invoke-direct {p0}, Lduc;->ahh()V

    .line 72
    return-void
.end method

.method protected c(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method protected drawableStateChanged()V
    .locals 3

    .prologue
    .line 209
    invoke-super {p0}, Landroid/widget/LinearLayout;->drawableStateChanged()V

    .line 212
    iget-object v0, p0, Lduc;->bLO:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    .line 214
    :goto_0
    iget-object v1, p0, Lduc;->bLO:Landroid/widget/TextView;

    iget-object v2, p0, Lduc;->bLO:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 216
    :cond_0
    return-void

    .line 213
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final getFragmentManager()Landroid/app/FragmentManager;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 244
    .line 245
    invoke-virtual {p0}, Lduc;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 246
    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 247
    check-cast v0, Landroid/app/Activity;

    .line 251
    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 254
    :goto_1
    return-object v0

    .line 248
    :cond_0
    instance-of v2, v0, Landroid/view/ContextThemeWrapper;

    if-eqz v2, :cond_2

    .line 249
    check-cast v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {v0}, Landroid/view/ContextThemeWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final getPrompt()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public onCreateDrawableState(I)[I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 186
    iget-object v0, p0, Lduc;->bLS:Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    move v0, v1

    .line 187
    :goto_0
    iget-object v3, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajG()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lduc;->ahk()Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    .line 189
    :cond_0
    if-eqz v0, :cond_5

    .line 190
    add-int/lit8 v1, p1, 0x1

    .line 192
    :goto_1
    if-eqz v2, :cond_1

    .line 193
    add-int/lit8 v1, v1, 0x1

    .line 196
    :cond_1
    invoke-super {p0, v1}, Landroid/widget/LinearLayout;->onCreateDrawableState(I)[I

    move-result-object v1

    .line 197
    if-eqz v0, :cond_2

    .line 198
    sget-object v0, Lduc;->bLM:[I

    invoke-static {v1, v0}, Lduc;->mergeDrawableStates([I[I)[I

    .line 200
    :cond_2
    if-eqz v2, :cond_3

    .line 201
    sget-object v0, Lduc;->bLN:[I

    invoke-static {v1, v0}, Lduc;->mergeDrawableStates([I[I)[I

    .line 204
    :cond_3
    return-object v1

    :cond_4
    move v0, v2

    .line 186
    goto :goto_0

    :cond_5
    move v1, p1

    goto :goto_1
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f11005c

    invoke-virtual {p0, v0}, Lduc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lduc;->apJ:Landroid/widget/TextView;

    .line 57
    const v0, 0x7f11005d

    invoke-virtual {p0, v0}, Lduc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lduc;->bLO:Landroid/widget/TextView;

    .line 58
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 2

    .prologue
    .line 260
    if-eqz p1, :cond_1

    iget-object v0, p0, Lduc;->bLQ:Landroid/app/DialogFragment;

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lduc;->bLQ:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 265
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lduc;->bLQ:Landroid/app/DialogFragment;

    .line 267
    :cond_1
    return-void
.end method

.method protected final tP()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lduc;->ahh()V

    .line 94
    return-void
.end method
