.class public final Lfxf;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 37
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 38
    iget-object v0, v0, Lizj;->dTS:Ljho;

    iget-object v0, v0, Ljho;->dPO:[Ljhn;

    aget-object v0, v0, v4

    .line 40
    invoke-virtual {p0, p1, v0}, Lfxf;->a(Landroid/content/Context;Ljhn;)Lanh;

    move-result-object v1

    .line 41
    invoke-virtual {p0, p2, v0}, Lfxf;->a(Lfmt;Ljhn;)Lanh;

    move-result-object v0

    .line 43
    new-instance v2, Lang;

    invoke-direct {v2}, Lang;-><init>()V

    .line 44
    const/4 v3, 0x2

    new-array v3, v3, [Lanh;

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    iput-object v3, v2, Lang;->ags:[Lanh;

    .line 45
    return-object v2
.end method

.method public final a(Landroid/content/Context;Ljhn;)Lanh;
    .locals 3

    .prologue
    .line 58
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 59
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lanh;->cm(I)Lanh;

    .line 60
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    .line 62
    new-instance v1, Lany;

    invoke-direct {v1}, Lany;-><init>()V

    iput-object v1, v0, Lanh;->agw:Lany;

    .line 63
    invoke-static {p2}, Lfyw;->a(Ljhn;)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    iget-object v2, v0, Lanh;->agw:Lany;

    invoke-virtual {v2, v1}, Lany;->bh(Ljava/lang/String;)Lany;

    .line 68
    :cond_0
    iget-object v1, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-static {p1, v1, p2}, Lfyw;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljhn;)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 71
    iget-object v2, v0, Lanh;->agw:Lany;

    invoke-virtual {v2, v1}, Lany;->bk(Ljava/lang/String;)Lany;

    .line 74
    :cond_1
    invoke-virtual {p2}, Ljhn;->pd()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 75
    iget-object v1, v0, Lanh;->agw:Lany;

    invoke-virtual {p2}, Ljhn;->pc()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lany;->bj(Ljava/lang/String;)Lany;

    .line 78
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lanh;->cn(I)Lanh;

    .line 79
    return-object v0
.end method

.method public final a(Lfmt;Ljhn;)Lanh;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 85
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 86
    invoke-virtual {v0, v5}, Lanh;->cm(I)Lanh;

    .line 87
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    .line 88
    new-instance v1, Laor;

    invoke-direct {v1}, Laor;-><init>()V

    .line 89
    iput-object v1, v0, Lanh;->agx:Laor;

    .line 90
    invoke-interface {p1}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBP()Landroid/location/Location;

    move-result-object v2

    invoke-static {v2}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v2

    iput-object v2, v1, Laor;->ajY:Ljbp;

    .line 93
    new-instance v2, Ljak;

    invoke-direct {v2}, Ljak;-><init>()V

    .line 94
    iget-object v3, p2, Ljhn;->aeB:Ljbp;

    iput-object v3, v2, Ljak;->aeB:Ljbp;

    .line 95
    new-instance v3, Ljal;

    invoke-direct {v3}, Ljal;-><init>()V

    .line 96
    iput-object v2, v3, Ljal;->dWL:Ljak;

    .line 97
    iput-object v3, v1, Laor;->ajZ:Ljal;

    .line 99
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Laor;->bf(Z)Laor;

    .line 100
    invoke-virtual {v1, v5}, Laor;->bg(Z)Laor;

    .line 102
    iget-object v1, p2, Ljhn;->aeB:Ljbp;

    invoke-static {v1, v4, v4, v5, v4}, Lgaz;->b(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    new-instance v2, Lfsa;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    invoke-virtual {v2, v1, v4}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 106
    return-object v0
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0, p1, p2}, Lfxf;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 53
    iget-object v0, v0, Lang;->ags:[Lanh;

    return-object v0
.end method
