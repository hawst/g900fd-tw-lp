.class public final Lkez;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eVo:I

.field private eVp:I

.field private eVq:I

.field private eVr:I

.field private eVs:I

.field private eVt:I

.field private eVu:D

.field private eVv:Lkew;

.field private eVw:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1084
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1085
    iput v0, p0, Lkez;->aez:I

    iput v0, p0, Lkez;->eVo:I

    iput v0, p0, Lkez;->eVp:I

    iput v0, p0, Lkez;->eVq:I

    iput v0, p0, Lkez;->eVr:I

    iput v0, p0, Lkez;->eVs:I

    iput v0, p0, Lkez;->eVt:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkez;->eVu:D

    iput-object v2, p0, Lkez;->eVv:Lkew;

    const-string v0, ""

    iput-object v0, p0, Lkez;->eVw:Ljava/lang/String;

    iput-object v2, p0, Lkez;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lkez;->eCz:I

    .line 1086
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 900
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lkez;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lkez;->eVo:I

    iget v0, p0, Lkez;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkez;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkez;->eVp:I

    iget v0, p0, Lkez;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkez;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkez;->eVq:I

    iget v0, p0, Lkez;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lkez;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkez;->eVr:I

    iget v0, p0, Lkez;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lkez;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkez;->eVs:I

    iget v0, p0, Lkez;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lkez;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkez;->eVt:I

    iget v0, p0, Lkez;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lkez;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lkez;->eVu:D

    iget v0, p0, Lkez;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lkez;->aez:I

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lkez;->eVv:Lkew;

    if-nez v0, :cond_1

    new-instance v0, Lkew;

    invoke-direct {v0}, Lkew;-><init>()V

    iput-object v0, p0, Lkez;->eVv:Lkew;

    :cond_1
    iget-object v0, p0, Lkez;->eVv:Lkew;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkez;->eVw:Ljava/lang/String;

    iget v0, p0, Lkez;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lkez;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x39 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1107
    iget v0, p0, Lkez;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1108
    const/4 v0, 0x1

    iget v1, p0, Lkez;->eVo:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1110
    :cond_0
    iget v0, p0, Lkez;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1111
    const/4 v0, 0x2

    iget v1, p0, Lkez;->eVp:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1113
    :cond_1
    iget v0, p0, Lkez;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1114
    const/4 v0, 0x3

    iget v1, p0, Lkez;->eVq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1116
    :cond_2
    iget v0, p0, Lkez;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1117
    const/4 v0, 0x4

    iget v1, p0, Lkez;->eVr:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1119
    :cond_3
    iget v0, p0, Lkez;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 1120
    const/4 v0, 0x5

    iget v1, p0, Lkez;->eVs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1122
    :cond_4
    iget v0, p0, Lkez;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 1123
    const/4 v0, 0x6

    iget v1, p0, Lkez;->eVt:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1125
    :cond_5
    iget v0, p0, Lkez;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 1126
    const/4 v0, 0x7

    iget-wide v2, p0, Lkez;->eVu:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 1128
    :cond_6
    iget-object v0, p0, Lkez;->eVv:Lkew;

    if-eqz v0, :cond_7

    .line 1129
    const/16 v0, 0x8

    iget-object v1, p0, Lkez;->eVv:Lkew;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1131
    :cond_7
    iget v0, p0, Lkez;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_8

    .line 1132
    const/16 v0, 0x9

    iget-object v1, p0, Lkez;->eVw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1134
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1135
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1139
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1140
    iget v1, p0, Lkez;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1141
    const/4 v1, 0x1

    iget v2, p0, Lkez;->eVo:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1144
    :cond_0
    iget v1, p0, Lkez;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1145
    const/4 v1, 0x2

    iget v2, p0, Lkez;->eVp:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1148
    :cond_1
    iget v1, p0, Lkez;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1149
    const/4 v1, 0x3

    iget v2, p0, Lkez;->eVq:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1152
    :cond_2
    iget v1, p0, Lkez;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1153
    const/4 v1, 0x4

    iget v2, p0, Lkez;->eVr:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1156
    :cond_3
    iget v1, p0, Lkez;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 1157
    const/4 v1, 0x5

    iget v2, p0, Lkez;->eVs:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1160
    :cond_4
    iget v1, p0, Lkez;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 1161
    const/4 v1, 0x6

    iget v2, p0, Lkez;->eVt:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1164
    :cond_5
    iget v1, p0, Lkez;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 1165
    const/4 v1, 0x7

    iget-wide v2, p0, Lkez;->eVu:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1168
    :cond_6
    iget-object v1, p0, Lkez;->eVv:Lkew;

    if-eqz v1, :cond_7

    .line 1169
    const/16 v1, 0x8

    iget-object v2, p0, Lkez;->eVv:Lkew;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1172
    :cond_7
    iget v1, p0, Lkez;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_8

    .line 1173
    const/16 v1, 0x9

    iget-object v2, p0, Lkez;->eVw:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1176
    :cond_8
    return v0
.end method
