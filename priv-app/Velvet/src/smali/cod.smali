.class public final Lcod;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/shared/search/SearchBoxStats;)Lith;
    .locals 9

    .prologue
    const/16 v0, 0xb

    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 27
    new-instance v2, Lith;

    invoke-direct {v2}, Lith;-><init>()V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arG()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lith;->pH(Ljava/lang/String;)Lith;

    .line 29
    new-instance v3, Ljsw;

    invoke-direct {v3}, Ljsw;-><init>()V

    iput-object v3, v2, Lith;->dHD:Ljsw;

    .line 30
    iget-object v3, v2, Lith;->dHD:Ljsw;

    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arG()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Ljsw;->sm(I)Ljsw;

    .line 31
    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arB()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 32
    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arF()I

    move-result v3

    if-eq v3, v5, :cond_1

    .line 35
    iget-object v3, v2, Lith;->dHD:Ljsw;

    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arF()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljsw;->sn(I)Ljsw;

    .line 45
    :goto_0
    iget-object v3, v2, Lith;->dHD:Ljsw;

    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arw()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljsw;->dL(J)Ljsw;

    .line 46
    iget-object v3, v2, Lith;->dHD:Ljsw;

    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arn()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    const-string v4, "OmniboxEventProtoPopulator"

    const-string v5, "Unexpected single searchbox context: %d"

    new-array v6, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arn()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x5

    invoke-static {v7, v4, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :goto_1
    :pswitch_0
    invoke-virtual {v3, v0}, Ljsw;->so(I)Ljsw;

    .line 54
    new-instance v0, Ljsy;

    invoke-direct {v0}, Ljsy;-><init>()V

    .line 55
    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Ljsy;->sp(I)Ljsy;

    .line 58
    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Ljsy;->sq(I)Ljsy;

    .line 59
    iget-object v3, v2, Lith;->dHD:Ljsw;

    new-array v1, v1, [Ljsy;

    aput-object v0, v1, v8

    iput-object v1, v3, Ljsw;->eCW:[Ljsy;

    .line 62
    invoke-virtual {p0}, Lcom/google/android/shared/search/SearchBoxStats;->arE()Lcom/google/android/shared/search/SuggestionLogInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/SuggestionLogInfo;->asK()[B

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 67
    :try_start_0
    invoke-static {v2, v0}, Lith;->c(Ljsr;[B)Ljsr;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_0
    :goto_2
    return-object v2

    .line 39
    :cond_1
    iget-object v3, v2, Lith;->dHD:Ljsw;

    invoke-virtual {v3, v5}, Ljsw;->sn(I)Ljsw;

    goto :goto_0

    .line 43
    :cond_2
    iget-object v3, v2, Lith;->dHD:Ljsw;

    invoke-virtual {v3, v8}, Ljsw;->sn(I)Ljsw;

    goto :goto_0

    .line 46
    :pswitch_1
    const/16 v0, 0xa

    goto :goto_1

    :pswitch_2
    const/16 v0, 0xc

    goto :goto_1

    :pswitch_3
    move v0, v1

    goto :goto_1

    .line 70
    :catch_0
    move-exception v1

    const-string v1, "OmniboxEventProtoPopulator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to parse summon encoding:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
