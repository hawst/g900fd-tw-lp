.class final Laeg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic Un:Ladz;


# direct methods
.method constructor <init>(Ladz;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Laeg;->Un:Ladz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Laeg;->Un:Ladz;

    invoke-static {v0}, Ladz;->e(Ladz;)Landroid/view/ActionMode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 429
    invoke-virtual {p1}, Landroid/view/View;->isLongClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Laeg;->Un:Ladz;

    invoke-static {v0}, Ladz;->f(Ladz;)Landroid/view/View$OnLongClickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    .line 440
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    iget-object v0, p0, Laeg;->Un:Ladz;

    iget-object v0, v0, Ladz;->Tz:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 435
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeu;

    .line 436
    invoke-virtual {v0}, Laeu;->isSelectable()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 437
    iget-object v1, p0, Laeg;->Un:Ladz;

    invoke-static {v1, p1}, Ladz;->a(Ladz;Landroid/view/View;)V

    .line 439
    :cond_2
    iget-object v1, p0, Laeg;->Un:Ladz;

    invoke-virtual {v0, v1}, Laeu;->a(Ladz;)V

    goto :goto_0
.end method
