.class public final Lgnn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cQp:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lgnn;->cQp:[C

    return-void
.end method

.method public static M(Ljava/lang/CharSequence;)[B
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    new-array v3, v0, [B

    .line 29
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v3

    .line 46
    :goto_0
    return-object v0

    .line 32
    :cond_0
    aput-byte v1, v3, v1

    .line 33
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    move v2, v0

    move v0, v1

    .line 34
    :goto_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ge v0, v4, :cond_7

    .line 35
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 36
    const/16 v4, 0x30

    if-lt v5, v4, :cond_1

    const/16 v4, 0x39

    if-le v5, v4, :cond_3

    :cond_1
    const/16 v4, 0x61

    if-lt v5, v4, :cond_2

    const/16 v4, 0x66

    if-le v5, v4, :cond_3

    :cond_2
    const/16 v4, 0x41

    if-lt v5, v4, :cond_4

    const/16 v4, 0x46

    if-gt v5, v4, :cond_4

    :cond_3
    const/4 v4, 0x1

    :goto_2
    if-nez v4, :cond_5

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "string contains non-hex chars"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v4, v1

    .line 36
    goto :goto_2

    .line 39
    :cond_5
    rem-int/lit8 v4, v2, 0x2

    if-nez v4, :cond_6

    .line 40
    shr-int/lit8 v4, v2, 0x1

    invoke-static {v5}, Lgnn;->d(C)I

    move-result v5

    shl-int/lit8 v5, v5, 0x4

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 44
    :goto_3
    add-int/lit8 v2, v2, 0x1

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_6
    shr-int/lit8 v4, v2, 0x1

    aget-byte v6, v3, v4

    invoke-static {v5}, Lgnn;->d(C)I

    move-result v5

    int-to-byte v5, v5

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    goto :goto_3

    :cond_7
    move-object v0, v3

    .line 46
    goto :goto_0
.end method

.method public static T([B)Ljava/lang/String;
    .locals 5

    .prologue
    .line 71
    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    new-array v1, v0, [C

    .line 72
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 73
    aget-byte v2, p0, v0

    .line 74
    sget-object v3, Lgnn;->cQp:[C

    shr-int/lit8 v4, v2, 0x4

    and-int/lit8 v4, v4, 0xf

    aget-char v3, v3, v4

    .line 75
    sget-object v4, Lgnn;->cQp:[C

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v4, v2

    .line 76
    mul-int/lit8 v4, v0, 0x2

    aput-char v3, v1, v4

    .line 77
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    aput-char v2, v1, v3

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method private static d(C)I
    .locals 1

    .prologue
    .line 56
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 57
    add-int/lit8 v0, p0, -0x30

    .line 61
    :goto_0
    return v0

    .line 58
    :cond_0
    const/16 v0, 0x61

    if-lt p0, v0, :cond_1

    const/16 v0, 0x66

    if-gt p0, v0, :cond_1

    .line 59
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 61
    :cond_1
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0
.end method
