.class final Lh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field private synthetic A:Lf;

.field private synthetic r:Landroid/support/v4/app/Fragment;

.field private synthetic s:Landroid/view/View;

.field private synthetic t:Ljava/lang/Object;

.field private synthetic u:Ljava/util/ArrayList;

.field private synthetic v:Lk;

.field private synthetic w:Z

.field private synthetic z:Landroid/support/v4/app/Fragment;


# direct methods
.method constructor <init>(Lf;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Lk;ZLandroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 1234
    iput-object p1, p0, Lh;->A:Lf;

    iput-object p2, p0, Lh;->s:Landroid/view/View;

    iput-object p3, p0, Lh;->t:Ljava/lang/Object;

    iput-object p4, p0, Lh;->u:Ljava/util/ArrayList;

    iput-object p5, p0, Lh;->v:Lk;

    iput-boolean p6, p0, Lh;->w:Z

    iput-object p7, p0, Lh;->r:Landroid/support/v4/app/Fragment;

    iput-object p8, p0, Lh;->z:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 6

    .prologue
    .line 1237
    iget-object v0, p0, Lh;->s:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1239
    iget-object v0, p0, Lh;->t:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1240
    iget-object v0, p0, Lh;->t:Ljava/lang/Object;

    iget-object v1, p0, Lh;->u:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Laf;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 1242
    iget-object v0, p0, Lh;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1244
    iget-object v0, p0, Lh;->A:Lf;

    iget-object v1, p0, Lh;->v:Lk;

    iget-boolean v2, p0, Lh;->w:Z

    iget-object v3, p0, Lh;->r:Landroid/support/v4/app/Fragment;

    invoke-static {v0, v1, v2, v3}, Lf;->a(Lf;Lk;ZLandroid/support/v4/app/Fragment;)Ldz;

    move-result-object v5

    .line 1246
    invoke-virtual {v5}, Ldz;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1247
    iget-object v0, p0, Lh;->u:Ljava/util/ArrayList;

    iget-object v1, p0, Lh;->v:Lk;

    iget-object v1, v1, Lk;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1251
    :goto_0
    iget-object v0, p0, Lh;->t:Ljava/lang/Object;

    iget-object v1, p0, Lh;->u:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Laf;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 1254
    iget-object v0, p0, Lh;->A:Lf;

    iget-object v1, p0, Lh;->v:Lk;

    invoke-static {v0, v5, v1}, Lf;->a(Lf;Ldz;Lk;)V

    .line 1256
    iget-object v0, p0, Lh;->A:Lf;

    iget-object v1, p0, Lh;->v:Lk;

    iget-object v2, p0, Lh;->r:Landroid/support/v4/app/Fragment;

    iget-object v3, p0, Lh;->z:Landroid/support/v4/app/Fragment;

    iget-boolean v4, p0, Lh;->w:Z

    invoke-static/range {v0 .. v5}, Lf;->a(Lf;Lk;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/Fragment;ZLdz;)V

    .line 1260
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 1249
    :cond_1
    iget-object v0, p0, Lh;->u:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ldz;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
