.class public final Liza;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dRu:[Liza;


# instance fields
.field private aez:I

.field private dRv:I

.field private dRw:Ljava/lang/String;

.field private dRx:Lizb;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31777
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 31778
    iput v0, p0, Liza;->aez:I

    iput v0, p0, Liza;->dRv:I

    const-string v0, ""

    iput-object v0, p0, Liza;->dRw:Ljava/lang/String;

    iput-object v1, p0, Liza;->dRx:Lizb;

    iput-object v1, p0, Liza;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liza;->eCz:I

    .line 31779
    return-void
.end method

.method public static bcD()[Liza;
    .locals 2

    .prologue
    .line 31720
    sget-object v0, Liza;->dRu:[Liza;

    if-nez v0, :cond_1

    .line 31721
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 31723
    :try_start_0
    sget-object v0, Liza;->dRu:[Liza;

    if-nez v0, :cond_0

    .line 31724
    const/4 v0, 0x0

    new-array v0, v0, [Liza;

    sput-object v0, Liza;->dRu:[Liza;

    .line 31726
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31728
    :cond_1
    sget-object v0, Liza;->dRu:[Liza;

    return-object v0

    .line 31726
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 31499
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liza;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Liza;->dRv:I

    iget v0, p0, Liza;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liza;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liza;->dRw:Ljava/lang/String;

    iget v0, p0, Liza;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liza;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Liza;->dRx:Lizb;

    if-nez v0, :cond_1

    new-instance v0, Lizb;

    invoke-direct {v0}, Lizb;-><init>()V

    iput-object v0, p0, Liza;->dRx:Lizb;

    :cond_1
    iget-object v0, p0, Liza;->dRx:Lizb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 31794
    iget v0, p0, Liza;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 31795
    const/4 v0, 0x1

    iget v1, p0, Liza;->dRv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 31797
    :cond_0
    iget v0, p0, Liza;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 31798
    const/4 v0, 0x2

    iget-object v1, p0, Liza;->dRw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 31800
    :cond_1
    iget-object v0, p0, Liza;->dRx:Lizb;

    if-eqz v0, :cond_2

    .line 31801
    const/4 v0, 0x3

    iget-object v1, p0, Liza;->dRx:Lizb;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 31803
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 31804
    return-void
.end method

.method public final bcE()I
    .locals 1

    .prologue
    .line 31736
    iget v0, p0, Liza;->dRv:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 31808
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 31809
    iget v1, p0, Liza;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 31810
    const/4 v1, 0x1

    iget v2, p0, Liza;->dRv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 31813
    :cond_0
    iget v1, p0, Liza;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 31814
    const/4 v1, 0x2

    iget-object v2, p0, Liza;->dRw:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31817
    :cond_1
    iget-object v1, p0, Liza;->dRx:Lizb;

    if-eqz v1, :cond_2

    .line 31818
    const/4 v1, 0x3

    iget-object v2, p0, Liza;->dRx:Lizb;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31821
    :cond_2
    return v0
.end method

.method public final nS(I)Liza;
    .locals 1

    .prologue
    .line 31739
    iput p1, p0, Liza;->dRv:I

    .line 31740
    iget v0, p0, Liza;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liza;->aez:I

    .line 31741
    return-object p0
.end method

.method public final rz(Ljava/lang/String;)Liza;
    .locals 1

    .prologue
    .line 31758
    if-nez p1, :cond_0

    .line 31759
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 31761
    :cond_0
    iput-object p1, p0, Liza;->dRw:Ljava/lang/String;

    .line 31762
    iget v0, p0, Liza;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liza;->aez:I

    .line 31763
    return-object p0
.end method

.method public final xZ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31755
    iget-object v0, p0, Liza;->dRw:Ljava/lang/String;

    return-object v0
.end method
