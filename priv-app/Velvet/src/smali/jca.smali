.class public final Ljca;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public dZR:Lixx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64865
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 64866
    const/4 v0, 0x0

    iput v0, p0, Ljca;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljca;->afh:Ljava/lang/String;

    iput-object v1, p0, Ljca;->dZR:Lixx;

    iput-object v1, p0, Ljca;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljca;->eCz:I

    .line 64867
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 64821
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljca;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljca;->afh:Ljava/lang/String;

    iget v0, p0, Ljca;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljca;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljca;->dZR:Lixx;

    if-nez v0, :cond_1

    new-instance v0, Lixx;

    invoke-direct {v0}, Lixx;-><init>()V

    iput-object v0, p0, Ljca;->dZR:Lixx;

    :cond_1
    iget-object v0, p0, Ljca;->dZR:Lixx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 64881
    iget v0, p0, Ljca;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 64882
    const/4 v0, 0x1

    iget-object v1, p0, Ljca;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 64884
    :cond_0
    iget-object v0, p0, Ljca;->dZR:Lixx;

    if-eqz v0, :cond_1

    .line 64885
    const/4 v0, 0x2

    iget-object v1, p0, Ljca;->dZR:Lixx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 64887
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 64888
    return-void
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64843
    iget-object v0, p0, Ljca;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 64892
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 64893
    iget v1, p0, Ljca;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 64894
    const/4 v1, 0x1

    iget-object v2, p0, Ljca;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64897
    :cond_0
    iget-object v1, p0, Ljca;->dZR:Lixx;

    if-eqz v1, :cond_1

    .line 64898
    const/4 v1, 0x2

    iget-object v2, p0, Ljca;->dZR:Lixx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64901
    :cond_1
    return v0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 64854
    iget v0, p0, Ljca;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final sN(Ljava/lang/String;)Ljca;
    .locals 1

    .prologue
    .line 64846
    if-nez p1, :cond_0

    .line 64847
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 64849
    :cond_0
    iput-object p1, p0, Ljca;->afh:Ljava/lang/String;

    .line 64850
    iget v0, p0, Ljca;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljca;->aez:I

    .line 64851
    return-object p0
.end method
