.class public final Lczm;
.super Landroid/app/Fragment;
.source "PG"

# interfaces
.implements Livf;


# instance fields
.field private aMC:Ljava/util/concurrent/ScheduledExecutorService;

.field private aMD:Leqo;

.field boc:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bof:Lepl;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bog:Z

.field boh:[Ljfs;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field boi:Z

.field private mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method private AO()V
    .locals 3

    .prologue
    .line 333
    new-instance v0, Lczl;

    iget-object v1, p0, Lczm;->mNetworkClient:Lfcx;

    iget-object v2, p0, Lczm;->boc:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lczl;-><init>(Lfcx;Ljava/lang/String;)V

    invoke-static {v0}, Lepl;->a(Lerh;)Lepl;

    move-result-object v0

    iput-object v0, p0, Lczm;->bof:Lepl;

    .line 335
    iget-object v0, p0, Lczm;->bof:Lepl;

    iget-object v1, p0, Lczm;->aMD:Leqo;

    invoke-static {v0, p0, v1}, Livg;->a(Livq;Livf;Ljava/util/concurrent/Executor;)V

    .line 336
    iget-object v0, p0, Lczm;->aMC:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lczm;->bof:Lepl;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 337
    return-void
.end method

.method private stopLoading()V
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lczm;->bof:Lepl;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lczm;->bof:Lepl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lepl;->cancel(Z)Z

    .line 342
    const/4 v0, 0x0

    iput-object v0, p0, Lczm;->bof:Lepl;

    .line 344
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic az(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 271
    check-cast p1, [Ljfs;

    iput-object p1, p0, Lczm;->boh:[Ljfs;

    invoke-virtual {p0}, Lczm;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lczj;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lczj;->a([Ljfs;)V

    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 357
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 358
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczm;->boi:Z

    .line 359
    invoke-virtual {p0}, Lczm;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lczj;

    .line 360
    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {v0}, Lczj;->Uk()V

    .line 364
    :cond_0
    return-void
.end method

.method final ji(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 309
    iput-object p1, p0, Lczm;->boc:Ljava/lang/String;

    .line 310
    const/4 v0, 0x0

    iput-object v0, p0, Lczm;->boh:[Ljfs;

    .line 311
    const/4 v0, 0x0

    iput-boolean v0, p0, Lczm;->boi:Z

    .line 312
    invoke-direct {p0}, Lczm;->stopLoading()V

    .line 313
    invoke-virtual {p0}, Lczm;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-direct {p0}, Lczm;->AO()V

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lczm;->bog:Z

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 288
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 289
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lczm;->setRetainInstance(Z)V

    .line 290
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    iget-object v0, v0, Lgql;->mAsyncServices:Lema;

    .line 291
    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iput-object v1, p0, Lczm;->aMC:Ljava/util/concurrent/ScheduledExecutorService;

    .line 292
    invoke-virtual {v0}, Lema;->aus()Leqo;

    move-result-object v0

    iput-object v0, p0, Lczm;->aMD:Leqo;

    .line 293
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axI()Lfcx;

    move-result-object v0

    iput-object v0, p0, Lczm;->mNetworkClient:Lfcx;

    .line 295
    iget-boolean v0, p0, Lczm;->bog:Z

    if-eqz v0, :cond_0

    .line 296
    const/4 v0, 0x0

    iput-boolean v0, p0, Lczm;->bog:Z

    .line 297
    invoke-direct {p0}, Lczm;->AO()V

    .line 299
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 0

    .prologue
    .line 303
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 305
    invoke-direct {p0}, Lczm;->stopLoading()V

    .line 306
    return-void
.end method
