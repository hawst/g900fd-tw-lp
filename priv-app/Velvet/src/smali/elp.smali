.class public Lelp;
.super Lenh;
.source "PG"

# interfaces
.implements Leoj;


# instance fields
.field public final Yx:Ljava/util/Map;

.field private final ceN:Ljava/util/concurrent/atomic/AtomicInteger;

.field private ceO:Lelr;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lenh;-><init>(Landroid/content/Context;)V

    .line 50
    iput-object p1, p0, Lelp;->mActivity:Landroid/app/Activity;

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lelp;->ceN:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 52
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lelp;->Yx:Ljava/util/Map;

    .line 53
    return-void
.end method


# virtual methods
.method public final CR()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lelp;->Yx:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leol;

    .line 106
    if-nez v0, :cond_1

    .line 107
    const-string v0, "ActivityIntentStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got result callback with request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with no callback in this object, could belong to someone else"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-object v1, p0, Lelp;->mActivity:Landroid/app/Activity;

    invoke-interface {v0, p2, p3, v1}, Leol;->a(ILandroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    new-instance v1, Lelr;

    invoke-direct {v1, p1, p2, p3}, Lelr;-><init>(IILandroid/content/Intent;)V

    iput-object v1, p0, Lelp;->ceO:Lelr;

    .line 115
    iget-object v1, p0, Lelp;->Yx:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;Leol;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 72
    :try_start_0
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v2, p0, Lelp;->ceN:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v2

    .line 76
    iget-object v3, p0, Lelp;->Yx:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-virtual {p0, p1}, Lelp;->y(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v3

    .line 78
    iget-object v4, p0, Lelp;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4, p1, v2, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 87
    :goto_0
    return v0

    .line 81
    :catch_0
    move-exception v2

    const-string v2, "ActivityIntentStarter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No activity found for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 82
    new-array v0, v0, [Landroid/content/Intent;

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lelp;->e([Landroid/content/Intent;)V

    move v0, v1

    .line 83
    goto :goto_0

    .line 84
    :catch_1
    move-exception v2

    .line 85
    invoke-virtual {p0, p1, v2}, Lelp;->a(Landroid/content/Intent;Ljava/lang/SecurityException;)V

    .line 86
    new-array v0, v0, [Landroid/content/Intent;

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lelp;->e([Landroid/content/Intent;)V

    move v0, v1

    .line 87
    goto :goto_0
.end method

.method public final aun()V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lelp;->ceO:Lelr;

    .line 121
    const/4 v1, 0x0

    iput-object v1, p0, Lelp;->ceO:Lelr;

    .line 123
    if-eqz v0, :cond_0

    .line 124
    iget v1, v0, Lelr;->LS:I

    iget v2, v0, Lelr;->ceP:I

    iget-object v0, v0, Lelr;->Jp:Landroid/content/Intent;

    invoke-virtual {p0, v1, v2, v0}, Lelp;->a(IILandroid/content/Intent;)V

    .line 129
    :cond_0
    return-void
.end method

.method public varargs b([Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    array-length v0, p1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    aget-object v0, p1, v2

    invoke-virtual {p0, v0}, Lelp;->z(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    aget-object v0, p1, v2

    new-instance v1, Lelq;

    invoke-direct {v1, p0}, Lelq;-><init>(Lelp;)V

    invoke-virtual {p0, v0, v1}, Lelp;->a(Landroid/content/Intent;Leol;)Z

    move-result v0

    .line 66
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lenh;->b([Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 136
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 138
    iget-object v0, p0, Lelp;->Yx:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 139
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leol;

    .line 140
    instance-of v4, v1, Landroid/os/Parcelable;

    if-eqz v4, :cond_0

    .line 141
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 146
    :cond_1
    invoke-virtual {v2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 147
    const-string v0, "velvet:activity_intent_starter:callbacks"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 149
    :cond_2
    return-void
.end method

.method public final x(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 152
    if-eqz p1, :cond_0

    const-string v0, "velvet:activity_intent_starter:callbacks"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    const-string v0, "velvet:activity_intent_starter:callbacks"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 154
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 155
    iget-object v3, p0, Lelp;->Yx:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Leol;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 159
    :cond_0
    return-void
.end method
