.class public final Lhqx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgco;
.implements Lgcz;


# instance fields
.field private cFS:Lglx;

.field private final cFT:Lgda;

.field private final cGq:Lgky;

.field private final doE:Lhqw;

.field private final doF:Ljava/util/List;

.field private final doG:Lgdi;

.field private doH:Z

.field private final mAudioCallback:Lgcd;


# direct methods
.method public constructor <init>(Lgcd;Lgky;Lgdi;Lglx;Lgda;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhqx;->doF:Ljava/util/List;

    .line 61
    iput-object p1, p0, Lhqx;->mAudioCallback:Lgcd;

    .line 62
    iput-object p2, p0, Lhqx;->cGq:Lgky;

    .line 63
    iput-object p4, p0, Lhqx;->cFS:Lglx;

    .line 64
    iput-object p5, p0, Lhqx;->cFT:Lgda;

    .line 65
    new-instance v0, Lhqw;

    invoke-direct {v0}, Lhqw;-><init>()V

    iput-object v0, p0, Lhqx;->doE:Lhqw;

    .line 66
    iput-object p3, p0, Lhqx;->doG:Lgdi;

    .line 67
    return-void
.end method

.method private e(Ljww;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 118
    invoke-virtual {p1}, Ljww;->getStatus()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 119
    const/16 v0, 0xa

    invoke-static {v0}, Lege;->ht(I)V

    .line 121
    iget-object v0, p0, Lhqx;->mAudioCallback:Lgcd;

    invoke-interface {v0}, Lgcd;->aEJ()V

    .line 143
    :cond_0
    :goto_0
    iget-object v0, p0, Lhqx;->doG:Lgdi;

    iget-object v1, p0, Lhqx;->cFS:Lglx;

    invoke-interface {v0, p1, v1}, Lgdi;->a(Ljww;Lglx;)V

    .line 144
    return-void

    .line 122
    :cond_1
    invoke-virtual {p1}, Ljww;->getStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "S3_STATUS_DONE_ERROR should be propagated as a ServerRecognizeException"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_2
    sget-object v0, Ljxg;->eKr:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxg;

    .line 128
    invoke-virtual {p1}, Ljww;->getStatus()I

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v1, v0, Ljxg;->eKs:Ljvv;

    if-eqz v1, :cond_3

    iget-object v0, v0, Ljxg;->eKs:Ljvv;

    invoke-virtual {v0}, Ljvv;->getEventType()I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 133
    const/16 v0, 0xb

    invoke-static {v0}, Lege;->ht(I)V

    .line 135
    iget-object v0, p0, Lhqx;->mAudioCallback:Lgcd;

    invoke-interface {v0}, Lgcd;->aEI()V

    goto :goto_0

    .line 136
    :cond_3
    invoke-virtual {p1}, Ljww;->getStatus()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljxb;->eKa:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lhqx;->cGq:Lgky;

    const/16 v1, 0xbb8

    invoke-interface {v0, v1}, Lgky;->kq(I)V

    goto :goto_0
.end method

.method private lq(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 91
    iget-boolean v0, p0, Lhqx;->doH:Z

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iput-boolean v1, p0, Lhqx;->doH:Z

    .line 96
    if-ne p1, v1, :cond_2

    .line 97
    const/16 v0, 0x46

    invoke-static {v0}, Lege;->ht(I)V

    goto :goto_0

    .line 99
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 100
    const/16 v0, 0x47

    invoke-static {v0}, Lege;->ht(I)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lehu;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lehv;

    invoke-virtual {p0, p1}, Lhqx;->a(Lehv;)V

    return-void
.end method

.method public final a(Lehv;)V
    .locals 3

    .prologue
    .line 71
    invoke-virtual {p1}, Lehv;->ata()I

    move-result v0

    .line 73
    iget-object v1, p0, Lhqx;->cFT:Lgda;

    invoke-interface {v1, v0}, Lgda;->jX(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 84
    :goto_0
    return-void

    .line 76
    :pswitch_0
    invoke-direct {p0, v0}, Lhqx;->lq(I)V

    .line 77
    iget-object v0, p0, Lhqx;->doE:Lhqw;

    invoke-virtual {p1}, Lehv;->atb()Ljww;

    move-result-object v1

    iget-object v2, v0, Lhqw;->doD:Ljkt;

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lhqw;->d(Ljww;)Ljkt;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Ljyy;->eMQ:Ljsm;

    iget-object v0, v0, Lhqw;->doD:Ljkt;

    invoke-virtual {v1, v2, v0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 78
    :cond_0
    invoke-virtual {p1}, Lehv;->atb()Ljww;

    move-result-object v0

    invoke-direct {p0, v0}, Lhqx;->e(Ljww;)V

    goto :goto_0

    .line 82
    :pswitch_1
    iget-object v0, p0, Lhqx;->doF:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v0, p0, Lhqx;->doE:Lhqw;

    invoke-virtual {p1}, Lehv;->atb()Ljww;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhqw;->d(Ljww;)Ljkt;

    move-result-object v1

    iput-object v1, v0, Lhqw;->doD:Ljkt;

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final aES()V
    .locals 3

    .prologue
    .line 109
    iget-object v0, p0, Lhqx;->doF:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehv;

    .line 110
    invoke-virtual {v0}, Lehv;->ata()I

    move-result v2

    invoke-direct {p0, v2}, Lhqx;->lq(I)V

    .line 111
    invoke-virtual {v0}, Lehv;->atb()Ljww;

    move-result-object v0

    invoke-direct {p0, v0}, Lhqx;->e(Ljww;)V

    goto :goto_0

    .line 113
    :cond_0
    return-void
.end method
