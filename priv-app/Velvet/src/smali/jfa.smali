.class public final Ljfa;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private egt:Z

.field private egu:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 14049
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 14050
    const/4 v0, 0x0

    iput v0, p0, Ljfa;->aez:I

    iput-boolean v1, p0, Ljfa;->egt:Z

    iput-boolean v1, p0, Ljfa;->egu:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljfa;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfa;->eCz:I

    .line 14051
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 13992
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfa;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfa;->egt:Z

    iget v0, p0, Ljfa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfa;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfa;->egu:Z

    iget v0, p0, Ljfa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfa;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 14065
    iget v0, p0, Ljfa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 14066
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljfa;->egt:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 14068
    :cond_0
    iget v0, p0, Ljfa;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 14069
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljfa;->egu:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 14071
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 14072
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 14076
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 14077
    iget v1, p0, Ljfa;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 14078
    const/4 v1, 0x1

    iget-boolean v2, p0, Ljfa;->egt:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14081
    :cond_0
    iget v1, p0, Ljfa;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 14082
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljfa;->egu:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 14085
    :cond_1
    return v0
.end method
