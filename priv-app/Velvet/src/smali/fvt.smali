.class public final Lfvt;
.super Lfuz;
.source "PG"


# static fields
.field private static final cCG:Lijm;


# instance fields
.field private final cqc:Ljal;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mStringEvaluator:Lgbr;

.field private final mTravelReport:Lgca;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 69
    invoke-static {}, Lijm;->aWZ()Lijn;

    move-result-object v0

    const v1, 0x7f0a01ab

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    invoke-static {v2}, Lius;->w([I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const v1, 0x7f0a01ad

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    invoke-static {v2}, Lius;->w([I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const v1, 0x7f10001c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lius;->w([I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const v1, 0x7f0a01af

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    invoke-static {v2}, Lius;->w([I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    const v1, 0x7f0a01b1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    invoke-static {v2}, Lius;->w([I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lijn;->u(Ljava/lang/Object;Ljava/lang/Object;)Lijn;

    move-result-object v0

    invoke-virtual {v0}, Lijn;->aXa()Lijm;

    move-result-object v0

    sput-object v0, Lfvt;->cCG:Lijm;

    return-void

    :array_0
    .array-data 4
        0x7f0a01ab
        0x7f0a01ac
        0x7f0a01a3
        0x7f0a01a4
        0x7f0a01b3
        0x7f0a01b4
    .end array-data

    :array_1
    .array-data 4
        0x7f0a01ad
        0x7f0a01ae
        0x7f0a01a5
        0x7f0a01a6
        0x7f0a01b5
        0x7f0a01b6
    .end array-data

    :array_2
    .array-data 4
        0x7f10001c
        0x7f10001d
        0x7f10001a
        0x7f10001b
        0x7f10001e
        0x7f10001f
    .end array-data

    :array_3
    .array-data 4
        0x7f0a01af
        0x7f0a01b0
        0x7f0a01a7
        0x7f0a01a8
        0x7f0a01b7
        0x7f0a01b8
    .end array-data

    :array_4
    .array-data 4
        0x7f0a01b1
        0x7f0a01b2
        0x7f0a01a9
        0x7f0a01aa
        0x7f0a01b9
        0x7f0a01ba
    .end array-data
.end method

.method public constructor <init>(Lizj;Lftz;Lemp;Lgbr;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 117
    iput-object p4, p0, Lfvt;->mStringEvaluator:Lgbr;

    .line 118
    iget-object v0, p1, Lizj;->dTg:Ljal;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p1, Lizj;->dTg:Ljal;

    iput-object v0, p0, Lfvt;->cqc:Ljal;

    .line 120
    iget-object v0, p0, Lfvt;->cqc:Ljal;

    iget-object v0, v0, Ljal;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 121
    new-instance v0, Lgca;

    iget-object v1, p0, Lfvt;->cqc:Ljal;

    iget-object v1, v1, Ljal;->dMF:[Liyg;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1, p3}, Lgca;-><init>(Liyg;Lemp;)V

    iput-object v0, p0, Lfvt;->mTravelReport:Lgca;

    .line 129
    :goto_0
    return-void

    .line 123
    :cond_0
    iput-object v1, p0, Lfvt;->mTravelReport:Lgca;

    goto :goto_0

    .line 126
    :cond_1
    iput-object v1, p0, Lfvt;->mTravelReport:Lgca;

    .line 127
    iput-object v1, p0, Lfvt;->cqc:Ljal;

    goto :goto_0
.end method

.method public static a(Ljal;I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 359
    if-nez p0, :cond_0

    .line 360
    const-string v0, "QpLastTrainHomeEntryAdapter"

    const-string v1, "null LastTrainHomeEntry"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :goto_0
    return p1

    .line 363
    :cond_0
    sget-object v0, Lfvt;->cCG:Lijm;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lijm;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 364
    const-string v0, "QpLastTrainHomeEntryAdapter"

    const-string v1, "unknown message id"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 368
    :cond_1
    iget-object v0, p0, Ljal;->dWL:Ljak;

    invoke-virtual {v0}, Ljak;->bed()I

    move-result v0

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 370
    :goto_1
    iget-object v3, p0, Ljal;->dMF:[Liyg;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 371
    iget-object v3, p0, Ljal;->dMF:[Liyg;

    aget-object v3, v3, v2

    iget-object v3, v3, Liyg;->dPL:Liyh;

    invoke-virtual {v3}, Liyh;->bbN()I

    move-result v3

    .line 372
    packed-switch v3, :pswitch_data_0

    .line 380
    if-eqz v0, :cond_5

    const/4 v0, 0x5

    :goto_2
    move v1, v0

    .line 383
    :cond_2
    :goto_3
    sget-object v0, Lfvt;->cCG:Lijm;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lijm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 368
    goto :goto_1

    .line 374
    :pswitch_0
    if-nez v0, :cond_2

    move v1, v2

    goto :goto_3

    .line 377
    :pswitch_1
    if-eqz v0, :cond_4

    const/4 v0, 0x3

    :goto_4
    move v1, v0

    .line 378
    goto :goto_3

    .line 377
    :cond_4
    const/4 v0, 0x2

    goto :goto_4

    .line 380
    :cond_5
    const/4 v0, 0x4

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_3

    .line 372
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/content/Context;Lani;)Lanh;
    .locals 12
    .param p2    # Lani;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 156
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    .line 157
    invoke-virtual {v1, v9}, Lanh;->cm(I)Lanh;

    .line 158
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, v1, Lanh;->ahu:Lizj;

    .line 159
    invoke-virtual {p0, p1}, Lfvt;->bg(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v1, v0}, Lanh;->cn(I)Lanh;

    .line 160
    new-instance v2, Lany;

    invoke-direct {v2}, Lany;-><init>()V

    .line 161
    iput-object v2, v1, Lanh;->agw:Lany;

    .line 162
    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aDF()Liyh;

    move-result-object v0

    if-eqz v0, :cond_3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    invoke-virtual {v0}, Liyh;->bbT()J

    move-result-wide v6

    sub-long v4, v6, v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x404e000000000000L    # 60.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    const-string v3, "<b>"

    const-string v4, "</b>"

    if-gez v0, :cond_1

    const v0, 0x7f0a01ab

    invoke-virtual {p0, v0}, Lfvt;->jH(I)I

    move-result v0

    new-array v5, v11, [Ljava/lang/Object;

    aput-object v3, v5, v9

    aput-object v4, v5, v10

    invoke-virtual {p1, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Lany;->bh(Ljava/lang/String;)Lany;

    .line 163
    if-eqz p2, :cond_0

    .line 164
    iput-object p2, v1, Lanh;->ahs:Lani;

    .line 166
    :cond_0
    invoke-virtual {v1, v10}, Lanh;->aV(Z)Lanh;

    .line 167
    return-object v1

    .line 162
    :cond_1
    if-nez v0, :cond_2

    const v0, 0x7f0a01ad

    invoke-virtual {p0, v0}, Lfvt;->jH(I)I

    move-result v0

    new-array v5, v11, [Ljava/lang/Object;

    aput-object v3, v5, v9

    aput-object v4, v5, v10

    invoke-virtual {p1, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f10001c

    invoke-virtual {p0, v6}, Lfvt;->jH(I)I

    move-result v6

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    aput-object v3, v7, v10

    aput-object v4, v7, v11

    invoke-virtual {v5, v6, v0, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method private aDD()Lani;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 232
    iget-object v0, p0, Lfvt;->cqc:Ljal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfvt;->cqc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    iget-object v0, v0, Lgca;->mRoute:Liyg;

    .line 234
    :goto_0
    iget-object v2, p0, Lfvt;->cqc:Ljal;

    iget-object v2, v2, Ljal;->dWL:Ljak;

    iget-object v2, v2, Ljak;->aeB:Ljbp;

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Lgaz;->a(Ljbp;Liyg;Z)Ljava/lang/String;

    move-result-object v0

    .line 236
    new-instance v2, Lfsa;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    invoke-virtual {v2, v0, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    .line 238
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    .line 233
    goto :goto_0
.end method

.method private aDG()I
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aDF()Liyh;

    move-result-object v0

    .line 335
    if-eqz v0, :cond_0

    .line 336
    invoke-virtual {v0}, Liyh;->bbP()I

    move-result v0

    .line 337
    add-int/lit8 v0, v0, 0xf

    .line 340
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xf

    goto :goto_0
.end method

.method private bf(Landroid/content/Context;)Lanh;
    .locals 11

    .prologue
    const/16 v10, 0x1f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 188
    iget-object v0, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0}, Lfvt;->aDE()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gtz v0, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    .line 189
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 190
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    .line 191
    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    iput-object v3, v0, Lanh;->ahu:Lizj;

    .line 192
    invoke-virtual {p0, p1}, Lfvt;->bg(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {v0, v3}, Lanh;->cn(I)Lanh;

    .line 193
    new-instance v3, Laoj;

    invoke-direct {v3}, Laoj;-><init>()V

    .line 194
    iput-object v3, v0, Lanh;->agz:Laoj;

    .line 195
    iget-object v4, p0, Lfuz;->mEntry:Lizj;

    new-array v5, v1, [I

    invoke-static {v4, v10, v5}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v4

    .line 197
    invoke-virtual {p0}, Lfvt;->aDE()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {p0}, Lfvt;->aDG()I

    move-result v5

    mul-int/lit8 v5, v5, 0x3c

    int-to-long v8, v5

    sub-long/2addr v6, v8

    iget-object v5, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v5}, Lemp;->currentTimeMillis()J

    move-result-wide v8

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    cmp-long v5, v8, v6

    if-ltz v5, :cond_0

    move v1, v2

    :cond_0
    if-eqz v1, :cond_2

    .line 198
    const v1, 0x7f0a02a2

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 207
    :goto_1
    invoke-virtual {v0, v2}, Lanh;->aV(Z)Lanh;

    .line 210
    :goto_2
    return-object v0

    :cond_1
    move v0, v1

    .line 188
    goto :goto_0

    .line 199
    :cond_2
    if-eqz v4, :cond_3

    .line 200
    invoke-virtual {v4}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 201
    new-instance v1, Lfsa;

    invoke-direct {v1, v10}, Lfsa;-><init>(I)V

    const v3, 0x7f02008b

    invoke-virtual {v1, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Lfsa;->jC(I)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    goto :goto_1

    .line 205
    :cond_3
    const v1, 0x7f0a01bb

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Laoj;->bS(Ljava/lang/String;)Laoj;

    goto :goto_1

    .line 210
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 133
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 134
    invoke-direct {p0}, Lfvt;->aDD()Lani;

    move-result-object v7

    .line 135
    invoke-direct {p0, p1, v7}, Lfvt;->a(Landroid/content/Context;Lani;)Lanh;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-direct {p0, p1}, Lfvt;->bf(Landroid/content/Context;)Lanh;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 138
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    :cond_0
    iget-object v0, p0, Lfvt;->cqc:Ljal;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfvt;->cqc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    if-eqz v0, :cond_1

    .line 142
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, p0, Lfvt;->cqc:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iget-object v2, v0, Ljak;->aeB:Ljbp;

    iget-object v0, p0, Lfvt;->cqc:Ljal;

    iget-object v0, v0, Ljal;->dMF:[Liyg;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lfvt;->mStringEvaluator:Lgbr;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lgbx;->a(Landroid/content/Context;Lizj;Ljbp;Ljava/lang/Iterable;ZLgbr;)Lanh;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_1
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    invoke-virtual {v0, v8}, Lanh;->cm(I)Lanh;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iput-object v1, v0, Lanh;->ahu:Lizj;

    new-instance v1, Laor;

    invoke-direct {v1}, Laor;-><init>()V

    iput-object v1, v0, Lanh;->agx:Laor;

    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBP()Landroid/location/Location;

    move-result-object v2

    invoke-static {v2}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v2

    iput-object v2, v1, Laor;->ajY:Ljbp;

    iget-object v2, p0, Lfvt;->cqc:Ljal;

    iput-object v2, v1, Laor;->ajZ:Ljal;

    invoke-virtual {v1, v8}, Laor;->bf(Z)Laor;

    if-eqz v7, :cond_2

    iput-object v7, v0, Lanh;->ahs:Lani;

    :cond_2
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 150
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 151
    invoke-virtual {v1, v8}, Lang;->aS(Z)Lang;

    .line 152
    return-object v1
.end method

.method protected final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 172
    packed-switch p3, :pswitch_data_0

    .line 178
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 174
    :pswitch_0
    invoke-direct {p0}, Lfvt;->aDD()Lani;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lfvt;->a(Landroid/content/Context;Lani;)Lanh;

    move-result-object v0

    goto :goto_0

    .line 176
    :pswitch_1
    invoke-direct {p0, p1}, Lfvt;->bf(Landroid/content/Context;)Lanh;

    move-result-object v0

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final aDE()Ljava/lang/Long;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aDF()Liyh;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aDF()Liyh;

    move-result-object v0

    invoke-virtual {v0}, Liyh;->bbT()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 293
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aDF()Liyh;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfvt;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aDF()Liyh;

    move-result-object v0

    goto :goto_0
.end method

.method public final bg(Landroid/content/Context;)I
    .locals 6

    .prologue
    .line 308
    const v0, 0x7f0b00b9

    .line 309
    invoke-virtual {p0}, Lfvt;->aDE()Ljava/lang/Long;

    move-result-object v1

    .line 310
    if-eqz v1, :cond_0

    .line 311
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lfuz;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    .line 313
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v2, v4, v2

    long-to-double v2, v2

    const-wide/high16 v4, 0x404e000000000000L    # 60.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 315
    invoke-direct {p0}, Lfvt;->aDG()I

    move-result v2

    .line 317
    if-ge v1, v2, :cond_1

    .line 318
    const v0, 0x7f0b00b7

    .line 324
    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0

    .line 319
    :cond_1
    add-int/lit8 v2, v2, 0xa

    if-ge v1, v2, :cond_0

    .line 321
    const v0, 0x7f0b00b8

    goto :goto_0
.end method

.method public final jH(I)I
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lfvt;->cqc:Ljal;

    invoke-static {v0, p1}, Lfvt;->a(Ljal;I)I

    move-result v0

    return v0
.end method
