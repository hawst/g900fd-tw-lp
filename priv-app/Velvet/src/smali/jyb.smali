.class public final Ljyb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dKS:I

.field private eLD:I

.field private eLE:I

.field private eLF:I

.field private eLG:[B


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 356
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 357
    iput v0, p0, Ljyb;->aez:I

    iput v1, p0, Ljyb;->dKS:I

    iput v1, p0, Ljyb;->eLD:I

    iput v0, p0, Ljyb;->eLE:I

    iput v0, p0, Ljyb;->eLF:I

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljyb;->eLG:[B

    const/4 v0, 0x0

    iput-object v0, p0, Ljyb;->eCq:Ljsn;

    iput v1, p0, Ljyb;->eCz:I

    .line 358
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 239
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljyb;->dKS:I

    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyb;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljyb;->eLD:I

    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyb;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljyb;->eLE:I

    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyb;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljyb;->eLF:I

    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljyb;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljyb;->eLG:[B

    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljyb;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 375
    iget v0, p0, Ljyb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 376
    const/4 v0, 0x1

    iget v1, p0, Ljyb;->dKS:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 378
    :cond_0
    iget v0, p0, Ljyb;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 379
    const/4 v0, 0x2

    iget v1, p0, Ljyb;->eLD:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 381
    :cond_1
    iget v0, p0, Ljyb;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 382
    const/4 v0, 0x3

    iget v1, p0, Ljyb;->eLE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 384
    :cond_2
    iget v0, p0, Ljyb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 385
    const/4 v0, 0x4

    iget v1, p0, Ljyb;->eLF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 387
    :cond_3
    iget v0, p0, Ljyb;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 388
    const/4 v0, 0x5

    iget-object v1, p0, Ljyb;->eLG:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 390
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 391
    return-void
.end method

.method public final aE([B)Ljyb;
    .locals 1

    .prologue
    .line 340
    if-nez p1, :cond_0

    .line 341
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 343
    :cond_0
    iput-object p1, p0, Ljyb;->eLG:[B

    .line 344
    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljyb;->aez:I

    .line 345
    return-object p0
.end method

.method public final bvV()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Ljyb;->dKS:I

    return v0
.end method

.method public final bvW()I
    .locals 1

    .prologue
    .line 280
    iget v0, p0, Ljyb;->eLD:I

    return v0
.end method

.method public final bvX()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Ljyb;->eLE:I

    return v0
.end method

.method public final bvY()I
    .locals 1

    .prologue
    .line 318
    iget v0, p0, Ljyb;->eLF:I

    return v0
.end method

.method public final bvZ()Z
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Ljyb;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bwa()[B
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Ljyb;->eLG:[B

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 395
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 396
    iget v1, p0, Ljyb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 397
    const/4 v1, 0x1

    iget v2, p0, Ljyb;->dKS:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    :cond_0
    iget v1, p0, Ljyb;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 401
    const/4 v1, 0x2

    iget v2, p0, Ljyb;->eLD:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_1
    iget v1, p0, Ljyb;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 405
    const/4 v1, 0x3

    iget v2, p0, Ljyb;->eLE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 408
    :cond_2
    iget v1, p0, Ljyb;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 409
    const/4 v1, 0x4

    iget v2, p0, Ljyb;->eLF:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 412
    :cond_3
    iget v1, p0, Ljyb;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 413
    const/4 v1, 0x5

    iget-object v2, p0, Ljyb;->eLG:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 416
    :cond_4
    return v0
.end method

.method public final tb(I)Ljyb;
    .locals 1

    .prologue
    .line 264
    iput p1, p0, Ljyb;->dKS:I

    .line 265
    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyb;->aez:I

    .line 266
    return-object p0
.end method

.method public final tc(I)Ljyb;
    .locals 1

    .prologue
    .line 283
    iput p1, p0, Ljyb;->eLD:I

    .line 284
    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljyb;->aez:I

    .line 285
    return-object p0
.end method

.method public final td(I)Ljyb;
    .locals 1

    .prologue
    .line 302
    iput p1, p0, Ljyb;->eLE:I

    .line 303
    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljyb;->aez:I

    .line 304
    return-object p0
.end method

.method public final te(I)Ljyb;
    .locals 1

    .prologue
    .line 321
    iput p1, p0, Ljyb;->eLF:I

    .line 322
    iget v0, p0, Ljyb;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljyb;->aez:I

    .line 323
    return-object p0
.end method
