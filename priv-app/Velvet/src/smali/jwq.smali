.class public final Ljwq;
.super Ljsl;
.source "PG"


# static fields
.field public static final eJt:Ljsm;


# instance fields
.field private aez:I

.field public eJu:[Ljwn;

.field public eJv:[Ljwo;

.field private eJw:Z

.field private eol:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xb

    const-class v1, Ljwq;

    const v2, 0x106160b2

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwq;->eJt:Ljsm;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 76
    iput v1, p0, Ljwq;->aez:I

    const-string v0, "/search"

    iput-object v0, p0, Ljwq;->eol:Ljava/lang/String;

    invoke-static {}, Ljwn;->bvm()[Ljwn;

    move-result-object v0

    iput-object v0, p0, Ljwq;->eJu:[Ljwn;

    invoke-static {}, Ljwo;->bvn()[Ljwo;

    move-result-object v0

    iput-object v0, p0, Ljwq;->eJv:[Ljwo;

    iput-boolean v1, p0, Ljwq;->eJw:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljwq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwq;->eCz:I

    .line 77
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwq;->eJu:[Ljwn;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljwn;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljwq;->eJu:[Ljwn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljwn;

    invoke-direct {v3}, Ljwn;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljwq;->eJu:[Ljwn;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljwn;

    invoke-direct {v3}, Ljwn;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljwq;->eJu:[Ljwn;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljwq;->eJv:[Ljwo;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljwo;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljwq;->eJv:[Ljwo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljwo;

    invoke-direct {v3}, Ljwo;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljwq;->eJv:[Ljwo;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljwo;

    invoke-direct {v3}, Ljwo;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljwq;->eJv:[Ljwo;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljwq;->eol:Ljava/lang/String;

    iget v0, p0, Ljwq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwq;->aez:I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljwq;->eJw:Z

    iget v0, p0, Ljwq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwq;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Ljwq;->eJu:[Ljwn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljwq;->eJu:[Ljwn;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 94
    :goto_0
    iget-object v2, p0, Ljwq;->eJu:[Ljwn;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 95
    iget-object v2, p0, Ljwq;->eJu:[Ljwn;

    aget-object v2, v2, v0

    .line 96
    if-eqz v2, :cond_0

    .line 97
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 94
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_1
    iget-object v0, p0, Ljwq;->eJv:[Ljwo;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljwq;->eJv:[Ljwo;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 102
    :goto_1
    iget-object v0, p0, Ljwq;->eJv:[Ljwo;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 103
    iget-object v0, p0, Ljwq;->eJv:[Ljwo;

    aget-object v0, v0, v1

    .line 104
    if-eqz v0, :cond_2

    .line 105
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 102
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 109
    :cond_3
    iget v0, p0, Ljwq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 110
    const/4 v0, 0x3

    iget-object v1, p0, Ljwq;->eol:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 112
    :cond_4
    iget v0, p0, Ljwq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 113
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljwq;->eJw:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 115
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 116
    return-void
.end method

.method public final bvu()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Ljwq;->eJw:Z

    return v0
.end method

.method public final jh(Z)Ljwq;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljwq;->eJw:Z

    .line 63
    iget v0, p0, Ljwq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljwq;->aez:I

    .line 64
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 120
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 121
    iget-object v2, p0, Ljwq;->eJu:[Ljwn;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljwq;->eJu:[Ljwn;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 122
    :goto_0
    iget-object v3, p0, Ljwq;->eJu:[Ljwn;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 123
    iget-object v3, p0, Ljwq;->eJu:[Ljwn;

    aget-object v3, v3, v0

    .line 124
    if-eqz v3, :cond_0

    .line 125
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 122
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 130
    :cond_2
    iget-object v2, p0, Ljwq;->eJv:[Ljwo;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljwq;->eJv:[Ljwo;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 131
    :goto_1
    iget-object v2, p0, Ljwq;->eJv:[Ljwo;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 132
    iget-object v2, p0, Ljwq;->eJv:[Ljwo;

    aget-object v2, v2, v1

    .line 133
    if-eqz v2, :cond_3

    .line 134
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 139
    :cond_4
    iget v1, p0, Ljwq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_5

    .line 140
    const/4 v1, 0x3

    iget-object v2, p0, Ljwq;->eol:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_5
    iget v1, p0, Ljwq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_6

    .line 144
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljwq;->eJw:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 147
    :cond_6
    return v0
.end method

.method public final zv(Ljava/lang/String;)Ljwq;
    .locals 1

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Ljwq;->eol:Ljava/lang/String;

    .line 38
    iget v0, p0, Ljwq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwq;->aez:I

    .line 39
    return-object p0
.end method
