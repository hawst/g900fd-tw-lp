.class public final Ljvw;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eIc:J

.field private eId:J

.field private eIj:[B

.field public eIk:[Ljvs;

.field private eIr:Ljvx;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 707
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 708
    const/4 v0, 0x0

    iput v0, p0, Ljvw;->aez:I

    iput-wide v2, p0, Ljvw;->eIc:J

    iput-wide v2, p0, Ljvw;->eId:J

    invoke-static {}, Ljvs;->bvd()[Ljvs;

    move-result-object v0

    iput-object v0, p0, Ljvw;->eIk:[Ljvs;

    iput-object v1, p0, Ljvw;->eIr:Ljvx;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljvw;->eIj:[B

    iput-object v1, p0, Ljvw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljvw;->eCz:I

    .line 709
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 499
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljvw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljvw;->eIc:J

    iget v0, p0, Ljvw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljvw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljvw;->eId:J

    iget v0, p0, Ljvw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljvw;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljvw;->eIk:[Ljvs;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljvs;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljvw;->eIk:[Ljvs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljvs;

    invoke-direct {v3}, Ljvs;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljvw;->eIk:[Ljvs;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljvs;

    invoke-direct {v3}, Ljvs;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljvw;->eIk:[Ljvs;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljvw;->eIj:[B

    iget v0, p0, Ljvw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljvw;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljvw;->eIr:Ljvx;

    if-nez v0, :cond_4

    new-instance v0, Ljvx;

    invoke-direct {v0}, Ljvx;-><init>()V

    iput-object v0, p0, Ljvw;->eIr:Ljvx;

    :cond_4
    iget-object v0, p0, Ljvw;->eIr:Ljvx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 726
    iget v0, p0, Ljvw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 727
    const/4 v0, 0x1

    iget-wide v2, p0, Ljvw;->eIc:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 729
    :cond_0
    iget v0, p0, Ljvw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 730
    const/4 v0, 0x2

    iget-wide v2, p0, Ljvw;->eId:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 732
    :cond_1
    iget-object v0, p0, Ljvw;->eIk:[Ljvs;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljvw;->eIk:[Ljvs;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 733
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljvw;->eIk:[Ljvs;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 734
    iget-object v1, p0, Ljvw;->eIk:[Ljvs;

    aget-object v1, v1, v0

    .line 735
    if-eqz v1, :cond_2

    .line 736
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 733
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 740
    :cond_3
    iget v0, p0, Ljvw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 741
    const/4 v0, 0x4

    iget-object v1, p0, Ljvw;->eIj:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 743
    :cond_4
    iget-object v0, p0, Ljvw;->eIr:Ljvx;

    if-eqz v0, :cond_5

    .line 744
    const/4 v0, 0x5

    iget-object v1, p0, Ljvw;->eIr:Ljvx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 746
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 747
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 751
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 752
    iget v1, p0, Ljvw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 753
    const/4 v1, 0x1

    iget-wide v2, p0, Ljvw;->eIc:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 756
    :cond_0
    iget v1, p0, Ljvw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 757
    const/4 v1, 0x2

    iget-wide v2, p0, Ljvw;->eId:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 760
    :cond_1
    iget-object v1, p0, Ljvw;->eIk:[Ljvs;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljvw;->eIk:[Ljvs;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 761
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljvw;->eIk:[Ljvs;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 762
    iget-object v2, p0, Ljvw;->eIk:[Ljvs;

    aget-object v2, v2, v0

    .line 763
    if-eqz v2, :cond_2

    .line 764
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 761
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 769
    :cond_4
    iget v1, p0, Ljvw;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 770
    const/4 v1, 0x4

    iget-object v2, p0, Ljvw;->eIj:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 773
    :cond_5
    iget-object v1, p0, Ljvw;->eIr:Ljvx;

    if-eqz v1, :cond_6

    .line 774
    const/4 v1, 0x5

    iget-object v2, p0, Ljvw;->eIr:Ljvx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 777
    :cond_6
    return v0
.end method
