.class public final Liws;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dMr:I

.field private dMs:I

.field private dMt:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1812
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1813
    iput v0, p0, Liws;->aez:I

    iput v0, p0, Liws;->dMr:I

    iput v0, p0, Liws;->dMs:I

    iput-boolean v0, p0, Liws;->dMt:Z

    const/4 v0, 0x0

    iput-object v0, p0, Liws;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liws;->eCz:I

    .line 1814
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1736
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liws;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liws;->dMr:I

    iget v0, p0, Liws;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liws;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liws;->dMs:I

    iget v0, p0, Liws;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liws;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Liws;->dMt:Z

    iget v0, p0, Liws;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liws;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1829
    iget v0, p0, Liws;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1830
    const/4 v0, 0x1

    iget v1, p0, Liws;->dMr:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1832
    :cond_0
    iget v0, p0, Liws;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1833
    const/4 v0, 0x2

    iget v1, p0, Liws;->dMs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1835
    :cond_1
    iget v0, p0, Liws;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1836
    const/4 v0, 0x3

    iget-boolean v1, p0, Liws;->dMt:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1838
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1839
    return-void
.end method

.method public final aZw()I
    .locals 1

    .prologue
    .line 1758
    iget v0, p0, Liws;->dMr:I

    return v0
.end method

.method public final aZx()I
    .locals 1

    .prologue
    .line 1777
    iget v0, p0, Liws;->dMs:I

    return v0
.end method

.method public final hf(Z)Liws;
    .locals 1

    .prologue
    .line 1799
    iput-boolean p1, p0, Liws;->dMt:Z

    .line 1800
    iget v0, p0, Liws;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Liws;->aez:I

    .line 1801
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1843
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1844
    iget v1, p0, Liws;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1845
    const/4 v1, 0x1

    iget v2, p0, Liws;->dMr:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1848
    :cond_0
    iget v1, p0, Liws;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1849
    const/4 v1, 0x2

    iget v2, p0, Liws;->dMs:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1852
    :cond_1
    iget v1, p0, Liws;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1853
    const/4 v1, 0x3

    iget-boolean v2, p0, Liws;->dMt:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1856
    :cond_2
    return v0
.end method

.method public final nf(I)Liws;
    .locals 1

    .prologue
    .line 1761
    iput p1, p0, Liws;->dMr:I

    .line 1762
    iget v0, p0, Liws;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liws;->aez:I

    .line 1763
    return-object p0
.end method

.method public final ng(I)Liws;
    .locals 1

    .prologue
    .line 1780
    iput p1, p0, Liws;->dMs:I

    .line 1781
    iget v0, p0, Liws;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liws;->aez:I

    .line 1782
    return-object p0
.end method
