.class public final Leyk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leyj;


# static fields
.field private static clP:Z


# instance fields
.field private clQ:Leyl;

.field private final mAppContext:Landroid/content/Context;

.field private final mClock:Lemp;

.field private mPredictiveCardsPreferences:Lcxs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Leyk;->clP:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lemp;Lcxs;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Leyk;->mAppContext:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Leyk;->mClock:Lemp;

    .line 42
    iput-object p3, p0, Leyk;->mPredictiveCardsPreferences:Lcxs;

    .line 43
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lfdb;)V
    .locals 3

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    sget-boolean v0, Leyk;->clP:Z

    if-nez v0, :cond_0

    .line 56
    invoke-virtual {p1}, Lfdb;->axN()Leym;

    move-result-object v0

    invoke-interface {v0}, Leym;->cV()V

    .line 59
    invoke-static {p1}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->c(Lfdb;)V

    .line 63
    new-instance v0, Leyl;

    iget-object v1, p0, Leyk;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Leyk;->mClock:Lemp;

    invoke-direct {v0, v1, v2}, Leyl;-><init>(Landroid/content/Context;Lemp;)V

    iput-object v0, p0, Leyk;->clQ:Leyl;

    new-instance v0, Leyg;

    iget-object v1, p0, Leyk;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Leyg;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Leyk;->clQ:Leyl;

    invoke-virtual {v0, v1}, Leyg;->a(Landroid/database/ContentObserver;)V

    .line 65
    const/4 v0, 0x1

    sput-boolean v0, Leyk;->clP:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :cond_0
    monitor-exit p0

    return-void

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final awy()Leym;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Leyn;

    iget-object v1, p0, Leyk;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Leyk;->mClock:Lemp;

    iget-object v3, p0, Leyk;->mPredictiveCardsPreferences:Lcxs;

    invoke-direct {v0, v1, v2, v3}, Leyn;-><init>(Landroid/content/Context;Lemp;Lcxs;)V

    return-object v0
.end method

.method public final declared-synchronized b(Lfdb;)V
    .locals 2

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Leyk;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->aD(Landroid/content/Context;)V

    .line 77
    sget-boolean v0, Leyk;->clP:Z

    if-eqz v0, :cond_1

    .line 81
    invoke-static {p1}, Lcom/google/android/sidekick/main/calendar/CalendarIntentService;->d(Lfdb;)V

    .line 84
    iget-object v0, p0, Leyk;->clQ:Leyl;

    if-eqz v0, :cond_0

    new-instance v0, Leyg;

    iget-object v1, p0, Leyk;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Leyg;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Leyk;->clQ:Leyl;

    invoke-virtual {v0, v1}, Leyg;->b(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Leyk;->clQ:Leyl;

    .line 86
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Leyk;->clP:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :cond_1
    monitor-exit p0

    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
