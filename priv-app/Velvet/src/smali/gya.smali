.class public Lgya;
.super Lesj;
.source "PG"

# interfaces
.implements Lesk;
.implements Lhiy;


# instance fields
.field private final btd:Ljava/lang/String;

.field private final cYX:Ljava/lang/String;

.field private final cYY:Z

.field private final mEventBus:Ldda;

.field private final mLocalTtsManager:Lice;

.field private final mQuery:Lcom/google/android/shared/search/Query;

.field private final mTtsAudioPlayer:Lhix;


# direct methods
.method constructor <init>(Ldda;Lcom/google/android/shared/search/Query;Lhix;Lice;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 457
    invoke-direct {p0}, Lesj;-><init>()V

    .line 458
    iput-object p1, p0, Lgya;->mEventBus:Ldda;

    .line 459
    iput-object p2, p0, Lgya;->mQuery:Lcom/google/android/shared/search/Query;

    .line 460
    iput-object p3, p0, Lgya;->mTtsAudioPlayer:Lhix;

    .line 461
    iput-object p4, p0, Lgya;->mLocalTtsManager:Lice;

    .line 462
    iput-object p5, p0, Lgya;->btd:Ljava/lang/String;

    .line 463
    iput-object p6, p0, Lgya;->cYX:Ljava/lang/String;

    .line 464
    iput-boolean p7, p0, Lgya;->cYY:Z

    .line 465
    return-void
.end method


# virtual methods
.method public final PO()V
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Lgya;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    iget-object v1, p0, Lgya;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldcw;->at(Lcom/google/android/shared/search/Query;)V

    .line 485
    return-void
.end method

.method public final a(Lejm;)V
    .locals 4

    .prologue
    .line 469
    iget-object v0, p0, Lgya;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iget-object v1, p0, Lgya;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lgya;->btd:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 471
    iget-object v1, p0, Lgya;->mLocalTtsManager:Lice;

    iget-object v2, p0, Lgya;->btd:Ljava/lang/String;

    iget-boolean v0, p0, Lgya;->cYY:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lgya;->cYX:Ljava/lang/String;

    invoke-virtual {v1, v2, p0, v0, v3}, Lice;->a(Ljava/lang/String;Lesk;ILjava/lang/String;)V

    .line 480
    :cond_0
    :goto_1
    return-void

    .line 471
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 477
    :cond_2
    iget-object v0, p0, Lgya;->mTtsAudioPlayer:Lhix;

    invoke-virtual {v0, p0}, Lhix;->a(Lhiy;)V

    goto :goto_1
.end method

.method public run()V
    .locals 0

    .prologue
    .line 492
    invoke-virtual {p0}, Lgya;->PO()V

    .line 493
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 497
    const-string v0, "PlayTtsTransaction"

    return-object v0
.end method
