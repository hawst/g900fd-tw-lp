.class public final Lgbr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Lemp;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lgbr;->mClock:Lemp;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljhe;)Ljava/lang/CharSequence;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-virtual {p2}, Ljhe;->blA()Z

    move-result v1

    if-nez v1, :cond_1

    .line 39
    const-string v1, "TemplatedStringEvaluator"

    const-string v2, "Got templated string with no display string"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :cond_0
    :goto_0
    return-object v0

    .line 43
    :cond_1
    invoke-virtual {p0, p1, p2}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v1

    .line 44
    if-eqz v1, :cond_0

    .line 48
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Ljhe;)Ljava/lang/String;
    .locals 12
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-virtual {p2}, Ljhe;->blA()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    const-string v0, "TemplatedStringEvaluator"

    const-string v2, "Got templated string with no display string"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 85
    :goto_0
    return-object v0

    .line 62
    :cond_0
    iget-object v0, p2, Ljhe;->elz:[Ljhf;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 63
    invoke-virtual {p2}, Ljhe;->bjI()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 67
    iget-object v4, p2, Ljhe;->elz:[Ljhf;

    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v0, v4, v2

    .line 68
    invoke-virtual {v0}, Ljhf;->getType()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :goto_2
    move-object v0, v1

    .line 71
    :goto_3
    if-nez v0, :cond_2

    .line 72
    const-string v0, "(invalid param)"

    .line 74
    :cond_2
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 68
    :pswitch_0
    invoke-virtual {v0}, Ljhf;->blC()Z

    move-result v6

    if-nez v6, :cond_3

    const-string v0, "TemplatedStringEvaluator"

    const-string v6, "ELAPSED_TIME template parameter had no timestamp"

    invoke-static {v0, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    iget-object v6, p0, Lgbr;->mClock:Lemp;

    invoke-interface {v6}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Ljhf;->bcB()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    sub-long/2addr v6, v8

    const/4 v0, 0x1

    invoke-static {p1, v6, v7, v0}, Lesi;->b(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 77
    :cond_4
    invoke-virtual {p2}, Ljhe;->bjI()Ljava/lang/String;

    move-result-object v1

    .line 79
    :try_start_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/IllegalFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 84
    const-string v2, "TemplatedStringEvaluator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error formatting display string \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 85
    goto/16 :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
