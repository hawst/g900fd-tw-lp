.class public final Lckj;
.super Lczq;
.source "PG"


# instance fields
.field private final aZK:Ljava/lang/Object;

.field private aZL:Ljava/util/concurrent/Future;

.field private aZM:Ljava/util/concurrent/Future;

.field private final mExecutor:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Lemp;Landroid/content/Context;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 5

    .prologue
    .line 62
    const-string v0, "base_page_cache_file"

    invoke-direct {p0, v0, p2}, Lczq;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lckj;->aZK:Ljava/lang/Object;

    .line 63
    iput-object p3, p0, Lckj;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 65
    iget-object v1, p0, Lckj;->aZK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lckj;->aZM:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lckj;->aZM:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lckj;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lckl;

    const-string v3, "Read contents from storage"

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-direct {v2, p0, v3, v4}, Lckl;-><init>(Lckj;Ljava/lang/String;[I)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lckj;->aZM:Ljava/util/concurrent/Future;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method private OQ()V
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lckj;->OR()Lcko;

    move-result-object v0

    .line 146
    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    .line 148
    invoke-direct {p0, v0}, Lckj;->s([B)V

    .line 149
    return-void
.end method

.method static synthetic a(Lckj;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-virtual {p0}, Lckj;->Um()[B

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {v0}, Lcko;->t([B)Lcko;

    move-result-object v0

    iget-object v2, v0, Lcko;->aZV:[Lckp;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcko;->aZV:[Lckp;

    array-length v2, v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v0, Lcko;->aZV:[Lckp;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    new-instance v5, Lcki;

    invoke-direct {v5, v4}, Lcki;-><init>(Lckp;)V

    const/4 v4, 0x0

    invoke-virtual {p0, v5, v4}, Lckj;->a(Lcki;Z)V
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "Search.BasePageContentCache"

    const-string v3, "Failed to load BasePageContentInfo"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lckj;[B)V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0}, Lczq;->Ul()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private s([B)V
    .locals 5

    .prologue
    .line 159
    iget-object v1, p0, Lckj;->aZK:Ljava/lang/Object;

    monitor-enter v1

    .line 160
    :try_start_0
    iget-object v0, p0, Lckj;->aZL:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lckj;->aZL:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lckj;->aZL:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 164
    :cond_0
    iget-object v0, p0, Lckj;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v2, Lckk;

    const-string v3, "Write contents to storage"

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-direct {v2, p0, v3, v4, p1}, Lckk;-><init>(Lckj;Ljava/lang/String;[I[B)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lckj;->aZL:Ljava/util/concurrent/Future;

    .line 171
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 164
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method


# virtual methods
.method public final OR()Lcko;
    .locals 6

    .prologue
    .line 193
    new-instance v2, Lcko;

    invoke-direct {v2}, Lcko;-><init>()V

    .line 194
    iget-object v0, p0, Lckj;->bon:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v3, v0, [Lckp;

    .line 195
    iget-object v0, p0, Lckj;->bon:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 197
    const/4 v0, 0x0

    move v1, v0

    .line 198
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 200
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcki;

    .line 201
    invoke-virtual {v0}, Lcki;->OM()Lckp;

    move-result-object v5

    .line 202
    add-int/lit8 v0, v1, 0x1

    aput-object v5, v3, v1

    move v1, v0

    .line 203
    goto :goto_0

    .line 204
    :cond_0
    iput-object v3, v2, Lcko;->aZV:[Lckp;

    .line 205
    return-object v2
.end method

.method public final a(Lcki;Z)V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p1}, Lcki;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lckj;->j(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 78
    if-eqz p2, :cond_0

    .line 79
    invoke-direct {p0}, Lckj;->OQ()V

    .line 81
    :cond_0
    return-void
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Lczq;->eA()V

    .line 122
    new-instance v0, Lcko;

    invoke-direct {v0}, Lcko;-><init>()V

    invoke-static {v0}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lckj;->s([B)V

    .line 123
    return-void
.end method

.method public final eA()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lckj;->OQ()V

    .line 113
    invoke-super {p0}, Lczq;->eA()V

    .line 114
    return-void
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lckj;->hh(Ljava/lang/String;)Lcki;

    move-result-object v0

    return-object v0
.end method

.method public final hh(Ljava/lang/String;)Lcki;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-super {p0, p1}, Lczq;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcki;

    if-eqz v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcki;->OP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    :cond_0
    return-object v0
.end method
