.class public final Ldsl;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"

# interfaces
.implements Landroid/animation/LayoutTransition$TransitionListener;


# instance fields
.field private final Vs:Landroid/animation/LayoutTransition;

.field private bIA:Z

.field private bIB:Z

.field private bIC:I

.field private bID:I

.field private bIE:I

.field private bIF:I

.field private bIG:Z

.field private synthetic bIp:Lcom/google/android/search/searchplate/SearchPlate;

.field private bIq:I

.field private bIr:Ldto;

.field private bIs:I

.field private bIt:Ljava/lang/String;

.field private bIu:Landroid/text/Spanned;

.field private bIv:Z

.field private bIw:Z

.field private bIx:Z

.field private bIy:Z

.field private bIz:Z


# direct methods
.method public constructor <init>(Lcom/google/android/search/searchplate/SearchPlate;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1353
    iput-object p1, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 1339
    const/4 v0, 0x0

    iput v0, p0, Ldsl;->bIq:I

    .line 1340
    iput-object v1, p0, Ldsl;->bIr:Ldto;

    .line 1343
    iput-object v1, p0, Ldsl;->bIu:Landroid/text/Spanned;

    .line 1354
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 1355
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    iput-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    .line 1356
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spanned;)V
    .locals 2

    .prologue
    .line 1510
    invoke-virtual {p0}, Ldsl;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1511
    iput-object p1, p0, Ldsl;->bIu:Landroid/text/Spanned;

    .line 1516
    :goto_0
    return-void

    .line 1513
    :cond_0
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Landroid/text/Spanned;Z)V

    .line 1514
    const/4 v0, 0x0

    iput-object v0, p0, Ldsl;->bIu:Landroid/text/Spanned;

    goto :goto_0
.end method

.method public final a(Ldto;)V
    .locals 2

    .prologue
    .line 1470
    invoke-virtual {p0}, Ldsl;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1471
    iput-object p1, p0, Ldsl;->bIr:Ldto;

    .line 1476
    :goto_0
    return-void

    .line 1473
    :cond_0
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Ldto;Z)V

    .line 1474
    const/4 v0, 0x0

    iput-object v0, p0, Ldsl;->bIr:Ldto;

    goto :goto_0
.end method

.method public final afA()Z
    .locals 1

    .prologue
    .line 1538
    iget-boolean v0, p0, Ldsl;->bIG:Z

    return v0
.end method

.method public final afB()I
    .locals 1

    .prologue
    .line 1542
    iget-boolean v0, p0, Ldsl;->bIw:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldsl;->bIC:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHX:I

    goto :goto_0
.end method

.method public final afC()I
    .locals 1

    .prologue
    .line 1546
    iget-boolean v0, p0, Ldsl;->bIy:Z

    if-eqz v0, :cond_0

    .line 1547
    iget v0, p0, Ldsl;->bIF:I

    .line 1549
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Ldsl;->bIv:Z

    if-eqz v0, :cond_1

    iget v0, p0, Ldsl;->bID:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {v0}, Lcom/google/android/search/searchplate/SearchPlate;->e(Lcom/google/android/search/searchplate/SearchPlate;)I

    move-result v0

    goto :goto_0
.end method

.method final afx()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1376
    invoke-virtual {p0}, Ldsl;->isRunning()Z

    move-result v2

    if-nez v2, :cond_8

    iget-boolean v2, p0, Ldsl;->bIB:Z

    if-nez v2, :cond_8

    .line 1377
    iput-boolean v0, p0, Ldsl;->bIB:Z

    .line 1378
    iget-boolean v2, p0, Ldsl;->bIv:Z

    if-nez v2, :cond_7

    iget-boolean v2, p0, Ldsl;->bIw:Z

    if-nez v2, :cond_7

    .line 1382
    :goto_0
    iget-boolean v2, p0, Ldsl;->bIv:Z

    if-eqz v2, :cond_0

    .line 1383
    iget v2, p0, Ldsl;->bID:I

    invoke-virtual {p0, v2}, Ldsl;->gn(I)V

    .line 1385
    :cond_0
    iget-boolean v2, p0, Ldsl;->bIw:Z

    if-eqz v2, :cond_1

    .line 1386
    iget v2, p0, Ldsl;->bIC:I

    iget v3, p0, Ldsl;->bIE:I

    invoke-virtual {p0, v2, v3}, Ldsl;->aw(II)V

    .line 1388
    :cond_1
    iget-object v2, p0, Ldsl;->bIr:Ldto;

    if-eqz v2, :cond_2

    .line 1389
    iget-object v2, p0, Ldsl;->bIr:Ldto;

    invoke-virtual {p0, v2}, Ldsl;->a(Ldto;)V

    .line 1391
    :cond_2
    iget-boolean v2, p0, Ldsl;->bIx:Z

    if-eqz v2, :cond_3

    .line 1392
    iget v2, p0, Ldsl;->bIs:I

    iget-object v3, p0, Ldsl;->bIt:Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Ldsl;->j(ILjava/lang/String;)V

    .line 1394
    :cond_3
    iget-boolean v2, p0, Ldsl;->bIz:Z

    if-eqz v2, :cond_4

    .line 1395
    invoke-virtual {p0}, Ldsl;->afy()V

    .line 1397
    :cond_4
    iget-boolean v2, p0, Ldsl;->bIA:Z

    if-eqz v2, :cond_5

    .line 1398
    invoke-virtual {p0}, Ldsl;->afz()V

    .line 1400
    :cond_5
    iget-object v2, p0, Ldsl;->bIu:Landroid/text/Spanned;

    if-eqz v2, :cond_6

    .line 1401
    iget-object v2, p0, Ldsl;->bIu:Landroid/text/Spanned;

    invoke-virtual {p0, v2}, Ldsl;->a(Landroid/text/Spanned;)V

    .line 1403
    :cond_6
    iput-boolean v1, p0, Ldsl;->bIB:Z

    .line 1404
    if-eqz v0, :cond_8

    .line 1405
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {v0}, Lcom/google/android/search/searchplate/SearchPlate;->d(Lcom/google/android/search/searchplate/SearchPlate;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsk;

    invoke-interface {v0}, Ldsk;->afw()V

    goto :goto_1

    :cond_7
    move v0, v1

    .line 1378
    goto :goto_0

    .line 1408
    :cond_8
    return-void
.end method

.method public final afy()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1490
    iput-boolean v1, p0, Ldsl;->bIA:Z

    .line 1491
    invoke-virtual {p0}, Ldsl;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1492
    iput-boolean v2, p0, Ldsl;->bIz:Z

    .line 1497
    :goto_0
    return-void

    .line 1494
    :cond_0
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->dX(Z)V

    .line 1495
    iput-boolean v1, p0, Ldsl;->bIz:Z

    goto :goto_0
.end method

.method public final afz()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1500
    iput-boolean v1, p0, Ldsl;->bIz:Z

    .line 1501
    invoke-virtual {p0}, Ldsl;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1502
    iput-boolean v2, p0, Ldsl;->bIA:Z

    .line 1507
    :goto_0
    return-void

    .line 1504
    :cond_0
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/SearchPlate;->dW(Z)V

    .line 1505
    iput-boolean v1, p0, Ldsl;->bIA:Z

    goto :goto_0
.end method

.method public final aw(II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1449
    invoke-virtual {p0}, Ldsl;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1450
    iget-boolean v0, p0, Ldsl;->bIw:Z

    if-eqz v0, :cond_0

    .line 1453
    iput-boolean v1, p0, Ldsl;->bIy:Z

    .line 1455
    :cond_0
    iput-boolean v2, p0, Ldsl;->bIw:Z

    .line 1456
    iput p1, p0, Ldsl;->bIC:I

    .line 1457
    iput p2, p0, Ldsl;->bIE:I

    .line 1467
    :cond_1
    :goto_0
    return-void

    .line 1459
    :cond_2
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1, p2, v2, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(IIZZ)V

    .line 1460
    iput-boolean v1, p0, Ldsl;->bIw:Z

    .line 1461
    iget-boolean v0, p0, Ldsl;->bIy:Z

    if-eqz v0, :cond_1

    .line 1462
    iput-boolean v2, p0, Ldsl;->bIv:Z

    .line 1463
    iget v0, p0, Ldsl;->bIF:I

    iput v0, p0, Ldsl;->bID:I

    .line 1464
    iput-boolean v1, p0, Ldsl;->bIy:Z

    goto :goto_0
.end method

.method public final ea(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1520
    if-eqz p1, :cond_0

    .line 1521
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v5}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1522
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1523
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1524
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1525
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 1533
    :goto_0
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/search/searchplate/TextContainer;->ea(Z)V

    .line 1534
    iput-boolean p1, p0, Ldsl;->bIG:Z

    .line 1535
    return-void

    .line 1527
    :cond_0
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v5}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 1528
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 1529
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v4}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 1530
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 1531
    iget-object v0, p0, Ldsl;->Vs:Landroid/animation/LayoutTransition;

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    goto :goto_0
.end method

.method public final endTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1367
    iget v0, p0, Ldsl;->bIq:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ldsl;->bIq:I

    .line 1370
    invoke-virtual {p0}, Ldsl;->afx()V

    .line 1371
    return-void
.end method

.method public final gn(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1430
    invoke-virtual {p0}, Ldsl;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1431
    iget-boolean v0, p0, Ldsl;->bIw:Z

    if-eqz v0, :cond_0

    .line 1436
    iput-boolean v1, p0, Ldsl;->bIy:Z

    .line 1437
    iput p1, p0, Ldsl;->bIF:I

    .line 1446
    :goto_0
    return-void

    .line 1439
    :cond_0
    iput-boolean v1, p0, Ldsl;->bIv:Z

    .line 1440
    iput p1, p0, Ldsl;->bID:I

    goto :goto_0

    .line 1443
    :cond_1
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-static {v0, p1, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(Lcom/google/android/search/searchplate/SearchPlate;IZ)V

    .line 1444
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldsl;->bIv:Z

    goto :goto_0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 1424
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SearchPlate;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/LayoutTransition;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    iget-object v0, v0, Lcom/google/android/search/searchplate/SearchPlate;->bHG:Lcom/google/android/search/searchplate/TextContainer;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/TextContainer;->afM()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0}, Lcom/google/android/search/searchplate/SearchPlate;->isLayoutRequested()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(ILjava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1479
    invoke-virtual {p0}, Ldsl;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1480
    iput-boolean v1, p0, Ldsl;->bIx:Z

    .line 1481
    iput p1, p0, Ldsl;->bIs:I

    .line 1482
    iput-object p2, p0, Ldsl;->bIt:Ljava/lang/String;

    .line 1487
    :goto_0
    return-void

    .line 1484
    :cond_0
    iget-object v0, p0, Ldsl;->bIp:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/search/searchplate/SearchPlate;->a(ILjava/lang/String;Z)V

    .line 1485
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldsl;->bIx:Z

    goto :goto_0
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1570
    invoke-virtual {p0}, Ldsl;->afx()V

    .line 1571
    return-void
.end method

.method public final startTransition(Landroid/animation/LayoutTransition;Landroid/view/ViewGroup;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1360
    iget v0, p0, Ldsl;->bIq:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldsl;->bIq:I

    .line 1363
    return-void
.end method
