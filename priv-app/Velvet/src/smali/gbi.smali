.class public final Lgbi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cEK:Ljag;

.field public final crh:I


# direct methods
.method public constructor <init>(Ljag;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lgbi;->cEK:Ljag;

    .line 34
    iput p2, p0, Lgbi;->crh:I

    .line 35
    return-void
.end method

.method private static a(Ljaj;)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x3e8

    .line 152
    invoke-virtual {p0}, Ljaj;->bea()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p0}, Ljaj;->bdZ()J

    move-result-wide v0

    mul-long/2addr v0, v2

    .line 155
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljaj;->bdX()J

    move-result-wide v0

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method public static a(Ljae;J)Lgbi;
    .locals 13
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 58
    .line 63
    iget-object v4, p0, Ljae;->dWa:[Ljag;

    array-length v6, v4

    move v3, v5

    move-object v0, v2

    :goto_0
    if-ge v3, v6, :cond_9

    aget-object v1, v4, v3

    .line 64
    invoke-virtual {v1}, Ljag;->getStatusCode()I

    move-result v7

    const/4 v8, 0x5

    if-ne v7, v8, :cond_0

    move-object v3, v2

    move-object v4, v2

    .line 93
    :goto_1
    if-eqz v1, :cond_5

    .line 94
    new-instance v0, Lgbi;

    invoke-direct {v0, v1, v5}, Lgbi;-><init>(Ljag;I)V

    .line 110
    :goto_2
    return-object v0

    .line 68
    :cond_0
    iget-object v7, v1, Ljag;->dWg:Ljaj;

    if-eqz v7, :cond_4

    iget-object v7, v1, Ljag;->dWk:Ljaj;

    if-eqz v7, :cond_4

    .line 69
    iget-object v7, v1, Ljag;->dWg:Ljaj;

    invoke-static {v7}, Lgbi;->a(Ljaj;)J

    move-result-wide v8

    .line 73
    iget-object v7, v1, Ljag;->dWk:Ljaj;

    invoke-static {v7}, Lgbi;->a(Ljaj;)J

    move-result-wide v10

    .line 75
    cmp-long v7, v8, p1

    if-lez v7, :cond_1

    move-object v3, v2

    move-object v4, v1

    move-object v1, v2

    .line 77
    goto :goto_1

    .line 80
    :cond_1
    cmp-long v7, v8, p1

    if-gez v7, :cond_2

    cmp-long v7, v10, p1

    if-lez v7, :cond_2

    move-object v3, v1

    move-object v4, v2

    move-object v1, v2

    .line 82
    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {v1}, Ljag;->bdJ()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v1}, Ljag;->getStatusCode()I

    move-result v7

    if-eq v7, v12, :cond_3

    invoke-virtual {v1}, Ljag;->getStatusCode()I

    move-result v7

    const/4 v8, 0x4

    if-eq v7, v8, :cond_3

    invoke-virtual {v1}, Ljag;->getStatusCode()I

    move-result v7

    const/4 v8, 0x6

    if-ne v7, v8, :cond_4

    :cond_3
    move-object v0, v1

    .line 63
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 97
    :cond_5
    if-eqz v4, :cond_6

    .line 98
    new-instance v0, Lgbi;

    const/4 v1, 0x1

    invoke-direct {v0, v4, v1}, Lgbi;-><init>(Ljag;I)V

    goto :goto_2

    .line 102
    :cond_6
    if-eqz v3, :cond_7

    .line 103
    new-instance v0, Lgbi;

    const/4 v1, 0x2

    invoke-direct {v0, v3, v1}, Lgbi;-><init>(Ljag;I)V

    goto :goto_2

    .line 106
    :cond_7
    if-eqz v0, :cond_8

    .line 107
    new-instance v1, Lgbi;

    invoke-direct {v1, v0, v12}, Lgbi;-><init>(Ljag;I)V

    move-object v0, v1

    goto :goto_2

    :cond_8
    move-object v0, v2

    .line 110
    goto :goto_2

    :cond_9
    move-object v1, v2

    move-object v3, v2

    move-object v4, v2

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ILjaj;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 141
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 142
    invoke-virtual {p2}, Ljaj;->beb()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    .line 143
    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 144
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/util/Date;

    invoke-static {p2}, Lgbi;->a(Ljaj;)J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bo(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 120
    iget v1, p0, Lgbi;->crh:I

    packed-switch v1, :pswitch_data_0

    .line 136
    :goto_0
    :pswitch_0
    return-object v0

    .line 125
    :pswitch_1
    const v0, 0x7f0a0213

    iget-object v1, p0, Lgbi;->cEK:Ljag;

    iget-object v1, v1, Ljag;->dWg:Ljaj;

    invoke-static {p1, v0, v1}, Lgbi;->a(Landroid/content/Context;ILjaj;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 129
    :pswitch_2
    const v0, 0x7f0a0215

    iget-object v1, p0, Lgbi;->cEK:Ljag;

    iget-object v1, v1, Ljag;->dWk:Ljaj;

    invoke-static {p1, v0, v1}, Lgbi;->a(Landroid/content/Context;ILjaj;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 133
    :pswitch_3
    const v0, 0x7f0a0216

    iget-object v1, p0, Lgbi;->cEK:Ljag;

    iget-object v1, v1, Ljag;->dWk:Ljaj;

    invoke-static {p1, v0, v1}, Lgbi;->a(Landroid/content/Context;ILjaj;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
