.class public Ldcw;
.super Lddj;
.source "PG"


# instance fields
.field private final bqt:Lcom/google/android/shared/util/BitFlags;

.field private bra:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private btd:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bte:Z

.field private mCommittedQuery:Lcom/google/android/shared/search/Query;

.field private final mEventBus:Ldda;

.field private final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Ldda;Lhym;)V
    .locals 2

    .prologue
    .line 100
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 71
    new-instance v0, Lcom/google/android/search/core/state/TtsState$1;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/search/core/state/TtsState$1;-><init>(Ldcw;Ljava/lang/Class;)V

    iput-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 84
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 101
    iput-object p1, p0, Ldcw;->mEventBus:Ldda;

    .line 102
    iput-object p2, p0, Ldcw;->mVoiceSettings:Lhym;

    .line 103
    return-void
.end method

.method private Zn()Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const-wide/16 v8, 0x10

    .line 320
    iget-object v0, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v2

    .line 321
    iget-object v0, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 322
    iget-object v3, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aan()Ldcy;

    move-result-object v3

    .line 324
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 326
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    .line 359
    :cond_0
    :goto_0
    return v1

    .line 332
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v4

    .line 333
    iget-object v5, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v5}, Ldda;->aao()Ldcu;

    move-result-object v5

    invoke-virtual {v5}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v5

    .line 334
    if-eqz v4, :cond_2

    invoke-virtual {v5}, Lcom/google/android/search/shared/service/ClientConfig;->ani()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v4}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiR()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    iget-object v4, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v4}, Lcom/google/android/search/core/state/QueryState;->X(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v5}, Lcom/google/android/search/shared/service/ClientConfig;->anj()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    const/4 v0, 0x1

    .line 342
    :goto_1
    iget-object v4, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v4}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v0, :cond_0

    :cond_4
    invoke-direct {p0}, Ldcw;->Zu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    invoke-virtual {v2}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v0

    .line 345
    invoke-direct {p0}, Ldcw;->Zw()Z

    move-result v4

    .line 346
    iget-object v5, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x82

    invoke-virtual {v5, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 348
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 334
    goto :goto_1

    .line 349
    :cond_6
    iget-object v5, p0, Ldcw;->bra:[B

    if-eqz v5, :cond_0

    .line 350
    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aoP()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz v4, :cond_7

    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x4

    invoke-virtual {v0, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 352
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    goto :goto_0

    .line 353
    :cond_7
    invoke-virtual {v3}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ldbd;->VK()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez v4, :cond_0

    .line 355
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    goto/16 :goto_0
.end method

.method private Zt()V
    .locals 6

    .prologue
    .line 463
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    const-wide/16 v4, 0x40

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/shared/util/BitFlags;->e(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 464
    invoke-virtual {p0}, Ldcw;->notifyChanged()V

    .line 466
    :cond_0
    return-void
.end method

.method private Zu()Z
    .locals 4

    .prologue
    .line 472
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x60

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Zw()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 503
    iget-object v2, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v2

    iget-object v3, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v3}, Lcom/google/android/search/core/state/QueryState;->T(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 513
    :cond_0
    :goto_0
    return v0

    .line 509
    :cond_1
    iget-object v2, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    .line 510
    iget-object v3, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v3}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    .line 511
    goto :goto_0

    .line 513
    :cond_2
    invoke-virtual {v2}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/velvet/ActionData;->aII()Lieb;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static synthetic a(Ldcw;)Lcom/google/android/shared/util/BitFlags;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    return-object v0
.end method

.method public static synthetic b(Ldcw;)[B
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Ldcw;->bra:[B

    return-object v0
.end method

.method private u(Lcom/google/android/search/shared/actions/VoiceAction;)Z
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 490
    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Zm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Ldcw;->btd:Ljava/lang/String;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final Zo()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 372
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Ldcw;->Zu()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 373
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 375
    const/16 v0, 0xf1

    invoke-static {v0}, Lege;->ht(I)V

    .line 377
    const/4 v0, 0x1

    .line 397
    :goto_0
    iget-object v2, p0, Ldcw;->mVoiceSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aTM()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Ldcw;->mVoiceSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aTL()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v2

    if-nez v2, :cond_5

    .line 403
    :cond_0
    const/16 v0, 0xf2

    invoke-static {v0}, Lege;->ht(I)V

    .line 406
    invoke-direct {p0}, Ldcw;->Zt()V

    move v0, v1

    .line 410
    :cond_1
    :goto_1
    return v0

    .line 378
    :cond_2
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 380
    const/16 v0, 0x130

    invoke-static {v0}, Lege;->ht(I)V

    .line 383
    const/4 v0, 0x3

    goto :goto_0

    .line 386
    :cond_3
    const/16 v0, 0xf0

    invoke-static {v0}, Lege;->ht(I)V

    .line 388
    const/4 v0, 0x2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 392
    goto :goto_1

    .line 407
    :cond_5
    iget-object v1, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 408
    invoke-virtual {p0}, Ldcw;->notifyChanged()V

    goto :goto_1
.end method

.method public final Zp()[B
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Ldcw;->bra:[B

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public final Zq()V
    .locals 1

    .prologue
    .line 421
    iget-boolean v0, p0, Ldcw;->bte:Z

    if-nez v0, :cond_0

    .line 422
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldcw;->bte:Z

    .line 423
    invoke-virtual {p0}, Ldcw;->notifyChanged()V

    .line 425
    :cond_0
    return-void
.end method

.method public final Zr()V
    .locals 4

    .prologue
    .line 431
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 432
    invoke-virtual {p0}, Ldcw;->Zq()V

    .line 434
    :cond_0
    return-void
.end method

.method public final Zs()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 437
    iget-boolean v0, p0, Ldcw;->bte:Z

    if-eqz v0, :cond_0

    .line 438
    iget-boolean v0, p0, Ldcw;->bte:Z

    .line 439
    iput-boolean v1, p0, Ldcw;->bte:Z

    .line 442
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final Zv()Z
    .locals 4

    .prologue
    .line 476
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x8

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;Ljava/lang/String;ZZ)V
    .locals 6
    .param p1    # Lcom/google/android/search/shared/actions/VoiceAction;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0x8

    .line 242
    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Ldcw;->u(Lcom/google/android/search/shared/actions/VoiceAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    :cond_0
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x86

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    :cond_1
    :goto_0
    return-void

    .line 252
    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 254
    iput-object p2, p0, Ldcw;->btd:Ljava/lang/String;

    .line 255
    if-eqz p4, :cond_4

    .line 256
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 260
    :goto_1
    if-eqz p3, :cond_3

    .line 261
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 263
    :cond_3
    invoke-direct {p0}, Ldcw;->Zn()Z

    .line 264
    invoke-virtual {p0}, Ldcw;->notifyChanged()V

    goto :goto_0

    .line 258
    :cond_4
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    goto :goto_1

    .line 265
    :cond_5
    invoke-direct {p0}, Ldcw;->Zw()Z

    move-result v0

    if-nez v0, :cond_6

    .line 267
    invoke-direct {p0}, Ldcw;->Zt()V

    goto :goto_0

    .line 268
    :cond_6
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 270
    invoke-direct {p0}, Ldcw;->Zt()V

    goto :goto_0

    .line 273
    :cond_7
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 274
    if-eqz p3, :cond_8

    .line 275
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 277
    :cond_8
    invoke-direct {p0}, Ldcw;->Zn()Z

    .line 278
    invoke-virtual {p0}, Ldcw;->notifyChanged()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;[B)V
    .locals 4

    .prologue
    .line 291
    iget-object v0, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    iget-object v0, p0, Ldcw;->bra:[B

    if-nez v0, :cond_0

    .line 301
    iput-object p2, p0, Ldcw;->bra:[B

    .line 302
    invoke-direct {p0}, Ldcw;->Zn()Z

    .line 303
    invoke-virtual {p0}, Ldcw;->notifyChanged()V

    goto :goto_0
.end method

.method public final a(Lddb;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x4

    const-wide/16 v8, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 107
    .line 108
    iget-object v0, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v4

    .line 131
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4, v0}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 132
    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 133
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x20

    invoke-virtual {v0, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    iput-boolean v0, p0, Ldcw;->bte:Z

    .line 134
    iput-object v3, p0, Ldcw;->btd:Ljava/lang/String;

    .line 135
    iput-object v3, p0, Ldcw;->bra:[B

    .line 136
    iget-object v0, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->alf()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0}, Lcom/google/android/shared/util/BitFlags;->auz()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 146
    :goto_0
    invoke-virtual {p0}, Ldcw;->isDone()Z

    move-result v3

    if-nez v3, :cond_15

    .line 147
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v3

    if-eqz v3, :cond_12

    .line 148
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 149
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    iput-boolean v0, p0, Ldcw;->bte:Z

    .line 150
    invoke-direct {p0}, Ldcw;->Zt()V

    .line 229
    :cond_1
    :goto_1
    return-void

    .line 140
    :cond_2
    invoke-virtual {p0}, Ldcw;->isDone()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 141
    :goto_2
    iget-object v3, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v3}, Lcom/google/android/shared/util/BitFlags;->auz()Z

    .line 142
    iget-object v3, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x40

    invoke-virtual {v3, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    goto :goto_0

    :cond_3
    move v0, v2

    .line 140
    goto :goto_2

    .line 154
    :cond_4
    iget-object v3, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aaj()Ldbd;

    move-result-object v6

    .line 155
    iget-object v3, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4, v3}, Lcom/google/android/search/core/state/QueryState;->V(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Ldcw;->bra:[B

    if-nez v3, :cond_8

    .line 156
    iget-object v3, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v6, v3}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v6}, Ldbd;->yo()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v6}, Ldbd;->VK()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    iget-object v3, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v3, v10, v11}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-direct {p0}, Ldcw;->Zw()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 161
    :cond_6
    invoke-direct {p0}, Ldcw;->Zt()V

    goto :goto_1

    .line 169
    :cond_7
    iget-object v3, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v3, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v3

    or-int/2addr v0, v3

    .line 179
    :cond_8
    invoke-virtual {v6}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v3

    .line 181
    if-eqz v3, :cond_e

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_e

    move v5, v1

    .line 182
    :goto_3
    if-eqz v3, :cond_f

    invoke-virtual {v3}, Lcom/google/android/velvet/ActionData;->aoP()Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {v6}, Ldbd;->yo()Z

    move-result v3

    if-eqz v3, :cond_f

    move v3, v1

    .line 184
    :goto_4
    iget-object v7, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v7, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v7

    .line 185
    iget-object v8, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v4, v8}, Lcom/google/android/search/core/state/QueryState;->T(Lcom/google/android/shared/search/Query;)Z

    move-result v8

    .line 187
    invoke-virtual {v6}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v4

    .line 188
    invoke-virtual {v6}, Ldbd;->yo()Z

    move-result v9

    if-eqz v9, :cond_10

    if-eqz v4, :cond_10

    invoke-virtual {v6, v4}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alf()Z

    move-result v4

    if-nez v4, :cond_10

    move v4, v1

    .line 195
    :goto_5
    iget-object v9, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v9, v10, v11}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v9

    if-eqz v9, :cond_11

    if-nez v7, :cond_9

    if-nez v8, :cond_11

    .line 201
    :cond_9
    :goto_6
    iget-object v2, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v6, v2}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v2

    if-eqz v2, :cond_12

    if-eqz v5, :cond_a

    if-nez v7, :cond_d

    if-nez v8, :cond_d

    :cond_a
    if-eqz v3, :cond_c

    invoke-virtual {v6}, Ldbd;->VK()Z

    move-result v2

    if-nez v2, :cond_b

    if-nez v8, :cond_d

    :cond_b
    if-nez v1, :cond_d

    :cond_c
    if-eqz v4, :cond_12

    .line 207
    :cond_d
    invoke-direct {p0}, Ldcw;->Zt()V

    goto/16 :goto_1

    :cond_e
    move v5, v2

    .line 181
    goto :goto_3

    :cond_f
    move v3, v2

    .line 182
    goto :goto_4

    :cond_10
    move v4, v2

    .line 188
    goto :goto_5

    :cond_11
    move v1, v2

    .line 195
    goto :goto_6

    .line 212
    :cond_12
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 213
    iget-object v1, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aan()Ldcy;

    move-result-object v1

    .line 214
    invoke-virtual {v1}, Ldcy;->Zx()Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-virtual {p0}, Ldcw;->isDone()Z

    move-result v1

    if-nez v1, :cond_13

    .line 215
    invoke-virtual {p0}, Ldcw;->Zr()V

    goto/16 :goto_1

    .line 221
    :cond_13
    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v1

    if-nez v1, :cond_14

    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v1

    if-nez v1, :cond_14

    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 222
    :cond_14
    invoke-direct {p0}, Ldcw;->Zn()Z

    move-result v1

    or-int/2addr v0, v1

    .line 226
    :cond_15
    if-eqz v0, :cond_1

    .line 227
    invoke-virtual {p0}, Ldcw;->notifyChanged()V

    goto/16 :goto_1

    :cond_16
    move v0, v2

    goto/16 :goto_0
.end method

.method public final at(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Ldcw;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    invoke-direct {p0}, Ldcw;->Zt()V

    .line 457
    :cond_0
    return-void
.end method

.method public final isDone()Z
    .locals 4

    .prologue
    .line 446
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x40

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final isPlaying()Z
    .locals 4

    .prologue
    .line 450
    iget-object v0, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final t(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 284
    invoke-direct {p0, p1}, Ldcw;->u(Lcom/google/android/search/shared/actions/VoiceAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    invoke-direct {p0}, Ldcw;->Zt()V

    .line 288
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 496
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TtsState(query="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldcw;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldcw;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stopPending="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Ldcw;->bte:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", available-network="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Ldcw;->bra:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
