.class public Lcpo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final bfr:Ljava/util/Map;

.field private final bfs:Ljava/util/Map;

.field private bft:Ljava/util/Map;

.field private bfu:Ljava/lang/String;

.field private bfv:Z

.field private bfw:Z

.field private bfx:I

.field private bfy:Lgnu;

.field private synthetic bfz:Lcpn;

.field private final dz:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcpn;Landroid/net/Uri;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2670
    iput-object p1, p0, Lcpo;->bfz:Lcpn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcpo;->bfs:Ljava/util/Map;

    .line 2652
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcpo;->bfv:Z

    .line 2657
    iput-boolean v1, p0, Lcpo;->bfw:Z

    .line 2659
    iput v1, p0, Lcpo;->bfx:I

    .line 2671
    iput-object p2, p0, Lcpo;->dz:Landroid/net/Uri;

    .line 2672
    iget-object v0, p0, Lcpo;->dz:Landroid/net/Uri;

    invoke-static {v0}, Lcpn;->n(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcpo;->bfr:Ljava/util/Map;

    .line 2673
    return-void
.end method

.method constructor <init>(Lcpn;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2666
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcpo;-><init>(Lcpn;Landroid/net/Uri;)V

    .line 2667
    return-void
.end method

.method private static b(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2726
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2727
    invoke-interface {p0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2728
    if-eqz v0, :cond_0

    .line 2729
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2735
    const-string v1, "Search.SearchUrlHelper"

    const-string v2, "URL param or header written twice. Key: %s, old value: \"%s\", new value: \"%s\""

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x2

    aput-object p2, v3, v0

    const/4 v0, 0x5

    invoke-static {v0, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 2740
    :cond_0
    return-void
.end method


# virtual methods
.method public final RH()Lcom/google/android/search/shared/api/UriRequest;
    .locals 1

    .prologue
    .line 2676
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcpo;->a(Lcpw;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0
.end method

.method public final RI()Lcpo;
    .locals 3

    .prologue
    .line 2770
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    iget-object v0, v0, Lcpn;->bfp:Landroid/graphics/Point;

    .line 2771
    if-eqz v0, :cond_0

    .line 2773
    const-string v1, "biw"

    iget v2, v0, Landroid/graphics/Point;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2774
    const-string v1, "bih"

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2776
    :cond_0
    return-object p0
.end method

.method public final RJ()Lcpo;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2781
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v0}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Kc()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2788
    :goto_0
    return-object p0

    .line 2784
    :cond_0
    const-string v0, "padt"

    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v1}, Lcpn;->b(Lcpn;)Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v2}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v2

    invoke-static {v1, v2, v4, v4}, Lcpn;->a(Landroid/content/res/Resources;Lchk;ZZ)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2786
    const-string v0, "padb"

    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v1}, Lcpn;->b(Lcpn;)Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v2}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v4}, Lcpn;->a(Landroid/content/res/Resources;Lchk;ZZ)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final RK()Lcpo;
    .locals 1

    .prologue
    .line 2792
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcpo;->bfv:Z

    .line 2793
    return-object p0
.end method

.method public final RL()Lcpo;
    .locals 1

    .prologue
    .line 2797
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcpo;->bfw:Z

    .line 2798
    return-object p0
.end method

.method public final RM()Lcpo;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2802
    iget v0, p0, Lcpo;->bfx:I

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 2805
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    iget-object v0, v0, Lcpn;->mDiscourseContextHelper:Lcla;

    iget-object v3, v0, Lcla;->mGsaConfigFlags:Lchk;

    invoke-virtual {v3}, Lchk;->Kn()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, v0, Lcla;->baH:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcla;->mRecentContextApiClient:Lclh;

    invoke-virtual {v3}, Lclh;->connect()V

    iput-boolean v2, v0, Lcla;->baH:Z

    :cond_0
    iget-object v2, v0, Lcla;->baF:Ljava/util/concurrent/Future;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcla;->baF:Ljava/util/concurrent/Future;

    invoke-interface {v2}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, v0, Lcla;->mRecentContextApiClient:Lclh;

    invoke-virtual {v2}, Lclh;->Py()Ljava/util/concurrent/Future;

    move-result-object v2

    iput-object v2, v0, Lcla;->baF:Ljava/util/concurrent/Future;

    .line 2807
    :cond_2
    iput v1, p0, Lcpo;->bfx:I

    .line 2808
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    iget-object v0, v0, Lcpn;->mDiscourseContextHelper:Lcla;

    iget-object v0, v0, Lcla;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    iget-object v1, v0, Lcky;->bau:Lckz;

    if-nez v1, :cond_5

    const/4 v0, 0x0

    .line 2810
    :goto_1
    if-eqz v0, :cond_3

    .line 2811
    invoke-virtual {p0, v0}, Lcpo;->hQ(Ljava/lang/String;)Lcpo;

    .line 2813
    :cond_3
    return-object p0

    :cond_4
    move v0, v2

    .line 2802
    goto :goto_0

    .line 2808
    :cond_5
    iget-object v0, v0, Lcky;->bau:Lckz;

    iget-object v0, v0, Lckz;->baD:Ljava/lang/String;

    goto :goto_1
.end method

.method public final RN()Lcpo;
    .locals 1

    .prologue
    .line 2817
    iget v0, p0, Lcpo;->bfx:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 2818
    const/4 v0, 0x2

    iput v0, p0, Lcpo;->bfx:I

    .line 2819
    return-object p0

    .line 2817
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final RO()Lcpo;
    .locals 3

    .prologue
    .line 2836
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    iget-object v1, v0, Lcpn;->bfm:Ljava/text/SimpleDateFormat;

    monitor-enter v1

    .line 2837
    :try_start_0
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    iget-object v0, v0, Lcpn;->bfm:Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2838
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2839
    const-string v1, "Date"

    invoke-virtual {p0, v1, v0}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2840
    return-object p0

    .line 2838
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final RP()Lcpo;
    .locals 2

    .prologue
    .line 2854
    const-string v0, "dbla"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2856
    return-object p0
.end method

.method public final RQ()Lcpo;
    .locals 2

    .prologue
    .line 2909
    const-string v0, "pbx"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2910
    return-object p0
.end method

.method public final RR()Lcpo;
    .locals 2

    .prologue
    .line 2917
    const-string v0, "rf"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2918
    return-object p0
.end method

.method public final RS()Lcpo;
    .locals 2

    .prologue
    .line 2933
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v0}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->JY()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2934
    const-string v0, "nrc"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2936
    :cond_0
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v0}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->JZ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2937
    const-string v0, "nrcils"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2939
    :cond_1
    return-object p0
.end method

.method public final RT()V
    .locals 2

    .prologue
    .line 2976
    const-string v0, "tch"

    const-string v1, "6"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2977
    return-void
.end method

.method public final RU()Lcpo;
    .locals 2

    .prologue
    .line 3037
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    iget-object v0, v0, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ob()Ljava/lang/String;

    move-result-object v0

    .line 3038
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3039
    const-string v1, "X-Speech-Cookie"

    invoke-virtual {p0, v1, v0}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3041
    :cond_0
    return-object p0
.end method

.method public final RV()Lcpo;
    .locals 1

    .prologue
    .line 3055
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    invoke-virtual {v0}, Lcpn;->Ry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcpo;->ig(Ljava/lang/String;)Lcpo;

    move-result-object v0

    return-object v0
.end method

.method public final RW()Lcpo;
    .locals 2

    .prologue
    .line 3083
    const-string v0, "ctzn"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3084
    return-object p0
.end method

.method public final RX()Lcpo;
    .locals 2

    .prologue
    .line 3103
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    invoke-virtual {v0}, Lcpn;->RG()Ljava/lang/String;

    move-result-object v0

    .line 3104
    if-eqz v0, :cond_0

    .line 3105
    const-string v1, "agsad"

    invoke-virtual {p0, v1, v0}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3107
    :cond_0
    return-object p0
.end method

.method public final RY()Lcpo;
    .locals 2

    .prologue
    .line 3122
    const-string v0, "sla"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3123
    return-object p0
.end method

.method public final RZ()Lcpo;
    .locals 2

    .prologue
    .line 3156
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v0}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->IJ()Ljava/lang/String;

    move-result-object v0

    .line 3157
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3158
    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v1}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v1

    invoke-virtual {v1}, Lchk;->IK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3160
    :cond_0
    return-object p0
.end method

.method public final Sa()Lcpo;
    .locals 2

    .prologue
    .line 3172
    const-string v0, "xssi"

    const-string v1, "t"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3173
    return-object p0
.end method

.method public final Sb()Lcpo;
    .locals 2

    .prologue
    .line 3177
    const-string v0, "getexp"

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3178
    return-object p0
.end method

.method public final Sc()Lcom/google/android/search/shared/api/UriRequest;
    .locals 1

    .prologue
    .line 3188
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcpo;->a(Lcpw;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0
.end method

.method final U(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2744
    iget-object v0, p0, Lcpo;->bfr:Ljava/util/Map;

    invoke-static {v0, p1, p2}, Lcpo;->b(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 2745
    return-void
.end method

.method final V(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2753
    iget-object v0, p0, Lcpo;->bfs:Ljava/util/Map;

    invoke-static {v0, p1, p2}, Lcpo;->b(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 2754
    return-void
.end method

.method public final a(Lcpw;)Lcom/google/android/search/shared/api/UriRequest;
    .locals 5

    .prologue
    .line 2680
    iget-object v0, p0, Lcpo;->dz:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->clearQuery()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 2683
    iget-object v0, p0, Lcpo;->bft:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 2684
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    .line 2687
    iget-object v1, p0, Lcpo;->bft:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2688
    iget-object v1, p0, Lcpo;->bfr:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2692
    :goto_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2693
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    .line 2690
    :cond_0
    iget-object v0, p0, Lcpo;->bfr:Ljava/util/Map;

    goto :goto_0

    .line 2695
    :cond_1
    iget-object v0, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v2}, Lcpn;->a(Landroid/net/Uri$Builder;)V

    .line 2697
    iget-object v0, p0, Lcpo;->bfu:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2698
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2700
    iget-boolean v1, p0, Lcpo;->bfw:Z

    if-eqz v1, :cond_2

    .line 2701
    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    iget-object v2, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v2}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v2

    invoke-virtual {v2}, Lchk;->IZ()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcpo;->bfz:Lcpn;

    invoke-static {v3}, Lcpn;->a(Lcpn;)Lchk;

    move-result-object v3

    invoke-virtual {v3}, Lchk;->IY()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcpo;->bfs:Ljava/util/Map;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcpn;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2705
    :cond_2
    iget-boolean v1, p0, Lcpo;->bfv:Z

    if-eqz v1, :cond_3

    .line 2706
    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    iget-object v2, p0, Lcpo;->bfs:Ljava/util/Map;

    invoke-virtual {v1, v0, v2}, Lcpn;->a(Landroid/net/Uri;Ljava/util/Map;)V

    .line 2708
    :cond_3
    if-eqz p1, :cond_4

    .line 2709
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcpw;->in(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2715
    :cond_4
    iget v1, p0, Lcpo;->bfx:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    .line 2716
    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    iget-object v2, p0, Lcpo;->bfy:Lgnu;

    iget-object v3, p0, Lcpo;->bfs:Ljava/util/Map;

    invoke-virtual {v1, v2, v3}, Lcpn;->a(Lgnu;Ljava/util/Map;)V

    .line 2720
    :cond_5
    :goto_2
    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    iget-object v2, p0, Lcpo;->bfs:Ljava/util/Map;

    invoke-virtual {v1, v2}, Lcpn;->f(Ljava/util/Map;)V

    .line 2722
    new-instance v1, Lcom/google/android/search/shared/api/UriRequest;

    iget-object v2, p0, Lcpo;->bfs:Ljava/util/Map;

    invoke-direct {v1, v0, v2}, Lcom/google/android/search/shared/api/UriRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;)V

    return-object v1

    .line 2717
    :cond_6
    iget v1, p0, Lcpo;->bfx:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 2718
    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    iget-object v2, p0, Lcpo;->bfy:Lgnu;

    iget-object v3, p0, Lcpo;->bfs:Ljava/util/Map;

    invoke-virtual {v1, v2, v3}, Lcpn;->b(Lgnu;Ljava/util/Map;)V

    goto :goto_2
.end method

.method public final a(ZLandroid/location/Location;Landroid/location/Location;)Lcpo;
    .locals 2
    .param p2    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2949
    if-eqz p1, :cond_2

    .line 2951
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 2952
    :cond_0
    const-string v0, "action"

    const-string v1, "devloc"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2954
    const-string v0, "X-Geo"

    invoke-static {p2, p3}, Lcqg;->a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2962
    :cond_1
    :goto_0
    return-object p0

    .line 2960
    :cond_2
    const-string v0, "devloc"

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final ac(J)Lcpo;
    .locals 3

    .prologue
    .line 3071
    const-string v0, "qsubts"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3072
    return-object p0
.end method

.method public final ad(J)Lcpo;
    .locals 3

    .prologue
    .line 3165
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 3166
    const-string v0, "auto_exec_ms"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3168
    :cond_0
    return-object p0
.end method

.method public final cC(Z)Lcpo;
    .locals 2

    .prologue
    .line 2922
    const-string v1, "noj"

    if-eqz p1, :cond_0

    const-string v0, "0"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2924
    return-object p0

    .line 2922
    :cond_0
    const-string v0, "1"

    goto :goto_0
.end method

.method public final cD(Z)Lcpo;
    .locals 2

    .prologue
    .line 3077
    const-string v1, "fheit"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3079
    return-object p0

    .line 3077
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final cE(Z)Lcpo;
    .locals 2

    .prologue
    .line 3197
    const-string v1, "tel"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3198
    return-object p0

    .line 3197
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final e(Lgnu;)V
    .locals 0
    .param p1    # Lgnu;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2823
    if-eqz p1, :cond_0

    .line 2824
    iput-object p1, p0, Lcpo;->bfy:Lgnu;

    .line 2826
    :cond_0
    return-void
.end method

.method public final fj(I)Lcpo;
    .locals 2

    .prologue
    .line 2884
    const-string v0, "ved"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2885
    return-object p0
.end method

.method public final fk(I)Lcpo;
    .locals 2

    .prologue
    .line 2889
    const-string v0, "rcid"

    const/16 v1, 0x1f9f

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2890
    return-object p0
.end method

.method public final fl(I)Lcpo;
    .locals 2

    .prologue
    .line 3127
    const-string v0, "cp"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3128
    return-object p0
.end method

.method public final fm(I)Lcpo;
    .locals 2

    .prologue
    .line 3137
    const-string v0, "ech"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3138
    return-object p0
.end method

.method public final fn(I)Lcpo;
    .locals 2

    .prologue
    .line 3207
    const-string v0, "ntyp"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3208
    return-object p0
.end method

.method public final g(Ljava/util/Map;)Lcpo;
    .locals 0

    .prologue
    .line 2980
    iput-object p1, p0, Lcpo;->bft:Ljava/util/Map;

    .line 2981
    return-object p0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2629
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcpo;->a(Lcpw;)Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    return-object v0
.end method

.method public final hN(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2764
    const-string v0, "aqs"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2765
    return-object p0
.end method

.method public final hO(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2829
    const-string v0, "gcc"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2830
    return-object p0
.end method

.method public final hP(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2849
    const-string v0, "host"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2850
    return-object p0
.end method

.method public final hQ(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2879
    const-string v0, "ei"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2880
    return-object p0
.end method

.method public final hR(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2894
    const-string v0, "ttsm"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2895
    return-object p0
.end method

.method public final hS(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2899
    const-string v0, "Host"

    invoke-virtual {p0, v0, p1}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 2900
    return-object p0
.end method

.method public final hT(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2904
    const-string v0, "inm"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2905
    return-object p0
.end method

.method public final hU(Ljava/lang/String;)Lcpo;
    .locals 0

    .prologue
    .line 2928
    iput-object p1, p0, Lcpo;->bfu:Ljava/lang/String;

    .line 2929
    return-object p0
.end method

.method public final hV(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2966
    const-string v0, "stick"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2967
    return-object p0
.end method

.method public final hW(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2971
    const-string v0, "oq"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2972
    return-object p0
.end method

.method public final hX(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2985
    const-string v0, "pws"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2986
    return-object p0
.end method

.method public final hY(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2990
    const-string v0, "pf"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2991
    return-object p0
.end method

.method public final hZ(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 2995
    const-string v0, "q"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 2996
    return-object p0
.end method

.method public final i([Ljava/lang/String;)Lcpo;
    .locals 4

    .prologue
    .line 2872
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 2873
    aget-object v1, p1, v0

    add-int/lit8 v2, v0, 0x1

    aget-object v2, p1, v2

    iget-object v3, p0, Lcpo;->bfr:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2872
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 2875
    :cond_0
    return-object p0
.end method

.method public final ia(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3000
    const-string v0, "start"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3001
    return-object p0
.end method

.method public final ib(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3005
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3006
    const-string v0, "rlz"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3010
    :cond_0
    return-object p0
.end method

.method public final ic(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3014
    const-string v0, "spknlang"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3015
    return-object p0
.end method

.method public final id(Ljava/lang/String;)Lcpo;
    .locals 4

    .prologue
    .line 3019
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3020
    const-string v0, "entrypoint"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "android-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3024
    :goto_0
    return-object p0

    .line 3022
    :cond_0
    const-string v0, "Search.SearchUrlHelper"

    const-string v1, "Not setting entrypoint on url. This shouldn\'t happen."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final ie(Ljava/lang/String;)Lcpo;
    .locals 4

    .prologue
    .line 3028
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3029
    const-string v0, "source"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3033
    :goto_0
    return-object p0

    .line 3031
    :cond_0
    const-string v0, "Search.SearchUrlHelper"

    const-string v1, "Not setting source on url. This shouldn\'t happen."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final if(Ljava/lang/String;)Lcpo;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 3048
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3049
    const-string v0, "X-Speech-RequestId"

    invoke-virtual {p0, v0, p1}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3051
    :cond_0
    return-object p0
.end method

.method public final ig(Ljava/lang/String;)Lcpo;
    .locals 2

    .prologue
    .line 3062
    const-string v0, "redir_esc"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3063
    const-string v0, "client"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3064
    const-string v0, "hl"

    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    invoke-virtual {v1}, Lcpn;->Rn()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3065
    const-string v0, "safe"

    iget-object v1, p0, Lcpo;->bfz:Lcpn;

    iget-object v1, v1, Lcpn;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->NV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3066
    const-string v0, "oe"

    const-string v1, "utf-8"

    invoke-virtual {p0, v0, v1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3067
    return-object p0
.end method

.method public final ih(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3093
    const-string v0, "User-Agent"

    invoke-virtual {p0, v0, p1}, Lcpo;->V(Ljava/lang/String;Ljava/lang/String;)V

    .line 3094
    return-object p0
.end method

.method public final ii(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3098
    const-string v0, "v"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3099
    return-object p0
.end method

.method public final ij(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3117
    const-string v0, "sclient"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3118
    return-object p0
.end method

.method public final ik(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3132
    const-string v0, "psi"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3133
    return-object p0
.end method

.method public final il(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3142
    const-string v0, "gl"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3143
    return-object p0
.end method

.method public final im(Ljava/lang/String;)Lcpo;
    .locals 1

    .prologue
    .line 3147
    const-string v0, "oi"

    invoke-virtual {p0, v0, p1}, Lcpo;->U(Ljava/lang/String;Ljava/lang/String;)V

    .line 3148
    return-object p0
.end method
