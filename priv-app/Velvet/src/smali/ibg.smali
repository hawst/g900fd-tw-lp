.class public final Libg;
.super Libf;
.source "PG"


# instance fields
.field private final dwk:Libb;

.field private final dwl:Libb;

.field private final dwm:I


# direct methods
.method public constructor <init>(Libb;Libb;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Libf;-><init>()V

    .line 34
    iput-object p1, p0, Libg;->dwk:Libb;

    .line 35
    iput-object p2, p0, Libg;->dwl:Libb;

    .line 36
    iput p3, p0, Libg;->dwm:I

    .line 37
    return-void
.end method

.method private static b(Landroid/transition/TransitionValues;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 100
    instance-of v1, v0, Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 104
    :cond_0
    check-cast v0, Landroid/widget/TextView;

    .line 105
    iget-object v1, p0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-virtual {v0}, Landroid/widget/TextView;->getGravity()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/transition/TransitionValues;Ljava/lang/CharSequence;Landroid/transition/TransitionValues;Ljava/lang/CharSequence;)Landroid/animation/Animator;
    .locals 9

    .prologue
    .line 54
    iget-object v1, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 55
    instance-of v0, v1, Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x0

    .line 95
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 58
    check-cast v0, Landroid/widget/TextView;

    .line 62
    iget-object v2, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v3, v2

    .line 64
    :goto_1
    iget-object v2, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v4, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v4, "com:google:android:googlequicksearchbox:text:gravity"

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 67
    :goto_2
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 70
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 72
    iget v4, p0, Libg;->dwm:I

    iget-object v5, p0, Libg;->dwk:Libb;

    iget v5, v5, Libb;->factor:I

    mul-int/2addr v4, v5

    .line 73
    const-string v5, "translationY"

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v8

    aput v8, v6, v7

    const/4 v7, 0x1

    int-to-float v4, v4

    aput v4, v6, v7

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 75
    const-string v5, "alpha"

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v1

    aput v1, v6, v7

    const/4 v1, 0x1

    const/4 v7, 0x0

    aput v7, v6, v1

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 77
    new-instance v5, Libh;

    invoke-direct {v5, p0, v0, p4, v2}, Libh;-><init>(Libg;Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 86
    iget v2, p0, Libg;->dwm:I

    iget-object v5, p0, Libg;->dwl:Libb;

    iget v5, v5, Libb;->factor:I

    mul-int/2addr v2, v5

    .line 87
    const-string v5, "translationY"

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    int-to-float v2, v2

    aput v2, v6, v7

    const/4 v2, 0x1

    const/4 v7, 0x0

    aput v7, v6, v2

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 89
    const-string v5, "alpha"

    const/4 v6, 0x2

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 92
    const/4 v5, 0x2

    new-array v5, v5, [Landroid/animation/Animator;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-virtual {v3, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 93
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v2, v1, v5

    const/4 v5, 0x1

    aput-object v0, v1, v5

    invoke-virtual {v3, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 94
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/animation/Animator;

    const/4 v1, 0x0

    aput-object v4, v0, v1

    const/4 v1, 0x1

    aput-object v2, v0, v1

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    move-object v0, v3

    .line 95
    goto/16 :goto_0

    .line 62
    :cond_1
    const v2, 0x800003

    move v3, v2

    goto/16 :goto_1

    .line 64
    :cond_2
    const v2, 0x800003

    goto/16 :goto_2

    .line 89
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0, p1}, Libf;->captureEndValues(Landroid/transition/TransitionValues;)V

    .line 48
    invoke-static {p1}, Libg;->b(Landroid/transition/TransitionValues;)V

    .line 49
    return-void
.end method

.method public final captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 41
    invoke-super {p0, p1}, Libf;->captureStartValues(Landroid/transition/TransitionValues;)V

    .line 42
    invoke-static {p1}, Libg;->b(Landroid/transition/TransitionValues;)V

    .line 43
    return-void
.end method
