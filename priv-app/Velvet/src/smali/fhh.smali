.class final Lfhh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Ljava/lang/Runnable;


# instance fields
.field private final ceU:Landroid/view/View;

.field private final crA:Lfhn;

.field private final crE:Landroid/view/ViewGroup;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final crF:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lfhn;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0
    .param p3    # Landroid/view/ViewGroup;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    iput-object p1, p0, Lfhh;->crA:Lfhn;

    .line 331
    iput-object p2, p0, Lfhh;->ceU:Landroid/view/View;

    .line 332
    iput-object p3, p0, Lfhh;->crE:Landroid/view/ViewGroup;

    .line 333
    iput-object p4, p0, Lfhh;->crF:Landroid/view/View;

    .line 334
    return-void
.end method

.method public static a(Lfhn;Landroid/view/View;)Lfhh;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 343
    new-instance v0, Lfhh;

    invoke-direct {v0, p0, p1, v1, v1}, Lfhh;-><init>(Lfhn;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V

    return-object v0
.end method

.method public static a(Lfhn;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)Lfhh;
    .locals 1

    .prologue
    .line 356
    new-instance v0, Lfhh;

    invoke-direct {v0, p0, p1, p2, p3}, Lfhh;-><init>(Lfhn;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V

    return-object v0
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lfhh;->ceU:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 365
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 369
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 373
    return-void
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lfhh;->crE:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfhh;->crF:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lfhh;->crE:Landroid/view/ViewGroup;

    iget-object v1, p0, Lfhh;->crF:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 380
    :cond_0
    iget-object v1, p0, Lfhh;->crA:Lfhn;

    invoke-static {}, Lenu;->auR()V

    iget v0, v1, Lfhn;->crN:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iget v0, v1, Lfhn;->crN:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v1, Lfhn;->crN:I

    invoke-virtual {v1}, Lfhn;->azW()V

    .line 381
    return-void

    .line 380
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
