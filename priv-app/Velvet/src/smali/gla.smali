.class final Lgla;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdf;


# instance fields
.field private final aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final bhO:Lggg;


# direct methods
.method constructor <init>(Lggg;)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lgla;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 98
    iput-object p1, p0, Lgla;->bhO:Lggg;

    .line 99
    return-void
.end method


# virtual methods
.method public final b(Leie;)V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lgla;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lgla;->bhO:Lggg;

    invoke-interface {v0, p1}, Lggg;->a(Leiq;)V

    goto :goto_0
.end method

.method public final b(Ljww;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x2

    .line 103
    iget-object v0, p0, Lgla;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v5, p0, Lgla;->bhO:Lggg;

    sget-object v0, Ljxg;->eKr:Ljsm;

    invoke-virtual {p1, v0}, Ljww;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxg;

    if-nez v0, :cond_1

    new-instance v0, Lehv;

    invoke-direct {v0, v8, p1}, Lehv;-><init>(ILjww;)V

    :goto_1
    invoke-interface {v5, v0}, Lggg;->a(Lehu;)V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Ljxg;->eKs:Ljvv;

    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    iget-object v4, v0, Ljxg;->eKt:Ljvr;

    if-eqz v4, :cond_4

    move v4, v2

    :goto_3
    if-ne v1, v4, :cond_2

    const-string v1, "NetworkRecognitionEngine"

    const-string v4, "Invalid response. Expecting exactly one recognition or %s %s "

    new-array v6, v8, [Ljava/lang/Object;

    const-string v7, "endpointer event:"

    aput-object v7, v6, v3

    aput-object p1, v6, v2

    invoke-static {v1, v4, v6}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_2
    iget-object v1, v0, Ljxg;->eKt:Ljvr;

    if-eqz v1, :cond_5

    new-instance v1, Lehn;

    iget-object v0, v0, Ljxg;->eKt:Ljvr;

    invoke-direct {v1, v8, v0}, Lehn;-><init>(ILjvr;)V

    move-object v0, v1

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v4, v3

    goto :goto_3

    :cond_5
    new-instance v0, Lehv;

    invoke-direct {v0, v8, p1}, Lehv;-><init>(ILjww;)V

    goto :goto_1
.end method

.method public final c(Leie;)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lgla;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lgla;->bhO:Lggg;

    invoke-interface {v0, p1}, Lggg;->b(Leiq;)V

    goto :goto_0
.end method

.method final invalidate()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lgla;->aSX:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 150
    return-void
.end method
