.class public final Lcxa;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final bmg:Z

.field private final mActivity:Landroid/app/Activity;

.field private final mLocationSettings:Lcob;


# direct methods
.method public constructor <init>(Lcke;Landroid/app/Activity;Lcob;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 33
    iput-object p2, p0, Lcxa;->mActivity:Landroid/app/Activity;

    .line 34
    iput-object p3, p0, Lcxa;->mLocationSettings:Lcob;

    .line 35
    iget-object v0, p0, Lcxa;->mLocationSettings:Lcob;

    invoke-interface {v0}, Lcob;->QP()Z

    move-result v0

    iput-boolean v0, p0, Lcxa;->bmg:Z

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 41
    iget-boolean v0, p0, Lcxa;->bmg:Z

    if-eqz v0, :cond_0

    .line 42
    const v0, 0x7f0a05a7

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setTitle(I)V

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    const v0, 0x7f0a030c

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setTitle(I)V

    goto :goto_0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    iget-boolean v1, p0, Lcxa;->bmg:Z

    if-eqz v1, :cond_2

    .line 53
    iget-object v1, p0, Lcxa;->mLocationSettings:Lcob;

    invoke-interface {v1}, Lcob;->QQ()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54
    invoke-virtual {p1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.google.android.gms.location.settings.GOOGLE_LOCATION_SETTINGS"

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "com.google.android.gms"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {v1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_0
    if-eqz v0, :cond_0

    .line 67
    iget-object v1, p0, Lcxa;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 69
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 54
    :catch_0
    move-exception v1

    const-string v1, "GCoreLocationSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Problem while starting settings activity"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 57
    :cond_1
    iget-object v0, p0, Lcxa;->mLocationSettings:Lcob;

    invoke-virtual {p0}, Lcxa;->TL()Lcke;

    move-result-object v1

    invoke-interface {v1}, Lcke;->ND()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcob;->hv(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 62
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
