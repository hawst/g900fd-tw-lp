.class public Lbur;
.super Ljava/lang/Object;


# instance fields
.field public final aGS:Lbpd;

.field public aLd:Lcom/google/android/gms/internal/qt;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILbus;)V
    .locals 6

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, v3

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lbur;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lbus;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lbus;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, v3

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lbur;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lbus;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lbus;Z)V
    .locals 7

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/google/android/gms/internal/qt;

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/qt;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    new-instance v0, Lbpd;

    new-instance v1, Lbpb;

    invoke-direct {v1, p5}, Lbpb;-><init>(Lbus;)V

    invoke-direct {v0, p1, v1}, Lbpd;-><init>(Landroid/content/Context;Lbpb;)V

    iput-object v0, p0, Lbur;->aGS:Lbpd;

    return-void

    :catch_0
    move-exception v0

    const-string v0, "PlayLogger"

    const-string v3, "This can\'t happen."

    invoke-static {v0, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final varargs a(JLjava/lang/String;[B[Ljava/lang/String;)V
    .locals 9

    iget-object v0, p0, Lbur;->aGS:Lbpd;

    iget-object v7, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    new-instance v1, Lcom/google/android/gms/internal/qp;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/internal/qp;-><init>(JLjava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v0, v7, v1}, Lbpd;->a(Lcom/google/android/gms/internal/qt;Lcom/google/android/gms/internal/qp;)V

    return-void
.end method

.method public varargs a(Ljava/lang/String;[B[Ljava/lang/String;)V
    .locals 7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lbur;->a(JLjava/lang/String;[B[Ljava/lang/String;)V

    return-void
.end method

.method public final fH(Ljava/lang/String;)Lbur;
    .locals 7

    iget-object v0, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget-object v0, v0, Lcom/google/android/gms/internal/qt;->aGW:Ljava/lang/String;

    invoke-static {p1, v0}, Lbjp;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/qt;

    iget-object v1, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget-object v1, v1, Lcom/google/android/gms/internal/qt;->packageName:Ljava/lang/String;

    iget-object v2, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget v2, v2, Lcom/google/android/gms/internal/qt;->aGU:I

    iget-object v3, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget v3, v3, Lcom/google/android/gms/internal/qt;->aGV:I

    iget-object v4, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget-object v5, v4, Lcom/google/android/gms/internal/qt;->aGX:Ljava/lang/String;

    iget-object v4, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    iget-boolean v6, v4, Lcom/google/android/gms/internal/qt;->aGY:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/qt;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lbur;->aLd:Lcom/google/android/gms/internal/qt;

    :cond_0
    return-object p0
.end method
