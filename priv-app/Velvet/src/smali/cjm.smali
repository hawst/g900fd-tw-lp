.class final Lcjm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final aYG:Ljava/util/Set;

.field private final aYH:Ljava/util/Set;

.field final aYI:Ljava/lang/String;

.field private synthetic aYJ:Lcjl;


# direct methods
.method constructor <init>(Lcjl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 284
    iput-object p1, p0, Lcjm;->aYJ:Lcjl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    iput-object p3, p0, Lcjm;->aYI:Ljava/lang/String;

    .line 287
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcjm;->aYG:Ljava/util/Set;

    .line 288
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcjm;->aYH:Ljava/util/Set;

    .line 289
    const-string v0, ","

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 290
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 291
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcjm;->aYJ:Lcjl;

    iget-object v4, v4, Lcjl;->mRelationshipNameLookup:Leai;

    invoke-interface {v4, v3}, Leai;->kK(Ljava/lang/String;)Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcjm;->aYH:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 289
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 291
    :cond_1
    invoke-virtual {p0, v4}, Lcjm;->a(Lcom/google/android/search/shared/contact/Relationship;)Z

    goto :goto_1

    .line 295
    :cond_2
    return-void
.end method


# virtual methods
.method final KY()Ljava/lang/String;
    .locals 3

    .prologue
    .line 355
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 356
    iget-object v0, p0, Lcjm;->aYG:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Relationship;

    .line 357
    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/Relationship;->amq()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 359
    :cond_0
    iget-object v0, p0, Lcjm;->aYH:Ljava/util/Set;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 360
    const-string v0, ", "

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    invoke-virtual {v0, v1}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final a(Lcom/google/android/search/shared/contact/Relationship;)Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcjm;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcjm;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 335
    :cond_0
    iget-object v0, p0, Lcjm;->aYG:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 336
    const/4 v0, 0x1

    return v0
.end method
