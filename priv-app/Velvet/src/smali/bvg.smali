.class public final Lbvg;
.super Lbpt;


# instance fields
.field private final aLx:Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;)V
    .locals 1

    sget-object v0, Lbut;->aLg:Lbhc;

    invoke-direct {p0, v0}, Lbpt;-><init>(Lbhc;)V

    iput-object p1, p0, Lbvg;->aLx:Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;

    return-void
.end method

.method private a(Lbqc;)V
    .locals 4

    invoke-virtual {p1}, Lbqc;->Aa()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbqi;

    iget-object v1, p0, Lbvg;->aLx:Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;

    new-instance v2, Lbqe;

    const-class v3, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;

    invoke-direct {v2, p0, v3}, Lbqe;-><init>(Lbhh;Ljava/lang/Class;)V

    invoke-interface {v0, v1, v2}, Lbqi;->a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Request;Lbqf;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/Status;)Lbho;
    .locals 1

    new-instance v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;-><init>()V

    iput-object p1, v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$Response;->avd:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final bridge synthetic a(Lbha;)V
    .locals 0

    check-cast p1, Lbqc;

    invoke-direct {p0, p1}, Lbvg;->a(Lbqc;)V

    return-void
.end method

.method protected final bridge synthetic a(Lbps;)V
    .locals 0

    check-cast p1, Lbqc;

    invoke-direct {p0, p1}, Lbvg;->a(Lbqc;)V

    return-void
.end method
