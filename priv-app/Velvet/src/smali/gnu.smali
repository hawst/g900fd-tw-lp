.class public final Lgnu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private cQA:Ljava/lang/String;

.field private cQx:J

.field public cQy:Ljjz;

.field public cQz:Lgny;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 255
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 256
    const/4 v0, 0x0

    iput v0, p0, Lgnu;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgnu;->cQx:J

    iput-object v2, p0, Lgnu;->cQy:Ljjz;

    iput-object v2, p0, Lgnu;->cQz:Lgny;

    const-string v0, ""

    iput-object v0, p0, Lgnu;->cQA:Ljava/lang/String;

    iput-object v2, p0, Lgnu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lgnu;->eCz:I

    .line 257
    return-void
.end method

.method public static U([B)Lgnu;
    .locals 1

    .prologue
    .line 355
    new-instance v0, Lgnu;

    invoke-direct {v0}, Lgnu;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lgnu;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 189
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lgnu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lgnu;->cQx:J

    iget v0, p0, Lgnu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgnu;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgnu;->cQy:Ljjz;

    if-nez v0, :cond_1

    new-instance v0, Ljjz;

    invoke-direct {v0}, Ljjz;-><init>()V

    iput-object v0, p0, Lgnu;->cQy:Ljjz;

    :cond_1
    iget-object v0, p0, Lgnu;->cQy:Ljjz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgnu;->cQz:Lgny;

    if-nez v0, :cond_2

    new-instance v0, Lgny;

    invoke-direct {v0}, Lgny;-><init>()V

    iput-object v0, p0, Lgnu;->cQz:Lgny;

    :cond_2
    iget-object v0, p0, Lgnu;->cQz:Lgny;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgnu;->cQA:Ljava/lang/String;

    iget v0, p0, Lgnu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgnu;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 273
    iget v0, p0, Lgnu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 274
    const/4 v0, 0x1

    iget-wide v2, p0, Lgnu;->cQx:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 276
    :cond_0
    iget-object v0, p0, Lgnu;->cQy:Ljjz;

    if-eqz v0, :cond_1

    .line 277
    const/4 v0, 0x2

    iget-object v1, p0, Lgnu;->cQy:Ljjz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 279
    :cond_1
    iget-object v0, p0, Lgnu;->cQz:Lgny;

    if-eqz v0, :cond_2

    .line 280
    const/4 v0, 0x3

    iget-object v1, p0, Lgnu;->cQz:Lgny;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 282
    :cond_2
    iget v0, p0, Lgnu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 283
    const/4 v0, 0x4

    iget-object v1, p0, Lgnu;->cQA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 285
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 286
    return-void
.end method

.method public final aIb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lgnu;->cQA:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 290
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 291
    iget v1, p0, Lgnu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 292
    const/4 v1, 0x1

    iget-wide v2, p0, Lgnu;->cQx:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_0
    iget-object v1, p0, Lgnu;->cQy:Ljjz;

    if-eqz v1, :cond_1

    .line 296
    const/4 v1, 0x2

    iget-object v2, p0, Lgnu;->cQy:Ljjz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    :cond_1
    iget-object v1, p0, Lgnu;->cQz:Lgny;

    if-eqz v1, :cond_2

    .line 300
    const/4 v1, 0x3

    iget-object v2, p0, Lgnu;->cQz:Lgny;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    :cond_2
    iget v1, p0, Lgnu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 304
    const/4 v1, 0x4

    iget-object v2, p0, Lgnu;->cQA:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    :cond_3
    return v0
.end method

.method public final nl(Ljava/lang/String;)Lgnu;
    .locals 1

    .prologue
    .line 239
    if-nez p1, :cond_0

    .line 240
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 242
    :cond_0
    iput-object p1, p0, Lgnu;->cQA:Ljava/lang/String;

    .line 243
    iget v0, p0, Lgnu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lgnu;->aez:I

    .line 244
    return-object p0
.end method
