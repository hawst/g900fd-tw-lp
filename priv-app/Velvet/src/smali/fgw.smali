.class final Lfgw;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final cnB:I

.field private final crl:Z

.field private synthetic crm:Lfgu;


# direct methods
.method constructor <init>(Lfgu;IZ)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lfgw;->crm:Lfgu;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 295
    iput p2, p0, Lfgw;->cnB:I

    .line 296
    iput-boolean p3, p0, Lfgw;->crl:Z

    .line 297
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 284
    check-cast p1, [Ljbj;

    array-length v1, p1

    if-nez v1, :cond_0

    const-string v0, "SecondScreenEntryProvider"

    const-string v1, "Expected at least one interest"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lfgv;

    new-instance v1, Lizo;

    invoke-direct {v1}, Lizo;-><init>()V

    invoke-direct {v0, v4, v1, v5}, Lfgv;-><init>(ILizo;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lfgw;->crm:Lfgu;

    iget-object v1, v1, Lfgu;->mLocationOracle:Lfdr;

    const-string v2, "SecondScreenEntryProvider"

    invoke-interface {v1, v2}, Lfdr;->lK(Ljava/lang/String;)Lfdt;

    move-result-object v1

    invoke-interface {v1}, Lfdt;->acquire()V

    iget-object v2, p0, Lfgw;->crm:Lfgu;

    iget-object v2, v2, Lfgu;->mLocationOracle:Lfdr;

    invoke-interface {v2}, Lfdr;->ayA()Landroid/location/Location;

    move-result-object v2

    invoke-interface {v1}, Lfdt;->release()V

    new-instance v1, Lizm;

    invoke-direct {v1}, Lizm;-><init>()V

    iput-object p1, v1, Lizm;->dUH:[Ljbj;

    iget v3, p0, Lfgw;->cnB:I

    invoke-static {v3}, Lfjw;->iQ(I)Ljed;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljed;->hK(Z)Ljed;

    move-result-object v3

    iput-object v1, v3, Ljed;->edr:Lizm;

    iget-object v1, p0, Lfgw;->crm:Lfgu;

    iget-object v1, v1, Lfgu;->mNetworkClient:Lfcx;

    invoke-interface {v1, v3}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v3, v1, Ljeh;->aeC:Lizn;

    if-eqz v3, :cond_1

    iget-object v3, v1, Ljeh;->aeC:Lizn;

    iget-object v3, v3, Lizn;->dUI:[Lizo;

    array-length v3, v3

    if-lez v3, :cond_1

    iget-object v0, v1, Ljeh;->aeC:Lizn;

    iget-object v1, p0, Lfgw;->crm:Lfgu;

    iget-object v1, v1, Lfgu;->mEntryTreePruner:Lfbh;

    invoke-virtual {v1, v0}, Lfbh;->d(Lizn;)V

    iget-object v0, v0, Lizn;->dUI:[Lizo;

    aget-object v1, v0, v4

    new-instance v3, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-direct {v3, v2, v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;-><init>(Landroid/location/Location;Landroid/location/Location;)V

    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget-object v0, v0, Lfgu;->cpc:Lezt;

    invoke-virtual {v0, v3, v1}, Lezt;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizo;)V

    new-instance v0, Lfbg;

    invoke-direct {v0, v3}, Lfbg;-><init>(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    invoke-virtual {v0, v1}, Lfbg;->f(Lizo;)V

    new-instance v0, Lfgv;

    invoke-direct {v0, v4, v1, v3}, Lfgv;-><init>(ILizo;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    goto :goto_0

    :cond_1
    const-string v1, "SecondScreenEntryProvider"

    const-string v2, "Failed to retrieve entries"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lfgw;->crm:Lfgu;

    iget-object v1, v1, Lfgu;->mNetworkClient:Lfcx;

    invoke-interface {v1}, Lfcx;->awo()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x2

    :cond_2
    new-instance v1, Lfgv;

    invoke-direct {v1, v0, v5, v5}, Lfgv;-><init>(ILizo;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    move-object v0, v1

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 284
    check-cast p1, Lfgv;

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget v2, p1, Lfgv;->crh:I

    iput v2, v0, Lfgu;->crh:I

    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget v0, v0, Lfgu;->crh:I

    if-nez v0, :cond_2

    iget-object v0, p1, Lfgv;->crk:Lizo;

    iget-object v2, p0, Lfgw;->crm:Lfgu;

    iput-boolean v1, v2, Lfgu;->crj:Z

    if-eqz v0, :cond_0

    iget-object v2, v0, Lizo;->dUQ:Lizq;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lizo;->dUQ:Lizq;

    iget-object v2, v2, Lizq;->dUW:[Lizq;

    if-nez v2, :cond_3

    :cond_0
    iget-boolean v0, p0, Lfgw;->crl:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget-object v2, p1, Lfgv;->crk:Lizo;

    iput-object v2, v0, Lfgu;->cri:Lizo;

    :cond_1
    :goto_0
    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget-object v2, p1, Lfgv;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iput-object v2, v0, Lfgu;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    :cond_2
    iget-object v0, p0, Lfgw;->crm:Lfgu;

    const/4 v2, 0x0

    iput-object v2, v0, Lfgu;->crg:Lfgw;

    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget-object v0, v0, Lfgu;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Lfgx;

    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget-object v0, v0, Lfgu;->aWy:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_8

    aget-object v1, v2, v0

    iget-object v4, p0, Lfgw;->crm:Lfgu;

    iget v4, v4, Lfgu;->crh:I

    if-eqz v4, :cond_7

    iget-object v4, p0, Lfgw;->crm:Lfgu;

    iget v4, v4, Lfgu;->crh:I

    invoke-interface {v1}, Lfgx;->wg()V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, v0, Lizo;->dUQ:Lizq;

    iget-object v3, v0, Lizq;->dUW:[Lizq;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    iget-object v5, v0, Lizq;->dUX:[Lizj;

    if-eqz v5, :cond_5

    iget-object v5, v0, Lizq;->dUX:[Lizj;

    array-length v6, v5

    move v0, v1

    :goto_4
    if-ge v0, v6, :cond_5

    aget-object v7, v5, v0

    iget-object v7, v7, Lizj;->dTy:Ljef;

    if-eqz v7, :cond_4

    iget-object v8, p0, Lfgw;->crm:Lfgu;

    invoke-virtual {v7}, Ljef;->biv()Z

    move-result v7

    iput-boolean v7, v8, Lfgu;->crj:Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_6
    iget-object v0, p1, Lfgv;->crk:Lizo;

    iget-object v0, v0, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lfgv;->crk:Lizo;

    iget-object v0, v0, Lizo;->dUQ:Lizq;

    iget-object v0, v0, Lizq;->dUW:[Lizq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget-object v0, v0, Lfgu;->cri:Lizo;

    iget-object v2, v0, Lizo;->dUQ:Lizq;

    iget-object v0, p0, Lfgw;->crm:Lfgu;

    iget-object v0, v0, Lfgu;->cri:Lizo;

    iget-object v0, v0, Lizo;->dUQ:Lizq;

    iget-object v0, v0, Lizq;->dUW:[Lizq;

    iget-object v3, p1, Lfgv;->crk:Lizo;

    iget-object v3, v3, Lizo;->dUQ:Lizq;

    iget-object v3, v3, Lizq;->dUW:[Lizq;

    invoke-static {v0, v3}, Leqh;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizq;

    iput-object v0, v2, Lizq;->dUW:[Lizq;

    goto/16 :goto_0

    :cond_7
    iget-object v4, p1, Lfgv;->crk:Lizo;

    iget-boolean v5, p0, Lfgw;->crl:Z

    invoke-interface {v1, v4, v5}, Lfgx;->a(Lizo;Z)V

    goto :goto_2

    :cond_8
    return-void
.end method
