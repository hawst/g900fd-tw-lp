.class public final Laog;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private aim:Ljava/lang/String;

.field public ajr:[Ljava/lang/String;

.field private ajs:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8941
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 8942
    iput v1, p0, Laog;->aez:I

    const-string v0, ""

    iput-object v0, p0, Laog;->afh:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Laog;->ajr:[Ljava/lang/String;

    iput v1, p0, Laog;->ajs:I

    const-string v0, ""

    iput-object v0, p0, Laog;->aim:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Laog;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laog;->eCz:I

    .line 8943
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8850
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laog;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laog;->afh:Ljava/lang/String;

    iget v0, p0, Laog;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laog;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Laog;->ajr:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Laog;->ajr:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Laog;->ajr:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Laog;->ajr:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Laog;->ajs:I

    iget v0, p0, Laog;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laog;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laog;->aim:Ljava/lang/String;

    iget v0, p0, Laog;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laog;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 8959
    iget v0, p0, Laog;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8960
    const/4 v0, 0x1

    iget-object v1, p0, Laog;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8962
    :cond_0
    iget-object v0, p0, Laog;->ajr:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Laog;->ajr:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 8963
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Laog;->ajr:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 8964
    iget-object v1, p0, Laog;->ajr:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 8965
    if-eqz v1, :cond_1

    .line 8966
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8963
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8970
    :cond_2
    iget v0, p0, Laog;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 8971
    const/4 v0, 0x4

    iget v1, p0, Laog;->ajs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 8973
    :cond_3
    iget v0, p0, Laog;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 8974
    const/4 v0, 0x5

    iget-object v1, p0, Laog;->aim:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8976
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 8977
    return-void
.end method

.method public final bO(Ljava/lang/String;)Laog;
    .locals 1

    .prologue
    .line 8881
    if-nez p1, :cond_0

    .line 8882
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8884
    :cond_0
    iput-object p1, p0, Laog;->afh:Ljava/lang/String;

    .line 8885
    iget v0, p0, Laog;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laog;->aez:I

    .line 8886
    return-object p0
.end method

.method public final bP(Ljava/lang/String;)Laog;
    .locals 1

    .prologue
    .line 8925
    if-nez p1, :cond_0

    .line 8926
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8928
    :cond_0
    iput-object p1, p0, Laog;->aim:Ljava/lang/String;

    .line 8929
    iget v0, p0, Laog;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laog;->aez:I

    .line 8930
    return-object p0
.end method

.method public final cD(I)Laog;
    .locals 1

    .prologue
    .line 8906
    iput p1, p0, Laog;->ajs:I

    .line 8907
    iget v0, p0, Laog;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laog;->aez:I

    .line 8908
    return-object p0
.end method

.method public final getStatusCode()I
    .locals 1

    .prologue
    .line 8903
    iget v0, p0, Laog;->ajs:I

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8878
    iget-object v0, p0, Laog;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 8981
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 8982
    iget v2, p0, Laog;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 8983
    const/4 v2, 0x1

    iget-object v3, p0, Laog;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 8986
    :cond_0
    iget-object v2, p0, Laog;->ajr:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Laog;->ajr:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 8989
    :goto_0
    iget-object v4, p0, Laog;->ajr:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 8990
    iget-object v4, p0, Laog;->ajr:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 8991
    if-eqz v4, :cond_1

    .line 8992
    add-int/lit8 v3, v3, 0x1

    .line 8993
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 8989
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 8997
    :cond_2
    add-int/2addr v0, v2

    .line 8998
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 9000
    :cond_3
    iget v1, p0, Laog;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 9001
    const/4 v1, 0x4

    iget v2, p0, Laog;->ajs:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9004
    :cond_4
    iget v1, p0, Laog;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 9005
    const/4 v1, 0x5

    iget-object v2, p0, Laog;->aim:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9008
    :cond_5
    return v0
.end method

.method public final pB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8922
    iget-object v0, p0, Laog;->aim:Ljava/lang/String;

    return-object v0
.end method

.method public final pC()Z
    .locals 1

    .prologue
    .line 8933
    iget v0, p0, Laog;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 8889
    iget v0, p0, Laog;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
