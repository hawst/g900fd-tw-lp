.class public final Lcho;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcim;


# instance fields
.field private final RO:Ljava/lang/String;

.field private final aXc:Ldnb;

.field private final aXd:Lfaq;

.field private final aXe:Ljava/util/concurrent/Executor;

.field private final mCoreSearchServices:Lcfo;

.field final mIntentStarter:Leqp;

.field final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field final mServiceState:Ldcu;

.field final mWebViewWorker:Ldpk;


# direct methods
.method public constructor <init>(Lcfo;Ldda;Ldpk;Ldnb;Lfaq;Leqp;Ljava/lang/String;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcho;->mCoreSearchServices:Lcfo;

    .line 101
    invoke-virtual {p2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Lcho;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 102
    invoke-virtual {p2}, Ldda;->aao()Ldcu;

    move-result-object v0

    iput-object v0, p0, Lcho;->mServiceState:Ldcu;

    .line 103
    iput-object p3, p0, Lcho;->mWebViewWorker:Ldpk;

    .line 104
    iput-object p4, p0, Lcho;->aXc:Ldnb;

    .line 105
    iput-object p5, p0, Lcho;->aXd:Lfaq;

    .line 106
    iput-object p6, p0, Lcho;->mIntentStarter:Leqp;

    .line 107
    iput-object p7, p0, Lcho;->RO:Ljava/lang/String;

    .line 108
    iput-object p8, p0, Lcho;->aXe:Ljava/util/concurrent/Executor;

    .line 109
    return-void
.end method


# virtual methods
.method public final c(Lorg/json/JSONObject;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x1

    const-wide/16 v6, -0x1

    const/4 v1, 0x0

    .line 113
    const-string v0, "agsase"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "agsase"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcho;->aXc:Ldnb;

    invoke-virtual {v3, v0}, Ldnb;->jX(Ljava/lang/String;)V

    .line 116
    :cond_0
    const-string v0, "gsais"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    const-string v0, "gsais"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcho;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yb()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcho;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DV()Livq;

    move-result-object v3

    new-instance v4, Lchq;

    invoke-direct {v4, p0, v3, v0}, Lchq;-><init>(Lcho;Livq;Ljava/lang/String;)V

    iget-object v0, p0, Lcho;->aXe:Ljava/util/concurrent/Executor;

    invoke-interface {v3, v4, v0}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 119
    :cond_1
    const-string v0, "gsaim"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    const-string v0, "gsaim"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcho;->mServiceState:Ldcu;

    invoke-virtual {v3, v1}, Ldcu;->dr(Z)V

    iget-object v3, p0, Lcho;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yb()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcho;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->DV()Livq;

    move-result-object v3

    new-instance v4, Lchp;

    invoke-direct {v4, p0, v3, v0}, Lchp;-><init>(Lcho;Livq;Ljava/lang/String;)V

    iget-object v0, p0, Lcho;->aXe:Ljava/util/concurrent/Executor;

    invoke-interface {v3, v4, v0}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 122
    :cond_2
    const-string v0, "wobtm"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    const-string v0, "wobtm"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    iget-object v0, p0, Lcho;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v4

    if-eqz v3, :cond_b

    move v0, v1

    :goto_0
    invoke-interface {v4, v0}, Lcke;->fc(I)V

    iget-object v0, p0, Lcho;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DE()Lcxs;

    move-result-object v0

    invoke-virtual {v0}, Lcxs;->TH()Lcxi;

    move-result-object v4

    if-eqz v3, :cond_c

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lcxi;->fq(I)V

    iget-object v0, p0, Lcho;->aXd:Lfaq;

    invoke-virtual {v0}, Lfaq;->invalidate()V

    .line 125
    :cond_3
    const-string v0, "pre"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 126
    const-string v0, "pre"

    invoke-virtual {p1, v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcho;->mWebViewWorker:Ldpk;

    invoke-virtual {v3, v0}, Ldpk;->kd(Ljava/lang/String;)V

    .line 129
    :cond_4
    const-string v0, "gsafs"

    invoke-virtual {p1, v0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_5

    .line 130
    iget-object v0, p0, Lcho;->mWebViewWorker:Ldpk;

    iget-object v0, v0, Ldpk;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Ne()Ldyl;

    move-result-object v0

    invoke-interface {v0}, Ldyl;->UK()V

    .line 133
    :cond_5
    const-string v0, "eao"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 134
    iget-object v0, p0, Lcho;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v3, p0, Lcho;->aXc:Ldnb;

    invoke-virtual {v3}, Ldnb;->adC()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/search/core/state/QueryState;->ae(Lcom/google/android/shared/search/Query;)V

    .line 137
    :cond_6
    const-string v0, "agsafr"

    invoke-virtual {p1, v0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-nez v0, :cond_7

    .line 138
    const-string v0, "GsaJsEventHandler"

    const-string v3, "Received \'force_restart\' in SERP. Downloading a new config and restarting the app."

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcho;->mCoreSearchServices:Lcfo;

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v3, "send_gsa_home_request_then_crash"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v3, v4, v5}, Lgpp;->r(Ljava/lang/String;J)V

    .line 141
    :cond_7
    const-string v0, "gsalumap"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 142
    const-string v0, "gsalumap"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldkv;->jM(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v3, p0, Lcho;->mIntentStarter:Leqp;

    new-array v4, v2, [Landroid/content/Intent;

    aput-object v0, v4, v1

    invoke-interface {v3, v4}, Leqp;->b([Landroid/content/Intent;)Z

    .line 145
    :cond_8
    const-string v0, "gsagc"

    invoke-virtual {p1, v0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_9

    .line 147
    cmp-long v0, v4, v8

    if-nez v0, :cond_d

    move v0, v2

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting \'Use Google.com\' preference to: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcho;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BK()Lcke;

    move-result-object v3

    invoke-interface {v3}, Lcke;->Ny()Z

    move-result v3

    if-ne v3, v0, :cond_e

    const/16 v0, 0xec

    invoke-static {v0}, Lege;->ht(I)V

    .line 149
    :cond_9
    :goto_3
    const-string v0, "gsasa"

    invoke-virtual {p1, v0, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-eqz v0, :cond_a

    .line 150
    long-to-int v0, v4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Handling account selection: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    packed-switch v0, :pswitch_data_0

    const-string v3, "GsaJsEventHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown account selection event type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_4
    iget-object v3, p0, Lcho;->mIntentStarter:Leqp;

    new-array v2, v2, [Landroid/content/Intent;

    iget-object v4, p0, Lcho;->RO:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/google/android/search/core/preferences/PrivacyAndAccountFragment;->k(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-interface {v3, v2}, Leqp;->b([Landroid/content/Intent;)Z

    .line 152
    :cond_a
    return-void

    :cond_b
    move v0, v2

    .line 123
    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto/16 :goto_1

    :cond_d
    move v0, v1

    .line 147
    goto :goto_2

    :cond_e
    if-eqz v0, :cond_f

    const/16 v3, 0xeb

    :goto_5
    invoke-static {v3}, Lege;->ht(I)V

    iget-object v3, p0, Lcho;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BK()Lcke;

    move-result-object v3

    invoke-interface {v3, v0}, Lcke;->cn(Z)V

    goto :goto_3

    :cond_f
    const/16 v3, 0xea

    goto :goto_5

    .line 150
    :pswitch_0
    const/16 v0, 0xee

    invoke-static {v0}, Lege;->ht(I)V

    move v0, v1

    goto :goto_4

    :pswitch_1
    const/16 v0, 0xed

    invoke-static {v0}, Lege;->ht(I)V

    move v0, v1

    goto :goto_4

    :pswitch_2
    const/16 v0, 0x137

    invoke-static {v0}, Lege;->ht(I)V

    move v0, v2

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
