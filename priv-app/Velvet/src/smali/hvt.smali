.class public final Lhvt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhvu;


# static fields
.field private static DEBUG:Z


# instance fields
.field private dsp:Z

.field private dsq:Z

.field private dsr:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput-boolean v0, Lhvt;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhvt;->dsr:Ljava/util/Set;

    .line 40
    iget-object v0, p0, Lhvt;->dsr:Ljava/util/Set;

    const/16 v1, 0x2e

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v0, p0, Lhvt;->dsr:Ljava/util/Set;

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 42
    iget-object v0, p0, Lhvt;->dsr:Ljava/util/Set;

    const/16 v1, 0x3f

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    iget-object v0, p0, Lhvt;->dsr:Ljava/util/Set;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/inputmethod/ExtractedText;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 102
    iput-boolean v3, p0, Lhvt;->dsp:Z

    .line 103
    iput-boolean v2, p0, Lhvt;->dsq:Z

    .line 105
    if-nez p1, :cond_0

    .line 116
    :goto_0
    return-void

    .line 110
    :cond_0
    :try_start_0
    iget v0, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v1, p1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    add-int/2addr v0, v1

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    if-nez v0, :cond_4

    iget v0, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v1, p1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-lez v1, :cond_3

    iget-object v0, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v4, 0xa

    if-eq v0, v4, :cond_2

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    :goto_3
    if-eqz v0, :cond_3

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lhvt;->dsr:Ljava/util/Set;

    iget-object v4, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v4, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_6

    iget-object v0, p1, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    const/4 v1, 0x0

    iget v4, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v5, p1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    add-int/2addr v4, v5

    const-class v5, Landroid/text/Annotation;

    invoke-interface {v0, v1, v4, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/Annotation;

    array-length v5, v1

    move v4, v2

    :goto_4
    if-ge v4, v5, :cond_6

    aget-object v6, v1, v4

    invoke-interface {v0, v6}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    iget v8, p1, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iget v9, p1, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    add-int/2addr v8, v9

    if-ne v7, v8, :cond_5

    invoke-static {v6}, Lerr;->a(Landroid/text/Annotation;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v0, v3

    :goto_5
    if-eqz v0, :cond_7

    :cond_4
    move v0, v3

    :goto_6
    iput-boolean v0, p0, Lhvt;->dsq:Z
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    const v0, 0x60452c

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 114
    iput-boolean v2, p0, Lhvt;->dsq:Z

    goto/16 :goto_0

    .line 110
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_6
.end method

.method public final a(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/ExtractedText;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 49
    iget-boolean v0, p0, Lhvt;->dsp:Z

    if-nez v0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    if-eqz p2, :cond_0

    .line 60
    iget v0, p2, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    .line 63
    if-gez v0, :cond_2

    .line 69
    const-string v1, "LatinTextFormatter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid selection start :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 73
    :cond_2
    iget-object v1, p2, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 74
    iget-object v1, p2, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-nez v1, :cond_3

    .line 77
    const-string v1, " "

    invoke-interface {p1, v1, v3}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    .line 84
    :cond_3
    if-lez v0, :cond_4

    .line 85
    iget-object v1, p2, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-nez v1, :cond_4

    .line 88
    const-string v1, " "

    const/4 v2, 0x1

    invoke-interface {p1, v1, v2}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    .line 92
    add-int/lit8 v0, v0, 0x1

    .line 96
    :cond_4
    invoke-interface {p1, v0, v0}, Landroid/view/inputmethod/InputConnection;->setSelection(II)Z

    .line 97
    iput-boolean v3, p0, Lhvt;->dsp:Z

    goto :goto_0
.end method

.method public final b(Lcom/google/android/shared/speech/Hypothesis;)Lcom/google/android/shared/speech/Hypothesis;
    .locals 6

    .prologue
    .line 174
    invoke-virtual {p1}, Lcom/google/android/shared/speech/Hypothesis;->asY()I

    move-result v0

    .line 175
    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 176
    invoke-virtual {p1}, Lcom/google/android/shared/speech/Hypothesis;->asX()Lijj;

    move-result-object v0

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/speech/Hypothesis$Span;

    .line 177
    iget v1, v0, Lcom/google/android/shared/speech/Hypothesis$Span;->cai:I

    if-eqz v1, :cond_0

    :goto_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    iget-object v1, v0, Lcom/google/android/shared/speech/Hypothesis$Span;->cak:Lijj;

    invoke-virtual {v1}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lhvt;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    invoke-static {v4}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/speech/Hypothesis$Span;->a(Lijj;)Lcom/google/android/shared/speech/Hypothesis$Span;

    move-result-object v0

    goto :goto_1

    .line 180
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/shared/speech/Hypothesis;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhvt;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/shared/speech/Hypothesis;->a(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Lcom/google/android/shared/speech/Hypothesis;

    move-result-object v0

    return-object v0
.end method

.method public final format(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 162
    iget-boolean v0, p0, Lhvt;->dsq:Z

    if-nez v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final reset()V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvt;->dsq:Z

    .line 186
    return-void
.end method
