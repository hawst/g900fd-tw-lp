.class public abstract Lfra;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private bLG:I

.field private final cxS:Lixx;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cxU:Ljava/lang/Integer;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mCardContainer:Lfmt;

.field protected final mEntry:Lizj;


# direct methods
.method public constructor <init>(Lfmt;Lizj;I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 51
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lfra;-><init>(Lfmt;Lizj;ILixx;Ljava/lang/Integer;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Lfmt;Lizj;ILixx;)V
    .locals 6

    .prologue
    .line 61
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfra;-><init>(Lfmt;Lizj;ILixx;Ljava/lang/Integer;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Lfmt;Lizj;ILixx;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lfra;->mCardContainer:Lfmt;

    .line 73
    iput-object p2, p0, Lfra;->mEntry:Lizj;

    .line 74
    iput p3, p0, Lfra;->bLG:I

    .line 75
    iput-object p4, p0, Lfra;->cxS:Lixx;

    .line 76
    iput-object p5, p0, Lfra;->cxU:Ljava/lang/Integer;

    .line 77
    return-void
.end method


# virtual methods
.method protected abstract bj(Landroid/view/View;)V
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 83
    iget-object v0, p0, Lfra;->mEntry:Lizj;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lfra;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfra;->mEntry:Lizj;

    iget v2, p0, Lfra;->bLG:I

    iget-object v3, p0, Lfra;->cxS:Lixx;

    iget-object v4, p0, Lfra;->cxU:Ljava/lang/Integer;

    invoke-interface {v0, v1, v2, v3, v4}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    .line 88
    :cond_0
    invoke-virtual {p0, p1}, Lfra;->bj(Landroid/view/View;)V

    .line 89
    return-void
.end method
