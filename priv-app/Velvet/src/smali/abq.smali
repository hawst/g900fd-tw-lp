.class public final Labq;
.super Landroid/animation/Animator;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private EA:Ljava/util/ArrayList;

.field private OG:Landroid/animation/TimeInterpolator;

.field private OU:Ljava/util/EnumSet;

.field private OV:Landroid/view/ViewPropertyAnimator;

.field private OW:F

.field private OX:F

.field private OY:F

.field private OZ:J

.field private Pa:J

.field private Pb:Luv;

.field private hB:Landroid/view/View;

.field private lD:Z

.field private mAlpha:F


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/animation/Animator;-><init>()V

    .line 41
    const-class v0, Labr;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Labq;->lD:Z

    .line 59
    iput-object p1, p0, Labq;->hB:Landroid/view/View;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    .line 61
    return-void
.end method


# virtual methods
.method public final addListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 73
    :cond_0
    return-void
.end method

.method public final clone()Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Labq;->clone()Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public final end()V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getDuration()J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Labq;->Pa:J

    return-wide v0
.end method

.method public final getListeners()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getStartDelay()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Labq;->OZ:J

    return-wide v0
.end method

.method public final iS()Labq;
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pl:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 273
    return-object p0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Labq;->lD:Z

    return v0
.end method

.method public final isStarted()Z
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    move v1, v2

    :goto_0
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 103
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 104
    invoke-interface {v0, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 102
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 106
    :cond_0
    iput-boolean v2, p0, Labq;->lD:Z

    .line 107
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    move v1, v2

    :goto_0
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 112
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 113
    invoke-interface {v0, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 111
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 115
    :cond_0
    iput-boolean v2, p0, Labq;->lD:Z

    .line 116
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 120
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 121
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 122
    invoke-interface {v0, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    .line 120
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 124
    :cond_0
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Labq;->Pb:Luv;

    invoke-virtual {v0, p1}, Luv;->onAnimationStart(Landroid/animation/Animator;)V

    .line 132
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 133
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator$AnimatorListener;

    .line 134
    invoke-interface {v0, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 132
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 136
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Labq;->lD:Z

    .line 137
    return-void
.end method

.method public final q(F)Labq;
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pd:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 243
    iput p1, p0, Labq;->OW:F

    .line 244
    return-object p0
.end method

.method public final r(F)Labq;
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pe:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 249
    iput p1, p0, Labq;->OX:F

    .line 250
    return-object p0
.end method

.method public final removeAllListeners()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 152
    return-void
.end method

.method public final removeListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Labq;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 157
    return-void
.end method

.method public final s(F)Labq;
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pf:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 255
    iput p1, p0, Labq;->OY:F

    .line 256
    return-object p0
.end method

.method public final setDuration(J)Landroid/animation/Animator;
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pj:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 162
    iput-wide p1, p0, Labq;->Pa:J

    .line 163
    return-object p0
.end method

.method public final setInterpolator(Landroid/animation/TimeInterpolator;)V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pk:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 169
    iput-object p1, p0, Labq;->OG:Landroid/animation/TimeInterpolator;

    .line 170
    return-void
.end method

.method public final setStartDelay(J)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pi:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 175
    iput-wide p1, p0, Labq;->OZ:J

    .line 176
    return-void
.end method

.method public final setTarget(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setupEndValues()V
    .locals 0

    .prologue
    .line 186
    return-void
.end method

.method public final setupStartValues()V
    .locals 0

    .prologue
    .line 190
    return-void
.end method

.method public final start()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 194
    iget-object v0, p0, Labq;->hB:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    .line 198
    new-instance v0, Luv;

    iget-object v1, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    iget-object v2, p0, Labq;->hB:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Luv;-><init>(Landroid/view/ViewPropertyAnimator;Landroid/view/View;)V

    iput-object v0, p0, Labq;->Pb:Luv;

    .line 200
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pc:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 203
    :cond_0
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pd:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Labq;->OW:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 206
    :cond_1
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pe:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Labq;->OX:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    .line 209
    :cond_2
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pg:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 210
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    .line 212
    :cond_3
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pf:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 213
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Labq;->OY:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    .line 215
    :cond_4
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Ph:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 216
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    iget v1, p0, Labq;->mAlpha:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 218
    :cond_5
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pi:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 219
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    iget-wide v2, p0, Labq;->OZ:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 221
    :cond_6
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pj:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 222
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    iget-wide v2, p0, Labq;->Pa:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 224
    :cond_7
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pk:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 225
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    iget-object v1, p0, Labq;->OG:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 227
    :cond_8
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Pl:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 228
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 230
    :cond_9
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 231
    iget-object v0, p0, Labq;->OV:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 232
    invoke-static {p0}, Lyr;->a(Landroid/animation/Animator;)V

    .line 233
    return-void
.end method

.method public final t(F)Labq;
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Labq;->OU:Ljava/util/EnumSet;

    sget-object v1, Labr;->Ph:Labr;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 267
    iput p1, p0, Labq;->mAlpha:F

    .line 268
    return-object p0
.end method
