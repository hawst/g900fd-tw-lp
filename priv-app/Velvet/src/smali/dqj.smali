.class final Ldqj;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private synthetic bFn:Ldqf;


# direct methods
.method constructor <init>(Ldqf;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Ldqj;->bFn:Ldqf;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs ko()Ljava/lang/Void;
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Ldqj;->bFn:Ldqf;

    iget-object v0, v0, Ldqf;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    const-string v0, "BUTTON_PRESS"

    const-string v1, "GET_GOOGLE_NOW_PROMO_DISMISS"

    invoke-static {v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->aJ(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v0

    .line 116
    iget-object v1, p0, Ldqj;->bFn:Ldqf;

    iget-object v1, v1, Ldqf;->mNowRemoteClient:Lfml;

    invoke-virtual {v1, v0}, Lfml;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    .line 119
    :try_start_0
    iget-object v0, p0, Ldqj;->bFn:Ldqf;

    iget-object v0, v0, Ldqf;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->azF()V

    .line 120
    iget-object v0, p0, Ldqj;->bFn:Ldqf;

    iget-object v0, v0, Ldqf;->aPu:Lenz;

    invoke-virtual {v0}, Lenz;->acJ()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    iget-object v0, p0, Ldqj;->bFn:Ldqf;

    invoke-virtual {v0}, Ldqf;->aeH()V

    .line 128
    const/4 v0, 0x0

    return-object v0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    const-string v1, "GoogleNowPromoControllerImpl"

    const-string v2, "Failed to record dismiss of Now promo"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 125
    :cond_0
    const-string v0, "GoogleNowPromoControllerImpl"

    const-string v1, "Service disconnected before we could log promo dismiss"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ldqj;->ko()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
