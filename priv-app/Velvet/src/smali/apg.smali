.class public final Lapg;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private aiC:Ljava/lang/String;

.field private alg:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9313
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 9314
    iput v0, p0, Lapg;->aez:I

    iput v0, p0, Lapg;->alg:I

    const-string v0, ""

    iput-object v0, p0, Lapg;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lapg;->aiC:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lapg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lapg;->eCz:I

    .line 9315
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 9231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lapg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Lapg;->alg:I

    iget v0, p0, Lapg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapg;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapg;->afh:Ljava/lang/String;

    iget v0, p0, Lapg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapg;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapg;->aiC:Ljava/lang/String;

    iget v0, p0, Lapg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lapg;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 9330
    iget v0, p0, Lapg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9331
    const/4 v0, 0x1

    iget v1, p0, Lapg;->alg:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 9333
    :cond_0
    iget v0, p0, Lapg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 9334
    const/4 v0, 0x2

    iget-object v1, p0, Lapg;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9336
    :cond_1
    iget v0, p0, Lapg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 9337
    const/4 v0, 0x3

    iget-object v1, p0, Lapg;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9339
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 9340
    return-void
.end method

.method public final cR(I)Lapg;
    .locals 1

    .prologue
    .line 9256
    iput p1, p0, Lapg;->alg:I

    .line 9257
    iget v0, p0, Lapg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapg;->aez:I

    .line 9258
    return-object p0
.end method

.method public final dd(Ljava/lang/String;)Lapg;
    .locals 1

    .prologue
    .line 9275
    if-nez p1, :cond_0

    .line 9276
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9278
    :cond_0
    iput-object p1, p0, Lapg;->afh:Ljava/lang/String;

    .line 9279
    iget v0, p0, Lapg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapg;->aez:I

    .line 9280
    return-object p0
.end method

.method public final de(Ljava/lang/String;)Lapg;
    .locals 1

    .prologue
    .line 9297
    if-nez p1, :cond_0

    .line 9298
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9300
    :cond_0
    iput-object p1, p0, Lapg;->aiC:Ljava/lang/String;

    .line 9301
    iget v0, p0, Lapg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lapg;->aez:I

    .line 9302
    return-object p0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9294
    iget-object v0, p0, Lapg;->aiC:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9272
    iget-object v0, p0, Lapg;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final hasText()Z
    .locals 1

    .prologue
    .line 9305
    iget v0, p0, Lapg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 9344
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 9345
    iget v1, p0, Lapg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9346
    const/4 v1, 0x1

    iget v2, p0, Lapg;->alg:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 9349
    :cond_0
    iget v1, p0, Lapg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 9350
    const/4 v1, 0x2

    iget-object v2, p0, Lapg;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9353
    :cond_1
    iget v1, p0, Lapg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 9354
    const/4 v1, 0x3

    iget-object v2, p0, Lapg;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9357
    :cond_2
    return v0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 9283
    iget v0, p0, Lapg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final sJ()I
    .locals 1

    .prologue
    .line 9253
    iget v0, p0, Lapg;->alg:I

    return v0
.end method

.method public final sK()Z
    .locals 1

    .prologue
    .line 9261
    iget v0, p0, Lapg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
