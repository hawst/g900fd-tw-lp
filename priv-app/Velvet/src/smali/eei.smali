.class public final Leei;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bWC:Landroid/content/ComponentName;

.field private final bWD:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Leei;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Leei;->bWC:Landroid/content/ComponentName;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Leei;->bWD:Z

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Leei;->mContext:Landroid/content/Context;

    .line 28
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.voiceinteraction.GsaVoiceInteractionService"

    invoke-direct {v0, p1, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Leei;->bWC:Landroid/content/ComponentName;

    .line 30
    iput-boolean p2, p0, Leei;->bWD:Z

    .line 31
    return-void
.end method


# virtual methods
.method public final aob()Z
    .locals 2

    .prologue
    .line 45
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Leei;->bWD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leei;->mContext:Landroid/content/Context;

    iget-object v1, p0, Leei;->bWC:Landroid/content/ComponentName;

    invoke-static {v0, v1}, Landroid/service/voice/VoiceInteractionService;->isActiveService(Landroid/content/Context;Landroid/content/ComponentName;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    .line 55
    invoke-virtual {p0}, Leei;->aob()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    const-string v1, "voice_intent"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 58
    iget-object v1, p0, Leei;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.voiceinteraction.START_VOICE_INTERACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Leei;->bWC:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 61
    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
