.class public final Ljfn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public eiD:[Ljfo;

.field private eiE:Z

.field private eiF:Z

.field private eiG:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15271
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15272
    iput v1, p0, Ljfn;->aez:I

    invoke-static {}, Ljfo;->bjD()[Ljfo;

    move-result-object v0

    iput-object v0, p0, Ljfn;->eiD:[Ljfo;

    iput-boolean v1, p0, Ljfn;->eiE:Z

    iput-boolean v1, p0, Ljfn;->eiF:Z

    iput-boolean v1, p0, Ljfn;->eiG:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ljfn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljfn;->eCz:I

    .line 15273
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 14913
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljfn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljfn;->eiD:[Ljfo;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljfo;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljfn;->eiD:[Ljfo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljfo;

    invoke-direct {v3}, Ljfo;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljfn;->eiD:[Ljfo;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljfo;

    invoke-direct {v3}, Ljfo;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljfn;->eiD:[Ljfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfn;->eiE:Z

    iget v0, p0, Ljfn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfn;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfn;->eiF:Z

    iget v0, p0, Ljfn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfn;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljfn;->eiG:Z

    iget v0, p0, Ljfn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfn;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 15289
    iget-object v0, p0, Ljfn;->eiD:[Ljfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljfn;->eiD:[Ljfo;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 15290
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljfn;->eiD:[Ljfo;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 15291
    iget-object v1, p0, Ljfn;->eiD:[Ljfo;

    aget-object v1, v1, v0

    .line 15292
    if-eqz v1, :cond_0

    .line 15293
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 15290
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15297
    :cond_1
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 15298
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljfn;->eiE:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 15300
    :cond_2
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 15301
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljfn;->eiF:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 15303
    :cond_3
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 15304
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljfn;->eiG:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 15306
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 15307
    return-void
.end method

.method public final bjA()Z
    .locals 1

    .prologue
    .line 15244
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bjB()Z
    .locals 1

    .prologue
    .line 15255
    iget-boolean v0, p0, Ljfn;->eiG:Z

    return v0
.end method

.method public final bjC()Z
    .locals 1

    .prologue
    .line 15263
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bjx()Z
    .locals 1

    .prologue
    .line 15217
    iget-boolean v0, p0, Ljfn;->eiE:Z

    return v0
.end method

.method public final bjy()Z
    .locals 1

    .prologue
    .line 15225
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bjz()Z
    .locals 1

    .prologue
    .line 15236
    iget-boolean v0, p0, Ljfn;->eiF:Z

    return v0
.end method

.method public final ic(Z)Ljfn;
    .locals 1

    .prologue
    .line 15220
    iput-boolean p1, p0, Ljfn;->eiE:Z

    .line 15221
    iget v0, p0, Ljfn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljfn;->aez:I

    .line 15222
    return-object p0
.end method

.method public final id(Z)Ljfn;
    .locals 1

    .prologue
    .line 15239
    iput-boolean p1, p0, Ljfn;->eiF:Z

    .line 15240
    iget v0, p0, Ljfn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljfn;->aez:I

    .line 15241
    return-object p0
.end method

.method public final ie(Z)Ljfn;
    .locals 1

    .prologue
    .line 15258
    iput-boolean p1, p0, Ljfn;->eiG:Z

    .line 15259
    iget v0, p0, Ljfn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljfn;->aez:I

    .line 15260
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 15311
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 15312
    iget-object v0, p0, Ljfn;->eiD:[Ljfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljfn;->eiD:[Ljfo;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 15313
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljfn;->eiD:[Ljfo;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 15314
    iget-object v2, p0, Ljfn;->eiD:[Ljfo;

    aget-object v2, v2, v0

    .line 15315
    if-eqz v2, :cond_0

    .line 15316
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 15313
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15321
    :cond_1
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 15322
    const/4 v0, 0x3

    iget-boolean v2, p0, Ljfn;->eiE:Z

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 15325
    :cond_2
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 15326
    const/4 v0, 0x4

    iget-boolean v2, p0, Ljfn;->eiF:Z

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 15329
    :cond_3
    iget v0, p0, Ljfn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 15330
    const/4 v0, 0x5

    iget-boolean v2, p0, Ljfn;->eiG:Z

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 15333
    :cond_4
    return v1
.end method
