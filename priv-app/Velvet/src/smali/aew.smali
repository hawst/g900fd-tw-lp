.class public final Laew;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field UD:Lcom/android/launcher3/MemoryTracker;

.field mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Laew;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 119
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    new-instance v0, Laex;

    invoke-direct {v0, p0}, Laex;-><init>(Laew;)V

    iput-object v0, p0, Laew;->mHandler:Landroid/os/Handler;

    .line 89
    new-instance v0, Laey;

    invoke-direct {v0, p0}, Laey;-><init>(Laew;)V

    .line 99
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/launcher3/MemoryTracker;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v1, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 102
    invoke-virtual {p0, v3}, Laew;->setOrientation(I)V

    .line 104
    const/high16 v0, -0x40000000    # -2.0f

    invoke-virtual {p0, v0}, Laew;->setBackgroundColor(I)V

    .line 105
    return-void
.end method

.method static a([II)I
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 51
    aget v1, p0, v0

    if-ne v1, p1, :cond_0

    .line 53
    :goto_1
    return v0

    .line 50
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic a(Laew;)Lcom/android/launcher3/MemoryTracker;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Laew;->UD:Lcom/android/launcher3/MemoryTracker;

    return-object v0
.end method

.method static synthetic a(Laew;Lcom/android/launcher3/MemoryTracker;)Lcom/android/launcher3/MemoryTracker;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Laew;->UD:Lcom/android/launcher3/MemoryTracker;

    return-object p1
.end method


# virtual methods
.method public final kD()V
    .locals 5

    .prologue
    .line 108
    invoke-virtual {p0}, Laew;->removeAllViews()V

    .line 109
    iget-object v0, p0, Laew;->UD:Lcom/android/launcher3/MemoryTracker;

    invoke-virtual {v0}, Lcom/android/launcher3/MemoryTracker;->iU()[I

    move-result-object v1

    .line 110
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 111
    new-instance v2, Laez;

    invoke-virtual {p0}, Laew;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Laez;-><init>(Laew;Landroid/content/Context;)V

    .line 112
    aget v3, v1, v0

    iput v3, v2, Laez;->UH:I

    iget-object v3, v2, Laez;->UE:Laew;

    iget-object v3, v3, Laew;->UD:Lcom/android/launcher3/MemoryTracker;

    iget v4, v2, Laez;->UH:I

    invoke-virtual {v3, v4}, Lcom/android/launcher3/MemoryTracker;->bq(I)Lacd;

    move-result-object v3

    iput-object v3, v2, Laez;->UI:Lacd;

    iget-object v3, v2, Laez;->UI:Lacd;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Missing info for pid "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v2, Laez;->UH:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", removing view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Laez;->UE:Laew;

    invoke-virtual {v3}, Laew;->kD()V

    .line 113
    :cond_0
    invoke-virtual {p0, v2}, Laew;->addView(Landroid/view/View;)V

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_1
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 124
    iget-object v0, p0, Laew;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 125
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 130
    iget-object v0, p0, Laew;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 131
    return-void
.end method
