.class public final Ljbb;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dXH:[Ljbb;


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private agq:Ljava/lang/String;

.field public dXI:Ljcn;

.field public dXJ:Ljcn;

.field public dXK:Ljcn;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59606
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 59607
    const/4 v0, 0x0

    iput v0, p0, Ljbb;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbb;->agq:Ljava/lang/String;

    iput-object v1, p0, Ljbb;->aeB:Ljbp;

    iput-object v1, p0, Ljbb;->dXI:Ljcn;

    iput-object v1, p0, Ljbb;->dXJ:Ljcn;

    iput-object v1, p0, Ljbb;->dXK:Ljcn;

    iput-object v1, p0, Ljbb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbb;->eCz:I

    .line 59608
    return-void
.end method

.method public static beG()[Ljbb;
    .locals 2

    .prologue
    .line 59559
    sget-object v0, Ljbb;->dXH:[Ljbb;

    if-nez v0, :cond_1

    .line 59560
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 59562
    :try_start_0
    sget-object v0, Ljbb;->dXH:[Ljbb;

    if-nez v0, :cond_0

    .line 59563
    const/4 v0, 0x0

    new-array v0, v0, [Ljbb;

    sput-object v0, Ljbb;->dXH:[Ljbb;

    .line 59565
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59567
    :cond_1
    sget-object v0, Ljbb;->dXH:[Ljbb;

    return-object v0

    .line 59565
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 59553
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbb;->agq:Ljava/lang/String;

    iget v0, p0, Ljbb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbb;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljbb;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljbb;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljbb;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljbb;->dXI:Ljcn;

    if-nez v0, :cond_2

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljbb;->dXI:Ljcn;

    :cond_2
    iget-object v0, p0, Ljbb;->dXI:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljbb;->dXJ:Ljcn;

    if-nez v0, :cond_3

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljbb;->dXJ:Ljcn;

    :cond_3
    iget-object v0, p0, Ljbb;->dXJ:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljbb;->dXK:Ljcn;

    if-nez v0, :cond_4

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljbb;->dXK:Ljcn;

    :cond_4
    iget-object v0, p0, Ljbb;->dXK:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 59625
    iget v0, p0, Ljbb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 59626
    const/4 v0, 0x1

    iget-object v1, p0, Ljbb;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 59628
    :cond_0
    iget-object v0, p0, Ljbb;->aeB:Ljbp;

    if-eqz v0, :cond_1

    .line 59629
    const/4 v0, 0x2

    iget-object v1, p0, Ljbb;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 59631
    :cond_1
    iget-object v0, p0, Ljbb;->dXI:Ljcn;

    if-eqz v0, :cond_2

    .line 59632
    const/4 v0, 0x3

    iget-object v1, p0, Ljbb;->dXI:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 59634
    :cond_2
    iget-object v0, p0, Ljbb;->dXJ:Ljcn;

    if-eqz v0, :cond_3

    .line 59635
    const/4 v0, 0x4

    iget-object v1, p0, Ljbb;->dXJ:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 59637
    :cond_3
    iget-object v0, p0, Ljbb;->dXK:Ljcn;

    if-eqz v0, :cond_4

    .line 59638
    const/4 v0, 0x5

    iget-object v1, p0, Ljbb;->dXK:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 59640
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 59641
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59575
    iget-object v0, p0, Ljbb;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 59645
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 59646
    iget v1, p0, Ljbb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 59647
    const/4 v1, 0x1

    iget-object v2, p0, Ljbb;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59650
    :cond_0
    iget-object v1, p0, Ljbb;->aeB:Ljbp;

    if-eqz v1, :cond_1

    .line 59651
    const/4 v1, 0x2

    iget-object v2, p0, Ljbb;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59654
    :cond_1
    iget-object v1, p0, Ljbb;->dXI:Ljcn;

    if-eqz v1, :cond_2

    .line 59655
    const/4 v1, 0x3

    iget-object v2, p0, Ljbb;->dXI:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59658
    :cond_2
    iget-object v1, p0, Ljbb;->dXJ:Ljcn;

    if-eqz v1, :cond_3

    .line 59659
    const/4 v1, 0x4

    iget-object v2, p0, Ljbb;->dXJ:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59662
    :cond_3
    iget-object v1, p0, Ljbb;->dXK:Ljcn;

    if-eqz v1, :cond_4

    .line 59663
    const/4 v1, 0x5

    iget-object v2, p0, Ljbb;->dXK:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59666
    :cond_4
    return v0
.end method
