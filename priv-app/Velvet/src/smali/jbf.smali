.class public final Ljbf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dXW:[Ljbf;


# instance fields
.field private aez:I

.field private aix:Ljava/lang/String;

.field private ajk:Ljava/lang/String;

.field private dXX:Z

.field private dXY:Z

.field private dXZ:Z

.field private dYa:Z

.field private dYb:I

.field private dYc:I

.field private dYd:Ljava/lang/String;

.field private dYe:[Ljava/lang/String;

.field private dYf:J

.field private dYg:Ljbe;

.field private dYh:[Ljbe;

.field private dYi:[Ljbe;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 64292
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 64293
    iput v0, p0, Ljbf;->aez:I

    iput-boolean v0, p0, Ljbf;->dXX:Z

    iput-boolean v0, p0, Ljbf;->dXY:Z

    iput-boolean v0, p0, Ljbf;->dXZ:Z

    iput-boolean v0, p0, Ljbf;->dYa:Z

    iput v0, p0, Ljbf;->dYb:I

    iput v0, p0, Ljbf;->dYc:I

    const-string v0, ""

    iput-object v0, p0, Ljbf;->dYd:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbf;->aix:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljbf;->ajk:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljbf;->dYe:[Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljbf;->dYf:J

    iput-object v2, p0, Ljbf;->dYg:Ljbe;

    invoke-static {}, Ljbe;->beL()[Ljbe;

    move-result-object v0

    iput-object v0, p0, Ljbf;->dYh:[Ljbe;

    invoke-static {}, Ljbe;->beL()[Ljbe;

    move-result-object v0

    iput-object v0, p0, Ljbf;->dYi:[Ljbe;

    iput-object v2, p0, Ljbf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbf;->eCz:I

    .line 64294
    return-void
.end method

.method public static beM()[Ljbf;
    .locals 2

    .prologue
    .line 64068
    sget-object v0, Ljbf;->dXW:[Ljbf;

    if-nez v0, :cond_1

    .line 64069
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 64071
    :try_start_0
    sget-object v0, Ljbf;->dXW:[Ljbf;

    if-nez v0, :cond_0

    .line 64072
    const/4 v0, 0x0

    new-array v0, v0, [Ljbf;

    sput-object v0, Ljbf;->dXW:[Ljbf;

    .line 64074
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64076
    :cond_1
    sget-object v0, Ljbf;->dXW:[Ljbf;

    return-object v0

    .line 64074
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64062
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbf;->dXX:Z

    iget v0, p0, Ljbf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbf;->dXY:Z

    iget v0, p0, Ljbf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbf;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbf;->dXZ:Z

    iget v0, p0, Ljbf;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbf;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbf;->dYd:Ljava/lang/String;

    iget v0, p0, Ljbf;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljbf;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbf;->aix:Ljava/lang/String;

    iget v0, p0, Ljbf;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljbf;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbf;->ajk:Ljava/lang/String;

    iget v0, p0, Ljbf;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljbf;->aez:I

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbf;->dYe:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljbf;->dYe:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljbf;->dYe:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljbf;->dYe:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljbf;->dYf:J

    iget v0, p0, Ljbf;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljbf;->aez:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljbf;->dYg:Ljbe;

    if-nez v0, :cond_4

    new-instance v0, Ljbe;

    invoke-direct {v0}, Ljbe;-><init>()V

    iput-object v0, p0, Ljbf;->dYg:Ljbe;

    :cond_4
    iget-object v0, p0, Ljbf;->dYg:Ljbe;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbf;->dYh:[Ljbe;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbe;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljbf;->dYh:[Ljbe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Ljbe;

    invoke-direct {v3}, Ljbe;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljbf;->dYh:[Ljbe;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Ljbe;

    invoke-direct {v3}, Ljbe;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbf;->dYh:[Ljbe;

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljbf;->dYi:[Ljbe;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbe;

    if-eqz v0, :cond_8

    iget-object v3, p0, Ljbf;->dYi:[Ljbe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    new-instance v3, Ljbe;

    invoke-direct {v3}, Ljbe;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Ljbf;->dYi:[Ljbe;

    array-length v0, v0

    goto :goto_5

    :cond_a
    new-instance v3, Ljbe;

    invoke-direct {v3}, Ljbe;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljbf;->dYi:[Ljbe;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljbf;->dYa:Z

    iget v0, p0, Ljbf;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljbf;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbf;->dYb:I

    iget v0, p0, Ljbf;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljbf;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljbf;->dYc:I

    iget v0, p0, Ljbf;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljbf;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64320
    iget v0, p0, Ljbf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 64321
    const/4 v0, 0x1

    iget-boolean v2, p0, Ljbf;->dXX:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 64323
    :cond_0
    iget v0, p0, Ljbf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 64324
    const/4 v0, 0x2

    iget-boolean v2, p0, Ljbf;->dXY:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 64326
    :cond_1
    iget v0, p0, Ljbf;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 64327
    const/4 v0, 0x3

    iget-boolean v2, p0, Ljbf;->dXZ:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 64329
    :cond_2
    iget v0, p0, Ljbf;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_3

    .line 64330
    const/4 v0, 0x4

    iget-object v2, p0, Ljbf;->dYd:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 64332
    :cond_3
    iget v0, p0, Ljbf;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_4

    .line 64333
    const/4 v0, 0x5

    iget-object v2, p0, Ljbf;->aix:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 64335
    :cond_4
    iget v0, p0, Ljbf;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_5

    .line 64336
    const/4 v0, 0x6

    iget-object v2, p0, Ljbf;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 64338
    :cond_5
    iget-object v0, p0, Ljbf;->dYe:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljbf;->dYe:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 64339
    :goto_0
    iget-object v2, p0, Ljbf;->dYe:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 64340
    iget-object v2, p0, Ljbf;->dYe:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 64341
    if-eqz v2, :cond_6

    .line 64342
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 64339
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64346
    :cond_7
    iget v0, p0, Ljbf;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_8

    .line 64347
    const/16 v0, 0x8

    iget-wide v2, p0, Ljbf;->dYf:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 64349
    :cond_8
    iget-object v0, p0, Ljbf;->dYg:Ljbe;

    if-eqz v0, :cond_9

    .line 64350
    const/16 v0, 0x9

    iget-object v2, p0, Ljbf;->dYg:Ljbe;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 64352
    :cond_9
    iget-object v0, p0, Ljbf;->dYh:[Ljbe;

    if-eqz v0, :cond_b

    iget-object v0, p0, Ljbf;->dYh:[Ljbe;

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    .line 64353
    :goto_1
    iget-object v2, p0, Ljbf;->dYh:[Ljbe;

    array-length v2, v2

    if-ge v0, v2, :cond_b

    .line 64354
    iget-object v2, p0, Ljbf;->dYh:[Ljbe;

    aget-object v2, v2, v0

    .line 64355
    if-eqz v2, :cond_a

    .line 64356
    const/16 v3, 0xa

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 64353
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64360
    :cond_b
    iget-object v0, p0, Ljbf;->dYi:[Ljbe;

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljbf;->dYi:[Ljbe;

    array-length v0, v0

    if-lez v0, :cond_d

    .line 64361
    :goto_2
    iget-object v0, p0, Ljbf;->dYi:[Ljbe;

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 64362
    iget-object v0, p0, Ljbf;->dYi:[Ljbe;

    aget-object v0, v0, v1

    .line 64363
    if-eqz v0, :cond_c

    .line 64364
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 64361
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 64368
    :cond_d
    iget v0, p0, Ljbf;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_e

    .line 64369
    const/16 v0, 0xc

    iget-boolean v1, p0, Ljbf;->dYa:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 64371
    :cond_e
    iget v0, p0, Ljbf;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_f

    .line 64372
    const/16 v0, 0xd

    iget v1, p0, Ljbf;->dYb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bs(II)V

    .line 64374
    :cond_f
    iget v0, p0, Ljbf;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_10

    .line 64375
    const/16 v0, 0xe

    iget v1, p0, Ljbf;->dYc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bs(II)V

    .line 64377
    :cond_10
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 64378
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 64382
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 64383
    iget v1, p0, Ljbf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 64384
    const/4 v1, 0x1

    iget-boolean v3, p0, Ljbf;->dXX:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 64387
    :cond_0
    iget v1, p0, Ljbf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 64388
    const/4 v1, 0x2

    iget-boolean v3, p0, Ljbf;->dXY:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 64391
    :cond_1
    iget v1, p0, Ljbf;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 64392
    const/4 v1, 0x3

    iget-boolean v3, p0, Ljbf;->dXZ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 64395
    :cond_2
    iget v1, p0, Ljbf;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_3

    .line 64396
    const/4 v1, 0x4

    iget-object v3, p0, Ljbf;->dYd:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64399
    :cond_3
    iget v1, p0, Ljbf;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_4

    .line 64400
    const/4 v1, 0x5

    iget-object v3, p0, Ljbf;->aix:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64403
    :cond_4
    iget v1, p0, Ljbf;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_5

    .line 64404
    const/4 v1, 0x6

    iget-object v3, p0, Ljbf;->ajk:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64407
    :cond_5
    iget-object v1, p0, Ljbf;->dYe:[Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Ljbf;->dYe:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    move v4, v2

    .line 64410
    :goto_0
    iget-object v5, p0, Ljbf;->dYe:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_7

    .line 64411
    iget-object v5, p0, Ljbf;->dYe:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 64412
    if-eqz v5, :cond_6

    .line 64413
    add-int/lit8 v4, v4, 0x1

    .line 64414
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 64410
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64418
    :cond_7
    add-int/2addr v0, v3

    .line 64419
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 64421
    :cond_8
    iget v1, p0, Ljbf;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    .line 64422
    const/16 v1, 0x8

    iget-wide v4, p0, Ljbf;->dYf:J

    invoke-static {v1, v4, v5}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 64425
    :cond_9
    iget-object v1, p0, Ljbf;->dYg:Ljbe;

    if-eqz v1, :cond_a

    .line 64426
    const/16 v1, 0x9

    iget-object v3, p0, Ljbf;->dYg:Ljbe;

    invoke-static {v1, v3}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64429
    :cond_a
    iget-object v1, p0, Ljbf;->dYh:[Ljbe;

    if-eqz v1, :cond_d

    iget-object v1, p0, Ljbf;->dYh:[Ljbe;

    array-length v1, v1

    if-lez v1, :cond_d

    move v1, v0

    move v0, v2

    .line 64430
    :goto_1
    iget-object v3, p0, Ljbf;->dYh:[Ljbe;

    array-length v3, v3

    if-ge v0, v3, :cond_c

    .line 64431
    iget-object v3, p0, Ljbf;->dYh:[Ljbe;

    aget-object v3, v3, v0

    .line 64432
    if-eqz v3, :cond_b

    .line 64433
    const/16 v4, 0xa

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v1, v3

    .line 64430
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_c
    move v0, v1

    .line 64438
    :cond_d
    iget-object v1, p0, Ljbf;->dYi:[Ljbe;

    if-eqz v1, :cond_f

    iget-object v1, p0, Ljbf;->dYi:[Ljbe;

    array-length v1, v1

    if-lez v1, :cond_f

    .line 64439
    :goto_2
    iget-object v1, p0, Ljbf;->dYi:[Ljbe;

    array-length v1, v1

    if-ge v2, v1, :cond_f

    .line 64440
    iget-object v1, p0, Ljbf;->dYi:[Ljbe;

    aget-object v1, v1, v2

    .line 64441
    if-eqz v1, :cond_e

    .line 64442
    const/16 v3, 0xb

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64439
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 64447
    :cond_f
    iget v1, p0, Ljbf;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_10

    .line 64448
    const/16 v1, 0xc

    iget-boolean v2, p0, Ljbf;->dYa:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 64451
    :cond_10
    iget v1, p0, Ljbf;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_11

    .line 64452
    const/16 v1, 0xd

    iget v2, p0, Ljbf;->dYb:I

    invoke-static {v1, v2}, Ljsj;->bw(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 64455
    :cond_11
    iget v1, p0, Ljbf;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_12

    .line 64456
    const/16 v1, 0xe

    iget v2, p0, Ljbf;->dYc:I

    invoke-static {v1, v2}, Ljsj;->bw(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 64459
    :cond_12
    return v0
.end method
