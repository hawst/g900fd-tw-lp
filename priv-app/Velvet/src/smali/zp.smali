.class final Lzp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic Nf:Lzi;

.field private synthetic Np:Landroid/content/Context;


# direct methods
.method constructor <init>(Lzi;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2958
    iput-object p1, p0, Lzp;->Nf:Lzi;

    iput-object p2, p0, Lzp;->Np:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 2962
    iget-object v0, p0, Lzp;->Np:Landroid/content/Context;

    invoke-static {v0}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v6

    .line 2963
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2964
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v3

    .line 2965
    sget-object v2, Lzi;->MV:Ljava/lang/Object;

    monitor-enter v2

    .line 2966
    :try_start_0
    sget-object v0, Lzi;->MW:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwq;

    .line 2967
    instance-of v7, v0, Ladh;

    if-eqz v7, :cond_1

    .line 2968
    check-cast v0, Ladh;

    .line 2969
    invoke-virtual {v0}, Ladh;->kg()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, Lahn;->j(Ljava/lang/String;Lahz;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2972
    invoke-virtual {v0}, Ladh;->kf()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2983
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 2974
    :cond_1
    :try_start_1
    instance-of v7, v0, Lyy;

    if-eqz v7, :cond_0

    .line 2975
    check-cast v0, Lyy;

    .line 2976
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, Lyy;->bn(I)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, v0, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, Lahn;->j(Ljava/lang/String;Lahz;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2979
    iget-object v0, v0, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2983
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2985
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2986
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2987
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2988
    invoke-virtual {v6, v0, v3}, Lahn;->i(Ljava/lang/String;Lahz;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lahk;

    .line 2989
    new-instance v0, Lrr;

    iget-object v1, p0, Lzp;->Np:Landroid/content/Context;

    iget-object v4, p0, Lzp;->Nf:Lzi;

    invoke-static {v4}, Lzi;->b(Lzi;)Lwi;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lrr;-><init>(Landroid/content/Context;Lahk;Lahz;Lwi;Ljava/util/HashMap;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2993
    :cond_4
    iget-object v0, p0, Lzp;->Nf:Lzi;

    invoke-static {v0}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lzp;->Nf:Lzi;

    invoke-static {v0}, Lzi;->a(Lzi;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laaf;

    .line 2994
    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2995
    iget-object v1, p0, Lzp;->Nf:Lzi;

    invoke-static {v1}, Lzi;->d(Lzi;)Ltj;

    move-result-object v1

    new-instance v2, Lzq;

    invoke-direct {v2, p0, v0, v7}, Lzq;-><init>(Lzp;Laaf;Ljava/util/ArrayList;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v2, v0}, Ltj;->a(Ljava/lang/Runnable;I)V

    .line 3006
    :cond_5
    return-void

    :cond_6
    move-object v0, v5

    .line 2993
    goto :goto_2
.end method
