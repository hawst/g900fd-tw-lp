.class public final Lfba;
.super Landroid/database/Observable;
.source "PG"


# static fields
.field private static final cnJ:Ljava/util/Collection;


# instance fields
.field private final mAppContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-array v0, v6, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lfba;->cnJ:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/database/Observable;-><init>()V

    .line 40
    iput-object p1, p0, Lfba;->mAppContext:Landroid/content/Context;

    .line 41
    return-void
.end method

.method private a(ILizj;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 2
    .param p2    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.now.ENTRIES_UPDATED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 163
    if-eqz p3, :cond_0

    .line 164
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 166
    :cond_0
    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 167
    const-string v1, "entry"

    invoke-static {v0, v1, p2}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 168
    iget-object v1, p0, Lfba;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    return-object v0
.end method

.method private a(Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 3
    .param p2    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 193
    iget-object v0, p0, Lfba;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 200
    const-string v0, "type"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 201
    sget-object v1, Lfba;->cnJ:Ljava/util/Collection;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 203
    if-nez p2, :cond_0

    .line 204
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lfba;->b(ILizj;)Landroid/content/Intent;

    move-result-object p2

    .line 207
    :cond_0
    iget-object v0, p0, Lfba;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 209
    :cond_1
    return-void
.end method

.method private static b(ILizj;)Landroid/content/Intent;
    .locals 3
    .param p1    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 179
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.now.cards_remote_broadcast"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    const-string v1, "type"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 183
    if-eqz p1, :cond_0

    .line 184
    invoke-static {p1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v1

    invoke-virtual {v1}, Lgbg;->hashCode()I

    move-result v1

    .line 185
    const-string v2, "change_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 189
    :cond_0
    return-object v0
.end method

.method private e(ILandroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-direct {p0, p1, v1, v1}, Lfba;->a(ILizj;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lfba;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 146
    return-void
.end method


# virtual methods
.method public final E(IZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    const/4 v0, 0x3

    invoke-direct {p0, v0, v2, v2}, Lfba;->a(ILizj;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 66
    const-string v1, "refresh_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 67
    const-string v1, "refresh_error_auth"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    invoke-direct {p0, v0, v2}, Lfba;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 69
    return-void
.end method

.method public final a(Lizj;Lizj;Lizj;)V
    .locals 4
    .param p3    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 94
    iget-object v2, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 95
    :try_start_0
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 96
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbb;

    invoke-interface {v0, p2, p3}, Lfbb;->b(Lizj;Lizj;)V

    .line 95
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 98
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    const/4 v0, 0x4

    invoke-direct {p0, v0, p1, v3}, Lfba;->a(ILizj;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 101
    const-string v1, "updated_entry"

    invoke-static {v0, v1, p2}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 103
    const-string v1, "entry_change"

    invoke-static {v0, v1, p3}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    .line 105
    invoke-direct {p0, v0, v3}, Lfba;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 106
    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lizj;Ljava/util/List;)V
    .locals 5
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x5

    .line 118
    iget-object v2, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 119
    :try_start_0
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 120
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbb;

    invoke-interface {v0, p1, p2}, Lfbb;->b(Lizj;Ljava/util/Collection;)V

    .line 119
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 122
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, v3, p1, v0}, Lfba;->a(ILizj;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    .line 128
    invoke-static {v3, p1}, Lfba;->b(ILizj;)Landroid/content/Intent;

    move-result-object v3

    .line 130
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 131
    const-string v0, "child_entries"

    invoke-static {v2, v0, p2}, Lgbm;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/Collection;)V

    .line 133
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [I

    .line 134
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 135
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsr;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    invoke-virtual {v0}, Lgbg;->hashCode()I

    move-result v0

    aput v0, v4, v1

    .line 134
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 137
    :cond_1
    const-string v0, "child_change_ids"

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 141
    :cond_2
    invoke-direct {p0, v2, v3}, Lfba;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 142
    return-void
.end method

.method public final a(Lizp;Ljie;)V
    .locals 3

    .prologue
    .line 110
    iget-object v2, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 111
    :try_start_0
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 112
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbb;

    invoke-interface {v0, p2}, Lfbb;->a(Ljie;)V

    .line 111
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 114
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final e(Lizo;)V
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    monitor-enter v1

    .line 85
    :try_start_0
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 86
    iget-object v2, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 85
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 88
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lfba;->e(ILandroid/os/Bundle;)V

    .line 91
    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g(Landroid/os/Bundle;I)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 53
    iget-object v2, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 54
    :try_start_0
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 55
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbb;

    invoke-interface {v0}, Lfbb;->axk()V

    .line 54
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 57
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, v0, v3, p1}, Lfba;->a(ILizj;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 60
    const-string v1, "refresh_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 61
    invoke-direct {p0, v0, v3}, Lfba;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 62
    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final iA(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    const/4 v0, 0x6

    invoke-direct {p0, v0, v2, v2}, Lfba;->a(ILizj;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 73
    const-string v1, "refresh_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 74
    invoke-direct {p0, v0, v2}, Lfba;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 75
    return-void
.end method

.method public final iB(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    const/4 v0, 0x7

    invoke-direct {p0, v0, v2, v2}, Lfba;->a(ILizj;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 79
    const-string v1, "disabled_reason"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 80
    invoke-direct {p0, v0, v2}, Lfba;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 81
    return-void
.end method

.method public final notifyInvalidated()V
    .locals 3

    .prologue
    .line 44
    iget-object v2, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    monitor-enter v2

    .line 45
    :try_start_0
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 46
    iget-object v0, p0, Lfba;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbb;

    invoke-interface {v0}, Lfbb;->onInvalidated()V

    .line 45
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 48
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lfba;->e(ILandroid/os/Bundle;)V

    .line 50
    return-void

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
