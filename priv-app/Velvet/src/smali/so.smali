.class final Lso;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lsp;


# instance fields
.field private synthetic zl:Lsi;


# direct methods
.method private constructor <init>(Lsi;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lso;->zl:Lsi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lsi;B)V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0, p1}, Lso;-><init>(Lsi;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;)J
    .locals 6

    .prologue
    const/4 v5, 0x0

    const-wide/16 v0, -0x1

    .line 316
    const-string v2, "url"

    invoke-static {p1, v2}, Lsi;->b(Landroid/content/res/XmlResourceParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 317
    const-string v3, "title"

    invoke-static {p1, v3, v5}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;I)I

    move-result v3

    .line 318
    const-string v4, "icon"

    invoke-static {p1, v4, v5}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;I)I

    move-result v4

    .line 320
    if-eqz v3, :cond_0

    if-nez v4, :cond_1

    .line 340
    :cond_0
    :goto_0
    return-wide v0

    .line 325
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-nez v5, :cond_3

    .line 326
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring shortcut, invalid url: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 329
    :cond_3
    invoke-virtual {p2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 330
    if-eqz v4, :cond_0

    .line 335
    iget-object v0, p0, Lso;->zl:Lsi;

    invoke-static {v0}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v0

    iget-object v1, p0, Lso;->zl:Lsi;

    invoke-static {v1}, Lsi;->c(Lsi;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v4, v1}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lwq;->a(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V

    .line 336
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lso;->zl:Lsi;

    invoke-virtual {p2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Lsi;->a(Ljava/lang/String;Landroid/content/Intent;I)J

    move-result-wide v0

    goto :goto_0
.end method
