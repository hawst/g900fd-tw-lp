.class public final Lgyv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cZP:Z

.field public cZQ:Z

.field public cZR:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    return-void
.end method

.method public static a(Lgyt;Z)Z
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lgyt;->aLJ()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 59
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Lgyt;ZZ)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 112
    sget-object v2, Lgyw;->cZO:[I

    invoke-virtual {p0}, Lgyt;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 129
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 117
    :pswitch_1
    if-eqz p2, :cond_0

    move v0, v1

    goto :goto_0

    .line 119
    :pswitch_2
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    .line 127
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static g(Lcom/google/android/shared/search/Query;Z)Z
    .locals 1

    .prologue
    .line 212
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lgyt;->aY(Lcom/google/android/shared/search/Query;)Lgyt;

    move-result-object v0

    invoke-virtual {v0}, Lgyt;->aLI()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgyt;)Z
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lgyt;->cZD:Lgyt;

    if-eq p1, v0, :cond_0

    sget-object v0, Lgyt;->cZF:Lgyt;

    if-ne p1, v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lgyv;->cZQ:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lgyt;Lcom/google/android/shared/search/Query;Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 182
    sget-object v1, Lgyt;->cZD:Lgyt;

    if-ne p1, v1, :cond_0

    .line 183
    invoke-static {p2, p3}, Lgyv;->g(Lcom/google/android/shared/search/Query;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 185
    :cond_0
    return v0
.end method

.method public final a(Lgyt;Lgyt;Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 106
    sget-object v1, Lgyt;->cZE:Lgyt;

    if-eq p1, v1, :cond_0

    sget-object v1, Lgyt;->cZI:Lgyt;

    if-eq p1, v1, :cond_0

    sget-object v1, Lgyt;->cZJ:Lgyt;

    if-ne p1, v1, :cond_1

    if-nez p3, :cond_0

    sget-object v1, Lgyt;->cZA:Lgyt;

    invoke-virtual {p0, p2, v1, v0}, Lgyv;->a(Lgyt;Lgyt;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lgyt;ZZ)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 68
    sget-object v2, Lgyw;->cZO:[I

    invoke-virtual {p1}, Lgyt;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 82
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 74
    :pswitch_1
    if-eqz p2, :cond_1

    if-nez p3, :cond_1

    iget-boolean v2, p0, Lgyv;->cZR:Z

    if-nez v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lgyt;Lcom/google/android/shared/search/Query;Z)Z
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lgyt;->cZD:Lgyt;

    if-ne p1, v0, :cond_0

    .line 201
    invoke-static {p2, p3}, Lgyv;->g(Lcom/google/android/shared/search/Query;Z)Z

    move-result v0

    .line 207
    :goto_0
    return v0

    .line 202
    :cond_0
    sget-object v0, Lgyt;->cZC:Lgyt;

    if-eq p1, v0, :cond_1

    sget-object v0, Lgyt;->cZH:Lgyt;

    if-eq p1, v0, :cond_1

    sget-object v0, Lgyt;->cZG:Lgyt;

    if-ne p1, v0, :cond_2

    .line 205
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 207
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lgyt;Z)Z
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p1}, Lgyt;->aLJ()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lgyt;->cZF:Lgyt;

    if-ne p1, v0, :cond_1

    :cond_0
    if-nez p2, :cond_1

    iget-boolean v0, p0, Lgyv;->cZP:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 100
    :goto_0
    return v0

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
