.class abstract Lafm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private Vm:Ljava/lang/ThreadLocal;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lafm;->Vm:Ljava/lang/ThreadLocal;

    .line 52
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lafm;->Vm:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 63
    if-nez v0, :cond_1

    .line 64
    invoke-virtual {p0}, Lafm;->initialValue()Ljava/lang/Object;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lafm;->Vm:Ljava/lang/ThreadLocal;

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 73
    :cond_0
    :goto_0
    return-object v0

    .line 68
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 69
    if-nez v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lafm;->initialValue()Ljava/lang/Object;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lafm;->Vm:Ljava/lang/ThreadLocal;

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method abstract initialValue()Ljava/lang/Object;
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lafm;->Vm:Ljava/lang/ThreadLocal;

    new-instance v1, Ljava/lang/ref/SoftReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 58
    return-void
.end method
