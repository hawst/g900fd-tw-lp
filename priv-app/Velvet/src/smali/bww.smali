.class public final Lbww;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private aNd:Z

.field private aNe:Lcom/google/android/gms/location/ActivityRecognitionResult;

.field public aNf:I

.field public aNg:I

.field public aNh:I

.field public aNi:I

.field public aNj:I

.field public aNk:I

.field public aNl:I

.field private aNm:I

.field public aNn:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lbww;->aNl:I

    .line 59
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Jx()I

    move-result v0

    iput v0, p0, Lbww;->aNf:I

    .line 61
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Jy()I

    move-result v0

    iput v0, p0, Lbww;->aNg:I

    .line 63
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Jz()I

    move-result v0

    iput v0, p0, Lbww;->aNh:I

    .line 65
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Jv()I

    move-result v0

    iput v0, p0, Lbww;->aNi:I

    .line 67
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Ju()I

    move-result v0

    iput v0, p0, Lbww;->aNj:I

    .line 69
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->Jw()I

    move-result v0

    iput v0, p0, Lbww;->aNk:I

    .line 71
    return-void
.end method


# virtual methods
.method public final Bf()Z
    .locals 2

    .prologue
    .line 202
    iget v0, p0, Lbww;->aNl:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;Z)Z
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 80
    const-wide/16 v0, 0x0

    .line 81
    iget-object v4, p0, Lbww;->aNe:Lcom/google/android/gms/location/ActivityRecognitionResult;

    if-eqz v4, :cond_0

    .line 82
    iget-object v0, p0, Lbww;->aNe:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->getTime()J

    move-result-wide v0

    .line 84
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->getTime()J

    move-result-wide v4

    .line 86
    sub-long v0, v4, v0

    iget v4, p0, Lbww;->aNh:I

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    .line 93
    iget-boolean v0, p0, Lbww;->aNd:Z

    .line 182
    :goto_0
    return v0

    .line 97
    :cond_1
    iput-object p1, p0, Lbww;->aNe:Lcom/google/android/gms/location/ActivityRecognitionResult;

    .line 98
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->Ab()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    .line 100
    invoke-virtual {p0, v1, p2}, Lbww;->a(Lcom/google/android/gms/location/DetectedActivity;Z)Z

    move-result v0

    .line 105
    iget-boolean v4, p0, Lbww;->aNd:Z

    if-eqz v4, :cond_a

    .line 107
    if-eqz v0, :cond_4

    .line 109
    iput v6, p0, Lbww;->aNl:I

    .line 138
    :cond_2
    :goto_1
    iput v3, p0, Lbww;->aNm:I

    .line 144
    :cond_3
    :goto_2
    iput v3, p0, Lbww;->aNn:I

    .line 180
    :goto_3
    invoke-virtual {p0}, Lbww;->Bf()Z

    move-result v0

    iput-boolean v0, p0, Lbww;->aNd:Z

    .line 182
    iget-boolean v0, p0, Lbww;->aNd:Z

    goto :goto_0

    .line 112
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v0

    const/4 v4, 0x6

    if-ne v0, v4, :cond_5

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->Ad()I

    move-result v0

    iget v4, p0, Lbww;->aNk:I

    if-le v0, v4, :cond_5

    move v0, v2

    :goto_4
    if-eqz v0, :cond_6

    .line 115
    iput v3, p0, Lbww;->aNl:I

    goto :goto_1

    :cond_5
    move v0, v3

    .line 112
    goto :goto_4

    .line 118
    :cond_6
    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_7

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->Ad()I

    move-result v0

    iget v1, p0, Lbww;->aNk:I

    if-le v0, v1, :cond_7

    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    .line 119
    iget v0, p0, Lbww;->aNm:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbww;->aNm:I

    .line 120
    iget v0, p0, Lbww;->aNm:I

    iget v1, p0, Lbww;->aNg:I

    if-lt v0, v1, :cond_8

    .line 122
    iput v3, p0, Lbww;->aNl:I

    goto :goto_2

    :cond_7
    move v0, v3

    .line 118
    goto :goto_5

    .line 128
    :cond_8
    iget v0, p0, Lbww;->aNl:I

    if-eqz v0, :cond_3

    .line 129
    iget v0, p0, Lbww;->aNl:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbww;->aNl:I

    goto :goto_2

    .line 135
    :cond_9
    iget v0, p0, Lbww;->aNl:I

    if-eqz v0, :cond_2

    .line 136
    iget v0, p0, Lbww;->aNl:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbww;->aNl:I

    goto :goto_1

    .line 148
    :cond_a
    if-eqz v0, :cond_c

    .line 149
    iget v0, p0, Lbww;->aNn:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbww;->aNn:I

    .line 150
    iget v0, p0, Lbww;->aNn:I

    iget v1, p0, Lbww;->aNf:I

    if-lt v0, v1, :cond_b

    .line 154
    iput v6, p0, Lbww;->aNl:I

    .line 177
    :goto_6
    iput v3, p0, Lbww;->aNm:I

    goto :goto_3

    .line 165
    :cond_b
    iput v3, p0, Lbww;->aNl:I

    goto :goto_6

    .line 169
    :cond_c
    iput v3, p0, Lbww;->aNl:I

    .line 170
    iput v3, p0, Lbww;->aNn:I

    goto :goto_6
.end method

.method public final a(Lcom/google/android/gms/location/DetectedActivity;Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 208
    if-eqz p2, :cond_1

    iget v0, p0, Lbww;->aNi:I

    .line 216
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/location/DetectedActivity;->getType()I

    move-result v2

    if-ne v2, v1, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/DetectedActivity;->Ad()I

    move-result v2

    if-le v2, v0, :cond_2

    move v0, v1

    .line 222
    :goto_1
    return v0

    .line 208
    :cond_1
    iget v0, p0, Lbww;->aNj:I

    goto :goto_0

    .line 222
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 277
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 278
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mLastActivityRecognitionResult="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lbww;->aNe:Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mDriving="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lbww;->aNd:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mDrivingCount="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbww;->aNl:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mNumberOfConsecutiveDrivingSamplesSeen="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbww;->aNn:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mNumberOfConsecutiveStillSamplesSeen="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbww;->aNm:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    const-string v1, "historySize=10"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DrivingStateSmoother(state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
