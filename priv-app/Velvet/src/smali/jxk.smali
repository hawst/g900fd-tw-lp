.class public final Ljxk;
.super Ljsl;
.source "PG"


# static fields
.field public static final eKN:Ljsm;


# instance fields
.field private aez:I

.field public eKO:Lidy;

.field public eKP:Liec;

.field private eKQ:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xb

    const-class v1, Ljxk;

    const v2, 0x10db6c4a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljxk;->eKN:Ljsm;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 54
    iput v1, p0, Ljxk;->aez:I

    iput-object v0, p0, Ljxk;->eKO:Lidy;

    iput-object v0, p0, Ljxk;->eKP:Liec;

    iput-boolean v1, p0, Ljxk;->eKQ:Z

    iput-object v0, p0, Ljxk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxk;->eCz:I

    .line 55
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljxk;->eKO:Lidy;

    if-nez v0, :cond_1

    new-instance v0, Lidy;

    invoke-direct {v0}, Lidy;-><init>()V

    iput-object v0, p0, Ljxk;->eKO:Lidy;

    :cond_1
    iget-object v0, p0, Ljxk;->eKO:Lidy;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljxk;->eKP:Liec;

    if-nez v0, :cond_2

    new-instance v0, Liec;

    invoke-direct {v0}, Liec;-><init>()V

    iput-object v0, p0, Ljxk;->eKP:Liec;

    :cond_2
    iget-object v0, p0, Ljxk;->eKP:Liec;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxk;->eKQ:Z

    iget v0, p0, Ljxk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxk;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Ljxk;->eKO:Lidy;

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    iget-object v1, p0, Ljxk;->eKO:Lidy;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 73
    :cond_0
    iget-object v0, p0, Ljxk;->eKP:Liec;

    if-eqz v0, :cond_1

    .line 74
    const/4 v0, 0x2

    iget-object v1, p0, Ljxk;->eKP:Liec;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 76
    :cond_1
    iget v0, p0, Ljxk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 77
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljxk;->eKQ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 79
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 80
    return-void
.end method

.method public final bvM()Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Ljxk;->eKQ:Z

    return v0
.end method

.method public final jt(Z)Ljxk;
    .locals 1

    .prologue
    .line 40
    iput-boolean p1, p0, Ljxk;->eKQ:Z

    .line 41
    iget v0, p0, Ljxk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxk;->aez:I

    .line 42
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 84
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 85
    iget-object v1, p0, Ljxk;->eKO:Lidy;

    if-eqz v1, :cond_0

    .line 86
    const/4 v1, 0x1

    iget-object v2, p0, Ljxk;->eKO:Lidy;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_0
    iget-object v1, p0, Ljxk;->eKP:Liec;

    if-eqz v1, :cond_1

    .line 90
    const/4 v1, 0x2

    iget-object v2, p0, Ljxk;->eKP:Liec;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_1
    iget v1, p0, Ljxk;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    .line 94
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljxk;->eKQ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 97
    :cond_2
    return v0
.end method
