.class final Lhaf;
.super Lesj;
.source "PG"


# instance fields
.field private synthetic daw:Lhad;


# direct methods
.method constructor <init>(Lhad;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lhaf;->daw:Lhad;

    invoke-direct {p0}, Lesj;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lejm;)V
    .locals 10

    .prologue
    const v9, 0x7f1100bc

    const/4 v8, 0x4

    const/4 v4, 0x0

    .line 203
    iget-object v0, p0, Lhaf;->daw:Lhad;

    invoke-virtual {v0}, Lhad;->aAU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    :goto_0
    return-void

    .line 207
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Lhaf;->daw:Lhad;

    iget-object v1, v1, Lhad;->bub:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 208
    if-nez v5, :cond_1

    .line 209
    iget-object v0, p0, Lhaf;->daw:Lhad;

    iget-object v0, v0, Lhad;->mView:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 212
    :cond_1
    iget-object v0, p0, Lhaf;->daw:Lhad;

    iget-object v0, v0, Lhad;->mView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Lhaf;->daw:Lhad;

    iget-object v0, v0, Lhad;->mView:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    .line 215
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    move v3, v4

    .line 216
    :goto_1
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v3, v1, :cond_2

    .line 218
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 219
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 220
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 221
    iget-object v2, p0, Lhaf;->daw:Lhad;

    iget-object v2, v2, Lhad;->bub:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 223
    :cond_2
    sub-int v1, v5, v6

    .line 224
    sub-int v1, v5, v1

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_3

    .line 225
    iget-object v1, p0, Lhaf;->daw:Lhad;

    invoke-virtual {v1}, Lhad;->aJU()Lgpu;

    iget-object v1, p0, Lhaf;->daw:Lhad;

    invoke-static {v1}, Lgpu;->a(Lhad;)Landroid/view/View;

    move-result-object v7

    .line 227
    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 228
    iget-object v2, p0, Lhaf;->daw:Lhad;

    iget-object v2, v2, Lhad;->bub:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v1, p0, Lhaf;->daw:Lhad;

    iget-object v1, v1, Lhad;->dau:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 224
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_3
    move v1, v5

    .line 232
    :goto_3
    if-ge v1, v6, :cond_4

    .line 233
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 232
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 235
    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1102e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 237
    const/16 v0, 0x123

    invoke-static {v0}, Lege;->ht(I)V

    goto/16 :goto_0
.end method
