.class public final Lgna;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgmz;


# instance fields
.field private final bfo:Ligi;

.field private final mApplicationVersion:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Ligi;Lcjs;Lcke;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 32
    iput-object p1, p0, Lgna;->mApplicationVersion:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lgna;->mContext:Landroid/content/Context;

    .line 34
    iput-object p3, p0, Lgna;->bfo:Ligi;

    .line 35
    iput-object p5, p0, Lgna;->mSearchSettings:Lcke;

    .line 37
    return-void

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Dm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lgna;->mApplicationVersion:Ljava/lang/String;

    return-object v0
.end method

.method public final NB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lgna;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->NB()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final OJ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lgna;->bfo:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisplayMetrics()Landroid/util/DisplayMetrics;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lgna;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 64
    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 70
    :goto_0
    return-object v0

    .line 68
    :cond_0
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 69
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    move-object v0, v1

    .line 70
    goto :goto_0
.end method
