.class public Licp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final dkw:Lglm;

.field private final mAppSelectionHelper:Libo;


# direct methods
.method public constructor <init>(Libo;Lglm;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Licp;->mAppSelectionHelper:Libo;

    .line 44
    iput-object p2, p0, Licp;->dkw:Lglm;

    .line 45
    return-void
.end method

.method public static varargs a(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;
    .locals 5

    .prologue
    .line 191
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-object p0

    .line 195
    :cond_1
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 197
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    .line 198
    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 199
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object p0, v1

    .line 202
    goto :goto_0
.end method

.method private static a(Lcom/google/android/shared/util/App;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :cond_0
    return-void
.end method

.method protected static b(Ljmh;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 162
    iget-object v1, p0, Ljmh;->etO:[I

    .line 163
    invoke-static {v1, v0}, Lius;->d([II)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lius;->d([II)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/PlayMediaAction;Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    invoke-virtual {p0, p1}, Licp;->b(Lcom/google/android/search/shared/actions/PlayMediaAction;)Licq;

    move-result-object v3

    .line 79
    if-nez v3, :cond_1

    .line 157
    :cond_0
    :goto_0
    return v2

    .line 84
    :cond_1
    iget-object v0, p0, Licp;->dkw:Lglm;

    invoke-interface {v0}, Lglm;->yM()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Licq;->oY(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 85
    if-eqz v0, :cond_2

    iget-object v4, p0, Licp;->mAppSelectionHelper:Libo;

    invoke-virtual {v4, v0}, Libo;->am(Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 86
    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->w(Landroid/content/Intent;)V

    .line 89
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Ljmh;->bpO()I

    .line 92
    invoke-static {}, Lior;->aYa()Ljava/util/LinkedHashMap;

    move-result-object v4

    .line 95
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    invoke-virtual {v0}, Ljmh;->bpU()Z

    move-result v0

    if-nez v0, :cond_5

    .line 96
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    iget-object v5, v0, Ljmh;->etR:[Ljava/lang/String;

    .line 98
    iget-object v0, p0, Licp;->dkw:Lglm;

    invoke-interface {v0}, Lglm;->yM()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Licq;->pc(Ljava/lang/String;)Lijj;

    move-result-object v0

    invoke-static {v0, v5}, Licp;->a(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 100
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 103
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->i(Lcom/google/android/shared/util/App;)V

    .line 109
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    .line 110
    invoke-static {v0, v4}, Licp;->a(Lcom/google/android/shared/util/App;Ljava/util/Map;)V

    goto :goto_1

    .line 114
    :cond_3
    invoke-virtual {v3}, Licq;->aUN()Lijj;

    move-result-object v0

    invoke-static {v0, v5}, Licp;->a(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 115
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    .line 116
    invoke-static {v0, v4}, Licp;->a(Lcom/google/android/shared/util/App;Ljava/util/Map;)V

    goto :goto_2

    .line 118
    :cond_4
    invoke-virtual {p1, v5}, Lcom/google/android/search/shared/actions/PlayMediaAction;->r(Ljava/util/List;)V

    .line 123
    :cond_5
    iget-object v0, p0, Licp;->mAppSelectionHelper:Libo;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v5

    invoke-virtual {v5}, Ljmh;->getUrl()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5}, Libo;->T(Landroid/net/Uri;)Lcom/google/android/shared/util/App;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v5, p0, Licp;->mAppSelectionHelper:Libo;

    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->auq()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v5, v6}, Libo;->am(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 124
    :goto_3
    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->h(Lcom/google/android/shared/util/App;)V

    .line 125
    if-eqz v0, :cond_6

    .line 126
    invoke-static {v0, v4}, Licp;->a(Lcom/google/android/shared/util/App;Ljava/util/Map;)V

    .line 129
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v0

    iget-object v0, v0, Ljmh;->etR:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_9

    move v0, v1

    .line 130
    :goto_4
    if-eqz v0, :cond_7

    .line 134
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v5

    iget-object v5, v5, Ljmh;->etR:[Ljava/lang/String;

    invoke-static {v0, v5}, Licp;->a(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move p2, v1

    .line 140
    :cond_7
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 141
    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->k(Ljava/util/Collection;)V

    .line 150
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 151
    iget-object v4, p0, Licp;->mAppSelectionHelper:Libo;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->getMimeType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Libo;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/shared/util/App;

    move-result-object v0

    if-eqz v0, :cond_a

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->em(Z)V

    .line 153
    invoke-virtual {v3, p2}, Licq;->gU(Z)Lcom/google/android/shared/util/App;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->k(Lcom/google/android/shared/util/App;)V

    move v2, v1

    .line 155
    goto/16 :goto_0

    .line 123
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    :cond_9
    move v0, v2

    .line 129
    goto :goto_4

    :cond_a
    move v0, v2

    .line 151
    goto :goto_5
.end method

.method protected b(Lcom/google/android/search/shared/actions/PlayMediaAction;)Licq;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 208
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aia()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    new-instance v0, Licm;

    iget-object v1, p0, Licp;->mAppSelectionHelper:Libo;

    invoke-direct {v0, p1, v1}, Licm;-><init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V

    .line 217
    :goto_0
    return-object v0

    .line 210
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->aib()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    new-instance v0, Licn;

    iget-object v1, p0, Licp;->mAppSelectionHelper:Libo;

    invoke-direct {v0, p1, v1}, Licn;-><init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V

    goto :goto_0

    .line 212
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahY()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    new-instance v0, Lict;

    iget-object v1, p0, Licp;->mAppSelectionHelper:Libo;

    invoke-direct {v0, p1, v1}, Lict;-><init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V

    goto :goto_0

    .line 214
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahZ()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 215
    new-instance v0, Licu;

    iget-object v1, p0, Licp;->mAppSelectionHelper:Libo;

    invoke-direct {v0, p1, v1}, Licu;-><init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V

    goto :goto_0

    .line 217
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
