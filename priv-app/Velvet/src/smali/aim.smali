.class public final Laim;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static YY:Lna;


# instance fields
.field private FW:I

.field private TT:I

.field private YL:Lqz;

.field private YZ:Laiq;

.field protected Za:I

.field private Zb:I

.field private Zc:I

.field private Zd:I

.field private Ze:I

.field private Zf:Z

.field private final Zg:Landroid/graphics/RectF;

.field private final Zh:Landroid/graphics/RectF;

.field private final Zi:Lee;

.field private final Zj:Ljava/lang/Object;

.field private final Zk:Laip;

.field private final Zl:Laip;

.field private final Zm:Laip;

.field protected Zn:I

.field protected Zo:I

.field private Zp:I

.field private Zq:I

.field private Zr:F

.field private Zs:Z

.field private final Zt:Landroid/graphics/Rect;

.field private final Zu:[Landroid/graphics/Rect;

.field private Zv:Laio;

.field private Zw:Z

.field private Zx:I

.field private Zy:I

.field private Zz:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lnc;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lnc;-><init>(I)V

    sput-object v0, Laim;->YY:Lna;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput v3, p0, Laim;->Zb:I

    .line 93
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Laim;->Zg:Landroid/graphics/RectF;

    .line 94
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Laim;->Zh:Landroid/graphics/RectF;

    .line 96
    new-instance v0, Lee;

    invoke-direct {v0}, Lee;-><init>()V

    iput-object v0, p0, Laim;->Zi:Lee;

    .line 99
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Laim;->Zj:Ljava/lang/Object;

    .line 100
    new-instance v0, Laip;

    invoke-direct {v0, v3}, Laip;-><init>(B)V

    iput-object v0, p0, Laim;->Zk:Laip;

    .line 101
    new-instance v0, Laip;

    invoke-direct {v0, v3}, Laip;-><init>(B)V

    iput-object v0, p0, Laim;->Zl:Laip;

    .line 102
    new-instance v0, Laip;

    invoke-direct {v0, v3}, Laip;-><init>(B)V

    iput-object v0, p0, Laim;->Zm:Laip;

    .line 105
    iput v1, p0, Laim;->Zn:I

    .line 106
    iput v1, p0, Laim;->Zo:I

    .line 116
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Laim;->Zt:Landroid/graphics/Rect;

    .line 117
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Laim;->Zu:[Landroid/graphics/Rect;

    .line 174
    iput-object p1, p0, Laim;->Zz:Landroid/view/View;

    .line 175
    new-instance v0, Laio;

    invoke-direct {v0, p0, v3}, Laio;-><init>(Laim;B)V

    iput-object v0, p0, Laim;->Zv:Laio;

    .line 176
    iget-object v0, p0, Laim;->Zv:Laio;

    invoke-virtual {v0}, Laio;->start()V

    .line 177
    return-void
.end method

.method public static E(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/16 v2, 0x800

    .line 162
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-gt v0, v2, :cond_0

    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    const/16 v0, 0x200

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/16 v0, 0x100

    goto :goto_1
.end method

.method static synthetic a(Laim;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Laim;->FW:I

    return v0
.end method

.method static synthetic a(Laim;III)Lain;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Laim;->j(III)Lain;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Laim;Lain;)V
    .locals 4

    .prologue
    .line 39
    iget-object v1, p0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p1, Lain;->ZF:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    iput v0, p1, Lain;->ZF:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-virtual {p1}, Lain;->lU()Z

    move-result v1

    iget-object v2, p0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget v0, p1, Lain;->ZF:I

    const/16 v3, 0x20

    if-ne v0, v3, :cond_2

    const/16 v0, 0x40

    iput v0, p1, Lain;->ZF:I

    iget-object v0, p1, Lain;->ZE:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    sget-object v0, Laim;->YY:Lna;

    iget-object v1, p1, Lain;->ZE:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lna;->Y(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p1, Lain;->ZE:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Laim;->Zk:Laip;

    invoke-virtual {v0, p1}, Laip;->c(Lain;)Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    if-eqz v1, :cond_3

    const/16 v0, 0x8

    :goto_1
    :try_start_2
    iput v0, p1, Lain;->ZF:I

    if-nez v1, :cond_4

    monitor-exit v2

    goto :goto_0

    :cond_3
    const/16 v0, 0x10

    goto :goto_1

    :cond_4
    iget-object v0, p0, Laim;->Zl:Laip;

    invoke-virtual {v0, p1}, Laip;->c(Lain;)Z

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Laim;->Zz:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    goto :goto_0
.end method

.method private a(Lain;)V
    .locals 3

    .prologue
    .line 482
    iget-object v1, p0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v1

    .line 483
    :try_start_0
    iget v0, p1, Lain;->ZF:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 484
    const/4 v0, 0x2

    iput v0, p1, Lain;->ZF:I

    .line 485
    iget-object v0, p0, Laim;->Zm:Laip;

    invoke-virtual {v0, p1}, Laip;->c(Lain;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Laim;->Zj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 489
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/graphics/Rect;IIIFI)V
    .locals 16

    .prologue
    .line 362
    move/from16 v0, p6

    neg-int v2, v0

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 363
    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zx:I

    int-to-double v4, v4

    .line 364
    move-object/from16 v0, p0

    iget v6, v0, Laim;->Zy:I

    int-to-double v6, v6

    .line 366
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    .line 367
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 368
    mul-double v10, v8, v4

    mul-double v12, v2, v6

    sub-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    mul-double v12, v8, v4

    mul-double v14, v2, v6

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    .line 370
    mul-double v12, v2, v4

    mul-double v14, v8, v6

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    mul-double/2addr v2, v4

    mul-double v4, v8, v6

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    invoke-static {v12, v13, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 373
    move/from16 v0, p2

    int-to-float v3, v0

    int-to-float v4, v10

    const/high16 v5, 0x40000000    # 2.0f

    mul-float v5, v5, p5

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 374
    move/from16 v0, p3

    int-to-float v4, v0

    int-to-float v5, v2

    const/high16 v6, 0x40000000    # 2.0f

    mul-float v6, v6, p5

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 375
    int-to-float v5, v3

    int-to-float v6, v10

    div-float v6, v6, p5

    add-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 376
    int-to-float v6, v4

    int-to-float v2, v2

    div-float v2, v2, p5

    add-float/2addr v2, v6

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    .line 379
    move-object/from16 v0, p0

    iget v6, v0, Laim;->FW:I

    shl-int v6, v6, p4

    .line 380
    const/4 v7, 0x0

    div-int/2addr v3, v6

    mul-int/2addr v3, v6

    invoke-static {v7, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 381
    const/4 v7, 0x0

    div-int/2addr v4, v6

    mul-int/2addr v4, v6

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 382
    move-object/from16 v0, p0

    iget v6, v0, Laim;->Zn:I

    invoke-static {v6, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 383
    move-object/from16 v0, p0

    iget v6, v0, Laim;->Zo:I

    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 385
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 386
    return-void
.end method

.method private a(Lain;Lrb;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 635
    :goto_0
    invoke-virtual {p1}, Lain;->dS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    invoke-interface {p2, p1, p3, p4}, Lrb;->a(Lqz;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 637
    const/4 v0, 0x1

    .line 643
    :goto_1
    return v0

    .line 641
    :cond_0
    invoke-virtual {p1}, Lain;->lV()Lain;

    move-result-object v0

    .line 642
    if-nez v0, :cond_1

    .line 643
    const/4 v0, 0x0

    goto :goto_1

    .line 645
    :cond_1
    iget v1, p1, Lain;->ZA:I

    iget v2, v0, Lain;->ZA:I

    if-ne v1, v2, :cond_2

    .line 646
    iget v1, p3, Landroid/graphics/RectF;->left:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->left:F

    .line 647
    iget v1, p3, Landroid/graphics/RectF;->right:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->right:F

    .line 652
    :goto_2
    iget v1, p1, Lain;->ZB:I

    iget v2, v0, Lain;->ZB:I

    if-ne v1, v2, :cond_3

    .line 653
    iget v1, p3, Landroid/graphics/RectF;->top:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->top:F

    .line 654
    iget v1, p3, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->bottom:F

    :goto_3
    move-object p1, v0

    .line 660
    goto :goto_0

    .line 649
    :cond_2
    iget v1, p0, Laim;->FW:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->left:F

    .line 650
    iget v1, p0, Laim;->FW:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->right:F

    goto :goto_2

    .line 656
    :cond_3
    iget v1, p0, Laim;->FW:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->top:F

    .line 657
    iget v1, p0, Laim;->FW:I

    int-to-float v1, v1

    iget v2, p3, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p3, Landroid/graphics/RectF;->bottom:F

    goto :goto_3
.end method

.method static synthetic b(Laim;)Laiq;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Laim;->YZ:Laiq;

    return-object v0
.end method

.method private b(Lain;)V
    .locals 3

    .prologue
    .line 532
    iget-object v1, p0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v1

    .line 533
    :try_start_0
    iget v0, p1, Lain;->ZF:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 534
    const/16 v0, 0x20

    iput v0, p1, Lain;->ZF:I

    .line 535
    monitor-exit v1

    .line 543
    :goto_0
    return-void

    .line 537
    :cond_0
    const/16 v0, 0x40

    iput v0, p1, Lain;->ZF:I

    .line 538
    iget-object v0, p1, Lain;->ZE:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 539
    sget-object v0, Laim;->YY:Lna;

    iget-object v2, p1, Lain;->ZE:Landroid/graphics/Bitmap;

    invoke-interface {v0, v2}, Lna;->Y(Ljava/lang/Object;)Z

    .line 540
    const/4 v0, 0x0

    iput-object v0, p1, Lain;->ZE:Landroid/graphics/Bitmap;

    .line 542
    :cond_1
    iget-object v0, p0, Laim;->Zk:Laip;

    invoke-virtual {v0, p1}, Laip;->c(Lain;)Z

    .line 543
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic c(Laim;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Laim;->Zb:I

    return v0
.end method

.method static synthetic d(Laim;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Laim;->Zj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic e(Laim;)Laip;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Laim;->Zm:Laip;

    return-object v0
.end method

.method private i(III)Lain;
    .locals 3

    .prologue
    .line 520
    iget-object v1, p0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v1

    .line 521
    :try_start_0
    iget-object v0, p0, Laim;->Zk:Laip;

    invoke-virtual {v0}, Laip;->lY()Lain;

    move-result-object v0

    .line 522
    if-eqz v0, :cond_0

    .line 523
    const/4 v2, 0x1

    iput v2, v0, Lain;->ZF:I

    .line 524
    invoke-virtual {v0, p1, p2, p3}, Lain;->l(III)V

    .line 525
    monitor-exit v1

    .line 527
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lain;

    invoke-direct {v0, p0, p1, p2, p3}, Lain;-><init>(Laim;III)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 528
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private j(III)Lain;
    .locals 4

    .prologue
    .line 560
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-static {p1, p2, p3}, Laim;->k(III)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lee;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lain;

    return-object v0
.end method

.method private static k(III)J
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 564
    int-to-long v0, p0

    .line 565
    shl-long/2addr v0, v4

    int-to-long v2, p1

    or-long/2addr v0, v2

    .line 566
    shl-long/2addr v0, v4

    int-to-long v2, p2

    or-long/2addr v0, v2

    .line 567
    return-wide v0
.end method

.method private lQ()V
    .locals 4

    .prologue
    .line 335
    iget-object v2, p0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v2

    .line 336
    :try_start_0
    iget-object v0, p0, Laim;->Zm:Laip;

    const/4 v1, 0x0

    iput-object v1, v0, Laip;->ZH:Lain;

    .line 337
    iget-object v0, p0, Laim;->Zl:Laip;

    const/4 v1, 0x0

    iput-object v1, v0, Laip;->ZH:Lain;

    .line 340
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-virtual {v0}, Lee;->size()I

    move-result v3

    .line 341
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 342
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-virtual {v0, v1}, Lee;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lain;

    .line 343
    invoke-direct {p0, v0}, Laim;->b(Lain;)V

    .line 341
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 345
    :cond_0
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-virtual {v0}, Lee;->clear()V

    .line 346
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private lS()V
    .locals 4

    .prologue
    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Laim;->Zw:Z

    .line 472
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-virtual {v0}, Lee;->size()I

    move-result v2

    .line 473
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 474
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-virtual {v0, v1}, Lee;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lain;

    .line 475
    invoke-virtual {v0}, Lain;->dS()Z

    move-result v3

    if-nez v3, :cond_0

    .line 476
    invoke-direct {p0, v0}, Laim;->a(Lain;)V

    .line 473
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 479
    :cond_1
    return-void
.end method

.method static synthetic lT()Lna;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Laim;->YY:Lna;

    return-object v0
.end method


# virtual methods
.method public final U(II)V
    .locals 0

    .prologue
    .line 236
    iput p1, p0, Laim;->Zx:I

    .line 237
    iput p2, p0, Laim;->Zy:I

    .line 238
    return-void
.end method

.method public final a(IIF)V
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Laim;->Zp:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Laim;->Zq:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Laim;->Zr:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    .line 249
    :goto_0
    return-void

    .line 245
    :cond_0
    iput p1, p0, Laim;->Zp:I

    .line 246
    iput p2, p0, Laim;->Zq:I

    .line 247
    iput p3, p0, Laim;->Zr:F

    .line 248
    const/4 v0, 0x1

    iput-boolean v0, p0, Laim;->Zs:Z

    goto :goto_0
.end method

.method public final a(Laiq;I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 192
    iget-object v0, p0, Laim;->YZ:Laiq;

    if-eq v0, p1, :cond_0

    .line 193
    iput-object p1, p0, Laim;->YZ:Laiq;

    .line 194
    invoke-direct {p0}, Laim;->lQ()V

    iget-object v0, p0, Laim;->YZ:Laiq;

    if-nez v0, :cond_2

    iput v3, p0, Laim;->Zn:I

    iput v3, p0, Laim;->Zo:I

    iput v3, p0, Laim;->Za:I

    const/4 v0, 0x0

    iput-object v0, p0, Laim;->YL:Lqz;

    :goto_0
    iput-boolean v2, p0, Laim;->Zs:Z

    .line 196
    :cond_0
    iget v0, p0, Laim;->TT:I

    if-eq v0, p2, :cond_1

    .line 197
    iput p2, p0, Laim;->TT:I

    .line 198
    iput-boolean v2, p0, Laim;->Zs:Z

    .line 200
    :cond_1
    return-void

    .line 194
    :cond_2
    iget-object v0, p0, Laim;->YZ:Laiq;

    invoke-interface {v0}, Laiq;->gi()I

    move-result v0

    iput v0, p0, Laim;->Zn:I

    iget-object v0, p0, Laim;->YZ:Laiq;

    invoke-interface {v0}, Laiq;->gj()I

    move-result v0

    iput v0, p0, Laim;->Zo:I

    iget-object v0, p0, Laim;->YZ:Laiq;

    invoke-interface {v0}, Laiq;->gk()Lqz;

    move-result-object v0

    iput-object v0, p0, Laim;->YL:Lqz;

    iget-object v0, p0, Laim;->YZ:Laiq;

    invoke-interface {v0}, Laiq;->gh()I

    move-result v0

    iput v0, p0, Laim;->FW:I

    iget-object v0, p0, Laim;->YL:Lqz;

    if-eqz v0, :cond_3

    iget v0, p0, Laim;->Zn:I

    int-to-float v0, v0

    iget-object v1, p0, Laim;->YL:Lqz;

    invoke-virtual {v1}, Lqz;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Lqm;->f(F)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Laim;->Za:I

    goto :goto_0

    :cond_3
    iget v0, p0, Laim;->Zn:I

    iget v1, p0, Laim;->Zo:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v0, p0, Laim;->FW:I

    move v1, v2

    :goto_1
    if-ge v0, v3, :cond_4

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iput v1, p0, Laim;->Za:I

    goto :goto_0
.end method

.method public final d(Lrb;)Z
    .locals 20

    .prologue
    .line 414
    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zx:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zy:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Laim;->Zs:Z

    if-nez v2, :cond_2

    .line 415
    :cond_0
    :goto_0
    const/4 v3, 0x1

    const/4 v2, 0x0

    :cond_1
    :goto_1
    if-lez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zl:Laip;

    invoke-virtual {v2}, Laip;->lY()Lain;

    move-result-object v2

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Lain;->dS()Z

    move-result v4

    if-nez v4, :cond_1

    iget v4, v2, Lain;->ZF:I

    const/16 v5, 0x8

    if-ne v4, v5, :cond_e

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lain;->c(Lrb;)V

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 414
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Laim;->Zs:Z

    const/high16 v2, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v3, v0, Laim;->Zr:F

    div-float/2addr v2, v3

    invoke-static {v2}, Lqm;->g(F)I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Za:I

    invoke-static {v2, v3, v4}, Lqm;->f(III)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Laim;->Zb:I

    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zb:I

    move-object/from16 v0, p0

    iget v3, v0, Laim;->Za:I

    if-eq v2, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Laim;->Zt:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zp:I

    move-object/from16 v0, p0

    iget v5, v0, Laim;->Zq:I

    move-object/from16 v0, p0

    iget v6, v0, Laim;->Zb:I

    move-object/from16 v0, p0

    iget v7, v0, Laim;->Zr:F

    move-object/from16 v0, p0

    iget v8, v0, Laim;->TT:I

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Laim;->a(Landroid/graphics/Rect;IIIFI)V

    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zx:I

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    iget v4, v3, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v5, v0, Laim;->Zp:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Laim;->Zr:F

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Laim;->Zc:I

    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zy:I

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    iget v3, v3, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zq:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zr:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Laim;->Zd:I

    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zr:F

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zb:I

    shl-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f400000    # 0.75f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zb:I

    add-int/lit8 v2, v2, -0x1

    :goto_2
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Za:I

    add-int/lit8 v4, v4, -0x2

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v9

    add-int/lit8 v2, v9, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Laim;->Za:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Laim;->Zu:[Landroid/graphics/Rect;

    move v6, v9

    :goto_3
    if-ge v6, v10, :cond_5

    sub-int v2, v6, v9

    aget-object v3, v11, v2

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zp:I

    move-object/from16 v0, p0

    iget v5, v0, Laim;->Zq:I

    move-object/from16 v0, p0

    iget v8, v0, Laim;->TT:I

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    add-int/lit8 v12, v6, 0x1

    shl-int/2addr v7, v12

    int-to-float v7, v7

    div-float v7, v2, v7

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Laim;->a(Landroid/graphics/Rect;IIIFI)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zb:I

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zb:I

    add-int/lit8 v2, v2, -0x2

    move-object/from16 v0, p0

    iget v3, v0, Laim;->Zx:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zp:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Laim;->Zr:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Laim;->Zc:I

    move-object/from16 v0, p0

    iget v3, v0, Laim;->Zy:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zq:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v5, v0, Laim;->Zr:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Laim;->Zd:I

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Laim;->TT:I

    rem-int/lit8 v2, v2, 0x5a

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zm:Laip;

    const/4 v3, 0x0

    iput-object v3, v2, Laip;->ZH:Lain;

    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zl:Laip;

    const/4 v3, 0x0

    iput-object v3, v2, Laip;->ZH:Lain;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Laim;->Zw:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zi:Lee;

    invoke-virtual {v2}, Lee;->size()I

    move-result v4

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zi:Lee;

    invoke-virtual {v2, v3}, Lee;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lain;

    iget v6, v2, Lain;->ZC:I

    if-lt v6, v9, :cond_6

    if-ge v6, v10, :cond_6

    sub-int/2addr v6, v9

    aget-object v6, v11, v6

    iget v7, v2, Lain;->ZA:I

    iget v8, v2, Lain;->ZB:I

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-nez v6, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Laim;->Zi:Lee;

    invoke-virtual {v6, v3}, Lee;->removeAt(I)V

    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Laim;->b(Lain;)V

    :cond_7
    move v2, v3

    move v3, v4

    add-int/lit8 v2, v2, 0x1

    move v4, v3

    move v3, v2

    goto :goto_4

    :cond_8
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v9

    :goto_5
    if-ge v3, v10, :cond_d

    move-object/from16 v0, p0

    iget v2, v0, Laim;->FW:I

    shl-int v6, v2, v3

    sub-int v2, v3, v9

    aget-object v7, v11, v2

    iget v2, v7, Landroid/graphics/Rect;->top:I

    iget v8, v7, Landroid/graphics/Rect;->bottom:I

    move v4, v2

    :goto_6
    if-ge v4, v8, :cond_c

    iget v2, v7, Landroid/graphics/Rect;->left:I

    iget v12, v7, Landroid/graphics/Rect;->right:I

    move v5, v2

    :goto_7
    if-ge v5, v12, :cond_b

    invoke-static {v5, v4, v3}, Laim;->k(III)J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zi:Lee;

    invoke-virtual {v2, v14, v15}, Lee;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lain;

    if-eqz v2, :cond_a

    iget v13, v2, Lain;->ZF:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_9

    const/4 v13, 0x1

    iput v13, v2, Lain;->ZF:I

    :cond_9
    :goto_8
    add-int v2, v5, v6

    move v5, v2

    goto :goto_7

    :catchall_0
    move-exception v2

    monitor-exit v5

    throw v2

    :cond_a
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4, v3}, Laim;->i(III)Lain;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v13, v0, Laim;->Zi:Lee;

    invoke-virtual {v13, v14, v15, v2}, Lee;->put(JLjava/lang/Object;)V

    goto :goto_8

    :cond_b
    add-int v2, v4, v6

    move v4, v2

    goto :goto_6

    :cond_c
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zz:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->postInvalidate()V

    goto/16 :goto_0

    .line 415
    :catchall_1
    move-exception v2

    monitor-exit v4

    throw v2

    :cond_e
    const-string v4, "TiledImageRenderer"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Tile in upload queue has invalid state: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v2, Lain;->ZF:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_f
    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zz:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->postInvalidate()V

    .line 417
    :cond_10
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Laim;->Ze:I

    .line 418
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Laim;->Zf:Z

    .line 420
    move-object/from16 v0, p0

    iget v6, v0, Laim;->Zb:I

    .line 421
    move-object/from16 v0, p0

    iget v3, v0, Laim;->TT:I

    .line 422
    const/4 v2, 0x0

    .line 423
    if-eqz v3, :cond_20

    .line 424
    const/4 v2, 0x2

    move v8, v2

    .line 427
    :goto_9
    if-eqz v8, :cond_11

    .line 428
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lrb;->aK(I)V

    .line 429
    if-eqz v3, :cond_11

    .line 430
    move-object/from16 v0, p0

    iget v2, v0, Laim;->Zx:I

    div-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zy:I

    div-int/lit8 v4, v4, 0x2

    .line 431
    int-to-float v5, v2

    int-to-float v7, v4

    move-object/from16 v0, p1

    invoke-interface {v0, v5, v7}, Lrb;->translate(FF)V

    .line 432
    int-to-float v3, v3

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v5, v7, v9}, Lrb;->rotate(FFFF)V

    .line 433
    neg-int v2, v2

    int-to-float v2, v2

    neg-int v3, v4

    int-to-float v3, v3

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lrb;->translate(FF)V

    .line 437
    :cond_11
    :try_start_2
    move-object/from16 v0, p0

    iget v2, v0, Laim;->Za:I

    if-eq v6, v2, :cond_19

    .line 438
    move-object/from16 v0, p0

    iget v2, v0, Laim;->FW:I

    shl-int v7, v2, v6

    .line 439
    int-to-float v2, v7

    move-object/from16 v0, p0

    iget v3, v0, Laim;->Zr:F

    mul-float v9, v2, v3

    .line 440
    move-object/from16 v0, p0

    iget-object v10, v0, Laim;->Zt:Landroid/graphics/Rect;

    .line 442
    iget v3, v10, Landroid/graphics/Rect;->top:I

    const/4 v2, 0x0

    :goto_a
    iget v4, v10, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v4, :cond_1a

    .line 443
    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zd:I

    int-to-float v4, v4

    int-to-float v5, v2

    mul-float/2addr v5, v9

    add-float v11, v4, v5

    .line 444
    iget v5, v10, Landroid/graphics/Rect;->left:I

    const/4 v4, 0x0

    :goto_b
    iget v12, v10, Landroid/graphics/Rect;->right:I

    if-ge v5, v12, :cond_18

    .line 445
    move-object/from16 v0, p0

    iget v12, v0, Laim;->Zc:I

    int-to-float v12, v12

    int-to-float v13, v4

    mul-float/2addr v13, v9

    add-float/2addr v12, v13

    .line 446
    move-object/from16 v0, p0

    iget-object v13, v0, Laim;->Zg:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v14, v0, Laim;->Zh:Landroid/graphics/RectF;

    add-float v15, v12, v9

    add-float v16, v11, v9

    move/from16 v0, v16

    invoke-virtual {v14, v12, v11, v15, v0}, Landroid/graphics/RectF;->set(FFFF)V

    const/4 v12, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Laim;->FW:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Laim;->FW:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v13, v12, v15, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v3, v6}, Laim;->j(III)Lain;

    move-result-object v12

    if-eqz v12, :cond_13

    invoke-virtual {v12}, Lain;->dS()Z

    move-result v15

    if-nez v15, :cond_12

    iget v15, v12, Lain;->ZF:I

    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v15, v0, :cond_17

    move-object/from16 v0, p0

    iget v15, v0, Laim;->Ze:I

    if-lez v15, :cond_15

    move-object/from16 v0, p0

    iget v15, v0, Laim;->Ze:I

    add-int/lit8 v15, v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Laim;->Ze:I

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lain;->c(Lrb;)V

    :cond_12
    :goto_c
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v1, v13, v14}, Laim;->a(Lain;Lrb;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v12

    if-nez v12, :cond_14

    :cond_13
    move-object/from16 v0, p0

    iget-object v12, v0, Laim;->YL:Lqz;

    if-eqz v12, :cond_14

    move-object/from16 v0, p0

    iget v12, v0, Laim;->FW:I

    shl-int/2addr v12, v6

    move-object/from16 v0, p0

    iget-object v15, v0, Laim;->YL:Lqz;

    invoke-virtual {v15}, Lqz;->getWidth()I

    move-result v15

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget v0, v0, Laim;->Zn:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    div-float v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Laim;->YL:Lqz;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lqz;->getHeight()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Laim;->Zo:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v16, v16, v17

    int-to-float v0, v5

    move/from16 v17, v0

    mul-float v17, v17, v15

    int-to-float v0, v3

    move/from16 v18, v0

    mul-float v18, v18, v16

    add-int v19, v5, v12

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v15, v15, v19

    add-int/2addr v12, v3

    int-to-float v12, v12

    mul-float v12, v12, v16

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1, v15, v12}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v12, v0, Laim;->YL:Lqz;

    move-object/from16 v0, p1

    invoke-interface {v0, v12, v13, v14}, Lrb;->a(Lqz;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 444
    :cond_14
    add-int/2addr v5, v7

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_b

    .line 446
    :cond_15
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Laim;->Zf:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_c

    .line 455
    :catchall_2
    move-exception v2

    if-eqz v8, :cond_16

    .line 456
    invoke-interface/range {p1 .. p1}, Lrb;->restore()V

    :cond_16
    throw v2

    .line 446
    :cond_17
    :try_start_3
    iget v15, v12, Lain;->ZF:I

    const/16 v16, 0x10

    move/from16 v0, v16

    if-eq v15, v0, :cond_12

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Laim;->Zf:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Laim;->a(Lain;)V

    goto/16 :goto_c

    .line 442
    :cond_18
    add-int/2addr v3, v7

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_a

    .line 449
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->YL:Lqz;

    if-eqz v2, :cond_1a

    .line 450
    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->YL:Lqz;

    move-object/from16 v0, p0

    iget v4, v0, Laim;->Zc:I

    move-object/from16 v0, p0

    iget v5, v0, Laim;->Zd:I

    move-object/from16 v0, p0

    iget v3, v0, Laim;->Zn:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v6, v0, Laim;->Zr:F

    mul-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v6

    move-object/from16 v0, p0

    iget v3, v0, Laim;->Zo:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v7, v0, Laim;->Zr:F

    mul-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lqz;->a(Lrb;IIII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 455
    :cond_1a
    if-eqz v8, :cond_1b

    .line 456
    invoke-interface/range {p1 .. p1}, Lrb;->restore()V

    .line 460
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Laim;->Zf:Z

    if-eqz v2, :cond_1e

    .line 461
    move-object/from16 v0, p0

    iget-boolean v2, v0, Laim;->Zw:Z

    if-nez v2, :cond_1c

    .line 462
    invoke-direct/range {p0 .. p0}, Laim;->lS()V

    .line 467
    :cond_1c
    :goto_d
    move-object/from16 v0, p0

    iget-boolean v2, v0, Laim;->Zf:Z

    if-nez v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->YL:Lqz;

    if-eqz v2, :cond_1f

    :cond_1d
    const/4 v2, 0x1

    :goto_e
    return v2

    .line 465
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Laim;->Zz:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->postInvalidate()V

    goto :goto_d

    .line 467
    :cond_1f
    const/4 v2, 0x0

    goto :goto_e

    :cond_20
    move v8, v2

    goto/16 :goto_9
.end method

.method public final lR()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 389
    const/4 v0, 0x1

    iput-boolean v0, p0, Laim;->Zs:Z

    .line 391
    iget-object v0, p0, Laim;->Zv:Laio;

    invoke-virtual {v0}, Laio;->lW()V

    .line 392
    iget-object v1, p0, Laim;->Zj:Ljava/lang/Object;

    monitor-enter v1

    .line 393
    :try_start_0
    iget-object v0, p0, Laim;->Zl:Laip;

    const/4 v3, 0x0

    iput-object v3, v0, Laip;->ZH:Lain;

    .line 394
    iget-object v0, p0, Laim;->Zm:Laip;

    const/4 v3, 0x0

    iput-object v3, v0, Laip;->ZH:Lain;

    .line 395
    iget-object v0, p0, Laim;->Zk:Laip;

    invoke-virtual {v0}, Laip;->lY()Lain;

    move-result-object v0

    .line 396
    :goto_0
    if-eqz v0, :cond_0

    .line 397
    invoke-virtual {v0}, Lain;->recycle()V

    .line 398
    iget-object v0, p0, Laim;->Zk:Laip;

    invoke-virtual {v0}, Laip;->lY()Lain;

    move-result-object v0

    goto :goto_0

    .line 400
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-virtual {v0}, Lee;->size()I

    move-result v3

    move v1, v2

    .line 403
    :goto_1
    if-ge v1, v3, :cond_1

    .line 404
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-virtual {v0, v1}, Lee;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lain;

    .line 405
    invoke-virtual {v0}, Lain;->recycle()V

    .line 403
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 407
    :cond_1
    iget-object v0, p0, Laim;->Zi:Lee;

    invoke-virtual {v0}, Lee;->clear()V

    .line 408
    iget-object v0, p0, Laim;->Zt:Landroid/graphics/Rect;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 410
    :cond_2
    sget-object v0, Laim;->YY:Lna;

    invoke-interface {v0}, Lna;->ch()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 411
    return-void
.end method
