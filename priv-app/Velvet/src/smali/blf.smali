.class public abstract Lblf;
.super Ljava/lang/Object;


# instance fields
.field protected final aEl:Lblv;

.field public final aEm:Ljava/lang/String;

.field public aEn:Lbly;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lblo;->ft(Ljava/lang/String;)V

    iput-object p1, p0, Lblf;->aEm:Ljava/lang/String;

    new-instance v0, Lblv;

    invoke-direct {v0, p2}, Lblv;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lblf;->aEl:Lblv;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lblf;->aEl:Lblv;

    invoke-virtual {v0, p3}, Lblv;->fx(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lblf;->aEl:Lblv;

    const-string v1, "Sending text message: %s to: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lblv;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lblf;->aEn:Lbly;

    iget-object v1, p0, Lblf;->aEm:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2, p3}, Lbly;->a(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public b(JI)V
    .locals 0

    return-void
.end method

.method public fr(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public zk()V
    .locals 0

    return-void
.end method
