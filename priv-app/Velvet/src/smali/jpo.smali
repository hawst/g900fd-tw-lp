.class public final Ljpo;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dWC:J

.field private dWD:J

.field private dWE:I

.field private dWF:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 249
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 250
    iput v0, p0, Ljpo;->aez:I

    iput-wide v2, p0, Ljpo;->dWC:J

    iput-wide v2, p0, Ljpo;->dWD:J

    iput v0, p0, Ljpo;->dWE:I

    const-string v0, ""

    iput-object v0, p0, Ljpo;->dWF:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljpo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpo;->eCz:I

    .line 251
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 151
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljpo;->dWC:J

    iget v0, p0, Ljpo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljpo;->dWD:J

    iget v0, p0, Ljpo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpo;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljpo;->dWE:I

    iget v0, p0, Ljpo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljpo;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpo;->dWF:Ljava/lang/String;

    iget v0, p0, Ljpo;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljpo;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 267
    iget v0, p0, Ljpo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 268
    const/4 v0, 0x1

    iget-wide v2, p0, Ljpo;->dWC:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 270
    :cond_0
    iget v0, p0, Ljpo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 271
    const/4 v0, 0x2

    iget-wide v2, p0, Ljpo;->dWD:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 273
    :cond_1
    iget v0, p0, Ljpo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 274
    const/4 v0, 0x3

    iget v1, p0, Ljpo;->dWE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 276
    :cond_2
    iget v0, p0, Ljpo;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 277
    const/4 v0, 0x4

    iget-object v1, p0, Ljpo;->dWF:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 279
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 280
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 284
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 285
    iget v1, p0, Ljpo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 286
    const/4 v1, 0x1

    iget-wide v2, p0, Ljpo;->dWC:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    :cond_0
    iget v1, p0, Ljpo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 290
    const/4 v1, 0x2

    iget-wide v2, p0, Ljpo;->dWD:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    :cond_1
    iget v1, p0, Ljpo;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 294
    const/4 v1, 0x3

    iget v2, p0, Ljpo;->dWE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    :cond_2
    iget v1, p0, Ljpo;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 298
    const/4 v1, 0x4

    iget-object v2, p0, Ljpo;->dWF:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 301
    :cond_3
    return v0
.end method
