.class public Lgix;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final cLJ:Ljava/io/File;

.field private static final cLR:Ljava/io/FileFilter;


# instance fields
.field final bhl:Ljava/util/concurrent/Executor;

.field private final cLK:[I

.field private final cLL:Lijj;

.field private final cLM:Ljava/io/File;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cLN:Ljava/util/List;

.field cLO:Lgjd;

.field private cLP:Ljava/util/Map;

.field private cLQ:I

.field final mContext:Landroid/content/Context;

.field private final mGreco3Prefs:Lgjq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mUpdateExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/usr/srec"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgix;->cLJ:Ljava/io/File;

    .line 565
    new-instance v0, Lgjc;

    invoke-direct {v0}, Lgjc;-><init>()V

    sput-object v0, Lgix;->cLR:Ljava/io/FileFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lgjq;[ILijj;Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p2    # Lgjq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/io/File;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p1, p0, Lgix;->mContext:Landroid/content/Context;

    .line 105
    iput-object p2, p0, Lgix;->mGreco3Prefs:Lgjq;

    .line 106
    iput-object p3, p0, Lgix;->cLK:[I

    .line 107
    iput-object p4, p0, Lgix;->cLL:Lijj;

    .line 108
    iget-object v0, p0, Lgix;->cLL:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 109
    invoke-virtual {v0}, Ljava/io/File;->isAbsolute()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    goto :goto_0

    .line 111
    :cond_0
    iput-object p5, p0, Lgix;->cLM:Ljava/io/File;

    .line 112
    iput-object p6, p0, Lgix;->mUpdateExecutor:Ljava/util/concurrent/Executor;

    .line 113
    iput-object p7, p0, Lgix;->bhl:Ljava/util/concurrent/Executor;

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lgix;->cLQ:I

    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lgix;->cLP:Ljava/util/Map;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgix;->cLN:Ljava/util/List;

    .line 118
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lgjq;[ILjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 94
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/io/File;

    const-string v1, "g3_models"

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    sget-object v2, Lgix;->cLJ:Ljava/io/File;

    aput-object v2, v0, v1

    invoke-static {v0}, Lgix;->a([Ljava/io/File;)Lijj;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "g3_grammars"

    invoke-direct {v5, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lgix;-><init>(Landroid/content/Context;Lgjq;[ILijj;Ljava/io/File;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    .line 97
    return-void
.end method

.method private static varargs a([Ljava/io/File;)Lijj;
    .locals 4

    .prologue
    .line 573
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 575
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    .line 576
    if-eqz v3, :cond_0

    .line 577
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 575
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 581
    :cond_1
    invoke-static {v1}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method private fL(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 207
    iget-object v0, p0, Lgix;->cLO:Lgjd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 209
    iget v0, p0, Lgix;->cLQ:I

    if-lez v0, :cond_1

    if-nez p1, :cond_1

    .line 221
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 207
    goto :goto_0

    .line 213
    :cond_1
    iget v0, p0, Lgix;->cLQ:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgix;->cLQ:I

    .line 214
    iget-object v0, p0, Lgix;->mUpdateExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lgiy;

    const-string v3, "update resource list"

    new-array v1, v1, [I

    invoke-direct {v2, p0, v3, v1}, Lgiy;-><init>(Lgix;Ljava/lang/String;[I)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method private static mQ(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/16 v2, 0x2d

    .line 562
    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgjl;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 532
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Lgix;->cLM:Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 534
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Lgjl;->aGv()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 536
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 537
    const/4 v0, 0x0

    .line 540
    :cond_0
    return-object v0
.end method

.method public final a(Lgjl;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 517
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lgix;->cLM:Ljava/io/File;

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 519
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Lgjl;->aGv()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 521
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 523
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 524
    const/4 v0, 0x0

    .line 527
    :cond_0
    return-object v0
.end method

.method public final a(Ljava/lang/String;Lgjl;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 427
    invoke-virtual {p0, p1}, Lgix;->mM(Ljava/lang/String;)Lgkd;

    move-result-object v2

    .line 428
    if-nez v2, :cond_1

    .line 441
    :cond_0
    :goto_0
    return-object v0

    .line 435
    :cond_1
    iget-object v1, p0, Lgix;->mGreco3Prefs:Lgjq;

    invoke-virtual {v1, p2}, Lgjq;->a(Lgjl;)Ljava/lang/String;

    move-result-object v1

    .line 436
    if-eqz v1, :cond_0

    invoke-virtual {v2, p2, v1}, Lgkd;->c(Lgjl;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 441
    goto :goto_0
.end method

.method public final a(Lgjd;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lgix;->cLO:Lgjd;

    .line 122
    return-void
.end method

.method final a(Ljzp;Lesk;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 545
    invoke-virtual {p1}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgix;->mContext:Landroid/content/Context;

    const-string v2, "g3_models"

    invoke-virtual {v1, v2, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 546
    iget-object v0, p0, Lgix;->cLO:Lgjd;

    const/4 v1, 0x1

    new-instance v3, Lgjb;

    const-string v4, "delete language inner"

    new-array v5, v5, [I

    invoke-direct {v3, p0, v4, v5, p2}, Lgjb;-><init>(Lgix;Ljava/lang/String;[ILesk;)V

    invoke-interface {v0, v2, v1, v3}, Lgjd;->a(Ljava/io/File;ZLjava/lang/Runnable;)V

    .line 555
    return-void
.end method

.method public final a(Ljzp;Ljava/util/concurrent/Executor;Lesk;)V
    .locals 6

    .prologue
    .line 500
    new-instance v0, Lgja;

    const-string v2, "delete language"

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lgja;-><init>(Lgix;Ljava/lang/String;[ILjzp;Lesk;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 506
    return-void
.end method

.method public final a(Ljava/lang/String;Lgjo;)Z
    .locals 1

    .prologue
    .line 413
    invoke-virtual {p0, p1}, Lgix;->mM(Ljava/lang/String;)Lgkd;

    move-result-object v0

    .line 414
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lgkd;->d(Lgjo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected declared-synchronized aGk()V
    .locals 3

    .prologue
    .line 185
    monitor-enter p0

    :goto_0
    :try_start_0
    iget v0, p0, Lgix;->cLQ:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    .line 187
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 190
    const-string v0, "Greco3DataManager"

    const-string v1, "Interrupted waiting for resource update."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 193
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final aGl()Ligi;
    .locals 1

    .prologue
    .line 225
    new-instance v0, Lgiz;

    invoke-direct {v0, p0}, Lgiz;-><init>(Lgix;)V

    invoke-static {v0}, Ligj;->b(Ligi;)Ligi;

    move-result-object v0

    return-object v0
.end method

.method public aGm()Ljava/util/Map;
    .locals 21

    .prologue
    .line 237
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v8

    .line 240
    move-object/from16 v0, p0

    iget-object v4, v0, Lgix;->cLL:Lijj;

    invoke-virtual {v4}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/File;

    sget-object v5, Lgix;->cLR:Ljava/io/FileFilter;

    invoke-virtual {v4, v5}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v7

    if-eqz v7, :cond_0

    array-length v9, v7

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v9, :cond_0

    aget-object v10, v7, v5

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lgix;->mQ(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v8, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lgkd;

    if-nez v4, :cond_1

    invoke-virtual/range {p0 .. p0}, Lgix;->aGo()Lgkd;

    move-result-object v4

    invoke-interface {v8, v11, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v4, v10}, Lgkd;->m(Ljava/io/File;)V

    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    :cond_3
    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lgkd;

    invoke-virtual {v4}, Lgkd;->aGF()V

    goto :goto_1

    .line 242
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lgix;->cLM:Ljava/io/File;

    if-eqz v4, :cond_5

    .line 246
    move-object/from16 v0, p0

    iget-object v4, v0, Lgix;->cLM:Ljava/io/File;

    sget-object v5, Lgix;->cLR:Ljava/io/FileFilter;

    invoke-virtual {v4, v5}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v9

    if-eqz v9, :cond_5

    array-length v4, v9

    if-nez v4, :cond_6

    .line 249
    :cond_5
    return-object v8

    .line 246
    :cond_6
    array-length v10, v9

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v10, :cond_a

    aget-object v6, v9, v5

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lgix;->mQ(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v8, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lgkd;

    if-eqz v4, :cond_5

    sget-object v7, Lgix;->cLR:Ljava/io/FileFilter;

    invoke-virtual {v6, v7}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v11

    if-eqz v11, :cond_9

    array-length v6, v11

    if-eqz v6, :cond_9

    array-length v12, v11

    const/4 v6, 0x0

    :goto_3
    if-ge v6, v12, :cond_9

    aget-object v7, v11, v6

    invoke-static {v7}, Lgjl;->i(Ljava/io/File;)Lgjl;

    move-result-object v13

    if-eqz v13, :cond_8

    sget-object v14, Lgix;->cLR:Ljava/io/FileFilter;

    invoke-virtual {v7, v14}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v14

    if-eqz v14, :cond_8

    array-length v7, v14

    if-eqz v7, :cond_8

    move-object/from16 v0, p0

    iget-object v7, v0, Lgix;->mGreco3Prefs:Lgjq;

    invoke-virtual {v7, v13}, Lgjq;->a(Lgjl;)Ljava/lang/String;

    move-result-object v15

    array-length v0, v14

    move/from16 v16, v0

    const/4 v7, 0x0

    :goto_4
    move/from16 v0, v16

    if-ge v7, v0, :cond_8

    aget-object v17, v14, v7

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lgix;->cLO:Lgjd;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    move/from16 v2, v19

    move-object/from16 v3, v20

    invoke-interface {v0, v1, v2, v3}, Lgjd;->a(Ljava/io/File;ZLjava/lang/Runnable;)V

    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_7
    move-object/from16 v0, v17

    invoke-virtual {v4, v13, v15, v0}, Lgkd;->a(Lgjl;Ljava/lang/String;Ljava/io/File;)V

    goto :goto_5

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_9
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :cond_a
    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_b
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lgkd;

    invoke-virtual {v4}, Lgkd;->aGG()Z

    move-result v4

    if-nez v4, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_6
.end method

.method final aGn()V
    .locals 3

    .prologue
    .line 254
    invoke-virtual {p0}, Lgix;->aGm()Ljava/util/Map;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lgix;->cLN:Ljava/util/List;

    invoke-static {v1}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 269
    monitor-enter p0

    .line 270
    :try_start_0
    iput-object v0, p0, Lgix;->cLP:Ljava/util/Map;

    .line 271
    iget-object v0, p0, Lgix;->cLN:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 272
    iget v0, p0, Lgix;->cLQ:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lgix;->cLQ:I

    .line 275
    iget v0, p0, Lgix;->cLQ:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 276
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 277
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesk;

    .line 280
    iget-object v2, p0, Lgix;->bhl:Ljava/util/concurrent/Executor;

    invoke-interface {v2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 275
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 282
    :cond_1
    return-void
.end method

.method protected aGo()Lgkd;
    .locals 2

    .prologue
    .line 306
    new-instance v0, Lgkd;

    iget-object v1, p0, Lgix;->cLK:[I

    invoke-direct {v0, v1}, Lgkd;-><init>([I)V

    return-object v0
.end method

.method public final aGp()Ljava/util/Map;
    .locals 5

    .prologue
    .line 477
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 478
    monitor-enter p0

    .line 479
    :try_start_0
    invoke-virtual {p0}, Lgix;->isInitialized()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 480
    iget-object v0, p0, Lgix;->cLP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkd;

    .line 481
    invoke-virtual {v0}, Lgkd;->aGC()Ljzp;

    move-result-object v3

    .line 484
    if-eqz v3, :cond_0

    if-eqz v0, :cond_2

    sget-object v4, Lgjo;->cMK:Lgjo;

    invoke-virtual {v0, v4}, Lgkd;->d(Lgjo;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    sget-object v4, Lgjo;->cMP:Lgjo;

    invoke-virtual {v0, v4}, Lgkd;->d(Lgjo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    .line 485
    invoke-virtual {v3}, Ljzp;->bwQ()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 488
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 484
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 488
    :cond_3
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 490
    return-object v1
.end method

.method public final aGq()Ljava/util/List;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lgix;->cLN:Ljava/util/List;

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lgjl;)Z
    .locals 1

    .prologue
    .line 445
    invoke-virtual {p0, p1, p2}, Lgix;->a(Ljava/lang/String;Lgjl;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized cV()V
    .locals 1

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lgix;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgix;->fL(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    :cond_0
    monitor-exit p0

    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized fK(Z)V
    .locals 1

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lenu;->auQ()V

    .line 179
    invoke-direct {p0, p1}, Lgix;->fL(Z)V

    .line 180
    invoke-virtual {p0}, Lgix;->aGk()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    monitor-exit p0

    return-void

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g(Lesk;)V
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lgix;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lgix;->cLN:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lgix;->bhl:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final declared-synchronized h(Lesk;)V
    .locals 1

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lgix;->g(Lesk;)V

    .line 150
    invoke-virtual {p0}, Lgix;->cV()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    monitor-exit p0

    return-void

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized isInitialized()Z
    .locals 1

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgix;->cLP:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final mM(Ljava/lang/String;)Lgkd;
    .locals 1

    .prologue
    .line 403
    monitor-enter p0

    .line 404
    :try_start_0
    invoke-virtual {p0}, Lgix;->isInitialized()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 405
    iget-object v0, p0, Lgix;->cLP:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkd;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 406
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final mN(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 418
    sget-object v0, Lgjo;->cMO:Lgjo;

    invoke-virtual {p0, p1, v0}, Lgix;->a(Ljava/lang/String;Lgjo;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lgjo;->cMP:Lgjo;

    invoke-virtual {p0, p1, v0}, Lgix;->a(Ljava/lang/String;Lgjo;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mO(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0, p1}, Lgix;->mM(Ljava/lang/String;)Lgkd;

    move-result-object v0

    .line 459
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgkd;->aGD()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mP(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 467
    invoke-virtual {p0, p1}, Lgix;->mM(Ljava/lang/String;)Lgkd;

    move-result-object v0

    .line 468
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgkd;->aGE()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
