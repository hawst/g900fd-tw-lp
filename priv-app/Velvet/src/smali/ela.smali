.class public final Lela;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cer:I

.field private final ces:I

.field private final cet:[Ljava/util/List;


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p2, p0, Lela;->cer:I

    .line 39
    iput p1, p0, Lela;->ces:I

    .line 40
    iget v0, p0, Lela;->ces:I

    new-array v0, v0, [Ljava/util/List;

    iput-object v0, p0, Lela;->cet:[Ljava/util/List;

    .line 41
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lela;->ces:I

    if-ge v0, v1, :cond_0

    .line 42
    iget-object v1, p0, Lela;->cet:[Ljava/util/List;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public final g(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 92
    if-ltz p2, :cond_0

    .line 93
    iget-object v0, p0, Lela;->cet:[Ljava/util/List;

    aget-object v0, v0, p2

    .line 94
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lela;->cer:I

    invoke-static {p3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 95
    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 96
    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 97
    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 98
    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 99
    invoke-virtual {p1, v3}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 100
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_0
    return-void
.end method

.method public final ie(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 64
    if-ltz p1, :cond_1

    iget-object v1, p0, Lela;->cet:[Ljava/util/List;

    aget-object v1, v1, p1

    .line 67
    :goto_0
    if-eqz v1, :cond_0

    .line 68
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 69
    if-lez v2, :cond_0

    .line 71
    add-int/lit8 v0, v2, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 81
    :cond_0
    return-object v0

    :cond_1
    move-object v1, v0

    .line 64
    goto :goto_0
.end method
