.class public Lck;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cq:Z

.field public cs:I

.field private dB:Lcm;

.field public dC:Z

.field public dD:Z

.field dE:Z

.field dF:Z

.field public mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v1, p0, Lck;->cq:Z

    .line 39
    iput-boolean v1, p0, Lck;->dC:Z

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lck;->dD:Z

    .line 41
    iput-boolean v1, p0, Lck;->dE:Z

    .line 42
    iput-boolean v1, p0, Lck;->dF:Z

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lck;->mContext:Landroid/content/Context;

    .line 93
    return-void
.end method


# virtual methods
.method public final a(ILcm;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lck;->dB:Lcm;

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is already a listener registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    iput-object p2, p0, Lck;->dB:Lcm;

    .line 134
    iput p1, p0, Lck;->cs:I

    .line 135
    return-void
.end method

.method public final a(Lcm;)V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lck;->dB:Lcm;

    if-nez v0, :cond_0

    .line 144
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No listener register"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_0
    iget-object v0, p0, Lck;->dB:Lcm;

    if-eq v0, p1, :cond_1

    .line 147
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to unregister the wrong listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lck;->dB:Lcm;

    .line 150
    return-void
.end method

.method public deliverResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lck;->dB:Lcm;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lck;->dB:Lcm;

    invoke-interface {v0, p0, p1}, Lcm;->b(Lck;Ljava/lang/Object;)V

    .line 106
    :cond_0
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 393
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mId="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lck;->cs:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 394
    const-string v0, " mListener="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lck;->dB:Lcm;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 395
    iget-boolean v0, p0, Lck;->cq:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lck;->dE:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lck;->dF:Z

    if-eqz v0, :cond_1

    .line 396
    :cond_0
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStarted="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lck;->cq:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 397
    const-string v0, " mContentChanged="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lck;->dE:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 398
    const-string v0, " mProcessingChange="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lck;->dF:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 400
    :cond_1
    iget-boolean v0, p0, Lck;->dC:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lck;->dD:Z

    if-eqz v0, :cond_3

    .line 401
    :cond_2
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAbandoned="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lck;->dC:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 402
    const-string v0, " mReset="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lck;->dD:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 404
    :cond_3
    return-void
.end method

.method public final forceLoad()V
    .locals 0

    .prologue
    .line 218
    invoke-virtual {p0}, Lck;->onForceLoad()V

    .line 219
    return-void
.end method

.method public final onContentChanged()V
    .locals 1

    .prologue
    .line 353
    iget-boolean v0, p0, Lck;->cq:Z

    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {p0}, Lck;->onForceLoad()V

    .line 361
    :goto_0
    return-void

    .line 359
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lck;->dE:Z

    goto :goto_0
.end method

.method protected onForceLoad()V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method public onReset()V
    .locals 0

    .prologue
    .line 306
    return-void
.end method

.method public onStartLoading()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 254
    return-void
.end method

.method public final reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 291
    invoke-virtual {p0}, Lck;->onReset()V

    .line 292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lck;->dD:Z

    .line 293
    iput-boolean v1, p0, Lck;->cq:Z

    .line 294
    iput-boolean v1, p0, Lck;->dC:Z

    .line 295
    iput-boolean v1, p0, Lck;->dE:Z

    .line 296
    iput-boolean v1, p0, Lck;->dF:Z

    .line 297
    return-void
.end method

.method public final startLoading()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Lck;->cq:Z

    .line 196
    iput-boolean v1, p0, Lck;->dD:Z

    .line 197
    iput-boolean v1, p0, Lck;->dC:Z

    .line 198
    invoke-virtual {p0}, Lck;->onStartLoading()V

    .line 199
    return-void
.end method

.method public final stopLoading()V
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lck;->cq:Z

    .line 244
    invoke-virtual {p0}, Lck;->onStopLoading()V

    .line 245
    return-void
.end method

.method public final takeContentChanged()Z
    .locals 2

    .prologue
    .line 314
    iget-boolean v0, p0, Lck;->dE:Z

    .line 315
    const/4 v1, 0x0

    iput-boolean v1, p0, Lck;->dE:Z

    .line 316
    iget-boolean v1, p0, Lck;->dF:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Lck;->dF:Z

    .line 317
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 377
    invoke-static {p0, v0}, Lec;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 378
    const-string v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    iget v1, p0, Lck;->cs:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 380
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
