.class public final Lisr;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a([BLjava/io/File;)V
    .locals 3

    .prologue
    .line 247
    invoke-static {p1}, Lisr;->v(Ljava/io/File;)Lisv;

    move-result-object v0

    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Lisv;->auX()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    :try_start_0
    invoke-virtual {v0, p0}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lisq;->a(Ljava/io/Closeable;Z)V

    return-void

    :catchall_0
    move-exception v1

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lisq;->a(Ljava/io/Closeable;Z)V

    throw v1
.end method

.method public static e(Ljava/io/File;)[B
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 194
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 195
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 197
    invoke-static {p0}, Lisr;->u(Ljava/io/File;)Lisu;

    move-result-object v0

    invoke-static {v0}, Liso;->a(Lisu;)[B

    move-result-object v0

    .line 209
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 194
    goto :goto_0

    .line 200
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v0, v4

    new-array v0, v0, [B

    .line 201
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 204
    :try_start_0
    invoke-static {v3, v0}, Liso;->a(Ljava/io/InputStream;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    invoke-static {v3, v2}, Lisq;->a(Ljava/io/Closeable;Z)V

    goto :goto_1

    .line 207
    :catchall_0
    move-exception v0

    invoke-static {v3, v1}, Lisq;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method

.method public static u(Ljava/io/File;)Lisu;
    .locals 1

    .prologue
    .line 101
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    new-instance v0, Liss;

    invoke-direct {v0, p0}, Liss;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public static v(Ljava/io/File;)Lisv;
    .locals 2

    .prologue
    .line 119
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, List;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, List;-><init>(Ljava/io/File;Z)V

    return-object v0
.end method
