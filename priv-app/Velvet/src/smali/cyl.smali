.class public final Lcyl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcyh;


# instance fields
.field private aVe:Z

.field final synthetic bmW:Lcyi;

.field private final bmZ:Ljava/lang/Object;

.field private bna:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcyi;)V
    .locals 1

    .prologue
    .line 688
    iput-object p1, p0, Lcyl;->bmW:Lcyi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 689
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcyl;->bmZ:Ljava/lang/Object;

    .line 691
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyl;->bna:Ljava/util/Map;

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 770
    iget-object v1, p0, Lcyl;->bmZ:Ljava/lang/Object;

    monitor-enter v1

    .line 771
    :try_start_0
    iget-object v0, p0, Lcyl;->bna:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 772
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private cQ(Z)Z
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 776
    .line 781
    iget-object v0, p0, Lcyl;->bmW:Lcyi;

    iget-object v8, v0, Lcyi;->dK:Ljava/lang/Object;

    monitor-enter v8

    .line 783
    :try_start_0
    iget-object v0, p0, Lcyl;->bmW:Lcyi;

    iget-object v0, v0, Lcyi;->aVd:Ljava/util/Map;

    .line 784
    iget-object v1, p0, Lcyl;->bmW:Lcyi;

    iget-object v1, v1, Lcyi;->bmV:Lcyn;

    if-eqz v1, :cond_13

    iget-object v1, p0, Lcyl;->bmW:Lcyi;

    iget-object v1, v1, Lcyi;->bmV:Lcyn;

    iget-object v1, v1, Lcyn;->bnd:Ljava/util/Map;

    if-ne v1, v0, :cond_13

    .line 785
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 790
    :goto_0
    iget-object v9, p0, Lcyl;->bmZ:Ljava/lang/Object;

    monitor-enter v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 791
    :try_start_1
    iget-boolean v0, p0, Lcyl;->aVe:Z

    if-eqz v0, :cond_6

    .line 796
    iget-object v0, p0, Lcyl;->bna:Ljava/util/Map;

    iget-object v1, p0, Lcyl;->bmW:Lcyi;

    iget-object v1, v1, Lcyi;->bmS:Ljava/lang/Object;

    invoke-static {v1}, Lifx;->bg(Ljava/lang/Object;)Lifw;

    move-result-object v1

    invoke-static {v0, v1}, Lior;->a(Ljava/util/Map;Lifw;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 797
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyl;->bna:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyl;->bmW:Lcyi;

    iget-boolean v0, v0, Lcyi;->bmT:Z

    if-nez v0, :cond_12

    .line 799
    :cond_0
    new-instance v5, Ljava/util/HashSet;

    iget-object v0, p0, Lcyl;->bna:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 800
    iget-object v1, p0, Lcyl;->bna:Ljava/util/Map;

    .line 801
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcyl;->bna:Ljava/util/Map;

    .line 802
    iget-object v0, p0, Lcyl;->bmW:Lcyi;

    const/4 v6, 0x1

    iput-boolean v6, v0, Lcyi;->bmT:Z

    .line 803
    iget-object v0, p0, Lcyl;->bmZ:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    move v0, v2

    .line 805
    :goto_1
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcyl;->aVe:Z

    move-object v6, v1

    move-object v7, v5

    move v1, v0

    .line 827
    :goto_2
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 830
    if-eqz v7, :cond_11

    :try_start_2
    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcyl;->bmW:Lcyi;

    iget-object v0, v0, Lcyi;->bmU:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    .line 831
    new-instance v0, Ljava/util/ArrayList;

    iget-object v5, p0, Lcyl;->bmW:Lcyi;

    iget-object v5, v5, Lcyi;->bmU:Ljava/util/Set;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v5, v0

    .line 834
    :goto_3
    if-eqz v1, :cond_10

    .line 847
    iget-object v0, p0, Lcyl;->bmW:Lcyi;

    iget-object v0, v0, Lcyi;->bmV:Lcyn;

    if-nez v0, :cond_d

    move v0, v2

    .line 848
    :goto_4
    iget-object v1, p0, Lcyl;->bmW:Lcyi;

    iget-object v1, v1, Lcyi;->bmV:Lcyn;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcyl;->bmW:Lcyi;

    iget-object v1, v1, Lcyi;->bmV:Lcyn;

    iget-object v1, v1, Lcyn;->bnd:Ljava/util/Map;

    iget-object v9, p0, Lcyl;->bmW:Lcyi;

    iget-object v9, v9, Lcyi;->aVd:Ljava/util/Map;

    if-ne v1, v9, :cond_2

    .line 849
    :cond_1
    iget-object v1, p0, Lcyl;->bmW:Lcyi;

    new-instance v9, Lcyn;

    iget-object v10, p0, Lcyl;->bmW:Lcyi;

    invoke-direct {v9, v10}, Lcyn;-><init>(Lcyi;)V

    iput-object v9, v1, Lcyi;->bmV:Lcyn;

    .line 857
    :cond_2
    iget-object v1, p0, Lcyl;->bmW:Lcyi;

    iput-object v6, v1, Lcyi;->aVd:Ljava/util/Map;

    move v1, v0

    .line 861
    :goto_5
    if-eqz p1, :cond_f

    .line 862
    iget-object v0, p0, Lcyl;->bmW:Lcyi;

    iget-object v0, v0, Lcyi;->bmV:Lcyn;

    .line 863
    iget-object v4, p0, Lcyl;->bmW:Lcyi;

    iget v4, v4, Lcyi;->aXn:I

    if-eqz v4, :cond_3

    .line 864
    const-string v4, "Search.SharedPreferencesProto"

    const-string v6, "potential deadlock: commit while delayWrites"

    new-instance v9, Ljava/lang/Throwable;

    invoke-direct {v9}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4, v6, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 867
    :cond_3
    :goto_6
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 871
    if-eqz v1, :cond_4

    .line 874
    new-instance v1, Lcym;

    const-string v4, "Search.SharedPreferencesProto"

    invoke-direct {v1, p0, v4}, Lcym;-><init>(Lcyl;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcym;->start()V

    .line 884
    :cond_4
    if-eqz v5, :cond_5

    .line 885
    iget-object v1, p0, Lcyl;->bmW:Lcyi;

    invoke-virtual {v1, v5, v7}, Lcyi;->a(Ljava/util/Collection;Ljava/util/Set;)V

    .line 889
    :cond_5
    if-eqz v0, :cond_e

    .line 891
    :try_start_3
    iget-object v1, v0, Lcyn;->bnc:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 892
    iget-boolean v3, v0, Lcyn;->bne:Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 898
    :goto_7
    return v3

    .line 807
    :cond_6
    :try_start_4
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 808
    iget-object v0, p0, Lcyl;->bna:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_7
    :goto_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 809
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 810
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    .line 811
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    .line 812
    iget-object v11, p0, Lcyl;->bmW:Lcyi;

    iget-boolean v11, v11, Lcyi;->bmT:Z

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcyl;->bmW:Lcyi;

    iget-object v11, v11, Lcyi;->bmS:Ljava/lang/Object;

    if-ne v0, v11, :cond_8

    .line 813
    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 814
    invoke-interface {v5, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_8

    .line 827
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v9

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 867
    :catchall_1
    move-exception v0

    monitor-exit v8

    throw v0

    .line 817
    :cond_8
    :try_start_6
    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    if-nez v0, :cond_9

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    if-nez v11, :cond_a

    :cond_9
    if-eqz v0, :cond_7

    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 820
    :cond_a
    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 824
    :cond_b
    iget-object v0, p0, Lcyl;->bna:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 825
    invoke-interface {v6}, Ljava/util/Set;->isEmpty()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v0

    if-nez v0, :cond_c

    move v0, v2

    :goto_9
    move v1, v0

    move-object v7, v6

    move-object v6, v5

    goto/16 :goto_2

    :cond_c
    move v0, v3

    goto :goto_9

    :cond_d
    move v0, v3

    .line 847
    goto/16 :goto_4

    .line 894
    :catch_0
    move-exception v0

    .line 895
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_7

    :cond_e
    move v3, v2

    goto/16 :goto_7

    :cond_f
    move-object v0, v4

    goto/16 :goto_6

    :cond_10
    move v1, v3

    goto/16 :goto_5

    :cond_11
    move-object v5, v4

    goto/16 :goto_3

    :cond_12
    move v0, v3

    move-object v1, v5

    move-object v5, v4

    goto/16 :goto_1

    :cond_13
    move-object v5, v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final B(Ljava/lang/String;Ljava/lang/String;)Lcyh;
    .locals 0

    .prologue
    .line 741
    invoke-direct {p0, p1, p2}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 742
    return-object p0
.end method

.method public final EL()Lcyh;
    .locals 2

    .prologue
    .line 709
    iget-object v1, p0, Lcyl;->bmZ:Ljava/lang/Object;

    monitor-enter v1

    .line 710
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcyl;->aVe:Z

    .line 711
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 712
    return-object p0

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final apply()V
    .locals 1

    .prologue
    .line 698
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcyl;->cQ(Z)Z

    .line 699
    return-void
.end method

.method public final b(Ljava/lang/String;F)Lcyh;
    .locals 1

    .prologue
    .line 723
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 724
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/util/Set;)Lcyh;
    .locals 1

    .prologue
    .line 747
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 748
    return-object p0

    .line 747
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;[I)Lcyh;
    .locals 2

    .prologue
    .line 759
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 760
    return-object p0

    .line 759
    :cond_0
    new-instance v1, Leqj;

    invoke-virtual {p2}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    invoke-direct {v1, v0}, Leqj;-><init>([I)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;J)Lcyh;
    .locals 2

    .prologue
    .line 735
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 736
    return-object p0
.end method

.method public final c(Ljava/lang/String;[B)Lcyh;
    .locals 2

    .prologue
    .line 753
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 754
    return-object p0

    .line 753
    :cond_0
    new-instance v1, Leqi;

    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, Leqi;-><init>([B)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final synthetic clear()Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 687
    invoke-virtual {p0}, Lcyl;->EL()Lcyh;

    move-result-object v0

    return-object v0
.end method

.method public final commit()Z
    .locals 1

    .prologue
    .line 704
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcyl;->cQ(Z)Z

    move-result v0

    return v0
.end method

.method public final gl(Ljava/lang/String;)Lcyh;
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lcyl;->bmW:Lcyi;

    iget-object v0, v0, Lcyi;->bmS:Ljava/lang/Object;

    invoke-direct {p0, p1, v0}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 766
    return-object p0
.end method

.method public final h(Ljava/lang/String;Z)Lcyh;
    .locals 1

    .prologue
    .line 717
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 718
    return-object p0
.end method

.method public final l(Ljava/lang/String;I)Lcyh;
    .locals 1

    .prologue
    .line 729
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcyl;->c(Ljava/lang/String;Ljava/lang/Object;)V

    .line 730
    return-object p0
.end method

.method public final synthetic putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 687
    invoke-virtual {p0, p1, p2}, Lcyl;->h(Ljava/lang/String;Z)Lcyh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 687
    invoke-virtual {p0, p1, p2}, Lcyl;->b(Ljava/lang/String;F)Lcyh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 687
    invoke-virtual {p0, p1, p2}, Lcyl;->l(Ljava/lang/String;I)Lcyh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;
    .locals 2

    .prologue
    .line 687
    invoke-virtual {p0, p1, p2, p3}, Lcyl;->c(Ljava/lang/String;J)Lcyh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 687
    invoke-virtual {p0, p1, p2}, Lcyl;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 687
    invoke-virtual {p0, p1, p2}, Lcyl;->b(Ljava/lang/String;Ljava/util/Set;)Lcyh;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 687
    invoke-virtual {p0, p1}, Lcyl;->gl(Ljava/lang/String;)Lcyh;

    move-result-object v0

    return-object v0
.end method
