.class public final Lamb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aeq:Lamd;

.field private final aer:Lamc;

.field private volatile aes:Z

.field private volatile aet:Z

.field private aeu:J

.field private aev:J

.field private aew:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.######"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v0, p0, Lamb;->aes:Z

    .line 36
    iput-boolean v0, p0, Lamb;->aet:Z

    .line 44
    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lamb;->aeu:J

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lamb;->aew:Z

    .line 49
    iput-object v2, p0, Lamb;->aeq:Lamd;

    .line 50
    iput-object v2, p0, Lamb;->aer:Lamc;

    .line 51
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lamd;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v2, p0, Lamb;->aes:Z

    .line 36
    iput-boolean v2, p0, Lamb;->aet:Z

    .line 44
    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lamb;->aeu:J

    .line 46
    iput-boolean v3, p0, Lamb;->aew:Z

    .line 54
    if-nez p1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trackingId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    iput-object p2, p0, Lamb;->aeq:Lamd;

    .line 58
    new-instance v0, Lamc;

    invoke-direct {v0, v2}, Lamc;-><init>(B)V

    iput-object v0, p0, Lamb;->aer:Lamc;

    .line 60
    iget-object v0, p0, Lamb;->aer:Lamc;

    const-string v1, "trackingId"

    invoke-virtual {v0, v1, p1}, Lamc;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lamb;->aer:Lamc;

    const-string v1, "sampleRate"

    const-string v2, "100"

    invoke-virtual {v0, v1, v2}, Lamc;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lamb;->aer:Lamc;

    const-string v1, "sessionControl"

    const-string v2, "start"

    invoke-virtual {v0, v1, v2}, Lamc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lamb;->aer:Lamc;

    const-string v1, "useSecure"

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lamc;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 375
    iput-boolean v1, p0, Lamb;->aet:Z

    .line 376
    if-nez p2, :cond_0

    .line 377
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 379
    :cond_0
    const-string v0, "hitType"

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    iget-object v0, p0, Lamb;->aer:Lamc;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lamc;->a(Ljava/util/Map;Ljava/lang/Boolean;)V

    .line 381
    invoke-direct {p0}, Lamb;->mJ()Z

    move-result v0

    if-nez v0, :cond_1

    .line 382
    const-string v0, "Too many hits sent too quickly, throttling invoked."

    .line 386
    :goto_0
    iget-object v0, p0, Lamb;->aer:Lamc;

    invoke-virtual {v0}, Lamc;->mK()V

    .line 387
    return-void

    .line 384
    :cond_1
    iget-object v0, p0, Lamb;->aeq:Lamd;

    iget-object v1, p0, Lamb;->aer:Lamc;

    invoke-virtual {v1}, Lamc;->mL()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lamd;->a(Ljava/util/Map;)V

    goto :goto_0
.end method

.method private declared-synchronized mJ()Z
    .locals 12

    .prologue
    const-wide/32 v6, 0x1d4c0

    const-wide/16 v10, 0x7d0

    const/4 v0, 0x1

    .line 766
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lamb;->aew:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 782
    :goto_0
    monitor-exit p0

    return v0

    .line 769
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 770
    iget-wide v4, p0, Lamb;->aeu:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_1

    .line 771
    iget-wide v4, p0, Lamb;->aev:J

    sub-long v4, v2, v4

    .line 772
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 773
    const-wide/32 v6, 0x1d4c0

    iget-wide v8, p0, Lamb;->aeu:J

    add-long/2addr v4, v8

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, p0, Lamb;->aeu:J

    .line 776
    :cond_1
    iput-wide v2, p0, Lamb;->aev:J

    .line 777
    iget-wide v2, p0, Lamb;->aeu:J

    cmp-long v1, v2, v10

    if-ltz v1, :cond_2

    .line 778
    iget-wide v2, p0, Lamb;->aeu:J

    sub-long/2addr v2, v10

    iput-wide v2, p0, Lamb;->aeu:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 766
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 781
    :cond_2
    :try_start_2
    const-string v0, "Excessive tracking detected.  Tracking call ignored."
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 782
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final J(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 186
    .line 187
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "trackView requires a appScreen to be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_0
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    sget-object v1, Lale;->acK:Lale;

    invoke-virtual {v0, v1}, Lald;->a(Lale;)V

    .line 191
    iget-object v0, p0, Lamb;->aer:Lamc;

    const-string v1, "description"

    invoke-virtual {v0, v1, p1}, Lamc;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v0, "appview"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lamb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 193
    return-void
.end method

.method public final a(D)V
    .locals 3

    .prologue
    .line 455
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    sget-object v1, Lale;->adf:Lale;

    invoke-virtual {v0, v1}, Lald;->a(Lale;)V

    .line 456
    iget-object v0, p0, Lamb;->aer:Lamc;

    const-string v1, "sampleRate"

    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lamc;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    return-void
.end method

.method public final a(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 322
    .line 323
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    sget-object v1, Lale;->acQ:Lale;

    invoke-virtual {v0, v1}, Lald;->a(Lale;)V

    .line 324
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lald;->aA(Z)V

    .line 325
    const-string v0, "timing"

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "timingCategory"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "timingValue"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "timingVar"

    invoke-interface {v1, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "timingLabel"

    invoke-interface {v1, v2, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lald;->mD()Lald;

    move-result-object v2

    sget-object v3, Lale;->ads:Lale;

    invoke-virtual {v2, v3}, Lald;->a(Lale;)V

    invoke-direct {p0, v0, v1}, Lamb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 327
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lald;->aA(Z)V

    .line 328
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6

    .prologue
    .line 213
    .line 214
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    sget-object v1, Lale;->acL:Lale;

    invoke-virtual {v0, v1}, Lald;->a(Lale;)V

    .line 215
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lald;->aA(Z)V

    .line 216
    const-string v0, "event"

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "eventCategory"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "eventAction"

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "eventLabel"

    invoke-interface {v1, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    const-string v2, "eventValue"

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v2

    sget-object v3, Lale;->adH:Lale;

    invoke-virtual {v2, v3}, Lald;->a(Lale;)V

    invoke-direct {p0, v0, v1}, Lamb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 217
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lald;->aA(Z)V

    .line 218
    return-void
.end method

.method public final aC(Z)V
    .locals 3

    .prologue
    .line 99
    .line 100
    invoke-static {}, Lald;->mD()Lald;

    move-result-object v0

    sget-object v1, Lale;->acV:Lale;

    invoke-virtual {v0, v1}, Lald;->a(Lale;)V

    .line 101
    iget-object v0, p0, Lamb;->aer:Lamc;

    const-string v1, "sessionControl"

    const-string v2, "start"

    invoke-virtual {v0, v1, v2}, Lamc;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void
.end method
