.class public final Lfzc;
.super Lfro;
.source "PG"


# instance fields
.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;Lemp;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 28
    iput-object p4, p0, Lfzc;->mClock:Lemp;

    .line 29
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 7

    .prologue
    .line 39
    new-instance v0, Lfze;

    iget-object v1, p0, Lfzc;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lfzc;->mCardContainer:Lfmt;

    invoke-virtual {p0}, Lfzc;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lfzc;->mActivityHelper:Lfzw;

    iget-object v5, p0, Lfro;->cBc:Lanh;

    iget-object v5, v5, Lanh;->ahu:Lizj;

    iget-object v6, p0, Lfzc;->mClock:Lemp;

    invoke-direct/range {v0 .. v6}, Lfze;-><init>(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Lfzw;Lizj;Lemp;)V

    .line 41
    iget-object v1, p0, Lfro;->mView:Landroid/view/View;

    iget-object v2, p0, Lfro;->cBc:Lanh;

    iget-object v2, v2, Lanh;->ahi:Lapc;

    invoke-virtual {v0, v1, v2}, Lfze;->a(Landroid/view/View;Lapc;)V

    .line 42
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 4

    .prologue
    .line 33
    invoke-virtual {p0}, Lfzc;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040136

    iget-object v2, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v2}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
