.class public final Lfgm;
.super Lfet;
.source "PG"


# instance fields
.field private final cpV:Ljhk;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cqS:Ljak;


# direct methods
.method public constructor <init>(Lizj;Ljbp;Lgah;Lemp;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lfet;-><init>(Lizj;Ljbp;Lgah;Lemp;)V

    .line 41
    iget-object v0, p0, Lfgm;->mFrequentPlaceEntry:Ljal;

    iget-object v0, v0, Ljal;->dWL:Ljak;

    iput-object v0, p0, Lfgm;->cqS:Ljak;

    .line 42
    iget-object v0, p1, Lizj;->dUv:Ljhk;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lizj;->dUv:Ljhk;

    :goto_0
    iput-object v0, p0, Lfgm;->cpV:Ljhk;

    .line 43
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aJ(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 77
    iget-object v0, p0, Lfgm;->mTravelReport:Lgca;

    invoke-virtual {v0}, Lgca;->aEB()Ljava/lang/Integer;

    move-result-object v0

    .line 78
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    .line 80
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3c

    .line 81
    iget-object v1, p0, Lfgm;->mFrequentPlaceEntry:Ljal;

    invoke-virtual {v1}, Ljal;->bek()J

    move-result-wide v2

    int-to-long v0, v0

    sub-long v0, v2, v0

    .line 83
    new-instance v2, Ljava/util/Date;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 84
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 85
    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 48
    const v0, 0x7f0a01e1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfgm;->cqS:Ljak;

    invoke-static {p1, v3}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aH(Landroid/content/Context;)Landroid/content/Intent;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 146
    iget-object v0, p0, Lfgm;->cqS:Ljak;

    invoke-virtual {v0}, Ljak;->bed()I

    move-result v0

    .line 148
    packed-switch v0, :pswitch_data_0

    .line 167
    const-string v1, "TimeToLeaveNotification"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported source type for time to leave notification: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const/4 v0, 0x0

    .line 170
    :goto_0
    return-object v0

    .line 150
    :pswitch_0
    const v0, 0x7f0a0499

    new-array v1, v8, [Ljava/lang/Object;

    iget-object v2, p0, Lfgm;->cqS:Ljak;

    invoke-static {p1, v2}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-direct {p0, p1}, Lfgm;->aJ(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 170
    :goto_1
    new-instance v1, Lffz;

    invoke-direct {v1, v0}, Lffz;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lfgm;->cqS:Ljak;

    iget-object v0, v0, Ljak;->aeB:Ljbp;

    invoke-virtual {v1, v0, p1}, Lffz;->a(Ljbp;Landroid/content/Context;)Lffz;

    move-result-object v0

    invoke-virtual {v0}, Lffz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 155
    :pswitch_1
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lfgm;->mFrequentPlaceEntry:Ljal;

    invoke-virtual {v1}, Ljal;->bek()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 157
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 158
    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 160
    const v1, 0x7f0a049a

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lfgm;->cqS:Ljak;

    invoke-static {p1, v3}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    aput-object v0, v2, v7

    invoke-direct {p0, p1}, Lfgm;->aJ(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 148
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final aI(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const v4, 0x7f0a01e2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 55
    iget-object v1, p0, Lfgm;->mTravelReport:Lgca;

    if-nez v1, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-object v0

    .line 56
    :cond_1
    iget-object v1, p0, Lfgm;->mFrequentPlaceEntry:Ljal;

    invoke-virtual {v1}, Ljal;->bel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    iget-object v0, p0, Lfgm;->cpV:Ljhk;

    if-nez v0, :cond_2

    .line 58
    new-array v0, v7, [Ljava/lang/Object;

    invoke-direct {p0, p1}, Lfgm;->aJ(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p1, v4, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 61
    :cond_2
    iget-object v0, p0, Lfgm;->cpV:Ljhk;

    iget-object v0, v0, Ljhk;->elQ:Ljhj;

    invoke-virtual {v0}, Ljhj;->blN()I

    move-result v0

    .line 63
    if-nez v0, :cond_3

    .line 64
    new-array v0, v7, [Ljava/lang/Object;

    iget-object v1, p0, Lfgm;->cpV:Ljhk;

    invoke-virtual {v1}, Ljhk;->blO()J

    move-result-wide v2

    invoke-static {p1, v2, v3, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-virtual {p1, v4, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_3
    const v1, 0x7f0a01e3

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lfgm;->cpV:Ljhk;

    invoke-virtual {v3}, Ljhk;->blO()J

    move-result-wide v4

    invoke-static {p1, v4, v5, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v6

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ayS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    const-string v0, "event"

    return-object v0
.end method

.method public final ayU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lfgm;->cqS:Ljak;

    invoke-virtual {v0}, Ljak;->bed()I

    move-result v0

    .line 132
    packed-switch v0, :pswitch_data_0

    .line 140
    invoke-super {p0}, Lfet;->ayU()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 134
    :pswitch_0
    const-string v0, "GmailRestaurantReservation"

    goto :goto_0

    .line 136
    :pswitch_1
    const-string v0, "GmailEventReservation"

    goto :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final ayW()I
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x2

    return v0
.end method

.method public final ayY()I
    .locals 1

    .prologue
    .line 90
    const v0, 0x7f020224

    return v0
.end method

.method public final ayZ()Lfgb;
    .locals 4

    .prologue
    .line 106
    iget-object v0, p0, Lfgm;->cqS:Ljak;

    invoke-virtual {v0}, Ljak;->bed()I

    move-result v0

    .line 108
    packed-switch v0, :pswitch_data_0

    .line 116
    const-string v1, "TimeToLeaveNotification"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported source type for time to leave notification: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    sget-object v0, Lfgb;->cqC:Lfgb;

    :goto_0
    return-object v0

    .line 110
    :pswitch_0
    sget-object v0, Lfgb;->cqB:Lfgb;

    goto :goto_0

    .line 112
    :pswitch_1
    sget-object v0, Lfgb;->cqC:Lfgb;

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0, p1, p2}, Lfgm;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
