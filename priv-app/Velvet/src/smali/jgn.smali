.class public final Ljgn;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ekl:[Ljgn;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private ahq:I

.field private ajX:Ljava/lang/String;

.field private ejt:Ljava/lang/String;

.field private eju:Z

.field private ejv:Ljava/lang/String;

.field private ekm:Ljava/lang/String;

.field private ekn:Ljava/lang/String;

.field private eko:Z

.field private ekp:Ljava/lang/String;

.field private ekq:I

.field private ekr:I

.field public eks:Ljgk;

.field public ekt:Ljgo;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 52896
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 52897
    iput v1, p0, Ljgn;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljgn;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljgn;->ejt:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljgn;->ekm:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljgn;->ekn:Ljava/lang/String;

    iput-boolean v1, p0, Ljgn;->eko:Z

    const-string v0, ""

    iput-object v0, p0, Ljgn;->ekp:Ljava/lang/String;

    iput v1, p0, Ljgn;->ekq:I

    iput v1, p0, Ljgn;->ekr:I

    const-string v0, ""

    iput-object v0, p0, Ljgn;->ajX:Ljava/lang/String;

    iput-object v2, p0, Ljgn;->eks:Ljgk;

    iput-boolean v1, p0, Ljgn;->eju:Z

    const-string v0, ""

    iput-object v0, p0, Ljgn;->ejv:Ljava/lang/String;

    iput-object v2, p0, Ljgn;->ekt:Ljgo;

    iput v1, p0, Ljgn;->ahq:I

    iput-object v2, p0, Ljgn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgn;->eCz:I

    .line 52898
    return-void
.end method

.method public static bkz()[Ljgn;
    .locals 2

    .prologue
    .line 52628
    sget-object v0, Ljgn;->ekl:[Ljgn;

    if-nez v0, :cond_1

    .line 52629
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 52631
    :try_start_0
    sget-object v0, Ljgn;->ekl:[Ljgn;

    if-nez v0, :cond_0

    .line 52632
    const/4 v0, 0x0

    new-array v0, v0, [Ljgn;

    sput-object v0, Ljgn;->ekl:[Ljgn;

    .line 52634
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52636
    :cond_1
    sget-object v0, Ljgn;->ekl:[Ljgn;

    return-object v0

    .line 52634
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 52622
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgn;->agq:Ljava/lang/String;

    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljgn;->eko:Z

    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljgn;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgn;->ekq:I

    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljgn;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljgn;->ekr:I

    iget v0, p0, Ljgn;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljgn;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgn;->ajX:Ljava/lang/String;

    iget v0, p0, Ljgn;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljgn;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljgn;->eks:Ljgk;

    if-nez v0, :cond_1

    new-instance v0, Ljgk;

    invoke-direct {v0}, Ljgk;-><init>()V

    iput-object v0, p0, Ljgn;->eks:Ljgk;

    :cond_1
    iget-object v0, p0, Ljgn;->eks:Ljgk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljgn;->eju:Z

    iget v0, p0, Ljgn;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljgn;->aez:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgn;->ekp:Ljava/lang/String;

    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljgn;->aez:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgn;->ejv:Ljava/lang/String;

    iget v0, p0, Ljgn;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljgn;->aez:I

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljgn;->ekt:Ljgo;

    if-nez v0, :cond_2

    new-instance v0, Ljgo;

    invoke-direct {v0}, Ljgo;-><init>()V

    iput-object v0, p0, Ljgn;->ekt:Ljgo;

    :cond_2
    iget-object v0, p0, Ljgn;->ekt:Ljgo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgn;->ejt:Ljava/lang/String;

    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgn;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgn;->ekm:Ljava/lang/String;

    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgn;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgn;->ekn:Ljava/lang/String;

    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljgn;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    iput v0, p0, Ljgn;->ahq:I

    iget v0, p0, Ljgn;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljgn;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x75 -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 52924
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 52925
    const/4 v0, 0x1

    iget-object v1, p0, Ljgn;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 52927
    :cond_0
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 52928
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljgn;->eko:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 52930
    :cond_1
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_2

    .line 52931
    const/4 v0, 0x3

    iget v1, p0, Ljgn;->ekq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 52933
    :cond_2
    iget v0, p0, Ljgn;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_3

    .line 52934
    const/4 v0, 0x4

    iget v1, p0, Ljgn;->ekr:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 52936
    :cond_3
    iget v0, p0, Ljgn;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_4

    .line 52937
    const/4 v0, 0x5

    iget-object v1, p0, Ljgn;->ajX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 52939
    :cond_4
    iget-object v0, p0, Ljgn;->eks:Ljgk;

    if-eqz v0, :cond_5

    .line 52940
    const/4 v0, 0x6

    iget-object v1, p0, Ljgn;->eks:Ljgk;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 52942
    :cond_5
    iget v0, p0, Ljgn;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_6

    .line 52943
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljgn;->eju:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 52945
    :cond_6
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_7

    .line 52946
    const/16 v0, 0x8

    iget-object v1, p0, Ljgn;->ekp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 52948
    :cond_7
    iget v0, p0, Ljgn;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_8

    .line 52949
    const/16 v0, 0x9

    iget-object v1, p0, Ljgn;->ejv:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 52951
    :cond_8
    iget-object v0, p0, Ljgn;->ekt:Ljgo;

    if-eqz v0, :cond_9

    .line 52952
    const/16 v0, 0xa

    iget-object v1, p0, Ljgn;->ekt:Ljgo;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 52954
    :cond_9
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_a

    .line 52955
    const/16 v0, 0xb

    iget-object v1, p0, Ljgn;->ejt:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 52957
    :cond_a
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_b

    .line 52958
    const/16 v0, 0xc

    iget-object v1, p0, Ljgn;->ekm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 52960
    :cond_b
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_c

    .line 52961
    const/16 v0, 0xd

    iget-object v1, p0, Ljgn;->ekn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 52963
    :cond_c
    iget v0, p0, Ljgn;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_d

    .line 52964
    const/16 v0, 0xe

    iget v1, p0, Ljgn;->ahq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->br(II)V

    .line 52966
    :cond_d
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 52967
    return-void
.end method

.method public final bjR()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52666
    iget-object v0, p0, Ljgn;->ejt:Ljava/lang/String;

    return-object v0
.end method

.method public final bjS()Z
    .locals 1

    .prologue
    .line 52836
    iget-boolean v0, p0, Ljgn;->eju:Z

    return v0
.end method

.method public final bkA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52688
    iget-object v0, p0, Ljgn;->ekm:Ljava/lang/String;

    return-object v0
.end method

.method public final bkB()Z
    .locals 1

    .prologue
    .line 52699
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bkC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52710
    iget-object v0, p0, Ljgn;->ekn:Ljava/lang/String;

    return-object v0
.end method

.method public final bkD()Z
    .locals 1

    .prologue
    .line 52721
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bkE()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52751
    iget-object v0, p0, Ljgn;->ekp:Ljava/lang/String;

    return-object v0
.end method

.method public final bkF()Z
    .locals 1

    .prologue
    .line 52762
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bkG()Z
    .locals 1

    .prologue
    .line 52822
    iget v0, p0, Ljgn;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bkH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52855
    iget-object v0, p0, Ljgn;->ejv:Ljava/lang/String;

    return-object v0
.end method

.method public final getBackgroundColor()I
    .locals 1

    .prologue
    .line 52880
    iget v0, p0, Ljgn;->ahq:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52644
    iget-object v0, p0, Ljgn;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final hasBackgroundColor()Z
    .locals 1

    .prologue
    .line 52888
    iget v0, p0, Ljgn;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ig(Z)Ljgn;
    .locals 1

    .prologue
    .line 52839
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljgn;->eju:Z

    .line 52840
    iget v0, p0, Ljgn;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljgn;->aez:I

    .line 52841
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 52971
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 52972
    iget v1, p0, Ljgn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 52973
    const/4 v1, 0x1

    iget-object v2, p0, Ljgn;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52976
    :cond_0
    iget v1, p0, Ljgn;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_1

    .line 52977
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljgn;->eko:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 52980
    :cond_1
    iget v1, p0, Ljgn;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_2

    .line 52981
    const/4 v1, 0x3

    iget v2, p0, Ljgn;->ekq:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 52984
    :cond_2
    iget v1, p0, Ljgn;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_3

    .line 52985
    const/4 v1, 0x4

    iget v2, p0, Ljgn;->ekr:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 52988
    :cond_3
    iget v1, p0, Ljgn;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_4

    .line 52989
    const/4 v1, 0x5

    iget-object v2, p0, Ljgn;->ajX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52992
    :cond_4
    iget-object v1, p0, Ljgn;->eks:Ljgk;

    if-eqz v1, :cond_5

    .line 52993
    const/4 v1, 0x6

    iget-object v2, p0, Ljgn;->eks:Ljgk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52996
    :cond_5
    iget v1, p0, Ljgn;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_6

    .line 52997
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljgn;->eju:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 53000
    :cond_6
    iget v1, p0, Ljgn;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_7

    .line 53001
    const/16 v1, 0x8

    iget-object v2, p0, Ljgn;->ekp:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53004
    :cond_7
    iget v1, p0, Ljgn;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_8

    .line 53005
    const/16 v1, 0x9

    iget-object v2, p0, Ljgn;->ejv:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53008
    :cond_8
    iget-object v1, p0, Ljgn;->ekt:Ljgo;

    if-eqz v1, :cond_9

    .line 53009
    const/16 v1, 0xa

    iget-object v2, p0, Ljgn;->ekt:Ljgo;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53012
    :cond_9
    iget v1, p0, Ljgn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_a

    .line 53013
    const/16 v1, 0xb

    iget-object v2, p0, Ljgn;->ejt:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53016
    :cond_a
    iget v1, p0, Ljgn;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_b

    .line 53017
    const/16 v1, 0xc

    iget-object v2, p0, Ljgn;->ekm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53020
    :cond_b
    iget v1, p0, Ljgn;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_c

    .line 53021
    const/16 v1, 0xd

    iget-object v2, p0, Ljgn;->ekn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53024
    :cond_c
    iget v1, p0, Ljgn;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_d

    .line 53025
    const/16 v1, 0xe

    iget v2, p0, Ljgn;->ahq:I

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 53028
    :cond_d
    return v0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 52655
    iget v0, p0, Ljgn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52811
    iget-object v0, p0, Ljgn;->ajX:Ljava/lang/String;

    return-object v0
.end method

.method public final uJ(Ljava/lang/String;)Ljgn;
    .locals 1

    .prologue
    .line 52647
    if-nez p1, :cond_0

    .line 52648
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52650
    :cond_0
    iput-object p1, p0, Ljgn;->agq:Ljava/lang/String;

    .line 52651
    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgn;->aez:I

    .line 52652
    return-object p0
.end method

.method public final uK(Ljava/lang/String;)Ljgn;
    .locals 1

    .prologue
    .line 52691
    if-nez p1, :cond_0

    .line 52692
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52694
    :cond_0
    iput-object p1, p0, Ljgn;->ekm:Ljava/lang/String;

    .line 52695
    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgn;->aez:I

    .line 52696
    return-object p0
.end method

.method public final uL(Ljava/lang/String;)Ljgn;
    .locals 1

    .prologue
    .line 52713
    if-nez p1, :cond_0

    .line 52714
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52716
    :cond_0
    iput-object p1, p0, Ljgn;->ekn:Ljava/lang/String;

    .line 52717
    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljgn;->aez:I

    .line 52718
    return-object p0
.end method

.method public final uM(Ljava/lang/String;)Ljgn;
    .locals 1

    .prologue
    .line 52754
    if-nez p1, :cond_0

    .line 52755
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52757
    :cond_0
    iput-object p1, p0, Ljgn;->ekp:Ljava/lang/String;

    .line 52758
    iget v0, p0, Ljgn;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljgn;->aez:I

    .line 52759
    return-object p0
.end method

.method public final uN(Ljava/lang/String;)Ljgn;
    .locals 1

    .prologue
    .line 52814
    if-nez p1, :cond_0

    .line 52815
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52817
    :cond_0
    iput-object p1, p0, Ljgn;->ajX:Ljava/lang/String;

    .line 52818
    iget v0, p0, Ljgn;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljgn;->aez:I

    .line 52819
    return-object p0
.end method

.method public final uO(Ljava/lang/String;)Ljgn;
    .locals 1

    .prologue
    .line 52858
    if-nez p1, :cond_0

    .line 52859
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52861
    :cond_0
    iput-object p1, p0, Ljgn;->ejv:Ljava/lang/String;

    .line 52862
    iget v0, p0, Ljgn;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljgn;->aez:I

    .line 52863
    return-object p0
.end method
