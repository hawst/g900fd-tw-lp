.class final Lgpw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private synthetic cSM:Lgpu;


# direct methods
.method constructor <init>(Lgpu;)V
    .locals 0

    .prologue
    .line 851
    iput-object p1, p0, Lgpw;->cSM:Lgpu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 851
    iget-object v0, p0, Lgpw;->cSM:Lgpu;

    invoke-virtual {v0}, Lgpu;->aJs()Lchr;

    move-result-object v0

    iget-object v0, v0, Lchr;->mGelStartupPrefs:Ldku;

    const-string v1, "GEL.GSAPrefs.now_enabled"

    invoke-virtual {v0, v1}, Ldku;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lgpw;->cSM:Lgpu;

    invoke-virtual {v1}, Lgpu;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DF()Lcin;

    move-result-object v1

    invoke-interface {v1}, Lcin;->Kz()Z

    move-result v1

    const-string v2, "GEL.GSAPrefs.now_enabled"

    invoke-virtual {v0, v2, v1}, Ldku;->m(Ljava/lang/String;Z)V

    :cond_0
    const-string v1, "GSAPrefs.hotword_enabled"

    iget-object v2, p0, Lgpw;->cSM:Lgpu;

    invoke-virtual {v2}, Lgpu;->aJq()Lhhq;

    move-result-object v2

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aTP()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Ldku;->m(Ljava/lang/String;Z)V

    const-string v1, "GSAPrefs.first_run_screens_shown"

    iget-object v2, p0, Lgpw;->cSM:Lgpu;

    invoke-virtual {v2}, Lgpu;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DF()Lcin;

    move-result-object v2

    invoke-interface {v2}, Lcin;->KA()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Ldku;->m(Ljava/lang/String;Z)V

    const-string v1, "GEL.GSAPrefs.log_gel_events"

    iget-object v2, p0, Lgpw;->cSM:Lgpu;

    invoke-virtual {v2}, Lgpu;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    invoke-interface {v2}, Lcke;->Ow()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Ldku;->m(Ljava/lang/String;Z)V

    const-string v1, "GEL.GSAPrefs.gel_tag"

    iget-object v2, p0, Lgpw;->cSM:Lgpu;

    invoke-virtual {v2}, Lgpu;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-virtual {v2}, Lchk;->ID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldku;->as(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "GSAPrefs.google_account"

    iget-object v2, p0, Lgpw;->cSM:Lgpu;

    invoke-virtual {v2}, Lgpu;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    invoke-interface {v2}, Lcke;->ND()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ldku;->as(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "GSAPrefs.high_contrast"

    iget-object v2, p0, Lgpw;->cSM:Lgpu;

    invoke-virtual {v2}, Lgpu;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->BK()Lcke;

    move-result-object v2

    invoke-interface {v2}, Lcke;->wu()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Ldku;->m(Ljava/lang/String;Z)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.now.gel_prefs_synced_broadcast"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lgpw;->cSM:Lgpu;

    iget-object v1, v1, Lgpu;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    return-object v0
.end method
