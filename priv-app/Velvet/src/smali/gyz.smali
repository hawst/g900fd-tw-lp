.class public Lgyz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lddc;
.implements Leti;


# instance fields
.field private aOR:Landroid/os/Bundle;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private as:Z

.field private bB:Z

.field private bFB:Landroid/content/Intent;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bUp:Ledu;

.field private bXu:Leen;

.field private cYN:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cYs:Lgxe;

.field final cZT:Lhac;

.field private final cZU:Ljava/lang/Runnable;

.field private final cZV:Ljava/lang/Runnable;

.field private final cZW:Ljava/lang/Runnable;

.field private final cZX:Lgpe;

.field private cZY:Lgyv;

.field private cZZ:Lgyd;

.field private cq:Z

.field private daa:Lgxa;

.field private dab:Lfnx;

.field private dac:Lela;

.field private dad:Lgxi;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private dae:Lgxi;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private daf:Lcom/google/android/shared/search/SearchBoxStats;

.field private dag:Z

.field private dah:Lgyt;

.field private dai:Lgyt;

.field private daj:Z

.field private dak:Z

.field private dal:Z

.field private dam:Z

.field private final dan:Ljava/lang/Runnable;

.field private final dao:Ljava/lang/Runnable;

.field final mAppContext:Landroid/content/Context;

.field private final mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

.field private final mDebugFeatures:Lckw;

.field private mEventBus:Ldda;

.field private final mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field public mQueryState:Lcom/google/android/search/core/state/QueryState;

.field mSearchServiceClient:Lhaa;

.field private final mUiThread:Leqo;

.field private mUserInteractionLogger:Lcpx;

.field final mVelvetFactory:Lgpu;

.field final mVelvetServices:Lgql;

.field private rE:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhac;Lcom/google/android/search/shared/service/ClientConfig;Lgql;Lema;Lgpu;Lgpe;Lckw;)V
    .locals 2

    .prologue
    .line 269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    new-instance v0, Lgza;

    const-string v1, "Delayed initalise"

    invoke-direct {v0, p0, v1}, Lgza;-><init>(Lgyz;Ljava/lang/String;)V

    iput-object v0, p0, Lgyz;->cZU:Ljava/lang/Runnable;

    .line 177
    new-instance v0, Lgzl;

    const-string v1, "Delayed resume"

    invoke-direct {v0, p0, v1}, Lgzl;-><init>(Lgyz;Ljava/lang/String;)V

    iput-object v0, p0, Lgyz;->cZV:Ljava/lang/Runnable;

    .line 185
    new-instance v0, Lgzo;

    const-string v1, "Delayed service connect"

    invoke-direct {v0, p0, v1}, Lgzo;-><init>(Lgyz;Ljava/lang/String;)V

    iput-object v0, p0, Lgyz;->cZW:Ljava/lang/Runnable;

    .line 234
    sget-object v0, Lgyt;->cZA:Lgyt;

    iput-object v0, p0, Lgyz;->dah:Lgyt;

    .line 236
    sget-object v0, Lgyt;->cZA:Lgyt;

    iput-object v0, p0, Lgyz;->dai:Lgyt;

    .line 252
    new-instance v0, Lgzp;

    const-string v1, "Change mode"

    invoke-direct {v0, p0, v1}, Lgzp;-><init>(Lgyz;Ljava/lang/String;)V

    iput-object v0, p0, Lgyz;->dan:Ljava/lang/Runnable;

    .line 259
    new-instance v0, Lgzq;

    const-string v1, "Update main content"

    invoke-direct {v0, p0, v1}, Lgzq;-><init>(Lgyz;Ljava/lang/String;)V

    iput-object v0, p0, Lgyz;->dao:Ljava/lang/Runnable;

    .line 271
    iput-object p1, p0, Lgyz;->mAppContext:Landroid/content/Context;

    .line 272
    iput-object p2, p0, Lgyz;->cZT:Lhac;

    .line 273
    iput-object p3, p0, Lgyz;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    .line 274
    iput-object p6, p0, Lgyz;->mVelvetFactory:Lgpu;

    .line 275
    iput-object p4, p0, Lgyz;->mVelvetServices:Lgql;

    .line 276
    invoke-virtual {p5}, Lema;->aus()Leqo;

    move-result-object v0

    iput-object v0, p0, Lgyz;->mUiThread:Leqo;

    .line 277
    invoke-virtual {p5}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lgyz;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 278
    iput-object p8, p0, Lgyz;->mDebugFeatures:Lckw;

    .line 280
    iput-object p7, p0, Lgyz;->cZX:Lgpe;

    .line 282
    new-instance v0, Lgyv;

    invoke-direct {v0}, Lgyv;-><init>()V

    iput-object v0, p0, Lgyz;->cZY:Lgyv;

    .line 283
    return-void
.end method

.method private DR()Lcpx;
    .locals 4

    .prologue
    .line 291
    iget-object v0, p0, Lgyz;->mUserInteractionLogger:Lcpx;

    if-nez v0, :cond_0

    .line 292
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DR()Lcpx;

    move-result-object v0

    iput-object v0, p0, Lgyz;->mUserInteractionLogger:Lcpx;

    .line 293
    iget-object v0, p0, Lgyz;->mUserInteractionLogger:Lcpx;

    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v1}, Lhac;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v2}, Lhac;->aMz()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcpx;->a(Landroid/app/Activity;J)V

    .line 296
    :cond_0
    iget-object v0, p0, Lgyz;->mUserInteractionLogger:Lcpx;

    return-object v0
.end method

.method public static P(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1724
    if-nez p0, :cond_1

    .line 1735
    :cond_0
    :goto_0
    return v0

    .line 1727
    :cond_1
    invoke-static {p0}, Lhgn;->aa(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1730
    invoke-static {p0}, Lhgn;->ab(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 1731
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lgyt;->cZD:Lgyt;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqI()Ljava/io/Serializable;

    move-result-object v1

    invoke-virtual {v2, v1}, Lgyt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const-string v1, "commit-query"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1733
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private UA()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1698
    iget-boolean v1, p0, Lgyz;->daj:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lgyz;->dal:Z

    if-eqz v1, :cond_1

    .line 1713
    :cond_0
    :goto_0
    return v0

    .line 1704
    :cond_1
    iget-object v1, p0, Lgyz;->bFB:Landroid/content/Intent;

    if-nez v1, :cond_0

    .line 1708
    iget-object v1, p0, Lgyz;->aOR:Landroid/os/Bundle;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgyz;->aOR:Landroid/os/Bundle;

    const-string v2, "velvet:velvet_presenter:changing_config"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1713
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ldcy;Z)I
    .locals 3

    .prologue
    .line 1614
    iget-object v0, p0, Lgyz;->cZY:Lgyv;

    iget-object v1, p0, Lgyz;->dah:Lgyt;

    iget-object v0, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0}, Lgyt;->aLK()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldcy;->ZR()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0, p2}, Lgyv;->b(Lgyt;ZZ)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lgxi;Z)Lgxi;
    .locals 0

    .prologue
    .line 546
    if-eqz p2, :cond_0

    .line 547
    iput-object p1, p0, Lgyz;->dae:Lgxi;

    .line 551
    :goto_0
    return-object p1

    .line 549
    :cond_0
    iput-object p1, p0, Lgyz;->dad:Lgxi;

    goto :goto_0
.end method

.method private a(Lgyt;Ldda;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 435
    iget-object v0, p0, Lgyz;->cZY:Lgyv;

    iget-boolean v0, v0, Lgyv;->cZP:Z

    if-eqz v0, :cond_0

    .line 436
    invoke-virtual {p0}, Lgyz;->aLT()V

    .line 439
    :cond_0
    sget-object v0, Lgyt;->cZA:Lgyt;

    if-ne p1, v0, :cond_2

    move v0, v1

    .line 440
    :goto_0
    invoke-virtual {p2}, Ldda;->aan()Ldcy;

    move-result-object v2

    .line 441
    invoke-virtual {p2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v3

    .line 442
    iget-object v4, p0, Lgyz;->cZT:Lhac;

    invoke-direct {p0, v2, v3}, Lgyz;->a(Ldcy;Z)I

    move-result v3

    invoke-interface {v4, v3, v0, v1}, Lhac;->e(IZZ)V

    .line 444
    invoke-direct {p0, v0, v2}, Lgyz;->a(ZLdcy;)V

    .line 446
    iget-object v1, p0, Lgyz;->cZY:Lgyv;

    iget-object v1, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v1}, Lgyt;->aLI()Z

    move-result v1

    .line 447
    iget-object v2, p0, Lgyz;->daa:Lgxa;

    invoke-virtual {v2, v1}, Lgxa;->setEnabled(Z)V

    .line 448
    iget-object v2, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v2, v1, v0}, Lhac;->G(ZZ)V

    .line 450
    iget-object v1, p0, Lgyz;->cZZ:Lgyd;

    iget-boolean v2, p0, Lgyz;->dak:Z

    iget-object v3, p0, Lgyz;->dah:Lgyt;

    iget-object v4, p0, Lgyz;->cZY:Lgyv;

    iget-boolean v4, v4, Lgyv;->cZP:Z

    invoke-virtual {v1, v2, v0, v3, v4}, Lgyd;->a(ZZLgyt;Z)V

    .line 452
    invoke-direct {p0, p1}, Lgyz;->b(Lgyt;)V

    .line 454
    invoke-direct {p0}, Lgyz;->aMo()V

    .line 456
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0}, Lgyt;->aLM()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgyz;->cZY:Lgyv;

    iget-boolean v0, v0, Lgyv;->cZP:Z

    if-eqz v0, :cond_1

    .line 459
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DP()Lcob;

    move-result-object v0

    invoke-interface {v0}, Lcob;->QS()V

    .line 461
    :cond_1
    return-void

    .line 439
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ZLdcy;)V
    .locals 4

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x3

    .line 1571
    iget-object v2, p0, Lgyz;->cZY:Lgyv;

    iget-object v2, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {p2}, Ldcy;->aac()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1573
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v2, v0, p1}, Lhac;->I(IZ)V

    .line 1575
    iget-object v2, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v2}, Lgyt;->aLK()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Ldcy;->ZQ()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1576
    iget-object v2, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v2}, Lhac;->aME()V

    .line 1578
    :cond_1
    if-eqz v0, :cond_2

    if-ne v0, v1, :cond_3

    .line 1580
    :cond_2
    invoke-virtual {p2}, Ldcy;->ZK()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1581
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aMD()V

    .line 1584
    :cond_3
    return-void

    .line 1571
    :cond_4
    sget-object v3, Lgyw;->cZO:[I

    invoke-virtual {v2}, Lgyt;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p2}, Ldcy;->ZI()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Ldcy;->ZS()I

    move-result v2

    if-eq v2, v0, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private a(ZLjava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 494
    if-eqz p1, :cond_1

    iget-object v0, p0, Lgyz;->dae:Lgxi;

    .line 495
    :goto_0
    if-nez v0, :cond_2

    if-nez p2, :cond_2

    .line 543
    :cond_0
    :goto_1
    return-void

    .line 494
    :cond_1
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    goto :goto_0

    .line 499
    :cond_2
    if-eqz p1, :cond_9

    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v1}, Lhac;->aMG()Lcom/google/android/velvet/ui/MainContentView;

    move-result-object v1

    .line 500
    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lgxi;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 505
    iget-boolean v2, p0, Lgyz;->as:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lgyz;->dam:Z

    if-nez v2, :cond_3

    .line 507
    invoke-virtual {v0}, Lgxi;->onPause()V

    .line 509
    :cond_3
    iget-boolean v2, p0, Lgyz;->cq:Z

    if-eqz v2, :cond_4

    .line 511
    invoke-virtual {v0}, Lgxi;->onStop()V

    .line 514
    :cond_4
    invoke-virtual {v0}, Lgxi;->onDetach()V

    .line 515
    invoke-virtual {v1, v3}, Lcom/google/android/velvet/ui/MainContentView;->b(Lgxi;)V

    .line 516
    invoke-direct {p0, v3, p1}, Lgyz;->a(Lgxi;Z)Lgxi;

    move-result-object v0

    .line 518
    :cond_5
    if-eqz p2, :cond_7

    if-nez v0, :cond_7

    .line 519
    iget-object v0, p0, Lgyz;->mVelvetFactory:Lgpu;

    invoke-virtual {v0, p2, v1}, Lgpu;->a(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)Lgxi;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lgyz;->a(Lgxi;Z)Lgxi;

    move-result-object v0

    .line 524
    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/MainContentView;->b(Lgxi;)V

    .line 526
    iget-object v2, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0, p0, v2, p3}, Lgxi;->a(Lgyz;Ldda;Landroid/os/Bundle;)V

    .line 527
    iget-boolean v2, p0, Lgyz;->cq:Z

    if-eqz v2, :cond_6

    .line 529
    invoke-virtual {v0}, Lgxi;->onStart()V

    .line 531
    :cond_6
    iget-boolean v2, p0, Lgyz;->as:Z

    if-eqz v2, :cond_7

    iget-boolean v2, p0, Lgyz;->dam:Z

    if-nez v2, :cond_7

    .line 533
    invoke-virtual {v0}, Lgxi;->onResume()V

    .line 536
    :cond_7
    if-eqz v0, :cond_8

    iget-object v2, p0, Lgyz;->dah:Lgyt;

    iget-object v3, p0, Lgyz;->dai:Lgyt;

    if-ne v2, v3, :cond_8

    .line 537
    iget-object v2, p0, Lgyz;->cZY:Lgyv;

    iget-object v3, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0, v2, v3}, Lgxi;->a(Lgyv;Lgyt;)V

    .line 539
    :cond_8
    if-eqz p1, :cond_0

    .line 541
    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v1, v0}, Lcom/google/android/velvet/ui/MainContentView;->gj(Z)V

    goto :goto_1

    .line 499
    :cond_9
    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v1}, Lhac;->aMH()Lcom/google/android/velvet/ui/MainContentView;

    move-result-object v1

    goto :goto_2

    .line 541
    :cond_a
    const/4 v0, 0x0

    goto :goto_3
.end method

.method static synthetic a(Lgyz;)Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lgyz;->bB:Z

    return v0
.end method

.method private aLQ()V
    .locals 7

    .prologue
    .line 341
    new-instance v0, Ledu;

    iget-object v1, p0, Lgyz;->daf:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v1}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgyz;->daf:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v2}, Lcom/google/android/shared/search/SearchBoxStats;->getSource()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgyz;->mVelvetServices:Lgql;

    iget-object v3, v3, Lgql;->mClock:Lemp;

    iget-object v4, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v4

    iget-object v5, p0, Lgyz;->dah:Lgyt;

    sget-object v6, Lgzn;->cZO:[I

    invoke-virtual {v5}, Lgyt;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    const/4 v5, 0x0

    :goto_0
    invoke-direct/range {v0 .. v5}, Ledu;-><init>(Ljava/lang/String;Ljava/lang/String;Lemp;Lcom/google/android/shared/search/Query;I)V

    iput-object v0, p0, Lgyz;->bUp:Ledu;

    .line 344
    return-void

    .line 341
    :pswitch_0
    const/4 v5, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v5, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v5, 0x3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private aLR()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->dan:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 376
    return-void
.end method

.method private aLU()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 480
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lgyz;->a(ZLjava/lang/String;Landroid/os/Bundle;)V

    .line 481
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, Lgyz;->a(ZLjava/lang/String;Landroid/os/Bundle;)V

    .line 482
    return-void
.end method

.method private aMi()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1345
    invoke-virtual {p0}, Lgyz;->aLP()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1346
    const-string v0, "disabled_serp"

    .line 1354
    :goto_0
    return-object v0

    .line 1349
    :cond_0
    sget-object v0, Lgzn;->cZO:[I

    iget-object v1, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v1}, Lgyt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1354
    const-string v0, "now_cards"

    goto :goto_0

    .line 1352
    :pswitch_0
    const-string v0, "now_serp"

    goto :goto_0

    .line 1349
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private aMk()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1394
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->UF()V

    .line 1396
    iget-object v0, p0, Lgyz;->cZY:Lgyv;

    iget-object v0, p0, Lgyz;->dah:Lgyt;

    iget-object v2, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v2

    invoke-virtual {v0}, Lgyt;->aLI()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lgyt;->aLJ()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 1398
    invoke-virtual {p0}, Lgyz;->wj()Lekf;

    move-result-object v0

    invoke-interface {v0, v1}, Lekf;->hG(I)V

    .line 1400
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1396
    goto :goto_0
.end method

.method private aMm()V
    .locals 2

    .prologue
    .line 1604
    iget-object v0, p0, Lgyz;->cYN:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1605
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    iget-object v1, p0, Lgyz;->cYN:Landroid/view/View;

    invoke-interface {v0, v1}, Lhac;->bF(Landroid/view/View;)V

    .line 1606
    const/4 v0, 0x0

    iput-object v0, p0, Lgyz;->cYN:Landroid/view/View;

    .line 1608
    :cond_0
    return-void
.end method

.method private aMo()V
    .locals 2

    .prologue
    .line 1877
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    sget-object v1, Lgyt;->cZA:Lgyt;

    if-eq v0, v1, :cond_0

    .line 1878
    invoke-direct {p0}, Lgyz;->DR()Lcpx;

    move-result-object v0

    iget-object v1, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v1}, Lgyt;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpx;->io(Ljava/lang/String;)V

    .line 1880
    :cond_0
    return-void
.end method

.method private b(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1654
    iput-object p2, p0, Lgyz;->aOR:Landroid/os/Bundle;

    .line 1655
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aMA()V

    .line 1657
    if-eqz p1, :cond_2

    .line 1658
    iget-object v0, p0, Lgyz;->daf:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v0}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v1

    const-string v0, "search-app"

    invoke-static {p1, v0}, Lhgn;->e(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lhgn;->ab(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {p1}, Lhgn;->ac(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/shared/search/SearchBoxStats;->getSource()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {v1, v0}, Lcom/google/android/shared/search/SearchBoxStats;->aA(Ljava/lang/String;Ljava/lang/String;)Lehj;

    move-result-object v2

    invoke-virtual {v2}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    iput-object v2, p0, Lgyz;->daf:Lcom/google/android/shared/search/SearchBoxStats;

    iget-boolean v2, p0, Lgyz;->rE:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DQ()Lcpd;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcpd;->P(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661
    :cond_2
    iget-object v0, p0, Lgyz;->aOR:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lgyz;->dag:Z

    .line 1662
    iget-boolean v0, p0, Lgyz;->dag:Z

    if-eqz v0, :cond_3

    const/4 p1, 0x0

    :cond_3
    iput-object p1, p0, Lgyz;->bFB:Landroid/content/Intent;

    .line 1663
    return-void

    .line 1661
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lgyt;)V
    .locals 3

    .prologue
    .line 464
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->UG()Ldep;

    move-result-object v0

    invoke-virtual {v0}, Ldep;->abp()Z

    move-result v0

    .line 465
    iget-object v1, p0, Lgyz;->cZY:Lgyv;

    iget-object v2, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v1, v2, p1, v0}, Lgyv;->a(Lgyt;Lgyt;Z)Z

    move-result v0

    .line 468
    iget-object v1, p0, Lgyz;->cYs:Lgxe;

    invoke-virtual {v1, v0}, Lgxe;->fV(Z)V

    .line 469
    return-void
.end method

.method private gd(Z)V
    .locals 2

    .prologue
    .line 1587
    iget-object v0, p0, Lgyz;->cYN:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1588
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0, p1}, Lhaa;->cl(Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgyz;->cYN:Landroid/view/View;

    .line 1589
    iget-object v0, p0, Lgyz;->cYN:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1590
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    iget-object v1, p0, Lgyz;->cYN:Landroid/view/View;

    invoke-interface {v0, v1}, Lhac;->bE(Landroid/view/View;)V

    .line 1593
    :cond_0
    return-void
.end method

.method private ge(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1675
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lgyz;->as:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lgyz;->cq:Z

    if-eqz v0, :cond_1

    .line 1676
    :cond_0
    iget-object v0, p0, Lgyz;->aOR:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 1677
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    iget-object v1, p0, Lgyz;->aOR:Landroid/os/Bundle;

    invoke-direct {p0}, Lgyz;->UA()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lhaa;->f(Landroid/os/Bundle;I)V

    .line 1678
    iput-object v3, p0, Lgyz;->aOR:Landroid/os/Bundle;

    .line 1685
    :goto_0
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->aMy()V

    .line 1687
    :cond_1
    return-void

    .line 1679
    :cond_2
    iget-object v0, p0, Lgyz;->bFB:Landroid/content/Intent;

    if-eqz v0, :cond_3

    .line 1680
    iget-object v0, p0, Lgyz;->bFB:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lgyz;->Q(Landroid/content/Intent;)V

    .line 1681
    iput-object v3, p0, Lgyz;->bFB:Landroid/content/Intent;

    goto :goto_0

    .line 1683
    :cond_3
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-direct {p0}, Lgyz;->UA()I

    move-result v1

    invoke-virtual {v0, v1}, Lhaa;->hg(I)V

    goto :goto_0
.end method

.method private isChangingConfigurations()Z
    .locals 1

    .prologue
    .line 1883
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->isChangingConfigurations()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 2006
    const/16 v0, 0x1fff

    return v0
.end method

.method final Cq()V
    .locals 1

    .prologue
    .line 1228
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aMM()Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atH()V

    .line 1229
    return-void
.end method

.method public final DD()Lcjs;
    .locals 1

    .prologue
    .line 726
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DD()Lcjs;

    move-result-object v0

    return-object v0
.end method

.method public final H(IZ)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1139
    iget-boolean v1, p0, Lgyz;->rE:Z

    if-eqz v1, :cond_2

    .line 1142
    iget-object v1, p0, Lgyz;->cZY:Lgyv;

    iget-object v2, p0, Lgyz;->dah:Lgyt;

    iget-object v3, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v4, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lgyv;->b(Lgyt;Lcom/google/android/shared/search/Query;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1144
    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xu()V

    .line 1161
    :goto_0
    return v0

    .line 1146
    :cond_0
    iget-object v1, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v1}, Lgyt;->aLL()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1147
    iget-object v1, p0, Lgyz;->cZZ:Lgyd;

    invoke-virtual {v1}, Lgyd;->aLD()V

    goto :goto_0

    .line 1149
    :cond_1
    if-nez p1, :cond_2

    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xh()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    iget-object v1, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aas()Ldbj;

    move-result-object v1

    invoke-virtual {v1}, Ldbj;->Wl()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1157
    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xb()Z

    goto :goto_0

    .line 1161
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Pm()Lcom/google/android/search/shared/actions/VoiceAction;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 629
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    if-nez v0, :cond_0

    .line 630
    const-string v0, "VelvetPresenter"

    const-string v1, "Can\'t get current voice action."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 631
    const v0, 0xb9f6b8

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 632
    const/4 v0, 0x0

    .line 634
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    goto :goto_0
.end method

.method public final Q(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1744
    const-string v0, "VelvetPresenter"

    const-string v4, "setupFromIntent(%s)"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v2

    invoke-static {v0, v4, v5}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1746
    const-string v0, "notification_entries"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "notification_entries"

    invoke-static {p1, v0}, Lgbm;->d(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    invoke-direct {p0}, Lgyz;->DR()Lcpx;

    move-result-object v5

    const-string v6, "NOTIFICATION_CLICK"

    invoke-virtual {v5, v6, v0, v3}, Lcpx;->a(Ljava/lang/String;Lizj;Lixx;)V

    goto :goto_0

    :cond_0
    const-string v0, "target_entry"

    invoke-static {p1, v0}, Lgbm;->b(Landroid/content/Intent;Ljava/lang/String;)Lizj;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v4

    invoke-virtual {v4}, Lgql;->aJr()Lfdb;

    move-result-object v4

    invoke-virtual {v4}, Lfdb;->axP()Lfjs;

    move-result-object v4

    invoke-interface {v4, v0}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    if-nez v0, :cond_1

    const-string v5, "target_group_entry_tree"

    invoke-static {p1, v5}, Lgbm;->c(Landroid/content/Intent;Ljava/lang/String;)Lizq;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v4, v5}, Lfjs;->f(Lizq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    :cond_1
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lgyz;->DR()Lcpx;

    move-result-object v4

    const-string v5, "WIDGET_PRESS"

    invoke-virtual {v4, v5, v0}, Lcpx;->a(Ljava/lang/String;Lfkd;)V

    :cond_2
    const-string v0, "source"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lgyz;->DR()Lcpx;

    move-result-object v4

    const-string v5, "START"

    invoke-virtual {v4, v5, v0}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 1749
    invoke-direct {p0}, Lgyz;->aLR()V

    .line 1751
    sget-object v0, Lgyt;->cZA:Lgyt;

    iput-object v0, p0, Lgyz;->dai:Lgyt;

    .line 1752
    sget-object v0, Lgyt;->cZA:Lgyt;

    iput-object v0, p0, Lgyz;->dah:Lgyt;

    .line 1754
    invoke-static {p1}, Lhgn;->ab(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqq()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DL()Lcrh;

    move-result-object v4

    invoke-virtual {v4}, Lcrh;->Su()Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_3
    iget-object v4, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v4}, Lgql;->SC()Lcfo;

    move-result-object v4

    invoke-virtual {v4}, Lcfo;->DF()Lcin;

    move-result-object v4

    invoke-interface {v4}, Lcin;->KA()Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {p1}, Lhgn;->aa(Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_7

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v4

    if-nez v4, :cond_7

    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_5
    const-string v0, "disable-opt-in"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    new-instance v0, Landroid/content/Intent;

    iget-object v4, p0, Lgyz;->mAppContext:Landroid/content/Context;

    const-class v5, Lcom/google/android/velvet/tg/FirstRunActivity;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v4, 0x10008000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v4, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v4, v0}, Lhac;->startActivity(Landroid/content/Intent;)V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_8

    .line 1822
    :goto_2
    return-void

    :cond_7
    move v0, v2

    .line 1754
    goto :goto_1

    .line 1765
    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_9

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1766
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_9

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "search"

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    :goto_3
    if-eqz v0, :cond_9

    const-string v4, "velvet-query"

    sget-object v5, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-virtual {v5, v0}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "commit-query"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1770
    :cond_9
    invoke-static {p1}, Lhgn;->ab(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1774
    invoke-static {p1}, Lhgn;->aa(Landroid/content/Intent;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1775
    if-nez v0, :cond_b

    move v0, v1

    :goto_4
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 1777
    const-string v0, "VelvetPresenter"

    const-string v1, "Resuming with current state."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1778
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    const-string v1, "handover-session-id"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lhaa;->aD(J)V

    .line 1780
    invoke-static {p1}, Lhgn;->ac(Landroid/content/Intent;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 1781
    const-string v0, "handover-backup-action"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/velvet/ActionData;

    .line 1782
    iget-object v2, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/search/core/state/QueryState;->d(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V

    goto/16 :goto_2

    :cond_a
    move-object v0, v3

    .line 1766
    goto :goto_3

    :cond_b
    move v0, v2

    .line 1775
    goto :goto_4

    .line 1784
    :cond_c
    if-eqz v0, :cond_f

    .line 1786
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v1

    if-eqz v1, :cond_d

    sget-object v1, Lgyt;->cZB:Lgyt;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqI()Ljava/io/Serializable;

    move-result-object v4

    invoke-virtual {v1, v4}, Lgyt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lgyz;->aLP()Z

    move-result v1

    if-nez v1, :cond_d

    .line 1788
    sget-object v1, Lgyt;->cZD:Lgyt;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/shared/search/Query;->a(Ljava/io/Serializable;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1793
    :cond_d
    const-string v1, "commit-query"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1797
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    if-nez v1, :cond_e

    .line 1798
    iget-object v1, p0, Lgyz;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v1}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1801
    :cond_e
    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v1, v0}, Lhaa;->y(Lcom/google/android/shared/search/Query;)V

    .line 1812
    :cond_f
    :goto_5
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0}, Lgyz;->UA()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Lhaa;->e(Landroid/os/Bundle;I)V

    .line 1814
    const-string v0, "clear_srp_cache"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1815
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->Ng()V

    .line 1817
    :cond_10
    const-string v0, "keepDiscourseContext"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1818
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->Eg()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Nd()V

    .line 1820
    :cond_11
    invoke-direct {p0}, Lgyz;->aLU()V

    goto/16 :goto_2

    .line 1804
    :cond_12
    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    sget-object v4, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    sget-object v5, Lgyt;->cZD:Lgyt;

    invoke-virtual {v4, v5, v3}, Lcom/google/android/shared/search/Query;->a(Ljava/io/Serializable;Landroid/os/Bundle;)Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v4, p0, Lgyz;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v4}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-virtual {v1, v3}, Lhaa;->y(Lcom/google/android/shared/search/Query;)V

    .line 1807
    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v1}, Lhaa;->UF()V

    .line 1808
    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v1, v0}, Lhaa;->x(Lcom/google/android/shared/search/Query;)V

    goto :goto_5
.end method

.method public final Sv()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Sv()Z

    move-result v0

    return v0
.end method

.method public final UG()Ldep;
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->UG()Ldep;

    move-result-object v0

    return-object v0
.end method

.method final Xb()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1212
    iget-object v2, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v2}, Lhaa;->isActive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1221
    :cond_0
    :goto_0
    return v0

    .line 1215
    :cond_1
    iget-object v2, p0, Lgyz;->dah:Lgyt;

    sget-object v3, Lgyt;->cZE:Lgyt;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v2}, Lhaa;->cN()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 1216
    goto :goto_0

    .line 1218
    :cond_2
    iget-object v2, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xb()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 1219
    goto :goto_0
.end method

.method public final Xe()Lcom/google/android/shared/search/Query;
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lecr;Landroid/os/Bundle;)Lhaa;
    .locals 6

    .prologue
    .line 760
    new-instance v0, Lhaa;

    invoke-virtual {p0}, Lgyz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lgyz;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    move-object v2, p1

    move-object v4, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhaa;-><init>(Landroid/content/Context;Lecr;Lcom/google/android/search/shared/service/ClientConfig;Lgyz;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;ZLandroid/view/View$OnClickListener;)V
    .locals 1
    .param p3    # Landroid/view/View$OnClickListener;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1917
    iget-object v0, p0, Lgyz;->daa:Lgxa;

    invoke-virtual {v0, p1, p2, p3}, Lgxa;->a(Landroid/graphics/drawable/Drawable;ZLandroid/view/View$OnClickListener;)V

    .line 1920
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0, p2}, Lhac;->gf(Z)V

    .line 1921
    return-void
.end method

.method public final a(Landroid/os/Bundle;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 737
    iget-object v0, p0, Lgyz;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    iput-object v0, p0, Lgyz;->daf:Lcom/google/android/shared/search/SearchBoxStats;

    .line 742
    invoke-direct {p0, p2, p1}, Lgyz;->b(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 744
    new-instance v0, Lgzr;

    invoke-direct {v0, p0, p1}, Lgzr;-><init>(Lgyz;Landroid/os/Bundle;)V

    .line 755
    invoke-virtual {p0, v0, p1}, Lgyz;->a(Lecr;Landroid/os/Bundle;)Lhaa;

    move-result-object v0

    iput-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    .line 756
    return-void
.end method

.method public final a(Lddb;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1458
    iget-object v2, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v2}, Lhac;->aML()V

    .line 1459
    iget-object v2, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v2}, Lhaa;->isActive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1460
    const-string v0, "VelvetPresenter"

    const-string v2, "Still observing while not the active client"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1522
    :cond_0
    :goto_0
    return-void

    .line 1463
    :cond_1
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1464
    :cond_2
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v2

    invoke-virtual {v2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v2

    .line 1465
    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v3

    .line 1466
    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->XX()Lcom/google/android/shared/search/Query;

    move-result-object v4

    .line 1467
    if-eqz v4, :cond_3

    .line 1468
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v4

    const v5, 0x7f0a0657

    iget-object v6, p0, Lgyz;->mAppContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1470
    iget-object v4, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v4}, Lhac;->aMF()V

    .line 1473
    :cond_3
    iget-object v4, p0, Lgyz;->cZT:Lhac;

    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v5

    invoke-virtual {v5}, Ldda;->aan()Ldcy;

    move-result-object v5

    invoke-direct {p0, v5, v3}, Lgyz;->a(Ldcy;Z)I

    move-result v3

    invoke-interface {v4, v3, v1, v0}, Lhac;->e(IZZ)V

    .line 1475
    iget-object v3, p0, Lgyz;->cZZ:Lgyd;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v3, v2}, Lgyd;->z(Lcom/google/android/shared/search/Query;)V

    .line 1476
    iget-object v2, p0, Lgyz;->dah:Lgyt;

    invoke-direct {p0, v2}, Lgyz;->b(Lgyt;)V

    .line 1477
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v2

    invoke-virtual {p0, v2}, Lgyz;->t(Ldda;)Lgyt;

    move-result-object v2

    .line 1481
    sget-object v3, Lgyt;->cZE:Lgyt;

    if-ne v2, v3, :cond_4

    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1482
    iget-object v3, p0, Lgyz;->cZT:Lhac;

    iget-object v4, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v4}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lhac;->nK(Ljava/lang/String;)V

    .line 1484
    :cond_4
    if-eqz v2, :cond_f

    iget-object v3, p0, Lgyz;->dai:Lgyt;

    if-eq v2, v3, :cond_f

    .line 1485
    iget-object v3, p0, Lgyz;->mUiThread:Leqo;

    invoke-interface {v3}, Leqo;->Du()Z

    move-result v3

    invoke-static {v3}, Lifv;->gY(Z)V

    invoke-static {v2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lgyz;->aLR()V

    iput-object v2, p0, Lgyz;->dai:Lgyt;

    iget-object v3, p0, Lgyz;->dah:Lgyt;

    if-ne v2, v3, :cond_b

    .line 1491
    :cond_5
    :goto_1
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lddb;->aaB()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1493
    :cond_6
    iget-boolean v0, p0, Lgyz;->dak:Z

    if-eqz v0, :cond_7

    .line 1494
    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    iget-boolean v2, p0, Lgyz;->dak:Z

    iget-object v3, p0, Lgyz;->dah:Lgyt;

    iget-object v4, p0, Lgyz;->cZY:Lgyv;

    iget-boolean v4, v4, Lgyv;->cZP:Z

    invoke-virtual {v0, v2, v1, v3, v4}, Lgyd;->a(ZZLgyt;Z)V

    .line 1499
    :cond_7
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    .line 1504
    invoke-virtual {v0}, Ldcy;->ZA()Z

    move-result v2

    invoke-direct {p0, v2}, Lgyz;->gd(Z)V

    .line 1506
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1507
    invoke-virtual {v0}, Ldcy;->ZP()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1508
    iget-object v2, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v2}, Lhac;->aMC()V

    .line 1510
    :cond_8
    invoke-virtual {v0}, Ldcy;->ZO()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1511
    iget-object v2, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v2}, Lhac;->aMB()V

    .line 1513
    :cond_9
    invoke-virtual {v0}, Ldcy;->ZL()[I

    move-result-object v2

    .line 1514
    if-eqz v2, :cond_a

    .line 1515
    iget-object v3, p0, Lgyz;->cYs:Lgxe;

    invoke-virtual {v3, v2}, Lgxe;->t([I)V

    .line 1517
    :cond_a
    invoke-direct {p0, v1, v0}, Lgyz;->a(ZLdcy;)V

    .line 1518
    invoke-virtual {v0}, Ldcy;->ZT()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1519
    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-virtual {v0}, Ldcy;->aac()Z

    move-result v0

    invoke-interface {v1, v0}, Lhac;->gg(Z)V

    goto/16 :goto_0

    .line 1485
    :cond_b
    sget-object v2, Lgyt;->cZD:Lgyt;

    iget-object v3, p0, Lgyz;->dai:Lgyt;

    if-eq v2, v3, :cond_c

    sget-object v2, Lgyt;->cZF:Lgyt;

    iget-object v3, p0, Lgyz;->dai:Lgyt;

    if-ne v2, v3, :cond_d

    :cond_c
    invoke-direct {p0}, Lgyz;->aLQ()V

    :cond_d
    iget-boolean v2, p0, Lgyz;->as:Z

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lgyz;->bB:Z

    if-nez v2, :cond_e

    :goto_2
    invoke-static {v0}, Lifv;->gY(Z)V

    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v2, p0, Lgyz;->dan:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Leqo;->execute(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_e
    move v0, v1

    goto :goto_2

    .line 1486
    :cond_f
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    iget-object v2, p0, Lgyz;->dai:Lgyt;

    if-ne v0, v2, :cond_5

    .line 1487
    invoke-virtual {p0}, Lgyz;->aLT()V

    goto/16 :goto_1
.end method

.method public final a(Letj;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1888
    const-string v0, "VelvetPresenter state"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1889
    const-string v0, "mCurrentMode"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0, v1}, Letn;->f(Ljava/lang/Enum;)V

    .line 1890
    const-string v0, "mPendingMode"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgyz;->dai:Lgyt;

    invoke-virtual {v0, v1}, Letn;->f(Ljava/lang/Enum;)V

    .line 1891
    const-string v0, "mStarted"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lgyz;->cq:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 1892
    const-string v0, "mResumed"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lgyz;->as:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 1893
    const-string v0, "mFocused"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lgyz;->dak:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 1894
    const-string v0, "mRestoredInstance"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lgyz;->dag:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 1897
    const-string v0, "Webview"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgyz;->cYN:Landroid/view/View;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1898
    iget-object v0, p0, Lgyz;->cYN:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1899
    const-string v0, "Webview (layout params)"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgyz;->cYN:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 1902
    :cond_0
    const-string v0, "SearchServiceClient"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 1903
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0}, Lgyt;->aLI()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a0746

    .line 1906
    :goto_0
    const v1, 0x7f0a0745

    invoke-virtual {p1, v1}, Letj;->in(I)Letn;

    move-result-object v1

    invoke-virtual {v1}, Letn;->avS()Letn;

    move-result-object v1

    invoke-virtual {v1, v0}, Letn;->io(I)V

    .line 1909
    return-void

    .line 1903
    :cond_1
    const v0, 0x7f0a0747

    goto :goto_0
.end method

.method final a(Lgxi;)Z
    .locals 1

    .prologue
    .line 1113
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aJU()Lgpu;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lgyz;->mVelvetFactory:Lgpu;

    return-object v0
.end method

.method public final aLP()Z
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DL()Lcrh;

    move-result-object v0

    invoke-virtual {v0}, Lcrh;->Su()Z

    move-result v0

    return v0
.end method

.method final aLS()V
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lgyz;->dai:Lgyt;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    iget-object v0, p0, Lgyz;->dai:Lgyt;

    sget-object v1, Lgyt;->cZA:Lgyt;

    if-ne v0, v1, :cond_0

    .line 419
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->finish()V

    .line 421
    :cond_0
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    .line 422
    iget-object v1, p0, Lgyz;->dai:Lgyt;

    iput-object v1, p0, Lgyz;->dah:Lgyt;

    .line 423
    iget-object v1, p0, Lgyz;->mEventBus:Ldda;

    invoke-direct {p0, v0, v1}, Lgyz;->a(Lgyt;Ldda;)V

    .line 424
    return-void
.end method

.method final aLT()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 475
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0}, Lgyt;->aLN()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0, v2}, Lgyz;->a(ZLjava/lang/String;Landroid/os/Bundle;)V

    .line 476
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0}, Lgyt;->aLO()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0, v2}, Lgyz;->a(ZLjava/lang/String;Landroid/os/Bundle;)V

    .line 477
    return-void
.end method

.method public final aLV()I
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aLV()I

    move-result v0

    return v0
.end method

.method public final aLW()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 566
    iget-object v0, p0, Lgyz;->bFB:Landroid/content/Intent;

    invoke-static {v0}, Lgyz;->P(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    invoke-virtual {p0}, Lgyz;->aMc()Lgzz;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1, v2}, Lgzz;->c(IIZ)V

    .line 570
    invoke-virtual {p0}, Lgyz;->aMc()Lgzz;

    move-result-object v0

    invoke-interface {v0}, Lgzz;->UK()V

    .line 571
    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->cZW:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->j(Ljava/lang/Runnable;)V

    .line 575
    :goto_0
    return-void

    .line 573
    :cond_0
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->connect()V

    goto :goto_0
.end method

.method public final aLX()Ledu;
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lgyz;->bUp:Ledu;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lgyz;->aLQ()V

    .line 665
    :cond_0
    iget-object v0, p0, Lgyz;->bUp:Ledu;

    return-object v0
.end method

.method public final aLY()Leen;
    .locals 1

    .prologue
    .line 669
    invoke-static {}, Lenu;->auR()V

    .line 670
    iget-object v0, p0, Lgyz;->bXu:Leen;

    if-nez v0, :cond_0

    .line 671
    new-instance v0, Lgzv;

    invoke-direct {v0, p0}, Lgzv;-><init>(Lgyz;)V

    iput-object v0, p0, Lgyz;->bXu:Leen;

    .line 673
    :cond_0
    iget-object v0, p0, Lgyz;->bXu:Leen;

    return-object v0
.end method

.method public final aLZ()Lela;
    .locals 1

    .prologue
    .line 681
    invoke-static {}, Lenu;->auR()V

    .line 682
    iget-object v0, p0, Lgyz;->dac:Lela;

    if-nez v0, :cond_0

    .line 683
    iget-object v0, p0, Lgyz;->mVelvetFactory:Lgpu;

    invoke-virtual {v0}, Lgpu;->aJy()Lela;

    move-result-object v0

    iput-object v0, p0, Lgyz;->dac:Lela;

    .line 685
    :cond_0
    iget-object v0, p0, Lgyz;->dac:Lela;

    return-object v0
.end method

.method public final aMa()V
    .locals 3

    .prologue
    .line 693
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    const-string v2, "summons"

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->kT(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {p0}, Lgyz;->aLX()Ledu;

    move-result-object v2

    invoke-virtual {v2}, Ledu;->anZ()Lehj;

    move-result-object v2

    invoke-virtual {v2}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhaa;->y(Lcom/google/android/shared/search/Query;)V

    .line 697
    return-void
.end method

.method public final aMb()V
    .locals 4

    .prologue
    .line 700
    iget-object v0, p0, Lgyz;->cZY:Lgyv;

    iget-object v1, p0, Lgyz;->dah:Lgyt;

    iget-object v2, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    iget-object v3, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lgyv;->a(Lgyt;Lcom/google/android/shared/search/Query;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->UF()V

    .line 703
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    const-string v2, "summons"

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->kT(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->x(Lcom/google/android/shared/search/Query;)V

    .line 707
    :goto_0
    return-void

    .line 705
    :cond_0
    invoke-virtual {p0}, Lgyz;->Xb()Z

    goto :goto_0
.end method

.method final aMc()Lgzz;
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aMI()Lgzz;

    move-result-object v0

    return-object v0
.end method

.method final aMd()V
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZA()Z

    move-result v0

    invoke-direct {p0, v0}, Lgyz;->gd(Z)V

    .line 768
    :cond_0
    return-void
.end method

.method final aMe()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 806
    invoke-direct {p0}, Lgyz;->DR()Lcpx;

    .line 810
    iget-object v0, p0, Lgyz;->mDebugFeatures:Lckw;

    iget-object v1, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->BK()Lcke;

    move-result-object v1

    invoke-virtual {v0, v1}, Lckw;->a(Lcke;)V

    .line 812
    invoke-virtual {p0}, Lgyz;->aMx()Lfnx;

    move-result-object v0

    iput-object v0, p0, Lgyz;->dab:Lfnx;

    .line 813
    iget-object v0, p0, Lgyz;->mDebugFeatures:Lckw;

    invoke-virtual {v0}, Lckw;->Pd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyz;->mDebugFeatures:Lckw;

    invoke-virtual {v0}, Lckw;->Pb()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyz;->dab:Lfnx;

    const-string v1, "[DEBUG] Sysdump"

    new-instance v2, Lgzu;

    invoke-direct {v2, p0}, Lgzu;-><init>(Lgyz;)V

    invoke-virtual {v0, v1, v2}, Lfnx;->a(Ljava/lang/CharSequence;Lfok;)V

    iget-object v0, p0, Lgyz;->dab:Lfnx;

    const-string v1, "[DEBUG] Dump SRP HTML"

    new-instance v2, Lgzb;

    invoke-direct {v2, p0}, Lgzb;-><init>(Lgyz;)V

    invoke-virtual {v0, v1, v2}, Lfnx;->a(Ljava/lang/CharSequence;Lfok;)V

    iget-object v0, p0, Lgyz;->dab:Lfnx;

    const-string v1, "[DEBUG] View in Chrome"

    new-instance v2, Lgzc;

    invoke-direct {v2, p0}, Lgzc;-><init>(Lgyz;)V

    invoke-virtual {v0, v1, v2}, Lfnx;->a(Ljava/lang/CharSequence;Lfok;)V

    iget-object v0, p0, Lgyz;->dab:Lfnx;

    const-string v1, "[DEBUG] Icing corpora update"

    new-instance v2, Lgzd;

    invoke-direct {v2, p0}, Lgzd;-><init>(Lgyz;)V

    invoke-virtual {v0, v1, v2}, Lfnx;->a(Ljava/lang/CharSequence;Lfok;)V

    iget-object v0, p0, Lgyz;->dab:Lfnx;

    invoke-virtual {p0}, Lgyz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0a04ab

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lgze;

    invoke-direct {v2, p0}, Lgze;-><init>(Lgyz;)V

    invoke-virtual {v0, v1, v2}, Lfnx;->a(Ljava/lang/CharSequence;Lfok;)V

    .line 815
    :cond_0
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 816
    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v1

    .line 817
    iget-object v2, v1, Lchr;->dK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v3, v1, Lchr;->aXn:I

    if-nez v3, :cond_1

    iget-object v3, v1, Lchr;->aXk:Lcyg;

    invoke-interface {v3}, Lcyg;->EI()V

    iget-object v3, v1, Lchr;->mMainPrefs:Lcyg;

    if-eqz v3, :cond_1

    iget-object v3, v1, Lchr;->mMainPrefs:Lcyg;

    invoke-interface {v3}, Lcyg;->EI()V

    :cond_1
    iget v3, v1, Lchr;->aXn:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lchr;->aXn:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 821
    iget-object v2, p0, Lgyz;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lgzs;

    const-string v4, "Allow writes"

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    invoke-direct {v3, p0, v4, v5, v1}, Lgzs;-><init>(Lgyz;Ljava/lang/String;[ILchr;)V

    const-wide/16 v4, 0x7d0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 828
    iget-object v1, p0, Lgyz;->cZY:Lgyv;

    iput-boolean v6, v1, Lgyv;->cZQ:Z

    .line 829
    iget-object v1, p0, Lgyz;->dah:Lgyt;

    sget-object v2, Lgyt;->cZB:Lgyt;

    if-ne v1, v2, :cond_2

    .line 830
    iget-object v1, p0, Lgyz;->cZY:Lgyv;

    iput-boolean v6, v1, Lgyv;->cZR:Z

    .line 832
    :cond_2
    invoke-virtual {p0}, Lgyz;->aLT()V

    .line 833
    iget-object v1, p0, Lgyz;->dah:Lgyt;

    sget-object v2, Lgyt;->cZB:Lgyt;

    if-eq v1, v2, :cond_3

    iget-boolean v1, p0, Lgyz;->bB:Z

    if-nez v1, :cond_3

    .line 834
    iget-object v1, p0, Lgyz;->cZY:Lgyv;

    iput-boolean v6, v1, Lgyv;->cZR:Z

    .line 835
    iget-object v1, p0, Lgyz;->mUiThread:Leqo;

    iget-object v2, p0, Lgyz;->dao:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 840
    :cond_3
    iget-object v1, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DO()Lgpp;

    move-result-object v1

    invoke-interface {v1}, Lgpp;->aJi()V

    .line 841
    iget-object v1, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->DO()Lgpp;

    move-result-object v1

    invoke-interface {v1}, Lgpp;->aJj()V

    .line 845
    invoke-virtual {v0}, Lgql;->aJP()V

    .line 846
    iget-object v0, p0, Lgyz;->cZY:Lgyv;

    iput-boolean v6, v0, Lgyv;->cZP:Z

    .line 847
    return-void

    .line 817
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 821
    nop

    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method

.method final aMf()V
    .locals 6

    .prologue
    .line 854
    new-instance v0, Lgzt;

    const-string v2, "Profile restriction lookup"

    iget-object v3, p0, Lgyz;->mUiThread:Leqo;

    iget-object v4, p0, Lgyz;->mNonUiExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    const/4 v1, 0x2

    new-array v5, v1, [I

    fill-array-data v5, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lgzt;-><init>(Lgyz;Ljava/lang/String;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;[I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lgzt;->a([Ljava/lang/Object;)Lenp;

    .line 874
    const/16 v0, 0x109

    invoke-static {v0}, Lege;->ht(I)V

    .line 876
    return-void

    .line 854
    :array_0
    .array-data 4
        0x2
        0x0
    .end array-data
.end method

.method public final aMg()V
    .locals 1

    .prologue
    .line 1088
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgyz;->dal:Z

    .line 1089
    return-void
.end method

.method public final aMh()Letj;
    .locals 2

    .prologue
    .line 1238
    new-instance v0, Letj;

    iget-object v1, p0, Lgyz;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 1240
    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-virtual {v0, v1}, Letj;->b(Leti;)V

    .line 1241
    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v1, v0}, Lhaa;->b(Letj;)V

    .line 1242
    return-object v0
.end method

.method final aMj()V
    .locals 2

    .prologue
    .line 1388
    invoke-direct {p0}, Lgyz;->aMk()V

    .line 1389
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apt()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->x(Lcom/google/android/shared/search/Query;)V

    .line 1390
    invoke-virtual {p0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Ledu;->aQ(Lcom/google/android/shared/search/Query;)Ledu;

    .line 1391
    return-void
.end method

.method public final aMl()Landroid/view/View;
    .locals 1

    .prologue
    .line 1597
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1598
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgyz;->gd(Z)V

    .line 1600
    :cond_0
    iget-object v0, p0, Lgyz;->cYN:Landroid/view/View;

    return-object v0
.end method

.method public final aMn()I
    .locals 3

    .prologue
    .line 1621
    iget-object v0, p0, Lgyz;->cZY:Lgyv;

    iget-object v0, p0, Lgyz;->dah:Lgyt;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lgyv;->b(Lgyt;ZZ)I

    move-result v0

    return v0
.end method

.method public final aMp()Z
    .locals 1

    .prologue
    .line 1912
    iget-object v0, p0, Lgyz;->daa:Lgxa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyz;->daa:Lgxa;

    invoke-virtual {v0}, Lgxa;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final aMq()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 2028
    .line 2029
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v4

    .line 2030
    invoke-virtual {v4}, Lchk;->Kk()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2031
    invoke-virtual {p0}, Lgyz;->aMh()Letj;

    move-result-object v3

    .line 2032
    const-string v0, "FEEDBACK_LOG"

    const-string v1, " VelvetActivity Dump ======="

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v5}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 2033
    const-string v0, "FEEDBACK_LOG"

    const-string v1, " Use scripts/dump_from_syslog.py to make a readable dump."

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v5}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 2039
    const-string v0, "##"

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    invoke-virtual {v3}, Letj;->avP()Ljava/lang/Iterable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v5, "##"

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2044
    const/16 v0, 0xfa0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v2

    .line 2045
    :goto_0
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 2046
    const-string v7, "FEEDBACK_LOG"

    invoke-virtual {v5, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v7, v1, v8}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 2048
    add-int/lit16 v1, v0, 0xfa0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_0

    .line 2050
    :cond_0
    const-string v0, "FEEDBACK_LOG"

    const-string v1, " VelvetActivity Dump Complete ======="

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v3

    .line 2057
    :goto_1
    invoke-virtual {v4}, Lchk;->HV()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2058
    if-eqz v0, :cond_1

    .line 2059
    :goto_2
    invoke-virtual {v0}, Letj;->avK()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v7

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2058
    :cond_1
    invoke-virtual {p0}, Lgyz;->aMh()Letj;

    move-result-object v0

    goto :goto_2

    :cond_2
    move-object v7, v6

    .line 2062
    :cond_3
    new-instance v0, Lgpk;

    iget-object v1, p0, Lgyz;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lgpk;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lgyz;->aMi()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgpk;->nv(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2063
    invoke-virtual {p0}, Lgyz;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0}, Lgyz;->aMi()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v2}, Lgql;->SC()Lcfo;

    move-result-object v2

    invoke-virtual {v2}, Lcfo;->DL()Lcrh;

    move-result-object v2

    invoke-virtual {v2}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v2

    iget-object v4, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v4}, Lgyt;->aLI()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v4}, Lhac;->atC()Lcom/google/android/shared/ui/CoScrollContainer;

    move-result-object v4

    :goto_4
    invoke-virtual {p0}, Lgyz;->aLP()Z

    move-result v5

    invoke-static/range {v0 .. v7}, Lgbk;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/accounts/Account;Landroid/net/Uri;Landroid/view/View;ZLjava/util/List;Ljava/util/List;)V

    .line 2071
    return-void

    .line 2063
    :cond_4
    iget-object v4, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v4}, Lhac;->aMM()Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->getContentView()Landroid/view/View;

    move-result-object v4

    goto :goto_4

    :cond_5
    move-object v0, v6

    goto/16 :goto_1
.end method

.method final aMr()V
    .locals 2

    .prologue
    .line 2075
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2080
    :goto_0
    return-void

    .line 2079
    :cond_0
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-virtual {p0}, Lgyz;->aMh()Letj;

    move-result-object v1

    invoke-virtual {v1}, Letj;->avL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lhac;->nJ(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final aMs()V
    .locals 3

    .prologue
    .line 2084
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2091
    :goto_0
    return-void

    .line 2088
    :cond_0
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 2089
    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v1, v2}, Lhaa;->a(Ljava/io/PrintWriter;)V

    .line 2090
    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lhac;->nJ(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final aMt()V
    .locals 1

    .prologue
    .line 2095
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2096
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->Nl()V

    .line 2100
    :cond_0
    return-void
.end method

.method final aMu()V
    .locals 2

    .prologue
    .line 2104
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109
    :goto_0
    return-void

    .line 2108
    :cond_0
    iget-object v0, p0, Lgyz;->mAppContext:Landroid/content/Context;

    iget-object v1, p0, Lgyz;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->al(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method final aMv()V
    .locals 3

    .prologue
    .line 2113
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lgyz;->mAppContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/sidekick/main/TestLauncherActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2115
    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v1, v0}, Lhac;->startActivity(Landroid/content/Intent;)V

    .line 2116
    return-void
.end method

.method public final aMw()V
    .locals 1

    .prologue
    .line 2119
    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    invoke-virtual {v0}, Lgyd;->aLD()V

    .line 2120
    return-void
.end method

.method protected aMx()Lfnx;
    .locals 6

    .prologue
    .line 2124
    new-instance v0, Lfnx;

    invoke-virtual {p0}, Lgyz;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v2}, Lhac;->aMM()Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->aCL()Lfqn;

    move-result-object v2

    iget-object v3, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v3}, Lhac;->aMM()Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    move-result-object v3

    new-instance v4, Lgyx;

    iget-object v5, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-direct {v4, v5}, Lgyx;-><init>(Lgql;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lfnx;-><init>(Landroid/app/Activity;Lfom;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfon;)V

    .line 2129
    new-instance v1, Lgzm;

    invoke-virtual {p0}, Lgyz;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v3}, Lhac;->aMM()Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lgzm;-><init>(Lgyz;Landroid/app/Activity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lfnx;->a(Lfoh;)V

    .line 2145
    iget-object v1, p0, Lgyz;->aOR:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lfnx;->onPostCreate(Landroid/os/Bundle;)V

    .line 2146
    iget-boolean v1, p0, Lgyz;->cq:Z

    if-eqz v1, :cond_0

    .line 2147
    invoke-virtual {v0}, Lfnx;->onStart()V

    .line 2149
    :cond_0
    iget-boolean v1, p0, Lgyz;->as:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lgyz;->dam:Z

    if-nez v1, :cond_1

    .line 2150
    invoke-virtual {v0}, Lfnx;->onResume()V

    .line 2152
    :cond_1
    return-object v0
.end method

.method final ab(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 584
    iput-boolean v4, p0, Lgyz;->rE:Z

    .line 585
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->Nf()Ldda;

    move-result-object v0

    iput-object v0, p0, Lgyz;->mEventBus:Ldda;

    .line 586
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 587
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    .line 591
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DQ()Lcpd;

    move-result-object v0

    iget-object v1, p0, Lgyz;->daf:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v1}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgyz;->daf:Lcom/google/android/shared/search/SearchBoxStats;

    invoke-virtual {v2}, Lcom/google/android/shared/search/SearchBoxStats;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcpd;->P(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    iget-object v0, p0, Lgyz;->mVelvetFactory:Lgpu;

    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v1}, Lhac;->aMK()Lgxh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgpu;->a(Lgxh;)Lgxe;

    move-result-object v0

    iput-object v0, p0, Lgyz;->cYs:Lgxe;

    .line 597
    iget-object v0, p0, Lgyz;->cYs:Lgxe;

    iget-object v1, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0, p0, v1, p1}, Lgxe;->a(Lgyz;Ldda;Landroid/os/Bundle;)V

    .line 599
    iget-object v0, p0, Lgyz;->mVelvetFactory:Lgpu;

    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aMJ()Lgxb;

    move-result-object v0

    invoke-static {v0}, Lgpu;->a(Lgxb;)Lgxa;

    move-result-object v0

    iput-object v0, p0, Lgyz;->daa:Lgxa;

    .line 602
    iget-object v0, p0, Lgyz;->mVelvetFactory:Lgpu;

    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aMI()Lgzz;

    move-result-object v0

    invoke-static {v0}, Lgpu;->a(Lgzz;)Lgyd;

    move-result-object v0

    iput-object v0, p0, Lgyz;->cZZ:Lgyd;

    .line 604
    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    iget-object v1, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0, p0, v1, p1}, Lgyd;->a(Lgyz;Ldda;Landroid/os/Bundle;)V

    .line 609
    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgyd;->aX(Lcom/google/android/shared/search/Query;)V

    .line 611
    if-eqz p1, :cond_0

    .line 612
    const-string v0, "velvet:velvet_presenter:back"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0, p1}, Lgyz;->a(ZLjava/lang/String;Landroid/os/Bundle;)V

    .line 614
    const-string v0, "velvet:velvet_presenter:front"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v4, v0, p1}, Lgyz;->a(ZLjava/lang/String;Landroid/os/Bundle;)V

    .line 619
    :cond_0
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aun()V

    .line 620
    invoke-direct {p0, v3}, Lgyz;->ge(Z)V

    .line 621
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    iget-object v1, p0, Lgyz;->mEventBus:Ldda;

    invoke-direct {p0, v0, v1}, Lgyz;->a(Lgyt;Ldda;)V

    .line 623
    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->cZU:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->j(Ljava/lang/Runnable;)V

    .line 624
    return-void
.end method

.method public final afm()V
    .locals 0

    .prologue
    .line 1093
    invoke-direct {p0}, Lgyz;->aMk()V

    .line 1094
    return-void
.end method

.method public final afn()V
    .locals 2

    .prologue
    .line 1100
    invoke-virtual {p0}, Lgyz;->wj()Lekf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lekf;->hG(I)V

    .line 1101
    invoke-virtual {p0}, Lgyz;->aLX()Ledu;

    move-result-object v0

    invoke-virtual {v0}, Ledu;->anU()Ledu;

    .line 1102
    return-void
.end method

.method public final c(Lcom/google/android/search/suggest/SuggestionListView;)V
    .locals 1

    .prologue
    .line 2022
    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    invoke-virtual {v0, p1}, Lgyd;->b(Lcom/google/android/search/suggest/SuggestionListView;)V

    .line 2023
    return-void
.end method

.method final d(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0, p1}, Lhac;->d(Landroid/view/View$OnTouchListener;)V

    .line 880
    return-void
.end method

.method public final e(Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 1868
    iget-boolean v0, p0, Lgyz;->bB:Z

    if-nez v0, :cond_0

    .line 1869
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0, p1}, Lhaa;->b(Landroid/graphics/Point;)V

    .line 1871
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 1409
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1410
    invoke-virtual {v0, p1, p2}, Lcom/google/android/shared/search/Query;->b(Ljava/lang/CharSequence;I)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1411
    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v0}, Lcom/google/android/search/core/state/QueryState;->x(Lcom/google/android/shared/search/Query;)V

    .line 1412
    invoke-virtual {p0}, Lgyz;->aLX()Ledu;

    move-result-object v1

    invoke-virtual {v1, v0}, Ledu;->aQ(Lcom/google/android/shared/search/Query;)Ledu;

    .line 1413
    return-void
.end method

.method public final f(Landroid/view/Menu;)V
    .locals 4

    .prologue
    .line 1233
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    invoke-interface {v0, p1}, Lcke;->e(Landroid/view/Menu;)V

    new-instance v0, Lbyl;

    invoke-direct {v0}, Lbyl;-><init>()V

    invoke-static {}, Lbyl;->BW()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a05cd

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lgyz;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lbwx;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "entry-point"

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    :cond_0
    const v0, 0x7f0a0583

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lgzk;

    invoke-direct {v1, p0}, Lgzk;-><init>(Lgyz;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    iget-object v0, p0, Lgyz;->mDebugFeatures:Lckw;

    invoke-virtual {v0}, Lckw;->Pd()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgyz;->mDebugFeatures:Lckw;

    invoke-virtual {v0}, Lckw;->Pb()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "[DEBUG] Sysdump"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lgzf;

    invoke-direct {v1, p0}, Lgzf;-><init>(Lgyz;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v0, "[DEBUG] Dump SRP HTML"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lgzg;

    invoke-direct {v1, p0}, Lgzg;-><init>(Lgyz;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v0, "[DEBUG] View in Chrome"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lgzh;

    invoke-direct {v1, p0}, Lgzh;-><init>(Lgyz;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const-string v0, "[DEBUG] Icing corpora update"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lgzi;

    invoke-direct {v1, p0}, Lgzi;-><init>(Lgyz;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    const v0, 0x7f0a04ab

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, Lgzj;

    invoke-direct {v1, p0}, Lgzj;-><init>(Lgyz;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1234
    :cond_1
    return-void
.end method

.method public final g(Ljava/lang/CharSequence;II)V
    .locals 5

    .prologue
    .line 1417
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 1418
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    .line 1419
    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1423
    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1453
    :goto_0
    return-void

    .line 1427
    :cond_0
    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/shared/search/Query;->c(Ljava/lang/CharSequence;II)Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 1429
    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1436
    if-ne p2, p3, :cond_1

    instance-of v1, v0, Landroid/text/Spanned;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/text/Spanned;

    const-class v1, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-static {v0, p2, v1}, Leqt;->a(Landroid/text/Spanned;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1444
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apD()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1451
    :goto_1
    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v0}, Lcom/google/android/search/core/state/QueryState;->x(Lcom/google/android/shared/search/Query;)V

    .line 1452
    invoke-virtual {p0}, Lgyz;->aLX()Ledu;

    move-result-object v1

    invoke-virtual {v1, v0}, Ledu;->aQ(Lcom/google/android/shared/search/Query;)Ledu;

    goto :goto_0

    .line 1446
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apE()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1

    .line 1449
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apE()Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1
.end method

.method final gc(Z)V
    .locals 3

    .prologue
    .line 771
    invoke-direct {p0}, Lgyz;->aMm()V

    .line 772
    invoke-direct {p0}, Lgyz;->aLU()V

    .line 773
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lgyz;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 774
    const-string v0, "VelvetPresenter"

    const-string v1, "Velvet detached from service, closing."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 775
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->finish()V

    .line 777
    :cond_0
    return-void
.end method

.method public final getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 652
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method protected final getDimensionPixelSize(I)I
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Lgyz;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final getLayoutInflater()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public final i(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    if-eqz v0, :cond_0

    .line 639
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbd;->i(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 641
    :cond_0
    return-void
.end method

.method public final kJ(I)V
    .locals 3

    .prologue
    .line 1106
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    iget-object v1, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {p0}, Lgyz;->aLX()Ledu;

    move-result-object v2

    iput p1, v2, Ledu;->bVW:I

    invoke-virtual {v2}, Ledu;->anZ()Lehj;

    move-result-object v2

    invoke-virtual {v2}, Lehj;->arV()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhaa;->y(Lcom/google/android/shared/search/Query;)V

    .line 1110
    return-void
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1060
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgyz;->bB:Z

    .line 1061
    invoke-direct {p0}, Lgyz;->aLR()V

    .line 1062
    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->cZU:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->cZW:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->cZV:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 1063
    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->dao:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 1065
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    if-eqz v0, :cond_0

    .line 1066
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    invoke-virtual {v0}, Lfnx;->onDestroy()V

    .line 1067
    iput-object v2, p0, Lgyz;->dab:Lfnx;

    .line 1070
    :cond_0
    iget-boolean v0, p0, Lgyz;->rE:Z

    if-eqz v0, :cond_1

    .line 1071
    invoke-direct {p0}, Lgyz;->aLU()V

    .line 1072
    iget-object v0, p0, Lgyz;->cYs:Lgxe;

    invoke-virtual {v0}, Lgxe;->onDetach()V

    .line 1073
    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    invoke-virtual {v0}, Lgyd;->onDetach()V

    .line 1075
    iput-object v2, p0, Lgyz;->cYs:Lgxe;

    .line 1076
    iput-object v2, p0, Lgyz;->cZZ:Lgyd;

    .line 1077
    iput-object v2, p0, Lgyz;->daa:Lgxa;

    .line 1080
    :cond_1
    invoke-direct {p0}, Lgyz;->aMm()V

    .line 1081
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->disconnect()V

    .line 1083
    iput-object v2, p0, Lgyz;->mSearchServiceClient:Lhaa;

    .line 1084
    iput-object v2, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 1085
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1169
    iget-object v2, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v2}, Lhaa;->isActive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1194
    :cond_0
    :goto_0
    return v0

    .line 1172
    :cond_1
    const/4 v2, 0x4

    if-ne p1, v2, :cond_5

    .line 1173
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aMM()Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->cN()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1174
    goto :goto_0

    .line 1175
    :cond_2
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v0}, Lgxi;->cN()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1176
    goto :goto_0

    .line 1177
    :cond_3
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v0}, Lgxi;->cN()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 1178
    goto :goto_0

    .line 1180
    :cond_4
    invoke-virtual {p0}, Lgyz;->Xb()Z

    move-result v0

    goto :goto_0

    .line 1182
    :cond_5
    const/16 v2, 0x54

    if-ne p1, v2, :cond_6

    .line 1183
    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    invoke-virtual {v0}, Lgyd;->UK()V

    move v0, v1

    .line 1184
    goto :goto_0

    .line 1185
    :cond_6
    iget-object v2, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v2}, Lgyt;->aLI()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1186
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v2

    int-to-char v2, v2

    .line 1187
    invoke-static {v2}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1189
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->UF()V

    .line 1190
    iget-object v0, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v3, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/search/core/state/QueryState;->x(Lcom/google/android/shared/search/Query;)V

    move v0, v1

    .line 1191
    goto :goto_0
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1199
    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v1}, Lhaa;->isActive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1208
    :cond_0
    :goto_0
    return v0

    .line 1202
    :cond_1
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1203
    iget-object v1, p0, Lgyz;->cZY:Lgyv;

    iget-object v1, p0, Lgyz;->dah:Lgyt;

    iget-object v2, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Yl()Z

    iget-object v2, p0, Lgyz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v2

    invoke-static {v1, v2}, Lgyv;->a(Lgyt;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1205
    invoke-virtual {p0}, Lgyz;->Xb()Z

    move-result v0

    goto :goto_0
.end method

.method public final onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1053
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgyz;->b(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 1054
    invoke-direct {p0}, Lgyz;->aLU()V

    .line 1055
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgyz;->ge(Z)V

    .line 1056
    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 975
    iget-boolean v0, p0, Lgyz;->as:Z

    if-nez v0, :cond_0

    .line 1016
    :goto_0
    return-void

    .line 980
    :cond_0
    iput-boolean v1, p0, Lgyz;->daj:Z

    .line 981
    iput-boolean v4, p0, Lgyz;->dal:Z

    .line 982
    iput-boolean v1, p0, Lgyz;->dam:Z

    .line 983
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    if-eqz v0, :cond_1

    .line 984
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v0}, Lgxi;->onPause()V

    .line 986
    :cond_1
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    if-eqz v0, :cond_2

    .line 987
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v0}, Lgxi;->onPause()V

    .line 990
    :cond_2
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "send_gsa_home_request"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lgpp;->r(Ljava/lang/String;J)V

    .line 994
    invoke-direct {p0}, Lgyz;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 995
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    if-eqz v0, :cond_3

    .line 996
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->aab()V

    .line 998
    :cond_3
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->anz()V

    .line 1005
    :goto_1
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0, p0}, Lhaa;->b(Lddc;)V

    .line 1006
    iput-boolean v4, p0, Lgyz;->as:Z

    .line 1008
    invoke-direct {p0}, Lgyz;->aLR()V

    .line 1009
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1010
    const/4 v0, 0x2

    invoke-static {v0}, Lege;->ht(I)V

    .line 1012
    invoke-static {}, Legm;->aoz()Legm;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Legm;)V

    .line 1013
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->closeOptionsMenu()V

    .line 1015
    :cond_4
    iput-boolean v4, p0, Lgyz;->dam:Z

    goto :goto_0

    .line 1000
    :cond_5
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    if-eqz v0, :cond_6

    .line 1001
    iget-object v0, p0, Lgyz;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZZ()V

    .line 1003
    :cond_6
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->stop()V

    goto :goto_1
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 916
    iput-boolean v2, p0, Lgyz;->as:Z

    .line 917
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgyz;->ge(Z)V

    .line 921
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0}, Lgyt;->aLI()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgyz;->aLP()Z

    move-result v0

    if-nez v0, :cond_0

    .line 922
    sget-object v0, Lgyt;->cZD:Lgyt;

    iput-object v0, p0, Lgyz;->dai:Lgyt;

    .line 924
    invoke-virtual {p0}, Lgyz;->aMj()V

    .line 927
    :cond_0
    iget-object v0, p0, Lgyz;->dai:Lgyt;

    iget-object v1, p0, Lgyz;->dah:Lgyt;

    if-eq v0, v1, :cond_1

    .line 928
    invoke-direct {p0}, Lgyz;->aLR()V

    .line 929
    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->dan:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 933
    :cond_1
    invoke-direct {p0}, Lgyz;->aMo()V

    .line 935
    iget-object v0, p0, Lgyz;->cZY:Lgyv;

    iget-boolean v0, v0, Lgyv;->cZP:Z

    if-eqz v0, :cond_2

    .line 936
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    invoke-interface {v0}, Lgpp;->aJi()V

    .line 939
    :cond_2
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_3

    .line 940
    const-string v0, "voice-search"

    invoke-static {v0}, Legm;->kS(Ljava/lang/String;)Legm;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Legm;)V

    .line 941
    invoke-static {v2}, Lege;->ht(I)V

    .line 945
    :cond_3
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    if-eqz v0, :cond_4

    .line 946
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v0}, Lgxi;->onResume()V

    .line 948
    :cond_4
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    if-eqz v0, :cond_5

    .line 949
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v0}, Lgxi;->onResume()V

    .line 955
    :cond_5
    iget-object v0, p0, Lgyz;->mVelvetServices:Lgql;

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BL()Lchk;

    move-result-object v0

    invoke-virtual {v0}, Lchk;->GR()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 956
    invoke-static {}, Legs;->aoI()V

    .line 963
    :cond_6
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0, p0}, Lhaa;->a(Lddc;)V

    .line 965
    iget-object v0, p0, Lgyz;->mUiThread:Leqo;

    iget-object v1, p0, Lgyz;->cZV:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->j(Ljava/lang/Runnable;)V

    .line 967
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    if-eqz v0, :cond_7

    .line 968
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    invoke-virtual {v0}, Lfnx;->onResume()V

    .line 970
    :cond_7
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 781
    invoke-direct {p0}, Lgyz;->isChangingConfigurations()Z

    move-result v0

    .line 782
    iget-object v1, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v1, p1, v0}, Lhaa;->a(Landroid/os/Bundle;Z)V

    .line 783
    iget-object v1, p0, Lgyz;->dae:Lgxi;

    if-eqz v1, :cond_0

    .line 784
    const-string v1, "velvet:velvet_presenter:front"

    iget-object v2, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v2}, Lgxi;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    iget-object v1, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v1, p1}, Lgxi;->Y(Landroid/os/Bundle;)V

    .line 787
    :cond_0
    iget-object v1, p0, Lgyz;->dad:Lgxi;

    if-eqz v1, :cond_1

    .line 788
    const-string v1, "velvet:velvet_presenter:back"

    iget-object v2, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v2}, Lgxi;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    iget-object v1, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v1, p1}, Lgxi;->Y(Landroid/os/Bundle;)V

    .line 791
    :cond_1
    iget-object v1, p0, Lgyz;->cYs:Lgxe;

    if-eqz v1, :cond_2

    .line 792
    iget-object v1, p0, Lgyz;->cYs:Lgxe;

    invoke-virtual {v1, p1}, Lgxe;->Y(Landroid/os/Bundle;)V

    .line 794
    :cond_2
    iget-object v1, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v1}, Lhac;->aMI()Lgzz;

    move-result-object v1

    invoke-interface {v1, p1}, Lgzz;->w(Landroid/os/Bundle;)V

    .line 795
    if-eqz v0, :cond_3

    .line 796
    const-string v0, "velvet:velvet_presenter:changing_config"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 798
    :cond_3
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    if-eqz v0, :cond_4

    .line 799
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    invoke-virtual {v0, p1}, Lfnx;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 801
    :cond_4
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 890
    iput-boolean v1, p0, Lgyz;->cq:Z

    .line 891
    iget-object v0, p0, Lgyz;->cZX:Lgpe;

    if-eqz v0, :cond_0

    .line 892
    iget-object v0, p0, Lgyz;->cZX:Lgpe;

    invoke-interface {v0}, Lgpe;->axE()V

    .line 894
    :cond_0
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    if-eqz v0, :cond_4

    .line 895
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v0}, Lgxi;->onStart()V

    .line 896
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v0, v1}, Lgxi;->ga(Z)V

    .line 905
    :cond_1
    :goto_0
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    if-eqz v0, :cond_2

    .line 906
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v0}, Lgxi;->onStart()V

    .line 907
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v0, v1}, Lgxi;->ga(Z)V

    .line 909
    :cond_2
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    if-eqz v0, :cond_3

    .line 910
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    invoke-virtual {v0}, Lfnx;->onStart()V

    .line 912
    :cond_3
    return-void

    .line 900
    :cond_4
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->aMH()Lcom/google/android/velvet/ui/MainContentView;

    move-result-object v0

    .line 901
    if-eqz v0, :cond_1

    .line 902
    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->atB()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->eZ(Z)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1020
    iput-boolean v1, p0, Lgyz;->cq:Z

    .line 1021
    iput-boolean v1, p0, Lgyz;->daj:Z

    .line 1025
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    if-eqz v0, :cond_0

    .line 1026
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v0, v1}, Lgxi;->ga(Z)V

    .line 1027
    iget-object v0, p0, Lgyz;->dad:Lgxi;

    invoke-virtual {v0}, Lgxi;->onStop()V

    .line 1029
    :cond_0
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    if-eqz v0, :cond_1

    .line 1030
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v0, v1}, Lgxi;->ga(Z)V

    .line 1031
    iget-object v0, p0, Lgyz;->dae:Lgxi;

    invoke-virtual {v0}, Lgxi;->onStop()V

    .line 1033
    :cond_1
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    if-eqz v0, :cond_2

    .line 1034
    iget-object v0, p0, Lgyz;->dab:Lfnx;

    invoke-virtual {v0}, Lfnx;->onStop()V

    .line 1039
    :cond_2
    invoke-direct {p0}, Lgyz;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1040
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->stop()V

    .line 1045
    :goto_0
    invoke-direct {p0}, Lgyz;->aLU()V

    .line 1046
    iget-object v0, p0, Lgyz;->cZX:Lgpe;

    if-eqz v0, :cond_3

    .line 1047
    iget-object v0, p0, Lgyz;->cZX:Lgpe;

    invoke-interface {v0}, Lgpe;->onActivityStop()V

    .line 1049
    :cond_3
    return-void

    .line 1042
    :cond_4
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0}, Lhaa;->anz()V

    goto :goto_0
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 5

    .prologue
    .line 1117
    iput-boolean p1, p0, Lgyz;->dak:Z

    .line 1118
    iget-object v0, p0, Lgyz;->mSearchServiceClient:Lhaa;

    invoke-virtual {v0, p1}, Lhaa;->bq(Z)V

    .line 1119
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lgyz;->as:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    if-eqz v0, :cond_0

    .line 1120
    iget-object v0, p0, Lgyz;->cZZ:Lgyd;

    iget-boolean v1, p0, Lgyz;->dak:Z

    const/4 v2, 0x0

    iget-object v3, p0, Lgyz;->dah:Lgyt;

    iget-object v4, p0, Lgyz;->cZY:Lgyv;

    iget-boolean v4, v4, Lgyv;->cZP:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lgyd;->a(ZZLgyt;Z)V

    .line 1123
    :cond_0
    return-void
.end method

.method public final t(Ldda;)Lgyt;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1527
    invoke-virtual {p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 1528
    invoke-virtual {p1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    .line 1529
    invoke-virtual {p1}, Ldda;->aan()Ldcy;

    move-result-object v2

    .line 1530
    invoke-virtual {p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    .line 1532
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1533
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apF()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1534
    sget-object v0, Lgyt;->cZM:Lgyt;

    .line 1567
    :goto_0
    return-object v0

    .line 1536
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v1

    .line 1537
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xh()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqs()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1538
    if-eqz v1, :cond_1

    sget-object v0, Lgyt;->cZI:Lgyt;

    goto :goto_0

    :cond_1
    sget-object v0, Lgyt;->cZF:Lgyt;

    goto :goto_0

    .line 1540
    :cond_2
    if-eqz v1, :cond_3

    sget-object v0, Lgyt;->cZK:Lgyt;

    goto :goto_0

    :cond_3
    sget-object v0, Lgyt;->cZD:Lgyt;

    goto :goto_0

    .line 1543
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqk()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1544
    invoke-static {v3}, Lgyt;->aY(Lcom/google/android/shared/search/Query;)Lgyt;

    move-result-object v0

    goto :goto_0

    .line 1545
    :cond_5
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqU()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1546
    sget-object v0, Lgyt;->cZI:Lgyt;

    goto :goto_0

    .line 1547
    :cond_6
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqs()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1548
    sget-object v0, Lgyt;->cZB:Lgyt;

    goto :goto_0

    .line 1549
    :cond_7
    invoke-virtual {v2}, Ldcy;->ZC()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1550
    sget-object v0, Lgyt;->cZJ:Lgyt;

    goto :goto_0

    .line 1551
    :cond_8
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v2}, Ldcy;->ZA()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v2}, Ldcy;->ZB()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1553
    sget-object v0, Lgyt;->cZC:Lgyt;

    goto :goto_0

    .line 1554
    :cond_9
    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    invoke-virtual {v1, v3}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1556
    sget-object v0, Lgyt;->cZL:Lgyt;

    goto/16 :goto_0

    .line 1557
    :cond_b
    sget-object v0, Lcgg;->aVn:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {v2}, Ldcy;->ZB()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lgyz;->dah:Lgyt;

    sget-object v1, Lgyt;->cZG:Lgyt;

    if-ne v0, v1, :cond_d

    .line 1559
    :cond_c
    sget-object v0, Lgyt;->cZG:Lgyt;

    goto/16 :goto_0

    .line 1560
    :cond_d
    sget-object v0, Lcgg;->aVn:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v3}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v2}, Ldcy;->ZA()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1562
    sget-object v0, Lgyt;->cZH:Lgyt;

    goto/16 :goto_0

    .line 1564
    :cond_e
    sget-object v0, Lgyt;->cZE:Lgyt;

    goto/16 :goto_0
.end method

.method public final wj()Lekf;
    .locals 1

    .prologue
    .line 1126
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->atC()Lcom/google/android/shared/ui/CoScrollContainer;

    move-result-object v0

    return-object v0
.end method

.method protected final wk()Z
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lgyz;->dah:Lgyt;

    invoke-virtual {v0}, Lgyt;->aLI()Z

    move-result v0

    return v0
.end method

.method public final wr()Leoj;
    .locals 1

    .prologue
    .line 1404
    iget-object v0, p0, Lgyz;->cZT:Lhac;

    invoke-interface {v0}, Lhac;->wr()Leoj;

    move-result-object v0

    return-object v0
.end method
