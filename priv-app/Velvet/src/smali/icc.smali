.class public final Licc;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ldgm;Ljoe;)Licd;
    .locals 13
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 47
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p1, Ljoe;->ewg:[Ljof;

    if-nez v0, :cond_1

    .line 48
    :cond_0
    const-string v0, "IcingMatchHelper"

    const-string v1, "getFirstIcingMatch: Icing connection or query constraint is null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 49
    const/4 v0, 0x0

    .line 107
    :goto_0
    return-object v0

    .line 51
    :cond_1
    new-instance v0, Lbca;

    invoke-direct {v0}, Lbca;-><init>()V

    .line 53
    invoke-virtual {p1}, Ljoe;->brd()Ljava/lang/String;

    move-result-object v5

    .line 54
    if-eqz v5, :cond_2

    .line 55
    invoke-virtual {v0, v5}, Lbca;->eL(Ljava/lang/String;)Lbca;

    .line 57
    :cond_2
    invoke-virtual {p1}, Ljoe;->getQuery()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x14

    invoke-virtual {v0}, Lbca;->xb()Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v0}, Ldgm;->a(Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_d

    .line 65
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/SearchResults;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbch;

    .line 66
    iget-object v9, p1, Ljoe;->ewg:[Ljof;

    array-length v10, v9

    const/4 v0, 0x0

    move v7, v0

    :goto_1
    if-ge v7, v10, :cond_3

    aget-object v11, v9, v7

    .line 67
    invoke-virtual {v11}, Ljof;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljym;->hz(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    .line 68
    :goto_2
    invoke-virtual {v11}, Ljof;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljym;->hz(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    move v2, v1

    .line 69
    :goto_3
    invoke-virtual {v11}, Ljof;->xg()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljym;->hz(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x1

    move v3, v1

    .line 71
    :goto_4
    const-string v1, "text1"

    invoke-virtual {v4, v1}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    const-string v6, "text2"

    invoke-virtual {v4, v6}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 73
    invoke-virtual {v4}, Lbch;->xg()Ljava/lang/String;

    move-result-object v12

    .line 80
    if-eqz v3, :cond_4

    invoke-virtual {v11}, Ljof;->xg()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 82
    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v11}, Ljof;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 85
    :cond_5
    if-eqz v2, :cond_6

    invoke-virtual {v11}, Ljof;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 89
    :cond_6
    invoke-virtual {v11}, Ljof;->aUK()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v11}, Ljof;->brg()I

    move-result v8

    .line 96
    :goto_5
    invoke-virtual {v11}, Ljof;->bri()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v11}, Ljof;->brh()Ljava/lang/String;

    move-result-object v2

    .line 99
    :goto_6
    new-instance v0, Licd;

    const-string v3, "icon"

    invoke-virtual {v4, v3}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "intent_data"

    invoke-virtual {v4, v6}, Lbch;->eQ(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Ljof;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11}, Ljof;->brf()I

    move-result v7

    invoke-direct/range {v0 .. v8}, Licd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 67
    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 68
    :cond_8
    const/4 v1, 0x0

    move v2, v1

    goto :goto_3

    .line 69
    :cond_9
    const/4 v1, 0x0

    move v3, v1

    goto :goto_4

    .line 89
    :cond_a
    const/4 v8, -0x1

    goto :goto_5

    :cond_b
    move-object v2, v6

    .line 96
    goto :goto_6

    .line 66
    :cond_c
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_1

    .line 107
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
