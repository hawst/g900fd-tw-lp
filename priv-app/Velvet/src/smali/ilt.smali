.class final Lilt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field private synthetic dDS:Lilf;

.field private dEa:Lils;

.field dEb:Lils;

.field private dEc:Lils;

.field private dEf:I


# direct methods
.method constructor <init>(Lilf;)V
    .locals 1

    .prologue
    .line 285
    iput-object p1, p0, Lilt;->dDS:Lilf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    invoke-static {p1}, Lilf;->a(Lilf;)Lils;

    move-result-object v0

    iput-object v0, p0, Lilt;->dEa:Lils;

    .line 287
    return-void
.end method

.method constructor <init>(Lilf;I)V
    .locals 2

    .prologue
    .line 288
    iput-object p1, p0, Lilt;->dDS:Lilf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    invoke-virtual {p1}, Lilf;->size()I

    move-result v1

    .line 290
    invoke-static {p2, v1}, Lifv;->bk(II)I

    .line 291
    div-int/lit8 v0, v1, 0x2

    if-lt p2, v0, :cond_0

    .line 292
    invoke-static {p1}, Lilf;->b(Lilf;)Lils;

    move-result-object v0

    iput-object v0, p0, Lilt;->dEc:Lils;

    .line 293
    iput v1, p0, Lilt;->dEf:I

    .line 294
    :goto_0
    add-int/lit8 v0, p2, 0x1

    if-ge p2, v1, :cond_1

    .line 295
    invoke-virtual {p0}, Lilt;->aXn()Lils;

    move p2, v0

    goto :goto_0

    .line 298
    :cond_0
    invoke-static {p1}, Lilf;->a(Lilf;)Lils;

    move-result-object v0

    iput-object v0, p0, Lilt;->dEa:Lils;

    .line 299
    :goto_1
    add-int/lit8 v0, p2, -0x1

    if-lez p2, :cond_1

    .line 300
    invoke-virtual {p0}, Lilt;->aXm()Lils;

    move p2, v0

    goto :goto_1

    .line 303
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lilt;->dEb:Lils;

    .line 304
    return-void
.end method


# virtual methods
.method public final aXm()Lils;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lilt;->dEa:Lils;

    invoke-static {v0}, Lilf;->bA(Ljava/lang/Object;)V

    .line 312
    iget-object v0, p0, Lilt;->dEa:Lils;

    iput-object v0, p0, Lilt;->dEb:Lils;

    iput-object v0, p0, Lilt;->dEc:Lils;

    .line 313
    iget-object v0, p0, Lilt;->dEa:Lils;

    iget-object v0, v0, Lils;->dEa:Lils;

    iput-object v0, p0, Lilt;->dEa:Lils;

    .line 314
    iget v0, p0, Lilt;->dEf:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lilt;->dEf:I

    .line 315
    iget-object v0, p0, Lilt;->dEb:Lils;

    return-object v0
.end method

.method public final aXn()Lils;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lilt;->dEc:Lils;

    invoke-static {v0}, Lilf;->bA(Ljava/lang/Object;)V

    .line 336
    iget-object v0, p0, Lilt;->dEc:Lils;

    iput-object v0, p0, Lilt;->dEb:Lils;

    iput-object v0, p0, Lilt;->dEa:Lils;

    .line 337
    iget-object v0, p0, Lilt;->dEc:Lils;

    iget-object v0, v0, Lils;->dEc:Lils;

    iput-object v0, p0, Lilt;->dEc:Lils;

    .line 338
    iget v0, p0, Lilt;->dEf:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lilt;->dEf:I

    .line 339
    iget-object v0, p0, Lilt;->dEb:Lils;

    return-object v0
.end method

.method public final synthetic add(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 279
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lilt;->dEa:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lilt;->dEc:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 279
    invoke-virtual {p0}, Lilt;->aXm()Lils;

    move-result-object v0

    return-object v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 343
    iget v0, p0, Lilt;->dEf:I

    return v0
.end method

.method public final synthetic previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 279
    invoke-virtual {p0}, Lilt;->aXn()Lils;

    move-result-object v0

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 347
    iget v0, p0, Lilt;->dEf:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lilt;->dEb:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 320
    iget-object v0, p0, Lilt;->dEb:Lils;

    iget-object v1, p0, Lilt;->dEa:Lils;

    if-eq v0, v1, :cond_1

    .line 321
    iget-object v0, p0, Lilt;->dEb:Lils;

    iget-object v0, v0, Lils;->dEc:Lils;

    iput-object v0, p0, Lilt;->dEc:Lils;

    .line 322
    iget v0, p0, Lilt;->dEf:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lilt;->dEf:I

    .line 326
    :goto_1
    iget-object v0, p0, Lilt;->dDS:Lilf;

    iget-object v1, p0, Lilt;->dEb:Lils;

    invoke-static {v0, v1}, Lilf;->a(Lilf;Lils;)V

    .line 327
    const/4 v0, 0x0

    iput-object v0, p0, Lilt;->dEb:Lils;

    .line 328
    return-void

    .line 319
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 324
    :cond_1
    iget-object v0, p0, Lilt;->dEb:Lils;

    iget-object v0, v0, Lils;->dEa:Lils;

    iput-object v0, p0, Lilt;->dEa:Lils;

    goto :goto_1
.end method

.method public final synthetic set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 279
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
