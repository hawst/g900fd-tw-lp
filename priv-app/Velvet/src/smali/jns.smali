.class public final Ljns;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dMg:J

.field private dMh:J

.field private dzc:I

.field private evC:Ljava/lang/String;

.field private evD:Z

.field private evE:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1214
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1215
    iput v1, p0, Ljns;->aez:I

    iput-wide v2, p0, Ljns;->dMg:J

    iput-wide v2, p0, Ljns;->dMh:J

    iput v1, p0, Ljns;->dzc:I

    const-string v0, ""

    iput-object v0, p0, Ljns;->evC:Ljava/lang/String;

    iput-boolean v1, p0, Ljns;->evD:Z

    iput v1, p0, Ljns;->evE:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljns;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljns;->eCz:I

    .line 1216
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 1072
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljns;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljns;->dMg:J

    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljns;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljns;->dMh:J

    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljns;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljns;->dzc:I

    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljns;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljns;->evC:Ljava/lang/String;

    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljns;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljns;->evD:Z

    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljns;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljns;->evE:I

    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljns;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1234
    iget v0, p0, Ljns;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1235
    const/4 v0, 0x1

    iget-wide v2, p0, Ljns;->dMg:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1237
    :cond_0
    iget v0, p0, Ljns;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1238
    const/4 v0, 0x2

    iget-wide v2, p0, Ljns;->dMh:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1240
    :cond_1
    iget v0, p0, Ljns;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1241
    const/4 v0, 0x3

    iget v1, p0, Ljns;->dzc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1243
    :cond_2
    iget v0, p0, Ljns;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1244
    const/4 v0, 0x4

    iget-object v1, p0, Ljns;->evC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1246
    :cond_3
    iget v0, p0, Ljns;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 1247
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljns;->evD:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 1249
    :cond_4
    iget v0, p0, Ljns;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 1250
    const/4 v0, 0x6

    iget v1, p0, Ljns;->evE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1252
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1253
    return-void
.end method

.method public final aTg()I
    .locals 1

    .prologue
    .line 1138
    iget v0, p0, Ljns;->dzc:I

    return v0
.end method

.method public final agW()I
    .locals 1

    .prologue
    .line 1198
    iget v0, p0, Ljns;->evE:I

    return v0
.end method

.method public final bqV()J
    .locals 2

    .prologue
    .line 1100
    iget-wide v0, p0, Ljns;->dMg:J

    return-wide v0
.end method

.method public final bqW()J
    .locals 2

    .prologue
    .line 1119
    iget-wide v0, p0, Ljns;->dMh:J

    return-wide v0
.end method

.method public final bqX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1157
    iget-object v0, p0, Ljns;->evC:Ljava/lang/String;

    return-object v0
.end method

.method public final bqY()Z
    .locals 1

    .prologue
    .line 1179
    iget-boolean v0, p0, Ljns;->evD:Z

    return v0
.end method

.method public final dB(J)Ljns;
    .locals 1

    .prologue
    .line 1103
    iput-wide p1, p0, Ljns;->dMg:J

    .line 1104
    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljns;->aez:I

    .line 1105
    return-object p0
.end method

.method public final dC(J)Ljns;
    .locals 1

    .prologue
    .line 1122
    iput-wide p1, p0, Ljns;->dMh:J

    .line 1123
    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljns;->aez:I

    .line 1124
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1257
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1258
    iget v1, p0, Ljns;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1259
    const/4 v1, 0x1

    iget-wide v2, p0, Ljns;->dMg:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1262
    :cond_0
    iget v1, p0, Ljns;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1263
    const/4 v1, 0x2

    iget-wide v2, p0, Ljns;->dMh:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1266
    :cond_1
    iget v1, p0, Ljns;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1267
    const/4 v1, 0x3

    iget v2, p0, Ljns;->dzc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1270
    :cond_2
    iget v1, p0, Ljns;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1271
    const/4 v1, 0x4

    iget-object v2, p0, Ljns;->evC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1274
    :cond_3
    iget v1, p0, Ljns;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 1275
    const/4 v1, 0x5

    iget-boolean v2, p0, Ljns;->evD:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1278
    :cond_4
    iget v1, p0, Ljns;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 1279
    const/4 v1, 0x6

    iget v2, p0, Ljns;->evE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1282
    :cond_5
    return v0
.end method

.method public final qW(I)Ljns;
    .locals 1

    .prologue
    .line 1201
    iput p1, p0, Ljns;->evE:I

    .line 1202
    iget v0, p0, Ljns;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljns;->aez:I

    .line 1203
    return-object p0
.end method
