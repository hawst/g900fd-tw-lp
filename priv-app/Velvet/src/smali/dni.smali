.class public abstract Ldni;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Ldni;->mWebView:Landroid/webkit/WebView;

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 42
    iget-object v0, p0, Ldni;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v0, p2}, Ldni;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 43
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/webkit/WebView;Ljava/lang/String;)V
.end method

.method protected final kc(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ldni;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Ldni;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 57
    :cond_0
    return-void
.end method
