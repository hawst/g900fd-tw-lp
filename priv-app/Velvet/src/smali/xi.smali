.class public final Lxi;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private synthetic KX:Lcom/android/launcher3/Launcher;

.field private synthetic Lj:Landroid/view/View;

.field private synthetic Lk:Z

.field private synthetic Ll:Lcom/android/launcher3/AppsCustomizeTabHost;

.field private synthetic Lm:Landroid/view/View;

.field private synthetic Ln:Landroid/view/View;

.field private synthetic Lo:Lcom/android/launcher3/AppsCustomizePagedView;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Launcher;Landroid/view/View;ZLcom/android/launcher3/AppsCustomizeTabHost;Landroid/view/View;Landroid/view/View;Lcom/android/launcher3/AppsCustomizePagedView;)V
    .locals 0

    .prologue
    .line 3360
    iput-object p1, p0, Lxi;->KX:Lcom/android/launcher3/Launcher;

    iput-object p2, p0, Lxi;->Lj:Landroid/view/View;

    iput-boolean p3, p0, Lxi;->Lk:Z

    iput-object p4, p0, Lxi;->Ll:Lcom/android/launcher3/AppsCustomizeTabHost;

    iput-object p5, p0, Lxi;->Lm:Landroid/view/View;

    iput-object p6, p0, Lxi;->Ln:Landroid/view/View;

    iput-object p7, p0, Lxi;->Lo:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 3363
    iget-object v0, p0, Lxi;->KX:Lcom/android/launcher3/Launcher;

    iget-object v1, p0, Lxi;->Lj:Landroid/view/View;

    iget-boolean v2, p0, Lxi;->Lk:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher3/Launcher;->a(Lcom/android/launcher3/Launcher;Landroid/view/View;ZZ)V

    .line 3364
    iget-object v0, p0, Lxi;->KX:Lcom/android/launcher3/Launcher;

    iget-object v1, p0, Lxi;->Ll:Lcom/android/launcher3/AppsCustomizeTabHost;

    iget-boolean v2, p0, Lxi;->Lk:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher3/Launcher;->a(Lcom/android/launcher3/Launcher;Landroid/view/View;ZZ)V

    .line 3366
    iget-object v0, p0, Lxi;->Lm:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3367
    iget-object v0, p0, Lxi;->Lm:Landroid/view/View;

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3368
    iget-object v0, p0, Lxi;->Ln:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3369
    iget-object v0, p0, Lxi;->Ln:Landroid/view/View;

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 3371
    :cond_0
    iget-object v0, p0, Lxi;->Lo:Lcom/android/launcher3/AppsCustomizePagedView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher3/AppsCustomizePagedView;->z(Z)V

    .line 3374
    iget-object v0, p0, Lxi;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->f(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/SearchDropTargetBar;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3375
    iget-object v0, p0, Lxi;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->f(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/SearchDropTargetBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/launcher3/SearchDropTargetBar;->ao(Z)V

    .line 3377
    :cond_1
    return-void
.end method
