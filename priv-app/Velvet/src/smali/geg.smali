.class public final Lgeg;
.super Lgce;
.source "PG"


# instance fields
.field public cGj:Lgco;

.field private cHl:Z

.field private cHm:Z

.field private cHn:Lgej;


# direct methods
.method public constructor <init>(IIJZLgil;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Lemp;Lgdo;)V
    .locals 3

    .prologue
    .line 48
    invoke-direct/range {p0 .. p10}, Lgce;-><init>(IIJZLgil;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ScheduledExecutorService;Lemp;Lgdo;)V

    .line 51
    const/4 v0, 0x2

    iput v0, p0, Lgeg;->cFz:I

    .line 52
    iget-object v0, p0, Lgeg;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lgeg;->cFA:J

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Lgco;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lgeg;->cGj:Lgco;

    .line 58
    return-void
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lgeg;->cHn:Lgej;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lgeg;->cHn:Lgej;

    invoke-virtual {v0}, Lgej;->invalidate()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lgeg;->cHn:Lgej;

    .line 66
    :cond_0
    invoke-super {p0}, Lgce;->invalidate()V

    .line 67
    return-void
.end method

.method public final jY(I)I
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x1

    .line 73
    invoke-virtual {p0, p1}, Lgeg;->kc(I)V

    .line 75
    iget-boolean v0, p0, Lgeg;->cHl:Z

    if-nez v0, :cond_0

    .line 76
    iput-boolean v8, p0, Lgeg;->cHl:Z

    .line 77
    new-instance v3, Lgeh;

    const-string v0, "Timed out waiting for server endpoint"

    new-array v1, v6, [I

    invoke-direct {v3, p0, v0, v1}, Lgeh;-><init>(Lgeg;Ljava/lang/String;[I)V

    new-instance v0, Lgej;

    const-string v2, "Endpointer timeout"

    iget-wide v4, p0, Lgeg;->cFy:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lgej;-><init>(Lgeg;Ljava/lang/String;Lepm;J)V

    iput-object v0, p0, Lgeg;->cHn:Lgej;

    iget-object v0, p0, Lgeg;->cHn:Lgej;

    invoke-virtual {v0}, Lgej;->run()V

    .line 80
    :cond_0
    iget v0, p0, Lgeg;->cFz:I

    if-ne p1, v0, :cond_1

    move v0, v6

    .line 91
    :goto_0
    return v0

    .line 83
    :cond_1
    if-ne p1, v7, :cond_2

    move v0, v7

    .line 85
    goto :goto_0

    .line 86
    :cond_2
    if-ne p1, v8, :cond_3

    move v0, v8

    .line 88
    goto :goto_0

    :cond_3
    move v0, v7

    .line 91
    goto :goto_0
.end method

.method public final jZ(I)I
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Lgeg;->kc(I)V

    .line 143
    iget-boolean v0, p0, Lgeg;->bqM:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lgeg;->cFz:I

    if-ne v0, p1, :cond_1

    .line 144
    :cond_0
    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final onEndOfSpeech()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 111
    iget-boolean v0, p0, Lgeg;->cHm:Z

    if-nez v0, :cond_2

    .line 112
    iput-boolean v1, p0, Lgeg;->cHm:Z

    .line 113
    iget-object v0, p0, Lgeg;->cHn:Lgej;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lgeg;->cHn:Lgej;

    invoke-virtual {v0}, Lgej;->invalidate()V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lgeg;->cHn:Lgej;

    .line 117
    :cond_0
    iget-object v0, p0, Lgeg;->cFB:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lgeg;->cFB:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 120
    :cond_1
    new-instance v3, Lgei;

    const-string v0, "Timed out waiting for server result"

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {v3, p0, v0, v1}, Lgei;-><init>(Lgeg;Ljava/lang/String;[I)V

    new-instance v0, Lgej;

    const-string v2, "Result timeout"

    iget-wide v4, p0, Lgeg;->cFu:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lgej;-><init>(Lgeg;Ljava/lang/String;Lepm;J)V

    iput-object v0, p0, Lgeg;->cHn:Lgej;

    iget-object v0, p0, Lgeg;->cHn:Lgej;

    invoke-virtual {v0}, Lgej;->run()V

    .line 122
    :cond_2
    return-void
.end method
