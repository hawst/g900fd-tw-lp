.class public final Lere;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lemp;


# instance fields
.field private chV:Leoy;

.field private chW:Leoy;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lere;->mContext:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lemq;)V
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lere;->chV:Leoy;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Leoy;

    iget-object v1, p0, Lere;->mContext:Landroid/content/Context;

    const-string v2, "android.intent.action.TIME_SET"

    new-instance v3, Lerf;

    invoke-direct {v3, p0}, Lerf;-><init>(Lere;)V

    invoke-direct {v0, v1, v2, v3}, Leoy;-><init>(Landroid/content/Context;Ljava/lang/String;Leoz;)V

    iput-object v0, p0, Lere;->chV:Leoy;

    .line 52
    :cond_0
    iget-object v0, p0, Lere;->chV:Leoy;

    invoke-virtual {v0, p1}, Leoy;->aK(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public final a(Lemr;)V
    .locals 4

    .prologue
    .line 67
    iget-object v0, p0, Lere;->chW:Leoy;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Leoy;

    iget-object v1, p0, Lere;->mContext:Landroid/content/Context;

    const-string v2, "android.intent.action.TIME_TICK"

    new-instance v3, Lerg;

    invoke-direct {v3, p0}, Lerg;-><init>(Lere;)V

    invoke-direct {v0, v1, v2, v3}, Leoy;-><init>(Landroid/content/Context;Ljava/lang/String;Leoz;)V

    iput-object v0, p0, Lere;->chW:Leoy;

    .line 77
    :cond_0
    iget-object v0, p0, Lere;->chW:Leoy;

    invoke-virtual {v0, p1}, Leoy;->aK(Ljava/lang/Object;)V

    .line 78
    return-void
.end method

.method public final b(Lemq;)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lere;->chV:Leoy;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lere;->chV:Leoy;

    invoke-virtual {v0, p1}, Leoy;->aL(Ljava/lang/Object;)V

    .line 59
    iget-object v0, p0, Lere;->chV:Leoy;

    invoke-virtual {v0}, Leoy;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lere;->chV:Leoy;

    .line 63
    :cond_0
    return-void
.end method

.method public final b(Lemr;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lere;->chW:Leoy;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lere;->chW:Leoy;

    invoke-virtual {v0, p1}, Leoy;->aL(Ljava/lang/Object;)V

    .line 84
    iget-object v0, p0, Lere;->chW:Leoy;

    invoke-virtual {v0}, Leoy;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lere;->chW:Leoy;

    .line 88
    :cond_0
    return-void
.end method

.method public final currentTimeMillis()J
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public final elapsedRealtime()J
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public final uptimeMillis()J
    .locals 2

    .prologue
    .line 37
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method
