.class public final Lbqt;
.super Ljava/lang/Object;


# instance fields
.field final aHj:I

.field private final buffer:[B

.field position:I


# direct methods
.method private constructor <init>([BII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbqt;->buffer:[B

    iput p2, p0, Lbqt;->position:I

    add-int v0, p2, p3

    iput v0, p0, Lbqt;->aHj:I

    return-void
.end method

.method public static c([BII)Lbqt;
    .locals 1

    new-instance v0, Lbqt;

    invoke-direct {v0, p0, p1, p2}, Lbqt;-><init>([BII)V

    return-object v0
.end method

.method private dW(I)V
    .locals 4

    int-to-byte v0, p1

    iget v1, p0, Lbqt;->position:I

    iget v2, p0, Lbqt;->aHj:I

    if-ne v1, v2, :cond_0

    new-instance v0, Lbqu;

    iget v1, p0, Lbqt;->position:I

    iget v2, p0, Lbqt;->aHj:I

    invoke-direct {v0, v1, v2}, Lbqu;-><init>(II)V

    throw v0

    :cond_0
    iget-object v1, p0, Lbqt;->buffer:[B

    iget v2, p0, Lbqt;->position:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lbqt;->position:I

    aput-byte v0, v1, v2

    return-void
.end method

.method public static dY(I)I
    .locals 1

    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method


# virtual methods
.method public final dX(I)V
    .locals 1

    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lbqt;->dW(I)V

    return-void

    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Lbqt;->dW(I)V

    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method

.method public final h([B)V
    .locals 4

    array-length v0, p1

    iget v1, p0, Lbqt;->aHj:I

    iget v2, p0, Lbqt;->position:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Lbqt;->buffer:[B

    iget v3, p0, Lbqt;->position:I

    invoke-static {p1, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lbqt;->position:I

    add-int/2addr v0, v1

    iput v0, p0, Lbqt;->position:I

    return-void

    :cond_0
    new-instance v0, Lbqu;

    iget v1, p0, Lbqt;->position:I

    iget v2, p0, Lbqt;->aHj:I

    invoke-direct {v0, v1, v2}, Lbqu;-><init>(II)V

    throw v0
.end method
