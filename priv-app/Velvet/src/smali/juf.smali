.class public final Ljuf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eEK:[Ljuf;


# instance fields
.field private aez:I

.field private eEL:F

.field private eEM:Z

.field private eEN:F

.field private eEO:F

.field private eEP:F

.field private eEQ:F

.field private eER:F

.field private eES:F

.field private eET:F

.field private eEU:F

.field private eEV:F

.field private eEW:F

.field private eEX:F

.field private eEY:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 293
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 294
    iput v1, p0, Ljuf;->aez:I

    iput v0, p0, Ljuf;->eEL:F

    iput-boolean v1, p0, Ljuf;->eEM:Z

    iput v0, p0, Ljuf;->eEN:F

    iput v0, p0, Ljuf;->eEO:F

    iput v0, p0, Ljuf;->eEP:F

    iput v0, p0, Ljuf;->eEQ:F

    iput v0, p0, Ljuf;->eER:F

    iput v0, p0, Ljuf;->eES:F

    iput v0, p0, Ljuf;->eET:F

    iput v0, p0, Ljuf;->eEU:F

    iput v0, p0, Ljuf;->eEV:F

    iput v0, p0, Ljuf;->eEW:F

    iput v0, p0, Ljuf;->eEX:F

    iput v0, p0, Ljuf;->eEY:F

    const/4 v0, 0x0

    iput-object v0, p0, Ljuf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuf;->eCz:I

    .line 295
    return-void
.end method

.method public static buF()[Ljuf;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljuf;->eEK:[Ljuf;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljuf;->eEK:[Ljuf;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljuf;

    sput-object v0, Ljuf;->eEK:[Ljuf;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljuf;->eEK:[Ljuf;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEL:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljuf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEN:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljuf;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEO:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljuf;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEP:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljuf;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEQ:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljuf;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eER:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljuf;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eES:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Ljuf;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eET:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Ljuf;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEU:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Ljuf;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEV:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Ljuf;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEW:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Ljuf;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEX:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Ljuf;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuf;->eEY:F

    iget v0, p0, Ljuf;->aez:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Ljuf;->aez:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljuf;->eEM:Z

    iget v0, p0, Ljuf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljuf;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x4d -> :sswitch_9
        0x55 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6d -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 321
    iget v0, p0, Ljuf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 322
    const/4 v0, 0x1

    iget v1, p0, Ljuf;->eEL:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 324
    :cond_0
    iget v0, p0, Ljuf;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 325
    const/4 v0, 0x2

    iget v1, p0, Ljuf;->eEN:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 327
    :cond_1
    iget v0, p0, Ljuf;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    .line 328
    const/4 v0, 0x3

    iget v1, p0, Ljuf;->eEO:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 330
    :cond_2
    iget v0, p0, Ljuf;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    .line 331
    const/4 v0, 0x4

    iget v1, p0, Ljuf;->eEP:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 333
    :cond_3
    iget v0, p0, Ljuf;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_4

    .line 334
    const/4 v0, 0x5

    iget v1, p0, Ljuf;->eEQ:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 336
    :cond_4
    iget v0, p0, Ljuf;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    .line 337
    const/4 v0, 0x6

    iget v1, p0, Ljuf;->eER:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 339
    :cond_5
    iget v0, p0, Ljuf;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    .line 340
    const/4 v0, 0x7

    iget v1, p0, Ljuf;->eES:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 342
    :cond_6
    iget v0, p0, Ljuf;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_7

    .line 343
    const/16 v0, 0x8

    iget v1, p0, Ljuf;->eET:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 345
    :cond_7
    iget v0, p0, Ljuf;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_8

    .line 346
    const/16 v0, 0x9

    iget v1, p0, Ljuf;->eEU:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 348
    :cond_8
    iget v0, p0, Ljuf;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_9

    .line 349
    const/16 v0, 0xa

    iget v1, p0, Ljuf;->eEV:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 351
    :cond_9
    iget v0, p0, Ljuf;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_a

    .line 352
    const/16 v0, 0xb

    iget v1, p0, Ljuf;->eEW:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 354
    :cond_a
    iget v0, p0, Ljuf;->aez:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_b

    .line 355
    const/16 v0, 0xc

    iget v1, p0, Ljuf;->eEX:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 357
    :cond_b
    iget v0, p0, Ljuf;->aez:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_c

    .line 358
    const/16 v0, 0xd

    iget v1, p0, Ljuf;->eEY:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 360
    :cond_c
    iget v0, p0, Ljuf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d

    .line 361
    const/16 v0, 0xe

    iget-boolean v1, p0, Ljuf;->eEM:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 363
    :cond_d
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 364
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 368
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 369
    iget v1, p0, Ljuf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 370
    const/4 v1, 0x1

    iget v2, p0, Ljuf;->eEL:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 373
    :cond_0
    iget v1, p0, Ljuf;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_1

    .line 374
    const/4 v1, 0x2

    iget v2, p0, Ljuf;->eEN:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 377
    :cond_1
    iget v1, p0, Ljuf;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 378
    const/4 v1, 0x3

    iget v2, p0, Ljuf;->eEO:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 381
    :cond_2
    iget v1, p0, Ljuf;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_3

    .line 382
    const/4 v1, 0x4

    iget v2, p0, Ljuf;->eEP:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 385
    :cond_3
    iget v1, p0, Ljuf;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_4

    .line 386
    const/4 v1, 0x5

    iget v2, p0, Ljuf;->eEQ:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 389
    :cond_4
    iget v1, p0, Ljuf;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 390
    const/4 v1, 0x6

    iget v2, p0, Ljuf;->eER:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 393
    :cond_5
    iget v1, p0, Ljuf;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_6

    .line 394
    const/4 v1, 0x7

    iget v2, p0, Ljuf;->eES:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 397
    :cond_6
    iget v1, p0, Ljuf;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_7

    .line 398
    const/16 v1, 0x8

    iget v2, p0, Ljuf;->eET:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 401
    :cond_7
    iget v1, p0, Ljuf;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_8

    .line 402
    const/16 v1, 0x9

    iget v2, p0, Ljuf;->eEU:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 405
    :cond_8
    iget v1, p0, Ljuf;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_9

    .line 406
    const/16 v1, 0xa

    iget v2, p0, Ljuf;->eEV:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 409
    :cond_9
    iget v1, p0, Ljuf;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_a

    .line 410
    const/16 v1, 0xb

    iget v2, p0, Ljuf;->eEW:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 413
    :cond_a
    iget v1, p0, Ljuf;->aez:I

    and-int/lit16 v1, v1, 0x1000

    if-eqz v1, :cond_b

    .line 414
    const/16 v1, 0xc

    iget v2, p0, Ljuf;->eEX:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 417
    :cond_b
    iget v1, p0, Ljuf;->aez:I

    and-int/lit16 v1, v1, 0x2000

    if-eqz v1, :cond_c

    .line 418
    const/16 v1, 0xd

    iget v2, p0, Ljuf;->eEY:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 421
    :cond_c
    iget v1, p0, Ljuf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_d

    .line 422
    const/16 v1, 0xe

    iget-boolean v2, p0, Ljuf;->eEM:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 425
    :cond_d
    return v0
.end method
