.class public final Ljix;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dHR:I

.field private eoc:I

.field private eod:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 108
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Ljix;->aez:I

    iput v1, p0, Ljix;->eoc:I

    iput v1, p0, Ljix;->eod:I

    iput v1, p0, Ljix;->dHR:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljix;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljix;->eCz:I

    .line 110
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljix;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljix;->eoc:I

    iget v0, p0, Ljix;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljix;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljix;->eod:I

    iget v0, p0, Ljix;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljix;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Ljix;->dHR:I

    iget v0, p0, Ljix;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljix;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 125
    iget v0, p0, Ljix;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x1

    iget v1, p0, Ljix;->eoc:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 128
    :cond_0
    iget v0, p0, Ljix;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 129
    const/4 v0, 0x2

    iget v1, p0, Ljix;->eod:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 131
    :cond_1
    iget v0, p0, Ljix;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 132
    const/4 v0, 0x3

    iget v1, p0, Ljix;->dHR:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 134
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 135
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 139
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 140
    iget v1, p0, Ljix;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 141
    const/4 v1, 0x1

    iget v2, p0, Ljix;->eoc:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_0
    iget v1, p0, Ljix;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 145
    const/4 v1, 0x2

    iget v2, p0, Ljix;->eod:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_1
    iget v1, p0, Ljix;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 149
    const/4 v1, 0x3

    iget v2, p0, Ljix;->dHR:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_2
    return v0
.end method

.method public final qi(I)Ljix;
    .locals 1

    .prologue
    .line 57
    iput p1, p0, Ljix;->eoc:I

    .line 58
    iget v0, p0, Ljix;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljix;->aez:I

    .line 59
    return-object p0
.end method

.method public final qj(I)Ljix;
    .locals 1

    .prologue
    .line 76
    iput p1, p0, Ljix;->eod:I

    .line 77
    iget v0, p0, Ljix;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljix;->aez:I

    .line 78
    return-object p0
.end method

.method public final qk(I)Ljix;
    .locals 1

    .prologue
    .line 95
    iput p1, p0, Ljix;->dHR:I

    .line 96
    iget v0, p0, Ljix;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljix;->aez:I

    .line 97
    return-object p0
.end method
