.class public final Lcxi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final KH:Landroid/content/SharedPreferences;

.field public bmm:Ljel;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final bmn:Ljava/lang/String;

.field final dK:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcxi;->dK:Ljava/lang/Object;

    .line 71
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iput-object p1, p0, Lcxi;->KH:Landroid/content/SharedPreferences;

    .line 73
    iput-object p2, p0, Lcxi;->bmn:Ljava/lang/String;

    .line 74
    return-void
.end method

.method private d(ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 429
    iget-object v1, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 430
    :try_start_0
    invoke-virtual {p0}, Lcxi;->Ty()Ljel;

    move-result-object v0

    .line 431
    if-nez v0, :cond_0

    .line 436
    const-string v0, "NowConfigurationPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attempt to write "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " without backing configuration"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    monitor-exit v1

    .line 441
    :goto_0
    return-void

    .line 439
    :cond_0
    invoke-static {v0, p1, p2}, Lfgz;->b(Ljel;ILjava/lang/Object;)V

    .line 440
    invoke-virtual {p0}, Lcxi;->Tz()V

    .line 441
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final TA()I
    .locals 2

    .prologue
    .line 247
    const/16 v0, 0x9

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final TB()I
    .locals 2

    .prologue
    .line 280
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final TC()Z
    .locals 2

    .prologue
    .line 357
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcxi;->c(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final Ty()Ljel;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 82
    iget-object v1, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-object v2, p0, Lcxi;->bmm:Ljel;

    if-eqz v2, :cond_0

    .line 84
    iget-object v0, p0, Lcxi;->bmm:Ljel;

    monitor-exit v1

    .line 99
    :goto_0
    return-object v0

    .line 87
    :cond_0
    iget-object v2, p0, Lcxi;->KH:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcxi;->bmn:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 88
    if-nez v2, :cond_1

    .line 89
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 92
    :cond_1
    :try_start_1
    new-instance v0, Ljel;

    invoke-direct {v0}, Ljel;-><init>()V

    iput-object v0, p0, Lcxi;->bmm:Ljel;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    :try_start_2
    iget-object v0, p0, Lcxi;->bmm:Ljel;

    invoke-static {v0, v2}, Leqh;->a(Ljsr;Ljava/lang/String;)Ljsr;
    :try_end_2
    .catch Ljsq; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 99
    :goto_1
    :try_start_3
    iget-object v0, p0, Lcxi;->bmm:Ljel;

    monitor-exit v1

    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 97
    invoke-static {v0}, Ligm;->g(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final Tz()V
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lcxi;->KH:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcxi;->bmm:Ljel;

    invoke-static {v1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "NowConfigurationPreferences.dirty"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcxi;->bmm:Ljel;

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcxi;->bmn:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 160
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 161
    return-void
.end method

.method public final ah(J)Ljfo;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 201
    iget-object v2, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 202
    :try_start_0
    invoke-virtual {p0}, Lcxi;->Ty()Ljel;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_1

    iget-object v1, v0, Ljel;->efF:Ljfn;

    if-eqz v1, :cond_1

    .line 205
    iget-object v0, v0, Ljel;->efF:Ljfn;

    iget-object v3, v0, Ljfn;->eiD:[Ljfo;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 206
    invoke-virtual {v0}, Ljfo;->bjE()J

    move-result-wide v6

    cmp-long v5, p1, v6

    if-nez v5, :cond_0

    .line 207
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    :goto_1
    return-object v0

    .line 205
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 211
    :cond_1
    const/4 v0, 0x0

    monitor-exit v2

    goto :goto_1

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(JZ)V
    .locals 5

    .prologue
    .line 182
    iget-object v1, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 183
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcxi;->ah(J)Ljfo;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {v0, p3}, Ljfo;->if(Z)Ljfo;

    .line 187
    invoke-virtual {p0}, Lcxi;->Tz()V

    .line 192
    :goto_0
    monitor-exit v1

    return-void

    .line 189
    :cond_0
    const-string v0, "NowConfigurationPreferences"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Location sharer to update was not found in settings: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljel;)V
    .locals 4

    .prologue
    .line 113
    iget-object v1, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    :try_start_0
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 118
    :try_start_1
    new-instance v0, Ljel;

    invoke-direct {v0}, Ljel;-><init>()V

    invoke-static {v0, v2}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljel;

    iput-object v0, p0, Lcxi;->bmm:Ljel;
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 123
    const/4 v0, 0x3

    :try_start_2
    invoke-static {v2, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 125
    iget-object v2, p0, Lcxi;->KH:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget-object v3, p0, Lcxi;->bmn:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "NowConfigurationPreferences.dirty"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 129
    monitor-exit v1

    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 121
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public c(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 413
    iget-object v1, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 414
    :try_start_0
    invoke-virtual {p0}, Lcxi;->Ty()Ljel;

    move-result-object v0

    .line 415
    if-nez v0, :cond_0

    .line 416
    monitor-exit v1

    .line 418
    :goto_0
    return-object p2

    :cond_0
    invoke-static {v0, p1, p2}, Lfgz;->a(Ljel;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Ljel;)Ljga;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 473
    iget-object v1, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 474
    :try_start_0
    invoke-virtual {p0}, Lcxi;->Ty()Ljel;

    move-result-object v0

    .line 475
    if-nez v0, :cond_0

    .line 476
    const/4 v0, 0x0

    monitor-exit v1

    .line 478
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, v0}, Lfha;->a(Ljel;Ljel;)Ljga;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 479
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final cL(Z)V
    .locals 2

    .prologue
    .line 394
    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcxi;->d(ILjava/lang/Object;)V

    .line 395
    return-void
.end method

.method public final cM(Z)V
    .locals 2

    .prologue
    .line 401
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcxi;->d(ILjava/lang/Object;)V

    .line 402
    return-void
.end method

.method public final fq(I)V
    .locals 2

    .prologue
    .line 238
    const/16 v0, 0x9

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcxi;->d(ILjava/lang/Object;)V

    .line 239
    return-void
.end method

.method public final fr(I)V
    .locals 2

    .prologue
    .line 292
    const/4 v0, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcxi;->d(ILjava/lang/Object;)V

    .line 293
    return-void
.end method

.method public fs(I)Lijj;
    .locals 2

    .prologue
    .line 453
    iget-object v1, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 454
    :try_start_0
    invoke-virtual {p0}, Lcxi;->Ty()Ljel;

    move-result-object v0

    .line 455
    if-nez v0, :cond_0

    .line 456
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    monitor-exit v1

    .line 458
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p1}, Lfgz;->b(Ljel;I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 460
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isDirty()Z
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcxi;->KH:Landroid/content/SharedPreferences;

    const-string v1, "NowConfigurationPreferences.dirty"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final s(IZ)V
    .locals 6

    .prologue
    .line 336
    iget-object v1, p0, Lcxi;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 337
    :try_start_0
    invoke-virtual {p0}, Lcxi;->Ty()Ljel;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_0

    iget-object v2, v0, Ljel;->efG:Ljer;

    if-nez v2, :cond_1

    .line 339
    :cond_0
    monitor-exit v1

    .line 350
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-object v0, v0, Ljel;->efG:Ljer;

    iget-object v2, v0, Ljer;->efW:[Ljes;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 344
    invoke-virtual {v4}, Ljes;->bcO()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 345
    invoke-virtual {v4, p2}, Ljes;->hU(Z)Ljes;

    .line 346
    invoke-virtual {p0}, Lcxi;->Tz()V

    .line 347
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 350
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 343
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 350
    :cond_3
    monitor-exit v1

    goto :goto_0
.end method
