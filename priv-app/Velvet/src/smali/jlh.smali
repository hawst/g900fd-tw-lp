.class public final Ljlh;
.super Ljsl;
.source "PG"


# static fields
.field public static final ers:Ljsm;


# instance fields
.field private aez:I

.field private dYd:Ljava/lang/String;

.field private epX:Ljava/lang/String;

.field public ert:[Ljoq;

.field private eru:Ljsc;

.field private erv:Ljsc;

.field private erw:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 4520
    const/16 v0, 0xb

    const-class v1, Ljlh;

    const v2, 0xc7cc19a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljlh;->ers:Ljsm;

    .line 4532
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 4611
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 4612
    iput v2, p0, Ljlh;->aez:I

    invoke-static {}, Ljoq;->brt()[Ljoq;

    move-result-object v0

    iput-object v0, p0, Ljlh;->ert:[Ljoq;

    const-string v0, ""

    iput-object v0, p0, Ljlh;->dYd:Ljava/lang/String;

    iput-object v1, p0, Ljlh;->eru:Ljsc;

    const-string v0, ""

    iput-object v0, p0, Ljlh;->epX:Ljava/lang/String;

    iput-object v1, p0, Ljlh;->erv:Ljsc;

    iput v2, p0, Ljlh;->erw:I

    iput-object v1, p0, Ljlh;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlh;->eCz:I

    .line 4613
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4513
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlh;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlh;->dYd:Ljava/lang/String;

    iget v0, p0, Ljlh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlh;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlh;->epX:Ljava/lang/String;

    iget v0, p0, Ljlh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlh;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljlh;->eru:Ljsc;

    if-nez v0, :cond_1

    new-instance v0, Ljsc;

    invoke-direct {v0}, Ljsc;-><init>()V

    iput-object v0, p0, Ljlh;->eru:Ljsc;

    :cond_1
    iget-object v0, p0, Ljlh;->eru:Ljsc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljlh;->erv:Ljsc;

    if-nez v0, :cond_2

    new-instance v0, Ljsc;

    invoke-direct {v0}, Ljsc;-><init>()V

    iput-object v0, p0, Ljlh;->erv:Ljsc;

    :cond_2
    iget-object v0, p0, Ljlh;->erv:Ljsc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x42

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljlh;->ert:[Ljoq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljoq;

    if-eqz v0, :cond_3

    iget-object v3, p0, Ljlh;->ert:[Ljoq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Ljoq;

    invoke-direct {v3}, Ljoq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ljlh;->ert:[Ljoq;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Ljoq;

    invoke-direct {v3}, Ljoq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljlh;->ert:[Ljoq;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljlh;->erw:I

    iget v0, p0, Ljlh;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljlh;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x2a -> :sswitch_2
        0x32 -> :sswitch_3
        0x3a -> :sswitch_4
        0x42 -> :sswitch_5
        0x48 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 4631
    iget v0, p0, Ljlh;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 4632
    const/4 v0, 0x4

    iget-object v1, p0, Ljlh;->dYd:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4634
    :cond_0
    iget v0, p0, Ljlh;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 4635
    const/4 v0, 0x5

    iget-object v1, p0, Ljlh;->epX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 4637
    :cond_1
    iget-object v0, p0, Ljlh;->eru:Ljsc;

    if-eqz v0, :cond_2

    .line 4638
    const/4 v0, 0x6

    iget-object v1, p0, Ljlh;->eru:Ljsc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4640
    :cond_2
    iget-object v0, p0, Ljlh;->erv:Ljsc;

    if-eqz v0, :cond_3

    .line 4641
    const/4 v0, 0x7

    iget-object v1, p0, Ljlh;->erv:Ljsc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 4643
    :cond_3
    iget-object v0, p0, Ljlh;->ert:[Ljoq;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljlh;->ert:[Ljoq;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 4644
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljlh;->ert:[Ljoq;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 4645
    iget-object v1, p0, Ljlh;->ert:[Ljoq;

    aget-object v1, v1, v0

    .line 4646
    if-eqz v1, :cond_4

    .line 4647
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 4644
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4651
    :cond_5
    iget v0, p0, Ljlh;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 4652
    const/16 v0, 0x9

    iget v1, p0, Ljlh;->erw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 4654
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 4655
    return-void
.end method

.method public final getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4570
    iget-object v0, p0, Ljlh;->epX:Ljava/lang/String;

    return-object v0
.end method

.method public final getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4545
    iget-object v0, p0, Ljlh;->dYd:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 4659
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 4660
    iget v1, p0, Ljlh;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 4661
    const/4 v1, 0x4

    iget-object v2, p0, Ljlh;->dYd:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4664
    :cond_0
    iget v1, p0, Ljlh;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 4665
    const/4 v1, 0x5

    iget-object v2, p0, Ljlh;->epX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4668
    :cond_1
    iget-object v1, p0, Ljlh;->eru:Ljsc;

    if-eqz v1, :cond_2

    .line 4669
    const/4 v1, 0x6

    iget-object v2, p0, Ljlh;->eru:Ljsc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4672
    :cond_2
    iget-object v1, p0, Ljlh;->erv:Ljsc;

    if-eqz v1, :cond_3

    .line 4673
    const/4 v1, 0x7

    iget-object v2, p0, Ljlh;->erv:Ljsc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4676
    :cond_3
    iget-object v1, p0, Ljlh;->ert:[Ljoq;

    if-eqz v1, :cond_6

    iget-object v1, p0, Ljlh;->ert:[Ljoq;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 4677
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljlh;->ert:[Ljoq;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 4678
    iget-object v2, p0, Ljlh;->ert:[Ljoq;

    aget-object v2, v2, v0

    .line 4679
    if-eqz v2, :cond_4

    .line 4680
    const/16 v3, 0x8

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4677
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 4685
    :cond_6
    iget v1, p0, Ljlh;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 4686
    const/16 v1, 0x9

    iget v2, p0, Ljlh;->erw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4689
    :cond_7
    return v0
.end method

.method public final wE(Ljava/lang/String;)Ljlh;
    .locals 1

    .prologue
    .line 4548
    if-nez p1, :cond_0

    .line 4549
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4551
    :cond_0
    iput-object p1, p0, Ljlh;->dYd:Ljava/lang/String;

    .line 4552
    iget v0, p0, Ljlh;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlh;->aez:I

    .line 4553
    return-object p0
.end method

.method public final wF(Ljava/lang/String;)Ljlh;
    .locals 1

    .prologue
    .line 4573
    if-nez p1, :cond_0

    .line 4574
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4576
    :cond_0
    iput-object p1, p0, Ljlh;->epX:Ljava/lang/String;

    .line 4577
    iget v0, p0, Ljlh;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlh;->aez:I

    .line 4578
    return-object p0
.end method
