.class public final Lfit;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public final ctd:Ljava/lang/String;

.field public final cte:Ljbp;

.field public final ctf:I

.field public final ctg:Z

.field private cth:F


# direct methods
.method public constructor <init>(Ljie;Ljbp;IF)V
    .locals 2

    .prologue
    .line 490
    iget-object v0, p1, Ljie;->end:[I

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lius;->d([II)Z

    move-result v0

    invoke-direct {p0, v0, p2, p3, p4}, Lfit;-><init>(ZLjbp;IF)V

    .line 492
    return-void
.end method

.method public constructor <init>(ZLjbp;IF)V
    .locals 1

    .prologue
    .line 496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 497
    invoke-static {p2}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->b(Ljbp;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfit;->ctd:Ljava/lang/String;

    .line 498
    iput-object p2, p0, Lfit;->cte:Ljbp;

    .line 499
    iput p3, p0, Lfit;->ctf:I

    .line 500
    iput-boolean p1, p0, Lfit;->ctg:Z

    .line 501
    iput p4, p0, Lfit;->cth:F

    .line 502
    return-void
.end method


# virtual methods
.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 480
    check-cast p1, Lfit;

    iget v0, p0, Lfit;->cth:F

    iget v1, p1, Lfit;->cth:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    return v0
.end method
