.class public final Laoe;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agv:I

.field private ajk:Ljava/lang/String;

.field private ajl:Z

.field private ajm:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12749
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 12750
    iput v1, p0, Laoe;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Laoe;->agv:I

    const-string v0, ""

    iput-object v0, p0, Laoe;->ajk:Ljava/lang/String;

    iput-boolean v1, p0, Laoe;->ajl:Z

    const-string v0, ""

    iput-object v0, p0, Laoe;->ajm:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Laoe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laoe;->eCz:I

    .line 12751
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 12644
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laoe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoe;->ajk:Ljava/lang/String;

    iget v0, p0, Laoe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laoe;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Laoe;->agv:I

    iget v0, p0, Laoe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laoe;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Laoe;->ajl:Z

    iget v0, p0, Laoe;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laoe;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoe;->ajm:Ljava/lang/String;

    iget v0, p0, Laoe;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laoe;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 12767
    iget v0, p0, Laoe;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 12768
    const/4 v0, 0x1

    iget-object v1, p0, Laoe;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 12770
    :cond_0
    iget v0, p0, Laoe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 12771
    const/4 v0, 0x2

    iget v1, p0, Laoe;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 12773
    :cond_1
    iget v0, p0, Laoe;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 12774
    const/4 v0, 0x3

    iget-boolean v1, p0, Laoe;->ajl:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 12776
    :cond_2
    iget v0, p0, Laoe;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 12777
    const/4 v0, 0x4

    iget-object v1, p0, Laoe;->ajm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 12779
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 12780
    return-void
.end method

.method public final bG(Ljava/lang/String;)Laoe;
    .locals 1

    .prologue
    .line 12692
    if-nez p1, :cond_0

    .line 12693
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12695
    :cond_0
    iput-object p1, p0, Laoe;->ajk:Ljava/lang/String;

    .line 12696
    iget v0, p0, Laoe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laoe;->aez:I

    .line 12697
    return-object p0
.end method

.method public final bH(Ljava/lang/String;)Laoe;
    .locals 1

    .prologue
    .line 12733
    if-nez p1, :cond_0

    .line 12734
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 12736
    :cond_0
    iput-object p1, p0, Laoe;->ajm:Ljava/lang/String;

    .line 12737
    iget v0, p0, Laoe;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laoe;->aez:I

    .line 12738
    return-object p0
.end method

.method public final bb(Z)Laoe;
    .locals 1

    .prologue
    .line 12714
    iput-boolean p1, p0, Laoe;->ajl:Z

    .line 12715
    iget v0, p0, Laoe;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laoe;->aez:I

    .line 12716
    return-object p0
.end method

.method public final cB(I)Laoe;
    .locals 1

    .prologue
    .line 12673
    iput p1, p0, Laoe;->agv:I

    .line 12674
    iget v0, p0, Laoe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laoe;->aez:I

    .line 12675
    return-object p0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12730
    iget-object v0, p0, Laoe;->ajm:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 12670
    iget v0, p0, Laoe;->agv:I

    return v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12689
    iget-object v0, p0, Laoe;->ajk:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 12784
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 12785
    iget v1, p0, Laoe;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 12786
    const/4 v1, 0x1

    iget-object v2, p0, Laoe;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12789
    :cond_0
    iget v1, p0, Laoe;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 12790
    const/4 v1, 0x2

    iget v2, p0, Laoe;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12793
    :cond_1
    iget v1, p0, Laoe;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 12794
    const/4 v1, 0x3

    iget-boolean v2, p0, Laoe;->ajl:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12797
    :cond_2
    iget v1, p0, Laoe;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 12798
    const/4 v1, 0x4

    iget-object v2, p0, Laoe;->ajm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12801
    :cond_3
    return v0
.end method

.method public final oP()Z
    .locals 1

    .prologue
    .line 12678
    iget v0, p0, Laoe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qA()Z
    .locals 1

    .prologue
    .line 12741
    iget v0, p0, Laoe;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qz()Z
    .locals 1

    .prologue
    .line 12711
    iget-boolean v0, p0, Laoe;->ajl:Z

    return v0
.end method
