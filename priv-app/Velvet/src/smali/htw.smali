.class final Lhtw;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lhtv;


# instance fields
.field private dqP:Lhuc;

.field dqV:Lhts;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-virtual {p0}, Lhtw;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040099

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f110056

    invoke-virtual {p0, v0}, Lhtw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lhtx;

    invoke-direct {v1, p0}, Lhtx;-><init>(Lhtw;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lhuc;

    invoke-direct {v0, p0}, Lhuc;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lhtw;->dqP:Lhuc;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Lhts;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lhtw;->dqV:Lhts;

    .line 129
    return-void
.end method

.method public final aQo()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lhtw;->dqP:Lhuc;

    invoke-virtual {v0}, Lhuc;->aQo()V

    .line 134
    return-void
.end method

.method public final aQp()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lhtw;->dqP:Lhuc;

    invoke-virtual {v0}, Lhuc;->aQp()V

    .line 139
    return-void
.end method

.method public final g(Ljava/util/List;Z)V
    .locals 9

    .prologue
    const v8, 0x7f110134

    const/4 v7, 0x0

    .line 70
    invoke-static {}, Lenu;->auR()V

    .line 72
    if-nez p2, :cond_1

    .line 74
    const v0, 0x7f1101f6

    invoke-virtual {p0, v0}, Lhtw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 75
    if-nez v0, :cond_0

    .line 76
    const v0, 0x7f1101a9

    invoke-virtual {p0, v0}, Lhtw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 78
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    :cond_1
    invoke-virtual {p0}, Lhtw;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 84
    const v1, 0x7f1101f7

    invoke-virtual {p0, v1}, Lhtw;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 86
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    :goto_0
    if-ltz v4, :cond_4

    .line 87
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/search/shared/contact/Contact;

    .line 89
    if-eqz p2, :cond_2

    const v3, 0x7f04009a

    :goto_1
    invoke-virtual {v0, v3, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    if-eqz p2, :cond_3

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    :goto_2
    const v3, 0x7f1101f9

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/voicesearch/handsfree/ui/QuotedTextView;

    add-int/lit8 v6, v4, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/voicesearch/handsfree/ui/QuotedTextView;->O(Ljava/lang/CharSequence;)V

    .line 93
    new-instance v3, Lhty;

    invoke-direct {v3, p0, v2}, Lhty;-><init>(Lhtw;Lcom/google/android/search/shared/contact/Contact;)V

    invoke-virtual {v5, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    invoke-virtual {v1, v5, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 86
    add-int/lit8 v2, v4, -0x1

    move v4, v2

    goto :goto_0

    .line 89
    :cond_2
    const v3, 0x7f04009b

    goto :goto_1

    :cond_3
    const v3, 0x7f110120

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/Contact;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/Contact;->getLabel()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 105
    :cond_4
    return-void
.end method

.method public final oa(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lhtw;->dqP:Lhuc;

    invoke-virtual {v0, p1}, Lhuc;->oa(Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 60
    iget-object v0, p0, Lhtw;->dqV:Lhts;

    invoke-virtual {v0}, Lhts;->azY()V

    .line 61
    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f1101a9

    invoke-virtual {p0, v0}, Lhtw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    return-void
.end method
