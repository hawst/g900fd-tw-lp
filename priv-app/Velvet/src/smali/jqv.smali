.class public final Ljqv;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private bmJ:Z

.field private bmL:I

.field private bmN:Ljava/lang/String;

.field public ezA:[Ljava/lang/String;

.field private ezB:D

.field public ezC:[Ljqu;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6884
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6885
    iput v1, p0, Ljqv;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqv;->bmN:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljqv;->ezA:[Ljava/lang/String;

    iput v1, p0, Ljqv;->bmL:I

    iput-boolean v1, p0, Ljqv;->bmJ:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljqv;->ezB:D

    invoke-static {}, Ljqu;->bsD()[Ljqu;

    move-result-object v0

    iput-object v0, p0, Ljqv;->ezC:[Ljqu;

    const/4 v0, 0x0

    iput-object v0, p0, Ljqv;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqv;->eCz:I

    .line 6886
    return-void
.end method


# virtual methods
.method public final TO()Z
    .locals 1

    .prologue
    .line 6846
    iget-boolean v0, p0, Ljqv;->bmJ:Z

    return v0
.end method

.method public final TP()Z
    .locals 1

    .prologue
    .line 6854
    iget v0, p0, Ljqv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TS()Z
    .locals 1

    .prologue
    .line 6835
    iget v0, p0, Ljqv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6802
    iget-object v0, p0, Ljqv;->bmN:Ljava/lang/String;

    return-object v0
.end method

.method public final TW()Z
    .locals 1

    .prologue
    .line 6813
    iget v0, p0, Ljqv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6780
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqv;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqv;->bmN:Ljava/lang/String;

    iget v0, p0, Ljqv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqv;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqv;->ezC:[Ljqu;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljqu;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljqv;->ezC:[Ljqu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljqu;

    invoke-direct {v3}, Ljqu;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljqv;->ezC:[Ljqu;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljqu;

    invoke-direct {v3}, Ljqu;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljqv;->ezC:[Ljqu;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqv;->ezA:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljqv;->ezA:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljqv;->ezA:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljqv;->ezA:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqv;->bmL:I

    iget v0, p0, Ljqv;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqv;->aez:I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljqv;->bmJ:Z

    iget v0, p0, Ljqv;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqv;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btT()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    iput-wide v2, p0, Ljqv;->ezB:D

    iget v0, p0, Ljqv;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljqv;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x30 -> :sswitch_4
        0x38 -> :sswitch_5
        0x41 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6904
    iget v0, p0, Ljqv;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6905
    const/4 v0, 0x1

    iget-object v2, p0, Ljqv;->bmN:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 6907
    :cond_0
    iget-object v0, p0, Ljqv;->ezC:[Ljqu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljqv;->ezC:[Ljqu;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 6908
    :goto_0
    iget-object v2, p0, Ljqv;->ezC:[Ljqu;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 6909
    iget-object v2, p0, Ljqv;->ezC:[Ljqu;

    aget-object v2, v2, v0

    .line 6910
    if-eqz v2, :cond_1

    .line 6911
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 6908
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6915
    :cond_2
    iget-object v0, p0, Ljqv;->ezA:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljqv;->ezA:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 6916
    :goto_1
    iget-object v0, p0, Ljqv;->ezA:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 6917
    iget-object v0, p0, Ljqv;->ezA:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 6918
    if-eqz v0, :cond_3

    .line 6919
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 6916
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6923
    :cond_4
    iget v0, p0, Ljqv;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_5

    .line 6924
    const/4 v0, 0x6

    iget v1, p0, Ljqv;->bmL:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6926
    :cond_5
    iget v0, p0, Ljqv;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 6927
    const/4 v0, 0x7

    iget-boolean v1, p0, Ljqv;->bmJ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 6929
    :cond_6
    iget v0, p0, Ljqv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_7

    .line 6930
    const/16 v0, 0x8

    iget-wide v2, p0, Ljqv;->ezB:D

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->a(ID)V

    .line 6932
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6933
    return-void
.end method

.method public final bsG()D
    .locals 2

    .prologue
    .line 6865
    iget-wide v0, p0, Ljqv;->ezB:D

    return-wide v0
.end method

.method public final bsH()Z
    .locals 1

    .prologue
    .line 6873
    iget v0, p0, Ljqv;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getIntValue()I
    .locals 1

    .prologue
    .line 6827
    iget v0, p0, Ljqv;->bmL:I

    return v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6937
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6938
    iget v2, p0, Ljqv;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 6939
    const/4 v2, 0x1

    iget-object v3, p0, Ljqv;->bmN:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6942
    :cond_0
    iget-object v2, p0, Ljqv;->ezC:[Ljqu;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljqv;->ezC:[Ljqu;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 6943
    :goto_0
    iget-object v3, p0, Ljqv;->ezC:[Ljqu;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 6944
    iget-object v3, p0, Ljqv;->ezC:[Ljqu;

    aget-object v3, v3, v0

    .line 6945
    if-eqz v3, :cond_1

    .line 6946
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6943
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 6951
    :cond_3
    iget-object v2, p0, Ljqv;->ezA:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljqv;->ezA:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    .line 6954
    :goto_1
    iget-object v4, p0, Ljqv;->ezA:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 6955
    iget-object v4, p0, Ljqv;->ezA:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 6956
    if-eqz v4, :cond_4

    .line 6957
    add-int/lit8 v3, v3, 0x1

    .line 6958
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6954
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6962
    :cond_5
    add-int/2addr v0, v2

    .line 6963
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 6965
    :cond_6
    iget v1, p0, Ljqv;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_7

    .line 6966
    const/4 v1, 0x6

    iget v2, p0, Ljqv;->bmL:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6969
    :cond_7
    iget v1, p0, Ljqv;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_8

    .line 6970
    const/4 v1, 0x7

    iget-boolean v2, p0, Ljqv;->bmJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6973
    :cond_8
    iget v1, p0, Ljqv;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_9

    .line 6974
    const/16 v1, 0x8

    iget-wide v2, p0, Ljqv;->ezB:D

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6977
    :cond_9
    return v0
.end method

.method public final yl(Ljava/lang/String;)Ljqv;
    .locals 1

    .prologue
    .line 6805
    if-nez p1, :cond_0

    .line 6806
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6808
    :cond_0
    iput-object p1, p0, Ljqv;->bmN:Ljava/lang/String;

    .line 6809
    iget v0, p0, Ljqv;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqv;->aez:I

    .line 6810
    return-object p0
.end method
