.class public final Ljwd;
.super Ljsl;
.source "PG"


# static fields
.field public static final eIQ:Ljsm;

.field public static final eIR:Ljsm;


# instance fields
.field private aez:I

.field private eIS:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 209
    const-class v0, Ljwd;

    const v1, 0x23c76a

    invoke-static {v2, v0, v1}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwd;->eIQ:Ljsm;

    .line 219
    const-class v0, Ljwd;

    const v1, 0x1de8d3f2

    invoke-static {v2, v0, v1}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljwd;->eIR:Ljsm;

    .line 225
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 254
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 255
    const/4 v0, 0x0

    iput v0, p0, Ljwd;->aez:I

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljwd;->eIS:[B

    const/4 v0, 0x0

    iput-object v0, p0, Ljwd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljwd;->eCz:I

    .line 256
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 202
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljwd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljwd;->eIS:[B

    iget v0, p0, Ljwd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwd;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 269
    iget v0, p0, Ljwd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 270
    const/4 v0, 0x1

    iget-object v1, p0, Ljwd;->eIS:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 272
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 273
    return-void
.end method

.method public final asV()[B
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Ljwd;->eIS:[B

    return-object v0
.end method

.method public final az([B)Ljwd;
    .locals 1

    .prologue
    .line 238
    if-nez p1, :cond_0

    .line 239
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 241
    :cond_0
    iput-object p1, p0, Ljwd;->eIS:[B

    .line 242
    iget v0, p0, Ljwd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljwd;->aez:I

    .line 243
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 277
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 278
    iget v1, p0, Ljwd;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 279
    const/4 v1, 0x1

    iget-object v2, p0, Ljwd;->eIS:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 282
    :cond_0
    return v0
.end method
