.class final Lfqz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leky;


# instance fields
.field private synthetic czW:Lfqw;

.field private czX:Landroid/view/View;


# direct methods
.method constructor <init>(Lfqw;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lfqz;->czW:Lfqw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final aR(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    invoke-virtual {v0, p1}, Lfqw;->br(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public final aS(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 193
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    invoke-virtual {v0}, Lfqw;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 194
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    iput-boolean v1, v0, Lfqw;->RJ:Z

    .line 195
    iput-object p1, p0, Lfqz;->czX:Landroid/view/View;

    .line 196
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    invoke-virtual {v0}, Lfqw;->invalidate()V

    .line 197
    return-void
.end method

.method public final aT(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 202
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    invoke-static {}, Lfqw;->aCM()V

    .line 204
    new-instance v0, Lfqy;

    iget-object v1, p0, Lfqz;->czW:Lfqw;

    invoke-direct {v0, v1, p1}, Lfqy;-><init>(Lfqw;Landroid/view/View;)V

    .line 206
    iget-object v1, p0, Lfqz;->czW:Lfqw;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lfqw;->RJ:Z

    .line 207
    iget-object v1, p0, Lfqz;->czW:Lfqw;

    invoke-virtual {v1}, Lfqw;->invalidate()V

    .line 209
    iget-object v1, p0, Lfqz;->czW:Lfqw;

    invoke-static {}, Lfqw;->aCN()V

    .line 210
    iget-object v1, p0, Lfqz;->czW:Lfqw;

    invoke-virtual {v1}, Lfqw;->aCQ()V

    .line 212
    iget-object v1, p0, Lfqz;->czW:Lfqw;

    iget-object v1, v1, Lfqw;->czU:Lfqx;

    if-eqz v1, :cond_0

    .line 213
    iget-object v1, p0, Lfqz;->czW:Lfqw;

    iget-object v1, v1, Lfqw;->czU:Lfqx;

    invoke-interface {v1, v0}, Lfqx;->e(Lejw;)V

    .line 216
    :cond_0
    iget-boolean v1, v0, Lejw;->ccq:Z

    if-nez v1, :cond_1

    .line 217
    invoke-virtual {v0}, Lejw;->commit()V

    .line 219
    :cond_1
    return-void
.end method

.method public final aU(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lfqw;->RJ:Z

    iget-object v0, p0, Lfqz;->czW:Lfqw;

    invoke-virtual {v0}, Lfqw;->invalidate()V

    .line 228
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    invoke-virtual {v0}, Lfqw;->aCQ()V

    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Lfqz;->czX:Landroid/view/View;

    .line 230
    return-void
.end method

.method public final auh()V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    iget-object v1, p0, Lfqz;->czX:Landroid/view/View;

    invoke-virtual {v0, v1}, Lfqw;->bs(Landroid/view/View;)V

    .line 240
    return-void
.end method

.method public final p(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 3

    .prologue
    .line 178
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    iget-boolean v0, v0, Lfqw;->RJ:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 183
    :goto_0
    return-object v0

    .line 182
    :cond_0
    iget-object v0, p0, Lfqz;->czW:Lfqw;

    iget-object v1, p0, Lfqz;->czW:Lfqw;

    invoke-virtual {v0, v1, p1}, Lfqw;->a(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lfqz;->czW:Lfqw;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v0}, Lfqw;->aX(II)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
