.class public final enum Lhha;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum dgR:Lhha;

.field public static final enum dgS:Lhha;

.field public static final enum dgT:Lhha;

.field private static final synthetic dgU:[Lhha;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lhha;

    const-string v1, "NO_EFFECT"

    invoke-direct {v0, v1, v2}, Lhha;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhha;->dgR:Lhha;

    .line 15
    new-instance v0, Lhha;

    const-string v1, "SUPPRESS"

    invoke-direct {v0, v1, v3}, Lhha;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhha;->dgS:Lhha;

    .line 18
    new-instance v0, Lhha;

    const-string v1, "PREVENT"

    invoke-direct {v0, v1, v4}, Lhha;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhha;->dgT:Lhha;

    .line 10
    const/4 v0, 0x3

    new-array v0, v0, [Lhha;

    sget-object v1, Lhha;->dgR:Lhha;

    aput-object v1, v0, v2

    sget-object v1, Lhha;->dgS:Lhha;

    aput-object v1, v0, v3

    sget-object v1, Lhha;->dgT:Lhha;

    aput-object v1, v0, v4

    sput-object v0, Lhha;->dgU:[Lhha;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static aOK()Lhha;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lhha;->dgS:Lhha;

    return-object v0
.end method

.method public static c(Ljrp;)Lhha;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Ljrp;->btA()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lhha;->dgS:Lhha;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhha;->dgR:Lhha;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lhha;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lhha;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhha;

    return-object v0
.end method

.method public static values()[Lhha;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lhha;->dgU:[Lhha;

    invoke-virtual {v0}, [Lhha;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhha;

    return-object v0
.end method


# virtual methods
.method public final aOI()Z
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lhha;->dgS:Lhha;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aOJ()Z
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lhha;->dgT:Lhha;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
