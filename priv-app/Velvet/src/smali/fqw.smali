.class public abstract Lfqw;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lekk;


# instance fields
.field private Fb:Landroid/graphics/Rect;

.field RJ:Z

.field ccZ:Lekt;

.field private czS:Leky;

.field czT:Landroid/view/ViewGroup;

.field czU:Lfqx;

.field public czV:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfqw;->Fb:Landroid/graphics/Rect;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfqw;->RJ:Z

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqw;->czV:Z

    .line 39
    invoke-direct {p0}, Lfqw;->ed()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfqw;->Fb:Landroid/graphics/Rect;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfqw;->RJ:Z

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqw;->czV:Z

    .line 44
    invoke-direct {p0}, Lfqw;->ed()V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfqw;->Fb:Landroid/graphics/Rect;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfqw;->RJ:Z

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqw;->czV:Z

    .line 49
    invoke-direct {p0}, Lfqw;->ed()V

    .line 50
    return-void
.end method

.method protected static aCM()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method protected static aCN()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method protected static aCO()V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method protected static aCP()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method private ed()V
    .locals 5

    .prologue
    .line 53
    invoke-virtual {p0}, Lfqw;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 55
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    .line 58
    new-instance v2, Lfqz;

    invoke-direct {v2, p0}, Lfqz;-><init>(Lfqw;)V

    iput-object v2, p0, Lfqw;->czS:Leky;

    .line 59
    new-instance v2, Lekt;

    const/4 v3, 0x0

    iget-object v4, p0, Lfqw;->czS:Leky;

    int-to-float v0, v0

    invoke-direct {v2, v3, v4, v1, v0}, Lekt;-><init>(ILeky;FF)V

    iput-object v2, p0, Lfqw;->ccZ:Lekt;

    .line 61
    iput-object p0, p0, Lfqw;->czT:Landroid/view/ViewGroup;

    .line 62
    return-void
.end method


# virtual methods
.method final a(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;
    .locals 4

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 251
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v2

    float-to-int v1, v0

    .line 252
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v2

    float-to-int v2, v0

    .line 255
    if-ne p1, p0, :cond_0

    iget-object v0, p0, Lfqw;->czT:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 256
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 267
    :goto_0
    return-object v0

    .line 261
    :cond_0
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3, v1, v2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 262
    iget-object v0, p0, Lfqw;->czT:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p0, Lfqw;->czT:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0, v3}, Landroid/view/ViewGroup;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 267
    :goto_1
    new-instance v0, Landroid/graphics/Point;

    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget v2, v3, Landroid/graphics/Rect;->top:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 265
    :cond_1
    invoke-virtual {p1, p0, v3}, Landroid/view/ViewGroup;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_1
.end method

.method public final a(Lfqx;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lfqw;->czU:Lfqx;

    .line 76
    return-void
.end method

.method public final a(Lcom/google/android/shared/ui/SuggestionGridLayout;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 272
    iget-boolean v0, p0, Lfqw;->czV:Z

    if-nez v0, :cond_1

    .line 286
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    move v2, v1

    .line 276
    :goto_1
    iget-object v3, p0, Lfqw;->czT:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lfqw;->czT:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_2

    add-int/lit8 v2, v2, 0x1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    if-le v2, v0, :cond_0

    .line 280
    invoke-virtual {p0, p1, p2}, Lfqw;->a(Landroid/view/ViewGroup;Landroid/view/MotionEvent;)Landroid/graphics/Point;

    move-result-object v0

    .line 282
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v2, v0}, Lfqw;->aX(II)Landroid/view/View;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_0

    .line 284
    iget-object v1, p0, Lfqw;->czS:Leky;

    invoke-interface {v1, v0}, Leky;->aR(Landroid/view/View;)Z

    move-result v1

    goto :goto_0
.end method

.method protected aCQ()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method final aX(II)Landroid/view/View;
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lfqw;->czT:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 127
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 128
    iget-object v0, p0, Lfqw;->czT:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 130
    iget-object v2, p0, Lfqw;->Fb:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 133
    iget-object v2, p0, Lfqw;->Fb:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    :goto_1
    return-object v0

    .line 127
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 137
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected br(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    return v0
.end method

.method protected bs(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public final fa(Z)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfqw;->czV:Z

    .line 72
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 104
    iget-boolean v1, p0, Lfqw;->czV:Z

    if-nez v1, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    iget-object v1, p0, Lfqw;->ccZ:Lekt;

    invoke-virtual {v1, p1}, Lekt;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 109
    if-eqz v1, :cond_2

    .line 110
    invoke-virtual {p0}, Lfqw;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 112
    :cond_2
    if-nez v1, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lfqw;->czV:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 122
    :goto_0
    return v0

    .line 119
    :cond_0
    iget-object v0, p0, Lfqw;->ccZ:Lekt;

    invoke-virtual {v0, p1}, Lekt;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 122
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final w(ZZ)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lfqw;->ccZ:Lekt;

    iput-boolean p1, v0, Lekt;->cel:Z

    .line 305
    iget-object v0, p0, Lfqw;->ccZ:Lekt;

    iput-boolean p2, v0, Lekt;->cem:Z

    .line 306
    return-void
.end method
