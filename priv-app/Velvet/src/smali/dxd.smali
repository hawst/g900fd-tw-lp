.class public Ldxd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Ldxd;->mContext:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Ldxd;->mResources:Landroid/content/res/Resources;

    .line 28
    return-void
.end method

.method private a(Ljqu;Ldxb;)Ldws;
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p1}, Ljqu;->bsF()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljqu;->bqD()Z

    move-result v0

    if-nez v0, :cond_1

    .line 84
    invoke-virtual {p1}, Ljqu;->TW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Ldws;

    invoke-virtual {p1}, Ljqu;->TV()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    .line 95
    :goto_0
    return-object v0

    .line 87
    :cond_0
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 91
    :cond_1
    invoke-virtual {p1}, Ljqu;->bqD()Z

    move-result v0

    if-nez v0, :cond_4

    .line 92
    invoke-virtual {p1}, Ljqu;->bsE()I

    move-result v0

    const/16 v1, 0xf

    if-ne v0, v1, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    new-instance v0, Ldws;

    iget-object v1, p0, Ldxd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    :cond_3
    invoke-interface {p2, p1}, Ldxb;->b(Ljqu;)Ldws;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_4
    iget-object v0, p0, Ldxd;->mResources:Landroid/content/res/Resources;

    invoke-interface {p2, p1, v0}, Ldxb;->a(Ljqu;Landroid/content/res/Resources;)Ldws;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ldxb;Ljqv;[I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 120
    iget-object v2, p1, Ljqv;->ezC:[Ljqu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 122
    invoke-virtual {v4}, Ljqu;->bqD()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Ljqu;->bqC()I

    move-result v5

    invoke-static {p2, v5}, Lius;->d([II)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 124
    invoke-interface {p0, v4}, Ldxb;->a(Ljqu;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 131
    :goto_1
    return v0

    .line 120
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 131
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljqv;Ldxb;)Ldws;
    .locals 6
    .param p1    # Ljqv;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 43
    iget-object v1, p1, Ljqv;->ezC:[Ljqu;

    array-length v1, v1

    .line 45
    invoke-virtual {p1}, Ljqv;->TW()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 46
    invoke-virtual {p1}, Ljqv;->TV()Ljava/lang/String;

    move-result-object v2

    .line 48
    if-nez v1, :cond_0

    .line 49
    new-instance v0, Ldws;

    invoke-direct {v0, v2}, Ldws;-><init>(Ljava/lang/String;)V

    .line 78
    :goto_0
    return-object v0

    .line 52
    :cond_0
    new-array v3, v1, [Ljava/lang/Object;

    .line 53
    :goto_1
    if-ge v0, v1, :cond_2

    .line 54
    iget-object v4, p1, Ljqv;->ezC:[Ljqu;

    aget-object v4, v4, v0

    invoke-direct {p0, v4, p2}, Ldxd;->a(Ljqu;Ldxb;)Ldws;

    move-result-object v4

    .line 56
    invoke-virtual {v4}, Ldws;->Pd()Z

    move-result v5

    if-nez v5, :cond_1

    .line 57
    sget-object v0, Ldws;->bOO:Ldws;

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {v4}, Ldws;->getObject()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v0

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 62
    :cond_2
    new-instance v0, Ldws;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldws;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_3
    iget-object v2, p1, Ljqv;->ezA:[Ljava/lang/String;

    array-length v2, v2

    if-eqz v2, :cond_4

    .line 65
    new-instance v0, Ldws;

    iget-object v1, p1, Ljqv;->ezA:[Ljava/lang/String;

    invoke-direct {v0, v1}, Ldws;-><init>([Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :cond_4
    invoke-virtual {p1}, Ljqv;->TP()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 67
    new-instance v0, Ldws;

    invoke-virtual {p1}, Ljqv;->TO()Z

    move-result v1

    invoke-direct {v0, v1}, Ldws;-><init>(Z)V

    goto :goto_0

    .line 68
    :cond_5
    invoke-virtual {p1}, Ljqv;->TS()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 69
    new-instance v0, Ldws;

    invoke-virtual {p1}, Ljqv;->getIntValue()I

    move-result v1

    invoke-direct {v0, v1}, Ldws;-><init>(I)V

    goto :goto_0

    .line 70
    :cond_6
    invoke-virtual {p1}, Ljqv;->bsH()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 71
    new-instance v0, Ldws;

    invoke-virtual {p1}, Ljqv;->bsG()D

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ldws;-><init>(D)V

    goto :goto_0

    .line 72
    :cond_7
    if-eqz v1, :cond_9

    .line 73
    const/4 v2, 0x1

    if-eq v1, v2, :cond_8

    .line 74
    invoke-interface {p2}, Ldxb;->ajo()Ldwd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No format string but "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Ljqv;->ezC:[Ljqu;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " values; picking first"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    :cond_8
    iget-object v1, p1, Ljqv;->ezC:[Ljqu;

    aget-object v0, v1, v0

    invoke-direct {p0, v0, p2}, Ldxd;->a(Ljqu;Ldxb;)Ldws;

    move-result-object v0

    goto/16 :goto_0

    .line 78
    :cond_9
    sget-object v0, Ldws;->bOO:Ldws;

    goto/16 :goto_0
.end method
