.class public final Laha;
.super Ljsr;
.source "PG"


# instance fields
.field public XL:[B

.field public XM:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 170
    invoke-direct {p0}, Ljsr;-><init>()V

    .line 171
    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Laha;->XL:[B

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Laha;->XM:J

    const/4 v0, -0x1

    iput v0, p0, Laha;->eCz:I

    .line 172
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 147
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Ljsu;->b(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Laha;->XL:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Laha;->XM:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 184
    const/4 v0, 0x1

    iget-object v1, p0, Laha;->XL:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 185
    const/4 v0, 0x2

    iget-wide v2, p0, Laha;->XM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 186
    invoke-super {p0, p1}, Ljsr;->a(Ljsj;)V

    .line 187
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 191
    invoke-super {p0}, Ljsr;->lF()I

    move-result v0

    .line 192
    const/4 v1, 0x1

    iget-object v2, p0, Laha;->XL:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    const/4 v1, 0x2

    iget-wide v2, p0, Laha;->XM:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    return v0
.end method
