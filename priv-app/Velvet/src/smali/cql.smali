.class public final Lcql;
.super Lcqu;
.source "PG"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mSearchUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lcjs;Lemp;Landroid/content/Context;Lcpn;Lcke;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p5}, Lcqu;-><init>(Lcjs;Lemp;Lcke;)V

    .line 68
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcql;->mContext:Landroid/content/Context;

    .line 69
    invoke-static {p4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpn;

    iput-object v0, p0, Lcql;->mSearchUrlHelper:Lcpn;

    .line 70
    return-void
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 183
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184
    if-eqz p2, :cond_0

    .line 185
    invoke-static {v0}, Laqh;->dX(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 192
    :cond_0
    :goto_0
    return-object v0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    const-string v1, "GwsSuggestionParser"

    const-string v2, "Couldn\'t get string value from extras"

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 192
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lorg/json/JSONArray;Ljava/util/List;)V
    .locals 13

    .prologue
    const/4 v11, -0x1

    const/4 v10, 0x5

    const/4 v0, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 85
    invoke-virtual {p1, v0, v11}, Lorg/json/JSONArray;->optInt(II)I

    move-result v1

    .line 87
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Lorg/json/JSONArray;->optJSONArray(I)Lorg/json/JSONArray;

    move-result-object v7

    .line 88
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v6, v2

    move v4, v2

    .line 90
    :goto_0
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v6, v8, :cond_1

    .line 91
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->getInt(I)I

    move-result v8

    .line 92
    const/16 v9, 0x27

    if-ne v8, v9, :cond_0

    move v4, v0

    .line 95
    :cond_0
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 98
    :cond_1
    if-ne v1, v11, :cond_2

    .line 99
    const-string v1, "GwsSuggestionParser"

    const-string v6, "Suggestion missing type. Defaulting to query suggestion."

    new-array v7, v2, [Ljava/lang/Object;

    invoke-static {v10, v1, v6, v7}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v1, v2

    .line 104
    :cond_2
    const-wide/16 v6, 0x0

    .line 105
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v8

    const/4 v9, 0x3

    if-le v8, v9, :cond_d

    .line 106
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 107
    const-string v6, "zc"

    invoke-virtual {v8, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 110
    :goto_1
    if-nez v1, :cond_3

    .line 111
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Laqh;->dX(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-static {v0, v4, v5, v6, v7}, Ldfx;->a(Ljava/lang/CharSequence;ZLjava/util/ArrayList;J)Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    :goto_2
    return-void

    .line 114
    :cond_3
    const/16 v9, 0x23

    if-ne v1, v9, :cond_4

    .line 115
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Laqh;->dX(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    new-instance v2, Lded;

    invoke-direct {v2}, Lded;-><init>()V

    iput-object v1, v2, Lded;->bux:Ljava/lang/CharSequence;

    const-string v3, "android.intent.action.WEB_SEARCH"

    iput-object v3, v2, Lded;->buE:Ljava/lang/String;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lded;->buJ:Ljava/lang/String;

    const/16 v1, 0x23

    invoke-virtual {v2, v1}, Lded;->fI(I)Lded;

    move-result-object v1

    iput-object v5, v1, Lded;->buM:Ljava/util/ArrayList;

    iput-boolean v0, v1, Lded;->buN:Z

    iput-wide v6, v1, Lded;->bjh:J

    invoke-virtual {v1}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 118
    :cond_4
    if-ne v1, v10, :cond_7

    .line 119
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Laqh;->dX(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 123
    if-eqz v8, :cond_c

    .line 124
    const-string v3, "b"

    invoke-static {v8, v3, v0}, Lcql;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 125
    const-string v0, "a"

    invoke-static {v8, v0, v2}, Lcql;->a(Lorg/json/JSONObject;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 126
    iget-object v2, p0, Lcql;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v2, v0}, Lcpn;->hK(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v3

    move-object v3, v0

    .line 128
    :goto_3
    if-nez v3, :cond_5

    .line 129
    invoke-static {v1}, Landroid/webkit/URLUtil;->guessUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 131
    :cond_5
    if-nez v2, :cond_6

    move-object v2, v1

    .line 134
    :cond_6
    iget-object v0, p0, Lcql;->mContext:Landroid/content/Context;

    invoke-static/range {v1 .. v7}, Ldfx;->a(Ljava/lang/String;Ljava/lang/CharSequence;Landroid/net/Uri;ZLjava/util/ArrayList;J)Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 137
    :cond_7
    const/16 v4, 0x2a

    if-ne v1, v4, :cond_9

    .line 138
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Laqh;->dX(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    const-string v1, "web"

    .line 144
    if-eqz v8, :cond_b

    .line 145
    const-string v1, "a"

    const-string v3, "web"

    invoke-virtual {v8, v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    const-string v3, "b"

    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 148
    const-string v3, "c"

    invoke-virtual {v8, v3, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 152
    if-eqz v2, :cond_8

    .line 153
    invoke-static {v1}, Lckh;->hg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 156
    :cond_8
    const-string v2, "d"

    invoke-virtual {v8, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 157
    const-string v3, "e"

    invoke-virtual {v8, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v12, v2

    move-object v2, v3

    move-object v3, v1

    move-object v1, v12

    .line 164
    :goto_4
    invoke-static/range {v0 .. v7}, Ldfx;->a(Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;J)Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 166
    :cond_9
    const/16 v3, 0x53

    if-ne v1, v3, :cond_a

    .line 167
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Laqh;->dX(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 168
    invoke-static {v1}, Landroid/webkit/URLUtil;->guessUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 170
    iget-object v3, p0, Lcql;->mContext:Landroid/content/Context;

    new-instance v3, Lded;

    invoke-direct {v3}, Lded;-><init>()V

    iput-object v1, v3, Lded;->bux:Ljava/lang/CharSequence;

    iput-object v1, v3, Lded;->buz:Ljava/lang/String;

    const-string v1, "android.intent.action.VIEW"

    iput-object v1, v3, Lded;->buE:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lded;->buF:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lded;->buJ:Ljava/lang/String;

    iput-boolean v0, v3, Lded;->buN:Z

    const/16 v0, 0x53

    invoke-virtual {v3, v0}, Lded;->fI(I)Lded;

    move-result-object v0

    iput-object v5, v0, Lded;->buM:Ljava/util/ArrayList;

    iput-wide v6, v0, Lded;->bjh:J

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 173
    :cond_a
    const-string v0, "GwsSuggestionParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown suggestion type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v10, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_b
    move-object v2, v3

    move-object v4, v3

    move-object v12, v3

    move-object v3, v1

    move-object v1, v12

    goto :goto_4

    :cond_c
    move-object v2, v3

    goto/16 :goto_3

    :cond_d
    move-object v8, v3

    goto/16 :goto_1
.end method
