.class public Lgma;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final cOT:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lgma;->cOT:Ljava/util/Map;

    .line 77
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(ILjava/lang/String;III)V
    .locals 3

    .prologue
    .line 37
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgma;->cOT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    .line 38
    iget-object v0, p0, Lgma;->cOT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 39
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 40
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 43
    :cond_0
    iget-object v0, p0, Lgma;->cOT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lgmb;

    invoke-direct {v2, p2, p3, p4, p5}, Lgmb;-><init>(Ljava/lang/String;III)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final aHk()Ljava/util/Map;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lgma;->cOT:Ljava/util/Map;

    return-object v0
.end method

.method public final declared-synchronized c(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgma;->cOT:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmb;

    .line 53
    const/16 v1, 0xf

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    .line 56
    if-eqz v0, :cond_0

    .line 57
    new-instance v2, Litg;

    invoke-direct {v2}, Litg;-><init>()V

    iget v3, v0, Lgmb;->cOU:I

    invoke-virtual {v2, v3}, Litg;->mo(I)Litg;

    move-result-object v2

    iget v3, v0, Lgmb;->bgm:I

    invoke-virtual {v2, v3}, Litg;->mp(I)Litg;

    move-result-object v2

    iget v3, v0, Lgmb;->mLength:I

    invoke-virtual {v2, v3}, Litg;->mq(I)Litg;

    move-result-object v2

    invoke-virtual {v2, p2}, Litg;->pF(Ljava/lang/String;)Litg;

    move-result-object v2

    invoke-virtual {v2, p3}, Litg;->pG(Ljava/lang/String;)Litg;

    move-result-object v2

    iput-object v2, v1, Litu;->dIr:Litg;

    .line 63
    iget-object v0, v0, Lgmb;->mRequestId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Litu;->pL(Ljava/lang/String;)Litu;

    .line 65
    :cond_0
    invoke-static {v1}, Lege;->a(Litu;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
