.class public abstract Lbpy;
.super Landroid/os/Binder;

# interfaces
.implements Lbpx;


# direct methods
.method public static v(Landroid/os/IBinder;)Lbpx;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lbpx;

    if-eqz v1, :cond_1

    check-cast v0, Lbpx;

    goto :goto_0

    :cond_1
    new-instance v0, Lbpz;

    invoke-direct {v0, p0}, Lbpz;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    const-string v0, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v2, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    sget-object v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;->CREATOR:Lbve;

    invoke-static {p2}, Lbve;->ae(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;

    move-result-object v0

    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbpv;->u(Landroid/os/IBinder;)Lbpu;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbpy;->a(Lcom/google/android/gms/search/corpora/RequestIndexingCall$b;Lbpu;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_2
    const-string v2, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;->CREATOR:Lbuy;

    invoke-static {p2}, Lbuy;->Y(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;

    move-result-object v0

    :cond_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbpv;->u(Landroid/os/IBinder;)Lbpu;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbpy;->a(Lcom/google/android/gms/search/corpora/ClearCorpusCall$b;Lbpu;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_3
    const-string v2, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$b;->CREATOR:Lbvc;

    invoke-static {p2}, Lbvc;->ac(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$b;

    move-result-object v0

    :cond_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbpv;->u(Landroid/os/IBinder;)Lbpu;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbpy;->a(Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$b;Lbpu;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto :goto_0

    :sswitch_4
    const-string v2, "com.google.android.gms.search.corpora.internal.ISearchCorporaService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$b;->CREATOR:Lbva;

    invoke-static {p2}, Lbva;->aa(Landroid/os/Parcel;)Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$b;

    move-result-object v0

    :cond_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lbpv;->u(Landroid/os/IBinder;)Lbpu;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lbpy;->a(Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$b;Lbpu;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
