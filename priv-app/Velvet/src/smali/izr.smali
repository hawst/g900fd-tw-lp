.class public final Lizr;
.super Ljsl;
.source "PG"


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private afh:Ljava/lang/String;

.field private afi:J

.field private afj:J

.field private agv:I

.field private aiK:Ljava/lang/String;

.field public aiX:Ljcn;

.field private dMk:Ljava/lang/String;

.field public dNE:Lixx;

.field public dOb:[Ljava/lang/String;

.field private dVb:Ljava/lang/String;

.field private dVc:Ljava/lang/String;

.field private dVd:Ljava/lang/String;

.field private dVe:[Ljava/lang/String;

.field private dVf:Lixa;

.field private dVg:Ljava/lang/String;

.field public dVh:[Ljava/lang/String;

.field private dVi:[Ljbp;

.field private dVj:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60425
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 60426
    iput v2, p0, Lizr;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Lizr;->agv:I

    const-string v0, ""

    iput-object v0, p0, Lizr;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lizr;->aiK:Ljava/lang/String;

    iput-object v1, p0, Lizr;->aiX:Ljcn;

    iput-object v1, p0, Lizr;->aeB:Ljbp;

    const-string v0, ""

    iput-object v0, p0, Lizr;->dVb:Ljava/lang/String;

    iput-wide v4, p0, Lizr;->afi:J

    const-string v0, ""

    iput-object v0, p0, Lizr;->dVc:Ljava/lang/String;

    iput-wide v4, p0, Lizr;->afj:J

    const-string v0, ""

    iput-object v0, p0, Lizr;->dVd:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lizr;->dVe:[Ljava/lang/String;

    iput-object v1, p0, Lizr;->dNE:Lixx;

    iput-object v1, p0, Lizr;->dVf:Lixa;

    const-string v0, ""

    iput-object v0, p0, Lizr;->dMk:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lizr;->dOb:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lizr;->dVg:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lizr;->dVh:[Ljava/lang/String;

    invoke-static {}, Ljbp;->beZ()[Ljbp;

    move-result-object v0

    iput-object v0, p0, Lizr;->dVi:[Ljbp;

    iput-boolean v2, p0, Lizr;->dVj:Z

    iput-object v1, p0, Lizr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizr;->eCz:I

    .line 60427
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 60148
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lizr;->agv:I

    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizr;->afh:Ljava/lang/String;

    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizr;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lizr;->aiX:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Lizr;->aiX:Ljcn;

    :cond_1
    iget-object v0, p0, Lizr;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lizr;->aeB:Ljbp;

    if-nez v0, :cond_2

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Lizr;->aeB:Ljbp;

    :cond_2
    iget-object v0, p0, Lizr;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lizr;->afi:J

    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lizr;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lizr;->afj:J

    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lizr;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizr;->dVd:Ljava/lang/String;

    iget v0, p0, Lizr;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lizr;->aez:I

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizr;->dVe:[Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lizr;->dVe:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lizr;->dVe:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lizr;->dVe:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lizr;->dNE:Lixx;

    if-nez v0, :cond_6

    new-instance v0, Lixx;

    invoke-direct {v0}, Lixx;-><init>()V

    iput-object v0, p0, Lizr;->dNE:Lixx;

    :cond_6
    iget-object v0, p0, Lizr;->dNE:Lixx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizr;->aiK:Ljava/lang/String;

    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lizr;->aez:I

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lizr;->dVf:Lixa;

    if-nez v0, :cond_7

    new-instance v0, Lixa;

    invoke-direct {v0}, Lixa;-><init>()V

    iput-object v0, p0, Lizr;->dVf:Lixa;

    :cond_7
    iget-object v0, p0, Lizr;->dVf:Lixa;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizr;->dMk:Ljava/lang/String;

    iget v0, p0, Lizr;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lizr;->aez:I

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizr;->dOb:[Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v3, p0, Lizr;->dOb:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lizr;->dOb:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lizr;->dOb:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizr;->dVb:Ljava/lang/String;

    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lizr;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizr;->dVc:Ljava/lang/String;

    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lizr;->aez:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizr;->dVg:Ljava/lang/String;

    iget v0, p0, Lizr;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lizr;->aez:I

    goto/16 :goto_0

    :sswitch_11
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizr;->dVh:[Ljava/lang/String;

    if-nez v0, :cond_c

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v3, p0, Lizr;->dVh:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_c
    iget-object v0, p0, Lizr;->dVh:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_5

    :cond_d
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lizr;->dVh:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_12
    const/16 v0, 0x92

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lizr;->dVi:[Ljbp;

    if-nez v0, :cond_f

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbp;

    if-eqz v0, :cond_e

    iget-object v3, p0, Lizr;->dVi:[Ljbp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_10

    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_f
    iget-object v0, p0, Lizr;->dVi:[Ljbp;

    array-length v0, v0

    goto :goto_7

    :cond_10
    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lizr;->dVi:[Ljbp;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lizr;->dVj:Z

    iget v0, p0, Lizr;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lizr;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x98 -> :sswitch_13
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 60458
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 60459
    const/4 v0, 0x1

    iget v2, p0, Lizr;->agv:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 60461
    :cond_0
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 60462
    const/4 v0, 0x2

    iget-object v2, p0, Lizr;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60464
    :cond_1
    iget-object v0, p0, Lizr;->aiX:Ljcn;

    if-eqz v0, :cond_2

    .line 60465
    const/4 v0, 0x3

    iget-object v2, p0, Lizr;->aiX:Ljcn;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 60467
    :cond_2
    iget-object v0, p0, Lizr;->aeB:Ljbp;

    if-eqz v0, :cond_3

    .line 60468
    const/4 v0, 0x4

    iget-object v2, p0, Lizr;->aeB:Ljbp;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 60470
    :cond_3
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 60471
    const/4 v0, 0x5

    iget-wide v2, p0, Lizr;->afi:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 60473
    :cond_4
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_5

    .line 60474
    const/4 v0, 0x6

    iget-wide v2, p0, Lizr;->afj:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 60476
    :cond_5
    iget v0, p0, Lizr;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    .line 60477
    const/4 v0, 0x7

    iget-object v2, p0, Lizr;->dVd:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60479
    :cond_6
    iget-object v0, p0, Lizr;->dVe:[Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lizr;->dVe:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 60480
    :goto_0
    iget-object v2, p0, Lizr;->dVe:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 60481
    iget-object v2, p0, Lizr;->dVe:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 60482
    if-eqz v2, :cond_7

    .line 60483
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60480
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60487
    :cond_8
    iget-object v0, p0, Lizr;->dNE:Lixx;

    if-eqz v0, :cond_9

    .line 60488
    const/16 v0, 0x9

    iget-object v2, p0, Lizr;->dNE:Lixx;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 60490
    :cond_9
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_a

    .line 60491
    const/16 v0, 0xa

    iget-object v2, p0, Lizr;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60493
    :cond_a
    iget-object v0, p0, Lizr;->dVf:Lixa;

    if-eqz v0, :cond_b

    .line 60494
    const/16 v0, 0xb

    iget-object v2, p0, Lizr;->dVf:Lixa;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 60496
    :cond_b
    iget v0, p0, Lizr;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_c

    .line 60497
    const/16 v0, 0xc

    iget-object v2, p0, Lizr;->dMk:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60499
    :cond_c
    iget-object v0, p0, Lizr;->dOb:[Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lizr;->dOb:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_e

    move v0, v1

    .line 60500
    :goto_1
    iget-object v2, p0, Lizr;->dOb:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_e

    .line 60501
    iget-object v2, p0, Lizr;->dOb:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 60502
    if-eqz v2, :cond_d

    .line 60503
    const/16 v3, 0xd

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60500
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 60507
    :cond_e
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_f

    .line 60508
    const/16 v0, 0xe

    iget-object v2, p0, Lizr;->dVb:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60510
    :cond_f
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_10

    .line 60511
    const/16 v0, 0xf

    iget-object v2, p0, Lizr;->dVc:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60513
    :cond_10
    iget v0, p0, Lizr;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_11

    .line 60514
    const/16 v0, 0x10

    iget-object v2, p0, Lizr;->dVg:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60516
    :cond_11
    iget-object v0, p0, Lizr;->dVh:[Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lizr;->dVh:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_13

    move v0, v1

    .line 60517
    :goto_2
    iget-object v2, p0, Lizr;->dVh:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_13

    .line 60518
    iget-object v2, p0, Lizr;->dVh:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 60519
    if-eqz v2, :cond_12

    .line 60520
    const/16 v3, 0x11

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 60517
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 60524
    :cond_13
    iget-object v0, p0, Lizr;->dVi:[Ljbp;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lizr;->dVi:[Ljbp;

    array-length v0, v0

    if-lez v0, :cond_15

    .line 60525
    :goto_3
    iget-object v0, p0, Lizr;->dVi:[Ljbp;

    array-length v0, v0

    if-ge v1, v0, :cond_15

    .line 60526
    iget-object v0, p0, Lizr;->dVi:[Ljbp;

    aget-object v0, v0, v1

    .line 60527
    if-eqz v0, :cond_14

    .line 60528
    const/16 v2, 0x12

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 60525
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 60532
    :cond_15
    iget v0, p0, Lizr;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_16

    .line 60533
    const/16 v0, 0x13

    iget-boolean v1, p0, Lizr;->dVj:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 60535
    :cond_16
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 60536
    return-void
.end method

.method public final bdq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60243
    iget-object v0, p0, Lizr;->dVb:Ljava/lang/String;

    return-object v0
.end method

.method public final bdr()Z
    .locals 1

    .prologue
    .line 60254
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bds()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60284
    iget-object v0, p0, Lizr;->dVc:Ljava/lang/String;

    return-object v0
.end method

.method public final bdt()Z
    .locals 1

    .prologue
    .line 60295
    iget v0, p0, Lizr;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bdu()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60381
    iget-object v0, p0, Lizr;->dVg:Ljava/lang/String;

    return-object v0
.end method

.method public final bdv()Z
    .locals 1

    .prologue
    .line 60409
    iget-boolean v0, p0, Lizr;->dVj:Z

    return v0
.end method

.method public final cv(J)Lizr;
    .locals 2

    .prologue
    .line 60268
    const-wide/32 v0, 0x50664f18

    iput-wide v0, p0, Lizr;->afi:J

    .line 60269
    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lizr;->aez:I

    .line 60270
    return-object p0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60215
    iget-object v0, p0, Lizr;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60193
    iget-object v0, p0, Lizr;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 60174
    iget v0, p0, Lizr;->agv:I

    return v0
.end method

.method public final hx(Z)Lizr;
    .locals 1

    .prologue
    .line 60412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lizr;->dVj:Z

    .line 60413
    iget v0, p0, Lizr;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lizr;->aez:I

    .line 60414
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 60540
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 60541
    iget v1, p0, Lizr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 60542
    const/4 v1, 0x1

    iget v3, p0, Lizr;->agv:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 60545
    :cond_0
    iget v1, p0, Lizr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 60546
    const/4 v1, 0x2

    iget-object v3, p0, Lizr;->afh:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60549
    :cond_1
    iget-object v1, p0, Lizr;->aiX:Ljcn;

    if-eqz v1, :cond_2

    .line 60550
    const/4 v1, 0x3

    iget-object v3, p0, Lizr;->aiX:Ljcn;

    invoke-static {v1, v3}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60553
    :cond_2
    iget-object v1, p0, Lizr;->aeB:Ljbp;

    if-eqz v1, :cond_3

    .line 60554
    const/4 v1, 0x4

    iget-object v3, p0, Lizr;->aeB:Ljbp;

    invoke-static {v1, v3}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60557
    :cond_3
    iget v1, p0, Lizr;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 60558
    const/4 v1, 0x5

    iget-wide v4, p0, Lizr;->afi:J

    invoke-static {v1, v4, v5}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 60561
    :cond_4
    iget v1, p0, Lizr;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_5

    .line 60562
    const/4 v1, 0x6

    iget-wide v4, p0, Lizr;->afj:J

    invoke-static {v1, v4, v5}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 60565
    :cond_5
    iget v1, p0, Lizr;->aez:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_6

    .line 60566
    const/4 v1, 0x7

    iget-object v3, p0, Lizr;->dVd:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60569
    :cond_6
    iget-object v1, p0, Lizr;->dVe:[Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lizr;->dVe:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    move v4, v2

    .line 60572
    :goto_0
    iget-object v5, p0, Lizr;->dVe:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_8

    .line 60573
    iget-object v5, p0, Lizr;->dVe:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 60574
    if-eqz v5, :cond_7

    .line 60575
    add-int/lit8 v4, v4, 0x1

    .line 60576
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 60572
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60580
    :cond_8
    add-int/2addr v0, v3

    .line 60581
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 60583
    :cond_9
    iget-object v1, p0, Lizr;->dNE:Lixx;

    if-eqz v1, :cond_a

    .line 60584
    const/16 v1, 0x9

    iget-object v3, p0, Lizr;->dNE:Lixx;

    invoke-static {v1, v3}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60587
    :cond_a
    iget v1, p0, Lizr;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_b

    .line 60588
    const/16 v1, 0xa

    iget-object v3, p0, Lizr;->aiK:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60591
    :cond_b
    iget-object v1, p0, Lizr;->dVf:Lixa;

    if-eqz v1, :cond_c

    .line 60592
    const/16 v1, 0xb

    iget-object v3, p0, Lizr;->dVf:Lixa;

    invoke-static {v1, v3}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60595
    :cond_c
    iget v1, p0, Lizr;->aez:I

    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_d

    .line 60596
    const/16 v1, 0xc

    iget-object v3, p0, Lizr;->dMk:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60599
    :cond_d
    iget-object v1, p0, Lizr;->dOb:[Ljava/lang/String;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lizr;->dOb:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_10

    move v1, v2

    move v3, v2

    move v4, v2

    .line 60602
    :goto_1
    iget-object v5, p0, Lizr;->dOb:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_f

    .line 60603
    iget-object v5, p0, Lizr;->dOb:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 60604
    if-eqz v5, :cond_e

    .line 60605
    add-int/lit8 v4, v4, 0x1

    .line 60606
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 60602
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 60610
    :cond_f
    add-int/2addr v0, v3

    .line 60611
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 60613
    :cond_10
    iget v1, p0, Lizr;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_11

    .line 60614
    const/16 v1, 0xe

    iget-object v3, p0, Lizr;->dVb:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60617
    :cond_11
    iget v1, p0, Lizr;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_12

    .line 60618
    const/16 v1, 0xf

    iget-object v3, p0, Lizr;->dVc:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60621
    :cond_12
    iget v1, p0, Lizr;->aez:I

    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_13

    .line 60622
    const/16 v1, 0x10

    iget-object v3, p0, Lizr;->dVg:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60625
    :cond_13
    iget-object v1, p0, Lizr;->dVh:[Ljava/lang/String;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lizr;->dVh:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_16

    move v1, v2

    move v3, v2

    move v4, v2

    .line 60628
    :goto_2
    iget-object v5, p0, Lizr;->dVh:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_15

    .line 60629
    iget-object v5, p0, Lizr;->dVh:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 60630
    if-eqz v5, :cond_14

    .line 60631
    add-int/lit8 v4, v4, 0x1

    .line 60632
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 60628
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 60636
    :cond_15
    add-int/2addr v0, v3

    .line 60637
    mul-int/lit8 v1, v4, 0x2

    add-int/2addr v0, v1

    .line 60639
    :cond_16
    iget-object v1, p0, Lizr;->dVi:[Ljbp;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lizr;->dVi:[Ljbp;

    array-length v1, v1

    if-lez v1, :cond_18

    .line 60640
    :goto_3
    iget-object v1, p0, Lizr;->dVi:[Ljbp;

    array-length v1, v1

    if-ge v2, v1, :cond_18

    .line 60641
    iget-object v1, p0, Lizr;->dVi:[Ljbp;

    aget-object v1, v1, v2

    .line 60642
    if-eqz v1, :cond_17

    .line 60643
    const/16 v3, 0x12

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60640
    :cond_17
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 60648
    :cond_18
    iget v1, p0, Lizr;->aez:I

    and-int/lit16 v1, v1, 0x400

    if-eqz v1, :cond_19

    .line 60649
    const/16 v1, 0x13

    iget-boolean v2, p0, Lizr;->dVj:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 60652
    :cond_19
    return v0
.end method

.method public final nW(I)Lizr;
    .locals 1

    .prologue
    .line 60177
    iput p1, p0, Lizr;->agv:I

    .line 60178
    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizr;->aez:I

    .line 60179
    return-object p0
.end method

.method public final nk()J
    .locals 2

    .prologue
    .line 60265
    iget-wide v0, p0, Lizr;->afi:J

    return-wide v0
.end method

.method public final rM(Ljava/lang/String;)Lizr;
    .locals 1

    .prologue
    .line 60196
    if-nez p1, :cond_0

    .line 60197
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60199
    :cond_0
    iput-object p1, p0, Lizr;->afh:Ljava/lang/String;

    .line 60200
    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizr;->aez:I

    .line 60201
    return-object p0
.end method

.method public final rN(Ljava/lang/String;)Lizr;
    .locals 1

    .prologue
    .line 60218
    if-nez p1, :cond_0

    .line 60219
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60221
    :cond_0
    iput-object p1, p0, Lizr;->aiK:Ljava/lang/String;

    .line 60222
    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lizr;->aez:I

    .line 60223
    return-object p0
.end method

.method public final rO(Ljava/lang/String;)Lizr;
    .locals 1

    .prologue
    .line 60246
    if-nez p1, :cond_0

    .line 60247
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60249
    :cond_0
    iput-object p1, p0, Lizr;->dVb:Ljava/lang/String;

    .line 60250
    iget v0, p0, Lizr;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lizr;->aez:I

    .line 60251
    return-object p0
.end method

.method public final rP(Ljava/lang/String;)Lizr;
    .locals 1

    .prologue
    .line 60384
    if-nez p1, :cond_0

    .line 60385
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60387
    :cond_0
    iput-object p1, p0, Lizr;->dVg:Ljava/lang/String;

    .line 60388
    iget v0, p0, Lizr;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lizr;->aez:I

    .line 60389
    return-object p0
.end method
