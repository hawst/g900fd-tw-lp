.class public final Ljnr;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile evz:[Ljnr;


# instance fields
.field private aez:I

.field private evA:I

.field private evB:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 600
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 601
    iput v0, p0, Ljnr;->aez:I

    iput v0, p0, Ljnr;->evA:I

    iput v0, p0, Ljnr;->evB:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljnr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnr;->eCz:I

    .line 602
    return-void
.end method

.method public static bqS()[Ljnr;
    .locals 2

    .prologue
    .line 549
    sget-object v0, Ljnr;->evz:[Ljnr;

    if-nez v0, :cond_1

    .line 550
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 552
    :try_start_0
    sget-object v0, Ljnr;->evz:[Ljnr;

    if-nez v0, :cond_0

    .line 553
    const/4 v0, 0x0

    new-array v0, v0, [Ljnr;

    sput-object v0, Ljnr;->evz:[Ljnr;

    .line 555
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    :cond_1
    sget-object v0, Ljnr;->evz:[Ljnr;

    return-object v0

    .line 555
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 536
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljnr;->evA:I

    iget v0, p0, Ljnr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnr;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljnr;->evB:I

    iget v0, p0, Ljnr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnr;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 616
    iget v0, p0, Ljnr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 617
    const/4 v0, 0x1

    iget v1, p0, Ljnr;->evA:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 619
    :cond_0
    iget v0, p0, Ljnr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 620
    const/4 v0, 0x2

    iget v1, p0, Ljnr;->evB:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 622
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 623
    return-void
.end method

.method public final bqT()Z
    .locals 1

    .prologue
    .line 573
    iget v0, p0, Ljnr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bqU()Z
    .locals 1

    .prologue
    .line 592
    iget v0, p0, Ljnr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getMethod()I
    .locals 1

    .prologue
    .line 584
    iget v0, p0, Ljnr;->evB:I

    return v0
.end method

.method public final getMinutes()I
    .locals 1

    .prologue
    .line 565
    iget v0, p0, Ljnr;->evA:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 627
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 628
    iget v1, p0, Ljnr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 629
    const/4 v1, 0x1

    iget v2, p0, Ljnr;->evA:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 632
    :cond_0
    iget v1, p0, Ljnr;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 633
    const/4 v1, 0x2

    iget v2, p0, Ljnr;->evB:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 636
    :cond_1
    return v0
.end method

.method public final qU(I)Ljnr;
    .locals 1

    .prologue
    .line 568
    iput p1, p0, Ljnr;->evA:I

    .line 569
    iget v0, p0, Ljnr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnr;->aez:I

    .line 570
    return-object p0
.end method

.method public final qV(I)Ljnr;
    .locals 1

    .prologue
    .line 587
    iput p1, p0, Ljnr;->evB:I

    .line 588
    iget v0, p0, Ljnr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnr;->aez:I

    .line 589
    return-object p0
.end method
