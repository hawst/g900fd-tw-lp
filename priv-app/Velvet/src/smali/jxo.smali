.class public final Ljxo;
.super Ljsl;
.source "PG"


# static fields
.field public static final eKU:Ljsm;


# instance fields
.field private aez:I

.field private eIS:[B

.field private eJL:Z

.field private eKV:Ljxn;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 534
    const-class v0, Ljxo;

    const v1, 0xda33022

    invoke-static {v2, v0, v1}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljxo;->eKU:Ljsm;

    .line 544
    const-class v0, Ljxo;

    const v1, 0xe2db4d2

    invoke-static {v2, v0, v1}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    .line 550
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 601
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 602
    iput v1, p0, Ljxo;->aez:I

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljxo;->eIS:[B

    iput-boolean v1, p0, Ljxo;->eJL:Z

    iput-object v2, p0, Ljxo;->eKV:Ljxn;

    iput-object v2, p0, Ljxo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxo;->eCz:I

    .line 603
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 527
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljxo;->eIS:[B

    iget v0, p0, Ljxo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxo;->eJL:Z

    iget v0, p0, Ljxo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxo;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljxo;->eKV:Ljxn;

    if-nez v0, :cond_1

    new-instance v0, Ljxn;

    invoke-direct {v0}, Ljxn;-><init>()V

    iput-object v0, p0, Ljxo;->eKV:Ljxn;

    :cond_1
    iget-object v0, p0, Ljxo;->eKV:Ljxn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 618
    iget v0, p0, Ljxo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 619
    const/4 v0, 0x1

    iget-object v1, p0, Ljxo;->eIS:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 621
    :cond_0
    iget v0, p0, Ljxo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 622
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljxo;->eJL:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 624
    :cond_1
    iget-object v0, p0, Ljxo;->eKV:Ljxn;

    if-eqz v0, :cond_2

    .line 625
    const/4 v0, 0x3

    iget-object v1, p0, Ljxo;->eKV:Ljxn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 627
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 628
    return-void
.end method

.method public final aB([B)Ljxo;
    .locals 1

    .prologue
    .line 563
    if-nez p1, :cond_0

    .line 564
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 566
    :cond_0
    iput-object p1, p0, Ljxo;->eIS:[B

    .line 567
    iget v0, p0, Ljxo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxo;->aez:I

    .line 568
    return-object p0
.end method

.method public final asV()[B
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Ljxo;->eIS:[B

    return-object v0
.end method

.method public final bvB()Z
    .locals 1

    .prologue
    .line 582
    iget-boolean v0, p0, Ljxo;->eJL:Z

    return v0
.end method

.method public final ju(Z)Ljxo;
    .locals 1

    .prologue
    .line 585
    iput-boolean p1, p0, Ljxo;->eJL:Z

    .line 586
    iget v0, p0, Ljxo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxo;->aez:I

    .line 587
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 632
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 633
    iget v1, p0, Ljxo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 634
    const/4 v1, 0x1

    iget-object v2, p0, Ljxo;->eIS:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 637
    :cond_0
    iget v1, p0, Ljxo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 638
    const/4 v1, 0x2

    iget-boolean v2, p0, Ljxo;->eJL:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 641
    :cond_1
    iget-object v1, p0, Ljxo;->eKV:Ljxn;

    if-eqz v1, :cond_2

    .line 642
    const/4 v1, 0x3

    iget-object v2, p0, Ljxo;->eKV:Ljxn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 645
    :cond_2
    return v0
.end method
