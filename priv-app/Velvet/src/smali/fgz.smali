.class public final Lfgz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final crn:[I

.field public static final cro:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfgz;->crn:[I

    .line 58
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lfgz;->cro:[I

    return-void

    .line 41
    nop

    :array_0
    .array-data 4
        0x1
        0x3
        0x5
        0x9
        0xa
    .end array-data

    .line 58
    :array_1
    .array-data 4
        0x65
        0x66
    .end array-data
.end method

.method public static a(Ljel;I)Ljava/lang/Object;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 80
    packed-switch p1, :pswitch_data_0

    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid scalar key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :pswitch_0
    iget-object v1, p0, Ljel;->efF:Ljfn;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljel;->efF:Ljfn;

    invoke-virtual {v1}, Ljfn;->bjy()Z

    move-result v1

    if-nez v1, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-object v0

    .line 86
    :cond_1
    iget-object v0, p0, Ljel;->efF:Ljfn;

    invoke-virtual {v0}, Ljfn;->bjx()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 89
    :pswitch_1
    iget-object v1, p0, Ljel;->efF:Ljfn;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljel;->efF:Ljfn;

    invoke-virtual {v1}, Ljfn;->bjC()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v0, p0, Ljel;->efF:Ljfn;

    invoke-virtual {v0}, Ljfn;->bjB()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 96
    :pswitch_2
    iget-object v1, p0, Ljel;->efF:Ljfn;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljel;->efF:Ljfn;

    invoke-virtual {v1}, Ljfn;->bjA()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    iget-object v0, p0, Ljel;->efF:Ljfn;

    invoke-virtual {v0}, Ljfn;->bjz()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 103
    :pswitch_3
    iget-object v1, p0, Ljel;->efm:Ljex;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljel;->efm:Ljex;

    invoke-virtual {v1}, Ljex;->bji()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    iget-object v0, p0, Ljel;->efm:Ljex;

    invoke-virtual {v0}, Ljex;->bjh()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 110
    :pswitch_4
    invoke-virtual {p0}, Ljel;->bjd()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljel;->TB()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 113
    :pswitch_5
    invoke-virtual {p0}, Ljel;->biV()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljel;->biU()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 116
    :pswitch_6
    invoke-virtual {p0}, Ljel;->biX()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljel;->biW()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 119
    :pswitch_7
    iget-object v1, p0, Ljel;->efc:Ljff;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljel;->efc:Ljff;

    invoke-virtual {v1}, Ljff;->bjm()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    iget-object v0, p0, Ljel;->efc:Ljff;

    invoke-virtual {v0}, Ljff;->bjl()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 126
    :pswitch_8
    invoke-virtual {p0}, Ljel;->bjb()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljel;->bja()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 129
    :pswitch_9
    invoke-virtual {p0}, Ljel;->bjc()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljel;->aCF()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Ljel;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 67
    invoke-static {p0, p1}, Lfgz;->a(Ljel;I)Ljava/lang/Object;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    move-object p2, v0

    :cond_0
    return-object p2
.end method

.method public static a(Ljel;ILjava/util/List;)V
    .locals 3

    .prologue
    .line 221
    packed-switch p1, :pswitch_data_0

    .line 240
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid repeated key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :pswitch_0
    iget-object v0, p0, Ljel;->efG:Ljer;

    if-nez v0, :cond_0

    .line 225
    new-instance v0, Ljer;

    invoke-direct {v0}, Ljer;-><init>()V

    iput-object v0, p0, Ljel;->efG:Ljer;

    .line 227
    :cond_0
    iget-object v1, p0, Ljel;->efG:Ljer;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljes;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljes;

    iput-object v0, v1, Ljer;->efW:[Ljes;

    .line 237
    :goto_0
    return-void

    .line 232
    :pswitch_1
    iget-object v0, p0, Ljel;->efF:Ljfn;

    if-nez v0, :cond_1

    .line 233
    new-instance v0, Ljfn;

    invoke-direct {v0}, Ljfn;-><init>()V

    iput-object v0, p0, Ljel;->efF:Ljfn;

    .line 235
    :cond_1
    iget-object v1, p0, Ljel;->efF:Ljfn;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljfo;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljfo;

    iput-object v0, v1, Ljfn;->eiD:[Ljfo;

    goto :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Ljel;I)Ljava/util/List;
    .locals 3

    .prologue
    .line 187
    packed-switch p1, :pswitch_data_0

    .line 207
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid repeated key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :pswitch_0
    iget-object v0, p0, Ljel;->efm:Ljex;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 204
    :goto_0
    return-object v0

    .line 192
    :cond_0
    iget-object v0, p0, Ljel;->efm:Ljex;

    iget-object v0, v0, Ljex;->egm:[Ljey;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 195
    :pswitch_1
    iget-object v0, p0, Ljel;->efG:Ljer;

    if-nez v0, :cond_1

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 198
    :cond_1
    iget-object v0, p0, Ljel;->efG:Ljer;

    iget-object v0, v0, Ljer;->efW:[Ljes;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 201
    :pswitch_2
    iget-object v0, p0, Ljel;->efF:Ljfn;

    if-nez v0, :cond_2

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 204
    :cond_2
    iget-object v0, p0, Ljel;->efF:Ljfn;

    iget-object v0, v0, Ljfn;->eiD:[Ljfo;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Ljel;ILjava/lang/Object;)V
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 146
    packed-switch p1, :pswitch_data_0

    .line 174
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid scalar key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :pswitch_1
    iget-object v0, p0, Ljel;->efF:Ljfn;

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Ljfn;

    invoke-direct {v0}, Ljfn;-><init>()V

    iput-object v0, p0, Ljel;->efF:Ljfn;

    .line 151
    :cond_0
    iget-object v0, p0, Ljel;->efF:Ljfn;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljfn;->ic(Z)Ljfn;

    .line 171
    :goto_0
    return-void

    .line 155
    :pswitch_2
    iget-object v0, p0, Ljel;->efF:Ljfn;

    if-nez v0, :cond_1

    .line 156
    new-instance v0, Ljfn;

    invoke-direct {v0}, Ljfn;-><init>()V

    iput-object v0, p0, Ljel;->efF:Ljfn;

    .line 158
    :cond_1
    iget-object v0, p0, Ljel;->efF:Ljfn;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljfn;->id(Z)Ljfn;

    goto :goto_0

    .line 162
    :pswitch_3
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Ljel;->pq(I)Ljel;

    goto :goto_0

    .line 166
    :pswitch_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Ljel;->po(I)Ljel;

    goto :goto_0

    .line 170
    :pswitch_5
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Ljel;->pp(I)Ljel;

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
