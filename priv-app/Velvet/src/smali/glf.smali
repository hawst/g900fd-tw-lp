.class public final Lglf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cOD:Lifj;


# instance fields
.field public final cOE:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final cOF:I

.field final cOG:J

.field cOH:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, " "

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    sput-object v0, Lglf;->cOD:Lifj;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lglf;->cOE:Ljava/lang/String;

    .line 32
    iput p2, p0, Lglf;->cOF:I

    .line 33
    iput-wide p3, p0, Lglf;->cOG:J

    .line 34
    return-void
.end method

.method private static a(Ljava/lang/String;DLjava/util/Map;)V
    .locals 1

    .prologue
    .line 60
    invoke-interface {p3, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglj;

    invoke-virtual {v0, p1, p2}, Lglj;->i(D)V

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_0
    new-instance v0, Lglj;

    invoke-direct {v0, p0, p1, p2}, Lglj;-><init>(Ljava/lang/String;D)V

    invoke-interface {p3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final q(Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lglf;->cOE:Ljava/lang/String;

    invoke-static {v0}, Lgle;->nf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {v0}, Lgle;->ng(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iget-wide v2, p0, Lglf;->cOH:D

    invoke-static {v1, v2, v3, p1}, Lglf;->a(Ljava/lang/String;DLjava/util/Map;)V

    .line 53
    array-length v1, v0

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 54
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    iget-wide v2, p0, Lglf;->cOH:D

    invoke-static {v1, v2, v3, p1}, Lglf;->a(Ljava/lang/String;DLjava/util/Map;)V

    .line 55
    sget-object v1, Lglf;->cOD:Lifj;

    invoke-virtual {v1, v0}, Lifj;->b([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lglf;->cOH:D

    invoke-static {v0, v2, v3, p1}, Lglf;->a(Ljava/lang/String;DLjava/util/Map;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GrammarContact["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lglf;->cOE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lglf;->cOF:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",last-time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lglf;->cOG:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",weigth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lglf;->cOH:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
