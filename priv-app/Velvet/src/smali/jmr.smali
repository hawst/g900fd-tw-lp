.class public final Ljmr;
.super Ljsl;
.source "PG"


# static fields
.field public static final eur:Ljsm;


# instance fields
.field private aez:I

.field public etw:[Ljks;

.field public etx:Ljoq;

.field private eus:Z

.field private eut:Z

.field private euu:Z

.field private euv:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11296
    const/16 v0, 0xb

    const-class v1, Ljmr;

    const v2, 0x11355c9a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmr;->eur:Ljsm;

    .line 11302
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 11391
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 11392
    iput v1, p0, Ljmr;->aez:I

    invoke-static {}, Ljks;->boq()[Ljks;

    move-result-object v0

    iput-object v0, p0, Ljmr;->etw:[Ljks;

    iput-object v2, p0, Ljmr;->etx:Ljoq;

    iput-boolean v1, p0, Ljmr;->eus:Z

    iput-boolean v1, p0, Ljmr;->eut:Z

    iput-boolean v1, p0, Ljmr;->euu:Z

    iput-boolean v1, p0, Ljmr;->euv:Z

    iput-object v2, p0, Ljmr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmr;->eCz:I

    .line 11393
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11289
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljmr;->etw:[Ljks;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljks;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljmr;->etw:[Ljks;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljks;

    invoke-direct {v3}, Ljks;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljmr;->etw:[Ljks;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljks;

    invoke-direct {v3}, Ljks;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljmr;->etw:[Ljks;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljmr;->eus:Z

    iget v0, p0, Ljmr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmr;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljmr;->eut:Z

    iget v0, p0, Ljmr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmr;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljmr;->euu:Z

    iget v0, p0, Ljmr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmr;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljmr;->euv:Z

    iget v0, p0, Ljmr;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljmr;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljmr;->etx:Ljoq;

    if-nez v0, :cond_4

    new-instance v0, Ljoq;

    invoke-direct {v0}, Ljoq;-><init>()V

    iput-object v0, p0, Ljmr;->etx:Ljoq;

    :cond_4
    iget-object v0, p0, Ljmr;->etx:Ljoq;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 11411
    iget-object v0, p0, Ljmr;->etw:[Ljks;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljmr;->etw:[Ljks;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 11412
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljmr;->etw:[Ljks;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 11413
    iget-object v1, p0, Ljmr;->etw:[Ljks;

    aget-object v1, v1, v0

    .line 11414
    if-eqz v1, :cond_0

    .line 11415
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 11412
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11419
    :cond_1
    iget v0, p0, Ljmr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 11420
    const/4 v0, 0x2

    iget-boolean v1, p0, Ljmr;->eus:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11422
    :cond_2
    iget v0, p0, Ljmr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 11423
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljmr;->eut:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11425
    :cond_3
    iget v0, p0, Ljmr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 11426
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljmr;->euu:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11428
    :cond_4
    iget v0, p0, Ljmr;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 11429
    const/4 v0, 0x5

    iget-boolean v1, p0, Ljmr;->euv:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 11431
    :cond_5
    iget-object v0, p0, Ljmr;->etx:Ljoq;

    if-eqz v0, :cond_6

    .line 11432
    const/4 v0, 0x6

    iget-object v1, p0, Ljmr;->etx:Ljoq;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 11434
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 11435
    return-void
.end method

.method public final bqr()Z
    .locals 1

    .prologue
    .line 11318
    iget-boolean v0, p0, Ljmr;->eus:Z

    return v0
.end method

.method public final bqs()Z
    .locals 1

    .prologue
    .line 11337
    iget-boolean v0, p0, Ljmr;->eut:Z

    return v0
.end method

.method public final bqt()Z
    .locals 1

    .prologue
    .line 11356
    iget-boolean v0, p0, Ljmr;->euu:Z

    return v0
.end method

.method public final iI(Z)Ljmr;
    .locals 1

    .prologue
    .line 11321
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljmr;->eus:Z

    .line 11322
    iget v0, p0, Ljmr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmr;->aez:I

    .line 11323
    return-object p0
.end method

.method public final iJ(Z)Ljmr;
    .locals 1

    .prologue
    .line 11340
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljmr;->eut:Z

    .line 11341
    iget v0, p0, Ljmr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmr;->aez:I

    .line 11342
    return-object p0
.end method

.method public final iK(Z)Ljmr;
    .locals 1

    .prologue
    .line 11359
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljmr;->euu:Z

    .line 11360
    iget v0, p0, Ljmr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljmr;->aez:I

    .line 11361
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 11439
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 11440
    iget-object v0, p0, Ljmr;->etw:[Ljks;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljmr;->etw:[Ljks;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 11441
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljmr;->etw:[Ljks;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 11442
    iget-object v2, p0, Ljmr;->etw:[Ljks;

    aget-object v2, v2, v0

    .line 11443
    if-eqz v2, :cond_0

    .line 11444
    const/4 v3, 0x1

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 11441
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11449
    :cond_1
    iget v0, p0, Ljmr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 11450
    const/4 v0, 0x2

    iget-boolean v2, p0, Ljmr;->eus:Z

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 11453
    :cond_2
    iget v0, p0, Ljmr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 11454
    const/4 v0, 0x3

    iget-boolean v2, p0, Ljmr;->eut:Z

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 11457
    :cond_3
    iget v0, p0, Ljmr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 11458
    const/4 v0, 0x4

    iget-boolean v2, p0, Ljmr;->euu:Z

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 11461
    :cond_4
    iget v0, p0, Ljmr;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 11462
    const/4 v0, 0x5

    iget-boolean v2, p0, Ljmr;->euv:Z

    invoke-static {v0}, Ljsj;->sc(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 11465
    :cond_5
    iget-object v0, p0, Ljmr;->etx:Ljoq;

    if-eqz v0, :cond_6

    .line 11466
    const/4 v0, 0x6

    iget-object v2, p0, Ljmr;->etx:Ljoq;

    invoke-static {v0, v2}, Ljsj;->c(ILjsr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 11469
    :cond_6
    return v1
.end method
