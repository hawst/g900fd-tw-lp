.class public final Lfve;
.super Lfuz;
.source "PG"


# instance fields
.field private final mStringEvaluator:Lgbr;

.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 49
    iput-object p4, p0, Lfve;->mTimeToLeaveFactory:Lfyk;

    .line 50
    iput-object p5, p0, Lfve;->mStringEvaluator:Lgbr;

    .line 51
    return-void
.end method

.method private static a(Ljbv;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x3

    .line 188
    iget-object v0, p0, Ljbv;->dZq:[Lizt;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 190
    invoke-static {v5}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 191
    const/4 v0, 0x0

    move v6, v0

    move-object v0, v1

    move v1, v6

    :goto_0
    iget-object v3, p0, Ljbv;->dZq:[Lizt;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    if-ge v1, v5, :cond_2

    .line 192
    iget-object v3, p0, Ljbv;->dZq:[Lizt;

    aget-object v3, v3, v1

    .line 193
    invoke-virtual {v3}, Lizt;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 195
    :cond_0
    invoke-virtual {v3}, Lizt;->oj()Ljava/lang/String;

    move-result-object v0

    .line 191
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 199
    :cond_2
    const-string v0, ", "

    invoke-static {v0, v2}, Lgab;->e(Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 203
    :cond_3
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v6, 0x0

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 55
    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    .line 56
    iget-object v8, v3, Lizj;->dTl:Ljbv;

    .line 57
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 60
    iget-object v0, v8, Ljbv;->dMF:[Liyg;

    array-length v0, v0

    if-ne v0, v10, :cond_c

    .line 61
    new-instance v0, Lgca;

    iget-object v1, v8, Ljbv;->dMF:[Liyg;

    aget-object v1, v1, v7

    iget-object v2, p0, Lfuz;->mClock:Lemp;

    invoke-direct {v0, v1, v2}, Lgca;-><init>(Liyg;Lemp;)V

    .line 62
    iget-object v1, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgca;->p(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 64
    new-instance v1, Lfzb;

    iget-object v2, p0, Lfuz;->mClock:Lemp;

    iget-object v4, p0, Lfve;->mStringEvaluator:Lgbr;

    invoke-direct {v1, v2, v0, v4}, Lfzb;-><init>(Lemp;Lgca;Lgbr;)V

    .line 66
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v2, p0, Lfuz;->mCardContainer:Lfmt;

    iget-object v4, v8, Ljbv;->dZr:Ljbp;

    invoke-virtual {v1, p1, v0, v2, v4}, Lfzb;->a(Landroid/content/Context;Lizj;Lfmt;Ljbp;)Lanh;

    move-result-object v0

    .line 68
    invoke-static {v9, v0}, Lfve;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 71
    :goto_0
    iget-object v1, v8, Ljbv;->dMF:[Liyg;

    array-length v1, v1

    if-eqz v1, :cond_0

    iget-object v1, v3, Lizj;->dUv:Ljhk;

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lfve;->mTimeToLeaveFactory:Lfyk;

    iget-object v1, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-interface {v1}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    iget-object v4, v8, Ljbv;->dMF:[Liyg;

    iget-object v5, v8, Ljbv;->dZr:Ljbp;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lfyk;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;)Lanh;

    move-result-object v0

    invoke-static {v9, v0}, Lfve;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 79
    :cond_0
    iget-object v1, v8, Ljbv;->dZq:[Lizt;

    array-length v2, v1

    move v0, v7

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 80
    invoke-virtual {v3}, Lizt;->ok()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lizt;->oj()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 81
    invoke-virtual {v3}, Lizt;->oj()Ljava/lang/String;

    move-result-object v0

    .line 86
    :goto_2
    invoke-virtual {v8}, Ljbv;->bfB()Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x7f0a034a

    new-array v2, v10, [Ljava/lang/Object;

    invoke-virtual {v8}, Ljbv;->agN()J

    move-result-wide v4

    invoke-static {p1, v4, v5, v10}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v8}, Ljbv;->bfB()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v8}, Ljbv;->agN()J

    move-result-wide v2

    const v4, 0x80012

    invoke-static {p1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    :goto_4
    const-string v3, " \u00b7 "

    new-array v4, v11, [Ljava/lang/CharSequence;

    aput-object v1, v4, v7

    aput-object v2, v4, v10

    invoke-static {v3, v4}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfrs;

    invoke-virtual {v8}, Ljbv;->getTitle()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lfuz;->mEntry:Lizj;

    invoke-direct {v2, p1, v3, v4}, Lfrs;-><init>(Landroid/content/Context;Ljava/lang/String;Lizj;)V

    iput-object v1, v2, Lfrs;->ceJ:Ljava/lang/String;

    invoke-static {v8}, Lfve;->a(Ljbv;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lfrs;->cBj:Ljava/lang/String;

    iput-object v0, v2, Lfrs;->bPf:Ljava/lang/String;

    iput-boolean v10, v2, Lfrs;->cBg:Z

    const/16 v0, 0x12

    invoke-virtual {v2, v0}, Lfrs;->jz(I)Lanh;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v0, v8, Ljbv;->dMM:Ljcn;

    if-eqz v0, :cond_1

    iget-object v0, v8, Ljbv;->dMM:Ljcn;

    invoke-virtual {v0}, Ljcn;->qG()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    new-instance v0, Lfrt;

    iget-object v1, v8, Ljbv;->dMM:Ljcn;

    invoke-virtual {v1}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lfrt;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lfrt;->aDh()Lanh;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_1
    iget-object v0, v8, Ljbv;->dZw:[Lizu;

    array-length v0, v0

    if-gtz v0, :cond_7

    move-object v0, v6

    :goto_5
    invoke-static {v9, v0}, Lfve;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 93
    iget-object v0, v8, Ljbv;->dZr:Ljbp;

    if-eqz v0, :cond_2

    iget-object v0, v8, Ljbv;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_2

    new-instance v0, Lfvv;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    const/16 v2, 0xba

    iget-object v3, v8, Ljbv;->dMF:[Liyg;

    aget-object v3, v3, v7

    iget-object v4, v8, Ljbv;->dZr:Ljbp;

    invoke-direct {v0, v1, v2, v3, v4}, Lfvv;-><init>(Lizj;ILiyg;Ljbp;)V

    iput-boolean v7, v0, Lfvv;->cCH:Z

    invoke-virtual {v0}, Lfvv;->aDh()Lanh;

    move-result-object v6

    :cond_2
    invoke-static {v9, v6}, Lfve;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 95
    iget-object v0, v8, Ljbv;->dOz:Ljbg;

    if-eqz v0, :cond_3

    iget-object v0, v8, Ljbv;->dOz:Ljbg;

    invoke-virtual {v0}, Ljbg;->beR()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 96
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v8, Ljbv;->dOz:Ljbg;

    const/16 v2, 0x34

    invoke-static {p1, v0, v1, v2}, Lfsx;->a(Landroid/content/Context;Lizj;Ljbg;I)Lanh;

    move-result-object v0

    invoke-static {v9, v0}, Lfve;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 100
    :cond_3
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 101
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v9, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 102
    invoke-virtual {v1, v10}, Lang;->aS(Z)Lang;

    .line 103
    return-object v1

    .line 79
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_5
    move-object v1, v6

    .line 86
    goto/16 :goto_3

    :cond_6
    move-object v2, v6

    goto/16 :goto_4

    .line 92
    :cond_7
    new-instance v1, Laou;

    invoke-direct {v1}, Laou;-><init>()V

    const v0, 0x7f0a040b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laou;->cs(Ljava/lang/String;)Laou;

    invoke-virtual {v8}, Ljbv;->bfA()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laou;->ct(Ljava/lang/String;)Laou;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v8, Ljbv;->dZw:[Lizu;

    aget-object v0, v0, v7

    new-instance v3, Lfyg;

    invoke-direct {v3, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a045d

    invoke-virtual {v0}, Lizu;->bdA()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v11}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    move-result-object v3

    const v4, 0x7f0a0352

    invoke-virtual {v0}, Lizu;->aZI()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v3

    const v4, 0x7f0a0353

    invoke-virtual {v0}, Lizu;->bdz()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v3

    invoke-virtual {v3}, Lfyg;->aDR()Laow;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lfyg;

    invoke-direct {v3, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a0463

    invoke-virtual {v8}, Ljbv;->baL()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v11}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    move-result-object v3

    const v4, 0x7f0a0354

    invoke-virtual {v0}, Lizu;->bdy()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v3

    const v4, 0x7f0a0351

    invoke-virtual {v8}, Ljbv;->bfD()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {v8}, Ljbv;->bfC()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {v8}, Ljbv;->bfC()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v3, v4, v0}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v0

    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Laow;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laow;

    iput-object v0, v1, Laou;->akm:[Laow;

    invoke-virtual {v8}, Ljbv;->bfG()Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f0a034c

    new-array v2, v10, [Ljava/lang/Object;

    invoke-virtual {v8}, Ljbv;->bfF()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Laou;->cu(Ljava/lang/String;)Laou;

    :cond_8
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    invoke-virtual {v8}, Ljbv;->bfE()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v8}, Ljbv;->baR()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v8}, Ljbv;->baR()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lfsa;

    const/16 v4, 0x33

    invoke-direct {v3, v4}, Lfsa;-><init>(I)V

    const v4, 0x7f0201e1

    invoke-virtual {v3, v4}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    invoke-virtual {v3, v2, v6}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v2

    iput-object v2, v0, Lanh;->ahs:Lani;

    :cond_9
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    iput-object v1, v0, Lanh;->agN:Laou;

    goto/16 :goto_5

    :cond_a
    const-string v0, ""

    goto :goto_6

    :cond_b
    move-object v0, v6

    goto/16 :goto_2

    :cond_c
    move-object v0, v6

    goto/16 :goto_0
.end method
