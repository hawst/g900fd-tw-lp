.class final Lfwg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final cqQ:Ljdw;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Ljdw;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdw;

    iput-object v0, p0, Lfwg;->cqQ:Ljdw;

    .line 341
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lfwg;->mContext:Landroid/content/Context;

    .line 342
    return-void
.end method


# virtual methods
.method public final aDH()Ljava/lang/Integer;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 354
    iget-object v0, p0, Lfwg;->cqQ:Ljdw;

    invoke-virtual {v0}, Ljdw;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 366
    const-string v0, "QpReminderEntryAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported smart action type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lfwg;->cqQ:Ljdw;

    invoke-virtual {v2}, Ljdw;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 356
    :sswitch_0
    const v0, 0x7f0200f7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 358
    :sswitch_1
    const v0, 0x7f02014e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 360
    :sswitch_2
    const v0, 0x7f02012d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 362
    :sswitch_3
    const v0, 0x7f020134

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 364
    :sswitch_4
    const v0, 0x7f0200da

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 354
    nop

    :sswitch_data_0
    .sparse-switch
        0x94 -> :sswitch_0
        0xab -> :sswitch_2
        0xac -> :sswitch_1
        0xad -> :sswitch_4
        0xae -> :sswitch_3
    .end sparse-switch
.end method

.method public final aDI()Ljava/lang/String;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 373
    iget-object v0, p0, Lfwg;->cqQ:Ljdw;

    invoke-virtual {v0}, Ljdw;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 399
    const-string v0, "QpReminderEntryAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported smart action type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lfwg;->cqQ:Ljdw;

    invoke-virtual {v3}, Ljdw;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    move-object v0, v1

    .line 401
    :cond_0
    :goto_1
    return-object v0

    .line 375
    :sswitch_0
    iget-object v0, p0, Lfwg;->cqQ:Ljdw;

    iget-object v0, v0, Ljdw;->dME:Lixn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfwg;->cqQ:Ljdw;

    iget-object v0, v0, Ljdw;->dME:Lixn;

    invoke-virtual {v0}, Lixn;->oK()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lfwg;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0466

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lfwg;->cqQ:Ljdw;

    iget-object v4, v4, Ljdw;->dME:Lixn;

    invoke-virtual {v4}, Lixn;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 381
    :cond_1
    iget-object v0, p0, Lfwg;->mContext:Landroid/content/Context;

    const v1, 0x7f0a090a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 383
    :sswitch_1
    iget-object v0, p0, Lfwg;->mContext:Landroid/content/Context;

    const v1, 0x7f0a090c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 385
    :sswitch_2
    iget-object v0, p0, Lfwg;->mContext:Landroid/content/Context;

    const v1, 0x7f0a090d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 387
    :sswitch_3
    iget-object v0, p0, Lfwg;->mContext:Landroid/content/Context;

    const v1, 0x7f0a090e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 389
    :sswitch_4
    iget-object v0, p0, Lfwg;->cqQ:Ljdw;

    iget-object v0, v0, Ljdw;->ahD:Lixx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfwg;->cqQ:Ljdw;

    iget-object v0, v0, Ljdw;->ahD:Lixx;

    invoke-virtual {v0}, Lixx;->baX()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfwg;->cqQ:Ljdw;

    iget-object v0, v0, Ljdw;->ahD:Lixx;

    invoke-virtual {v0}, Lixx;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 390
    :goto_2
    iget-object v2, p0, Lfwg;->cqQ:Ljdw;

    iget-object v2, v2, Ljdw;->ahD:Lixx;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lfwg;->cqQ:Ljdw;

    iget-object v2, v2, Ljdw;->ahD:Lixx;

    invoke-virtual {v2}, Lixx;->rn()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 392
    iget-object v0, p0, Lfwg;->cqQ:Ljdw;

    iget-object v0, v0, Ljdw;->ahD:Lixx;

    invoke-virtual {v0}, Lixx;->getLabel()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 393
    :cond_2
    if-nez v0, :cond_0

    .line 396
    const-string v0, "QpReminderEntryAdapter"

    const-string v2, "Received a URL Reminder smart action with no label."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2

    .line 373
    nop

    :sswitch_data_0
    .sparse-switch
        0x94 -> :sswitch_0
        0xab -> :sswitch_2
        0xac -> :sswitch_1
        0xad -> :sswitch_4
        0xae -> :sswitch_3
    .end sparse-switch
.end method
