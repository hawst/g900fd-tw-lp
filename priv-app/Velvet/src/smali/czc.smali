.class public final Lczc;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Lefk;


# instance fields
.field private bnE:Lcze;

.field private bnF:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

.field private bnx:Ldjs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 286
    return-void
.end method

.method public static b(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfhs;Ljava/lang/String;)Lczc;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    const-string v1, "WORKER_FRAGMENT_TAG_KEY"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v1, "ADD_STOCK_QUESTION_KEY"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    new-instance v1, Lczc;

    invoke-direct {v1}, Lczc;-><init>()V

    .line 60
    invoke-virtual {v1, v0}, Lczc;->setArguments(Landroid/os/Bundle;)V

    .line 61
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v0}, Lczc;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 62
    return-object v1
.end method


# virtual methods
.method public final synthetic ar(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 36
    check-cast p1, Lczd;

    invoke-virtual {p0}, Lczc;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lfhs;

    iget-object v1, p0, Lczc;->bnF:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    invoke-virtual {v0, p1, v1}, Lfhs;->a(Lczd;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;)V

    invoke-virtual {p0}, Lczc;->dismiss()V

    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 102
    iget-object v0, p0, Lczc;->bnE:Lcze;

    iget-object v0, v0, Lcze;->bnI:Ldjr;

    iget-object v1, p0, Lczc;->bnx:Ldjs;

    invoke-virtual {v1, v0}, Ldjs;->a(Landroid/widget/BaseAdapter;)V

    .line 103
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 67
    invoke-virtual {p0}, Lczc;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 69
    invoke-virtual {p0}, Lczc;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "WORKER_FRAGMENT_TAG_KEY"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcze;

    iput-object v0, p0, Lczc;->bnE:Lcze;

    .line 71
    invoke-virtual {p0}, Lczc;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ADD_STOCK_QUESTION_KEY"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    iput-object v0, p0, Lczc;->bnF:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 73
    new-instance v0, Ldjs;

    invoke-virtual {p0}, Lczc;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f040012

    iget-object v4, p0, Lczc;->bnE:Lcze;

    const v5, 0x7f0a03c8

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Ldjs;-><init>(Landroid/content/Context;ILefk;Ldjw;I)V

    iput-object v0, p0, Lczc;->bnx:Ldjs;

    .line 79
    iget-object v0, p0, Lczc;->bnx:Ldjs;

    const v1, 0x7f0a036e

    invoke-virtual {v0, v1}, Ldjs;->setTitle(I)V

    .line 80
    iget-object v0, p0, Lczc;->bnx:Ldjs;

    invoke-virtual {v0}, Ldjs;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 83
    iget-object v0, p0, Lczc;->bnx:Ldjs;

    return-object v0
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 112
    iget-object v0, p0, Lczc;->bnE:Lcze;

    iget-object v0, v0, Lcze;->bnI:Ldjr;

    invoke-virtual {v0}, Ldjr;->clear()V

    .line 113
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    .line 88
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 91
    invoke-virtual {p0}, Lczc;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 92
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 93
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 94
    const/4 v2, -0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 95
    const/4 v2, -0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 96
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 97
    return-void
.end method
