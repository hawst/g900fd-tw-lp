.class public final Ldtg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bJA:Ldtl;

.field private bKi:Ldte;

.field private bKj:Z

.field public bKk:Ldti;


# direct methods
.method public constructor <init>(Ldte;Ldtl;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Ldtg;->bKi:Ldte;

    .line 35
    iput-object p2, p0, Ldtg;->bJA:Ldtl;

    .line 36
    return-void
.end method

.method private a(Ljava/lang/CharSequence;ZLdtj;)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Ldtg;->bJA:Ldtl;

    if-eqz v0, :cond_0

    .line 317
    sget-object v0, Ldtj;->bKu:Ldtj;

    if-ne p3, v0, :cond_1

    .line 318
    iget-object v0, p0, Ldtg;->bJA:Ldtl;

    invoke-static {p1}, Ldtg;->u(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Ldtl;->a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;)V

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    sget-object v0, Ldtj;->bKx:Ldtj;

    if-ne p3, v0, :cond_0

    .line 321
    iget-object v0, p0, Ldtg;->bJA:Ldtl;

    invoke-static {p1}, Ldtg;->u(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ldtl;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private d(Landroid/text/Editable;)Z
    .locals 1

    .prologue
    .line 203
    invoke-static {p1}, Ldtg;->e(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Landroid/text/Editable;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 209
    if-nez p0, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-object v0

    .line 214
    :cond_1
    invoke-interface {p0}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Ljava/lang/Object;

    invoke-interface {p0, v1, v2, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    .line 215
    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 216
    invoke-interface {p0, v4}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v5

    and-int/lit16 v5, v5, 0x100

    if-eqz v5, :cond_2

    .line 217
    invoke-interface {p0, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {p0, v4}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {p0, v0, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 215
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static u(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 350
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 351
    instance-of v0, p0, Landroid/text/Spanned;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 352
    check-cast v0, Landroid/text/Spanned;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const-class v4, Landroid/text/style/SuggestionSpan;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/SuggestionSpan;

    .line 354
    array-length v3, v0

    if-eqz v3, :cond_1

    .line 355
    const-string v3, " (Suggestions:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 357
    const-string v5, "<"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    const-string v5, ", "

    invoke-virtual {v4}, Landroid/text/style/SuggestionSpan;->getSuggestions()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    const-string v4, ">"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 361
    :cond_0
    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    if-nez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    invoke-direct {p0, p2}, Ldtg;->d(Landroid/text/Editable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    sget-object v1, Ldtk;->bKB:Ldtk;

    iput-object v1, v0, Ldti;->bKn:Ldtk;

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    sget-object v1, Ldtk;->bKA:Ldtk;

    iput-object v1, v0, Ldti;->bKn:Ldtk;

    goto :goto_0
.end method

.method public final afS()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Ldtg;->bKk:Ldti;

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldtg;->ef(Z)V

    .line 43
    return-void
.end method

.method public final b(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    if-nez v0, :cond_0

    .line 156
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    invoke-static {p1}, Ldtg;->e(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    .line 155
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldti;->bKq:Z

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;Landroid/text/Editable;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 97
    iget-object v1, p0, Ldtg;->bKk:Ldti;

    if-nez v1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    invoke-direct {p0, p2}, Ldtg;->d(Landroid/text/Editable;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 104
    iget-object v1, p0, Ldtg;->bKk:Ldti;

    iget-object v1, v1, Ldti;->bKn:Ldtk;

    sget-object v2, Ldtk;->bKz:Ldtk;

    if-ne v1, v2, :cond_2

    .line 106
    iget-object v1, p0, Ldtg;->bKk:Ldti;

    sget-object v2, Ldtk;->bKD:Ldtk;

    iput-object v2, v1, Ldti;->bKn:Ldtk;

    .line 111
    :cond_2
    iget-object v1, p0, Ldtg;->bKk:Ldti;

    iput-object p1, v1, Ldti;->bKr:Ljava/lang/CharSequence;

    .line 112
    iget-object v1, p0, Ldtg;->bKk:Ldti;

    invoke-static {p2}, Ldtg;->e(Landroid/text/Editable;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v0, 0x1

    :cond_3
    iput-boolean v0, v1, Ldti;->bKp:Z

    goto :goto_0

    .line 115
    :cond_4
    if-eqz p1, :cond_0

    instance-of v1, p1, Landroid/text/Spanned;

    if-nez v1, :cond_0

    .line 119
    iget-object v1, p0, Ldtg;->bKk:Ldti;

    iget-boolean v1, v1, Ldti;->bKo:Z

    if-eqz v1, :cond_7

    .line 121
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    sget-object v1, Ldtk;->bKE:Ldtk;

    iput-object v1, v0, Ldti;->bKn:Ldtk;

    .line 123
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iput-object p1, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    .line 138
    :cond_5
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget-object v0, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iput-object p1, v0, Ldti;->bKs:Ljava/lang/CharSequence;

    goto :goto_0

    .line 125
    :cond_6
    add-int/lit8 v0, v0, 0x1

    :cond_7
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 126
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 128
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    sget-object v1, Ldtk;->bKC:Ldtk;

    iput-object v1, v0, Ldti;->bKn:Ldtk;

    .line 130
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iput-object p1, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final c(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget v1, v0, Ldti;->bKm:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Ldti;->bKm:I

    if-eqz v1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    invoke-virtual {v0}, Ldti;->afT()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Ldth;->bKl:[I

    iget-object v1, p0, Ldtg;->bKk:Ldti;

    iget-object v1, v1, Ldti;->bKn:Ldtk;

    invoke-virtual {v1}, Ldtk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 173
    :cond_2
    :goto_1
    invoke-direct {p0, p1}, Ldtg;->d(Landroid/text/Editable;)Z

    move-result v0

    iget-object v1, p0, Ldtg;->bKk:Ldti;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldtg;->bKk:Ldti;

    iget-object v1, v1, Ldti;->bKn:Ldtk;

    sget-object v2, Ldtk;->bKB:Ldtk;

    if-ne v1, v2, :cond_4

    if-eqz v0, :cond_4

    new-instance v0, Ldti;

    invoke-direct {v0}, Ldti;-><init>()V

    iput-object v0, p0, Ldtg;->bKk:Ldti;

    iget-object v0, p0, Ldtg;->bKk:Ldti;

    sget-object v1, Ldtk;->bKB:Ldtk;

    iput-object v1, v0, Ldti;->bKn:Ldtk;

    goto :goto_0

    .line 172
    :pswitch_0
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget-object v0, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    iget-object v1, p0, Ldtg;->bKk:Ldti;

    iget-object v1, v1, Ldti;->bKn:Ldtk;

    invoke-virtual {v1}, Ldtk;->afV()Z

    move-result v1

    iget-object v2, p0, Ldtg;->bKk:Ldti;

    invoke-virtual {v2}, Ldti;->afU()Ldtj;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Ldtg;->a(Ljava/lang/CharSequence;ZLdtj;)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget-object v0, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    iget-object v1, p0, Ldtg;->bKk:Ldti;

    iget-object v1, v1, Ldti;->bKn:Ldtk;

    invoke-virtual {v1}, Ldtk;->afV()Z

    move-result v1

    iget-object v2, p0, Ldtg;->bKk:Ldti;

    invoke-virtual {v2}, Ldti;->afU()Ldtj;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Ldtg;->a(Ljava/lang/CharSequence;ZLdtj;)V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, p1}, Ldtg;->d(Landroid/text/Editable;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget-object v0, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    iget-object v1, p0, Ldtg;->bKk:Ldti;

    iget-object v1, v1, Ldti;->bKn:Ldtk;

    invoke-virtual {v1}, Ldtk;->afV()Z

    move-result v1

    sget-object v2, Ldtj;->bKu:Ldtj;

    invoke-direct {p0, v0, v1, v2}, Ldtg;->a(Ljava/lang/CharSequence;ZLdtj;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget-object v0, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    iget-object v1, p0, Ldtg;->bJA:Ldtl;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldtg;->bJA:Ldtl;

    invoke-static {v0}, Ldtg;->u(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ldtl;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget-boolean v0, v0, Ldti;->bKp:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget-object v0, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    iget-object v1, p0, Ldtg;->bJA:Ldtl;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldtg;->bJA:Ldtl;

    invoke-static {v0}, Ldtg;->u(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ldtl;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget-object v0, v0, Ldti;->bKr:Ljava/lang/CharSequence;

    iget-object v1, p0, Ldtg;->bJA:Ldtl;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldtg;->bJA:Ldtl;

    invoke-interface {v1, v0}, Ldtl;->q(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 173
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Ldtg;->bKk:Ldti;

    goto/16 :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final ee(Z)V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Ldti;

    invoke-direct {v0}, Ldti;-><init>()V

    iput-object v0, p0, Ldtg;->bKk:Ldti;

    .line 60
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iput-boolean p1, v0, Ldti;->bKo:Z

    .line 62
    :cond_0
    iget-object v0, p0, Ldtg;->bKk:Ldti;

    iget v1, v0, Ldti;->bKm:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Ldti;->bKm:I

    .line 63
    return-void
.end method

.method public ef(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Ldtg;->bKj:Z

    if-nez v3, :cond_2

    :goto_1
    and-int/2addr v0, v1

    if-eqz v0, :cond_3

    .line 241
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 234
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 238
    :cond_3
    iget-boolean v0, p0, Ldtg;->bKj:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Ldtg;->bKj:Z

    .line 239
    iget-boolean v0, p0, Ldtg;->bKj:Z

    iget-object v1, p0, Ldtg;->bKi:Ldte;

    invoke-virtual {v1}, Ldte;->afR()Z

    move-result v1

    and-int/2addr v0, v1

    iput-boolean v0, p0, Ldtg;->bKj:Z

    .line 240
    iget-boolean v0, p0, Ldtg;->bKj:Z

    iget-object v1, p0, Ldtg;->bJA:Ldtl;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldtg;->bJA:Ldtl;

    invoke-interface {v1, v0}, Ldtl;->dY(Z)V

    goto :goto_2
.end method
