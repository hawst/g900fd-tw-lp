.class public final Ljuu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dYX:I

.field private eFJ:F

.field private eFK:I

.field private eFL:[Ljuv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1763
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1764
    iput v1, p0, Ljuu;->aez:I

    iput v1, p0, Ljuu;->dYX:I

    const/4 v0, 0x0

    iput v0, p0, Ljuu;->eFJ:F

    iput v1, p0, Ljuu;->eFK:I

    invoke-static {}, Ljuv;->buQ()[Ljuv;

    move-result-object v0

    iput-object v0, p0, Ljuu;->eFL:[Ljuv;

    const/4 v0, 0x0

    iput-object v0, p0, Ljuu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuu;->eCz:I

    .line 1765
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1562
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljuu;->dYX:I

    iget v0, p0, Ljuu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljuu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuu;->eFJ:F

    iget v0, p0, Ljuu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljuu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljuu;->eFK:I

    iget v0, p0, Ljuu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljuu;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljuu;->eFL:[Ljuv;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljuv;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljuu;->eFL:[Ljuv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljuv;

    invoke-direct {v3}, Ljuv;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljuu;->eFL:[Ljuv;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljuv;

    invoke-direct {v3}, Ljuv;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljuu;->eFL:[Ljuv;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 1781
    iget v0, p0, Ljuu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1782
    const/4 v0, 0x1

    iget v1, p0, Ljuu;->dYX:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1784
    :cond_0
    iget v0, p0, Ljuu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1785
    const/4 v0, 0x2

    iget v1, p0, Ljuu;->eFJ:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 1787
    :cond_1
    iget v0, p0, Ljuu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1788
    const/4 v0, 0x3

    iget v1, p0, Ljuu;->eFK:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1790
    :cond_2
    iget-object v0, p0, Ljuu;->eFL:[Ljuv;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljuu;->eFL:[Ljuv;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 1791
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljuu;->eFL:[Ljuv;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 1792
    iget-object v1, p0, Ljuu;->eFL:[Ljuv;

    aget-object v1, v1, v0

    .line 1793
    if-eqz v1, :cond_3

    .line 1794
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 1791
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1798
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1799
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 1803
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1804
    iget v1, p0, Ljuu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1805
    const/4 v1, 0x1

    iget v2, p0, Ljuu;->dYX:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1808
    :cond_0
    iget v1, p0, Ljuu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1809
    const/4 v1, 0x2

    iget v2, p0, Ljuu;->eFJ:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1812
    :cond_1
    iget v1, p0, Ljuu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1813
    const/4 v1, 0x3

    iget v2, p0, Ljuu;->eFK:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1816
    :cond_2
    iget-object v1, p0, Ljuu;->eFL:[Ljuv;

    if-eqz v1, :cond_5

    iget-object v1, p0, Ljuu;->eFL:[Ljuv;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 1817
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljuu;->eFL:[Ljuv;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 1818
    iget-object v2, p0, Ljuu;->eFL:[Ljuv;

    aget-object v2, v2, v0

    .line 1819
    if-eqz v2, :cond_3

    .line 1820
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1817
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1825
    :cond_5
    return v0
.end method
