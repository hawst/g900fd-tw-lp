.class public final Ljnk;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile euS:[Ljnk;


# instance fields
.field private aez:I

.field private erm:Ljava/lang/String;

.field private euT:Ljava/lang/String;

.field private euU:Ljava/lang/String;

.field private euV:Ljava/lang/String;

.field private euW:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 119
    const/4 v0, 0x0

    iput v0, p0, Ljnk;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljnk;->euT:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnk;->euU:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnk;->euV:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljnk;->euW:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljnk;->erm:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljnk;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljnk;->eCz:I

    .line 120
    return-void
.end method

.method public static bqH()[Ljnk;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljnk;->euS:[Ljnk;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljnk;->euS:[Ljnk;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljnk;

    sput-object v0, Ljnk;->euS:[Ljnk;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljnk;->euS:[Ljnk;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljnk;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnk;->euT:Ljava/lang/String;

    iget v0, p0, Ljnk;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljnk;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnk;->euV:Ljava/lang/String;

    iget v0, p0, Ljnk;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljnk;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljnk;->euW:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljnk;->euW:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljnk;->euW:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljnk;->euW:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnk;->euU:Ljava/lang/String;

    iget v0, p0, Ljnk;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljnk;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljnk;->erm:Ljava/lang/String;

    iget v0, p0, Ljnk;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljnk;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 137
    iget v0, p0, Ljnk;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 138
    const/4 v0, 0x1

    iget-object v1, p0, Ljnk;->euT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 140
    :cond_0
    iget v0, p0, Ljnk;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 141
    const/4 v0, 0x2

    iget-object v1, p0, Ljnk;->euV:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 143
    :cond_1
    iget-object v0, p0, Ljnk;->euW:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljnk;->euW:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 144
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljnk;->euW:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 145
    iget-object v1, p0, Ljnk;->euW:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 146
    if-eqz v1, :cond_2

    .line 147
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 144
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_3
    iget v0, p0, Ljnk;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 152
    const/4 v0, 0x4

    iget-object v1, p0, Ljnk;->euU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 154
    :cond_4
    iget v0, p0, Ljnk;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 155
    const/4 v0, 0x5

    iget-object v1, p0, Ljnk;->erm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 157
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 158
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 163
    iget v2, p0, Ljnk;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 164
    const/4 v2, 0x1

    iget-object v3, p0, Ljnk;->euT:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 167
    :cond_0
    iget v2, p0, Ljnk;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    .line 168
    const/4 v2, 0x2

    iget-object v3, p0, Ljnk;->euV:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 171
    :cond_1
    iget-object v2, p0, Ljnk;->euW:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Ljnk;->euW:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 174
    :goto_0
    iget-object v4, p0, Ljnk;->euW:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 175
    iget-object v4, p0, Ljnk;->euW:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 176
    if-eqz v4, :cond_2

    .line 177
    add-int/lit8 v3, v3, 0x1

    .line 178
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 174
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 182
    :cond_3
    add-int/2addr v0, v2

    .line 183
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 185
    :cond_4
    iget v1, p0, Ljnk;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_5

    .line 186
    const/4 v1, 0x4

    iget-object v2, p0, Ljnk;->euU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_5
    iget v1, p0, Ljnk;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 190
    const/4 v1, 0x5

    iget-object v2, p0, Ljnk;->erm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_6
    return v0
.end method
