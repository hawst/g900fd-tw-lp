.class public final Lfxg;
.super Lfuz;
.source "PG"


# instance fields
.field private final cCW:Ljhv;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 46
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dSg:Ljhv;

    iput-object v0, p0, Lfxg;->cCW:Ljhv;

    .line 47
    return-void
.end method

.method private bk(Landroid/content/Context;)Lanh;
    .locals 28

    .prologue
    .line 92
    move-object/from16 v0, p0

    iget-object v2, v0, Lfxg;->cCW:Ljhv;

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2}, Ljhv;->bmg()J

    move-result-wide v16

    iget-object v15, v2, Ljhv;->emt:[Ljhw;

    array-length v0, v15

    move/from16 v18, v0

    const/4 v2, 0x0

    move v13, v2

    :goto_0
    move/from16 v0, v18

    if-ge v13, v0, :cond_8

    aget-object v19, v15, v13

    invoke-virtual/range {v19 .. v19}, Ljhw;->bml()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {v19 .. v19}, Ljhw;->getColor()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    :goto_1
    invoke-virtual/range {v19 .. v19}, Ljhw;->sk()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {v19 .. v19}, Ljhw;->sj()Ljava/lang/String;

    move-result-object v10

    :goto_2
    move-object/from16 v0, v19

    iget-object v0, v0, Ljhw;->emw:[Ljhx;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    const/4 v2, 0x0

    move v12, v2

    :goto_3
    move/from16 v0, v21

    if-ge v12, v0, :cond_7

    aget-object v22, v20, v12

    invoke-virtual/range {v22 .. v22}, Ljhx;->bch()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v22

    iget-object v0, v0, Ljhx;->emy:[Ljhy;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v24, v0

    const/4 v2, 0x0

    move v11, v2

    :goto_4
    move/from16 v0, v24

    if-ge v11, v0, :cond_6

    aget-object v6, v23, v11

    invoke-virtual {v6}, Ljhy;->bmu()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v6}, Ljhy;->bmt()J

    move-result-wide v2

    move-wide v4, v2

    :goto_5
    invoke-virtual {v6}, Ljhy;->bmw()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v6}, Ljhy;->bmv()I

    move-result v6

    :goto_6
    invoke-virtual/range {v19 .. v19}, Ljhw;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {v22 .. v22}, Ljhx;->bmp()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {v22 .. v22}, Ljhx;->bmo()Ljava/lang/String;

    move-result-object v7

    :goto_7
    new-instance v3, Lfxh;

    const-wide/16 v26, 0x3e8

    mul-long v4, v4, v26

    invoke-direct/range {v3 .. v10}, Lfxh;-><init>(JILjava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-interface {v14, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_4

    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    :cond_2
    const/4 v10, 0x0

    goto :goto_2

    :cond_3
    invoke-virtual {v6}, Ljhy;->bms()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v6}, Ljhy;->bmr()I

    move-result v2

    int-to-long v2, v2

    add-long v2, v2, v16

    move-wide v4, v2

    goto :goto_5

    :cond_4
    const/4 v6, 0x0

    goto :goto_6

    :cond_5
    invoke-virtual/range {v19 .. v19}, Ljhw;->getName()Ljava/lang/String;

    move-result-object v7

    goto :goto_7

    :cond_6
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_3

    :cond_7
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto/16 :goto_0

    :cond_8
    invoke-static {v14}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 93
    move-object/from16 v0, p0

    iget-object v2, v0, Lfuz;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x1d4c0

    sub-long v4, v2, v4

    .line 94
    const/4 v2, 0x0

    move v3, v2

    :goto_8
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_9

    .line 96
    invoke-interface {v14, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfxh;

    invoke-virtual {v2}, Lfxh;->aDP()J

    move-result-wide v6

    cmp-long v2, v6, v4

    if-gtz v2, :cond_9

    .line 97
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    .line 100
    :cond_9
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v4, v3, 0x5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-interface {v14, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 102
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 103
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfxh;

    .line 104
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lfxh;->bl(Landroid/content/Context;)Lapd;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 106
    :cond_a
    new-instance v4, Lanh;

    invoke-direct {v4}, Lanh;-><init>()V

    .line 107
    const/16 v2, 0x20

    invoke-virtual {v4, v2}, Lanh;->cm(I)Lanh;

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lfuz;->mEntry:Lizj;

    iput-object v2, v4, Lanh;->ahu:Lizj;

    .line 109
    new-instance v2, Lape;

    invoke-direct {v2}, Lape;-><init>()V

    iput-object v2, v4, Lanh;->agY:Lape;

    .line 110
    iget-object v5, v4, Lanh;->agY:Lape;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lapd;

    invoke-interface {v3, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lapd;

    iput-object v2, v5, Lape;->akX:[Lapd;

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lfxg;->cCW:Ljhv;

    iget-object v2, v2, Ljhv;->emo:Ljbc;

    if-eqz v2, :cond_b

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lfxg;->cCW:Ljhv;

    iget-object v2, v2, Ljhv;->emo:Ljbc;

    invoke-static {v2}, Ledx;->a(Ljbc;)Landroid/net/Uri;

    move-result-object v2

    .line 113
    new-instance v3, Lfsa;

    const/4 v5, 0x3

    invoke-direct {v3, v5}, Lfsa;-><init>(I)V

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v2

    iput-object v2, v4, Lanh;->ahs:Lani;

    .line 116
    :cond_b
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lanh;->aV(Z)Lanh;

    .line 117
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x0

    .line 51
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 52
    new-instance v0, Lftn;

    iget-object v3, p0, Lfxg;->cCW:Ljhv;

    invoke-virtual {v3}, Ljhv;->bmh()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lftn;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lfxg;->cCW:Ljhv;

    invoke-virtual {v3}, Ljhv;->bmj()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lfxg;->cCW:Ljhv;

    invoke-virtual {v3}, Ljhv;->bmi()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lftn;->ceJ:Ljava/lang/String;

    :cond_0
    iget-object v3, p0, Lfxg;->cCW:Ljhv;

    iget-object v3, v3, Ljhv;->emo:Ljbc;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lfxg;->cCW:Ljhv;

    iget-object v3, v3, Ljhv;->emo:Ljbc;

    invoke-static {v3}, Ledx;->a(Ljbc;)Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Lfsa;

    invoke-direct {v4, v5}, Lfsa;-><init>(I)V

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v3

    iget-object v4, p0, Lfuz;->mEntry:Lizj;

    invoke-virtual {v0, v3, v4}, Lftn;->a(Lani;Lizj;)Lftn;

    :cond_1
    invoke-virtual {v0}, Lftn;->aDh()Lanh;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    invoke-direct {p0, p1}, Lfxg;->bk(Landroid/content/Context;)Lanh;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lfxg;->cCW:Ljhv;

    iget-object v0, v0, Ljhv;->emo:Ljbc;

    if-eqz v0, :cond_3

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    invoke-virtual {v0, v5}, Lanh;->cm(I)Lanh;

    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    iput-object v3, v0, Lanh;->ahu:Lizj;

    new-instance v3, Laoj;

    invoke-direct {v3}, Laoj;-><init>()V

    const v4, 0x7f0a01f4

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Laoj;->bS(Ljava/lang/String;)Laoj;

    iput-object v3, v0, Lanh;->agz:Laoj;

    iget-object v3, p0, Lfxg;->cCW:Ljhv;

    iget-object v3, v3, Ljhv;->emo:Ljbc;

    invoke-static {v3}, Ledx;->a(Ljbc;)Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Lfsa;

    const/16 v5, 0x68

    invoke-direct {v4, v5}, Lfsa;-><init>(I)V

    const v5, 0x7f0200ed

    invoke-virtual {v4, v5}, Lfsa;->jB(I)Lfsa;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v1}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 56
    :goto_0
    if-eqz v0, :cond_2

    .line 57
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_2
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 61
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 62
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 63
    return-object v1

    :cond_3
    move-object v0, v1

    .line 55
    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 69
    invoke-direct {p0, p1}, Lfxg;->bk(Landroid/content/Context;)Lanh;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
