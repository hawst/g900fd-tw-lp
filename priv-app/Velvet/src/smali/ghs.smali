.class public final Lghs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private final cJY:Lchk;

.field private final cKr:Lgho;

.field private cKs:Ljava/util/List;

.field private cKt:J

.field private final mSearchSettings:Lcke;


# direct methods
.method public constructor <init>(Lchk;Lcke;Lgho;)V
    .locals 0
    .param p1    # Lchk;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcke;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lgho;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lghs;->cJY:Lchk;

    .line 31
    iput-object p2, p0, Lghs;->mSearchSettings:Lcke;

    .line 32
    iput-object p3, p0, Lghs;->cKr:Lgho;

    .line 33
    return-void
.end method


# virtual methods
.method public final aFL()Ljava/util/List;
    .locals 10

    .prologue
    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 39
    iget-object v1, p0, Lghs;->mSearchSettings:Lcke;

    invoke-interface {v1}, Lcke;->CS()Z

    move-result v1

    .line 41
    iget-object v2, p0, Lghs;->cJY:Lchk;

    invoke-virtual {v2}, Lchk;->FJ()I

    move-result v2

    .line 43
    iget-object v3, p0, Lghs;->cJY:Lchk;

    invoke-virtual {v3}, Lchk;->FK()I

    move-result v3

    .line 46
    if-lez v3, :cond_0

    if-lez v2, :cond_0

    if-nez v1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-object v0

    .line 50
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 51
    iget-wide v6, p0, Lghs;->cKt:J

    sub-long v6, v4, v6

    .line 52
    const-wide/32 v8, 0xea60

    cmp-long v1, v6, v8

    if-lez v1, :cond_2

    .line 53
    const/4 v1, 0x0

    iput-object v1, p0, Lghs;->cKs:Ljava/util/List;

    .line 56
    :cond_2
    iget-object v1, p0, Lghs;->cKs:Ljava/util/List;

    if-nez v1, :cond_3

    .line 58
    mul-int/lit8 v1, v3, 0x3c

    mul-int/lit8 v1, v1, 0x3c

    mul-int/lit16 v1, v1, 0x3e8

    .line 59
    int-to-long v6, v1

    sub-long v6, v4, v6

    .line 60
    iget-object v1, p0, Lghs;->cKr:Lgho;

    invoke-virtual {v1, v6, v7, v2}, Lgho;->i(JI)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lghs;->cKs:Ljava/util/List;

    .line 61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 63
    iput-wide v4, p0, Lghs;->cKt:J

    .line 67
    :cond_3
    iget-object v1, p0, Lghs;->cKs:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lghs;->aFL()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
