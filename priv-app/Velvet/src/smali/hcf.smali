.class public final Lhcf;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private atJ:Landroid/widget/ProgressBar;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lhcf;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lhcf;->atJ:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic b(Lhcf;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 469
    invoke-virtual {p0}, Lhcf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 471
    iget-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 474
    :cond_0
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    .line 475
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DS()Lckg;

    move-result-object v0

    .line 477
    iget-object v2, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Lckg;->a(Landroid/webkit/WebView;)V

    .line 479
    iget-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lhcg;

    invoke-direct {v2, p0}, Lhcg;-><init>(Lhcf;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 487
    iget-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lhch;

    invoke-direct {v2, p0}, Lhch;-><init>(Lhcf;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 495
    if-eqz p1, :cond_1

    const-string v0, "savedWebView"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 496
    iget-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 508
    :goto_0
    new-instance v0, Lhce;

    invoke-direct {v0, v1}, Lhce;-><init>(Landroid/content/Context;)V

    .line 510
    new-instance v2, Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    const v4, 0x1010078

    invoke-direct {v2, v1, v3, v4}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lhcf;->atJ:Landroid/widget/ProgressBar;

    .line 512
    iget-object v1, p0, Lhcf;->atJ:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 514
    iget-object v1, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 517
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    const/16 v3, 0x30

    invoke-direct {v1, v6, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 519
    invoke-virtual {p0}, Lhcf;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d01a8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 521
    iget-object v2, p0, Lhcf;->atJ:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 523
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lhcf;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 524
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 525
    invoke-virtual {p0}, Lhcf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v2, 0x104000a

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lhci;

    invoke-direct {v2, p0}, Lhci;-><init>(Lhcf;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 532
    new-instance v0, Lhcj;

    invoke-direct {v0, p0}, Lhcj;-><init>(Lhcf;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 545
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 498
    :cond_1
    const-string v2, "<div dir=\"%s\">%s</div>"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {}, Leot;->afY()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "rtl"

    :goto_1
    aput-object v0, v3, v7

    const v0, 0x7f0a030a

    new-array v4, v4, [Ljava/lang/Object;

    const v5, 0x7f0a030b

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const v5, 0x7f0a010d

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v1, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 503
    iget-object v2, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    const-string v3, "text/html; charset=utf-8"

    const-string v4, "utf-8"

    invoke-virtual {v2, v0, v3, v4}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 498
    :cond_2
    const-string v0, "ltr"

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 550
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 551
    iget-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 552
    iget-object v0, p0, Lhcf;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 553
    const-string v0, "savedWebView"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 555
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 559
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 560
    invoke-virtual {p0}, Lhcf;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 561
    if-eqz v0, :cond_0

    .line 564
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 567
    :cond_0
    return-void
.end method
