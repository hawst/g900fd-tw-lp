.class public final Lfkt;
.super Lfjz;
.source "PG"


# instance fields
.field private final cuj:Ljhd;


# direct methods
.method public constructor <init>(Lizj;Lfzw;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lfjz;-><init>(Lizj;Lfzw;)V

    .line 21
    iget-object v0, p1, Lizj;->dUg:Ljhd;

    iput-object v0, p0, Lfkt;->cuj:Ljhd;

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 27
    const v0, 0x7f040185

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 28
    const v1, 0x7f1101a9

    iget-object v2, p0, Lfkt;->cuj:Ljhd;

    invoke-virtual {v2}, Ljhd;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 29
    const v1, 0x7f1100bc

    iget-object v2, p0, Lfkt;->cuj:Ljhd;

    invoke-virtual {v2}, Ljhd;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 30
    return-object v0
.end method

.method public final c(Landroid/content/Context;Lfmt;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    invoke-interface {p2}, Lfmt;->wr()Leoj;

    move-result-object v3

    new-array v4, v1, [Landroid/content/Intent;

    iget-object v5, p0, Lfjz;->mEntry:Lizj;

    invoke-virtual {v5}, Lizj;->getType()I

    move-result v0

    const/16 v6, 0x7e

    if-ne v0, v6, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    iget-object v0, v5, Lizj;->dUg:Ljhd;

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v1}, Lifv;->gX(Z)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v6, "com.google.android.sidekick.main.feedback.SurveyActivity"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "survey_lure_entry"

    invoke-static {v5}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    aput-object v0, v4, v2

    invoke-interface {v3, v4}, Leoj;->b([Landroid/content/Intent;)Z

    .line 37
    return-void

    :cond_0
    move v0, v2

    .line 35
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
