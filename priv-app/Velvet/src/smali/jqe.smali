.class public final Ljqe;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eyy:[Ljqe;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private agv:I

.field public eyz:Ljqv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5370
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5371
    const/4 v0, 0x0

    iput v0, p0, Ljqe;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqe;->agq:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljqe;->agv:I

    iput-object v1, p0, Ljqe;->eyz:Ljqv;

    iput-object v1, p0, Ljqe;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqe;->eCz:I

    .line 5372
    return-void
.end method

.method public static bsm()[Ljqe;
    .locals 2

    .prologue
    .line 5313
    sget-object v0, Ljqe;->eyy:[Ljqe;

    if-nez v0, :cond_1

    .line 5314
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 5316
    :try_start_0
    sget-object v0, Ljqe;->eyy:[Ljqe;

    if-nez v0, :cond_0

    .line 5317
    const/4 v0, 0x0

    new-array v0, v0, [Ljqe;

    sput-object v0, Ljqe;->eyy:[Ljqe;

    .line 5319
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5321
    :cond_1
    sget-object v0, Ljqe;->eyy:[Ljqe;

    return-object v0

    .line 5319
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5297
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqe;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqe;->agq:Ljava/lang/String;

    iget v0, p0, Ljqe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqe;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljqe;->agv:I

    iget v0, p0, Ljqe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqe;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljqe;->eyz:Ljqv;

    if-nez v0, :cond_1

    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    iput-object v0, p0, Ljqe;->eyz:Ljqv;

    :cond_1
    iget-object v0, p0, Ljqe;->eyz:Ljqv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 5387
    iget v0, p0, Ljqe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5388
    const/4 v0, 0x1

    iget-object v1, p0, Ljqe;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 5390
    :cond_0
    iget v0, p0, Ljqe;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 5391
    const/4 v0, 0x2

    iget v1, p0, Ljqe;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 5393
    :cond_1
    iget-object v0, p0, Ljqe;->eyz:Ljqv;

    if-eqz v0, :cond_2

    .line 5394
    const/4 v0, 0x3

    iget-object v1, p0, Ljqe;->eyz:Ljqv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5396
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5397
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5329
    iget-object v0, p0, Ljqe;->agq:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 5351
    iget v0, p0, Ljqe;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 5401
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5402
    iget v1, p0, Ljqe;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5403
    const/4 v1, 0x1

    iget-object v2, p0, Ljqe;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5406
    :cond_0
    iget v1, p0, Ljqe;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 5407
    const/4 v1, 0x2

    iget v2, p0, Ljqe;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5410
    :cond_1
    iget-object v1, p0, Ljqe;->eyz:Ljqv;

    if-eqz v1, :cond_2

    .line 5411
    const/4 v1, 0x3

    iget-object v2, p0, Ljqe;->eyz:Ljqv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5414
    :cond_2
    return v0
.end method

.method public final oK()Z
    .locals 1

    .prologue
    .line 5340
    iget v0, p0, Ljqe;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final rp(I)Ljqe;
    .locals 1

    .prologue
    .line 5354
    iput p1, p0, Ljqe;->agv:I

    .line 5355
    iget v0, p0, Ljqe;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqe;->aez:I

    .line 5356
    return-object p0
.end method

.method public final yf(Ljava/lang/String;)Ljqe;
    .locals 1

    .prologue
    .line 5332
    if-nez p1, :cond_0

    .line 5333
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5335
    :cond_0
    iput-object p1, p0, Ljqe;->agq:Ljava/lang/String;

    .line 5336
    iget v0, p0, Ljqe;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqe;->aez:I

    .line 5337
    return-object p0
.end method
