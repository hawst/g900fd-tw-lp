.class public final Lhgg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final dgg:Lhgh;

.field private dgh:Z

.field private dgi:Landroid/view/MotionEvent;

.field private dgj:F

.field private dgk:F

.field private dgl:Z

.field private dgm:Z


# direct methods
.method public constructor <init>(Lhgh;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lhgg;->dgg:Lhgh;

    .line 55
    return-void
.end method


# virtual methods
.method public final bf(II)Z
    .locals 16

    .prologue
    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lhgg;->dgg:Lhgh;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lhgg;->dgi:Landroid/view/MotionEvent;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhgg;->dgm:Z

    if-eqz v2, :cond_1

    .line 61
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhgg;->dgh:Z

    if-nez v2, :cond_0

    .line 62
    move-object/from16 v0, p0

    iget-object v2, v0, Lhgg;->dgg:Lhgh;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhgg;->dgi:Landroid/view/MotionEvent;

    invoke-interface {v2, v3}, Lhgh;->r(Landroid/view/MotionEvent;)V

    .line 63
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lhgg;->dgh:Z

    .line 66
    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lhgg;->dgj:F

    move/from16 v0, p1

    int-to-float v3, v0

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lhgg;->dgj:F

    .line 67
    move-object/from16 v0, p0

    iget v2, v0, Lhgg;->dgk:F

    move/from16 v0, p2

    int-to-float v3, v0

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lhgg;->dgk:F

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lhgg;->dgi:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget v7, v0, Lhgg;->dgj:F

    move-object/from16 v0, p0

    iget v8, v0, Lhgg;->dgk:F

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v2

    .line 75
    move-object/from16 v0, p0

    iget-object v3, v0, Lhgg;->dgg:Lhgh;

    invoke-interface {v3, v2}, Lhgh;->s(Landroid/view/MotionEvent;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lhgg;->dgl:Z

    .line 76
    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 79
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lhgg;->dgl:Z

    return v2
.end method

.method public final v(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 89
    if-nez v0, :cond_1

    .line 90
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lhgg;->dgi:Landroid/view/MotionEvent;

    .line 91
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lhgg;->dgj:F

    .line 92
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lhgg;->dgk:F

    .line 96
    iput-boolean v2, p0, Lhgg;->dgl:Z

    .line 98
    iget-object v0, p0, Lhgg;->dgg:Lhgh;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lhgg;->dgg:Lhgh;

    invoke-interface {v0, p1}, Lhgh;->u(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lhgg;->dgm:Z

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 103
    :cond_2
    iput-boolean v2, p0, Lhgg;->dgm:Z

    .line 106
    iget-boolean v0, p0, Lhgg;->dgh:Z

    if-eqz v0, :cond_0

    .line 107
    iput-boolean v2, p0, Lhgg;->dgh:Z

    .line 108
    iget-object v0, p0, Lhgg;->dgg:Lhgh;

    if-eqz v0, :cond_3

    .line 109
    iget-object v0, p0, Lhgg;->dgg:Lhgh;

    invoke-interface {v0, p1}, Lhgh;->t(Landroid/view/MotionEvent;)V

    .line 112
    :cond_3
    iget-object v0, p0, Lhgg;->dgi:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lhgg;->dgi:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lhgg;->dgi:Landroid/view/MotionEvent;

    goto :goto_0
.end method
