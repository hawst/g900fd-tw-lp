.class final Lkdh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final eTt:Ljava/io/DataInputStream;

.field final eTu:Ljava/util/BitSet;

.field final eTv:Ljava/util/List;

.field final eTw:Ljava/util/List;

.field private eTx:J

.field eTy:J


# direct methods
.method constructor <init>(Ljava/io/DataInputStream;Z)V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lkdh;->eTu:Ljava/util/BitSet;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkdh;->eTw:Ljava/util/List;

    .line 97
    const-wide/16 v0, 0x1000

    iput-wide v0, p0, Lkdh;->eTx:J

    .line 98
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkdh;->eTy:J

    .line 101
    iput-object p1, p0, Lkdh;->eTt:Ljava/io/DataInputStream;

    .line 102
    new-instance v1, Ljava/util/ArrayList;

    if-eqz p2, :cond_0

    sget-object v0, Lkdg;->eTr:Ljava/util/List;

    :goto_0
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lkdh;->eTv:Ljava/util/List;

    .line 105
    return-void

    .line 102
    :cond_0
    sget-object v0, Lkdg;->eTs:Ljava/util/List;

    goto :goto_0
.end method

.method private f(ILjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1000

    const/4 v0, 0x0

    .line 247
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    .line 250
    :goto_0
    iget-wide v2, p0, Lkdh;->eTx:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    cmp-long v2, v2, v6

    if-lez v2, :cond_0

    .line 251
    invoke-direct {p0, v0}, Lkdh;->remove(I)V

    .line 252
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 255
    :cond_0
    int-to-long v2, v1

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    .line 264
    :goto_1
    return-void

    .line 259
    :cond_1
    if-nez p1, :cond_2

    move p1, v0

    .line 261
    :cond_2
    iget-object v0, p0, Lkdh;->eTv:Ljava/util/List;

    mul-int/lit8 v2, p1, 0x2

    invoke-interface {v0, v2, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 262
    iget-object v0, p0, Lkdh;->eTv:Ljava/util/List;

    mul-int/lit8 v2, p1, 0x2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v2, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 263
    iget-wide v2, p0, Lkdh;->eTx:J

    int-to-long v0, v1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lkdh;->eTx:J

    goto :goto_1
.end method

.method private remove(I)V
    .locals 4

    .prologue
    .line 267
    iget-object v0, p0, Lkdh;->eTv:Ljava/util/List;

    mul-int/lit8 v1, p1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 268
    iget-object v1, p0, Lkdh;->eTv:Ljava/util/List;

    mul-int/lit8 v2, p1, 0x2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 270
    iget-wide v2, p0, Lkdh;->eTx:J

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x20

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    int-to-long v0, v0

    sub-long v0, v2, v0

    iput-wide v0, p0, Lkdh;->eTx:J

    .line 271
    return-void
.end method


# virtual methods
.method final bC(II)I
    .locals 3

    .prologue
    .line 279
    and-int v0, p1, p2

    .line 280
    if-ge v0, p2, :cond_0

    .line 296
    :goto_0
    return v0

    .line 286
    :cond_0
    const/4 v0, 0x0

    .line 288
    :goto_1
    invoke-virtual {p0}, Lkdh;->bzl()I

    move-result v1

    .line 289
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_1

    .line 290
    and-int/lit8 v1, v1, 0x7f

    shl-int/2addr v1, v0

    add-int/2addr p2, v1

    .line 291
    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    .line 293
    :cond_1
    shl-int v0, v1, v0

    add-int/2addr v0, p2

    .line 294
    goto :goto_0
.end method

.method bm(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lkdh;->eTv:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lkdh;->f(ILjava/lang/String;Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method public final bzk()V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lkdh;->eTu:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 144
    iget-object v1, p0, Lkdh;->eTw:Ljava/util/List;

    invoke-virtual {p0, v0}, Lkdh;->getName(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v1, p0, Lkdh;->eTw:Ljava/util/List;

    invoke-virtual {p0, v0}, Lkdh;->getValue(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v1, p0, Lkdh;->eTu:Ljava/util/BitSet;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    goto :goto_0

    .line 147
    :cond_0
    return-void
.end method

.method bzl()I
    .locals 4

    .prologue
    .line 274
    iget-wide v0, p0, Lkdh;->eTy:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lkdh;->eTy:J

    .line 275
    iget-object v0, p0, Lkdh;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method e(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0, p1}, Lkdh;->remove(I)V

    .line 241
    invoke-direct {p0, p1, p2, p3}, Lkdh;->f(ILjava/lang/String;Ljava/lang/String;)V

    .line 242
    return-void
.end method

.method getName(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lkdh;->eTv:Ljava/util/List;

    mul-int/lit8 v1, p1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method getValue(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lkdh;->eTv:Ljava/util/List;

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final readString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 305
    invoke-virtual {p0}, Lkdh;->bzl()I

    move-result v0

    .line 306
    const/16 v1, 0xff

    invoke-virtual {p0, v0, v1}, Lkdh;->bC(II)I

    move-result v0

    .line 307
    new-array v1, v0, [B

    .line 308
    iget-wide v2, p0, Lkdh;->eTy:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lkdh;->eTy:J

    .line 309
    iget-object v0, p0, Lkdh;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0, v1}, Ljava/io/DataInputStream;->readFully([B)V

    .line 310
    new-instance v0, Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v0
.end method
