.class public Lbwx;
.super Landroid/app/Activity;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static el(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    packed-switch p0, :pswitch_data_0

    .line 105
    const-string v0, "unknown"

    :goto_0
    return-object v0

    .line 91
    :pswitch_0
    const-string v0, "icon"

    goto :goto_0

    .line 93
    :pswitch_1
    const-string v0, "voice"

    goto :goto_0

    .line 95
    :pswitch_2
    const-string v0, "resume"

    goto :goto_0

    .line 97
    :pswitch_3
    const-string v0, "menu"

    goto :goto_0

    .line 99
    :pswitch_4
    const-string v0, "auto driving"

    goto :goto_0

    .line 101
    :pswitch_5
    const-string v0, "auto navigation"

    goto :goto_0

    .line 103
    :pswitch_6
    const-string v0, "auto bt"

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    new-instance v0, Lbyl;

    invoke-direct {v0}, Lbyl;-><init>()V

    .line 64
    invoke-static {}, Lbyl;->BW()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    invoke-virtual {v0}, Lbyl;->BT()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.search.core.action.ENTER_MANUAL_CAR_MODE"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lbwx;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "entry-point"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 72
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbwx;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    const/4 v0, 0x1

    .line 77
    :cond_0
    const-string v2, "entry-point"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 78
    const-class v0, Lcom/google/android/search/core/service/SearchService;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0, v1}, Lbwx;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 85
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lbwx;->finish()V

    .line 86
    return-void

    .line 81
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lhdv;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    invoke-virtual {p0, v0}, Lbwx;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
