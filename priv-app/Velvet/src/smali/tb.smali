.class public final Ltb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic BF:Lta;


# direct methods
.method public constructor <init>(Lta;)V
    .locals 0

    .prologue
    .line 2234
    iput-object p1, p0, Ltb;->BF:Lta;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 2237
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 2238
    iget-object v0, p0, Ltb;->BF:Lta;

    iget v0, v0, Lta;->mode:I

    if-nez v0, :cond_0

    iget-object v0, p0, Ltb;->BF:Lta;

    iget-boolean v0, v0, Lta;->BD:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 2239
    :goto_0
    iget-object v3, p0, Ltb;->BF:Lta;

    iget v3, v3, Lta;->Bx:F

    mul-float/2addr v3, v0

    sub-float v4, v1, v0

    iget-object v5, p0, Ltb;->BF:Lta;

    iget v5, v5, Lta;->Bz:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 2240
    iget-object v4, p0, Ltb;->BF:Lta;

    iget v4, v4, Lta;->By:F

    mul-float/2addr v4, v0

    sub-float v0, v1, v0

    iget-object v5, p0, Ltb;->BF:Lta;

    iget v5, v5, Lta;->BA:F

    mul-float/2addr v0, v5

    add-float/2addr v0, v4

    .line 2241
    iget-object v4, p0, Ltb;->BF:Lta;

    iget-object v4, v4, Lta;->Bw:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 2242
    iget-object v3, p0, Ltb;->BF:Lta;

    iget-object v3, v3, Lta;->Bw:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 2243
    iget-object v0, p0, Ltb;->BF:Lta;

    iget v0, v0, Lta;->BB:F

    mul-float/2addr v0, v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Ltb;->BF:Lta;

    iget v2, v2, Lta;->BC:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 2244
    iget-object v1, p0, Ltb;->BF:Lta;

    iget-object v1, v1, Lta;->Bw:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 2245
    iget-object v1, p0, Ltb;->BF:Lta;

    iget-object v1, v1, Lta;->Bw:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 2246
    return-void

    :cond_0
    move v0, v2

    .line 2238
    goto :goto_0
.end method
