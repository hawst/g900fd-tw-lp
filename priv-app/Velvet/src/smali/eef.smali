.class public final Leef;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lequ;)Ldtb;
    .locals 1
    .param p0    # Lequ;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 54
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    new-instance v0, Leeg;

    invoke-direct {v0, p0}, Leeg;-><init>(Lequ;)V

    return-object v0
.end method

.method public static aR(Lcom/google/android/shared/search/Query;)Ldto;
    .locals 6

    .prologue
    const/4 v5, -0x2

    .line 45
    new-instance v0, Ldto;

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v1

    const-class v2, Landroid/text/style/SuggestionSpan;

    invoke-virtual {p0, v2}, Lcom/google/android/shared/search/Query;->b(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    const-class v2, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-virtual {p0, v2}, Lcom/google/android/shared/search/Query;->b(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->afX()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v4

    if-eqz v4, :cond_2

    or-int/lit8 v3, v3, 0x4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apF()Z

    move-result v4

    if-eqz v4, :cond_3

    or-int/lit8 v3, v3, 0x2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->getSelectionStart()I

    move-result v4

    if-ne v4, v5, :cond_4

    or-int/lit8 v3, v3, 0x8

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->getSelectionEnd()I

    move-result v4

    if-ne v4, v5, :cond_5

    or-int/lit8 v3, v3, 0x10

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->getSelectionStart()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->getSelectionEnd()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Ldto;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;III)V

    return-object v0

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
