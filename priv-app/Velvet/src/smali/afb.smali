.class public final Lafb;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final UN:Ljava/util/HashSet;


# instance fields
.field private final Oe:Lahh;

.field private final UO:Lafg;

.field private final UP:Lafk;

.field private final UQ:Lafj;

.field private final UR:Lafj;

.field private final US:Lafl;

.field private final UT:Lafl;

.field private final UU:Lafk;

.field private final UV:Lafk;

.field private final UW:Lafh;

.field private final UX:Ljava/util/HashMap;

.field private final UY:Ljava/util/ArrayList;

.field private final UZ:I

.field private Va:I

.field private Vb:I

.field private Vc:Ljava/lang/String;

.field private Vd:Ljava/lang/String;

.field private Ve:Lafi;

.field private final Vf:Laby;

.field private final mContext:Landroid/content/Context;

.field private final xn:Lwi;

.field private yo:Laco;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lafb;->UN:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Lafg;

    invoke-direct {v0, v2}, Lafg;-><init>(B)V

    iput-object v0, p0, Lafb;->UO:Lafg;

    .line 122
    new-instance v0, Lafk;

    invoke-direct {v0, v2}, Lafk;-><init>(B)V

    iput-object v0, p0, Lafb;->UP:Lafk;

    .line 123
    new-instance v0, Lafj;

    invoke-direct {v0, v2}, Lafj;-><init>(B)V

    iput-object v0, p0, Lafb;->UQ:Lafj;

    .line 126
    new-instance v0, Lafj;

    invoke-direct {v0, v2}, Lafj;-><init>(B)V

    iput-object v0, p0, Lafb;->UR:Lafj;

    .line 127
    new-instance v0, Lafl;

    invoke-direct {v0, v2}, Lafl;-><init>(B)V

    iput-object v0, p0, Lafb;->US:Lafl;

    .line 128
    new-instance v0, Lafl;

    invoke-direct {v0, v2}, Lafl;-><init>(B)V

    iput-object v0, p0, Lafb;->UT:Lafl;

    .line 129
    new-instance v0, Lafk;

    invoke-direct {v0, v2}, Lafk;-><init>(B)V

    iput-object v0, p0, Lafb;->UU:Lafk;

    .line 130
    new-instance v0, Lafk;

    invoke-direct {v0, v2}, Lafk;-><init>(B)V

    iput-object v0, p0, Lafb;->UV:Lafk;

    .line 131
    new-instance v0, Lafh;

    invoke-direct {v0, v2}, Lafh;-><init>(B)V

    iput-object v0, p0, Lafb;->UW:Lafh;

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lafb;->UX:Ljava/util/HashMap;

    .line 134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lafb;->UY:Ljava/util/ArrayList;

    .line 151
    new-instance v0, Laby;

    invoke-direct {v0}, Laby;-><init>()V

    iput-object v0, p0, Lafb;->Vf:Laby;

    .line 154
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 155
    iget-object v1, v0, Lyu;->Mk:Lur;

    invoke-virtual {v1}, Lur;->gl()Ltu;

    move-result-object v1

    .line 157
    iput-object p1, p0, Lafb;->mContext:Landroid/content/Context;

    .line 158
    iget v1, v1, Ltu;->DI:I

    iput v1, p0, Lafb;->UZ:I

    .line 159
    iget-object v1, v0, Lyu;->xn:Lwi;

    iput-object v1, p0, Lafb;->xn:Lwi;

    .line 160
    invoke-static {p1}, Lahh;->A(Landroid/content/Context;)Lahh;

    move-result-object v1

    iput-object v1, p0, Lafb;->Oe:Lahh;

    .line 162
    iget-object v0, v0, Lyu;->Mc:Lafi;

    iput-object v0, p0, Lafb;->Ve:Lafi;

    .line 164
    const-string v0, "com.android.launcher3.prefs"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 166
    const-string v1, "android.incremental.version"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 167
    sget-object v2, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    .line 168
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    iget-object v1, p0, Lafb;->Ve:Lafi;

    invoke-virtual {v1}, Lafi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    :try_start_0
    const-string v3, "shortcut_and_widget_previews"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 174
    const-string v1, "android.incremental.version"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 175
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 177
    :cond_0
    return-void

    .line 171
    :catch_0
    move-exception v0

    invoke-static {}, Lafb;->kF()V

    throw v0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 435
    iget-object v0, p0, Lafb;->Vd:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 436
    const-string v0, "name = ? AND size = ?"

    iput-object v0, p0, Lafb;->Vd:Ljava/lang/String;

    .line 439
    :cond_0
    iget-object v0, p0, Lafb;->Ve:Lafi;

    invoke-virtual {v0}, Lafi;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 442
    :try_start_0
    const-string v1, "shortcut_and_widget_previews"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "preview_bitmap"

    aput-object v4, v2, v3

    iget-object v3, p0, Lafb;->Vd:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lafb;->Vc:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 457
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 458
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 459
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 460
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 461
    iget-object v0, p0, Lafb;->UW:Lafh;

    invoke-virtual {v0}, Lafh;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/BitmapFactory$Options;

    .line 462
    iput-object p2, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 463
    iput v11, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 465
    const/4 v2, 0x0

    :try_start_1
    array-length v3, v1

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 472
    :goto_0
    return-object v0

    .line 451
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lafb;->kE()V

    move-object v0, v9

    .line 452
    goto :goto_0

    .line 453
    :catch_1
    move-exception v0

    .line 454
    invoke-static {}, Lafb;->kF()V

    .line 455
    throw v0

    .line 467
    :catch_2
    move-exception v0

    iget-object v0, p0, Lafb;->Ve:Lafi;

    new-instance v1, Lafe;

    invoke-direct {v1, v0, p1}, Lafe;-><init>(Lafi;Ljava/lang/String;)V

    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v11, [Ljava/lang/Void;

    aput-object v9, v2, v10

    invoke-virtual {v1, v0, v2}, Lafe;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-object v0, v9

    .line 468
    goto :goto_0

    .line 471
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    move-object v0, v9

    .line 472
    goto :goto_0
.end method

.method static synthetic a(Lafb;Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 46
    invoke-static {p1}, Lafb;->ad(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lafb;->Ve:Lafi;

    invoke-virtual {v1}, Lafi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "name"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {p2, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    const-string v3, "preview_bitmap"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v0, "size"

    iget-object v3, p0, Lafb;->Vc:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "shortcut_and_widget_previews"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lafb;->kE()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lafb;->kF()V

    throw v0
.end method

.method public static a(Lafi;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 388
    sget-object v1, Lafb;->UN:Ljava/util/HashSet;

    monitor-enter v1

    .line 389
    :try_start_0
    sget-object v0, Lafb;->UN:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 390
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    new-instance v0, Lafd;

    invoke-direct {v0, p0, p1}, Lafd;-><init>(Lafi;Ljava/lang/String;)V

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lafd;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 414
    return-void

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    .locals 4

    .prologue
    .line 683
    if-eqz p1, :cond_0

    .line 684
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 685
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 686
    add-int v2, p2, p4

    add-int v3, p3, p5

    invoke-virtual {p0, p2, p3, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 687
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 688
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 689
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 691
    :cond_0
    return-void
.end method

.method private static ad(Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 327
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 329
    instance-of v0, p0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v0, :cond_0

    .line 330
    const-string v0, "Widget:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    check-cast p0, Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {p0}, Landroid/appwidget/AppWidgetProviderInfo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 333
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 343
    :goto_0
    return-object v0

    .line 335
    :cond_0
    const-string v0, "Shortcut:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    check-cast p0, Landroid/content/pm/ResolveInfo;

    .line 338
    new-instance v0, Landroid/content/ComponentName;

    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v3, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_0
.end method

.method private d(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 695
    :try_start_0
    iget-object v0, p0, Lafb;->Vf:Laby;

    new-instance v1, Laff;

    invoke-direct {v1, p0, p1}, Laff;-><init>(Lafb;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v1}, Laby;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 701
    :catch_0
    move-exception v0

    .line 702
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 703
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 704
    :catch_1
    move-exception v0

    .line 705
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private kE()V
    .locals 1

    .prologue
    .line 180
    invoke-static {}, Lyu;->ij()Lyu;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lyu;->ik()V

    .line 182
    iget-object v0, v0, Lyu;->Mc:Lafi;

    iput-object v0, p0, Lafb;->Ve:Lafi;

    .line 183
    return-void
.end method

.method private static kF()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 716
    :try_start_0
    const-string v0, "WidgetPreviewLoader"

    const-string v1, "DUMP OF OPEN FILES (sample rate: 1 every 23):"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "apk"

    aput-object v3, v0, v1

    const/4 v1, 0x1

    const-string v3, "jar"

    aput-object v3, v0, v1

    const/4 v1, 0x2

    const-string v3, "pipe"

    aput-object v3, v0, v1

    const/4 v1, 0x3

    const-string v3, "socket"

    aput-object v3, v0, v1

    const/4 v1, 0x4

    const-string v3, "db"

    aput-object v3, v0, v1

    const/4 v1, 0x5

    const-string v3, "anon_inode"

    aput-object v3, v0, v1

    const/4 v1, 0x6

    const-string v3, "dev"

    aput-object v3, v0, v1

    const/4 v1, 0x7

    const-string v3, "non-fs"

    aput-object v3, v0, v1

    const/16 v1, 0x8

    const-string v3, "other"

    aput-object v3, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 728
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [I

    .line 729
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v6, v0, [I

    .line 730
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    move v3, v2

    move v1, v2

    .line 732
    :goto_0
    const/16 v0, 0x400

    if-ge v3, v0, :cond_b

    .line 738
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "/proc/self/fd/"

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 742
    :try_start_1
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v8

    .line 743
    const-string v0, "other"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 744
    const-string v9, "/dev/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 745
    const-string v0, "dev"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 764
    :cond_0
    :goto_1
    aget v9, v5, v0

    add-int/lit8 v9, v9, 0x1

    aput v9, v5, v0

    .line 765
    add-int/lit8 v1, v1, 0x1

    .line 766
    invoke-virtual {v7, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 767
    aget v9, v6, v0

    add-int/lit8 v9, v9, 0x1

    aput v9, v6, v0

    .line 769
    :cond_1
    invoke-virtual {v7, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 770
    rem-int/lit8 v9, v1, 0x17

    if-nez v9, :cond_2

    .line 771
    const-string v9, "WidgetPreviewLoader"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, " fd "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " ("

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, ")"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v0, v1

    .line 732
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto/16 :goto_0

    .line 746
    :cond_3
    const-string v9, ".apk"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 747
    const-string v0, "apk"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1

    .line 748
    :cond_4
    const-string v9, ".jar"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 749
    const-string v0, "jar"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1

    .line 750
    :cond_5
    const-string v9, "/fd/pipe:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 751
    const-string v0, "pipe"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto/16 :goto_1

    .line 752
    :cond_6
    const-string v9, "/fd/socket:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 753
    const-string v0, "socket"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto/16 :goto_1

    .line 754
    :cond_7
    const-string v9, "/fd/anon_inode:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 755
    const-string v0, "anon_inode"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto/16 :goto_1

    .line 756
    :cond_8
    const-string v9, ".db"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_9

    const-string v9, "/databases/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 757
    :cond_9
    const-string v0, "db"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto/16 :goto_1

    .line 758
    :cond_a
    const-string v9, "/proc/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "/fd/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 762
    const-string v0, "non-fs"

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    goto/16 :goto_1

    :cond_b
    move v0, v2

    .line 778
    :goto_3
    :try_start_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_c

    .line 779
    const-string v1, "WidgetPreviewLoader"

    const-string v2, "Open %10s files: %4d total, %4d duplicates"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v3, v7

    const/4 v7, 0x1

    aget v8, v5, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v7

    const/4 v7, 0x2

    aget v8, v6, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 778
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 782
    :catch_0
    move-exception v0

    .line 784
    const-string v1, "WidgetPreviewLoader"

    const-string v2, "Unable to log open files."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 786
    :cond_c
    return-void

    :catch_1
    move-exception v0

    move v0, v1

    goto/16 :goto_2
.end method

.method static synthetic kG()V
    .locals 0

    .prologue
    .line 46
    invoke-static {}, Lafb;->kF()V

    return-void
.end method

.method static synthetic kH()Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lafb;->UN:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/appwidget/AppWidgetProviderInfo;IIIILandroid/graphics/Bitmap;[I)Landroid/graphics/Bitmap;
    .locals 14

    .prologue
    .line 511
    if-gez p4, :cond_0

    const p4, 0x7fffffff

    .line 512
    :cond_0
    const/4 v1, 0x0

    .line 515
    iget v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    if-eqz v2, :cond_e

    .line 516
    iget-object v1, p0, Lafb;->Oe:Lahh;

    invoke-virtual {v1, p1}, Lahh;->d(Landroid/appwidget/AppWidgetProviderInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 517
    if-eqz v1, :cond_4

    .line 518
    invoke-direct {p0, v1}, Lafb;->d(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v10, v1

    .line 527
    :goto_0
    const/4 v2, 0x0

    .line 528
    if-eqz v10, :cond_5

    const/4 v1, 0x1

    move v9, v1

    .line 529
    :goto_1
    if-eqz v9, :cond_6

    .line 530
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 531
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    move-object v8, v2

    .line 584
    :goto_2
    const/high16 v1, 0x3f800000    # 1.0f

    .line 585
    if-eqz p7, :cond_1

    .line 586
    const/4 v2, 0x0

    aput v5, p7, v2

    .line 588
    :cond_1
    move/from16 v0, p4

    if-le v5, v0, :cond_2

    .line 589
    move/from16 v0, p4

    int-to-float v1, v0

    int-to-float v2, v5

    div-float/2addr v1, v2

    .line 591
    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_3

    .line 592
    int-to-float v2, v5

    mul-float/2addr v2, v1

    float-to-int v5, v2

    .line 593
    int-to-float v2, v6

    mul-float/2addr v1, v2

    float-to-int v6, v1

    .line 597
    :cond_3
    if-nez p6, :cond_d

    .line 598
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 602
    :goto_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v1, v5

    div-int/lit8 v3, v1, 0x2

    .line 603
    if-eqz v9, :cond_b

    .line 604
    const/4 v4, 0x0

    move-object v1, v10

    invoke-static/range {v1 .. v6}, Lafb;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    .line 623
    :goto_4
    iget-object v1, p0, Lafb;->Oe:Lahh;

    invoke-virtual {v1, p1, v2}, Lahh;->b(Landroid/appwidget/AppWidgetProviderInfo;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1

    .line 520
    :cond_4
    const-string v2, "WidgetPreviewLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can\'t load widget preview drawable 0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for provider: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v10, v1

    goto :goto_0

    .line 528
    :cond_5
    const/4 v1, 0x0

    move v9, v1

    goto :goto_1

    .line 534
    :cond_6
    if-gtz p2, :cond_7

    const/16 p2, 0x1

    .line 535
    :cond_7
    if-gtz p3, :cond_8

    const/16 p3, 0x1

    .line 538
    :cond_8
    iget-object v1, p0, Lafb;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02031d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 540
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v5

    .line 542
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v6

    .line 544
    mul-int v8, v5, p2

    .line 545
    mul-int v7, v6, p3

    .line 547
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v7, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 548
    iget-object v3, p0, Lafb;->UR:Lafj;

    invoke-virtual {v3}, Lafj;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Canvas;

    .line 549
    invoke-virtual {v3, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 550
    iget-object v4, p0, Lafb;->UV:Lafk;

    invoke-virtual {v4}, Lafk;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Paint;

    .line 551
    if-nez v4, :cond_9

    .line 552
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 553
    new-instance v11, Landroid/graphics/BitmapShader;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v12, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v13, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct {v11, v1, v12, v13}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    invoke-virtual {v4, v11}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 555
    iget-object v1, p0, Lafb;->UV:Lafk;

    invoke-virtual {v1, v4}, Lafk;->set(Ljava/lang/Object;)V

    .line 557
    :cond_9
    iget-object v1, p0, Lafb;->UT:Lafl;

    invoke-virtual {v1}, Lafl;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 558
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v1, v11, v12, v8, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 559
    invoke-virtual {v3, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 560
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 563
    iget v1, p0, Lafb;->UZ:I

    int-to-float v1, v1

    const/high16 v3, 0x3e800000    # 0.25f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 564
    invoke-static {v8, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 565
    int-to-float v3, v3

    iget v4, p0, Lafb;->UZ:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    int-to-float v1, v1

    div-float v1, v3, v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v11

    .line 569
    :try_start_0
    iget-object v1, p0, Lafb;->Oe:Lahh;

    iget-object v3, p0, Lafb;->xn:Lwi;

    invoke-virtual {v1, p1, v3}, Lahh;->a(Landroid/appwidget/AppWidgetProviderInfo;Lwi;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 570
    if-eqz v1, :cond_a

    .line 571
    int-to-float v3, v5

    iget v4, p0, Lafb;->UZ:I

    int-to-float v4, v4

    mul-float/2addr v4, v11

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    float-to-int v3, v3

    .line 572
    int-to-float v4, v6

    iget v5, p0, Lafb;->UZ:I

    int-to-float v5, v5

    mul-float/2addr v5, v11

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    .line 573
    invoke-direct {p0, v1}, Lafb;->d(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 574
    iget v5, p0, Lafb;->UZ:I

    int-to-float v5, v5

    mul-float/2addr v5, v11

    float-to-int v5, v5

    iget v6, p0, Lafb;->UZ:I

    int-to-float v6, v6

    mul-float/2addr v6, v11

    float-to-int v6, v6

    invoke-static/range {v1 .. v6}, Lafb;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_a
    move v6, v7

    move v5, v8

    move-object v8, v2

    .line 579
    goto/16 :goto_2

    :catch_0
    move-exception v1

    move v6, v7

    move v5, v8

    move-object v8, v2

    goto/16 :goto_2

    .line 607
    :cond_b
    iget-object v1, p0, Lafb;->UR:Lafj;

    invoke-virtual {v1}, Lafj;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Canvas;

    .line 608
    iget-object v4, p0, Lafb;->US:Lafl;

    invoke-virtual {v4}, Lafl;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Rect;

    .line 609
    iget-object v7, p0, Lafb;->UT:Lafl;

    invoke-virtual {v7}, Lafl;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Rect;

    .line 610
    invoke-virtual {v1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 611
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    invoke-virtual {v4, v9, v10, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    .line 612
    const/4 v9, 0x0

    add-int/2addr v5, v3

    invoke-virtual {v7, v3, v9, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 614
    iget-object v3, p0, Lafb;->UU:Lafk;

    invoke-virtual {v3}, Lafk;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Paint;

    .line 615
    if-nez v3, :cond_c

    .line 616
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 617
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 618
    iget-object v5, p0, Lafb;->UU:Lafk;

    invoke-virtual {v5, v3}, Lafk;->set(Ljava/lang/Object;)V

    .line 620
    :cond_c
    invoke-virtual {v1, v8, v4, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 621
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_4

    :cond_d
    move-object/from16 v2, p6

    goto/16 :goto_3

    :cond_e
    move-object v10, v1

    goto/16 :goto_0
.end method

.method public final a(Landroid/appwidget/AppWidgetProviderInfo;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 491
    iget-object v0, p0, Lafb;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/launcher3/Launcher;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v0

    .line 492
    aget v1, v0, v6

    invoke-virtual {p0, v1}, Lafb;->bL(I)I

    move-result v4

    .line 493
    aget v1, v0, v7

    iget v2, p0, Lafb;->Vb:I

    iget-object v3, p0, Lafb;->yo:Laco;

    invoke-virtual {v3, v1}, Laco;->bF(I)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 494
    aget v2, v0, v6

    aget v3, v0, v7

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v6, p2

    invoke-virtual/range {v0 .. v7}, Lafb;->a(Landroid/appwidget/AppWidgetProviderInfo;IIIILandroid/graphics/Bitmap;[I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final a(IILaco;)V
    .locals 2

    .prologue
    .line 187
    iput p1, p0, Lafb;->Va:I

    .line 188
    iput p2, p0, Lafb;->Vb:I

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lafb;->Vc:Ljava/lang/String;

    .line 190
    iput-object p3, p0, Lafb;->yo:Laco;

    .line 191
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 270
    invoke-static {p1}, Lafb;->ad(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 271
    iget-object v2, p0, Lafb;->UX:Ljava/util/HashMap;

    monitor-enter v2

    .line 272
    :try_start_0
    iget-object v0, p0, Lafb;->UX:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lafb;->UX:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 274
    if-ne v0, p2, :cond_1

    .line 275
    iget-object v3, p0, Lafb;->UX:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iget-object v1, p0, Lafb;->UY:Ljava/util/ArrayList;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 278
    :try_start_1
    iget-object v3, p0, Lafb;->UY:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/ref/SoftReference;

    invoke-direct {v4, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 285
    :cond_0
    :try_start_2
    monitor-exit v2

    return-void

    .line 279
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 285
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    .line 282
    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Bitmap passed in doesn\'t match up"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
.end method

.method public final ac(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    .line 194
    invoke-static {p1}, Lafb;->ad(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 195
    instance-of v0, p1, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 197
    :goto_0
    sget-object v1, Lafb;->UN:Ljava/util/HashSet;

    monitor-enter v1

    .line 198
    :try_start_0
    sget-object v2, Lafb;->UN:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 199
    :goto_1
    if-nez v0, :cond_2

    .line 200
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 265
    :goto_2
    return-object v0

    :cond_0
    move-object v0, p1

    .line 195
    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    .line 198
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 202
    :cond_2
    monitor-exit v1

    .line 203
    iget-object v1, p0, Lafb;->UX:Ljava/util/HashMap;

    monitor-enter v1

    .line 205
    :try_start_1
    iget-object v0, p0, Lafb;->UX:Ljava/util/HashMap;

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 206
    iget-object v0, p0, Lafb;->UX:Ljava/util/HashMap;

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 207
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 208
    if-eqz v0, :cond_3

    .line 209
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 202
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 212
    :cond_3
    monitor-exit v1

    .line 214
    const/4 v1, 0x0

    .line 215
    iget-object v2, p0, Lafb;->UY:Ljava/util/ArrayList;

    monitor-enter v2

    .line 217
    :goto_3
    if-nez v1, :cond_4

    :try_start_2
    iget-object v0, p0, Lafb;->UY:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 218
    iget-object v0, p0, Lafb;->UY:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 219
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget v4, p0, Lafb;->Va:I

    if-ne v3, v4, :cond_12

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget v4, p0, Lafb;->Vb:I

    if-ne v3, v4, :cond_12

    :goto_4
    move-object v1, v0

    .line 224
    goto :goto_3

    .line 225
    :cond_4
    if-eqz v1, :cond_5

    .line 226
    iget-object v0, p0, Lafb;->UR:Lafj;

    invoke-virtual {v0}, Lafj;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Canvas;

    .line 227
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 228
    const/4 v3, 0x0

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 229
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 231
    :cond_5
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 233
    if-nez v1, :cond_11

    .line 234
    iget v0, p0, Lafb;->Va:I

    iget v1, p0, Lafb;->Vb:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v7, v1

    .line 237
    :goto_5
    invoke-direct {p0, v9, v7}, Lafb;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 239
    if-eqz v0, :cond_6

    .line 240
    iget-object v1, p0, Lafb;->UX:Ljava/util/HashMap;

    monitor-enter v1

    .line 241
    :try_start_3
    iget-object v2, p0, Lafb;->UX:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v9, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto/16 :goto_2

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 231
    :catchall_3
    move-exception v0

    monitor-exit v2

    throw v0

    .line 246
    :cond_6
    if-eqz v7, :cond_8

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Lafb;->Va:I

    if-ne v0, v1, :cond_7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v1, p0, Lafb;->Vb:I

    if-eq v0, v1, :cond_8

    :cond_7
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Improperly sized bitmap passed as argument"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    instance-of v0, p1, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v0, :cond_9

    move-object v0, p1

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {p0, v0, v7}, Lafb;->a(Landroid/appwidget/AppWidgetProviderInfo;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 248
    :goto_6
    if-eq v0, v7, :cond_f

    .line 249
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "generatePreview is not recycling the bitmap "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    move-object v0, p1

    .line 246
    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget v8, p0, Lafb;->Va:I

    iget v10, p0, Lafb;->Vb:I

    iget-object v1, p0, Lafb;->UO:Lafg;

    invoke-virtual {v1}, Lafg;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iget-object v2, p0, Lafb;->UQ:Lafj;

    invoke-virtual {v2}, Lafj;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Landroid/graphics/Canvas;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v8, :cond_a

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, v10, :cond_c

    :cond_a
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v10, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lafb;->UO:Lafg;

    invoke-virtual {v2, v1}, Lafg;->set(Ljava/lang/Object;)V

    :goto_7
    iget-object v2, p0, Lafb;->xn:Lwi;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v2, v0}, Lwi;->a(Landroid/content/pm/ActivityInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lafb;->d(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lafb;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0089

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iget-object v2, p0, Lafb;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0d0087

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iget-object v4, p0, Lafb;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0088

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    sub-int v5, v8, v2

    sub-int v4, v5, v4

    move v5, v4

    invoke-static/range {v0 .. v5}, Lafb;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    if-eqz v7, :cond_d

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v8, :cond_b

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, v10, :cond_d

    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Improperly sized bitmap passed as argument"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    invoke-virtual {v6, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v6, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_7

    :cond_d
    if-nez v7, :cond_10

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v10, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v8, v2

    :goto_8
    invoke-virtual {v6, v8}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lafb;->UP:Lafk;

    invoke-virtual {v2}, Lafk;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Paint;

    if-nez v2, :cond_e

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    new-instance v3, Landroid/graphics/ColorMatrix;

    invoke-direct {v3}, Landroid/graphics/ColorMatrix;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v4, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v4, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v3, p0, Lafb;->UP:Lafk;

    invoke-virtual {v3, v2}, Lafk;->set(Ljava/lang/Object;)V

    :cond_e
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v6, v1, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v4, p0, Lafb;->UZ:I

    iget v5, p0, Lafb;->UZ:I

    move-object v1, v8

    invoke-static/range {v0 .. v5}, Lafb;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    move-object v0, v8

    goto/16 :goto_6

    .line 252
    :cond_f
    iget-object v1, p0, Lafb;->UX:Ljava/util/HashMap;

    monitor-enter v1

    .line 253
    :try_start_4
    iget-object v2, p0, Lafb;->UX:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v9, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 258
    new-instance v1, Lafc;

    invoke-direct {v1, p0, p1, v0}, Lafc;-><init>(Lafb;Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Void;

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lafc;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_2

    .line 254
    :catchall_4
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_10
    move-object v8, v7

    goto :goto_8

    :cond_11
    move-object v7, v1

    goto/16 :goto_5

    :cond_12
    move-object v0, v1

    goto/16 :goto_4
.end method

.method public final bL(I)I
    .locals 2

    .prologue
    .line 499
    iget v0, p0, Lafb;->Va:I

    iget-object v1, p0, Lafb;->yo:Laco;

    invoke-virtual {v1, p1}, Laco;->bE(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
