.class public Lebx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldsk;


# instance fields
.field private synthetic bUB:Lebm;


# direct methods
.method public constructor <init>(Lebm;)V
    .locals 0

    .prologue
    .line 956
    iput-object p1, p0, Lebx;->bUB:Lebm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afw()V
    .locals 1

    .prologue
    .line 1021
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUj:Lebe;

    invoke-virtual {v0}, Lebe;->amI()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1022
    iget-object v0, p0, Lebx;->bUB:Lebm;

    invoke-virtual {v0}, Lebm;->amM()V

    .line 1024
    :cond_0
    return-void
.end method

.method public w(IZ)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 960
    iget-object v0, p0, Lebx;->bUB:Lebm;

    invoke-virtual {v0, p1, p2}, Lebm;->v(IZ)V

    .line 961
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v4, v0, Lebm;->bUj:Lebe;

    if-nez p2, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, p1, v0}, Lebe;->z(IZ)V

    .line 962
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUe:Lebc;

    invoke-virtual {v0, p1, p2}, Lebc;->z(IZ)V

    .line 967
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v4, v0, Lebm;->bUf:Lcom/google/android/search/shared/ui/ReverseDrawRestrictedLayout;

    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/search/shared/ui/ReverseDrawRestrictedLayout;->eJ(Z)V

    .line 969
    if-nez p1, :cond_6

    .line 970
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dX(Z)V

    .line 971
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iput-boolean v2, v0, Lebm;->bUt:Z

    .line 973
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUs:Lebj;

    if-eqz v0, :cond_0

    .line 974
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUs:Lebj;

    invoke-interface {v0, v2}, Lebj;->bZ(Z)V

    .line 977
    :cond_0
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUh:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 978
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget v0, v0, Lebm;->bUx:I

    invoke-static {v0}, Ldtd;->gr(I)Z

    move-result v4

    .line 979
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v5, v0, Lebm;->bUk:Ldsm;

    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v6, v0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    if-eqz v4, :cond_4

    move v0, v3

    :goto_2
    invoke-virtual {v5, v6, v0}, Ldsm;->n(Landroid/view/View;I)V

    .line 981
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUk:Ldsm;

    iget-object v5, p0, Lebx;->bUB:Lebm;

    iget-object v5, v5, Lebm;->bUm:Landroid/view/View;

    if-eqz v4, :cond_5

    :goto_3
    invoke-virtual {v0, v5, v1}, Ldsm;->n(Landroid/view/View;I)V

    .line 983
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v0, v2}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setVisibility(I)V

    .line 984
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUm:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1016
    :cond_1
    :goto_4
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iput p1, v0, Lebm;->bUx:I

    .line 1017
    return-void

    :cond_2
    move v0, v2

    .line 961
    goto :goto_0

    :cond_3
    move v0, v2

    .line 967
    goto :goto_1

    :cond_4
    move v0, v1

    .line 979
    goto :goto_2

    :cond_5
    move v1, v3

    .line 981
    goto :goto_3

    .line 987
    :cond_6
    if-ne p1, v1, :cond_9

    .line 989
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-boolean v0, v0, Lebm;->aPF:Z

    if-eqz v0, :cond_8

    .line 990
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dW(Z)V

    .line 991
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iput-boolean v2, v0, Lebm;->bUt:Z

    .line 1001
    :goto_5
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget v0, v0, Lebm;->bUx:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUs:Lebj;

    if-eqz v0, :cond_7

    .line 1002
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUs:Lebj;

    invoke-interface {v0, v1}, Lebj;->bZ(Z)V

    .line 1005
    :cond_7
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUh:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 1006
    invoke-static {p1}, Ldtd;->gr(I)Z

    move-result v2

    .line 1007
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v4, v0, Lebm;->bUl:Ldsm;

    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v5, v0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    if-eqz v2, :cond_a

    move v0, v3

    :goto_6
    invoke-virtual {v4, v5, v0}, Ldsm;->n(Landroid/view/View;I)V

    .line 1009
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUl:Ldsm;

    iget-object v4, p0, Lebx;->bUB:Lebm;

    iget-object v4, v4, Lebm;->bUm:Landroid/view/View;

    if-eqz v2, :cond_b

    :goto_7
    invoke-virtual {v0, v4, v1}, Ldsm;->n(Landroid/view/View;I)V

    .line 1011
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUn:Lcom/google/android/search/searchplate/ClearOrVoiceButton;

    invoke-virtual {v0, v6}, Lcom/google/android/search/searchplate/ClearOrVoiceButton;->setVisibility(I)V

    .line 1012
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUm:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 993
    :cond_8
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iput-boolean v1, v0, Lebm;->bUt:Z

    goto :goto_5

    .line 997
    :cond_9
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    invoke-virtual {v0, v1}, Lcom/google/android/search/searchplate/SearchPlate;->dX(Z)V

    .line 998
    iget-object v0, p0, Lebx;->bUB:Lebm;

    iput-boolean v2, v0, Lebm;->bUt:Z

    goto :goto_5

    :cond_a
    move v0, v1

    .line 1007
    goto :goto_6

    :cond_b
    move v1, v3

    .line 1009
    goto :goto_7
.end method
