.class public final Lhzu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lccz;


# instance fields
.field private final bbo:I

.field private final mHttpHelper:Ldkx;


# direct methods
.method public constructor <init>(Ldkx;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lhzu;->mHttpHelper:Ldkx;

    .line 26
    const/16 v0, 0xe

    iput v0, p0, Lhzu;->bbo:I

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Ljava/net/URL;)Lbzh;
    .locals 4

    .prologue
    .line 37
    const-string v0, "https"

    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SpdyConnectionFactory only supports HTTPS connections"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    new-instance v0, Ldlb;

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldlb;-><init>(Ljava/lang/String;)V

    .line 43
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldlb;->dz(Z)V

    .line 44
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldlb;->setUseCaches(Z)V

    .line 46
    iget-object v1, p0, Lhzu;->mHttpHelper:Ldkx;

    iget v2, p0, Lhzu;->bbo:I

    new-instance v3, Lhzv;

    invoke-direct {v3}, Lhzv;-><init>()V

    invoke-virtual {v1, v0, v2, v3}, Ldkx;->a(Ldlb;ILdlg;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzh;

    .line 49
    return-object v0
.end method

.method public final a(Ljzm;)Lbzh;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/net/URL;

    invoke-virtual {p1}, Ljzm;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lhzu;->a(Ljava/net/URL;)Lbzh;

    move-result-object v0

    return-object v0
.end method
