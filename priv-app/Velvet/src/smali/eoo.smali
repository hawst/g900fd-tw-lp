.class public final Leoo;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/io/InputStream;[BII)I
    .locals 3

    .prologue
    .line 177
    if-gez p3, :cond_0

    .line 178
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "len is negative"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_0
    const/4 v0, 0x0

    .line 181
    :goto_0
    if-ge v0, p3, :cond_1

    .line 182
    add-int v1, p2, v0

    sub-int v2, p3, v0

    invoke-virtual {p0, p1, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 183
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 184
    add-int/2addr v0, v1

    .line 187
    goto :goto_0

    .line 188
    :cond_1
    return v0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 6

    .prologue
    .line 311
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 312
    const-wide/16 v0, 0x0

    .line 314
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 315
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 316
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 319
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 320
    goto :goto_0

    .line 321
    :cond_0
    return-wide v0
.end method

.method private static a(Ljava/io/Closeable;Z)V
    .locals 4
    .param p0    # Ljava/io/Closeable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 136
    if-nez p0, :cond_0

    .line 147
    :goto_0
    return-void

    .line 140
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    if-eqz p1, :cond_1

    .line 143
    const-string v1, "IoUtils"

    const-string v2, "IOException thrown while closing Closeable."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 145
    :cond_1
    throw v0
.end method

.method public static a(Ljava/io/InputStream;[B)V
    .locals 2

    .prologue
    .line 367
    array-length v0, p1

    const/4 v1, 0x0

    invoke-static {p0, p1, v1, v0}, Leoo;->a(Ljava/io/InputStream;[BII)I

    move-result v1

    if-eq v1, v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 368
    :cond_0
    return-void
.end method

.method public static a([BLisv;)V
    .locals 3

    .prologue
    .line 252
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    invoke-interface {p1}, Lisv;->auX()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    .line 256
    :try_start_0
    invoke-virtual {v0, p0}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 257
    const/4 v1, 0x0

    invoke-static {v0, v1}, Leoo;->a(Ljava/io/Closeable;Z)V

    .line 260
    return-void

    .line 259
    :catchall_0
    move-exception v1

    const/4 v2, 0x1

    invoke-static {v0, v2}, Leoo;->a(Ljava/io/Closeable;Z)V

    throw v1
.end method

.method private static a(Lisu;)[B
    .locals 3

    .prologue
    .line 217
    const/4 v1, 0x1

    .line 218
    invoke-interface {p0}, Lisu;->PN()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 220
    :try_start_0
    invoke-static {v0}, Leoo;->j(Ljava/io/InputStream;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 224
    const/4 v2, 0x0

    invoke-static {v0, v2}, Leoo;->a(Ljava/io/Closeable;Z)V

    return-object v1

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Leoo;->a(Ljava/io/Closeable;Z)V

    throw v2
.end method

.method public static b(Landroid/util/JsonReader;)V
    .locals 2
    .param p0    # Landroid/util/JsonReader;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v0}, Leoo;->a(Ljava/io/Closeable;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static e(Ljava/io/File;)[B
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 430
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 431
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 433
    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Leoq;

    invoke-direct {v0, p0}, Leoq;-><init>(Ljava/io/File;)V

    invoke-static {v0}, Leoo;->a(Lisu;)[B

    move-result-object v0

    .line 445
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 430
    goto :goto_0

    .line 436
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v0, v4

    new-array v0, v0, [B

    .line 437
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 440
    :try_start_0
    invoke-static {v3, v0}, Leoo;->a(Ljava/io/InputStream;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 441
    invoke-static {v3, v2}, Leoo;->a(Ljava/io/Closeable;Z)V

    goto :goto_1

    .line 443
    :catchall_0
    move-exception v0

    invoke-static {v3, v1}, Leoo;->a(Ljava/io/Closeable;Z)V

    throw v0
.end method

.method public static i(Ljava/io/InputStream;)V
    .locals 2
    .param p0    # Ljava/io/InputStream;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 70
    const/4 v0, 0x1

    :try_start_0
    invoke-static {p0, v0}, Leoo;->a(Ljava/io/Closeable;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public static j(Ljava/io/InputStream;)[B
    .locals 1

    .prologue
    .line 202
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 203
    invoke-static {p0, v0}, Leoo;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 204
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
