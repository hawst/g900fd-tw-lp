.class public final Ljdb;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agv:I

.field public ebi:[J

.field private ebj:[Lizj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38143
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 38144
    const/4 v0, 0x0

    iput v0, p0, Ljdb;->aez:I

    const/16 v0, 0x30

    iput v0, p0, Ljdb;->agv:I

    sget-object v0, Ljsu;->eCB:[J

    iput-object v0, p0, Ljdb;->ebi:[J

    invoke-static {}, Lizj;->bcP()[Lizj;

    move-result-object v0

    iput-object v0, p0, Ljdb;->ebj:[Lizj;

    const/4 v0, 0x0

    iput-object v0, p0, Ljdb;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljdb;->eCz:I

    .line 38145
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 38099
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljdb;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljdb;->agv:I

    iget v0, p0, Ljdb;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljdb;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdb;->ebi:[J

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljdb;->ebi:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljdb;->ebi:[J

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Ljdb;->ebi:[J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btR()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljdb;->ebi:[J

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljdb;->ebi:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljdb;->ebi:[J

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljdb;->ebi:[J

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljdb;->ebj:[Lizj;

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lizj;

    if-eqz v0, :cond_8

    iget-object v3, p0, Ljdb;->ebj:[Lizj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    new-instance v3, Lizj;

    invoke-direct {v3}, Lizj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    iget-object v0, p0, Ljdb;->ebj:[Lizj;

    array-length v0, v0

    goto :goto_6

    :cond_a
    new-instance v3, Lizj;

    invoke-direct {v3}, Lizj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljdb;->ebj:[Lizj;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 38160
    iget v0, p0, Ljdb;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 38161
    const/4 v0, 0x1

    iget v2, p0, Ljdb;->agv:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 38163
    :cond_0
    iget-object v0, p0, Ljdb;->ebi:[J

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljdb;->ebi:[J

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 38164
    :goto_0
    iget-object v2, p0, Ljdb;->ebi:[J

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 38165
    const/4 v2, 0x2

    iget-object v3, p0, Ljdb;->ebi:[J

    aget-wide v4, v3, v0

    invoke-virtual {p1, v2, v4, v5}, Ljsj;->h(IJ)V

    .line 38164
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38168
    :cond_1
    iget-object v0, p0, Ljdb;->ebj:[Lizj;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljdb;->ebj:[Lizj;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 38169
    :goto_1
    iget-object v0, p0, Ljdb;->ebj:[Lizj;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 38170
    iget-object v0, p0, Ljdb;->ebj:[Lizj;

    aget-object v0, v0, v1

    .line 38171
    if-eqz v0, :cond_2

    .line 38172
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 38169
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 38176
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 38177
    return-void
.end method

.method public final getType()I
    .locals 1

    .prologue
    .line 38121
    iget v0, p0, Ljdb;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 38181
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 38182
    iget v1, p0, Ljdb;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 38183
    const/4 v1, 0x1

    iget v3, p0, Ljdb;->agv:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 38186
    :cond_0
    iget-object v1, p0, Ljdb;->ebi:[J

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljdb;->ebi:[J

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v2

    move v3, v2

    .line 38188
    :goto_0
    iget-object v4, p0, Ljdb;->ebi:[J

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 38189
    iget-object v4, p0, Ljdb;->ebi:[J

    aget-wide v4, v4, v1

    .line 38190
    invoke-static {v4, v5}, Ljsj;->dJ(J)I

    move-result v4

    add-int/2addr v3, v4

    .line 38188
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38193
    :cond_1
    add-int/2addr v0, v3

    .line 38194
    iget-object v1, p0, Ljdb;->ebi:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 38196
    :cond_2
    iget-object v1, p0, Ljdb;->ebj:[Lizj;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljdb;->ebj:[Lizj;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 38197
    :goto_1
    iget-object v1, p0, Ljdb;->ebj:[Lizj;

    array-length v1, v1

    if-ge v2, v1, :cond_4

    .line 38198
    iget-object v1, p0, Ljdb;->ebj:[Lizj;

    aget-object v1, v1, v2

    .line 38199
    if-eqz v1, :cond_3

    .line 38200
    const/4 v3, 0x3

    invoke-static {v3, v1}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38197
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 38205
    :cond_4
    return v0
.end method
