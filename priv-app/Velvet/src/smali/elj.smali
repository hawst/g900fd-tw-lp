.class public final Lelj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static ceI:Lelj;


# instance fields
.field public final ceD:I

.field public final ceE:I

.field public final ceF:I

.field public final ceG:I

.field public final ceH:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 19
    new-instance v0, Lelj;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lelj;-><init>(IIIII)V

    sput-object v0, Lelj;->ceI:Lelj;

    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lelj;->ceD:I

    .line 24
    iput p2, p0, Lelj;->ceE:I

    .line 25
    iput p3, p0, Lelj;->ceF:I

    .line 26
    iput p4, p0, Lelj;->ceG:I

    .line 27
    iput p5, p0, Lelj;->ceH:I

    .line 28
    return-void
.end method


# virtual methods
.method public final aZ(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 40
    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 41
    new-array v0, v5, [I

    iget v4, p0, Lelj;->ceD:I

    aput v4, v0, v2

    aput v3, v0, v1

    invoke-static {v0}, Leln;->o([I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 42
    new-array v0, v5, [I

    iget v4, p0, Lelj;->ceE:I

    aput v4, v0, v2

    aput v3, v0, v1

    invoke-static {v0}, Leln;->o([I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 43
    new-array v0, v5, [I

    iget v4, p0, Lelj;->ceF:I

    aput v4, v0, v2

    aput v3, v0, v1

    invoke-static {v0}, Leln;->o([I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 44
    new-array v0, v5, [I

    iget v4, p0, Lelj;->ceG:I

    aput v4, v0, v2

    aput v3, v0, v1

    invoke-static {v0}, Leln;->o([I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 45
    new-array v0, v5, [I

    iget v4, p0, Lelj;->ceH:I

    aput v4, v0, v2

    aput v3, v0, v1

    invoke-static {v0}, Leln;->o([I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setNextFocusForwardId(I)V

    .line 46
    return-void

    :cond_0
    move v0, v2

    .line 40
    goto :goto_0
.end method
