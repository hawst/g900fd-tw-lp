.class final Leuh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private synthetic cjf:Leue;

.field private synthetic cjg:Lcom/google/android/gms/location/reporting/UploadRequest;


# direct methods
.method constructor <init>(Leue;Lcom/google/android/gms/location/reporting/UploadRequest;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Leuh;->cjf:Leue;

    iput-object p2, p0, Leuh;->cjg:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private avY()Ljava/lang/Integer;
    .locals 8

    .prologue
    .line 170
    iget-object v0, p0, Leuh;->cjf:Leue;

    iget-object v1, v0, Leue;->cje:Ljava/util/Map;

    monitor-enter v1

    .line 171
    :try_start_0
    iget-object v0, p0, Leuh;->cjf:Leue;

    iget-object v2, p0, Leuh;->cjg:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/location/reporting/UploadRequest;->AD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Leue;->lA(Ljava/lang/String;)I

    .line 173
    iget-object v0, p0, Leuh;->cjf:Leue;

    iget-object v0, v0, Lcgl;->aWd:Lbgo;

    check-cast v0, Lbse;

    iget-object v2, p0, Leuh;->cjg:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v0, v2}, Lbse;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;

    move-result-object v0

    .line 176
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/UploadRequestResult;->getResultCode()I

    move-result v2

    if-nez v2, :cond_0

    .line 177
    iget-object v2, p0, Leuh;->cjf:Leue;

    iget-object v2, v2, Leue;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    iget-object v4, p0, Leuh;->cjg:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/location/reporting/UploadRequest;->AA()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 179
    new-instance v4, Leuj;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/UploadRequestResult;->AF()J

    move-result-wide v6

    invoke-direct {v4, v2, v3, v6, v7}, Leuj;-><init>(JJ)V

    .line 181
    iget-object v2, p0, Leuh;->cjf:Leue;

    iget-object v2, v2, Leue;->cje:Ljava/util/Map;

    iget-object v3, p0, Leuh;->cjg:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/location/reporting/UploadRequest;->AD()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/UploadRequestResult;->getResultCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Leuh;->avY()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
