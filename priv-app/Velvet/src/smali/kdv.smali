.class final Lkdv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdc;


# instance fields
.field private final eTC:Z

.field private final eTT:Lkdo;

.field private final eTt:Ljava/io/DataInputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;Z)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    .line 106
    new-instance v0, Lkdo;

    invoke-direct {v0, p1}, Lkdo;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lkdv;->eTT:Lkdo;

    .line 107
    iput-boolean p2, p0, Lkdv;->eTC:Z

    .line 108
    return-void
.end method

.method private b(Lkdd;II)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 261
    iget-object v2, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 262
    mul-int/lit8 v2, v3, 0x8

    add-int/lit8 v2, v2, 0x4

    if-eq p3, v2, :cond_0

    .line 263
    const-string v2, "TYPE_SETTINGS length: %d != 4 + 8 * %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v2, v4}, Lkdv;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 265
    :cond_0
    new-instance v4, Lkdt;

    invoke-direct {v4}, Lkdt;-><init>()V

    move v2, v1

    .line 266
    :goto_0
    if-ge v2, v3, :cond_1

    .line 267
    iget-object v5, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v5}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    .line 268
    iget-object v6, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v6

    .line 269
    const/high16 v7, -0x1000000

    and-int/2addr v7, v5

    ushr-int/lit8 v7, v7, 0x18

    .line 270
    const v8, 0xffffff

    and-int/2addr v5, v8

    .line 271
    invoke-virtual {v4, v5, v7, v6}, Lkdt;->set(III)V

    .line 266
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 273
    :cond_1
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_2

    .line 274
    :goto_1
    invoke-interface {p1, v0, v4}, Lkdd;->a(ZLkdt;)V

    .line 275
    return-void

    :cond_2
    move v0, v1

    .line 273
    goto :goto_1
.end method

.method private static varargs f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 2

    .prologue
    .line 278
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Lkdd;)Z
    .locals 10

    .prologue
    const/4 v4, -0x1

    const/16 v9, 0x8

    const v8, 0x7fffffff

    const/4 v1, 0x0

    const/4 v7, 0x1

    .line 120
    :try_start_0
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 124
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 126
    const/high16 v0, -0x80000000

    and-int/2addr v0, v2

    if-eqz v0, :cond_1

    move v0, v7

    .line 127
    :goto_0
    const/high16 v5, -0x1000000

    and-int/2addr v5, v3

    ushr-int/lit8 v6, v5, 0x18

    .line 128
    const v5, 0xffffff

    and-int/2addr v5, v3

    .line 130
    if-eqz v0, :cond_e

    .line 131
    const/high16 v0, 0x7fff0000

    and-int/2addr v0, v2

    ushr-int/lit8 v0, v0, 0x10

    .line 132
    const v3, 0xffff

    and-int/2addr v2, v3

    .line 134
    const/4 v3, 0x3

    if-eq v0, v3, :cond_2

    .line 135
    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "version != 3: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 122
    :catch_0
    move-exception v0

    move v7, v1

    .line 187
    :cond_0
    :goto_1
    return v7

    :cond_1
    move v0, v1

    .line 126
    goto :goto_0

    .line 138
    :cond_2
    packed-switch v2, :pswitch_data_0

    .line 181
    :pswitch_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexpected frame"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :pswitch_1
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    iget-object v2, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    iget-object v2, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readShort()S

    move-result v2

    and-int v3, v0, v8

    const v0, 0xe000

    and-int/2addr v0, v2

    ushr-int/lit8 v4, v0, 0xd

    iget-object v0, p0, Lkdv;->eTT:Lkdo;

    add-int/lit8 v2, v5, -0xa

    invoke-virtual {v0, v2}, Lkdo;->tQ(I)Ljava/util/List;

    move-result-object v5

    and-int/lit8 v0, v6, 0x1

    if-eqz v0, :cond_4

    move v2, v7

    :goto_2
    and-int/lit8 v0, v6, 0x2

    if-eqz v0, :cond_3

    move v1, v7

    :cond_3
    sget-object v6, Lkdf;->eTm:Lkdf;

    move-object v0, p1

    invoke-interface/range {v0 .. v6}, Lkdd;->a(ZZIILjava/util/List;Lkdf;)V

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_2

    .line 144
    :pswitch_2
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    and-int v3, v0, v8

    iget-object v0, p0, Lkdv;->eTT:Lkdo;

    add-int/lit8 v2, v5, -0x4

    invoke-virtual {v0, v2}, Lkdo;->tQ(I)Ljava/util/List;

    move-result-object v5

    and-int/lit8 v0, v6, 0x1

    if-eqz v0, :cond_5

    move v2, v7

    :goto_3
    sget-object v6, Lkdf;->eTn:Lkdf;

    move-object v0, p1

    invoke-interface/range {v0 .. v6}, Lkdd;->a(ZZIILjava/util/List;Lkdf;)V

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_3

    .line 148
    :pswitch_3
    if-eq v5, v9, :cond_6

    const-string v0, "TYPE_RST_STREAM length: %d != 8"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lkdv;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_6
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    and-int/2addr v0, v8

    iget-object v2, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    invoke-static {v2}, Lkdb;->tM(I)Lkdb;

    move-result-object v3

    if-nez v3, :cond_7

    const-string v0, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Lkdv;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_7
    invoke-interface {p1, v0, v3}, Lkdd;->a(ILkdb;)V

    goto/16 :goto_1

    .line 152
    :pswitch_4
    invoke-direct {p0, p1, v6, v5}, Lkdv;->b(Lkdd;II)V

    goto/16 :goto_1

    .line 156
    :pswitch_5
    if-eqz v5, :cond_0

    const-string v0, "TYPE_NOOP length: %d != 0"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lkdv;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 161
    :pswitch_6
    const/4 v0, 0x4

    if-eq v5, v0, :cond_8

    const-string v0, "TYPE_PING length: %d != 4"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lkdv;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_8
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    iget-boolean v3, p0, Lkdv;->eTC:Z

    rem-int/lit8 v0, v2, 0x2

    if-ne v0, v7, :cond_9

    move v0, v7

    :goto_4
    if-ne v3, v0, :cond_a

    move v0, v7

    :goto_5
    invoke-interface {p1, v0, v2, v1}, Lkdd;->c(ZII)V

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_5

    .line 165
    :pswitch_7
    if-eq v5, v9, :cond_b

    const-string v0, "TYPE_GOAWAY length: %d != 8"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lkdv;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_b
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    and-int/2addr v0, v8

    iget-object v2, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    invoke-static {v2}, Lkdb;->tO(I)Lkdb;

    move-result-object v3

    if-nez v3, :cond_c

    const-string v0, "TYPE_GOAWAY unexpected error code: %d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Lkdv;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_c
    invoke-interface {p1, v0}, Lkdd;->tP(I)V

    goto/16 :goto_1

    .line 169
    :pswitch_8
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    and-int v3, v0, v8

    iget-object v0, p0, Lkdv;->eTT:Lkdo;

    add-int/lit8 v2, v5, -0x4

    invoke-virtual {v0, v2}, Lkdo;->tQ(I)Ljava/util/List;

    move-result-object v5

    sget-object v6, Lkdf;->eTo:Lkdf;

    move-object v0, p1

    move v2, v1

    invoke-interface/range {v0 .. v6}, Lkdd;->a(ZZIILjava/util/List;Lkdf;)V

    goto/16 :goto_1

    .line 173
    :pswitch_9
    if-eq v5, v9, :cond_d

    const-string v0, "TYPE_WINDOW_UPDATE length: %d != 8"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lkdv;->f(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_d
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    iget-object v1, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    and-int/2addr v0, v8

    and-int/2addr v1, v8

    invoke-interface {p1, v0, v1}, Lkdd;->bA(II)V

    goto/16 :goto_1

    .line 177
    :pswitch_a
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    int-to-long v2, v5

    invoke-static {v0, v2, v3}, Lkbt;->b(Ljava/io/InputStream;J)J

    .line 178
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TODO"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_e
    and-int v0, v2, v8

    .line 185
    and-int/lit8 v2, v6, 0x1

    if-eqz v2, :cond_f

    move v1, v7

    .line 186
    :cond_f
    iget-object v2, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    invoke-interface {p1, v1, v0, v2, v5}, Lkdd;->a(ZILjava/io/InputStream;I)V

    goto/16 :goto_1

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lkdv;->eTt:Ljava/io/DataInputStream;

    iget-object v1, p0, Lkdv;->eTT:Lkdo;

    invoke-static {v0, v1}, Lkbt;->a(Ljava/io/Closeable;Ljava/io/Closeable;)V

    .line 283
    return-void
.end method
