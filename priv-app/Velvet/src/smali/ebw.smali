.class final Lebw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldsj;


# instance fields
.field private synthetic bUB:Lebm;


# direct methods
.method constructor <init>(Lebm;)V
    .locals 0

    .prologue
    .line 1234
    iput-object p1, p0, Lebw;->bUB:Lebm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;IZ)V
    .locals 2

    .prologue
    .line 1270
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->getSelectionStart()I

    move-result v0

    if-eq v0, p2, :cond_1

    .line 1272
    :cond_0
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    iget-object v1, p0, Lebw;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/shared/search/Query;->b(Ljava/lang/CharSequence;I)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apB()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lebq;->z(Lcom/google/android/shared/search/Query;)V

    .line 1276
    :cond_1
    return-void
.end method

.method public final afc()V
    .locals 1

    .prologue
    .line 1237
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1238
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->ann:Lecq;

    invoke-virtual {v0}, Lecq;->stopListening()V

    .line 1240
    :cond_0
    return-void
.end method

.method public final afd()V
    .locals 2

    .prologue
    .line 1244
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget v0, v0, Lebm;->bUx:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget v0, v0, Lebm;->bUx:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1245
    :cond_0
    invoke-virtual {p0}, Lebw;->afc()V

    .line 1247
    :cond_1
    iget-object v0, p0, Lebw;->bUB:Lebm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lebm;->bW(Z)Z

    .line 1248
    return-void
.end method

.method public final afe()V
    .locals 2

    .prologue
    .line 1252
    iget-object v0, p0, Lebw;->bUB:Lebm;

    const-string v1, "android-search-app"

    invoke-virtual {v0, v1}, Lebm;->ke(Ljava/lang/String;)V

    .line 1253
    return-void
.end method

.method public final afl()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1259
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-boolean v0, v0, Lebm;->bUv:Z

    if-nez v0, :cond_0

    .line 1260
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apc()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1261
    iget-object v1, p0, Lebw;->bUB:Lebm;

    invoke-virtual {v1, v0}, Lebm;->aP(Lcom/google/android/shared/search/Query;)V

    .line 1262
    iget-object v1, p0, Lebw;->bUB:Lebm;

    invoke-virtual {v1, v0}, Lebm;->aO(Lcom/google/android/shared/search/Query;)V

    .line 1263
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bIS:Lcom/google/android/search/searchplate/SearchPlate;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/android/search/searchplate/SearchPlate;->c(IIZ)V

    .line 1265
    :cond_0
    return-void
.end method

.method public final afm()V
    .locals 4

    .prologue
    .line 1282
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget v0, v0, Lebm;->bUx:I

    if-nez v0, :cond_0

    .line 1283
    iget-object v0, p0, Lebw;->bUB:Lebm;

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    const/4 v2, 0x1

    const-string v3, "android-launcher-search"

    invoke-virtual {v0, v1, v2, v3}, Lebm;->a(Lcom/google/android/shared/search/Query;ZLjava/lang/String;)V

    .line 1285
    :cond_0
    return-void
.end method

.method public final afn()V
    .locals 1

    .prologue
    .line 1290
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_0

    .line 1291
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    invoke-virtual {v0}, Ledu;->anU()Ledu;

    .line 1293
    :cond_0
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bTL:Lecd;

    if-eqz v0, :cond_1

    .line 1294
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bTL:Lecd;

    invoke-virtual {v0}, Lecd;->amO()V

    .line 1296
    :cond_1
    return-void
.end method

.method public final afo()V
    .locals 2

    .prologue
    .line 1309
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v1, p0, Lebw;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lebm;->aO(Lcom/google/android/shared/search/Query;)V

    .line 1310
    return-void
.end method

.method public final afp()V
    .locals 3

    .prologue
    .line 1324
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUo:Lebq;

    iget-object v1, p0, Lebw;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apB()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lebq;->z(Lcom/google/android/shared/search/Query;)V

    .line 1325
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_0

    .line 1326
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    iget-object v1, p0, Lebw;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ledu;->aQ(Lcom/google/android/shared/search/Query;)Ledu;

    .line 1328
    :cond_0
    return-void
.end method

.method public final afq()V
    .locals 4

    .prologue
    .line 1314
    iget-object v0, p0, Lebw;->bUB:Lebm;

    sget-object v1, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    const/4 v2, 0x1

    const-string v3, "android-launcher-search"

    invoke-virtual {v0, v1, v2, v3}, Lebm;->a(Lcom/google/android/shared/search/Query;ZLjava/lang/String;)V

    .line 1315
    return-void
.end method

.method public final afr()V
    .locals 1

    .prologue
    .line 1333
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_0

    .line 1334
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    invoke-virtual {v0}, Ledu;->anW()Ledu;

    .line 1336
    :cond_0
    return-void
.end method

.method public final afs()V
    .locals 1

    .prologue
    .line 1348
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_0

    .line 1349
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    invoke-virtual {v0}, Ledu;->anX()Ledu;

    .line 1351
    :cond_0
    return-void
.end method

.method public final aft()V
    .locals 1

    .prologue
    .line 1355
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_0

    .line 1356
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    invoke-virtual {v0}, Ledu;->anY()Ledu;

    .line 1358
    :cond_0
    return-void
.end method

.method public final afu()V
    .locals 2

    .prologue
    .line 1372
    iget-object v0, p0, Lebw;->bUB:Lebm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lebm;->bW(Z)Z

    .line 1373
    return-void
.end method

.method public final afv()V
    .locals 1

    .prologue
    .line 1377
    iget-object v0, p0, Lebw;->bUB:Lebm;

    invoke-virtual {v0}, Lebm;->Cq()V

    .line 1378
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;II)V
    .locals 0

    .prologue
    .line 1320
    return-void
.end method

.method public final dY(Z)V
    .locals 1

    .prologue
    .line 1362
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_0

    .line 1363
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    iput-boolean p1, v0, Ledu;->bWp:Z

    .line 1365
    :cond_0
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bTL:Lecd;

    if-eqz v0, :cond_1

    .line 1366
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bTL:Lecd;

    invoke-virtual {v0}, Lecd;->amO()V

    .line 1368
    :cond_1
    return-void
.end method

.method public final dZ(Z)V
    .locals 1

    .prologue
    .line 1341
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    if-eqz v0, :cond_0

    .line 1342
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v0, v0, Lebm;->bUp:Ledu;

    invoke-virtual {v0, p1}, Ledu;->eN(Z)Ledu;

    .line 1344
    :cond_0
    return-void
.end method

.method public final gm(I)Z
    .locals 2

    .prologue
    .line 1300
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 1301
    iget-object v0, p0, Lebw;->bUB:Lebm;

    iget-object v1, p0, Lebw;->bUB:Lebm;

    iget-object v1, v1, Lebm;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lebm;->aO(Lcom/google/android/shared/search/Query;)V

    .line 1302
    const/4 v0, 0x1

    .line 1304
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
