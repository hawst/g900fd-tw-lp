.class public final Lfkz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leko;
.implements Lfpp;


# instance fields
.field private final asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

.field public cuA:Lfkd;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cuB:Lflc;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field cuC:Landroid/util/SparseArray;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field cuD:Z

.field cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cuF:Landroid/view/View;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cuz:Lflb;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mCardContainer:Lfmt;

.field private final mScrollView:Lekf;


# direct methods
.method public constructor <init>(Lfmt;Lekf;Lcom/google/android/shared/ui/SuggestionGridLayout;Lflb;)V
    .locals 0
    .param p4    # Lflb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lfkz;->mCardContainer:Lfmt;

    .line 79
    iput-object p2, p0, Lfkz;->mScrollView:Lekf;

    .line 80
    iput-object p3, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    .line 81
    iput-object p4, p0, Lfkz;->cuz:Lflb;

    .line 82
    return-void
.end method


# virtual methods
.method public final Cr()V
    .locals 0

    .prologue
    .line 327
    return-void
.end method

.method public final Cs()V
    .locals 0

    .prologue
    .line 332
    return-void
.end method

.method public final Y(Ljava/util/List;)Z
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 122
    if-eqz p1, :cond_1

    .line 123
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lfkz;->aAG()Z

    .line 135
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 127
    :cond_0
    invoke-virtual {p0, p1}, Lfkz;->Z(Ljava/util/List;)V

    goto :goto_0

    .line 130
    :cond_1
    const-string v0, "CardSettingsController"

    const-string v1, "Failed to load training questions"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 131
    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0516

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 133
    invoke-virtual {p0}, Lfkz;->aAG()Z

    goto :goto_0
.end method

.method Z(Ljava/util/List;)V
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const v6, 0x7f11000a

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 139
    if-nez p1, :cond_0

    iget-object v0, p0, Lfkz;->cuC:Landroid/util/SparseArray;

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 140
    iget-object v0, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    if-nez v0, :cond_3

    :goto_1
    invoke-static {v2}, Lifv;->gY(Z)V

    move v2, v1

    .line 141
    :goto_2
    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_8

    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v4, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    iget-object v5, p0, Lfkz;->cuA:Lfkd;

    if-ne v0, v5, :cond_4

    move-object v2, v4

    .line 142
    :goto_3
    if-nez v2, :cond_5

    move-object v0, v3

    :goto_4
    iput-object v0, p0, Lfkz;->cuF:Landroid/view/View;

    .line 143
    iget-object v0, p0, Lfkz;->cuF:Landroid/view/View;

    if-nez v0, :cond_6

    .line 146
    const-string v0, "CardSettingsController"

    const-string v2, "Card and/or menu button disappeared, not showing card settings"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 147
    invoke-virtual {p0}, Lfkz;->aAG()Z

    .line 170
    :cond_1
    :goto_5
    return-void

    :cond_2
    move v0, v1

    .line 139
    goto :goto_0

    :cond_3
    move v2, v1

    .line 140
    goto :goto_1

    .line 141
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 142
    :cond_5
    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aL(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_4

    .line 151
    :cond_6
    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 152
    const v4, 0x7f040039

    iget-object v5, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v4, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/training/CardSettingsView;

    iput-object v0, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    .line 154
    iget-object v0, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    iget-object v4, p0, Lfkz;->mCardContainer:Lfmt;

    iget-object v5, p0, Lfkz;->cuA:Lfkd;

    new-instance v6, Lfqh;

    iget-object v7, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v7}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lfqh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4, v5, v6, p0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->a(Lfmt;Lfkd;Lfqh;Lfpp;)V

    .line 156
    if-nez p1, :cond_7

    .line 157
    iget-object v0, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    iget-object v4, p0, Lfkz;->cuC:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 158
    iput-object v3, p0, Lfkz;->cuC:Landroid/util/SparseArray;

    .line 162
    :goto_6
    iget-object v0, p0, Lfkz;->cuF:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget-object v3, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    iget-object v4, p0, Lfkz;->mScrollView:Lekf;

    iget-boolean v5, p0, Lfkz;->cuD:Z

    invoke-virtual {v0, v3, v2, v4, v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Lekf;Z)V

    .line 164
    iput-boolean v1, p0, Lfkz;->cuD:Z

    .line 165
    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Leko;)V

    .line 167
    iget-object v0, p0, Lfkz;->cuz:Lflb;

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lfkz;->cuz:Lflb;

    invoke-interface {v0}, Lflb;->wo()V

    goto :goto_5

    .line 160
    :cond_7
    iget-object v0, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    invoke-virtual {v0, p1}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->ab(Ljava/util/List;)V

    goto :goto_6

    :cond_8
    move-object v2, v3

    goto/16 :goto_3
.end method

.method public final a(Livq;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 288
    iget-object v0, p0, Lfkz;->cuF:Landroid/view/View;

    .line 289
    new-instance v1, Lfla;

    const-string v2, "Make menu button visible"

    invoke-direct {v1, p0, v2, v0}, Lfla;-><init>(Lfkz;Ljava/lang/String;Landroid/view/View;)V

    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 304
    iget-object v0, p0, Lfkz;->mCardContainer:Lfmt;

    iget-object v1, p0, Lfkz;->cuA:Lfkd;

    invoke-interface {v1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    const/16 v2, 0x75

    invoke-interface {v0, v1, v2, v3, v3}, Lfmt;->a(Lizj;ILixx;Ljava/lang/Integer;)V

    .line 306
    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->b(Leko;)V

    .line 307
    iget-object v0, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->onClose()V

    .line 308
    iget-object v0, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->aCG()Lesk;

    move-result-object v0

    .line 309
    iput-object v3, p0, Lfkz;->cuA:Lfkd;

    .line 310
    iput-object v3, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    .line 311
    iput-object v3, p0, Lfkz;->cuF:Landroid/view/View;

    .line 314
    if-eqz v0, :cond_0

    .line 315
    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Livq;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 318
    :cond_0
    iget-object v0, p0, Lfkz;->cuz:Lflb;

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lfkz;->cuz:Lflb;

    invoke-interface {v0}, Lflb;->wp()V

    .line 322
    :cond_1
    return-void
.end method

.method public aAF()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 114
    new-instance v0, Lflc;

    invoke-direct {v0, p0}, Lflc;-><init>(Lfkz;)V

    iput-object v0, p0, Lfkz;->cuB:Lflc;

    .line 115
    iget-object v0, p0, Lfkz;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v1

    iget-object v0, p0, Lfkz;->cuA:Lfkd;

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    iget-object v0, v0, Lizj;->dTp:[Ljdj;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lfkz;->cuB:Lflc;

    new-instance v0, Lfmn;

    const-string v2, "resolveTrainingQuestions"

    iget-object v3, v1, Lfml;->mTaskRunner:Lerk;

    new-array v4, v7, [I

    invoke-direct/range {v0 .. v6}, Lfmn;-><init>(Lfml;Ljava/lang/String;Lerk;[ILjava/util/Collection;Lemy;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lfmn;->a([Ljava/lang/Object;)Lenp;

    .line 118
    return-void
.end method

.method public final aAG()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 178
    iget-object v2, p0, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    if-eqz v2, :cond_0

    .line 180
    iget-object v1, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aud()V

    .line 192
    :goto_0
    return v0

    .line 182
    :cond_0
    iget-object v2, p0, Lfkz;->cuA:Lfkd;

    if-eqz v2, :cond_2

    .line 183
    iput-object v3, p0, Lfkz;->cuA:Lfkd;

    .line 184
    iget-object v2, p0, Lfkz;->cuB:Lflc;

    if-eqz v2, :cond_1

    .line 185
    iget-object v2, p0, Lfkz;->cuB:Lflc;

    invoke-virtual {v2}, Lflc;->invalidate()V

    .line 186
    iput-object v3, p0, Lfkz;->cuB:Lflc;

    .line 188
    :cond_1
    iput-object v3, p0, Lfkz;->cuC:Landroid/util/SparseArray;

    .line 189
    iput-boolean v1, p0, Lfkz;->cuD:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 192
    goto :goto_0
.end method

.method public final aAH()Z
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lfkz;->cuA:Lfkd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aAI()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lfkz;->asE:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aud()V

    .line 283
    return-void
.end method
