.class public final Lcum;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final bjr:Lcum;

.field public static final bjs:Lcum;

.field public static final bjt:Lcum;

.field public static final bju:Lcum;

.field public static final bjv:Lcum;

.field static final bjw:Lijp;

.field private static final bjx:Lijp;


# instance fields
.field private final bjy:Lcuo;

.field private final bjz:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 22
    new-instance v7, Lcum;

    sget-object v8, Lcuo;->bjM:Lcuo;

    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    const v1, 0x7f0a0668

    const v2, 0x7f0a0669

    const v3, 0x7f020116

    const-string v4, "android.intent.action.MAIN"

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v7, v8, v0}, Lcum;-><init>(Lcuo;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    sput-object v7, Lcum;->bjr:Lcum;

    .line 31
    new-instance v7, Lcum;

    sget-object v8, Lcuo;->bjN:Lcuo;

    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    const v1, 0x7f0a066b

    const v2, 0x7f0a066c

    const v3, 0x7f020117

    const-string v4, "android.intent.action.VIEW"

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v7, v8, v0}, Lcum;-><init>(Lcuo;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    sput-object v7, Lcum;->bjs:Lcum;

    .line 41
    new-instance v0, Lcum;

    sget-object v1, Lcuo;->bjO:Lcuo;

    invoke-direct {v0, v1, v5}, Lcum;-><init>(Lcuo;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    sput-object v0, Lcum;->bjt:Lcum;

    .line 44
    new-instance v0, Lcum;

    sget-object v1, Lcuo;->bjP:Lcuo;

    invoke-direct {v0, v1, v5}, Lcum;-><init>(Lcuo;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    sput-object v0, Lcum;->bju:Lcum;

    .line 47
    new-instance v0, Lcum;

    sget-object v1, Lcuo;->bjQ:Lcuo;

    invoke-direct {v0, v1, v5}, Lcum;-><init>(Lcuo;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    sput-object v0, Lcum;->bjv:Lcum;

    .line 50
    sget-object v0, Lcum;->bjr:Lcum;

    sget-object v1, Lcum;->bjs:Lcum;

    sget-object v2, Lcum;->bjt:Lcum;

    sget-object v3, Lcum;->bju:Lcum;

    sget-object v4, Lcum;->bjv:Lcum;

    invoke-static {v0, v1, v2, v3, v4}, Lijp;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcum;->bjw:Lijp;

    .line 58
    sget-object v0, Lcum;->bjr:Lcum;

    sget-object v1, Lcum;->bjs:Lcum;

    invoke-static {v0, v1}, Lijp;->v(Ljava/lang/Object;Ljava/lang/Object;)Lijp;

    move-result-object v0

    sput-object v0, Lcum;->bjx:Lijp;

    return-void
.end method

.method public constructor <init>(Lcuo;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lcum;->bjy:Lcuo;

    .line 120
    iput-object p2, p0, Lcum;->bjz:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    .line 121
    return-void
.end method

.method private static a([Ljava/lang/String;Z)Lijp;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 93
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_2

    .line 95
    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lcum;->bjw:Lijp;

    .line 110
    :goto_0
    return-object v0

    .line 95
    :cond_1
    sget-object v0, Lcum;->bjx:Lijp;

    goto :goto_0

    .line 99
    :cond_2
    invoke-static {}, Lijp;->aXc()Lijr;

    move-result-object v4

    .line 100
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 101
    sget-object v0, Lcum;->bjw:Lijp;

    invoke-virtual {v0}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcum;

    .line 102
    iget-object v1, v0, Lcum;->bjz:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    if-eqz v1, :cond_5

    move v1, v2

    :goto_2
    if-nez v1, :cond_4

    if-eqz p1, :cond_3

    .line 103
    :cond_4
    iget-object v1, v0, Lcum;->bjy:Lcuo;

    invoke-virtual {v1}, Lcuo;->xF()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v2

    .line 105
    :goto_3
    if-eqz v1, :cond_3

    .line 106
    invoke-virtual {v4, v0}, Lijr;->bv(Ljava/lang/Object;)Lijr;

    goto :goto_1

    :cond_5
    move v1, v3

    .line 102
    goto :goto_2

    :cond_6
    move v1, v3

    .line 103
    goto :goto_3

    .line 110
    :cond_7
    invoke-virtual {v4}, Lijr;->aXd()Lijp;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcjs;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcjs;->MM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcjs;->MN()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcum;->a([Ljava/lang/String;Z)Lijp;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method static b(Lcjs;)Z
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Lcjs;->MN()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcum;->a([Ljava/lang/String;Z)Lijp;

    move-result-object v0

    sget-object v1, Lcum;->bjr:Lcum;

    invoke-virtual {v0, v1}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static c(Lcjs;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 75
    invoke-virtual {p0}, Lcjs;->MN()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcum;->a([Ljava/lang/String;Z)Lijp;

    move-result-object v1

    .line 77
    sget-object v2, Lcum;->bjs:Lcum;

    invoke-virtual {v1, v2}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcum;->bjt:Lcum;

    invoke-virtual {v1, v2}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcum;->bju:Lcum;

    invoke-virtual {v1, v2}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcum;->bjv:Lcum;

    invoke-virtual {v1, v2}, Lijp;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j([Ljava/lang/String;)Lijp;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcum;->a([Ljava/lang/String;Z)Lijp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final SY()Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcum;->bjz:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    return-object v0
.end method

.method public final SZ()Lbem;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcum;->bjy:Lcuo;

    invoke-virtual {v0}, Lcuo;->SZ()Lbem;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "InternalCorpus["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcum;->bjy:Lcuo;

    invoke-virtual {v1}, Lcuo;->xF()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final xF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcum;->bjy:Lcuo;

    invoke-virtual {v0}, Lcuo;->xF()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
