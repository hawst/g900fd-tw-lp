.class public final Lfxw;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 29
    return-void
.end method

.method private a(Landroid/view/View;IIILaoq;I)V
    .locals 2

    .prologue
    .line 136
    invoke-virtual {p5}, Laoq;->re()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lfxw;->c(Landroid/view/View;ILjava/lang/String;)V

    .line 137
    invoke-virtual {p5}, Laoq;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p3, v0}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 138
    invoke-virtual {p5}, Laoq;->pt()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    invoke-virtual {p5}, Laoq;->pu()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0, p4}, Lfxw;->jy(I)V

    .line 142
    :cond_0
    invoke-virtual {p5}, Laoq;->ps()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p4, v0}, Lfxw;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 149
    :cond_1
    :goto_0
    return-void

    .line 143
    :cond_2
    if-eqz p6, :cond_1

    .line 144
    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 145
    invoke-virtual {v0, p6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 146
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 147
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static c(Landroid/view/View;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    invoke-static {p0, p1, p2}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 132
    :cond_0
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 17

    .prologue
    .line 58
    move-object/from16 v0, p0

    iget-object v2, v0, Lfro;->mView:Landroid/view/View;

    .line 59
    move-object/from16 v0, p0

    iget-object v1, v0, Lfro;->cBc:Lanh;

    .line 60
    iget-object v15, v1, Lanh;->agK:Laop;

    .line 61
    move-object/from16 v0, p0

    iget-object v1, v0, Lfro;->cBc:Lanh;

    invoke-virtual {v1}, Lanh;->getType()I

    move-result v16

    .line 64
    const v1, 0x7f11007f

    invoke-virtual {v15}, Laop;->qn()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lfxw;->c(Landroid/view/View;ILjava/lang/String;)V

    .line 65
    const v1, 0x7f1102f8

    invoke-virtual {v15}, Laop;->pn()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    move-result-object v1

    .line 67
    if-eqz v1, :cond_0

    invoke-virtual {v15}, Laop;->po()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 68
    const/16 v3, 0x1a

    move/from16 v0, v16

    if-ne v0, v3, :cond_5

    .line 70
    invoke-virtual {v15}, Laop;->getHighlightColor()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 77
    :cond_0
    :goto_0
    const/4 v7, 0x0

    .line 78
    invoke-virtual {v15}, Laop;->rd()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    invoke-virtual {v15}, Laop;->rc()I

    move-result v7

    .line 82
    :cond_1
    iget-object v6, v15, Laop;->ajP:Laoq;

    .line 83
    const v3, 0x7f11032f

    const v4, 0x7f11032c

    const v5, 0x7f110328

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lfxw;->a(Landroid/view/View;IIILaoq;I)V

    .line 86
    iget-object v13, v15, Laop;->ajQ:Laoq;

    .line 87
    const v10, 0x7f110332

    const v11, 0x7f11032d

    const v12, 0x7f11032b

    move-object/from16 v8, p0

    move-object v9, v2

    move v14, v7

    invoke-direct/range {v8 .. v14}, Lfxw;->a(Landroid/view/View;IIILaoq;I)V

    .line 91
    invoke-virtual {v6}, Laoq;->rf()Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-virtual {v13}, Laoq;->rf()Ljava/lang/String;

    move-result-object v3

    .line 93
    const/16 v4, 0x19

    move/from16 v0, v16

    if-eq v0, v4, :cond_2

    const/16 v4, 0x1f

    move/from16 v0, v16

    if-ne v0, v4, :cond_7

    .line 96
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 97
    :cond_3
    const v1, 0x7f110329

    move-object/from16 v0, p0

    iget-object v3, v0, Lfxw;->mContext:Landroid/content/Context;

    const v4, 0x7f0a034e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 109
    :goto_1
    const v1, 0x7f11032a

    invoke-virtual {v15}, Laop;->qX()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lfxw;->c(Landroid/view/View;ILjava/lang/String;)V

    .line 112
    iget-object v1, v15, Laop;->ajU:Lani;

    if-eqz v1, :cond_8

    invoke-virtual {v15}, Laop;->rb()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 113
    const v1, 0x7f110335

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 114
    if-eqz v1, :cond_4

    .line 115
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 116
    const v3, 0x7f110379

    invoke-virtual {v15}, Laop;->ra()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lfxw;->a(Landroid/view/View;ILjava/lang/String;)Lcom/google/android/search/shared/ui/WebImageView;

    .line 117
    iget-object v2, v15, Laop;->ajU:Lani;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lfxw;->a(Landroid/view/View;Lani;)V

    .line 122
    :cond_4
    :goto_2
    return-void

    .line 72
    :cond_5
    invoke-virtual {v15}, Laop;->getHighlightColor()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    goto/16 :goto_0

    .line 99
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lfxw;->mContext:Landroid/content/Context;

    const v5, 0x7f0a0262

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v1, 0x1

    aput-object v3, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 101
    const v3, 0x7f110329

    invoke-static {v2, v3, v1}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    goto :goto_1

    .line 105
    :cond_7
    const v4, 0x7f110330

    invoke-static {v2, v4, v1}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 106
    const v1, 0x7f110333

    invoke-static {v2, v1, v3}, Lgab;->c(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    goto :goto_1

    .line 119
    :cond_8
    invoke-virtual {v15}, Laop;->qZ()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 120
    const v1, 0x7f110334

    invoke-virtual {v15}, Laop;->qY()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lfxw;->c(Landroid/view/View;ILjava/lang/String;)V

    goto :goto_2
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 5

    .prologue
    const v0, 0x7f040126

    const v1, 0x7f040125

    .line 33
    iget-object v2, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v2}, Lanh;->getType()I

    move-result v2

    .line 35
    iget-object v3, p0, Lfro;->cBc:Lanh;

    invoke-virtual {v3}, Lanh;->getType()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 49
    const-string v1, "SportVersusModulePresenter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bad sport type for sport versus module: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :goto_0
    :sswitch_0
    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 40
    :sswitch_1
    const v0, 0x7f040127

    .line 41
    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 44
    goto :goto_0

    :sswitch_3
    move v0, v1

    .line 47
    goto :goto_0

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x19 -> :sswitch_2
        0x1a -> :sswitch_1
        0x1f -> :sswitch_3
    .end sparse-switch
.end method
