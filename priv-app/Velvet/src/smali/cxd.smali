.class public final Lcxd;
.super Landroid/app/ProgressDialog;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field private final bmh:Ljava/lang/String;

.field bmi:Z

.field final mFlags:Lchk;

.field private final mLoginHelper:Lcrh;

.field private final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Lcrh;Lcpn;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 56
    invoke-direct {p0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 57
    iput-object p2, p0, Lcxd;->mFlags:Lchk;

    .line 58
    iput-object p3, p0, Lcxd;->mLoginHelper:Lcrh;

    .line 59
    iput-object p4, p0, Lcxd;->mUrlHelper:Lcpn;

    .line 60
    iput-object p5, p0, Lcxd;->bmh:Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcxd;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v0

    .line 62
    const v1, 0x7f0a05b8

    invoke-virtual {p0, v1}, Lcxd;->setTitle(I)V

    .line 63
    invoke-virtual {p0}, Lcxd;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05b9

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcxd;->setMessage(Ljava/lang/CharSequence;)V

    .line 66
    invoke-virtual {p0, v5}, Lcxd;->setIndeterminate(Z)V

    .line 67
    invoke-virtual {p0, v5}, Lcxd;->setCancelable(Z)V

    .line 68
    invoke-virtual {p0, p0}, Lcxd;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 69
    const/4 v0, -0x2

    invoke-virtual {p0}, Lcxd;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcxe;

    invoke-direct {v2, p0}, Lcxe;-><init>(Lcxd;)V

    invoke-virtual {p0, v0, v1, v2}, Lcxd;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 75
    return-void
.end method


# virtual methods
.method public final Tw()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcxd;->bmi:Z

    .line 110
    invoke-virtual {p0}, Lcxd;->dismiss()V

    .line 111
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 115
    invoke-virtual {p0}, Lcxd;->Tw()V

    .line 116
    return-void
.end method

.method public final start()V
    .locals 4

    .prologue
    .line 78
    invoke-virtual {p0}, Lcxd;->show()V

    .line 79
    iget-object v0, p0, Lcxd;->mUrlHelper:Lcpn;

    iget-object v1, p0, Lcxd;->bmh:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcpn;->hE(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcxd;->mLoginHelper:Lcrh;

    const-string v2, "hist"

    new-instance v3, Lcxf;

    invoke-direct {v3, p0, v0}, Lcxf;-><init>(Lcxd;Landroid/net/Uri;)V

    invoke-virtual {v1, v0, v2, v3}, Lcrh;->a(Landroid/net/Uri;Ljava/lang/String;Lemy;)V

    .line 106
    return-void
.end method
