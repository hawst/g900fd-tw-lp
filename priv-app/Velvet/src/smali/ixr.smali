.class public final Lixr;
.super Ljsl;
.source "PG"


# instance fields
.field public aeB:Ljbp;

.field private aez:I

.field private afh:Ljava/lang/String;

.field private afi:J

.field private afj:J

.field public dMF:[Liyg;

.field private dOg:Ljava/lang/String;

.field private dOh:I

.field private dOi:Ljava/lang/String;

.field private dOj:I

.field private dOk:Ljava/lang/String;

.field private dOl:I

.field private dOm:Ljava/lang/String;

.field private dOn:I

.field public dOo:[Ljava/lang/String;

.field public dOp:Ljck;

.field private dOq:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 29188
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 29189
    iput v1, p0, Lixr;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lixr;->dOg:Ljava/lang/String;

    iput-object v2, p0, Lixr;->aeB:Ljbp;

    invoke-static {}, Liyg;->bbv()[Liyg;

    move-result-object v0

    iput-object v0, p0, Lixr;->dMF:[Liyg;

    iput v1, p0, Lixr;->dOh:I

    const-string v0, ""

    iput-object v0, p0, Lixr;->afh:Ljava/lang/String;

    iput-wide v4, p0, Lixr;->afi:J

    const-string v0, ""

    iput-object v0, p0, Lixr;->dOi:Ljava/lang/String;

    iput v1, p0, Lixr;->dOj:I

    iput-wide v4, p0, Lixr;->afj:J

    const-string v0, ""

    iput-object v0, p0, Lixr;->dOk:Ljava/lang/String;

    iput v1, p0, Lixr;->dOl:I

    const-string v0, ""

    iput-object v0, p0, Lixr;->dOm:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lixr;->dOn:I

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lixr;->dOo:[Ljava/lang/String;

    iput-object v2, p0, Lixr;->dOp:Ljck;

    iput-boolean v1, p0, Lixr;->dOq:Z

    iput-object v2, p0, Lixr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lixr;->eCz:I

    .line 29190
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 28908
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lixr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixr;->dOg:Ljava/lang/String;

    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixr;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lixr;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Lixr;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Lixr;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lixr;->dMF:[Liyg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liyg;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lixr;->dMF:[Liyg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lixr;->dMF:[Liyg;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Liyg;

    invoke-direct {v3}, Liyg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lixr;->dMF:[Liyg;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lixr;->dOh:I

    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixr;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixr;->afh:Ljava/lang/String;

    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lixr;->afi:J

    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixr;->dOi:Ljava/lang/String;

    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lixr;->dOj:I

    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lixr;->afj:J

    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixr;->dOk:Ljava/lang/String;

    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lixr;->dOl:I

    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lixr;->dOm:Ljava/lang/String;

    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Lixr;->dOn:I

    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x72

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lixr;->dOo:[Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lixr;->dOo:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lixr;->dOo:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lixr;->dOo:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lixr;->dOp:Ljck;

    if-nez v0, :cond_8

    new-instance v0, Ljck;

    invoke-direct {v0}, Ljck;-><init>()V

    iput-object v0, p0, Lixr;->dOp:Ljck;

    :cond_8
    iget-object v0, p0, Lixr;->dOp:Ljck;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lixr;->dOq:Z

    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lixr;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 29218
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 29219
    const/4 v0, 0x1

    iget-object v2, p0, Lixr;->dOg:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 29221
    :cond_0
    iget-object v0, p0, Lixr;->aeB:Ljbp;

    if-eqz v0, :cond_1

    .line 29222
    const/4 v0, 0x2

    iget-object v2, p0, Lixr;->aeB:Ljbp;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 29224
    :cond_1
    iget-object v0, p0, Lixr;->dMF:[Liyg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lixr;->dMF:[Liyg;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 29225
    :goto_0
    iget-object v2, p0, Lixr;->dMF:[Liyg;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 29226
    iget-object v2, p0, Lixr;->dMF:[Liyg;

    aget-object v2, v2, v0

    .line 29227
    if-eqz v2, :cond_2

    .line 29228
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 29225
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29232
    :cond_3
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    .line 29233
    const/4 v0, 0x4

    iget v2, p0, Lixr;->dOh:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 29235
    :cond_4
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    .line 29236
    const/4 v0, 0x5

    iget-object v2, p0, Lixr;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 29238
    :cond_5
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 29239
    const/4 v0, 0x6

    iget-wide v2, p0, Lixr;->afi:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 29241
    :cond_6
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_7

    .line 29242
    const/4 v0, 0x7

    iget-object v2, p0, Lixr;->dOi:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 29244
    :cond_7
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_8

    .line 29245
    const/16 v0, 0x8

    iget v2, p0, Lixr;->dOj:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 29247
    :cond_8
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_9

    .line 29248
    const/16 v0, 0x9

    iget-wide v2, p0, Lixr;->afj:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 29250
    :cond_9
    iget v0, p0, Lixr;->aez:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_a

    .line 29251
    const/16 v0, 0xa

    iget-object v2, p0, Lixr;->dOk:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 29253
    :cond_a
    iget v0, p0, Lixr;->aez:I

    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_b

    .line 29254
    const/16 v0, 0xb

    iget v2, p0, Lixr;->dOl:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 29256
    :cond_b
    iget v0, p0, Lixr;->aez:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_c

    .line 29257
    const/16 v0, 0xc

    iget-object v2, p0, Lixr;->dOm:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 29259
    :cond_c
    iget v0, p0, Lixr;->aez:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_d

    .line 29260
    const/16 v0, 0xd

    iget v2, p0, Lixr;->dOn:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 29262
    :cond_d
    iget-object v0, p0, Lixr;->dOo:[Ljava/lang/String;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lixr;->dOo:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_f

    .line 29263
    :goto_1
    iget-object v0, p0, Lixr;->dOo:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_f

    .line 29264
    iget-object v0, p0, Lixr;->dOo:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 29265
    if-eqz v0, :cond_e

    .line 29266
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Ljsj;->p(ILjava/lang/String;)V

    .line 29263
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 29270
    :cond_f
    iget-object v0, p0, Lixr;->dOp:Ljck;

    if-eqz v0, :cond_10

    .line 29271
    const/16 v0, 0xf

    iget-object v1, p0, Lixr;->dOp:Ljck;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 29273
    :cond_10
    iget v0, p0, Lixr;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_11

    .line 29274
    const/16 v0, 0x10

    iget-boolean v1, p0, Lixr;->dOq:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 29276
    :cond_11
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 29277
    return-void
.end method

.method public final baD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28936
    iget-object v0, p0, Lixr;->dOg:Ljava/lang/String;

    return-object v0
.end method

.method public final baE()Z
    .locals 1

    .prologue
    .line 28947
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final baF()I
    .locals 1

    .prologue
    .line 28964
    iget v0, p0, Lixr;->dOh:I

    return v0
.end method

.method public final baG()Z
    .locals 1

    .prologue
    .line 28972
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final baH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29125
    iget-object v0, p0, Lixr;->dOm:Ljava/lang/String;

    return-object v0
.end method

.method public final baI()Z
    .locals 1

    .prologue
    .line 29172
    iget-boolean v0, p0, Lixr;->dOq:Z

    return v0
.end method

.method public final baJ()Z
    .locals 1

    .prologue
    .line 29180
    iget v0, p0, Lixr;->aez:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ch(J)Lixr;
    .locals 1

    .prologue
    .line 29008
    iput-wide p1, p0, Lixr;->afi:J

    .line 29009
    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lixr;->aez:I

    .line 29010
    return-object p0
.end method

.method public final ci(J)Lixr;
    .locals 1

    .prologue
    .line 29068
    iput-wide p1, p0, Lixr;->afj:J

    .line 29069
    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lixr;->aez:I

    .line 29070
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28983
    iget-object v0, p0, Lixr;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final hh(Z)Lixr;
    .locals 1

    .prologue
    .line 29175
    iput-boolean p1, p0, Lixr;->dOq:Z

    .line 29176
    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lixr;->aez:I

    .line 29177
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 29281
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 29282
    iget v2, p0, Lixr;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 29283
    const/4 v2, 0x1

    iget-object v3, p0, Lixr;->dOg:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 29286
    :cond_0
    iget-object v2, p0, Lixr;->aeB:Ljbp;

    if-eqz v2, :cond_1

    .line 29287
    const/4 v2, 0x2

    iget-object v3, p0, Lixr;->aeB:Ljbp;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 29290
    :cond_1
    iget-object v2, p0, Lixr;->dMF:[Liyg;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lixr;->dMF:[Liyg;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 29291
    :goto_0
    iget-object v3, p0, Lixr;->dMF:[Liyg;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 29292
    iget-object v3, p0, Lixr;->dMF:[Liyg;

    aget-object v3, v3, v0

    .line 29293
    if-eqz v3, :cond_2

    .line 29294
    const/4 v4, 0x3

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 29291
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 29299
    :cond_4
    iget v2, p0, Lixr;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_5

    .line 29300
    const/4 v2, 0x4

    iget v3, p0, Lixr;->dOh:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 29303
    :cond_5
    iget v2, p0, Lixr;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_6

    .line 29304
    const/4 v2, 0x5

    iget-object v3, p0, Lixr;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 29307
    :cond_6
    iget v2, p0, Lixr;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_7

    .line 29308
    const/4 v2, 0x6

    iget-wide v4, p0, Lixr;->afi:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 29311
    :cond_7
    iget v2, p0, Lixr;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_8

    .line 29312
    const/4 v2, 0x7

    iget-object v3, p0, Lixr;->dOi:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 29315
    :cond_8
    iget v2, p0, Lixr;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_9

    .line 29316
    const/16 v2, 0x8

    iget v3, p0, Lixr;->dOj:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 29319
    :cond_9
    iget v2, p0, Lixr;->aez:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_a

    .line 29320
    const/16 v2, 0x9

    iget-wide v4, p0, Lixr;->afj:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 29323
    :cond_a
    iget v2, p0, Lixr;->aez:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_b

    .line 29324
    const/16 v2, 0xa

    iget-object v3, p0, Lixr;->dOk:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 29327
    :cond_b
    iget v2, p0, Lixr;->aez:I

    and-int/lit16 v2, v2, 0x100

    if-eqz v2, :cond_c

    .line 29328
    const/16 v2, 0xb

    iget v3, p0, Lixr;->dOl:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 29331
    :cond_c
    iget v2, p0, Lixr;->aez:I

    and-int/lit16 v2, v2, 0x200

    if-eqz v2, :cond_d

    .line 29332
    const/16 v2, 0xc

    iget-object v3, p0, Lixr;->dOm:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 29335
    :cond_d
    iget v2, p0, Lixr;->aez:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_e

    .line 29336
    const/16 v2, 0xd

    iget v3, p0, Lixr;->dOn:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 29339
    :cond_e
    iget-object v2, p0, Lixr;->dOo:[Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lixr;->dOo:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_11

    move v2, v1

    move v3, v1

    .line 29342
    :goto_1
    iget-object v4, p0, Lixr;->dOo:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_10

    .line 29343
    iget-object v4, p0, Lixr;->dOo:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 29344
    if-eqz v4, :cond_f

    .line 29345
    add-int/lit8 v3, v3, 0x1

    .line 29346
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 29342
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 29350
    :cond_10
    add-int/2addr v0, v2

    .line 29351
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 29353
    :cond_11
    iget-object v1, p0, Lixr;->dOp:Ljck;

    if-eqz v1, :cond_12

    .line 29354
    const/16 v1, 0xf

    iget-object v2, p0, Lixr;->dOp:Ljck;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29357
    :cond_12
    iget v1, p0, Lixr;->aez:I

    and-int/lit16 v1, v1, 0x800

    if-eqz v1, :cond_13

    .line 29358
    const/16 v1, 0x10

    iget-boolean v2, p0, Lixr;->dOq:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 29361
    :cond_13
    return v0
.end method

.method public final nk()J
    .locals 2

    .prologue
    .line 29005
    iget-wide v0, p0, Lixr;->afi:J

    return-wide v0
.end method

.method public final nm()J
    .locals 2

    .prologue
    .line 29065
    iget-wide v0, p0, Lixr;->afj:J

    return-wide v0
.end method

.method public final nn(I)Lixr;
    .locals 1

    .prologue
    .line 28967
    const/16 v0, 0x384

    iput v0, p0, Lixr;->dOh:I

    .line 28968
    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lixr;->aez:I

    .line 28969
    return-object p0
.end method

.method public final no(I)Lixr;
    .locals 1

    .prologue
    .line 29049
    iput p1, p0, Lixr;->dOj:I

    .line 29050
    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lixr;->aez:I

    .line 29051
    return-object p0
.end method

.method public final np(I)Lixr;
    .locals 1

    .prologue
    .line 29109
    iput p1, p0, Lixr;->dOl:I

    .line 29110
    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lixr;->aez:I

    .line 29111
    return-object p0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 28994
    iget v0, p0, Lixr;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final qO(Ljava/lang/String;)Lixr;
    .locals 1

    .prologue
    .line 28939
    if-nez p1, :cond_0

    .line 28940
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28942
    :cond_0
    iput-object p1, p0, Lixr;->dOg:Ljava/lang/String;

    .line 28943
    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixr;->aez:I

    .line 28944
    return-object p0
.end method

.method public final qP(Ljava/lang/String;)Lixr;
    .locals 1

    .prologue
    .line 28986
    if-nez p1, :cond_0

    .line 28987
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 28989
    :cond_0
    iput-object p1, p0, Lixr;->afh:Ljava/lang/String;

    .line 28990
    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lixr;->aez:I

    .line 28991
    return-object p0
.end method

.method public final qQ(Ljava/lang/String;)Lixr;
    .locals 1

    .prologue
    .line 29027
    if-nez p1, :cond_0

    .line 29028
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29030
    :cond_0
    iput-object p1, p0, Lixr;->dOi:Ljava/lang/String;

    .line 29031
    iget v0, p0, Lixr;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lixr;->aez:I

    .line 29032
    return-object p0
.end method

.method public final qR(Ljava/lang/String;)Lixr;
    .locals 1

    .prologue
    .line 29087
    if-nez p1, :cond_0

    .line 29088
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29090
    :cond_0
    iput-object p1, p0, Lixr;->dOk:Ljava/lang/String;

    .line 29091
    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lixr;->aez:I

    .line 29092
    return-object p0
.end method

.method public final qS(Ljava/lang/String;)Lixr;
    .locals 1

    .prologue
    .line 29128
    if-nez p1, :cond_0

    .line 29129
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 29131
    :cond_0
    iput-object p1, p0, Lixr;->dOm:Ljava/lang/String;

    .line 29132
    iget v0, p0, Lixr;->aez:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lixr;->aez:I

    .line 29133
    return-object p0
.end method
