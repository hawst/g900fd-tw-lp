.class public final Ljkj;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile epT:[Ljkj;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private dNw:Ljava/lang/String;

.field private epU:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput v0, p0, Ljkj;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljkj;->agq:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljkj;->epU:[I

    const-string v0, ""

    iput-object v0, p0, Ljkj;->dNw:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljkj;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkj;->eCz:I

    .line 76
    return-void
.end method

.method public static bol()[Ljkj;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Ljkj;->epT:[Ljkj;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Ljkj;->epT:[Ljkj;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Ljkj;

    sput-object v0, Ljkj;->epT:[Ljkj;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Ljkj;->epT:[Ljkj;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkj;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkj;->agq:Ljava/lang/String;

    iget v0, p0, Ljkj;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkj;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljkj;->epU:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljkj;->epU:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljkj;->epU:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljkj;->epU:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljkj;->epU:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljkj;->epU:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljkj;->epU:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljkj;->epU:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkj;->dNw:Ljava/lang/String;

    iget v0, p0, Ljkj;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkj;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 91
    iget v0, p0, Ljkj;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 92
    const/4 v0, 0x1

    iget-object v1, p0, Ljkj;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 94
    :cond_0
    iget-object v0, p0, Ljkj;->epU:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljkj;->epU:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 95
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljkj;->epU:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 96
    const/4 v1, 0x2

    iget-object v2, p0, Ljkj;->epU:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_1
    iget v0, p0, Ljkj;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 100
    const/4 v0, 0x3

    iget-object v1, p0, Ljkj;->dNw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 102
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 103
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 107
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 108
    iget v2, p0, Ljkj;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 109
    const/4 v2, 0x1

    iget-object v3, p0, Ljkj;->agq:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    :cond_0
    iget-object v2, p0, Ljkj;->epU:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljkj;->epU:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 114
    :goto_0
    iget-object v3, p0, Ljkj;->epU:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 115
    iget-object v3, p0, Ljkj;->epU:[I

    aget v3, v3, v1

    .line 116
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    :cond_1
    add-int/2addr v0, v2

    .line 120
    iget-object v1, p0, Ljkj;->epU:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 122
    :cond_2
    iget v1, p0, Ljkj;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 123
    const/4 v1, 0x3

    iget-object v2, p0, Ljkj;->dNw:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    :cond_3
    return v0
.end method
