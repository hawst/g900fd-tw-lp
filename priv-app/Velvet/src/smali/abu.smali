.class final Labu;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private Po:I

.field private synthetic Pp:Labt;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Labt;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 135
    iput-object p1, p0, Labu;->Pp:Labt;

    .line 136
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 137
    iput-object p2, p0, Labu;->mContext:Landroid/content/Context;

    .line 138
    const/4 v0, 0x0

    iput v0, p0, Labu;->Po:I

    .line 139
    return-void
.end method

.method private varargs a([Ljava/util/List;)Ljava/lang/Void;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 143
    iget-object v0, p0, Labu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 145
    aget-object v0, p1, v7

    .line 147
    new-instance v1, Labv;

    invoke-direct {v1, p0, v2}, Labv;-><init>(Labu;Landroid/content/pm/PackageManager;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 160
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 161
    :try_start_0
    new-instance v1, Landroid/app/WallpaperInfo;

    iget-object v4, p0, Labu;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4, v0}, Landroid/app/WallpaperInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 173
    invoke-virtual {v1, v2}, Landroid/app/WallpaperInfo;->loadThumbnail(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 174
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.service.wallpaper.WallpaperService"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 175
    invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getServiceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    new-instance v4, Labw;

    invoke-direct {v4, v0, v1}, Labw;-><init>(Landroid/graphics/drawable/Drawable;Landroid/app/WallpaperInfo;)V

    .line 177
    new-array v0, v8, [Labw;

    aput-object v4, v0, v7

    invoke-virtual {p0, v0}, Labu;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0

    .line 164
    :catch_0
    move-exception v1

    .line 165
    const-string v4, "LiveWallpaperListAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Skipping wallpaper "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 167
    :catch_1
    move-exception v1

    .line 168
    const-string v4, "LiveWallpaperListAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Skipping wallpaper "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 180
    :cond_0
    new-array v0, v8, [Labw;

    aput-object v9, v0, v7

    invoke-virtual {p0, v0}, Labu;->publishProgress([Ljava/lang/Object;)V

    .line 182
    return-object v9
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 130
    check-cast p1, [Ljava/util/List;

    invoke-direct {p0, p1}, Labu;->a([Ljava/util/List;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 130
    check-cast p1, [Labw;

    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    if-nez v2, :cond_1

    iget-object v0, p0, Labu;->Pp:Labt;

    invoke-virtual {v0}, Labt;->notifyDataSetChanged()V

    :cond_0
    return-void

    :cond_1
    iget-object v3, v2, Labw;->Pr:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_2

    iget-object v3, v2, Labw;->Pr:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    :cond_2
    iget v3, p0, Labu;->Po:I

    iget-object v4, p0, Labu;->Pp:Labt;

    invoke-static {v4}, Labt;->a(Labt;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    iget-object v3, p0, Labu;->Pp:Labt;

    invoke-static {v3}, Labt;->a(Labt;)Ljava/util/List;

    move-result-object v3

    iget v4, p0, Labu;->Po:I

    invoke-interface {v3, v4, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget v2, p0, Labu;->Po:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Labu;->Po:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Labu;->Pp:Labt;

    invoke-static {v3}, Labt;->a(Labt;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
