.class public final Lior;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static dFN:Lifn;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1982
    sget-object v0, Liia;->dCE:Lifj;

    const-string v1, "="

    invoke-virtual {v0, v1}, Lifj;->pr(Ljava/lang/String;)Lifn;

    move-result-object v0

    sput-object v0, Lior;->dFN:Lifn;

    return-void
.end method

.method static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1991
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1993
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;Lifw;)Ljava/util/Map;
    .locals 3

    .prologue
    .line 1407
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_0

    .line 1408
    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Liot;

    invoke-direct {v0, p1}, Liot;-><init>(Lifw;)V

    invoke-static {p0, v0}, Lior;->a(Ljava/util/SortedMap;Lifw;)Ljava/util/SortedMap;

    move-result-object v0

    .line 1418
    :goto_0
    return-object v0

    .line 1410
    :cond_0
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1411
    new-instance v2, Lios;

    invoke-direct {v2, p1}, Lios;-><init>(Lifw;)V

    .line 1418
    instance-of v0, p0, Ljava/util/SortedMap;

    if-eqz v0, :cond_1

    check-cast p0, Ljava/util/SortedMap;

    invoke-static {p0, v2}, Lior;->a(Ljava/util/SortedMap;Lifw;)Ljava/util/SortedMap;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v0, p0, Liou;

    if-eqz v0, :cond_2

    check-cast p0, Liou;

    iget-object v0, p0, Liou;->dBv:Lifw;

    invoke-static {v0, v2}, Lifx;->a(Lifw;Lifw;)Lifw;

    move-result-object v1

    new-instance v0, Lioy;

    iget-object v2, p0, Liou;->dFP:Ljava/util/Map;

    invoke-direct {v0, v2, v1}, Lioy;-><init>(Ljava/util/Map;Lifw;)V

    goto :goto_0

    :cond_2
    new-instance v1, Lioy;

    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {v1, v0, v2}, Lioy;-><init>(Ljava/util/Map;Lifw;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/util/SortedMap;Lifw;)Ljava/util/SortedMap;
    .locals 3

    .prologue
    .line 1541
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1542
    instance-of v0, p0, Lipe;

    if-eqz v0, :cond_0

    check-cast p0, Lipe;

    iget-object v0, p0, Lipe;->dBv:Lifw;

    invoke-static {v0, p1}, Lifx;->a(Lifw;Lifw;)Lifw;

    move-result-object v1

    new-instance v0, Lipe;

    invoke-virtual {p0}, Lipe;->aYe()Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lipe;-><init>(Ljava/util/SortedMap;Lifw;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lipe;

    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, v0, p1}, Lipe;-><init>(Ljava/util/SortedMap;Lifw;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static aXZ()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public static aYa()Ljava/util/LinkedHashMap;
    .locals 1

    .prologue
    .line 151
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    return-object v0
.end method

.method public static aYb()Ljava/util/concurrent/ConcurrentMap;
    .locals 1

    .prologue
    .line 186
    new-instance v0, Limd;

    invoke-direct {v0}, Limd;-><init>()V

    invoke-virtual {v0}, Limd;->aXt()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method

.method public static aYc()Ljava/util/TreeMap;
    .locals 1

    .prologue
    .line 199
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    return-object v0
.end method

.method public static aYd()Ljava/util/IdentityHashMap;
    .locals 1

    .prologue
    .line 268
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    return-object v0
.end method

.method static b(Ljava/util/Map;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2003
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2005
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static mk(I)Ljava/util/HashMap;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-static {p0}, Lior;->ml(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    return-object v0
.end method

.method static ml(I)I
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x3

    if-ge p0, v0, :cond_1

    .line 113
    if-ltz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 114
    add-int/lit8 v0, p0, 0x1

    .line 119
    :goto_1
    return v0

    .line 113
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 116
    :cond_1
    const/high16 v0, 0x40000000    # 2.0f

    if-ge p0, v0, :cond_2

    .line 117
    div-int/lit8 v0, p0, 0x3

    add-int/2addr v0, p0

    goto :goto_1

    .line 119
    :cond_2
    const v0, 0x7fffffff

    goto :goto_1
.end method

.method public static t(Ljava/util/Map;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 138
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method static u(Ljava/util/Map;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2074
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Liia;->lY(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2076
    sget-object v1, Lior;->dFN:Lifn;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lifn;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    .line 2077
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static x(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .locals 1
    .param p0    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 722
    new-instance v0, Liji;

    invoke-direct {v0, p0, p1}, Liji;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
