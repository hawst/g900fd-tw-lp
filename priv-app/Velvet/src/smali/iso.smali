.class public final Liso;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/io/InputStream;[BII)I
    .locals 3

    .prologue
    .line 801
    if-gez p3, :cond_0

    .line 802
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "len is negative"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 804
    :cond_0
    const/4 v0, 0x0

    .line 805
    :goto_0
    if-ge v0, p3, :cond_1

    .line 806
    add-int v1, p2, v0

    sub-int v2, p3, v0

    invoke-virtual {p0, p1, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 807
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 808
    add-int/2addr v0, v1

    .line 811
    goto :goto_0

    .line 812
    :cond_1
    return v0
.end method

.method public static a(Lisu;Lisv;)J
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 115
    .line 116
    invoke-interface {p0}, Lisu;->PN()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 118
    :try_start_0
    invoke-interface {p1}, Lisv;->auX()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 120
    :try_start_1
    invoke-static {v0, v1}, Liso;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    .line 122
    const/4 v6, 0x0

    :try_start_2
    invoke-static {v1, v6}, Lisq;->a(Ljava/io/Closeable;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 125
    invoke-static {v0, v3}, Lisq;->a(Ljava/io/Closeable;Z)V

    return-wide v4

    .line 124
    :catchall_0
    move-exception v5

    move v4, v2

    :try_start_3
    invoke-static {v1, v4}, Lisq;->a(Ljava/io/Closeable;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 125
    :try_start_4
    throw v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 128
    :catchall_1
    move-exception v1

    move-object v4, v1

    move v1, v2

    :goto_0
    const/4 v5, 0x2

    if-ge v1, v5, :cond_0

    move v1, v2

    :goto_1
    invoke-static {v0, v1}, Lisq;->a(Ljava/io/Closeable;Z)V

    throw v4

    :cond_0
    move v1, v3

    goto :goto_1

    :catchall_2
    move-exception v1

    move-object v4, v1

    move v1, v3

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 6

    .prologue
    .line 190
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 191
    const-wide/16 v0, 0x0

    .line 193
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 194
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 195
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 198
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 199
    goto :goto_0

    .line 200
    :cond_0
    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;[B)V
    .locals 2

    .prologue
    .line 642
    array-length v0, p1

    const/4 v1, 0x0

    invoke-static {p0, p1, v1, v0}, Liso;->a(Ljava/io/InputStream;[BII)I

    move-result v1

    if-eq v1, v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 643
    :cond_0
    return-void
.end method

.method public static a(Lisu;)[B
    .locals 3

    .prologue
    .line 248
    const/4 v1, 0x1

    .line 249
    invoke-interface {p0}, Lisu;->PN()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    .line 251
    :try_start_0
    invoke-static {v0}, Liso;->j(Ljava/io/InputStream;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 255
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lisq;->a(Ljava/io/Closeable;Z)V

    return-object v1

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Lisq;->a(Ljava/io/Closeable;Z)V

    throw v2
.end method

.method public static j(Ljava/io/InputStream;)[B
    .locals 1

    .prologue
    .line 235
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 236
    invoke-static {p0, v0}, Liso;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 237
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
