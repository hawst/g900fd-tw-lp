.class public final Lbje;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic aDt:Lbjd;


# direct methods
.method public constructor <init>(Lbjd;)V
    .locals 0

    iput-object p1, p0, Lbje;->aDt:Lbjd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    iget-object v0, p0, Lbje;->aDt:Lbjd;

    iget-object v0, v0, Lbjd;->aDs:Lbjc;

    invoke-static {v0}, Lbjc;->a(Lbjc;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbje;->aDt:Lbjd;

    iput-object p2, v0, Lbjd;->aDq:Landroid/os/IBinder;

    iget-object v0, p0, Lbje;->aDt:Lbjd;

    iput-object p1, v0, Lbjd;->aDr:Landroid/content/ComponentName;

    iget-object v0, p0, Lbje;->aDt:Lbjd;

    iget-object v0, v0, Lbjd;->aDo:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiw;

    invoke-virtual {v0, p1, p2}, Lbiw;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbje;->aDt:Lbjd;

    const/4 v2, 0x1

    iput v2, v0, Lbjd;->ag:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    iget-object v0, p0, Lbje;->aDt:Lbjd;

    iget-object v0, v0, Lbjd;->aDs:Lbjc;

    invoke-static {v0}, Lbjc;->a(Lbjc;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbje;->aDt:Lbjd;

    const/4 v2, 0x0

    iput-object v2, v0, Lbjd;->aDq:Landroid/os/IBinder;

    iget-object v0, p0, Lbje;->aDt:Lbjd;

    iput-object p1, v0, Lbjd;->aDr:Landroid/content/ComponentName;

    iget-object v0, p0, Lbje;->aDt:Lbjd;

    iget-object v0, v0, Lbjd;->aDo:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiw;

    invoke-virtual {v0, p1}, Lbiw;->onServiceDisconnected(Landroid/content/ComponentName;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbje;->aDt:Lbjd;

    const/4 v2, 0x2

    iput v2, v0, Lbjd;->ag:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
