.class public Lhik;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgfr;


# static fields
.field private static final dhX:[B


# instance fields
.field private final aWy:Ljava/util/List;

.field private final dhY:Landroid/util/SparseArray;

.field private final dhZ:Ljava/util/concurrent/Executor;

.field private dia:I

.field private dib:Z

.field private final mAudioRouter:Lhhu;

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lhik;->dhX:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lhhu;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lhik;->dhY:Landroid/util/SparseArray;

    .line 58
    iput-object p1, p0, Lhik;->mContext:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lhik;->mAudioRouter:Lhhu;

    .line 60
    iput-object p3, p0, Lhik;->dhZ:Ljava/util/concurrent/Executor;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhik;->aWy:Ljava/util/List;

    .line 62
    return-void
.end method

.method static a(JJJ)J
    .locals 6

    .prologue
    const-wide/16 v0, 0x1f4

    const-wide/16 v2, 0x32

    .line 346
    cmp-long v4, p0, v0

    if-lez v4, :cond_1

    move-wide p0, v0

    :cond_0
    :goto_0
    return-wide p0

    :cond_1
    cmp-long v0, p0, v2

    if-gez v0, :cond_0

    move-wide p0, v2

    goto :goto_0
.end method

.method static synthetic a(Lhik;I)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lhik;->kV(I)V

    return-void
.end method

.method private static aX(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 257
    :try_start_0
    instance-of v0, p0, Landroid/content/res/AssetFileDescriptor;

    if-eqz v0, :cond_1

    .line 258
    check-cast p0, Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {p0}, Landroid/content/res/AssetFileDescriptor;->close()V

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    instance-of v0, p0, Ljava/io/Closeable;

    if-eqz v0, :cond_0

    .line 260
    check-cast p0, Ljava/io/Closeable;

    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized kV(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 77
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lhik;->dia:I

    if-eqz v2, :cond_1

    move v2, v0

    .line 78
    :goto_0
    iget v3, p0, Lhik;->dia:I

    add-int/2addr v3, p1

    iput v3, p0, Lhik;->dia:I

    .line 79
    iget v3, p0, Lhik;->dia:I

    if-eqz v3, :cond_0

    move v1, v0

    .line 80
    :cond_0
    if-eq v1, v2, :cond_3

    .line 81
    iget-boolean v0, p0, Lhik;->dib:Z

    if-nez v0, :cond_3

    .line 85
    iget-object v0, p0, Lhik;->aWy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhim;

    .line 86
    if-eqz v1, :cond_2

    .line 87
    invoke-interface {v0}, Lhim;->aeb()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v2, v1

    goto :goto_0

    .line 89
    :cond_2
    :try_start_1
    invoke-interface {v0}, Lhim;->aec()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 94
    :cond_3
    monitor-exit p0

    return-void
.end method

.method private kX(I)[B
    .locals 6

    .prologue
    .line 219
    iget-object v0, p0, Lhik;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    .line 221
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v0

    .line 222
    const-wide/32 v4, 0x7fffffff

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 223
    invoke-static {v2}, Lhik;->aX(Ljava/lang/Object;)V

    .line 224
    sget-object v0, Lhik;->dhX:[B

    .line 242
    :goto_0
    return-object v0

    .line 227
    :cond_0
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v0

    long-to-int v3, v0

    .line 228
    new-array v0, v3, [B

    .line 229
    const/4 v1, 0x0

    .line 231
    :try_start_0
    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    .line 232
    const/4 v4, 0x0

    invoke-static {v1, v0, v4, v3}, Liso;->a(Ljava/io/InputStream;[BII)I

    move-result v4

    if-eq v4, v3, :cond_1

    .line 233
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :catch_0
    move-exception v0

    :try_start_1
    sget-object v0, Lhik;->dhX:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    invoke-static {v1}, Lhik;->aX(Ljava/lang/Object;)V

    .line 239
    invoke-static {v2}, Lhik;->aX(Ljava/lang/Object;)V

    goto :goto_0

    .line 238
    :cond_1
    invoke-static {v1}, Lhik;->aX(Ljava/lang/Object;)V

    .line 239
    invoke-static {v2}, Lhik;->aX(Ljava/lang/Object;)V

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lhik;->aX(Ljava/lang/Object;)V

    .line 239
    invoke-static {v2}, Lhik;->aX(Ljava/lang/Object;)V

    throw v0
.end method

.method private n(ILjava/lang/String;)V
    .locals 7

    .prologue
    .line 136
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhik;->kV(I)V

    .line 137
    iget-object v6, p0, Lhik;->dhZ:Ljava/util/concurrent/Executor;

    new-instance v0, Lhil;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Play sound: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    new-array v3, v1, [I

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhil;-><init>(Lhik;Ljava/lang/String;[IILjava/lang/String;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 143
    return-void
.end method


# virtual methods
.method final J(IZ)I
    .locals 2

    .prologue
    .line 148
    if-eqz p2, :cond_0

    .line 149
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhik;->kV(I)V

    .line 151
    :cond_0
    iget-object v0, p0, Lhik;->dhY:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 152
    if-nez v0, :cond_1

    .line 153
    invoke-direct {p0, p1}, Lhik;->kX(I)[B

    move-result-object v0

    .line 156
    :cond_1
    sget-object v1, Lhik;->dhX:[B

    if-ne v0, v1, :cond_2

    .line 158
    const/4 v0, -0x1

    .line 161
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0, v0}, Lhik;->W([B)I

    move-result v0

    goto :goto_0
.end method

.method public final W([B)I
    .locals 5

    .prologue
    .line 166
    array-length v1, p1

    .line 167
    invoke-virtual {p0, v1}, Lhik;->kW(I)Landroid/media/AudioTrack;

    move-result-object v2

    .line 169
    invoke-virtual {v2}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    .line 173
    const/4 v0, -0x1

    .line 181
    :goto_0
    return v0

    .line 176
    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    sub-int v3, v1, v0

    invoke-virtual {v2, p1, v0, v3}, Landroid/media/AudioTrack;->write([BII)I

    move-result v3

    if-ltz v3, :cond_1

    add-int/2addr v0, v3

    goto :goto_1

    .line 177
    :cond_1
    invoke-virtual {v2}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v0

    .line 178
    invoke-virtual {v2}, Landroid/media/AudioTrack;->play()V

    .line 179
    iget-object v3, p0, Lhik;->dhZ:Ljava/util/concurrent/Executor;

    new-instance v4, Lhin;

    invoke-direct {v4, p0, v2, v1}, Lhin;-><init>(Lhik;Landroid/media/AudioTrack;I)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lhim;)V
    .locals 1

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhik;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final aFD()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 98
    invoke-static {}, Lenu;->auQ()V

    .line 99
    iget-object v0, p0, Lhik;->mAudioRouter:Lhhu;

    invoke-interface {v0}, Lhhu;->aPs()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const v0, 0x7f08000e

    invoke-virtual {p0, v0, v1}, Lhik;->J(IZ)I

    move-result v0

    .line 103
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f08000d

    invoke-virtual {p0, v0, v1}, Lhik;->J(IZ)I

    move-result v0

    goto :goto_0
.end method

.method public final aPD()V
    .locals 2

    .prologue
    .line 108
    const v0, 0x7f08000f

    const-string v1, "<beep>Success</beep>"

    invoke-direct {p0, v0, v1}, Lhik;->n(ILjava/lang/String;)V

    .line 109
    return-void
.end method

.method public final aPE()V
    .locals 2

    .prologue
    .line 113
    const v0, 0x7f08000b

    const-string v1, "<beep>No-Input</beep>"

    invoke-direct {p0, v0, v1}, Lhik;->n(ILjava/lang/String;)V

    .line 114
    return-void
.end method

.method public final aPF()V
    .locals 2

    .prologue
    .line 117
    const v0, 0x7f08000b

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lhik;->J(IZ)I

    .line 118
    return-void
.end method

.method public final aPG()V
    .locals 2

    .prologue
    .line 121
    const v0, 0x7f08000b

    const-string v1, "<beep>No-Input</beep>"

    invoke-direct {p0, v0, v1}, Lhik;->n(ILjava/lang/String;)V

    .line 122
    return-void
.end method

.method public final aPH()V
    .locals 2

    .prologue
    .line 125
    const v0, 0x7f080009

    const-string v1, "<beep>Failure</beep>"

    invoke-direct {p0, v0, v1}, Lhik;->n(ILjava/lang/String;)V

    .line 126
    return-void
.end method

.method public final aPI()V
    .locals 2

    .prologue
    .line 132
    const v0, 0x7f08000c

    const-string v1, "<beep>Notification</beep>"

    invoke-direct {p0, v0, v1}, Lhik;->n(ILjava/lang/String;)V

    .line 133
    return-void
.end method

.method public final declared-synchronized b(Lhim;)V
    .locals 1

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhik;->aWy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit p0

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized gt(Z)V
    .locals 1

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lhik;->dib:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected kW(I)Landroid/media/AudioTrack;
    .locals 7

    .prologue
    .line 188
    new-instance v0, Landroid/media/AudioTrack;

    iget-object v1, p0, Lhik;->mAudioRouter:Lhhu;

    invoke-interface {v1}, Lhhu;->aPq()I

    move-result v1

    const/16 v2, 0x3e80

    const/4 v3, 0x4

    const/4 v4, 0x2

    const/4 v6, 0x1

    move v5, p1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    return-object v0
.end method
