.class public final Ljqf;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eyA:Ljava/lang/String;

.field public eyB:[Ljava/lang/String;

.field private eyC:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6660
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6661
    iput v1, p0, Ljqf;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqf;->eyA:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljqf;->eyB:[Ljava/lang/String;

    iput v1, p0, Ljqf;->eyC:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljqf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqf;->eCz:I

    .line 6662
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6597
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqf;->eyA:Ljava/lang/String;

    iget v0, p0, Ljqf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqf;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqf;->eyB:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljqf;->eyB:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljqf;->eyB:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljqf;->eyB:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqf;->eyC:I

    iget v0, p0, Ljqf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqf;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 6677
    iget v0, p0, Ljqf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6678
    const/4 v0, 0x1

    iget-object v1, p0, Ljqf;->eyA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6680
    :cond_0
    iget-object v0, p0, Ljqf;->eyB:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljqf;->eyB:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 6681
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljqf;->eyB:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 6682
    iget-object v1, p0, Ljqf;->eyB:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 6683
    if-eqz v1, :cond_1

    .line 6684
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6681
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6688
    :cond_2
    iget v0, p0, Ljqf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 6689
    const/4 v0, 0x3

    iget v1, p0, Ljqf;->eyC:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6691
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6692
    return-void
.end method

.method public final bsn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6619
    iget-object v0, p0, Ljqf;->eyA:Ljava/lang/String;

    return-object v0
.end method

.method public final bso()Z
    .locals 1

    .prologue
    .line 6630
    iget v0, p0, Ljqf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsp()I
    .locals 1

    .prologue
    .line 6644
    iget v0, p0, Ljqf;->eyC:I

    return v0
.end method

.method public final bsq()Z
    .locals 1

    .prologue
    .line 6652
    iget v0, p0, Ljqf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6696
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6697
    iget v2, p0, Ljqf;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 6698
    const/4 v2, 0x1

    iget-object v3, p0, Ljqf;->eyA:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6701
    :cond_0
    iget-object v2, p0, Ljqf;->eyB:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljqf;->eyB:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 6704
    :goto_0
    iget-object v4, p0, Ljqf;->eyB:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 6705
    iget-object v4, p0, Ljqf;->eyB:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 6706
    if-eqz v4, :cond_1

    .line 6707
    add-int/lit8 v3, v3, 0x1

    .line 6708
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 6704
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6712
    :cond_2
    add-int/2addr v0, v2

    .line 6713
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 6715
    :cond_3
    iget v1, p0, Ljqf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 6716
    const/4 v1, 0x3

    iget v2, p0, Ljqf;->eyC:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6719
    :cond_4
    return v0
.end method

.method public final yg(Ljava/lang/String;)Ljqf;
    .locals 1

    .prologue
    .line 6622
    if-nez p1, :cond_0

    .line 6623
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6625
    :cond_0
    iput-object p1, p0, Ljqf;->eyA:Ljava/lang/String;

    .line 6626
    iget v0, p0, Ljqf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqf;->aez:I

    .line 6627
    return-object p0
.end method
