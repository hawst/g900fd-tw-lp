.class public final Lgkw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgky;


# instance fields
.field private final cOn:Lglx;

.field private cOo:J

.field private final cOp:Ljzk;

.field private final cOq:Z

.field private cOr:I

.field private final mStateMachine:Leqx;


# direct methods
.method public constructor <init>(Lglx;Ljzk;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "DefaultEndpointerEventProcessor"

    sget-object v1, Lgkx;->cOs:Lgkx;

    invoke-static {v0, v1}, Leqx;->a(Ljava/lang/String;Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgkx;->cOs:Lgkx;

    new-array v2, v5, [Lgkx;

    sget-object v3, Lgkx;->cOt:Lgkx;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgkx;->cOs:Lgkx;

    new-array v2, v5, [Lgkx;

    sget-object v3, Lgkx;->cOv:Lgkx;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgkx;->cOt:Lgkx;

    new-array v2, v5, [Lgkx;

    sget-object v3, Lgkx;->cOu:Lgkx;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgkx;->cOt:Lgkx;

    new-array v2, v5, [Lgkx;

    sget-object v3, Lgkx;->cOv:Lgkx;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgkx;->cOu:Lgkx;

    new-array v2, v5, [Lgkx;

    sget-object v3, Lgkx;->cOt:Lgkx;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgkx;->cOu:Lgkx;

    new-array v2, v5, [Lgkx;

    sget-object v3, Lgkx;->cOv:Lgkx;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    iput-boolean v4, v0, Leqy;->chK:Z

    invoke-virtual {v0}, Leqy;->avx()Leqx;

    move-result-object v0

    iput-object v0, p0, Lgkw;->mStateMachine:Leqx;

    .line 51
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglx;

    iput-object v0, p0, Lgkw;->cOn:Lglx;

    .line 52
    iput-object p2, p0, Lgkw;->cOp:Ljzk;

    .line 53
    iput v4, p0, Lgkw;->cOr:I

    .line 54
    iput-boolean p3, p0, Lgkw;->cOq:Z

    .line 55
    return-void
.end method

.method private declared-synchronized aGZ()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 142
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v2, Lgkx;->cOs:Lgkx;

    invoke-virtual {v1, v2}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 143
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOt:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    const/4 v0, 0x1

    .line 149
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 145
    :cond_1
    :try_start_1
    iget-object v1, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v2, Lgkx;->cOu:Lgkx;

    invoke-virtual {v1, v2}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v2, Lgkx;->cOt:Lgkx;

    invoke-virtual {v1, v2}, Leqx;->a(Ljava/lang/Enum;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized aHa()Z
    .locals 2

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOt:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOv:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    const/4 v0, 0x1

    .line 180
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized aHb()Z
    .locals 2

    .prologue
    .line 185
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOs:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOv:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    const/4 v0, 0x1

    .line 189
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Ljvr;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 64
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljvr;->bvb()Z

    move-result v1

    if-nez v1, :cond_2

    .line 65
    :cond_0
    const-string v1, "DefaultEndpointerEventProcessor"

    const-string v2, "Received EP event without type."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 74
    :cond_1
    :goto_0
    return v0

    .line 69
    :cond_2
    iget-object v1, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v2, Lgkx;->cOv:Lgkx;

    invoke-virtual {v1, v2}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 74
    invoke-virtual {p1}, Ljvr;->getEventType()I

    move-result v0

    goto :goto_0
.end method

.method private declared-synchronized bC(J)Z
    .locals 3

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lgkw;->cOr:I

    if-lez v0, :cond_0

    .line 157
    iget v0, p0, Lgkw;->cOr:I

    .line 163
    :goto_0
    if-lez v0, :cond_1

    .line 166
    iget-object v1, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v2, Lgkx;->cOu:Lgkx;

    invoke-virtual {v1, v2}, Leqx;->a(Ljava/lang/Enum;)V

    .line 167
    int-to-long v0, v0

    add-long/2addr v0, p1

    invoke-direct {p0, v0, v1}, Lgkw;->bD(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    const/4 v0, 0x0

    .line 171
    :goto_1
    monitor-exit p0

    return v0

    .line 160
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgkw;->cOp:Ljzk;

    invoke-virtual {v0}, Ljzk;->bxa()I

    move-result v0

    goto :goto_0

    .line 170
    :cond_1
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOv:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    const/4 v0, 0x1

    goto :goto_1

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized bD(J)V
    .locals 1

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lgkw;->cOo:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    monitor-exit p0

    return-void

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized bE(J)Z
    .locals 3

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lgkw;->cOo:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOu:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOv:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    const/4 v0, 0x1

    .line 203
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized bF(J)Z
    .locals 3

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOs:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkw;->cOp:Ljzk;

    invoke-virtual {v0}, Ljzk;->bwZ()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 210
    iget-object v0, p0, Lgkw;->mStateMachine:Leqx;

    sget-object v1, Lgkx;->cOv:Lgkx;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    const/4 v0, 0x1

    .line 213
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized bB(J)V
    .locals 1

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lgkw;->bE(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lgkw;->cOn:Lglx;

    invoke-interface {v0}, Lglx;->onEndOfSpeech()V

    .line 135
    :cond_0
    invoke-direct {p0, p1, p2}, Lgkw;->bF(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lgkw;->cOn:Lglx;

    invoke-interface {v0}, Lglx;->Nw()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :cond_1
    monitor-exit p0

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Ljvr;)V
    .locals 6

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lgkw;->b(Ljvr;)I

    move-result v0

    .line 80
    if-nez v0, :cond_1

    .line 81
    iget-boolean v0, p0, Lgkw;->cOq:Z

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lgkw;->cOn:Lglx;

    check-cast v0, Lgck;

    invoke-virtual {p1}, Ljvr;->bvc()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lgck;->bw(J)V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 87
    iget-boolean v0, p0, Lgkw;->cOq:Z

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lgkw;->cOn:Lglx;

    check-cast v0, Lgck;

    invoke-virtual {v0}, Lgck;->Ns()V

    goto :goto_0
.end method

.method public final d(Ljvr;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x3e8

    .line 104
    invoke-direct {p0, p1}, Lgkw;->b(Ljvr;)I

    move-result v0

    .line 105
    if-nez v0, :cond_1

    .line 106
    invoke-direct {p0}, Lgkw;->aGZ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lgkw;->cOn:Lglx;

    invoke-virtual {p1}, Ljvr;->bvc()J

    move-result-wide v2

    div-long/2addr v2, v4

    invoke-interface {v0, v2, v3}, Lglx;->au(J)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 111
    invoke-virtual {p1}, Ljvr;->bvc()J

    move-result-wide v0

    div-long/2addr v0, v4

    invoke-direct {p0, v0, v1}, Lgkw;->bC(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lgkw;->cOn:Lglx;

    invoke-interface {v0}, Lglx;->onEndOfSpeech()V

    goto :goto_0

    .line 115
    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 116
    invoke-direct {p0}, Lgkw;->aHa()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 118
    iget-object v0, p0, Lgkw;->cOn:Lglx;

    invoke-interface {v0}, Lglx;->onEndOfSpeech()V

    .line 121
    :cond_3
    invoke-direct {p0}, Lgkw;->aHb()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lgkw;->cOn:Lglx;

    invoke-interface {v0}, Lglx;->Nw()V

    goto :goto_0
.end method

.method public final declared-synchronized kq(I)V
    .locals 1

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lgkw;->cOr:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
