.class public final Ljof;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ewh:[Ljof;


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private aiK:Ljava/lang/String;

.field private dMI:Ljava/lang/String;

.field private ewi:Ljava/lang/String;

.field private ewj:I

.field private ewk:I

.field private ewl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 246
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 247
    iput v1, p0, Ljof;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljof;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljof;->aiK:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljof;->ewi:Ljava/lang/String;

    iput v1, p0, Ljof;->ewj:I

    iput v1, p0, Ljof;->ewk:I

    const-string v0, ""

    iput-object v0, p0, Ljof;->dMI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljof;->ewl:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljof;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljof;->eCz:I

    .line 248
    return-void
.end method

.method public static bre()[Ljof;
    .locals 2

    .prologue
    .line 85
    sget-object v0, Ljof;->ewh:[Ljof;

    if-nez v0, :cond_1

    .line 86
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 88
    :try_start_0
    sget-object v0, Ljof;->ewh:[Ljof;

    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x0

    new-array v0, v0, [Ljof;

    sput-object v0, Ljof;->ewh:[Ljof;

    .line 91
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_1
    sget-object v0, Ljof;->ewh:[Ljof;

    return-object v0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljof;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljof;->afh:Ljava/lang/String;

    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljof;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljof;->aiK:Ljava/lang/String;

    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljof;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljof;->ewi:Ljava/lang/String;

    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljof;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljof;->ewj:I

    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljof;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljof;->ewk:I

    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljof;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljof;->dMI:Ljava/lang/String;

    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljof;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljof;->ewl:Ljava/lang/String;

    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljof;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 267
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 268
    const/4 v0, 0x1

    iget-object v1, p0, Ljof;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 270
    :cond_0
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 271
    const/4 v0, 0x2

    iget-object v1, p0, Ljof;->aiK:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 273
    :cond_1
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 274
    const/4 v0, 0x3

    iget-object v1, p0, Ljof;->ewi:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 276
    :cond_2
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 277
    const/4 v0, 0x4

    iget v1, p0, Ljof;->ewj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 279
    :cond_3
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 280
    const/4 v0, 0x5

    iget v1, p0, Ljof;->ewk:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 282
    :cond_4
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 283
    const/4 v0, 0x6

    iget-object v1, p0, Ljof;->dMI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 285
    :cond_5
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_6

    .line 286
    const/16 v0, 0x8

    iget-object v1, p0, Ljof;->ewl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 288
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 289
    return-void
.end method

.method public final aUK()Z
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final brf()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Ljof;->ewj:I

    return v0
.end method

.method public final brg()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Ljof;->ewk:I

    return v0
.end method

.method public final brh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Ljof;->ewl:Ljava/lang/String;

    return-object v0
.end method

.method public final bri()Z
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Ljof;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Ljof;->aiK:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Ljof;->afh:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Ljof;->dMI:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 293
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 294
    iget v1, p0, Ljof;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 295
    const/4 v1, 0x1

    iget-object v2, p0, Ljof;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_0
    iget v1, p0, Ljof;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 299
    const/4 v1, 0x2

    iget-object v2, p0, Ljof;->aiK:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 302
    :cond_1
    iget v1, p0, Ljof;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 303
    const/4 v1, 0x3

    iget-object v2, p0, Ljof;->ewi:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 306
    :cond_2
    iget v1, p0, Ljof;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 307
    const/4 v1, 0x4

    iget v2, p0, Ljof;->ewj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 310
    :cond_3
    iget v1, p0, Ljof;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 311
    const/4 v1, 0x5

    iget v2, p0, Ljof;->ewk:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 314
    :cond_4
    iget v1, p0, Ljof;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 315
    const/4 v1, 0x6

    iget-object v2, p0, Ljof;->dMI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 318
    :cond_5
    iget v1, p0, Ljof;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_6

    .line 319
    const/16 v1, 0x8

    iget-object v2, p0, Ljof;->ewl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 322
    :cond_6
    return v0
.end method

.method public final qY(I)Ljof;
    .locals 1

    .prologue
    .line 170
    iput p1, p0, Ljof;->ewj:I

    .line 171
    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljof;->aez:I

    .line 172
    return-object p0
.end method

.method public final qZ(I)Ljof;
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x0

    iput v0, p0, Ljof;->ewk:I

    .line 190
    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljof;->aez:I

    .line 191
    return-object p0
.end method

.method public final xA(Ljava/lang/String;)Ljof;
    .locals 1

    .prologue
    .line 126
    if-nez p1, :cond_0

    .line 127
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 129
    :cond_0
    iput-object p1, p0, Ljof;->aiK:Ljava/lang/String;

    .line 130
    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljof;->aez:I

    .line 131
    return-object p0
.end method

.method public final xB(Ljava/lang/String;)Ljof;
    .locals 1

    .prologue
    .line 148
    if-nez p1, :cond_0

    .line 149
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 151
    :cond_0
    iput-object p1, p0, Ljof;->ewi:Ljava/lang/String;

    .line 152
    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljof;->aez:I

    .line 153
    return-object p0
.end method

.method public final xC(Ljava/lang/String;)Ljof;
    .locals 1

    .prologue
    .line 208
    if-nez p1, :cond_0

    .line 209
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 211
    :cond_0
    iput-object p1, p0, Ljof;->dMI:Ljava/lang/String;

    .line 212
    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljof;->aez:I

    .line 213
    return-object p0
.end method

.method public final xg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Ljof;->ewi:Ljava/lang/String;

    return-object v0
.end method

.method public final xz(Ljava/lang/String;)Ljof;
    .locals 1

    .prologue
    .line 104
    if-nez p1, :cond_0

    .line 105
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 107
    :cond_0
    iput-object p1, p0, Ljof;->afh:Ljava/lang/String;

    .line 108
    iget v0, p0, Ljof;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljof;->aez:I

    .line 109
    return-object p0
.end method
