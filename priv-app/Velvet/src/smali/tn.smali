.class public final Ltn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic CH:Lcom/android/launcher3/DeleteDropTarget;

.field private synthetic CI:Landroid/content/ComponentName;

.field private synthetic CJ:Lahz;

.field private synthetic CK:Lui;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/DeleteDropTarget;Landroid/content/ComponentName;Lahz;Lui;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Ltn;->CH:Lcom/android/launcher3/DeleteDropTarget;

    iput-object p2, p0, Ltn;->CI:Landroid/content/ComponentName;

    iput-object p3, p0, Ltn;->CJ:Lahz;

    iput-object p4, p0, Ltn;->CK:Lui;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 320
    iget-object v2, p0, Ltn;->CH:Lcom/android/launcher3/DeleteDropTarget;

    invoke-static {v2, v1}, Lcom/android/launcher3/DeleteDropTarget;->a(Lcom/android/launcher3/DeleteDropTarget;Z)Z

    .line 321
    iget-object v2, p0, Ltn;->CI:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 322
    iget-object v3, p0, Ltn;->CH:Lcom/android/launcher3/DeleteDropTarget;

    invoke-virtual {v3}, Lcom/android/launcher3/DeleteDropTarget;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Ltn;->CJ:Lahz;

    invoke-static {v3}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v3

    invoke-virtual {v3, v2, v4}, Lahn;->i(Ljava/lang/String;Lahz;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    move v2, v0

    :goto_0
    if-nez v2, :cond_0

    move v1, v0

    .line 324
    :cond_0
    iget-object v0, p0, Ltn;->CK:Lui;

    instance-of v0, v0, Lcom/android/launcher3/Folder;

    if-eqz v0, :cond_3

    .line 325
    iget-object v0, p0, Ltn;->CK:Lui;

    check-cast v0, Lcom/android/launcher3/Folder;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Folder;->P(Z)V

    .line 331
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v2, v1

    .line 322
    goto :goto_0

    .line 327
    :cond_3
    iget-object v0, p0, Ltn;->CK:Lui;

    instance-of v0, v0, Lcom/android/launcher3/Workspace;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Ltn;->CK:Lui;

    check-cast v0, Lcom/android/launcher3/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->P(Z)V

    goto :goto_1
.end method
