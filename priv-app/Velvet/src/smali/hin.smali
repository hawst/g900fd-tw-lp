.class final Lhin;
.super Lepm;
.source "PG"


# instance fields
.field private synthetic dic:Lhik;

.field private did:Landroid/media/AudioTrack;

.field private die:I


# direct methods
.method constructor <init>(Lhik;Landroid/media/AudioTrack;I)V
    .locals 3

    .prologue
    .line 283
    iput-object p1, p0, Lhin;->dic:Lhik;

    .line 284
    const-string v0, "AudioTrackSoundManager"

    const-string v1, "Wait for playback"

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-direct {p0, v0, v1, v2}, Lepm;-><init>(Ljava/lang/String;Ljava/lang/String;[I)V

    .line 285
    iput-object p2, p0, Lhin;->did:Landroid/media/AudioTrack;

    .line 286
    iput p3, p0, Lhin;->die:I

    .line 287
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const-wide/16 v4, 0x1f4

    const-wide/16 v8, 0x0

    const/4 v11, -0x1

    .line 294
    iget v0, p0, Lhin;->die:I

    div-int/lit8 v13, v0, 0x2

    move-wide v6, v8

    move v10, v11

    .line 300
    :goto_0
    iget-object v0, p0, Lhin;->did:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v12

    if-ge v12, v13, :cond_0

    iget-object v0, p0, Lhin;->did:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 303
    sub-int v0, v13, v12

    mul-int/lit16 v0, v0, 0x3e8

    div-int/lit16 v0, v0, 0x3e80

    int-to-long v0, v0

    .line 305
    const-wide/16 v2, 0x32

    invoke-static/range {v0 .. v5}, Lhik;->a(JJJ)J

    move-result-wide v2

    .line 310
    if-ne v12, v10, :cond_1

    .line 313
    add-long v0, v6, v2

    .line 315
    cmp-long v6, v0, v4

    if-lez v6, :cond_2

    .line 316
    const-string v0, "AudioTrackSoundManager"

    const-string v1, "Waited unsuccessfully for 500ms for AudioTrack to make progress, Aborting"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_0
    :goto_1
    iget-object v0, p0, Lhin;->did:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 340
    iget-object v0, p0, Lhin;->dic:Lhik;

    invoke-static {v0, v11}, Lhik;->a(Lhik;I)V

    .line 341
    return-void

    :cond_1
    move-wide v0, v8

    .line 332
    :cond_2
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v6, v0

    move v10, v12

    .line 335
    goto :goto_0

    .line 334
    :catch_0
    move-exception v0

    goto :goto_1
.end method
