.class public final Lgux;
.super Lesj;
.source "PG"


# instance fields
.field private bYY:Z

.field buZ:Z

.field private cWX:I

.field private cWY:Z

.field private cWZ:Z

.field private cXa:Landroid/view/View;

.field private final cXb:Lguw;

.field private synthetic cXc:Lguv;


# direct methods
.method constructor <init>(Lguv;Lguw;)V
    .locals 1

    .prologue
    .line 721
    iput-object p1, p0, Lgux;->cXc:Lguv;

    invoke-direct {p0}, Lesj;-><init>()V

    .line 709
    const/4 v0, 0x0

    iput v0, p0, Lgux;->cWX:I

    .line 722
    iput-object p2, p0, Lgux;->cXb:Lguw;

    .line 723
    return-void
.end method

.method private aKJ()I
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lgux;->cXc:Lguv;

    invoke-virtual {v0}, Lguv;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgux;->cXc:Lguv;

    invoke-virtual {v0}, Lguv;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->DD()Lcjs;

    move-result-object v0

    invoke-virtual {v0}, Lcjs;->LF()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 913
    const/4 v0, -0x1

    .line 915
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lejm;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 808
    iget-boolean v0, p0, Lgux;->buZ:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgux;->cWY:Z

    if-nez v0, :cond_1

    .line 908
    :cond_0
    :goto_0
    return-void

    .line 812
    :cond_1
    invoke-interface {p1}, Lejm;->atB()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v5

    .line 814
    iget-object v0, p0, Lgux;->cXc:Lguv;

    invoke-static {}, Lguv;->aKG()V

    .line 816
    iget-boolean v0, p0, Lgux;->cWZ:Z

    if-eqz v0, :cond_2

    .line 818
    invoke-interface {p1}, Lejm;->wj()Lekf;

    move-result-object v0

    invoke-interface {v0, v2}, Lekf;->hG(I)V

    .line 822
    :cond_2
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 823
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 824
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguy;

    .line 825
    invoke-virtual {v0}, Lguy;->aKL()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 827
    iget-object v0, v0, Lguy;->cXi:Lhjs;

    invoke-virtual {v5, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    .line 828
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 832
    :cond_4
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWR:Leds;

    iget-boolean v6, v0, Leds;->bVS:Z

    .line 833
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWR:Leds;

    iget-boolean v3, v0, Leds;->bVT:Z

    iput-boolean v2, v0, Leds;->bVT:Z

    .line 834
    if-nez v3, :cond_5

    if-nez v6, :cond_6

    .line 835
    :cond_5
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWR:Leds;

    iget-object v0, v0, Leds;->bVU:Landroid/widget/TextView;

    .line 836
    if-eqz v0, :cond_6

    .line 838
    invoke-virtual {v5, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    .line 842
    :cond_6
    if-eqz v6, :cond_f

    .line 843
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWR:Leds;

    invoke-virtual {v0}, Leds;->apply()V

    .line 845
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWR:Leds;

    iget-object v0, v0, Leds;->bVU:Landroid/widget/TextView;

    .line 846
    invoke-virtual {v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v5, v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eq v3, v0, :cond_8

    .line 851
    :cond_7
    invoke-direct {p0}, Lgux;->aKJ()I

    move-result v3

    invoke-virtual {v5, v0, v2, v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->f(Landroid/view/View;II)V

    :cond_8
    move v0, v1

    .line 858
    :goto_2
    new-instance v7, Legt;

    invoke-direct {v7}, Legt;-><init>()V

    .line 860
    iget-object v3, p0, Lgux;->cXc:Lguv;

    iget-object v3, v3, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v2

    move v4, v0

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguy;

    .line 861
    invoke-virtual {v0}, Lguy;->aKK()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 862
    invoke-virtual {v0}, Lguy;->aKK()Z

    move-result v2

    invoke-static {v2}, Lifv;->gY(Z)V

    iput v1, v0, Lguy;->ag:I

    .line 864
    iget-object v2, v0, Lguy;->cXi:Lhjs;

    .line 866
    invoke-virtual {v2, v7}, Lhjs;->p(Ljava/lang/Runnable;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 867
    invoke-virtual {v7}, Legt;->aoK()V

    .line 871
    :cond_9
    invoke-direct {p0}, Lgux;->aKJ()I

    move-result v9

    invoke-virtual {v5, v2, v4, v9}, Lcom/google/android/shared/ui/SuggestionGridLayout;->f(Landroid/view/View;II)V

    move v2, v1

    .line 878
    :cond_a
    iget-object v9, v0, Lguy;->cXi:Lhjs;

    invoke-virtual {v9}, Lhjs;->aQj()Z

    move-result v9

    if-eqz v9, :cond_e

    .line 879
    iget-object v0, v0, Lguy;->cXi:Lhjs;

    invoke-virtual {v0}, Lhjs;->aQi()V

    move v0, v1

    .line 882
    :goto_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    .line 883
    goto :goto_3

    .line 886
    :cond_b
    if-eqz v6, :cond_c

    if-le v4, v1, :cond_c

    .line 887
    invoke-virtual {v5, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 891
    :cond_c
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v1, p0, Lgux;->cXa:Landroid/view/View;

    invoke-virtual {v0, v1}, Lguv;->bB(Landroid/view/View;)V

    .line 894
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GR()Z

    move-result v0

    if-eqz v0, :cond_d

    if-eqz v2, :cond_d

    iget-object v0, p0, Lgux;->cXb:Lguw;

    if-eqz v0, :cond_d

    .line 901
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->mActionCardEventLogger:Legs;

    iget-object v1, p0, Lgux;->cXc:Lguv;

    invoke-virtual {v1}, Lguv;->aLt()Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lgux;->cXb:Lguw;

    iget-object v2, v2, Lguw;->cWW:Legu;

    invoke-virtual {v7, v0, v1, v2}, Legt;->a(Legs;Landroid/view/ViewGroup;Legu;)V

    goto/16 :goto_0

    .line 905
    :cond_d
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GR()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 906
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->mActionCardEventLogger:Legs;

    iget-object v1, p0, Lgux;->cXc:Lguv;

    invoke-virtual {v1}, Lguv;->aLt()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Legt;->a(Legs;Landroid/view/ViewGroup;)V

    goto/16 :goto_0

    :cond_e
    move v0, v3

    goto :goto_4

    :cond_f
    move v0, v2

    goto/16 :goto_2
.end method

.method public final avD()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 728
    iget-boolean v0, p0, Lgux;->buZ:Z

    if-eqz v0, :cond_1

    .line 802
    :cond_0
    :goto_0
    return v2

    .line 732
    :cond_1
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->bth:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIP()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lgux;->bYY:Z

    .line 737
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWR:Leds;

    iget-boolean v0, v0, Leds;->bVS:Z

    if-eqz v0, :cond_e

    .line 738
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWR:Leds;

    iget-object v1, p0, Lgux;->cXc:Lguv;

    invoke-virtual {v1}, Lguv;->aLH()Lgyz;

    move-result-object v1

    invoke-virtual {v1}, Lgyz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Leds;->a(Landroid/view/LayoutInflater;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v1, v3

    .line 743
    :goto_2
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWR:Leds;

    iget-object v0, v0, Leds;->bVU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 745
    iget-boolean v4, p0, Lgux;->bYY:Z

    if-eqz v4, :cond_4

    sget-object v4, Lekn;->cdQ:Lekn;

    :goto_3
    iput-object v4, v0, Lekm;->cdz:Lekn;

    .line 749
    :goto_4
    iget-boolean v0, p0, Lgux;->cWY:Z

    iget-object v4, p0, Lgux;->cXc:Lguv;

    iget-object v4, v4, Lguv;->cWR:Leds;

    iget-boolean v4, v4, Leds;->bbz:Z

    or-int/2addr v0, v4

    iput-boolean v0, p0, Lgux;->cWY:Z

    .line 751
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v4, v1

    .line 753
    :goto_5
    if-eqz v4, :cond_c

    iget v0, p0, Lgux;->cWX:I

    if-ge v0, v5, :cond_c

    .line 754
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWQ:Ljava/util/List;

    iget v1, p0, Lgux;->cWX:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lguy;

    .line 756
    invoke-virtual {v1}, Lguy;->aKL()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 758
    iput-boolean v2, p0, Lgux;->cWY:Z

    .line 799
    :cond_2
    :goto_6
    iget v0, p0, Lgux;->cWX:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgux;->cWX:I

    goto :goto_5

    :cond_3
    move v0, v3

    .line 732
    goto :goto_1

    .line 745
    :cond_4
    sget-object v4, Lekn;->cdM:Lekn;

    goto :goto_3

    .line 759
    :cond_5
    invoke-virtual {v1}, Lguy;->aKL()Z

    move-result v0

    if-nez v0, :cond_2

    .line 762
    iget-object v0, v1, Lguy;->cXi:Lhjs;

    if-nez v0, :cond_7

    move v0, v2

    :goto_7
    if-eqz v0, :cond_a

    .line 764
    iget-object v0, p0, Lgux;->cXc:Lguv;

    iget-object v0, v0, Lguv;->cWP:Lhgz;

    iget-object v4, v1, Lguy;->cXh:Lhjo;

    invoke-virtual {v0, v4}, Lhgz;->a(Lhjo;)Lhjs;

    move-result-object v4

    .line 766
    iget-object v0, v1, Lguy;->cXi:Lhjs;

    if-nez v0, :cond_8

    move v0, v2

    :goto_8
    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-static {v4}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjs;

    iput-object v0, v1, Lguy;->cXi:Lhjs;

    .line 771
    invoke-virtual {v4}, Lhjs;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_6

    .line 772
    new-instance v6, Lekm;

    const/4 v0, -0x1

    const/4 v7, -0x2

    invoke-direct {v6, v0, v7, v3}, Lekm;-><init>(III)V

    .line 776
    sget-object v0, Lekn;->cdM:Lekn;

    iput-object v0, v6, Lekm;->cdz:Lekn;

    .line 781
    iput-boolean v3, v6, Lekm;->cdx:Z

    .line 782
    iget-boolean v0, p0, Lgux;->bYY:Z

    if-eqz v0, :cond_9

    sget-object v0, Lekn;->cdO:Lekn;

    :goto_9
    iput-object v0, v6, Lekm;->cdz:Lekn;

    .line 786
    invoke-virtual {v4, v6}, Lhjs;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 789
    :cond_6
    iput-boolean v2, p0, Lgux;->cWZ:Z

    .line 790
    iput-boolean v2, p0, Lgux;->cWY:Z

    move v0, v3

    .line 797
    :goto_a
    iget-object v1, v1, Lguy;->cXi:Lhjs;

    iput-object v1, p0, Lgux;->cXa:Landroid/view/View;

    move v4, v0

    goto :goto_6

    :cond_7
    move v0, v3

    .line 762
    goto :goto_7

    :cond_8
    move v0, v3

    .line 766
    goto :goto_8

    .line 782
    :cond_9
    sget-object v0, Lekn;->cdK:Lekn;

    goto :goto_9

    .line 794
    :cond_a
    invoke-virtual {v1}, Lguy;->aKK()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 795
    iput-boolean v2, p0, Lgux;->cWZ:Z

    :cond_b
    move v0, v4

    goto :goto_a

    .line 802
    :cond_c
    iget v0, p0, Lgux;->cWX:I

    if-eq v0, v5, :cond_0

    move v2, v3

    goto/16 :goto_0

    :cond_d
    move v1, v2

    goto/16 :goto_2

    :cond_e
    move v1, v2

    goto/16 :goto_4
.end method
