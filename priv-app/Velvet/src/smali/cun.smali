.class public final Lcun;
.super Lbdz;
.source "PG"


# instance fields
.field private final bjJ:Lcsx;

.field private final bjK:Lcto;

.field private final bjL:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbdy;)V
    .locals 7

    .prologue
    .line 437
    const-string v2, "icingcorpora.db"

    const/4 v3, 0x0

    const/16 v4, 0xe

    invoke-static {}, Lcuo;->Tc()[Lbem;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lbdz;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I[Lbem;Lbdy;)V

    .line 439
    new-instance v0, Lcsx;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Lere;

    invoke-direct {v2, p1}, Lere;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcuv;

    invoke-direct {v3}, Lcuv;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcsx;-><init>(Landroid/content/pm/PackageManager;Lemp;Lcuv;)V

    iput-object v0, p0, Lcun;->bjJ:Lcsx;

    .line 442
    new-instance v0, Lcto;

    new-instance v1, Lcub;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, Lcub;-><init>(Landroid/content/ContentResolver;)V

    invoke-direct {v0, v1}, Lcto;-><init>(Lcub;)V

    iput-object v0, p0, Lcun;->bjK:Lcto;

    .line 444
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcun;->bjL:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 445
    return-void
.end method

.method private a(ILbem;)V
    .locals 4

    .prologue
    .line 649
    if-lez p1, :cond_0

    .line 651
    invoke-direct {p0, p2}, Lcun;->b(Lbem;)Z

    move-result v0

    .line 652
    if-nez v0, :cond_0

    .line 653
    const-string v0, "IcingCorporaProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Table change notification failed for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 656
    :cond_0
    return-void
.end method

.method private static varargs b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 681
    const-string v0, "table"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "view"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "trigger"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v4

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 683
    const-string v1, "SQLITE_MASTER"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "name"

    aput-object v0, v2, v6

    const-string v3, "type == ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 686
    :try_start_0
    invoke-static {p2}, Liqs;->m([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    .line 687
    :cond_1
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 688
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 689
    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 694
    if-nez v3, :cond_1

    .line 695
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DROP "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 699
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move v0, v6

    .line 681
    goto :goto_0

    .line 699
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 700
    return-void
.end method

.method private b(Lbem;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 660
    :try_start_0
    iget-object v1, p0, Lbea;->axA:Lbdy;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbea;->axA:Lbdy;

    invoke-interface {v1, p1}, Lbdy;->c(Lbej;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 665
    :goto_0
    return v0

    .line 660
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 661
    :catch_0
    move-exception v1

    .line 662
    const-string v2, "IcingCorporaProvider"

    const-string v3, "Exception thrown from notifyTableChanged"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 663
    invoke-static {}, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->Tb()Z

    .line 664
    const-string v1, "IcingCorporaProvider"

    const-string v2, "notifyTableChanged failed."

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .prologue
    .line 605
    const-string v0, "delta"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-virtual {v0, p1, p2}, Lcto;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;)I

    move-result v0

    .line 628
    :goto_0
    sget-object v1, Lcuo;->bjN:Lcuo;

    invoke-virtual {v1}, Lcuo;->SZ()Lbem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcun;->a(ILbem;)V

    .line 631
    sget-object v1, Lcuo;->bjO:Lcuo;

    invoke-virtual {v1}, Lcuo;->SZ()Lbem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcun;->a(ILbem;)V

    .line 633
    sget-object v1, Lcuo;->bjP:Lcuo;

    invoke-virtual {v1}, Lcuo;->SZ()Lbem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcun;->a(ILbem;)V

    .line 635
    sget-object v1, Lcuo;->bjQ:Lcuo;

    invoke-virtual {v1}, Lcuo;->SZ()Lbem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcun;->a(ILbem;)V

    .line 637
    return v0

    .line 607
    :cond_0
    const-string v0, "ids"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p4, :cond_2

    .line 608
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 609
    array-length v2, p4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, p4, v0

    .line 610
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 609
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 612
    :cond_1
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-virtual {v0, p1, p2, v1}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;Ljava/util/Collection;)I

    move-result v0

    goto :goto_0

    .line 613
    :cond_2
    if-nez p3, :cond_3

    .line 614
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auU()J

    move-result-wide v2

    .line 615
    const/16 v0, 0x11a

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 619
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-virtual {v0, p1, p2}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;)I

    move-result v0

    .line 621
    const/16 v1, 0x11b

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    goto :goto_0

    .line 625
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unrecognized selection"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;Lcjs;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcun;->bjJ:Lcsx;

    invoke-virtual {v0, p1, p2}, Lcsx;->a(Landroid/database/sqlite/SQLiteDatabase;Lcjs;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8

    .prologue
    const/4 v0, 0x7

    const/4 v1, 0x6

    const/4 v2, 0x5

    const/4 v4, 0x3

    const/4 v3, 0x4

    .line 465
    const-string v5, "IcingCorporaProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Upgrading DB from "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v5, v6, v7}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 466
    if-ge p2, v4, :cond_b

    .line 467
    iget-object v5, p0, Lcun;->bjK:Lcto;

    invoke-static {p1}, Lcto;->x(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 470
    :goto_0
    if-ge v4, v3, :cond_a

    .line 471
    iget-object v4, p0, Lcun;->bjK:Lcto;

    invoke-static {p1}, Lcto;->y(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 474
    :goto_1
    if-ge v3, v2, :cond_9

    .line 475
    iget-object v3, p0, Lcun;->bjK:Lcto;

    invoke-static {p1}, Lcto;->z(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 478
    :goto_2
    if-ge v2, v1, :cond_8

    .line 479
    iget-object v2, p0, Lcun;->bjJ:Lcsx;

    invoke-static {p1}, Lcsx;->l(Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object v2, p0, Lcun;->bjK:Lcto;

    invoke-static {p1}, Lcto;->l(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 482
    :goto_3
    if-ge v1, v0, :cond_7

    .line 483
    iget-object v1, p0, Lcun;->bjJ:Lcsx;

    invoke-virtual {v1, p1}, Lcsx;->m(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 486
    :goto_4
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 487
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-static {p1}, Lcto;->A(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 488
    const/16 v0, 0x8

    .line 490
    :cond_0
    const/16 v1, 0x9

    if-ge v0, v1, :cond_1

    .line 491
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-static {p1}, Lcto;->B(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 492
    const/16 v0, 0x9

    .line 494
    :cond_1
    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 495
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-virtual {v0, p1}, Lcto;->C(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 496
    const/16 v0, 0xa

    .line 498
    :cond_2
    const/16 v1, 0xb

    if-ge v0, v1, :cond_3

    .line 499
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-static {p1}, Lcto;->D(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 500
    const/16 v0, 0xb

    .line 502
    :cond_3
    const/16 v1, 0xc

    if-ge v0, v1, :cond_4

    .line 503
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-static {p1}, Lcto;->E(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 504
    const/16 v0, 0xc

    .line 506
    :cond_4
    const/16 v1, 0xd

    if-ge v0, v1, :cond_5

    .line 507
    iget-object v0, p0, Lcun;->bjJ:Lcsx;

    invoke-static {p1}, Lcsx;->o(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 508
    const/16 v0, 0xd

    .line 510
    :cond_5
    const/16 v1, 0xe

    if-ge v0, v1, :cond_6

    .line 511
    iget-object v0, p0, Lcun;->bjJ:Lcsx;

    invoke-static {p1}, Lcsx;->p(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 512
    :cond_6
    return-void

    :cond_7
    move v0, v1

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_3

    :cond_9
    move v2, v3

    goto :goto_2

    :cond_a
    move v3, v4

    goto :goto_1

    :cond_b
    move v4, p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 704
    invoke-virtual {p0}, Lcun;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 705
    new-array v0, v10, [Ljava/lang/Object;

    aput-object p1, v0, v8

    const-string v2, "Max seqnos:"

    aput-object v2, v0, v9

    invoke-static {p2, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 706
    sget-object v0, Lcum;->bjw:Lijp;

    invoke-virtual {v0}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcum;

    .line 707
    invoke-virtual {v0}, Lcum;->SZ()Lbem;

    move-result-object v0

    .line 708
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v8

    const-string v4, "  "

    aput-object v4, v3, v9

    invoke-virtual {v0}, Lbem;->xF()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x3

    const-string v5, ": "

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-virtual {p0, v0}, Lcun;->b(Lbej;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {p2, v3}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    goto :goto_0

    .line 710
    :cond_0
    new-array v0, v8, [Ljava/lang/Object;

    invoke-static {p2, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 712
    iget-object v0, p0, Lcun;->bjJ:Lcsx;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcsx;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 713
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 714
    return-void
.end method

.method public final g(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 582
    if-nez p2, :cond_0

    .line 583
    sget-object v0, Leoi;->cgG:Leoi;

    invoke-virtual {v0}, Leoi;->auU()J

    move-result-wide v2

    .line 584
    const/16 v0, 0x118

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 588
    iget-object v0, p0, Lcun;->bjJ:Lcsx;

    invoke-virtual {v0, p1}, Lcsx;->r(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 590
    const/16 v1, 0x119

    invoke-static {v1}, Lege;->hs(I)Litu;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Litu;->bW(J)Litu;

    move-result-object v1

    invoke-static {v1}, Lege;->a(Litu;)V

    .line 597
    :goto_0
    sget-object v1, Lcuo;->bjM:Lcuo;

    invoke-virtual {v1}, Lcuo;->SZ()Lbem;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcun;->a(ILbem;)V

    .line 599
    return v0

    .line 594
    :cond_0
    iget-object v0, p0, Lcun;->bjJ:Lcsx;

    invoke-virtual {v0, p1, p2}, Lcsx;->d(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final g(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcun;->bjJ:Lcsx;

    invoke-virtual {v0, p1}, Lcsx;->k(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 450
    iget-object v0, p0, Lcun;->bjK:Lcto;

    invoke-virtual {v0, p1}, Lcto;->t(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 451
    return-void
.end method

.method public final getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 4

    .prologue
    .line 568
    :try_start_0
    invoke-super {p0}, Lbdz;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteCantOpenDatabaseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 575
    :goto_0
    return-object v0

    .line 569
    :catch_0
    move-exception v0

    .line 571
    const-string v1, "IcingCorporaProvider"

    const-string v2, "Failed to get a writable database."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Leor;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 572
    iget-object v0, p0, Lcun;->bjL:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 573
    const v0, 0xc5a89b

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 575
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 457
    const-string v0, "IcingCorporaProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Downgrading DB from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 459
    const-string v0, "IcingCorporaProvider"

    const-string v1, "Clearing all tables, triggers and views on db."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const-string v0, "table"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "sqlite_sequence"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string v3, "android_metadata"

    aput-object v3, v1, v2

    invoke-static {p1, v0, v1}, Lcun;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "trigger"

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcun;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "view"

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcun;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)V

    .line 460
    invoke-virtual {p0, p1}, Lcun;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 461
    return-void
.end method

.method public final q(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcun;->bjJ:Lcsx;

    invoke-static {p1}, Lcsx;->q(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 642
    return-void
.end method
