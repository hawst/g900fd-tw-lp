.class public final Lcvx;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mGsaPreferenceController:Lchr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Lchr;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcyc;-><init>()V

    .line 25
    iput-object p1, p0, Lcvx;->mContext:Landroid/content/Context;

    .line 26
    iput-object p3, p0, Lcvx;->mGsaPreferenceController:Lchr;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 45
    const-string v0, "gel_usage_stats"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 48
    :cond_0
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    const-string v1, "gel_usage_stats"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    iget-object v1, p0, Lcvx;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lhgn;->bI(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 40
    :cond_0
    return v0
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 52
    const-string v0, "gel_usage_stats"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcvx;->mGsaPreferenceController:Lchr;

    iget-object v1, v1, Lchr;->mGelStartupPrefs:Ldku;

    const-string v2, "GEL.GSAPrefs.log_gel_events"

    invoke-virtual {v1, v2, v0}, Ldku;->m(Ljava/lang/String;Z)V

    .line 55
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
