.class final Lkam;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final XT:Ljava/lang/String;

.field final ePA:[Ljava/security/cert/Certificate;

.field final ePv:Lkcq;

.field final ePw:Ljava/lang/String;

.field final ePx:Lkcq;

.field final ePy:Ljava/lang/String;

.field final ePz:[Ljava/security/cert/Certificate;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    :try_start_0
    new-instance v2, Lkbr;

    sget-object v1, Lkbt;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v1}, Lkbr;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 489
    invoke-virtual {v2}, Lkbr;->readLine()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkam;->XT:Ljava/lang/String;

    .line 490
    invoke-virtual {v2}, Lkbr;->readLine()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkam;->ePw:Ljava/lang/String;

    .line 491
    new-instance v1, Lkcq;

    invoke-direct {v1}, Lkcq;-><init>()V

    iput-object v1, p0, Lkam;->ePv:Lkcq;

    .line 492
    invoke-virtual {v2}, Lkbr;->readInt()I

    move-result v3

    move v1, v0

    .line 493
    :goto_0
    if-ge v1, v3, :cond_0

    .line 494
    iget-object v4, p0, Lkam;->ePv:Lkcq;

    invoke-virtual {v2}, Lkbr;->readLine()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lkcq;->AA(Ljava/lang/String;)V

    .line 493
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 497
    :cond_0
    new-instance v1, Lkcq;

    invoke-direct {v1}, Lkcq;-><init>()V

    iput-object v1, p0, Lkam;->ePx:Lkcq;

    .line 498
    iget-object v1, p0, Lkam;->ePx:Lkcq;

    invoke-virtual {v2}, Lkbr;->readLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lkcq;->Az(Ljava/lang/String;)V

    .line 499
    invoke-virtual {v2}, Lkbr;->readInt()I

    move-result v1

    .line 500
    :goto_1
    if-ge v0, v1, :cond_1

    .line 501
    iget-object v3, p0, Lkam;->ePx:Lkcq;

    invoke-virtual {v2}, Lkbr;->readLine()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkcq;->AA(Ljava/lang/String;)V

    .line 500
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 504
    :cond_1
    invoke-virtual {p0}, Lkam;->bxJ()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 505
    invoke-virtual {v2}, Lkbr;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 506
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 507
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "expected \"\" but was \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0

    .line 509
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lkbr;->readLine()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkam;->ePy:Ljava/lang/String;

    .line 510
    invoke-static {v2}, Lkam;->a(Lkbr;)[Ljava/security/cert/Certificate;

    move-result-object v0

    iput-object v0, p0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    .line 511
    invoke-static {v2}, Lkam;->a(Lkbr;)[Ljava/security/cert/Certificate;

    move-result-object v0

    iput-object v0, p0, Lkam;->ePA:[Ljava/security/cert/Certificate;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 518
    :goto_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 519
    return-void

    .line 513
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lkam;->ePy:Ljava/lang/String;

    .line 514
    const/4 v0, 0x0

    iput-object v0, p0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    .line 515
    const/4 v0, 0x0

    iput-object v0, p0, Lkam;->ePA:[Ljava/security/cert/Certificate;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public constructor <init>(Ljava/net/URI;Lkcq;Ljava/net/HttpURLConnection;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524
    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkam;->XT:Ljava/lang/String;

    .line 525
    iput-object p2, p0, Lkam;->ePv:Lkcq;

    .line 526
    invoke-virtual {p3}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkam;->ePw:Ljava/lang/String;

    .line 527
    invoke-virtual {p3}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lkcq;->a(Ljava/util/Map;Z)Lkcq;

    move-result-object v0

    iput-object v0, p0, Lkam;->ePx:Lkcq;

    .line 529
    instance-of v0, p3, Lkcm;

    if-eqz v0, :cond_0

    check-cast p3, Lkcm;

    invoke-virtual {p3}, Lkcm;->byK()Lkcc;

    move-result-object v0

    :goto_0
    instance-of v2, v0, Lkcl;

    if-eqz v2, :cond_1

    check-cast v0, Lkcl;

    invoke-virtual {v0}, Lkcl;->byN()Ljavax/net/ssl/SSLSocket;

    move-result-object v0

    move-object v2, v0

    .line 530
    :goto_1
    if-eqz v2, :cond_2

    .line 531
    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getCipherSuite()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkam;->ePy:Ljava/lang/String;

    .line 534
    :try_start_0
    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 537
    :goto_2
    iput-object v0, p0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    .line 538
    invoke-virtual {v2}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-interface {v0}, Ljavax/net/ssl/SSLSession;->getLocalCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    iput-object v0, p0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    .line 544
    :goto_3
    return-void

    .line 529
    :cond_0
    check-cast p3, Lkcj;

    invoke-virtual {p3}, Lkcj;->byK()Lkcc;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_2

    .line 540
    :cond_2
    iput-object v1, p0, Lkam;->ePy:Ljava/lang/String;

    .line 541
    iput-object v1, p0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    .line 542
    iput-object v1, p0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    goto :goto_3
.end method

.method private static a(Ljava/io/Writer;[Ljava/security/cert/Certificate;)V
    .locals 4

    .prologue
    .line 612
    if-nez p1, :cond_1

    .line 613
    const-string v0, "-1\n"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 625
    :cond_0
    return-void

    .line 617
    :cond_1
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    array-length v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 618
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 619
    invoke-virtual {v2}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v2

    .line 620
    invoke-static {v2}, Lkba;->aH([B)Ljava/lang/String;

    move-result-object v2

    .line 621
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 623
    :catch_0
    move-exception v0

    .line 624
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/security/cert/CertificateEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static a(Lkbr;)[Ljava/security/cert/Certificate;
    .locals 5

    .prologue
    .line 593
    invoke-virtual {p0}, Lkbr;->readInt()I

    move-result v0

    .line 594
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 595
    const/4 v0, 0x0

    .line 605
    :cond_0
    return-object v0

    .line 598
    :cond_1
    :try_start_0
    const-string v1, "X.509"

    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v2

    .line 599
    new-array v0, v0, [Ljava/security/cert/Certificate;

    .line 600
    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 601
    invoke-virtual {p0}, Lkbr;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 602
    const-string v4, "US-ASCII"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lkba;->decode([B)[B

    move-result-object v3

    .line 603
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v2, v4}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v3

    aput-object v3, v0, v1
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 606
    :catch_0
    move-exception v0

    .line 607
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final b(Lkbe;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0xa

    .line 563
    invoke-virtual {p1, v1}, Lkbe;->tG(I)Ljava/io/OutputStream;

    move-result-object v0

    .line 564
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/OutputStreamWriter;

    sget-object v4, Lkbt;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v3, v0, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 566
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lkam;->XT:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 567
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lkam;->ePw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 568
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lkam;->ePv:Lkcq;

    invoke-virtual {v3}, Lkcq;->length()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    move v0, v1

    .line 569
    :goto_0
    iget-object v3, p0, Lkam;->ePv:Lkcq;

    invoke-virtual {v3}, Lkcq;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 570
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lkam;->ePv:Lkcq;

    invoke-virtual {v4, v0}, Lkcq;->tL(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lkam;->ePv:Lkcq;

    invoke-virtual {v4, v0}, Lkcq;->getValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 569
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 573
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lkam;->ePx:Lkcq;

    invoke-virtual {v3}, Lkcq;->byP()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 574
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lkam;->ePx:Lkcq;

    invoke-virtual {v3}, Lkcq;->length()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 575
    :goto_1
    iget-object v0, p0, Lkam;->ePx:Lkcq;

    invoke-virtual {v0}, Lkcq;->length()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lkam;->ePx:Lkcq;

    invoke-virtual {v3, v1}, Lkcq;->tL(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lkam;->ePx:Lkcq;

    invoke-virtual {v3, v1}, Lkcq;->getValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 575
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 579
    :cond_1
    invoke-virtual {p0}, Lkam;->bxJ()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 580
    invoke-virtual {v2, v5}, Ljava/io/Writer;->write(I)V

    .line 581
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lkam;->ePy:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 582
    iget-object v0, p0, Lkam;->ePz:[Ljava/security/cert/Certificate;

    invoke-static {v2, v0}, Lkam;->a(Ljava/io/Writer;[Ljava/security/cert/Certificate;)V

    .line 583
    iget-object v0, p0, Lkam;->ePA:[Ljava/security/cert/Certificate;

    invoke-static {v2, v0}, Lkam;->a(Ljava/io/Writer;[Ljava/security/cert/Certificate;)V

    .line 585
    :cond_2
    invoke-virtual {v2}, Ljava/io/Writer;->close()V

    .line 586
    return-void
.end method

.method bxJ()Z
    .locals 2

    .prologue
    .line 589
    iget-object v0, p0, Lkam;->XT:Ljava/lang/String;

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
