.class public final Lgca;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mClock:Lemp;

.field public final mRoute:Liyg;


# direct methods
.method public constructor <init>(Liyg;Lemp;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyg;

    iput-object v0, p0, Lgca;->mRoute:Liyg;

    .line 71
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemp;

    iput-object v0, p0, Lgca;->mClock:Lemp;

    .line 72
    return-void
.end method

.method public static a(Liyk;)J
    .locals 10

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    .line 844
    iget-object v0, p0, Liyk;->dQw:[J

    array-length v0, v0

    if-nez v0, :cond_0

    .line 845
    const-string v0, "TravelReport"

    const-string v1, "No arrival time is set in a transit step. This should not happen!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    :cond_0
    const/4 v0, 0x0

    move-wide v2, v4

    :goto_0
    iget-object v1, p0, Liyk;->dQw:[J

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 849
    iget-object v1, p0, Liyk;->dQw:[J

    array-length v1, v1

    if-gt v1, v0, :cond_1

    const-string v1, "TravelReport"

    const-string v6, "Steps out of bound error"

    invoke-static {v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v6, v4

    .line 850
    :goto_1
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 848
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 849
    :cond_1
    iget-object v1, p0, Liyk;->dQu:[J

    array-length v1, v1

    if-le v1, v0, :cond_2

    iget-object v1, p0, Liyk;->dQw:[J

    aget-wide v6, v1, v0

    iget-object v1, p0, Liyk;->dQu:[J

    aget-wide v8, v1, v0

    add-long/2addr v6, v8

    goto :goto_1

    :cond_2
    iget-object v1, p0, Liyk;->dQw:[J

    aget-wide v6, v1, v0

    goto :goto_1

    .line 852
    :cond_3
    return-wide v2
.end method

.method public static a(Landroid/content/Context;Liyi;J)Ljava/lang/String;
    .locals 8

    .prologue
    .line 625
    const/4 v3, 0x0

    .line 626
    const/4 v2, 0x0

    .line 627
    const/4 v1, 0x0

    .line 628
    const-string v0, ""

    .line 630
    invoke-virtual {p1}, Liyi;->getType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 659
    const-string v4, "TravelReport"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unknown Alert Type:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Liyi;->getType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    :goto_0
    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v4

    cmp-long v4, p2, v4

    if-ltz v4, :cond_0

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v4

    cmp-long v4, p2, v4

    if-gez v4, :cond_0

    .line 663
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v4, "<b>"

    aput-object v4, v1, v2

    const/4 v2, 0x1

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x2

    const-string v2, "</b>"

    aput-object v2, v1, v0

    const/4 v0, 0x3

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    const/4 v2, 0x0

    invoke-static {p0, v4, v5, v2}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p0, v3, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 679
    :goto_1
    return-object v0

    .line 632
    :pswitch_0
    const v0, 0x7f0a0192

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "<b>"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Liyi;->sl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "</b>"

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 638
    :pswitch_1
    const v3, 0x7f0a0195

    .line 639
    const v2, 0x7f0a0196

    .line 640
    const v1, 0x7f0a018f

    .line 641
    iget-object v0, p1, Liyi;->dPR:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 645
    :pswitch_2
    const v3, 0x7f0a0199

    .line 646
    const v2, 0x7f0a019a

    .line 647
    const v1, 0x7f0a0191

    .line 648
    invoke-virtual {p1}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 652
    :pswitch_3
    const v3, 0x7f0a0197

    .line 653
    const v2, 0x7f0a0198

    .line 654
    const v1, 0x7f0a0190

    .line 655
    invoke-virtual {p1}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 669
    :cond_0
    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v4

    cmp-long v3, p2, v4

    if-gez v3, :cond_1

    .line 670
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "<b>"

    aput-object v4, v1, v3

    const/4 v3, 0x1

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    const/4 v0, 0x2

    const-string v3, "</b>"

    aput-object v3, v1, v0

    const/4 v0, 0x3

    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    const/4 v3, 0x0

    invoke-static {p0, v4, v5, v3}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v0

    const/4 v0, 0x4

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    const/4 v3, 0x0

    invoke-static {p0, v4, v5, v3}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 679
    :cond_1
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "<b>"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v3, "</b>"

    aput-object v3, v2, v0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 630
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Liyk;)J
    .locals 10

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    .line 859
    iget-object v0, p0, Liyk;->dQt:[J

    array-length v0, v0

    if-nez v0, :cond_0

    .line 860
    const-string v0, "TravelReport"

    const-string v1, "No departure time is set in a transit step. This should not happen!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    :cond_0
    const/4 v0, 0x0

    move-wide v2, v4

    :goto_0
    iget-object v1, p0, Liyk;->dQt:[J

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 864
    iget-object v1, p0, Liyk;->dQt:[J

    array-length v1, v1

    if-gt v1, v0, :cond_1

    const-string v1, "TravelReport"

    const-string v6, "Steps out of bound error"

    invoke-static {v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v6, v4

    .line 865
    :goto_1
    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 863
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 864
    :cond_1
    iget-object v1, p0, Liyk;->dQu:[J

    array-length v1, v1

    if-le v1, v0, :cond_2

    iget-object v1, p0, Liyk;->dQt:[J

    aget-wide v6, v1, v0

    iget-object v1, p0, Liyk;->dQu:[J

    aget-wide v8, v1, v0

    add-long/2addr v6, v8

    goto :goto_1

    :cond_2
    iget-object v1, p0, Liyk;->dQt:[J

    aget-wide v6, v1, v0

    goto :goto_1

    .line 867
    :cond_3
    return-wide v2
.end method

.method public static b(Landroid/content/Context;Liyi;J)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x2

    const-wide/16 v8, 0x3e8

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 690
    invoke-virtual {p1}, Liyi;->getType()I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-object v0, p1, Liyi;->dPR:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    .line 694
    :goto_0
    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v2

    cmp-long v1, p2, v2

    if-ltz v1, :cond_1

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v2

    cmp-long v1, p2, v2

    if-gez v1, :cond_1

    .line 695
    const v1, 0x7f0a019b

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "<b>"

    aput-object v3, v2, v6

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    const-string v0, "</b>"

    aput-object v0, v2, v5

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {p0, v4, v5, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 712
    :goto_1
    return-object v0

    .line 690
    :cond_0
    invoke-virtual {p1}, Liyi;->sl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 701
    :cond_1
    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v2

    cmp-long v1, p2, v2

    if-gez v1, :cond_2

    .line 702
    const v1, 0x7f0a019c

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "<b>"

    aput-object v3, v2, v6

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    const-string v0, "</b>"

    aput-object v0, v2, v5

    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {p0, v4, v5, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v2, v7

    const/4 v0, 0x4

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {p0, v4, v5, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 712
    :cond_2
    const-string v1, "<b>%s</b>"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private bu(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 378
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbB()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 379
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bby()I

    move-result v0

    iget-object v1, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v1}, Liyg;->bbA()I

    move-result v1

    sub-int v1, v0, v1

    .line 382
    if-nez v1, :cond_0

    .line 383
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f100025

    iget-object v2, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v2}, Liyg;->bby()I

    move-result v2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v4}, Liyg;->bby()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 400
    :goto_0
    return-object v0

    .line 387
    :cond_0
    if-lez v1, :cond_1

    const v0, 0x7f100023

    .line 390
    :goto_1
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 391
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 387
    :cond_1
    const v0, 0x7f100024

    goto :goto_1

    .line 395
    :cond_2
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbz()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 396
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bby()I

    move-result v0

    .line 397
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100022

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 400
    :cond_3
    const-string v0, ""

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Liyi;J)Ljava/lang/String;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const-wide/16 v6, 0x3e8

    const/4 v4, 0x0

    .line 728
    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_0

    .line 729
    const v0, 0x7f0a0193

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v2

    mul-long/2addr v2, v6

    invoke-static {p0, v2, v3, v4}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 739
    :goto_0
    return-object v0

    .line 732
    :cond_0
    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_1

    .line 733
    const v0, 0x7f0a0194

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Liyi;->nk()J

    move-result-wide v2

    mul-long/2addr v2, v6

    invoke-static {p0, v2, v3, v4}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1}, Liyi;->nm()J

    move-result-wide v2

    mul-long/2addr v2, v6

    invoke-static {p0, v2, v3, v4}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 739
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static jW(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 204
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "#%1$h"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0xffffff

    and-int/2addr v4, p0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 749
    invoke-virtual {p0, p1}, Lgca;->bv(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 750
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 751
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 752
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 756
    :goto_0
    return-void

    .line 754
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final aDF()Liyh;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 233
    invoke-virtual {p0}, Lgca;->aEz()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 234
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    .line 236
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aEA()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbF()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const/4 v0, -0x1

    .line 90
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->aEA()I

    move-result v0

    goto :goto_0
.end method

.method public final aEB()Ljava/lang/Integer;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lgca;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lgca;->d(JZ)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final aEC()Ljava/lang/Integer;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 284
    iget-object v0, p0, Lgca;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lgca;->bu(J)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final aED()Liyk;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 358
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    .line 359
    if-eqz v0, :cond_0

    iget-object v2, v0, Liyh;->dQi:[Liyk;

    array-length v2, v2

    if-nez v2, :cond_2

    :cond_0
    move-object v0, v1

    .line 373
    :cond_1
    :goto_0
    return-object v0

    .line 362
    :cond_2
    iget-object v3, v0, Liyh;->dQi:[Liyk;

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v0, v3, v2

    .line 363
    invoke-virtual {p0, v0}, Lgca;->e(Liyk;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 366
    invoke-virtual {p0, v0}, Lgca;->c(Liyk;)Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 369
    goto :goto_0

    .line 362
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 373
    goto :goto_0
.end method

.method public final aEE()Liyk;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v0, 0x0

    .line 474
    invoke-virtual {p0}, Lgca;->aEz()I

    move-result v1

    if-eq v1, v10, :cond_1

    .line 494
    :cond_0
    :goto_0
    return-object v0

    .line 477
    :cond_1
    invoke-virtual {p0}, Lgca;->aDF()Liyh;

    move-result-object v1

    .line 478
    if-eqz v1, :cond_0

    .line 482
    iget-object v2, p0, Lgca;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v4, v2, v4

    .line 483
    iget-object v3, v1, Liyh;->dQi:[Liyk;

    array-length v6, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_0

    aget-object v1, v3, v2

    .line 484
    iget-object v7, v1, Liyk;->dQt:[J

    array-length v7, v7

    if-eqz v7, :cond_0

    .line 487
    invoke-static {v1}, Lgca;->b(Liyk;)J

    move-result-wide v8

    .line 489
    invoke-virtual {v1}, Liyk;->getState()I

    move-result v7

    if-ne v7, v10, :cond_2

    cmp-long v7, v4, v8

    if-gez v7, :cond_2

    move-object v0, v1

    .line 491
    goto :goto_0

    .line 483
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public final aEF()Ljava/lang/String;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 559
    invoke-virtual {p0}, Lgca;->aEE()Liyk;

    move-result-object v0

    .line 560
    if-eqz v0, :cond_0

    iget-object v1, v0, Liyk;->dQq:[Liyj;

    array-length v1, v1

    if-nez v1, :cond_2

    :cond_0
    move-object v0, v2

    .line 579
    :cond_1
    :goto_0
    return-object v0

    .line 566
    :cond_2
    iget-object v3, v0, Liyk;->dQq:[Liyj;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    move-object v0, v2

    :goto_1
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    .line 567
    invoke-virtual {v5}, Liyj;->sk()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 568
    invoke-virtual {v5}, Liyj;->sj()Ljava/lang/String;

    move-result-object v1

    .line 574
    :goto_2
    if-eqz v1, :cond_5

    move-object v0, v1

    .line 575
    goto :goto_0

    .line 570
    :cond_3
    if-nez v0, :cond_4

    invoke-virtual {v5}, Liyj;->bcf()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 571
    invoke-virtual {v5}, Liyj;->bce()Ljava/lang/String;

    move-result-object v0

    .line 566
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 576
    :cond_5
    if-nez v0, :cond_1

    move-object v0, v2

    .line 579
    goto :goto_0

    :cond_6
    move-object v1, v2

    goto :goto_2
.end method

.method public final aEG()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 799
    invoke-virtual {p0}, Lgca;->aDF()Liyh;

    move-result-object v1

    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v1, v1, Liyh;->dQe:[Liyi;

    array-length v1, v1

    goto :goto_0
.end method

.method public final aEH()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 980
    iget-object v1, p0, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    if-nez v1, :cond_1

    .line 992
    :cond_0
    :goto_0
    return v0

    .line 986
    :cond_1
    iget-object v1, p0, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    .line 987
    iget-object v2, v1, Liyh;->dQi:[Liyk;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 988
    invoke-virtual {v4}, Liyk;->bcm()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 989
    const/4 v0, 0x1

    goto :goto_0

    .line 987
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final aEz()I
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbG()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    const/4 v0, -0x1

    .line 82
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->aEz()I

    move-result v0

    goto :goto_0
.end method

.method public final br(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 98
    const v0, 0x7f0b00ba

    .line 100
    invoke-virtual {p0}, Lgca;->aEA()I

    move-result v1

    .line 101
    packed-switch v1, :pswitch_data_0

    .line 112
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0

    .line 103
    :pswitch_0
    const v0, 0x7f0b00b7

    .line 104
    goto :goto_0

    .line 106
    :pswitch_1
    const v0, 0x7f0b00b9

    .line 107
    goto :goto_0

    .line 109
    :pswitch_2
    const v0, 0x7f0b00b8

    goto :goto_0

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final bs(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 304
    invoke-virtual {p0}, Lgca;->aEB()Ljava/lang/Integer;

    move-result-object v0

    .line 305
    if-nez v0, :cond_0

    .line 306
    const/4 v0, 0x0

    .line 331
    :goto_0
    return-object v0

    .line 308
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x3c

    if-lt v1, v2, :cond_1

    .line 310
    const v1, 0x7f0a0178

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    div-int/lit8 v3, v3, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    rem-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 317
    :goto_1
    invoke-direct {p0, p1}, Lgca;->bu(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 318
    iget-object v2, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v2}, Liyg;->bbx()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lgca;->aEz()I

    move-result v2

    if-eq v2, v4, :cond_3

    .line 320
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 321
    const v1, 0x7f0a01c5

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbw()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 313
    :cond_1
    const v1, 0x7f0a0177

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 324
    :cond_2
    const v2, 0x7f0a01c4

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    aput-object v1, v3, v4

    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbw()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 327
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 328
    const v1, 0x7f0a01d2

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 331
    :cond_4
    const v2, 0x7f0a01c3

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    aput-object v1, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final bt(Landroid/content/Context;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 335
    invoke-direct {p0, p1}, Lgca;->bu(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 336
    invoke-virtual {p0}, Lgca;->aED()Liyk;

    move-result-object v1

    .line 338
    if-eqz v1, :cond_0

    iget-object v2, v1, Liyk;->dQw:[J

    array-length v2, v2

    if-nez v2, :cond_1

    .line 340
    :cond_0
    const-string v0, ""

    .line 350
    :goto_0
    return-object v0

    .line 342
    :cond_1
    invoke-static {v1}, Lgca;->a(Liyk;)J

    move-result-wide v2

    .line 344
    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {p1, v2, v3, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v1

    .line 346
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 347
    const v0, 0x7f0a01c7

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v1, v2, v6

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 350
    :cond_2
    const v2, 0x7f0a01c6

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bu(J)Ljava/lang/Integer;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 289
    invoke-virtual {p0}, Lgca;->aEz()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgca;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    if-nez v0, :cond_1

    .line 291
    :cond_0
    const/4 v0, 0x0

    .line 298
    :goto_0
    return-object v0

    .line 293
    :cond_1
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    iget-object v0, v0, Liyg;->dPL:Liyh;

    .line 294
    invoke-virtual {v0}, Liyh;->bbV()J

    move-result-wide v0

    .line 298
    sub-long/2addr v0, p1

    long-to-double v0, v0

    const-wide/high16 v2, 0x404e000000000000L    # 60.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final bv(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 12
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 405
    invoke-virtual {p0}, Lgca;->aEz()I

    move-result v0

    .line 406
    if-nez v0, :cond_4

    .line 407
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbw()Ljava/lang/String;

    move-result-object v2

    .line 408
    invoke-virtual {p0}, Lgca;->aEA()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 409
    :goto_0
    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 410
    const v1, 0x7f0a01bc

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v10

    aput-object v2, v3, v8

    invoke-virtual {p1, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 469
    :cond_0
    :goto_1
    return-object v0

    .line 408
    :pswitch_0
    const v0, 0x7f0a01ed

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0a01ef

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a01ee

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 412
    :cond_2
    if-eqz v2, :cond_3

    .line 413
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_1

    .line 414
    :cond_3
    if-nez v0, :cond_0

    move-object v0, v1

    .line 417
    goto :goto_1

    .line 419
    :cond_4
    if-ne v0, v8, :cond_b

    .line 420
    invoke-virtual {p0}, Lgca;->aDF()Liyh;

    move-result-object v0

    .line 422
    if-nez v0, :cond_5

    move-object v0, v1

    .line 423
    goto :goto_1

    .line 425
    :cond_5
    iget-object v2, v0, Liyh;->dPR:Ljbp;

    .line 427
    if-eqz v2, :cond_6

    invoke-virtual {v0}, Liyh;->bbQ()Z

    move-result v3

    if-nez v3, :cond_7

    :cond_6
    move-object v0, v1

    .line 428
    goto :goto_1

    .line 431
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 433
    invoke-virtual {v0}, Liyh;->bbU()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0}, Liyh;->bbT()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_9

    :cond_8
    move-object v0, v1

    .line 435
    goto :goto_1

    .line 438
    :cond_9
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Liyh;->bbT()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 440
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    .line 441
    invoke-virtual {v3, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-virtual {v0}, Liyh;->bbX()Ljava/lang/String;

    move-result-object v3

    .line 445
    invoke-virtual {v0}, Liyh;->bbP()I

    move-result v4

    if-lez v4, :cond_a

    .line 446
    const v4, 0x7f0a01bd

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "<b>"

    aput-object v6, v5, v10

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v8

    const-string v3, "</b>"

    aput-object v3, v5, v9

    aput-object v1, v5, v11

    const/4 v1, 0x4

    invoke-virtual {v0}, Liyh;->bbP()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v0, 0x5

    invoke-virtual {v2}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 463
    :goto_2
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto/16 :goto_1

    .line 455
    :cond_a
    const v0, 0x7f0a01be

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "<b>"

    aput-object v5, v4, v10

    invoke-static {v3}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v8

    const-string v3, "</b>"

    aput-object v3, v4, v9

    aput-object v1, v4, v11

    const/4 v1, 0x4

    invoke-virtual {v2}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 464
    :cond_b
    if-ne v0, v9, :cond_c

    .line 465
    const v0, 0x7f0a02c5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 466
    :cond_c
    if-ne v0, v11, :cond_d

    .line 467
    const v0, 0x7f0a02c6

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_d
    move-object v0, v1

    .line 469
    goto/16 :goto_1

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final bw(Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 500
    invoke-virtual {p0}, Lgca;->aEE()Liyk;

    move-result-object v0

    .line 501
    if-eqz v0, :cond_0

    iget-object v1, v0, Liyk;->dQt:[J

    array-length v1, v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Liyk;->dQq:[Liyj;

    array-length v1, v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Liyk;->dQq:[Liyj;

    aget-object v1, v1, v6

    invoke-virtual {v1}, Liyj;->oK()Z

    move-result v1

    if-nez v1, :cond_1

    .line 503
    :cond_0
    const/4 v0, 0x0

    .line 512
    :goto_0
    return-object v0

    .line 506
    :cond_1
    invoke-static {v0}, Lgca;->b(Liyk;)J

    move-result-wide v2

    .line 509
    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {p1, v2, v3, v6}, Lesi;->a(Landroid/content/Context;JI)Ljava/lang/CharSequence;

    move-result-object v1

    .line 512
    const v2, 0x7f0a01c0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "<b>"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    iget-object v0, v0, Liyk;->dQq:[Liyj;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Liyj;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x2

    const-string v4, "</b>"

    aput-object v4, v3, v0

    const/4 v0, 0x3

    aput-object v1, v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bx(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 523
    invoke-virtual {p0}, Lgca;->aEE()Liyk;

    move-result-object v0

    .line 524
    if-eqz v0, :cond_0

    iget-object v1, v0, Liyk;->dQr:Ljbp;

    if-nez v1, :cond_1

    .line 525
    :cond_0
    const/4 v0, 0x0

    .line 551
    :goto_0
    return-object v0

    .line 527
    :cond_1
    invoke-virtual {v0}, Liyk;->bbP()I

    move-result v1

    .line 528
    iget-object v2, v0, Liyk;->dQr:Ljbp;

    invoke-virtual {v2}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v2

    .line 529
    if-lez v1, :cond_3

    .line 530
    invoke-virtual {v0}, Liyk;->bcl()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 531
    invoke-virtual {v0}, Liyk;->bck()Ljava/lang/String;

    move-result-object v0

    .line 532
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100020

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v2}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v9

    invoke-virtual {v3, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 539
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f100021

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 544
    :cond_3
    invoke-virtual {v0}, Liyk;->bcl()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 545
    invoke-virtual {v0}, Liyk;->bck()Ljava/lang/String;

    move-result-object v0

    .line 546
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a01c1

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v2}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 551
    :cond_4
    invoke-static {v2}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final by(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 591
    invoke-virtual {p0}, Lgca;->aDF()Liyh;

    move-result-object v0

    .line 592
    if-nez v0, :cond_0

    .line 593
    const/4 v0, 0x0

    .line 596
    :goto_0
    return-object v0

    .line 595
    :cond_0
    iget-object v0, v0, Liyh;->dQe:[Liyi;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 596
    const-string v1, "<b>"

    const-string v2, "</b>"

    invoke-static {p1, v0, v1, v2}, Lgbx;->a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Liyk;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 882
    iget-object v1, p0, Lgca;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 885
    invoke-virtual {p1}, Liyk;->getState()I

    move-result v1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    .line 897
    :cond_0
    :goto_0
    return v0

    .line 891
    :cond_1
    iget-object v1, p1, Liyk;->dQw:[J

    array-length v1, v1

    if-lez v1, :cond_2

    .line 892
    invoke-static {p1}, Lgca;->a(Liyk;)J

    move-result-wide v4

    .line 893
    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 897
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(JZ)Ljava/lang/Integer;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 254
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbD()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    const/4 v0, 0x0

    .line 279
    :goto_0
    return-object v0

    .line 258
    :cond_0
    invoke-virtual {p0}, Lgca;->aEz()I

    move-result v1

    .line 260
    const/4 v0, 0x0

    .line 261
    if-nez v1, :cond_3

    .line 262
    if-eqz p3, :cond_2

    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbB()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 263
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bbA()I

    move-result v0

    .line 279
    :cond_1
    :goto_1
    iget-object v1, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v1}, Liyg;->bbC()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 265
    :cond_2
    iget-object v0, p0, Lgca;->mRoute:Liyg;

    invoke-virtual {v0}, Liyg;->bby()I

    move-result v0

    goto :goto_1

    .line 268
    :cond_3
    iget-object v1, p0, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    invoke-virtual {v1}, Liyh;->bbT()J

    move-result-wide v2

    sub-long/2addr v2, p1

    iget-object v1, p0, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    invoke-virtual {v1}, Liyh;->bbP()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3c

    int-to-long v6, v1

    sub-long/2addr v2, v6

    .line 273
    :goto_2
    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 274
    long-to-double v0, v2

    const-wide/high16 v2, 0x404e000000000000L    # 60.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_1

    :cond_4
    move-wide v2, v4

    .line 268
    goto :goto_2
.end method

.method public final d(Liyk;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 910
    iget-object v1, p0, Lgca;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 912
    invoke-virtual {p1}, Liyk;->bcm()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Liyk;->getState()I

    move-result v1

    const/4 v4, 0x2

    if-eq v1, v4, :cond_0

    invoke-virtual {p1}, Liyk;->getState()I

    move-result v1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    .line 925
    :cond_0
    :goto_0
    return v0

    .line 919
    :cond_1
    iget-object v1, p1, Liyk;->dQt:[J

    array-length v1, v1

    if-lez v1, :cond_2

    .line 920
    iget-object v1, p1, Liyk;->dQt:[J

    invoke-static {v1}, Liuu;->c([J)J

    move-result-wide v4

    .line 921
    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 925
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Liyk;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 996
    invoke-virtual {p1}, Liyk;->getState()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 1013
    :cond_0
    :goto_0
    return v0

    .line 999
    :cond_1
    iget-object v2, p1, Liyk;->dQt:[J

    array-length v2, v2

    iget-object v3, p1, Liyk;->dQw:[J

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 1004
    iget-object v2, p1, Liyk;->dQt:[J

    array-length v2, v2

    if-nez v2, :cond_2

    .line 1007
    invoke-virtual {p1}, Liyk;->getState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1009
    :cond_2
    invoke-static {p1}, Lgca;->a(Liyk;)J

    move-result-wide v2

    .line 1010
    invoke-static {p1}, Lgca;->b(Liyk;)J

    move-result-wide v4

    .line 1011
    iget-object v6, p0, Lgca;->mClock:Lemp;

    invoke-interface {v6}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    .line 1013
    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    cmp-long v2, v6, v2

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final p(Landroid/location/Location;)Z
    .locals 8
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 938
    invoke-virtual {p0}, Lgca;->aEH()Z

    move-result v1

    if-nez v1, :cond_1

    .line 968
    :cond_0
    :goto_0
    return v0

    .line 943
    :cond_1
    iget-object v1, p0, Lgca;->mRoute:Liyg;

    iget-object v1, v1, Liyg;->dPL:Liyh;

    .line 944
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lgca;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    .line 945
    invoke-virtual {v1}, Liyh;->bbW()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Liyh;->bbV()J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-ltz v2, :cond_0

    .line 951
    :cond_2
    if-eqz p1, :cond_4

    .line 952
    iget-object v2, v1, Liyh;->dQi:[Liyk;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 953
    iget-object v5, v4, Liyk;->dQr:Ljbp;

    invoke-static {v5, p1}, Lgay;->a(Ljbp;Landroid/location/Location;)F

    move-result v5

    const/high16 v6, 0x43480000    # 200.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    iget-object v5, v4, Liyk;->dQt:[J

    array-length v5, v5

    if-lez v5, :cond_3

    .line 956
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    iget-object v4, v4, Liyk;->dQt:[J

    aget-wide v4, v4, v0

    sub-long v4, v6, v4

    .line 959
    const-wide/16 v6, 0x12c

    cmp-long v4, v4, v6

    if-gtz v4, :cond_0

    .line 952
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 968
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method
