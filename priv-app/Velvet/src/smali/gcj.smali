.class public final enum Lgcj;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cFI:Lgcj;

.field public static final enum cFJ:Lgcj;

.field public static final enum cFK:Lgcj;

.field public static final enum cFL:Lgcj;

.field public static final enum cFM:Lgcj;

.field private static final synthetic cFN:[Lgcj;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lgcj;

    const-string v1, "WAITING"

    invoke-direct {v0, v1, v2}, Lgcj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgcj;->cFI:Lgcj;

    .line 56
    new-instance v0, Lgcj;

    const-string v1, "WAITING_SECONDARY_FAILED"

    invoke-direct {v0, v1, v3}, Lgcj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgcj;->cFJ:Lgcj;

    .line 57
    new-instance v0, Lgcj;

    const-string v1, "USE_PRIMARY"

    invoke-direct {v0, v1, v4}, Lgcj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgcj;->cFK:Lgcj;

    .line 58
    new-instance v0, Lgcj;

    const-string v1, "USE_SECONDARY"

    invoke-direct {v0, v1, v5}, Lgcj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgcj;->cFL:Lgcj;

    .line 59
    new-instance v0, Lgcj;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, Lgcj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgcj;->cFM:Lgcj;

    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [Lgcj;

    sget-object v1, Lgcj;->cFI:Lgcj;

    aput-object v1, v0, v2

    sget-object v1, Lgcj;->cFJ:Lgcj;

    aput-object v1, v0, v3

    sget-object v1, Lgcj;->cFK:Lgcj;

    aput-object v1, v0, v4

    sget-object v1, Lgcj;->cFL:Lgcj;

    aput-object v1, v0, v5

    sget-object v1, Lgcj;->cFM:Lgcj;

    aput-object v1, v0, v6

    sput-object v0, Lgcj;->cFN:[Lgcj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgcj;
    .locals 1

    .prologue
    .line 54
    const-class v0, Lgcj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgcj;

    return-object v0
.end method

.method public static values()[Lgcj;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lgcj;->cFN:[Lgcj;

    invoke-virtual {v0}, [Lgcj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgcj;

    return-object v0
.end method
