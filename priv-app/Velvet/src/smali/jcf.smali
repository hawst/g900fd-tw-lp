.class public final Ljcf;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private aiX:Ljcn;

.field private aix:Ljava/lang/String;

.field private ajk:Ljava/lang/String;

.field private dRq:J

.field private dZZ:Ljava/lang/String;

.field private eaa:Ljava/lang/String;

.field private eab:Ljbp;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59008
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 59009
    const/4 v0, 0x0

    iput v0, p0, Ljcf;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljcf;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljcf;->aix:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljcf;->dRq:J

    const-string v0, ""

    iput-object v0, p0, Ljcf;->dZZ:Ljava/lang/String;

    iput-object v2, p0, Ljcf;->aiX:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljcf;->ajk:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljcf;->eaa:Ljava/lang/String;

    iput-object v2, p0, Ljcf;->eab:Ljbp;

    iput-object v2, p0, Ljcf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcf;->eCz:I

    .line 59010
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 58854
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcf;->afh:Ljava/lang/String;

    iget v0, p0, Ljcf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcf;->aix:Ljava/lang/String;

    iget v0, p0, Ljcf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljcf;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljcf;->dRq:J

    iget v0, p0, Ljcf;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljcf;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcf;->dZZ:Ljava/lang/String;

    iget v0, p0, Ljcf;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljcf;->aez:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljcf;->aiX:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljcf;->aiX:Ljcn;

    :cond_1
    iget-object v0, p0, Ljcf;->aiX:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcf;->ajk:Ljava/lang/String;

    iget v0, p0, Ljcf;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljcf;->aez:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcf;->eaa:Ljava/lang/String;

    iget v0, p0, Ljcf;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljcf;->aez:I

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ljcf;->eab:Ljbp;

    if-nez v0, :cond_2

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljcf;->eab:Ljbp;

    :cond_2
    iget-object v0, p0, Ljcf;->eab:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 59030
    iget v0, p0, Ljcf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 59031
    const/4 v0, 0x1

    iget-object v1, p0, Ljcf;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 59033
    :cond_0
    iget v0, p0, Ljcf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 59034
    const/4 v0, 0x2

    iget-object v1, p0, Ljcf;->aix:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 59036
    :cond_1
    iget v0, p0, Ljcf;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 59037
    const/4 v0, 0x3

    iget-wide v2, p0, Ljcf;->dRq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 59039
    :cond_2
    iget v0, p0, Ljcf;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 59040
    const/4 v0, 0x4

    iget-object v1, p0, Ljcf;->dZZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 59042
    :cond_3
    iget-object v0, p0, Ljcf;->aiX:Ljcn;

    if-eqz v0, :cond_4

    .line 59043
    const/4 v0, 0x6

    iget-object v1, p0, Ljcf;->aiX:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 59045
    :cond_4
    iget v0, p0, Ljcf;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 59046
    const/4 v0, 0x7

    iget-object v1, p0, Ljcf;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 59048
    :cond_5
    iget v0, p0, Ljcf;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_6

    .line 59049
    const/16 v0, 0x8

    iget-object v1, p0, Ljcf;->eaa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 59051
    :cond_6
    iget-object v0, p0, Ljcf;->eab:Ljbp;

    if-eqz v0, :cond_7

    .line 59052
    const/16 v0, 0x9

    iget-object v1, p0, Ljcf;->eab:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 59054
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 59055
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 59059
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 59060
    iget v1, p0, Ljcf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 59061
    const/4 v1, 0x1

    iget-object v2, p0, Ljcf;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59064
    :cond_0
    iget v1, p0, Ljcf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 59065
    const/4 v1, 0x2

    iget-object v2, p0, Ljcf;->aix:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59068
    :cond_1
    iget v1, p0, Ljcf;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 59069
    const/4 v1, 0x3

    iget-wide v2, p0, Ljcf;->dRq:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 59072
    :cond_2
    iget v1, p0, Ljcf;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 59073
    const/4 v1, 0x4

    iget-object v2, p0, Ljcf;->dZZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59076
    :cond_3
    iget-object v1, p0, Ljcf;->aiX:Ljcn;

    if-eqz v1, :cond_4

    .line 59077
    const/4 v1, 0x6

    iget-object v2, p0, Ljcf;->aiX:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59080
    :cond_4
    iget v1, p0, Ljcf;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_5

    .line 59081
    const/4 v1, 0x7

    iget-object v2, p0, Ljcf;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59084
    :cond_5
    iget v1, p0, Ljcf;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_6

    .line 59085
    const/16 v1, 0x8

    iget-object v2, p0, Ljcf;->eaa:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59088
    :cond_6
    iget-object v1, p0, Ljcf;->eab:Ljbp;

    if-eqz v1, :cond_7

    .line 59089
    const/16 v1, 0x9

    iget-object v2, p0, Ljcf;->eab:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59092
    :cond_7
    return v0
.end method
