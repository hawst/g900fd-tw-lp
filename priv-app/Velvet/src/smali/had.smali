.class public final Lhad;
.super Lgxi;
.source "PG"

# interfaces
.implements Lddc;


# instance fields
.field bub:Ljava/util/List;

.field private bwv:Lcom/google/android/shared/search/Query;

.field final dau:Landroid/view/View$OnClickListener;

.field private final dav:Lhaf;

.field mCommittedQuery:Lcom/google/android/shared/search/Query;

.field mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/velvet/ui/MainContentView;)V
    .locals 1

    .prologue
    .line 86
    const-string v0, "voicecorrection"

    invoke-direct {p0, v0, p1}, Lgxi;-><init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)V

    .line 40
    new-instance v0, Lhae;

    invoke-direct {v0, p0}, Lhae;-><init>(Lhad;)V

    iput-object v0, p0, Lhad;->dau:Landroid/view/View$OnClickListener;

    .line 74
    new-instance v0, Lhaf;

    invoke-direct {v0, p0}, Lhaf;-><init>(Lhad;)V

    iput-object v0, p0, Lhad;->dav:Lhaf;

    .line 87
    return-void
.end method

.method static a(Ljava/lang/String;IILjava/lang/String;)V
    .locals 3

    .prologue
    .line 189
    const/16 v0, 0x8a

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    .line 191
    new-instance v1, Litv;

    invoke-direct {v1}, Litv;-><init>()V

    invoke-virtual {v1, p1}, Litv;->mM(I)Litv;

    move-result-object v1

    sub-int v2, p2, p1

    invoke-virtual {v1, v2}, Litv;->mN(I)Litv;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Litv;->pR(Ljava/lang/String;)Litv;

    move-result-object v1

    invoke-virtual {v1, p3}, Litv;->pS(Ljava/lang/String;)Litv;

    move-result-object v1

    iput-object v1, v0, Litu;->dIs:Litv;

    .line 196
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 197
    return-void
.end method

.method public static aZ(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    .line 101
    instance-of v1, v0, Landroid/text/Spanned;

    if-nez v1, :cond_0

    move-object v0, v2

    .line 123
    :goto_0
    return-object v0

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->getSelectionStart()I

    move-result v1

    .line 105
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->getSelectionEnd()I

    move-result v3

    .line 106
    if-eq v1, v3, :cond_1

    move-object v0, v2

    .line 107
    goto :goto_0

    .line 109
    :cond_1
    check-cast v0, Landroid/text/Spanned;

    .line 111
    const-class v3, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-static {v0, v1, v3}, Leqt;->a(Landroid/text/Spanned;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    .line 113
    if-nez v1, :cond_2

    move-object v0, v2

    .line 114
    goto :goto_0

    .line 117
    :cond_2
    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    .line 118
    invoke-interface {v0, v1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 123
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/android/shared/search/Query;->c(Ljava/lang/CharSequence;II)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_0
.end method

.method private as(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 145
    iput-object p1, p0, Lhad;->bub:Ljava/util/List;

    .line 146
    iget-object v0, p0, Lhad;->dav:Lhaf;

    invoke-virtual {p0, v0}, Lhad;->a(Lesj;)V

    .line 147
    return-void
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x1

    return v0
.end method

.method protected final X(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    iget-object v0, v0, Lgyz;->mVelvetFactory:Lgpu;

    invoke-virtual {p0}, Lhad;->aLt()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-static {p0, v0}, Lgpu;->a(Lhad;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhad;->mView:Landroid/view/View;

    .line 129
    iget-object v0, p0, Lhad;->mView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    iget-object v2, p0, Lhad;->mView:Landroid/view/View;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lhad;->a([Landroid/view/View;)V

    .line 131
    invoke-virtual {p0}, Lhad;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lhad;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 132
    invoke-virtual {p0, p0}, Lhad;->a(Lddc;)V

    .line 133
    return-void
.end method

.method public final a(Lddb;)V
    .locals 4

    .prologue
    .line 156
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lhad;->bwv:Lcom/google/android/shared/search/Query;

    if-ne v0, v1, :cond_0

    .line 184
    :goto_0
    return-void

    .line 160
    :cond_0
    iput-object v0, p0, Lhad;->bwv:Lcom/google/android/shared/search/Query;

    .line 162
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 164
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v2

    .line 165
    instance-of v2, v2, Landroid/text/Spanned;

    if-nez v2, :cond_1

    .line 166
    invoke-direct {p0, v1}, Lhad;->as(Ljava/util/List;)V

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->getSelectionStart()I

    move-result v2

    .line 171
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->getSelectionEnd()I

    move-result v3

    .line 172
    if-eq v2, v3, :cond_2

    .line 173
    invoke-direct {p0, v1}, Lhad;->as(Ljava/util/List;)V

    goto :goto_0

    .line 176
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 178
    const-class v3, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-static {v0, v2, v3}, Leqt;->a(Landroid/text/Spanned;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    .line 180
    if-eqz v0, :cond_3

    .line 181
    invoke-virtual {v0}, Lcom/google/android/shared/util/VoiceCorrectionSpan;->avH()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 183
    :cond_3
    invoke-direct {p0, v1}, Lhad;->as(Ljava/util/List;)V

    goto :goto_0
.end method

.method protected final aKF()V
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0, p0}, Lhad;->b(Lddc;)V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lhad;->mCommittedQuery:Lcom/google/android/shared/search/Query;

    .line 139
    sget-object v0, Lgxi;->cYu:Lesj;

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 140
    return-void
.end method
