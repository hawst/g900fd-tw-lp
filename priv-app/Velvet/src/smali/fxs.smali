.class public Lfxs;
.super Lfus;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbr;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 46
    return-void
.end method

.method public static d(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 221
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 223
    const-string v0, "xhdpi"

    .line 224
    sparse-switch v1, :sswitch_data_0

    .line 237
    :goto_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, p0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 226
    :sswitch_0
    const-string v0, "xhdpi"

    goto :goto_0

    .line 224
    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xa0 -> :sswitch_0
        0xd5 -> :sswitch_0
        0xf0 -> :sswitch_0
        0x140 -> :sswitch_0
    .end sparse-switch
.end method

.method private static e(Landroid/content/Context;Lizj;)Lani;
    .locals 4

    .prologue
    .line 209
    new-instance v0, Lfsa;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    const v1, 0x7f0a0326

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lizj;->dSd:Ljir;

    iget-object v2, v2, Ljir;->aeB:Ljbp;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lfsa;->a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;

    move-result-object v0

    return-object v0
.end method

.method private g(Landroid/content/Context;Lizj;)Lapv;
    .locals 5

    .prologue
    .line 167
    iget-object v1, p2, Lizj;->dSd:Ljir;

    .line 168
    new-instance v2, Lapv;

    invoke-direct {v2}, Lapv;-><init>()V

    .line 170
    iget-object v3, p2, Lizj;->dSd:Ljir;

    const-string v0, ""

    iget-object v4, v3, Ljir;->aeB:Ljbp;

    if-eqz v4, :cond_0

    iget-object v4, v3, Ljir;->aeB:Ljbp;

    invoke-virtual {v4}, Ljbp;->oK()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, v3, Ljir;->aeB:Ljbp;

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x2c

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v2, v0}, Lapv;->dO(Ljava/lang/String;)Lapv;

    .line 172
    iget-object v0, v1, Ljir;->enM:Ljis;

    if-eqz v0, :cond_3

    .line 173
    iget-object v0, v1, Ljir;->enM:Ljis;

    .line 174
    invoke-virtual {v0}, Ljis;->pX()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    invoke-virtual {v0}, Ljis;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lapv;->dR(Ljava/lang/String;)Lapv;

    .line 178
    :cond_1
    invoke-virtual {v0}, Ljis;->ok()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 179
    invoke-virtual {v0}, Ljis;->oj()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lfxs;->d(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 180
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 181
    invoke-virtual {v2, v1}, Lapv;->dP(Ljava/lang/String;)Lapv;

    .line 185
    :cond_2
    invoke-virtual {v0}, Ljis;->bnt()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 186
    invoke-virtual {v0}, Ljis;->bns()I

    move-result v0

    invoke-static {v0}, Lemg;->ih(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lapv;->dQ(Ljava/lang/String;)Lapv;

    .line 191
    :cond_3
    return-object v2
.end method


# virtual methods
.method public final A(Lizj;)Ljau;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 217
    iget-object v0, p1, Lizj;->dSe:Ljau;

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 2

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Lfus;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 58
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lang;->aS(Z)Lang;

    .line 59
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lfxs;->cCl:Lftx;

    invoke-virtual {v0, p1, p3}, Lftx;->j(Landroid/content/Context;I)Lanh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 3

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Lfxs;->g(Landroid/content/Context;Lizj;)Lapv;

    move-result-object v1

    .line 147
    iget-object v0, p2, Lizj;->dSd:Ljir;

    iget-object v0, v0, Ljir;->enN:[Ljis;

    array-length v0, v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 150
    :goto_0
    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {v0}, Ljis;->bns()I

    move-result v2

    invoke-static {v2}, Lemg;->ih(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lapv;->dS(Ljava/lang/String;)Lapv;

    .line 153
    invoke-virtual {v0}, Ljis;->bnu()I

    move-result v0

    invoke-static {v0}, Lemg;->ih(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapv;->dT(Ljava/lang/String;)Lapv;

    .line 157
    :cond_0
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 158
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 159
    iput-object v1, v0, Lanh;->agB:Lapv;

    .line 160
    invoke-static {p1, p2}, Lfxs;->e(Landroid/content/Context;Lizj;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 161
    iput-object p2, v0, Lanh;->ahu:Lizj;

    .line 163
    return-object v0

    .line 147
    :cond_1
    iget-object v0, p2, Lizj;->dSd:Ljir;

    iget-object v0, v0, Ljir;->enN:[Ljis;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 80
    iget-object v2, p2, Lizj;->dSd:Ljir;

    .line 82
    new-instance v3, Lapt;

    invoke-direct {v3}, Lapt;-><init>()V

    .line 83
    invoke-direct {p0, p1, p2}, Lfxs;->g(Landroid/content/Context;Lizj;)Lapv;

    move-result-object v0

    iput-object v0, v3, Lapt;->alM:Lapv;

    .line 85
    iget-object v0, v2, Ljir;->enM:Ljis;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, v2, Ljir;->enM:Ljis;

    invoke-virtual {v0}, Ljis;->bnz()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, ""

    .line 88
    iget-object v4, v2, Ljir;->enM:Ljis;

    invoke-virtual {v4}, Ljis;->bnx()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 100
    :goto_0
    const v4, 0x7f0a0237

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, v2, Ljir;->enM:Ljis;

    invoke-virtual {v6}, Ljis;->bny()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    aput-object v0, v5, v9

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lapt;->dI(Ljava/lang/String;)Lapt;

    .line 104
    :cond_0
    iget-object v0, v2, Ljir;->enM:Ljis;

    invoke-virtual {v0}, Ljis;->bnw()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    const v0, 0x7f0a0238

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, v2, Ljir;->enM:Ljis;

    invoke-virtual {v5}, Ljis;->bnv()I

    move-result v5

    int-to-double v6, v5

    invoke-static {v6, v7, v1, v1}, Lemg;->a(DII)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lapt;->dJ(Ljava/lang/String;)Lapt;

    .line 112
    :cond_1
    iget-object v0, v2, Ljir;->enN:[Ljis;

    array-length v0, v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 114
    iget-object v2, v2, Ljir;->enN:[Ljis;

    array-length v5, v2

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v6, v2, v0

    .line 115
    new-instance v7, Lapu;

    invoke-direct {v7}, Lapu;-><init>()V

    .line 116
    invoke-virtual {v6}, Ljis;->getLabel()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lapu;->dK(Ljava/lang/String;)Lapu;

    .line 117
    invoke-virtual {v6}, Ljis;->oj()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, p1}, Lfxs;->d(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lapu;->dL(Ljava/lang/String;)Lapu;

    .line 118
    invoke-virtual {v6}, Ljis;->bns()I

    move-result v8

    invoke-static {v8}, Lemg;->ih(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lapu;->dM(Ljava/lang/String;)Lapu;

    .line 120
    invoke-virtual {v6}, Ljis;->bnu()I

    move-result v6

    invoke-static {v6}, Lemg;->ih(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lapu;->dN(Ljava/lang/String;)Lapu;

    .line 122
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 90
    :pswitch_0
    const v0, 0x7f0a023a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 93
    :pswitch_1
    const v0, 0x7f0a0239

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 96
    :pswitch_2
    const v0, 0x7f0a023b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 124
    :cond_2
    iget-object v0, v3, Lapt;->alP:[Lapu;

    invoke-static {v0, v4}, Leqh;->a([Ljava/lang/Object;Ljava/util/Collection;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapu;

    iput-object v0, v3, Lapt;->alP:[Lapu;

    .line 126
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 127
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lanh;->cm(I)Lanh;

    .line 128
    iput-object v3, v0, Lanh;->agC:Lapt;

    .line 129
    iput-object p2, v0, Lanh;->ahu:Lizj;

    .line 131
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 132
    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lanh;->cm(I)Lanh;

    .line 133
    iput-object p2, v2, Lanh;->ahu:Lizj;

    .line 134
    new-instance v3, Laoj;

    invoke-direct {v3}, Laoj;-><init>()V

    iput-object v3, v2, Lanh;->agz:Laoj;

    .line 135
    iget-object v3, v2, Lanh;->agz:Laoj;

    const v4, 0x7f0a0508

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 136
    invoke-static {p1, p2}, Lfxs;->e(Landroid/content/Context;Lizj;)Lani;

    move-result-object v3

    iput-object v3, v2, Lanh;->ahs:Lani;

    .line 137
    iget-object v3, v2, Lanh;->ahs:Lani;

    new-instance v4, Lanb;

    invoke-direct {v4}, Lanb;-><init>()V

    iput-object v4, v3, Lani;->ahC:Lanb;

    .line 138
    iget-object v3, v2, Lanh;->ahs:Lani;

    iget-object v3, v3, Lani;->ahC:Lanb;

    const v4, 0x7f0200ed

    invoke-virtual {v3, v4}, Lanb;->ch(I)Lanb;

    .line 140
    new-array v3, v10, [Lanh;

    aput-object v0, v3, v1

    aput-object v2, v3, v9

    return-object v3

    .line 88
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lfxs;->aAw()Lizj;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 72
    invoke-virtual {p0, p1, v0}, Lfxs;->b(Landroid/content/Context;Lizj;)[Lanh;

    move-result-object v0

    return-object v0
.end method
