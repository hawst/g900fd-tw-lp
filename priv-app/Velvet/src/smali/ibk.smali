.class public final Libk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private final czY:Landroid/widget/TextView;

.field private final dxg:I

.field private final dxh:Landroid/text/SpannableStringBuilder;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Libk;->czY:Landroid/widget/TextView;

    .line 89
    iput p3, p0, Libk;->dxg:I

    .line 90
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    .line 91
    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    invoke-static {}, Libi;->aUI()Landroid/text/style/ForegroundColorSpan;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Libk;->czY:Landroid/widget/TextView;

    iget-object v1, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Libk;->czY:Landroid/widget/TextView;

    iget v1, p0, Libk;->dxg:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 114
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    invoke-static {}, Libi;->aUI()Landroid/text/style/ForegroundColorSpan;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 105
    iget-object v0, p0, Libk;->czY:Landroid/widget/TextView;

    iget-object v1, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Libk;->czY:Landroid/widget/TextView;

    iget v1, p0, Libk;->dxg:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 107
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 124
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 119
    return-void
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    .line 95
    iget-object v0, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    invoke-static {}, Libi;->aUI()Landroid/text/style/ForegroundColorSpan;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 96
    iget-object v1, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    invoke-static {}, Libi;->aUI()Landroid/text/style/ForegroundColorSpan;

    move-result-object v2

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 99
    iget-object v0, p0, Libk;->czY:Landroid/widget/TextView;

    iget-object v1, p0, Libk;->dxh:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    return-void
.end method
