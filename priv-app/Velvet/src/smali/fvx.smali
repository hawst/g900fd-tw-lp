.class public final Lfvx;
.super Lfuz;
.source "PG"


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 16

    .prologue
    .line 43
    move-object/from16 v0, p0

    iget-object v8, v0, Lfuz;->mEntry:Lizj;

    .line 45
    new-instance v9, Lang;

    invoke-direct {v9}, Lang;-><init>()V

    .line 46
    const/4 v1, 0x1

    invoke-virtual {v9, v1}, Lang;->aS(Z)Lang;

    .line 47
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 50
    new-instance v1, Lftn;

    const v2, 0x7f0a0345

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lftn;-><init>(Ljava/lang/String;)V

    iget-object v2, v8, Lizj;->dSv:Ljby;

    invoke-virtual {v2}, Ljby;->bfQ()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lftn;->ceJ:Ljava/lang/String;

    invoke-virtual {v1}, Lftn;->aDh()Lanh;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v1, v8, Lizj;->dSv:Ljby;

    iget-object v11, v1, Ljby;->dZI:[Ljbw;

    array-length v12, v11

    const/4 v1, 0x0

    move v5, v1

    :goto_0
    if-ge v5, v12, :cond_a

    aget-object v13, v11, v5

    .line 54
    new-instance v14, Lanh;

    invoke-direct {v14}, Lanh;-><init>()V

    const/16 v1, 0x2e

    invoke-virtual {v14, v1}, Lanh;->cm(I)Lanh;

    new-instance v1, Laoa;

    invoke-direct {v1}, Laoa;-><init>()V

    invoke-virtual {v13}, Ljbw;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laoa;->bl(Ljava/lang/String;)Laoa;

    move-result-object v15

    invoke-virtual {v13}, Ljbw;->bfI()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13}, Ljbw;->bfJ()Ljava/lang/String;

    move-result-object v7

    iget-object v1, v13, Ljbw;->dZG:[Ljdq;

    if-eqz v1, :cond_5

    iget-object v1, v13, Ljbw;->dZG:[Ljdq;

    array-length v1, v1

    if-lez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    :goto_3
    const-string v4, ""

    if-nez v2, :cond_0

    if-eqz v3, :cond_d

    :cond_0
    if-eqz v2, :cond_8

    if-eqz v3, :cond_8

    const-string v2, "%s \u00b7 %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v6, v3, v4

    const/4 v4, 0x1

    aput-object v7, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_4
    if-eqz v1, :cond_c

    const-string v1, "&nbsp;\u00b7 %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {v15, v1}, Laoa;->bm(Ljava/lang/String;)Laoa;

    move-result-object v2

    iget-object v3, v13, Ljbw;->dZC:[Ljava/lang/String;

    const-string v1, ""

    if-eqz v3, :cond_1

    array-length v4, v3

    if-lez v4, :cond_1

    const-string v1, "  "

    invoke-static {v1}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v1

    invoke-virtual {v1, v3}, Lifj;->b([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-virtual {v2, v1}, Laoa;->bn(Ljava/lang/String;)Laoa;

    move-result-object v1

    iget-object v2, v13, Ljbw;->aiX:Ljcn;

    iput-object v2, v1, Laoa;->aiX:Ljcn;

    iget-object v2, v13, Ljbw;->dZG:[Ljdq;

    if-eqz v2, :cond_2

    iget-object v2, v13, Ljbw;->dZG:[Ljdq;

    array-length v2, v2

    if-lez v2, :cond_2

    iget-object v2, v13, Ljbw;->dZG:[Ljdq;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/16 v3, 0x89

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lgbh;->a(Landroid/content/Context;Ljdq;I)Laok;

    move-result-object v2

    iput-object v2, v1, Laoa;->ahN:Laok;

    :cond_2
    invoke-virtual {v13}, Ljbw;->bfN()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Lfsa;

    const/16 v3, 0x8a

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    invoke-virtual {v13}, Ljbw;->bfM()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v2

    iput-object v2, v14, Lanh;->ahs:Lani;

    :cond_3
    invoke-virtual {v13}, Ljbw;->bfL()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Lfsa;

    const/16 v3, 0x4a

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    invoke-virtual {v13}, Ljbw;->bfK()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v2

    iput-object v2, v1, Laoa;->aiY:Lani;

    :cond_4
    iput-object v1, v14, Lanh;->ahj:Laoa;

    iput-object v8, v14, Lanh;->ahu:Lizj;

    invoke-interface {v10, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_0

    .line 54
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_8
    if-eqz v2, :cond_9

    move-object v2, v6

    goto/16 :goto_4

    :cond_9
    move-object v2, v7

    goto/16 :goto_4

    .line 58
    :cond_a
    iget-object v1, v8, Lizj;->dSv:Ljby;

    invoke-virtual {v1}, Ljby;->bfP()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 59
    new-instance v1, Lfsa;

    const/16 v2, 0x47

    invoke-direct {v1, v2}, Lfsa;-><init>(I)V

    const v2, 0x7f0200ed

    invoke-virtual {v1, v2}, Lfsa;->jB(I)Lfsa;

    move-result-object v1

    iget-object v2, v8, Lizj;->dSv:Ljby;

    invoke-virtual {v2}, Ljby;->bfO()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lfsa;->D(Ljava/lang/String;I)Lani;

    move-result-object v1

    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    const v3, 0x7f0a0346

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lanh;->cm(I)Lanh;

    iput-object v8, v3, Lanh;->ahu:Lizj;

    iput-object v2, v3, Lanh;->agz:Laoj;

    iput-object v1, v3, Lanh;->ahs:Lani;

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_b
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lanh;

    invoke-interface {v10, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lanh;

    iput-object v1, v9, Lang;->ags:[Lanh;

    .line 63
    return-object v9

    :cond_c
    move-object v1, v2

    goto/16 :goto_5

    :cond_d
    move-object v1, v4

    goto/16 :goto_5
.end method
