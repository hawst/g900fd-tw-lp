.class public final Leln;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final JJ:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Leln;->JJ:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public static e(Landroid/view/ViewGroup;I)I
    .locals 5

    .prologue
    .line 80
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 81
    :cond_0
    const-string v0, "ViewIds"

    const-string v1, "Child index %d out of range (childCount is %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 83
    const/4 v0, -0x1

    .line 85
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    goto :goto_0
.end method

.method public static generateViewId()I
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 29
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 30
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    .line 40
    :goto_0
    return v0

    .line 35
    :cond_0
    sget-object v0, Leln;->JJ:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 37
    add-int/lit8 v0, v1, 0x1

    .line 38
    const v2, 0xffffff

    if-le v0, v2, :cond_1

    const/4 v0, 0x1

    .line 39
    :cond_1
    sget-object v2, Leln;->JJ:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 40
    goto :goto_0
.end method

.method public static varargs o([I)I
    .locals 5

    .prologue
    .line 55
    invoke-static {p0}, Leln;->p([I)I

    move-result v0

    .line 56
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 57
    const-string v1, "ViewIds"

    const-string v2, "No ID found."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 59
    :cond_0
    return v0
.end method

.method public static varargs p([I)I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 66
    array-length v3, p0

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    aget v0, p0, v2

    .line 67
    if-eq v0, v1, :cond_0

    .line 71
    :goto_1
    return v0

    .line 66
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 71
    goto :goto_1
.end method
