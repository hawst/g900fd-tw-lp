.class public Ldao;
.super Lech;
.source "PG"

# interfaces
.implements Ldam;
.implements Lddc;
.implements Leoj;
.implements Leti;


# instance fields
.field private final aNA:Lhym;

.field private final bpL:Ldal;

.field private bpQ:Ldee;

.field private bpR:Z

.field private bpS:Lcom/google/android/search/shared/actions/VoiceAction;

.field private bpT:Lbwq;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bpU:Z

.field private bpV:Z

.field private bpW:Z

.field private final mActionState:Ldbd;

.field private mBackgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mClock:Lemp;

.field private mContext:Landroid/content/Context;

.field private final mEventBus:Ldda;

.field private final mFactory:Lgpu;

.field private final mHotwordState:Ldby;

.field private final mIntentStarter:Leqp;

.field private final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mScreenStateHelper:Ldmh;

.field private mSearchController:Lcjt;

.field private final mSearchPlateState:Ldct;

.field private final mSearchSettings:Lcke;

.field private final mSearchUrlHelper:Lcpn;

.field private mServiceState:Ldcu;

.field private final mSettingsUtil:Lbyl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgql;Ldaz;Leqp;Ldmh;Lbyl;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 123
    invoke-direct {p0}, Lech;-><init>()V

    .line 114
    iput-boolean v0, p0, Ldao;->bpV:Z

    .line 117
    iput-boolean v0, p0, Ldao;->bpW:Z

    .line 124
    iput-object p1, p0, Ldao;->mContext:Landroid/content/Context;

    .line 125
    invoke-virtual {p2}, Lgql;->aJU()Lgpu;

    move-result-object v0

    invoke-virtual {v0, p0, p3, p4}, Lgpu;->a(Ldao;Ldaz;Leqp;)Lcjt;

    move-result-object v0

    iput-object v0, p0, Ldao;->mSearchController:Lcjt;

    .line 127
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nf()Ldda;

    move-result-object v0

    iput-object v0, p0, Ldao;->mEventBus:Ldda;

    .line 128
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 129
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    iput-object v0, p0, Ldao;->mActionState:Ldbd;

    .line 130
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aal()Ldby;

    move-result-object v0

    iput-object v0, p0, Ldao;->mHotwordState:Ldby;

    .line 131
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aao()Ldcu;

    move-result-object v0

    iput-object v0, p0, Ldao;->mServiceState:Ldcu;

    .line 132
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    iput-object v0, p0, Ldao;->mSearchPlateState:Ldct;

    .line 133
    invoke-virtual {p2}, Lgql;->aJU()Lgpu;

    move-result-object v0

    iput-object v0, p0, Ldao;->mFactory:Lgpu;

    .line 134
    iget-object v0, p2, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v0}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Ldao;->mBackgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 135
    iput-object p6, p0, Ldao;->mSettingsUtil:Lbyl;

    .line 136
    iput-object p4, p0, Ldao;->mIntentStarter:Leqp;

    .line 137
    invoke-virtual {p2}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->BK()Lcke;

    move-result-object v0

    iput-object v0, p0, Ldao;->mSearchSettings:Lcke;

    .line 138
    invoke-virtual {p2}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DC()Lemp;

    move-result-object v0

    iput-object v0, p0, Ldao;->mClock:Lemp;

    .line 139
    invoke-virtual {p2}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DI()Lcpn;

    move-result-object v0

    iput-object v0, p0, Ldao;->mSearchUrlHelper:Lcpn;

    .line 140
    new-instance v0, Ldal;

    invoke-direct {v0, p0, p1}, Ldal;-><init>(Ldam;Landroid/content/Context;)V

    iput-object v0, p0, Ldao;->bpL:Ldal;

    .line 141
    iput-object p5, p0, Ldao;->mScreenStateHelper:Ldmh;

    .line 142
    invoke-virtual {p2}, Lgql;->aJq()Lhhq;

    move-result-object v0

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    iput-object v0, p0, Ldao;->aNA:Lhym;

    .line 143
    return-void
.end method

.method private UY()Ldee;
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Ldao;->bpQ:Ldee;

    if-nez v0, :cond_0

    .line 187
    iget-object v0, p0, Ldao;->mFactory:Lgpu;

    iget-object v1, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v2, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, p0, v1, v2}, Lgpu;->a(Leqp;Lcom/google/android/search/core/state/QueryState;Ldcu;)Ldee;

    move-result-object v0

    iput-object v0, p0, Ldao;->bpQ:Ldee;

    .line 190
    :cond_0
    iget-object v0, p0, Ldao;->bpQ:Ldee;

    return-object v0
.end method

.method private Vc()Z
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nb()Ldah;

    move-result-object v0

    .line 501
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldah;->Ux()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Vd()Z
    .locals 5

    .prologue
    .line 644
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->YX()Z

    move-result v0

    .line 645
    iget-object v1, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v1}, Ldcu;->YY()Z

    move-result v1

    .line 646
    iget-object v2, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v2}, Ldcu;->YZ()Z

    move-result v2

    .line 648
    iget-object v3, p0, Ldao;->mSettingsUtil:Lbyl;

    invoke-virtual {v3}, Lbyl;->BP()Z

    move-result v3

    .line 649
    iget-object v4, p0, Ldao;->mSettingsUtil:Lbyl;

    invoke-virtual {v4}, Lbyl;->BQ()Z

    move-result v4

    .line 651
    if-eqz v4, :cond_0

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    :cond_0
    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 659
    :goto_0
    return v0

    .line 651
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final Bd()I
    .locals 1

    .prologue
    .line 839
    const/16 v0, 0x1fff

    return v0
.end method

.method public final Bq()V
    .locals 3

    .prologue
    .line 606
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    .line 607
    iget-object v1, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    .line 608
    invoke-virtual {v0}, Ldcw;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 610
    invoke-virtual {v0}, Ldcw;->Zq()V

    .line 612
    :cond_0
    invoke-virtual {v1}, Ldbd;->VI()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 613
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VN()Z

    .line 615
    :cond_1
    return-void
.end method

.method public final CR()Z
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nb()Ldah;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->CR()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Nc()V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nc()V

    .line 431
    return-void
.end method

.method public final UF()V
    .locals 1

    .prologue
    .line 555
    invoke-direct {p0}, Ldao;->Vc()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->UF()V

    .line 560
    :cond_0
    return-void
.end method

.method public final UH()Lcjt;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    return-object v0
.end method

.method public final UV()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 762
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, v3}, Ldcu;->de(Z)V

    .line 763
    iget-object v0, p0, Ldao;->mHotwordState:Ldby;

    invoke-virtual {v0, v3}, Ldby;->de(Z)V

    .line 765
    iget-object v0, p0, Ldao;->mActionState:Ldbd;

    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    .line 766
    iget-boolean v0, p0, Ldao;->bpW:Z

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/google/android/search/shared/actions/PhoneCallAction;

    if-nez v0, :cond_1

    instance-of v0, v1, Lcom/google/android/search/shared/actions/LocalResultsAction;

    if-nez v0, :cond_1

    .line 816
    :cond_0
    :goto_0
    return-void

    .line 773
    :cond_1
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->YY()Z

    move-result v5

    .line 774
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->YX()Z

    move-result v6

    .line 775
    iget-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v3

    .line 777
    :goto_1
    instance-of v0, v1, Lcom/google/android/search/shared/actions/PhoneCallAction;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Lcom/google/android/search/shared/actions/PhoneCallAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PhoneCallAction;->ahz()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    .line 779
    :goto_2
    instance-of v7, v1, Lcom/google/android/search/shared/actions/LocalResultsAction;

    if-eqz v7, :cond_6

    check-cast v1, Lcom/google/android/search/shared/actions/LocalResultsAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahA()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/LocalResultsAction;->ahz()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    :goto_3
    if-eqz v1, :cond_6

    move v1, v3

    .line 797
    :goto_4
    if-nez v6, :cond_0

    if-nez v5, :cond_0

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Ldao;->bpW:Z

    if-nez v2, :cond_2

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    .line 799
    :cond_2
    iget-object v0, p0, Ldao;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 801
    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v1

    iput-boolean v1, p0, Ldao;->bpV:Z

    .line 802
    iput-boolean v3, p0, Ldao;->bpU:Z

    .line 804
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 805
    new-instance v1, Ldaq;

    const-string v2, "Turn speakerphone on"

    new-array v3, v4, [I

    invoke-direct {v1, p0, v2, v3, v0}, Ldaq;-><init>(Ldao;Ljava/lang/String;[ILandroid/media/AudioManager;)V

    .line 814
    iget-object v0, p0, Ldao;->mBackgroundExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x96

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    :cond_3
    move v2, v4

    .line 775
    goto :goto_1

    :cond_4
    move v0, v4

    .line 777
    goto :goto_2

    :cond_5
    move v1, v4

    .line 779
    goto :goto_3

    :cond_6
    move v1, v4

    goto :goto_4
.end method

.method public final UW()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 826
    iput-boolean v2, p0, Ldao;->bpW:Z

    .line 827
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, v2}, Ldcu;->de(Z)V

    .line 828
    iget-object v0, p0, Ldao;->mHotwordState:Ldby;

    invoke-virtual {v0, v2}, Ldby;->de(Z)V

    .line 829
    iget-boolean v0, p0, Ldao;->bpU:Z

    if-eqz v0, :cond_0

    .line 830
    iget-object v0, p0, Ldao;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iget-boolean v1, p0, Ldao;->bpV:Z

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 832
    iput-boolean v2, p0, Ldao;->bpU:Z

    .line 834
    :cond_0
    return-void
.end method

.method final UX()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldcu;->ds(Z)V

    .line 159
    return-void
.end method

.method public final UZ()V
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->UZ()V

    .line 403
    return-void
.end method

.method public final Va()V
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Va()V

    .line 407
    return-void
.end method

.method public final Vb()V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Vb()V

    .line 439
    return-void
.end method

.method public final Ve()V
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Ve()V

    .line 671
    return-void
.end method

.method public final a(Lbwp;ZZ)V
    .locals 2

    .prologue
    .line 632
    invoke-direct {p0}, Ldao;->Vd()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lbwp;->Bc()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 637
    :goto_0
    invoke-static {p1}, Lbwp;->a(Lbwp;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 639
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0, p1, p2, p3}, Lcjt;->a(Lbwp;ZZ)V

    .line 641
    :cond_1
    return-void

    .line 632
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;I)V
    .locals 3

    .prologue
    .line 619
    invoke-static {}, Lenu;->auR()V

    .line 620
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->ahN()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p2, v2}, Ldbd;->a(Lcom/google/android/search/shared/actions/VoiceAction;IZ)V

    .line 621
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 480
    invoke-direct {p0}, Ldao;->UY()Ldee;

    move-result-object v1

    const/16 v0, 0x69

    invoke-virtual {v1, p1, p2, v0}, Ldee;->a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;I)Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asz()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    iget-object v2, v1, Ldee;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iget-object v2, v1, Ldee;->mServiceState:Ldcu;

    invoke-virtual {v2}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->anu()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoZ()Lcom/google/android/shared/search/Query;

    move-result-object v0

    :cond_1
    iget-object v1, v1, Ldee;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v0}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    .line 481
    :goto_0
    return-void

    .line 480
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->ass()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v1, Ldee;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/shared/search/Query;->x(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->ask()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/shared/search/Query;->kT(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoY()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iget-object v2, v1, Ldee;->mServiceState:Ldcu;

    invoke-virtual {v2}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->anu()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoZ()Lcom/google/android/shared/search/Query;

    move-result-object v0

    :cond_3
    iget-object v1, v1, Ldee;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v0}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    goto :goto_0

    :cond_4
    iget-object v0, v1, Ldee;->bvh:Leeh;

    invoke-static {p1}, Leeh;->o(Lcom/google/android/shared/search/Suggestion;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v1, v0}, Ldee;->q(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    iget-object v0, v1, Ldee;->bvh:Leeh;

    invoke-virtual {v0, p1, v5}, Leeh;->a(Lcom/google/android/shared/search/Suggestion;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asy()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_6
    iget-object v2, v1, Ldee;->mServiceState:Ldcu;

    invoke-virtual {v2}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhgn;->g(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_7
    iget-object v2, v1, Ldee;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    iget-object v4, v1, Ldee;->mServiceState:Ldcu;

    invoke-virtual {v4}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v0, v5, v3, v4}, Lhgn;->a(Landroid/content/Context;Landroid/content/Intent;ZZLjava/lang/String;)V

    invoke-virtual {v1, v0}, Ldee;->q(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;ZLcom/google/android/shared/search/SearchBoxStats;)V
    .locals 2

    .prologue
    .line 491
    invoke-direct {p0}, Ldao;->UY()Ldee;

    move-result-object v0

    if-eqz p2, :cond_0

    const/16 v1, 0x93

    invoke-virtual {v0, p1, p3, v1}, Ldee;->a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;I)Lcom/google/android/shared/search/SearchBoxStats;

    .line 492
    :cond_0
    return-void
.end method

.method public final a(Ldah;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0, p1}, Lcjt;->a(Ldah;)V

    .line 164
    return-void
.end method

.method public final a(Lddb;)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 262
    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->YG()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nm()V

    .line 268
    :cond_0
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nb()Ldah;

    move-result-object v9

    .line 269
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Ldah;->Ux()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 367
    :cond_1
    :goto_0
    return-void

    .line 274
    :cond_2
    invoke-virtual {v9}, Ldah;->Ne()Ldyl;

    move-result-object v0

    .line 275
    invoke-virtual {p1}, Lddb;->aaC()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 276
    iget-object v1, p0, Ldao;->mSearchPlateState:Ldct;

    invoke-virtual {v1}, Ldct;->Yv()I

    move-result v1

    iget-object v2, p0, Ldao;->mSearchPlateState:Ldct;

    invoke-virtual {v2}, Ldct;->tR()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, v8}, Ldyl;->a(ILjava/lang/String;Z)V

    .line 277
    iget-object v1, p0, Ldao;->mSearchPlateState:Ldct;

    invoke-virtual {v1}, Ldct;->Yy()I

    move-result v1

    invoke-interface {v0, v1}, Ldyl;->en(I)V

    .line 279
    iget-boolean v1, p0, Ldao;->bpR:Z

    if-nez v1, :cond_3

    .line 280
    iget-object v1, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v1

    if-eqz v1, :cond_9

    new-instance v2, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    invoke-direct {v2, v1}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;-><init>(Lcom/google/android/search/shared/actions/VoiceAction;)V

    invoke-interface {v0, v2}, Ldyl;->a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V

    .line 284
    :cond_3
    :goto_1
    invoke-virtual {p1}, Lddb;->aaD()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 285
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Zf()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 290
    invoke-virtual {v9}, Ldah;->Uv()V

    .line 291
    const/16 v0, 0x11c

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 294
    new-array v0, v7, [Landroid/content/Intent;

    iget-object v1, p0, Ldao;->mContext:Landroid/content/Context;

    iget-object v2, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v2

    iget-object v3, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aaj()Ldbd;

    move-result-object v3

    invoke-virtual {v3}, Ldbd;->Vo()Lcom/google/android/velvet/ActionData;

    move-result-object v3

    invoke-virtual {v9}, Ldah;->getId()J

    move-result-wide v4

    iget-object v6, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v6}, Ldcu;->Zg()I

    move-result v6

    const/4 v10, 0x2

    if-ne v6, v10, :cond_a

    move v6, v7

    :goto_2
    invoke-static/range {v1 .. v6}, Lhgn;->a(Landroid/content/Context;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;JZ)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-virtual {p0, v0}, Ldao;->b([Landroid/content/Intent;)Z

    .line 305
    :cond_4
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 306
    iget-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->XI()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 307
    if-eqz v0, :cond_5

    .line 308
    invoke-virtual {v9, v0}, Ldah;->w(Lcom/google/android/shared/search/Query;)V

    .line 312
    :cond_5
    invoke-virtual {v9}, Ldah;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    .line 315
    iget-object v1, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    .line 316
    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 317
    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 319
    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->ana()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 320
    invoke-virtual {v1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    .line 324
    iget-object v3, p0, Ldao;->bpS:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-static {v3, v2}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 325
    iput-object v2, p0, Ldao;->bpS:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 326
    iget-object v2, p0, Ldao;->bpS:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v1, v2}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    .line 333
    iget-object v3, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v3}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v3

    iget-object v4, p0, Ldao;->bpS:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-virtual {v9, v3, v4, v2}, Ldah;->a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V

    .line 339
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anp()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v1}, Ldbd;->Vy()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 341
    invoke-virtual {v9}, Ldah;->CV()V

    .line 345
    :cond_7
    invoke-virtual {p1}, Lddb;->aaG()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_8
    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 346
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaq()Ldbi;

    move-result-object v0

    invoke-virtual {v0}, Ldbi;->Wk()Lcom/google/android/shared/search/Query;

    move-result-object v2

    .line 347
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaq()Ldbi;

    move-result-object v0

    invoke-virtual {v0}, Ldbi;->Qv()Ljyl;

    move-result-object v0

    .line 349
    iget-object v1, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaq()Ldbi;

    move-result-object v1

    invoke-virtual {v1}, Ldbi;->Wj()Ljava/util/List;

    move-result-object v1

    .line 351
    if-eqz v1, :cond_b

    .line 352
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    .line 353
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, [F

    invoke-virtual {v9, v2, v0, v1}, Ldah;->a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V

    goto :goto_3

    .line 280
    :cond_9
    iget-object v1, p0, Ldao;->mSearchPlateState:Ldct;

    invoke-virtual {v1}, Ldct;->Yw()I

    move-result v1

    iget-object v2, p0, Ldao;->mSearchPlateState:Ldct;

    invoke-virtual {v2}, Ldct;->Yx()I

    move-result v2

    invoke-interface {v0, v1, v2, v8}, Ldyl;->c(IIZ)V

    iget-object v1, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-interface {v0, v1}, Ldyl;->z(Lcom/google/android/shared/search/Query;)V

    iget-object v1, p0, Ldao;->mSearchPlateState:Ldct;

    invoke-virtual {v1}, Ldct;->getProgress()I

    move-result v1

    invoke-interface {v0, v1}, Ldyl;->fv(I)V

    goto/16 :goto_1

    :cond_a
    move v6, v8

    .line 294
    goto/16 :goto_2

    .line 355
    :cond_b
    if-eqz v0, :cond_c

    .line 356
    invoke-virtual {v9, v0}, Ldah;->c(Ljyl;)V

    .line 361
    :cond_c
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZU()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Ldao;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Ldao;->mSearchSettings:Lcke;

    invoke-interface {v2, v0, v1}, Lcke;->X(J)V

    iget-object v0, p0, Ldao;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->OA()V

    iget-object v0, p0, Ldao;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0}, Lcpn;->RF()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v0, p0, Ldao;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/velvet/ui/InAppWebPageActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "full_screen"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v1}, Lcjt;->Nb()Ldah;

    move-result-object v1

    new-array v2, v7, [Landroid/content/Intent;

    aput-object v0, v2, v8

    invoke-virtual {v1, v2}, Ldah;->a([Landroid/content/Intent;)V

    const/16 v0, 0x112

    invoke-static {v0}, Lege;->ht(I)V

    goto/16 :goto_0
.end method

.method public final a(Letj;)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {p1, v0}, Letj;->b(Leti;)V

    .line 200
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v0

    .line 201
    const-string v1, "Recent log events"

    invoke-virtual {v0, v1}, Letj;->lt(Ljava/lang/String;)V

    .line 202
    invoke-static {v0}, Lege;->c(Letj;)V

    .line 203
    return-void
.end method

.method public final a(ZZI)V
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, p1, p2, p3}, Ldcu;->b(ZZI)V

    .line 423
    return-void
.end method

.method public final a(ZZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, p1, p2}, Ldcu;->m(ZZ)V

    .line 387
    return-void
.end method

.method public final a(Landroid/content/Intent;Leol;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 241
    const-string v0, "com.google.android.shared.util.SimpleIntentStarter.START_ACTIVITY_FOR_RESULT"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 242
    new-array v0, v1, [Landroid/content/Intent;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Ldao;->b([Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public final ag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 663
    invoke-direct {p0}, Ldao;->Vd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0, p2, p1}, Lcjt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 2

    .prologue
    .line 485
    invoke-direct {p0}, Ldao;->UY()Ldee;

    move-result-object v0

    const/16 v1, 0x69

    invoke-virtual {v0, p1, p2, v1}, Ldee;->a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;I)Lcom/google/android/shared/search/SearchBoxStats;

    .line 486
    return-void
.end method

.method public final b(ZIZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 675
    if-nez p2, :cond_1

    move v4, v1

    .line 677
    :goto_0
    if-ne p2, v1, :cond_2

    move v0, v1

    .line 707
    :goto_1
    iget-object v3, p0, Ldao;->mScreenStateHelper:Ldmh;

    invoke-virtual {v3}, Ldmh;->isScreenOn()Z

    move-result v3

    .line 708
    if-eqz p1, :cond_3

    if-eqz v3, :cond_3

    move v3, v1

    .line 721
    :goto_2
    if-eqz v3, :cond_4

    if-nez v0, :cond_4

    if-nez p3, :cond_4

    .line 724
    :goto_3
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, v1}, Ldcu;->dq(Z)V

    .line 731
    if-eqz v1, :cond_0

    if-eqz v4, :cond_0

    .line 732
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nn()V

    .line 734
    :cond_0
    return-void

    :cond_1
    move v4, v2

    .line 675
    goto :goto_0

    :cond_2
    move v0, v2

    .line 677
    goto :goto_1

    :cond_3
    move v3, v2

    .line 708
    goto :goto_2

    :cond_4
    move v1, v2

    .line 721
    goto :goto_3
.end method

.method public final varargs b([Landroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 210
    array-length v0, p1

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 213
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->Nb()Ldah;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_2

    .line 216
    invoke-virtual {v0, p1}, Ldah;->a([Landroid/content/Intent;)V

    .line 227
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 210
    goto :goto_0

    .line 217
    :cond_2
    sget-object v0, Lcgg;->aVt:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcgg;->aVr:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    :cond_3
    array-length v0, p1

    :goto_2
    if-ge v2, v0, :cond_4

    aget-object v1, p1, v2

    .line 223
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 222
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 225
    :cond_4
    iget-object v0, p0, Ldao;->mIntentStarter:Leqp;

    invoke-interface {v0, p1}, Leqp;->b([Landroid/content/Intent;)Z

    move-result v1

    goto :goto_1
.end method

.method public final bq(Z)V
    .locals 1

    .prologue
    .line 584
    invoke-static {}, Lenu;->auR()V

    .line 585
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldao;->bpR:Z

    .line 586
    iget-object v0, p0, Ldao;->mHotwordState:Ldby;

    invoke-virtual {v0, p1}, Ldby;->dc(Z)V

    .line 587
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldao;->bpR:Z

    .line 588
    return-void
.end method

.method public final varargs c([Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Ldao;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lepd;->a(Landroid/content/pm/PackageManager;[Landroid/content/Intent;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/shared/search/Suggestion;)V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->MW()Ldfl;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldfl;->c(Lcom/google/android/shared/search/Suggestion;)V

    .line 497
    return-void
.end method

.method public final c(ZI)V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, p1, p2}, Ldcu;->c(ZI)V

    .line 399
    return-void
.end method

.method public final cS(Z)V
    .locals 0

    .prologue
    .line 372
    iput-boolean p1, p0, Ldao;->bpR:Z

    .line 373
    return-void
.end method

.method public final cT(Z)V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, p1}, Ldcu;->do(Z)V

    .line 391
    return-void
.end method

.method public final cU(Z)V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, p1}, Ldcu;->dp(Z)V

    .line 395
    return-void
.end method

.method public final cV(Z)V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, p1}, Ldcu;->dm(Z)V

    .line 411
    return-void
.end method

.method public final cW(Z)V
    .locals 4

    .prologue
    .line 414
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    iget-object v1, p0, Ldao;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, Ldcu;->a(ZJ)V

    .line 415
    return-void
.end method

.method public final cX(Z)V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, p1}, Ldcu;->dn(Z)V

    .line 419
    return-void
.end method

.method public final cY(Z)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 506
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v9

    .line 508
    if-eqz p1, :cond_2

    .line 509
    iget-object v0, p0, Ldao;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->adq()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v5

    :goto_0
    iget-object v1, p0, Ldao;->mScreenStateHelper:Ldmh;

    invoke-virtual {v1}, Ldmh;->isScreenOn()Z

    move-result v1

    iget-object v2, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v2}, Ldcu;->Zc()Z

    move-result v2

    iget-object v3, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v3}, Ldcu;->YZ()Z

    move-result v3

    invoke-virtual {v9}, Lcom/google/android/search/shared/service/ClientConfig;->YA()Z

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Ldmu;->a(ZZZZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 523
    :goto_1
    invoke-virtual {v9}, Lcom/google/android/search/shared/service/ClientConfig;->amT()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524
    iget-object v1, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    const-wide/16 v2, 0x64

    const-wide/16 v4, 0x1

    move-object v8, v6

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/search/core/state/QueryState;->a(JJLandroid/os/Bundle;ILandroid/os/Bundle;)V

    .line 527
    :cond_0
    iget-object v1, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v9}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/shared/search/Query;->c(Lcom/google/android/shared/search/SearchBoxStats;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    .line 528
    return-void

    :cond_1
    move v0, v7

    .line 509
    goto :goto_0

    .line 514
    :cond_2
    iget-object v0, p0, Ldao;->aNA:Lhym;

    iget-object v1, p0, Ldao;->aNA:Lhym;

    invoke-virtual {v1}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhym;->ow(Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Ldao;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->adq()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v5

    :goto_2
    iget-object v1, p0, Ldao;->mScreenStateHelper:Ldmh;

    invoke-virtual {v1}, Ldmh;->isScreenOn()Z

    move-result v1

    iget-object v2, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v2}, Ldcu;->Zc()Z

    move-result v2

    iget-object v3, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v3}, Ldcu;->YZ()Z

    move-result v3

    invoke-virtual {v9}, Lcom/google/android/search/shared/service/ClientConfig;->YA()Z

    move-result v4

    sget-object v8, Lcgg;->aVs:Lcgg;

    invoke-virtual {v8}, Lcgg;->isEnabled()Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Ldao;->aNA:Lhym;

    invoke-virtual {v8}, Lhym;->aTQ()Z

    move-result v8

    if-eqz v8, :cond_4

    :goto_3
    invoke-static/range {v0 .. v5}, Ldmu;->a(ZZZZZZ)Lcom/google/android/shared/search/Query;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_2

    :cond_4
    move v5, v7

    goto :goto_3
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xb()Z

    .line 575
    return-void
.end method

.method public final create()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Ldao;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->OC()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const/16 v0, 0x101

    invoke-static {v0}, Lege;->ht(I)V

    .line 151
    :cond_0
    iget-object v0, p0, Ldao;->mSearchSettings:Lcke;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcke;->cr(Z)V

    .line 152
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->start()V

    .line 153
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->c(Lddc;)V

    .line 154
    iget-object v0, p0, Ldao;->bpL:Ldal;

    invoke-virtual {v0}, Ldal;->startListening()V

    .line 155
    return-void
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcjt;->a(Ldah;)V

    .line 178
    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    invoke-virtual {v0, p0}, Ldda;->d(Lddc;)V

    .line 179
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->stop()V

    .line 180
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->ca()V

    .line 181
    iget-object v0, p0, Ldao;->bpL:Ldal;

    invoke-virtual {v0}, Ldal;->stopListening()V

    .line 182
    iget-object v0, p0, Ldao;->mSearchSettings:Lcke;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcke;->cr(Z)V

    .line 183
    return-void
.end method

.method public final eA()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0}, Lcjt;->eA()V

    .line 195
    return-void
.end method

.method public final eX(I)V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Ldao;->mSearchController:Lcjt;

    invoke-virtual {v0, p1}, Lcjt;->eX(I)V

    .line 427
    return-void
.end method

.method public final jp(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 739
    iput-boolean v8, p0, Ldao;->bpW:Z

    .line 741
    invoke-direct {p0}, Ldao;->Vd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 742
    iget-object v0, p0, Ldao;->bpT:Lbwq;

    if-nez v0, :cond_0

    .line 743
    new-instance v0, Lbwq;

    iget-object v1, p0, Ldao;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lbwq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldao;->bpT:Lbwq;

    .line 745
    :cond_0
    iget-object v1, p0, Ldao;->bpT:Lbwq;

    iget-object v0, p0, Ldao;->mEventBus:Ldda;

    iget-object v2, p0, Ldao;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0977

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v5, Ldap;

    invoke-direct {v5, p0}, Ldap;-><init>(Ldao;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, Ldda;->c(Lddc;)V

    new-instance v0, Lbwr;

    const-string v2, "Call announcement"

    new-array v3, v8, [I

    const/4 v4, 0x0

    const/4 v7, 0x2

    aput v7, v3, v4

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lbwr;-><init>(Lbwq;Ljava/lang/String;[ILjava/lang/String;Lbwt;Ljava/lang/String;)V

    iget-object v1, v1, Lbwq;->aMC:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x3e8

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 755
    :cond_1
    iget-object v0, p0, Ldao;->mServiceState:Ldcu;

    invoke-virtual {v0, v8}, Ldcu;->de(Z)V

    .line 756
    iget-object v0, p0, Ldao;->mHotwordState:Ldby;

    invoke-virtual {v0, v8}, Ldby;->de(Z)V

    .line 757
    return-void
.end method

.method public final stopListening()V
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->N(Lcom/google/android/shared/search/Query;)V

    .line 580
    return-void
.end method

.method public final x(Lcom/google/android/shared/search/Query;)V
    .locals 2

    .prologue
    .line 535
    iget-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/shared/search/Query;->aT(Lcom/google/android/shared/search/Query;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->x(Lcom/google/android/shared/search/Query;)V

    .line 536
    return-void
.end method

.method public final y(Lcom/google/android/shared/search/Query;)V
    .locals 4

    .prologue
    .line 541
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqQ()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    if-nez v0, :cond_0

    .line 543
    const-string v0, "ISearchServiceImpl"

    const-string v1, "Any committed query should have SearchBoxStats."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 545
    :cond_0
    invoke-direct {p0}, Ldao;->Vc()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Ldao;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    .line 550
    :cond_1
    return-void
.end method
