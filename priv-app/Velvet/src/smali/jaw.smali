.class public final Ljaw;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dXt:[Ljaw;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field private bmN:Ljava/lang/String;

.field private dRq:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljaw;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljaw;->akf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljaw;->bmN:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljaw;->dRq:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljaw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljaw;->eCz:I

    return-void
.end method

.method public static beE()[Ljaw;
    .locals 2

    sget-object v0, Ljaw;->dXt:[Ljaw;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljaw;->dXt:[Ljaw;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljaw;

    sput-object v0, Ljaw;->dXt:[Ljaw;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljaw;->dXt:[Ljaw;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljaw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaw;->akf:Ljava/lang/String;

    iget v0, p0, Ljaw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljaw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljaw;->bmN:Ljava/lang/String;

    iget v0, p0, Ljaw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljaw;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljaw;->dRq:J

    iget v0, p0, Ljaw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljaw;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljaw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljaw;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljaw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljaw;->bmN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljaw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v2, p0, Ljaw;->dRq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljaw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljaw;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljaw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljaw;->bmN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljaw;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Ljaw;->dRq:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    return v0
.end method
