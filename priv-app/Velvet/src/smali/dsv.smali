.class public final Ldsv;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field private final bJE:Landroid/graphics/Paint;

.field private final bJF:F

.field private final bJG:F

.field private bJH:F

.field private bJI:I

.field private hO:Z


# direct methods
.method public constructor <init>(III)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 32
    int-to-float v0, p2

    iput v0, p0, Ldsv;->bJG:F

    .line 33
    int-to-float v0, p1

    iget v1, p0, Ldsv;->bJG:F

    div-float/2addr v0, v1

    iput v0, p0, Ldsv;->bJF:F

    .line 35
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ldsv;->bJE:Landroid/graphics/Paint;

    .line 36
    iget-object v0, p0, Ldsv;->bJE:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 37
    iget-object v0, p0, Ldsv;->bJE:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 38
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 53
    iget-boolean v0, p0, Ldsv;->hO:Z

    if-nez v0, :cond_0

    .line 67
    :goto_0
    return-void

    .line 58
    :cond_0
    iget v0, p0, Ldsv;->bJI:I

    int-to-float v0, v0

    iget v1, p0, Ldsv;->bJH:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 59
    iget v0, p0, Ldsv;->bJH:F

    iget v1, p0, Ldsv;->bJI:I

    int-to-float v1, v1

    iget v2, p0, Ldsv;->bJH:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Ldsv;->bJH:F

    .line 64
    :goto_1
    iget v0, p0, Ldsv;->bJF:F

    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Ldsv;->bJF:F

    sub-float/2addr v1, v2

    iget v2, p0, Ldsv;->bJH:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ldsv;->bJG:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 66
    invoke-virtual {p0}, Ldsv;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Ldsv;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    iget-object v3, p0, Ldsv;->bJE:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 61
    :cond_1
    iget v0, p0, Ldsv;->bJH:F

    const v1, 0x3f733333    # 0.95f

    mul-float/2addr v0, v1

    iput v0, p0, Ldsv;->bJH:F

    goto :goto_1
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, -0x3

    return v0
.end method

.method public final gp(I)V
    .locals 0

    .prologue
    .line 42
    iput p1, p0, Ldsv;->bJI:I

    .line 43
    invoke-virtual {p0}, Ldsv;->invalidateSelf()V

    .line 44
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ldsv;->bJE:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 72
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Ldsv;->bJE:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 77
    return-void
.end method

.method public final setEnabled(Z)V
    .locals 0

    .prologue
    .line 47
    iput-boolean p1, p0, Ldsv;->hO:Z

    .line 48
    invoke-virtual {p0}, Ldsv;->invalidateSelf()V

    .line 49
    return-void
.end method
