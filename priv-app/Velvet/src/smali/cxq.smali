.class public final Lcxq;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private bkR:Landroid/preference/SwitchPreference;

.field private final ble:Lhxr;

.field private final mActivity:Landroid/app/Activity;

.field private final mSettings:Lhym;


# direct methods
.method public constructor <init>(Lcke;Landroid/app/Activity;Lhym;Lhxr;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 35
    iput-object p2, p0, Lcxq;->mActivity:Landroid/app/Activity;

    .line 36
    iput-object p3, p0, Lcxq;->mSettings:Lhym;

    .line 37
    iput-object p4, p0, Lcxq;->ble:Lhxr;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 42
    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Lcxq;->bkR:Landroid/preference/SwitchPreference;

    .line 43
    iget-object v0, p0, Lcxq;->bkR:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 44
    iget-object v0, p0, Lcxq;->bkR:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcxq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFa()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 45
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcxq;->ble:Lhxr;

    invoke-virtual {v0}, Lhxr;->aTt()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 54
    if-nez p2, :cond_0

    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    .line 56
    :cond_0
    check-cast p2, Ljava/lang/Boolean;

    .line 57
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.voicesearch.action.PERSONALIZATION_OPT_IN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 59
    const-string v1, "PERSONALIZATION_OPT_IN_ENABLE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 62
    :try_start_0
    iget-object v1, p0, Lcxq;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    const-string v1, "PersonalizationSettingController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t start personalization opt-in: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcxq;->bkR:Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcxq;->bkR:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcxq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFa()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 74
    :cond_0
    return-void
.end method
