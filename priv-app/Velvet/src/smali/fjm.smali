.class public Lfjm;
.super Landroid/appwidget/AppWidgetProvider;
.source "PG"


# instance fields
.field private arA:Lfjq;

.field private ctz:Lchg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 174
    return-void
.end method


# virtual methods
.method public final a(Lcha;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lfjm;->ctz:Lchg;

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lfjm;->ctz:Lchg;

    invoke-virtual {p1, v0}, Lcha;->b(Lchg;)V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lfjm;->ctz:Lchg;

    goto :goto_0
.end method

.method public final a(Lcha;Lfjo;)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lfjm;->ctz:Lchg;

    if-eqz v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 151
    :cond_0
    new-instance v0, Lfjn;

    invoke-direct {v0, p0, p2}, Lfjn;-><init>(Lfjm;Lfjo;)V

    iput-object v0, p0, Lfjm;->ctz:Lchg;

    .line 157
    iget-object v0, p0, Lfjm;->ctz:Lchg;

    invoke-virtual {p1, v0}, Lcha;->a(Lchg;)V

    goto :goto_0
.end method

.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lfjm;->arA:Lfjq;

    invoke-interface {v0, p1, p3, p4}, Lfjq;->a(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 87
    invoke-virtual {p2, p3, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 88
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 112
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lcfo;->DR()Lcpx;

    move-result-object v1

    const-string v2, "WIDGET_REMOVED"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {v0}, Lcfo;->Em()Lcha;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfjm;->a(Lcha;)V

    .line 119
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 120
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 129
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v1

    .line 131
    invoke-virtual {v1}, Lcfo;->DR()Lcpx;

    move-result-object v2

    const-string v3, "WIDGET_INSTALLED"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcpx;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-virtual {v1}, Lcfo;->Em()Lcha;

    move-result-object v1

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axU()Lfjo;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lfjm;->a(Lcha;Lfjo;)V

    .line 137
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 138
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 43
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 44
    const-string v1, "internal_request"

    invoke-virtual {p2, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axU()Lfjo;

    move-result-object v0

    invoke-virtual {v0}, Lfjo;->aAt()V

    .line 80
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayk()Livq;

    move-result-object v0

    .line 55
    invoke-interface {v0}, Livq;->isDone()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    :try_start_0
    invoke-static {v0}, Lgtx;->c(Livq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjq;

    iput-object v0, p0, Lfjm;->arA:Lfjq;
    :try_end_0
    .catch Lgos; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    const-string v0, "PredictiveCardsWidgetProvider"

    const-string v1, "Failed to load widgets, disk full"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    :cond_1
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->Ev()Lgtx;

    move-result-object v0

    sget-object v1, Lfjl;->ctv:Lgor;

    invoke-virtual {v0, v1}, Lgtx;->b(Lgor;)V

    .line 69
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 70
    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v1

    invoke-virtual {v1}, Lgql;->SC()Lcfo;

    move-result-object v1

    invoke-virtual {v1}, Lcfo;->Ea()Ldjx;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    add-long/2addr v4, v6

    const/high16 v3, 0x8000000

    invoke-static {p1, v8, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v2, v4, v5, v0}, Ldjx;->a(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 93
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p3, v0

    .line 95
    invoke-virtual {p2, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v3

    .line 96
    iget-object v4, p0, Lfjm;->arA:Lfjq;

    invoke-interface {v4, p1, v2, v3}, Lfjq;->a(Landroid/content/Context;ILandroid/os/Bundle;)Landroid/widget/RemoteViews;

    move-result-object v3

    .line 97
    invoke-virtual {p2, v2, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_0
    array-length v0, p3

    if-lez v0, :cond_1

    .line 102
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DR()Lcpx;

    move-result-object v0

    const-string v1, "WIDGET_UPDATE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcpx;->Y(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 107
    return-void
.end method
