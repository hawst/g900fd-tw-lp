.class public final Leqf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lesm;


# instance fields
.field private final bSy:Lesm;

.field private final mExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lesm;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Leqf;->mExecutor:Ljava/util/concurrent/Executor;

    .line 32
    iput-object p2, p0, Leqf;->bSy:Lesm;

    .line 33
    return-void
.end method


# virtual methods
.method public final D(Landroid/net/Uri;)Leml;
    .locals 3

    .prologue
    .line 37
    new-instance v0, Leqd;

    iget-object v1, p0, Leqf;->mExecutor:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Leqf;->bSy:Lesm;

    invoke-interface {v2, p1}, Lesm;->D(Landroid/net/Uri;)Leml;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Leqd;-><init>(Ljava/util/concurrent/Executor;Leps;)V

    return-object v0
.end method

.method public final clearCache()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Leqf;->bSy:Lesm;

    invoke-interface {v0}, Lesm;->clearCache()V

    .line 43
    return-void
.end method

.method public final u(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Leqf;->bSy:Lesm;

    invoke-interface {v0, p1}, Lesm;->u(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method
