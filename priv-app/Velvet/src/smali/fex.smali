.class public final Lfex;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lffk;


# instance fields
.field private final mCalendarData:Lamk;


# direct methods
.method public constructor <init>(Lamk;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lfex;->mCalendarData:Lamk;

    .line 20
    return-void
.end method


# virtual methods
.method public final aK(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lfex;->mCalendarData:Lamk;

    iget-object v0, v0, Lamk;->aeQ:Lamo;

    invoke-virtual {v0}, Lamo;->nj()J

    move-result-wide v0

    .line 25
    invoke-static {v0, v1}, Lfzy;->bs(J)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final aL(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f0a03d8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aif()I
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f020187

    return v0
.end method

.method public final aza()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "EMAIL_GUESTS"

    return-object v0
.end method

.method public final azb()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const-string v0, "broadcast"

    return-object v0
.end method

.method public final azc()Z
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    return v0
.end method

.method public final isActive()Z
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lfex;->mCalendarData:Lamk;

    iget-object v0, v0, Lamk;->aeQ:Lamo;

    .line 46
    invoke-virtual {v0}, Lamo;->nr()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 51
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
