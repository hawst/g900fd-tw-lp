.class public final Ljkw;
.super Ljsl;
.source "PG"


# static fields
.field public static final eqw:Ljsm;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field private ecQ:Ljava/lang/String;

.field public eqA:Ljls;

.field public eqB:Ljkt;

.field private eqC:Ljava/lang/String;

.field public eqD:Lier;

.field public eqE:Liey;

.field public eqF:Ljle;

.field private eqG:I

.field private eqx:Ljsc;

.field private eqy:Ljava/lang/String;

.field public eqz:Ljkr;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9017
    const/16 v0, 0xb

    const-class v1, Ljkw;

    const v2, 0x1417f382

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljkw;->eqw:Ljsm;

    .line 9029
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 9164
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 9165
    iput v2, p0, Ljkw;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljkw;->akf:Ljava/lang/String;

    iput-object v1, p0, Ljkw;->eqx:Ljsc;

    const-string v0, ""

    iput-object v0, p0, Ljkw;->eqy:Ljava/lang/String;

    iput-object v1, p0, Ljkw;->eqz:Ljkr;

    iput-object v1, p0, Ljkw;->eqA:Ljls;

    iput-object v1, p0, Ljkw;->eqB:Ljkt;

    const-string v0, ""

    iput-object v0, p0, Ljkw;->eqC:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljkw;->ecQ:Ljava/lang/String;

    iput-object v1, p0, Ljkw;->eqD:Lier;

    iput-object v1, p0, Ljkw;->eqE:Liey;

    iput-object v1, p0, Ljkw;->eqF:Ljle;

    iput v2, p0, Ljkw;->eqG:I

    iput-object v1, p0, Ljkw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljkw;->eCz:I

    .line 9166
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 9010
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljkw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkw;->akf:Ljava/lang/String;

    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkw;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljkw;->eqx:Ljsc;

    if-nez v0, :cond_1

    new-instance v0, Ljsc;

    invoke-direct {v0}, Ljsc;-><init>()V

    iput-object v0, p0, Ljkw;->eqx:Ljsc;

    :cond_1
    iget-object v0, p0, Ljkw;->eqx:Ljsc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljkw;->eqz:Ljkr;

    if-nez v0, :cond_2

    new-instance v0, Ljkr;

    invoke-direct {v0}, Ljkr;-><init>()V

    iput-object v0, p0, Ljkw;->eqz:Ljkr;

    :cond_2
    iget-object v0, p0, Ljkw;->eqz:Ljkr;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljkw;->eqA:Ljls;

    if-nez v0, :cond_3

    new-instance v0, Ljls;

    invoke-direct {v0}, Ljls;-><init>()V

    iput-object v0, p0, Ljkw;->eqA:Ljls;

    :cond_3
    iget-object v0, p0, Ljkw;->eqA:Ljls;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkw;->eqC:Ljava/lang/String;

    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkw;->aez:I

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljkw;->eqB:Ljkt;

    if-nez v0, :cond_4

    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    iput-object v0, p0, Ljkw;->eqB:Ljkt;

    :cond_4
    iget-object v0, p0, Ljkw;->eqB:Ljkt;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkw;->ecQ:Ljava/lang/String;

    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljkw;->aez:I

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ljkw;->eqD:Lier;

    if-nez v0, :cond_5

    new-instance v0, Lier;

    invoke-direct {v0}, Lier;-><init>()V

    iput-object v0, p0, Ljkw;->eqD:Lier;

    :cond_5
    iget-object v0, p0, Ljkw;->eqD:Lier;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljkw;->eqy:Ljava/lang/String;

    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljkw;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljkw;->eqG:I

    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljkw;->aez:I

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Ljkw;->eqE:Liey;

    if-nez v0, :cond_6

    new-instance v0, Liey;

    invoke-direct {v0}, Liey;-><init>()V

    iput-object v0, p0, Ljkw;->eqE:Liey;

    :cond_6
    iget-object v0, p0, Ljkw;->eqE:Liey;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Ljkw;->eqF:Ljle;

    if-nez v0, :cond_7

    new-instance v0, Ljle;

    invoke-direct {v0}, Ljle;-><init>()V

    iput-object v0, p0, Ljkw;->eqF:Ljle;

    :cond_7
    iget-object v0, p0, Ljkw;->eqF:Ljle;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 9190
    iget v0, p0, Ljkw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 9191
    const/4 v0, 0x1

    iget-object v1, p0, Ljkw;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9193
    :cond_0
    iget-object v0, p0, Ljkw;->eqx:Ljsc;

    if-eqz v0, :cond_1

    .line 9194
    const/4 v0, 0x2

    iget-object v1, p0, Ljkw;->eqx:Ljsc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 9196
    :cond_1
    iget-object v0, p0, Ljkw;->eqz:Ljkr;

    if-eqz v0, :cond_2

    .line 9197
    const/4 v0, 0x3

    iget-object v1, p0, Ljkw;->eqz:Ljkr;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 9199
    :cond_2
    iget-object v0, p0, Ljkw;->eqA:Ljls;

    if-eqz v0, :cond_3

    .line 9200
    const/4 v0, 0x4

    iget-object v1, p0, Ljkw;->eqA:Ljls;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 9202
    :cond_3
    iget v0, p0, Ljkw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 9203
    const/4 v0, 0x5

    iget-object v1, p0, Ljkw;->eqC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9205
    :cond_4
    iget-object v0, p0, Ljkw;->eqB:Ljkt;

    if-eqz v0, :cond_5

    .line 9206
    const/4 v0, 0x6

    iget-object v1, p0, Ljkw;->eqB:Ljkt;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 9208
    :cond_5
    iget v0, p0, Ljkw;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_6

    .line 9209
    const/4 v0, 0x7

    iget-object v1, p0, Ljkw;->ecQ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9211
    :cond_6
    iget-object v0, p0, Ljkw;->eqD:Lier;

    if-eqz v0, :cond_7

    .line 9212
    const/16 v0, 0x8

    iget-object v1, p0, Ljkw;->eqD:Lier;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 9214
    :cond_7
    iget v0, p0, Ljkw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    .line 9215
    const/16 v0, 0x9

    iget-object v1, p0, Ljkw;->eqy:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 9217
    :cond_8
    iget v0, p0, Ljkw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 9218
    const/16 v0, 0xa

    iget v1, p0, Ljkw;->eqG:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 9220
    :cond_9
    iget-object v0, p0, Ljkw;->eqE:Liey;

    if-eqz v0, :cond_a

    .line 9221
    const/16 v0, 0xb

    iget-object v1, p0, Ljkw;->eqE:Liey;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 9223
    :cond_a
    iget-object v0, p0, Ljkw;->eqF:Ljle;

    if-eqz v0, :cond_b

    .line 9224
    const/16 v0, 0xc

    iget-object v1, p0, Ljkw;->eqF:Ljle;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 9226
    :cond_b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 9227
    return-void
.end method

.method public final aiw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9095
    iget-object v0, p0, Ljkw;->eqC:Ljava/lang/String;

    return-object v0
.end method

.method public final aiy()I
    .locals 1

    .prologue
    .line 9148
    iget v0, p0, Ljkw;->eqG:I

    return v0
.end method

.method public final bhU()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9117
    iget-object v0, p0, Ljkw;->ecQ:Ljava/lang/String;

    return-object v0
.end method

.method public final boC()Z
    .locals 1

    .prologue
    .line 9106
    iget v0, p0, Ljkw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final boD()Ljkw;
    .locals 1

    .prologue
    .line 9109
    const-string v0, ""

    iput-object v0, p0, Ljkw;->eqC:Ljava/lang/String;

    .line 9110
    iget v0, p0, Ljkw;->aez:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Ljkw;->aez:I

    .line 9111
    return-object p0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9039
    iget-object v0, p0, Ljkw;->akf:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 9231
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 9232
    iget v1, p0, Ljkw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 9233
    const/4 v1, 0x1

    iget-object v2, p0, Ljkw;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9236
    :cond_0
    iget-object v1, p0, Ljkw;->eqx:Ljsc;

    if-eqz v1, :cond_1

    .line 9237
    const/4 v1, 0x2

    iget-object v2, p0, Ljkw;->eqx:Ljsc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9240
    :cond_1
    iget-object v1, p0, Ljkw;->eqz:Ljkr;

    if-eqz v1, :cond_2

    .line 9241
    const/4 v1, 0x3

    iget-object v2, p0, Ljkw;->eqz:Ljkr;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9244
    :cond_2
    iget-object v1, p0, Ljkw;->eqA:Ljls;

    if-eqz v1, :cond_3

    .line 9245
    const/4 v1, 0x4

    iget-object v2, p0, Ljkw;->eqA:Ljls;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9248
    :cond_3
    iget v1, p0, Ljkw;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_4

    .line 9249
    const/4 v1, 0x5

    iget-object v2, p0, Ljkw;->eqC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9252
    :cond_4
    iget-object v1, p0, Ljkw;->eqB:Ljkt;

    if-eqz v1, :cond_5

    .line 9253
    const/4 v1, 0x6

    iget-object v2, p0, Ljkw;->eqB:Ljkt;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9256
    :cond_5
    iget v1, p0, Ljkw;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_6

    .line 9257
    const/4 v1, 0x7

    iget-object v2, p0, Ljkw;->ecQ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9260
    :cond_6
    iget-object v1, p0, Ljkw;->eqD:Lier;

    if-eqz v1, :cond_7

    .line 9261
    const/16 v1, 0x8

    iget-object v2, p0, Ljkw;->eqD:Lier;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9264
    :cond_7
    iget v1, p0, Ljkw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_8

    .line 9265
    const/16 v1, 0x9

    iget-object v2, p0, Ljkw;->eqy:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9268
    :cond_8
    iget v1, p0, Ljkw;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_9

    .line 9269
    const/16 v1, 0xa

    iget v2, p0, Ljkw;->eqG:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9272
    :cond_9
    iget-object v1, p0, Ljkw;->eqE:Liey;

    if-eqz v1, :cond_a

    .line 9273
    const/16 v1, 0xb

    iget-object v2, p0, Ljkw;->eqE:Liey;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9276
    :cond_a
    iget-object v1, p0, Ljkw;->eqF:Ljle;

    if-eqz v1, :cond_b

    .line 9277
    const/16 v1, 0xc

    iget-object v2, p0, Ljkw;->eqF:Ljle;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9280
    :cond_b
    return v0
.end method

.method public final qz(I)Ljkw;
    .locals 1

    .prologue
    .line 9151
    iput p1, p0, Ljkw;->eqG:I

    .line 9152
    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljkw;->aez:I

    .line 9153
    return-object p0
.end method

.method public final wt(Ljava/lang/String;)Ljkw;
    .locals 1

    .prologue
    .line 9042
    if-nez p1, :cond_0

    .line 9043
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9045
    :cond_0
    iput-object p1, p0, Ljkw;->akf:Ljava/lang/String;

    .line 9046
    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljkw;->aez:I

    .line 9047
    return-object p0
.end method

.method public final wu(Ljava/lang/String;)Ljkw;
    .locals 1

    .prologue
    .line 9098
    if-nez p1, :cond_0

    .line 9099
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9101
    :cond_0
    iput-object p1, p0, Ljkw;->eqC:Ljava/lang/String;

    .line 9102
    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljkw;->aez:I

    .line 9103
    return-object p0
.end method

.method public final wv(Ljava/lang/String;)Ljkw;
    .locals 1

    .prologue
    .line 9120
    if-nez p1, :cond_0

    .line 9121
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 9123
    :cond_0
    iput-object p1, p0, Ljkw;->ecQ:Ljava/lang/String;

    .line 9124
    iget v0, p0, Ljkw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljkw;->aez:I

    .line 9125
    return-object p0
.end method
