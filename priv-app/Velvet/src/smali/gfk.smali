.class public Lgfk;
.super Lgfm;
.source "PG"


# instance fields
.field private cIF:Landroid/media/audiofx/NoiseSuppressor;


# direct methods
.method public constructor <init>(IIZLgfn;Z)V
    .locals 0
    .param p4    # Lgfn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct/range {p0 .. p5}, Lgfm;-><init>(IIZLgfn;Z)V

    .line 28
    return-void
.end method


# virtual methods
.method protected final aFE()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    iget-boolean v0, p0, Lgfk;->cIK:Z

    if-eqz v0, :cond_0

    .line 51
    :try_start_0
    iget-object v0, p0, Lgfk;->cIM:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getAudioSessionId()I

    move-result v0

    invoke-static {v0}, Landroid/media/audiofx/NoiseSuppressor;->create(I)Landroid/media/audiofx/NoiseSuppressor;

    move-result-object v0

    iput-object v0, p0, Lgfk;->cIF:Landroid/media/audiofx/NoiseSuppressor;

    .line 52
    iget-object v0, p0, Lgfk;->cIF:Landroid/media/audiofx/NoiseSuppressor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/audiofx/NoiseSuppressor;->setEnabled(Z)I

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lgfk;->cIF:Landroid/media/audiofx/NoiseSuppressor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    iput-object v2, p0, Lgfk;->cIF:Landroid/media/audiofx/NoiseSuppressor;

    goto :goto_0
.end method

.method protected final aFF()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lgfk;->cIF:Landroid/media/audiofx/NoiseSuppressor;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lgfk;->cIF:Landroid/media/audiofx/NoiseSuppressor;

    invoke-virtual {v0}, Landroid/media/audiofx/NoiseSuppressor;->release()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lgfk;->cIF:Landroid/media/audiofx/NoiseSuppressor;

    .line 73
    :cond_0
    return-void
.end method

.method protected final startRecording()V
    .locals 2

    .prologue
    .line 33
    const/4 v0, 0x0

    .line 34
    iget-object v1, p0, Lgfk;->cII:Lgfn;

    invoke-virtual {v1}, Lgfn;->aFD()I

    move-result v1

    .line 35
    if-lez v1, :cond_0

    .line 36
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/media/MediaSyncEvent;->createEvent(I)Landroid/media/MediaSyncEvent;

    move-result-object v0

    .line 38
    invoke-virtual {v0, v1}, Landroid/media/MediaSyncEvent;->setAudioSessionId(I)Landroid/media/MediaSyncEvent;

    .line 40
    :cond_0
    if-eqz v0, :cond_1

    .line 41
    iget-object v1, p0, Lgfk;->cIM:Landroid/media/AudioRecord;

    invoke-virtual {v1, v0}, Landroid/media/AudioRecord;->startRecording(Landroid/media/MediaSyncEvent;)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_1
    iget-object v0, p0, Lgfk;->cIM:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    goto :goto_0
.end method
