.class public final Lcon;
.super Lcol;
.source "PG"


# instance fields
.field private final bdT:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcol;-><init>()V

    .line 37
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    iput-object v0, p0, Lcon;->bdT:Ljava/io/InputStream;

    .line 38
    return-void
.end method


# virtual methods
.method public final QY()Lcog;
    .locals 7
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const v6, 0x30012

    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 48
    :try_start_0
    iget-object v1, p0, Lcon;->bdT:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 49
    if-ne v1, v5, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 100
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget-object v2, p0, Lcon;->bdT:Ljava/io/InputStream;

    invoke-static {v1, v2}, Leqh;->a(ILjava/io/InputStream;)I
    :try_end_0
    .catch Leqk; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 66
    new-array v2, v1, [B

    .line 70
    :goto_1
    if-ge v0, v1, :cond_2

    .line 71
    :try_start_1
    iget-object v3, p0, Lcon;->bdT:Ljava/io/InputStream;

    array-length v4, v2

    sub-int/2addr v4, v0

    invoke-virtual {v3, v2, v0, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 72
    if-ne v3, v5, :cond_1

    .line 73
    const-string v2, "PelletParserProto"

    const-string v3, "Expected %d bytes but received %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    const/4 v0, 0x5

    invoke-static {v0, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 74
    new-instance v0, Lefs;

    const v1, 0x30012

    invoke-direct {v0, v1}, Lefs;-><init>(I)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 78
    :catch_0
    move-exception v0

    .line 80
    new-instance v1, Lefs;

    invoke-direct {v1, v0, v6}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 56
    :catch_1
    move-exception v0

    .line 58
    new-instance v1, Lefs;

    const v2, 0x30013

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 59
    :catch_2
    move-exception v0

    .line 61
    new-instance v1, Lefs;

    const v2, 0x30011

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1

    .line 76
    :cond_1
    add-int/2addr v0, v3

    .line 77
    goto :goto_1

    .line 85
    :cond_2
    new-instance v3, Lkem;

    invoke-direct {v3}, Lkem;-><init>()V

    .line 87
    :try_start_2
    invoke-static {v3, v2}, Ljsr;->c(Ljsr;[B)Ljsr;
    :try_end_2
    .catch Ljsq; {:try_start_2 .. :try_end_2} :catch_3

    .line 95
    const-string v2, ""

    .line 100
    new-instance v0, Lcog;

    invoke-direct {v0, v2, v3, v1}, Lcog;-><init>(Ljava/lang/String;Lkem;I)V

    goto :goto_0

    .line 88
    :catch_3
    move-exception v0

    .line 89
    new-instance v1, Lefs;

    const v2, 0x30014

    invoke-direct {v1, v0, v2}, Lefs;-><init>(Ljava/lang/Throwable;I)V

    throw v1
.end method
