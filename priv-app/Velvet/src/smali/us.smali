.class public final Lus;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# static fields
.field public static final Gn:Landroid/animation/TimeInterpolator;

.field private static Go:Landroid/graphics/ColorMatrix;

.field private static final Gp:Landroid/graphics/ColorMatrix;

.field private static final Gq:Landroid/util/SparseArray;


# instance fields
.field private Gr:I

.field private Gs:I

.field private Gt:Z

.field private Gu:Z

.field private Gv:Landroid/animation/ObjectAnimator;

.field private final ob:Landroid/graphics/Paint;

.field private final rI:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lut;

    invoke-direct {v0}, Lut;-><init>()V

    sput-object v0, Lus;->Gn:Landroid/animation/TimeInterpolator;

    .line 54
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    sput-object v0, Lus;->Gp:Landroid/graphics/ColorMatrix;

    .line 60
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lus;->Gq:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 65
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lus;->ob:Landroid/graphics/Paint;

    .line 69
    iput v2, p0, Lus;->Gs:I

    .line 70
    iput-boolean v2, p0, Lus;->Gt:Z

    .line 72
    iput-boolean v2, p0, Lus;->Gu:Z

    .line 76
    const/16 v0, 0xff

    iput v0, p0, Lus;->Gr:I

    .line 77
    iput-object p1, p0, Lus;->rI:Landroid/graphics/Bitmap;

    .line 78
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v2, v2, v0, v1}, Lus;->setBounds(IIII)V

    .line 79
    return-void
.end method

.method private gn()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v2, 0xff

    .line 183
    iget-boolean v0, p0, Lus;->Gt:Z

    if-eqz v0, :cond_2

    .line 184
    sget-object v0, Lus;->Go:Landroid/graphics/ColorMatrix;

    if-nez v0, :cond_0

    .line 185
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 186
    sput-object v0, Lus;->Go:Landroid/graphics/ColorMatrix;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 189
    sget-object v0, Lus;->Gp:Landroid/graphics/ColorMatrix;

    const/16 v1, 0x14

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->set([F)V

    .line 195
    sget-object v0, Lus;->Go:Landroid/graphics/ColorMatrix;

    sget-object v1, Lus;->Gp:Landroid/graphics/ColorMatrix;

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->preConcat(Landroid/graphics/ColorMatrix;)V

    .line 198
    :cond_0
    iget v0, p0, Lus;->Gs:I

    if-nez v0, :cond_1

    .line 199
    iget-object v0, p0, Lus;->ob:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    sget-object v2, Lus;->Go:Landroid/graphics/ColorMatrix;

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 216
    :goto_0
    return-void

    .line 201
    :cond_1
    sget-object v0, Lus;->Gp:Landroid/graphics/ColorMatrix;

    iget v1, p0, Lus;->Gs:I

    int-to-float v2, v1

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v2, v3

    sub-float v2, v4, v2

    invoke-virtual {v0, v2, v2, v2, v4}, Landroid/graphics/ColorMatrix;->setScale(FFFF)V

    invoke-virtual {v0}, Landroid/graphics/ColorMatrix;->getArray()[F

    move-result-object v0

    const/4 v2, 0x4

    int-to-float v3, v1

    aput v3, v0, v2

    const/16 v2, 0x9

    int-to-float v3, v1

    aput v3, v0, v2

    const/16 v2, 0xe

    int-to-float v1, v1

    aput v1, v0, v2

    .line 202
    sget-object v0, Lus;->Gp:Landroid/graphics/ColorMatrix;

    sget-object v1, Lus;->Go:Landroid/graphics/ColorMatrix;

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    .line 203
    iget-object v0, p0, Lus;->ob:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    sget-object v2, Lus;->Gp:Landroid/graphics/ColorMatrix;

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0

    .line 205
    :cond_2
    iget v0, p0, Lus;->Gs:I

    if-eqz v0, :cond_4

    .line 206
    sget-object v0, Lus;->Gq:Landroid/util/SparseArray;

    iget v1, p0, Lus;->Gs:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/ColorFilter;

    .line 207
    if-nez v0, :cond_3

    .line 208
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    iget v1, p0, Lus;->Gs:I

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 210
    sget-object v1, Lus;->Gq:Landroid/util/SparseArray;

    iget v2, p0, Lus;->Gs:I

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 212
    :cond_3
    iget-object v1, p0, Lus;->ob:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0

    .line 214
    :cond_4
    iget-object v0, p0, Lus;->ob:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0

    .line 189
    nop

    :array_0
    .array-data 4
        0x3efafafb
        0x0
        0x0
        0x0
        0x43020000    # 130.0f
        0x0
        0x3efafafb
        0x0
        0x0
        0x43020000    # 130.0f
        0x0
        0x0
        0x3efafafb
        0x0
        0x43020000    # 130.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public final M(Z)V
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lus;->Gt:Z

    if-eq v0, p1, :cond_0

    .line 144
    iput-boolean p1, p0, Lus;->Gt:Z

    .line 145
    invoke-direct {p0}, Lus;->gn()V

    .line 147
    :cond_0
    return-void
.end method

.method public final bc(I)V
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lus;->Gs:I

    if-eq v0, p1, :cond_0

    .line 176
    iput p1, p0, Lus;->Gs:I

    .line 177
    invoke-direct {p0}, Lus;->gn()V

    .line 178
    invoke-virtual {p0}, Lus;->invalidateSelf()V

    .line 180
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 83
    invoke-virtual {p0}, Lus;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lus;->rI:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iget-object v3, p0, Lus;->ob:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 86
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lus;->Gr:I

    return v0
.end method

.method public final getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lus;->rI:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lus;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lus;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    return v0
.end method

.method public final getMinimumHeight()I
    .locals 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lus;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public final getMinimumWidth()I
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Lus;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, -0x3

    return v0
.end method

.method public final gm()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lus;->Gs:I

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 100
    iput p1, p0, Lus;->Gr:I

    .line 101
    iget-object v0, p0, Lus;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 102
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public final setFilterBitmap(Z)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lus;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 107
    iget-object v0, p0, Lus;->ob:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 108
    return-void
.end method

.method public final setPressed(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 150
    iget-boolean v0, p0, Lus;->Gu:Z

    if-eq v0, p1, :cond_0

    .line 151
    iput-boolean p1, p0, Lus;->Gu:Z

    .line 152
    iget-boolean v0, p0, Lus;->Gu:Z

    if-eqz v0, :cond_1

    .line 153
    const-string v0, "brightness"

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/16 v2, 0x64

    aput v2, v1, v3

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lus;->Gv:Landroid/animation/ObjectAnimator;

    .line 156
    iget-object v0, p0, Lus;->Gv:Landroid/animation/ObjectAnimator;

    sget-object v1, Lus;->Gn:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 157
    iget-object v0, p0, Lus;->Gv:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 163
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lus;->invalidateSelf()V

    .line 164
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lus;->Gv:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lus;->Gv:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 160
    invoke-virtual {p0, v3}, Lus;->bc(I)V

    goto :goto_0
.end method
