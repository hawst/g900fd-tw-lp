.class public final Lhop;
.super Lhjo;
.source "PG"


# instance fields
.field final aoG:Lhla;

.field final bOj:Ldxd;

.field private final dmN:Ldwp;

.field public dmO:Z


# direct methods
.method public constructor <init>(Ledr;Ldxd;Lhla;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lhjo;-><init>(Ledr;)V

    .line 60
    new-instance v0, Lhoq;

    invoke-direct {v0, p0}, Lhoq;-><init>(Lhop;)V

    iput-object v0, p0, Lhop;->dmN:Ldwp;

    .line 77
    iput-object p2, p0, Lhop;->bOj:Ldxd;

    .line 78
    iput-object p3, p0, Lhop;->aoG:Lhla;

    .line 79
    return-void
.end method

.method static synthetic a(Lhop;)Lelb;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lhop;->aQa()Lelb;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/search/shared/actions/modular/ModularAction;Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 214
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 224
    :goto_0
    return v0

    .line 218
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajC()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 219
    goto :goto_0

    .line 224
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->agq()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v0

    invoke-static {v0}, Lhop;->b(Ljrl;)Z

    move-result v0

    if-eqz v0, :cond_2

    instance-of v0, p1, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/modular/arguments/AmbiguousArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/Disambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isInteractive()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private static b(Ljrl;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 231
    invoke-virtual {p0}, Ljrl;->btq()I

    move-result v1

    .line 232
    if-eq v1, v0, :cond_0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final K(IZ)V
    .locals 0

    .prologue
    .line 427
    invoke-super {p0, p1, p2}, Lhjo;->K(IZ)V

    .line 429
    return-void
.end method

.method public final L(IZ)V
    .locals 1

    .prologue
    .line 476
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Ledr;->anS()V

    .line 477
    if-eqz p2, :cond_0

    .line 478
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-interface {v0, p1}, Ledr;->hk(I)V

    .line 482
    :goto_0
    return-void

    .line 480
    :cond_0
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-interface {v0, p1}, Ledr;->hl(I)V

    goto :goto_0
.end method

.method public final VC()V
    .locals 1

    .prologue
    .line 495
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-interface {v0}, Ledr;->VC()V

    .line 496
    return-void
.end method

.method public final a(Lcom/google/android/search/shared/actions/utils/Disambiguation;)V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Lhjo;->onUserInteraction()V

    .line 129
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 130
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v1

    invoke-static {v1}, Lhop;->b(Ljrl;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/Disambiguation;->isInteractive()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->canExecute()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhop;->gu(Z)V

    .line 134
    invoke-virtual {p0}, Lhop;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lhor;

    invoke-interface {v0}, Lhor;->auj()V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    invoke-interface {v0, v1}, Ledr;->w(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 139
    invoke-virtual {p0}, Lhop;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lhop;->tP()V

    .line 141
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ledr;->eK(Z)V

    goto :goto_0
.end method

.method public final aLg()Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 2

    .prologue
    .line 411
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    invoke-interface {v0, v1}, Ledr;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method public final aQQ()V
    .locals 2

    .prologue
    .line 485
    invoke-virtual {p0}, Lhop;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    invoke-virtual {p0}, Lhop;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lhor;

    invoke-interface {v0}, Lhor;->tP()V

    .line 487
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ledr;->eK(Z)V

    .line 489
    :cond_0
    return-void
.end method

.method public final aQk()V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lhop;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->canExecute()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhop;->mCardController:Ledr;

    iget-object v1, p0, Lhop;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0, v1}, Ledr;->B(Lcom/google/android/search/shared/actions/VoiceAction;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lhop;->mCardController:Ledr;

    iget-object v1, p0, Lhop;->mVoiceAction:Lcom/google/android/search/shared/actions/VoiceAction;

    iget-object v2, p0, Lhop;->diL:Lesk;

    invoke-interface {v0, v1, v2}, Ledr;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lesk;)J

    move-result-wide v0

    .line 150
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 151
    invoke-virtual {p0}, Lhop;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lhor;

    invoke-interface {v0}, Lhor;->aRb()V

    .line 154
    :cond_0
    return-void
.end method

.method public final aRd()Z
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0}, Lhop;->aRe()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aRe()Lcom/google/android/search/shared/actions/modular/arguments/Argument;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p0}, Lhop;->aLg()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    .line 190
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 191
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alm()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->all()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    .line 193
    if-nez v1, :cond_2

    .line 194
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->all()I

    move-result v2

    invoke-virtual {v1, v2}, Ldvv;->gE(I)V

    .line 205
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajh()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 206
    invoke-static {v0, v1}, Lhop;->a(Lcom/google/android/search/shared/actions/modular/ModularAction;Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 210
    :goto_0
    return-object v1

    .line 196
    :cond_2
    invoke-static {v0, v1}, Lhop;->a(Lcom/google/android/search/shared/actions/modular/ModularAction;Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 210
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final aRf()Ljava/util/List;
    .locals 4

    .prologue
    .line 241
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajh()Ljava/util/List;

    move-result-object v1

    .line 243
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajl()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 253
    :goto_0
    return-object v0

    .line 246
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 247
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 248
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->d(Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 249
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method public final aRg()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 260
    invoke-virtual {p0}, Lhop;->aLg()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alc()Ljava/lang/String;

    move-result-object v0

    .line 261
    if-nez v0, :cond_0

    .line 262
    const-string v0, ""

    .line 264
    :cond_0
    return-object v0
.end method

.method public final aRh()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 287
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v0

    iget-object v1, v0, Ljrl;->eAQ:Ljqv;

    .line 288
    if-eqz v1, :cond_1

    .line 290
    :try_start_0
    iget-object v2, p0, Lhop;->bOj:Ldxd;

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Ldxb;

    invoke-virtual {v2, v1, v0}, Ldxd;->a(Ljqv;Ldxb;)Ldws;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Ldws;->Pd()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 293
    invoke-virtual {v0}, Ldws;->getString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 310
    :cond_0
    :goto_0
    return-object v0

    .line 299
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Ldvv;->a(ILjqv;)V

    .line 306
    :cond_1
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v0

    invoke-virtual {v0}, Ljrl;->btr()Ljava/lang/String;

    move-result-object v0

    .line 307
    if-nez v0, :cond_0

    .line 308
    const-string v0, ""

    goto :goto_0
.end method

.method public final aRi()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 317
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v0

    iget-object v1, v0, Ljrl;->eAS:Ljqv;

    .line 318
    if-eqz v1, :cond_1

    .line 320
    :try_start_0
    iget-object v2, p0, Lhop;->bOj:Ldxd;

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Ldxb;

    invoke-virtual {v2, v1, v0}, Ldxd;->a(Ljqv;Ldxb;)Ldws;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Ldws;->Pd()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 323
    invoke-virtual {v0}, Ldws;->getString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 339
    :cond_0
    :goto_0
    return-object v0

    .line 329
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Ldvv;->a(ILjqv;)V

    .line 335
    :cond_1
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v0

    invoke-virtual {v0}, Ljrl;->bts()Ljava/lang/String;

    move-result-object v0

    .line 336
    if-nez v0, :cond_0

    .line 337
    const-string v0, ""

    goto :goto_0
.end method

.method public final aRj()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 357
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    .line 360
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->avj()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->avh()I

    move-result v2

    if-ne v2, v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ajr()Ljqs;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aRk()I
    .locals 3

    .prologue
    .line 377
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    invoke-virtual {p0}, Lhop;->aLg()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v1

    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v2

    invoke-interface {v2}, Ledr;->VG()Z

    move-result v2

    invoke-static {v0, v1, v2}, Leds;->a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;Z)I

    move-result v0

    return v0
.end method

.method public final aRl()Z
    .locals 1

    .prologue
    .line 407
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->agp()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->agn()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aRm()V
    .locals 3

    .prologue
    .line 447
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajn()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v1

    .line 448
    if-nez v1, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v0

    const-string v2, "PersonArgument is not set"

    invoke-static {v0, v2}, Lifv;->c(ZLjava/lang/Object;)V

    .line 452
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-interface {v2, v0}, Ledr;->m(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V

    .line 453
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v2

    invoke-interface {v0, v2}, Ledr;->w(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 454
    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->PM()V

    .line 455
    invoke-virtual {p0}, Lhop;->tP()V

    .line 456
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-interface {v0}, Ledr;->GR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ledr;->eK(Z)V

    goto :goto_0
.end method

.method public final aRn()V
    .locals 4

    .prologue
    .line 466
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajn()Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;

    move-result-object v0

    .line 467
    if-nez v0, :cond_0

    .line 473
    :goto_0
    return-void

    .line 470
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->Pd()Z

    move-result v1

    const-string v2, "PersonArgument is not set"

    invoke-static {v1, v2}, Lifv;->c(ZLjava/lang/Object;)V

    .line 471
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v1

    invoke-interface {v1}, Ledr;->wr()Leoj;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/PersonArgument;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/PersonDisambiguation;

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    invoke-static {v0}, Lico;->l(Lcom/google/android/search/shared/contact/Person;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Leoj;->b([Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public final ahg()V
    .locals 1

    .prologue
    .line 438
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Ledr;->anS()V

    .line 439
    return-void
.end method

.method public final anO()Z
    .locals 2

    .prologue
    .line 158
    invoke-super {p0}, Lhjo;->anO()Z

    move-result v1

    .line 159
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lhop;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p0}, Lhop;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lhor;

    invoke-interface {v0}, Lhor;->tP()V

    .line 162
    :cond_0
    return v1
.end method

.method public final anP()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lhop;->mCardController:Ledr;

    invoke-interface {v0}, Ledr;->anP()Z

    move-result v0

    return v0
.end method

.method public final anR()Z
    .locals 1

    .prologue
    .line 418
    invoke-virtual {p0}, Lhop;->aPZ()Ledr;

    move-result-object v0

    invoke-interface {v0}, Ledr;->anR()Z

    move-result v0

    return v0
.end method

.method public final ave()Lcom/google/android/shared/util/App;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 368
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajm()Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularActionMatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v0

    return-object v0
.end method

.method public final gu(Z)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 98
    invoke-virtual {p0}, Lhop;->aLf()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 99
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->aji()Ljrl;

    move-result-object v1

    iget-object v3, v1, Ljrl;->ezI:[I

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_2

    aget v5, v3, v1

    .line 100
    invoke-virtual {v0, v5}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v5

    iget-object v6, p0, Lhop;->dmN:Ldwp;

    invoke-virtual {v5, v6}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Ldwp;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 114
    :cond_0
    :goto_1
    return-void

    .line 99
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    :cond_2
    invoke-virtual {p0}, Lhop;->aRj()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lhop;->ave()Lcom/google/android/shared/util/App;

    move-result-object v0

    if-nez v0, :cond_3

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhop;->dmO:Z

    .line 107
    invoke-virtual {p0}, Lhop;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lhop;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lhor;

    invoke-interface {v0}, Lhor;->tP()V

    goto :goto_1

    .line 111
    :cond_3
    iput-boolean v2, p0, Lhop;->dmO:Z

    .line 112
    invoke-super {p0, p1}, Lhjo;->gu(Z)V

    goto :goto_1
.end method

.method public final m(Lcom/google/android/shared/util/App;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 118
    invoke-super {p0, p1}, Lhjo;->m(Lcom/google/android/shared/util/App;)Z

    move-result v0

    .line 119
    iget-boolean v1, p0, Lhop;->dmO:Z

    if-eqz v1, :cond_0

    .line 120
    iput-boolean v2, p0, Lhop;->dmO:Z

    .line 121
    invoke-virtual {p0, v2}, Lhop;->gu(Z)V

    .line 122
    const/4 v0, 0x1

    .line 124
    :cond_0
    return v0
.end method

.method public final onUserInteraction()V
    .locals 0

    .prologue
    .line 433
    invoke-super {p0}, Lhjo;->onUserInteraction()V

    .line 435
    return-void
.end method

.method public final tP()V
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lhop;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lhop;->aQa()Lelb;

    move-result-object v0

    check-cast v0, Lhor;

    invoke-interface {v0}, Lhor;->tP()V

    .line 94
    :cond_0
    return-void
.end method

.method protected final ui()V
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lhop;->aAU()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p0}, Lhop;->tP()V

    goto :goto_0
.end method
