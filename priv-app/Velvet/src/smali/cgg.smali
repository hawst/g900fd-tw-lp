.class public final enum Lcgg;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum aVA:Lcgg;

.field private static enum aVB:Lcgg;

.field public static final enum aVC:Lcgg;

.field public static final enum aVD:Lcgg;

.field public static final enum aVE:Lcgg;

.field public static final enum aVF:Lcgg;

.field public static final enum aVG:Lcgg;

.field public static final enum aVH:Lcgg;

.field public static final enum aVI:Lcgg;

.field public static final enum aVJ:Lcgg;

.field public static final enum aVK:Lcgg;

.field private static enum aVL:Lcgg;

.field private static enum aVM:Lcgg;

.field private static enum aVN:Lcgg;

.field private static final synthetic aVP:[Lcgg;

.field public static final enum aVh:Lcgg;

.field public static final enum aVi:Lcgg;

.field public static final enum aVj:Lcgg;

.field public static final enum aVk:Lcgg;

.field public static final enum aVl:Lcgg;

.field public static final enum aVm:Lcgg;

.field public static final enum aVn:Lcgg;

.field public static final enum aVo:Lcgg;

.field private static enum aVp:Lcgg;

.field private static enum aVq:Lcgg;

.field public static final enum aVr:Lcgg;

.field public static final enum aVs:Lcgg;

.field public static final enum aVt:Lcgg;

.field public static final enum aVu:Lcgg;

.field public static final enum aVv:Lcgg;

.field public static final enum aVw:Lcgg;

.field public static final enum aVx:Lcgg;

.field public static final enum aVy:Lcgg;

.field private static enum aVz:Lcgg;


# instance fields
.field private final aVO:Z

.field private hO:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lcgg;

    const-string v1, "ALLOW_PRERECORDED_AUDIO"

    invoke-direct {v0, v1, v3, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVh:Lcgg;

    .line 34
    new-instance v0, Lcgg;

    const-string v1, "ALLOW_SHORTCUT_CREATION_ALWAYS"

    invoke-direct {v0, v1, v4, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVi:Lcgg;

    .line 40
    new-instance v0, Lcgg;

    const-string v1, "ALLOW_SHORTCUT_CREATION_FROM_PROMOTED_SUMMONS"

    invoke-direct {v0, v1, v5, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVj:Lcgg;

    .line 45
    new-instance v0, Lcgg;

    const-string v1, "CALL_NO_COUNTDOWN"

    invoke-direct {v0, v1, v6, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVk:Lcgg;

    .line 50
    new-instance v0, Lcgg;

    const-string v1, "CONFIGURABLE_RELATIONSHIP"

    invoke-direct {v0, v1, v7, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVl:Lcgg;

    .line 55
    new-instance v0, Lcgg;

    const-string v1, "CONNECTION_ERROR_CARDS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVm:Lcgg;

    .line 60
    new-instance v0, Lcgg;

    const-string v1, "DEMAND_SPACE_IN_VELVET"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVn:Lcgg;

    .line 65
    new-instance v0, Lcgg;

    const-string v1, "DETECT_BT_DEVICE_AS_CAR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVo:Lcgg;

    .line 71
    new-instance v0, Lcgg;

    const-string v1, "DISMISS_KEYGUARD_EYES_FREE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVp:Lcgg;

    .line 76
    new-instance v0, Lcgg;

    const-string v1, "DOCS_VOICE_IME_UI"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVq:Lcgg;

    .line 81
    new-instance v0, Lcgg;

    const-string v1, "E300"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVr:Lcgg;

    .line 86
    new-instance v0, Lcgg;

    const-string v1, "E300_VEL_ON_LOCKSCREEN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVs:Lcgg;

    .line 91
    new-instance v0, Lcgg;

    const-string v1, "EYES_FREE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVt:Lcgg;

    .line 97
    new-instance v0, Lcgg;

    const-string v1, "EYES_FREE_FOLLOW_ON_END_POINT_BEEP"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVu:Lcgg;

    .line 102
    new-instance v0, Lcgg;

    const-string v1, "EYES_FREE_HEADSETS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVv:Lcgg;

    .line 109
    new-instance v0, Lcgg;

    const-string v1, "EYES_FREE_NOTIFICATIONS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVw:Lcgg;

    .line 116
    new-instance v0, Lcgg;

    const-string v1, "EYES_FREE_NOTIFICATIONS_FOR_HEADSETS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVx:Lcgg;

    .line 121
    new-instance v0, Lcgg;

    const-string v1, "FORCE_EYES_FREE_TTS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVy:Lcgg;

    .line 126
    new-instance v0, Lcgg;

    const-string v1, "LOG_CONTACT_DATA"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVz:Lcgg;

    .line 131
    new-instance v0, Lcgg;

    const-string v1, "MESSAGE_SEARCH_ACTION"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVA:Lcgg;

    .line 141
    new-instance v0, Lcgg;

    const-string v1, "NOW_USES_NEW_OPT_IN_LANGUAGE_WHEN_TALKING_TO_KENAFA"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVB:Lcgg;

    .line 147
    new-instance v0, Lcgg;

    const-string v1, "OMNIBOX"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVC:Lcgg;

    .line 152
    new-instance v0, Lcgg;

    const-string v1, "SCO_VOLUME_FIX"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVD:Lcgg;

    .line 157
    new-instance v0, Lcgg;

    const-string v1, "SEARCH_HISTORY_IN_APP"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVE:Lcgg;

    .line 162
    new-instance v0, Lcgg;

    const-string v1, "SAVE_NICKNAME_TO_CONTACTS_PROVIDER"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVF:Lcgg;

    .line 167
    new-instance v0, Lcgg;

    const-string v1, "SHOW_LOGGING_TOASTS"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVG:Lcgg;

    .line 172
    new-instance v0, Lcgg;

    const-string v1, "TAG_N_BEST_HYPOTHESES"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVH:Lcgg;

    .line 175
    new-instance v0, Lcgg;

    const-string v1, "TEST_FEATURE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVI:Lcgg;

    .line 181
    new-instance v0, Lcgg;

    const-string v1, "UNIVERSAL_SUGGESTIONS"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVJ:Lcgg;

    .line 187
    new-instance v0, Lcgg;

    const-string v1, "UPDATE_PROMO"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2, v4}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVK:Lcgg;

    .line 193
    new-instance v0, Lcgg;

    const-string v1, "USE_GMM_DEV_PACKAGE"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVL:Lcgg;

    .line 199
    new-instance v0, Lcgg;

    const-string v1, "USE_GMM_DOGFOOD_PACKAGE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVM:Lcgg;

    .line 205
    new-instance v0, Lcgg;

    const-string v1, "USE_GMM_FISHFOOD_PACKAGE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcgg;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcgg;->aVN:Lcgg;

    .line 20
    const/16 v0, 0x21

    new-array v0, v0, [Lcgg;

    sget-object v1, Lcgg;->aVh:Lcgg;

    aput-object v1, v0, v3

    sget-object v1, Lcgg;->aVi:Lcgg;

    aput-object v1, v0, v4

    sget-object v1, Lcgg;->aVj:Lcgg;

    aput-object v1, v0, v5

    sget-object v1, Lcgg;->aVk:Lcgg;

    aput-object v1, v0, v6

    sget-object v1, Lcgg;->aVl:Lcgg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcgg;->aVm:Lcgg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcgg;->aVn:Lcgg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcgg;->aVo:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcgg;->aVp:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcgg;->aVq:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcgg;->aVr:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcgg;->aVs:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcgg;->aVt:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcgg;->aVu:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcgg;->aVv:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcgg;->aVw:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcgg;->aVx:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcgg;->aVy:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcgg;->aVz:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcgg;->aVA:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcgg;->aVB:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcgg;->aVC:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcgg;->aVD:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcgg;->aVE:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcgg;->aVF:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcgg;->aVG:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcgg;->aVH:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcgg;->aVI:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcgg;->aVJ:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcgg;->aVK:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcgg;->aVL:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcgg;->aVM:Lcgg;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcgg;->aVN:Lcgg;

    aput-object v2, v0, v1

    sput-object v0, Lcgg;->aVP:[Lcgg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 222
    iput-boolean p3, p0, Lcgg;->aVO:Z

    .line 223
    iput-boolean p3, p0, Lcgg;->hO:Z

    .line 224
    return-void
.end method

.method public static EM()V
    .locals 5

    .prologue
    .line 270
    invoke-static {}, Lcgg;->values()[Lcgg;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 271
    iget-boolean v4, v3, Lcgg;->aVO:Z

    iput-boolean v4, v3, Lcgg;->hO:Z

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 273
    :cond_0
    return-void
.end method

.method public static varargs a(Z[Lcgg;)V
    .locals 3

    .prologue
    .line 256
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 257
    iput-boolean p0, v2, Lcgg;->hO:Z

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 259
    :cond_0
    return-void
.end method

.method public static b(Landroid/content/SharedPreferences;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 238
    invoke-static {v1}, Lifv;->gY(Z)V

    .line 239
    const-string v0, "enabled_features"

    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v2

    invoke-interface {p0, v0, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v3

    .line 241
    const-string v0, "disabled_features"

    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v2

    invoke-interface {p0, v0, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    .line 243
    invoke-static {}, Lcgg;->values()[Lcgg;

    move-result-object v5

    array-length v6, v5

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_2

    aget-object v7, v5, v2

    .line 244
    invoke-virtual {v7}, Lcgg;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, v7, Lcgg;->aVO:Z

    if-nez v0, :cond_0

    invoke-virtual {v7}, Lcgg;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v7, Lcgg;->hO:Z

    .line 243
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 244
    goto :goto_1

    .line 247
    :cond_2
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcgg;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcgg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcgg;

    return-object v0
.end method

.method public static values()[Lcgg;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcgg;->aVP:[Lcgg;

    invoke-virtual {v0}, [Lcgg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcgg;

    return-object v0
.end method


# virtual methods
.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcgg;->hO:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Feature["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcgg;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcgg;->hO:Z

    if-eqz v0, :cond_0

    const-string v0, "ENABLED"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcgg;->hO:Z

    iget-boolean v2, p0, Lcgg;->aVO:Z

    if-ne v0, v2, :cond_1

    const-string v0, " (DEFAULT)"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "DISABLED"

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method
