.class public final Ljqt;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ezo:I

.field public ezs:Ljrg;

.field public ezt:[Ljqs;

.field public ezu:Ljqv;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5124
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5125
    iput v2, p0, Ljqt;->aez:I

    iput-object v1, p0, Ljqt;->ezs:Ljrg;

    invoke-static {}, Ljqs;->bsx()[Ljqs;

    move-result-object v0

    iput-object v0, p0, Ljqt;->ezt:[Ljqs;

    iput-object v1, p0, Ljqt;->ezu:Ljqv;

    iput v2, p0, Ljqt;->ezo:I

    iput-object v1, p0, Ljqt;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqt;->eCz:I

    .line 5126
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5077
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqt;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqt;->ezt:[Ljqs;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljqs;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljqt;->ezt:[Ljqs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljqs;

    invoke-direct {v3}, Ljqs;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljqt;->ezt:[Ljqs;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljqs;

    invoke-direct {v3}, Ljqs;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljqt;->ezt:[Ljqs;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljqt;->ezs:Ljrg;

    if-nez v0, :cond_4

    new-instance v0, Ljrg;

    invoke-direct {v0}, Ljrg;-><init>()V

    iput-object v0, p0, Ljqt;->ezs:Ljrg;

    :cond_4
    iget-object v0, p0, Ljqt;->ezs:Ljrg;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljqt;->ezu:Ljqv;

    if-nez v0, :cond_5

    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    iput-object v0, p0, Ljqt;->ezu:Ljqv;

    :cond_5
    iget-object v0, p0, Ljqt;->ezu:Ljqv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljqt;->ezo:I

    iget v0, p0, Ljqt;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqt;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 5142
    iget-object v0, p0, Ljqt;->ezt:[Ljqs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljqt;->ezt:[Ljqs;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 5143
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljqt;->ezt:[Ljqs;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 5144
    iget-object v1, p0, Ljqt;->ezt:[Ljqs;

    aget-object v1, v1, v0

    .line 5145
    if-eqz v1, :cond_0

    .line 5146
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 5143
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5150
    :cond_1
    iget-object v0, p0, Ljqt;->ezs:Ljrg;

    if-eqz v0, :cond_2

    .line 5151
    const/4 v0, 0x3

    iget-object v1, p0, Ljqt;->ezs:Ljrg;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5153
    :cond_2
    iget-object v0, p0, Ljqt;->ezu:Ljqv;

    if-eqz v0, :cond_3

    .line 5154
    const/4 v0, 0x4

    iget-object v1, p0, Ljqt;->ezu:Ljqv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 5156
    :cond_3
    iget v0, p0, Ljqt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 5157
    const/4 v0, 0x5

    iget v1, p0, Ljqt;->ezo:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 5159
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5160
    return-void
.end method

.method public final bsC()Z
    .locals 1

    .prologue
    .line 5116
    iget v0, p0, Ljqt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsy()I
    .locals 1

    .prologue
    .line 5108
    iget v0, p0, Ljqt;->ezo:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 5164
    invoke-super {p0}, Ljsl;->lF()I

    move-result v1

    .line 5165
    iget-object v0, p0, Ljqt;->ezt:[Ljqs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljqt;->ezt:[Ljqs;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 5166
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljqt;->ezt:[Ljqs;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 5167
    iget-object v2, p0, Ljqt;->ezt:[Ljqs;

    aget-object v2, v2, v0

    .line 5168
    if-eqz v2, :cond_0

    .line 5169
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 5166
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5174
    :cond_1
    iget-object v0, p0, Ljqt;->ezs:Ljrg;

    if-eqz v0, :cond_2

    .line 5175
    const/4 v0, 0x3

    iget-object v2, p0, Ljqt;->ezs:Ljrg;

    invoke-static {v0, v2}, Ljsj;->c(ILjsr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 5178
    :cond_2
    iget-object v0, p0, Ljqt;->ezu:Ljqv;

    if-eqz v0, :cond_3

    .line 5179
    const/4 v0, 0x4

    iget-object v2, p0, Ljqt;->ezu:Ljqv;

    invoke-static {v0, v2}, Ljsj;->c(ILjsr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 5182
    :cond_3
    iget v0, p0, Ljqt;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 5183
    const/4 v0, 0x5

    iget v2, p0, Ljqt;->ezo:I

    invoke-static {v0, v2}, Ljsj;->bv(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 5186
    :cond_4
    return v1
.end method
