.class public final Lcjl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aYE:Lcis;

.field final aYF:Lcfm;

.field final mBgExecutor:Ljava/util/concurrent/Executor;

.field final mLoginHelper:Lcrh;

.field final mRelationshipNameLookup:Leai;

.field final mUiExecutor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Leai;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcrh;Lcis;Lcfm;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcjl;->mRelationshipNameLookup:Leai;

    .line 55
    iput-object p2, p0, Lcjl;->mUiExecutor:Ljava/util/concurrent/Executor;

    .line 56
    iput-object p3, p0, Lcjl;->mBgExecutor:Ljava/util/concurrent/Executor;

    .line 57
    iput-object p4, p0, Lcjl;->mLoginHelper:Lcrh;

    .line 58
    iput-object p5, p0, Lcjl;->aYE:Lcis;

    .line 59
    iput-object p6, p0, Lcjl;->aYF:Lcfm;

    .line 60
    return-void
.end method

.method private static d(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 92
    :try_start_0
    const-string v0, "nicknames"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 93
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 94
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 95
    const-string v3, "metadata"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 96
    const-string v4, "container"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "container"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "contact"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 98
    const-string v0, "value"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 104
    :goto_1
    return-object v0

    .line 93
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    const-string v1, "RelationshipManagerForPeopleClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t parse json nickname field: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method


# virtual methods
.method G(Ljava/lang/String;Ljava/lang/String;)Lcjm;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 114
    .line 116
    :try_start_0
    iget-object v0, p0, Lcjl;->aYE:Lcis;

    invoke-interface {v0, p1, p2}, Lcis;->E(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 118
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 119
    const-string v0, "items"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 120
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 121
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 122
    const-string v4, "metadata"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "deleted"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "metadata"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "deleted"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 126
    :cond_0
    const-string v0, "id"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 130
    const-string v0, "RelationshipManagerForPeopleClient"

    const-string v2, "getNicknamesField(): Person ID is empty."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_1
    :goto_1
    return-object v1

    .line 133
    :cond_2
    invoke-static {v3}, Lcjl;->d(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v3

    .line 134
    new-instance v0, Lcjm;

    invoke-direct {v0, p0, v3, v2}, Lcjm;-><init>(Lcjl;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move-object v1, v0

    .line 142
    goto :goto_1

    .line 120
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_4
    const-string v0, "RelationshipManagerForPeopleClient"

    const-string v2, "Lookup response is null or empty."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_2

    .line 140
    :catch_0
    move-exception v0

    .line 141
    const-string v2, "RelationshipManagerForPeopleClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Lookup broken json parse: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method a(Ljava/lang/String;Lcjm;Lcjn;)V
    .locals 3

    .prologue
    .line 157
    invoke-virtual {p2}, Lcjm;->KY()Ljava/lang/String;

    .line 158
    iget-object v0, p3, Lcjn;->aYK:Lcjo;

    sget-object v1, Lcjo;->aYN:Lcjo;

    if-ne v0, v1, :cond_2

    .line 159
    iget-object v0, p3, Lcjn;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    invoke-virtual {p2, v0}, Lcjm;->a(Lcom/google/android/search/shared/contact/Relationship;)Z

    .line 167
    :goto_0
    iget-object v0, p2, Lcjm;->aYI:Ljava/lang/String;

    .line 168
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "{\'nicknames\': [{\'value\': \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcjm;->KY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\',\'metadata\': {\'container\': \'contact\'}}]}"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 180
    iget-object v2, p0, Lcjl;->aYE:Lcis;

    invoke-interface {v2, p1, v0, v1}, Lcis;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    :cond_0
    const-string v0, "RelationshipManagerForPeopleClient"

    const-string v1, "Update (merge) response is null or empty."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_1
    return-void

    .line 160
    :cond_2
    iget-object v0, p3, Lcjn;->aYK:Lcjo;

    sget-object v1, Lcjo;->aYO:Lcjo;

    if-ne v0, v1, :cond_3

    .line 161
    iget-object v0, p3, Lcjn;->aYM:Lcom/google/android/search/shared/contact/Relationship;

    iget-object v1, p2, Lcjm;->aYG:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_3
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SaveType not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p3, Lcjn;->aYK:Lcjo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V
    .locals 5

    .prologue
    .line 69
    new-instance v0, Lcjp;

    invoke-direct {v0, p0}, Lcjp;-><init>(Lcjl;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcjn;

    const/4 v2, 0x0

    new-instance v3, Lcjn;

    sget-object v4, Lcjo;->aYN:Lcjo;

    invoke-direct {v3, v4, p1, p2}, Lcjn;-><init>(Lcjo;Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcjp;->a([Ljava/lang/Object;)Lenp;

    .line 71
    return-void
.end method

.method public final d(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V
    .locals 5

    .prologue
    .line 80
    new-instance v0, Lcjp;

    invoke-direct {v0, p0}, Lcjp;-><init>(Lcjl;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcjn;

    const/4 v2, 0x0

    new-instance v3, Lcjn;

    sget-object v4, Lcjo;->aYO:Lcjo;

    invoke-direct {v3, v4, p1, p2}, Lcjn;-><init>(Lcjo;Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcjp;->a([Ljava/lang/Object;)Lenp;

    .line 82
    return-void
.end method
