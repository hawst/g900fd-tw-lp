.class public final Lcdb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdp;


# instance fields
.field private final aSi:Ligi;

.field private aSj:I

.field private aSk:J

.field private aSl:I

.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Ligi;Lemp;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcdb;->aSi:Ligi;

    .line 39
    iput-object p2, p0, Lcdb;->mClock:Lemp;

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcdb;->aSj:I

    .line 41
    iget-object v0, p0, Lcdb;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcdb;->aSk:J

    .line 42
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljww;)Leie;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 47
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljww;->getStatus()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 48
    new-instance v0, Leii;

    invoke-virtual {p1}, Ljww;->getErrorCode()I

    move-result v1

    invoke-direct {v0, v1}, Leii;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :goto_0
    monitor-exit p0

    return-object v0

    .line 57
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcdb;->aSj:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 58
    const/4 v0, 0x0

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Leie;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 65
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcdb;->aSj:I

    if-nez v0, :cond_0

    .line 67
    const/16 v0, 0x1b

    invoke-static {v0}, Lege;->ht(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 105
    :goto_0
    monitor-exit p0

    return v0

    .line 78
    :cond_0
    :try_start_1
    iget v0, p0, Lcdb;->aSj:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 79
    iget-object v0, p0, Lcdb;->aSi:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzs;

    invoke-virtual {v0}, Ljzs;->bxr()I

    move-result v0

    iput v0, p0, Lcdb;->aSj:I

    .line 80
    iget-object v0, p0, Lcdb;->aSi:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzs;

    invoke-virtual {v0}, Ljzs;->bxs()I

    move-result v0

    iput v0, p0, Lcdb;->aSl:I

    .line 83
    :cond_1
    iget-wide v2, p0, Lcdb;->aSk:J

    iget v0, p0, Lcdb;->aSl:I

    int-to-long v4, v0

    add-long/2addr v2, v4

    iget-object v0, p0, Lcdb;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    .line 85
    const/16 v0, 0x1c

    invoke-static {v0}, Lege;->ht(I)V

    move v0, v1

    .line 86
    goto :goto_0

    .line 90
    :cond_2
    instance-of v0, p1, Leig;

    if-nez v0, :cond_3

    instance-of v0, p1, Leii;

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p1}, Leie;->aja()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 92
    :cond_4
    iget v0, p0, Lcdb;->aSj:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcdb;->aSj:I

    .line 94
    invoke-virtual {p1}, Leie;->aja()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 95
    const/16 v0, 0x1a

    invoke-static {v0}, Lege;->ht(I)V

    .line 101
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 97
    :cond_5
    const/16 v0, 0x19

    invoke-static {v0}, Lege;->ht(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_6
    move v0, v1

    .line 105
    goto :goto_0
.end method
