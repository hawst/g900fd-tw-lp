.class public final Lclf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final baN:Ljava/lang/Object;

.field private final baO:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lclf;->baN:Ljava/lang/Object;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lclf;->baO:Ljava/util/List;

    .line 21
    return-void
.end method


# virtual methods
.method public final Px()Lcle;
    .locals 2

    .prologue
    .line 31
    iget-object v1, p0, Lclf;->baO:Ljava/util/List;

    monitor-enter v1

    .line 32
    :try_start_0
    iget-object v0, p0, Lclf;->baO:Ljava/util/List;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 33
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 35
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    .line 37
    new-instance v1, Lclg;

    invoke-direct {v1, p0, v0}, Lclg;-><init>(Lclf;Ljava/util/List;)V

    return-object v1

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcld;)V
    .locals 2

    .prologue
    .line 24
    iget-object v1, p0, Lclf;->baO:Ljava/util/List;

    monitor-enter v1

    .line 25
    :try_start_0
    iget-object v0, p0, Lclf;->baO:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
