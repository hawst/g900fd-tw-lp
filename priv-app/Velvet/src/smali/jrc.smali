.class public final Ljrc;
.super Ljsl;
.source "PG"


# static fields
.field public static final ezW:Ljsm;


# instance fields
.field private aez:I

.field private eAa:Ljsa;

.field public eAb:Ljqb;

.field public eAc:Ljpu;

.field public eAd:[Ljrf;

.field private eyF:I

.field public ezX:[Ljqg;

.field public ezY:[Ljrl;

.field private ezZ:[Ljrv;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/16 v0, 0xb

    const-class v1, Ljrc;

    const v2, 0x1af2cdfa

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljrc;->ezW:Ljsm;

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 82
    iput v2, p0, Ljrc;->aez:I

    invoke-static {}, Ljqg;->bsr()[Ljqg;

    move-result-object v0

    iput-object v0, p0, Ljrc;->ezX:[Ljqg;

    invoke-static {}, Ljrl;->bto()[Ljrl;

    move-result-object v0

    iput-object v0, p0, Ljrc;->ezY:[Ljrl;

    invoke-static {}, Ljrv;->btG()[Ljrv;

    move-result-object v0

    iput-object v0, p0, Ljrc;->ezZ:[Ljrv;

    iput-object v1, p0, Ljrc;->eAa:Ljsa;

    iput-object v1, p0, Ljrc;->eAb:Ljqb;

    iput v2, p0, Ljrc;->eyF:I

    iput-object v1, p0, Ljrc;->eAc:Ljpu;

    invoke-static {}, Ljrf;->btf()[Ljrf;

    move-result-object v0

    iput-object v0, p0, Ljrc;->eAd:[Ljrf;

    iput-object v1, p0, Ljrc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrc;->eCz:I

    .line 83
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 21
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljrc;->ezX:[Ljqg;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljqg;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljrc;->ezX:[Ljqg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljqg;

    invoke-direct {v3}, Ljqg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljrc;->ezX:[Ljqg;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljqg;

    invoke-direct {v3}, Ljqg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljrc;->ezX:[Ljqg;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljrc;->ezY:[Ljrl;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljrl;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljrc;->ezY:[Ljrl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljrl;

    invoke-direct {v3}, Ljrl;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljrc;->ezY:[Ljrl;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljrl;

    invoke-direct {v3}, Ljrl;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljrc;->ezY:[Ljrl;

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Ljrc;->eAb:Ljqb;

    if-nez v0, :cond_7

    new-instance v0, Ljqb;

    invoke-direct {v0}, Ljqb;-><init>()V

    iput-object v0, p0, Ljrc;->eAb:Ljqb;

    :cond_7
    iget-object v0, p0, Ljrc;->eAb:Ljqb;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljrc;->eyF:I

    iget v0, p0, Ljrc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrc;->aez:I

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ljrc;->eAc:Ljpu;

    if-nez v0, :cond_8

    new-instance v0, Ljpu;

    invoke-direct {v0}, Ljpu;-><init>()V

    iput-object v0, p0, Ljrc;->eAc:Ljpu;

    :cond_8
    iget-object v0, p0, Ljrc;->eAc:Ljpu;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljrc;->eAd:[Ljrf;

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljrf;

    if-eqz v0, :cond_9

    iget-object v3, p0, Ljrc;->eAd:[Ljrf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_b

    new-instance v3, Ljrf;

    invoke-direct {v3}, Ljrf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    iget-object v0, p0, Ljrc;->eAd:[Ljrf;

    array-length v0, v0

    goto :goto_5

    :cond_b
    new-instance v3, Ljrf;

    invoke-direct {v3}, Ljrf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljrc;->eAd:[Ljrf;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljrc;->ezZ:[Ljrv;

    if-nez v0, :cond_d

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ljrv;

    if-eqz v0, :cond_c

    iget-object v3, p0, Ljrc;->ezZ:[Ljrv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_e

    new-instance v3, Ljrv;

    invoke-direct {v3}, Ljrv;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Ljrc;->ezZ:[Ljrv;

    array-length v0, v0

    goto :goto_7

    :cond_e
    new-instance v3, Ljrv;

    invoke-direct {v3}, Ljrv;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljrc;->ezZ:[Ljrv;

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Ljrc;->eAa:Ljsa;

    if-nez v0, :cond_f

    new-instance v0, Ljsa;

    invoke-direct {v0}, Ljsa;-><init>()V

    iput-object v0, p0, Ljrc;->eAa:Ljsa;

    :cond_f
    iget-object v0, p0, Ljrc;->eAa:Ljsa;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Ljrc;->ezX:[Ljqg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljrc;->ezX:[Ljqg;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 104
    :goto_0
    iget-object v2, p0, Ljrc;->ezX:[Ljqg;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 105
    iget-object v2, p0, Ljrc;->ezX:[Ljqg;

    aget-object v2, v2, v0

    .line 106
    if-eqz v2, :cond_0

    .line 107
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 104
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    :cond_1
    iget-object v0, p0, Ljrc;->ezY:[Ljrl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljrc;->ezY:[Ljrl;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 112
    :goto_1
    iget-object v2, p0, Ljrc;->ezY:[Ljrl;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 113
    iget-object v2, p0, Ljrc;->ezY:[Ljrl;

    aget-object v2, v2, v0

    .line 114
    if-eqz v2, :cond_2

    .line 115
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 112
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 119
    :cond_3
    iget-object v0, p0, Ljrc;->eAb:Ljqb;

    if-eqz v0, :cond_4

    .line 120
    const/4 v0, 0x3

    iget-object v2, p0, Ljrc;->eAb:Ljqb;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 122
    :cond_4
    iget v0, p0, Ljrc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 123
    const/4 v0, 0x4

    iget v2, p0, Ljrc;->eyF:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 125
    :cond_5
    iget-object v0, p0, Ljrc;->eAc:Ljpu;

    if-eqz v0, :cond_6

    .line 126
    const/4 v0, 0x5

    iget-object v2, p0, Ljrc;->eAc:Ljpu;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 128
    :cond_6
    iget-object v0, p0, Ljrc;->eAd:[Ljrf;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljrc;->eAd:[Ljrf;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 129
    :goto_2
    iget-object v2, p0, Ljrc;->eAd:[Ljrf;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 130
    iget-object v2, p0, Ljrc;->eAd:[Ljrf;

    aget-object v2, v2, v0

    .line 131
    if-eqz v2, :cond_7

    .line 132
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 129
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 136
    :cond_8
    iget-object v0, p0, Ljrc;->ezZ:[Ljrv;

    if-eqz v0, :cond_a

    iget-object v0, p0, Ljrc;->ezZ:[Ljrv;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 137
    :goto_3
    iget-object v0, p0, Ljrc;->ezZ:[Ljrv;

    array-length v0, v0

    if-ge v1, v0, :cond_a

    .line 138
    iget-object v0, p0, Ljrc;->ezZ:[Ljrv;

    aget-object v0, v0, v1

    .line 139
    if-eqz v0, :cond_9

    .line 140
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 137
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 144
    :cond_a
    iget-object v0, p0, Ljrc;->eAa:Ljsa;

    if-eqz v0, :cond_b

    .line 145
    const/16 v0, 0x8

    iget-object v1, p0, Ljrc;->eAa:Ljsa;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 147
    :cond_b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 148
    return-void
.end method

.method public final bsA()Z
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Ljrc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bss()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Ljrc;->eyF:I

    return v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 152
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 153
    iget-object v2, p0, Ljrc;->ezX:[Ljqg;

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljrc;->ezX:[Ljqg;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 154
    :goto_0
    iget-object v3, p0, Ljrc;->ezX:[Ljqg;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 155
    iget-object v3, p0, Ljrc;->ezX:[Ljqg;

    aget-object v3, v3, v0

    .line 156
    if-eqz v3, :cond_0

    .line 157
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 154
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 162
    :cond_2
    iget-object v2, p0, Ljrc;->ezY:[Ljrl;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljrc;->ezY:[Ljrl;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 163
    :goto_1
    iget-object v3, p0, Ljrc;->ezY:[Ljrl;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 164
    iget-object v3, p0, Ljrc;->ezY:[Ljrl;

    aget-object v3, v3, v0

    .line 165
    if-eqz v3, :cond_3

    .line 166
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 163
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 171
    :cond_5
    iget-object v2, p0, Ljrc;->eAb:Ljqb;

    if-eqz v2, :cond_6

    .line 172
    const/4 v2, 0x3

    iget-object v3, p0, Ljrc;->eAb:Ljqb;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 175
    :cond_6
    iget v2, p0, Ljrc;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_7

    .line 176
    const/4 v2, 0x4

    iget v3, p0, Ljrc;->eyF:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 179
    :cond_7
    iget-object v2, p0, Ljrc;->eAc:Ljpu;

    if-eqz v2, :cond_8

    .line 180
    const/4 v2, 0x5

    iget-object v3, p0, Ljrc;->eAc:Ljpu;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 183
    :cond_8
    iget-object v2, p0, Ljrc;->eAd:[Ljrf;

    if-eqz v2, :cond_b

    iget-object v2, p0, Ljrc;->eAd:[Ljrf;

    array-length v2, v2

    if-lez v2, :cond_b

    move v2, v0

    move v0, v1

    .line 184
    :goto_2
    iget-object v3, p0, Ljrc;->eAd:[Ljrf;

    array-length v3, v3

    if-ge v0, v3, :cond_a

    .line 185
    iget-object v3, p0, Ljrc;->eAd:[Ljrf;

    aget-object v3, v3, v0

    .line 186
    if-eqz v3, :cond_9

    .line 187
    const/4 v4, 0x6

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 184
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_a
    move v0, v2

    .line 192
    :cond_b
    iget-object v2, p0, Ljrc;->ezZ:[Ljrv;

    if-eqz v2, :cond_d

    iget-object v2, p0, Ljrc;->ezZ:[Ljrv;

    array-length v2, v2

    if-lez v2, :cond_d

    .line 193
    :goto_3
    iget-object v2, p0, Ljrc;->ezZ:[Ljrv;

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 194
    iget-object v2, p0, Ljrc;->ezZ:[Ljrv;

    aget-object v2, v2, v1

    .line 195
    if-eqz v2, :cond_c

    .line 196
    const/4 v3, 0x7

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 193
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 201
    :cond_d
    iget-object v1, p0, Ljrc;->eAa:Ljsa;

    if-eqz v1, :cond_e

    .line 202
    const/16 v1, 0x8

    iget-object v2, p0, Ljrc;->eAa:Ljsa;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    :cond_e
    return v0
.end method

.method public final rF(I)Ljrc;
    .locals 1

    .prologue
    .line 62
    iput p1, p0, Ljrc;->eyF:I

    .line 63
    iget v0, p0, Ljrc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrc;->aez:I

    .line 64
    return-object p0
.end method
