.class public Lfph;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static cyp:Ljava/lang/String;


# instance fields
.field private final cyu:Z

.field private final cyv:Z

.field private final cyw:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lfph;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfph;->cyp:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ZZZ)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean p1, p0, Lfph;->cyu:Z

    .line 25
    iput-boolean p2, p0, Lfph;->cyv:Z

    .line 26
    iput-boolean p3, p0, Lfph;->cyw:Z

    .line 27
    return-void
.end method

.method public static k(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Z
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lfph;->cyp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->lT(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfph;
    .locals 5

    .prologue
    .line 71
    sget-object v0, Lfph;->cyp:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->lT(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 72
    new-instance v1, Lfph;

    const-string v2, "location_enabled"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "commute_enabled"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "commute_prompted"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lfph;-><init>(ZZZ)V

    return-object v1
.end method


# virtual methods
.method public final TC()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lfph;->cyv:Z

    return v0
.end method

.method public final aCD()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lfph;->cyu:Z

    return v0
.end method

.method public final aCE()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lfph;->cyw:Z

    return v0
.end method

.method public final j(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 55
    const-string v1, "location_enabled"

    iget-boolean v2, p0, Lfph;->cyu:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    const-string v1, "commute_enabled"

    iget-boolean v2, p0, Lfph;->cyv:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 57
    const-string v1, "commute_prompted"

    iget-boolean v2, p0, Lfph;->cyw:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    sget-object v1, Lfph;->cyp:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->a(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/os/Parcelable;

    .line 59
    return-void
.end method
