.class public final Ldjr;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# instance fields
.field private final bAp:Landroid/text/style/CharacterStyle;

.field private final bAq:Landroid/text/style/CharacterStyle;

.field private final bAr:Landroid/text/style/CharacterStyle;

.field private bnB:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 2

    .prologue
    const v1, 0x7f090133

    .line 32
    const v0, 0x1020016

    invoke-direct {p0, p1, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    .line 34
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Ldjr;->bAq:Landroid/text/style/CharacterStyle;

    .line 35
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Ldjr;->bAr:Landroid/text/style/CharacterStyle;

    .line 36
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const v1, 0x7f090134

    invoke-direct {v0, p1, v1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Ldjr;->bAp:Landroid/text/style/CharacterStyle;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f040173

    invoke-direct {p0, p1, v0, p2}, Ldjr;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Ldjr;->clear()V

    .line 56
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjr;->bnB:Ljava/lang/String;

    .line 57
    invoke-virtual {p0, p1}, Ldjr;->addAll(Ljava/util/Collection;)V

    .line 58
    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 41
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 42
    invoke-virtual {p0, p1}, Ldjr;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 45
    iget-object v0, p0, Ldjr;->bnB:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 46
    const v0, 0x1020016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 48
    iget-object v3, p0, Ldjr;->bnB:Ljava/lang/String;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    if-ltz v4, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v4

    iget-object v6, p0, Ldjr;->bAq:Landroid/text/style/CharacterStyle;

    invoke-virtual {v5, v6, v7, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v6, p0, Ldjr;->bAp:Landroid/text/style/CharacterStyle;

    invoke-virtual {v5, v6, v4, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v4, p0, Ldjr;->bAr:Landroid/text/style/CharacterStyle;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v5, v4, v3, v2, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :goto_0
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :cond_0
    return-object v1

    .line 48
    :cond_1
    iget-object v3, p0, Ldjr;->bAq:Landroid/text/style/CharacterStyle;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v5, v3, v7, v2, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method
