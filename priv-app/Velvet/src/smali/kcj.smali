.class public Lkcj;
.super Ljava/net/HttpURLConnection;
.source "PG"

# interfaces
.implements Lkcp;


# instance fields
.field protected eQU:Lkcc;

.field private final eRJ:Lkcq;

.field private eRK:J

.field private eRL:I

.field private eRM:Ljava/io/IOException;

.field private eRN:Ljava/net/Proxy;

.field final eRd:Lkas;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lkas;)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0, p1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    .line 73
    new-instance v0, Lkcq;

    invoke-direct {v0}, Lkcq;-><init>()V

    iput-object v0, p0, Lkcj;->eRJ:Lkcq;

    .line 75
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lkcj;->eRK:J

    .line 83
    iput-object p2, p0, Lkcj;->eRd:Lkas;

    .line 84
    return-void
.end method

.method private H(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 565
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 566
    if-eqz p2, :cond_0

    .line 567
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxT()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 569
    :cond_0
    const-string v0, ","

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 570
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 572
    :cond_1
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0, v1}, Lkas;->aF(Ljava/util/List;)Lkas;

    .line 573
    return-void
.end method

.method private a(Ljava/lang/String;Lkcq;Lkac;Lkcw;)Lkcc;
    .locals 7

    .prologue
    .line 275
    iget-object v0, p0, Lkcj;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    new-instance v0, Lkcc;

    iget-object v1, p0, Lkcj;->eRd:Lkas;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lkcc;-><init>(Lkas;Lkcp;Ljava/lang/String;Lkcq;Lkac;Lkcw;)V

    .line 278
    :goto_0
    return-object v0

    .line 277
    :cond_0
    iget-object v0, p0, Lkcj;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 278
    new-instance v0, Lkcl;

    iget-object v1, p0, Lkcj;->eRd:Lkas;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lkcl;-><init>(Lkas;Lkcp;Ljava/lang/String;Lkcq;Lkac;Lkcw;)V

    goto :goto_0

    .line 280
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static a(Ljava/net/Proxy;)Z
    .locals 2

    .prologue
    .line 501
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private byH()V
    .locals 4

    .prologue
    .line 245
    iget-object v0, p0, Lkcj;->eRM:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lkcj;->eRM:Ljava/io/IOException;

    throw v0

    .line 247
    :cond_0
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    if-eqz v0, :cond_1

    .line 266
    :goto_0
    return-void

    .line 251
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcj;->connected:Z

    .line 253
    :try_start_0
    iget-boolean v0, p0, Lkcj;->doOutput:Z

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Lkcj;->method:Ljava/lang/String;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 256
    const-string v0, "POST"

    iput-object v0, p0, Lkcj;->method:Ljava/lang/String;

    .line 262
    :cond_2
    iget-object v0, p0, Lkcj;->method:Ljava/lang/String;

    iget-object v1, p0, Lkcj;->eRJ:Lkcq;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lkcj;->a(Ljava/lang/String;Lkcq;Lkac;Lkcw;)Lkcc;

    move-result-object v0

    iput-object v0, p0, Lkcj;->eQU:Lkcc;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    .line 264
    iput-object v0, p0, Lkcj;->eRM:Ljava/io/IOException;

    .line 265
    throw v0

    .line 257
    :cond_3
    :try_start_1
    iget-object v0, p0, Lkcj;->method:Ljava/lang/String;

    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lkcj;->method:Ljava/lang/String;

    const-string v1, "PUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lkcj;->method:Ljava/lang/String;

    const-string v1, "PATCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 259
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lkcj;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support writing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private byJ()Lkcc;
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 290
    invoke-direct {p0}, Lkcj;->byH()V

    .line 292
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byp()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    .line 304
    :goto_0
    return-object v0

    .line 328
    :cond_0
    sget-object v3, Lkck;->eRQ:Lkck;

    if-ne v0, v3, :cond_1

    .line 329
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byv()V

    .line 332
    :cond_1
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0, v7}, Lkcc;->jI(Z)V

    .line 334
    iget-object v3, p0, Lkcj;->eRJ:Lkcq;

    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byu()Lkac;

    move-result-object v4

    move-object v0, v1

    check-cast v0, Lkcw;

    invoke-direct {p0, v2, v3, v4, v0}, Lkcj;->a(Ljava/lang/String;Lkcq;Lkac;Lkcw;)Lkcc;

    move-result-object v0

    iput-object v0, p0, Lkcj;->eQU:Lkcc;

    .line 337
    if-nez v1, :cond_2

    .line 339
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byq()Lkcs;

    move-result-object v0

    iget-wide v2, v0, Lkcs;->eSg:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    iget-object v1, v0, Lkcs;->eRZ:Lkcq;

    const-string v2, "Content-Length"

    invoke-virtual {v1, v2}, Lkcq;->AB(Ljava/lang/String;)V

    iput-wide v8, v0, Lkcs;->eSg:J

    .line 297
    :cond_2
    invoke-direct {p0, v6}, Lkcj;->jJ(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRf:Lkac;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0}, Lkac;->bxC()Lkaw;

    move-result-object v0

    iget-object v4, v0, Lkaw;->eOL:Ljava/net/Proxy;

    :goto_1
    invoke-virtual {p0}, Lkcj;->getResponseCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_3
    sget-object v0, Lkck;->eRO:Lkck;

    .line 302
    :goto_2
    sget-object v1, Lkck;->eRO:Lkck;

    if-ne v0, v1, :cond_d

    .line 303
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byv()V

    .line 304
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    goto :goto_0

    .line 298
    :cond_4
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxK()Ljava/net/Proxy;

    move-result-object v4

    goto :goto_1

    :sswitch_0
    invoke-virtual {v4}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_5

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :sswitch_1
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxP()Lkap;

    move-result-object v0

    invoke-virtual {p0}, Lkcj;->getResponseCode()I

    move-result v1

    iget-object v2, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v2}, Lkcc;->byr()Lkcu;

    move-result-object v2

    invoke-virtual {v2}, Lkcu;->byX()Lkcq;

    move-result-object v2

    iget-object v3, p0, Lkcj;->eRJ:Lkcq;

    iget-object v5, p0, Lkcj;->url:Ljava/net/URL;

    invoke-static/range {v0 .. v5}, Lkby;->a(Lkap;ILkcq;Lkcq;Ljava/net/Proxy;Ljava/net/URL;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lkck;->eRP:Lkck;

    goto :goto_2

    :cond_6
    sget-object v0, Lkck;->eRO:Lkck;

    goto :goto_2

    :sswitch_2
    invoke-virtual {p0}, Lkcj;->getInstanceFollowRedirects()Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lkcj;->eRL:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lkcj;->eRL:I

    const/16 v2, 0x14

    if-le v1, v2, :cond_7

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Too many redirects: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lkcj;->eRL:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    const/16 v1, 0x133

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lkcj;->method:Ljava/lang/String;

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lkcj;->method:Ljava/lang/String;

    const-string v1, "HEAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_8
    const-string v0, "Location"

    invoke-virtual {p0, v0}, Lkcj;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lkcj;->url:Ljava/net/URL;

    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1, v0}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    iput-object v2, p0, Lkcj;->url:Ljava/net/URL;

    iget-object v0, p0, Lkcj;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lkcj;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_9
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lkcj;->url:Ljava/net/URL;

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxR()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_a
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lkcj;->url:Ljava/net/URL;

    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v1}, Lkbt;->d(Ljava/net/URL;)I

    move-result v0

    iget-object v1, p0, Lkcj;->url:Ljava/net/URL;

    invoke-static {v1}, Lkbt;->d(Ljava/net/URL;)I

    move-result v1

    if-ne v0, v1, :cond_b

    move v0, v6

    :goto_3
    if-eqz v3, :cond_c

    if-eqz v0, :cond_c

    if-eqz v2, :cond_c

    sget-object v0, Lkck;->eRP:Lkck;

    goto/16 :goto_2

    :cond_b
    move v0, v7

    goto :goto_3

    :cond_c
    sget-object v0, Lkck;->eRQ:Lkck;

    goto/16 :goto_2

    .line 308
    :cond_d
    iget-object v2, p0, Lkcj;->method:Ljava/lang/String;

    .line 309
    iget-object v1, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v1}, Lkcc;->byo()Ljava/io/OutputStream;

    move-result-object v1

    .line 314
    invoke-virtual {p0}, Lkcj;->getResponseCode()I

    move-result v3

    .line 315
    const/16 v4, 0x12c

    if-eq v3, v4, :cond_e

    const/16 v4, 0x12d

    if-eq v3, v4, :cond_e

    const/16 v4, 0x12e

    if-eq v3, v4, :cond_e

    const/16 v4, 0x12f

    if-ne v3, v4, :cond_f

    .line 319
    :cond_e
    const-string v2, "GET"

    .line 320
    const/4 v1, 0x0

    .line 323
    :cond_f
    if-eqz v1, :cond_0

    instance-of v3, v1, Lkcw;

    if-nez v3, :cond_0

    .line 324
    new-instance v0, Ljava/net/HttpRetryException;

    const-string v1, "Cannot retry streamed HTTP body"

    iget-object v2, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v2}, Lkcc;->getResponseCode()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/net/HttpRetryException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 298
    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x12e -> :sswitch_2
        0x12f -> :sswitch_2
        0x133 -> :sswitch_2
        0x191 -> :sswitch_1
        0x197 -> :sswitch_0
    .end sparse-switch
.end method

.method private jJ(Z)Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 351
    :try_start_0
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byl()V

    .line 352
    if-eqz p1, :cond_0

    .line 353
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byA()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :cond_0
    :goto_0
    return v2

    .line 357
    :catch_0
    move-exception v0

    move-object v4, v0

    .line 358
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    iget-object v6, v0, Lkcc;->eRg:Lkcx;

    if-eqz v6, :cond_2

    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRf:Lkac;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRf:Lkac;

    invoke-virtual {v0}, Lkac;->bxC()Lkaw;

    move-result-object v0

    iget-object v1, v0, Lkaw;->eOL:Ljava/net/Proxy;

    invoke-virtual {v1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v5, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v1, v5, :cond_1

    iget-object v1, v6, Lkcx;->proxySelector:Ljava/net/ProxySelector;

    if-eqz v1, :cond_1

    iget-object v1, v6, Lkcx;->proxySelector:Ljava/net/ProxySelector;

    iget-object v5, v6, Lkcx;->eRo:Ljava/net/URI;

    iget-object v7, v0, Lkaw;->eOL:Ljava/net/Proxy;

    invoke-virtual {v7}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v7

    invoke-virtual {v1, v5, v7, v4}, Ljava/net/ProxySelector;->connectFailed(Ljava/net/URI;Ljava/net/SocketAddress;Ljava/io/IOException;)V

    :cond_1
    iget-object v1, v6, Lkcx;->ePH:Lkax;

    invoke-virtual {v1, v0, v4}, Lkax;->a(Lkaw;Ljava/io/IOException;)V

    :cond_2
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byo()Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_3

    instance-of v1, v0, Lkcw;

    if-eqz v1, :cond_8

    :cond_3
    move v5, v2

    :goto_1
    if-nez v6, :cond_4

    iget-object v1, p0, Lkcj;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRf:Lkac;

    if-eqz v1, :cond_7

    :cond_4
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Lkcx;->bzd()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v6}, Lkcx;->bzc()Z

    move-result v1

    if-nez v1, :cond_5

    iget-boolean v1, v6, Lkcx;->eSK:Z

    if-nez v1, :cond_5

    invoke-virtual {v6}, Lkcx;->bze()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_5
    move v1, v2

    :goto_2
    if-eqz v1, :cond_7

    :cond_6
    instance-of v1, v4, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v1, :cond_a

    invoke-virtual {v4}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/security/cert/CertificateException;

    if-eqz v1, :cond_a

    move v1, v2

    :goto_3
    instance-of v7, v4, Ljava/net/ProtocolException;

    if-nez v1, :cond_b

    if-nez v7, :cond_b

    move v1, v2

    :goto_4
    if-eqz v1, :cond_7

    if-nez v5, :cond_c

    :cond_7
    iput-object v4, p0, Lkcj;->eRM:Ljava/io/IOException;

    move v2, v3

    :goto_5
    if-eqz v2, :cond_d

    move v2, v3

    .line 359
    goto/16 :goto_0

    :cond_8
    move v5, v3

    .line 358
    goto :goto_1

    :cond_9
    move v1, v3

    goto :goto_2

    :cond_a
    move v1, v3

    goto :goto_3

    :cond_b
    move v1, v3

    goto :goto_4

    :cond_c
    iget-object v1, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v1, v2}, Lkcc;->jI(Z)V

    check-cast v0, Lkcw;

    iget-object v1, p0, Lkcj;->method:Ljava/lang/String;

    iget-object v5, p0, Lkcj;->eRJ:Lkcq;

    const/4 v7, 0x0

    invoke-direct {p0, v1, v5, v7, v0}, Lkcj;->a(Ljava/lang/String;Lkcq;Lkac;Lkcw;)Lkcc;

    move-result-object v0

    iput-object v0, p0, Lkcj;->eQU:Lkcc;

    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    iput-object v6, v0, Lkcc;->eRg:Lkcx;

    goto :goto_5

    .line 361
    :cond_d
    throw v4
.end method


# virtual methods
.method public final addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 537
    iget-boolean v0, p0, Lkcj;->connected:Z

    if-eqz v0, :cond_0

    .line 538
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 540
    :cond_0
    if-nez p1, :cond_1

    .line 541
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_1
    if-nez p2, :cond_2

    .line 549
    invoke-static {}, Lkbl;->byd()Lkbl;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ignoring header "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " because its value was null."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkbl;->At(Ljava/lang/String;)V

    .line 558
    :goto_0
    return-void

    .line 553
    :cond_2
    const-string v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 554
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lkcj;->H(Ljava/lang/String;Z)V

    goto :goto_0

    .line 556
    :cond_3
    iget-object v0, p0, Lkcj;->eRJ:Lkcq;

    invoke-virtual {v0, p1, p2}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/net/Proxy;)V
    .locals 0

    .prologue
    .line 589
    iput-object p1, p0, Lkcj;->eRN:Ljava/net/Proxy;

    .line 590
    return-void
.end method

.method public byI()Ljava/net/HttpURLConnection;
    .locals 0

    .prologue
    .line 270
    return-object p0
.end method

.method public final byK()Lkcc;
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    return-object v0
.end method

.method public final byL()J
    .locals 2

    .prologue
    .line 480
    iget-wide v0, p0, Lkcj;->eRK:J

    return-wide v0
.end method

.method public final byM()I
    .locals 1

    .prologue
    .line 484
    iget v0, p0, Lkcj;->chunkLength:I

    return v0
.end method

.method public final connect()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lkcj;->byH()V

    .line 90
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lkcj;->jJ(Z)Z

    move-result v0

    .line 91
    if-eqz v0, :cond_0

    .line 92
    return-void
.end method

.method public final disconnect()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->bys()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lkbt;->b(Ljava/io/Closeable;)V

    .line 105
    :cond_0
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkcc;->jI(Z)V

    .line 107
    :cond_1
    return-void
.end method

.method public getConnectTimeout()I
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->getConnectTimeout()I

    move-result v0

    return v0
.end method

.method public final getErrorStream()Ljava/io/InputStream;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 115
    :try_start_0
    invoke-direct {p0}, Lkcj;->byJ()Lkcc;

    move-result-object v1

    .line 116
    invoke-virtual {v1}, Lkcc;->byw()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lkcc;->getResponseCode()I

    move-result v2

    const/16 v3, 0x190

    if-lt v2, v3, :cond_0

    .line 117
    invoke-virtual {v1}, Lkcc;->bys()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 121
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getHeaderField(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    :try_start_0
    invoke-direct {p0}, Lkcj;->byJ()Lkcc;

    move-result-object v0

    invoke-virtual {v0}, Lkcc;->byr()Lkcu;

    move-result-object v0

    invoke-virtual {v0}, Lkcu;->byX()Lkcq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkcq;->getValue(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    :try_start_0
    invoke-direct {p0}, Lkcj;->byJ()Lkcc;

    move-result-object v0

    invoke-virtual {v0}, Lkcc;->byr()Lkcu;

    move-result-object v0

    invoke-virtual {v0}, Lkcu;->byX()Lkcq;

    move-result-object v0

    .line 145
    if-nez p1, :cond_0

    invoke-virtual {v0}, Lkcq;->byP()Ljava/lang/String;

    move-result-object v0

    .line 147
    :goto_0
    return-object v0

    .line 145
    :cond_0
    invoke-virtual {v0, p1}, Lkcq;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    :try_start_0
    invoke-direct {p0}, Lkcj;->byJ()Lkcc;

    move-result-object v0

    invoke-virtual {v0}, Lkcc;->byr()Lkcu;

    move-result-object v0

    invoke-virtual {v0}, Lkcu;->byX()Lkcq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkcq;->tL(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderFields()Ljava/util/Map;
    .locals 2

    .prologue
    .line 161
    :try_start_0
    invoke-direct {p0}, Lkcj;->byJ()Lkcc;

    move-result-object v0

    invoke-virtual {v0}, Lkcc;->byr()Lkcu;

    move-result-object v0

    invoke-virtual {v0}, Lkcu;->byX()Lkcq;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkcq;->jK(Z)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 176
    iget-boolean v0, p0, Lkcj;->doInput:Z

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "This protocol does not support input"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_0
    invoke-direct {p0}, Lkcj;->byJ()Lkcc;

    move-result-object v0

    .line 186
    invoke-virtual {p0}, Lkcj;->getResponseCode()I

    move-result v1

    const/16 v2, 0x190

    if-lt v1, v2, :cond_1

    .line 187
    new-instance v0, Ljava/io/FileNotFoundException;

    iget-object v1, p0, Lkcj;->url:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_1
    invoke-virtual {v0}, Lkcc;->bys()Ljava/io/InputStream;

    move-result-object v0

    .line 191
    if-nez v0, :cond_2

    .line 192
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No response body exists; responseCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lkcj;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_2
    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 3

    .prologue
    .line 198
    invoke-virtual {p0}, Lkcj;->connect()V

    .line 200
    iget-object v0, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byo()Ljava/io/OutputStream;

    move-result-object v0

    .line 201
    if-nez v0, :cond_0

    .line 202
    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "method does not support a request body: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lkcj;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_0
    iget-object v1, p0, Lkcj;->eQU:Lkcc;

    invoke-virtual {v1}, Lkcc;->byp()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "cannot write request body after response has been read"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_1
    return-object v0
.end method

.method public final getPermission()Ljava/security/Permission;
    .locals 4

    .prologue
    .line 211
    invoke-virtual {p0}, Lkcj;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 212
    invoke-virtual {p0}, Lkcj;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lkbt;->d(Ljava/net/URL;)I

    move-result v0

    .line 213
    invoke-virtual {p0}, Lkcj;->usingProxy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxK()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    .line 215
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    .line 218
    :cond_0
    new-instance v2, Ljava/net/SocketPermission;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "connect, resolve"

    invoke-direct {v2, v0, v1}, Ljava/net/SocketPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public getReadTimeout()I
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->getReadTimeout()I

    move-result v0

    return v0
.end method

.method public final getRequestProperties()Ljava/util/Map;
    .locals 2

    .prologue
    .line 168
    iget-boolean v0, p0, Lkcj;->connected:Z

    if-eqz v0, :cond_0

    .line 169
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot access request header fields after connection is set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    iget-object v0, p0, Lkcj;->eRJ:Lkcq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkcq;->jK(Z)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    if-nez p1, :cond_0

    .line 223
    const/4 v0, 0x0

    .line 225
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkcj;->eRJ:Lkcq;

    invoke-virtual {v0, p1}, Lkcq;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getResponseCode()I
    .locals 1

    .prologue
    .line 509
    invoke-direct {p0}, Lkcj;->byJ()Lkcc;

    move-result-object v0

    invoke-virtual {v0}, Lkcc;->getResponseCode()I

    move-result v0

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 505
    invoke-direct {p0}, Lkcj;->byJ()Lkcc;

    move-result-object v0

    invoke-virtual {v0}, Lkcc;->byr()Lkcu;

    move-result-object v0

    invoke-virtual {v0}, Lkcu;->byX()Lkcq;

    move-result-object v0

    invoke-virtual {v0}, Lkcq;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setConnectTimeout(I)V
    .locals 4

    .prologue
    .line 229
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lkas;->f(JLjava/util/concurrent/TimeUnit;)V

    .line 230
    return-void
.end method

.method public setFixedLengthStreamingMode(I)V
    .locals 2

    .prologue
    .line 576
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lkcj;->setFixedLengthStreamingMode(J)V

    .line 577
    return-void
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 3

    .prologue
    .line 581
    iget-boolean v0, p0, Ljava/net/HttpURLConnection;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 582
    :cond_0
    iget v0, p0, Lkcj;->chunkLength:I

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already in chunked mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 583
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "contentLength < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 584
    :cond_2
    iput-wide p1, p0, Lkcj;->eRK:J

    .line 585
    const-wide/32 v0, 0x7fffffff

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Ljava/net/HttpURLConnection;->fixedContentLength:I

    .line 586
    return-void
.end method

.method public setReadTimeout(I)V
    .locals 4

    .prologue
    .line 237
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lkas;->g(JLjava/util/concurrent/TimeUnit;)V

    .line 238
    return-void
.end method

.method public final setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 513
    iget-boolean v0, p0, Lkcj;->connected:Z

    if-eqz v0, :cond_0

    .line 514
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 516
    :cond_0
    if-nez p1, :cond_1

    .line 517
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 519
    :cond_1
    if-nez p2, :cond_2

    .line 525
    invoke-static {}, Lkbl;->byd()Lkbl;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ignoring header "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " because its value was null."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkbl;->At(Ljava/lang/String;)V

    .line 534
    :goto_0
    return-void

    .line 529
    :cond_2
    const-string v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 530
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lkcj;->H(Ljava/lang/String;Z)V

    goto :goto_0

    .line 532
    :cond_3
    iget-object v0, p0, Lkcj;->eRJ:Lkcq;

    invoke-virtual {v0, p1, p2}, Lkcq;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final usingProxy()Z
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lkcj;->eRN:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lkcj;->eRN:Ljava/net/Proxy;

    invoke-static {v0}, Lkcj;->a(Ljava/net/Proxy;)Z

    move-result v0

    .line 497
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lkcj;->eRd:Lkas;

    invoke-virtual {v0}, Lkas;->bxK()Ljava/net/Proxy;

    move-result-object v0

    invoke-static {v0}, Lkcj;->a(Ljava/net/Proxy;)Z

    move-result v0

    goto :goto_0
.end method
