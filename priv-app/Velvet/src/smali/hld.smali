.class public final Lhld;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SimpleDateFormat"
    }
.end annotation


# static fields
.field private static final djL:Ljava/text/SimpleDateFormat;

.field private static final djM:Ljava/text/SimpleDateFormat;

.field private static final djN:Ljava/text/SimpleDateFormat;

.field private static final djO:Ljava/text/SimpleDateFormat;

.field private static final djP:Ljava/text/SimpleDateFormat;

.field private static final mr:Ljava/text/SimpleDateFormat;


# instance fields
.field private bMd:Ljava/lang/String;

.field private bMe:Ljava/util/List;

.field private bMf:Ljava/util/Map;

.field private bMg:Ljava/lang/String;

.field private final djQ:Ljava/text/SimpleDateFormat;

.field private djR:Lhle;

.field private djS:Z

.field private djT:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEEE"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhld;->djL:Ljava/text/SimpleDateFormat;

    .line 37
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhld;->djM:Ljava/text/SimpleDateFormat;

    .line 38
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhld;->mr:Ljava/text/SimpleDateFormat;

    .line 39
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhld;->djN:Ljava/text/SimpleDateFormat;

    .line 40
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "KK"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhld;->djO:Ljava/text/SimpleDateFormat;

    .line 41
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "mm"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhld;->djP:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/search/shared/actions/HelpAction;Z)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lhld;->djT:I

    .line 57
    if-eqz p2, :cond_0

    sget-object v0, Lhld;->djN:Ljava/text/SimpleDateFormat;

    :goto_0
    iput-object v0, p0, Lhld;->djQ:Ljava/text/SimpleDateFormat;

    .line 58
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/HelpAction;->ahr()Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/HelpAction;->ahu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/HelpAction;->ahs()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/HelpAction;->aht()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lhld;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)V

    .line 63
    :goto_1
    return-void

    .line 57
    :cond_0
    sget-object v0, Lhld;->djO:Ljava/text/SimpleDateFormat;

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/HelpAction;->ahu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/HelpAction;->ahv()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lhld;->djS:Z

    iput-object v0, p0, Lhld;->bMd:Ljava/lang/String;

    iput-object v1, p0, Lhld;->bMg:Ljava/lang/String;

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 126
    const-string v0, "\\"

    const-string v1, "\\\\"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 127
    const-string v1, "$"

    const-string v2, "\\$"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(?<!%)%"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(?![0-9])"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;[ILjava/util/Calendar;Lcom/google/android/search/shared/actions/utils/ExampleContact;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 137
    .line 140
    array-length v3, p1

    move v1, v0

    move v2, v0

    move-object v0, p0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v4, p1, v1

    .line 141
    packed-switch v4, :pswitch_data_0

    .line 167
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 140
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 143
    :pswitch_0
    if-eqz p2, :cond_0

    .line 144
    sget-object v4, Lhld;->djL:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lhld;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 149
    :pswitch_1
    if-eqz p2, :cond_0

    .line 150
    sget-object v4, Lhld;->mr:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lhld;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 155
    :pswitch_2
    if-eqz p2, :cond_0

    .line 156
    sget-object v4, Lhld;->djM:Ljava/text/SimpleDateFormat;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lhld;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 162
    :pswitch_3
    if-eqz p3, :cond_0

    .line 163
    iget-object v4, p3, Lcom/google/android/search/shared/actions/utils/ExampleContact;->bQv:Ljava/lang/String;

    invoke-static {v0, v2, v4}, Lhld;->a(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 171
    :cond_1
    const-string v1, "%%"

    const-string v2, "%"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 141
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhld;->djS:Z

    .line 73
    iput-object p1, p0, Lhld;->bMd:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lhld;->bMe:Ljava/util/List;

    .line 77
    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 78
    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lhld;->bMf:Ljava/util/Map;

    .line 79
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Integer;

    .line 80
    iget-object v3, p0, Lhld;->bMf:Ljava/util/Map;

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Likr;->w(Ljava/lang/Iterable;)Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method

.method private b(Ljlk;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x5

    const/4 v3, 0x1

    .line 193
    invoke-virtual {p1}, Ljlk;->getQuery()Ljava/lang/String;

    move-result-object v5

    .line 195
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 198
    const/4 v2, 0x0

    .line 204
    invoke-static {p1}, Lcom/google/android/search/shared/actions/HelpAction;->a(Ljlk;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v4, p0, Lhld;->bMf:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/utils/ExampleContact;

    .line 206
    :goto_0
    invoke-virtual {p1}, Ljlk;->ok()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 208
    iget-object v2, p0, Lhld;->djR:Lhle;

    invoke-virtual {p1}, Ljlk;->oj()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lhle;->el(Ljava/lang/String;)V

    .line 260
    :cond_0
    :goto_1
    iget-object v2, p1, Ljlk;->erN:[I

    array-length v2, v2

    if-lez v2, :cond_8

    .line 261
    iget-object v2, p0, Lhld;->djR:Lhle;

    iget-object v3, p1, Ljlk;->erN:[I

    invoke-static {v5, v3, v1, v0}, Lhld;->a(Ljava/lang/String;[ILjava/util/Calendar;Lcom/google/android/search/shared/actions/utils/ExampleContact;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lhle;->ek(Ljava/lang/String;)V

    .line 266
    :goto_2
    return-void

    :cond_1
    move-object v0, v1

    .line 204
    goto :goto_0

    .line 209
    :cond_2
    if-eqz v0, :cond_3

    .line 211
    iget-object v2, p0, Lhld;->djR:Lhle;

    invoke-interface {v2, v0}, Lhle;->a(Lcom/google/android/search/shared/actions/utils/ExampleContact;)V

    goto :goto_1

    .line 212
    :cond_3
    invoke-virtual {p1}, Ljlk;->bpa()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {p1}, Ljlk;->boY()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p1, Ljlk;->erP:Ljkh;

    if-eqz v4, :cond_0

    .line 215
    :cond_4
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 218
    invoke-virtual {v4, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 221
    invoke-virtual {p1}, Ljlk;->boY()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 222
    const/16 v1, 0xd

    invoke-virtual {p1}, Ljlk;->boX()I

    move-result v7

    invoke-virtual {v4, v1, v7}, Ljava/util/Calendar;->add(II)V

    move v1, v2

    .line 228
    :goto_3
    iget-object v2, p1, Ljlk;->erP:Ljkh;

    if-eqz v2, :cond_5

    .line 229
    iget-object v2, p1, Ljlk;->erP:Ljkh;

    .line 232
    const/16 v7, 0xb

    invoke-virtual {v2}, Ljkh;->getHour()I

    move-result v8

    invoke-virtual {v4, v7, v8}, Ljava/util/Calendar;->set(II)V

    .line 233
    const/16 v7, 0xc

    invoke-virtual {v2}, Ljkh;->getMinute()I

    move-result v2

    invoke-virtual {v4, v7, v2}, Ljava/util/Calendar;->set(II)V

    .line 235
    invoke-virtual {p1}, Ljlk;->bpa()Z

    move-result v2

    if-nez v2, :cond_5

    .line 237
    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 239
    invoke-virtual {v4, v9, v3}, Ljava/util/Calendar;->add(II)V

    .line 245
    :cond_5
    if-eqz v1, :cond_7

    .line 247
    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 249
    iget-object v2, p0, Lhld;->djR:Lhle;

    sget-object v3, Lhld;->djL:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lhld;->mr:Ljava/text/SimpleDateFormat;

    invoke-virtual {v6, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lhld;->djM:Ljava/text/SimpleDateFormat;

    invoke-virtual {v7, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v6, v1}, Lhle;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v4

    .line 251
    goto/16 :goto_1

    .line 223
    :cond_6
    invoke-virtual {p1}, Ljlk;->bpa()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 225
    invoke-virtual {p1}, Ljlk;->boZ()I

    move-result v1

    invoke-virtual {v4, v9, v1}, Ljava/util/Calendar;->add(II)V

    move v1, v3

    goto :goto_3

    .line 253
    :cond_7
    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 255
    iget-object v2, p0, Lhld;->djR:Lhle;

    iget-object v3, p0, Lhld;->djQ:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lhld;->djP:Ljava/text/SimpleDateFormat;

    invoke-virtual {v6, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Lhle;->f(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v4

    goto/16 :goto_1

    .line 264
    :cond_8
    iget-object v0, p0, Lhld;->djR:Lhle;

    invoke-interface {v0, v5}, Lhle;->ek(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    move v1, v2

    goto :goto_3
.end method


# virtual methods
.method public final a(Lhle;)V
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p0, Lhld;->djS:Z

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lhld;->bMd:Ljava/lang/String;

    iget-object v1, p0, Lhld;->bMg:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lhle;->g(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_0
    return-void

    .line 98
    :cond_0
    iput-object p1, p0, Lhld;->djR:Lhle;

    .line 99
    iget-object v0, p0, Lhld;->bMd:Ljava/lang/String;

    invoke-interface {p1, v0}, Lhle;->ej(Ljava/lang/String;)V

    .line 102
    const/4 v0, 0x0

    iput v0, p0, Lhld;->djT:I

    .line 103
    iget-object v0, p0, Lhld;->bMe:Ljava/util/List;

    iget v1, p0, Lhld;->djT:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljlk;

    invoke-direct {p0, v0}, Lhld;->b(Ljlk;)V

    goto :goto_0
.end method

.method public final aQz()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 108
    iget-boolean v0, p0, Lhld;->djS:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 111
    iget v0, p0, Lhld;->djT:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhld;->djT:I

    .line 112
    iget v0, p0, Lhld;->djT:I

    iget-object v2, p0, Lhld;->bMe:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 113
    iput v1, p0, Lhld;->djT:I

    .line 116
    :cond_0
    iget-object v0, p0, Lhld;->bMe:Ljava/util/List;

    iget v1, p0, Lhld;->djT:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljlk;

    invoke-direct {p0, v0}, Lhld;->b(Ljlk;)V

    .line 117
    return-void

    :cond_1
    move v0, v1

    .line 108
    goto :goto_0
.end method
