.class public final Ljmw;
.super Ljsl;
.source "PG"


# static fields
.field public static final euD:Ljsm;


# instance fields
.field private aez:I

.field private aim:Ljava/lang/String;

.field private euE:Ljsc;

.field private euF:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 8018
    const/16 v0, 0xb

    const-class v1, Ljmw;

    const v2, 0xc22f54a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmw;->euD:Ljsm;

    .line 8029
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8080
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 8081
    iput v1, p0, Ljmw;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmw;->aim:Ljava/lang/String;

    iput-object v2, p0, Ljmw;->euE:Ljsc;

    iput v1, p0, Ljmw;->euF:I

    iput-object v2, p0, Ljmw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmw;->eCz:I

    .line 8082
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8011
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmw;->aim:Ljava/lang/String;

    iget v0, p0, Ljmw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmw;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljmw;->euE:Ljsc;

    if-nez v0, :cond_1

    new-instance v0, Ljsc;

    invoke-direct {v0}, Ljsc;-><init>()V

    iput-object v0, p0, Ljmw;->euE:Ljsc;

    :cond_1
    iget-object v0, p0, Ljmw;->euE:Ljsc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljmw;->euF:I

    iget v0, p0, Ljmw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmw;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 8097
    iget v0, p0, Ljmw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 8098
    const/4 v0, 0x1

    iget-object v1, p0, Ljmw;->aim:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 8100
    :cond_0
    iget-object v0, p0, Ljmw;->euE:Ljsc;

    if-eqz v0, :cond_1

    .line 8101
    const/4 v0, 0x2

    iget-object v1, p0, Ljmw;->euE:Ljsc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 8103
    :cond_1
    iget v0, p0, Ljmw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 8104
    const/4 v0, 0x3

    iget v1, p0, Ljmw;->euF:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 8106
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 8107
    return-void
.end method

.method public final bqw()I
    .locals 1

    .prologue
    .line 8064
    iget v0, p0, Ljmw;->euF:I

    return v0
.end method

.method public final bqx()Z
    .locals 1

    .prologue
    .line 8072
    iget v0, p0, Ljmw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 8111
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 8112
    iget v1, p0, Ljmw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 8113
    const/4 v1, 0x1

    iget-object v2, p0, Ljmw;->aim:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8116
    :cond_0
    iget-object v1, p0, Ljmw;->euE:Ljsc;

    if-eqz v1, :cond_1

    .line 8117
    const/4 v1, 0x2

    iget-object v2, p0, Ljmw;->euE:Ljsc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8120
    :cond_1
    iget v1, p0, Ljmw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 8121
    const/4 v1, 0x3

    iget v2, p0, Ljmw;->euF:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8124
    :cond_2
    return v0
.end method

.method public final pB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8039
    iget-object v0, p0, Ljmw;->aim:Ljava/lang/String;

    return-object v0
.end method

.method public final xl(Ljava/lang/String;)Ljmw;
    .locals 1

    .prologue
    .line 8042
    if-nez p1, :cond_0

    .line 8043
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 8045
    :cond_0
    iput-object p1, p0, Ljmw;->aim:Ljava/lang/String;

    .line 8046
    iget v0, p0, Ljmw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmw;->aez:I

    .line 8047
    return-object p0
.end method
