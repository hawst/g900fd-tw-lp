.class public final Lfxq;
.super Lfus;
.source "PG"


# instance fields
.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Lizq;Lftz;Lemp;Lgbr;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3, p4}, Lfus;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    .line 56
    iput-object p3, p0, Lfxq;->mClock:Lemp;

    .line 57
    return-void
.end method

.method private E(Lizj;)Ljal;
    .locals 2

    .prologue
    .line 134
    new-instance v0, Ljak;

    invoke-direct {v0}, Ljak;-><init>()V

    .line 135
    invoke-static {p1}, Lfxq;->F(Lizj;)Ljhm;

    move-result-object v1

    iget-object v1, v1, Ljhm;->aeB:Ljbp;

    iput-object v1, v0, Ljak;->aeB:Ljbp;

    .line 136
    new-instance v1, Ljal;

    invoke-direct {v1}, Ljal;-><init>()V

    .line 137
    iput-object v0, v1, Ljal;->dWL:Ljak;

    .line 138
    return-object v1
.end method

.method private static F(Lizj;)Ljhm;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lizj;->dTN:Ljio;

    .line 193
    iget-object v0, v0, Ljio;->enz:Ljhm;

    .line 194
    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljhm;)Ljava/lang/CharSequence;
    .locals 12
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 200
    invoke-virtual {p3}, Ljhm;->blC()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Ljhm;->bcB()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-object v0, p0, Lfxq;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-static {p1, v2, v3, v10}, Lesi;->b(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    .line 201
    :goto_0
    iget-object v6, p3, Ljhm;->aeB:Ljbp;

    invoke-virtual {p2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v2

    if-eqz v2, :cond_0

    new-array v8, v11, [F

    invoke-virtual {v2}, Ljbp;->mR()D

    move-result-wide v0

    invoke-virtual {v2}, Ljbp;->mS()D

    move-result-wide v2

    invoke-virtual {v6}, Ljbp;->mR()D

    move-result-wide v4

    invoke-virtual {v6}, Ljbp;->mS()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    aget v0, v8, v10

    float-to-int v0, v0

    invoke-static {p2}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->m(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->aCF()I

    move-result v1

    const v2, 0x3e4ccccd    # 0.2f

    const v3, 0x3dcccccd    # 0.1f

    invoke-static {p1, v0, v2, v3, v1}, Lgay;->a(Landroid/content/Context;IFFI)Ljava/lang/String;

    move-result-object v1

    .line 203
    :cond_0
    const-string v0, " \u00b7 "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v9, v2, v10

    aput-object v1, v2, v11

    invoke-static {v0, v2}, Lgab;->a(Ljava/lang/String;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v9, v1

    .line 200
    goto :goto_0
.end method

.method private b(Ljal;)Lani;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 126
    new-instance v0, Lfsa;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    const v1, 0x7f0201b8

    invoke-virtual {v0, v1}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    iget-object v1, p1, Ljal;->dWL:Ljak;

    iget-object v1, v1, Ljak;->aeB:Ljbp;

    const/4 v2, 0x0

    invoke-static {v1, v3, v3, v2, v3}, Lgaz;->b(Ljbp;Lgba;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    .line 129
    return-object v0
.end method

.method private static c(Ljal;)Laor;
    .locals 2

    .prologue
    .line 152
    new-instance v0, Laor;

    invoke-direct {v0}, Laor;-><init>()V

    .line 153
    iput-object p0, v0, Laor;->ajZ:Ljal;

    .line 154
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laor;->bf(Z)Laor;

    .line 155
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laor;->bh(Z)Laor;

    .line 156
    const v1, 0x7f0201b7

    invoke-virtual {v0, v1}, Laor;->cI(I)Laor;

    .line 157
    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 2

    .prologue
    .line 61
    invoke-super {p0, p1, p2}, Lfus;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 63
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lang;->aS(Z)Lang;

    .line 64
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lizj;)Lanh;
    .locals 6

    .prologue
    .line 69
    invoke-direct {p0, p2}, Lfxq;->E(Lizj;)Ljal;

    move-result-object v0

    .line 70
    invoke-direct {p0, v0}, Lfxq;->b(Ljal;)Lani;

    move-result-object v1

    .line 72
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 73
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lanh;->cm(I)Lanh;

    .line 75
    invoke-static {p2}, Lfxq;->F(Lizj;)Ljhm;

    move-result-object v3

    .line 77
    new-instance v4, Lapr;

    invoke-direct {v4}, Lapr;-><init>()V

    .line 78
    iput-object v4, v2, Lanh;->agJ:Lapr;

    .line 79
    iget-object v5, p0, Lfuz;->mEntry:Lizj;

    iput-object v5, v2, Lanh;->ahu:Lizj;

    .line 80
    iput-object v1, v2, Lanh;->ahs:Lani;

    .line 81
    iget-object v1, v3, Ljhm;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lapr;->dA(Ljava/lang/String;)Lapr;

    .line 82
    iget-object v1, v3, Ljhm;->aeB:Ljbp;

    invoke-virtual {v1}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lapr;->dB(Ljava/lang/String;)Lapr;

    .line 83
    invoke-virtual {p0}, Lfxq;->aDy()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-direct {p0, p1, v1, v3}, Lfxq;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljhm;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 84
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 85
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lapr;->dC(Ljava/lang/String;)Lapr;

    .line 88
    :cond_0
    iget-object v1, v2, Lanh;->agJ:Lapr;

    invoke-static {v0}, Lfxq;->c(Ljal;)Laor;

    move-result-object v0

    iput-object v0, v1, Lapr;->agx:Laor;

    .line 90
    return-object v2
.end method

.method public final b(Landroid/content/Context;Ljava/util/List;)Lanh;
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Landroid/content/Context;Lizj;)[Lanh;
    .locals 12

    .prologue
    const/16 v11, 0xcd

    const/4 v10, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 103
    iget-object v3, p0, Lfuz;->mEntryTreeNode:Lizq;

    .line 104
    iget-object v4, v3, Lizq;->dUX:[Lizj;

    array-length v4, v4

    if-ne v4, v1, :cond_0

    iget-object v3, v3, Lizq;->dUX:[Lizj;

    aget-object v3, v3, v2

    iget-object v3, v3, Lizj;->dTN:Ljio;

    invoke-virtual {v3}, Ljio;->bnj()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v3, v1

    .line 106
    :goto_0
    if-eqz v3, :cond_2

    .line 121
    :goto_1
    return-object v0

    :cond_1
    move v3, v2

    .line 104
    goto :goto_0

    .line 108
    :cond_2
    invoke-direct {p0, p2}, Lfxq;->E(Lizj;)Ljal;

    move-result-object v3

    .line 109
    invoke-direct {p0, v3}, Lfxq;->b(Ljal;)Lani;

    move-result-object v4

    .line 111
    invoke-static {v10}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 113
    new-instance v6, Lanh;

    invoke-direct {v6}, Lanh;-><init>()V

    invoke-virtual {v6, v1}, Lanh;->cm(I)Lanh;

    iput-object v4, v6, Lanh;->ahs:Lani;

    iget-object v7, p0, Lfuz;->mEntry:Lizj;

    iput-object v7, v6, Lanh;->ahu:Lizj;

    invoke-static {v3}, Lfxq;->c(Ljal;)Laor;

    move-result-object v3

    iput-object v3, v6, Lanh;->agx:Laor;

    .line 114
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    new-instance v3, Lanh;

    invoke-direct {v3}, Lanh;-><init>()V

    invoke-virtual {v3, v2}, Lanh;->cm(I)Lanh;

    invoke-static {p2}, Lfxq;->F(Lizj;)Ljhm;

    move-result-object v6

    iget-object v7, p0, Lfxq;->mStringEvaluator:Lgbr;

    iget-object v8, p0, Lfuz;->mEntry:Lizj;

    iget-object v8, v8, Lizj;->dTO:Ljau;

    iget-object v8, v8, Ljau;->dQX:Ljhe;

    invoke-virtual {v7, p1, v8}, Lgbr;->b(Landroid/content/Context;Ljhe;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lany;

    invoke-direct {v8}, Lany;-><init>()V

    iput-object v8, v3, Lanh;->agw:Lany;

    iget-object v9, p0, Lfuz;->mEntry:Lizj;

    iput-object v9, v3, Lanh;->ahu:Lizj;

    iput-object v4, v3, Lanh;->ahs:Lani;

    invoke-virtual {v8, v7}, Lany;->bh(Ljava/lang/String;)Lany;

    iget-object v4, v6, Ljhm;->aeB:Ljbp;

    invoke-virtual {v4}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v8, v4}, Lany;->bi(Ljava/lang/String;)Lany;

    :cond_3
    invoke-virtual {p0}, Lfxq;->aDy()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v4

    invoke-direct {p0, p1, v4, v6}, Lfxq;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Ljhm;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Lany;->bk(Ljava/lang/String;)Lany;

    .line 117
    :cond_4
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    iget-object v4, p2, Lizj;->dTN:Ljio;

    iget-object v3, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v3, v3, Lizq;->dUZ:Lizj;

    new-array v6, v2, [I

    invoke-static {v3, v11, v6}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v6

    iget-object v3, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v3, v3, Lizq;->dUZ:Lizj;

    const/16 v7, 0xd1

    new-array v8, v2, [I

    invoke-static {v3, v7, v8}, Lgbm;->a(Lizj;I[I)Liwk;

    move-result-object v7

    if-eqz v6, :cond_9

    invoke-virtual {v6}, Liwk;->aZp()Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v1

    :goto_2
    if-eqz v7, :cond_a

    invoke-virtual {v7}, Liwk;->aZp()Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-virtual {v4}, Ljio;->bnl()Z

    move-result v8

    if-eqz v8, :cond_a

    :goto_3
    if-nez v3, :cond_5

    if-eqz v1, :cond_8

    :cond_5
    new-instance v2, Laoj;

    invoke-direct {v2}, Laoj;-><init>()V

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lfxq;->aDy()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v1

    invoke-static {v1}, Lfpg;->i(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lfpg;

    move-result-object v1

    invoke-virtual {v4}, Ljio;->bnk()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lfpg;->lW(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v7}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Laoj;->bT(Ljava/lang/String;)Laoj;

    new-instance v4, Lfsa;

    invoke-virtual {v7}, Liwk;->getType()I

    move-result v7

    invoke-direct {v4, v7}, Lfsa;-><init>(I)V

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1, v0}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v2, Laoj;->ajx:Lani;

    :cond_6
    new-instance v1, Lanh;

    invoke-direct {v1}, Lanh;-><init>()V

    invoke-virtual {v1, v10}, Lanh;->cm(I)Lanh;

    iput-object v2, v1, Lanh;->agz:Laoj;

    iput-object p2, v1, Lanh;->ahu:Lizj;

    if-eqz v3, :cond_7

    invoke-virtual {v6}, Liwk;->aZo()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laoj;->bS(Ljava/lang/String;)Laoj;

    new-instance v2, Lfsa;

    invoke-direct {v2, v11}, Lfsa;-><init>(I)V

    const v3, 0x7f0200ed

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    iget-object v3, p0, Lfuz;->mEntryTreeNode:Lizq;

    invoke-virtual {v2, v3, v6, v0}, Lfsa;->a(Lizq;Liwk;Ljei;)Lani;

    move-result-object v0

    iput-object v0, v1, Lanh;->ahs:Lani;

    :cond_7
    move-object v0, v1

    :cond_8
    invoke-static {v5, v0}, Lfxq;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 121
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    goto/16 :goto_1

    :cond_9
    move v3, v2

    .line 119
    goto :goto_2

    :cond_a
    move v1, v2

    goto :goto_3
.end method
