.class public final Lhlr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final ckD:Leuc;

.field private final mGsaConfigFlags:Lchk;

.field private final mHttpHelper:Ldkx;

.field private final mLoginHelper:Lcrh;

.field private final mSearchUrlHelper:Lcpn;

.field private final mTaskRunner:Lerk;


# direct methods
.method public constructor <init>(Ldkx;Lcpn;Lerk;Leuc;Lcrh;Lchk;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p3, p0, Lhlr;->mTaskRunner:Lerk;

    .line 79
    iput-object p1, p0, Lhlr;->mHttpHelper:Ldkx;

    .line 80
    iput-object p2, p0, Lhlr;->mSearchUrlHelper:Lcpn;

    .line 81
    iput-object p4, p0, Lhlr;->ckD:Leuc;

    .line 82
    iput-object p5, p0, Lhlr;->mLoginHelper:Lcrh;

    .line 83
    iput-object p6, p0, Lhlr;->mGsaConfigFlags:Lchk;

    .line 84
    return-void
.end method

.method private d(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 225
    iget-object v0, p0, Lhlr;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GM()Ljava/lang/String;

    move-result-object v7

    .line 226
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v6, v1

    .line 227
    :goto_0
    if-eqz v6, :cond_5

    const/4 v0, 0x3

    :goto_1
    move v5, v2

    move-object v2, v4

    .line 229
    :goto_2
    if-ge v5, v0, :cond_7

    .line 230
    if-ne v5, v1, :cond_0

    .line 232
    const v3, 0x8ef661

    invoke-static {v3}, Lhwt;->lx(I)V

    .line 234
    :cond_0
    if-eqz v6, :cond_2

    .line 236
    if-eqz v2, :cond_1

    .line 237
    iget-object v3, p0, Lhlr;->mLoginHelper:Lcrh;

    invoke-virtual {v3, v2}, Lcrh;->iC(Ljava/lang/String;)V

    .line 239
    :cond_1
    iget-object v2, p0, Lhlr;->mLoginHelper:Lcrh;

    invoke-virtual {v2, v7, v10, v11}, Lcrh;->k(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    .line 240
    if-eqz v2, :cond_2

    .line 241
    const-string v3, "Authorization"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Bearer "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    :cond_2
    invoke-virtual {p0, p1, p2}, Lhlr;->e(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v3

    .line 245
    if-eqz v3, :cond_6

    move-object v0, v3

    .line 285
    :cond_3
    :goto_3
    return-object v0

    :cond_4
    move v6, v2

    .line 226
    goto :goto_0

    :cond_5
    move v0, v1

    .line 227
    goto :goto_1

    .line 229
    :cond_6
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    .line 252
    :cond_7
    if-nez v6, :cond_9

    .line 253
    const v0, 0x8556f5

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 254
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 257
    const-string v1, "client"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_8

    .line 258
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "client"

    const-string v2, "mobile-legacy"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    .line 266
    :cond_8
    const-string v0, "User-Agent"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lhlr;->mLoginHelper:Lcrh;

    const-string v2, "mobilepersonalfeeds"

    invoke-virtual {v1, v2, v10, v11}, Lcrh;->k(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_a

    const-string v0, "HttpActionExecutor"

    const-string v1, "#getFallbackHeaders: Failed to get auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v4

    .line 269
    :goto_4
    if-eqz v0, :cond_9

    .line 270
    invoke-virtual {p0, p1, v0}, Lhlr;->e(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 271
    if-nez v0, :cond_3

    .line 274
    const v0, 0x861cb4

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 278
    invoke-static {p1}, Lhlr;->of(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_9

    .line 280
    const-string v1, "HttpActionExecutor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HttpActionExecutor failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    move-object v0, v4

    .line 285
    goto :goto_3

    .line 266
    :cond_a
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "User-Agent"

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    const-string v0, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "GoogleLogin auth="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    goto :goto_4
.end method

.method public static oe(Ljava/lang/String;)Lhlt;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 150
    .line 151
    if-nez p0, :cond_0

    .line 152
    const-string v0, "HttpActionExecutor"

    const-string v2, "No response."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 161
    :goto_0
    if-nez v0, :cond_1

    .line 162
    new-instance v0, Lhlt;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lhlt;-><init>(ZLjava/lang/String;)V

    .line 166
    :goto_1
    return-object v0

    .line 155
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 158
    :catch_0
    move-exception v0

    const-string v0, "HttpActionExecutor"

    const-string v2, "Failed to parse response!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    .line 165
    :cond_1
    const-string v1, "vInfo"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 166
    new-instance v0, Lhlt;

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Lhlt;-><init>(ZLjava/lang/String;)V

    goto :goto_1
.end method

.method public static of(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 215
    const-string v0, "sig=[^&]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 216
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 220
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljkt;Ljava/lang/String;Lefk;)V
    .locals 8
    .param p1    # Ljkt;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 92
    new-instance v0, Lhls;

    const-string v2, "HttpActionExecutor"

    iget-object v3, p0, Lhlr;->mTaskRunner:Lerk;

    const/4 v1, 0x2

    new-array v4, v1, [I

    fill-array-data v4, :array_0

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lhls;-><init>(Lhlr;Ljava/lang/String;Lerk;[ILjkt;Ljava/lang/String;Lefk;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhls;->a([Ljava/lang/Object;)Lenp;

    .line 103
    return-void

    .line 92
    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public final a(Ljkt;Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljkt;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 109
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    invoke-virtual {p0}, Lhlr;->aQG()Ljava/lang/String;

    move-result-object p2

    .line 112
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    const-string v1, "HttpActionExecutor"

    const-string v2, "#executeActionSync: failed to retrieve confirmation URL"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_0
    :goto_0
    return v0

    .line 119
    :cond_1
    iget-object v1, p0, Lhlr;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v1, v5}, Lcpn;->cB(Z)Lcpo;

    move-result-object v1

    invoke-virtual {v1}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v1

    .line 122
    invoke-static {v5}, Lior;->mk(I)Ljava/util/HashMap;

    move-result-object v2

    .line 123
    const-string v3, "pinfo"

    invoke-static {p1}, Leqh;->c(Ljsr;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    invoke-virtual {v1}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "pinfo"

    aput-object v6, v5, v0

    invoke-static {v5}, Liqs;->m([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v5

    invoke-static {v3, v4, v5, v2}, Lcpn;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/util/Set;Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-virtual {v1}, Lcom/google/android/search/shared/api/UriRequest;->alI()Ljava/util/Map;

    move-result-object v1

    invoke-direct {p0, v2, v1}, Lhlr;->d(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lhlr;->oe(Ljava/lang/String;)Lhlt;

    move-result-object v1

    .line 134
    if-eqz v1, :cond_0

    .line 138
    iget-object v0, v1, Lhlt;->dkB:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 141
    iget-object v0, p0, Lhlr;->ckD:Leuc;

    iget-object v2, v1, Lhlt;->dkB:Ljava/lang/String;

    invoke-virtual {v0, v2}, Leuc;->lx(Ljava/lang/String;)V

    .line 144
    :cond_2
    iget-boolean v0, v1, Lhlt;->dkA:Z

    goto :goto_0
.end method

.method public final aQG()Ljava/lang/String;
    .locals 8
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lhlr;->mSearchUrlHelper:Lcpn;

    invoke-virtual {v0, v6}, Lcpn;->cB(Z)Lcpo;

    move-result-object v0

    invoke-virtual {v0}, Lcpo;->RH()Lcom/google/android/search/shared/api/UriRequest;

    move-result-object v0

    .line 178
    const-string v3, "ctzn"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lijm;->s(Ljava/lang/Object;Ljava/lang/Object;)Lijm;

    move-result-object v3

    .line 180
    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->getUri()Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lhlr;->mGsaConfigFlags:Lchk;

    invoke-virtual {v5}, Lchk;->GN()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/String;

    const-string v7, "pinfo"

    aput-object v7, v6, v2

    invoke-static {v6}, Liqs;->m([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    invoke-static {v4, v5, v6, v3}, Lcpn;->a(Landroid/net/Uri;Landroid/net/Uri;Ljava/util/Set;Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 186
    invoke-virtual {v0}, Lcom/google/android/search/shared/api/UriRequest;->alI()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lhlr;->d(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 187
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 188
    const-string v0, "HttpActionExecutor"

    const-string v2, "Failed to retrieve fetch confirmation URL"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 210
    :goto_0
    return-object v0

    .line 194
    :cond_0
    const/16 v3, 0x8

    :try_start_0
    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 195
    new-instance v3, Ljrp;

    invoke-direct {v3}, Ljrp;-><init>()V

    invoke-static {v3, v0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Ljrp;

    .line 196
    iget-object v3, v0, Ljrp;->eBt:[Ljkt;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 197
    sget-object v5, Ljkw;->eqw:Ljsm;

    invoke-virtual {v0, v5}, Ljkt;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljkw;

    .line 199
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljkw;->boC()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 200
    invoke-virtual {v0}, Ljkw;->aiw()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 196
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 203
    :cond_2
    const-string v0, "HttpActionExecutor"

    const-string v2, "Fetch confirmation URL response did not contain confirmation URL"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    move-object v0, v1

    .line 210
    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    const-string v0, "HttpActionExecutor"

    const-string v2, "Failed to decode fetch confirmation URL response"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 207
    :catch_1
    move-exception v0

    const-string v0, "HttpActionExecutor"

    const-string v2, "Failed to parse fetch confirmation URL peanut"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public final e(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 293
    :try_start_0
    new-instance v0, Ldlb;

    invoke-direct {v0, p1, p2}, Ldlb;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 294
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldlb;->setUseCaches(Z)V

    .line 295
    iget-object v1, p0, Lhlr;->mHttpHelper:Ldkx;

    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, Ldkx;->b(Ldlb;I)Ljava/lang/String;
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 304
    :goto_0
    return-object v0

    .line 299
    :catch_0
    move-exception v0

    .line 300
    const-string v1, "HttpActionExecutor"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HTTP request failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Left;->getErrorCode()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 301
    :catch_1
    move-exception v0

    .line 302
    const-string v1, "HttpActionExecutor"

    const-string v2, "HTTP request failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
