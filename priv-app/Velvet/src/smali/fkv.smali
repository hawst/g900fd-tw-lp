.class public abstract Lfkv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfmt;


# instance fields
.field private cT:I

.field private final cuk:Lern;

.field private final cul:Lfkx;

.field protected final mAppContext:Landroid/content/Context;

.field private final mClock:Lemp;

.field protected final mNowRemoteClient:Lfml;

.field private volatile mRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

.field private mUndoDismissManager:Lfnn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfml;Lfnn;Lern;Lemp;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x2

    iput v0, p0, Lfkv;->cT:I

    .line 37
    new-instance v0, Lfkx;

    invoke-direct {v0}, Lfkx;-><init>()V

    iput-object v0, p0, Lfkv;->cul:Lfkx;

    .line 39
    sget-object v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->cxD:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iput-object v0, p0, Lfkv;->mRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 47
    iput-object p1, p0, Lfkv;->mAppContext:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lfkv;->mNowRemoteClient:Lfml;

    .line 49
    iput-object p3, p0, Lfkv;->mUndoDismissManager:Lfnn;

    .line 50
    iput-object p4, p0, Lfkv;->cuk:Lern;

    .line 51
    iput-object p5, p0, Lfkv;->mClock:Lemp;

    .line 52
    return-void
.end method


# virtual methods
.method public final W(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 99
    invoke-static {p1, p2}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->aJ(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lfkv;->mNowRemoteClient:Lfml;

    invoke-virtual {v1, v0}, Lfml;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    .line 101
    return-void
.end method

.method public a(Landroid/content/Context;Lfzw;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 161
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p2, p1, v0, p4, v1}, Lfzw;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Z)Z

    .line 162
    return-void
.end method

.method public final varargs a(Landroid/view/View;[Lizj;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lfkv;->mClock:Lemp;

    invoke-static {p1, p0, v0, p2}, Lfln;->a(Landroid/view/View;Lfmt;Lemp;[Lizj;)V

    .line 146
    return-void
.end method

.method public a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V
    .locals 1

    .prologue
    .line 129
    invoke-static {}, Lenu;->auR()V

    .line 130
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    iput-object v0, p0, Lfkv;->mRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 131
    return-void
.end method

.method public final a(Lejw;Lizj;)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lfkv;->mUndoDismissManager:Lfnn;

    invoke-virtual {v0, p1, p2}, Lfnn;->a(Lejw;Lizj;)V

    .line 141
    return-void
.end method

.method public a(Lizj;I)V
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lfkv;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lfor;->b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making record feedback prompt request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Lizj;ILixx;Ljava/lang/Integer;)V
    .locals 2
    .param p3    # Lixx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 92
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->b(Lizj;ILixx;Ljava/lang/Integer;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lfkv;->mNowRemoteClient:Lfml;

    invoke-virtual {v1, v0}, Lfml;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    .line 95
    return-void
.end method

.method public a(Lizj;Lizj;)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lfkv;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lfor;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making dismiss child entry request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Lizj;Z)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lfkv;->mNowRemoteClient:Lfml;

    invoke-virtual {v0, p1, p2}, Lfml;->a(Lizj;Z)V

    .line 67
    return-void
.end method

.method public final aAD()Lfml;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lfkv;->mNowRemoteClient:Lfml;

    return-object v0
.end method

.method public final aAE()Landroid/database/Observable;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lfkv;->cul:Lfkx;

    return-object v0
.end method

.method public final c(Lizj;Z)V
    .locals 7

    .prologue
    .line 115
    iget-object v6, p0, Lfkv;->cuk:Lern;

    new-instance v0, Lfkw;

    const-string v2, "freshenEntries"

    const/4 v1, 0x2

    new-array v3, v1, [I

    fill-array-data v3, :array_0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lfkw;-><init>(Lfkv;Ljava/lang/String;[ILizj;Z)V

    invoke-interface {v6, v0}, Lern;->a(Leri;)V

    .line 124
    return-void

    .line 115
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public dt(I)V
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lfkv;->mNowRemoteClient:Lfml;

    const/16 v1, 0x24

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, v1}, Lfor;->iI(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making refreshEntries request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final getVisibility()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lfkv;->cT:I

    return v0
.end method

.method public final setVisibility(I)V
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lfkv;->cT:I

    if-eq v0, p1, :cond_0

    .line 169
    iput p1, p0, Lfkv;->cT:I

    .line 170
    iget-object v0, p0, Lfkv;->cul:Lfkx;

    invoke-virtual {v0, p1}, Lfkx;->iR(I)V

    .line 172
    :cond_0
    return-void
.end method

.method public final t(Lizj;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lfkv;->a(Lizj;Z)V

    .line 62
    return-void
.end method

.method public final u(Lizj;)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lfkv;->mNowRemoteClient:Lfml;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lfml;->d(Lizj;Z)V

    .line 77
    return-void
.end method

.method public final v(Lizj;)V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lfkv;->mNowRemoteClient:Lfml;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lfml;->d(Lizj;Z)V

    .line 82
    return-void
.end method

.method public ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lfkv;->mRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    return-object v0
.end method

.method public wt()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lfkv;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->wt()V

    .line 106
    return-void
.end method
