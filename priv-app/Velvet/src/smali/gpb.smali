.class public final Lgpb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 19

    .prologue
    .line 182
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    const-class v2, Ljrp;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/velvet/ActionData;->c(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v7

    check-cast v7, Ljrp;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    const-class v2, Lcom/google/android/speech/embedded/TaggerResult;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/speech/embedded/TaggerResult;

    const-class v2, Lieb;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/velvet/ActionData;->c(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v11

    check-cast v11, Lieb;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v12

    check-cast v12, Leiq;

    const-class v2, Ljyw;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/velvet/ActionData;->c(Landroid/os/Parcel;Ljava/lang/Class;)Ljsr;

    move-result-object v13

    check-cast v13, Ljyw;

    invoke-static {}, Lhha;->values()[Lhha;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v14, v2, v3

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v18

    if-nez v7, :cond_0

    if-nez v8, :cond_0

    if-nez v9, :cond_0

    if-nez v10, :cond_0

    if-nez v11, :cond_0

    if-nez v12, :cond_0

    sget-object v3, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    :goto_0
    return-object v3

    :cond_0
    new-instance v3, Lcom/google/android/velvet/ActionData;

    const/4 v6, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v3 .. v18}, Lcom/google/android/velvet/ActionData;-><init>(JZLjrp;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/speech/embedded/TaggerResult;Lieb;Leiq;Ljyw;Lhha;Ljava/lang/String;ZII)V

    goto :goto_0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 182
    new-array v0, p1, [Lcom/google/android/velvet/ActionData;

    return-object v0
.end method
