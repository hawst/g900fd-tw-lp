.class final Lexy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final clA:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 271
    const/4 v0, 0x3

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    sput-object v0, Lexy;->clA:[[I

    return-void

    :array_0
    .array-data 4
        0x7f040152
        0x7f1103a7
    .end array-data

    :array_1
    .array-data 4
        0x7f040154
        0x7f1103aa
    .end array-data

    :array_2
    .array-data 4
        0x1090009
        0x0
    .end array-data
.end method

.method static a(ILjava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 281
    sget-object v1, Lexy;->clA:[[I

    aget-object v1, v1, p0

    aget v1, v1, v4

    .line 282
    sget-object v2, Lexy;->clA:[[I

    aget-object v2, v2, p0

    const/4 v3, 0x1

    aget v3, v2, v3

    .line 286
    if-nez p2, :cond_1

    .line 287
    invoke-virtual {p4, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 292
    :goto_0
    if-eqz p1, :cond_0

    .line 294
    if-nez v3, :cond_2

    .line 295
    :try_start_0
    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    move-object v1, v0

    .line 299
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    :cond_0
    return-object v2

    :cond_1
    move-object v2, p2

    .line 289
    goto :goto_0

    .line 297
    :cond_2
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 300
    :catch_0
    move-exception v1

    .line 301
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Expected TextView"

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method static b(Ljcu;)I
    .locals 1

    .prologue
    .line 310
    if-eqz p0, :cond_1

    .line 311
    iget-object v0, p0, Ljcu;->dQU:Liym;

    if-eqz v0, :cond_0

    .line 312
    const/4 v0, 0x0

    .line 319
    :goto_0
    return v0

    .line 313
    :cond_0
    iget-object v0, p0, Ljcu;->dME:Lixn;

    if-eqz v0, :cond_1

    .line 314
    const/4 v0, 0x1

    goto :goto_0

    .line 319
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
