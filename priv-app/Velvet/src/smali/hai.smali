.class public final Lhai;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final day:Ldef;


# instance fields
.field bTh:Lcom/google/android/search/suggest/SuggestionListView;

.field bpl:Ldex;

.field cZo:Landroid/view/View;

.field private daA:I

.field daB:Z

.field daC:Z

.field daD:Z

.field daE:Z

.field daF:Lham;

.field daG:Lcom/google/android/search/suggest/SuggestionListView;

.field private daH:Lekn;

.field final daz:Lgxi;

.field private final mConfig:Lcjs;

.field final mSearchBoxLogging:Lcpd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    new-instance v0, Ldei;

    const-string v1, ""

    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    invoke-direct {v0, v1, v2}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;)V

    sput-object v0, Lhai;->day:Ldef;

    return-void
.end method

.method public constructor <init>(Lcpd;Lcjs;Lgxi;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lhai;->mSearchBoxLogging:Lcpd;

    .line 76
    iput-object p2, p0, Lhai;->mConfig:Lcjs;

    .line 77
    iput-object p3, p0, Lhai;->daz:Lgxi;

    .line 78
    return-void
.end method

.method static synthetic a(Lhai;)Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lhai;->daz:Lgxi;

    invoke-virtual {v0}, Lgxi;->aAU()Z

    move-result v0

    return v0
.end method

.method static synthetic aMP()Ldef;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lhai;->day:Ldef;

    return-object v0
.end method

.method static synthetic b(Lhai;)Lgyz;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lhai;->daz:Lgxi;

    invoke-virtual {v0}, Lgxi;->aLH()Lgyz;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lhai;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lhai;->daA:I

    return v0
.end method

.method private gh(Z)V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lhai;->daz:Lgxi;

    invoke-virtual {v0}, Lgxi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 169
    iget-boolean v0, p0, Lhai;->daB:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhai;->daC:Z

    if-eqz v0, :cond_1

    .line 170
    :cond_0
    iget-boolean v0, p0, Lhai;->daD:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhai;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhai;->daz:Lgxi;

    iget-object v1, p0, Lhai;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0, v1}, Lgxi;->aP(Landroid/view/View;)V

    .line 174
    :cond_1
    :goto_0
    iget-object v0, p0, Lhai;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Lhai;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    invoke-virtual {v0}, Lcom/google/android/search/suggest/SuggestionListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lekm;

    .line 176
    iget-object v1, p0, Lhai;->daH:Lekn;

    iput-object v1, v0, Lekm;->cdA:Lekn;

    .line 179
    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lhai;->bpl:Ldex;

    if-eqz v0, :cond_3

    .line 180
    iget-object v0, p0, Lhai;->daz:Lgxi;

    invoke-virtual {v0}, Lgxi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->UG()Ldep;

    move-result-object v0

    iget-object v1, p0, Lhai;->bpl:Ldex;

    invoke-virtual {v0, v1}, Ldep;->c(Ldex;)V

    .line 183
    :cond_3
    iget-object v0, p0, Lhai;->daF:Lham;

    invoke-virtual {v0}, Lham;->aMQ()V

    .line 185
    :cond_4
    return-void

    .line 170
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhai;->daD:Z

    iget-object v0, p0, Lhai;->daz:Lgxi;

    new-instance v1, Lhak;

    invoke-direct {v1, p0}, Lhak;-><init>(Lhai;)V

    invoke-virtual {v0, v1}, Lgxi;->a(Lesj;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lgyv;Lgyt;ZZ)V
    .locals 4

    .prologue
    .line 85
    invoke-virtual {p1, p2}, Lgyv;->a(Lgyt;)Z

    move-result v2

    .line 86
    invoke-virtual {p1, p2, p3}, Lgyv;->b(Lgyt;Z)Z

    move-result v3

    .line 89
    invoke-virtual {p2}, Lgyt;->aLI()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lekn;->cdL:Lekn;

    :goto_0
    iput-object v0, p0, Lhai;->daH:Lekn;

    .line 91
    iget-boolean v0, p0, Lhai;->daE:Z

    if-ne p4, v0, :cond_0

    iget-boolean v0, p0, Lhai;->daB:Z

    if-ne v2, v0, :cond_0

    iget-boolean v0, p0, Lhai;->daC:Z

    if-eq v3, v0, :cond_2

    .line 94
    :cond_0
    sget-object v0, Lhaj;->cZO:[I

    invoke-virtual {p2}, Lgyt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const v0, 0x7fffffff

    .line 95
    :goto_1
    iget-boolean v1, p0, Lhai;->daB:Z

    if-ne v2, v1, :cond_1

    iget-boolean v1, p0, Lhai;->daC:Z

    if-ne v3, v1, :cond_1

    iget v1, p0, Lhai;->daA:I

    if-eq v1, v0, :cond_4

    :cond_1
    const/4 v1, 0x1

    .line 97
    :goto_2
    iput-boolean p4, p0, Lhai;->daE:Z

    .line 98
    iput-boolean v2, p0, Lhai;->daB:Z

    .line 99
    iput-boolean v3, p0, Lhai;->daC:Z

    .line 100
    iput v0, p0, Lhai;->daA:I

    .line 101
    iget-object v0, p0, Lhai;->daF:Lham;

    iget-boolean v2, v0, Lham;->daP:Z

    iget-object v3, v0, Lham;->daJ:Lhai;

    iget-boolean v3, v3, Lhai;->daC:Z

    and-int/2addr v2, v3

    iput-boolean v2, v0, Lham;->daP:Z

    .line 102
    invoke-direct {p0, v1}, Lhai;->gh(Z)V

    .line 104
    :cond_2
    return-void

    .line 89
    :cond_3
    sget-object v0, Lekn;->cdR:Lekn;

    goto :goto_0

    .line 94
    :pswitch_0
    iget-object v0, p0, Lhai;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Lh()I

    move-result v0

    goto :goto_1

    .line 95
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 94
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final aKF()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120
    iget-object v0, p0, Lhai;->daz:Lgxi;

    invoke-virtual {v0}, Lgxi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0, v2}, Lgyz;->c(Lcom/google/android/search/suggest/SuggestionListView;)V

    .line 121
    iget-object v0, p0, Lhai;->daz:Lgxi;

    invoke-virtual {v0}, Lgxi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->UG()Ldep;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lhai;->daF:Lham;

    invoke-virtual {v0, v1}, Ldep;->b(Lder;)V

    .line 123
    iget-object v1, p0, Lhai;->bpl:Ldex;

    invoke-virtual {v0, v1}, Ldep;->b(Ldex;)V

    .line 124
    iput-object v2, p0, Lhai;->bpl:Ldex;

    .line 125
    iput-object v2, p0, Lhai;->daF:Lham;

    .line 126
    iput-object v2, p0, Lhai;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    .line 127
    iput-object v2, p0, Lhai;->daG:Lcom/google/android/search/suggest/SuggestionListView;

    .line 128
    iput-object v2, p0, Lhai;->cZo:Landroid/view/View;

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhai;->daD:Z

    .line 130
    const v0, 0x7fffffff

    iput v0, p0, Lhai;->daA:I

    .line 131
    return-void
.end method

.method public final aMO()V
    .locals 2

    .prologue
    .line 110
    new-instance v0, Lham;

    invoke-direct {v0, p0}, Lham;-><init>(Lhai;)V

    iput-object v0, p0, Lhai;->daF:Lham;

    .line 111
    iget-object v0, p0, Lhai;->daz:Lgxi;

    invoke-virtual {v0}, Lgxi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->UG()Ldep;

    move-result-object v0

    .line 112
    iget-object v1, p0, Lhai;->daF:Lham;

    invoke-virtual {v0, v1}, Ldep;->a(Lder;)V

    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhai;->gh(Z)V

    .line 114
    return-void
.end method

.method public final bG(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lhai;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    if-ne p1, v0, :cond_0

    .line 138
    iget-object v0, p0, Lhai;->daz:Lgxi;

    invoke-virtual {v0}, Lgxi;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->aMb()V

    .line 140
    :cond_0
    return-void
.end method

.method public final bH(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lhai;->bTh:Lcom/google/android/search/suggest/SuggestionListView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lhai;->daG:Lcom/google/android/search/suggest/SuggestionListView;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lhai;->cZo:Landroid/view/View;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
