.class public final Ljqr;
.super Ljsl;
.source "PG"


# static fields
.field public static final ezl:Ljsm;


# instance fields
.field private aez:I

.field private euN:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 5881
    const/16 v0, 0xb

    const-class v1, Ljqr;

    const v2, 0x206db76a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqr;->ezl:Ljsm;

    .line 5887
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5913
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 5914
    iput v0, p0, Ljqr;->aez:I

    iput v0, p0, Ljqr;->euN:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljqr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqr;->eCz:I

    .line 5915
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5874
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqr;->euN:I

    iget v0, p0, Ljqr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqr;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 5928
    iget v0, p0, Ljqr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 5929
    const/4 v0, 0x1

    iget v1, p0, Ljqr;->euN:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 5931
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 5932
    return-void
.end method

.method public final bqC()I
    .locals 1

    .prologue
    .line 5897
    iget v0, p0, Ljqr;->euN:I

    return v0
.end method

.method public final bqD()Z
    .locals 1

    .prologue
    .line 5905
    iget v0, p0, Ljqr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 5936
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 5937
    iget v1, p0, Ljqr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 5938
    const/4 v1, 0x1

    iget v2, p0, Ljqr;->euN:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5941
    :cond_0
    return v0
.end method
