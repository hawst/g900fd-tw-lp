.class public final Lgqo;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static declared-synchronized a(Landroid/content/Context;Lcke;Lcjs;Lgpp;)V
    .locals 12

    .prologue
    .line 26
    const-class v2, Lgqo;

    monitor-enter v2

    :try_start_0
    invoke-interface {p1}, Lcke;->Oh()I

    move-result v3

    .line 27
    invoke-static {}, Lcom/google/android/velvet/VelvetApplication;->xT()I

    move-result v4

    .line 30
    invoke-interface {p1}, Lcke;->Oi()Ljava/lang/String;

    move-result-object v5

    .line 31
    sget-object v6, Landroid/os/Build;->ID:Ljava/lang/String;

    .line 34
    const/4 v0, -0x1

    if-ne v3, v0, :cond_0

    .line 37
    const-string v0, "update_gservices_config"

    invoke-interface {p3, v0}, Lgpp;->nw(Ljava/lang/String;)V

    .line 41
    :cond_0
    if-eq v3, v4, :cond_7

    const/4 v0, 0x1

    move v1, v0

    .line 42
    :goto_0
    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    .line 44
    :goto_1
    if-eqz v1, :cond_4

    .line 45
    const-string v7, "Velvet.VelvetUpgradeTasks"

    const-string v8, "Velvet upgraded from %d to %d: running upgrade tasks."

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 48
    const-string v7, "migrate_relationships"

    const-wide/16 v8, 0x0

    invoke-interface {p3, v7, v8, v9}, Lgpp;->r(Ljava/lang/String;J)V

    .line 50
    invoke-virtual {p2}, Lcjs;->MM()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 51
    invoke-static {p0}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->al(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 56
    :cond_1
    invoke-static {}, Lckw;->OZ()Lckw;

    move-result-object v7

    .line 57
    invoke-virtual {v7}, Lckw;->Pd()Z

    move-result v8

    if-nez v8, :cond_2

    .line 58
    invoke-virtual {v7, p1}, Lckw;->a(Lcke;)V

    .line 60
    :cond_2
    const-string v7, "send_gsa_home_request"

    const-wide/16 v8, 0x0

    invoke-interface {p3, v7, v8, v9}, Lgpp;->r(Ljava/lang/String;J)V

    .line 66
    const-string v7, "delete_local_search_history"

    const-wide/16 v8, 0x0

    invoke-interface {p3, v7, v8, v9}, Lgpp;->r(Ljava/lang/String;J)V

    .line 70
    const-string v7, "refresh_auth_tokens"

    invoke-interface {p3, v7}, Lgpp;->nw(Ljava/lang/String;)V

    .line 71
    const-string v7, "refresh_search_domain_and_cookies"

    invoke-interface {p3, v7}, Lgpp;->nw(Ljava/lang/String;)V

    .line 74
    const/4 v7, -0x1

    if-eq v3, v7, :cond_3

    const v7, 0x11e646a8

    if-ge v3, v7, :cond_3

    .line 76
    const/4 v3, 0x1

    invoke-interface {p1, v3}, Lcke;->cp(Z)V

    .line 79
    :cond_3
    const-string v3, "sync_gel_prefs"

    const-wide/16 v8, 0x0

    invoke-interface {p3, v3, v8, v9}, Lgpp;->r(Ljava/lang/String;J)V

    .line 81
    const-string v3, "silent_speakerid_enrollment"

    const-wide/16 v8, 0x0

    invoke-interface {p3, v3, v8, v9}, Lgpp;->r(Ljava/lang/String;J)V

    .line 88
    invoke-interface {p1}, Lcke;->OB()V

    .line 90
    invoke-interface {p1, v4}, Lcke;->fa(I)V

    .line 93
    :cond_4
    if-eqz v0, :cond_6

    .line 94
    const-string v0, "Velvet.VelvetUpgradeTasks"

    const-string v3, "System upgraded from %s to %s: running system upgrade tasks."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v4, v7

    const/4 v5, 0x1

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 99
    if-nez v1, :cond_5

    invoke-virtual {p2}, Lcjs;->MM()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 100
    invoke-static {p0}, Lcom/google/android/search/core/icingsync/UpdateIcingCorporaService;->ak(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 102
    :cond_5
    invoke-interface {p1, v6}, Lcke;->hb(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :cond_6
    monitor-exit v2

    return-void

    .line 41
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    .line 42
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
