.class public final Lgil;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private cLl:Ljava/util/Collection;

.field public cLm:Lgim;

.field private final cpB:Leqx;

.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mSpeechLibFactory:Lgdn;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Lgdn;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-string v0, "RecognitionDispatcher"

    sget-object v1, Lgin;->cLn:Lgin;

    invoke-static {v0, v1}, Leqx;->a(Ljava/lang/String;Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgin;->cLn:Lgin;

    new-array v2, v4, [Lgin;

    sget-object v3, Lgin;->cLo:Lgin;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    sget-object v1, Lgin;->cLo:Lgin;

    new-array v2, v4, [Lgin;

    sget-object v3, Lgin;->cLn:Lgin;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Leqy;->a(Ljava/lang/Enum;[Ljava/lang/Enum;)Leqy;

    move-result-object v0

    iput-boolean v4, v0, Leqy;->chN:Z

    iput-boolean v4, v0, Leqy;->chJ:Z

    invoke-virtual {v0}, Leqy;->avx()Leqx;

    move-result-object v0

    iput-object v0, p0, Lgil;->cpB:Leqx;

    .line 52
    iput-object p1, p0, Lgil;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 53
    iput-object p2, p0, Lgil;->mSpeechLibFactory:Lgdn;

    .line 54
    return-void
.end method

.method private stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    iget-object v0, p0, Lgil;->cpB:Leqx;

    sget-object v1, Lgin;->cLo:Lgin;

    invoke-virtual {v0, v1}, Leqx;->d(Ljava/lang/Enum;)V

    .line 116
    iget-object v0, p0, Lgil;->cpB:Leqx;

    sget-object v1, Lgin;->cLn:Lgin;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V

    .line 117
    iget-object v0, p0, Lgil;->cLm:Lgim;

    invoke-interface {v0}, Lgim;->invalidate()V

    .line 118
    iput-object v2, p0, Lgil;->cLm:Lgim;

    .line 119
    iget-object v0, p0, Lgil;->cLl:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 120
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lglc;

    invoke-interface {v0}, Lglc;->close()V

    goto :goto_0

    .line 122
    :cond_0
    iput-object v2, p0, Lgil;->cLl:Ljava/util/Collection;

    .line 123
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;Lger;Lgnj;Lgcl;Lgcd;Lglx;)V
    .locals 7

    .prologue
    .line 63
    iget-object v0, p0, Lgil;->cpB:Leqx;

    sget-object v1, Lgin;->cLo:Lgin;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "RecognitionDispatcher"

    const-string v1, "Multiple recognitions in progress, the first will be cancelled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 65
    invoke-direct {p0}, Lgil;->stop()V

    .line 67
    :cond_0
    iget-object v0, p0, Lgil;->cpB:Leqx;

    sget-object v1, Lgin;->cLo:Lgin;

    invoke-virtual {v0, v1}, Leqx;->a(Ljava/lang/Enum;)V

    .line 68
    iput-object p1, p0, Lgil;->cLl:Ljava/util/Collection;

    .line 70
    iget-object v0, p0, Lgil;->mSpeechLibFactory:Lgdn;

    iget-object v6, p0, Lgil;->mExecutor:Ljava/util/concurrent/ExecutorService;

    move-object v1, p3

    move-object v2, p0

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-interface/range {v0 .. v6}, Lgdn;->a(Lgnj;Lgil;Lgcl;Lgcd;Lglx;Ljava/util/concurrent/ExecutorService;)Lgim;

    move-result-object v0

    iput-object v0, p0, Lgil;->cLm:Lgim;

    .line 74
    iget-object v0, p0, Lgil;->mExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lgil;->cLm:Lgim;

    invoke-static {v0, v1}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggg;

    .line 75
    iget-object v1, p0, Lgil;->cLl:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 76
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lglc;

    invoke-interface {v1, p2, v0, p3}, Lglc;->a(Lger;Lggg;Lgnj;)V

    goto :goto_0

    .line 78
    :cond_1
    return-void
.end method

.method public final cancel()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lgil;->cpB:Leqx;

    sget-object v1, Lgin;->cLo:Lgin;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0}, Lgil;->stop()V

    .line 85
    :cond_0
    return-void
.end method

.method public final isRunning()Z
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lgil;->cpB:Leqx;

    sget-object v1, Lgin;->cLo:Lgin;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    return v0
.end method

.method public final km(I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 89
    iget-object v0, p0, Lgil;->cpB:Leqx;

    sget-object v1, Lgin;->cLo:Lgin;

    invoke-virtual {v0, v1}, Leqx;->b(Ljava/lang/Enum;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    iget-object v0, p0, Lgil;->cLl:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v3

    .line 93
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 95
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 96
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lglc;

    invoke-interface {v0}, Lglc;->close()V

    .line 97
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 98
    const/4 v0, 0x1

    move v2, v0

    goto :goto_0

    .line 101
    :cond_1
    if-nez v2, :cond_2

    .line 102
    const-string v0, "RecognitionDispatcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not stop engine "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 104
    :cond_2
    iget-object v0, p0, Lgil;->cLl:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 105
    invoke-direct {p0}, Lgil;->stop()V

    .line 108
    :cond_3
    return-void
.end method
