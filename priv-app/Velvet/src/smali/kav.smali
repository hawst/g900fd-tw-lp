.class public final enum Lkav;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum ePR:Lkav;

.field public static final enum ePS:Lkav;

.field public static final enum ePT:Lkav;

.field private static final synthetic ePU:[Lkav;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lkav;

    const-string v1, "CACHE"

    invoke-direct {v0, v1, v2}, Lkav;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkav;->ePR:Lkav;

    .line 29
    new-instance v0, Lkav;

    const-string v1, "CONDITIONAL_CACHE"

    invoke-direct {v0, v1, v3}, Lkav;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkav;->ePS:Lkav;

    .line 32
    new-instance v0, Lkav;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v4}, Lkav;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lkav;->ePT:Lkav;

    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Lkav;

    sget-object v1, Lkav;->ePR:Lkav;

    aput-object v1, v0, v2

    sget-object v1, Lkav;->ePS:Lkav;

    aput-object v1, v0, v3

    sget-object v1, Lkav;->ePT:Lkav;

    aput-object v1, v0, v4

    sput-object v0, Lkav;->ePU:[Lkav;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lkav;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lkav;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lkav;

    return-object v0
.end method

.method public static values()[Lkav;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lkav;->ePU:[Lkav;

    invoke-virtual {v0}, [Lkav;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkav;

    return-object v0
.end method


# virtual methods
.method public final bxU()Z
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lkav;->ePS:Lkav;

    if-eq p0, v0, :cond_0

    sget-object v0, Lkav;->ePT:Lkav;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
