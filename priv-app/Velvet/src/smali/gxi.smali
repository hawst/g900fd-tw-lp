.class public abstract Lgxi;
.super Lgyy;
.source "PG"


# static fields
.field static final cYt:Lesj;

.field static final cYu:Lesj;


# instance fields
.field final mView:Lcom/google/android/velvet/ui/MainContentView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lgxj;

    const-string v1, "RESET SCROLL"

    invoke-direct {v0, v1}, Lgxj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgxi;->cYt:Lesj;

    .line 49
    new-instance v0, Lgxp;

    const-string v1, "REMOVE ALL VIEWS"

    invoke-direct {v0, v1}, Lgxp;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgxi;->cYu:Lesj;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Lcom/google/android/velvet/ui/MainContentView;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lgyy;-><init>(Ljava/lang/String;)V

    .line 61
    iput-object p2, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    .line 62
    return-void
.end method


# virtual methods
.method protected final B(Landroid/view/View;I)V
    .locals 7
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 196
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    new-instance v0, Lgxt;

    const-string v2, "setVisibility"

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lgxt;-><init>(Lgxi;Ljava/lang/String;Ljava/lang/Object;ILandroid/view/View;I)V

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 203
    return-void
.end method

.method protected final a(ILjava/lang/Iterable;)V
    .locals 7
    .param p2    # Ljava/lang/Iterable;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 115
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    new-instance v0, Lgxq;

    const-string v2, "addViews"

    move-object v1, p0

    move-object v3, p2

    move v4, p1

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lgxq;-><init>(Lgxi;Ljava/lang/String;Ljava/lang/Object;IILjava/lang/Iterable;)V

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 128
    return-void
.end method

.method public a(Lejw;)V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method public final a(Lesj;)V
    .locals 1
    .param p1    # Lesj;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 261
    invoke-virtual {p0}, Lgxi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0, p1}, Lcom/google/android/velvet/ui/MainContentView;->a(Lesj;)V

    .line 264
    :cond_0
    return-void
.end method

.method protected varargs a([Landroid/view/View;)V
    .locals 2
    .param p1    # [Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 131
    const/4 v0, -0x1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lgxi;->a(ILjava/lang/Iterable;)V

    .line 132
    return-void
.end method

.method protected aKQ()F
    .locals 1

    .prologue
    .line 302
    invoke-virtual {p0}, Lgxi;->aAU()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 303
    iget-object v0, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->aKQ()F

    move-result v0

    return v0
.end method

.method public final aLt()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lgxi;->aAU()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 77
    iget-object v0, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->atB()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v0

    return-object v0
.end method

.method public aLu()Lcom/google/android/sidekick/shared/ui/NowProgressBar;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p0}, Lgxi;->aAU()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 93
    iget-object v0, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->aLu()Lcom/google/android/sidekick/shared/ui/NowProgressBar;

    move-result-object v0

    return-object v0
.end method

.method protected final aP(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 97
    invoke-virtual {p0}, Lgxi;->aAU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lgxi;->mView:Lcom/google/android/velvet/ui/MainContentView;

    invoke-virtual {v0}, Lcom/google/android/velvet/ui/MainContentView;->atB()Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-static {p1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aP(Landroid/view/View;)V

    .line 100
    :cond_0
    return-void
.end method

.method protected final varargs b([Landroid/view/View;)V
    .locals 2
    .param p1    # [Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 163
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    new-instance v0, Lgxr;

    const-string v1, "removeViews"

    invoke-direct {v0, p0, v1, p1, p1}, Lgxr;-><init>(Lgxi;Ljava/lang/String;Ljava/lang/Object;[Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 172
    return-void
.end method

.method public fT(Z)V
    .locals 0

    .prologue
    .line 326
    return-void
.end method

.method protected final fX(Z)V
    .locals 2

    .prologue
    .line 206
    new-instance v0, Lgxk;

    const-string v1, "setScrimVisible"

    invoke-direct {v0, p0, v1, p1}, Lgxk;-><init>(Lgxi;Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 212
    return-void
.end method

.method protected final fY(Z)V
    .locals 1

    .prologue
    .line 224
    new-instance v0, Lgxm;

    invoke-direct {v0, p0, p1}, Lgxm;-><init>(Lgxi;Z)V

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 230
    return-void
.end method

.method protected final fZ(Z)V
    .locals 1

    .prologue
    .line 239
    new-instance v0, Lgxn;

    invoke-direct {v0, p0, p1}, Lgxn;-><init>(Lgxi;Z)V

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 245
    return-void
.end method

.method protected final ga(Z)V
    .locals 3

    .prologue
    .line 248
    new-instance v0, Lgxo;

    const-string v1, "setLayoutTransitionsEnabled"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lgxo;-><init>(Lgxi;Ljava/lang/String;Ljava/lang/Object;Z)V

    invoke-virtual {p0, v0}, Lgxi;->a(Lesj;)V

    .line 254
    return-void
.end method
