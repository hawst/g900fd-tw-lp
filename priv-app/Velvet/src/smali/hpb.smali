.class public final Lhpb;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private dnf:I

.field private dng:I

.field private dnh:Landroid/widget/NumberPicker;

.field private dni:Landroid/widget/NumberPicker;

.field private dnj:Landroid/widget/NumberPicker;

.field private dnk:Lhpd;

.field private pH:Landroid/widget/TextView;

.field private pR:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 48
    return-void
.end method

.method static synthetic a(Lhpb;)Landroid/widget/NumberPicker;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lhpb;->dnh:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method public static a(Lhpd;III)Lhpb;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lhpb;

    invoke-direct {v0}, Lhpb;-><init>()V

    .line 61
    iput-object p0, v0, Lhpb;->dnk:Lhpd;

    iput p1, v0, Lhpb;->dnf:I

    iput p2, v0, Lhpb;->pR:I

    iput p3, v0, Lhpb;->dng:I

    .line 62
    return-object v0
.end method

.method static synthetic b(Lhpb;)Landroid/widget/NumberPicker;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lhpb;->dni:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method static synthetic c(Lhpb;)Landroid/widget/NumberPicker;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lhpb;->dnj:Landroid/widget/NumberPicker;

    return-object v0
.end method

.method static synthetic d(Lhpb;)Lhpd;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lhpb;->dnk:Lhpd;

    return-object v0
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/16 v8, 0x3b

    const v7, 0x7f11044b

    const v6, 0x7f11044a

    const/4 v5, 0x0

    .line 75
    invoke-virtual {p0}, Lhpb;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 77
    const v0, 0x7f04018c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 78
    const v0, 0x7f110446

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 79
    const v1, 0x7f110447

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 80
    const v2, 0x7f110448

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 82
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/NumberPicker;

    iput-object v3, p0, Lhpb;->dnh:Landroid/widget/NumberPicker;

    .line 83
    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/NumberPicker;

    iput-object v3, p0, Lhpb;->dni:Landroid/widget/NumberPicker;

    .line 84
    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/NumberPicker;

    iput-object v3, p0, Lhpb;->dnj:Landroid/widget/NumberPicker;

    .line 86
    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0a088a

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 87
    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a088b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 88
    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a088c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 90
    const v0, 0x7f110449

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhpb;->pH:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lhpb;->dnh:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v5}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 93
    iget-object v0, p0, Lhpb;->dnh:Landroid/widget/NumberPicker;

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 94
    iget-object v0, p0, Lhpb;->dnh:Landroid/widget/NumberPicker;

    iget v1, p0, Lhpb;->dnf:I

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 95
    iget-object v0, p0, Lhpb;->dni:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v5}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 96
    iget-object v0, p0, Lhpb;->dni:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v8}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 97
    iget-object v0, p0, Lhpb;->dnh:Landroid/widget/NumberPicker;

    iget v1, p0, Lhpb;->pR:I

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 98
    iget-object v0, p0, Lhpb;->dnj:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v5}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 99
    iget-object v0, p0, Lhpb;->dnj:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v8}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 100
    iget-object v0, p0, Lhpb;->dnh:Landroid/widget/NumberPicker;

    iget v1, p0, Lhpb;->dng:I

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 102
    iget-object v0, p0, Lhpb;->pH:Landroid/widget/TextView;

    new-instance v1, Lhpc;

    invoke-direct {v1, p0}, Lhpc;-><init>(Lhpb;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    return-object v4
.end method
