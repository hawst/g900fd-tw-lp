.class public final Lhap;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final aTW:Ljava/util/concurrent/Executor;

.field final daT:Ljava/util/concurrent/Executor;

.field final daU:Lcmd;

.field final mCookies:Lgpf;

.field final mFlags:Lchk;

.field final mSearchUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lgpf;Lchk;Lcpn;Lcmd;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Livu;->aZh()Livs;

    move-result-object v0

    iput-object v0, p0, Lhap;->daT:Ljava/util/concurrent/Executor;

    .line 70
    iput-object p1, p0, Lhap;->aTW:Ljava/util/concurrent/Executor;

    .line 71
    iput-object p2, p0, Lhap;->mCookies:Lgpf;

    .line 72
    iput-object p3, p0, Lhap;->mFlags:Lchk;

    .line 73
    iput-object p4, p0, Lhap;->mSearchUrlHelper:Lcpn;

    .line 74
    iput-object p5, p0, Lhap;->daU:Lcmd;

    .line 75
    return-void
.end method

.method static a(Lhau;Ljava/lang/Object;)V
    .locals 0
    .param p0    # Lhau;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 352
    if-eqz p0, :cond_0

    .line 353
    invoke-interface {p0, p1}, Lhau;->aV(Ljava/lang/Object;)V

    .line 355
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lhau;)Livq;
    .locals 8
    .param p2    # Lhau;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84
    invoke-static {}, Livy;->aZj()Livy;

    move-result-object v0

    .line 85
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    new-instance v1, Lhar;

    invoke-direct {v1, p0, v0, p1, p2}, Lhar;-><init>(Lhap;Livy;Landroid/net/Uri;Lhau;)V

    .line 87
    iget-object v2, p0, Lhap;->aTW:Ljava/util/concurrent/Executor;

    invoke-interface {v2, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 92
    :goto_0
    return-object v0

    .line 89
    :cond_0
    new-instance v1, Lhas;

    invoke-direct {v1, p0, v0, p1, p2}, Lhas;-><init>(Lhap;Livy;Landroid/net/Uri;Lhau;)V

    .line 90
    iget-object v2, v1, Lhas;->daY:Lhap;

    iget-object v2, v1, Lhas;->daX:Lhau;

    new-instance v3, Ldls;

    const-string v4, "Loading %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v1, Lhas;->daZ:Landroid/net/Uri;

    invoke-static {v7}, Lcpn;->m(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-direct {v3, v4, v5}, Ldls;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2, v3}, Lhap;->a(Lhau;Ljava/lang/Object;)V

    iget-object v2, v1, Lhas;->daY:Lhap;

    iget-object v2, v2, Lhap;->aTW:Ljava/util/concurrent/Executor;

    new-instance v3, Lhat;

    iget-object v4, v1, Lhas;->daZ:Landroid/net/Uri;

    invoke-direct {v3, v1, v4}, Lhat;-><init>(Lhas;Landroid/net/Uri;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
