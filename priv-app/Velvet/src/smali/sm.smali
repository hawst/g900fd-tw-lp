.class final Lsm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lsp;


# instance fields
.field private synthetic zl:Lsi;

.field private final zm:Ljava/util/HashMap;


# direct methods
.method private constructor <init>(Lsi;)V
    .locals 1

    .prologue
    .line 432
    iput-object p1, p0, Lsm;->zl:Lsi;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 433
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-virtual {v0}, Lsi;->eD()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lsm;->zm:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lsi;B)V
    .locals 0

    .prologue
    .line 432
    invoke-direct {p0, p1}, Lsm;-><init>(Lsi;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;)J
    .locals 10

    .prologue
    .line 439
    const-string v0, "title"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lsi;->a(Landroid/content/res/XmlResourceParser;Ljava/lang/String;I)I

    move-result v0

    .line 440
    if-eqz v0, :cond_1

    .line 441
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 446
    :goto_0
    iget-object v1, p0, Lsm;->zl:Lsi;

    invoke-static {v1}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "itemType"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 448
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "spanX"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 449
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "spanY"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 450
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "_id"

    iget-object v2, p0, Lsm;->zl:Lsi;

    invoke-static {v2}, Lsi;->e(Lsi;)Lsn;

    move-result-object v2

    invoke-interface {v2}, Lsn;->eE()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 451
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->e(Lsi;)Lsn;

    move-result-object v0

    iget-object v1, p0, Lsm;->zl:Lsi;

    invoke-static {v1}, Lsi;->f(Lsi;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iget-object v2, p0, Lsm;->zl:Lsi;

    invoke-static {v2}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lsn;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 452
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    .line 453
    const-string v0, "AutoInstalls"

    const-string v1, "Unable to add folder"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    const-wide/16 v0, -0x1

    .line 507
    :cond_0
    :goto_1
    return-wide v0

    .line 443
    :cond_1
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->c(Lsi;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0084

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 457
    :cond_2
    new-instance v4, Landroid/content/ContentValues;

    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 458
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 461
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v1

    .line 462
    :cond_3
    :goto_2
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v0

    const/4 v6, 0x3

    if-ne v0, v6, :cond_4

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v6

    if-le v6, v1, :cond_6

    .line 464
    :cond_4
    const/4 v6, 0x2

    if-ne v0, v6, :cond_3

    .line 465
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 468
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->b(Lsi;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v6, "container"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 470
    iget-object v0, p0, Lsm;->zm:Ljava/util/HashMap;

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsp;

    .line 471
    if-eqz v0, :cond_5

    .line 472
    invoke-interface {v0, p1, p2}, Lsp;->a(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;)J

    move-result-wide v6

    .line 473
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-ltz v0, :cond_3

    .line 474
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 477
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid folder item "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 486
    :cond_6
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_7

    .line 488
    const/4 v0, 0x0

    invoke-static {v2, v3, v0}, Labn;->a(JZ)Landroid/net/Uri;

    move-result-object v0

    .line 489
    new-instance v1, Labg;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Labg;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 490
    iget-object v0, p0, Lsm;->zl:Lsi;

    invoke-static {v0}, Lsi;->f(Lsi;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v2, v1, Labg;->On:Ljava/lang/String;

    iget-object v3, v1, Labg;->Oo:Ljava/lang/String;

    iget-object v1, v1, Labg;->Op:[Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 491
    const-wide/16 v0, -0x1

    .line 495
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 496
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 497
    const-string v0, "container"

    invoke-static {v4, v2, v0}, Lsi;->a(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 498
    const-string v0, "screen"

    invoke-static {v4, v2, v0}, Lsi;->a(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 499
    const-string v0, "cellX"

    invoke-static {v4, v2, v0}, Lsi;->a(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 500
    const-string v0, "cellY"

    invoke-static {v4, v2, v0}, Lsi;->a(Landroid/content/ContentValues;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 502
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 503
    iget-object v3, p0, Lsm;->zl:Lsi;

    invoke-static {v3}, Lsi;->f(Lsi;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "favorites"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    move-wide v0, v2

    goto/16 :goto_1
.end method
