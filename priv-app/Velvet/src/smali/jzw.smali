.class public final Ljzw;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eOx:[Ljzw;


# instance fields
.field private aez:I

.field private bmI:Ljava/lang/String;

.field private dHr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3639
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3640
    const/4 v0, 0x0

    iput v0, p0, Ljzw;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljzw;->bmI:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljzw;->dHr:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljzw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljzw;->eCz:I

    .line 3641
    return-void
.end method

.method public static bxv()[Ljzw;
    .locals 2

    .prologue
    .line 3582
    sget-object v0, Ljzw;->eOx:[Ljzw;

    if-nez v0, :cond_1

    .line 3583
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3585
    :try_start_0
    sget-object v0, Ljzw;->eOx:[Ljzw;

    if-nez v0, :cond_0

    .line 3586
    const/4 v0, 0x0

    new-array v0, v0, [Ljzw;

    sput-object v0, Ljzw;->eOx:[Ljzw;

    .line 3588
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3590
    :cond_1
    sget-object v0, Ljzw;->eOx:[Ljzw;

    return-object v0

    .line 3588
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3576
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljzw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzw;->bmI:Ljava/lang/String;

    iget v0, p0, Ljzw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljzw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljzw;->dHr:Ljava/lang/String;

    iget v0, p0, Ljzw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljzw;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3655
    iget v0, p0, Ljzw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3656
    const/4 v0, 0x1

    iget-object v1, p0, Ljzw;->bmI:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3658
    :cond_0
    iget v0, p0, Ljzw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3659
    const/4 v0, 0x2

    iget-object v1, p0, Ljzw;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3661
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3662
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3666
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3667
    iget v1, p0, Ljzw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3668
    const/4 v1, 0x1

    iget-object v2, p0, Ljzw;->bmI:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3671
    :cond_0
    iget v1, p0, Ljzw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3672
    const/4 v1, 0x2

    iget-object v2, p0, Ljzw;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3675
    :cond_1
    return v0
.end method
