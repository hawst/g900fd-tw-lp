.class public Ldnz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bDR:Ldob;

.field private bDS:I

.field private bDT:Z

.field private final bDU:Ljava/lang/Runnable;

.field private cq:Z

.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mAudioRouter:Lhhu;

.field final mAudioState:Ldbh;

.field private final mContext:Landroid/content/Context;

.field final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mServiceState:Ldcu;

.field private final mSettings:Lhym;

.field private final mSoundManager:Lhik;

.field final mTtsState:Ldcw;

.field final mUiThread:Leqo;


# direct methods
.method public constructor <init>(Ldbh;Ldcw;Lcom/google/android/search/core/state/QueryState;Lhhq;Leqo;Landroid/content/Context;Landroid/media/AudioManager;Lhym;Ldcu;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ldoa;

    const-string v1, "Refresh external audio state"

    invoke-direct {v0, p0, v1}, Ldoa;-><init>(Ldnz;Ljava/lang/String;)V

    iput-object v0, p0, Ldnz;->bDU:Ljava/lang/Runnable;

    .line 69
    iput-object p1, p0, Ldnz;->mAudioState:Ldbh;

    .line 70
    iput-object p2, p0, Ldnz;->mTtsState:Ldcw;

    .line 71
    iput-object p3, p0, Ldnz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 72
    invoke-virtual {p4}, Lhhq;->aPb()Lhhu;

    move-result-object v0

    iput-object v0, p0, Ldnz;->mAudioRouter:Lhhu;

    .line 73
    invoke-virtual {p4}, Lhhq;->aOZ()Lhik;

    move-result-object v0

    iput-object v0, p0, Ldnz;->mSoundManager:Lhik;

    .line 74
    iput-object p5, p0, Ldnz;->mUiThread:Leqo;

    .line 75
    iput-object p6, p0, Ldnz;->mContext:Landroid/content/Context;

    .line 76
    new-instance v0, Ldob;

    invoke-direct {v0, p0}, Ldob;-><init>(Ldnz;)V

    iput-object v0, p0, Ldnz;->bDR:Ldob;

    .line 78
    iget-object v0, p0, Ldnz;->mSoundManager:Lhik;

    iget-object v1, p0, Ldnz;->bDR:Ldob;

    invoke-virtual {v0, v1}, Lhik;->a(Lhim;)V

    .line 79
    iput-object p7, p0, Ldnz;->mAudioManager:Landroid/media/AudioManager;

    .line 80
    iput-object p8, p0, Ldnz;->mSettings:Lhym;

    .line 81
    iput-object p9, p0, Ldnz;->mServiceState:Ldcu;

    .line 82
    return-void
.end method


# virtual methods
.method public final Rm()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldnz;->cq:Z

    .line 90
    invoke-virtual {p0}, Ldnz;->adY()V

    .line 91
    return-void
.end method

.method public final adX()V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Ldnz;->mUiThread:Leqo;

    iget-object v1, p0, Ldnz;->bDU:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 100
    iget-object v0, p0, Ldnz;->mUiThread:Leqo;

    iget-object v1, p0, Ldnz;->bDU:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 102
    return-void
.end method

.method public final adY()V
    .locals 10

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 105
    .line 112
    iget-boolean v0, p0, Ldnz;->cq:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Ldnz;->mAudioState:Ldbh;

    invoke-virtual {v0}, Ldbh;->Wg()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 113
    iget-object v0, p0, Ldnz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v8

    .line 114
    new-instance v6, Ldoe;

    invoke-direct {v6, p0, v8}, Ldoe;-><init>(Ldnz;Lcom/google/android/shared/search/Query;)V

    .line 116
    iget-object v0, p0, Ldnz;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTN()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    move v3, v4

    .line 126
    :goto_0
    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aoQ()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 171
    :goto_1
    return-void

    :cond_0
    move v0, v2

    move v3, v1

    .line 125
    goto :goto_0

    .line 137
    :cond_1
    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aqZ()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aqG()Z

    move-result v9

    if-eqz v9, :cond_3

    move-object v0, v5

    move v1, v4

    move v3, v7

    .line 162
    :goto_2
    iget-object v7, p0, Ldnz;->mAudioRouter:Lhhu;

    iget-object v5, p0, Ldnz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v8

    iget-object v5, p0, Ldnz;->mServiceState:Ldcu;

    invoke-virtual {v5}, Ldcu;->YZ()Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v5

    if-eqz v5, :cond_7

    move v5, v4

    :goto_3
    iget-object v6, p0, Ldnz;->mSettings:Lhym;

    invoke-virtual {v6}, Lhym;->aTN()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Ldnz;->mSettings:Lhym;

    invoke-virtual {v6}, Lhym;->aTO()Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_2
    move v6, v4

    :goto_4
    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v8

    if-eqz v8, :cond_9

    if-nez v6, :cond_9

    if-nez v5, :cond_9

    :goto_5
    invoke-interface {v7, v3, v1, v0, v4}, Lhhu;->a(IILhhv;Z)V

    goto :goto_1

    .line 142
    :cond_3
    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aqn()Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aqg()Z

    move-result v7

    if-nez v7, :cond_4

    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aqZ()Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_4
    move-object v0, v5

    move v3, v1

    move v1, v4

    .line 152
    goto :goto_2

    .line 153
    :cond_5
    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aqb()Z

    move-result v5

    if-eqz v5, :cond_6

    move-object v0, v6

    move v3, v2

    .line 156
    goto :goto_2

    .line 157
    :cond_6
    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aql()Z

    move-result v5

    if-nez v5, :cond_a

    .line 158
    invoke-virtual {v8}, Lcom/google/android/shared/search/Query;->aqz()Z

    move-result v5

    if-eqz v5, :cond_a

    move-object v0, v6

    move v3, v1

    move v1, v4

    .line 161
    goto :goto_2

    :cond_7
    move v5, v2

    .line 162
    goto :goto_3

    :cond_8
    move v6, v2

    goto :goto_4

    :cond_9
    move v4, v2

    goto :goto_5

    :cond_a
    move v1, v0

    move-object v0, v6

    goto :goto_2

    :cond_b
    move-object v0, v5

    move v1, v4

    move v3, v7

    goto :goto_2
.end method

.method public final adZ()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 199
    iget-object v0, p0, Ldnz;->mAudioState:Ldbh;

    invoke-virtual {v0}, Ldbh;->Wg()Z

    move-result v0

    .line 200
    iget-boolean v1, p0, Ldnz;->bDT:Z

    if-eqz v1, :cond_1

    .line 201
    if-nez v0, :cond_0

    .line 202
    iget-object v0, p0, Ldnz;->mAudioManager:Landroid/media/AudioManager;

    iget v1, p0, Ldnz;->bDS:I

    invoke-virtual {v0, v1, v3, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 203
    iput-boolean v3, p0, Ldnz;->bDT:Z

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    iget-object v1, p0, Ldnz;->mAudioRouter:Lhhu;

    invoke-interface {v1}, Lhhu;->aPq()I

    move-result v1

    .line 207
    if-eqz v0, :cond_0

    iget-object v0, p0, Ldnz;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldnz;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iget-object v0, p0, Ldnz;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Ldnz;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldnz;->bDT:Z

    .line 213
    iput v1, p0, Ldnz;->bDS:I

    goto :goto_0
.end method

.method final aea()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 225
    iget-object v0, p0, Ldnz;->mContext:Landroid/content/Context;

    const-string v3, "accessibility"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 227
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 228
    const/4 v3, 0x0

    .line 230
    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 239
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 242
    :goto_1
    return v0

    .line 236
    :catch_0
    move-exception v0

    const-string v0, "AudioWorker"

    const-string v4, "NPE in getEnabledAccessibilityServiceList"

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    goto :goto_0

    :cond_0
    move v0, v2

    .line 239
    goto :goto_1

    :cond_1
    move v0, v2

    .line 242
    goto :goto_1
.end method

.method public final ca()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Ldnz;->mSoundManager:Lhik;

    iget-object v1, p0, Ldnz;->bDR:Ldob;

    invoke-virtual {v0, v1}, Lhik;->b(Lhim;)V

    .line 86
    return-void
.end method

.method public final onStopped()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldnz;->cq:Z

    .line 95
    invoke-virtual {p0}, Ldnz;->adY()V

    .line 96
    return-void
.end method
