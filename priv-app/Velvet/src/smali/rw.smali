.class public final Lrw;
.super Lcom/android/launcher3/CellLayout;
.source "PG"

# interfaces
.implements Lacf;


# instance fields
.field public final yd:Lcom/android/launcher3/FocusIndicatorView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/16 v2, 0x64

    .line 27
    invoke-direct {p0, p1}, Lcom/android/launcher3/CellLayout;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Lcom/android/launcher3/FocusIndicatorView;

    invoke-direct {v0, p1}, Lcom/android/launcher3/FocusIndicatorView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lrw;->yd:Lcom/android/launcher3/FocusIndicatorView;

    .line 30
    iget-object v0, p0, Lrw;->yd:Lcom/android/launcher3/FocusIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lrw;->addView(Landroid/view/View;I)V

    .line 31
    iget-object v0, p0, Lrw;->yd:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v0}, Lcom/android/launcher3/FocusIndicatorView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 32
    iget-object v0, p0, Lrw;->yd:Lcom/android/launcher3/FocusIndicatorView;

    invoke-virtual {v0}, Lcom/android/launcher3/FocusIndicatorView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 33
    return-void
.end method


# virtual methods
.method public final eb()V
    .locals 2

    .prologue
    .line 37
    invoke-virtual {p0}, Lrw;->removeAllViews()V

    .line 38
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lrw;->setLayerType(ILandroid/graphics/Paint;)V

    .line 39
    return-void
.end method

.method public final ec()I
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lrw;->getChildCount()I

    move-result v0

    return v0
.end method
