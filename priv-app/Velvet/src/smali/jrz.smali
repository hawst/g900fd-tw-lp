.class public final Ljrz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eBU:[Ljrz;


# instance fields
.field private aez:I

.field private eBV:I

.field private eBW:Ljod;

.field private eBX:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 221
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 222
    iput v0, p0, Ljrz;->aez:I

    iput v0, p0, Ljrz;->eBV:I

    iput-object v1, p0, Ljrz;->eBW:Ljod;

    iput v0, p0, Ljrz;->eBX:I

    iput-object v1, p0, Ljrz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljrz;->eCz:I

    .line 223
    return-void
.end method

.method public static btH()[Ljrz;
    .locals 2

    .prologue
    .line 167
    sget-object v0, Ljrz;->eBU:[Ljrz;

    if-nez v0, :cond_1

    .line 168
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 170
    :try_start_0
    sget-object v0, Ljrz;->eBU:[Ljrz;

    if-nez v0, :cond_0

    .line 171
    const/4 v0, 0x0

    new-array v0, v0, [Ljrz;

    sput-object v0, Ljrz;->eBU:[Ljrz;

    .line 173
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :cond_1
    sget-object v0, Ljrz;->eBU:[Ljrz;

    return-object v0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 154
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljrz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljrz;->eBV:I

    iget v0, p0, Ljrz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljrz;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljrz;->eBW:Ljod;

    if-nez v0, :cond_1

    new-instance v0, Ljod;

    invoke-direct {v0}, Ljod;-><init>()V

    iput-object v0, p0, Ljrz;->eBW:Ljod;

    :cond_1
    iget-object v0, p0, Ljrz;->eBW:Ljod;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljrz;->eBX:I

    iget v0, p0, Ljrz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljrz;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 238
    iget v0, p0, Ljrz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 239
    const/4 v0, 0x1

    iget v1, p0, Ljrz;->eBV:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 241
    :cond_0
    iget-object v0, p0, Ljrz;->eBW:Ljod;

    if-eqz v0, :cond_1

    .line 242
    const/4 v0, 0x2

    iget-object v1, p0, Ljrz;->eBW:Ljod;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 244
    :cond_1
    iget v0, p0, Ljrz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 245
    const/4 v0, 0x3

    iget v1, p0, Ljrz;->eBX:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 247
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 248
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 252
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 253
    iget v1, p0, Ljrz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 254
    const/4 v1, 0x1

    iget v2, p0, Ljrz;->eBV:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_0
    iget-object v1, p0, Ljrz;->eBW:Ljod;

    if-eqz v1, :cond_1

    .line 258
    const/4 v1, 0x2

    iget-object v2, p0, Ljrz;->eBW:Ljod;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_1
    iget v1, p0, Ljrz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 262
    const/4 v1, 0x3

    iget v2, p0, Ljrz;->eBX:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_2
    return v0
.end method
