.class public Leqw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private final chB:Ljava/lang/CharSequence;

.field private final chC:I

.field private final chD:Z

.field private final chE:Z

.field private chF:I

.field private chG:I

.field private chH:I


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;IZZ)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Leqw;->chB:Ljava/lang/CharSequence;

    .line 75
    iput p2, p0, Leqw;->chC:I

    .line 76
    iput-boolean p3, p0, Leqw;->chD:Z

    .line 77
    iput-boolean p4, p0, Leqw;->chE:Z

    .line 78
    invoke-direct {p0}, Leqw;->avw()V

    .line 79
    return-void
.end method

.method public static J(Ljava/lang/CharSequence;)Leqw;
    .locals 4

    .prologue
    .line 36
    new-instance v0, Leqw;

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Leqw;-><init>(Ljava/lang/CharSequence;IZZ)V

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;C)Leqw;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Leqw;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, p1, v1, v2}, Leqw;-><init>(Ljava/lang/CharSequence;IZZ)V

    return-object v0
.end method

.method private avw()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 82
    iget v0, p0, Leqw;->chF:I

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 83
    iget v0, p0, Leqw;->chC:I

    if-ne v0, v1, :cond_6

    .line 84
    iget v0, p0, Leqw;->chH:I

    if-ne v0, v1, :cond_2

    iput v1, p0, Leqw;->chF:I

    .line 88
    :cond_0
    :goto_1
    return-void

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :cond_2
    iget-object v0, p0, Leqw;->chB:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-boolean v2, p0, Leqw;->chE:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Leqw;->chB:Ljava/lang/CharSequence;

    iget v3, p0, Leqw;->chH:I

    invoke-static {v2, v3, v0}, Letd;->e(Ljava/lang/CharSequence;II)I

    move-result v2

    iput v2, p0, Leqw;->chF:I

    iget v2, p0, Leqw;->chF:I

    if-eq v2, v1, :cond_0

    iget-object v2, p0, Leqw;->chB:Ljava/lang/CharSequence;

    iget v3, p0, Leqw;->chF:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v3, v0}, Letd;->d(Ljava/lang/CharSequence;II)I

    move-result v2

    iput v2, p0, Leqw;->chG:I

    iget v2, p0, Leqw;->chG:I

    if-eq v2, v1, :cond_3

    iget v0, p0, Leqw;->chG:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leqw;->chH:I

    goto :goto_1

    :cond_3
    iput v0, p0, Leqw;->chG:I

    iput v1, p0, Leqw;->chH:I

    goto :goto_1

    :cond_4
    iget v2, p0, Leqw;->chH:I

    iput v2, p0, Leqw;->chF:I

    iget-object v2, p0, Leqw;->chB:Ljava/lang/CharSequence;

    iget v3, p0, Leqw;->chH:I

    invoke-static {v2, v3, v0}, Letd;->d(Ljava/lang/CharSequence;II)I

    move-result v2

    iput v2, p0, Leqw;->chG:I

    iget v2, p0, Leqw;->chG:I

    if-eq v2, v1, :cond_5

    iget v0, p0, Leqw;->chG:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leqw;->chH:I

    goto :goto_1

    :cond_5
    iput v0, p0, Leqw;->chG:I

    iput v1, p0, Leqw;->chH:I

    goto :goto_1

    .line 86
    :cond_6
    iget v0, p0, Leqw;->chC:I

    int-to-char v4, v0

    iget-object v0, p0, Leqw;->chB:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v5

    :cond_7
    iget v0, p0, Leqw;->chH:I

    if-eq v0, v1, :cond_c

    iget v3, p0, Leqw;->chH:I

    iget v2, p0, Leqw;->chH:I

    :goto_2
    if-eq v2, v5, :cond_8

    iget-object v0, p0, Leqw;->chB:Ljava/lang/CharSequence;

    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-eq v0, v4, :cond_8

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    if-ne v2, v5, :cond_9

    move v0, v1

    :goto_3
    iput v0, p0, Leqw;->chH:I

    iget-boolean v0, p0, Leqw;->chD:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Leqw;->chB:Ljava/lang/CharSequence;

    invoke-static {v0, v3, v2}, Letd;->e(Ljava/lang/CharSequence;II)I

    move-result v0

    if-eq v0, v1, :cond_a

    iput v0, p0, Leqw;->chF:I

    iget-object v1, p0, Leqw;->chB:Ljava/lang/CharSequence;

    invoke-static {v1, v0, v2}, Letd;->f(Ljava/lang/CharSequence;II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leqw;->chG:I

    goto/16 :goto_1

    :cond_9
    add-int/lit8 v0, v2, 0x1

    goto :goto_3

    :cond_a
    move v0, v2

    :goto_4
    if-ne v0, v2, :cond_b

    iget-boolean v3, p0, Leqw;->chE:Z

    if-nez v3, :cond_7

    :cond_b
    iput v0, p0, Leqw;->chF:I

    iput v2, p0, Leqw;->chG:I

    goto/16 :goto_1

    :cond_c
    iput v1, p0, Leqw;->chF:I

    goto/16 :goto_1

    :cond_d
    move v0, v3

    goto :goto_4
.end method

.method public static b(Ljava/lang/CharSequence;C)Leqw;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 52
    new-instance v0, Leqw;

    invoke-direct {v0, p0, p1, v1, v1}, Leqw;-><init>(Ljava/lang/CharSequence;IZZ)V

    return-object v0
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Leqw;->chF:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Leqw;->next()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final next()Ljava/lang/String;
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Leqw;->chB:Ljava/lang/CharSequence;

    iget v1, p0, Leqw;->chF:I

    iget v2, p0, Leqw;->chG:I

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-direct {p0}, Leqw;->avw()V

    .line 64
    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
