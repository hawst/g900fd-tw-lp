.class public final enum Lgff;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cIt:Lgff;

.field public static final enum cIu:Lgff;

.field private static final synthetic cIy:[Lgff;


# instance fields
.field private final bWw:Ljava/lang/String;

.field private final cIv:Ljava/lang/String;

.field private final cIw:I

.field private final cIx:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 41
    new-instance v0, Lgff;

    const-string v1, "AMR"

    const-string v3, "audio/AMR"

    const-string v4, "amr"

    const/4 v6, 0x3

    invoke-direct/range {v0 .. v6}, Lgff;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    sput-object v0, Lgff;->cIt:Lgff;

    .line 42
    new-instance v3, Lgff;

    const-string v4, "AMRWB"

    const-string v6, "audio/amr-wb"

    const-string v7, "amr"

    const/16 v9, 0x9

    invoke-direct/range {v3 .. v9}, Lgff;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V

    sput-object v3, Lgff;->cIu:Lgff;

    .line 40
    new-array v0, v8, [Lgff;

    sget-object v1, Lgff;->cIt:Lgff;

    aput-object v1, v0, v2

    sget-object v1, Lgff;->cIu:Lgff;

    aput-object v1, v0, v5

    sput-object v0, Lgff;->cIy:[Lgff;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput-object p3, p0, Lgff;->bWw:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lgff;->cIv:Ljava/lang/String;

    .line 52
    iput p5, p0, Lgff;->cIw:I

    .line 53
    iput p6, p0, Lgff;->cIx:I

    .line 54
    return-void
.end method

.method public static ki(I)Lgff;
    .locals 5

    .prologue
    .line 73
    invoke-static {}, Lgff;->values()[Lgff;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 74
    iget v4, v3, Lgff;->cIw:I

    if-ne v4, p0, :cond_0

    .line 75
    return-object v3

    .line 73
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lgff;
    .locals 1

    .prologue
    .line 40
    const-class v0, Lgff;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgff;

    return-object v0
.end method

.method public static values()[Lgff;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lgff;->cIy:[Lgff;

    invoke-virtual {v0}, [Lgff;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgff;

    return-object v0
.end method


# virtual methods
.method public final aFA()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lgff;->cIw:I

    return v0
.end method

.method public final aFB()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lgff;->cIx:I

    return v0
.end method

.method public final aFz()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lgff;->cIv:Ljava/lang/String;

    return-object v0
.end method

.method public final getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lgff;->bWw:Ljava/lang/String;

    return-object v0
.end method
