.class final Lcjz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhli;


# instance fields
.field private synthetic aZt:Lcjt;

.field private final mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method public constructor <init>(Lcjt;Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 1269
    iput-object p1, p0, Lcjz;->aZt:Lcjt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1270
    iput-object p2, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    .line 1271
    return-void
.end method


# virtual methods
.method public final CZ()V
    .locals 5

    .prologue
    .line 1395
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->U(Lcom/google/android/shared/search/Query;)V

    .line 1397
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mVoiceSearchServices:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    .line 1398
    invoke-virtual {v0}, Lhym;->Me()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1400
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aPg()Lgfb;

    move-result-object v0

    .line 1401
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lgfb;->aFy()Lgfc;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1402
    iget-object v1, p0, Lcjz;->aZt:Lcjt;

    iget-object v1, v1, Lcjt;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcjz;->aZt:Lcjt;

    iget-object v2, v2, Lcjt;->mVoiceSearchServices:Lhhq;

    iget-object v2, v2, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    invoke-interface {v0}, Lgfb;->aFy()Lgfc;

    move-result-object v0

    invoke-virtual {v0}, Lgfc;->asV()[B

    move-result-object v0

    iget-object v3, p0, Lcjz;->aZt:Lcjt;

    iget-object v3, v3, Lcjt;->mContext:Landroid/content/Context;

    const-string v4, "-vs"

    invoke-static {v3, v4}, Lgep;->q(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lgep;->a(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;[BLjava/lang/String;)V

    .line 1408
    :cond_0
    return-void
.end method

.method public final Nq()V
    .locals 2

    .prologue
    .line 1275
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1277
    return-void
.end method

.method public final Nr()V
    .locals 2

    .prologue
    .line 1347
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1348
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aas()Ldbj;

    move-result-object v0

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldbj;->C(Lcom/google/android/shared/search/Query;)V

    .line 1349
    return-void
.end method

.method public final Ns()V
    .locals 2

    .prologue
    .line 1353
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1354
    return-void
.end method

.method public final Nt()V
    .locals 2

    .prologue
    .line 1358
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->R(Lcom/google/android/shared/search/Query;)V

    .line 1359
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1360
    return-void
.end method

.method public final Nu()V
    .locals 2

    .prologue
    .line 1364
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1365
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aas()Ldbj;

    move-result-object v0

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ldbj;->D(Lcom/google/android/shared/search/Query;)V

    .line 1366
    return-void
.end method

.method public final Nv()V
    .locals 1

    .prologue
    .line 1370
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->Nv()V

    .line 1371
    return-void
.end method

.method public final Nw()V
    .locals 8

    .prologue
    .line 1379
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v2, p0, Lcjz;->aZt:Lcjt;

    iget-boolean v2, v2, Lcjt;->aZs:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/search/core/state/QueryState;->e(Lcom/google/android/shared/search/Query;Z)V

    .line 1380
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ldct;->fE(I)V

    .line 1384
    iget-object v0, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqD()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1385
    iget-object v0, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v3

    .line 1386
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v6, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v7, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    new-instance v0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    new-instance v2, Leit;

    invoke-direct {v2}, Leit;-><init>()V

    iget-object v4, p0, Lcjz;->aZt:Lcjt;

    iget-object v4, v4, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v4}, Lhhq;->aPg()Lgfb;

    move-result-object v4

    invoke-interface {v4, v3}, Lgfb;->mz(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;-><init>(Lcom/google/android/shared/search/Query;Leiq;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/google/android/search/core/state/QueryState;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 1391
    :cond_0
    return-void
.end method

.method public final a(Leil;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1413
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    invoke-static {v0}, Lcjt;->a(Lcjt;)Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->ani()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0930

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1415
    :goto_0
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v6, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v7, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    new-instance v0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v2, p0, Lcjz;->aZt:Lcjt;

    iget-object v2, v2, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v2}, Lhhq;->aPg()Lgfb;

    move-result-object v2

    invoke-interface {v2, p2}, Lgfb;->mz(Ljava/lang/String;)Z

    move-result v4

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;-><init>(Lcom/google/android/shared/search/Query;Leiq;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/google/android/search/core/state/QueryState;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 1419
    return-void

    .line 1413
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final a(Leiq;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1317
    const/4 v5, 0x0

    .line 1319
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    invoke-static {v0}, Lcjt;->a(Lcjt;)Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->ani()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1324
    const v0, 0x7f0a0934

    .line 1327
    iget-object v1, p0, Lcjz;->aZt:Lcjt;

    iget-object v1, v1, Lcjt;->mCoreServices:Lcfo;

    invoke-virtual {v1}, Lcfo;->DY()Lgno;

    move-result-object v1

    invoke-virtual {v1}, Lgno;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1328
    const v0, 0x7f0a0933

    .line 1332
    :cond_0
    iget-object v1, p0, Lcjz;->aZt:Lcjt;

    iget-object v1, v1, Lcjt;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lesp;->aA(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1333
    const v0, 0x7f0a0932

    .line 1336
    :cond_1
    iget-object v1, p0, Lcjz;->aZt:Lcjt;

    iget-object v1, v1, Lcjt;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1339
    :cond_2
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v6, v0, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v7, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    new-instance v0, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v2, p0, Lcjz;->aZt:Lcjt;

    iget-object v2, v2, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v2}, Lhhq;->aPg()Lgfb;

    move-result-object v2

    invoke-interface {v2, p2}, Lgfb;->mz(Ljava/lang/String;)Z

    move-result v4

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;-><init>(Lcom/google/android/shared/search/Query;Leiq;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/google/android/search/core/state/QueryState;->b(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 1343
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Lijj;Lcmq;)V
    .locals 2

    .prologue
    .line 1286
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1287
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1288
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    invoke-virtual {v0}, Lcjt;->MX()Lcmt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcmt;->a(Lcmq;)V

    .line 1289
    invoke-virtual {p3}, Lcmq;->Qi()Lcom/google/android/shared/search/Query;

    move-result-object v0

    .line 1290
    iget-object v1, p0, Lcjz;->aZt:Lcjt;

    iget-object v1, v1, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v0, p1, p2}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Ljava/lang/CharSequence;Lijj;)V

    .line 1291
    iget-object v1, p0, Lcjz;->aZt:Lcjt;

    iget-object v1, v1, Lcjt;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1, v0, p3}, Lcom/google/android/search/core/state/QueryState;->a(Lcom/google/android/shared/search/Query;Lcmq;)V

    .line 1292
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->aOS()Ligi;

    move-result-object v0

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgij;

    invoke-interface {v0}, Lgij;->aFX()Ligi;

    move-result-object v0

    check-cast v0, Lgih;

    .line 1294
    iget-object v1, p0, Lcjz;->aZt:Lcjt;

    iget-object v1, v1, Lcjt;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1}, Lhhq;->aOS()Ligi;

    move-result-object v1

    invoke-interface {v1}, Ligi;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgij;

    invoke-interface {v1}, Lgij;->aFY()Ligi;

    move-result-object v1

    check-cast v1, Lgii;

    .line 1296
    iput-object p3, v0, Lgih;->mSearchResult:Lcmq;

    .line 1297
    iput-object p3, v1, Lgii;->mSearchResult:Lcmq;

    .line 1299
    :cond_0
    return-void
.end method

.method public final a(Ljrp;)V
    .locals 2

    .prologue
    .line 1308
    if-eqz p1, :cond_0

    .line 1309
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aav()Ldcv;

    move-result-object v0

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, p1}, Ldcv;->a(Lcom/google/android/shared/search/Query;Ljrp;)V

    .line 1311
    :cond_0
    return-void
.end method

.method public final n(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1428
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    invoke-virtual {v0}, Lcjt;->Ne()Ldyl;

    move-result-object v0

    invoke-interface {v0, p1}, Ldyl;->n(Ljava/lang/CharSequence;)V

    .line 1429
    return-void
.end method

.method public final p([B)V
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    iget-object v0, v0, Lcjt;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    iget-object v1, p0, Lcjz;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, p1}, Ldcw;->a(Lcom/google/android/shared/search/Query;[B)V

    .line 1304
    return-void
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1423
    iget-object v0, p0, Lcjz;->aZt:Lcjt;

    invoke-virtual {v0}, Lcjt;->Ne()Ldyl;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ldyl;->x(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    return-void
.end method
