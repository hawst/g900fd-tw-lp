.class public final Lghj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final cKh:[Ljava/lang/String;


# instance fields
.field private final mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "times_contacted"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "last_time_contacted"

    aput-object v2, v0, v1

    sput-object v0, Lghj;->cKh:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lghj;->mContentResolver:Landroid/content/ContentResolver;

    .line 32
    return-void
.end method


# virtual methods
.method public final bc(II)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 77
    const/16 v0, 0x64

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 78
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "limit"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 82
    const-string v3, "times_contacted > ?"

    .line 83
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "0"

    aput-object v0, v4, v6

    .line 84
    iget-object v0, p0, Lghj;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lghj;->cKh:[Ljava/lang/String;

    const-string v5, "times_contacted DESC, last_time_contacted DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 86
    new-instance v2, Lghk;

    invoke-direct {v2}, Lghk;-><init>()V

    .line 87
    invoke-static {v2, v0}, Lhgl;->a(Lhgm;Landroid/database/Cursor;)V

    .line 88
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 89
    iget-object v0, v2, Lghk;->bxW:Ljava/util/List;

    new-instance v1, Lghi;

    invoke-direct {v1}, Lghi;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move v1, v6

    .line 90
    :goto_0
    iget-object v0, v2, Lghk;->bxW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 91
    iget-object v0, v2, Lghk;->bxW:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghh;

    .line 96
    iget-object v0, v0, Lghh;->mName:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 98
    :cond_0
    return-object v3
.end method
