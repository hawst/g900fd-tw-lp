.class public final Ldfd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final bvR:[J

.field private final bvS:[J

.field private final bvT:[I

.field private final bvU:[[I


# direct methods
.method public constructor <init>([J[I[J[[I)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-direct {p0, p1, p2, p3, p4}, Ldfd;->a([J[I[J[[I)Z

    move-result v0

    invoke-static {v0}, Lifv;->gX(Z)V

    .line 79
    iput-object p1, p0, Ldfd;->bvR:[J

    .line 80
    iput-object p2, p0, Ldfd;->bvT:[I

    .line 81
    iput-object p3, p0, Ldfd;->bvS:[J

    .line 82
    iput-object p4, p0, Ldfd;->bvU:[[I

    .line 83
    return-void
.end method

.method private a([J[I[J[[I)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_2

    :cond_0
    move v1, v2

    .line 107
    :cond_1
    :goto_0
    return v1

    .line 91
    :cond_2
    array-length v0, p1

    if-eqz v0, :cond_3

    array-length v0, p2

    if-eqz v0, :cond_3

    array-length v0, p3

    if-eqz v0, :cond_3

    array-length v0, p4

    if-nez v0, :cond_4

    :cond_3
    move v1, v2

    .line 93
    goto :goto_0

    .line 95
    :cond_4
    array-length v0, p1

    array-length v3, p3

    if-ne v0, v3, :cond_5

    array-length v0, p1

    array-length v3, p4

    if-eq v0, v3, :cond_6

    :cond_5
    move v1, v2

    .line 97
    goto :goto_0

    .line 99
    :cond_6
    array-length v3, p4

    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_8

    aget-object v4, p4, v0

    .line 100
    array-length v5, p2

    array-length v4, v4

    if-eq v5, v4, :cond_7

    move v1, v2

    .line 101
    goto :goto_0

    .line 99
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    move v0, v1

    .line 104
    :goto_2
    array-length v3, p1

    if-ge v0, v3, :cond_b

    add-int/lit8 v3, v0, -0x1

    aget-wide v4, p1, v3

    aget-wide v6, p1, v0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_a

    move v0, v2

    :goto_3
    if-eqz v0, :cond_9

    move v0, v1

    :goto_4
    array-length v3, p2

    if-ge v0, v3, :cond_d

    add-int/lit8 v3, v0, -0x1

    aget v3, p2, v3

    aget v4, p2, v0

    if-lt v3, v4, :cond_c

    move v0, v2

    :goto_5
    if-nez v0, :cond_1

    :cond_9
    move v1, v2

    .line 105
    goto :goto_0

    .line 104
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_b
    move v0, v1

    goto :goto_3

    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_d
    move v0, v1

    goto :goto_5
.end method


# virtual methods
.method public final ar(J)J
    .locals 3

    .prologue
    .line 132
    invoke-virtual {p0, p1, p2}, Ldfd;->as(J)I

    move-result v0

    .line 133
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    :goto_0
    return-wide v0

    :cond_0
    iget-object v1, p0, Ldfd;->bvS:[J

    aget-wide v0, v1, v0

    goto :goto_0
.end method

.method public final as(J)I
    .locals 5

    .prologue
    .line 158
    const/4 v1, -0x1

    .line 159
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ldfd;->bvR:[J

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 160
    iget-object v2, p0, Ldfd;->bvR:[J

    aget-wide v2, v2, v0

    cmp-long v2, v2, p1

    if-gtz v2, :cond_0

    move v1, v0

    .line 159
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 164
    :cond_1
    return v1
.end method

.method public final fO(I)I
    .locals 3

    .prologue
    .line 172
    const/4 v1, -0x1

    .line 173
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ldfd;->bvT:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 174
    iget-object v2, p0, Ldfd;->bvT:[I

    aget v2, v2, v0

    if-gt v2, p1, :cond_0

    move v1, v0

    .line 173
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 178
    :cond_1
    return v1
.end method

.method public final g(JI)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 141
    invoke-virtual {p0, p1, p2}, Ldfd;->as(J)I

    move-result v2

    .line 142
    if-ne v2, v3, :cond_0

    .line 149
    :goto_0
    return v0

    .line 145
    :cond_0
    invoke-virtual {p0, p3}, Ldfd;->fO(I)I

    move-result v1

    .line 146
    if-ne v1, v3, :cond_1

    .line 149
    :goto_1
    iget-object v1, p0, Ldfd;->bvU:[[I

    aget-object v1, v1, v2

    aget v0, v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
