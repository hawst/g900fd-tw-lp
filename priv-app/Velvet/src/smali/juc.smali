.class public final Ljuc;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eEq:F

.field private eEr:F

.field private eEs:F

.field private eEt:F

.field private eEu:[Ljud;

.field private eEv:Ljava/lang/String;

.field private eEw:Ljava/lang/String;

.field private eEx:Ljava/lang/String;

.field private eEy:Ljava/lang/String;

.field private eEz:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 393
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 394
    const/4 v0, 0x0

    iput v0, p0, Ljuc;->aez:I

    iput v1, p0, Ljuc;->eEq:F

    iput v1, p0, Ljuc;->eEr:F

    iput v1, p0, Ljuc;->eEs:F

    iput v1, p0, Ljuc;->eEt:F

    invoke-static {}, Ljud;->buE()[Ljud;

    move-result-object v0

    iput-object v0, p0, Ljuc;->eEu:[Ljud;

    const-string v0, ""

    iput-object v0, p0, Ljuc;->eEv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuc;->eEw:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuc;->eEx:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljuc;->eEy:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ljuc;->eEz:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljuc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljuc;->eCz:I

    .line 395
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljuc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuc;->eEq:F

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuc;->eEr:F

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuc;->eEs:F

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljuc;->eEt:F

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2b

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljuc;->eEu:[Ljud;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljud;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljuc;->eEu:[Ljud;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljud;

    invoke-direct {v3}, Ljud;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3, v4}, Ljsi;->a(Ljsr;I)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljuc;->eEu:[Ljud;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljud;

    invoke-direct {v3}, Ljud;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0, v4}, Ljsi;->a(Ljsr;I)V

    iput-object v2, p0, Ljuc;->eEu:[Ljud;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuc;->eEv:Ljava/lang/String;

    iget v0, p0, Ljuc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljuc;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuc;->eEw:Ljava/lang/String;

    iget v0, p0, Ljuc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljuc;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuc;->eEx:Ljava/lang/String;

    iget v0, p0, Ljuc;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljuc;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuc;->eEy:Ljava/lang/String;

    iget v0, p0, Ljuc;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljuc;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljuc;->eEz:I

    iget v0, p0, Ljuc;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljuc;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2b -> :sswitch_5
        0x8a -> :sswitch_6
        0x92 -> :sswitch_7
        0x9a -> :sswitch_8
        0xa2 -> :sswitch_9
        0xa8 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 417
    const/4 v0, 0x1

    iget v1, p0, Ljuc;->eEq:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 418
    const/4 v0, 0x2

    iget v1, p0, Ljuc;->eEr:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 419
    iget v0, p0, Ljuc;->eEs:F

    invoke-virtual {p1, v2, v0}, Ljsj;->a(IF)V

    .line 420
    iget v0, p0, Ljuc;->eEt:F

    invoke-virtual {p1, v3, v0}, Ljsj;->a(IF)V

    .line 421
    iget-object v0, p0, Ljuc;->eEu:[Ljud;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljuc;->eEu:[Ljud;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 422
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljuc;->eEu:[Ljud;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 423
    iget-object v1, p0, Ljuc;->eEu:[Ljud;

    aget-object v1, v1, v0

    .line 424
    if-eqz v1, :cond_0

    .line 425
    invoke-virtual {p1, v4, v2}, Ljsj;->by(II)V

    invoke-virtual {v1, p1}, Ljsr;->a(Ljsj;)V

    invoke-virtual {p1, v4, v3}, Ljsj;->by(II)V

    .line 422
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 429
    :cond_1
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 430
    const/16 v0, 0x11

    iget-object v1, p0, Ljuc;->eEv:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 432
    :cond_2
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 433
    const/16 v0, 0x12

    iget-object v1, p0, Ljuc;->eEw:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 435
    :cond_3
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 436
    const/16 v0, 0x13

    iget-object v1, p0, Ljuc;->eEx:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 438
    :cond_4
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 439
    const/16 v0, 0x14

    iget-object v1, p0, Ljuc;->eEy:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 441
    :cond_5
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 442
    const/16 v0, 0x15

    iget v1, p0, Ljuc;->eEz:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 444
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 445
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 449
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 450
    const/4 v1, 0x1

    iget v2, p0, Ljuc;->eEq:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 452
    const/4 v1, 0x2

    iget v2, p0, Ljuc;->eEr:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 454
    const/4 v1, 0x3

    iget v2, p0, Ljuc;->eEs:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 456
    const/4 v1, 0x4

    iget v2, p0, Ljuc;->eEt:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v1, v0

    .line 458
    iget-object v0, p0, Ljuc;->eEu:[Ljud;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljuc;->eEu:[Ljud;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 459
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Ljuc;->eEu:[Ljud;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 460
    iget-object v2, p0, Ljuc;->eEu:[Ljud;

    aget-object v2, v2, v0

    .line 461
    if-eqz v2, :cond_0

    .line 462
    const/4 v3, 0x5

    invoke-static {v3, v2}, Ljsj;->b(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 459
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 467
    :cond_1
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 468
    const/16 v0, 0x11

    iget-object v2, p0, Ljuc;->eEv:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 471
    :cond_2
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 472
    const/16 v0, 0x12

    iget-object v2, p0, Ljuc;->eEw:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 475
    :cond_3
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 476
    const/16 v0, 0x13

    iget-object v2, p0, Ljuc;->eEx:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 479
    :cond_4
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_5

    .line 480
    const/16 v0, 0x14

    iget-object v2, p0, Ljuc;->eEy:Ljava/lang/String;

    invoke-static {v0, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 483
    :cond_5
    iget v0, p0, Ljuc;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_6

    .line 484
    const/16 v0, 0x15

    iget v2, p0, Ljuc;->eEz:I

    invoke-static {v0, v2}, Ljsj;->bv(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 487
    :cond_6
    return v1
.end method
