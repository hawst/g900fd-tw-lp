.class public final Lijw;
.super Likh;
.source "PG"

# interfaces
.implements Ljava/util/SortedMap;


# static fields
.field private static final dDj:Ljava/util/Comparator;

.field private static final dDk:Lijw;

.field private static final serialVersionUID:J


# instance fields
.field final transient dDl:Lijj;

.field private final transient dDm:Ljava/util/Comparator;

.field private transient dDn:Lijp;

.field private transient dDo:Liki;

.field private transient dDp:Lijd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 71
    invoke-static {}, Liqa;->aYi()Liqa;

    move-result-object v0

    sput-object v0, Lijw;->dDj:Ljava/util/Comparator;

    .line 75
    new-instance v0, Lijw;

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v1

    sget-object v2, Lijw;->dDj:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lijw;-><init>(Lijj;Ljava/util/Comparator;)V

    sput-object v0, Lijw;->dDk:Lijw;

    return-void
.end method

.method constructor <init>(Lijj;Ljava/util/Comparator;)V
    .locals 0

    .prologue
    .line 417
    invoke-direct {p0}, Likh;-><init>()V

    .line 418
    iput-object p1, p0, Lijw;->dDl:Lijj;

    .line 419
    iput-object p2, p0, Lijw;->dDm:Ljava/util/Comparator;

    .line 420
    return-void
.end method

.method private a(Ljava/lang/Object;Lirk;Lirg;)I
    .locals 3

    .prologue
    .line 726
    new-instance v0, Lika;

    iget-object v1, p0, Lijw;->dDl:Lijj;

    invoke-direct {v0, p0, v1}, Lika;-><init>(Lijw;Lijj;)V

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lijw;->dDm:Ljava/util/Comparator;

    invoke-static {v0, v1, v2, p2, p3}, Lirf;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lirk;Lirg;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lijx;

    invoke-direct {v0, p1}, Lijx;-><init>(Ljava/util/Comparator;)V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method private aXe()Liki;
    .locals 3

    .prologue
    .line 527
    iget-object v0, p0, Lijw;->dDo:Liki;

    .line 528
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lijw;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lijw;->dDm:Ljava/util/Comparator;

    invoke-static {v0}, Liki;->a(Ljava/util/Comparator;)Liki;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lijw;->dDo:Liki;

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Liqn;

    new-instance v1, Lijy;

    iget-object v2, p0, Lijw;->dDl:Lijj;

    invoke-direct {v1, p0, v2}, Lijy;-><init>(Lijw;Lijj;)V

    iget-object v2, p0, Lijw;->dDm:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Liqn;-><init>(Lijj;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method static synthetic b(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 4

    .prologue
    .line 61
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    add-int/lit8 v0, v1, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Duplicate keys in mappings "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v1, -0x1

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private bn(II)Lijw;
    .locals 3

    .prologue
    .line 732
    if-ge p1, p2, :cond_0

    .line 733
    new-instance v0, Lijw;

    iget-object v1, p0, Lijw;->dDl:Lijj;

    invoke-virtual {v1, p1, p2}, Lijj;->bm(II)Lijj;

    move-result-object v1

    iget-object v2, p0, Lijw;->dDm:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lijw;-><init>(Lijj;Ljava/util/Comparator;)V

    .line 736
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lijw;->dDm:Ljava/util/Comparator;

    sget-object v0, Lijw;->dDj:Ljava/util/Comparator;

    invoke-interface {v0, v1}, Ljava/util/Comparator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lijw;->dDk:Lijw;

    goto :goto_0

    :cond_1
    new-instance v0, Lijw;

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lijw;-><init>(Lijj;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method private n(Ljava/lang/Object;Z)Lijw;
    .locals 2

    .prologue
    .line 656
    if-eqz p2, :cond_0

    .line 657
    sget-object v0, Lirk;->dGE:Lirk;

    sget-object v1, Lirg;->dGA:Lirg;

    invoke-direct {p0, p1, v0, v1}, Lijw;->a(Ljava/lang/Object;Lirk;Lirg;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 661
    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lijw;->bn(II)Lijw;

    move-result-object v0

    return-object v0

    .line 659
    :cond_0
    sget-object v0, Lirk;->dGE:Lirk;

    sget-object v1, Lirg;->dGB:Lirg;

    invoke-direct {p0, p1, v0, v1}, Lijw;->a(Ljava/lang/Object;Lirk;Lirg;)I

    move-result v0

    goto :goto_0
.end method

.method private o(Ljava/lang/Object;Z)Lijw;
    .locals 2

    .prologue
    .line 707
    if-eqz p2, :cond_0

    .line 708
    sget-object v0, Lirk;->dGE:Lirk;

    sget-object v1, Lirg;->dGB:Lirg;

    invoke-direct {p0, p1, v0, v1}, Lijw;->a(Ljava/lang/Object;Lirk;Lirg;)I

    move-result v0

    .line 712
    :goto_0
    invoke-virtual {p0}, Lijw;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lijw;->bn(II)Lijw;

    move-result-object v0

    return-object v0

    .line 710
    :cond_0
    sget-object v0, Lirk;->dGE:Lirk;

    sget-object v1, Lirg;->dGA:Lirg;

    invoke-direct {p0, p1, v0, v1}, Lijw;->a(Ljava/lang/Object;Lirk;Lirg;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method final aWH()Z
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lijw;->dDl:Lijj;

    invoke-virtual {v0}, Lijj;->aWH()Z

    move-result v0

    return v0
.end method

.method public final aWK()Lijp;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lijw;->dDn:Lijp;

    .line 467
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lijw;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lijw;->dDn:Lijp;

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Likc;

    invoke-direct {v0, p0}, Likc;-><init>(Lijw;)V

    goto :goto_0
.end method

.method public final synthetic aWL()Lijp;
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lijw;->aXe()Liki;

    move-result-object v0

    return-object v0
.end method

.method public final aWM()Lijd;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lijw;->dDp:Lijd;

    .line 554
    if-nez v0, :cond_0

    new-instance v0, Likf;

    invoke-direct {v0, p0}, Likf;-><init>(Lijw;)V

    iput-object v0, p0, Lijw;->dDp:Lijd;

    :cond_0
    return-object v0
.end method

.method final aXf()Lirv;
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Lijw;->dDl:Lijj;

    invoke-virtual {v0}, Lijj;->aWI()Lirv;

    move-result-object v0

    .line 559
    new-instance v1, Lijz;

    invoke-direct {v1, p0, v0}, Lijz;-><init>(Lijw;Lirv;)V

    return-object v1
.end method

.method public final comparator()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lijw;->dDm:Ljava/util/Comparator;

    return-object v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 449
    if-nez p1, :cond_0

    .line 450
    const/4 v0, 0x0

    .line 452
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lijw;->aXf()Lirv;

    move-result-object v0

    invoke-static {v0, p1}, Likr;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lijw;->aWK()Lijp;

    move-result-object v0

    return-object v0
.end method

.method public final firstKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 625
    invoke-virtual {p0}, Lijw;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 628
    :cond_0
    iget-object v0, p0, Lijw;->dDl:Lijj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 436
    if-nez p1, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-object v0

    .line 441
    :cond_1
    :try_start_0
    sget-object v1, Lirk;->dGE:Lirk;

    sget-object v2, Lirg;->dGC:Lirg;

    invoke-direct {p0, p1, v1, v2}, Lijw;->a(Ljava/lang/Object;Lirk;Lirg;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 445
    if-ltz v1, :cond_0

    iget-object v0, p0, Lijw;->dDl:Lijj;

    invoke-virtual {v0, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 443
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lijw;->n(Ljava/lang/Object;Z)Lijw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lijw;->aXe()Liki;

    move-result-object v0

    return-object v0
.end method

.method public final lastKey()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 633
    invoke-virtual {p0}, Lijw;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 636
    :cond_0
    iget-object v0, p0, Lijw;->dDl:Lijj;

    invoke-virtual {p0}, Lijw;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lijj;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lijw;->dDl:Lijj;

    invoke-virtual {v0}, Lijj;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lijw;->dDm:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    invoke-direct {p0, p1, v1}, Lijw;->o(Ljava/lang/Object;Z)Lijw;

    move-result-object v0

    invoke-direct {v0, p2, v2}, Lijw;->n(Ljava/lang/Object;Z)Lijw;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public final synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lijw;->o(Ljava/lang/Object;Z)Lijw;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lijw;->aWM()Lijd;

    move-result-object v0

    return-object v0
.end method

.method final writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 761
    new-instance v0, Like;

    invoke-direct {v0, p0}, Like;-><init>(Lijw;)V

    return-object v0
.end method
