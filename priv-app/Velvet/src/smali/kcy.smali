.class public final Lkcy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkcz;


# instance fields
.field private final eOW:Lkdx;

.field private final eQU:Lkcc;

.field private eSS:Lkee;


# direct methods
.method public constructor <init>(Lkcc;Lkdx;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lkcy;->eQU:Lkcc;

    .line 36
    iput-object p2, p0, Lkcy;->eOW:Lkdx;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Ljava/net/CacheRequest;)Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Lkda;

    iget-object v1, p0, Lkcy;->eSS:Lkee;

    invoke-virtual {v1}, Lkee;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iget-object v2, p0, Lkcy;->eQU:Lkcc;

    invoke-direct {v0, v1, p1, v2}, Lkda;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lkcc;)V

    return-object v0
.end method

.method public final a(Lkcw;)V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 86
    if-eqz p1, :cond_0

    .line 87
    iget-object v1, p0, Lkcy;->eSS:Lkee;

    if-eqz v1, :cond_1

    .line 88
    iget-object v1, p0, Lkcy;->eSS:Lkee;

    sget-object v2, Lkdb;->eTf:Lkdb;

    invoke-virtual {v1, v2}, Lkee;->b(Lkdb;)V

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final byC()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lkcy;->byE()V

    .line 42
    iget-object v0, p0, Lkcy;->eSS:Lkee;

    invoke-virtual {v0}, Lkee;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final byD()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lkcy;->eSS:Lkee;

    invoke-virtual {v0}, Lkee;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 68
    return-void
.end method

.method public final byE()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 46
    iget-object v0, p0, Lkcy;->eSS:Lkee;

    if-eqz v0, :cond_0

    .line 60
    :goto_0
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lkcy;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->bym()V

    .line 50
    iget-object v0, p0, Lkcy;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->eRZ:Lkcq;

    .line 51
    iget-object v1, p0, Lkcy;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRf:Lkac;

    invoke-virtual {v1}, Lkac;->bxG()I

    move-result v1

    if-ne v1, v6, :cond_1

    const-string v3, "HTTP/1.1"

    .line 52
    :goto_1
    iget-object v1, p0, Lkcy;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRc:Lkcp;

    invoke-interface {v1}, Lkcp;->getURL()Ljava/net/URL;

    move-result-object v4

    .line 53
    iget-object v1, p0, Lkcy;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->method:Ljava/lang/String;

    invoke-static {v4}, Lkcc;->e(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Lkcc;->f(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lkcy;->eQU:Lkcc;

    iget-object v5, v5, Lkcc;->eRo:Ljava/net/URI;

    invoke-virtual {v5}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lkcq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v1, p0, Lkcy;->eQU:Lkcc;

    invoke-virtual {v1}, Lkcc;->byn()Z

    move-result v1

    .line 56
    iget-object v2, p0, Lkcy;->eOW:Lkdx;

    invoke-virtual {v0}, Lkcq;->byR()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0, v1, v6}, Lkdx;->a(Ljava/util/List;ZZ)Lkee;

    move-result-object v0

    iput-object v0, p0, Lkcy;->eSS:Lkee;

    .line 59
    iget-object v0, p0, Lkcy;->eSS:Lkee;

    iget-object v1, p0, Lkcy;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRd:Lkas;

    invoke-virtual {v1}, Lkas;->getReadTimeout()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lkee;->dV(J)V

    goto :goto_0

    .line 51
    :cond_1
    const-string v3, "HTTP/1.0"

    goto :goto_1
.end method

.method public final byF()Lkcu;
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lkcy;->eSS:Lkee;

    invoke-virtual {v0}, Lkee;->bzq()Ljava/util/List;

    move-result-object v0

    .line 72
    invoke-static {v0}, Lkcq;->aH(Ljava/util/List;)Lkcq;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lkcy;->eQU:Lkcc;

    invoke-virtual {v1, v0}, Lkcc;->a(Lkcq;)V

    .line 75
    new-instance v1, Lkcu;

    iget-object v2, p0, Lkcy;->eQU:Lkcc;

    iget-object v2, v2, Lkcc;->eRo:Ljava/net/URI;

    invoke-direct {v1, v2, v0}, Lkcu;-><init>(Ljava/net/URI;Lkcq;)V

    .line 76
    const-string v0, "spdy/3"

    invoke-virtual {v1, v0}, Lkcu;->AC(Ljava/lang/String;)V

    .line 77
    return-object v1
.end method
