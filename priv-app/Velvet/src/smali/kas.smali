.class public final Lkas;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/net/URLStreamHandlerFactory;


# static fields
.field private static final ePG:Ljava/util/List;


# instance fields
.field private eOL:Ljava/net/Proxy;

.field private eOO:Ljavax/net/ssl/SSLSocketFactory;

.field private eOP:Lkap;

.field private eOQ:Ljava/util/List;

.field private final ePH:Lkax;

.field private final ePI:Lkaf;

.field private ePJ:Ljava/net/CookieHandler;

.field private ePK:Ljava/net/ResponseCache;

.field private ePL:Lkad;

.field private ePM:Z

.field private ePN:I

.field private ePO:I

.field private hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

.field private proxySelector:Ljava/net/ProxySelector;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "spdy/3"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "http/1.1"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkbt;->aG(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lkas;->ePG:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkas;->ePM:Z

    .line 61
    new-instance v0, Lkax;

    invoke-direct {v0}, Lkax;-><init>()V

    iput-object v0, p0, Lkas;->ePH:Lkax;

    .line 62
    new-instance v0, Lkaf;

    invoke-direct {v0}, Lkaf;-><init>()V

    iput-object v0, p0, Lkas;->ePI:Lkaf;

    .line 63
    return-void
.end method

.method private constructor <init>(Lkas;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkas;->ePM:Z

    .line 66
    iget-object v0, p1, Lkas;->ePH:Lkax;

    iput-object v0, p0, Lkas;->ePH:Lkax;

    .line 67
    iget-object v0, p1, Lkas;->ePI:Lkaf;

    iput-object v0, p0, Lkas;->ePI:Lkaf;

    .line 68
    return-void
.end method


# virtual methods
.method final a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;
    .locals 4

    .prologue
    .line 343
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    .line 344
    new-instance v2, Lkas;

    invoke-direct {v2, p0}, Lkas;-><init>(Lkas;)V

    iget-object v0, p0, Lkas;->eOL:Ljava/net/Proxy;

    iput-object v0, v2, Lkas;->eOL:Ljava/net/Proxy;

    iget-object v0, p0, Lkas;->proxySelector:Ljava/net/ProxySelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkas;->proxySelector:Ljava/net/ProxySelector;

    :goto_0
    iput-object v0, v2, Lkas;->proxySelector:Ljava/net/ProxySelector;

    iget-object v0, p0, Lkas;->ePJ:Ljava/net/CookieHandler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkas;->ePJ:Ljava/net/CookieHandler;

    :goto_1
    iput-object v0, v2, Lkas;->ePJ:Ljava/net/CookieHandler;

    iget-object v0, p0, Lkas;->ePK:Ljava/net/ResponseCache;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkas;->ePK:Ljava/net/ResponseCache;

    :goto_2
    iput-object v0, v2, Lkas;->ePK:Ljava/net/ResponseCache;

    iget-object v0, p0, Lkas;->eOO:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkas;->eOO:Ljavax/net/ssl/SSLSocketFactory;

    :goto_3
    iput-object v0, v2, Lkas;->eOO:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v0, p0, Lkas;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lkas;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    :goto_4
    iput-object v0, v2, Lkas;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    iget-object v0, p0, Lkas;->eOP:Lkap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lkas;->eOP:Lkap;

    :goto_5
    iput-object v0, v2, Lkas;->eOP:Lkap;

    iget-object v0, p0, Lkas;->ePL:Lkad;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lkas;->ePL:Lkad;

    :goto_6
    iput-object v0, v2, Lkas;->ePL:Lkad;

    iget-boolean v0, p0, Lkas;->ePM:Z

    iput-boolean v0, v2, Lkas;->ePM:Z

    iget-object v0, p0, Lkas;->eOQ:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lkas;->eOQ:Ljava/util/List;

    :goto_7
    iput-object v0, v2, Lkas;->eOQ:Ljava/util/List;

    iget v0, p0, Lkas;->ePN:I

    iput v0, v2, Lkas;->ePN:I

    iget v0, p0, Lkas;->ePO:I

    iput v0, v2, Lkas;->ePO:I

    .line 345
    iput-object p2, v2, Lkas;->eOL:Ljava/net/Proxy;

    .line 347
    const-string v0, "http"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lkcj;

    invoke-direct {v0, p1, v2}, Lkcj;-><init>(Ljava/net/URL;Lkas;)V

    .line 348
    :goto_8
    return-object v0

    .line 344
    :cond_0
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/net/CookieHandler;->getDefault()Ljava/net/CookieHandler;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {}, Ljava/net/ResponseCache;->getDefault()Ljava/net/ResponseCache;

    move-result-object v0

    goto :goto_2

    :cond_3
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    goto :goto_3

    :cond_4
    sget-object v0, Lkej;->eUE:Lkej;

    goto :goto_4

    :cond_5
    sget-object v0, Lkby;->eQX:Lkap;

    goto :goto_5

    :cond_6
    invoke-static {}, Lkad;->bxH()Lkad;

    move-result-object v0

    goto :goto_6

    :cond_7
    sget-object v0, Lkas;->ePG:Ljava/util/List;

    goto :goto_7

    .line 348
    :cond_8
    const-string v0, "https"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lkcm;

    invoke-direct {v0, p1, v2}, Lkcm;-><init>(Ljava/net/URL;Lkas;)V

    goto :goto_8

    .line 349
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected protocol: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/net/ResponseCache;)Lkas;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lkas;->ePK:Ljava/net/ResponseCache;

    .line 175
    return-object p0
.end method

.method public final a(Ljavax/net/ssl/HostnameVerifier;)Lkas;
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lkas;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    .line 216
    return-object p0
.end method

.method public final a(Ljavax/net/ssl/SSLSocketFactory;)Lkas;
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lkas;->eOO:Ljavax/net/ssl/SSLSocketFactory;

    .line 200
    return-object p0
.end method

.method public final aF(Ljava/util/List;)Lkas;
    .locals 4

    .prologue
    .line 302
    invoke-static {p1}, Lkbt;->aG(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 303
    const-string v1, "http/1.1"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 304
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "transports doesn\'t contain http/1.1: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 306
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 307
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "transports must not contain null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :cond_1
    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 310
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "transports contains an empty string"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :cond_2
    iput-object v0, p0, Lkas;->eOQ:Ljava/util/List;

    .line 313
    return-object p0
.end method

.method public final b(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lkas;->eOL:Ljava/net/Proxy;

    invoke-virtual {p0, p1, v0}, Lkas;->a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method public final bxK()Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lkas;->eOL:Ljava/net/Proxy;

    return-object v0
.end method

.method public final bxL()Ljava/net/CookieHandler;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lkas;->ePJ:Ljava/net/CookieHandler;

    return-object v0
.end method

.method public final bxM()Ljava/net/ResponseCache;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lkas;->ePK:Ljava/net/ResponseCache;

    return-object v0
.end method

.method public final bxN()Lkau;
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lkas;->ePK:Ljava/net/ResponseCache;

    instance-of v0, v0, Lkag;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lkas;->ePK:Ljava/net/ResponseCache;

    check-cast v0, Lkag;

    iget-object v0, v0, Lkag;->ePm:Lkau;

    .line 188
    :goto_0
    return-object v0

    .line 185
    :cond_0
    iget-object v0, p0, Lkas;->ePK:Ljava/net/ResponseCache;

    if-eqz v0, :cond_1

    .line 186
    new-instance v0, Lkco;

    iget-object v1, p0, Lkas;->ePK:Ljava/net/ResponseCache;

    invoke-direct {v0, v1}, Lkco;-><init>(Ljava/net/ResponseCache;)V

    goto :goto_0

    .line 188
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bxO()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lkas;->eOO:Ljavax/net/ssl/SSLSocketFactory;

    return-object v0
.end method

.method public final bxP()Lkap;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lkas;->eOP:Lkap;

    return-object v0
.end method

.method public final bxQ()Lkad;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lkas;->ePL:Lkad;

    return-object v0
.end method

.method public final bxR()Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lkas;->ePM:Z

    return v0
.end method

.method public final bxS()Lkax;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lkas;->ePH:Lkax;

    return-object v0
.end method

.method public final bxT()Ljava/util/List;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lkas;->eOQ:Ljava/util/List;

    return-object v0
.end method

.method public final createURLStreamHandler(Ljava/lang/String;)Ljava/net/URLStreamHandler;
    .locals 1

    .prologue
    .line 390
    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 392
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkat;

    invoke-direct {v0, p0, p1}, Lkat;-><init>(Lkas;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f(JLjava/util/concurrent/TimeUnit;)V
    .locals 5

    .prologue
    .line 76
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    if-nez p3, :cond_1

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 83
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_2
    long-to-int v0, v0

    iput v0, p0, Lkas;->ePN:I

    .line 87
    return-void
.end method

.method public final g(JLjava/util/concurrent/TimeUnit;)V
    .locals 5

    .prologue
    .line 100
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    if-nez p3, :cond_1

    .line 104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 107
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_2
    long-to-int v0, v0

    iput v0, p0, Lkas;->ePO:I

    .line 111
    return-void
.end method

.method public final getConnectTimeout()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lkas;->ePN:I

    return v0
.end method

.method public final getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lkas;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public final getProxySelector()Ljava/net/ProxySelector;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lkas;->proxySelector:Ljava/net/ProxySelector;

    return-object v0
.end method

.method public final getReadTimeout()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lkas;->ePO:I

    return v0
.end method
