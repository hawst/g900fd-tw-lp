.class public abstract Liah;
.super Landroid/transition/Transition;
.source "PG"


# static fields
.field private static final dwq:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com:google:android:googlequicksearchbox:argument:argument"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com:google:android:googlequicksearchbox:argument:prompt"

    aput-object v2, v0, v1

    sput-object v0, Liah;->dwq:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    return-void
.end method

.method private static a(Landroid/transition/TransitionValues;Z)V
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 81
    instance-of v1, v0, Ldua;

    if-nez v1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 85
    :cond_0
    check-cast v0, Ldua;

    .line 86
    iget-object v2, p0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "com:google:android:googlequicksearchbox:argument:argument"

    if-eqz p1, :cond_1

    invoke-interface {v0}, Ldua;->ahf()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajE()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    :goto_1
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v1, p0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "com:google:android:googlequicksearchbox:argument:prompt"

    invoke-interface {v0}, Ldua;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 86
    :cond_1
    invoke-interface {v0}, Ldua;->ahf()Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;Ljava/lang/CharSequence;Landroid/transition/TransitionValues;Lcom/google/android/search/shared/actions/modular/arguments/Argument;Ljava/lang/CharSequence;)Landroid/animation/Animator;
.end method

.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-static {p1, v0}, Liah;->a(Landroid/transition/TransitionValues;Z)V

    .line 40
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-static {p1, v0}, Liah;->a(Landroid/transition/TransitionValues;Z)V

    .line 35
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 46
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 60
    :cond_0
    :goto_0
    return-object v0

    .line 49
    :cond_1
    iget-object v1, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v2, "com:google:android:googlequicksearchbox:argument:argument"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 50
    iget-object v2, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "com:google:android:googlequicksearchbox:argument:argument"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    .line 51
    iget-object v2, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v3, "com:google:android:googlequicksearchbox:argument:prompt"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 52
    iget-object v3, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v5, "com:google:android:googlequicksearchbox:argument:prompt"

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    .line 54
    if-eqz v1, :cond_0

    if-eqz v4, :cond_0

    invoke-static {v1, v4}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;Lcom/google/android/search/shared/actions/modular/arguments/Argument;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move-object v0, p0

    move-object v3, p3

    .line 60
    invoke-virtual/range {v0 .. v5}, Liah;->a(Lcom/google/android/search/shared/actions/modular/arguments/Argument;Ljava/lang/CharSequence;Landroid/transition/TransitionValues;Lcom/google/android/search/shared/actions/modular/arguments/Argument;Ljava/lang/CharSequence;)Landroid/animation/Animator;

    move-result-object v0

    goto :goto_0
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Liah;->dwq:[Ljava/lang/String;

    return-object v0
.end method
