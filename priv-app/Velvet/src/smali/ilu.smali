.class final Lilu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field private dCo:Ljava/lang/Object;

.field private synthetic dDS:Lilf;

.field private dEa:Lils;

.field private dEb:Lils;

.field private dEc:Lils;

.field private dEf:I


# direct methods
.method constructor <init>(Lilf;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 400
    iput-object p1, p0, Lilu;->dDS:Lilf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    iput-object p2, p0, Lilu;->dCo:Ljava/lang/Object;

    .line 402
    invoke-static {p1}, Lilf;->c(Lilf;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lils;

    iput-object v0, p0, Lilu;->dEa:Lils;

    .line 403
    return-void
.end method

.method public constructor <init>(Lilf;Ljava/lang/Object;I)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 414
    iput-object p1, p0, Lilu;->dDS:Lilf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415
    invoke-static {p1}, Lilf;->d(Lilf;)Lipn;

    move-result-object v0

    invoke-interface {v0, p2}, Lipn;->bj(Ljava/lang/Object;)I

    move-result v1

    .line 416
    invoke-static {p3, v1}, Lifv;->bk(II)I

    .line 417
    div-int/lit8 v0, v1, 0x2

    if-lt p3, v0, :cond_0

    .line 418
    invoke-static {p1}, Lilf;->e(Lilf;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lils;

    iput-object v0, p0, Lilu;->dEc:Lils;

    .line 419
    iput v1, p0, Lilu;->dEf:I

    .line 420
    :goto_0
    add-int/lit8 v0, p3, 0x1

    if-ge p3, v1, :cond_1

    .line 421
    invoke-virtual {p0}, Lilu;->previous()Ljava/lang/Object;

    move p3, v0

    goto :goto_0

    .line 424
    :cond_0
    invoke-static {p1}, Lilf;->c(Lilf;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lils;

    iput-object v0, p0, Lilu;->dEa:Lils;

    .line 425
    :goto_1
    add-int/lit8 v0, p3, -0x1

    if-lez p3, :cond_1

    .line 426
    invoke-virtual {p0}, Lilu;->next()Ljava/lang/Object;

    move p3, v0

    goto :goto_1

    .line 429
    :cond_1
    iput-object p2, p0, Lilu;->dCo:Ljava/lang/Object;

    .line 430
    const/4 v0, 0x0

    iput-object v0, p0, Lilu;->dEb:Lils;

    .line 431
    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 493
    iget-object v0, p0, Lilu;->dDS:Lilf;

    iget-object v1, p0, Lilu;->dCo:Ljava/lang/Object;

    iget-object v2, p0, Lilu;->dEa:Lils;

    invoke-static {v0, v1, p1, v2}, Lilf;->a(Lilf;Ljava/lang/Object;Ljava/lang/Object;Lils;)Lils;

    move-result-object v0

    iput-object v0, p0, Lilu;->dEc:Lils;

    .line 494
    iget v0, p0, Lilu;->dEf:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lilu;->dEf:I

    .line 495
    const/4 v0, 0x0

    iput-object v0, p0, Lilu;->dEb:Lils;

    .line 496
    return-void
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lilu;->dEa:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPrevious()Z
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lilu;->dEc:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lilu;->dEa:Lils;

    invoke-static {v0}, Lilf;->bA(Ljava/lang/Object;)V

    .line 441
    iget-object v0, p0, Lilu;->dEa:Lils;

    iput-object v0, p0, Lilu;->dEb:Lils;

    iput-object v0, p0, Lilu;->dEc:Lils;

    .line 442
    iget-object v0, p0, Lilu;->dEa:Lils;

    iget-object v0, v0, Lils;->dEd:Lils;

    iput-object v0, p0, Lilu;->dEa:Lils;

    .line 443
    iget v0, p0, Lilu;->dEf:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lilu;->dEf:I

    .line 444
    iget-object v0, p0, Lilu;->dEb:Lils;

    iget-object v0, v0, Lils;->ciy:Ljava/lang/Object;

    return-object v0
.end method

.method public final nextIndex()I
    .locals 1

    .prologue
    .line 463
    iget v0, p0, Lilu;->dEf:I

    return v0
.end method

.method public final previous()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lilu;->dEc:Lils;

    invoke-static {v0}, Lilf;->bA(Ljava/lang/Object;)V

    .line 455
    iget-object v0, p0, Lilu;->dEc:Lils;

    iput-object v0, p0, Lilu;->dEb:Lils;

    iput-object v0, p0, Lilu;->dEa:Lils;

    .line 456
    iget-object v0, p0, Lilu;->dEc:Lils;

    iget-object v0, v0, Lils;->dEe:Lils;

    iput-object v0, p0, Lilu;->dEc:Lils;

    .line 457
    iget v0, p0, Lilu;->dEf:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lilu;->dEf:I

    .line 458
    iget-object v0, p0, Lilu;->dEb:Lils;

    iget-object v0, v0, Lils;->ciy:Ljava/lang/Object;

    return-object v0
.end method

.method public final previousIndex()I
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Lilu;->dEf:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lilu;->dEb:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 474
    iget-object v0, p0, Lilu;->dEb:Lils;

    iget-object v1, p0, Lilu;->dEa:Lils;

    if-eq v0, v1, :cond_1

    .line 475
    iget-object v0, p0, Lilu;->dEb:Lils;

    iget-object v0, v0, Lils;->dEe:Lils;

    iput-object v0, p0, Lilu;->dEc:Lils;

    .line 476
    iget v0, p0, Lilu;->dEf:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lilu;->dEf:I

    .line 480
    :goto_1
    iget-object v0, p0, Lilu;->dDS:Lilf;

    iget-object v1, p0, Lilu;->dEb:Lils;

    invoke-static {v0, v1}, Lilf;->a(Lilf;Lils;)V

    .line 481
    const/4 v0, 0x0

    iput-object v0, p0, Lilu;->dEb:Lils;

    .line 482
    return-void

    .line 473
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 478
    :cond_1
    iget-object v0, p0, Lilu;->dEb:Lils;

    iget-object v0, v0, Lils;->dEd:Lils;

    iput-object v0, p0, Lilu;->dEa:Lils;

    goto :goto_1
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lilu;->dEb:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 487
    iget-object v0, p0, Lilu;->dEb:Lils;

    iput-object p1, v0, Lils;->ciy:Ljava/lang/Object;

    .line 488
    return-void

    .line 486
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
