.class final Ldgs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic anv:Lemy;

.field private synthetic bxg:Ldgm;

.field private synthetic bxm:Z

.field private synthetic bxn:Z

.field private synthetic bxo:Ljava/lang/String;

.field private synthetic bxp:I

.field private synthetic bxq:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;


# direct methods
.method constructor <init>(Ldgm;ZZLjava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Lemy;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Ldgs;->bxg:Ldgm;

    iput-boolean p2, p0, Ldgs;->bxm:Z

    iput-boolean p3, p0, Ldgs;->bxn:Z

    iput-object p4, p0, Ldgs;->bxo:Ljava/lang/String;

    iput p5, p0, Ldgs;->bxp:I

    iput-object p6, p0, Ldgs;->bxq:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    iput-object p7, p0, Ldgs;->anv:Lemy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 167
    iget-boolean v0, p0, Ldgs;->bxm:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldgs;->bxn:Z

    if-nez v0, :cond_0

    .line 168
    iget-object v0, p0, Ldgs;->bxg:Ldgm;

    iget-object v0, v0, Ldgm;->bwZ:Ldgy;

    iget-object v1, p0, Ldgs;->bxo:Ljava/lang/String;

    iget v2, p0, Ldgs;->bxp:I

    iget-object v3, p0, Ldgs;->bxq:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    invoke-virtual {v0, v1, v2, v3}, Ldgy;->b(Ljava/lang/String;ILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v0

    .line 170
    iget-object v1, p0, Ldgs;->anv:Lemy;

    invoke-interface {v1, v0}, Lemy;->aj(Ljava/lang/Object;)Z

    .line 179
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-boolean v0, p0, Ldgs;->bxm:Z

    if-nez v0, :cond_1

    const-string v0, "queryGlobalSearch when not waiting for queries."

    .line 175
    :goto_1
    const-string v1, "Search.IcingConnection"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v1, v0, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 177
    iget-object v0, p0, Ldgs;->anv:Lemy;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lemy;->aj(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    :cond_1
    const-string v0, "queryGlobalSearch with no enabled sources"

    goto :goto_1
.end method
