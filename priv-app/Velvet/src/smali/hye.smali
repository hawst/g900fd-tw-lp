.class public Lhye;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final EA:Ljava/util/ArrayList;

.field private final duE:Lchw;

.field private duF:Lhyh;

.field private volatile duG:Ljava/util/concurrent/Future;

.field private final duH:Ljava/lang/Runnable;

.field private final duI:Ljava/lang/Object;

.field final duJ:Ljava/lang/Object;

.field mConfiguration:Ljze;

.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mPrefController:Lchr;

.field final mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lchr;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Lchw;)V
    .locals 3

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhye;->duI:Ljava/lang/Object;

    .line 96
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhye;->duJ:Ljava/lang/Object;

    .line 101
    iput-object p1, p0, Lhye;->mPrefController:Lchr;

    .line 102
    iput-object p2, p0, Lhye;->mResources:Landroid/content/res/Resources;

    .line 103
    iput-object p3, p0, Lhye;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 104
    iput-object p4, p0, Lhye;->duE:Lchw;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhye;->EA:Ljava/util/ArrayList;

    .line 109
    new-instance v0, Lhyf;

    const-string v1, "Load from shared prefs"

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-direct {v0, p0, v1, v2}, Lhyf;-><init>(Lhye;Ljava/lang/String;[I)V

    iput-object v0, p0, Lhye;->duH:Ljava/lang/Runnable;

    .line 133
    return-void
.end method

.method private static Z([B)Ljze;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 535
    if-nez p0, :cond_0

    .line 542
    :goto_0
    return-object v0

    .line 540
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljze;->aG([B)Ljze;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 542
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Lcyh;)V
    .locals 0

    .prologue
    .line 573
    if-eqz p1, :cond_0

    .line 574
    invoke-interface {p2, p0, p1}, Lcyh;->B(Ljava/lang/String;Ljava/lang/String;)Lcyh;

    .line 578
    :goto_0
    return-void

    .line 576
    :cond_0
    invoke-interface {p2, p0}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljze;Lcyh;)V
    .locals 1

    .prologue
    .line 581
    if-eqz p1, :cond_0

    .line 582
    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v0

    invoke-interface {p2, p0, v0}, Lcyh;->c(Ljava/lang/String;[B)Lcyh;

    .line 586
    :goto_0
    return-void

    .line 584
    :cond_0
    invoke-interface {p2, p0}, Lcyh;->gl(Ljava/lang/String;)Lcyh;

    goto :goto_0
.end method

.method static a(Lhyh;Landroid/content/res/Resources;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 213
    iget-object v0, p0, Lhyh;->duL:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    iget-object v0, p0, Lhyh;->duM:Ljze;

    if-eqz v0, :cond_5

    .line 224
    :goto_1
    return v2

    .line 213
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x13

    if-eq v3, v4, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v3, "2013_10_04_22_22_03"

    invoke-virtual {v3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_3

    move v3, v1

    :goto_2
    if-eqz v3, :cond_2

    const-string v4, "GStaticConfiguration"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bundled: 2013_10_04_22_22_03, pref: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pref obsolete"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "2013_10_04_22_22_03"

    invoke-virtual {v6, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v0, v3

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    .line 222
    :cond_5
    invoke-static {p1}, Lhye;->j(Landroid/content/res/Resources;)Ljze;

    move-result-object v0

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljze;

    iput-object v0, p0, Lhyh;->duM:Ljze;

    .line 223
    const-string v0, "2013_10_04_22_22_03"

    iput-object v0, p0, Lhyh;->duL:Ljava/lang/String;

    move v2, v1

    .line 224
    goto :goto_1
.end method

.method private a(Lhyh;Ldkx;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 320
    iget-object v3, p0, Lhye;->duE:Lchw;

    const-string v4, "voice_search:gstatic_experiment_url"

    invoke-virtual {v3, v4}, Lchw;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 323
    iget-object v4, p1, Lhyh;->duN:Ljava/lang/String;

    if-eqz v4, :cond_b

    if-nez v3, :cond_b

    .line 325
    iput-object v2, p1, Lhyh;->duN:Ljava/lang/String;

    .line 326
    iput-object v2, p1, Lhyh;->duO:Ljze;

    move v5, v0

    .line 332
    :goto_0
    if-eqz v3, :cond_2

    .line 333
    iget-object v4, p1, Lhyh;->duN:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 347
    :cond_0
    :goto_1
    if-nez v2, :cond_8

    .line 348
    if-eqz v5, :cond_7

    .line 353
    iget-object v1, p0, Lhye;->mResources:Landroid/content/res/Resources;

    invoke-static {p1, v1}, Lhye;->a(Lhyh;Landroid/content/res/Resources;)Z

    .line 388
    :goto_2
    return v0

    :cond_1
    move-object v2, v3

    .line 339
    goto :goto_1

    .line 344
    :cond_2
    iget-object v4, p0, Lhye;->duE:Lchw;

    const-string v6, "voice_search:gstatic_url"

    invoke-virtual {v4, v6}, Lchw;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    const/16 v6, 0x2f

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_4

    :cond_3
    const-string v4, "GStaticConfiguration"

    const-string v6, "No valid gstatic url found."

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    invoke-static {v4}, Lhye;->oB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_5

    const-string v4, "GStaticConfiguration"

    const-string v6, "No valid timestamp in gstatic url."

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    iget-object v7, p1, Lhyh;->duL:Ljava/lang/String;

    if-nez v7, :cond_6

    const-string v4, "GStaticConfiguration"

    const-string v6, "Ignore gservice update: no configuration"

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    invoke-virtual {v7, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-gez v8, :cond_0

    const-string v2, "GStaticConfiguration"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "#getNewConfigurationUrl [pref="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", gservice="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v4

    goto :goto_1

    :cond_7
    move v0, v1

    .line 356
    goto :goto_2

    .line 362
    :cond_8
    invoke-static {v2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    invoke-static {p2, v2}, Lhye;->a(Ldkx;Ljava/lang/String;)[B

    move-result-object v4

    .line 366
    if-nez v4, :cond_9

    .line 368
    const-string v0, "GStaticConfiguration"

    const-string v2, "Configuration not updated - error"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 369
    goto :goto_2

    .line 373
    :cond_9
    :try_start_0
    invoke-static {v4}, Ljze;->aG([B)Ljze;

    move-result-object v4

    .line 375
    if-eqz v3, :cond_a

    .line 377
    iput-object v3, p1, Lhyh;->duN:Ljava/lang/String;

    .line 378
    iput-object v4, p1, Lhyh;->duO:Ljze;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 386
    :catch_0
    move-exception v0

    .line 387
    const-string v2, "GStaticConfiguration"

    const-string v3, "Downloaded Configuration cannot be parsed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 388
    goto/16 :goto_2

    .line 381
    :cond_a
    :try_start_1
    invoke-static {v2}, Lhye;->oB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lhyh;->duL:Ljava/lang/String;

    .line 382
    iput-object v4, p1, Lhyh;->duM:Ljze;
    :try_end_1
    .catch Ljsq; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :cond_b
    move v5, v1

    goto/16 :goto_0
.end method

.method private static a(Ldkx;Ljava/lang/String;)[B
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 459
    :try_start_0
    new-instance v1, Ldlb;

    invoke-direct {v1, p1}, Ldlb;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Ldkx;->c(Ldlb;I)[B
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 465
    :goto_0
    return-object v0

    .line 461
    :catch_0
    move-exception v1

    const-string v1, "GStaticConfiguration"

    const-string v2, "HTTPException error in updating the configuration"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 464
    :catch_1
    move-exception v1

    const-string v1, "GStaticConfiguration"

    const-string v2, "IOException error in updating the configuration"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static j(Landroid/content/res/Resources;)Ljze;
    .locals 4

    .prologue
    .line 270
    const/4 v1, 0x0

    .line 272
    const v0, 0x7f080002

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 273
    invoke-static {v1}, Liso;->j(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 277
    invoke-static {v0}, Ljze;->aG([B)Ljze;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 282
    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    return-object v0

    .line 278
    :catch_0
    move-exception v0

    .line 280
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Unable to load from asset"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lisq;->b(Ljava/io/Closeable;)V

    throw v0
.end method

.method public static oB(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 437
    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 438
    if-ne v1, v3, :cond_1

    .line 453
    :cond_0
    :goto_0
    return-object v0

    .line 443
    :cond_1
    const/16 v2, 0x5f

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 444
    if-eq v1, v3, :cond_0

    .line 447
    add-int/lit8 v1, v1, 0x1

    .line 451
    add-int/lit8 v2, v1, 0x13

    :try_start_0
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 453
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method final PM()V
    .locals 3

    .prologue
    .line 154
    invoke-static {}, Lenu;->auQ()V

    .line 155
    iget-object v0, p0, Lhye;->duJ:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 157
    iget-object v0, p0, Lhye;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyo;

    .line 158
    iget-object v2, p0, Lhye;->mConfiguration:Ljze;

    invoke-interface {v0, v2}, Lhyo;->j(Ljze;)V

    goto :goto_1

    .line 155
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 160
    :cond_1
    return-void
.end method

.method public final a(Ldkx;)V
    .locals 2

    .prologue
    .line 290
    invoke-static {}, Lenu;->auQ()V

    .line 292
    invoke-virtual {p0}, Lhye;->aTD()Lhyh;

    move-result-object v0

    .line 293
    invoke-direct {p0, v0, p1}, Lhye;->a(Lhyh;Ldkx;)Z

    move-result v1

    .line 295
    invoke-virtual {p0, v0}, Lhye;->a(Lhyh;)V

    .line 297
    if-eqz v1, :cond_0

    .line 299
    invoke-virtual {p0, v0}, Lhye;->b(Lhyh;)V

    .line 300
    invoke-virtual {p0}, Lhye;->PM()V

    .line 306
    :cond_0
    return-void
.end method

.method final a(Lhyh;)V
    .locals 3

    .prologue
    .line 309
    iget-object v0, p1, Lhyh;->duO:Ljze;

    if-nez v0, :cond_0

    iget-object v0, p1, Lhyh;->duM:Ljze;

    :goto_0
    iget-object v1, p1, Lhyh;->duP:Ljze;

    if-nez v1, :cond_1

    .line 311
    :goto_1
    iget-object v1, p0, Lhye;->duJ:Ljava/lang/Object;

    monitor-enter v1

    .line 312
    :try_start_0
    iput-object p1, p0, Lhye;->duF:Lhyh;

    .line 313
    iput-object v0, p0, Lhye;->mConfiguration:Ljze;

    .line 314
    iget-object v0, p0, Lhye;->duJ:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 315
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 309
    :cond_0
    iget-object v0, p1, Lhyh;->duO:Ljze;

    goto :goto_0

    :cond_1
    iget-object v1, p1, Lhyh;->duP:Ljze;

    new-instance v2, Ljze;

    invoke-direct {v2}, Ljze;-><init>()V

    invoke-static {v0, v2}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    invoke-static {v1, v0}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljze;

    goto :goto_1

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lhyo;)V
    .locals 2

    .prologue
    .line 138
    iget-object v1, p0, Lhye;->duI:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    iget-object v0, p0, Lhye;->duG:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 140
    iget-object v0, p0, Lhye;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 139
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final aTA()Ljze;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 175
    iget-object v1, p0, Lhye;->duJ:Ljava/lang/Object;

    monitor-enter v1

    .line 176
    :try_start_0
    iget-object v0, p0, Lhye;->mConfiguration:Ljze;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public aTB()Ljze;
    .locals 3

    .prologue
    .line 185
    iget-object v1, p0, Lhye;->duJ:Ljava/lang/Object;

    monitor-enter v1

    .line 186
    :try_start_0
    iget-object v0, p0, Lhye;->mConfiguration:Ljze;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lhye;->mConfiguration:Ljze;

    monitor-exit v1

    .line 207
    :goto_0
    return-object v0

    .line 190
    :cond_0
    const/16 v0, 0x14b

    invoke-static {v0}, Lege;->ht(I)V

    .line 193
    :goto_1
    iget-object v0, p0, Lhye;->mConfiguration:Ljze;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 195
    :try_start_1
    iget-object v0, p0, Lhye;->duJ:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 197
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "GStaticConfiguration"

    const-string v2, "Interrupted waiting for configuration"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 200
    new-instance v0, Ljze;

    invoke-direct {v0}, Ljze;-><init>()V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 204
    :cond_1
    const/16 v0, 0x14c

    :try_start_3
    invoke-static {v0}, Lege;->ht(I)V

    .line 207
    iget-object v0, p0, Lhye;->mConfiguration:Ljze;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final aTC()Ljze;
    .locals 2

    .prologue
    .line 488
    invoke-virtual {p0}, Lhye;->aTD()Lhyh;

    move-result-object v0

    .line 490
    iget-object v1, v0, Lhyh;->duP:Ljze;

    if-eqz v1, :cond_0

    .line 491
    iget-object v0, v0, Lhyh;->duP:Ljze;

    .line 496
    :goto_0
    return-object v0

    .line 494
    :cond_0
    new-instance v0, Ljze;

    invoke-direct {v0}, Ljze;-><init>()V

    .line 495
    const-string v1, "override"

    iput-object v1, v0, Ljze;->auq:Ljava/lang/String;

    goto :goto_0
.end method

.method public aTD()Lhyh;
    .locals 5

    .prologue
    .line 509
    iget-object v1, p0, Lhye;->duJ:Ljava/lang/Object;

    monitor-enter v1

    .line 510
    :try_start_0
    iget-object v0, p0, Lhye;->duF:Lhyh;

    if-eqz v0, :cond_0

    .line 511
    new-instance v0, Lhyh;

    invoke-direct {v0}, Lhyh;-><init>()V

    .line 512
    iget-object v2, p0, Lhye;->duF:Lhyh;

    invoke-virtual {v0, v2}, Lhyh;->c(Lhyh;)V

    .line 513
    monitor-exit v1

    .line 529
    :goto_0
    return-object v0

    .line 515
    :cond_0
    iget-object v0, p0, Lhye;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v2

    .line 517
    new-instance v0, Lhyh;

    invoke-direct {v0}, Lhyh;-><init>()V

    .line 518
    const-string v3, "gstatic_configuration_timestamp"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lhyh;->duL:Ljava/lang/String;

    .line 520
    const-string v3, "gstatic_configuration_data"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v3

    invoke-static {v3}, Lhye;->Z([B)Ljze;

    move-result-object v3

    iput-object v3, v0, Lhyh;->duM:Ljze;

    .line 522
    const-string v3, "gstatic_configuration_expriment_url"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcyg;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lhyh;->duN:Ljava/lang/String;

    .line 524
    const-string v3, "gstatic_configuration_experiment_data"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v3

    invoke-static {v3}, Lhye;->Z([B)Ljze;

    move-result-object v3

    iput-object v3, v0, Lhyh;->duO:Ljze;

    .line 526
    const-string v3, "gstatic_configuration_override_1"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcyg;->b(Ljava/lang/String;[B)[B

    move-result-object v2

    invoke-static {v2}, Lhye;->Z([B)Ljze;

    move-result-object v2

    iput-object v2, v0, Lhyh;->duP:Ljze;

    .line 529
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aTz()V
    .locals 3

    .prologue
    .line 163
    iget-object v1, p0, Lhye;->duI:Ljava/lang/Object;

    monitor-enter v1

    .line 165
    :try_start_0
    iget-object v0, p0, Lhye;->EA:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 167
    iget-object v0, p0, Lhye;->duG:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    .line 168
    iget-object v0, p0, Lhye;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lhye;->duH:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lhye;->duG:Ljava/util/concurrent/Future;

    .line 170
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 165
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b(Lhyh;)V
    .locals 4

    .prologue
    .line 548
    iget-object v1, p0, Lhye;->duJ:Ljava/lang/Object;

    monitor-enter v1

    .line 549
    :try_start_0
    iget-object v0, p0, Lhye;->mPrefController:Lchr;

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    invoke-interface {v0}, Lcyg;->EH()Lcyh;

    move-result-object v0

    .line 557
    const-string v2, "gstatic_configuration_timestamp"

    iget-object v3, p1, Lhyh;->duL:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lhye;->a(Ljava/lang/String;Ljava/lang/String;Lcyh;)V

    .line 559
    const-string v2, "gstatic_configuration_data"

    iget-object v3, p1, Lhyh;->duM:Ljze;

    invoke-static {v2, v3, v0}, Lhye;->a(Ljava/lang/String;Ljze;Lcyh;)V

    .line 561
    const-string v2, "gstatic_configuration_expriment_url"

    iget-object v3, p1, Lhyh;->duN:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lhye;->a(Ljava/lang/String;Ljava/lang/String;Lcyh;)V

    .line 563
    const-string v2, "gstatic_configuration_experiment_data"

    iget-object v3, p1, Lhyh;->duO:Ljze;

    invoke-static {v2, v3, v0}, Lhye;->a(Ljava/lang/String;Ljze;Lcyh;)V

    .line 565
    const-string v2, "gstatic_configuration_override_1"

    iget-object v3, p1, Lhyh;->duP:Ljze;

    invoke-static {v2, v3, v0}, Lhye;->a(Ljava/lang/String;Ljze;Lcyh;)V

    .line 568
    invoke-interface {v0}, Lcyh;->apply()V

    .line 569
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final getTimestamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, Lhye;->aTD()Lhyh;

    move-result-object v0

    iget-object v0, v0, Lhyh;->duL:Ljava/lang/String;

    return-object v0
.end method

.method public final l(Ljze;)V
    .locals 4
    .param p1    # Ljze;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 473
    invoke-virtual {p0}, Lhye;->aTD()Lhyh;

    move-result-object v0

    .line 474
    iput-object p1, v0, Lhyh;->duP:Ljze;

    .line 478
    iget-object v1, p0, Lhye;->mResources:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lhye;->a(Lhyh;Landroid/content/res/Resources;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 479
    invoke-virtual {p0, v0}, Lhye;->b(Lhyh;)V

    .line 482
    :cond_0
    invoke-virtual {p0, v0}, Lhye;->a(Lhyh;)V

    .line 484
    iget-object v0, p0, Lhye;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lhyg;

    const-string v2, "Notify listener"

    const/4 v3, 0x0

    new-array v3, v3, [I

    invoke-direct {v1, p0, v2, v3}, Lhyg;-><init>(Lhye;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 485
    return-void
.end method
