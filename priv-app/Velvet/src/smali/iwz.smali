.class public final Liwz;
.super Ljsl;
.source "PG"


# instance fields
.field private aeB:Ljbp;

.field private dMH:[Liwx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58660
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 58661
    iput-object v1, p0, Liwz;->aeB:Ljbp;

    invoke-static {}, Liwx;->aZA()[Liwx;

    move-result-object v0

    iput-object v0, p0, Liwz;->dMH:[Liwx;

    iput-object v1, p0, Liwz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liwz;->eCz:I

    .line 58662
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58637
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liwz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Liwz;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Liwz;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Liwz;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Liwz;->dMH:[Liwx;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liwx;

    if-eqz v0, :cond_2

    iget-object v3, p0, Liwz;->dMH:[Liwx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Liwx;

    invoke-direct {v3}, Liwx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Liwz;->dMH:[Liwx;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Liwx;

    invoke-direct {v3}, Liwx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Liwz;->dMH:[Liwx;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 58675
    iget-object v0, p0, Liwz;->aeB:Ljbp;

    if-eqz v0, :cond_0

    .line 58676
    const/4 v0, 0x1

    iget-object v1, p0, Liwz;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 58678
    :cond_0
    iget-object v0, p0, Liwz;->dMH:[Liwx;

    if-eqz v0, :cond_2

    iget-object v0, p0, Liwz;->dMH:[Liwx;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 58679
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Liwz;->dMH:[Liwx;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 58680
    iget-object v1, p0, Liwz;->dMH:[Liwx;

    aget-object v1, v1, v0

    .line 58681
    if-eqz v1, :cond_1

    .line 58682
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 58679
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58686
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 58687
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 58691
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 58692
    iget-object v1, p0, Liwz;->aeB:Ljbp;

    if-eqz v1, :cond_0

    .line 58693
    const/4 v1, 0x1

    iget-object v2, p0, Liwz;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 58696
    :cond_0
    iget-object v1, p0, Liwz;->dMH:[Liwx;

    if-eqz v1, :cond_3

    iget-object v1, p0, Liwz;->dMH:[Liwx;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 58697
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Liwz;->dMH:[Liwx;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 58698
    iget-object v2, p0, Liwz;->dMH:[Liwx;

    aget-object v2, v2, v0

    .line 58699
    if-eqz v2, :cond_1

    .line 58700
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 58697
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 58705
    :cond_3
    return v0
.end method
