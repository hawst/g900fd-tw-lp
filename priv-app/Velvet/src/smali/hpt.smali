.class public final Lhpt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajr;


# instance fields
.field private synthetic dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lhpt;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final z(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 269
    if-eqz p1, :cond_1

    .line 270
    new-instance v0, Laiw;

    invoke-direct {v0}, Laiw;-><init>()V

    .line 271
    invoke-virtual {v0, p1}, Laiw;->parse(Ljava/lang/String;)V

    .line 272
    iget-object v1, p0, Lhpt;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    invoke-virtual {v1, v0}, Lhpn;->b(Laiw;)V

    .line 274
    iget-object v1, p0, Lhpt;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    iget-object v1, v1, Lhpn;->mReminderAction:Lcom/google/android/search/shared/actions/SetReminderAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/SetReminderAction;->aiu()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Ldxx;->a(Laiw;J)Landroid/text/format/Time;

    move-result-object v0

    .line 276
    if-eqz v0, :cond_0

    .line 277
    iget-object v1, p0, Lhpt;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v1, v1, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    iget v2, v0, Landroid/text/format/Time;->year:I

    iget v3, v0, Landroid/text/format/Time;->month:I

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v1, v2, v3, v0}, Lhpn;->r(III)V

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v0, p0, Lhpt;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->mPresenter:Lhpn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhpn;->b(Laiw;)V

    goto :goto_0
.end method
