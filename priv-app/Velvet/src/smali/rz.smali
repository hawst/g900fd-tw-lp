.class public final Lrz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic yG:Lcom/android/launcher3/AppsCustomizePagedView;

.field private synthetic yH:Landroid/appwidget/AppWidgetProviderInfo;

.field private synthetic yJ:Lacy;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/AppsCustomizePagedView;Landroid/appwidget/AppWidgetProviderInfo;Lacy;)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lrz;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    iput-object p2, p0, Lrz;->yH:Landroid/appwidget/AppWidgetProviderInfo;

    iput-object p3, p0, Lrz;->yJ:Lacy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 534
    iget-object v0, p0, Lrz;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    iget v0, v0, Lcom/android/launcher3/AppsCustomizePagedView;->yu:I

    if-eq v0, v6, :cond_0

    .line 553
    :goto_0
    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lrz;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {v0}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/AppsCustomizePagedView;)Lcom/android/launcher3/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hr()Lyw;

    move-result-object v0

    iget-object v1, p0, Lrz;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lrz;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    iget v2, v2, Lcom/android/launcher3/AppsCustomizePagedView;->yv:I

    iget-object v3, p0, Lrz;->yH:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {v0, v1, v2, v3}, Lyw;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v0

    .line 539
    iget-object v1, p0, Lrz;->yJ:Lacy;

    iput-object v0, v1, Lacy;->RR:Landroid/appwidget/AppWidgetHostView;

    .line 540
    iget-object v1, p0, Lrz;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    const/4 v2, 0x2

    iput v2, v1, Lcom/android/launcher3/AppsCustomizePagedView;->yu:I

    .line 541
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetHostView;->setVisibility(I)V

    .line 542
    iget-object v1, p0, Lrz;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/AppsCustomizePagedView;)Lcom/android/launcher3/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->ho()Lcom/android/launcher3/Workspace;

    move-result-object v1

    iget-object v2, p0, Lrz;->yJ:Lacy;

    iget v2, v2, Lacy;->AY:I

    iget-object v3, p0, Lrz;->yJ:Lacy;

    iget v3, v3, Lacy;->AZ:I

    iget-object v4, p0, Lrz;->yJ:Lacy;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/launcher3/Workspace;->a(IILwq;Z)[I

    move-result-object v1

    .line 547
    new-instance v2, Lcom/android/launcher3/DragLayer$LayoutParams;

    aget v3, v1, v5

    aget v1, v1, v6

    invoke-direct {v2, v3, v1}, Lcom/android/launcher3/DragLayer$LayoutParams;-><init>(II)V

    .line 549
    iput v5, v2, Lcom/android/launcher3/DragLayer$LayoutParams;->y:I

    iput v5, v2, Lcom/android/launcher3/DragLayer$LayoutParams;->x:I

    .line 550
    iput-boolean v6, v2, Lcom/android/launcher3/DragLayer$LayoutParams;->FC:Z

    .line 551
    invoke-virtual {v0, v2}, Landroid/appwidget/AppWidgetHostView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 552
    iget-object v1, p0, Lrz;->yG:Lcom/android/launcher3/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher3/AppsCustomizePagedView;->a(Lcom/android/launcher3/AppsCustomizePagedView;)Lcom/android/launcher3/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher3/Launcher;->hn()Lcom/android/launcher3/DragLayer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher3/DragLayer;->addView(Landroid/view/View;)V

    goto :goto_0
.end method
