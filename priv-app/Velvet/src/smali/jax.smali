.class public final Ljax;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field public dXu:[Ljhg;

.field public dXv:[Ljay;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50179
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 50180
    const/4 v0, 0x0

    iput v0, p0, Ljax;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljax;->afh:Ljava/lang/String;

    invoke-static {}, Ljhg;->blD()[Ljhg;

    move-result-object v0

    iput-object v0, p0, Ljax;->dXu:[Ljhg;

    invoke-static {}, Ljay;->beF()[Ljay;

    move-result-object v0

    iput-object v0, p0, Ljax;->dXv:[Ljay;

    const/4 v0, 0x0

    iput-object v0, p0, Ljax;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljax;->eCz:I

    .line 50181
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49985
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljax;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljax;->afh:Ljava/lang/String;

    iget v0, p0, Ljax;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljax;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljax;->dXu:[Ljhg;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhg;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljax;->dXu:[Ljhg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljhg;

    invoke-direct {v3}, Ljhg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljax;->dXu:[Ljhg;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljhg;

    invoke-direct {v3}, Ljhg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljax;->dXu:[Ljhg;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljax;->dXv:[Ljay;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljay;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljax;->dXv:[Ljay;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Ljay;

    invoke-direct {v3}, Ljay;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljax;->dXv:[Ljay;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Ljay;

    invoke-direct {v3}, Ljay;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljax;->dXv:[Ljay;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50196
    iget v0, p0, Ljax;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 50197
    const/4 v0, 0x1

    iget-object v2, p0, Ljax;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 50199
    :cond_0
    iget-object v0, p0, Ljax;->dXu:[Ljhg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljax;->dXu:[Ljhg;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 50200
    :goto_0
    iget-object v2, p0, Ljax;->dXu:[Ljhg;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 50201
    iget-object v2, p0, Ljax;->dXu:[Ljhg;

    aget-object v2, v2, v0

    .line 50202
    if-eqz v2, :cond_1

    .line 50203
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 50200
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50207
    :cond_2
    iget-object v0, p0, Ljax;->dXv:[Ljay;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljax;->dXv:[Ljay;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 50208
    :goto_1
    iget-object v0, p0, Ljax;->dXv:[Ljay;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 50209
    iget-object v0, p0, Ljax;->dXv:[Ljay;

    aget-object v0, v0, v1

    .line 50210
    if-eqz v0, :cond_3

    .line 50211
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 50208
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 50215
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 50216
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 50220
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 50221
    iget v2, p0, Ljax;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 50222
    const/4 v2, 0x1

    iget-object v3, p0, Ljax;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 50225
    :cond_0
    iget-object v2, p0, Ljax;->dXu:[Ljhg;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljax;->dXu:[Ljhg;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 50226
    :goto_0
    iget-object v3, p0, Ljax;->dXu:[Ljhg;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 50227
    iget-object v3, p0, Ljax;->dXu:[Ljhg;

    aget-object v3, v3, v0

    .line 50228
    if-eqz v3, :cond_1

    .line 50229
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 50226
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 50234
    :cond_3
    iget-object v2, p0, Ljax;->dXv:[Ljay;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljax;->dXv:[Ljay;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 50235
    :goto_1
    iget-object v2, p0, Ljax;->dXv:[Ljay;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 50236
    iget-object v2, p0, Ljax;->dXv:[Ljay;

    aget-object v2, v2, v1

    .line 50237
    if-eqz v2, :cond_4

    .line 50238
    const/4 v3, 0x3

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 50235
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 50243
    :cond_5
    return v0
.end method
