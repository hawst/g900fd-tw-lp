.class public final Laih;
.super Laif;
.source "PG"


# instance fields
.field private Uw:I

.field private zi:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;II)V
    .locals 1

    .prologue
    .line 327
    const/16 v0, 0x400

    invoke-direct {p0, v0}, Laif;-><init>(I)V

    .line 328
    iput-object p1, p0, Laih;->zi:Landroid/content/res/Resources;

    .line 329
    iput p2, p0, Laih;->Uw:I

    .line 330
    return-void
.end method

.method private kp()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Laih;->zi:Landroid/content/res/Resources;

    iget v1, p0, Laih;->Uw:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 333
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v1
.end method


# virtual methods
.method public final a(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Laih;->zi:Landroid/content/res/Resources;

    iget v1, p0, Laih;->Uw:I

    invoke-static {v0, v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lqp;)Z
    .locals 3

    .prologue
    .line 355
    :try_start_0
    invoke-direct {p0}, Laih;->kp()Ljava/io/InputStream;

    move-result-object v0

    .line 356
    invoke-virtual {p1, v0}, Lqp;->a(Ljava/io/InputStream;)V

    .line 357
    invoke-static {v0}, Lqm;->a(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    const/4 v0, 0x1

    .line 361
    :goto_0
    return v0

    .line 359
    :catch_0
    move-exception v0

    .line 360
    const-string v1, "BitmapRegionTileSource"

    const-string v2, "Error reading resource"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 361
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lP()Laik;
    .locals 2

    .prologue
    .line 337
    invoke-direct {p0}, Laih;->kp()Ljava/io/InputStream;

    move-result-object v1

    .line 338
    const/4 v0, 0x0

    invoke-static {v1, v0}, Lail;->a(Ljava/io/InputStream;Z)Lail;

    move-result-object v0

    .line 340
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V

    .line 341
    if-nez v0, :cond_0

    .line 342
    invoke-direct {p0}, Laih;->kp()Ljava/io/InputStream;

    move-result-object v1

    .line 343
    invoke-static {v1}, Laij;->d(Ljava/io/InputStream;)Laij;

    move-result-object v0

    .line 344
    invoke-static {v1}, Lqm;->a(Ljava/io/Closeable;)V

    .line 346
    :cond_0
    return-object v0
.end method
