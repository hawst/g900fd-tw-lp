.class public final Lapw;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private alX:Ljava/lang/String;

.field private alY:Ljava/lang/String;

.field private alZ:Z

.field private ama:Z

.field public amb:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13664
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 13665
    iput v1, p0, Lapw;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lapw;->alX:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lapw;->alY:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lapw;->afh:Ljava/lang/String;

    iput-boolean v1, p0, Lapw;->alZ:Z

    iput-boolean v1, p0, Lapw;->ama:Z

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lapw;->amb:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lapw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lapw;->eCz:I

    .line 13666
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13538
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lapw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapw;->alX:Ljava/lang/String;

    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapw;->alY:Ljava/lang/String;

    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapw;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapw;->afh:Ljava/lang/String;

    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lapw;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lapw;->alZ:Z

    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lapw;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lapw;->ama:Z

    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lapw;->aez:I

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lapw;->amb:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lapw;->amb:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lapw;->amb:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lapw;->amb:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 13684
    iget v0, p0, Lapw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 13685
    const/4 v0, 0x1

    iget-object v1, p0, Lapw;->alX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13687
    :cond_0
    iget v0, p0, Lapw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 13688
    const/4 v0, 0x2

    iget-object v1, p0, Lapw;->alY:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13690
    :cond_1
    iget v0, p0, Lapw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 13691
    const/4 v0, 0x3

    iget-object v1, p0, Lapw;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13693
    :cond_2
    iget v0, p0, Lapw;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 13694
    const/4 v0, 0x4

    iget-boolean v1, p0, Lapw;->alZ:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13696
    :cond_3
    iget v0, p0, Lapw;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 13697
    const/4 v0, 0x5

    iget-boolean v1, p0, Lapw;->ama:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 13699
    :cond_4
    iget-object v0, p0, Lapw;->amb:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lapw;->amb:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 13700
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lapw;->amb:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 13701
    iget-object v1, p0, Lapw;->amb:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 13702
    if-eqz v1, :cond_5

    .line 13703
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 13700
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 13707
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 13708
    return-void
.end method

.method public final bm(Z)Lapw;
    .locals 1

    .prologue
    .line 13629
    iput-boolean p1, p0, Lapw;->alZ:Z

    .line 13630
    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lapw;->aez:I

    .line 13631
    return-object p0
.end method

.method public final bn(Z)Lapw;
    .locals 1

    .prologue
    .line 13648
    iput-boolean p1, p0, Lapw;->ama:Z

    .line 13649
    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lapw;->aez:I

    .line 13650
    return-object p0
.end method

.method public final dU(Ljava/lang/String;)Lapw;
    .locals 1

    .prologue
    .line 13563
    if-nez p1, :cond_0

    .line 13564
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13566
    :cond_0
    iput-object p1, p0, Lapw;->alX:Ljava/lang/String;

    .line 13567
    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapw;->aez:I

    .line 13568
    return-object p0
.end method

.method public final dV(Ljava/lang/String;)Lapw;
    .locals 1

    .prologue
    .line 13585
    if-nez p1, :cond_0

    .line 13586
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13588
    :cond_0
    iput-object p1, p0, Lapw;->alY:Ljava/lang/String;

    .line 13589
    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapw;->aez:I

    .line 13590
    return-object p0
.end method

.method public final dW(Ljava/lang/String;)Lapw;
    .locals 1

    .prologue
    .line 13607
    if-nez p1, :cond_0

    .line 13608
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13610
    :cond_0
    iput-object p1, p0, Lapw;->afh:Ljava/lang/String;

    .line 13611
    iget v0, p0, Lapw;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lapw;->aez:I

    .line 13612
    return-object p0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13604
    iget-object v0, p0, Lapw;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 13712
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 13713
    iget v2, p0, Lapw;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 13714
    const/4 v2, 0x1

    iget-object v3, p0, Lapw;->alX:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 13717
    :cond_0
    iget v2, p0, Lapw;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 13718
    const/4 v2, 0x2

    iget-object v3, p0, Lapw;->alY:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 13721
    :cond_1
    iget v2, p0, Lapw;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 13722
    const/4 v2, 0x3

    iget-object v3, p0, Lapw;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 13725
    :cond_2
    iget v2, p0, Lapw;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    .line 13726
    const/4 v2, 0x4

    iget-boolean v3, p0, Lapw;->alZ:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 13729
    :cond_3
    iget v2, p0, Lapw;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_4

    .line 13730
    const/4 v2, 0x5

    iget-boolean v3, p0, Lapw;->ama:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 13733
    :cond_4
    iget-object v2, p0, Lapw;->amb:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lapw;->amb:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    move v3, v1

    .line 13736
    :goto_0
    iget-object v4, p0, Lapw;->amb:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 13737
    iget-object v4, p0, Lapw;->amb:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 13738
    if-eqz v4, :cond_5

    .line 13739
    add-int/lit8 v3, v3, 0x1

    .line 13740
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 13736
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13744
    :cond_6
    add-int/2addr v0, v2

    .line 13745
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 13747
    :cond_7
    return v0
.end method

.method public final pb()Z
    .locals 1

    .prologue
    .line 13615
    iget v0, p0, Lapw;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13560
    iget-object v0, p0, Lapw;->alX:Ljava/lang/String;

    return-object v0
.end method

.method public final tE()Z
    .locals 1

    .prologue
    .line 13571
    iget v0, p0, Lapw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13582
    iget-object v0, p0, Lapw;->alY:Ljava/lang/String;

    return-object v0
.end method

.method public final tG()Z
    .locals 1

    .prologue
    .line 13593
    iget v0, p0, Lapw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tH()Z
    .locals 1

    .prologue
    .line 13626
    iget-boolean v0, p0, Lapw;->alZ:Z

    return v0
.end method

.method public final tI()Z
    .locals 1

    .prologue
    .line 13645
    iget-boolean v0, p0, Lapw;->ama:Z

    return v0
.end method
