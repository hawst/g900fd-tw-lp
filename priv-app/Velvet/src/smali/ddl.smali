.class public final Lddl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcox;


# static fields
.field private static final bua:Ljava/lang/Object;


# instance fields
.field private final aTW:Ljava/util/concurrent/Executor;

.field private final bub:Ljava/util/List;

.field private final buc:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mSettings:Lcke;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lddl;->bua:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcke;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lddl;->mSettings:Lcke;

    .line 48
    iput-object p2, p0, Lddl;->aTW:Ljava/util/concurrent/Executor;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lddl;->bub:Ljava/util/List;

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lddl;->buc:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 52
    invoke-direct {p0}, Lddl;->aaV()V

    .line 53
    return-void
.end method

.method private a(Ljji;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lddl;->mSettings:Lcke;

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcke;->r([B)V

    .line 80
    iget-object v0, p0, Lddl;->buc:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 81
    return-void
.end method

.method private aaV()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 100
    iget-object v0, p0, Lddl;->buc:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lddl;->aTW:Ljava/util/concurrent/Executor;

    new-instance v1, Lddm;

    invoke-direct {v1, p0, v2}, Lddm;-><init>(Lddl;B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public final Ez()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x4

    return v0
.end method

.method public final a(Ljje;Z)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p1, Ljje;->eoE:Ljji;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p1, Ljje;->eoE:Ljji;

    invoke-direct {p0, v0}, Lddl;->a(Ljji;)V

    .line 69
    :cond_0
    return-void
.end method

.method public final aC(Lcom/google/android/shared/search/Query;)Ldef;
    .locals 4

    .prologue
    .line 91
    invoke-direct {p0}, Lddl;->aaV()V

    .line 93
    sget-object v1, Lddl;->bua:Ljava/lang/Object;

    monitor-enter v1

    .line 94
    :try_start_0
    new-instance v0, Ldei;

    const-string v2, "action-discovery-suggest"

    iget-object v3, p0, Lddl;->bub:Ljava/util/List;

    invoke-direct {v0, v2, p1, v3}, Ldei;-><init>(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;)V

    .line 95
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final aaW()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 113
    iget-object v0, p0, Lddl;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Ox()[B

    move-result-object v0

    .line 116
    if-eqz v0, :cond_2

    .line 118
    :try_start_0
    invoke-static {v0}, Ljji;->aq([B)Ljji;

    move-result-object v0

    .line 119
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, v0, Ljji;->eoM:[Ljjj;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    iget-object v7, v6, Ljjj;->eoR:[Ljjk;

    array-length v8, v7

    move v0, v1

    :goto_1
    if-ge v0, v8, :cond_0

    aget-object v9, v7, v0

    invoke-virtual {v9}, Ljjk;->getText()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9}, Ljjk;->bnT()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Ljjj;->ps()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lded;

    invoke-direct {v12}, Lded;-><init>()V

    iput-object v10, v12, Lded;->bux:Ljava/lang/CharSequence;

    iput-object v9, v12, Lded;->buy:Ljava/lang/CharSequence;

    iput-object v11, v12, Lded;->buA:Ljava/lang/String;

    iput-object v10, v12, Lded;->buJ:Ljava/lang/String;

    const-string v9, "action-discovery"

    iput-object v9, v12, Lded;->buw:Ljava/lang/String;

    const/16 v9, 0x50

    invoke-virtual {v12, v9}, Lded;->fI(I)Lded;

    move-result-object v9

    invoke-virtual {v9}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    sget-object v1, Lddl;->bua:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lddl;->bub:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lddl;->bub:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    :cond_2
    :goto_2
    return-void

    .line 119
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljsq; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public final ci(Z)V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ljji;

    invoke-direct {v0}, Ljji;-><init>()V

    invoke-direct {p0, v0}, Lddl;->a(Ljji;)V

    .line 75
    return-void
.end method
