.class final Lcra;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final bgN:Ljava/util/Map;

.field final bgO:Ljava/util/Map;

.field final dK:Ljava/lang/Object;

.field final mClock:Lemp;

.field private final mGsaConfig:Lchk;


# direct methods
.method public constructor <init>(Lchk;Lemp;)V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcra;->dK:Ljava/lang/Object;

    .line 123
    iput-object p1, p0, Lcra;->mGsaConfig:Lchk;

    .line 124
    iput-object p2, p0, Lcra;->mClock:Lemp;

    .line 125
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcra;->bgN:Ljava/util/Map;

    .line 126
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcra;->bgO:Ljava/util/Map;

    .line 127
    return-void
.end method


# virtual methods
.method public final get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .prologue
    .line 171
    iget-object v1, p0, Lcra;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 172
    :try_start_0
    iget-object v0, p0, Lcra;->bgN:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 173
    if-eqz v0, :cond_1

    .line 174
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrb;

    .line 175
    if-eqz v0, :cond_1

    .line 176
    iget-object v2, p0, Lcra;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, v0, Lcrb;->bgQ:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v7, p0, Lcra;->mGsaConfig:Lchk;

    invoke-virtual {v7}, Lchk;->Ja()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    sub-long v2, v4, v2

    .line 177
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 180
    iget-object v2, v0, Lcrb;->bgS:Lbfb;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcrb;->bgS:Lbfb;

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 180
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcrb;->bgR:Ljava/lang/String;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :goto_0
    return-object v0

    .line 187
    :cond_1
    monitor-exit v1

    .line 188
    const/4 v0, 0x0

    goto :goto_0
.end method
