.class final Lhtz;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lhto;


# instance fields
.field private final apc:Landroid/widget/TextView;

.field private final dqP:Lhuc;

.field dqQ:Lhtl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-virtual {p0}, Lhtz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 33
    const v1, 0x7f040098

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 35
    const v0, 0x7f1101f5

    invoke-virtual {p0, v0}, Lhtz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lhua;

    invoke-direct {v1, p0}, Lhua;-><init>(Lhtz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    const v0, 0x7f110140

    invoke-virtual {p0, v0}, Lhtz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lhub;

    invoke-direct {v1, p0}, Lhub;-><init>(Lhtz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    const v0, 0x7f1100d7

    invoke-virtual {p0, v0}, Lhtz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhtz;->apc:Landroid/widget/TextView;

    .line 52
    new-instance v0, Lhuc;

    invoke-direct {v0, p0}, Lhuc;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lhtz;->dqP:Lhuc;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Lhtl;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lhtz;->dqQ:Lhtl;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 67
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtl;

    iput-object v0, p0, Lhtz;->dqQ:Lhtl;

    .line 68
    return-void

    .line 65
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aQo()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lhtz;->dqP:Lhuc;

    invoke-virtual {v0}, Lhuc;->aQo()V

    .line 73
    return-void
.end method

.method public final aQp()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lhtz;->dqP:Lhuc;

    invoke-virtual {v0}, Lhuc;->aQp()V

    .line 78
    return-void
.end method

.method public final l(Lcom/google/android/search/shared/contact/Contact;)V
    .locals 2

    .prologue
    .line 57
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-static {}, Lenu;->auR()V

    .line 60
    iget-object v0, p0, Lhtz;->apc:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/Contact;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    return-void
.end method

.method public final oa(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lhtz;->dqP:Lhuc;

    invoke-virtual {v0, p1}, Lhuc;->oa(Ljava/lang/String;)V

    .line 83
    return-void
.end method
