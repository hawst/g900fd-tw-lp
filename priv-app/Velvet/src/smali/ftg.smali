.class public final Lftg;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 43
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 13

    .prologue
    .line 52
    iget-object v5, p0, Lfro;->mView:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lfro;->cBc:Lanh;

    iget-object v6, v0, Lanh;->ahc:Lanu;

    .line 55
    invoke-virtual {v6}, Lanu;->pb()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const v0, 0x7f1101a9

    invoke-virtual {v6}, Lanu;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v0, v1}, Lgab;->e(Landroid/view/View;ILjava/lang/String;)Landroid/widget/TextView;

    .line 59
    :cond_0
    invoke-virtual {v6}, Lanu;->hasText()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    invoke-virtual {v6}, Lanu;->getText()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Lfth;

    invoke-direct {v2}, Lfth;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v1

    .line 61
    const v0, 0x7f1100bc

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;

    .line 62
    iget-object v2, p0, Lftg;->mCardContainer:Lfmt;

    invoke-interface {v2}, Lfmt;->aAD()Lfml;

    move-result-object v2

    iget-object v2, v2, Lfml;->cvY:Lfnh;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->a(Ljava/lang/CharSequence;Lesm;)V

    .line 63
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/TextViewWithImages;->setVisibility(I)V

    .line 68
    :cond_1
    iget-object v0, v6, Lanu;->aiF:[Ljcn;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 70
    invoke-virtual {v6}, Lanu;->pT()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v6}, Lanu;->pS()I

    move-result v0

    .line 72
    :goto_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    .line 74
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    const v0, 0x7f1101eb

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    move-object v1, v0

    .line 78
    :goto_1
    iget-object v0, v6, Lanu;->aiF:[Ljcn;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    .line 79
    invoke-static {v0}, Lgbm;->c(Ljcn;)Ljava/lang/String;

    move-result-object v2

    .line 80
    if-eqz v2, :cond_4

    .line 81
    iget-object v4, p0, Lftg;->mCardContainer:Lfmt;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v0}, Ljcn;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Ljcn;->getHeight()I

    move-result v3

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setVisibility(I)V

    invoke-interface {v4}, Lfmt;->aAD()Lfml;

    move-result-object v0

    iget-object v0, v0, Lfml;->cvY:Lfnh;

    invoke-virtual {v1, v7, v0}, Lcom/google/android/search/shared/ui/WebImageView;->a(Landroid/net/Uri;Lesm;)V

    if-gtz v2, :cond_3

    if-lez v3, :cond_4

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/search/shared/ui/WebImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-lez v2, :cond_9

    :goto_2
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    if-lez v3, :cond_a

    move v2, v3

    :goto_3
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-virtual {v1, v0}, Lcom/google/android/search/shared/ui/WebImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    :cond_4
    invoke-virtual {v6}, Lanu;->pU()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v6}, Lanu;->getStyle()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    .line 94
    invoke-virtual {v6}, Lanu;->getStyle()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    const v0, 0x7f02002e

    invoke-virtual {v5, v0}, Landroid/view/View;->setBackgroundResource(I)V

    const v0, 0x7f1101a9

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lesp;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_5

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTextAlignment(I)V

    :cond_5
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lftg;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0259

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->ahs:Lani;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lftg;->mContext:Landroid/content/Context;

    const v2, 0x7f0900ea

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 96
    :cond_6
    :goto_4
    return-void

    .line 70
    :cond_7
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 74
    :cond_8
    const v0, 0x7f1101ed

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/ui/WebImageView;

    move-object v1, v0

    goto/16 :goto_1

    .line 81
    :cond_9
    const/4 v2, -0x2

    goto :goto_2

    :cond_a
    const/4 v2, -0x2

    goto :goto_3

    .line 85
    :cond_b
    const/4 v1, 0x3

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    move v2, v0

    .line 87
    :goto_5
    iget-object v7, p0, Lftg;->mCardContainer:Lfmt;

    invoke-virtual {p0}, Lftg;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    iget-object v0, v6, Lanu;->aiF:[Ljcn;

    array-length v0, v0

    if-lez v0, :cond_4

    const v0, 0x7f1101ee

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v3, 0x0

    iget-object v9, v6, Lanu;->aiF:[Ljcn;

    array-length v10, v9

    const/4 v1, 0x0

    move v4, v1

    move v1, v3

    :goto_6
    if-ge v4, v10, :cond_4

    aget-object v11, v9, v4

    if-ge v1, v2, :cond_d

    add-int/lit8 v3, v1, 0x1

    const v1, 0x7f040091

    const/4 v12, 0x0

    invoke-virtual {v8, v1, v0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/search/shared/ui/WebImageView;

    invoke-static {v11}, Lgbm;->c(Ljcn;)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_c

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-interface {v7}, Lfmt;->aAD()Lfml;

    move-result-object v12

    iget-object v12, v12, Lfml;->cvY:Lfnh;

    invoke-virtual {v1, v11, v12}, Lcom/google/android/search/shared/ui/WebImageView;->a(Landroid/net/Uri;Lesm;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_c
    move v1, v3

    :cond_d
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    .line 85
    :cond_e
    const/4 v0, 0x4

    move v2, v0

    goto :goto_5

    .line 94
    :cond_f
    iget-object v1, p0, Lftg;->mContext:Landroid/content/Context;

    const v2, 0x7f0900e9

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_4
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 47
    const v0, 0x7f04010f

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
