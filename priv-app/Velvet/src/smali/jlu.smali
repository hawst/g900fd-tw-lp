.class public final Ljlu;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile esP:[Ljlu;


# instance fields
.field private aez:I

.field private esQ:[B

.field private esR:Ljava/lang/String;

.field private esS:Ljly;

.field private esT:Ljlx;

.field private esU:Ljlw;

.field private esV:Ljlv;

.field private esW:Ljlz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15073
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 15074
    const/4 v0, 0x0

    iput v0, p0, Ljlu;->aez:I

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljlu;->esQ:[B

    const-string v0, ""

    iput-object v0, p0, Ljlu;->esR:Ljava/lang/String;

    iput-object v1, p0, Ljlu;->esS:Ljly;

    iput-object v1, p0, Ljlu;->esT:Ljlx;

    iput-object v1, p0, Ljlu;->esU:Ljlw;

    iput-object v1, p0, Ljlu;->esV:Ljlv;

    iput-object v1, p0, Ljlu;->esW:Ljlz;

    iput-object v1, p0, Ljlu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljlu;->eCz:I

    .line 15075
    return-void
.end method

.method public static bpv()[Ljlu;
    .locals 2

    .prologue
    .line 15001
    sget-object v0, Ljlu;->esP:[Ljlu;

    if-nez v0, :cond_1

    .line 15002
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 15004
    :try_start_0
    sget-object v0, Ljlu;->esP:[Ljlu;

    if-nez v0, :cond_0

    .line 15005
    const/4 v0, 0x0

    new-array v0, v0, [Ljlu;

    sput-object v0, Ljlu;->esP:[Ljlu;

    .line 15007
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 15009
    :cond_1
    sget-object v0, Ljlu;->esP:[Ljlu;

    return-object v0

    .line 15007
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 13810
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljlu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljlu;->esQ:[B

    iget v0, p0, Ljlu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljlu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljlu;->esR:Ljava/lang/String;

    iget v0, p0, Ljlu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljlu;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljlu;->esS:Ljly;

    if-nez v0, :cond_1

    new-instance v0, Ljly;

    invoke-direct {v0}, Ljly;-><init>()V

    iput-object v0, p0, Ljlu;->esS:Ljly;

    :cond_1
    iget-object v0, p0, Ljlu;->esS:Ljly;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljlu;->esT:Ljlx;

    if-nez v0, :cond_2

    new-instance v0, Ljlx;

    invoke-direct {v0}, Ljlx;-><init>()V

    iput-object v0, p0, Ljlu;->esT:Ljlx;

    :cond_2
    iget-object v0, p0, Ljlu;->esT:Ljlx;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljlu;->esU:Ljlw;

    if-nez v0, :cond_3

    new-instance v0, Ljlw;

    invoke-direct {v0}, Ljlw;-><init>()V

    iput-object v0, p0, Ljlu;->esU:Ljlw;

    :cond_3
    iget-object v0, p0, Ljlu;->esU:Ljlw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljlu;->esV:Ljlv;

    if-nez v0, :cond_4

    new-instance v0, Ljlv;

    invoke-direct {v0}, Ljlv;-><init>()V

    iput-object v0, p0, Ljlu;->esV:Ljlv;

    :cond_4
    iget-object v0, p0, Ljlu;->esV:Ljlv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljlu;->esW:Ljlz;

    if-nez v0, :cond_5

    new-instance v0, Ljlz;

    invoke-direct {v0}, Ljlz;-><init>()V

    iput-object v0, p0, Ljlu;->esW:Ljlz;

    :cond_5
    iget-object v0, p0, Ljlu;->esW:Ljlz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 15094
    iget v0, p0, Ljlu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 15095
    const/4 v0, 0x1

    iget-object v1, p0, Ljlu;->esQ:[B

    invoke-virtual {p1, v0, v1}, Ljsj;->c(I[B)V

    .line 15097
    :cond_0
    iget v0, p0, Ljlu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 15098
    const/4 v0, 0x2

    iget-object v1, p0, Ljlu;->esR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 15100
    :cond_1
    iget-object v0, p0, Ljlu;->esS:Ljly;

    if-eqz v0, :cond_2

    .line 15101
    const/4 v0, 0x3

    iget-object v1, p0, Ljlu;->esS:Ljly;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 15103
    :cond_2
    iget-object v0, p0, Ljlu;->esT:Ljlx;

    if-eqz v0, :cond_3

    .line 15104
    const/4 v0, 0x4

    iget-object v1, p0, Ljlu;->esT:Ljlx;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 15106
    :cond_3
    iget-object v0, p0, Ljlu;->esU:Ljlw;

    if-eqz v0, :cond_4

    .line 15107
    const/4 v0, 0x5

    iget-object v1, p0, Ljlu;->esU:Ljlw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 15109
    :cond_4
    iget-object v0, p0, Ljlu;->esV:Ljlv;

    if-eqz v0, :cond_5

    .line 15110
    const/4 v0, 0x6

    iget-object v1, p0, Ljlu;->esV:Ljlv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 15112
    :cond_5
    iget-object v0, p0, Ljlu;->esW:Ljlz;

    if-eqz v0, :cond_6

    .line 15113
    const/4 v0, 0x7

    iget-object v1, p0, Ljlu;->esW:Ljlz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 15115
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 15116
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 15120
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 15121
    iget v1, p0, Ljlu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 15122
    const/4 v1, 0x1

    iget-object v2, p0, Ljlu;->esQ:[B

    invoke-static {v1, v2}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 15125
    :cond_0
    iget v1, p0, Ljlu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 15126
    const/4 v1, 0x2

    iget-object v2, p0, Ljlu;->esR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15129
    :cond_1
    iget-object v1, p0, Ljlu;->esS:Ljly;

    if-eqz v1, :cond_2

    .line 15130
    const/4 v1, 0x3

    iget-object v2, p0, Ljlu;->esS:Ljly;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15133
    :cond_2
    iget-object v1, p0, Ljlu;->esT:Ljlx;

    if-eqz v1, :cond_3

    .line 15134
    const/4 v1, 0x4

    iget-object v2, p0, Ljlu;->esT:Ljlx;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15137
    :cond_3
    iget-object v1, p0, Ljlu;->esU:Ljlw;

    if-eqz v1, :cond_4

    .line 15138
    const/4 v1, 0x5

    iget-object v2, p0, Ljlu;->esU:Ljlw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15141
    :cond_4
    iget-object v1, p0, Ljlu;->esV:Ljlv;

    if-eqz v1, :cond_5

    .line 15142
    const/4 v1, 0x6

    iget-object v2, p0, Ljlu;->esV:Ljlv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15145
    :cond_5
    iget-object v1, p0, Ljlu;->esW:Ljlz;

    if-eqz v1, :cond_6

    .line 15146
    const/4 v1, 0x7

    iget-object v2, p0, Ljlu;->esW:Ljlz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15149
    :cond_6
    return v0
.end method
