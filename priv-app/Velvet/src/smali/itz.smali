.class public final Litz;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dJu:[Litz;


# instance fields
.field private aez:I

.field private dJv:I

.field private dJw:I

.field private dJx:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3055
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3056
    iput v0, p0, Litz;->aez:I

    iput v0, p0, Litz;->dJv:I

    iput v0, p0, Litz;->dJw:I

    iput v0, p0, Litz;->dJx:I

    const/4 v0, 0x0

    iput-object v0, p0, Litz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Litz;->eCz:I

    .line 3057
    return-void
.end method

.method public static aZa()[Litz;
    .locals 2

    .prologue
    .line 2985
    sget-object v0, Litz;->dJu:[Litz;

    if-nez v0, :cond_1

    .line 2986
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2988
    :try_start_0
    sget-object v0, Litz;->dJu:[Litz;

    if-nez v0, :cond_0

    .line 2989
    const/4 v0, 0x0

    new-array v0, v0, [Litz;

    sput-object v0, Litz;->dJu:[Litz;

    .line 2991
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2993
    :cond_1
    sget-object v0, Litz;->dJu:[Litz;

    return-object v0

    .line 2991
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2923
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Litz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litz;->dJv:I

    iget v0, p0, Litz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litz;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litz;->dJw:I

    iget v0, p0, Litz;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litz;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Litz;->dJx:I

    iget v0, p0, Litz;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Litz;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3072
    iget v0, p0, Litz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3073
    const/4 v0, 0x1

    iget v1, p0, Litz;->dJv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3075
    :cond_0
    iget v0, p0, Litz;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3076
    const/4 v0, 0x2

    iget v1, p0, Litz;->dJw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3078
    :cond_1
    iget v0, p0, Litz;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3079
    const/4 v0, 0x3

    iget v1, p0, Litz;->dJx:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3081
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3082
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3086
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3087
    iget v1, p0, Litz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3088
    const/4 v1, 0x1

    iget v2, p0, Litz;->dJv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3091
    :cond_0
    iget v1, p0, Litz;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3092
    const/4 v1, 0x2

    iget v2, p0, Litz;->dJw:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3095
    :cond_1
    iget v1, p0, Litz;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 3096
    const/4 v1, 0x3

    iget v2, p0, Litz;->dJx:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3099
    :cond_2
    return v0
.end method
