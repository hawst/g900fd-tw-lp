.class public final Lgtq;
.super Lgrs;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lchk;Z)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lgrs;-><init>(Landroid/content/res/Resources;Lchk;Z)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Ljrc;Z)Ljqb;
    .locals 10

    .prologue
    .line 35
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lgtq;->a(Ljrc;I)Ljqg;

    move-result-object v1

    .line 36
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lgtq;->a(Ljrc;I)Ljqg;

    move-result-object v7

    .line 38
    if-eqz v1, :cond_0

    if-nez v7, :cond_3

    .line 39
    :cond_0
    const-string v2, "SmsActionPromptsBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Missing argument: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_1

    const-string v0, "+"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "recipient "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz v7, :cond_2

    const-string v0, "+"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    const/4 v0, 0x0

    .line 78
    :goto_2
    return-object v0

    .line 39
    :cond_1
    const-string v0, "-"

    goto :goto_0

    :cond_2
    const-string v0, "-"

    goto :goto_1

    .line 44
    :cond_3
    new-instance v6, Ljqb;

    invoke-direct {v6}, Ljqb;-><init>()V

    .line 46
    invoke-static {v6, v1}, Lgtq;->a(Ljqb;Ljqg;)Ljqh;

    move-result-object v8

    sget-object v9, Ljqj;->eyK:Ljsm;

    sget-object v3, Lgqu;->cTo:Lgtl;

    sget-object v4, Lgqu;->cTi:Lgra;

    const/4 v5, 0x2

    move-object v0, p0

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lgtq;->a(Ljqg;ZLgtl;Lgra;I)Ljqj;

    move-result-object v0

    invoke-virtual {v8, v9, v0}, Ljqh;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 52
    iget-boolean v0, p0, Lgrs;->cNN:Z

    if-eqz v0, :cond_4

    .line 53
    new-instance v0, Ljqp;

    invoke-direct {v0}, Ljqp;-><init>()V

    .line 54
    const/4 v2, 0x1

    new-array v2, v2, [Ljrg;

    const/4 v3, 0x0

    const v4, 0x7f0a0602

    const v5, 0x7f0a0609

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-array v9, v9, [Ljqu;

    invoke-virtual {p0, v4, v5, v8, v9}, Lgtq;->a(IIZ[Ljqu;)Ljrg;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljrg;->rL(I)Ljrg;

    move-result-object v4

    aput-object v4, v2, v3

    iput-object v2, v0, Ljqp;->ezf:[Ljrg;

    .line 59
    invoke-static {v6, v7}, Lgtq;->a(Ljqb;Ljqg;)Ljqh;

    move-result-object v2

    sget-object v3, Ljqp;->eze:Ljsm;

    invoke-virtual {v2, v3, v0}, Ljqh;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 62
    sget-object v0, Lgqu;->cTz:Lgqy;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lgqy;->aS(Ljava/lang/String;Ljava/lang/String;)Lgtl;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x2

    new-array v3, v3, [Ljqu;

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-static {v1, v5}, Lgtq;->a(Ljqg;I)Ljqu;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    invoke-static {v7}, Lgtq;->c(Ljqg;)Ljqu;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, p2, v2, v3}, Lgtq;->a(Lgtl;ZZ[Ljqu;)[Ljrg;

    move-result-object v0

    iput-object v0, v6, Ljqb;->eyf:[Ljrg;

    .line 68
    :cond_4
    const v0, 0x7f0a060a

    const v1, 0x7f0a060b

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljqu;

    invoke-virtual {p0, v0, v1, v2, v3}, Lgtq;->a(IIZ[Ljqu;)Ljrg;

    move-result-object v0

    iput-object v0, v6, Ljqb;->eyg:Ljrg;

    .line 73
    const v0, 0x7f0a0969

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljqu;

    invoke-virtual {p0, v0, v1, v2, v3}, Lgtq;->a(IIZ[Ljqu;)Ljrg;

    move-result-object v0

    iput-object v0, v6, Ljqb;->eyh:Ljrg;

    move-object v0, v6

    .line 78
    goto/16 :goto_2
.end method
