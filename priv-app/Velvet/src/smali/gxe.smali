.class public Lgxe;
.super Lgyy;
.source "PG"


# instance fields
.field private final cYi:Lgxh;

.field private cYj:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cYk:Landroid/util/SparseIntArray;

.field private final cYl:Ljava/util/Comparator;

.field private final cYm:Landroid/database/DataSetObserver;

.field private final mCorpora:Lcfu;


# direct methods
.method public constructor <init>(Lcfu;Lgxh;)V
    .locals 1

    .prologue
    .line 61
    const-string v0, "footer"

    invoke-direct {p0, v0}, Lgyy;-><init>(Ljava/lang/String;)V

    .line 30
    new-instance v0, Lgxf;

    invoke-direct {v0, p0}, Lgxf;-><init>(Lgxe;)V

    iput-object v0, p0, Lgxe;->cYl:Ljava/util/Comparator;

    .line 53
    new-instance v0, Lgxg;

    invoke-direct {v0, p0}, Lgxg;-><init>(Lgxe;)V

    iput-object v0, p0, Lgxe;->cYm:Landroid/database/DataSetObserver;

    .line 62
    iput-object p2, p0, Lgxe;->cYi:Lgxh;

    .line 63
    iput-object p1, p0, Lgxe;->mCorpora:Lcfu;

    .line 64
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0, p0}, Lgxh;->c(Lgxe;)V

    .line 65
    return-void
.end method

.method static synthetic a(Lgxe;)Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lgxe;->cYk:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method private aLo()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0}, Lgxh;->aLq()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0}, Lgxh;->aLs()V

    .line 105
    iget-object v0, p0, Lgxe;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->ED()Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lgxe;->cYk:Landroid/util/SparseIntArray;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lgxe;->cYl:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 109
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfy;

    .line 110
    invoke-virtual {v0}, Lcfy;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 111
    iget-object v2, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v2, v0}, Lgxh;->b(Lcfy;)V

    goto :goto_0

    .line 115
    :cond_2
    return-void
.end method

.method static synthetic b(Lgxe;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lgxe;->aLo()V

    return-void
.end method


# virtual methods
.method protected final X(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lgxe;->mCorpora:Lcfu;

    iget-object v1, p0, Lgxe;->cYm:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcfu;->registerObserver(Ljava/lang/Object;)V

    .line 142
    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0, p1}, Lgxh;->Z(Landroid/os/Bundle;)V

    .line 145
    :cond_0
    return-void
.end method

.method public final Y(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0, p1}, Lgxh;->aa(Landroid/os/Bundle;)V

    .line 155
    return-void
.end method

.method public final a(Lcfy;)V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0, p1}, Lgxh;->c(Lcfy;)V

    .line 127
    invoke-virtual {p0}, Lgxe;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iget-object v2, p1, Lcfy;->aqV:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/shared/search/Query;->kU(Ljava/lang/String;)Lcom/google/android/shared/search/Query;

    move-result-object v1

    .line 130
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 131
    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->y(Lcom/google/android/shared/search/Query;)V

    .line 133
    :cond_0
    return-void
.end method

.method protected final aKF()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lgxe;->mCorpora:Lcfu;

    iget-object v1, p0, Lgxe;->cYm:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcfu;->unregisterObserver(Ljava/lang/Object;)V

    .line 150
    return-void
.end method

.method public final aLp()V
    .locals 3

    .prologue
    .line 118
    invoke-direct {p0}, Lgxe;->aLo()V

    .line 119
    iget-object v0, p0, Lgxe;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->EB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    iget-object v1, p0, Lgxe;->mCorpora:Lcfu;

    iget-object v2, p0, Lgxe;->cYj:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcfu;->gi(Ljava/lang/String;)Lcfy;

    move-result-object v1

    invoke-interface {v0, v1}, Lgxh;->c(Lcfy;)V

    .line 122
    :cond_0
    return-void
.end method

.method public final fV(Z)V
    .locals 3

    .prologue
    .line 68
    invoke-virtual {p0}, Lgxe;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apU()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgxe;->cYj:Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0, p1}, Lgxh;->fW(Z)V

    .line 71
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0}, Lgxh;->aLq()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    if-eqz p1, :cond_1

    .line 73
    iget-object v0, p0, Lgxe;->mCorpora:Lcfu;

    invoke-virtual {v0}, Lcfu;->EB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    iget-object v1, p0, Lgxe;->mCorpora:Lcfu;

    iget-object v2, p0, Lgxe;->cYj:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcfu;->gi(Ljava/lang/String;)Lcfy;

    move-result-object v1

    invoke-interface {v0, v1}, Lgxh;->c(Lcfy;)V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lgxe;->cYi:Lgxh;

    invoke-interface {v0}, Lgxh;->aLr()V

    goto :goto_0
.end method

.method public final t([I)V
    .locals 4
    .param p1    # [I
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lgxe;->cYk:Landroid/util/SparseIntArray;

    .line 100
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lgxe;->cYk:Landroid/util/SparseIntArray;

    if-nez v0, :cond_1

    .line 93
    new-instance v0, Landroid/util/SparseIntArray;

    array-length v1, p1

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lgxe;->cYk:Landroid/util/SparseIntArray;

    .line 95
    :cond_1
    iget-object v0, p0, Lgxe;->cYk:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 96
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 97
    iget-object v1, p0, Lgxe;->cYk:Landroid/util/SparseIntArray;

    aget v2, p1, v0

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 99
    :cond_2
    invoke-direct {p0}, Lgxe;->aLo()V

    goto :goto_0
.end method
