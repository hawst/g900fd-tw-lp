.class public final Lfud;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field bLI:Ljava/lang/String;

.field private final cCc:Lixn;

.field cCd:I

.field kO:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mFifeImageUrlUtil:Lgan;

.field private final mPlaceDataHelper:Lgbe;

.field private final mRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lixn;Lgbe;Lgan;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lfud;->mContext:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lfud;->mRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    .line 38
    iput-object p3, p0, Lfud;->cCc:Lixn;

    .line 39
    iput-object p4, p0, Lfud;->mPlaceDataHelper:Lgbe;

    .line 40
    iput-object p5, p0, Lfud;->mFifeImageUrlUtil:Lgan;

    .line 41
    return-void
.end method


# virtual methods
.method public final aDt()Laoc;
    .locals 10

    .prologue
    const v9, 0x7f0d015b

    .line 60
    new-instance v0, Laoc;

    invoke-direct {v0}, Laoc;-><init>()V

    .line 63
    iget-object v1, p0, Lfud;->bLI:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lfud;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laoc;->bs(Ljava/lang/String;)Laoc;

    .line 68
    :cond_0
    iget v1, p0, Lfud;->cCd:I

    if-lez v1, :cond_1

    .line 69
    iget-object v1, p0, Lfud;->mRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-static {v1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->m(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/renderingcontext/NavigationContext;->aCF()I

    move-result v1

    .line 71
    iget-object v2, p0, Lfud;->mContext:Landroid/content/Context;

    iget v3, p0, Lfud;->cCd:I

    invoke-static {v2, v3, v1}, Lgay;->a(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laoc;->bu(Ljava/lang/String;)Laoc;

    .line 76
    :cond_1
    iget-object v1, p0, Lfud;->cCc:Lixn;

    if-eqz v1, :cond_2

    .line 77
    iget-object v1, p0, Lfud;->mPlaceDataHelper:Lgbe;

    iget-object v1, p0, Lfud;->cCc:Lixn;

    iget-object v2, p0, Lfud;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lgbe;->a(Lixn;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_2

    .line 80
    invoke-virtual {v0, v1}, Laoc;->bv(Ljava/lang/String;)Laoc;

    .line 84
    :cond_2
    iget-object v1, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v1}, Lixn;->bay()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v1}, Lixn;->baw()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v1}, Lixn;->baA()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 87
    :cond_3
    new-instance v1, Laok;

    invoke-direct {v1}, Laok;-><init>()V

    .line 90
    iget-object v2, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v2}, Lixn;->bay()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 91
    iget-object v2, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v2}, Lixn;->bax()I

    move-result v2

    invoke-static {v2}, Lgbe;->jR(I)D

    move-result-wide v2

    .line 93
    invoke-static {v2, v3}, Lgbe;->g(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Laok;->bU(Ljava/lang/String;)Laok;

    .line 94
    invoke-static {v2, v3}, Lgbe;->h(D)I

    move-result v2

    invoke-virtual {v1, v2}, Laok;->cE(I)Laok;

    .line 98
    :cond_4
    const/4 v2, 0x2

    invoke-static {v2}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 99
    iget-object v3, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v3}, Lixn;->baw()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 100
    iget-object v3, p0, Lfud;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100036

    iget-object v5, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v5}, Lixn;->bav()I

    move-result v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v8}, Lixn;->bav()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_5
    iget-object v3, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v3}, Lixn;->baA()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 104
    iget-object v3, p0, Lfud;->cCc:Lixn;

    invoke-virtual {v3}, Lixn;->baz()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    :cond_6
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_7

    .line 107
    const-string v3, " \u00b7 "

    invoke-static {v3, v2}, Lgab;->e(Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laok;->bV(Ljava/lang/String;)Laok;

    .line 111
    :cond_7
    iput-object v1, v0, Laoc;->ahN:Laok;

    .line 115
    :cond_8
    iget-object v1, p0, Lfud;->kO:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 116
    iget-object v1, p0, Lfud;->kO:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laoc;->bw(Ljava/lang/String;)Laoc;

    .line 120
    :cond_9
    iget-object v1, p0, Lfud;->cCc:Lixn;

    iget-object v1, v1, Lixn;->dNR:Ljcn;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lfud;->cCc:Lixn;

    iget-object v1, v1, Lixn;->dNR:Ljcn;

    invoke-virtual {v1}, Ljcn;->qG()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 121
    iget-object v1, p0, Lfud;->mFifeImageUrlUtil:Lgan;

    iget-object v2, p0, Lfud;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lfud;->cCc:Lixn;

    iget-object v3, v3, Lixn;->dNR:Ljcn;

    invoke-virtual {v1, v2, v3, v9, v9}, Lgan;->a(Landroid/content/Context;Ljcn;II)Ljava/lang/String;

    move-result-object v1

    .line 123
    if-eqz v1, :cond_a

    .line 124
    invoke-virtual {v0, v1}, Laoc;->bx(Ljava/lang/String;)Laoc;

    .line 127
    :cond_a
    return-object v0
.end method
