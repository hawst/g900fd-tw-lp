.class public final Litx;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile dJp:[Litx;


# instance fields
.field private aez:I

.field private dJq:I

.field private dJr:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2370
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2371
    iput v0, p0, Litx;->aez:I

    iput v0, p0, Litx;->dJq:I

    iput v0, p0, Litx;->dJr:I

    const/4 v0, 0x0

    iput-object v0, p0, Litx;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Litx;->eCz:I

    .line 2372
    return-void
.end method

.method public static aYZ()[Litx;
    .locals 2

    .prologue
    .line 2319
    sget-object v0, Litx;->dJp:[Litx;

    if-nez v0, :cond_1

    .line 2320
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2322
    :try_start_0
    sget-object v0, Litx;->dJp:[Litx;

    if-nez v0, :cond_0

    .line 2323
    const/4 v0, 0x0

    new-array v0, v0, [Litx;

    sput-object v0, Litx;->dJp:[Litx;

    .line 2325
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2327
    :cond_1
    sget-object v0, Litx;->dJp:[Litx;

    return-object v0

    .line 2325
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2302
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Litx;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Litx;->dJq:I

    iget v0, p0, Litx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litx;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Litx;->dJr:I

    iget v0, p0, Litx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litx;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2386
    iget v0, p0, Litx;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2387
    const/4 v0, 0x1

    iget v1, p0, Litx;->dJq:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2389
    :cond_0
    iget v0, p0, Litx;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2390
    const/4 v0, 0x2

    iget v1, p0, Litx;->dJr:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2392
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2393
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2397
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2398
    iget v1, p0, Litx;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2399
    const/4 v1, 0x1

    iget v2, p0, Litx;->dJq:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2402
    :cond_0
    iget v1, p0, Litx;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2403
    const/4 v1, 0x2

    iget v2, p0, Litx;->dJr:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2406
    :cond_1
    return v0
.end method

.method public final mO(I)Litx;
    .locals 1

    .prologue
    .line 2338
    iput p1, p0, Litx;->dJq:I

    .line 2339
    iget v0, p0, Litx;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Litx;->aez:I

    .line 2340
    return-object p0
.end method

.method public final mP(I)Litx;
    .locals 1

    .prologue
    .line 2357
    iput p1, p0, Litx;->dJr:I

    .line 2358
    iget v0, p0, Litx;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Litx;->aez:I

    .line 2359
    return-object p0
.end method
