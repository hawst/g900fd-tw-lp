.class final Lhbw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcik;


# instance fields
.field private synthetic dcc:Lhbq;


# direct methods
.method constructor <init>(Lhbq;)V
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lhbw;->dcc:Lhbq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final addInAppUrlPattern(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lhbw;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbW:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 456
    return-void
.end method

.method public final addOptionsMenuItem(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 464
    :try_start_0
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 465
    if-eqz p4, :cond_0

    iget-object v0, p0, Lhbw;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->mUrlHelper:Lcpn;

    invoke-virtual {v0, v4}, Lcpn;->q(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    .line 466
    :goto_0
    new-instance v0, Lhbr;

    iget-object v1, p0, Lhbw;->dcc:Lhbq;

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lhbr;-><init>(Lhbq;Ljava/lang/String;ILandroid/net/Uri;Z)V

    .line 468
    iget-object v1, p0, Lhbw;->dcc:Lhbq;

    iget-object v1, v1, Lhbq;->dbX:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 473
    :goto_1
    return-void

    :cond_0
    move v5, v6

    .line 465
    goto :goto_0

    .line 470
    :catch_0
    move-exception v0

    const-string v0, "Velvet.WebViewPageController"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring Menu Item with invalid URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lcpn;->hB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final delayedPageLoad()V
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lhbw;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbV:Lhby;

    invoke-virtual {v0}, Lhby;->delayedPageLoad()V

    .line 436
    return-void
.end method

.method public final j(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 449
    iget-object v0, p0, Lhbw;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbU:Lhbx;

    new-instance v1, Lcom/google/android/velvet/presenter/inappwebpage/Request;

    invoke-static {}, Lijp;->aXb()Lijp;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/google/android/velvet/presenter/inappwebpage/Request;-><init>(Landroid/net/Uri;Lijp;)V

    invoke-interface {v0, v1}, Lhbx;->b(Lcom/google/android/velvet/presenter/inappwebpage/Request;)V

    .line 450
    return-void
.end method

.method public final pageReady()V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lhbw;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbV:Lhby;

    invoke-virtual {v0}, Lhby;->aNr()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lhbw;->dcc:Lhbq;

    iget-object v0, v0, Lhbq;->dbU:Lhbx;

    invoke-interface {v0}, Lhbx;->pageReady()V

    .line 444
    :cond_0
    return-void
.end method
