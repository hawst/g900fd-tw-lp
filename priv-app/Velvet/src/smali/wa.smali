.class public final Lwa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v3, 0x1

    .line 46
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Ladg;

    invoke-virtual {v0}, Ladg;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher3/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher3/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/android/launcher3/Folder;

    iget-object v5, v2, Lcom/android/launcher3/Folder;->Ho:Lcom/android/launcher3/FolderEditText;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-eq v2, v3, :cond_0

    move v2, v3

    :goto_0
    sparse-switch p2, :sswitch_data_0

    move v0, v4

    :goto_1
    return v0

    :cond_0
    move v2, v4

    goto :goto_0

    :sswitch_0
    if-eqz v2, :cond_1

    invoke-static {v1, v0, p1, v6}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    :cond_1
    move v0, v3

    goto :goto_1

    :sswitch_1
    if-eqz v2, :cond_2

    invoke-static {v1, v0, p1, v3}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :goto_2
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    goto :goto_2

    :sswitch_2
    if-eqz v2, :cond_4

    invoke-static {v1, v0, p1, v6}, Luy;->b(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    invoke-virtual {p1, v7}, Landroid/view/View;->playSoundEffect(I)V

    :cond_4
    move v0, v3

    goto :goto_1

    :sswitch_3
    if-eqz v2, :cond_5

    invoke-static {v1, v0, p1, v3}, Luy;->b(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :goto_3
    invoke-virtual {p1, v8}, Landroid/view/View;->playSoundEffect(I)V

    :cond_5
    move v0, v3

    goto :goto_1

    :cond_6
    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    :sswitch_4
    if-eqz v2, :cond_7

    invoke-static {v1, v0, v6, v3}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    invoke-virtual {p1, v7}, Landroid/view/View;->playSoundEffect(I)V

    :cond_7
    move v0, v3

    goto :goto_1

    :sswitch_5
    if-eqz v2, :cond_8

    invoke-virtual {v0}, Ladg;->getChildCount()I

    move-result v2

    invoke-static {v1, v0, v2, v6}, Luy;->a(Lcom/android/launcher3/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    invoke-virtual {p1, v8}, Landroid/view/View;->playSoundEffect(I)V

    :cond_8
    move v0, v3

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x7a -> :sswitch_4
        0x7b -> :sswitch_5
    .end sparse-switch
.end method
