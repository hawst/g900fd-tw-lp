.class public final Loi;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Lod;


# instance fields
.field private mJ:Lnd;

.field private od:I

.field private of:I

.field private oi:Ljava/lang/String;

.field private oj:Ljava/lang/String;

.field private os:Z

.field private pG:Lop;

.field private pH:Landroid/widget/TextView;

.field private pI:Landroid/widget/TextView;

.field private pJ:Landroid/widget/TextView;

.field private pK:Landroid/widget/TextView;

.field private pL:Landroid/widget/TextView;

.field private pM:Landroid/widget/TextView;

.field private pN:Landroid/view/View;

.field private pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

.field private pP:Z

.field private pQ:I

.field private pR:I

.field private pS:Z

.field private pT:C

.field private pU:Ljava/lang/String;

.field private pV:Ljava/lang/String;

.field private pW:Z

.field private pX:Ljava/util/ArrayList;

.field private pY:Loo;

.field private pZ:I

.field private qa:I

.field private qb:Ljava/lang/String;

.field private qc:Ljava/lang/String;

.field private qd:Ljava/lang/String;

.field private qe:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 129
    return-void
.end method

.method public static a(Lop;IIZ)Loi;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    new-instance v0, Loi;

    invoke-direct {v0}, Loi;-><init>()V

    .line 139
    iput-object p0, v0, Loi;->pG:Lop;

    iput p1, v0, Loi;->pQ:I

    iput p2, v0, Loi;->pR:I

    iput-boolean p3, v0, Loi;->os:Z

    iput-boolean v1, v0, Loi;->pW:Z

    iput-boolean v1, v0, Loi;->pS:Z

    .line 140
    return-object v0
.end method

.method static synthetic a(Loi;I)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Loi;->am(I)V

    return-void
.end method

.method static synthetic a(Loi;IZZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0, v1}, Loi;->b(IZZZ)V

    return-void
.end method

.method static synthetic a(Loi;Z)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Loi;->o(Z)V

    return-void
.end method

.method static synthetic a(Loi;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Loi;->pW:Z

    return v0
.end method

.method private a([Ljava/lang/Boolean;)[I
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 734
    .line 736
    iget-boolean v0, p0, Loi;->os:Z

    if-nez v0, :cond_7

    invoke-direct {p0}, Loi;->cF()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    iget-object v4, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 738
    invoke-direct {p0, v1}, Loi;->aq(I)I

    move-result v4

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_0
    move v4, v5

    move v6, v0

    :goto_1
    move v7, v3

    move v8, v3

    move v3, v4

    .line 747
    :goto_2
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v3, v0, :cond_5

    .line 748
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    iget-object v9, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    sub-int/2addr v9, v3

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Loi;->ap(I)I

    move-result v0

    .line 749
    if-ne v3, v4, :cond_2

    move v8, v0

    .line 747
    :cond_0
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 740
    :cond_1
    invoke-direct {p0, v2}, Loi;->aq(I)I

    move-result v4

    if-ne v0, v4, :cond_6

    move v0, v2

    .line 741
    goto :goto_0

    .line 751
    :cond_2
    add-int/lit8 v9, v4, 0x1

    if-ne v3, v9, :cond_3

    .line 752
    mul-int/lit8 v9, v0, 0xa

    add-int/2addr v8, v9

    .line 753
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 754
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, p1, v2

    goto :goto_3

    .line 756
    :cond_3
    add-int/lit8 v9, v4, 0x2

    if-ne v3, v9, :cond_4

    move v7, v0

    .line 757
    goto :goto_3

    .line 758
    :cond_4
    add-int/lit8 v9, v4, 0x3

    if-ne v3, v9, :cond_0

    .line 759
    mul-int/lit8 v9, v0, 0xa

    add-int/2addr v7, v9

    .line 760
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 761
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, p1, v1

    goto :goto_3

    .line 766
    :cond_5
    const/4 v0, 0x3

    new-array v0, v0, [I

    aput v7, v0, v1

    aput v8, v0, v2

    aput v6, v0, v5

    .line 767
    return-object v0

    :cond_6
    move v0, v3

    goto :goto_0

    :cond_7
    move v4, v2

    move v6, v3

    goto :goto_1
.end method

.method private am(I)V
    .locals 2

    .prologue
    .line 359
    if-nez p1, :cond_0

    .line 360
    iget-object v0, p0, Loi;->pM:Landroid/widget/TextView;

    iget-object v1, p0, Loi;->oi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v1, p0, Loi;->oi:Ljava/lang/String;

    invoke-static {v0, v1}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 362
    iget-object v0, p0, Loi;->pN:Landroid/view/View;

    iget-object v1, p0, Loi;->oi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 370
    :goto_0
    return-void

    .line 363
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 364
    iget-object v0, p0, Loi;->pM:Landroid/widget/TextView;

    iget-object v1, p0, Loi;->oj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v1, p0, Loi;->oj:Ljava/lang/String;

    invoke-static {v0, v1}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 366
    iget-object v0, p0, Loi;->pN:Landroid/view/View;

    iget-object v1, p0, Loi;->oj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 368
    :cond_1
    iget-object v0, p0, Loi;->pM:Landroid/widget/TextView;

    iget-object v1, p0, Loi;->pU:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private an(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 563
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0, v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->n(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-direct {p0, p1}, Loi;->ao(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 565
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Loi;->pW:Z

    .line 566
    iget-object v0, p0, Loi;->pH:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 567
    invoke-direct {p0, v1}, Loi;->l(Z)V

    .line 569
    :cond_1
    return-void
.end method

.method private ao(I)Z
    .locals 9

    .prologue
    const/4 v8, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 574
    iget-boolean v0, p0, Loi;->os:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Loi;->os:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Loi;->cF()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 596
    :cond_1
    :goto_0
    return v2

    .line 579
    :cond_2
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    iget-object v0, p0, Loi;->pY:Loo;

    iget-object v1, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, v1, Loo;->qh:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    iget-object v0, v1, Loo;->qh:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loo;

    move v1, v2

    :goto_2
    iget-object v7, v0, Loo;->qg:[I

    array-length v7, v7

    if-ge v1, v7, :cond_5

    iget-object v7, v0, Loo;->qg:[I

    aget v7, v7, v1

    if-ne v7, v5, :cond_4

    move v1, v3

    :goto_3
    if-eqz v1, :cond_3

    :goto_4
    if-nez v0, :cond_7

    move v0, v2

    :goto_5
    if-nez v0, :cond_9

    .line 581
    invoke-direct {p0}, Loi;->cG()I

    goto :goto_0

    .line 580
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    move-object v1, v0

    goto :goto_1

    :cond_8
    move v0, v3

    goto :goto_5

    .line 585
    :cond_9
    invoke-static {p1}, Loi;->ap(I)I

    move-result v0

    .line 586
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    const-string v4, "%d"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 588
    invoke-direct {p0}, Loi;->cF()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 589
    iget-boolean v0, p0, Loi;->os:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_a

    .line 590
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    iget-object v1, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 591
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    iget-object v1, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 593
    :cond_a
    iget-object v0, p0, Loi;->pH:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_b
    move v2, v3

    .line 596
    goto/16 :goto_0
.end method

.method private static ap(I)I
    .locals 1

    .prologue
    .line 699
    packed-switch p0, :pswitch_data_0

    .line 721
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 701
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 703
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 705
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 707
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 709
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 711
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 713
    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 715
    :pswitch_7
    const/4 v0, 0x7

    goto :goto_0

    .line 717
    :pswitch_8
    const/16 v0, 0x8

    goto :goto_0

    .line 719
    :pswitch_9
    const/16 v0, 0x9

    goto :goto_0

    .line 699
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private aq(I)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 775
    iget v0, p0, Loi;->pZ:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Loi;->qa:I

    if-ne v0, v2, :cond_1

    .line 777
    :cond_0
    invoke-static {v2}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v3

    move v0, v1

    .line 780
    :goto_0
    iget-object v4, p0, Loi;->oi:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Loi;->oj:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 781
    iget-object v4, p0, Loi;->oi:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 782
    iget-object v5, p0, Loi;->oj:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 783
    if-eq v4, v5, :cond_3

    .line 784
    new-array v0, v8, [C

    aput-char v4, v0, v1

    aput-char v5, v0, v7

    invoke-virtual {v3, v0}, Landroid/view/KeyCharacterMap;->getEvents([C)[Landroid/view/KeyEvent;

    move-result-object v0

    .line 786
    if-eqz v0, :cond_2

    array-length v3, v0

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 787
    aget-object v1, v0, v1

    invoke-virtual {v1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iput v1, p0, Loi;->pZ:I

    .line 788
    aget-object v0, v0, v8

    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    iput v0, p0, Loi;->qa:I

    .line 796
    :cond_1
    :goto_1
    if-nez p1, :cond_4

    .line 797
    iget v0, p0, Loi;->pZ:I

    .line 802
    :goto_2
    return v0

    .line 790
    :cond_2
    const-string v0, "TimePickerDialog"

    const-string v1, "Unable to find keycodes for AM and PM."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 780
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 798
    :cond_4
    if-ne p1, v7, :cond_5

    .line 799
    iget v0, p0, Loi;->qa:I

    goto :goto_2

    :cond_5
    move v0, v2

    .line 802
    goto :goto_2
.end method

.method private b(IZZZ)V
    .locals 4

    .prologue
    .line 449
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0, p1, p2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->d(IZ)V

    .line 452
    if-nez p1, :cond_3

    .line 453
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getHours()I

    move-result v0

    .line 454
    iget-boolean v1, p0, Loi;->os:Z

    if-nez v1, :cond_0

    .line 455
    rem-int/lit8 v0, v0, 0xc

    .line 457
    :cond_0
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Loi;->qb:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 458
    if-eqz p4, :cond_1

    .line 459
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v1, p0, Loi;->qc:Ljava/lang/String;

    invoke-static {v0, v1}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 461
    :cond_1
    iget-object v0, p0, Loi;->pI:Landroid/widget/TextView;

    move-object v2, v0

    .line 471
    :goto_0
    if-nez p1, :cond_5

    iget v0, p0, Loi;->of:I

    move v1, v0

    .line 472
    :goto_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_6

    iget v0, p0, Loi;->of:I

    .line 473
    :goto_2
    iget-object v3, p0, Loi;->pI:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 474
    iget-object v1, p0, Loi;->pK:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 476
    const v0, 0x3f59999a    # 0.85f

    const v1, 0x3f8ccccd    # 1.1f

    invoke-static {v2, v0, v1}, Lnf;->b(Landroid/view/View;FF)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 477
    if-eqz p3, :cond_2

    .line 478
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 480
    :cond_2
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 481
    return-void

    .line 463
    :cond_3
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getMinutes()I

    move-result v0

    .line 464
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Loi;->qd:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 465
    if-eqz p4, :cond_4

    .line 466
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v1, p0, Loi;->qe:Ljava/lang/String;

    invoke-static {v0, v1}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 468
    :cond_4
    iget-object v0, p0, Loi;->pK:Landroid/widget/TextView;

    move-object v2, v0

    goto :goto_0

    .line 471
    :cond_5
    iget v0, p0, Loi;->od:I

    move v1, v0

    goto :goto_1

    .line 472
    :cond_6
    iget v0, p0, Loi;->od:I

    goto :goto_2
.end method

.method static synthetic b(Loi;)Z
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Loi;->cF()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Loi;I)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 50
    const/16 v0, 0x6f

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Loi;->dismiss()V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x3d

    if-ne p1, v0, :cond_3

    iget-boolean v0, p0, Loi;->pW:Z

    if-eqz v0, :cond_8

    invoke-direct {p0}, Loi;->cF()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Loi;->o(Z)V

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/16 v0, 0x42

    if-ne p1, v0, :cond_7

    iget-boolean v0, p0, Loi;->pW:Z

    if-eqz v0, :cond_5

    invoke-direct {p0}, Loi;->cF()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-direct {p0, v2}, Loi;->o(Z)V

    :cond_5
    iget-object v0, p0, Loi;->pG:Lop;

    if-eqz v0, :cond_6

    iget-object v0, p0, Loi;->pG:Lop;

    iget-object v2, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v2, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getHours()I

    move-result v2

    iget-object v3, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v3}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getMinutes()I

    move-result v3

    invoke-interface {v0, v2, v3}, Lop;->t(II)V

    :cond_6
    invoke-virtual {p0}, Loi;->dismiss()V

    move v0, v1

    goto :goto_0

    :cond_7
    const/16 v0, 0x43

    if-ne p1, v0, :cond_b

    iget-boolean v0, p0, Loi;->pW:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0}, Loi;->cG()I

    move-result v0

    invoke-direct {p0, v2}, Loi;->aq(I)I

    move-result v3

    if-ne v0, v3, :cond_9

    iget-object v0, p0, Loi;->oi:Ljava/lang/String;

    :goto_1
    iget-object v3, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v4, p0, Loi;->pV:Ljava/lang/String;

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    invoke-direct {p0, v1}, Loi;->l(Z)V

    :cond_8
    move v0, v2

    goto :goto_0

    :cond_9
    invoke-direct {p0, v1}, Loi;->aq(I)I

    move-result v3

    if-ne v0, v3, :cond_a

    iget-object v0, p0, Loi;->oj:Ljava/lang/String;

    goto :goto_1

    :cond_a
    const-string v3, "%d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0}, Loi;->ap(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_b
    const/4 v0, 0x7

    if-eq p1, v0, :cond_c

    const/16 v0, 0x8

    if-eq p1, v0, :cond_c

    const/16 v0, 0x9

    if-eq p1, v0, :cond_c

    const/16 v0, 0xa

    if-eq p1, v0, :cond_c

    const/16 v0, 0xb

    if-eq p1, v0, :cond_c

    const/16 v0, 0xc

    if-eq p1, v0, :cond_c

    const/16 v0, 0xd

    if-eq p1, v0, :cond_c

    const/16 v0, 0xe

    if-eq p1, v0, :cond_c

    const/16 v0, 0xf

    if-eq p1, v0, :cond_c

    const/16 v0, 0x10

    if-eq p1, v0, :cond_c

    iget-boolean v0, p0, Loi;->os:Z

    if-nez v0, :cond_8

    invoke-direct {p0, v2}, Loi;->aq(I)I

    move-result v0

    if-eq p1, v0, :cond_c

    invoke-direct {p0, v1}, Loi;->aq(I)I

    move-result v0

    if-ne p1, v0, :cond_8

    :cond_c
    iget-boolean v0, p0, Loi;->pW:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    if-nez v0, :cond_d

    const-string v0, "TimePickerDialog"

    const-string v2, "Unable to initiate keyboard mode, TimePicker was null."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0, p1}, Loi;->an(I)V

    move v0, v1

    goto/16 :goto_0

    :cond_e
    invoke-direct {p0, p1}, Loi;->ao(I)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-direct {p0, v2}, Loi;->l(Z)V

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method static synthetic c(Loi;)Lop;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Loi;->pG:Lop;

    return-object v0
.end method

.method private cF()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 618
    iget-boolean v2, p0, Loi;->os:Z

    if-eqz v2, :cond_2

    .line 621
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Loi;->a([Ljava/lang/Boolean;)[I

    move-result-object v2

    .line 622
    aget v3, v2, v1

    if-ltz v3, :cond_1

    aget v3, v2, v0

    if-ltz v3, :cond_1

    aget v2, v2, v0

    const/16 v3, 0x3c

    if-ge v2, v3, :cond_1

    .line 626
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 622
    goto :goto_0

    .line 626
    :cond_2
    iget-object v2, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Loi;->aq(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Loi;->aq(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private cG()I
    .locals 3

    .prologue
    .line 632
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    iget-object v1, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 633
    invoke-direct {p0}, Loi;->cF()Z

    move-result v1

    if-nez v1, :cond_0

    .line 634
    iget-object v1, p0, Loi;->pH:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 636
    :cond_0
    return v0
.end method

.method static synthetic d(Loi;)Lcom/android/datetimepicker/time/RadialPickerLayout;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    return-object v0
.end method

.method private e(IZ)V
    .locals 4

    .prologue
    .line 418
    iget-boolean v0, p0, Loi;->os:Z

    if-eqz v0, :cond_2

    .line 419
    const-string v0, "%02d"

    .line 428
    :cond_0
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 429
    iget-object v1, p0, Loi;->pI:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v1, p0, Loi;->pJ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    if-eqz p2, :cond_1

    .line 432
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-static {v1, v0}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 434
    :cond_1
    return-void

    .line 421
    :cond_2
    const-string v0, "%d"

    .line 422
    rem-int/lit8 p1, p1, 0xc

    .line 423
    if-nez p1, :cond_0

    .line 424
    const/16 p1, 0xc

    goto :goto_0
.end method

.method private l(Z)V
    .locals 10

    .prologue
    const/16 v9, 0x20

    const/4 v8, 0x2

    const/4 v7, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 667
    if-nez p1, :cond_3

    iget-object v2, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 668
    iget-object v2, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getHours()I

    move-result v2

    .line 669
    iget-object v3, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v3}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getMinutes()I

    move-result v3

    .line 670
    invoke-direct {p0, v2, v1}, Loi;->e(IZ)V

    .line 671
    invoke-direct {p0, v3}, Loi;->setMinute(I)V

    .line 672
    iget-boolean v3, p0, Loi;->os:Z

    if-nez v3, :cond_0

    .line 673
    const/16 v3, 0xc

    if-ge v2, v3, :cond_2

    :goto_0
    invoke-direct {p0, v0}, Loi;->am(I)V

    .line 675
    :cond_0
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v0

    invoke-direct {p0, v0, v1, v1, v1}, Loi;->b(IZZZ)V

    .line 676
    iget-object v0, p0, Loi;->pH:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 696
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 673
    goto :goto_0

    .line 678
    :cond_3
    new-array v3, v8, [Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v3, v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v3, v1

    .line 679
    invoke-direct {p0, v3}, Loi;->a([Ljava/lang/Boolean;)[I

    move-result-object v4

    .line 680
    aget-object v2, v3, v0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "%02d"

    .line 681
    :goto_2
    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "%02d"

    .line 682
    :goto_3
    aget v5, v4, v0

    if-ne v5, v7, :cond_6

    iget-object v2, p0, Loi;->pU:Ljava/lang/String;

    .line 684
    :goto_4
    aget v5, v4, v1

    if-ne v5, v7, :cond_7

    iget-object v0, p0, Loi;->pU:Ljava/lang/String;

    .line 686
    :goto_5
    iget-object v1, p0, Loi;->pI:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 687
    iget-object v1, p0, Loi;->pJ:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 688
    iget-object v1, p0, Loi;->pI:Landroid/widget/TextView;

    iget v2, p0, Loi;->od:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 689
    iget-object v1, p0, Loi;->pK:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 690
    iget-object v1, p0, Loi;->pL:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 691
    iget-object v0, p0, Loi;->pK:Landroid/widget/TextView;

    iget v1, p0, Loi;->od:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 692
    iget-boolean v0, p0, Loi;->os:Z

    if-nez v0, :cond_1

    .line 693
    aget v0, v4, v8

    invoke-direct {p0, v0}, Loi;->am(I)V

    goto :goto_1

    .line 680
    :cond_4
    const-string v2, "%2d"

    goto :goto_2

    .line 681
    :cond_5
    const-string v3, "%2d"

    goto :goto_3

    .line 682
    :cond_6
    new-array v5, v1, [Ljava/lang/Object;

    aget v6, v4, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-char v5, p0, Loi;->pT:C

    invoke-virtual {v2, v9, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 684
    :cond_7
    new-array v5, v1, [Ljava/lang/Object;

    aget v1, v4, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-char v1, p0, Loi;->pT:C

    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method private o(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 644
    iput-boolean v4, p0, Loi;->pW:Z

    .line 645
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 646
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Loi;->a([Ljava/lang/Boolean;)[I

    move-result-object v0

    .line 647
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    aget v2, v0, v4

    aget v3, v0, v5

    invoke-virtual {v1, v2, v3}, Lcom/android/datetimepicker/time/RadialPickerLayout;->p(II)V

    .line 648
    iget-boolean v1, p0, Loi;->os:Z

    if-nez v1, :cond_0

    .line 649
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    const/4 v2, 0x2

    aget v0, v0, v2

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->al(I)V

    .line 651
    :cond_0
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 653
    :cond_1
    if-eqz p1, :cond_2

    .line 654
    invoke-direct {p0, v4}, Loi;->l(Z)V

    .line 655
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0, v5}, Lcom/android/datetimepicker/time/RadialPickerLayout;->n(Z)Z

    .line 657
    :cond_2
    return-void
.end method

.method private setMinute(I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 437
    const/16 v1, 0x3c

    if-ne p1, v1, :cond_0

    move p1, v0

    .line 440
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "%02d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 441
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-static {v1, v0}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 442
    iget-object v1, p0, Loi;->pK:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    iget-object v1, p0, Loi;->pL:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 444
    return-void
.end method


# virtual methods
.method public final a(IIZ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 392
    if-nez p1, :cond_2

    .line 393
    invoke-direct {p0, p2, v4}, Loi;->e(IZ)V

    .line 394
    const-string v0, "%d"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 395
    iget-boolean v1, p0, Loi;->pP:Z

    if-eqz v1, :cond_1

    if-eqz p3, :cond_1

    .line 396
    invoke-direct {p0, v3, v3, v3, v4}, Loi;->b(IZZZ)V

    .line 397
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Loi;->qe:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 402
    :goto_0
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-static {v1, v0}, Lnf;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 414
    :cond_0
    :goto_1
    return-void

    .line 399
    :cond_1
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Loi;->qb:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 403
    :cond_2
    if-ne p1, v3, :cond_3

    .line 404
    invoke-direct {p0, p2}, Loi;->setMinute(I)V

    .line 405
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Loi;->qd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 406
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 407
    invoke-direct {p0, p2}, Loi;->am(I)V

    goto :goto_1

    .line 408
    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 409
    invoke-direct {p0}, Loi;->cF()Z

    move-result v0

    if-nez v0, :cond_5

    .line 410
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 412
    :cond_5
    invoke-direct {p0, v3}, Loi;->o(Z)V

    goto :goto_1
.end method

.method public final a(Lop;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Loi;->pG:Lop;

    .line 167
    return-void
.end method

.method public final ci()V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Loi;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->ci()V

    .line 356
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 178
    if-eqz p1, :cond_0

    const-string v0, "hour_of_day"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "minute"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "is_24_hour_view"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    const-string v0, "hour_of_day"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Loi;->pQ:I

    .line 182
    const-string v0, "minute"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Loi;->pR:I

    .line 183
    const-string v0, "is_24_hour_view"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Loi;->os:Z

    .line 184
    const-string v0, "in_kb_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Loi;->pW:Z

    .line 185
    const-string v0, "dark_theme"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Loi;->pS:Z

    .line 187
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    .line 192
    invoke-virtual {p0}, Loi;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 194
    const v0, 0x7f04018f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 195
    new-instance v6, Lon;

    const/4 v0, 0x0

    invoke-direct {v6, p0, v0}, Lon;-><init>(Loi;B)V

    .line 196
    const v0, 0x7f110454

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 198
    invoke-virtual {p0}, Loi;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 199
    const v0, 0x7f0a005c

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loi;->qb:Ljava/lang/String;

    .line 200
    const v0, 0x7f0a005e

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loi;->qc:Ljava/lang/String;

    .line 201
    const v0, 0x7f0a005d

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loi;->qd:Ljava/lang/String;

    .line 202
    const v0, 0x7f0a005f

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loi;->qe:Ljava/lang/String;

    .line 203
    iget-boolean v0, p0, Loi;->pS:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0b003f

    :goto_0
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Loi;->of:I

    .line 204
    iget-boolean v0, p0, Loi;->pS:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0b0028

    :goto_1
    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Loi;->od:I

    .line 206
    const v0, 0x7f11044f

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Loi;->pI:Landroid/widget/TextView;

    .line 207
    iget-object v0, p0, Loi;->pI:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 208
    const v0, 0x7f11044e

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Loi;->pJ:Landroid/widget/TextView;

    .line 209
    const v0, 0x7f110450

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Loi;->pL:Landroid/widget/TextView;

    .line 210
    const v0, 0x7f110451

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Loi;->pK:Landroid/widget/TextView;

    .line 211
    iget-object v0, p0, Loi;->pK:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 212
    const v0, 0x7f110453

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Loi;->pM:Landroid/widget/TextView;

    .line 213
    iget-object v0, p0, Loi;->pM:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 214
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v0

    .line 215
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Loi;->oi:Ljava/lang/String;

    .line 216
    const/4 v1, 0x1

    aget-object v0, v0, v1

    iput-object v0, p0, Loi;->oj:Ljava/lang/String;

    .line 218
    new-instance v0, Lnd;

    invoke-virtual {p0}, Loi;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lnd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Loi;->mJ:Lnd;

    .line 220
    const v0, 0x7f110456

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/datetimepicker/time/RadialPickerLayout;

    iput-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    .line 221
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0, p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(Lod;)V

    .line 222
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0, v6}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 223
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {p0}, Loi;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Loi;->mJ:Lnd;

    iget v3, p0, Loi;->pQ:I

    iget v4, p0, Loi;->pR:I

    iget-boolean v5, p0, Loi;->os:Z

    invoke-virtual/range {v0 .. v5}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(Landroid/content/Context;Lnd;IIZ)V

    .line 226
    const/4 v0, 0x0

    .line 227
    if-eqz p3, :cond_0

    const-string v1, "current_item_showing"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    const-string v0, "current_item_showing"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 231
    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, Loi;->b(IZZZ)V

    .line 232
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->invalidate()V

    .line 234
    iget-object v0, p0, Loi;->pI:Landroid/widget/TextView;

    new-instance v1, Loj;

    invoke-direct {v1, p0}, Loj;-><init>(Loi;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    iget-object v0, p0, Loi;->pK:Landroid/widget/TextView;

    new-instance v1, Lok;

    invoke-direct {v1, p0}, Lok;-><init>(Loi;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    const v0, 0x7f110458

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Loi;->pH:Landroid/widget/TextView;

    .line 250
    iget-object v0, p0, Loi;->pH:Landroid/widget/TextView;

    new-instance v1, Lol;

    invoke-direct {v1, p0}, Lol;-><init>(Loi;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    iget-object v0, p0, Loi;->pH:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 268
    const v0, 0x7f110452

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Loi;->pN:Landroid/view/View;

    .line 269
    iget-boolean v0, p0, Loi;->os:Z

    if-eqz v0, :cond_4

    .line 270
    iget-object v0, p0, Loi;->pM:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v2, -0x2

    invoke-direct {v1, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 274
    const/16 v0, 0xd

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 275
    const v0, 0x7f110272

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 276
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 296
    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Loi;->pP:Z

    .line 297
    iget v0, p0, Loi;->pQ:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Loi;->e(IZ)V

    .line 298
    iget v0, p0, Loi;->pR:I

    invoke-direct {p0, v0}, Loi;->setMinute(I)V

    .line 301
    const v0, 0x7f0a0066

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loi;->pU:Ljava/lang/String;

    .line 302
    const v0, 0x7f0a0065

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loi;->pV:Ljava/lang/String;

    .line 303
    iget-object v0, p0, Loi;->pU:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, Loi;->pT:C

    .line 304
    const/4 v0, -0x1

    iput v0, p0, Loi;->qa:I

    iput v0, p0, Loi;->pZ:I

    .line 305
    new-instance v0, Loo;

    const/4 v1, 0x0

    new-array v1, v1, [I

    invoke-direct {v0, p0, v1}, Loo;-><init>(Loi;[I)V

    iput-object v0, p0, Loi;->pY:Loo;

    iget-boolean v0, p0, Loi;->os:Z

    if-eqz v0, :cond_6

    new-instance v0, Loo;

    const/4 v1, 0x6

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, p0, v1}, Loo;-><init>(Loi;[I)V

    new-instance v1, Loo;

    const/16 v2, 0xa

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    invoke-direct {v1, p0, v2}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v0, v1}, Loo;->a(Loo;)V

    new-instance v2, Loo;

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_2

    invoke-direct {v2, p0, v3}, Loo;-><init>(Loi;[I)V

    iget-object v3, p0, Loi;->pY:Loo;

    invoke-virtual {v3, v2}, Loo;->a(Loo;)V

    new-instance v3, Loo;

    const/4 v4, 0x6

    new-array v4, v4, [I

    fill-array-data v4, :array_3

    invoke-direct {v3, p0, v4}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v2, v3}, Loo;->a(Loo;)V

    invoke-virtual {v3, v0}, Loo;->a(Loo;)V

    new-instance v4, Loo;

    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_4

    invoke-direct {v4, p0, v5}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v3, v4}, Loo;->a(Loo;)V

    new-instance v3, Loo;

    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_5

    invoke-direct {v3, p0, v4}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v2, v3}, Loo;->a(Loo;)V

    invoke-virtual {v3, v0}, Loo;->a(Loo;)V

    new-instance v2, Loo;

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x9

    aput v5, v3, v4

    invoke-direct {v2, p0, v3}, Loo;-><init>(Loi;[I)V

    iget-object v3, p0, Loi;->pY:Loo;

    invoke-virtual {v3, v2}, Loo;->a(Loo;)V

    new-instance v3, Loo;

    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_6

    invoke-direct {v3, p0, v4}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v2, v3}, Loo;->a(Loo;)V

    invoke-virtual {v3, v0}, Loo;->a(Loo;)V

    new-instance v3, Loo;

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_7

    invoke-direct {v3, p0, v4}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v2, v3}, Loo;->a(Loo;)V

    invoke-virtual {v3, v1}, Loo;->a(Loo;)V

    new-instance v1, Loo;

    const/4 v2, 0x7

    new-array v2, v2, [I

    fill-array-data v2, :array_8

    invoke-direct {v1, p0, v2}, Loo;-><init>(Loi;[I)V

    iget-object v2, p0, Loi;->pY:Loo;

    invoke-virtual {v2, v1}, Loo;->a(Loo;)V

    invoke-virtual {v1, v0}, Loo;->a(Loo;)V

    .line 306
    :goto_3
    iget-boolean v0, p0, Loi;->pW:Z

    if-eqz v0, :cond_7

    .line 307
    const-string v0, "typed_times"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    .line 308
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Loi;->an(I)V

    .line 309
    iget-object v0, p0, Loi;->pI:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    .line 315
    :cond_1
    :goto_4
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {p0}, Loi;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v2, p0, Loi;->pS:Z

    invoke-virtual {v0, v1, v2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(Landroid/content/Context;Z)V

    .line 317
    const v0, 0x7f0b0028

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 318
    const v0, 0x7f0b002f

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 319
    const v0, 0x7f0b0030

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 320
    const v0, 0x7f0b0034

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 321
    const v0, 0x7f0b013b

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    .line 322
    const v0, 0x7f0b0042

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 325
    const v0, 0x7f0b0041

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 326
    const v0, 0x7f0b0043

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 327
    const v0, 0x7f0b013c

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    .line 328
    const v0, 0x7f110455

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iget-boolean v0, p0, Loi;->pS:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_5
    invoke-virtual {v11, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 332
    const v0, 0x7f11044c

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v11, p0, Loi;->pS:Z

    if-eqz v11, :cond_9

    :goto_6
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 333
    const v0, 0x7f110272

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v1, p0, Loi;->pS:Z

    if-eqz v1, :cond_a

    move v1, v2

    :goto_7
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 334
    const v0, 0x7f110453

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v1, p0, Loi;->pS:Z

    if-eqz v1, :cond_b

    :goto_8
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 335
    const v0, 0x7f110457

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, p0, Loi;->pS:Z

    if-eqz v0, :cond_c

    move v0, v4

    :goto_9
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 336
    iget-object v1, p0, Loi;->pH:Landroid/widget/TextView;

    iget-boolean v0, p0, Loi;->pS:Z

    if-eqz v0, :cond_d

    move-object v0, v6

    :goto_a
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 337
    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-boolean v0, p0, Loi;->pS:Z

    if-eqz v0, :cond_e

    move v0, v8

    :goto_b
    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setBackgroundColor(I)V

    .line 338
    iget-object v1, p0, Loi;->pH:Landroid/widget/TextView;

    iget-boolean v0, p0, Loi;->pS:Z

    if-eqz v0, :cond_f

    const v0, 0x7f02005c

    :goto_c
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 339
    return-object v10

    .line 203
    :cond_2
    const v0, 0x7f0b0036

    goto/16 :goto_0

    .line 204
    :cond_3
    const v0, 0x7f0b0034

    goto/16 :goto_1

    .line 278
    :cond_4
    iget-object v0, p0, Loi;->pM:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    iget v0, p0, Loi;->pQ:I

    const/16 v1, 0xc

    if-ge v0, v1, :cond_5

    const/4 v0, 0x0

    :goto_d
    invoke-direct {p0, v0}, Loi;->am(I)V

    .line 280
    iget-object v0, p0, Loi;->pN:Landroid/view/View;

    new-instance v1, Lom;

    invoke-direct {v1, p0}, Lom;-><init>(Loi;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 279
    :cond_5
    const/4 v0, 0x1

    goto :goto_d

    .line 305
    :cond_6
    new-instance v0, Loo;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Loi;->aq(I)I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Loi;->aq(I)I

    move-result v3

    aput v3, v1, v2

    invoke-direct {v0, p0, v1}, Loo;-><init>(Loi;[I)V

    new-instance v1, Loo;

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x8

    aput v4, v2, v3

    invoke-direct {v1, p0, v2}, Loo;-><init>(Loi;[I)V

    iget-object v2, p0, Loi;->pY:Loo;

    invoke-virtual {v2, v1}, Loo;->a(Loo;)V

    invoke-virtual {v1, v0}, Loo;->a(Loo;)V

    new-instance v2, Loo;

    const/4 v3, 0x3

    new-array v3, v3, [I

    fill-array-data v3, :array_9

    invoke-direct {v2, p0, v3}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v1, v2}, Loo;->a(Loo;)V

    invoke-virtual {v2, v0}, Loo;->a(Loo;)V

    new-instance v3, Loo;

    const/4 v4, 0x6

    new-array v4, v4, [I

    fill-array-data v4, :array_a

    invoke-direct {v3, p0, v4}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v2, v3}, Loo;->a(Loo;)V

    invoke-virtual {v3, v0}, Loo;->a(Loo;)V

    new-instance v4, Loo;

    const/16 v5, 0xa

    new-array v5, v5, [I

    fill-array-data v5, :array_b

    invoke-direct {v4, p0, v5}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v3, v4}, Loo;->a(Loo;)V

    invoke-virtual {v4, v0}, Loo;->a(Loo;)V

    new-instance v3, Loo;

    const/4 v4, 0x4

    new-array v4, v4, [I

    fill-array-data v4, :array_c

    invoke-direct {v3, p0, v4}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v2, v3}, Loo;->a(Loo;)V

    invoke-virtual {v3, v0}, Loo;->a(Loo;)V

    new-instance v2, Loo;

    const/4 v3, 0x3

    new-array v3, v3, [I

    fill-array-data v3, :array_d

    invoke-direct {v2, p0, v3}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v1, v2}, Loo;->a(Loo;)V

    new-instance v1, Loo;

    const/16 v3, 0xa

    new-array v3, v3, [I

    fill-array-data v3, :array_e

    invoke-direct {v1, p0, v3}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v2, v1}, Loo;->a(Loo;)V

    invoke-virtual {v1, v0}, Loo;->a(Loo;)V

    new-instance v1, Loo;

    const/16 v2, 0x8

    new-array v2, v2, [I

    fill-array-data v2, :array_f

    invoke-direct {v1, p0, v2}, Loo;-><init>(Loi;[I)V

    iget-object v2, p0, Loi;->pY:Loo;

    invoke-virtual {v2, v1}, Loo;->a(Loo;)V

    invoke-virtual {v1, v0}, Loo;->a(Loo;)V

    new-instance v2, Loo;

    const/4 v3, 0x6

    new-array v3, v3, [I

    fill-array-data v3, :array_10

    invoke-direct {v2, p0, v3}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v1, v2}, Loo;->a(Loo;)V

    new-instance v1, Loo;

    const/16 v3, 0xa

    new-array v3, v3, [I

    fill-array-data v3, :array_11

    invoke-direct {v1, p0, v3}, Loo;-><init>(Loi;[I)V

    invoke-virtual {v2, v1}, Loo;->a(Loo;)V

    invoke-virtual {v1, v0}, Loo;->a(Loo;)V

    goto/16 :goto_3

    .line 310
    :cond_7
    iget-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 311
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Loi;->pX:Ljava/util/ArrayList;

    goto/16 :goto_4

    :cond_8
    move v0, v2

    .line 328
    goto/16 :goto_5

    :cond_9
    move v1, v2

    .line 332
    goto/16 :goto_6

    :cond_a
    move v1, v3

    .line 333
    goto/16 :goto_7

    :cond_b
    move v2, v3

    .line 334
    goto/16 :goto_8

    :cond_c
    move v0, v5

    .line 335
    goto/16 :goto_9

    :cond_d
    move-object v0, v7

    .line 336
    goto/16 :goto_a

    :cond_e
    move v0, v9

    .line 337
    goto/16 :goto_b

    .line 338
    :cond_f
    const v0, 0x7f02005b

    goto/16 :goto_c

    .line 305
    nop

    :array_0
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
    .end array-data

    :array_1
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_2
    .array-data 4
        0x7
        0x8
    .end array-data

    :array_3
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
    .end array-data

    :array_4
    .array-data 4
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_5
    .array-data 4
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_6
    .array-data 4
        0x7
        0x8
        0x9
        0xa
    .end array-data

    :array_7
    .array-data 4
        0xb
        0xc
    .end array-data

    :array_8
    .array-data 4
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_9
    .array-data 4
        0x7
        0x8
        0x9
    .end array-data

    :array_a
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
    .end array-data

    :array_b
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_c
    .array-data 4
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_d
    .array-data 4
        0xa
        0xb
        0xc
    .end array-data

    :array_e
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_f
    .array-data 4
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_10
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
    .end array-data

    :array_11
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 350
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 351
    iget-object v0, p0, Loi;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->stop()V

    .line 352
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 344
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 345
    iget-object v0, p0, Loi;->mJ:Lnd;

    invoke-virtual {v0}, Lnd;->start()V

    .line 346
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    if-eqz v0, :cond_1

    .line 375
    const-string v0, "hour_of_day"

    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getHours()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 376
    const-string v0, "minute"

    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getMinutes()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 377
    const-string v0, "is_24_hour_view"

    iget-boolean v1, p0, Loi;->os:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 378
    const-string v0, "current_item_showing"

    iget-object v1, p0, Loi;->pO:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->cC()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 379
    const-string v0, "in_kb_mode"

    iget-boolean v1, p0, Loi;->pW:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 380
    iget-boolean v0, p0, Loi;->pW:Z

    if-eqz v0, :cond_0

    .line 381
    const-string v0, "typed_times"

    iget-object v1, p0, Loi;->pX:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 383
    :cond_0
    const-string v0, "dark_theme"

    iget-boolean v1, p0, Loi;->pS:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 385
    :cond_1
    return-void
.end method
