.class public Ldbj;
.super Lddj;
.source "PG"


# instance fields
.field private final aMD:Leqo;

.field private ag:I

.field private bqK:Lcom/google/android/shared/search/Query;

.field bqL:Z

.field bqM:Z

.field private bqN:Z

.field private bqO:Z

.field private final mGsaConfig:Lchk;

.field final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Ldda;Leqo;Lchk;Lcke;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 45
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldbj;->bqK:Lcom/google/android/shared/search/Query;

    .line 46
    iput v1, p0, Ldbj;->ag:I

    .line 49
    iput-boolean v1, p0, Ldbj;->bqL:Z

    .line 50
    iput-boolean v1, p0, Ldbj;->bqM:Z

    .line 52
    iput-boolean v1, p0, Ldbj;->bqN:Z

    .line 59
    invoke-virtual {p1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    iput-object v0, p0, Ldbj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 60
    iput-object p2, p0, Ldbj;->aMD:Leqo;

    .line 61
    iput-object p3, p0, Ldbj;->mGsaConfig:Lchk;

    .line 62
    iput-object p4, p0, Ldbj;->mSettings:Lcke;

    .line 63
    return-void
.end method

.method static synthetic a(Ldbj;I)Z
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Ldbj;->fB(I)Z

    move-result v0

    return v0
.end method

.method private fB(I)Z
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Ldbj;->ag:I

    if-eq v0, p1, :cond_0

    .line 187
    iput p1, p0, Ldbj;->ag:I

    .line 189
    const/4 v0, 0x1

    .line 191
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final C(Lcom/google/android/shared/search/Query;)V
    .locals 4

    .prologue
    .line 138
    sget-object v0, Lcgg;->aVn:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbj;->bqK:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldbj;->bqO:Z

    if-nez v0, :cond_0

    iget v0, p0, Ldbj;->ag:I

    if-nez v0, :cond_0

    .line 142
    iget-object v0, p0, Ldbj;->aMD:Leqo;

    new-instance v1, Ldbk;

    const-string v2, "Delayed show"

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, p1, v3}, Ldbk;-><init>(Ldbj;Ljava/lang/String;Lcom/google/android/shared/search/Query;Z)V

    iget-object v2, p0, Ldbj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->IB()I

    move-result v2

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Leqo;->a(Ljava/lang/Runnable;J)V

    .line 144
    :cond_0
    return-void
.end method

.method public final D(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Ldbj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v0, p1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbj;->bqM:Z

    .line 150
    :cond_0
    return-void
.end method

.method public final Wl()Z
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Ldbj;->ag:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Wm()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 125
    iget v1, p0, Ldbj;->ag:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Wn()Z
    .locals 2

    .prologue
    .line 129
    iget v0, p0, Ldbj;->ag:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Wo()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Ldbj;->bqL:Z

    return v0
.end method

.method public final Wp()V
    .locals 2

    .prologue
    .line 157
    iget-boolean v0, p0, Ldbj;->bqN:Z

    if-nez v0, :cond_0

    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbj;->bqN:Z

    .line 160
    iget-object v0, p0, Ldbj;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Oc()I

    move-result v0

    iget-object v1, p0, Ldbj;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->IC()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 162
    iget-object v0, p0, Ldbj;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->Od()V

    .line 165
    :cond_0
    return-void
.end method

.method public final Wq()V
    .locals 2

    .prologue
    .line 171
    iget-boolean v0, p0, Ldbj;->bqL:Z

    if-eqz v0, :cond_1

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbj;->bqL:Z

    .line 173
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Ldbj;->fB(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {p0}, Ldbj;->notifyChanged()V

    .line 177
    :cond_0
    iget-object v0, p0, Ldbj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v1, p0, Ldbj;->bqK:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->ad(Lcom/google/android/shared/search/Query;)V

    .line 179
    :cond_1
    return-void
.end method

.method public final a(Lddb;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 67
    .line 68
    invoke-virtual {p1}, Lddb;->aay()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v1

    invoke-virtual {v1}, Ldda;->aan()Ldcy;

    move-result-object v1

    invoke-virtual {v1}, Ldcy;->ZA()Z

    move-result v1

    iput-boolean v1, p0, Ldbj;->bqO:Z

    .line 71
    :cond_0
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ldbj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    iget-object v2, p0, Ldbj;->bqK:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1, v2}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 73
    iget-object v1, p0, Ldbj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v1

    iput-object v1, p0, Ldbj;->bqK:Lcom/google/android/shared/search/Query;

    .line 75
    iput-boolean v0, p0, Ldbj;->bqN:Z

    .line 76
    iput-boolean v0, p0, Ldbj;->bqM:Z

    .line 77
    iput-boolean v0, p0, Ldbj;->bqL:Z

    .line 79
    invoke-direct {p0, v0}, Ldbj;->fB(I)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 83
    sget-object v1, Lcgg;->aVn:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Ldbj;->bqK:Lcom/google/android/shared/search/Query;

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Ldbj;->bqO:Z

    if-nez v1, :cond_1

    .line 86
    iget-object v1, p0, Ldbj;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->Oc()I

    move-result v1

    iget-object v2, p0, Ldbj;->mGsaConfig:Lchk;

    invoke-virtual {v2}, Lchk;->IC()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 88
    iput-boolean v3, p0, Ldbj;->bqL:Z

    .line 89
    invoke-direct {p0, v3}, Ldbj;->fB(I)Z

    move-result v1

    or-int/2addr v0, v1

    .line 94
    :cond_1
    if-eqz v0, :cond_2

    .line 95
    invoke-virtual {p0}, Ldbj;->notifyChanged()V

    .line 97
    :cond_2
    return-void
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 196
    const-string v0, "DiscoveryState"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 197
    const-string v0, "CurrentCommittedQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldbj;->bqK:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 198
    const-string v0, "State"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget v1, p0, Ldbj;->ag:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 199
    const-string v0, "ShouldPeek"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldbj;->bqL:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 200
    const-string v0, "SpeechDetected"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldbj;->bqM:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 201
    const-string v0, "PeekCounted"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Ldbj;->bqN:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 202
    return-void
.end method
