.class public final enum Lagv;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field private static final synthetic XA:[Lagv;

.field public static final enum Xv:Lagv;

.field public static final enum Xw:Lagv;

.field public static final enum Xx:Lagv;

.field public static final enum Xy:Lagv;

.field public static final enum Xz:Lagv;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 200
    new-instance v0, Lagv;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lagv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagv;->Xv:Lagv;

    new-instance v0, Lagv;

    const-string v1, "NORMAL_HIDDEN"

    invoke-direct {v0, v1, v3}, Lagv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagv;->Xw:Lagv;

    new-instance v0, Lagv;

    const-string v1, "SPRING_LOADED"

    invoke-direct {v0, v1, v4}, Lagv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagv;->Xx:Lagv;

    new-instance v0, Lagv;

    const-string v1, "OVERVIEW"

    invoke-direct {v0, v1, v5}, Lagv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagv;->Xy:Lagv;

    new-instance v0, Lagv;

    const-string v1, "OVERVIEW_HIDDEN"

    invoke-direct {v0, v1, v6}, Lagv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lagv;->Xz:Lagv;

    const/4 v0, 0x5

    new-array v0, v0, [Lagv;

    sget-object v1, Lagv;->Xv:Lagv;

    aput-object v1, v0, v2

    sget-object v1, Lagv;->Xw:Lagv;

    aput-object v1, v0, v3

    sget-object v1, Lagv;->Xx:Lagv;

    aput-object v1, v0, v4

    sget-object v1, Lagv;->Xy:Lagv;

    aput-object v1, v0, v5

    sget-object v1, Lagv;->Xz:Lagv;

    aput-object v1, v0, v6

    sput-object v0, Lagv;->XA:[Lagv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lagv;
    .locals 1

    .prologue
    .line 200
    const-class v0, Lagv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lagv;

    return-object v0
.end method

.method public static values()[Lagv;
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lagv;->XA:[Lagv;

    invoke-virtual {v0}, [Lagv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lagv;

    return-object v0
.end method
