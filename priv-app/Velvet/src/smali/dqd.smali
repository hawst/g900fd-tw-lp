.class public final Ldqd;
.super Lebu;
.source "PG"


# instance fields
.field private synthetic bFj:Ldpz;


# direct methods
.method protected constructor <init>(Ldpz;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Ldqd;->bFj:Ldpz;

    invoke-direct {p0, p1}, Lebu;-><init>(Lebm;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Suggestion;Landroid/view/View;Ligi;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 339
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asA()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 361
    :goto_0
    return v0

    .line 343
    :cond_0
    iget-object v0, p0, Ldqd;->bFj:Ldpz;

    iget-object v0, v0, Ldpz;->bvh:Leeh;

    invoke-static {p1}, Leeh;->o(Lcom/google/android/shared/search/Suggestion;)Landroid/content/Intent;

    move-result-object v0

    .line 344
    if-eqz v0, :cond_1

    .line 345
    iget-object v3, p0, Ldqd;->bFj:Ldpz;

    iget-object v3, v3, Ldpz;->bFe:Ldpy;

    invoke-interface {v3, v0}, Ldpy;->d(Landroid/content/Intent;)Lwq;

    move-result-object v0

    .line 354
    :goto_1
    if-nez v0, :cond_5

    move v0, v1

    .line 355
    goto :goto_0

    .line 347
    :cond_1
    iget-object v0, p0, Ldqd;->bFj:Ldpz;

    iget-object v3, v0, Ldpz;->bvh:Leeh;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asA()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.google.android.googlequicksearchbox.GOOGLE_SEARCH"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Leeh;->bwa:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "query"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v3, v0

    .line 348
    :goto_2
    if-nez v3, :cond_4

    move v0, v1

    .line 349
    goto :goto_0

    .line 347
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->asw()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Leeh;->o(Lcom/google/android/shared/search/Suggestion;)Landroid/content/Intent;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    :cond_3
    invoke-virtual {v3, p1, v2}, Leeh;->a(Lcom/google/android/shared/search/Suggestion;Z)Landroid/content/Intent;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    .line 351
    :cond_4
    iget-object v0, p0, Ldqd;->bFj:Ldpz;

    iget-object v4, v0, Ldpz;->bFe:Ldpy;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {p3}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-interface {v4, v3, v5, v0}, Ldpy;->a(Landroid/content/Intent;Ljava/lang/CharSequence;Landroid/graphics/Bitmap;)Lwq;

    move-result-object v0

    goto :goto_1

    .line 358
    :cond_5
    iget-object v1, p0, Ldqd;->bFj:Ldpz;

    invoke-virtual {v1, v2}, Ldpz;->bW(Z)Z

    .line 359
    iget-object v1, p0, Ldqd;->bFj:Ldpz;

    iput-object p1, v1, Ldpz;->bFg:Lcom/google/android/shared/search/Suggestion;

    .line 360
    iget-object v1, p0, Ldqd;->bFj:Ldpz;

    iget-object v1, v1, Ldpz;->bFe:Ldpy;

    iget-object v3, p0, Ldqd;->bFj:Ldpz;

    invoke-interface {v1, p2, v0, v3}, Ldpy;->a(Landroid/view/View;Lwq;Lui;)V

    move v0, v2

    .line 361
    goto :goto_0
.end method
