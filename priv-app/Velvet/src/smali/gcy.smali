.class public final Lgcy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgcx;


# instance fields
.field private final aSZ:Ljava/util/concurrent/ExecutorService;

.field private final cGr:Lgnb;

.field private final cGs:Ljava/util/concurrent/ExecutorService;

.field private final cGt:Ljava/util/concurrent/ExecutorService;

.field private cGu:Lglc;

.field private cGv:Lglc;

.field private cGw:Lglc;

.field private cGx:Lglc;

.field private final mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

.field private final mSpeechLibFactory:Lgdn;


# direct methods
.method public constructor <init>(Lgnb;Lgdn;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lgcy;->cGr:Lgnb;

    .line 49
    iput-object p2, p0, Lgcy;->mSpeechLibFactory:Lgdn;

    .line 50
    iput-object p3, p0, Lgcy;->mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 51
    iput-object p4, p0, Lgcy;->aSZ:Ljava/util/concurrent/ExecutorService;

    .line 52
    iput-object p5, p0, Lgcy;->cGs:Ljava/util/concurrent/ExecutorService;

    .line 53
    iput-object p6, p0, Lgcy;->cGt:Ljava/util/concurrent/ExecutorService;

    .line 54
    return-void
.end method


# virtual methods
.method public final ah(Ljava/util/List;)Ljava/util/List;
    .locals 13

    .prologue
    .line 59
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v8

    .line 61
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 62
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 65
    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v0, p0, Lgcy;->cGu:Lglc;

    if-nez v0, :cond_0

    iget-object v0, p0, Lgcy;->cGr:Lgnb;

    iget-object v6, v0, Lgnb;->cPP:Lgnc;

    iget-object v11, p0, Lgcy;->mLocalExecutorService:Ljava/util/concurrent/ExecutorService;

    const-class v12, Lglc;

    new-instance v0, Lgjr;

    iget-object v1, v6, Lgnc;->cLH:Lgjh;

    iget v2, v6, Lgnc;->cMT:I

    iget v3, v6, Lgnc;->bhu:I

    iget-object v4, v6, Lgnc;->mSpeechSettings:Lgdo;

    iget-object v5, v6, Lgnc;->mCallbackFactory:Lgiv;

    iget-object v6, v6, Lgnc;->mModeSelector:Lgjp;

    new-instance v7, Lgjz;

    invoke-direct {v7}, Lgjz;-><init>()V

    invoke-direct/range {v0 .. v7}, Lgjr;-><init>(Lgjh;IILgdo;Lgiv;Lgjp;Lgjy;)V

    invoke-static {v11, v12, v0}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglc;

    iput-object v0, p0, Lgcy;->cGu:Lglc;

    :cond_0
    iget-object v0, p0, Lgcy;->cGu:Lglc;

    invoke-static {v10, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v0, p0, Lgcy;->cGv:Lglc;

    if-nez v0, :cond_1

    iget-object v0, p0, Lgcy;->cGr:Lgnb;

    iget-object v0, v0, Lgnb;->cPQ:Lgnf;

    iget-object v2, p0, Lgcy;->aSZ:Ljava/util/concurrent/ExecutorService;

    const-class v3, Lglc;

    new-instance v4, Lgkz;

    iget-object v5, v0, Lgnf;->mClock:Lemp;

    iget-object v0, v0, Lgnf;->mS3ConnectionFactory:Lcdr;

    iget-object v6, p0, Lgcy;->mSpeechLibFactory:Lgdn;

    invoke-direct {v4, v5, v0, v6}, Lgkz;-><init>(Lemp;Lcdr;Lgdn;)V

    invoke-static {v2, v3, v4}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglc;

    iput-object v0, p0, Lgcy;->cGv:Lglc;

    :cond_1
    iget-object v0, p0, Lgcy;->cGv:Lglc;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    :pswitch_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v0, p0, Lgcy;->cGw:Lglc;

    if-nez v0, :cond_2

    iget-object v0, p0, Lgcy;->cGr:Lgnb;

    iget-object v2, v0, Lgnb;->cPR:Lgne;

    new-instance v0, Lclm;

    iget-object v3, p0, Lgcy;->cGs:Ljava/util/concurrent/ExecutorService;

    iget-object v2, v2, Lgne;->mSettings:Lgdo;

    invoke-direct {v0, v3, v2}, Lclm;-><init>(Ljava/util/concurrent/Executor;Lgdo;)V

    check-cast v0, Lglc;

    iput-object v0, p0, Lgcy;->cGw:Lglc;

    :cond_2
    iget-object v0, p0, Lgcy;->cGw:Lglc;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 77
    :pswitch_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v0, p0, Lgcy;->cGx:Lglc;

    if-nez v0, :cond_3

    iget-object v0, p0, Lgcy;->cGt:Ljava/util/concurrent/ExecutorService;

    const-class v2, Lglc;

    new-instance v3, Lcsk;

    iget-object v4, p0, Lgcy;->cGr:Lgnb;

    iget-object v4, v4, Lgnb;->cPS:Lgnd;

    iget-object v4, v4, Lgnd;->bhN:Lcrz;

    iget-object v5, p0, Lgcy;->cGr:Lgnb;

    iget-object v5, v5, Lgnb;->cPS:Lgnd;

    iget-object v5, v5, Lgnd;->aGx:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcsk;-><init>(Lcrz;Ljava/lang/String;)V

    invoke-static {v0, v2, v3}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglc;

    iput-object v0, p0, Lgcy;->cGx:Lglc;

    :cond_3
    iget-object v0, p0, Lgcy;->cGx:Lglc;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 83
    :cond_4
    return-object v8

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
