.class public final Lfab;
.super Lfbk;
.source "PG"


# instance fields
.field private final cmM:Lfba;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cmN:Lgbg;

.field private final cmO:Ljava/util/Collection;


# direct methods
.method public constructor <init>(Lfba;Lgbg;Ljava/util/Collection;)V
    .locals 0
    .param p1    # Lfba;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0}, Lfbk;-><init>()V

    .line 26
    iput-object p1, p0, Lfab;->cmM:Lfba;

    .line 27
    iput-object p2, p0, Lfab;->cmN:Lgbg;

    .line 28
    iput-object p3, p0, Lfab;->cmO:Ljava/util/Collection;

    .line 29
    return-void
.end method


# virtual methods
.method protected final a(Lizq;)V
    .locals 7

    .prologue
    .line 33
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    if-eqz v0, :cond_3

    .line 34
    iget-object v1, p1, Lizq;->dUZ:Lizj;

    .line 35
    invoke-static {v1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    .line 36
    iget-object v2, p0, Lfab;->cmN:Lgbg;

    invoke-virtual {v0, v2}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 37
    iget-object v0, p1, Lizq;->dUX:[Lizj;

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    .line 38
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 39
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 40
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 42
    iget-object v5, p0, Lfab;->cmO:Ljava/util/Collection;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 43
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 44
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 47
    :cond_1
    iget-object v0, p1, Lizq;->dUX:[Lizj;

    array-length v0, v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v0, v3, :cond_2

    .line 49
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lizj;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizj;

    iput-object v0, p1, Lizq;->dUX:[Lizj;

    .line 51
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-boolean v0, p0, Lfbk;->cnX:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfab;->cmM:Lfba;

    if-eqz v0, :cond_3

    .line 52
    iget-object v0, p0, Lfab;->cmM:Lfba;

    invoke-virtual {v0, v1, v4}, Lfba;->a(Lizj;Ljava/util/List;)V

    .line 56
    :cond_3
    return-void
.end method
