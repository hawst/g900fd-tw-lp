.class public final Lcxb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcxt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method

.method public final a(Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public final d(Landroid/preference/Preference;)Z
    .locals 2

    .prologue
    .line 32
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public final onDestroy()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public final onPause()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public final onStart()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public final onStop()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method
