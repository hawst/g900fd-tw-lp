.class public final Ljid;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile emZ:[Ljid;


# instance fields
.field private aez:I

.field private dMT:Ljava/lang/String;

.field private dNf:Ljava/lang/String;

.field private dWA:Ljava/lang/String;

.field private ena:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljid;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljid;->dMT:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljid;->ena:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljid;->dWA:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljid;->dNf:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljid;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljid;->eCz:I

    return-void
.end method

.method public static bmP()[Ljid;
    .locals 2

    sget-object v0, Ljid;->emZ:[Ljid;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljid;->emZ:[Ljid;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljid;

    sput-object v0, Ljid;->emZ:[Ljid;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljid;->emZ:[Ljid;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljid;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljid;->dMT:Ljava/lang/String;

    iget v0, p0, Ljid;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljid;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljid;->ena:Ljava/lang/String;

    iget v0, p0, Ljid;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljid;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljid;->dWA:Ljava/lang/String;

    iget v0, p0, Ljid;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljid;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljid;->dNf:Ljava/lang/String;

    iget v0, p0, Ljid;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljid;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Ljid;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljid;->dMT:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljid;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljid;->ena:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljid;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljid;->dWA:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_2
    iget v0, p0, Ljid;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljid;->dNf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final aZG()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljid;->dMT:Ljava/lang/String;

    return-object v0
.end method

.method public final aZS()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljid;->dNf:Ljava/lang/String;

    return-object v0
.end method

.method public final bmQ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljid;->ena:Ljava/lang/String;

    return-object v0
.end method

.method public final bmR()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljid;->dWA:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljid;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljid;->dMT:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljid;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljid;->ena:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljid;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljid;->dWA:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljid;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljid;->dNf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    return v0
.end method

.method public final vD(Ljava/lang/String;)Ljid;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljid;->dMT:Ljava/lang/String;

    iget v0, p0, Ljid;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljid;->aez:I

    return-object p0
.end method

.method public final vE(Ljava/lang/String;)Ljid;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljid;->ena:Ljava/lang/String;

    iget v0, p0, Ljid;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljid;->aez:I

    return-object p0
.end method

.method public final vF(Ljava/lang/String;)Ljid;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljid;->dWA:Ljava/lang/String;

    iget v0, p0, Ljid;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljid;->aez:I

    return-object p0
.end method

.method public final vG(Ljava/lang/String;)Ljid;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljid;->dNf:Ljava/lang/String;

    iget v0, p0, Ljid;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljid;->aez:I

    return-object p0
.end method
