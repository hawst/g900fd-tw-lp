.class public final Ldoj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public anl:Larv;

.field public final anq:Lary;

.field final bEc:Ljava/util/concurrent/ScheduledExecutorService;

.field final bEd:Ldlu;

.field private final bEe:Ljava/lang/Runnable;

.field final bEf:Ljava/lang/Runnable;

.field private final bEg:Landroid/content/BroadcastReceiver;

.field private final bEh:Landroid/content/BroadcastReceiver;

.field private bEi:Lhum;

.field private bEj:Lhuo;

.field private bEk:Z

.field bEl:I

.field private bpu:Landroid/os/PowerManager;

.field private cq:Z

.field final mAssistContextHelper:Lckx;

.field final mAudioManager:Landroid/media/AudioManager;

.field private final mAudioWorker:Ldnz;

.field public final mContext:Landroid/content/Context;

.field final mGsaConfig:Lchk;

.field public final mHotwordState:Ldby;

.field final mQueryState:Lcom/google/android/search/core/state/QueryState;

.field final mScreenStateHelper:Ldmh;

.field public final mSearchSettings:Lcke;

.field final mServiceState:Ldcu;

.field private final mSettingsUtil:Lbyl;

.field final mUiThread:Leqo;

.field public final mVoiceSearchServices:Lhhq;

.field public final mVoiceSettings:Lhym;


# direct methods
.method public constructor <init>(Lcom/google/android/search/core/state/QueryState;Ldby;Ldcu;Ldnz;Landroid/content/Context;Lhhq;Leqo;Ljava/util/concurrent/ScheduledExecutorService;Lchk;Ldlu;Lckx;Ldmh;Lhym;Lcke;)V
    .locals 3

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v1, Ldok;

    const-string v2, "Hotword data manager initialized"

    invoke-direct {v1, p0, v2}, Ldok;-><init>(Ldoj;Ljava/lang/String;)V

    iput-object v1, p0, Ldoj;->bEe:Ljava/lang/Runnable;

    .line 112
    new-instance v1, Ldol;

    const-string v2, "Notify hotword state of an error"

    invoke-direct {v1, p0, v2}, Ldol;-><init>(Ldoj;Ljava/lang/String;)V

    iput-object v1, p0, Ldoj;->bEf:Ljava/lang/Runnable;

    .line 121
    new-instance v1, Ldom;

    invoke-direct {v1, p0}, Ldom;-><init>(Ldoj;)V

    iput-object v1, p0, Ldoj;->bEg:Landroid/content/BroadcastReceiver;

    .line 167
    new-instance v1, Ldon;

    invoke-direct {v1, p0}, Ldon;-><init>(Ldoj;)V

    iput-object v1, p0, Ldoj;->bEh:Landroid/content/BroadcastReceiver;

    .line 678
    new-instance v1, Ldoo;

    invoke-direct {v1, p0}, Ldoo;-><init>(Ldoj;)V

    iput-object v1, p0, Ldoj;->anq:Lary;

    .line 194
    iput-object p1, p0, Ldoj;->mQueryState:Lcom/google/android/search/core/state/QueryState;

    .line 195
    iput-object p2, p0, Ldoj;->mHotwordState:Ldby;

    .line 196
    iput-object p3, p0, Ldoj;->mServiceState:Ldcu;

    .line 197
    iput-object p4, p0, Ldoj;->mAudioWorker:Ldnz;

    .line 198
    iput-object p6, p0, Ldoj;->mVoiceSearchServices:Lhhq;

    .line 199
    iput-object p9, p0, Ldoj;->mGsaConfig:Lchk;

    .line 200
    iput-object p7, p0, Ldoj;->mUiThread:Leqo;

    .line 201
    iput-object p5, p0, Ldoj;->mContext:Landroid/content/Context;

    .line 202
    iput-object p11, p0, Ldoj;->mAssistContextHelper:Lckx;

    .line 203
    iput-object p8, p0, Ldoj;->bEc:Ljava/util/concurrent/ScheduledExecutorService;

    .line 205
    iget-object v1, p0, Ldoj;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1}, Lhhq;->aPa()Landroid/media/AudioManager;

    move-result-object v1

    iput-object v1, p0, Ldoj;->mAudioManager:Landroid/media/AudioManager;

    .line 206
    iput-object p10, p0, Ldoj;->bEd:Ldlu;

    .line 207
    iput-object p12, p0, Ldoj;->mScreenStateHelper:Ldmh;

    .line 208
    move-object/from16 v0, p14

    iput-object v0, p0, Ldoj;->mSearchSettings:Lcke;

    .line 209
    new-instance v1, Lbyl;

    invoke-direct {v1}, Lbyl;-><init>()V

    iput-object v1, p0, Ldoj;->mSettingsUtil:Lbyl;

    .line 210
    move-object/from16 v0, p13

    iput-object v0, p0, Ldoj;->mVoiceSettings:Lhym;

    .line 211
    const/4 v1, 0x0

    iput v1, p0, Ldoj;->bEl:I

    .line 212
    return-void
.end method

.method static synthetic a(Ldoj;Landroid/content/Intent;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 76
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_1

    const-string v0, "HotwordWorker"

    const-string v1, "Tried to pause/resume hotword on an old API level using incorrect intent."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "com.google.android.googlequicksearchbox.extra.PAUSE_HOTWORD_PENDING_INTENT_KEY"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    if-nez v0, :cond_2

    const-string v0, "HotwordWorker"

    const-string v1, "Tried to pause/resume hotword without setting pending intent"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/app/PendingIntent;->getCreatorPackage()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldoj;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, v0}, Lbgt;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v1, p2, v0}, Ldby;->a(ZLjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final Rm()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldoj;->cq:Z

    .line 217
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 218
    const-string v1, "com.google.android.googlequicksearchbox.action.PAUSE_HOTWORD"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 219
    const-string v1, "com.google.android.googlequicksearchbox.action.RESUME_HOTWORD"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 220
    iget-object v1, p0, Ldoj;->mContext:Landroid/content/Context;

    iget-object v2, p0, Ldoj;->bEg:Landroid/content/BroadcastReceiver;

    const-string v3, "com.google.android.googlequicksearchbox.permission.PAUSE_HOTWORD"

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 223
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 224
    const-string v1, "com.google.android.googlequicksearchbox.action.PAUSE_HOTWORD_FIRST_PARTY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 225
    const-string v1, "com.google.android.googlequicksearchbox.action.RESUME_HOTWORD_FIRST_PARTY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 226
    iget-object v1, p0, Ldoj;->mContext:Landroid/content/Context;

    iget-object v2, p0, Ldoj;->bEh:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0, v4, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 229
    invoke-virtual {p0}, Ldoj;->aef()V

    .line 230
    return-void
.end method

.method final a(ILcom/google/android/shared/search/Query;Lcom/google/android/shared/speech/HotwordResult;)V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v0}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->anv()Lcom/google/android/shared/search/SearchBoxStats;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/search/SearchBoxStats;->QW()Ljava/lang/String;

    move-result-object v0

    .line 378
    invoke-static {p1}, Lege;->hs(I)Litu;

    move-result-object v1

    iget-object v2, p0, Ldoj;->mScreenStateHelper:Ldmh;

    invoke-virtual {v2}, Ldmh;->adp()I

    move-result v2

    invoke-virtual {v1, v2}, Litu;->mK(I)Litu;

    move-result-object v1

    invoke-static {v0}, Lege;->kQ(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Litu;->mH(I)Litu;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aoO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v1

    invoke-virtual {v0, v1}, Litu;->ha(Z)Litu;

    move-result-object v0

    iget-object v1, p0, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v1}, Ldcu;->YZ()Z

    move-result v1

    invoke-virtual {v0, v1}, Litu;->hb(Z)Litu;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/shared/speech/HotwordResult;->asT()F

    move-result v1

    invoke-virtual {v0, v1}, Litu;->R(F)Litu;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/shared/speech/HotwordResult;->asS()F

    move-result v1

    invoke-virtual {v0, v1}, Litu;->Q(F)Litu;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/shared/speech/HotwordResult;->getHotwordScore()F

    move-result v1

    invoke-virtual {v0, v1}, Litu;->P(F)Litu;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/shared/speech/HotwordResult;->asU()F

    move-result v1

    invoke-virtual {v0, v1}, Litu;->S(F)Litu;

    move-result-object v0

    .line 389
    iget-object v1, p0, Ldoj;->mSettingsUtil:Lbyl;

    invoke-virtual {v1}, Lbyl;->BU()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 390
    iget-object v1, p0, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v1}, Ldcu;->Zb()Z

    move-result v1

    invoke-virtual {v0, v1}, Litu;->hc(Z)Litu;

    .line 392
    :cond_0
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 393
    return-void
.end method

.method public a(Lcom/google/android/shared/speech/HotwordResult;Z)V
    .locals 4

    .prologue
    .line 650
    iget-object v0, p0, Ldoj;->bEj:Lhuo;

    if-nez v0, :cond_0

    .line 651
    new-instance v0, Ldoq;

    invoke-direct {v0, p0}, Ldoq;-><init>(Ldoj;)V

    iput-object v0, p0, Ldoj;->bEj:Lhuo;

    .line 653
    :cond_0
    if-eqz p2, :cond_2

    .line 654
    iget-object v0, p0, Ldoj;->bpu:Landroid/os/PowerManager;

    if-nez v0, :cond_1

    iget-object v0, p0, Ldoj;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Ldoj;->bpu:Landroid/os/PowerManager;

    :cond_1
    iget-object v0, p0, Ldoj;->bpu:Landroid/os/PowerManager;

    const v1, 0x1000001a

    const-string v2, "HotwordWorker"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 658
    :cond_2
    iget-object v0, p0, Ldoj;->bEj:Lhuo;

    invoke-interface {v0, p1}, Lhuo;->b(Lcom/google/android/shared/speech/HotwordResult;)V

    .line 659
    return-void
.end method

.method public aef()V
    .locals 3

    .prologue
    .line 253
    iget-object v0, p0, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v0

    .line 254
    iget-object v1, p0, Ldoj;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1}, Lhhq;->ue()Lcse;

    move-result-object v1

    .line 255
    iget-object v2, p0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v1, v0}, Lcse;->iI(Ljava/lang/String;)Lgcq;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldby;->a(Lgcq;)V

    .line 256
    return-void
.end method

.method public final aeg()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 259
    iget-boolean v0, p0, Ldoj;->cq:Z

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Ldoj;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    iget-object v1, p0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v1}, Ldby;->WI()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldoj;->mVoiceSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldoj;->bEe:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2}, Lcse;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 262
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->isActive()Z

    move-result v0

    .line 263
    iget-boolean v1, p0, Ldoj;->bEk:Z

    if-eq v1, v0, :cond_1

    .line 268
    iput-boolean v0, p0, Ldoj;->bEk:Z

    .line 271
    iget-object v0, p0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WF()Z

    move-result v1

    .line 272
    iget-object v0, p0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WG()Z

    move-result v2

    .line 273
    if-eqz v2, :cond_3

    iget-object v0, p0, Ldoj;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0035

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v5}, Ldby;->getPrompt()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 276
    :goto_1
    iget-object v3, p0, Ldoj;->mContext:Landroid/content/Context;

    invoke-static {v3, v1, v2, v0}, Lcom/google/android/googlequicksearchbox/SearchWidgetProvider;->a(Landroid/content/Context;ZZLjava/lang/String;)V

    .line 278
    invoke-static {v1}, Lcom/google/android/ssb/service/SsbService;->fN(Z)V

    .line 296
    :cond_1
    return-void

    .line 260
    :cond_2
    iget-object v0, p0, Ldoj;->mAudioWorker:Ldnz;

    invoke-virtual {v0}, Ldnz;->adX()V

    iget-object v0, p0, Ldoj;->mHotwordState:Ldby;

    invoke-virtual {v0}, Ldby;->WJ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    iput v6, p0, Ldoj;->bEl:I

    iget-object v0, p0, Ldoj;->bEi:Lhum;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldoj;->aeh()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v6}, Ldoj;->dH(Z)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v4}, Ldoj;->dH(Z)V

    goto :goto_0

    .line 273
    :cond_3
    const-string v0, ""

    goto :goto_1

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method aeh()V
    .locals 2

    .prologue
    .line 362
    iget-object v0, p0, Ldoj;->mUiThread:Leqo;

    iget-object v1, p0, Ldoj;->bEf:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Leqo;->i(Ljava/lang/Runnable;)V

    .line 363
    iget-object v0, p0, Ldoj;->bEi:Lhum;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Ldoj;->bEi:Lhum;

    invoke-virtual {v0}, Lhum;->stop()V

    .line 366
    const/4 v0, 0x0

    iput-object v0, p0, Ldoj;->bEi:Lhum;

    .line 368
    :cond_0
    return-void
.end method

.method public at(J)V
    .locals 5

    .prologue
    .line 723
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.googlequicksearchbox.interactor.RESTART_RECOGNITION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 724
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 725
    const-string v1, "delayStartByMilliseconds"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 727
    :cond_0
    iget-object v1, p0, Ldoj;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 728
    return-void
.end method

.method dH(Z)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    .line 337
    iget-object v1, p0, Ldoj;->bEi:Lhum;

    if-nez v1, :cond_1

    .line 339
    iget-object v1, p0, Ldoj;->mVoiceSearchServices:Lhhq;

    invoke-virtual {v1}, Lhhq;->aPi()Lhum;

    move-result-object v1

    iput-object v1, p0, Ldoj;->bEi:Lhum;

    .line 340
    new-instance v1, Ldoq;

    invoke-direct {v1, p0}, Ldoq;-><init>(Ldoj;)V

    iput-object v1, p0, Ldoj;->bEj:Lhuo;

    .line 348
    :goto_0
    const/4 v1, 0x0

    .line 349
    iget-object v2, p0, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v2}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->ang()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 358
    :cond_0
    :goto_1
    iget-object v1, p0, Ldoj;->bEi:Lhum;

    iget-object v2, p0, Ldoj;->bEj:Lhuo;

    iget-object v3, p0, Ldoj;->mGsaConfig:Lchk;

    invoke-virtual {v1, v2, p1, v0, v3}, Lhum;->a(Lhuo;ZILchk;)V

    .line 359
    return-void

    .line 345
    :cond_1
    iget-object v1, p0, Ldoj;->bEi:Lhum;

    invoke-virtual {v1}, Lhum;->stop()V

    goto :goto_0

    .line 351
    :cond_2
    iget-object v2, p0, Ldoj;->mServiceState:Ldcu;

    invoke-virtual {v2}, Ldcu;->Bm()Lcom/google/android/search/shared/service/ClientConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/service/ClientConfig;->anf()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 352
    const/4 v0, 0x3

    goto :goto_1

    .line 353
    :cond_3
    iget-object v2, p0, Ldoj;->mVoiceSettings:Lhym;

    iget-object v3, p0, Ldoj;->mSearchSettings:Lcke;

    invoke-interface {v3}, Lcke;->ND()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lhym;->oC(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_1
.end method

.method public final onStopped()V
    .locals 2

    .prologue
    .line 233
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldoj;->cq:Z

    .line 239
    invoke-virtual {p0}, Ldoj;->aeh()V

    .line 240
    iget-object v0, p0, Ldoj;->mContext:Landroid/content/Context;

    iget-object v1, p0, Ldoj;->bEh:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 241
    iget-object v0, p0, Ldoj;->mContext:Landroid/content/Context;

    iget-object v1, p0, Ldoj;->bEg:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 242
    return-void
.end method
