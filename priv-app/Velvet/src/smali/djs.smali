.class public final Ldjs;
.super Landroid/app/AlertDialog;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field private final bAs:Ldjw;

.field final bAt:Landroid/widget/EditText;

.field private final bAu:Landroid/widget/ListView;

.field private final bAv:Landroid/widget/TextView;

.field private final bAw:I

.field private bAx:Z

.field final mCallback:Lefk;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILefk;Ldjw;I)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 59
    iput-object p3, p0, Ldjs;->mCallback:Lefk;

    .line 60
    iput-object p4, p0, Ldjs;->bAs:Ldjw;

    .line 62
    invoke-virtual {p0}, Ldjs;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 64
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 65
    invoke-virtual {p0, v1}, Ldjs;->setView(Landroid/view/View;)V

    .line 67
    const v0, 0x7f110077

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    .line 68
    const v0, 0x7f110078

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Ldjs;->bAu:Landroid/widget/ListView;

    .line 69
    const v0, 0x7f110079

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldjs;->bAv:Landroid/widget/TextView;

    .line 70
    iput p5, p0, Ldjs;->bAw:I

    .line 71
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 73
    invoke-virtual {p0}, Ldjs;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 74
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 76
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/BaseAdapter;)V
    .locals 2

    .prologue
    .line 93
    new-instance v0, Ldju;

    invoke-direct {v0, p0}, Ldju;-><init>(Ldjs;)V

    invoke-virtual {p1, v0}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 99
    iget-object v0, p0, Ldjs;->bAu:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    iget-object v0, p0, Ldjs;->bAu:Landroid/widget/ListView;

    new-instance v1, Ldjv;

    invoke-direct {v1, p0}, Ldjv;-><init>(Ldjs;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 110
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    iget-object v1, p0, Ldjs;->bAs:Ldjw;

    invoke-interface {v1, v0}, Ldjw;->jd(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public final acv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ldjs;->dx(Z)V

    .line 134
    :cond_0
    iget-object v1, p0, Ldjs;->bAs:Ldjw;

    invoke-interface {v1, v0}, Ldjw;->jd(Ljava/lang/String;)V

    .line 135
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method final dx(Z)V
    .locals 3

    .prologue
    .line 115
    iget-object v0, p0, Ldjs;->bAu:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 116
    :goto_0
    iget-object v1, p0, Ldjs;->bAs:Ldjw;

    invoke-interface {v1}, Ldjw;->Ud()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0a08ab

    .line 118
    :goto_1
    iget-object v2, p0, Ldjs;->bAv:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 119
    if-eqz p1, :cond_3

    if-nez v0, :cond_3

    .line 120
    iget-object v0, p0, Ldjs;->bAu:Landroid/widget/ListView;

    iget-object v1, p0, Ldjs;->bAv:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 125
    :cond_0
    :goto_2
    return-void

    .line 115
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 116
    :cond_2
    iget v1, p0, Ldjs;->bAw:I

    goto :goto_1

    .line 121
    :cond_3
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Ldjs;->bAv:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Ldjs;->bAu:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final jJ(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 150
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 152
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 153
    iput-boolean v1, p0, Ldjs;->bAx:Z

    .line 154
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 156
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 158
    :cond_0
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 2

    .prologue
    .line 81
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Ldjs;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_0

    .line 83
    new-instance v1, Ldjt;

    invoke-direct {v1, p0}, Ldjt;-><init>(Ldjs;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    :cond_0
    return-void
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Ldjs;->bAx:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 163
    iget-object v0, p0, Ldjs;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldjs;->bAx:Z

    .line 166
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 143
    return-void
.end method
