.class public abstract Lfuz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Lfkd;


# static fields
.field static final cCt:Lifw;


# instance fields
.field private final cCm:Landroid/os/Bundle;

.field private final cCn:Ljava/util/ArrayList;

.field private cCo:Lfrc;

.field private cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

.field private cCq:Lfvd;

.field cCr:Ljava/util/List;

.field private cCs:I

.field mCardContainer:Lfmt;

.field final mClock:Lemp;

.field volatile mEntry:Lizj;

.field final mEntryTreeNode:Lizq;

.field private final mModulePresenterFactory:Lftz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 610
    new-instance v0, Lfvc;

    invoke-direct {v0}, Lfvc;-><init>()V

    sput-object v0, Lfuz;->cCt:Lifw;

    return-void
.end method

.method public constructor <init>(Lizj;Lftz;Lemp;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lfuz;->cCm:Landroid/os/Bundle;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    .line 550
    const/4 v0, 0x0

    iput v0, p0, Lfuz;->cCs:I

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    .line 98
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 99
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftz;

    iput-object v0, p0, Lfuz;->mModulePresenterFactory:Lftz;

    .line 100
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemp;

    iput-object v0, p0, Lfuz;->mClock:Lemp;

    .line 101
    return-void
.end method

.method public constructor <init>(Lizq;Lftz;Lemp;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lfuz;->cCm:Landroid/os/Bundle;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    .line 550
    const/4 v0, 0x0

    iput v0, p0, Lfuz;->cCs:I

    .line 105
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizq;

    iput-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    .line 106
    iget-object v0, p1, Lizq;->dUZ:Lizj;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    iput-object v0, p0, Lfuz;->mEntry:Lizj;

    .line 107
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftz;

    iput-object v0, p0, Lfuz;->mModulePresenterFactory:Lftz;

    .line 108
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemp;

    iput-object v0, p0, Lfuz;->mClock:Lemp;

    .line 109
    return-void
.end method

.method private final a(Landroid/content/Context;Lfmt;Lanh;I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 182
    iget-object v0, p3, Lanh;->ahu:Lizj;

    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iput-object v0, p3, Lanh;->ahu:Lizj;

    .line 186
    :cond_0
    iget-object v0, p0, Lfuz;->mModulePresenterFactory:Lftz;

    invoke-virtual {v0, p1, p2, p3}, Lftz;->a(Landroid/content/Context;Lfmt;Lanh;)Lfro;

    move-result-object v3

    .line 189
    if-eqz v3, :cond_2

    .line 190
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p4, v0, :cond_3

    move v0, v1

    .line 191
    :goto_0
    if-eqz v0, :cond_4

    .line 192
    iget-object v4, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    :goto_1
    invoke-virtual {v3, p0, p3}, Lfro;->a(Lfuz;Lanh;)Landroid/view/View;

    move-result-object v3

    .line 199
    invoke-virtual {p3}, Lanh;->oS()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 200
    new-array v1, v1, [Lizj;

    iget-object v4, p3, Lanh;->ahu:Lizj;

    aput-object v4, v1, v2

    invoke-interface {p2, v3, v1}, Lfmt;->a(Landroid/view/View;[Lizj;)V

    .line 203
    :cond_1
    if-eqz v0, :cond_5

    .line 204
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v0, v3}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->addView(Landroid/view/View;)V

    .line 210
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v2

    .line 190
    goto :goto_0

    .line 194
    :cond_4
    iget-object v4, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v4, p4, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 206
    :cond_5
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v0, p4}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->removeViewAt(I)V

    .line 207
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v0, v3, p4}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->addView(Landroid/view/View;I)V

    goto :goto_2
.end method

.method private a(Landroid/content/Context;Lfro;Lanh;I)V
    .locals 3

    .prologue
    .line 341
    if-eqz p3, :cond_2

    .line 342
    invoke-virtual {p2, p3}, Lfro;->b(Lanh;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 347
    invoke-virtual {p2, p3}, Lfro;->a(Lanh;)Landroid/view/View;

    move-result-object v0

    .line 348
    iget-object v1, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v1, p4}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 349
    if-eq v0, v1, :cond_0

    .line 350
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "replacing module view at index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 351
    iget-object v1, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v1, p4}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->removeViewAt(I)V

    .line 352
    iget-object v1, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v1, v0, p4}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->addView(Landroid/view/View;I)V

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    iget-object v0, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-direct {p0, p1, v0, p3, p4}, Lfuz;->a(Landroid/content/Context;Lfmt;Lanh;I)V

    goto :goto_0

    .line 366
    :cond_2
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 367
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v0, p4}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->removeViewAt(I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;[Lanh;)V
    .locals 3
    .param p2    # [Lanh;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 378
    if-nez p2, :cond_1

    .line 390
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    array-length v0, p2

    iget-object v1, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 381
    :cond_2
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->removeAllViews()V

    .line 382
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 383
    iget-object v0, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-virtual {p0, p1, v0, p2}, Lfuz;->a(Landroid/content/Context;Lfmt;[Lanh;)V

    goto :goto_0

    .line 385
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    array-length v0, p2

    if-ge v1, v0, :cond_0

    .line 386
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfro;

    .line 387
    aget-object v2, p2, v1

    invoke-direct {p0, p1, v0, v2, v1}, Lfuz;->a(Landroid/content/Context;Lfro;Lanh;I)V

    .line 385
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public static a(Ljava/util/List;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 619
    if-eqz p1, :cond_0

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 620
    :cond_0
    return-void
.end method


# virtual methods
.method public final G(IZ)V
    .locals 2

    .prologue
    .line 300
    new-instance v0, Lfvb;

    invoke-direct {v0, p0, p1}, Lfvb;-><init>(Lfuz;I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lfuz;->a(Lifw;Z)V

    .line 306
    return-void
.end method

.method public final T(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lfuz;->cCm:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 576
    return-void
.end method

.method public final a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 124
    iput-object p2, p0, Lfuz;->mCardContainer:Lfmt;

    .line 125
    const v0, 0x7f040119

    const/4 v1, 0x0

    invoke-virtual {p3, v0, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    iput-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    .line 126
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v0, p0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 130
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 131
    invoke-virtual {p0, p1, p2}, Lfuz;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_1

    iget-object v1, v0, Lang;->ags:[Lanh;

    if-eqz v1, :cond_1

    .line 134
    invoke-virtual {v0}, Lang;->oN()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->aDs()V

    .line 137
    :cond_0
    iget-object v0, v0, Lang;->ags:[Lanh;

    invoke-virtual {p0, p1, p2, v0}, Lfuz;->a(Landroid/content/Context;Lfmt;[Lanh;)V

    .line 141
    :cond_1
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    if-eqz v0, :cond_2

    .line 142
    new-instance v0, Lfrc;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v2, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-direct {v0, v1, v2}, Lfrc;-><init>(Lizj;Lfmt;)V

    iput-object v0, p0, Lfuz;->cCo:Lfrc;

    .line 143
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    iget-object v1, p0, Lfuz;->cCo:Lfrc;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->a(Lfqx;)V

    .line 146
    :cond_2
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    return-object v0
.end method

.method public final a(Landroid/view/View;Lizj;)Landroid/view/View;
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Landroid/content/Context;Lfmt;)Lang;
.end method

.method protected a(Landroid/content/Context;Lanh;I)Lanh;
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/app/Activity;Lfmt;)Lfry;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 434
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;Lfmt;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 482
    return-void
.end method

.method public a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 464
    return-void
.end method

.method public final a(Landroid/content/Context;Lfmt;[Lanh;)V
    .locals 4

    .prologue
    .line 170
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p3, v0

    .line 172
    iget-object v3, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {p0, p1, p2, v2, v3}, Lfuz;->a(Landroid/content/Context;Lfmt;Lanh;I)V

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Lgbr;)V
    .locals 0

    .prologue
    .line 428
    return-void
.end method

.method public final a(Lfmt;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 488
    return-void
.end method

.method public final a(Lfro;)V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    iget-object v1, p1, Lfro;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->removeView(Landroid/view/View;)V

    .line 163
    return-void
.end method

.method final a(Lifw;Z)V
    .locals 4

    .prologue
    .line 314
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 315
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfro;

    .line 316
    invoke-interface {p1, v0}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 317
    iget-object v2, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 318
    iget-object v3, v0, Lfro;->cBc:Lanh;

    .line 319
    invoke-virtual {p0, v2, v3, v1}, Lfuz;->a(Landroid/content/Context;Lanh;I)Lanh;

    move-result-object v3

    .line 320
    invoke-direct {p0, v2, v0, v3, v1}, Lfuz;->a(Landroid/content/Context;Lfro;Lanh;I)V

    .line 322
    if-nez p2, :cond_1

    .line 323
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 327
    :cond_1
    return-void
.end method

.method public final aAA()Ljava/util/List;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 625
    iget-object v0, p0, Lfuz;->cCr:Ljava/util/List;

    return-object v0
.end method

.method public final aAv()Lizq;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    return-object v0
.end method

.method public aAw()Lizj;
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v0, v0, Lizq;->dUX:[Lizj;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v0, v0, Lizq;->dUX:[Lizj;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aAx()Z
    .locals 1

    .prologue
    .line 415
    const/4 v0, 0x0

    return v0
.end method

.method public final aAy()Lizj;
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p0}, Lfuz;->aAw()Lizj;

    move-result-object v0

    .line 442
    if-eqz v0, :cond_0

    .line 445
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    goto :goto_0
.end method

.method public final aAz()Landroid/os/Bundle;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 565
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfro;

    .line 567
    iget-object v2, p0, Lfuz;->cCm:Landroid/os/Bundle;

    invoke-virtual {v0, v2}, Lfro;->W(Landroid/os/Bundle;)V

    goto :goto_0

    .line 570
    :cond_0
    iget-object v0, p0, Lfuz;->cCm:Landroid/os/Bundle;

    return-object v0
.end method

.method public final aDA()I
    .locals 1

    .prologue
    .line 559
    iget v0, p0, Lfuz;->cCs:I

    return v0
.end method

.method protected final aDB()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 583
    iget-object v0, p0, Lfuz;->cCm:Landroid/os/Bundle;

    return-object v0
.end method

.method public final aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    return-object v0
.end method

.method protected final aDy()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v0

    return-object v0
.end method

.method public final aDz()I
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final aP(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-virtual {p0, p1, v0}, Lfuz;->d(Landroid/content/Context;Lfmt;)[Lanh;

    move-result-object v0

    .line 410
    invoke-direct {p0, p1, v0}, Lfuz;->a(Landroid/content/Context;[Lanh;)V

    .line 411
    return-void
.end method

.method public final ayU()Ljava/lang/String;
    .locals 3

    .prologue
    .line 395
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 396
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 397
    const-string v1, "Adapter"

    invoke-static {v0, v1}, Lesp;->aI(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    const-string v1, "Entry"

    invoke-static {v0, v1}, Lesp;->aI(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 399
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    invoke-static {v1}, Lgbm;->M(Lizj;)Ljava/lang/String;

    move-result-object v1

    .line 400
    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 401
    :cond_0
    return-object v0
.end method

.method public final b(Lfro;)I
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lftp;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 422
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;Lfmt;)V
    .locals 2

    .prologue
    .line 453
    invoke-virtual {p0}, Lfuz;->aAy()Lizj;

    move-result-object v0

    invoke-interface {p2, v0}, Lfmt;->t(Lizj;)V

    .line 454
    const-string v0, "DISMISS_CARD"

    invoke-virtual {p0}, Lfuz;->ayU()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lfmt;->W(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    return-void
.end method

.method public final b(Lfmt;)V
    .locals 0

    .prologue
    .line 509
    iput-object p1, p0, Lfuz;->mCardContainer:Lfmt;

    .line 510
    return-void
.end method

.method public final bP(Z)V
    .locals 3

    .prologue
    .line 591
    iget-object v0, p0, Lfuz;->cCm:Landroid/os/Bundle;

    const-string v1, "QpCardViewAdapter.Expanded"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 592
    return-void
.end method

.method protected d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 226
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f(Lizj;Lizj;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 253
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 254
    const-string v0, "QpCardViewAdapter"

    const-string v1, "Can\'t replace a child entry when there are no modules"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_0
    :goto_0
    return v2

    .line 260
    :cond_1
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    if-eqz v0, :cond_4

    .line 261
    iget-object v0, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v0, v0, Lizq;->dUX:[Lizj;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_4

    .line 262
    iget-object v3, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v3, v3, Lizq;->dUX:[Lizj;

    aget-object v3, v3, v0

    .line 263
    if-ne v3, p1, :cond_2

    .line 264
    iget-object v3, p0, Lfuz;->mEntryTreeNode:Lizq;

    iget-object v3, v3, Lizq;->dUX:[Lizj;

    aput-object p2, v3, v0

    move v0, v1

    .line 274
    :goto_2
    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lfuz;->cCp:Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 276
    invoke-virtual {p0}, Lfuz;->aAw()Lizj;

    move-result-object v3

    .line 278
    if-eqz v3, :cond_3

    .line 279
    iget-object v1, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-virtual {p0, v0, v1}, Lfuz;->d(Landroid/content/Context;Lfmt;)[Lanh;

    move-result-object v1

    .line 281
    invoke-direct {p0, v0, v1}, Lfuz;->a(Landroid/content/Context;[Lanh;)V

    goto :goto_0

    .line 261
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 283
    :cond_3
    invoke-static {p1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    .line 285
    new-instance v3, Lfva;

    invoke-direct {v3, p0, v0}, Lfva;-><init>(Lfuz;Lgbg;)V

    invoke-virtual {p0, v3, v1}, Lfuz;->a(Lifw;Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method public final getEntry()Lizj;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    return-object v0
.end method

.method public final jG(I)V
    .locals 2

    .prologue
    .line 552
    iput p1, p0, Lfuz;->cCs:I

    .line 553
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfro;

    .line 554
    invoke-virtual {v0, p1}, Lfro;->jw(I)V

    goto :goto_0

    .line 556
    :cond_0
    return-void
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 529
    const/4 v1, 0x0

    .line 530
    iget-object v0, p0, Lfuz;->cCn:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfro;

    .line 531
    iget-object v0, v0, Lfro;->cBc:Lanh;

    invoke-virtual {v0}, Lanh;->oU()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    const/4 v0, 0x1

    .line 536
    :goto_0
    if-eqz v0, :cond_1

    .line 537
    new-instance v0, Lfvd;

    invoke-direct {v0, p0}, Lfvd;-><init>(Lfuz;)V

    iput-object v0, p0, Lfuz;->cCq:Lfvd;

    .line 538
    iget-object v0, p0, Lfuz;->mClock:Lemp;

    iget-object v1, p0, Lfuz;->cCq:Lfvd;

    invoke-interface {v0, v1}, Lemp;->a(Lemr;)V

    .line 540
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 544
    iget-object v0, p0, Lfuz;->cCq:Lfvd;

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lfuz;->mClock:Lemp;

    iget-object v1, p0, Lfuz;->cCq:Lfvd;

    invoke-interface {v0, v1}, Lemp;->b(Lemr;)V

    .line 546
    const/4 v0, 0x0

    iput-object v0, p0, Lfuz;->cCq:Lfvd;

    .line 548
    :cond_0
    return-void
.end method

.method public final s(Lizj;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lfuz;->mEntry:Lizj;

    .line 119
    return-void
.end method

.method public final ul()Z
    .locals 3

    .prologue
    .line 598
    iget-object v0, p0, Lfuz;->cCm:Landroid/os/Bundle;

    const-string v1, "QpCardViewAdapter.Expanded"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
