.class public final Leoh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static cgF:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-boolean v0, Leoh;->cgF:Z

    return-void
.end method

.method public static as(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 37
    sget v0, Lesp;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.permission.CAPTURE_AUDIO_HOTWORD"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static at(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, Leoh;->as(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lesp;->ax(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized auT()V
    .locals 2

    .prologue
    .line 24
    const-class v1, Leoh;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Leoh;->cgF:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 29
    :goto_0
    monitor-exit v1

    return-void

    .line 27
    :cond_0
    :try_start_1
    const-string v0, "google_hotword_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 28
    const/4 v0, 0x1

    sput-boolean v0, Leoh;->cgF:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 24
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
