.class public final Lgcb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgaj;


# instance fields
.field private final dz:Landroid/net/Uri;

.field private final mActivityHelper:Lfzw;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLfzw;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lgcb;->mContext:Landroid/content/Context;

    .line 31
    const-wide/32 v0, 0x12d687

    invoke-static {v0, v1}, Lgbe;->bt(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->dz:Landroid/net/Uri;

    .line 32
    iput-object p4, p0, Lgcb;->mActivityHelper:Lfzw;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljbc;Lfzw;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lgcb;->mContext:Landroid/content/Context;

    .line 45
    invoke-static {p2}, Ledx;->a(Ljbc;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcb;->dz:Landroid/net/Uri;

    .line 46
    iput-object p3, p0, Lgcb;->mActivityHelper:Lfzw;

    .line 47
    return-void
.end method


# virtual methods
.method public final getPermissions()I
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x2

    return v0
.end method

.method public final getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lgcb;->dz:Landroid/net/Uri;

    return-object v0
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lgcb;->mActivityHelper:Lfzw;

    iget-object v1, p0, Lgcb;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lgcb;->dz:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lfzw;->a(Landroid/content/Context;Landroid/net/Uri;Z)Z

    .line 57
    return-void
.end method
