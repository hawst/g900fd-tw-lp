.class public Lgyd;
.super Lgyy;
.source "PG"

# interfaces
.implements Ldex;


# instance fields
.field private final cZa:Lgzz;

.field private final cZe:Lgyc;

.field private cZf:I

.field private cZg:Ldsj;


# direct methods
.method public constructor <init>(Lgzz;)V
    .locals 1

    .prologue
    .line 47
    const-string v0, "searchplate"

    invoke-direct {p0, v0}, Lgyy;-><init>(Ljava/lang/String;)V

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lgyd;->cZf:I

    .line 162
    new-instance v0, Lgye;

    invoke-direct {v0, p0}, Lgye;-><init>(Lgyd;)V

    iput-object v0, p0, Lgyd;->cZg:Ldsj;

    .line 48
    iput-object p1, p0, Lgyd;->cZa:Lgzz;

    .line 49
    iget-object v0, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v0, p0}, Lgzz;->b(Lgyd;)V

    .line 50
    new-instance v0, Lgyc;

    invoke-direct {v0, p1}, Lgyc;-><init>(Lgzz;)V

    iput-object v0, p0, Lgyd;->cZe:Lgyc;

    .line 51
    return-void
.end method

.method static synthetic a(Lgyd;)Lgzz;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lgyd;->cZa:Lgzz;

    return-object v0
.end method


# virtual methods
.method public final UK()V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v0}, Lgzz;->UK()V

    .line 127
    return-void
.end method

.method protected final X(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lgyy;->mPresenter:Lgyz;

    invoke-virtual {v0}, Lgyz;->aJU()Lgpu;

    move-result-object v0

    invoke-virtual {v0}, Lgpu;->aJu()Lddn;

    move-result-object v0

    .line 56
    invoke-virtual {p0}, Lgyd;->aLH()Lgyz;

    move-result-object v1

    invoke-virtual {v1}, Lgyz;->UG()Ldep;

    move-result-object v1

    .line 57
    invoke-virtual {v1, p0, v0}, Ldep;->a(Ldex;Lddn;)V

    .line 58
    iget-object v1, p0, Lgyd;->cZe:Lgyc;

    invoke-virtual {p0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->wj()Lekf;

    move-result-object v2

    iget-object v0, v1, Lgyc;->mScrollView:Lekf;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    iput-object v2, v1, Lgyc;->mScrollView:Lekf;

    iget-object v0, v1, Lgyc;->mScrollView:Lekf;

    invoke-interface {v0, v1}, Lekf;->a(Lekg;)V

    invoke-virtual {v1}, Lgyc;->aLC()V

    .line 60
    if-eqz p1, :cond_0

    .line 61
    iget-object v0, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v0, p1}, Lgzz;->x(Landroid/os/Bundle;)V

    .line 63
    :cond_0
    return-void

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ZZLgyt;Z)V
    .locals 4

    .prologue
    .line 74
    invoke-virtual {p0}, Lgyd;->Nf()Ldda;

    move-result-object v0

    .line 75
    invoke-virtual {p0}, Lgyd;->Nf()Ldda;

    move-result-object v1

    invoke-virtual {v1}, Ldda;->aap()Ldct;

    move-result-object v1

    invoke-virtual {v1}, Ldct;->Yw()I

    move-result v1

    .line 76
    iget-object v2, p0, Lgyd;->cZa:Lgzz;

    invoke-virtual {p0}, Lgyd;->Nf()Ldda;

    move-result-object v3

    invoke-virtual {v3}, Ldda;->aap()Ldct;

    move-result-object v3

    invoke-virtual {v3}, Ldct;->Yx()I

    move-result v3

    invoke-interface {v2, v1, v3, p2}, Lgzz;->c(IIZ)V

    .line 83
    if-eqz p1, :cond_0

    .line 84
    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v2

    .line 85
    invoke-virtual {v2}, Ldcy;->ZF()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    invoke-virtual {v2}, Ldcy;->ZE()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 89
    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xw()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v2}, Lgzz;->UK()V

    .line 98
    :cond_0
    :goto_0
    iget-object v2, p0, Lgyd;->cZa:Lgzz;

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v3

    invoke-virtual {v3}, Ldct;->getProgress()I

    move-result v3

    invoke-interface {v2, v3}, Lgzz;->fv(I)V

    .line 99
    iget-object v2, p0, Lgyd;->cZa:Lgzz;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZD()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p4, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2, v0}, Lgzz;->dN(Z)V

    .line 101
    invoke-virtual {p0}, Lgyd;->aAU()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lgyd;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aap()Ldct;

    move-result-object v0

    invoke-virtual {v0}, Ldct;->Yv()I

    move-result v2

    iget v3, p0, Lgyd;->cZf:I

    if-eq v3, v2, :cond_1

    iget-object v3, p0, Lgyd;->cZa:Lgzz;

    invoke-virtual {v0}, Ldct;->tR()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v2, v0, p2}, Lgzz;->a(ILjava/lang/String;Z)V

    iput v2, p0, Lgyd;->cZf:I

    .line 102
    :cond_1
    iget-object v0, p0, Lgyd;->cZe:Lgyc;

    iget v2, v0, Lgyc;->bso:I

    if-eq v2, v1, :cond_2

    iput v1, v0, Lgyc;->bso:I

    invoke-virtual {v0}, Lgyc;->aLC()V

    .line 103
    :cond_2
    return-void

    .line 93
    :cond_3
    iget-object v2, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v2}, Lgzz;->aLD()V

    goto :goto_0

    .line 99
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final aKF()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-virtual {p0}, Lgyd;->aLH()Lgyz;

    move-result-object v0

    invoke-virtual {v0}, Lgyz;->UG()Ldep;

    move-result-object v0

    .line 68
    invoke-virtual {v0, p0}, Ldep;->b(Ldex;)V

    .line 69
    iget-object v1, p0, Lgyd;->cZe:Lgyc;

    iget-object v0, v1, Lgyc;->mScrollView:Lekf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-virtual {v1, v2}, Lgyc;->a(Lcom/google/android/search/suggest/SuggestionListView;)V

    iget-object v0, v1, Lgyc;->mScrollView:Lekf;

    invoke-interface {v0, v1}, Lekf;->b(Lekg;)V

    iput-object v2, v1, Lgyc;->mScrollView:Lekf;

    invoke-virtual {v1}, Lgyc;->aLC()V

    .line 70
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aLD()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v0}, Lgzz;->aLD()V

    .line 131
    return-void
.end method

.method public final aLE()Ldsj;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lgyd;->cZg:Ldsj;

    return-object v0
.end method

.method public final aX(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v0, p1}, Lgzz;->aX(Lcom/google/android/shared/search/Query;)V

    .line 139
    return-void
.end method

.method public final b(Lcom/google/android/search/suggest/SuggestionListView;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lgyd;->cZe:Lgyc;

    invoke-virtual {v0, p1}, Lgyc;->a(Lcom/google/android/search/suggest/SuggestionListView;)V

    .line 160
    return-void
.end method

.method public final c(Landroid/util/SparseArray;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    .line 146
    invoke-interface {v0}, Ldef;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 147
    invoke-interface {v0, v2}, Ldef;->fJ(I)Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spanned;

    .line 149
    iget-object v1, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v1, v0}, Lgzz;->b(Landroid/text/Spanned;)V

    .line 151
    :cond_0
    return-void
.end method

.method public final z(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lgyd;->cZa:Lgzz;

    invoke-interface {v0, p1}, Lgzz;->z(Lcom/google/android/shared/search/Query;)V

    .line 135
    return-void
.end method
