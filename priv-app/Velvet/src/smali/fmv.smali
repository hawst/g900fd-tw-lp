.class public final Lfmv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cwA:Lizo;

.field private cwB:Lfrd;

.field cwC:Landroid/net/Uri;

.field private cwD:Leml;

.field private cwE:Lemy;

.field cwF:Z

.field cwG:Lizj;

.field private final cwo:Lfmy;

.field private final cwp:Lfnf;

.field public final cwq:Ljava/lang/Object;

.field public cwr:Landroid/view/View;

.field public cws:Lfkm;

.field private final cwt:Ljava/lang/Object;

.field private cwu:Lfnb;

.field cwv:Lfne;

.field private cww:Z

.field final cwx:Ljava/lang/Object;

.field public cwy:J

.field public cwz:Z

.field final mActivityHelper:Lfzw;

.field final mAppContext:Landroid/content/Context;

.field private final mCardViewAdapterFactory:Lfjs;

.field final mClock:Lemp;

.field private final mFifeImageUrlUtil:Lgan;

.field private final mImageLoader:Lesm;

.field final mModulePresenterFactory:Lftz;

.field final mNowRemoteClient:Lfml;

.field mPresenter:Lfnc;

.field final mRunner:Lerk;

.field public final mViewActionRecorder:Lfns;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lerk;Lfjs;Lfzw;Lfml;Lfns;Lgan;Lesm;Lemp;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    new-instance v0, Lfmy;

    invoke-direct {v0, p0}, Lfmy;-><init>(Lfmv;)V

    iput-object v0, p0, Lfmv;->cwo:Lfmy;

    .line 222
    new-instance v0, Lfnf;

    invoke-direct {v0, p0}, Lfnf;-><init>(Lfmv;)V

    iput-object v0, p0, Lfmv;->cwp:Lfnf;

    .line 227
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfmv;->cwq:Ljava/lang/Object;

    .line 233
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfmv;->cwt:Ljava/lang/Object;

    .line 239
    iput-boolean v2, p0, Lfmv;->cww:Z

    .line 242
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfmv;->cwx:Ljava/lang/Object;

    .line 243
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfmv;->cwy:J

    .line 245
    iput-boolean v2, p0, Lfmv;->cwz:Z

    .line 247
    iput-object v3, p0, Lfmv;->cwA:Lizo;

    .line 253
    iput-object v3, p0, Lfmv;->cwC:Landroid/net/Uri;

    .line 258
    iput-boolean v2, p0, Lfmv;->cwF:Z

    .line 272
    iput-object p1, p0, Lfmv;->mAppContext:Landroid/content/Context;

    .line 273
    iput-object p2, p0, Lfmv;->mRunner:Lerk;

    .line 274
    iput-object p3, p0, Lfmv;->mCardViewAdapterFactory:Lfjs;

    .line 275
    iput-object p4, p0, Lfmv;->mActivityHelper:Lfzw;

    .line 276
    iput-object p5, p0, Lfmv;->mNowRemoteClient:Lfml;

    .line 277
    iput-object p6, p0, Lfmv;->mViewActionRecorder:Lfns;

    .line 278
    iput-object p7, p0, Lfmv;->mFifeImageUrlUtil:Lgan;

    .line 279
    iput-object p8, p0, Lfmv;->mImageLoader:Lesm;

    .line 280
    iput-object p9, p0, Lfmv;->mClock:Lemp;

    .line 281
    new-instance v0, Lftz;

    iget-object v1, p0, Lfmv;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lfmv;->mRunner:Lerk;

    iget-object v3, p0, Lfmv;->mClock:Lemp;

    invoke-direct {v0, v1, v2, v3}, Lftz;-><init>(Lfzw;Lerk;Lemp;)V

    iput-object v0, p0, Lfmv;->mModulePresenterFactory:Lftz;

    .line 282
    return-void
.end method

.method private aBl()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 778
    iget-object v0, p0, Lfmv;->cwD:Leml;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmv;->cwE:Lemy;

    if-eqz v0, :cond_0

    .line 779
    iget-object v0, p0, Lfmv;->cwD:Leml;

    iget-object v1, p0, Lfmv;->cwE:Lemy;

    invoke-interface {v0, v1}, Leml;->f(Lemy;)V

    .line 780
    iput-object v2, p0, Lfmv;->cwD:Leml;

    .line 781
    iput-object v2, p0, Lfmv;->cwE:Lemy;

    .line 783
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;)Lfna;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 593
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->getResponseCode()I

    move-result v2

    .line 596
    if-ne v2, v0, :cond_4

    .line 597
    iget-object v8, p0, Lfmv;->cwx:Ljava/lang/Object;

    monitor-enter v8

    .line 599
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBS()J

    move-result-wide v2

    iget-wide v4, p0, Lfmv;->cwy:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 600
    monitor-exit v8

    .line 705
    :goto_0
    return-object v1

    .line 603
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBS()J

    move-result-wide v2

    iput-wide v2, p0, Lfmv;->cwy:J

    .line 604
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->awY()Z

    move-result v2

    iput-boolean v2, p0, Lfmv;->cwz:Z

    .line 606
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBR()Lizn;

    move-result-object v3

    .line 607
    iget-object v2, v3, Lizn;->dUI:[Lizo;

    array-length v2, v2

    if-lez v2, :cond_5

    iget-object v2, v3, Lizn;->dUI:[Lizo;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget-object v2, v2, Lizo;->dUQ:Lizq;

    if-eqz v2, :cond_5

    iget-object v2, v3, Lizn;->dUI:[Lizo;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget-object v2, v2, Lizo;->dUQ:Lizq;

    iget-object v2, v2, Lizq;->dUW:[Lizq;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_b

    .line 609
    iget-object v2, v3, Lizn;->dUI:[Lizo;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    .line 615
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->awY()Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lfmv;->cwA:Lizo;

    if-eqz v2, :cond_c

    .line 621
    iget-object v2, p0, Lfmv;->cwA:Lizo;

    iget-object v2, v2, Lizo;->dUQ:Lizq;

    iget-object v2, v2, Lizq;->dUW:[Lizq;

    array-length v4, v2

    .line 622
    iget-object v2, v3, Lizo;->dUQ:Lizq;

    iget-object v2, v2, Lizq;->dUW:[Lizq;

    array-length v5, v2

    .line 623
    if-ge v5, v4, :cond_6

    move-object v2, v3

    move v6, v0

    .line 646
    :goto_2
    invoke-static {v3}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizo;

    iput-object v0, p0, Lfmv;->cwA:Lizo;

    .line 649
    if-eqz v2, :cond_9

    .line 650
    new-instance v0, Lfll;

    iget-object v1, p0, Lfmv;->mCardViewAdapterFactory:Lfjs;

    invoke-direct {v0, v1}, Lfll;-><init>(Lfjs;)V

    invoke-virtual {v0, v2}, Lfll;->k(Lizo;)Ljava/util/List;

    move-result-object v7

    .line 657
    if-eqz v6, :cond_3

    .line 660
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBU()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v9, :cond_1

    .line 662
    const/4 v9, 0x2

    new-instance v0, Lfxl;

    iget-object v1, p0, Lfmv;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfmv;->mClock:Lemp;

    const v3, 0x7f0a051c

    const v4, 0x7f020233

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lfxl;-><init>(Lftz;Lemp;III)V

    invoke-interface {v7, v9, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 668
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBT()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 669
    const/4 v9, 0x0

    new-instance v0, Lfxl;

    iget-object v1, p0, Lfmv;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfmv;->mClock:Lemp;

    const v3, 0x7f0a051b

    const v4, 0x7f02022e

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lfxl;-><init>(Lftz;Lemp;III)V

    invoke-interface {v7, v9, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 676
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->aBV()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 677
    new-instance v0, Lfkq;

    iget-object v1, p0, Lfmv;->mActivityHelper:Lfzw;

    iget-object v2, p0, Lfmv;->mClock:Lemp;

    iget-wide v4, p1, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->cxF:J

    invoke-direct {v0, v1, v2, v4, v5}, Lfkq;-><init>(Lfzw;Lemp;J)V

    .line 680
    const/4 v1, 0x0

    invoke-interface {v7, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 686
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;->awY()Z

    move-result v0

    if-nez v0, :cond_a

    .line 687
    new-instance v0, Lfkm;

    iget-object v1, p0, Lfmv;->mActivityHelper:Lfzw;

    invoke-direct {v0, v1, p0}, Lfkm;-><init>(Lfzw;Lfmv;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v6

    move-object v1, v7

    .line 701
    :goto_3
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 705
    :cond_4
    new-instance v2, Lfna;

    invoke-direct {v2, p1, v1, v0}, Lfna;-><init>(Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;Ljava/util/List;Z)V

    move-object v1, v2

    goto/16 :goto_0

    :cond_5
    move v2, v6

    .line 607
    goto/16 :goto_1

    .line 625
    :cond_6
    if-ne v4, v5, :cond_7

    .line 628
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2}, Lfmv;->fs(Z)V

    move-object v2, v1

    move v6, v0

    .line 629
    goto/16 :goto_2

    .line 633
    :cond_7
    new-instance v2, Lizo;

    invoke-direct {v2}, Lizo;-><init>()V

    .line 634
    new-instance v0, Lizq;

    invoke-direct {v0}, Lizq;-><init>()V

    iput-object v0, v2, Lizo;->dUQ:Lizq;

    .line 635
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v0, v4

    .line 636
    :goto_4
    if-ge v0, v5, :cond_8

    .line 637
    iget-object v4, v3, Lizo;->dUQ:Lizq;

    iget-object v4, v4, Lizq;->dUW:[Lizq;

    aget-object v4, v4, v0

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 636
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 639
    :cond_8
    iget-object v4, v2, Lizo;->dUQ:Lizq;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lizq;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lizq;

    iput-object v0, v4, Lizq;->dUW:[Lizq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 701
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 692
    :cond_9
    :try_start_2
    invoke-virtual {p0}, Lfmv;->aAX()V

    .line 693
    monitor-exit v8

    goto/16 :goto_0

    :cond_a
    move v0, v6

    move-object v1, v7

    .line 695
    goto :goto_3

    .line 697
    :cond_b
    const/4 v1, 0x1

    new-array v1, v1, [Lfkd;

    const/4 v2, 0x0

    new-instance v3, Lfko;

    iget-object v4, p0, Lfmv;->mActivityHelper:Lfzw;

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct {v3, v4, v5, v6}, Lfko;-><init>(Lfzw;ZZ)V

    aput-object v3, v1, v2

    invoke-static {v1}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    .line 699
    const/4 v2, 0x0

    iput-object v2, p0, Lfmv;->cwA:Lizo;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_c
    move-object v2, v3

    move v6, v0

    goto/16 :goto_2
.end method

.method public final a(JZLizo;)V
    .locals 3

    .prologue
    .line 717
    iget-object v1, p0, Lfmv;->cwx:Ljava/lang/Object;

    monitor-enter v1

    .line 718
    :try_start_0
    iput-wide p1, p0, Lfmv;->cwy:J

    .line 719
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfmv;->cwz:Z

    .line 720
    iput-object p4, p0, Lfmv;->cwA:Lizo;

    .line 721
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 787
    invoke-static {p3}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 788
    :goto_0
    iput-object p1, p0, Lfmv;->cwC:Landroid/net/Uri;

    .line 789
    iget-object v1, p0, Lfmv;->mPresenter:Lfnc;

    if-eqz v0, :cond_1

    :goto_1
    invoke-interface {v1, p2, v0, p3}, Lfnc;->a(Landroid/graphics/drawable/Drawable;ZLjava/lang/String;)V

    .line 790
    return-void

    .line 787
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 789
    :cond_1
    const/4 p3, 0x0

    goto :goto_1
.end method

.method public final a(Lfnc;)V
    .locals 1

    .prologue
    .line 285
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnc;

    iput-object v0, p0, Lfmv;->mPresenter:Lfnc;

    .line 286
    iget-object v0, p0, Lfmv;->mViewActionRecorder:Lfns;

    invoke-virtual {v0, p1}, Lfns;->a(Lfnl;)V

    .line 287
    return-void
.end method

.method public final a(Lfrg;)V
    .locals 6

    .prologue
    .line 315
    iget-object v0, p0, Lfmv;->cwB:Lfrd;

    if-nez v0, :cond_0

    .line 316
    new-instance v0, Lfrd;

    iget-object v1, p0, Lfmv;->mAppContext:Landroid/content/Context;

    iget-object v2, p0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v2}, Lfnc;->wj()Lekf;

    move-result-object v2

    iget-object v3, p0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v3}, Lfnc;->wi()Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-result-object v3

    new-instance v5, Lfnd;

    invoke-direct {v5, p0}, Lfnd;-><init>(Lfmv;)V

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lfrd;-><init>(Landroid/content/Context;Lekf;Lcom/google/android/shared/ui/SuggestionGridLayout;Lfrg;Lfrf;)V

    iput-object v0, p0, Lfmv;->cwB:Lfrd;

    .line 322
    iget-object v0, p0, Lfmv;->cwB:Lfrd;

    invoke-virtual {v0}, Lfrd;->axt()V

    .line 324
    :cond_0
    return-void
.end method

.method final a(Ljcn;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljcn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 752
    iget-object v0, p0, Lfmv;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Leot;->av(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 754
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljcn;->bgJ()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 755
    iget-object v1, p0, Lfmv;->mFifeImageUrlUtil:Lgan;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1}, Ljcn;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lgan;->b(IILjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 758
    iget-object v0, p0, Lfmv;->cwC:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 760
    invoke-direct {p0}, Lfmv;->aBl()V

    .line 762
    iget-object v0, p0, Lfmv;->mImageLoader:Lesm;

    invoke-interface {v0, v1}, Lesm;->D(Landroid/net/Uri;)Leml;

    move-result-object v0

    .line 763
    invoke-interface {v0}, Leml;->auB()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 764
    invoke-interface {v0}, Leml;->auC()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1, v0, p2}, Lfmv;->a(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 775
    :cond_0
    :goto_0
    return-void

    .line 766
    :cond_1
    new-instance v2, Lfmz;

    invoke-direct {v2, p0, v1, p2}, Lfmz;-><init>(Lfmv;Landroid/net/Uri;Ljava/lang/String;)V

    iput-object v2, p0, Lfmv;->cwE:Lemy;

    .line 767
    iget-object v1, p0, Lfmv;->cwE:Lemy;

    invoke-interface {v0, v1}, Leml;->e(Lemy;)V

    .line 768
    iput-object v0, p0, Lfmv;->cwD:Leml;

    goto :goto_0

    .line 773
    :cond_2
    new-instance v1, Lfng;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v1, p0, v2, v0}, Lfng;-><init>(Lfmv;II)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lfng;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final aAX()V
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v0}, Lfnc;->aAX()V

    .line 504
    return-void
.end method

.method public final aBg()V
    .locals 4

    .prologue
    .line 291
    iget-boolean v0, p0, Lfmv;->cww:Z

    if-nez v0, :cond_0

    .line 292
    iget-object v0, p0, Lfmv;->mAppContext:Landroid/content/Context;

    iget-object v1, p0, Lfmv;->cwo:Lfmy;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.apps.now.ENTRIES_UPDATED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 295
    iget-object v0, p0, Lfmv;->mNowRemoteClient:Lfml;

    iget-object v1, p0, Lfmv;->cwp:Lfnf;

    invoke-virtual {v0, v1}, Lfml;->d(Lesk;)V

    .line 297
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfmv;->cww:Z

    .line 299
    :cond_0
    return-void
.end method

.method public final aBh()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lfmv;->cwB:Lfrd;

    if-eqz v0, :cond_0

    .line 336
    iget-object v0, p0, Lfmv;->cwB:Lfrd;

    invoke-virtual {v0}, Lfrd;->unregister()V

    .line 337
    const/4 v0, 0x0

    iput-object v0, p0, Lfmv;->cwB:Lfrd;

    .line 339
    :cond_0
    return-void
.end method

.method public final aBi()V
    .locals 2

    .prologue
    .line 342
    iget-boolean v0, p0, Lfmv;->cww:Z

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lfmv;->mAppContext:Landroid/content/Context;

    iget-object v1, p0, Lfmv;->cwo:Lfmy;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 344
    iget-object v0, p0, Lfmv;->mNowRemoteClient:Lfml;

    iget-object v1, p0, Lfmv;->cwp:Lfnf;

    invoke-virtual {v0, v1}, Lfml;->e(Lesk;)V

    .line 345
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfmv;->cww:Z

    .line 347
    :cond_0
    invoke-direct {p0}, Lfmv;->aBl()V

    .line 348
    invoke-virtual {p0}, Lfmv;->aBk()V

    .line 349
    return-void
.end method

.method public final aBj()V
    .locals 3

    .prologue
    .line 374
    iget-object v0, p0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v0}, Lfnc;->aAU()Z

    move-result v0

    if-nez v0, :cond_1

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 381
    :cond_1
    iget-object v0, p0, Lfmv;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lfmv;->mPresenter:Lfnc;

    invoke-interface {v0}, Lfnc;->wk()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 389
    iget-object v0, p0, Lfmv;->mNowRemoteClient:Lfml;

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    invoke-interface {v0}, Lfor;->ayp()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    :cond_2
    :goto_1
    iget-object v1, p0, Lfmv;->cwt:Ljava/lang/Object;

    monitor-enter v1

    .line 394
    :try_start_1
    iget-object v0, p0, Lfmv;->cwu:Lfnb;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfmv;->cwu:Lfnb;

    invoke-virtual {v0}, Lfnb;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v2, :cond_4

    .line 396
    :cond_3
    new-instance v0, Lfnb;

    invoke-direct {v0, p0}, Lfnb;-><init>(Lfmv;)V

    iput-object v0, p0, Lfmv;->cwu:Lfnb;

    .line 397
    iget-object v0, p0, Lfmv;->cwu:Lfnb;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lfnb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 399
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 389
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error making request to record interaction"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method final aBk()V
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lfmv;->cwv:Lfne;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lfmv;->mRunner:Lerk;

    iget-object v1, p0, Lfmv;->cwv:Lfne;

    invoke-interface {v0, v1}, Lerk;->b(Lesk;)V

    .line 478
    const/4 v0, 0x0

    iput-object v0, p0, Lfmv;->cwv:Lfne;

    .line 480
    :cond_0
    return-void
.end method

.method public final aa(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 575
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 576
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    .line 577
    invoke-interface {v0}, Lfkd;->ayU()Ljava/lang/String;

    move-result-object v3

    .line 578
    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 579
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 581
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 585
    :cond_1
    const-string v0, "CARD_RENDER"

    invoke-static {v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;->c(Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;

    move-result-object v0

    .line 588
    iget-object v1, p0, Lfmv;->mNowRemoteClient:Lfml;

    invoke-virtual {v1, v0}, Lfml;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V

    .line 589
    return-void
.end method

.method public final c(Landroid/os/Bundle;Z)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1152
    .line 1153
    if-eqz p1, :cond_4

    .line 1155
    const-string v0, "target_entry"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1156
    const-string v0, "target_entry"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->Q([B)Lizj;

    move-result-object v0

    .line 1159
    if-eqz p2, :cond_0

    const-string v2, "target_entry"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1164
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    const-string v2, "notification_entries"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1166
    const-string v0, "notification_entries"

    invoke-static {p1, v0}, Lgbm;->c(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1168
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 1170
    :goto_1
    if-eqz p2, :cond_1

    const-string v1, "notification_entries"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 1174
    :cond_1
    :goto_2
    iput-object v0, p0, Lfmv;->cwG:Lizj;

    .line 1175
    return-void

    :cond_2
    move-object v0, v1

    .line 1168
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public final f(IIZ)V
    .locals 4

    .prologue
    .line 421
    invoke-static {p2}, Lgal;->jP(I)Landroid/content/Intent;

    move-result-object v0

    .line 422
    const-string v1, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 425
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    iget-boolean v1, p0, Lfmv;->cwz:Z

    if-eqz v1, :cond_0

    .line 427
    const/4 p1, 0x3

    .line 430
    :cond_0
    const-string v1, "com.google.android.apps.sidekick.TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 431
    const-string v1, "com.google.android.apps.sidekick.SAVE_CALL_LOG"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 432
    iget-object v1, p0, Lfmv;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 435
    if-eqz p1, :cond_1

    invoke-static {p1}, Lflk;->iS(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    :cond_1
    iget-object v0, p0, Lfmv;->cwv:Lfne;

    if-nez v0, :cond_2

    new-instance v0, Lfne;

    invoke-direct {v0, p0}, Lfne;-><init>(Lfmv;)V

    iput-object v0, p0, Lfmv;->cwv:Lfne;

    iget-object v0, p0, Lfmv;->mRunner:Lerk;

    iget-object v1, p0, Lfmv;->cwv:Lfne;

    const-wide/16 v2, 0x4e20

    invoke-interface {v0, v1, v2, v3}, Lerk;->a(Lesk;J)V

    .line 439
    :cond_2
    return-void
.end method

.method fs(Z)V
    .locals 8

    .prologue
    .line 530
    iget-object v6, p0, Lfmv;->cwq:Ljava/lang/Object;

    monitor-enter v6

    .line 531
    :try_start_0
    iget-object v0, p0, Lfmv;->cws:Lfkm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmv;->cwr:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 534
    iget-object v4, p0, Lfmv;->cws:Lfkm;

    .line 535
    iget-object v5, p0, Lfmv;->cwr:Landroid/view/View;

    .line 538
    const/4 v0, 0x0

    iput-object v0, p0, Lfmv;->cws:Lfkm;

    .line 539
    const/4 v0, 0x0

    iput-object v0, p0, Lfmv;->cwr:Landroid/view/View;

    .line 541
    iget-object v7, p0, Lfmv;->mRunner:Lerk;

    new-instance v0, Lfmw;

    const-string v2, "Show load cards"

    move-object v1, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lfmw;-><init>(Lfmv;Ljava/lang/String;ZLfkm;Landroid/view/View;)V

    invoke-interface {v7, v0}, Lerk;->a(Lesk;)V

    .line 558
    :cond_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final reset()V
    .locals 4

    .prologue
    .line 352
    invoke-virtual {p0}, Lfmv;->aBk()V

    .line 353
    iget-object v1, p0, Lfmv;->cwx:Ljava/lang/Object;

    monitor-enter v1

    .line 354
    const-wide/16 v2, 0x0

    :try_start_0
    iput-wide v2, p0, Lfmv;->cwy:J

    .line 355
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfmv;->cwz:Z

    .line 356
    const/4 v0, 0x0

    iput-object v0, p0, Lfmv;->cwA:Lizo;

    .line 357
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
