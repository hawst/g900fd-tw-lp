.class public abstract Lrk;
.super Lqz;
.source "PG"


# static fields
.field private static wU:Ljava/util/HashMap;

.field private static wV:Lrl;


# instance fields
.field private rI:Landroid/graphics/Bitmap;

.field private wT:Z

.field private wW:Z

.field private wX:Z

.field private wY:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lrk;->wU:Ljava/util/HashMap;

    .line 49
    new-instance v0, Lrl;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lrl;-><init>(B)V

    sput-object v0, Lrk;->wV:Lrl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lrk;-><init>(Z)V

    .line 67
    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2, v2}, Lqz;-><init>(Lrb;II)V

    .line 53
    iput-boolean v1, p0, Lrk;->wW:Z

    .line 56
    iput-boolean v1, p0, Lrk;->wT:Z

    .line 58
    iput-boolean v2, p0, Lrk;->wX:Z

    .line 71
    if-eqz p1, :cond_0

    .line 72
    iput-boolean v1, p0, Lqz;->wr:Z

    .line 73
    iput v1, p0, Lrk;->wY:I

    .line 75
    :cond_0
    return-void
.end method

.method private static a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 120
    sget-object v1, Lrk;->wV:Lrl;

    .line 121
    iput-boolean p0, v1, Lrl;->wZ:Z

    .line 122
    iput-object p1, v1, Lrl;->xa:Landroid/graphics/Bitmap$Config;

    .line 123
    iput p2, v1, Lrl;->length:I

    .line 124
    sget-object v0, Lrk;->wU:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 125
    if-nez v0, :cond_0

    .line 126
    if-eqz p0, :cond_1

    invoke-static {v2, p2, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 129
    :goto_0
    sget-object v2, Lrk;->wU:Ljava/util/HashMap;

    invoke-virtual {v1}, Lrl;->dT()Lrl;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :cond_0
    return-object v0

    .line 126
    :cond_1
    invoke-static {p2, v2, p1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private dQ()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljunit/framework/Assert;->assertTrue(Z)V

    .line 148
    iget-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lrk;->d(Landroid/graphics/Bitmap;)V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    .line 150
    return-void

    .line 147
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBitmap()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p0}, Lrk;->dM()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    .line 137
    iget-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget v1, p0, Lrk;->wY:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 138
    iget-object v1, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p0, Lrk;->wY:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 139
    iget v2, p0, Lrk;->ny:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 140
    invoke-virtual {p0, v0, v1}, Lrk;->setSize(II)V

    .line 143
    :cond_0
    iget-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method protected final b(Lrb;)Z
    .locals 1

    .prologue
    .line 275
    invoke-virtual {p0, p1}, Lrk;->c(Lrb;)V

    .line 276
    invoke-virtual {p0}, Lrk;->dS()Z

    move-result v0

    return v0
.end method

.method public final c(Lrb;)V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 187
    invoke-virtual {p0}, Lrk;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_7

    .line 188
    invoke-direct {p0}, Lrk;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_6

    :try_start_0
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    iget v1, p0, Lrk;->wY:I

    iget v1, p0, Lrk;->wY:I

    invoke-virtual {p0}, Lrk;->dH()I

    move-result v10

    invoke-virtual {p0}, Lrk;->dI()I

    move-result v11

    if-gt v8, v10, :cond_0

    if-gt v9, v11, :cond_0

    move v0, v7

    :cond_0
    invoke-static {v0}, Ljunit/framework/Assert;->assertTrue(Z)V

    invoke-interface {p1}, Lrb;->dN()Lrh;

    move-result-object v0

    invoke-interface {v0}, Lrh;->dP()I

    move-result v0

    iput v0, p0, Lrk;->cs:I

    invoke-interface {p1, p0}, Lrb;->b(Lqz;)V

    if-ne v8, v10, :cond_3

    if-ne v9, v11, :cond_3

    invoke-interface {p1, p0, v4}, Lrb;->a(Lqz;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lrk;->dQ()V

    invoke-virtual {p0, p1}, Lrk;->a(Lrb;)V

    iput v7, p0, Lrk;->ag:I

    iput-boolean v7, p0, Lrk;->wW:Z

    .line 200
    :cond_2
    :goto_1
    return-void

    .line 188
    :cond_3
    :try_start_1
    invoke-static {v4}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    invoke-static {v4}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v12

    invoke-interface {p1, p0, v5, v6}, Lrb;->a(Lqz;II)V

    iget v2, p0, Lrk;->wY:I

    iget v3, p0, Lrk;->wY:I

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lrb;->a(Lqz;IILandroid/graphics/Bitmap;II)V

    iget v0, p0, Lrk;->wY:I

    if-lez v0, :cond_4

    const/4 v0, 0x1

    invoke-static {v0, v12, v11}, Lrk;->a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lrb;->a(Lqz;IILandroid/graphics/Bitmap;II)V

    const/4 v0, 0x0

    invoke-static {v0, v12, v10}, Lrk;->a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lrb;->a(Lqz;IILandroid/graphics/Bitmap;II)V

    :cond_4
    iget v0, p0, Lrk;->wY:I

    add-int/2addr v0, v8

    if-ge v0, v10, :cond_5

    const/4 v0, 0x1

    invoke-static {v0, v12, v11}, Lrk;->a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v4

    iget v0, p0, Lrk;->wY:I

    add-int v2, v0, v8

    const/4 v3, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lrb;->a(Lqz;IILandroid/graphics/Bitmap;II)V

    :cond_5
    iget v0, p0, Lrk;->wY:I

    add-int/2addr v0, v9

    if-ge v0, v11, :cond_1

    const/4 v0, 0x0

    invoke-static {v0, v12, v10}, Lrk;->a(ZLandroid/graphics/Bitmap$Config;I)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v2, 0x0

    iget v0, p0, Lrk;->wY:I

    add-int v3, v0, v9

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lrb;->a(Lqz;IILandroid/graphics/Bitmap;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lrk;->dQ()V

    throw v0

    :cond_6
    const/4 v0, -0x1

    iput v0, p0, Lrk;->ag:I

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Texture load fail, no bitmap"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_7
    iget-boolean v0, p0, Lrk;->wW:Z

    if-nez v0, :cond_2

    .line 193
    invoke-direct {p0}, Lrk;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 194
    invoke-static {v4}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    .line 195
    invoke-static {v4}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    .line 196
    iget v2, p0, Lrk;->wY:I

    iget v3, p0, Lrk;->wY:I

    move-object v0, p1

    move-object v1, p0

    invoke-interface/range {v0 .. v6}, Lrb;->a(Lqz;IILandroid/graphics/Bitmap;II)V

    .line 197
    invoke-direct {p0}, Lrk;->dQ()V

    .line 198
    iput-boolean v7, p0, Lrk;->wW:Z

    goto/16 :goto_1
.end method

.method protected abstract d(Landroid/graphics/Bitmap;)V
.end method

.method protected final dK()I
    .locals 1

    .prologue
    .line 281
    const/16 v0, 0xde1

    return v0
.end method

.method protected abstract dM()Landroid/graphics/Bitmap;
.end method

.method protected final dR()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 169
    iget-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lrk;->dQ()V

    .line 170
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lrk;->wW:Z

    .line 171
    iput v1, p0, Lrk;->ny:I

    .line 172
    iput v1, p0, Lrk;->wo:I

    .line 173
    return-void
.end method

.method public final dS()Z
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lrk;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lrk;->wW:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeight()I
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lrk;->ny:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lrk;->getBitmap()Landroid/graphics/Bitmap;

    .line 161
    :cond_0
    iget v0, p0, Lrk;->wo:I

    return v0
.end method

.method public final getWidth()I
    .locals 2

    .prologue
    .line 154
    iget v0, p0, Lrk;->ny:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lrk;->getBitmap()Landroid/graphics/Bitmap;

    .line 155
    :cond_0
    iget v0, p0, Lrk;->ny:I

    return v0
.end method

.method public final isOpaque()Z
    .locals 1

    .prologue
    .line 290
    iget-boolean v0, p0, Lrk;->wT:Z

    return v0
.end method

.method public final recycle()V
    .locals 1

    .prologue
    .line 295
    invoke-super {p0}, Lqz;->recycle()V

    .line 296
    iget-object v0, p0, Lrk;->rI:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lrk;->dQ()V

    .line 297
    :cond_0
    return-void
.end method
