.class public final Lfuy;
.super Lfuz;
.source "PG"


# instance fields
.field private final mStringEvaluator:Lgbr;

.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 45
    iput-object p4, p0, Lfuy;->mTimeToLeaveFactory:Lfyk;

    .line 46
    iput-object p5, p0, Lfuy;->mStringEvaluator:Lgbr;

    .line 47
    return-void
.end method

.method private static a(Lfmt;Ljava/lang/String;)Lani;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lfsa;

    const/16 v1, 0x31

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    const v1, 0x7f0200f7

    invoke-virtual {v0, v1}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lfsa;->a(Ljava/lang/String;Lfmt;)Lani;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;Ljhl;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 195
    invoke-virtual {p1}, Ljhl;->akO()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v0

    .line 196
    const v6, 0x18013

    move-object v1, p0

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 10

    .prologue
    .line 59
    iget-object v3, p0, Lfuz;->mEntry:Lizj;

    .line 60
    iget-object v7, v3, Lizj;->dTk:Lixs;

    .line 62
    invoke-virtual {v7}, Lixs;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    move v6, v0

    .line 63
    :goto_0
    if-eqz v6, :cond_d

    iget-object v5, v7, Lixs;->dOu:Ljbp;

    .line 66
    :goto_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 68
    const/4 v0, 0x0

    .line 69
    iget-object v1, v7, Lixs;->dMF:[Liyg;

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 70
    new-instance v1, Lgca;

    iget-object v2, v7, Lixs;->dMF:[Liyg;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    iget-object v4, p0, Lfuz;->mClock:Lemp;

    invoke-direct {v1, v2, v4}, Lgca;-><init>(Liyg;Lemp;)V

    .line 71
    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;->aBO()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgca;->p(Landroid/location/Location;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    new-instance v0, Lfzb;

    iget-object v2, p0, Lfuz;->mClock:Lemp;

    iget-object v4, p0, Lfuy;->mStringEvaluator:Lgbr;

    invoke-direct {v0, v2, v1, v4}, Lfzb;-><init>(Lemp;Lgca;Lgbr;)V

    .line 75
    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v2, p0, Lfuz;->mCardContainer:Lfmt;

    invoke-virtual {v0, p1, v1, v2, v5}, Lfzb;->a(Landroid/content/Context;Lizj;Lfmt;Ljbp;)Lanh;

    move-result-object v0

    .line 77
    invoke-static {v8, v0}, Lfuy;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 80
    :cond_0
    iget-object v1, v7, Lixs;->dMF:[Liyg;

    array-length v1, v1

    if-eqz v1, :cond_1

    iget-object v1, v3, Lizj;->dUv:Ljhk;

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lfuy;->mTimeToLeaveFactory:Lfyk;

    invoke-interface {p2}, Lfmt;->ws()Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    move-result-object v2

    iget-object v4, v7, Lixs;->dMF:[Liyg;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lfyk;->a(Landroid/content/Context;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;Lizj;[Liyg;Ljbp;)Lanh;

    move-result-object v0

    invoke-static {v8, v0}, Lfuy;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 86
    :cond_1
    new-instance v0, Lftn;

    invoke-virtual {v7}, Lixs;->baS()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lftn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lftn;->aDh()Lanh;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v0, Lizj;->dTk:Lixs;

    new-instance v2, Laou;

    invoke-direct {v2}, Laou;-><init>()V

    const v0, 0x7f0a0464

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Laou;->cs(Ljava/lang/String;)Laou;

    new-instance v0, Lfyg;

    invoke-direct {v0, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a045d

    invoke-virtual {v1}, Lixs;->baK()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v0

    const v3, 0x7f0a0463

    invoke-virtual {v1}, Lixs;->baL()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lfyg;->m(ILjava/lang/String;)Lfyg;

    move-result-object v0

    invoke-virtual {v0}, Lfyg;->aDR()Laow;

    move-result-object v3

    new-instance v4, Lfyg;

    invoke-direct {v4, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lixs;->getType()I

    move-result v0

    const/4 v5, 0x1

    if-eq v0, v5, :cond_2

    invoke-virtual {v1}, Lixs;->getType()I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_e

    :cond_2
    const/4 v0, 0x2

    :goto_2
    invoke-virtual {v1}, Lixs;->getType()I

    move-result v5

    const/4 v9, 0x2

    if-eq v5, v9, :cond_3

    const v5, 0x7f0a045e

    iget-object v9, v1, Lixs;->dOt:Ljhl;

    invoke-static {p1, v9}, Lfuy;->c(Landroid/content/Context;Ljhl;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v5, v9, v0}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    :cond_3
    invoke-virtual {v1}, Lixs;->getType()I

    move-result v5

    const/4 v9, 0x1

    if-eq v5, v9, :cond_4

    const v5, 0x7f0a045f

    iget-object v9, v1, Lixs;->dOw:Ljhl;

    invoke-static {p1, v9}, Lfuy;->c(Landroid/content/Context;Ljhl;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v5, v9, v0}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4}, Lfyg;->aDR()Laow;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lixs;->baT()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v3, Lfyg;

    invoke-direct {v3, p1}, Lfyg;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a0462

    invoke-virtual {v1}, Lixs;->baT()Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x2

    invoke-virtual {v3, v4, v5, v9}, Lfyg;->b(ILjava/lang/String;I)Lfyg;

    move-result-object v3

    invoke-virtual {v3}, Lfyg;->aDR()Laow;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Laow;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laow;

    iput-object v0, v2, Laou;->akm:[Laow;

    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Lanh;->cm(I)Lanh;

    iput-object v2, v0, Lanh;->agN:Laou;

    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    iput-object v2, v0, Lanh;->ahu:Lizj;

    invoke-virtual {v1}, Lixs;->baR()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v2, Lfsa;

    const/16 v3, 0x74

    invoke-direct {v2, v3}, Lfsa;-><init>(I)V

    const v3, 0x7f0201e1

    invoke-virtual {v2, v3}, Lfsa;->jB(I)Lfsa;

    move-result-object v2

    invoke-virtual {v1}, Lixs;->baR()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    :cond_6
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, v7, Lixs;->dOu:Ljbp;

    if-eqz v0, :cond_8

    invoke-virtual {v7}, Lixs;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_8

    .line 92
    if-eqz v6, :cond_f

    iget-object v0, v7, Lixs;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_f

    iget-object v0, v7, Lixs;->dMF:[Liyg;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 94
    :goto_3
    new-instance v1, Lfvv;

    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    const/16 v3, 0xba

    iget-object v4, v7, Lixs;->dOu:Ljbp;

    invoke-direct {v1, v2, v3, v0, v4}, Lfvv;-><init>(Lizj;ILiyg;Ljbp;)V

    const v0, 0x7f0a0460

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v7, Lixs;->dOu:Ljbp;

    invoke-virtual {v4}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lfvv;->mName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, v1, Lfvv;->cCH:Z

    invoke-virtual {v1}, Lfvv;->aDh()Lanh;

    move-result-object v0

    .line 100
    invoke-virtual {v7}, Lixs;->baN()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 101
    invoke-virtual {v7}, Lixs;->baM()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lfuy;->a(Lfmt;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->aht:Lani;

    .line 104
    :cond_7
    invoke-static {v8, v0}, Lfuy;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 107
    :cond_8
    iget-object v0, v7, Lixs;->dOx:Ljbp;

    if-eqz v0, :cond_a

    invoke-virtual {v7}, Lixs;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_a

    .line 109
    if-nez v6, :cond_10

    iget-object v0, v7, Lixs;->dMF:[Liyg;

    array-length v0, v0

    if-eqz v0, :cond_10

    iget-object v0, v7, Lixs;->dMF:[Liyg;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 111
    :goto_4
    new-instance v1, Lfvv;

    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    const/16 v3, 0xba

    iget-object v4, v7, Lixs;->dOx:Ljbp;

    invoke-direct {v1, v2, v3, v0, v4}, Lfvv;-><init>(Lizj;ILiyg;Ljbp;)V

    const v0, 0x7f0a0461

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v7, Lixs;->dOx:Ljbp;

    invoke-virtual {v4}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lfvv;->mName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, v1, Lfvv;->cCH:Z

    invoke-virtual {v1}, Lfvv;->aDh()Lanh;

    move-result-object v0

    .line 117
    invoke-virtual {v7}, Lixs;->baP()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 118
    invoke-virtual {v7}, Lixs;->baO()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lfuy;->a(Lfmt;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->aht:Lani;

    .line 121
    :cond_9
    invoke-static {v8, v0}, Lfuy;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 124
    :cond_a
    iget-object v0, v7, Lixs;->dOz:Ljbg;

    if-eqz v0, :cond_b

    iget-object v0, v7, Lixs;->dOz:Ljbg;

    invoke-virtual {v0}, Ljbg;->beR()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 126
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v7, Lixs;->dOz:Ljbg;

    const/16 v2, 0x32

    invoke-static {p1, v0, v1, v2}, Lfsx;->a(Landroid/content/Context;Lizj;Ljbg;I)Lanh;

    move-result-object v0

    .line 128
    invoke-static {v8, v0}, Lfuy;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 131
    :cond_b
    new-instance v1, Lang;

    invoke-direct {v1}, Lang;-><init>()V

    .line 132
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v8, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v1, Lang;->ags:[Lanh;

    .line 133
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lang;->aS(Z)Lang;

    .line 134
    return-object v1

    .line 62
    :cond_c
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_0

    .line 63
    :cond_d
    iget-object v5, v7, Lixs;->dOx:Ljbp;

    goto/16 :goto_1

    .line 88
    :cond_e
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 92
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 109
    :cond_10
    const/4 v0, 0x0

    goto :goto_4
.end method

.method protected final d(Landroid/content/Context;Lfmt;)[Lanh;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0, p1, p2}, Lfuy;->a(Landroid/content/Context;Lfmt;)Lang;

    move-result-object v0

    .line 54
    iget-object v0, v0, Lang;->ags:[Lanh;

    return-object v0
.end method
