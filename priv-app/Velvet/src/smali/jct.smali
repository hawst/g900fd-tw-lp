.class public final Ljct;
.super Ljsl;
.source "PG"


# instance fields
.field private eaV:Ljbp;

.field private eaW:[Ljbp;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24572
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 24573
    iput-object v1, p0, Ljct;->eaV:Ljbp;

    invoke-static {}, Ljbp;->beZ()[Ljbp;

    move-result-object v0

    iput-object v0, p0, Ljct;->eaW:[Ljbp;

    iput-object v1, p0, Ljct;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljct;->eCz:I

    .line 24574
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 24549
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljct;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljct;->eaV:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljct;->eaV:Ljbp;

    :cond_1
    iget-object v0, p0, Ljct;->eaV:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljct;->eaW:[Ljbp;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljbp;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljct;->eaW:[Ljbp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljct;->eaW:[Ljbp;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljbp;

    invoke-direct {v3}, Ljbp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljct;->eaW:[Ljbp;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 24587
    iget-object v0, p0, Ljct;->eaV:Ljbp;

    if-eqz v0, :cond_0

    .line 24588
    const/4 v0, 0x1

    iget-object v1, p0, Ljct;->eaV:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 24590
    :cond_0
    iget-object v0, p0, Ljct;->eaW:[Ljbp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljct;->eaW:[Ljbp;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 24591
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljct;->eaW:[Ljbp;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 24592
    iget-object v1, p0, Ljct;->eaW:[Ljbp;

    aget-object v1, v1, v0

    .line 24593
    if-eqz v1, :cond_1

    .line 24594
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 24591
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24598
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 24599
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 24603
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 24604
    iget-object v1, p0, Ljct;->eaV:Ljbp;

    if-eqz v1, :cond_0

    .line 24605
    const/4 v1, 0x1

    iget-object v2, p0, Ljct;->eaV:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24608
    :cond_0
    iget-object v1, p0, Ljct;->eaW:[Ljbp;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljct;->eaW:[Ljbp;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 24609
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljct;->eaW:[Ljbp;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 24610
    iget-object v2, p0, Ljct;->eaW:[Ljbp;

    aget-object v2, v2, v0

    .line 24611
    if-eqz v2, :cond_1

    .line 24612
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 24609
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 24617
    :cond_3
    return v0
.end method
