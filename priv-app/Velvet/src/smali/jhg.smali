.class public final Ljhg;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile elB:[Ljhg;


# instance fields
.field private aez:I

.field private aiC:Ljava/lang/String;

.field public dVY:Ljcn;

.field private elC:Z

.field private elD:Z

.field private elE:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 50799
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 50800
    iput v1, p0, Ljhg;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljhg;->aiC:Ljava/lang/String;

    iput-object v2, p0, Ljhg;->dVY:Ljcn;

    iput-boolean v1, p0, Ljhg;->elC:Z

    iput-boolean v1, p0, Ljhg;->elD:Z

    iput v1, p0, Ljhg;->elE:I

    iput-object v2, p0, Ljhg;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhg;->eCz:I

    .line 50801
    return-void
.end method

.method public static blD()[Ljhg;
    .locals 2

    .prologue
    .line 50704
    sget-object v0, Ljhg;->elB:[Ljhg;

    if-nez v0, :cond_1

    .line 50705
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 50707
    :try_start_0
    sget-object v0, Ljhg;->elB:[Ljhg;

    if-nez v0, :cond_0

    .line 50708
    const/4 v0, 0x0

    new-array v0, v0, [Ljhg;

    sput-object v0, Ljhg;->elB:[Ljhg;

    .line 50710
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50712
    :cond_1
    sget-object v0, Ljhg;->elB:[Ljhg;

    return-object v0

    .line 50710
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 50693
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhg;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhg;->aiC:Ljava/lang/String;

    iget v0, p0, Ljhg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhg;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljhg;->dVY:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljhg;->dVY:Ljcn;

    :cond_1
    iget-object v0, p0, Ljhg;->dVY:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljhg;->elC:Z

    iget v0, p0, Ljhg;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhg;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljhg;->elD:Z

    iget v0, p0, Ljhg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhg;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljhg;->elE:I

    iget v0, p0, Ljhg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhg;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 50818
    iget v0, p0, Ljhg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 50819
    const/4 v0, 0x1

    iget-object v1, p0, Ljhg;->aiC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 50821
    :cond_0
    iget-object v0, p0, Ljhg;->dVY:Ljcn;

    if-eqz v0, :cond_1

    .line 50822
    const/4 v0, 0x2

    iget-object v1, p0, Ljhg;->dVY:Ljcn;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 50824
    :cond_1
    iget v0, p0, Ljhg;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 50825
    const/4 v0, 0x3

    iget-boolean v1, p0, Ljhg;->elC:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 50827
    :cond_2
    iget v0, p0, Ljhg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 50828
    const/4 v0, 0x4

    iget-boolean v1, p0, Ljhg;->elD:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 50830
    :cond_3
    iget v0, p0, Ljhg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    .line 50831
    const/4 v0, 0x5

    iget v1, p0, Ljhg;->elE:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 50833
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 50834
    return-void
.end method

.method public final blE()Z
    .locals 1

    .prologue
    .line 50764
    iget-boolean v0, p0, Ljhg;->elD:Z

    return v0
.end method

.method public final blF()Z
    .locals 1

    .prologue
    .line 50772
    iget v0, p0, Ljhg;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final blG()I
    .locals 1

    .prologue
    .line 50783
    iget v0, p0, Ljhg;->elE:I

    return v0
.end method

.method public final blH()Z
    .locals 1

    .prologue
    .line 50791
    iget v0, p0, Ljhg;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50720
    iget-object v0, p0, Ljhg;->aiC:Ljava/lang/String;

    return-object v0
.end method

.method public final hasText()Z
    .locals 1

    .prologue
    .line 50731
    iget v0, p0, Ljhg;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ik(Z)Ljhg;
    .locals 1

    .prologue
    .line 50767
    iput-boolean p1, p0, Ljhg;->elD:Z

    .line 50768
    iget v0, p0, Ljhg;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljhg;->aez:I

    .line 50769
    return-object p0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 50838
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 50839
    iget v1, p0, Ljhg;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 50840
    const/4 v1, 0x1

    iget-object v2, p0, Ljhg;->aiC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50843
    :cond_0
    iget-object v1, p0, Ljhg;->dVY:Ljcn;

    if-eqz v1, :cond_1

    .line 50844
    const/4 v1, 0x2

    iget-object v2, p0, Ljhg;->dVY:Ljcn;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50847
    :cond_1
    iget v1, p0, Ljhg;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 50848
    const/4 v1, 0x3

    iget-boolean v2, p0, Ljhg;->elC:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 50851
    :cond_2
    iget v1, p0, Ljhg;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 50852
    const/4 v1, 0x4

    iget-boolean v2, p0, Ljhg;->elD:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 50855
    :cond_3
    iget v1, p0, Ljhg;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_4

    .line 50856
    const/4 v1, 0x5

    iget v2, p0, Ljhg;->elE:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 50859
    :cond_4
    return v0
.end method

.method public final pR(I)Ljhg;
    .locals 1

    .prologue
    .line 50786
    const/4 v0, 0x1

    iput v0, p0, Ljhg;->elE:I

    .line 50787
    iget v0, p0, Ljhg;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljhg;->aez:I

    .line 50788
    return-object p0
.end method

.method public final uX(Ljava/lang/String;)Ljhg;
    .locals 1

    .prologue
    .line 50723
    if-nez p1, :cond_0

    .line 50724
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 50726
    :cond_0
    iput-object p1, p0, Ljhg;->aiC:Ljava/lang/String;

    .line 50727
    iget v0, p0, Ljhg;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhg;->aez:I

    .line 50728
    return-object p0
.end method
