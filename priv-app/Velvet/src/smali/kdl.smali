.class final Lkdl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkde;


# instance fields
.field private final eTB:Ljava/io/DataOutputStream;

.field private final eTC:Z

.field private final eTD:Ljava/io/ByteArrayOutputStream;

.field private final eTE:Lkdi;


# direct methods
.method constructor <init>(Ljava/io/OutputStream;Z)V
    .locals 2

    .prologue
    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    .line 269
    iput-boolean p2, p0, Lkdl;->eTC:Z

    .line 270
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lkdl;->eTD:Ljava/io/ByteArrayOutputStream;

    .line 271
    new-instance v0, Lkdi;

    iget-object v1, p0, Lkdl;->eTD:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Lkdi;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lkdl;->eTE:Lkdi;

    .line 272
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(ILkdb;)V
    .locals 2

    .prologue
    .line 317
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "TODO"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lkdt;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 335
    monitor-enter p0

    :try_start_0
    iget v1, p1, Lkdt;->eTO:I

    invoke-static {v1}, Ljava/lang/Integer;->bitCount(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x8

    .line 337
    iget-object v2, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    const v3, 0xffff

    and-int/2addr v1, v3

    shl-int/lit8 v1, v1, 0x10

    or-int/lit16 v1, v1, 0x400

    or-int/lit8 v1, v1, 0x0

    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 340
    iget-object v1, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 341
    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 342
    invoke-virtual {p1, v0}, Lkdt;->isSet(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 343
    iget-object v1, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    const v2, 0xffffff

    and-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 344
    iget-object v1, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    iget-object v2, p1, Lkdt;->eTR:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    :cond_1
    monitor-exit p0

    return-void

    .line 335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ZI[BII)V
    .locals 3

    .prologue
    .line 326
    monitor-enter p0

    const/4 v0, 0x0

    .line 328
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 329
    :cond_0
    :try_start_0
    iget-object v1, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    const v2, 0xffff

    and-int/2addr v2, p5

    shl-int/lit8 v2, v2, 0x10

    or-int/lit8 v2, v2, 0x0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 330
    iget-object v0, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    const v1, 0x7fffffff

    and-int/2addr v1, p2

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 331
    iget-object v0, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p3, p4, p5}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    monitor-exit p0

    return-void

    .line 326
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(ZZIIIILjava/util/List;)V
    .locals 4

    .prologue
    .line 286
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 287
    :cond_0
    :try_start_1
    iget-object v0, p0, Lkdl;->eTD:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    iget-object v0, p0, Lkdl;->eTE:Lkdi;

    invoke-virtual {v0, p7}, Lkdi;->aI(Ljava/util/List;)V

    iget-object v0, p0, Lkdl;->eTD:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v1

    const/4 v0, 0x4

    if-eqz p1, :cond_1

    const/4 v0, 0x5

    :cond_1
    or-int/lit8 v0, v0, 0x8

    iget-object v2, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    const v3, 0xffff

    and-int/2addr v1, v3

    shl-int/lit8 v1, v1, 0x10

    or-int/lit16 v1, v1, 0x100

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    const v1, 0x7fffffff

    and-int/2addr v1, p3

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p0, Lkdl;->eTD:Ljava/io/ByteArrayOutputStream;

    iget-object v1, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(ILkdb;)V
    .locals 0

    .prologue
    .line 360
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized bB(II)V
    .locals 0

    .prologue
    .line 365
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized bzf()V
    .locals 2

    .prologue
    .line 279
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lkdl;->eTC:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 281
    :goto_0
    monitor-exit p0

    return-void

    .line 280
    :cond_0
    :try_start_1
    iget-object v0, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    invoke-static {}, Lkdj;->bzm()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 279
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 369
    return-void
.end method

.method public final declared-synchronized flush()V
    .locals 1

    .prologue
    .line 275
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkdl;->eTB:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    monitor-exit p0

    return-void

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i(ZI)V
    .locals 0

    .prologue
    .line 355
    monitor-enter p0

    monitor-exit p0

    return-void
.end method
