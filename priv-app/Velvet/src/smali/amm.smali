.class public final Lamm;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile aeY:[Lamm;


# instance fields
.field private aeU:Ljava/lang/String;

.field private aeZ:J

.field private aez:I

.field private afa:Ljava/lang/String;

.field private afb:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1532
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1533
    const/4 v0, 0x0

    iput v0, p0, Lamm;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamm;->aeZ:J

    const-string v0, ""

    iput-object v0, p0, Lamm;->aeU:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lamm;->afa:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lamm;->afb:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lamm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamm;->eCz:I

    .line 1534
    return-void
.end method

.method public static na()[Lamm;
    .locals 2

    .prologue
    .line 1434
    sget-object v0, Lamm;->aeY:[Lamm;

    if-nez v0, :cond_1

    .line 1435
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1437
    :try_start_0
    sget-object v0, Lamm;->aeY:[Lamm;

    if-nez v0, :cond_0

    .line 1438
    const/4 v0, 0x0

    new-array v0, v0, [Lamm;

    sput-object v0, Lamm;->aeY:[Lamm;

    .line 1440
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1442
    :cond_1
    sget-object v0, Lamm;->aeY:[Lamm;

    return-object v0

    .line 1440
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final S(Ljava/lang/String;)Lamm;
    .locals 1

    .prologue
    .line 1472
    if-nez p1, :cond_0

    .line 1473
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1475
    :cond_0
    iput-object p1, p0, Lamm;->aeU:Ljava/lang/String;

    .line 1476
    iget v0, p0, Lamm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamm;->aez:I

    .line 1477
    return-object p0
.end method

.method public final T(Ljava/lang/String;)Lamm;
    .locals 1

    .prologue
    .line 1494
    if-nez p1, :cond_0

    .line 1495
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1497
    :cond_0
    iput-object p1, p0, Lamm;->afa:Ljava/lang/String;

    .line 1498
    iget v0, p0, Lamm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamm;->aez:I

    .line 1499
    return-object p0
.end method

.method public final U(Ljava/lang/String;)Lamm;
    .locals 1

    .prologue
    .line 1516
    if-nez p1, :cond_0

    .line 1517
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1519
    :cond_0
    iput-object p1, p0, Lamm;->afb:Ljava/lang/String;

    .line 1520
    iget v0, p0, Lamm;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamm;->aez:I

    .line 1521
    return-object p0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 1428
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lamm;->aeZ:J

    iget v0, p0, Lamm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamm;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamm;->aeU:Ljava/lang/String;

    iget v0, p0, Lamm;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lamm;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamm;->afa:Ljava/lang/String;

    iget v0, p0, Lamm;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lamm;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamm;->afb:Ljava/lang/String;

    iget v0, p0, Lamm;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lamm;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1550
    iget v0, p0, Lamm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1551
    const/4 v0, 0x1

    iget-wide v2, p0, Lamm;->aeZ:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1553
    :cond_0
    iget v0, p0, Lamm;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1554
    const/4 v0, 0x2

    iget-object v1, p0, Lamm;->aeU:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1556
    :cond_1
    iget v0, p0, Lamm;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1557
    const/4 v0, 0x3

    iget-object v1, p0, Lamm;->afa:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1559
    :cond_2
    iget v0, p0, Lamm;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 1560
    const/4 v0, 0x4

    iget-object v1, p0, Lamm;->afb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1562
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1563
    return-void
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1513
    iget-object v0, p0, Lamm;->afb:Ljava/lang/String;

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1469
    iget-object v0, p0, Lamm;->aeU:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 1567
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1568
    iget v1, p0, Lamm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1569
    const/4 v1, 0x1

    iget-wide v2, p0, Lamm;->aeZ:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1572
    :cond_0
    iget v1, p0, Lamm;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1573
    const/4 v1, 0x2

    iget-object v2, p0, Lamm;->aeU:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1576
    :cond_1
    iget v1, p0, Lamm;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1577
    const/4 v1, 0x3

    iget-object v2, p0, Lamm;->afa:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1580
    :cond_2
    iget v1, p0, Lamm;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 1581
    const/4 v1, 0x4

    iget-object v2, p0, Lamm;->afb:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1584
    :cond_3
    return v0
.end method

.method public final nb()J
    .locals 2

    .prologue
    .line 1450
    iget-wide v0, p0, Lamm;->aeZ:J

    return-wide v0
.end method

.method public final nc()Z
    .locals 1

    .prologue
    .line 1458
    iget v0, p0, Lamm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1491
    iget-object v0, p0, Lamm;->afa:Ljava/lang/String;

    return-object v0
.end method

.method public final q(J)Lamm;
    .locals 1

    .prologue
    .line 1453
    iput-wide p1, p0, Lamm;->aeZ:J

    .line 1454
    iget v0, p0, Lamm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamm;->aez:I

    .line 1455
    return-object p0
.end method
