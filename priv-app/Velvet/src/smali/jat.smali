.class public final Ljat;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dXn:Ljava/lang/String;

.field private dXo:Ljava/lang/String;

.field private dXp:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39351
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 39352
    const/4 v0, 0x0

    iput v0, p0, Ljat;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljat;->dXn:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljat;->dXo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljat;->dXp:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljat;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljat;->eCz:I

    .line 39353
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 39266
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljat;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljat;->dXn:Ljava/lang/String;

    iget v0, p0, Ljat;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljat;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljat;->dXo:Ljava/lang/String;

    iget v0, p0, Ljat;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljat;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljat;->dXp:Ljava/lang/String;

    iget v0, p0, Ljat;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljat;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 39368
    iget v0, p0, Ljat;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 39369
    const/4 v0, 0x1

    iget-object v1, p0, Ljat;->dXn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 39371
    :cond_0
    iget v0, p0, Ljat;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 39372
    const/4 v0, 0x2

    iget-object v1, p0, Ljat;->dXo:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 39374
    :cond_1
    iget v0, p0, Ljat;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 39375
    const/4 v0, 0x3

    iget-object v1, p0, Ljat;->dXp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 39377
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 39378
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 39382
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 39383
    iget v1, p0, Ljat;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 39384
    const/4 v1, 0x1

    iget-object v2, p0, Ljat;->dXn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39387
    :cond_0
    iget v1, p0, Ljat;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 39388
    const/4 v1, 0x2

    iget-object v2, p0, Ljat;->dXo:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39391
    :cond_1
    iget v1, p0, Ljat;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 39392
    const/4 v1, 0x3

    iget-object v2, p0, Ljat;->dXp:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39395
    :cond_2
    return v0
.end method
