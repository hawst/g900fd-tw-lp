.class public final Lfxm;
.super Lfuz;
.source "PG"


# instance fields
.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lizj;Lftz;Lemp;Lgbd;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lfuz;-><init>(Lizj;Lftz;Lemp;)V

    .line 32
    iput-object p4, p0, Lfxm;->mPhotoWithAttributionDecorator:Lgbd;

    .line 33
    return-void
.end method

.method public static b(Landroid/content/Context;Lizj;Ljif;Z)Lanh;
    .locals 6
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 89
    .line 90
    invoke-virtual {p2}, Ljif;->pb()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljif;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0a0428

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 96
    new-instance v3, Laoj;

    invoke-direct {v3}, Laoj;-><init>()V

    .line 97
    const v0, 0x7f0a0152

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Laoj;->bS(Ljava/lang/String;)Laoj;

    .line 99
    new-instance v0, Lanh;

    invoke-direct {v0}, Lanh;-><init>()V

    .line 100
    invoke-virtual {v0, v4}, Lanh;->cm(I)Lanh;

    .line 101
    iput-object p1, v0, Lanh;->ahu:Lizj;

    .line 102
    iput-object v3, v0, Lanh;->agz:Laoj;

    .line 103
    new-instance v3, Lfsa;

    invoke-direct {v3, v4}, Lfsa;-><init>(I)V

    const v4, 0x7f0200ed

    invoke-virtual {v3, v4}, Lfsa;->jB(I)Lfsa;

    move-result-object v3

    invoke-virtual {v3, v2, v1, v1}, Lfsa;->a(Ljava/lang/String;Ljbp;Ljava/lang/String;)Lani;

    move-result-object v1

    iput-object v1, v0, Lanh;->ahs:Lani;

    .line 106
    if-eqz p3, :cond_0

    .line 107
    invoke-virtual {v0, v5}, Lanh;->aW(Z)Lanh;

    .line 108
    invoke-virtual {v0, v5}, Lanh;->aX(Z)Lanh;

    .line 111
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lfmt;)Lang;
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 37
    iget-object v2, p0, Lfuz;->mEntry:Lizj;

    .line 38
    iget-object v3, v2, Lizj;->dTc:Ljif;

    .line 40
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 41
    iget-object v0, p0, Lfxm;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxm;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhO()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 43
    :goto_0
    invoke-virtual {p0, p1, v2, v3, v0}, Lfxm;->a(Landroid/content/Context;Lizj;Ljif;Z)Lanh;

    move-result-object v0

    .line 44
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    invoke-virtual {v0}, Lanh;->oV()Z

    move-result v0

    invoke-static {p1, v2, v3, v0}, Lfxm;->b(Landroid/content/Context;Lizj;Ljif;Z)Lanh;

    move-result-object v0

    invoke-static {v4, v0}, Lfxm;->a(Ljava/util/List;Ljava/lang/Object;)V

    .line 48
    new-instance v2, Lang;

    invoke-direct {v2}, Lang;-><init>()V

    .line 49
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lanh;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanh;

    iput-object v0, v2, Lang;->ags:[Lanh;

    .line 50
    invoke-virtual {v2, v1}, Lang;->aS(Z)Lang;

    .line 52
    return-object v2

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lizj;Ljif;Z)Lanh;
    .locals 3

    .prologue
    .line 70
    const/4 v0, 0x0

    .line 71
    if-eqz p4, :cond_1

    .line 72
    iget-object v0, p0, Lfxm;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dUu:Ljdv;

    invoke-virtual {v0}, Ljdv;->bhN()Ljava/lang/String;

    move-result-object v0

    .line 77
    :cond_0
    :goto_0
    new-instance v1, Lfrs;

    invoke-virtual {p3}, Ljif;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2, p2}, Lfrs;-><init>(Landroid/content/Context;Ljava/lang/String;Lizj;)V

    iput-boolean p4, v1, Lfrs;->cBf:Z

    iput-object v0, v1, Lfrs;->cBi:Ljava/lang/String;

    invoke-virtual {p3}, Ljif;->bmZ()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lfrs;->cBj:Ljava/lang/String;

    iget-object v0, p3, Ljif;->aiX:Ljcn;

    iget-object v2, p0, Lfxm;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-virtual {v1, v0, v2}, Lfrs;->a(Ljcn;Lgbd;)Lfrs;

    move-result-object v0

    invoke-virtual {v0}, Lfrs;->aDh()Lanh;

    move-result-object v0

    return-object v0

    .line 73
    :cond_1
    invoke-virtual {p3}, Ljif;->bna()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {p3}, Ljif;->tf()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aW(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lfuz;->mEntry:Lizj;

    iget-object v0, v0, Lizj;->dTc:Ljif;

    iget-object v0, v0, Ljif;->aiX:Ljcn;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lfxm;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v1, p0, Lfuz;->mEntry:Lizj;

    iget-object v1, v1, Lizj;->dTc:Ljif;

    iget-object v1, v1, Ljif;->aiX:Ljcn;

    const v2, 0x7f0d0220

    const v3, 0x7f0d0214

    invoke-virtual {v0, p1, v1, v2, v3}, Lgbd;->c(Landroid/content/Context;Ljcn;II)Landroid/net/Uri;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
