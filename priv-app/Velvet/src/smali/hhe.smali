.class public final Lhhe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ledr;


# instance fields
.field private final aYZ:Lciu;

.field private final cUF:Lhhb;

.field final dep:Lgxi;

.field private dgK:Z

.field final dgW:Ldxy;

.field private final dgX:Lhhf;

.field dgY:Lesk;

.field private final mActionCardEventLogger:Legs;

.field private mActionData:Lcom/google/android/velvet/ActionData;

.field private final mClock:Lemp;

.field private final mContext:Landroid/content/Context;

.field private final mDiscourseContextSupplier:Ligi;

.field private final mEventBus:Ldda;

.field private final mGsaConfigFlags:Lchk;

.field private final mIntentStarter:Leoj;

.field private mQuery:Lcom/google/android/shared/search/Query;

.field private final mRelationshipManager:Lcjg;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lemp;Lema;Ldda;Ligi;Lgxi;Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;Lcjg;Legs;Leoj;Lchk;Lciu;Lhhb;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Lhhf;

    invoke-direct {v0, p0}, Lhhf;-><init>(Lhhe;)V

    iput-object v0, p0, Lhhe;->dgX:Lhhf;

    .line 104
    iput-object p1, p0, Lhhe;->mContext:Landroid/content/Context;

    .line 105
    iput-object p2, p0, Lhhe;->mClock:Lemp;

    .line 106
    iput-object p4, p0, Lhhe;->mEventBus:Ldda;

    .line 107
    iput-object p5, p0, Lhhe;->mDiscourseContextSupplier:Ligi;

    .line 108
    iput-object p6, p0, Lhhe;->dep:Lgxi;

    .line 109
    iput-object p7, p0, Lhhe;->mQuery:Lcom/google/android/shared/search/Query;

    .line 110
    iput-object p8, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 111
    iput-object p9, p0, Lhhe;->mRelationshipManager:Lcjg;

    .line 112
    iput-object p13, p0, Lhhe;->aYZ:Lciu;

    .line 113
    iput-object p11, p0, Lhhe;->mIntentStarter:Leoj;

    .line 114
    iput-object p12, p0, Lhhe;->mGsaConfigFlags:Lchk;

    .line 115
    invoke-static {p3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    new-instance v0, Ldxy;

    invoke-direct {v0, p3}, Ldxy;-><init>(Lema;)V

    iput-object v0, p0, Lhhe;->dgW:Ldxy;

    .line 117
    iput-object p10, p0, Lhhe;->mActionCardEventLogger:Legs;

    .line 118
    iput-object p14, p0, Lhhe;->cUF:Lhhb;

    .line 119
    return-void
.end method

.method private a(Ljava/lang/Integer;Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 3

    .prologue
    .line 369
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, p2}, Lhhe;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v1

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v2

    invoke-interface {p2, v0, v1, v2}, Lcom/google/android/search/shared/actions/VoiceAction;->a(ILcom/google/android/search/shared/actions/utils/CardDecision;I)V

    .line 370
    return-void
.end method

.method private kS(I)V
    .locals 3

    .prologue
    .line 505
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aat()Ldcf;

    move-result-object v0

    iget-object v1, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Ldcf;->b(Lcom/google/android/velvet/ActionData;I)V

    .line 506
    return-void
.end method

.method private kT(I)V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aat()Ldcf;

    move-result-object v0

    iget-object v1, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1, p1}, Ldcf;->a(Lcom/google/android/velvet/ActionData;I)V

    .line 510
    return-void
.end method


# virtual methods
.method public final A(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 280
    const/16 v0, 0xe

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lhhe;->a(Ljava/lang/Integer;Lcom/google/android/search/shared/actions/VoiceAction;)V

    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lhhe;->kS(I)V

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lhhe;->kT(I)V

    .line 282
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    .line 283
    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lhhe;->anJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p0, Lhhe;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    invoke-virtual {v0}, Lcky;->Nd()V

    goto :goto_0
.end method

.method public final B(Lcom/google/android/search/shared/actions/VoiceAction;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    iget-object v2, p0, Lhhe;->dgW:Ldxy;

    invoke-virtual {p0, p1}, Lhhe;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v3

    if-eqz v3, :cond_0

    iget v2, v2, Ldxy;->bQf:I

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aaj()Ldbd;

    move-result-object v2

    invoke-virtual {v2}, Ldbd;->Vq()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final C(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lhhe;->a(Ljava/lang/Integer;Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 366
    return-void
.end method

.method public final D(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 357
    const/16 v0, 0x48

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lhhe;->a(Ljava/lang/Integer;Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 359
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lhhe;->kS(I)V

    .line 360
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lhhe;->kT(I)V

    .line 361
    return-void
.end method

.method public final GR()Z
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lhhe;->mGsaConfigFlags:Lchk;

    invoke-virtual {v0}, Lchk;->GR()Z

    move-result v0

    return v0
.end method

.method public final VC()V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VC()V

    .line 427
    return-void
.end method

.method public final VG()Z
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VG()Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;Lesk;)J
    .locals 8

    .prologue
    .line 133
    iput-object p2, p0, Lhhe;->dgY:Lesk;

    .line 134
    iget-object v0, p0, Lhhe;->dgW:Ldxy;

    iget-object v1, p0, Lhhe;->dgX:Lhhf;

    invoke-virtual {p0, p1}, Lhhe;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v2

    iput-object v1, v0, Ldxy;->hC:Ljava/lang/Runnable;

    const/4 v1, 0x1

    iput v1, v0, Ldxy;->bQf:I

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    iget-object v1, v0, Ldxy;->mAsyncServices:Lema;

    invoke-virtual {v1}, Lema;->aus()Leqo;

    move-result-object v1

    iget-object v0, v0, Ldxy;->hC:Ljava/lang/Runnable;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v4

    invoke-interface {v1, v0, v4, v5}, Leqo;->a(Ljava/lang/Runnable;J)V

    :goto_0
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v0

    .line 136
    iget-object v2, p0, Lhhe;->dep:Lgxi;

    iget-object v3, p0, Lhhe;->dgX:Lhhf;

    invoke-virtual {v2, v3}, Lgxi;->a(Lddc;)V

    .line 137
    return-wide v0

    .line 134
    :cond_0
    iget-object v0, v0, Ldxy;->hC:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;IZ)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ldbd;->a(Lcom/google/android/search/shared/actions/VoiceAction;IZ)V

    .line 311
    return-void
.end method

.method public final a(Lcom/google/android/search/shared/actions/VoiceAction;Z)V
    .locals 1

    .prologue
    .line 347
    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lhhe;->a(Ljava/lang/Integer;Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 348
    if-eqz p2, :cond_0

    .line 349
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhhe;->kT(I)V

    .line 353
    :goto_0
    return-void

    .line 351
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhhe;->kT(I)V

    goto :goto_0
.end method

.method public final a(Lesj;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lhhe;->dep:Lgxi;

    invoke-virtual {v0, p1}, Lgxi;->a(Lesj;)V

    .line 165
    return-void
.end method

.method public final aG(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 174
    if-eqz p1, :cond_0

    .line 175
    iget-object v0, p0, Lhhe;->mDiscourseContextSupplier:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcky;

    new-instance v1, Lcld;

    iget-object v2, p0, Lhhe;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcld;-><init>(J)V

    invoke-virtual {v0, p1, v1}, Lcky;->a(Ljava/lang/Object;Lcld;)V

    .line 178
    :cond_0
    return-void
.end method

.method public final abj()Lcom/google/android/shared/search/Query;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 316
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->WW()Lcom/google/android/shared/search/Query;

    move-result-object v0

    return-object v0
.end method

.method public final anJ()Z
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VN()Z

    move-result v0

    return v0
.end method

.method public final anK()V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZZ()V

    .line 271
    return-void
.end method

.method public final anL()Z
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lhhe;->cUF:Lhhb;

    iget-object v1, p0, Lhhe;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lhhb;->ba(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    return v0
.end method

.method public final anM()Lesm;
    .locals 1

    .prologue
    .line 563
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->anM()Lesm;

    move-result-object v0

    return-object v0
.end method

.method public final anN()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->Vr()V

    .line 155
    return-void
.end method

.method public final anO()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 142
    iget-object v1, p0, Lhhe;->dgW:Ldxy;

    iget v2, v1, Ldxy;->bQf:I

    if-ne v2, v0, :cond_0

    :goto_0
    iget-object v2, v1, Ldxy;->mAsyncServices:Lema;

    invoke-virtual {v2}, Lema;->aus()Leqo;

    move-result-object v2

    iget-object v3, v1, Ldxy;->hC:Ljava/lang/Runnable;

    invoke-interface {v2, v3}, Leqo;->i(Ljava/lang/Runnable;)V

    const/4 v2, 0x2

    iput v2, v1, Ldxy;->bQf:I

    .line 144
    :try_start_0
    iget-object v1, p0, Lhhe;->dep:Lgxi;

    iget-object v2, p0, Lhhe;->dgX:Lhhf;

    invoke-virtual {v1, v2}, Lgxi;->b(Lddc;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_1
    return v0

    .line 142
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public final anP()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lhhe;->dgW:Ldxy;

    invoke-virtual {v0}, Ldxy;->isStarted()Z

    move-result v0

    return v0
.end method

.method public final anQ()V
    .locals 2

    .prologue
    .line 339
    const/16 v0, 0x32

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 342
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lhhe;->kT(I)V

    .line 343
    return-void
.end method

.method public final anR()Z
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->Vz()Z

    move-result v0

    return v0
.end method

.method public final anS()V
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VR()Z

    .line 589
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-virtual {v0}, Ldcy;->ZZ()V

    .line 590
    return-void
.end method

.method public final cZ(I)V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lhhe;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 170
    return-void
.end method

.method public final e(Lcom/google/android/search/shared/actions/errors/SearchError;)V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiQ()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->I(Lcom/google/android/shared/search/Query;)V

    .line 294
    return-void
.end method

.method public final eK(Z)V
    .locals 4

    .prologue
    .line 568
    if-eqz p1, :cond_0

    .line 569
    iget-object v0, p0, Lhhe;->mActionCardEventLogger:Legs;

    iget-object v1, p0, Lhhe;->dep:Lgxi;

    invoke-virtual {v1}, Lgxi;->aLt()Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v2}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v2

    iget-object v3, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v3}, Ldda;->aaj()Ldbd;

    move-result-object v3

    invoke-static {v2, v3}, Lguv;->a(Lcom/google/android/shared/search/Query;Ldbd;)Legu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Legs;->a(Landroid/view/View;Legu;)V

    .line 576
    :goto_0
    return-void

    .line 574
    :cond_0
    iget-object v0, p0, Lhhe;->mActionCardEventLogger:Legs;

    iget-object v1, p0, Lhhe;->dep:Lgxi;

    invoke-virtual {v1}, Lgxi;->aLt()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Legs;->aF(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final g(Lcom/google/android/shared/search/Query;Lcom/google/android/velvet/ActionData;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lhhe;->mQuery:Lcom/google/android/shared/search/Query;

    .line 182
    iput-object p2, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 183
    return-void
.end method

.method public final hk(I)V
    .locals 2

    .prologue
    .line 604
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcy;->aB(Ljava/lang/Object;)V

    .line 605
    return-void
.end method

.method public final hl(I)V
    .locals 2

    .prologue
    .line 609
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aan()Ldcy;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldcy;->aC(Ljava/lang/Object;)V

    .line 610
    return-void
.end method

.method public final m(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 3

    .prologue
    .line 379
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    const-string v1, "First step is not completed."

    invoke-static {v0, v1}, Lifv;->c(ZLjava/lang/Object;)V

    .line 381
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 388
    sget-object v1, Lcgg;->aVF:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 389
    iget-object v1, p0, Lhhe;->aYZ:Lciu;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lciu;->a(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    .line 396
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amw()V

    .line 399
    :cond_0
    return-void

    .line 392
    :cond_1
    iget-object v1, p0, Lhhe;->mRelationshipManager:Lcjg;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcjg;->a(Lcom/google/android/search/shared/contact/Relationship;Lcom/google/android/search/shared/contact/Person;)V

    goto :goto_0
.end method

.method public final n(Lcom/google/android/search/shared/contact/PersonDisambiguation;)V
    .locals 3

    .prologue
    .line 403
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alB()Z

    move-result v0

    const-string v1, "First step is not completed."

    invoke-static {v0, v1}, Lifv;->c(ZLjava/lang/Object;)V

    .line 405
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alz()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/contact/Person;

    .line 412
    sget-object v1, Lcgg;->aVF:Lcgg;

    invoke-virtual {v1}, Lcgg;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 413
    iget-object v1, p0, Lhhe;->aYZ:Lciu;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lciu;->b(Lcom/google/android/search/shared/contact/Person;Lcom/google/android/search/shared/contact/Relationship;)V

    .line 420
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amw()V

    .line 422
    :cond_0
    return-void

    .line 416
    :cond_1
    iget-object v1, p0, Lhhe;->mRelationshipManager:Lcjg;

    invoke-virtual {p1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->ami()Lcom/google/android/search/shared/contact/RelationshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/search/shared/contact/RelationshipStatus;->amt()Lcom/google/android/search/shared/contact/Relationship;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcjg;->b(Lcom/google/android/search/shared/contact/Relationship;Lcom/google/android/search/shared/contact/Person;)V

    goto :goto_0
.end method

.method public final o(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbd;->o(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 276
    return-void
.end method

.method public final p(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbd;->p(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 334
    return-void
.end method

.method public final q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    return-object v0
.end method

.method public final uF()I
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    sget-object v1, Lcom/google/android/velvet/ActionData;->cRQ:Lcom/google/android/velvet/ActionData;

    if-ne v0, v1, :cond_0

    .line 262
    const/4 v0, 0x0

    .line 264
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->oY()I

    move-result v0

    goto :goto_0
.end method

.method public final uv()V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VN()Z

    .line 305
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    iget-object v1, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Ldbd;->d(Lcom/google/android/velvet/ActionData;)V

    .line 306
    return-void
.end method

.method public final w(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldbd;->j(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 190
    return-void
.end method

.method public final wr()Leoj;
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lhhe;->mIntentStarter:Leoj;

    return-object v0
.end method

.method public final x(Lcom/google/android/search/shared/actions/VoiceAction;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 194
    invoke-virtual {p0, p1}, Lhhe;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v5

    .line 197
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->aga()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {v5}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alk()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v5}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhhe;->dgK:Z

    if-nez v0, :cond_0

    const/16 v0, 0x9d

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 200
    :cond_0
    invoke-virtual {p0, p1}, Lhhe;->z(Lcom/google/android/search/shared/actions/VoiceAction;)V

    .line 202
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->canExecute()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v5}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->alg()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aat()Ldcf;

    move-result-object v0

    iget-object v1, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    const/16 v3, 0x100

    invoke-virtual {v0, v1, v3}, Ldcf;->a(Lcom/google/android/velvet/ActionData;I)V

    .line 208
    :cond_2
    iget-boolean v0, p0, Lhhe;->dgK:Z

    if-nez v0, :cond_5

    .line 209
    iput-boolean v4, p0, Lhhe;->dgK:Z

    .line 210
    iget-object v0, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIL()I

    move-result v0

    .line 212
    const/16 v1, 0x5d

    if-ne v0, v1, :cond_7

    .line 213
    iget-object v1, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aat()Ldcf;

    move-result-object v1

    iget-object v2, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    const/16 v3, 0x1000

    invoke-virtual {v1, v2, v3}, Ldcf;->c(Lcom/google/android/velvet/ActionData;I)V

    .line 214
    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v1}, Lcom/google/android/velvet/ActionData;->aGY()Litp;

    move-result-object v1

    iput-object v1, v0, Litu;->dIM:Litp;

    .line 217
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 222
    :goto_1
    instance-of v0, p1, Lcom/google/android/search/shared/actions/errors/VoiceSearchError;

    if-eqz v0, :cond_3

    .line 223
    const/16 v0, 0x9f

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 227
    :cond_3
    iget-object v0, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0}, Lcom/google/android/velvet/ActionData;->aIS()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 228
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aat()Ldcf;

    move-result-object v0

    iget-object v1, p0, Lhhe;->mActionData:Lcom/google/android/velvet/ActionData;

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Ldcf;->a(Lcom/google/android/velvet/ActionData;I)V

    :cond_4
    move v2, v4

    .line 233
    :cond_5
    return v2

    .line 198
    :pswitch_0
    const/16 v0, 0x8d

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v6

    instance-of v0, p1, Lcom/google/android/search/shared/actions/CommunicationAction;

    if-eqz v0, :cond_6

    new-instance v7, Litl;

    invoke-direct {v7}, Litl;-><init>()V

    move-object v0, p1

    check-cast v0, Lcom/google/android/search/shared/actions/CommunicationAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/CommunicationAction;->ahn()Lcom/google/android/search/shared/contact/PersonDisambiguation;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->alu()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/search/shared/contact/PersonDisambiguation;->amk()I

    move-result v1

    :goto_2
    invoke-virtual {v7, v3}, Litl;->mv(I)Litl;

    invoke-virtual {v7, v1}, Litl;->mu(I)Litl;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/CommunicationAction;->agc()I

    move-result v0

    invoke-virtual {v6, v0}, Litu;->mC(I)Litu;

    iput-object v7, v6, Litu;->dIA:Litl;

    :cond_6
    invoke-static {v6}, Lege;->a(Litu;)V

    move v0, v4

    goto/16 :goto_0

    :pswitch_1
    const/16 v0, 0x8e

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    move v0, v4

    goto/16 :goto_0

    :pswitch_2
    const/16 v0, 0x8f

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    move v0, v4

    goto/16 :goto_0

    :pswitch_3
    const/16 v0, 0xd1

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    move v0, v4

    goto/16 :goto_0

    :pswitch_4
    const/16 v0, 0xd2

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    move v0, v4

    goto/16 :goto_0

    :pswitch_5
    const/16 v0, 0x9b

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    move v0, v4

    goto/16 :goto_0

    .line 219
    :cond_7
    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-virtual {p0}, Lhhe;->uF()I

    move-result v1

    invoke-virtual {v0, v1}, Litu;->mC(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    goto/16 :goto_1

    :cond_8
    move v1, v2

    move v3, v2

    goto/16 :goto_2

    .line 198
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final y(Lcom/google/android/search/shared/actions/VoiceAction;)Z
    .locals 4

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lhhe;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    .line 322
    invoke-interface {p1}, Lcom/google/android/search/shared/actions/VoiceAction;->canExecute()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z(Lcom/google/android/search/shared/actions/VoiceAction;)V
    .locals 4

    .prologue
    .line 238
    iget-object v0, p0, Lhhe;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aak()Ldcw;

    move-result-object v0

    .line 241
    invoke-virtual {p0, p1}, Lhhe;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v1

    .line 242
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alf()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 243
    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ale()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->Zv()Z

    move-result v1

    iget-object v3, p0, Lhhe;->mGsaConfigFlags:Lchk;

    invoke-virtual {v3}, Lchk;->GV()Z

    move-result v3

    invoke-virtual {v0, p1, v2, v1, v3}, Ldcw;->a(Lcom/google/android/search/shared/actions/VoiceAction;Ljava/lang/String;ZZ)V

    .line 249
    :goto_0
    return-void

    .line 247
    :cond_0
    invoke-virtual {v0, p1}, Ldcw;->t(Lcom/google/android/search/shared/actions/VoiceAction;)V

    goto :goto_0
.end method
