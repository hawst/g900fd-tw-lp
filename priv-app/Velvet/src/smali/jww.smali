.class public final Ljww;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ail:I

.field private dIb:I

.field private eJM:Ljava/lang/String;

.field public eJN:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2452
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2453
    iput v0, p0, Ljww;->aez:I

    iput v0, p0, Ljww;->ail:I

    iput v0, p0, Ljww;->dIb:I

    const-string v0, ""

    iput-object v0, p0, Ljww;->eJM:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljww;->eJN:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljww;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljww;->eCz:I

    .line 2454
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2364
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljww;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljww;->ail:I

    iget v0, p0, Ljww;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljww;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljww;->dIb:I

    iget v0, p0, Ljww;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljww;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljww;->eJM:Ljava/lang/String;

    iget v0, p0, Ljww;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljww;->aez:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljww;->eJN:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljww;->eJN:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljww;->eJN:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljww;->eJN:[Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 2470
    iget v0, p0, Ljww;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2471
    const/4 v0, 0x1

    iget v1, p0, Ljww;->ail:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2473
    :cond_0
    iget v0, p0, Ljww;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2474
    const/4 v0, 0x2

    iget v1, p0, Ljww;->dIb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2476
    :cond_1
    iget v0, p0, Ljww;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2477
    const/4 v0, 0x3

    iget-object v1, p0, Ljww;->eJM:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2479
    :cond_2
    iget-object v0, p0, Ljww;->eJN:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljww;->eJN:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 2480
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljww;->eJN:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 2481
    iget-object v1, p0, Ljww;->eJN:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 2482
    if-eqz v1, :cond_3

    .line 2483
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2480
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2487
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2488
    return-void
.end method

.method public final getErrorCode()I
    .locals 1

    .prologue
    .line 2411
    iget v0, p0, Ljww;->dIb:I

    return v0
.end method

.method public final getStatus()I
    .locals 1

    .prologue
    .line 2392
    iget v0, p0, Ljww;->ail:I

    return v0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2492
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2493
    iget v2, p0, Ljww;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 2494
    const/4 v2, 0x1

    iget v3, p0, Ljww;->ail:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2497
    :cond_0
    iget v2, p0, Ljww;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 2498
    const/4 v2, 0x2

    iget v3, p0, Ljww;->dIb:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2501
    :cond_1
    iget v2, p0, Ljww;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 2502
    const/4 v2, 0x3

    iget-object v3, p0, Ljww;->eJM:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2505
    :cond_2
    iget-object v2, p0, Ljww;->eJN:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ljww;->eJN:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    .line 2508
    :goto_0
    iget-object v4, p0, Ljww;->eJN:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 2509
    iget-object v4, p0, Ljww;->eJN:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 2510
    if-eqz v4, :cond_3

    .line 2511
    add-int/lit8 v3, v3, 0x1

    .line 2512
    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2508
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2516
    :cond_4
    add-int/2addr v0, v2

    .line 2517
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 2519
    :cond_5
    return v0
.end method

.method public final sQ(I)Ljww;
    .locals 1

    .prologue
    .line 2395
    iput p1, p0, Ljww;->ail:I

    .line 2396
    iget v0, p0, Ljww;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljww;->aez:I

    .line 2397
    return-object p0
.end method

.method public final sR(I)Ljww;
    .locals 1

    .prologue
    .line 2414
    iput p1, p0, Ljww;->dIb:I

    .line 2415
    iget v0, p0, Ljww;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljww;->aez:I

    .line 2416
    return-object p0
.end method
