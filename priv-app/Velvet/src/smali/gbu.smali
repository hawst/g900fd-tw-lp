.class public final Lgbu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public cFo:Landroid/widget/LinearLayout;

.field public cFp:Ljava/util/List;

.field public final mClock:Lemp;

.field public final mDirectionsLauncher:Lgah;

.field public final mEntry:Lizj;

.field public final mFrequentPlaceEntry:Ljal;


# direct methods
.method public constructor <init>(Ljal;Lizj;Lgah;Lemp;)V
    .locals 1
    .param p2    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lgah;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lgbu;->mFrequentPlaceEntry:Ljal;

    .line 58
    iput-object p2, p0, Lgbu;->mEntry:Lizj;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lgbu;->mDirectionsLauncher:Lgah;

    .line 60
    iput-object p4, p0, Lgbu;->mClock:Lemp;

    .line 61
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Lgbu;->cFp:Ljava/util/List;

    .line 62
    return-void
.end method


# virtual methods
.method public final aEy()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    iget-object v2, p0, Lgbu;->mFrequentPlaceEntry:Ljal;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgbu;->mFrequentPlaceEntry:Ljal;

    iget-object v2, v2, Ljal;->dMF:[Liyg;

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lgbu;->mFrequentPlaceEntry:Ljal;

    iget-object v2, v2, Ljal;->dMF:[Liyg;

    aget-object v2, v2, v1

    iget-object v2, v2, Liyg;->dPL:Liyh;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgbu;->mFrequentPlaceEntry:Ljal;

    iget-object v2, v2, Ljal;->dMF:[Liyg;

    aget-object v2, v2, v1

    iget-object v2, v2, Liyg;->dPL:Liyh;

    iget-object v2, v2, Liyh;->dQi:[Liyk;

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lgbu;->mFrequentPlaceEntry:Ljal;

    iget-object v2, v2, Ljal;->dMF:[Liyg;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Liyg;->aEz()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
