.class public final Licm;
.super Licq;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Licq;-><init>(Lcom/google/android/search/shared/actions/PlayMediaAction;Libo;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected final aUN()Lijj;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 44
    iget-object v1, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v1

    .line 45
    iget-object v2, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahO()Ljmh;

    move-result-object v2

    invoke-virtual {v2}, Ljmh;->bpF()Ljava/lang/String;

    move-result-object v2

    .line 48
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 49
    iget-object v3, p0, Licq;->mAppSelectionHelper:Libo;

    invoke-virtual {v3, v2}, Libo;->oW(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 51
    :cond_0
    iget-object v3, v1, Ljmh;->etD:Ljmi;

    invoke-virtual {v3}, Ljmi;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 52
    iget-object v2, p0, Licq;->mAppSelectionHelper:Libo;

    iget-object v1, v1, Ljmh;->etD:Ljmi;

    invoke-virtual {v1}, Ljmi;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Libo;->oW(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 55
    :cond_1
    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    return-object v0
.end method

.method protected final gU(Z)Lcom/google/android/shared/util/App;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64
    iget-object v0, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahQ()Ljava/util/List;

    move-result-object v3

    .line 65
    iget-object v0, p0, Licq;->dkT:Lcom/google/android/search/shared/actions/PlayMediaAction;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/PlayMediaAction;->ahP()Lcom/google/android/shared/util/App;

    move-result-object v2

    .line 66
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 67
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 68
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    .line 70
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 66
    goto :goto_0

    :cond_2
    move-object v0, v2

    .line 70
    goto :goto_1
.end method

.method public final oX(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return-object v0
.end method

.method public final oY(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method

.method public final oZ(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method
