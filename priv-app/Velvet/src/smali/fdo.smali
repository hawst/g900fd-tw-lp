.class public final Lfdo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final cks:Ljava/util/concurrent/CountDownLatch;

.field private cpl:Ljava/util/LinkedList;

.field private dK:Ljava/lang/Object;

.field private final mAsyncFileStorage:Lfbz;

.field private final mClock:Lemp;

.field public final mConfig:Lcjs;


# direct methods
.method public constructor <init>(Lemp;Lfbz;Lcjs;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfdo;->dK:Ljava/lang/Object;

    .line 45
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lfdo;->cpl:Ljava/util/LinkedList;

    .line 48
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lfdo;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 49
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lfdo;->cks:Ljava/util/concurrent/CountDownLatch;

    .line 54
    iput-object p2, p0, Lfdo;->mAsyncFileStorage:Lfbz;

    .line 55
    iput-object p1, p0, Lfdo;->mClock:Lemp;

    .line 56
    iput-object p3, p0, Lfdo;->mConfig:Lcjs;

    .line 57
    invoke-direct {p0}, Lfdo;->cV()V

    .line 58
    return-void
.end method

.method private awE()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 89
    iget-object v1, p0, Lfdo;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    :goto_0
    return v0

    .line 93
    :cond_0
    invoke-direct {p0}, Lfdo;->cV()V

    .line 95
    invoke-static {}, Lenu;->auQ()V

    .line 97
    :try_start_0
    iget-object v1, p0, Lfdo;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 99
    :catch_0
    move-exception v0

    const-string v0, "ActivityQueue"

    const-string v1, "Initialization latch wait interrupted"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 102
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cV()V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lfdo;->ckr:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lfdo;->mAsyncFileStorage:Lfbz;

    const-string v1, "activities"

    new-instance v2, Lfdp;

    invoke-direct {v2, p0}, Lfdp;-><init>(Lfdo;)V

    invoke-interface {v0, v1, v2}, Lfbz;->a(Ljava/lang/String;Lifg;)V

    .line 68
    :cond_0
    return-void
.end method


# virtual methods
.method public final N([B)V
    .locals 3

    .prologue
    .line 165
    if-eqz p1, :cond_0

    .line 166
    :try_start_0
    new-instance v0, Lapx;

    invoke-direct {v0}, Lapx;-><init>()V

    .line 167
    invoke-static {v0, p1}, Ljsr;->c(Ljsr;[B)Ljsr;

    .line 169
    iget-object v1, p0, Lfdo;->dK:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 170
    :try_start_1
    iget-object v2, p0, Lfdo;->cpl:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 171
    iget-object v2, p0, Lfdo;->cpl:Ljava/util/LinkedList;

    iget-object v0, v0, Lapx;->amc:[Liyy;

    invoke-static {v2, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 172
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    :cond_0
    iget-object v0, p0, Lfdo;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 178
    :goto_0
    return-void

    .line 172
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Ljsq; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 175
    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "ActivityQueue"

    const-string v1, "File storage contained invalid data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 177
    iget-object v0, p0, Lfdo;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    :catchall_1
    move-exception v0

    iget-object v1, p0, Lfdo;->cks:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 8

    .prologue
    .line 81
    invoke-static {p1}, Lgag;->c(Lcom/google/android/gms/location/ActivityRecognitionResult;)Liyy;

    move-result-object v0

    invoke-direct {p0}, Lfdo;->awE()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfdo;->dK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lfdo;->cpl:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    iget-object v0, p0, Lfdo;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iget-object v0, p0, Lfdo;->mConfig:Lcjs;

    invoke-virtual {v0}, Lcjs;->Mu()I

    move-result v0

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iget-object v0, p0, Lfdo;->cpl:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyy;

    invoke-virtual {v0}, Liyy;->bcB()J

    move-result-wide v6

    cmp-long v0, v6, v2

    if-gez v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lfdo;->awE()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v2, Lapx;

    invoke-direct {v2}, Lapx;-><init>()V

    iget-object v3, p0, Lfdo;->dK:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lfdo;->cpl:Ljava/util/LinkedList;

    iget-object v4, p0, Lfdo;->cpl:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    new-array v4, v4, [Liyy;

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Liyy;

    iput-object v0, v2, Lapx;->amc:[Liyy;

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, p0, Lfdo;->mAsyncFileStorage:Lfbz;

    const-string v3, "activities"

    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-interface {v0, v3, v2}, Lfbz;->g(Ljava/lang/String;[B)V

    :cond_2
    monitor-exit v1

    .line 82
    :cond_3
    return-void

    .line 81
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public final ayx()Ljava/util/List;
    .locals 2

    .prologue
    .line 139
    iget-object v1, p0, Lfdo;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 140
    :try_start_0
    iget-object v0, p0, Lfdo;->cpl:Ljava/util/LinkedList;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
