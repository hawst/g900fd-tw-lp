.class public final Lizc;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dRB:Ljava/lang/String;

.field private dRC:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1656
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1657
    const/4 v0, 0x0

    iput v0, p0, Lizc;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lizc;->dRB:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lizc;->dRC:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lizc;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lizc;->eCz:I

    .line 1658
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1593
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lizc;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizc;->dRB:Ljava/lang/String;

    iget v0, p0, Lizc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizc;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lizc;->dRC:Ljava/lang/String;

    iget v0, p0, Lizc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizc;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1672
    iget v0, p0, Lizc;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1673
    const/4 v0, 0x1

    iget-object v1, p0, Lizc;->dRB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1675
    :cond_0
    iget v0, p0, Lizc;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1676
    const/4 v0, 0x2

    iget-object v1, p0, Lizc;->dRC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1678
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1679
    return-void
.end method

.method public final getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1615
    iget-object v0, p0, Lizc;->dRB:Ljava/lang/String;

    return-object v0
.end method

.method public final getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1637
    iget-object v0, p0, Lizc;->dRC:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1683
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1684
    iget v1, p0, Lizc;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1685
    const/4 v1, 0x1

    iget-object v2, p0, Lizc;->dRB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1688
    :cond_0
    iget v1, p0, Lizc;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1689
    const/4 v1, 0x2

    iget-object v2, p0, Lizc;->dRC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1692
    :cond_1
    return v0
.end method

.method public final rA(Ljava/lang/String;)Lizc;
    .locals 1

    .prologue
    .line 1618
    if-nez p1, :cond_0

    .line 1619
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1621
    :cond_0
    iput-object p1, p0, Lizc;->dRB:Ljava/lang/String;

    .line 1622
    iget v0, p0, Lizc;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lizc;->aez:I

    .line 1623
    return-object p0
.end method

.method public final rB(Ljava/lang/String;)Lizc;
    .locals 1

    .prologue
    .line 1640
    if-nez p1, :cond_0

    .line 1641
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1643
    :cond_0
    iput-object p1, p0, Lizc;->dRC:Ljava/lang/String;

    .line 1644
    iget v0, p0, Lizc;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lizc;->aez:I

    .line 1645
    return-object p0
.end method
