.class public final Lewx;
.super Lewj;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field bAt:Landroid/widget/EditText;

.field private bAu:Landroid/widget/ListView;

.field private bAv:Landroid/widget/TextView;

.field private bAw:I

.field private bAx:Z

.field ckR:Liwk;

.field private ckS:Ljbp;

.field ckT:Ljbp;

.field ckU:Lexl;

.field ckV:Landroid/widget/EditText;

.field private ckW:Lhqm;

.field ckX:Z

.field ckY:Z

.field mEntry:Lizj;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lewj;-><init>()V

    return-void
.end method

.method public static a(Lizj;Liwk;ILjava/lang/String;)Lewx;
    .locals 3

    .prologue
    .line 136
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 137
    const-string v1, "entry_key"

    invoke-static {p0}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 138
    const-string v1, "action_key"

    invoke-static {p1}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 139
    const-string v1, "title_key"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    if-eqz p3, :cond_0

    .line 141
    const-string v1, "place_name_key"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_0
    new-instance v1, Lewx;

    invoke-direct {v1}, Lewx;-><init>()V

    .line 145
    invoke-virtual {v1, v0}, Lewx;->setArguments(Landroid/os/Bundle;)V

    .line 146
    return-object v1
.end method

.method private awt()V
    .locals 2

    .prologue
    .line 356
    invoke-direct {p0}, Lewx;->awu()Landroid/view/ViewParent;

    move-result-object v0

    .line 357
    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 358
    const/4 v0, 0x0

    iput-boolean v0, p0, Lewx;->ckY:Z

    .line 359
    return-void
.end method

.method private awu()Landroid/view/ViewParent;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 362
    iget-object v0, p0, Lewx;->ckU:Lexl;

    invoke-virtual {v0, v3}, Lexl;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 363
    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 364
    instance-of v0, v1, Landroid/view/View;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 367
    check-cast v0, Landroid/view/View;

    .line 368
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 369
    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v0

    .line 371
    const-string v2, "android:id/buttonPanel"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0
.end method

.method public static g(Landroid/content/Context;Landroid/content/Intent;)Lewx;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 156
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 157
    const-string v0, "entry"

    const-class v3, Lizj;

    invoke-static {v1, v0, v3}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lizj;

    .line 158
    const-string v3, "action"

    const-class v4, Liwk;

    invoke-static {v1, v3, v4}, Leqh;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljsr;

    move-result-object v1

    check-cast v1, Liwk;

    .line 159
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    move-object v0, v2

    .line 170
    :goto_0
    return-object v0

    .line 163
    :cond_1
    invoke-virtual {v1}, Liwk;->getType()I

    move-result v3

    const/16 v4, 0x11

    if-ne v3, v4, :cond_2

    .line 164
    const v3, 0x7f0a0316

    invoke-static {v0, v1, v3, v2}, Lewx;->a(Lizj;Liwk;ILjava/lang/String;)Lewx;

    move-result-object v0

    goto :goto_0

    .line 165
    :cond_2
    invoke-virtual {v1}, Liwk;->getType()I

    move-result v3

    const/16 v4, 0x12

    if-ne v3, v4, :cond_3

    .line 166
    iget-object v2, v0, Lizj;->dSc:Ljal;

    iget-object v2, v2, Ljal;->dWL:Ljak;

    .line 167
    invoke-static {p0, v2}, Lgbf;->c(Landroid/content/Context;Ljak;)Ljava/lang/String;

    move-result-object v2

    .line 168
    const v3, 0x7f0a0318

    invoke-static {v0, v1, v3, v2}, Lewx;->a(Lizj;Liwk;ILjava/lang/String;)Lewx;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    .line 170
    goto :goto_0
.end method

.method static h(Ljpd;)Ljbp;
    .locals 4

    .prologue
    .line 451
    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    .line 452
    invoke-virtual {p0}, Ljpd;->pb()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 453
    invoke-virtual {p0}, Ljpd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljbp;->st(Ljava/lang/String;)Ljbp;

    .line 455
    :cond_0
    invoke-virtual {p0}, Ljpd;->bfb()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 456
    invoke-virtual {p0}, Ljpd;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljbp;->su(Ljava/lang/String;)Ljbp;

    .line 458
    :cond_1
    invoke-virtual {p0}, Ljpd;->brM()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Ljpd;->brN()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 459
    invoke-virtual {p0}, Ljpd;->bpq()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljbp;->n(D)Ljbp;

    .line 460
    invoke-virtual {p0}, Ljpd;->bpr()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljbp;->o(D)Ljbp;

    .line 462
    :cond_2
    return-object v0
.end method


# virtual methods
.method final a(Ljbp;)V
    .locals 5

    .prologue
    .line 382
    invoke-virtual {p0}, Lewx;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 384
    if-nez v0, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 388
    :cond_1
    const-string v1, "editPlaceWorkerFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 389
    iget-object v1, p0, Lewx;->mEntry:Lizj;

    iget-object v2, p0, Lewx;->ckR:Liwk;

    iget-object v3, p0, Lewx;->ckS:Ljbp;

    invoke-virtual {v3}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lewx;->ckS:Ljbp;

    invoke-virtual {v4}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, p1, v3, v4}, Lexe;->a(Lizj;Liwk;Ljbp;Ljava/lang/String;Ljava/lang/String;)Lexe;

    move-result-object v1

    .line 393
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v2, "editPlaceWorkerFragment"

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    const-string v2, "editPlaceWorkerFragment"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 515
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 516
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 517
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lewx;->dx(Z)V

    .line 519
    :cond_0
    iget-boolean v1, p0, Lewx;->ckY:Z

    if-eqz v1, :cond_1

    .line 520
    invoke-direct {p0}, Lewx;->awt()V

    .line 522
    :cond_1
    iget-object v1, p0, Lewx;->ckW:Lhqm;

    invoke-virtual {v1, v0}, Lhqm;->jd(Ljava/lang/String;)V

    .line 523
    const/4 v0, 0x1

    iput-boolean v0, p0, Lewx;->ckX:Z

    .line 524
    return-void
.end method

.method final awr()V
    .locals 1

    .prologue
    .line 336
    iget-boolean v0, p0, Lewx;->ckY:Z

    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lewx;->aws()V

    .line 341
    :goto_0
    return-void

    .line 339
    :cond_0
    invoke-direct {p0}, Lewx;->awt()V

    goto :goto_0
.end method

.method final aws()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 345
    iget-object v0, p0, Lewx;->bAu:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lewx;->bAv:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 348
    invoke-direct {p0}, Lewx;->awu()Landroid/view/ViewParent;

    move-result-object v0

    .line 349
    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 351
    const/4 v0, 0x1

    iput-boolean v0, p0, Lewx;->ckY:Z

    .line 352
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 528
    return-void
.end method

.method final dx(Z)V
    .locals 3

    .prologue
    .line 500
    iget-object v0, p0, Lewx;->bAu:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 501
    :goto_0
    iget-object v1, p0, Lewx;->ckW:Lhqm;

    iget-boolean v1, v1, Lhqm;->bnA:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0a08ab

    .line 503
    :goto_1
    iget-object v2, p0, Lewx;->bAv:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 504
    if-eqz p1, :cond_3

    if-nez v0, :cond_3

    .line 505
    iget-object v0, p0, Lewx;->bAu:Landroid/widget/ListView;

    iget-object v1, p0, Lewx;->bAv:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 510
    :cond_0
    :goto_2
    return-void

    .line 500
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 501
    :cond_2
    iget v1, p0, Lewx;->bAw:I

    goto :goto_1

    .line 506
    :cond_3
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lewx;->bAv:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 508
    iget-object v0, p0, Lewx;->bAu:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 192
    invoke-virtual {p0}, Lewx;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 193
    const-string v0, "entry_key"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->Q([B)Lizj;

    move-result-object v0

    iput-object v0, p0, Lewx;->mEntry:Lizj;

    .line 194
    const-string v0, "action_key"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lgbm;->R([B)Liwk;

    move-result-object v0

    iput-object v0, p0, Lewx;->ckR:Liwk;

    .line 195
    iget-object v0, p0, Lewx;->mEntry:Lizj;

    invoke-static {v0}, Lgbf;->L(Lizj;)Ljbp;

    move-result-object v0

    iput-object v0, p0, Lewx;->ckS:Ljbp;

    .line 196
    new-instance v0, Lhqm;

    invoke-virtual {p0}, Lewx;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v4, 0x3

    invoke-direct {v0, v1, v4}, Lhqm;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lewx;->ckW:Lhqm;

    .line 198
    iget-object v0, p0, Lewx;->ckW:Lhqm;

    iput-boolean v6, v0, Lhqm;->doj:Z

    .line 200
    if-eqz p1, :cond_7

    const-string v0, "place_name_key"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 203
    :goto_0
    if-nez v0, :cond_b

    .line 204
    const-string v0, "place_name_key"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 207
    :goto_1
    iget-object v0, p0, Lewx;->ckS:Ljbp;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lewx;->ckS:Ljbp;

    invoke-static {v0}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Ljbp;

    iput-object v0, p0, Lewx;->ckT:Ljbp;

    .line 211
    :cond_0
    invoke-virtual {p0}, Lewx;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 213
    const v4, 0x7f040010

    invoke-virtual {v0, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lewx;->mView:Landroid/view/View;

    .line 215
    const-string v0, "title_key"

    invoke-virtual {v3, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 216
    new-instance v3, Lexl;

    invoke-virtual {p0}, Lewx;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {p0}, Lewx;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-direct {v3, v4, v5, v0}, Lexl;-><init>(Landroid/content/Context;Landroid/app/FragmentManager;I)V

    iput-object v3, p0, Lewx;->ckU:Lexl;

    .line 218
    iget-object v0, p0, Lewx;->ckU:Lexl;

    iget-object v3, p0, Lewx;->mView:Landroid/view/View;

    invoke-virtual {v0, v3}, Lexl;->setView(Landroid/view/View;)V

    .line 220
    iget-object v0, p0, Lewx;->mView:Landroid/view/View;

    const v3, 0x7f110077

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lewx;->bAt:Landroid/widget/EditText;

    .line 221
    iget-object v0, p0, Lewx;->mView:Landroid/view/View;

    const v3, 0x7f110078

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lewx;->bAu:Landroid/widget/ListView;

    .line 222
    iget-object v0, p0, Lewx;->mView:Landroid/view/View;

    const v3, 0x7f110079

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lewx;->bAv:Landroid/widget/TextView;

    .line 223
    const v0, 0x7f0a08a1

    iput v0, p0, Lewx;->bAw:I

    .line 226
    iput-boolean v6, p0, Lewx;->ckY:Z

    .line 228
    iput-boolean v6, p0, Lewx;->ckX:Z

    .line 230
    if-eqz p1, :cond_a

    .line 231
    const-string v0, "filter_key"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 232
    const-string v2, "buttons_visible_key"

    invoke-virtual {p1, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lewx;->ckY:Z

    .line 233
    const-string v2, "address_is_dirty_key"

    invoke-virtual {p1, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lewx;->ckX:Z

    .line 234
    iget-object v2, p0, Lewx;->ckT:Ljbp;

    if-nez v2, :cond_1

    .line 235
    new-instance v2, Ljbp;

    invoke-direct {v2}, Ljbp;-><init>()V

    iput-object v2, p0, Lewx;->ckT:Ljbp;

    .line 237
    :cond_1
    iget-object v2, p0, Lewx;->ckT:Ljbp;

    const-string v3, "new_location_name_key"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "new_location_name_key"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljbp;->st(Ljava/lang/String;)Ljbp;

    :cond_2
    const-string v3, "new_location_address_key"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "new_location_address_key"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljbp;->su(Ljava/lang/String;)Ljbp;

    :cond_3
    const-string v3, "new_location_latitude_key"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "new_location_longitude_key"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "new_location_latitude_key"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljbp;->n(D)Ljbp;

    const-string v3, "new_location_longitude_key"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljbp;->o(D)Ljbp;

    .line 240
    :cond_4
    :goto_2
    if-nez v0, :cond_5

    .line 241
    iget-object v0, p0, Lewx;->ckS:Ljbp;

    invoke-virtual {v0}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 243
    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 245
    iput-boolean v7, p0, Lewx;->ckX:Z

    .line 249
    :cond_6
    :goto_3
    iget-object v0, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 251
    iget-object v0, p0, Lewx;->mView:Landroid/view/View;

    const v2, 0x7f110076

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lewx;->ckV:Landroid/widget/EditText;

    .line 252
    if-eqz v1, :cond_9

    .line 253
    iget-object v0, p0, Lewx;->ckV:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lewx;->ckV:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :goto_4
    iget-object v0, p0, Lewx;->ckU:Lexl;

    const v1, 0x104000a

    new-instance v2, Lewz;

    invoke-direct {v2, p0}, Lewz;-><init>(Lewx;)V

    invoke-virtual {v0, v1, v2}, Lexl;->a(ILandroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lewx;->ckU:Lexl;

    const/high16 v1, 0x1040000

    new-instance v2, Lexa;

    invoke-direct {v2, p0}, Lexa;-><init>(Lewx;)V

    invoke-virtual {v0, v1, v2}, Lexl;->b(ILandroid/view/View$OnClickListener;)V

    .line 261
    iget-object v0, p0, Lewx;->ckW:Lhqm;

    iget-object v0, v0, Lhqm;->doh:Lhqr;

    new-instance v1, Lexb;

    invoke-direct {v1, p0}, Lexb;-><init>(Lewx;)V

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v1, p0, Lewx;->bAu:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lewx;->bAu:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lewx;->ckW:Lhqm;

    invoke-virtual {v1, v0}, Lhqm;->jd(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lewx;->ckU:Lexl;

    new-instance v1, Lewy;

    invoke-direct {v1, p0}, Lewy;-><init>(Lewx;)V

    invoke-virtual {v0, v1}, Lexl;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 275
    iget-object v0, p0, Lewx;->ckU:Lexl;

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Lexl;->ir(I)V

    .line 278
    iget-object v0, p0, Lewx;->ckU:Lexl;

    return-object v0

    :cond_7
    move-object v0, v2

    .line 200
    goto/16 :goto_0

    .line 247
    :cond_8
    iget-object v2, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v2, v7}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    iget-object v2, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v2, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iput-boolean v7, p0, Lewx;->bAx:Z

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    goto/16 :goto_3

    .line 256
    :cond_9
    iget-object v0, p0, Lewx;->ckV:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_4

    :cond_a
    move-object v0, v2

    goto/16 :goto_2

    :cond_b
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 546
    iget-object v0, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    if-eqz p2, :cond_2

    .line 548
    iget-boolean v0, p0, Lewx;->bAx:Z

    if-eqz v0, :cond_1

    .line 549
    iget-object v0, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 550
    const/4 v0, 0x0

    iput-boolean v0, p0, Lewx;->bAx:Z

    .line 564
    :cond_0
    :goto_0
    return-void

    .line 553
    :cond_1
    iget-object v0, p0, Lewx;->ckW:Lhqm;

    iget-object v1, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhqm;->jd(Ljava/lang/String;)V

    goto :goto_0

    .line 557
    :cond_2
    iget-boolean v0, p0, Lewx;->ckX:Z

    if-nez v0, :cond_0

    .line 560
    invoke-virtual {p0}, Lewx;->aws()V

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    .prologue
    .line 402
    new-instance v1, Lexc;

    invoke-direct {v1, p0}, Lexc;-><init>(Lewx;)V

    .line 436
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqq;

    .line 439
    iget-object v2, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {p0, v2}, Lewx;->bf(Landroid/view/View;)V

    .line 440
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    .line 441
    invoke-virtual {v2}, Lgql;->aJr()Lfdb;

    move-result-object v2

    invoke-virtual {v2}, Lfdb;->axI()Lfcx;

    move-result-object v2

    .line 443
    new-instance v3, Lhqp;

    invoke-direct {v3, v1, v0, v2}, Lhqp;-><init>(Lefk;Lhqq;Lfcx;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v3, v0}, Lhqp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 444
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 175
    invoke-super {p0, p1}, Lewj;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 177
    iget-object v0, p0, Lewx;->ckV:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 178
    const-string v0, "place_name_key"

    iget-object v1, p0, Lewx;->ckV:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_0
    const-string v0, "filter_key"

    iget-object v1, p0, Lewx;->bAt:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "buttons_visible_key"

    iget-boolean v1, p0, Lewx;->ckY:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 183
    const-string v0, "address_is_dirty_key"

    iget-boolean v1, p0, Lewx;->ckX:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    iget-object v0, p0, Lewx;->ckT:Ljbp;

    if-eqz v0, :cond_3

    .line 186
    iget-object v0, p0, Lewx;->ckT:Ljbp;

    invoke-virtual {v0}, Ljbp;->oK()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "new_location_name_key"

    invoke-virtual {v0}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Ljbp;->bfb()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "new_location_address_key"

    invoke-virtual {v0}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Ljbp;->nH()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljbp;->nI()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "new_location_latitude_key"

    invoke-virtual {v0}, Ljbp;->mR()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    const-string v1, "new_location_longitude_key"

    invoke-virtual {v0}, Ljbp;->mS()D

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 188
    :cond_3
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 532
    return-void
.end method
