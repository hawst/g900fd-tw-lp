.class public final Ldvy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bOj:Ldxd;

.field private final bOk:Z

.field private final bOl:Z

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Ldxd;Landroid/content/res/Resources;ZZ)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Ldvy;->bOj:Ldxd;

    .line 36
    iput-object p2, p0, Ldvy;->mResources:Landroid/content/res/Resources;

    .line 37
    iput-boolean p3, p0, Ldvy;->bOk:Z

    .line 38
    iput-boolean p4, p0, Ldvy;->bOl:Z

    .line 39
    return-void
.end method

.method private a(Ldwb;ZZIZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 133
    iget-object v0, p1, Ldwb;->bOp:Ljrg;

    if-nez v0, :cond_0

    .line 134
    invoke-virtual {p1}, Ldwb;->ajp()V

    .line 135
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    .line 168
    :goto_0
    return-object v0

    .line 138
    :cond_0
    new-instance v0, Ldya;

    invoke-direct {v0}, Ldya;-><init>()V

    .line 139
    iget-object v1, p0, Ldvy;->bOj:Ldxd;

    iget-object v2, p1, Ldwb;->bOp:Ljrg;

    iget-object v2, v2, Ljrg;->eAn:Ljqv;

    if-eqz v2, :cond_1

    iget-object v2, p1, Ldwb;->bOp:Ljrg;

    iget-object v2, v2, Ljrg;->eAn:Ljqv;

    iget-object v3, p1, Ldwb;->bOo:Ldvu;

    invoke-virtual {v1, v2, v3}, Ldxd;->a(Ljqv;Ldxb;)Ldws;

    move-result-object v2

    invoke-virtual {v2}, Ldws;->Pd()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Ldws;->getString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Ldwb;->a(Ldya;Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v2, p1, Ldwb;->bOp:Ljrg;

    iget-object v2, v2, Ljrg;->eAo:Ljqv;

    if-eqz v2, :cond_2

    iget-object v2, p1, Ldwb;->bOp:Ljrg;

    iget-object v2, v2, Ljrg;->eAo:Ljqv;

    iget-object v3, p1, Ldwb;->bOo:Ldvu;

    invoke-virtual {v1, v2, v3}, Ldxd;->a(Ljqv;Ldxb;)Ldws;

    move-result-object v1

    invoke-virtual {v1}, Ldws;->Pd()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Ldws;->getString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ldwb;->b(Ldya;Ljava/lang/String;)V

    .line 143
    :cond_2
    :goto_2
    if-eqz p3, :cond_6

    .line 144
    iget-object v1, p1, Ldwb;->bOp:Ljrg;

    invoke-virtual {v1}, Ljrg;->bth()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 168
    :cond_3
    :goto_3
    iput-boolean v5, v0, Ldya;->bQq:Z

    invoke-virtual {v0}, Ldya;->alp()Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    .line 139
    :cond_4
    iget-object v2, p1, Ldwb;->bOo:Ldvu;

    invoke-interface {v2}, Ldvu;->ajo()Ldwd;

    move-result-object v2

    const/4 v3, 0x5

    iget-object v4, p1, Ldwb;->bOp:Ljrg;

    iget-object v4, v4, Ljrg;->eAn:Ljqv;

    invoke-virtual {v2, v3, v4}, Ldwd;->a(ILjqv;)V

    goto :goto_1

    :cond_5
    iget-object v1, p1, Ldwb;->bOo:Ldvu;

    invoke-interface {v1}, Ldvu;->ajo()Ldwd;

    move-result-object v1

    const/4 v2, 0x6

    iget-object v3, p1, Ldwb;->bOp:Ljrg;

    iget-object v3, v3, Ljrg;->eAo:Ljqv;

    invoke-virtual {v1, v2, v3}, Ldwd;->a(ILjqv;)V

    goto :goto_2

    .line 146
    :pswitch_0
    invoke-virtual {v0}, Ldya;->alq()Ldya;

    goto :goto_3

    .line 149
    :pswitch_1
    invoke-virtual {v0}, Ldya;->alr()Ldya;

    goto :goto_3

    .line 155
    :cond_6
    if-eqz p2, :cond_3

    .line 156
    if-eqz p6, :cond_7

    .line 157
    int-to-long v2, p4

    invoke-virtual {v0, v2, v3}, Ldya;->aB(J)Ldya;

    .line 163
    if-nez p5, :cond_3

    .line 164
    iput-boolean v5, v0, Ldya;->bQl:Z

    goto :goto_3

    .line 161
    :cond_7
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto/16 :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a([Ljrg;I)Ljrg;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 208
    array-length v0, p0

    if-nez v0, :cond_1

    .line 209
    const/4 v0, 0x0

    .line 218
    :cond_0
    :goto_0
    return-object v0

    .line 212
    :cond_1
    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v0, p0, v1

    .line 213
    invoke-virtual {v0}, Ljrg;->Pq()I

    move-result v3

    if-eq v3, p1, :cond_0

    .line 212
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 218
    :cond_2
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p0, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/shared/util/MatchingAppInfo;Ljqs;)Z
    .locals 2
    .param p0    # Lcom/google/android/shared/util/MatchingAppInfo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljqs;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/shared/util/MatchingAppInfo;->avj()Z

    move-result v1

    if-nez v1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljqs;->bsB()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ldvu;ZII)Lcom/google/android/search/shared/actions/utils/CardDecision;
    .locals 8

    .prologue
    .line 46
    invoke-interface {p1}, Ldvu;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    invoke-interface {p1}, Ldvu;->aje()Ljqb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Ljqb;->eyh:Ljrg;

    if-eqz v1, :cond_0

    new-instance v1, Ldvz;

    iget-object v0, v0, Ljqb;->eyh:Ljrg;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v0, v2}, Ldvz;-><init>(Ldvy;Ldvu;Ljrg;I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Ldvy;->a(Ldwb;ZZIZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 49
    :cond_0
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0

    .line 51
    :cond_1
    invoke-interface {p1}, Ldvu;->agg()Lcom/google/android/search/shared/actions/ActionExecutionState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->yo()Z

    move-result v0

    if-nez v0, :cond_4

    .line 52
    invoke-interface {p1}, Ldvu;->agg()Lcom/google/android/search/shared/actions/ActionExecutionState;

    move-result-object v0

    invoke-interface {p1}, Ldvu;->aji()Ljrl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/search/shared/actions/ActionExecutionState;->a(Ljrl;)Ljqt;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, v0, Ljqt;->ezs:Ljrg;

    if-eqz v1, :cond_3

    new-instance v1, Ldvz;

    iget-object v0, v0, Ljqt;->ezs:Ljrg;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v0, v2}, Ldvz;-><init>(Ldvy;Ldvu;Ljrg;I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Ldvy;->a(Ldwb;ZZIZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Ldvu;->ajo()Ldwd;

    invoke-interface {p1}, Ldvu;->agg()Lcom/google/android/search/shared/actions/ActionExecutionState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/ActionExecutionState;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No ExecutionState for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0

    .line 55
    :cond_4
    invoke-interface {p1}, Ldvu;->aje()Ljqb;

    move-result-object v1

    .line 56
    if-nez v1, :cond_5

    .line 57
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQg:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto :goto_0

    .line 60
    :cond_5
    invoke-interface {p1}, Ldvu;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-interface {p1}, Ldvu;->ajf()Ljqs;

    move-result-object v2

    invoke-static {v0, v2}, Ldvy;->a(Lcom/google/android/shared/util/MatchingAppInfo;Ljqs;)Z

    move-result v7

    .line 64
    iget-object v2, v1, Ljqb;->eye:[Ljqh;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_a

    aget-object v5, v2, v0

    .line 65
    iget-object v4, p0, Ldvy;->mResources:Landroid/content/res/Resources;

    invoke-interface {p1, v5, v4}, Ldvu;->a(Ljqh;Landroid/content/res/Resources;)[Ljrg;

    move-result-object v6

    .line 66
    if-eqz v6, :cond_9

    .line 67
    new-instance v0, Ldwa;

    invoke-static {v6, p3}, Ldvy;->a([Ljrg;I)Ljrg;

    move-result-object v3

    invoke-virtual {v5}, Ljqh;->all()I

    move-result v4

    sget-object v1, Ljqj;->eyK:Ljsm;

    invoke-virtual {v5, v1}, Ljqh;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljqj;

    if-eqz v1, :cond_7

    iget-object v2, v1, Ljqj;->eyP:[Ljrg;

    if-eq v2, v6, :cond_6

    iget-object v1, v1, Ljqj;->eyO:[Ljrg;

    if-ne v1, v6, :cond_7

    :cond_6
    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_8

    const/4 v5, 0x6

    :goto_3
    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Ldwa;-><init>(Ldvy;Ldvu;Ljrg;II)V

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, v0

    invoke-direct/range {v1 .. v7}, Ldvy;->a(Ldwb;ZZIZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto :goto_2

    :cond_8
    invoke-virtual {v5}, Ljqh;->all()I

    move-result v1

    invoke-interface {p1, v1}, Ldvu;->gB(I)I

    move-result v5

    goto :goto_3

    .line 64
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 80
    :cond_a
    invoke-interface {p1}, Ldvu;->aji()Ljrl;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Ljrl;->btq()I

    move-result v2

    .line 82
    if-nez v2, :cond_b

    if-nez p2, :cond_b

    const/4 v0, 0x1

    .line 84
    :goto_4
    if-eqz v0, :cond_c

    .line 85
    new-instance v2, Ldvz;

    iget-object v0, v1, Ljqb;->eyf:[Ljrg;

    invoke-static {v0, p3}, Ldvy;->a([Ljrg;I)Ljrg;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {v2, p0, p1, v0, v1}, Ldvz;-><init>(Ldvy;Ldvu;Ljrg;I)V

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Ldvy;->a(Ldwb;ZZIZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    .line 82
    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    .line 90
    :cond_c
    iget-boolean v0, p0, Ldvy;->bOk:Z

    if-eqz v0, :cond_10

    .line 91
    const/4 v0, 0x2

    if-ne v2, v0, :cond_d

    iget-boolean v0, p0, Ldvy;->bOl:Z

    if-nez v0, :cond_e

    :cond_d
    const/4 v6, 0x1

    .line 95
    :goto_5
    const/4 v0, 0x1

    if-ne v2, v0, :cond_f

    move v5, p4

    .line 97
    :goto_6
    new-instance v2, Ldvz;

    iget-object v0, v1, Ljqb;->eyg:Ljrg;

    const/4 v1, 0x0

    invoke-direct {v2, p0, p1, v0, v1}, Ldvz;-><init>(Ldvy;Ldvu;Ljrg;I)V

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Ldvy;->a(Ldwb;ZZIZZ)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v0

    goto/16 :goto_0

    .line 91
    :cond_e
    const/4 v6, 0x0

    goto :goto_5

    .line 95
    :cond_f
    const/4 v5, 0x0

    goto :goto_6

    .line 103
    :cond_10
    sget-object v0, Lcom/google/android/search/shared/actions/utils/CardDecision;->bQh:Lcom/google/android/search/shared/actions/utils/CardDecision;

    goto/16 :goto_0
.end method
