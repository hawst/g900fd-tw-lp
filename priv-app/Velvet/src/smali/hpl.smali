.class final Lhpl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic anT:Lfzw;

.field private synthetic cqp:Lizj;

.field private synthetic dny:Lizq;

.field private synthetic dnz:Lhpk;


# direct methods
.method constructor <init>(Lhpk;Lfzw;Lizq;Lizj;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lhpl;->dnz:Lhpk;

    iput-object p2, p0, Lhpl;->anT:Lfzw;

    iput-object p3, p0, Lhpl;->dny:Lizq;

    iput-object p4, p0, Lhpl;->cqp:Lizj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 67
    iget-object v0, p0, Lhpl;->anT:Lfzw;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lhpl;->dnz:Lhpk;

    iget-object v3, p0, Lhpl;->dny:Lizq;

    iget-object v4, p0, Lhpl;->cqp:Lizj;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.google.android.voicesearch.fragments.personal.OPEN"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "com.google.android.googlequicksearchbox"

    const-class v7, Lcom/google/android/voicesearch/fragments/personal/PersonalSearchActivity;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v6, 0x20000000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v6, "expanded_tree"

    invoke-static {v5, v6, v3}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    const-string v3, "scroll_target"

    invoke-static {v5, v3, v4}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    iget-object v3, v2, Lhpk;->dnx:Lizq;

    if-eqz v3, :cond_0

    const-string v3, "full_tree"

    iget-object v4, v2, Lhpk;->dnx:Lizq;

    invoke-static {v5, v3, v4}, Leqh;->a(Landroid/content/Intent;Ljava/lang/String;Ljsr;)V

    :cond_0
    iget-object v3, v2, Lhpk;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    if-eqz v3, :cond_1

    const-string v3, "rendering_context"

    iget-object v4, v2, Lhpk;->mCardRenderingContext:Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;

    invoke-virtual {v5, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    iget-object v3, v2, Lhpk;->bLI:Ljava/lang/String;

    if-eqz v3, :cond_2

    const-string v3, "title"

    iget-object v2, v2, Lhpk;->bLI:Ljava/lang/String;

    invoke-virtual {v5, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    invoke-virtual {v0, v1, v5}, Lfzw;->h(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 69
    return-void
.end method
