.class public final Lgor;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final cRE:Ljava/lang/String;

.field private cRF:Ljava/lang/Class;

.field public final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lgor;->name:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lgor;->cRE:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lgor;->cRF:Ljava/lang/Class;

    .line 30
    return-void
.end method

.method private static equals(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 48
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38
    if-eqz p1, :cond_0

    instance-of v1, p1, Lgor;

    if-nez v1, :cond_1

    .line 42
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    check-cast p1, Lgor;

    .line 42
    iget-object v1, p0, Lgor;->name:Ljava/lang/String;

    iget-object v2, p1, Lgor;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lgor;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgor;->cRE:Ljava/lang/String;

    iget-object v2, p1, Lgor;->cRE:Ljava/lang/String;

    invoke-static {v1, v2}, Lgor;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgor;->cRF:Ljava/lang/Class;

    iget-object v2, p1, Lgor;->cRF:Ljava/lang/Class;

    invoke-static {v1, v2}, Lgor;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 53
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lgor;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lgor;->cRE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lgor;->cRF:Ljava/lang/Class;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
