.class public Ldcv;
.super Lddj;
.source "PG"


# instance fields
.field private bqv:Lcom/google/android/shared/search/Query;

.field private mActionData:Lcom/google/android/velvet/ActionData;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldda;)V
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x200

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 35
    sget-object v0, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v0, p0, Ldcv;->bqv:Lcom/google/android/shared/search/Query;

    .line 45
    return-void
.end method


# virtual methods
.method public final Zl()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ldcv;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Ljrp;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 95
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    iget-object v2, p0, Ldcv;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 96
    const/4 v0, 0x1

    invoke-static {p2, v4, v4, v0}, Lcom/google/android/velvet/ActionData;->a(Ljrp;Ljyw;Ljava/lang/String;Z)Lcom/google/android/velvet/ActionData;

    move-result-object v0

    iput-object v0, p0, Ldcv;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 97
    invoke-virtual {p0}, Ldcv;->notifyChanged()V

    .line 99
    :cond_0
    return-void
.end method

.method public final a(Lddb;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    .line 51
    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldcv;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Lcom/google/android/search/core/state/QueryState;->K(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 53
    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Ldcv;->bqv:Lcom/google/android/shared/search/Query;

    .line 54
    iput-object v2, p0, Ldcv;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 58
    :cond_0
    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    invoke-virtual {p1}, Lddb;->Nf()Ldda;

    move-result-object v0

    invoke-virtual {v0}, Ldda;->aaj()Ldbd;

    move-result-object v0

    invoke-virtual {v0}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agw()Z

    move-result v0

    if-nez v0, :cond_1

    .line 61
    iput-object v2, p0, Ldcv;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 67
    :cond_1
    return-void
.end method

.method public final a(Letj;)V
    .locals 2

    .prologue
    .line 103
    const-string v0, "Action data"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldcv;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 104
    const-string v0, "Query"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Ldcv;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 105
    return-void
.end method

.method public final as(Lcom/google/android/shared/search/Query;)Lcom/google/android/velvet/ActionData;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Ldcv;->bqv:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v0

    iget-object v2, p0, Ldcv;->bqv:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Ldcv;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 85
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
