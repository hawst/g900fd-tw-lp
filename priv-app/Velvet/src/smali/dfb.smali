.class public final Ldfb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldeb;


# instance fields
.field private final bgq:Lchk;


# direct methods
.method public constructor <init>(Lchk;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Ldfb;->bgq:Lchk;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Ldem;ILdef;Lddw;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-virtual {p1}, Ldem;->abk()Z

    move-result v0

    if-nez v0, :cond_2

    .line 44
    if-eqz p3, :cond_1

    .line 45
    invoke-interface {p3}, Ldef;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 46
    invoke-interface {p4}, Lddw;->getCount()I

    move-result v2

    if-ge v2, p2, :cond_0

    .line 47
    invoke-interface {p4, v0}, Lddw;->d(Lcom/google/android/shared/search/Suggestion;)Z

    goto :goto_0

    .line 51
    :cond_0
    invoke-interface {p3}, Ldef;->abd()Z

    move-result v0

    invoke-interface {p4, v0}, Lddw;->dt(Z)V

    .line 86
    :cond_1
    :goto_1
    return-void

    .line 56
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 57
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 59
    invoke-virtual {p1}, Ldem;->abm()Ldef;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_7

    .line 62
    invoke-interface {v0}, Ldef;->abd()Z

    move-result v1

    invoke-interface {p4, v1}, Lddw;->dt(Z)V

    .line 63
    invoke-interface {v0}, Ldef;->abb()Lcom/google/android/shared/search/Query;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v5

    .line 64
    invoke-interface {v0}, Ldef;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/search/Suggestion;

    .line 65
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->aso()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 66
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 67
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asr()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->ass()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asq()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/shared/search/Suggestion;->asj()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_3

    .line 70
    :cond_5
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    move v1, v2

    .line 67
    goto :goto_3

    .line 76
    :cond_7
    iget-object v0, p0, Ldfb;->bgq:Lchk;

    invoke-virtual {v0}, Lchk;->Hp()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 77
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p2, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-interface {v3, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {p4, v0}, Lddw;->c(Ljava/lang/Iterable;)I

    .line 79
    invoke-interface {p4, v4}, Lddw;->c(Ljava/lang/Iterable;)I

    .line 85
    :goto_4
    invoke-interface {p4}, Lddw;->aaZ()V

    goto/16 :goto_1

    .line 81
    :cond_8
    invoke-interface {p4, v4}, Lddw;->c(Ljava/lang/Iterable;)I

    .line 82
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p2, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-interface {v3, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {p4, v0}, Lddw;->c(Ljava/lang/Iterable;)I

    goto :goto_4
.end method
