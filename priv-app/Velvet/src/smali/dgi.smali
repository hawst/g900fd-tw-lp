.class public abstract Ldgi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected final bwW:Leqo;

.field protected final mShouldQueryStrategy:Ldfz;


# direct methods
.method public constructor <init>(Ldfz;Leqo;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfz;

    iput-object v0, p0, Ldgi;->mShouldQueryStrategy:Ldfz;

    .line 36
    invoke-static {p2}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqo;

    iput-object v0, p0, Ldgi;->bwW:Leqo;

    .line 37
    iget-object v0, p0, Ldgi;->bwW:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Query;Ldem;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ldgi;->bwW:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 45
    iget-object v0, p0, Ldgi;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0, p1}, Ldfz;->aD(Lcom/google/android/shared/search/Query;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ldem;->fL(I)V

    .line 47
    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Ldem;->fL(I)V

    .line 48
    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Ldem;->fL(I)V

    .line 71
    :goto_0
    return-void

    .line 51
    :cond_0
    new-instance v0, Ldgj;

    invoke-direct {v0, p0, p2}, Ldgj;-><init>(Ldgi;Ldem;)V

    invoke-virtual {p0, p1, v0}, Ldgi;->a(Lcom/google/android/shared/search/Query;Ldgk;)V

    goto :goto_0
.end method

.method protected abstract a(Lcom/google/android/shared/search/Query;Ldgk;)V
.end method

.method protected abT()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Ldgi;->bwW:Leqo;

    invoke-interface {v0}, Leqo;->Du()Z

    move-result v0

    invoke-static {v0}, Lifv;->gY(Z)V

    .line 81
    invoke-virtual {p0}, Ldgi;->abT()V

    .line 82
    return-void
.end method
