.class abstract Lcrw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private synthetic bhn:Lcrr;


# direct methods
.method constructor <init>(Lcrr;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcrw;->bhn:Lcrr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract h(Ljava/util/Map;)Ljava/lang/Object;
.end method

.method public final r(Landroid/accounts/Account;)Ljava/lang/Object;
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v0, 0x0

    .line 224
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v1, 0x2

    if-ge v2, v1, :cond_0

    .line 226
    :try_start_0
    iget-object v1, p0, Lcrw;->bhn:Lcrr;

    if-nez p1, :cond_1

    move-object v1, v0

    .line 227
    :goto_1
    if-nez v1, :cond_3

    .line 244
    :cond_0
    :goto_2
    return-object v0

    .line 226
    :cond_1
    iget-object v3, v1, Lcrr;->mLoginHelper:Lcrh;

    iget-object v1, v1, Lcrr;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->IX()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v4, 0x3e8

    invoke-virtual {v3, p1, v1, v4, v5}, Lcrh;->c(Landroid/accounts/Account;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    move-object v1, v0

    goto :goto_1

    :cond_2
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    const-string v4, "Authorization"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bearer "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "X-Developer-Key"

    const-string v4, "1016085902054.apps.googleusercontent.com"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Left; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 231
    :catch_0
    move-exception v1

    .line 232
    invoke-virtual {v1}, Left;->getErrorCode()I

    move-result v3

    const/16 v4, 0x193

    if-ne v3, v4, :cond_4

    .line 233
    const-string v3, "Search.SearchHistoryHelper"

    const-string v4, "Authorization exception"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 235
    iget-object v1, p0, Lcrw;->bhn:Lcrr;

    iget-object v1, v1, Lcrr;->mLoginHelper:Lcrh;

    iget-object v3, p0, Lcrw;->bhn:Lcrr;

    iget-object v3, v3, Lcrr;->mGsaConfig:Lchk;

    invoke-virtual {v3}, Lchk;->IX()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v8, v9}, Lcrh;->k(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    .line 237
    iget-object v3, p0, Lcrw;->bhn:Lcrr;

    iget-object v3, v3, Lcrr;->mLoginHelper:Lcrh;

    invoke-virtual {v3, v1}, Lcrh;->iC(Ljava/lang/String;)V

    .line 224
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 230
    :cond_3
    :try_start_1
    invoke-virtual {p0, v1}, Lcrw;->h(Ljava/util/Map;)Ljava/lang/Object;
    :try_end_1
    .catch Left; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_2

    .line 239
    :cond_4
    throw v1
.end method
