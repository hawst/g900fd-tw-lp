.class public final Lxj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic Fw:F

.field private synthetic KX:Lcom/android/launcher3/Launcher;

.field private synthetic Lj:Landroid/view/View;

.field private synthetic Lk:Z

.field private synthetic Ll:Lcom/android/launcher3/AppsCustomizeTabHost;

.field private synthetic Lm:Landroid/view/View;

.field private synthetic Lp:Landroid/animation/AnimatorSet;

.field private synthetic Lq:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Launcher;Landroid/animation/AnimatorSet;Landroid/view/View;ZLcom/android/launcher3/AppsCustomizeTabHost;Landroid/view/View;FLjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 3388
    iput-object p1, p0, Lxj;->KX:Lcom/android/launcher3/Launcher;

    iput-object p2, p0, Lxj;->Lp:Landroid/animation/AnimatorSet;

    iput-object p3, p0, Lxj;->Lj:Landroid/view/View;

    iput-boolean p4, p0, Lxj;->Lk:Z

    iput-object p5, p0, Lxj;->Ll:Lcom/android/launcher3/AppsCustomizeTabHost;

    iput-object p6, p0, Lxj;->Lm:Landroid/view/View;

    iput p7, p0, Lxj;->Fw:F

    iput-object p8, p0, Lxj;->Lq:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3392
    iget-object v1, p0, Lxj;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v1}, Lcom/android/launcher3/Launcher;->n(Lcom/android/launcher3/Launcher;)Landroid/animation/AnimatorSet;

    move-result-object v1

    iget-object v2, p0, Lxj;->Lp:Landroid/animation/AnimatorSet;

    if-eq v1, v2, :cond_0

    .line 3411
    :goto_0
    return-void

    .line 3394
    :cond_0
    iget-object v1, p0, Lxj;->KX:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lxj;->Lj:Landroid/view/View;

    iget-boolean v3, p0, Lxj;->Lk:Z

    invoke-static {v1, v2, v3, v0}, Lcom/android/launcher3/Launcher;->b(Lcom/android/launcher3/Launcher;Landroid/view/View;ZZ)V

    .line 3395
    iget-object v1, p0, Lxj;->KX:Lcom/android/launcher3/Launcher;

    iget-object v2, p0, Lxj;->Ll:Lcom/android/launcher3/AppsCustomizeTabHost;

    iget-boolean v3, p0, Lxj;->Lk:Z

    invoke-static {v1, v2, v3, v0}, Lcom/android/launcher3/Launcher;->b(Lcom/android/launcher3/Launcher;Landroid/view/View;ZZ)V

    .line 3397
    iget-object v1, p0, Lxj;->Lm:Landroid/view/View;

    iget v2, p0, Lxj;->Fw:F

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 3398
    invoke-static {}, Ladp;->km()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v0

    .line 3399
    :goto_1
    iget-object v0, p0, Lxj;->Lq:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3400
    iget-object v0, p0, Lxj;->Lq:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 3401
    if-eqz v0, :cond_2

    .line 3402
    const/4 v2, 0x1

    .line 3403
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_1

    .line 3404
    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v2

    .line 3406
    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->buildLayer()V

    .line 3399
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 3410
    :cond_3
    iget-object v0, p0, Lxj;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->n(Lcom/android/launcher3/Launcher;)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0
.end method
