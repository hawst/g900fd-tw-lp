.class public final Ljhf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile elA:[Ljhf;


# instance fields
.field private aeB:Ljbp;

.field private aez:I

.field private agv:I

.field private dRq:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljhf;->aez:I

    const/4 v0, 0x1

    iput v0, p0, Ljhf;->agv:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljhf;->dRq:J

    iput-object v2, p0, Ljhf;->aeB:Ljbp;

    iput-object v2, p0, Ljhf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljhf;->eCz:I

    return-void
.end method

.method public static blB()[Ljhf;
    .locals 2

    sget-object v0, Ljhf;->elA:[Ljhf;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljhf;->elA:[Ljhf;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljhf;

    sput-object v0, Ljhf;->elA:[Ljhf;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljhf;->elA:[Ljhf;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljhf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljhf;->agv:I

    iget v0, p0, Ljhf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljhf;->dRq:J

    iget v0, p0, Ljhf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhf;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljhf;->aeB:Ljbp;

    if-nez v0, :cond_1

    new-instance v0, Ljbp;

    invoke-direct {v0}, Ljbp;-><init>()V

    iput-object v0, p0, Ljhf;->aeB:Ljbp;

    :cond_1
    iget-object v0, p0, Ljhf;->aeB:Ljbp;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljhf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Ljhf;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_0
    iget v0, p0, Ljhf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget-wide v2, p0, Ljhf;->dRq:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_1
    iget-object v0, p0, Ljhf;->aeB:Ljbp;

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    iget-object v1, p0, Ljhf;->aeB:Ljbp;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bcB()J
    .locals 2

    iget-wide v0, p0, Ljhf;->dRq:J

    return-wide v0
.end method

.method public final blC()Z
    .locals 1

    iget v0, p0, Ljhf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final di(J)Ljhf;
    .locals 1

    iput-wide p1, p0, Ljhf;->dRq:J

    iget v0, p0, Ljhf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljhf;->aez:I

    return-object p0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Ljhf;->agv:I

    return v0
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljhf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    iget v2, p0, Ljhf;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljhf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-wide v2, p0, Ljhf;->dRq:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljhf;->aeB:Ljbp;

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    iget-object v2, p0, Ljhf;->aeB:Ljbp;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    return v0
.end method

.method public final pQ(I)Ljhf;
    .locals 1

    iput p1, p0, Ljhf;->agv:I

    iget v0, p0, Ljhf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhf;->aez:I

    return-object p0
.end method
