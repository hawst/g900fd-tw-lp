.class public final Lbzc;
.super Lbzf;
.source "PG"


# instance fields
.field private synthetic aPo:Lcom/google/android/hotword/service/HotwordService;


# direct methods
.method public constructor <init>(Lcom/google/android/hotword/service/HotwordService;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    invoke-direct {p0}, Lbzf;-><init>()V

    return-void
.end method


# virtual methods
.method public final Cc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->mSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aFc()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Cd()Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->mSettings:Lgdo;

    invoke-interface {v0}, Lgdo;->aFi()Z

    move-result v0

    return v0
.end method

.method public final bV(Z)V
    .locals 3

    .prologue
    .line 126
    const-string v0, "com.google.android.googlequicksearchbox"

    iget-object v1, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    invoke-virtual {v1}, Lcom/google/android/hotword/service/HotwordService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->mSettings:Lgdo;

    invoke-interface {v0, p1}, Lgdo;->fJ(Z)V

    .line 132
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    invoke-virtual {v0, p1}, Lcom/google/android/hotword/service/HotwordService;->fQ(Ljava/lang/String;)V

    .line 93
    if-eqz p2, :cond_1

    .line 94
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->aPl:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-boolean v0, v0, Lcom/google/android/hotword/service/HotwordService;->cq:Z

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->aPm:Lecq;

    invoke-virtual {v0}, Lecq;->start()V

    .line 98
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->aPm:Lecq;

    invoke-virtual {v0, v2}, Lecq;->bq(Z)V

    .line 99
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iput-boolean v2, v0, Lcom/google/android/hotword/service/HotwordService;->cq:Z

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->aPl:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 104
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-boolean v0, v0, Lcom/google/android/hotword/service/HotwordService;->cq:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->aPl:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->aPm:Lecq;

    invoke-virtual {v0, v1}, Lecq;->bq(Z)V

    .line 106
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iget-object v0, v0, Lcom/google/android/hotword/service/HotwordService;->aPm:Lecq;

    invoke-virtual {v0}, Lecq;->stop()V

    .line 107
    iget-object v0, p0, Lbzc;->aPo:Lcom/google/android/hotword/service/HotwordService;

    iput-boolean v1, v0, Lcom/google/android/hotword/service/HotwordService;->cq:Z

    goto :goto_0
.end method
