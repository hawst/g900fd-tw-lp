.class public final Ljol;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ewA:[Ljol;


# instance fields
.field private aez:I

.field private akf:Ljava/lang/String;

.field private ewB:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1438
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1439
    const/4 v0, 0x0

    iput v0, p0, Ljol;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljol;->akf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljol;->ewB:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljol;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljol;->eCz:I

    .line 1440
    return-void
.end method

.method public static bro()[Ljol;
    .locals 2

    .prologue
    .line 1381
    sget-object v0, Ljol;->ewA:[Ljol;

    if-nez v0, :cond_1

    .line 1382
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1384
    :try_start_0
    sget-object v0, Ljol;->ewA:[Ljol;

    if-nez v0, :cond_0

    .line 1385
    const/4 v0, 0x0

    new-array v0, v0, [Ljol;

    sput-object v0, Ljol;->ewA:[Ljol;

    .line 1387
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1389
    :cond_1
    sget-object v0, Ljol;->ewA:[Ljol;

    return-object v0

    .line 1387
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 1375
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljol;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljol;->akf:Ljava/lang/String;

    iget v0, p0, Ljol;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljol;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljol;->ewB:Ljava/lang/String;

    iget v0, p0, Ljol;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljol;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1454
    iget v0, p0, Ljol;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1455
    const/4 v0, 0x1

    iget-object v1, p0, Ljol;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1457
    :cond_0
    iget v0, p0, Ljol;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1458
    const/4 v0, 0x2

    iget-object v1, p0, Ljol;->ewB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1460
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1461
    return-void
.end method

.method public final brp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1419
    iget-object v0, p0, Ljol;->ewB:Ljava/lang/String;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1397
    iget-object v0, p0, Ljol;->akf:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1465
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1466
    iget v1, p0, Ljol;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1467
    const/4 v1, 0x1

    iget-object v2, p0, Ljol;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1470
    :cond_0
    iget v1, p0, Ljol;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1471
    const/4 v1, 0x2

    iget-object v2, p0, Ljol;->ewB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1474
    :cond_1
    return v0
.end method

.method public final rn()Z
    .locals 1

    .prologue
    .line 1408
    iget v0, p0, Ljol;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final xI(Ljava/lang/String;)Ljol;
    .locals 1

    .prologue
    .line 1400
    if-nez p1, :cond_0

    .line 1401
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1403
    :cond_0
    iput-object p1, p0, Ljol;->akf:Ljava/lang/String;

    .line 1404
    iget v0, p0, Ljol;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljol;->aez:I

    .line 1405
    return-object p0
.end method

.method public final xJ(Ljava/lang/String;)Ljol;
    .locals 1

    .prologue
    .line 1422
    if-nez p1, :cond_0

    .line 1423
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1425
    :cond_0
    iput-object p1, p0, Ljol;->ewB:Ljava/lang/String;

    .line 1426
    iget v0, p0, Ljol;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljol;->aez:I

    .line 1427
    return-object p0
.end method
