.class public final Lgjm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static cMG:I


# instance fields
.field private final cMH:Ljava/lang/String;

.field private final cMI:Ljava/lang/String;

.field private cMJ:Lgjn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x4

    sput v0, Lgjm;->cMG:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lgjm;->cMH:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lgjm;->cMI:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method public final aGx()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 56
    new-instance v1, Lgjn;

    invoke-direct {v1}, Lgjn;-><init>()V

    iput-object v1, p0, Lgjm;->cMJ:Lgjn;

    .line 59
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lgjm;->cMH:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lgjo;->k(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, p0, Lgjm;->cMJ:Lgjn;

    iget-object v2, p0, Lgjm;->cMH:Ljava/lang/String;

    iget-object v3, p0, Lgjm;->cMI:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lgjn;->bf(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 69
    :goto_0
    return v0

    .line 63
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lgjm;->cMH:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Leoo;->e(Ljava/io/File;)[B

    move-result-object v1

    .line 64
    iget-object v2, p0, Lgjm;->cMJ:Lgjn;

    iget-object v3, p0, Lgjm;->cMI:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lgjn;->d([BLjava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 67
    :catch_0
    move-exception v1

    .line 68
    const-string v2, "Greco3GrammarCompiler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "I/O Exception reading binary config file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Leor;->c(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final delete()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lgjm;->cMJ:Lgjn;

    invoke-virtual {v0}, Lgjn;->delete()V

    .line 53
    return-void
.end method

.method public final f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 84
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 87
    iget-object v4, p0, Lgjm;->cMJ:Lgjn;

    invoke-virtual {v4, p3}, Lgjn;->yY(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 90
    const-string v4, "Greco3GrammarCompiler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error reading cache file: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 94
    :cond_0
    iget-object v4, p0, Lgjm;->cMJ:Lgjn;

    invoke-virtual {v4, p1}, Lgjn;->yZ(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 120
    :cond_1
    :goto_0
    return v0

    .line 99
    :cond_2
    iget-object v4, p0, Lgjm;->cMJ:Lgjn;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/grammar_clg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/grammar_symbols"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lgjn;->bg(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 104
    iget-object v4, p0, Lgjm;->cMJ:Lgjn;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/semantic_fst"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/semantic_symbols"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lgjn;->bh(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 111
    iget-object v4, p0, Lgjm;->cMJ:Lgjn;

    invoke-virtual {v4, p3, v1}, Lgjn;->G(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    .line 113
    const-string v2, "Greco3GrammarCompiler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error writing cache to: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 114
    goto :goto_0

    .line 117
    :cond_3
    const-string v4, "Greco3GrammarCompiler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Compilation complete, time = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v2, v6, v2

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " s"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v4, v2, v0}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v0, v1

    .line 120
    goto/16 :goto_0
.end method
