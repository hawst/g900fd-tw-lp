.class public final Ldie;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lemy;


# instance fields
.field private final aVQ:Ldgc;

.field private final byF:Ljava/util/Collection;

.field private final byG:Lemy;

.field private final byd:Z

.field private final mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method public constructor <init>(Ldgc;Ljava/util/Collection;Lcom/google/android/shared/search/Query;ZLemy;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    iput-object p1, p0, Ldie;->aVQ:Ldgc;

    .line 173
    iput-object p2, p0, Ldie;->byF:Ljava/util/Collection;

    .line 174
    iput-object p3, p0, Ldie;->mQuery:Lcom/google/android/shared/search/Query;

    .line 175
    iput-boolean p4, p0, Ldie;->byd:Z

    .line 176
    iput-object p5, p0, Ldie;->byG:Lemy;

    .line 177
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/appdatasearch/SearchResults;)Ldid;
    .locals 6

    .prologue
    .line 188
    if-nez p1, :cond_0

    .line 189
    invoke-static {}, Ldib;->acd()Ldid;

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    .line 191
    :cond_0
    iget-object v0, p0, Ldie;->byF:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 194
    iget-boolean v0, p0, Ldie;->byd:Z

    if-eqz v0, :cond_2

    .line 195
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/SearchResults;->xf()Lbci;

    move-result-object v0

    .line 196
    iget-object v1, p0, Ldie;->aVQ:Ldgc;

    iget-object v3, p0, Ldie;->byF:Ljava/util/Collection;

    iget-object v4, p0, Ldie;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v1, v3, v4, v0}, Ldhz;->a(Ldgc;Ljava/util/Collection;Lcom/google/android/shared/search/Query;Lbci;)Ldhz;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    :cond_1
    new-instance v0, Ldid;

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/SearchResults;->wX()I

    move-result v1

    invoke-direct {v0, v1, v2}, Ldid;-><init>(ILjava/util/List;)V

    goto :goto_0

    .line 199
    :cond_2
    iget-object v0, p0, Ldie;->byF:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldht;

    .line 200
    invoke-virtual {v0}, Ldht;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 201
    invoke-virtual {v0}, Ldht;->acb()Ljava/lang/String;

    move-result-object v4

    .line 202
    if-nez v4, :cond_3

    invoke-virtual {p1, v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->eP(Ljava/lang/String;)Lbci;

    move-result-object v1

    .line 206
    :goto_2
    iget-object v4, p0, Ldie;->aVQ:Ldgc;

    iget-object v5, p0, Ldie;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-static {v4, v0, v5, v1}, Ldhz;->a(Ldgc;Ldht;Lcom/google/android/shared/search/Query;Lbci;)Ldhz;

    move-result-object v0

    .line 208
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 202
    :cond_3
    invoke-virtual {p1, v1, v4}, Lcom/google/android/gms/appdatasearch/SearchResults;->m(Ljava/lang/String;Ljava/lang/String;)Lbci;

    move-result-object v1

    goto :goto_2
.end method

.method public final synthetic aj(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 161
    check-cast p1, Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, p0, Ldie;->byG:Lemy;

    invoke-virtual {p0, p1}, Ldie;->a(Lcom/google/android/gms/appdatasearch/SearchResults;)Ldid;

    move-result-object v1

    invoke-interface {v0, v1}, Lemy;->aj(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method
