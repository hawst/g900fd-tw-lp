.class public final Ledn;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# instance fields
.field private final GS:Landroid/view/LayoutInflater;

.field bVO:Ledp;


# direct methods
.method public constructor <init>(Lcom/google/android/search/shared/ui/actions/AppSelectorView;Landroid/content/Context;ILjava/util/List;Landroid/view/LayoutInflater;Ledp;)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 92
    iput-object p5, p0, Ledn;->GS:Landroid/view/LayoutInflater;

    .line 93
    iput-object p6, p0, Ledn;->bVO:Ledp;

    .line 94
    return-void
.end method

.method private a(ILandroid/view/View;Z)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 97
    invoke-virtual {p0, p1}, Ledn;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    .line 100
    if-eqz p2, :cond_0

    .line 103
    check-cast p2, Landroid/widget/LinearLayout;

    .line 104
    invoke-virtual {p2, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 105
    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 112
    :goto_0
    invoke-virtual {p0}, Ledn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/shared/util/App;->ar(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 113
    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    if-eqz p3, :cond_1

    .line 115
    new-instance v1, Ledo;

    invoke-direct {v1, p0, v0}, Ledo;-><init>(Ledn;Lcom/google/android/shared/util/App;)V

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 127
    :goto_1
    return-object p2

    .line 107
    :cond_0
    iget-object v1, p0, Ledn;->GS:Landroid/view/LayoutInflater;

    const v2, 0x7f0400f9

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 109
    const v2, 0x7f1102d6

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 110
    const v3, 0x7f1102d7

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object p2, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_0

    .line 125
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ledn;->a(ILandroid/view/View;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ledn;->a(ILandroid/view/View;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
