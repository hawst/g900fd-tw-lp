.class final Lhcl;
.super Lenp;
.source "PG"


# instance fields
.field private synthetic dcy:Lhck;

.field private final mAccount:Landroid/accounts/Account;

.field private final mNowOptInHelper:Leux;


# direct methods
.method public constructor <init>(Lhck;Lerk;Leux;Landroid/accounts/Account;)V
    .locals 2

    .prologue
    .line 436
    iput-object p1, p0, Lhcl;->dcy:Lhck;

    .line 437
    const-string v0, "OptIn"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, p2, v1}, Lenp;-><init>(Ljava/lang/String;Lerk;[I)V

    .line 438
    iput-object p3, p0, Lhcl;->mNowOptInHelper:Leux;

    .line 439
    iput-object p4, p0, Lhcl;->mAccount:Landroid/accounts/Account;

    .line 440
    return-void

    .line 437
    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lhcl;->mNowOptInHelper:Leux;

    iget-object v1, p0, Lhcl;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Leux;->D(Landroid/accounts/Account;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 431
    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lhcl;->dcy:Lhck;

    invoke-static {v0}, Lhck;->a(Lhck;)Lhcl;

    move-result-object v0

    if-ne v0, p0, :cond_1

    iget-object v0, p0, Lhcl;->dcy:Lhck;

    invoke-static {v0}, Lhck;->b(Lhck;)Lhcd;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhcl;->dcy:Lhck;

    invoke-static {v0}, Lhck;->b(Lhck;)Lhcd;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lhcd;->a(Lhcd;Z)V

    :cond_0
    iget-object v0, p0, Lhcl;->dcy:Lhck;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhck;->a(Lhck;Lhcl;)Lhcl;

    :cond_1
    return-void
.end method
