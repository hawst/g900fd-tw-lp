.class public final Lyr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static LZ:Ljava/util/WeakHashMap;

.field private static Ma:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lyr;->LZ:Ljava/util/WeakHashMap;

    .line 33
    new-instance v0, Lys;

    invoke-direct {v0}, Lys;-><init>()V

    sput-object v0, Lyr;->Ma:Landroid/animation/Animator$AnimatorListener;

    return-void
.end method

.method public static varargs a(Landroid/view/View;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 103
    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 104
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 106
    sget-object v1, Lyr;->Ma:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 107
    new-instance v1, Luv;

    invoke-direct {v1, v0, p0}, Luv;-><init>(Landroid/animation/ValueAnimator;Landroid/view/View;)V

    .line 108
    return-object v0
.end method

.method public static varargs a(Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 114
    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 115
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 116
    sget-object v1, Lyr;->Ma:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 117
    new-instance v1, Luv;

    invoke-direct {v1, v0, p0}, Luv;-><init>(Landroid/animation/ValueAnimator;Landroid/view/View;)V

    .line 118
    return-object v0
.end method

.method public static varargs a(Ljava/lang/Object;Landroid/view/View;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 123
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 124
    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 125
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 126
    sget-object v1, Lyr;->Ma:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 127
    new-instance v1, Luv;

    invoke-direct {v1, v0, p1}, Luv;-><init>(Landroid/animation/ValueAnimator;Landroid/view/View;)V

    .line 128
    return-object v0
.end method

.method public static a(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lyr;->Ma:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p0, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 52
    return-void
.end method

.method public static varargs c([F)Landroid/animation/ValueAnimator;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 96
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 97
    sget-object v1, Lyr;->Ma:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 98
    return-object v0
.end method

.method public static createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;
    .locals 3

    .prologue
    .line 133
    invoke-static {p0, p1, p2, p3, p4}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v1

    .line 135
    instance-of v0, v1, Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 136
    new-instance v2, Luv;

    move-object v0, v1

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-direct {v2, v0, p0}, Luv;-><init>(Landroid/animation/ValueAnimator;Landroid/view/View;)V

    .line 138
    :cond_0
    return-object v1
.end method

.method public static ih()V
    .locals 3

    .prologue
    .line 79
    new-instance v0, Ljava/util/HashSet;

    sget-object v1, Lyr;->LZ:Ljava/util/WeakHashMap;

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 80
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 81
    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 84
    :cond_0
    sget-object v2, Lyr;->LZ:Ljava/util/WeakHashMap;

    invoke-virtual {v2, v0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 86
    :cond_1
    return-void
.end method

.method public static ii()Landroid/animation/AnimatorSet;
    .locals 2

    .prologue
    .line 89
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 90
    sget-object v1, Lyr;->Ma:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 91
    return-object v0
.end method
