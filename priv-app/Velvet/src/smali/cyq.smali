.class public final Lcyq;
.super Lcyc;
.source "PG"


# instance fields
.field private final mConfig:Lcjs;

.field private final mUrlHelper:Lcpn;


# direct methods
.method public constructor <init>(Lcke;Lcjs;Lcpn;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 23
    iput-object p2, p0, Lcyq;->mConfig:Lcjs;

    .line 24
    iput-object p3, p0, Lcyq;->mUrlHelper:Lcpn;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Landroid/preference/Preference;)V
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Lcyq;->mUrlHelper:Lcpn;

    iget-object v1, p0, Lcyq;->mConfig:Lcjs;

    invoke-virtual {v1}, Lcjs;->Lf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcpn;->hE(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 30
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 31
    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 32
    return-void
.end method
