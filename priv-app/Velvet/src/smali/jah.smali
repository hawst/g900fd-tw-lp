.class public final Ljah;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dWx:J

.field private dWy:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43376
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 43377
    iput v2, p0, Ljah;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljah;->dWx:J

    iput v2, p0, Ljah;->dWy:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljah;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljah;->eCz:I

    .line 43378
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 43319
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljah;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljah;->dWx:J

    iget v0, p0, Ljah;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljah;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljah;->dWy:I

    iget v0, p0, Ljah;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljah;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 43392
    iget v0, p0, Ljah;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 43393
    const/4 v0, 0x1

    iget-wide v2, p0, Ljah;->dWx:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 43395
    :cond_0
    iget v0, p0, Ljah;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 43396
    const/4 v0, 0x2

    iget v1, p0, Ljah;->dWy:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 43398
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 43399
    return-void
.end method

.method public final bdU()J
    .locals 2

    .prologue
    .line 43341
    iget-wide v0, p0, Ljah;->dWx:J

    return-wide v0
.end method

.method public final bdV()I
    .locals 1

    .prologue
    .line 43360
    iget v0, p0, Ljah;->dWy:I

    return v0
.end method

.method public final cz(J)Ljah;
    .locals 1

    .prologue
    .line 43344
    iput-wide p1, p0, Ljah;->dWx:J

    .line 43345
    iget v0, p0, Ljah;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljah;->aez:I

    .line 43346
    return-object p0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 43403
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 43404
    iget v1, p0, Ljah;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 43405
    const/4 v1, 0x1

    iget-wide v2, p0, Ljah;->dWx:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 43408
    :cond_0
    iget v1, p0, Ljah;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 43409
    const/4 v1, 0x2

    iget v2, p0, Ljah;->dWy:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43412
    :cond_1
    return v0
.end method

.method public final of(I)Ljah;
    .locals 1

    .prologue
    .line 43363
    const/16 v0, 0x91

    iput v0, p0, Ljah;->dWy:I

    .line 43364
    iget v0, p0, Ljah;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljah;->aez:I

    .line 43365
    return-object p0
.end method
