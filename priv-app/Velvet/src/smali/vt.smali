.class final Lvt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic It:I

.field private synthetic Iu:Lvs;


# direct methods
.method constructor <init>(Lvs;I)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lvt;->Iu:Lvs;

    iput p2, p0, Lvt;->It:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 239
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 240
    iget-object v1, p0, Lvt;->Iu:Lvs;

    const v2, 0x3e99999a    # 0.3f

    mul-float/2addr v2, v0

    add-float/2addr v2, v4

    iget v3, p0, Lvt;->It:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iput v2, v1, Lvs;->Il:F

    .line 241
    iget-object v1, p0, Lvt;->Iu:Lvs;

    const v2, 0x3e19999a    # 0.15f

    mul-float/2addr v0, v2

    add-float/2addr v0, v4

    iget v2, p0, Lvt;->It:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iput v0, v1, Lvs;->Im:F

    .line 242
    iget-object v0, p0, Lvt;->Iu:Lvs;

    invoke-static {v0}, Lvs;->c(Lvs;)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lvt;->Iu:Lvs;

    invoke-static {v0}, Lvs;->c(Lvs;)Lcom/android/launcher3/CellLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/CellLayout;->invalidate()V

    .line 245
    :cond_0
    return-void
.end method
