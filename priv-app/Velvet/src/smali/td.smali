.class public final Ltd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private synthetic AQ:Lcom/android/launcher3/CellLayout;

.field public BG:Ljava/util/ArrayList;

.field public BH:Lsz;

.field private BI:Landroid/graphics/Rect;

.field public BJ:[I

.field public BK:[I

.field public BL:[I

.field public BM:[I

.field public BN:Z

.field public BO:Z

.field public BP:Z

.field public BQ:Z

.field private BR:Z

.field public BS:Lte;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/CellLayout;Ljava/util/ArrayList;Lsz;)V
    .locals 1

    .prologue
    .line 1485
    iput-object p1, p0, Ltd;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1476
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ltd;->BI:Landroid/graphics/Rect;

    .line 1478
    iget-object v0, p0, Ltd;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->d(Lcom/android/launcher3/CellLayout;)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Ltd;->BJ:[I

    .line 1479
    iget-object v0, p0, Ltd;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->d(Lcom/android/launcher3/CellLayout;)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Ltd;->BK:[I

    .line 1480
    iget-object v0, p0, Ltd;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->e(Lcom/android/launcher3/CellLayout;)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Ltd;->BL:[I

    .line 1481
    iget-object v0, p0, Ltd;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->e(Lcom/android/launcher3/CellLayout;)I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Ltd;->BM:[I

    .line 1671
    new-instance v0, Lte;

    invoke-direct {v0, p0}, Lte;-><init>(Ltd;)V

    iput-object v0, p0, Ltd;->BS:Lte;

    .line 1486
    invoke-virtual {p2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Ltd;->BG:Ljava/util/ArrayList;

    .line 1487
    iput-object p3, p0, Ltd;->BH:Lsz;

    .line 1488
    invoke-virtual {p0}, Ltd;->fm()V

    .line 1489
    return-void
.end method


# virtual methods
.method public final a(I[I)V
    .locals 7

    .prologue
    .line 1508
    iget-object v0, p0, Ltd;->BG:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1509
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_7

    .line 1510
    iget-object v0, p0, Ltd;->BH:Lsz;

    iget-object v0, v0, Lsz;->Be:Ljava/util/HashMap;

    iget-object v1, p0, Ltd;->BG:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 1511
    packed-switch p1, :pswitch_data_0

    .line 1509
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1513
    :pswitch_0
    iget v4, v0, Lsx;->x:I

    .line 1514
    iget v1, v0, Lsx;->y:I

    :goto_1
    iget v5, v0, Lsx;->y:I

    iget v6, v0, Lsx;->AZ:I

    add-int/2addr v5, v6

    if-ge v1, v5, :cond_0

    .line 1515
    aget v5, p2, v1

    if-lt v4, v5, :cond_1

    aget v5, p2, v1

    if-gez v5, :cond_2

    .line 1516
    :cond_1
    aput v4, p2, v1

    .line 1514
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1521
    :pswitch_1
    iget v1, v0, Lsx;->x:I

    iget v4, v0, Lsx;->AY:I

    add-int/2addr v4, v1

    .line 1522
    iget v1, v0, Lsx;->y:I

    :goto_2
    iget v5, v0, Lsx;->y:I

    iget v6, v0, Lsx;->AZ:I

    add-int/2addr v5, v6

    if-ge v1, v5, :cond_0

    .line 1523
    aget v5, p2, v1

    if-le v4, v5, :cond_3

    .line 1524
    aput v4, p2, v1

    .line 1522
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1529
    :pswitch_2
    iget v4, v0, Lsx;->y:I

    .line 1530
    iget v1, v0, Lsx;->x:I

    :goto_3
    iget v5, v0, Lsx;->x:I

    iget v6, v0, Lsx;->AY:I

    add-int/2addr v5, v6

    if-ge v1, v5, :cond_0

    .line 1531
    aget v5, p2, v1

    if-lt v4, v5, :cond_4

    aget v5, p2, v1

    if-gez v5, :cond_5

    .line 1532
    :cond_4
    aput v4, p2, v1

    .line 1530
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1537
    :pswitch_3
    iget v1, v0, Lsx;->y:I

    iget v4, v0, Lsx;->AZ:I

    add-int/2addr v4, v1

    .line 1538
    iget v1, v0, Lsx;->x:I

    :goto_4
    iget v5, v0, Lsx;->x:I

    iget v6, v0, Lsx;->AY:I

    add-int/2addr v5, v6

    if-ge v1, v5, :cond_0

    .line 1539
    aget v5, p2, v1

    if-le v4, v5, :cond_6

    .line 1540
    aput v4, p2, v1

    .line 1538
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1546
    :cond_7
    return-void

    .line 1511
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final fm()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 1492
    move v0, v1

    :goto_0
    iget-object v2, p0, Ltd;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v2}, Lcom/android/launcher3/CellLayout;->e(Lcom/android/launcher3/CellLayout;)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1493
    iget-object v2, p0, Ltd;->BL:[I

    aput v4, v2, v0

    .line 1494
    iget-object v2, p0, Ltd;->BM:[I

    aput v4, v2, v0

    .line 1492
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1496
    :cond_0
    :goto_1
    iget-object v0, p0, Ltd;->AQ:Lcom/android/launcher3/CellLayout;

    invoke-static {v0}, Lcom/android/launcher3/CellLayout;->d(Lcom/android/launcher3/CellLayout;)I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1497
    iget-object v0, p0, Ltd;->BJ:[I

    aput v4, v0, v1

    .line 1498
    iget-object v0, p0, Ltd;->BK:[I

    aput v4, v0, v1

    .line 1496
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1500
    :cond_1
    iput-boolean v3, p0, Ltd;->BN:Z

    .line 1501
    iput-boolean v3, p0, Ltd;->BO:Z

    .line 1502
    iput-boolean v3, p0, Ltd;->BQ:Z

    .line 1503
    iput-boolean v3, p0, Ltd;->BP:Z

    .line 1504
    iput-boolean v3, p0, Ltd;->BR:Z

    .line 1505
    return-void
.end method

.method public final fn()Landroid/graphics/Rect;
    .locals 8

    .prologue
    .line 1614
    iget-boolean v0, p0, Ltd;->BR:Z

    if-eqz v0, :cond_1

    .line 1615
    const/4 v0, 0x1

    .line 1616
    iget-object v1, p0, Ltd;->BG:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1617
    iget-object v3, p0, Ltd;->BH:Lsz;

    iget-object v3, v3, Lsz;->Be:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsx;

    .line 1618
    if-eqz v1, :cond_0

    .line 1619
    iget-object v1, p0, Ltd;->BI:Landroid/graphics/Rect;

    iget v3, v0, Lsx;->x:I

    iget v4, v0, Lsx;->y:I

    iget v5, v0, Lsx;->x:I

    iget v6, v0, Lsx;->AY:I

    add-int/2addr v5, v6

    iget v6, v0, Lsx;->y:I

    iget v0, v0, Lsx;->AZ:I

    add-int/2addr v0, v6

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1620
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1622
    :cond_0
    iget-object v3, p0, Ltd;->BI:Landroid/graphics/Rect;

    iget v4, v0, Lsx;->x:I

    iget v5, v0, Lsx;->y:I

    iget v6, v0, Lsx;->x:I

    iget v7, v0, Lsx;->AY:I

    add-int/2addr v6, v7

    iget v7, v0, Lsx;->y:I

    iget v0, v0, Lsx;->AZ:I

    add-int/2addr v0, v7

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/graphics/Rect;->union(IIII)V

    goto :goto_0

    .line 1626
    :cond_1
    iget-object v0, p0, Ltd;->BI:Landroid/graphics/Rect;

    return-object v0
.end method
