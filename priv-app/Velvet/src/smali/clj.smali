.class final Lclj;
.super Lepm;
.source "PG"


# instance fields
.field private synthetic baU:Lclh;

.field private final baV:Livy;


# direct methods
.method constructor <init>(Lclh;Livy;)V
    .locals 4

    .prologue
    .line 114
    iput-object p1, p0, Lclj;->baU:Lclh;

    .line 115
    const-string v0, "get-displays"

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/4 v3, 0x2

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 116
    iput-object p2, p0, Lclj;->baV:Livy;

    .line 117
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 121
    iget-object v1, p0, Lclj;->baV:Livy;

    iget-object v0, p0, Lclj;->baU:Lclh;

    iget-object v2, v0, Lclh;->baR:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "RecentContextApiClient"

    const-string v2, "getDisplaysBlocking() called after disconnect()"

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Livy;->bB(Ljava/lang/Object;)Z

    .line 122
    return-void

    .line 121
    :cond_0
    iget-object v2, v0, Lclh;->auh:Lbhi;

    invoke-interface {v2}, Lbhi;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lclh;->auh:Lbhi;

    invoke-interface {v2}, Lbhi;->connect()V

    :cond_1
    sget-object v2, Lbbr;->auw:Lbcs;

    iget-object v0, v0, Lclh;->auh:Lbhi;

    invoke-interface {v2, v0}, Lbcs;->a(Lbhi;)Lbhm;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v4}, Lbhm;->a(JLjava/util/concurrent/TimeUnit;)Lbho;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;->wZ()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->yk()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "RecentContextApiClient"

    const-string v3, "Failed to get response."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;->wZ()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->pB()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;->ave:Ljava/util/List;

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v0

    goto :goto_0
.end method
