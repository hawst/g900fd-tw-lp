.class public final Libv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bcO:[Ljava/lang/String;

.field private static final dxw:Lifj;

.field private static final dxx:[Ljava/lang/String;

.field private static final dxy:[Ljava/lang/String;


# instance fields
.field private final dxA:Landroid/util/LongSparseArray;

.field private final dxB:Ljava/util/Map;

.field private final dxz:Z

.field private final mContactRetriever:Lghd;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v2

    sput-object v0, Libv;->bcO:[Ljava/lang/String;

    .line 42
    const-string v0, ","

    invoke-static {v0}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v0

    sput-object v0, Libv;->dxw:Lifj;

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "contact_id"

    aput-object v1, v0, v2

    const-string v1, "data2"

    aput-object v1, v0, v3

    sput-object v0, Libv;->dxx:[Ljava/lang/String;

    .line 49
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "vnd.android.cursor.item/name"

    aput-object v1, v0, v2

    sput-object v0, Libv;->dxy:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lghd;Z)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Libv;->dxA:Landroid/util/LongSparseArray;

    .line 57
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Libv;->dxB:Ljava/util/Map;

    .line 60
    iput-object p1, p0, Libv;->mContactRetriever:Lghd;

    .line 61
    iput-boolean p2, p0, Libv;->dxz:Z

    .line 62
    return-void
.end method

.method private V(Landroid/net/Uri;)Ljava/util/List;
    .locals 12
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/16 v3, 0xa

    const/4 v8, 0x0

    .line 83
    invoke-static {}, Lenu;->auQ()V

    .line 84
    invoke-static {v3}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v9

    .line 86
    new-instance v10, Libw;

    invoke-direct {v10, p0}, Libw;-><init>(Libv;)V

    .line 87
    iget-object v1, p0, Libv;->mContactRetriever:Lghd;

    sget-object v2, Libv;->bcO:[Ljava/lang/String;

    invoke-virtual {v1, p1, v3, v2, v10}, Lghd;->a(Landroid/net/Uri;I[Ljava/lang/String;Lhgm;)V

    .line 89
    iget-object v1, v10, Libw;->dxC:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 91
    iget-object v11, p0, Libv;->dxA:Landroid/util/LongSparseArray;

    monitor-enter v11

    .line 92
    :try_start_0
    iget-object v1, v10, Libw;->dxC:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-static {v1}, Liqs;->z(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v1

    move v0, v8

    :goto_1
    iget-object v2, p0, Libv;->dxA:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Libv;->dxA:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v8

    goto :goto_0

    :cond_1
    const-string v0, "contact_id IN (%s) AND mimetype = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v4, Libv;->dxw:Lifj;

    invoke-virtual {v4, v1}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Libx;

    invoke-direct {v7, p0}, Libx;-><init>(Libv;)V

    iget-object v0, p0, Libv;->mContactRetriever:Lghd;

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/16 v2, 0xa

    sget-object v3, Libv;->dxx:[Ljava/lang/String;

    sget-object v5, Libv;->dxy:[Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v7}, Lghd;->a(Landroid/net/Uri;I[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lhgm;)V

    move v0, v8

    :goto_2
    iget-object v1, v7, Libx;->dxD:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Libv;->dxA:Landroid/util/LongSparseArray;

    iget-object v2, v7, Libx;->dxD:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    iget-object v4, v7, Libx;->dxD:Landroid/util/LongSparseArray;

    invoke-virtual {v4, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 94
    :cond_2
    iget-object v0, v10, Libw;->dxC:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 95
    iget-object v0, p0, Libv;->dxA:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    move-result v0

    if-ltz v0, :cond_3

    .line 97
    new-instance v4, Lcom/google/android/search/shared/actions/utils/ExampleContact;

    iget-object v0, p0, Libv;->dxA:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v2, v3, v0}, Lcom/google/android/search/shared/actions/utils/ExampleContact;-><init>(JLjava/lang/String;)V

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0

    :cond_4
    :try_start_1
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    :cond_5
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-boolean v0, p0, Libv;->dxz:Z

    if-eqz v0, :cond_6

    .line 105
    invoke-static {v9}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 108
    :cond_6
    return-object v9
.end method


# virtual methods
.method public final U(Landroid/net/Uri;)Ljava/util/List;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Libv;->dxB:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Libv;->dxB:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 74
    :goto_0
    return-object v0

    .line 72
    :cond_0
    invoke-direct {p0, p1}, Libv;->V(Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    .line 73
    iget-object v1, p0, Libv;->dxB:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
