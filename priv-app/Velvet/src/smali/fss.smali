.class public final Lfss;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mClock:Lemp;

.field private final mPhotoWithAttributionDecorator:Lgbd;


# direct methods
.method public constructor <init>(Lemp;Lgbd;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lfss;->mClock:Lemp;

    .line 32
    iput-object p2, p0, Lfss;->mPhotoWithAttributionDecorator:Lgbd;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lizj;Ljef;)Lanh;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 37
    new-instance v0, Lans;

    invoke-direct {v0}, Lans;-><init>()V

    .line 38
    new-instance v1, Lanj;

    invoke-direct {v1}, Lanj;-><init>()V

    .line 39
    iput-object v1, v0, Lans;->aiw:Lanj;

    .line 41
    new-instance v2, Lanh;

    invoke-direct {v2}, Lanh;-><init>()V

    .line 42
    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lanh;->cm(I)Lanh;

    .line 43
    iput-object v0, v2, Lanh;->agG:Lans;

    .line 44
    iput-object p2, v2, Lanh;->ahu:Lizj;

    .line 46
    invoke-virtual {p3}, Ljef;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lanj;->ao(Ljava/lang/String;)Lanj;

    .line 48
    invoke-virtual {p3}, Ljef;->pX()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 49
    invoke-virtual {p3}, Ljef;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lans;->aU(Ljava/lang/String;)Lans;

    .line 54
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    invoke-virtual {p3}, Ljef;->bir()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 56
    const v3, 0x7f0a048a

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {p3}, Ljef;->biq()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_1
    invoke-virtual {p3}, Ljef;->bip()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 60
    invoke-virtual {p3}, Ljef;->bio()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_2
    invoke-virtual {p3}, Ljef;->bao()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 63
    iget-object v3, p0, Lfss;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p3}, Ljef;->ban()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 65
    invoke-static {p1, v4, v5, v8}, Lesi;->b(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    :cond_3
    invoke-virtual {p3}, Ljef;->bit()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 69
    iget-object v3, p0, Lfss;->mClock:Lemp;

    invoke-interface {v3}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p3}, Ljef;->bis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 71
    const v3, 0x7f0a048b

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {p1, v4, v5, v8}, Lesi;->a(Landroid/content/Context;JZ)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v9

    invoke-virtual {p1, v3, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 77
    const-string v3, " \u00b7 "

    invoke-static {v3, v0}, Lgab;->e(Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lanj;->aq(Ljava/lang/String;)Lanj;

    .line 81
    :cond_5
    invoke-virtual {p3}, Ljef;->pf()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 82
    invoke-virtual {p3}, Ljef;->oo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lanj;->ar(Ljava/lang/String;)Lanj;

    .line 85
    :cond_6
    iget-object v0, p3, Ljef;->aiX:Ljcn;

    if-eqz v0, :cond_7

    .line 87
    iget-object v0, p0, Lfss;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v3, p3, Ljef;->aiX:Ljcn;

    const v4, 0x7f0d012b

    const v5, 0x7f0d012c

    invoke-virtual {v0, p1, v3, v4, v5}, Lgbd;->b(Landroid/content/Context;Ljcn;II)Laoi;

    move-result-object v0

    .line 93
    iget-object v3, p0, Lfss;->mPhotoWithAttributionDecorator:Lgbd;

    const/16 v3, 0x4c

    iget-object v4, p3, Ljef;->aiX:Ljcn;

    invoke-static {v3, v4, v0}, Lgbd;->a(ILjcn;Laoi;)V

    .line 95
    iget-object v3, p0, Lfss;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v3, p3, Ljef;->aiX:Ljcn;

    invoke-static {v3, v0}, Lgbd;->a(Ljcn;Laoi;)V

    .line 97
    iput-object v0, v1, Lanj;->ahM:Laoi;

    .line 100
    :cond_7
    invoke-virtual {p3}, Ljef;->oX()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 101
    invoke-virtual {v2, v8}, Lanh;->aY(Z)Lanh;

    .line 104
    :cond_8
    invoke-virtual {p3}, Ljef;->qG()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 105
    new-instance v0, Lfsa;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lfsa;-><init>(I)V

    const v1, 0x7f0201ab

    invoke-virtual {v0, v1}, Lfsa;->jB(I)Lfsa;

    move-result-object v0

    invoke-virtual {p3}, Ljef;->getUrl()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lfsa;->aM(Ljava/lang/String;Ljava/lang/String;)Lani;

    move-result-object v0

    iput-object v0, v2, Lanh;->ahs:Lani;

    .line 110
    :cond_9
    return-object v2
.end method
