.class public final Laft;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic LK:J

.field private synthetic Nj:Lwq;

.field private synthetic WN:Lcom/android/launcher3/Workspace;

.field private synthetic WV:Lacw;

.field private synthetic WW:J


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Workspace;Lacw;Lwq;JJ)V
    .locals 0

    .prologue
    .line 3881
    iput-object p1, p0, Laft;->WN:Lcom/android/launcher3/Workspace;

    iput-object p2, p0, Laft;->WV:Lacw;

    iput-object p3, p0, Laft;->Nj:Lwq;

    iput-wide p4, p0, Laft;->WW:J

    iput-wide p6, p0, Laft;->LK:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 3887
    iget-object v0, p0, Laft;->WN:Lcom/android/launcher3/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher3/Workspace;->kI()V

    .line 3891
    iget-object v0, p0, Laft;->WV:Lacw;

    iget v0, v0, Lacw;->Jz:I

    packed-switch v0, :pswitch_data_0

    .line 3904
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown item type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Laft;->WV:Lacw;

    iget v2, v2, Lacw;->Jz:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3893
    :pswitch_1
    const/4 v0, 0x2

    new-array v7, v0, [I

    .line 3894
    const/4 v0, 0x0

    iget-object v1, p0, Laft;->Nj:Lwq;

    iget v1, v1, Lwq;->AY:I

    aput v1, v7, v0

    .line 3895
    const/4 v0, 0x1

    iget-object v1, p0, Laft;->Nj:Lwq;

    iget v1, v1, Lwq;->AZ:I

    aput v1, v7, v0

    .line 3896
    iget-object v0, p0, Laft;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v0}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Workspace;)Lcom/android/launcher3/Launcher;

    move-result-object v0

    iget-object v1, p0, Laft;->WV:Lacw;

    check-cast v1, Lacy;

    iget-wide v2, p0, Laft;->WW:J

    iget-wide v4, p0, Laft;->LK:J

    iget-object v6, p0, Laft;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v6}, Lcom/android/launcher3/Workspace;->p(Lcom/android/launcher3/Workspace;)[I

    move-result-object v6

    invoke-virtual/range {v0 .. v8}, Lcom/android/launcher3/Launcher;->a(Lacy;JJ[I[I[I)V

    .line 3902
    :goto_0
    return-void

    .line 3900
    :pswitch_2
    iget-object v0, p0, Laft;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v0}, Lcom/android/launcher3/Workspace;->a(Lcom/android/launcher3/Workspace;)Lcom/android/launcher3/Launcher;

    move-result-object v1

    iget-object v0, p0, Laft;->WV:Lacw;

    iget-object v2, v0, Lacw;->xr:Landroid/content/ComponentName;

    iget-wide v3, p0, Laft;->WW:J

    iget-wide v5, p0, Laft;->LK:J

    iget-object v0, p0, Laft;->WN:Lcom/android/launcher3/Workspace;

    invoke-static {v0}, Lcom/android/launcher3/Workspace;->p(Lcom/android/launcher3/Workspace;)[I

    move-result-object v7

    invoke-virtual/range {v1 .. v8}, Lcom/android/launcher3/Launcher;->a(Landroid/content/ComponentName;JJ[I[I)V

    goto :goto_0

    .line 3891
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
