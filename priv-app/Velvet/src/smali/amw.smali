.class public final Lamw;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field public afD:[Lamx;

.field public afE:[Lamy;

.field private afF:J

.field public afG:[Lamz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 728
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 729
    const/4 v0, 0x0

    iput v0, p0, Lamw;->aez:I

    invoke-static {}, Lamx;->nO()[Lamx;

    move-result-object v0

    iput-object v0, p0, Lamw;->afD:[Lamx;

    invoke-static {}, Lamy;->nZ()[Lamy;

    move-result-object v0

    iput-object v0, p0, Lamw;->afE:[Lamy;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamw;->afF:J

    invoke-static {}, Lamz;->oe()[Lamz;

    move-result-object v0

    iput-object v0, p0, Lamw;->afG:[Lamz;

    const/4 v0, 0x0

    iput-object v0, p0, Lamw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lamw;->eCz:I

    .line 730
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 681
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lamw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lamw;->afD:[Lamx;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lamx;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lamw;->afD:[Lamx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lamx;

    invoke-direct {v3}, Lamx;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lamw;->afD:[Lamx;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lamx;

    invoke-direct {v3}, Lamx;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lamw;->afD:[Lamx;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lamw;->afE:[Lamy;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lamy;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lamw;->afE:[Lamy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lamy;

    invoke-direct {v3}, Lamy;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lamw;->afE:[Lamy;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lamy;

    invoke-direct {v3}, Lamy;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lamw;->afE:[Lamy;

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lamw;->afF:J

    iget v0, p0, Lamw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamw;->aez:I

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lamw;->afG:[Lamz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lamz;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lamw;->afG:[Lamz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lamz;

    invoke-direct {v3}, Lamz;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lamw;->afG:[Lamz;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lamz;

    invoke-direct {v3}, Lamz;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Lamw;->afG:[Lamz;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 746
    iget-object v0, p0, Lamw;->afD:[Lamx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lamw;->afD:[Lamx;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 747
    :goto_0
    iget-object v2, p0, Lamw;->afD:[Lamx;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 748
    iget-object v2, p0, Lamw;->afD:[Lamx;

    aget-object v2, v2, v0

    .line 749
    if-eqz v2, :cond_0

    .line 750
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 747
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 754
    :cond_1
    iget-object v0, p0, Lamw;->afE:[Lamy;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lamw;->afE:[Lamy;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 755
    :goto_1
    iget-object v2, p0, Lamw;->afE:[Lamy;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 756
    iget-object v2, p0, Lamw;->afE:[Lamy;

    aget-object v2, v2, v0

    .line 757
    if-eqz v2, :cond_2

    .line 758
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 755
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 762
    :cond_3
    iget v0, p0, Lamw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 763
    const/4 v0, 0x3

    iget-wide v2, p0, Lamw;->afF:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 765
    :cond_4
    iget-object v0, p0, Lamw;->afG:[Lamz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lamw;->afG:[Lamz;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 766
    :goto_2
    iget-object v0, p0, Lamw;->afG:[Lamz;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 767
    iget-object v0, p0, Lamw;->afG:[Lamz;

    aget-object v0, v0, v1

    .line 768
    if-eqz v0, :cond_5

    .line 769
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 766
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 773
    :cond_6
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 774
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 778
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 779
    iget-object v2, p0, Lamw;->afD:[Lamx;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lamw;->afD:[Lamx;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 780
    :goto_0
    iget-object v3, p0, Lamw;->afD:[Lamx;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 781
    iget-object v3, p0, Lamw;->afD:[Lamx;

    aget-object v3, v3, v0

    .line 782
    if-eqz v3, :cond_0

    .line 783
    const/4 v4, 0x1

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 780
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 788
    :cond_2
    iget-object v2, p0, Lamw;->afE:[Lamy;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lamw;->afE:[Lamy;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 789
    :goto_1
    iget-object v3, p0, Lamw;->afE:[Lamy;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 790
    iget-object v3, p0, Lamw;->afE:[Lamy;

    aget-object v3, v3, v0

    .line 791
    if-eqz v3, :cond_3

    .line 792
    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 789
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 797
    :cond_5
    iget v2, p0, Lamw;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_6

    .line 798
    const/4 v2, 0x3

    iget-wide v4, p0, Lamw;->afF:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 801
    :cond_6
    iget-object v2, p0, Lamw;->afG:[Lamz;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lamw;->afG:[Lamz;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 802
    :goto_2
    iget-object v2, p0, Lamw;->afG:[Lamz;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 803
    iget-object v2, p0, Lamw;->afG:[Lamz;

    aget-object v2, v2, v1

    .line 804
    if-eqz v2, :cond_7

    .line 805
    const/4 v3, 0x4

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 802
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 810
    :cond_8
    return v0
.end method

.method public final nN()J
    .locals 2

    .prologue
    .line 709
    iget-wide v0, p0, Lamw;->afF:J

    return-wide v0
.end method

.method public final x(J)Lamw;
    .locals 1

    .prologue
    .line 712
    iput-wide p1, p0, Lamw;->afF:J

    .line 713
    iget v0, p0, Lamw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lamw;->aez:I

    .line 714
    return-object p0
.end method
