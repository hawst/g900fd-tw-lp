.class public final Lfml;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final cvU:Ljava/lang/Object;

.field final cvV:Ljava/util/List;

.field private final cvW:Ljava/lang/Object;

.field private cvX:I

.field public final cvY:Lfnh;

.field final cvZ:Lfnh;

.field public final cwa:Lgbo;

.field private final cwb:Ljava/lang/Object;

.field final cwc:Ljava/util/Set;

.field final cwd:Ljava/util/List;

.field cwe:Lfor;

.field cwf:Lfmp;

.field final mAppContext:Landroid/content/Context;

.field public final mTaskRunner:Lerk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lerk;)V
    .locals 19

    .prologue
    .line 113
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cvU:Ljava/lang/Object;

    .line 82
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cvV:Ljava/util/List;

    .line 86
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cvW:Ljava/lang/Object;

    .line 88
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lfml;->cvX:I

    .line 98
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cwb:Ljava/lang/Object;

    .line 100
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cwc:Ljava/util/Set;

    .line 104
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cwd:Ljava/util/List;

    .line 108
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cwe:Lfor;

    .line 110
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cwf:Lfmp;

    .line 114
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lfml;->mAppContext:Landroid/content/Context;

    .line 115
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lfml;->mTaskRunner:Lerk;

    .line 122
    new-instance v10, Leoa;

    const-string v2, "NowRemoteClient"

    const/16 v3, 0xa

    invoke-direct {v10, v2, v3}, Leoa;-><init>(Ljava/lang/String;I)V

    .line 127
    new-instance v3, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-wide/16 v6, 0x3c

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v9, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v9}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v3 .. v10}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 130
    new-instance v11, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v12, 0x0

    const/4 v13, 0x1

    const-wide/16 v14, 0x3c

    sget-object v16, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v17, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct/range {v17 .. v17}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move-object/from16 v18, v10

    invoke-direct/range {v11 .. v18}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 134
    new-instance v4, Lfnh;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfml;->mTaskRunner:Lerk;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/4 v9, 0x1

    move-object v6, v3

    move-object/from16 v8, p0

    invoke-direct/range {v4 .. v9}, Lfnh;-><init>(Lerp;Ljava/util/concurrent/Executor;Landroid/content/res/Resources;Lfml;Z)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lfml;->cvY:Lfnh;

    .line 136
    new-instance v9, Lfnh;

    move-object/from16 v0, p0

    iget-object v10, v0, Lfml;->mTaskRunner:Lerk;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const/4 v14, 0x0

    move-object/from16 v13, p0

    invoke-direct/range {v9 .. v14}, Lfnh;-><init>(Lerp;Ljava/util/concurrent/Executor;Landroid/content/res/Resources;Lfml;Z)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lfml;->cvZ:Lfnh;

    .line 138
    new-instance v2, Lfnk;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lfml;->mTaskRunner:Lerk;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v4, v11, v0}, Lfnk;-><init>(Landroid/content/res/Resources;Lerp;Ljava/util/concurrent/Executor;Lfml;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfml;->cwa:Lgbo;

    .line 140
    return-void
.end method

.method private g(ILandroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1031
    iget-object v1, p0, Lfml;->cwb:Ljava/lang/Object;

    monitor-enter v1

    .line 1032
    :try_start_0
    iget-object v0, p0, Lfml;->cwd:Ljava/util/List;

    new-instance v2, Lfmr;

    invoke-direct {v2, p1, p2}, Lfmr;-><init>(ILandroid/os/Bundle;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1033
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final V(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 226
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v1

    .line 227
    if-eqz v1, :cond_0

    .line 229
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 231
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizv;

    .line 232
    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making record executed-user-actions request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 241
    :cond_0
    :goto_1
    return-void

    .line 234
    :cond_1
    :try_start_1
    invoke-interface {v1, v2}, Lfor;->V(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public final a(Lixp;)Lixq;
    .locals 3

    .prologue
    .line 961
    invoke-static {}, Lenu;->auQ()V

    .line 962
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 963
    if-eqz v0, :cond_0

    .line 965
    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    invoke-interface {v0, v1}, Lfor;->d(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    .line 967
    const-class v1, Lixq;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    check-cast v0, Lixq;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 974
    :goto_0
    return-object v0

    .line 968
    :catch_0
    move-exception v0

    .line 969
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making executeBusinessDataFilterQuery request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 974
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V
    .locals 3

    .prologue
    .line 185
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_0

    .line 188
    :try_start_0
    invoke-interface {v0, p1}, Lfor;->a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 190
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making logging request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Lizj;Z)V
    .locals 3

    .prologue
    .line 318
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_0

    .line 321
    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lfor;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 322
    :catch_0
    move-exception v0

    .line 323
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making dismiss entry request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final aBa()Lfor;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 144
    iget-object v1, p0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 145
    :try_start_0
    iget-object v0, p0, Lfml;->cwe:Lfor;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aBb()Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 171
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_0

    .line 174
    :try_start_0
    invoke-interface {v0}, Lfor;->azG()Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 181
    :goto_0
    return-object v0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    const-string v1, "NowRemoteClient"

    const-string v2, "Error retrieving entries from service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 181
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aBc()V
    .locals 3

    .prologue
    .line 442
    iget-object v1, p0, Lfml;->cvW:Ljava/lang/Object;

    monitor-enter v1

    .line 444
    :try_start_0
    iget v0, p0, Lfml;->cvX:I

    if-nez v0, :cond_0

    .line 448
    monitor-exit v1

    .line 464
    :goto_0
    return-void

    .line 451
    :cond_0
    iget v0, p0, Lfml;->cvX:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lfml;->cvX:I

    .line 456
    iget v0, p0, Lfml;->cvX:I

    if-nez v0, :cond_1

    .line 460
    iget-object v0, p0, Lfml;->cvY:Lfnh;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lfnh;->Kl:Z

    .line 461
    iget-object v0, p0, Lfml;->cvZ:Lfnh;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lfnh;->Kl:Z

    .line 462
    iget-object v0, p0, Lfml;->cwa:Lgbo;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lgbo;->Kl:Z

    .line 464
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final aBd()V
    .locals 2

    .prologue
    .line 477
    iget-object v1, p0, Lfml;->cvW:Ljava/lang/Object;

    monitor-enter v1

    .line 478
    :try_start_0
    iget v0, p0, Lfml;->cvX:I

    if-nez v0, :cond_0

    .line 482
    iget-object v0, p0, Lfml;->cvY:Lfnh;

    invoke-virtual {v0}, Lfnh;->resume()V

    .line 483
    iget-object v0, p0, Lfml;->cvZ:Lfnh;

    invoke-virtual {v0}, Lfnh;->resume()V

    .line 484
    iget-object v0, p0, Lfml;->cwa:Lgbo;

    invoke-virtual {v0}, Lgbo;->resume()V

    .line 487
    :cond_0
    iget v0, p0, Lfml;->cvX:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfml;->cvX:I

    .line 491
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ayI()V
    .locals 3

    .prologue
    .line 788
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 789
    if-eqz v0, :cond_0

    .line 791
    :try_start_0
    invoke-interface {v0}, Lfor;->azI()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 798
    :cond_0
    :goto_0
    return-void

    .line 792
    :catch_0
    move-exception v0

    .line 793
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making location reporting opt in request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final ayr()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 380
    invoke-static {}, Lenu;->auQ()V

    .line 381
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 382
    if-eqz v0, :cond_0

    .line 384
    :try_start_0
    invoke-interface {v0}, Lfor;->ayr()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 391
    :goto_0
    return-object v0

    .line 385
    :catch_0
    move-exception v0

    .line 386
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making sample map request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 391
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final azF()V
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 159
    if-nez v0, :cond_0

    .line 160
    new-instance v0, Landroid/os/RemoteException;

    const-string v1, "Not connected"

    invoke-direct {v0, v1}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    invoke-interface {v0}, Lfor;->azF()V

    .line 163
    return-void
.end method

.method public final azN()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 916
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 917
    if-eqz v0, :cond_0

    .line 919
    :try_start_0
    invoke-interface {v0}, Lfor;->azN()Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 924
    :goto_0
    return-object v0

    .line 920
    :catch_0
    move-exception v0

    .line 921
    const-string v1, "NowRemoteClient"

    const-string v2, "Error getting configuration"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 924
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public final azO()V
    .locals 3

    .prologue
    .line 903
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 904
    if-eqz v0, :cond_0

    .line 906
    :try_start_0
    invoke-interface {v0}, Lfor;->azO()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 913
    :cond_0
    :goto_0
    return-void

    .line 907
    :catch_0
    move-exception v0

    .line 908
    const-string v1, "NowRemoteClient"

    const-string v2, "Error recording shared commute prompt"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final azP()Landroid/accounts/Account;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 979
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 980
    if-eqz v0, :cond_0

    .line 982
    :try_start_0
    invoke-interface {v0}, Lfor;->azP()Landroid/accounts/Account;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 989
    :goto_0
    return-object v0

    .line 983
    :catch_0
    move-exception v0

    .line 984
    const-string v1, "NowRemoteClient"

    const-string v2, "Error retrieving current account from service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 989
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Ljava/util/List;I)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 544
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v1

    .line 545
    if-eqz v1, :cond_0

    .line 547
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lilw;->me(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 548
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbb;

    .line 549
    invoke-static {v0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 552
    :catch_0
    move-exception v0

    .line 553
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making setup images request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 558
    :cond_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 551
    :cond_1
    :try_start_1
    invoke-interface {v1, v2, p2}, Lfor;->b(Ljava/util/List;I)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 365
    invoke-static {}, Lenu;->auQ()V

    .line 366
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_0

    .line 369
    :try_start_0
    invoke-interface {v0, p1}, Lfor;->b(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 376
    :goto_0
    return-object v0

    .line 370
    :catch_0
    move-exception v0

    .line 371
    const-string v1, "NowRemoteClient"

    const-string v2, "Error request static map request with options"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 376
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(JLjava/lang/String;JJLjava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p8    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 945
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 946
    if-eqz v0, :cond_0

    move-wide v1, p1

    move-object v3, p3

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    .line 948
    :try_start_0
    invoke-interface/range {v0 .. v8}, Lfor;->b(JLjava/lang/String;JJLjava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 956
    :goto_0
    return-object v0

    .line 950
    :catch_0
    move-exception v0

    .line 951
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making createCalendarEvent request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 956
    :cond_0
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final b(Lizj;Ljde;Liwk;)V
    .locals 4

    .prologue
    .line 770
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 771
    if-eqz v0, :cond_0

    .line 773
    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v2

    invoke-static {p3}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lfor;->b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 781
    :cond_0
    :goto_0
    return-void

    .line 775
    :catch_0
    move-exception v0

    .line 776
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making sendTrainingActionAsync request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final b(Ljde;Ljdf;Lizj;)V
    .locals 4
    .param p3    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 737
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 738
    if-eqz v0, :cond_0

    .line 740
    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v2

    invoke-static {p3}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lfor;->a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 748
    :cond_0
    :goto_0
    return-void

    .line 742
    :catch_0
    move-exception v0

    .line 743
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making setTrainingAnswer request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method final connect()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1133
    iget-object v1, p0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 1134
    :try_start_0
    iget-object v2, p0, Lfml;->cwf:Lfmp;

    if-nez v2, :cond_0

    .line 1136
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.sidekick.shared.remoteapi.IGoogleNowRemoteService"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1137
    iget-object v3, p0, Lfml;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1138
    new-instance v3, Lfmp;

    invoke-direct {v3, p0}, Lfmp;-><init>(Lfml;)V

    iput-object v3, p0, Lfml;->cwf:Lfmp;

    .line 1139
    iget-object v3, p0, Lfml;->mAppContext:Landroid/content/Context;

    iget-object v4, p0, Lfml;->cwf:Lfmp;

    const/4 v5, 0x1

    invoke-virtual {v3, v2, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    .line 1142
    if-nez v2, :cond_0

    .line 1143
    const-string v0, "NowRemoteClient"

    const-string v2, "Error binding to predictive cards service"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    const/4 v0, 0x0

    iput-object v0, p0, Lfml;->cwf:Lfmp;

    .line 1146
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1150
    :goto_0
    return v0

    .line 1149
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d(Lesk;)V
    .locals 2

    .prologue
    .line 1038
    iget-object v1, p0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 1039
    :try_start_0
    iget-object v0, p0, Lfml;->cvV:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1040
    iget-object v0, p0, Lfml;->cvV:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1042
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d(Lizj;Z)V
    .locals 3

    .prologue
    .line 928
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 929
    if-eqz v0, :cond_0

    .line 931
    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lfor;->c(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 938
    :cond_0
    :goto_0
    return-void

    .line 932
    :catch_0
    move-exception v0

    .line 933
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making queueDismissEntryAction request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final e(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 401
    invoke-static {}, Lenu;->auQ()V

    .line 402
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 403
    if-eqz v0, :cond_0

    .line 405
    :try_start_0
    invoke-interface {v0, p1, p2}, Lfor;->e(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 412
    :goto_0
    return-object v0

    .line 406
    :catch_0
    move-exception v0

    .line 407
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making blocking bitmap request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 412
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 679
    invoke-static {}, Lenu;->auQ()V

    .line 680
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 681
    if-eqz v0, :cond_0

    .line 683
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Lfor;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 690
    :goto_0
    return-object v0

    .line 684
    :catch_0
    move-exception v0

    .line 685
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making translateInPlace request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 690
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lesk;)V
    .locals 2

    .prologue
    .line 1047
    iget-object v1, p0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 1048
    :try_start_0
    iget-object v0, p0, Lfml;->cvV:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1049
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final f(ILandroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1001
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v1

    .line 1002
    if-eqz v1, :cond_0

    .line 1004
    packed-switch p1, :pswitch_data_0

    .line 1015
    :goto_0
    return-void

    .line 1004
    :pswitch_0
    :try_start_0
    const-string v0, "load-image-uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-interface {v1, v0}, Lfor;->K(Landroid/net/Uri;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1007
    :catch_0
    move-exception v0

    .line 1013
    :cond_0
    invoke-direct {p0, p1, p2}, Lfml;->g(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1008
    :catch_1
    move-exception v0

    .line 1009
    const-string v1, "NowRemoteClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Caught exception making request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1004
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final iH(I)Z
    .locals 3

    .prologue
    .line 660
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 661
    if-eqz v0, :cond_0

    .line 663
    :try_start_0
    invoke-interface {v0, p1}, Lfor;->iH(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 670
    :goto_0
    return v0

    .line 664
    :catch_0
    move-exception v0

    .line 665
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making isReminderSmartActionSupported request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 670
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final iJ(I)V
    .locals 3

    .prologue
    .line 807
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 808
    if-eqz v0, :cond_0

    .line 810
    :try_start_0
    invoke-interface {v0, p1}, Lfor;->iJ(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 811
    :catch_0
    move-exception v0

    .line 812
    const-string v1, "NowRemoteClient"

    const-string v2, "Error recording first-use card view"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final iK(I)V
    .locals 3

    .prologue
    .line 826
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 827
    if-eqz v0, :cond_0

    .line 829
    :try_start_0
    invoke-interface {v0, p1}, Lfor;->iK(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 830
    :catch_0
    move-exception v0

    .line 831
    const-string v1, "NowRemoteClient"

    const-string v2, "Error recording first-use card dismiss"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final isConnected()Z
    .locals 2

    .prologue
    .line 1053
    iget-object v1, p0, Lfml;->cvU:Ljava/lang/Object;

    monitor-enter v1

    .line 1054
    :try_start_0
    iget-object v0, p0, Lfml;->cwe:Lfor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1055
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final lN(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 563
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 564
    if-eqz v0, :cond_0

    .line 566
    :try_start_0
    invoke-interface {v0, p1}, Lfor;->lN(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 573
    :goto_0
    return-object v0

    .line 567
    :catch_0
    move-exception v0

    .line 568
    const-string v1, "NowRemoteClient"

    const-string v2, "Error requesting help intent"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 573
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lQ(Ljava/lang/String;)Lfmq;
    .locals 1

    .prologue
    .line 1064
    new-instance v0, Lfmq;

    invoke-direct {v0, p0, p1}, Lfmq;-><init>(Lfml;Ljava/lang/String;)V

    return-object v0
.end method

.method public final wt()V
    .locals 3

    .prologue
    .line 593
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 594
    if-eqz v0, :cond_0

    .line 596
    :try_start_0
    invoke-interface {v0}, Lfor;->wt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    :cond_0
    :goto_0
    return-void

    .line 597
    :catch_0
    move-exception v0

    .line 598
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making invalidateEntries request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final x(Lizj;)V
    .locals 3

    .prologue
    .line 273
    invoke-virtual {p0}, Lfml;->aBa()Lfor;

    move-result-object v0

    .line 274
    if-eqz v0, :cond_0

    .line 276
    :try_start_0
    invoke-static {p1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v1

    invoke-interface {v0, v1}, Lfor;->c(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 277
    :catch_0
    move-exception v0

    .line 278
    const-string v1, "NowRemoteClient"

    const-string v2, "Error making delete notifications request"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
