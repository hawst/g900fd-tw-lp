.class public final Leyf;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private clF:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static a(Landroid/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 51
    const-string v0, "spinner_dialog"

    invoke-virtual {p0, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Leyf;

    .line 53
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v0}, Leyf;->dismissAllowingStateLoss()V

    .line 56
    :cond_0
    return-void
.end method

.method static a(Landroid/app/FragmentManager;Landroid/app/Fragment;)V
    .locals 4

    .prologue
    .line 34
    const-string v0, "spinner_dialog"

    invoke-virtual {p0, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Leyf;

    .line 36
    if-nez v0, :cond_0

    .line 37
    new-instance v0, Leyf;

    invoke-direct {v0}, Leyf;-><init>()V

    .line 39
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 40
    const-string v2, "worker_tag_key"

    invoke-virtual {p1}, Landroid/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-virtual {v0, v1}, Leyf;->setArguments(Landroid/os/Bundle;)V

    .line 43
    const-string v1, "spinner_dialog"

    invoke-virtual {v0, p0, v1}, Leyf;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 45
    :cond_0
    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Leyf;->dismiss()V

    .line 73
    invoke-virtual {p0}, Leyf;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Leyf;->clF:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Leyf;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 78
    :cond_0
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0}, Leyf;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "worker_tag_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leyf;->clF:Ljava/lang/String;

    .line 62
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Leyf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 63
    const v1, 0x7f0a05de

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 64
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 65
    return-object v0
.end method
