.class public final Ljme;
.super Ljsl;
.source "PG"


# static fields
.field public static final etr:Ljsm;


# instance fields
.field private aez:I

.field private ajB:Ljava/lang/String;

.field private ets:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16270
    const/16 v0, 0xb

    const-class v1, Ljme;

    const v2, 0x1e4edb1a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljme;->etr:Ljsm;

    .line 16276
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16327
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 16328
    const/4 v0, 0x0

    iput v0, p0, Ljme;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljme;->ets:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljme;->ajB:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljme;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljme;->eCz:I

    .line 16329
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 16263
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljme;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljme;->ets:Ljava/lang/String;

    iget v0, p0, Ljme;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljme;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljme;->ajB:Ljava/lang/String;

    iget v0, p0, Ljme;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljme;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 16343
    iget v0, p0, Ljme;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 16344
    const/4 v0, 0x1

    iget-object v1, p0, Ljme;->ets:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 16346
    :cond_0
    iget v0, p0, Ljme;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 16347
    const/4 v0, 0x2

    iget-object v1, p0, Ljme;->ajB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 16349
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 16350
    return-void
.end method

.method public final bpD()Z
    .locals 1

    .prologue
    .line 16297
    iget v0, p0, Ljme;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16308
    iget-object v0, p0, Ljme;->ajB:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 16354
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 16355
    iget v1, p0, Ljme;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 16356
    const/4 v1, 0x1

    iget-object v2, p0, Ljme;->ets:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16359
    :cond_0
    iget v1, p0, Ljme;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 16360
    const/4 v1, 0x2

    iget-object v2, p0, Ljme;->ajB:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16363
    :cond_1
    return v0
.end method

.method public final wP(Ljava/lang/String;)Ljme;
    .locals 1

    .prologue
    .line 16311
    if-nez p1, :cond_0

    .line 16312
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 16314
    :cond_0
    iput-object p1, p0, Ljme;->ajB:Ljava/lang/String;

    .line 16315
    iget v0, p0, Ljme;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljme;->aez:I

    .line 16316
    return-object p0
.end method
