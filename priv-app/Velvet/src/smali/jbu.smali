.class public final Ljbu;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private dAg:Ljava/lang/String;

.field private dZo:J

.field private dZp:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljbu;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljbu;->dAg:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljbu;->dZo:J

    const-string v0, ""

    iput-object v0, p0, Ljbu;->dZp:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljbu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljbu;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljbu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbu;->dAg:Ljava/lang/String;

    iget v0, p0, Ljbu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljbu;->dZo:J

    iget v0, p0, Ljbu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljbu;->dZp:Ljava/lang/String;

    iget v0, p0, Ljbu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbu;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    iget v0, p0, Ljbu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbu;->dAg:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljbu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v2, p0, Ljbu;->dZo:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_1
    iget v0, p0, Ljbu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljbu;->dZp:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bfw()Z
    .locals 1

    iget v0, p0, Ljbu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfx()J
    .locals 2

    iget-wide v0, p0, Ljbu;->dZo:J

    return-wide v0
.end method

.method public final bfy()Z
    .locals 1

    iget v0, p0, Ljbu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bfz()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljbu;->dZp:Ljava/lang/String;

    return-object v0
.end method

.method public final cH(J)Ljbu;
    .locals 1

    iput-wide p1, p0, Ljbu;->dZo:J

    iget v0, p0, Ljbu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbu;->aez:I

    return-object p0
.end method

.method public final getCurrencyCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljbu;->dAg:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Ljbu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljbu;->dAg:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljbu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Ljbu;->dZo:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljbu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljbu;->dZp:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    return v0
.end method

.method public final sB(Ljava/lang/String;)Ljbu;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljbu;->dAg:Ljava/lang/String;

    iget v0, p0, Ljbu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbu;->aez:I

    return-object p0
.end method

.method public final sC(Ljava/lang/String;)Ljbu;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljbu;->dZp:Ljava/lang/String;

    iget v0, p0, Ljbu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljbu;->aez:I

    return-object p0
.end method
