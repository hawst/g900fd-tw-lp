.class public Lhlg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lefk;


# instance fields
.field private final aZw:Lhli;

.field private synthetic djV:Lhlf;

.field private final mQuery:Lcom/google/android/shared/search/Query;


# direct methods
.method public constructor <init>(Lhlf;Lhli;Lcom/google/android/shared/search/Query;)V
    .locals 0

    .prologue
    .line 396
    iput-object p1, p0, Lhlg;->djV:Lhlf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397
    iput-object p2, p0, Lhlg;->aZw:Lhli;

    .line 398
    iput-object p3, p0, Lhlg;->mQuery:Lcom/google/android/shared/search/Query;

    .line 399
    return-void
.end method

.method private d(Leiq;)V
    .locals 3

    .prologue
    .line 416
    const-string v0, "VoiceSearchController"

    const-string v1, "GrammarCompilationCallback #reportError: "

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, Leor;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 417
    iget-object v0, p0, Lhlg;->djV:Lhlf;

    iget-object v1, p0, Lhlg;->mQuery:Lcom/google/android/shared/search/Query;

    iget-object v2, p0, Lhlg;->aZw:Lhli;

    invoke-virtual {v0, v1, v2, p1}, Lhlf;->a(Lcom/google/android/shared/search/Query;Lhli;Leiq;)V

    .line 418
    return-void
.end method


# virtual methods
.method public final synthetic ar(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 391
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lhlg;->b(Ljava/lang/Integer;)V

    return-void
.end method

.method public final b(Ljava/lang/Integer;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 404
    iget-object v0, p0, Lhlg;->djV:Lhlf;

    iget-object v0, v0, Lhlf;->bbd:Lenw;

    invoke-virtual {v0}, Lenw;->auS()Lenw;

    .line 406
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 407
    iget-object v0, p0, Lhlg;->djV:Lhlf;

    iget-object v1, p0, Lhlg;->aZw:Lhli;

    iget-object v2, p0, Lhlg;->mQuery:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1, v2, v3}, Lhlf;->a(Lhli;Lcom/google/android/shared/search/Query;Z)V

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 409
    new-instance v0, Leib;

    invoke-direct {v0}, Leib;-><init>()V

    invoke-direct {p0, v0}, Lhlg;->d(Leiq;)V

    goto :goto_0

    .line 410
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 411
    new-instance v0, Lehz;

    const v1, 0x70002

    invoke-direct {v0, v1}, Lehz;-><init>(I)V

    invoke-direct {p0, v0}, Lhlg;->d(Leiq;)V

    goto :goto_0
.end method
