.class public final Lcqz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcrf;


# instance fields
.field private final bgL:Lcrf;

.field private final bgM:Lcra;


# direct methods
.method public constructor <init>(Lchk;Lemp;Lcrf;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p3, p0, Lcqz;->bgL:Lcrf;

    .line 47
    new-instance v0, Lcra;

    invoke-direct {v0, p1, p2}, Lcra;-><init>(Lchk;Lemp;)V

    iput-object v0, p0, Lcqz;->bgM:Lcra;

    .line 48
    return-void
.end method


# virtual methods
.method public final So()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcqz;->bgM:Lcra;

    iget-object v1, v0, Lcra;->dK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcra;->bgN:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    iget-object v0, v0, Lcra;->bgO:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Leua;)Ljava/lang/String;
    .locals 8

    .prologue
    .line 54
    const-string v0, "weblogin:"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    :try_start_0
    const-string v0, "CachingGoogleAuthAdapter: access token cache"

    invoke-virtual {p5, v0}, Leua;->beginSection(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcqz;->bgM:Lcra;

    invoke-virtual {v0, p3, p2}, Lcra;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 60
    invoke-virtual {p5}, Leua;->endSection()V

    .line 62
    if-eqz v6, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-object v6

    .line 60
    :catchall_0
    move-exception v0

    invoke-virtual {p5}, Leua;->endSection()V

    throw v0

    .line 67
    :cond_1
    const/16 v0, 0xa7

    :try_start_1
    invoke-static {v0}, Lege;->ht(I)V
    :try_end_1
    .catch Lbfb; {:try_start_1 .. :try_end_1} :catch_0

    .line 70
    :try_start_2
    const-string v0, "CachingGoogleAuthAdapter: get token from wrapped adapter"

    invoke-virtual {p5, v0}, Leua;->beginSection(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcqz;->bgL:Lcrf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcrf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Leua;)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 74
    :try_start_3
    invoke-virtual {p5}, Leua;->endSection()V

    .line 76
    const/4 v0, 0x0

    invoke-static {v0}, Lcrh;->fp(I)V
    :try_end_3
    .catch Lbfb; {:try_start_3 .. :try_end_3} :catch_0

    .line 84
    const-string v0, "weblogin:"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    :try_start_4
    const-string v0, "CachingGoogleAuthAdapter: put token in cache"

    invoke-virtual {p5, v0}, Leua;->beginSection(Ljava/lang/String;)V

    .line 87
    iget-object v7, p0, Lcqz;->bgM:Lcra;

    new-instance v1, Lcrb;

    iget-object v0, v7, Lcra;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v4

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v1 .. v6}, Lcrb;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    iget-object v2, v7, Lcra;->dK:Ljava/lang/Object;

    monitor-enter v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :try_start_5
    iget-object v0, v7, Lcra;->bgN:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_2

    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, v7, Lcra;->bgN:Ljava/util/Map;

    invoke-interface {v3, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v7, Lcra;->bgO:Ljava/util/Map;

    invoke-interface {v0, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 89
    invoke-virtual {p5}, Leua;->endSection()V

    goto :goto_0

    .line 74
    :catchall_1
    move-exception v0

    :try_start_6
    invoke-virtual {p5}, Leua;->endSection()V

    throw v0
    :try_end_6
    .catch Lbfb; {:try_start_6 .. :try_end_6} :catch_0

    .line 77
    :catch_0
    move-exception v6

    .line 78
    const-string v0, "weblogin:"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 79
    iget-object v7, p0, Lcqz;->bgM:Lcra;

    new-instance v1, Lcrb;

    iget-object v0, v7, Lcra;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->uptimeMillis()J

    move-result-wide v4

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v1 .. v6}, Lcrb;-><init>(Ljava/lang/String;Ljava/lang/String;JLbfb;)V

    iget-object v2, v7, Lcra;->dK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_7
    iget-object v0, v7, Lcra;->bgN:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_3

    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, v7, Lcra;->bgN:Ljava/util/Map;

    invoke-interface {v3, p3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 82
    :cond_4
    throw v6

    .line 79
    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0

    .line 87
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v2

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 89
    :catchall_4
    move-exception v0

    invoke-virtual {p5}, Leua;->endSection()V

    throw v0
.end method

.method public final d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 98
    iget-object v1, p0, Lcqz;->bgM:Lcra;

    iget-object v2, v1, Lcra;->dK:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lcra;->bgO:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcrb;

    if-eqz v0, :cond_0

    iget-object v1, v1, Lcra;->bgN:Ljava/util/Map;

    iget-object v3, v0, Lcrb;->bgP:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcrb;->bbH:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    iget-object v0, p0, Lcqz;->bgL:Lcrf;

    invoke-interface {v0, p1, p2}, Lcrf;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 100
    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
