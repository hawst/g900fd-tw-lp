.class public abstract Lhou;
.super Lduc;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lhou;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lduc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    return-void
.end method

.method static synthetic a(Lhou;)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0}, Lhou;->ahi()V

    return-void
.end method

.method static synthetic a(Lhou;Z)Z
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic b(Lhou;)V
    .locals 0

    .prologue
    .line 14
    invoke-virtual {p0}, Lhou;->tP()V

    return-void
.end method


# virtual methods
.method protected ahj()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lduc;->bLR:Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v0

    return v0
.end method

.method protected abstract onClick()V
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lduc;->onFinishInflate()V

    .line 32
    new-instance v0, Lhov;

    invoke-direct {v0, p0}, Lhov;-><init>(Lhou;)V

    invoke-virtual {p0, v0}, Lhou;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    return-void
.end method
