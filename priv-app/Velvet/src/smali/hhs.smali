.class final Lhhs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligi;


# instance fields
.field private synthetic dhy:Lhhq;


# direct methods
.method constructor <init>(Lhhq;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lhhs;->dhy:Lhhq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private aPn()Lgij;
    .locals 13

    .prologue
    .line 206
    iget-object v0, p0, Lhhs;->dhy:Lhhq;

    iget-object v8, v0, Lhhq;->aqp:Ljava/lang/Object;

    monitor-enter v8

    .line 207
    :try_start_0
    iget-object v0, p0, Lhhs;->dhy:Lhhq;

    iget-object v0, v0, Lhhq;->mSpeechContext:Lgij;

    if-nez v0, :cond_2

    .line 208
    iget-object v9, p0, Lhhs;->dhy:Lhhq;

    iget-object v10, p0, Lhhs;->dhy:Lhhq;

    new-instance v0, Lgik;

    new-instance v1, Lgih;

    iget-object v2, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v2}, Lcfo;->BL()Lchk;

    move-result-object v2

    invoke-direct {v1, v2}, Lgih;-><init>(Lchk;)V

    new-instance v2, Lghs;

    iget-object v3, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v3}, Lcfo;->BL()Lchk;

    move-result-object v3

    iget-object v4, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BK()Lcke;

    move-result-object v4

    invoke-static {}, Lenu;->auR()V

    iget-object v5, v10, Lhhq;->cKp:Lgho;

    if-nez v5, :cond_0

    new-instance v5, Lgho;

    iget-object v6, v10, Lhhq;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-direct {v5, v6}, Lgho;-><init>(Landroid/content/ContentResolver;)V

    iput-object v5, v10, Lhhq;->cKp:Lgho;

    :cond_0
    iget-object v5, v10, Lhhq;->cKp:Lgho;

    invoke-direct {v2, v3, v4, v5}, Lghs;-><init>(Lchk;Lcke;Lgho;)V

    new-instance v3, Lghl;

    iget-object v4, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v4}, Lcfo;->BL()Lchk;

    move-result-object v4

    iget-object v5, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->BK()Lcke;

    move-result-object v5

    invoke-static {}, Lenu;->auR()V

    iget-object v6, v10, Lhhq;->dhp:Lghj;

    if-nez v6, :cond_1

    new-instance v6, Lghj;

    iget-object v7, v10, Lhhq;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-direct {v6, v7}, Lghj;-><init>(Landroid/content/ContentResolver;)V

    iput-object v6, v10, Lhhq;->dhp:Lghj;

    :cond_1
    iget-object v6, v10, Lhhq;->dhp:Lghj;

    iget-object v7, v10, Lhhq;->mAsyncServices:Lema;

    invoke-virtual {v7}, Lema;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lghl;-><init>(Lchk;Lcke;Lghj;Ljava/util/concurrent/Executor;)V

    new-instance v4, Lghg;

    iget-object v5, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v5}, Lcfo;->Eg()Ligi;

    move-result-object v5

    iget-object v6, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->BL()Lchk;

    move-result-object v6

    iget-object v7, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->BK()Lcke;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lghg;-><init>(Ligi;Lchk;Lcke;)V

    new-instance v5, Lgnh;

    iget-object v6, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v6}, Lcfo;->BL()Lchk;

    move-result-object v6

    invoke-direct {v5, v6}, Lgnh;-><init>(Lgdm;)V

    new-instance v6, Lgig;

    iget-object v7, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v7}, Lcfo;->BL()Lchk;

    move-result-object v7

    iget-object v11, v10, Lhhq;->mSettings:Lhym;

    invoke-virtual {v11}, Lhym;->aTG()Ljava/util/Locale;

    move-result-object v11

    iget-object v12, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v12}, Lcfo;->Eg()Ligi;

    move-result-object v12

    invoke-direct {v6, v7, v11, v12}, Lgig;-><init>(Lchk;Ljava/util/Locale;Ligi;)V

    new-instance v7, Lgii;

    iget-object v11, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v11}, Lcfo;->BL()Lchk;

    move-result-object v11

    iget-object v10, v10, Lhhq;->mCoreSearchServices:Lcfo;

    invoke-virtual {v10}, Lcfo;->DC()Lemp;

    move-result-object v10

    invoke-direct {v7, v11, v10}, Lgii;-><init>(Lchk;Lemp;)V

    invoke-direct/range {v0 .. v7}, Lgik;-><init>(Lgih;Lghs;Lghl;Lghg;Lgnh;Lgig;Lgii;)V

    iput-object v0, v9, Lhhq;->mSpeechContext:Lgij;

    .line 210
    :cond_2
    iget-object v0, p0, Lhhs;->dhy:Lhhq;

    iget-object v0, v0, Lhhq;->mSpeechContext:Lgij;

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0}, Lhhs;->aPn()Lgij;

    move-result-object v0

    return-object v0
.end method
