.class public final Lfaq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cnl:Ljbj;


# instance fields
.field private cjK:Landroid/content/BroadcastReceiver;

.field private final cmM:Lfba;

.field private final cnj:Lfdg;

.field final cnm:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final cnn:Ljava/lang/Object;

.field final cno:Ljava/lang/Object;

.field private cnp:Lizo;

.field private cnq:[B
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cnr:Z

.field cns:J

.field private cnt:J

.field private cnu:Ljbp;

.field private cnv:Ljava/util/List;

.field private cnw:Ljava/util/Locale;

.field private cnx:Z

.field cny:Ljava/util/concurrent/ScheduledFuture;

.field private final mAppContext:Landroid/content/Context;

.field private final mAsyncFileStorage:Lfbz;

.field private final mClock:Lemp;

.field private final mConfiguration:Lcjs;

.field private final mEntryNotificationFactory:Lfjs;

.field private final mEntryTreePruner:Lfbh;

.field private final mLocationOracle:Lfdr;

.field private final mNetworkClient:Lfcx;

.field private final mNotificationStore:Lffp;

.field private final mPredictiveCardsPreferences:Lcxs;

.field final mSidekickInjector:Lfdb;

.field private final mTaskRunnerExecutors:Lerm;

.field private final mTriggerConditionScheduler:Lfiv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 116
    new-instance v0, Ljbj;

    invoke-direct {v0}, Ljbj;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljbj;->om(I)Ljbj;

    move-result-object v0

    sput-object v0, Lfaq;->cnl:Ljbj;

    return-void
.end method

.method public constructor <init>(Lemp;Landroid/content/Context;Lfbz;Lffp;Lfdr;Lcjs;Lerm;Lfdb;Lcxs;Lfjs;Lfiv;Lfbh;Lfcx;Lfdg;)V
    .locals 5

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v1, p0, Lfaq;->cnm:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 144
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    .line 145
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lfaq;->cno:Ljava/lang/Object;

    .line 149
    const/4 v1, 0x0

    iput-object v1, p0, Lfaq;->cnp:Lizo;

    .line 158
    const/4 v1, 0x0

    iput-boolean v1, p0, Lfaq;->cnr:Z

    .line 171
    const/4 v1, 0x0

    iput-object v1, p0, Lfaq;->cnv:Ljava/util/List;

    .line 181
    new-instance v1, Lfar;

    invoke-direct {v1, p0}, Lfar;-><init>(Lfaq;)V

    iput-object v1, p0, Lfaq;->cjK:Landroid/content/BroadcastReceiver;

    .line 200
    iput-object p1, p0, Lfaq;->mClock:Lemp;

    .line 201
    iput-object p2, p0, Lfaq;->mAppContext:Landroid/content/Context;

    .line 202
    iput-object p3, p0, Lfaq;->mAsyncFileStorage:Lfbz;

    .line 203
    iput-object p4, p0, Lfaq;->mNotificationStore:Lffp;

    .line 204
    iput-object p5, p0, Lfaq;->mLocationOracle:Lfdr;

    .line 205
    iput-object p6, p0, Lfaq;->mConfiguration:Lcjs;

    .line 206
    iput-object p7, p0, Lfaq;->mTaskRunnerExecutors:Lerm;

    .line 207
    iput-object p8, p0, Lfaq;->mSidekickInjector:Lfdb;

    .line 208
    iput-object p9, p0, Lfaq;->mPredictiveCardsPreferences:Lcxs;

    .line 209
    iput-object p10, p0, Lfaq;->mEntryNotificationFactory:Lfjs;

    .line 210
    move-object/from16 v0, p11

    iput-object v0, p0, Lfaq;->mTriggerConditionScheduler:Lfiv;

    .line 211
    new-instance v1, Lfba;

    iget-object v2, p0, Lfaq;->mAppContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lfba;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfaq;->cmM:Lfba;

    .line 212
    move-object/from16 v0, p12

    iput-object v0, p0, Lfaq;->mEntryTreePruner:Lfbh;

    .line 213
    move-object/from16 v0, p13

    iput-object v0, p0, Lfaq;->mNetworkClient:Lfcx;

    .line 214
    move-object/from16 v0, p14

    iput-object v0, p0, Lfaq;->cnj:Lfdg;

    .line 216
    invoke-static {p2}, Lcn;->a(Landroid/content/Context;)Lcn;

    move-result-object v1

    iget-object v2, p0, Lfaq;->cjK:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.google.android.apps.sidekick.DATA_BACKEND_VERSION_STORE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcn;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 218
    return-void
.end method

.method private static a([Lizj;)Ljava/util/List;
    .locals 4

    .prologue
    .line 871
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p0, v0

    .line 872
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lizj;->ht(Z)Lizj;

    .line 871
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 874
    :cond_0
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/location/Location;JLfbk;)V
    .locals 4

    .prologue
    .line 381
    const/4 v0, 0x0

    .line 385
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 386
    :try_start_0
    iget-object v2, p0, Lfaq;->cnp:Lizo;

    if-eqz v2, :cond_0

    .line 387
    iget-object v0, p0, Lfaq;->cnp:Lizo;

    new-instance v2, Lizo;

    invoke-direct {v2}, Lizo;-><init>()V

    invoke-static {v0, v2}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizo;

    .line 388
    invoke-virtual {p4, v0}, Lfbk;->i(Lizo;)V

    .line 389
    iput-object v0, p0, Lfaq;->cnp:Lizo;

    .line 390
    iget-object v0, p0, Lfaq;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lfaq;->cnt:J

    .line 391
    const/4 v0, 0x1

    .line 393
    :cond_0
    iget-object v2, p0, Lfaq;->cnp:Lizo;

    invoke-static {v2, p1, p2, p3}, Lfaq;->b(Lizo;Landroid/location/Location;J)Lfbi;

    move-result-object v2

    .line 395
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    if-eqz v0, :cond_1

    .line 399
    invoke-virtual {p0, v2}, Lfaq;->a(Lfbi;)V

    .line 402
    iget-object v0, p0, Lfaq;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axU()Lfjo;

    move-result-object v0

    invoke-virtual {v0}, Lfjo;->aAt()V

    .line 405
    invoke-direct {p0}, Lfaq;->awU()V

    .line 407
    :cond_1
    return-void

    .line 395
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lfbk;)V
    .locals 4

    .prologue
    .line 371
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 372
    iget-object v0, p0, Lfaq;->mLocationOracle:Lfdr;

    invoke-interface {v0}, Lfdr;->zH()Landroid/location/Location;

    move-result-object v0

    .line 376
    :goto_0
    invoke-direct {p0}, Lfaq;->axc()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3, p1}, Lfaq;->a(Landroid/location/Location;JLfbk;)V

    .line 377
    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Lfaq;->mLocationOracle:Lfdr;

    invoke-interface {v0}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lfga;Lizq;Ljava/util/List;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1226
    iget-object v0, p2, Lizq;->dUZ:Lizj;

    if-eqz v0, :cond_1

    .line 1228
    iget-object v0, p0, Lfaq;->mEntryNotificationFactory:Lfjs;

    invoke-interface {v0, p2}, Lfjs;->f(Lizq;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfey;

    .line 1229
    invoke-virtual {p0, p1, p3, v0}, Lfaq;->a(Lfga;Ljava/util/List;Lfey;)V

    .line 1246
    :cond_0
    return-void

    .line 1233
    :cond_1
    iget-object v3, p2, Lizq;->dUX:[Lizj;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 1234
    iget-object v5, p0, Lfaq;->mEntryNotificationFactory:Lfjs;

    invoke-interface {v5, v0}, Lfjs;->r(Lizj;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfey;

    .line 1236
    invoke-virtual {p0, p1, p3, v0}, Lfaq;->a(Lfga;Ljava/util/List;Lfey;)V

    .line 1233
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1241
    :cond_2
    iget-object v2, p2, Lizq;->dUW:[Lizq;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_0

    aget-object v1, v2, v0

    .line 1242
    invoke-direct {p0, p1, v1, p3}, Lfaq;->a(Lfga;Lizq;Ljava/util/List;)V

    .line 1241
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static a(Lizo;Landroid/location/Location;J)V
    .locals 2
    .param p0    # Lizo;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1168
    if-eqz p0, :cond_0

    .line 1169
    new-instance v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    invoke-direct {v0}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;-><init>()V

    const/4 v1, 0x0

    invoke-static {p1, p2, p3, v0, v1}, Lfbi;->a(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;Z)Lfbi;

    move-result-object v0

    .line 1172
    invoke-virtual {v0, p0}, Lfbi;->j(Lizo;)V

    .line 1174
    :cond_0
    return-void
.end method

.method private a(Lfga;Lfey;Z)Z
    .locals 5

    .prologue
    .line 1306
    invoke-interface {p2}, Lfey;->ayZ()Lfgb;

    move-result-object v1

    .line 1307
    const/4 v0, 0x0

    .line 1308
    invoke-interface {p2}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1309
    iget-object v0, p0, Lfaq;->mAppContext:Landroid/content/Context;

    invoke-interface {p2}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;->a(Landroid/content/Context;Ljava/util/Collection;Lfgb;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1312
    :cond_0
    invoke-interface {p1, p2, v0, p3}, Lfga;->a(Lfey;Landroid/app/PendingIntent;Z)Landroid/app/Notification;

    move-result-object v1

    .line 1316
    if-eqz v1, :cond_3

    .line 1317
    invoke-interface {p2}, Lfey;->ayQ()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1320
    invoke-interface {p2}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 1321
    iget-object v3, p0, Lfaq;->mNotificationStore:Lffp;

    sget-object v4, Lfaq;->cnl:Ljbj;

    invoke-virtual {v3, v0, v4}, Lffp;->a(Lizj;Ljbj;)V

    goto :goto_0

    .line 1325
    :cond_1
    invoke-interface {p1, v1, p2}, Lfga;->a(Landroid/app/Notification;Lfey;)V

    .line 1327
    invoke-interface {p2}, Lfey;->ayR()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1328
    invoke-interface {p2}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 1329
    invoke-interface {p1, v0}, Lfga;->p(Lizj;)V

    .line 1330
    iget-object v2, p0, Lfaq;->mNotificationStore:Lffp;

    invoke-virtual {v2, v0}, Lffp;->l(Lizj;)V

    goto :goto_1

    .line 1333
    :cond_2
    const/4 v0, 0x1

    .line 1335
    :goto_2
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private awU()V
    .locals 3

    .prologue
    .line 415
    iget-object v0, p0, Lfaq;->mAsyncFileStorage:Lfbz;

    const-string v1, "entry_provider"

    new-instance v2, Lfau;

    invoke-direct {v2, p0}, Lfau;-><init>(Lfaq;)V

    invoke-interface {v0, v1, v2}, Lfbz;->b(Ljava/lang/String;Lifg;)V

    .line 464
    return-void
.end method

.method private axc()J
    .locals 4

    .prologue
    .line 1301
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lfaq;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private b(Lizq;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1350
    iget-object v1, p1, Lizq;->dUZ:Lizj;

    if-eqz v1, :cond_1

    .line 1353
    const/4 v0, 0x1

    .line 1365
    :cond_0
    :goto_0
    return v0

    .line 1354
    :cond_1
    iget-object v1, p1, Lizq;->dUX:[Lizj;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 1356
    iget-object v0, p1, Lizq;->dUX:[Lizj;

    array-length v0, v0

    goto :goto_0

    .line 1357
    :cond_2
    iget-object v1, p1, Lizq;->dUW:[Lizq;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 1360
    iget-object v3, p1, Lizq;->dUW:[Lizq;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v2, v3, v1

    .line 1361
    invoke-direct {p0, v2}, Lfaq;->b(Lizq;)I

    move-result v2

    add-int/2addr v2, v0

    .line 1360
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1
.end method

.method private static b(Lizo;Landroid/location/Location;J)Lfbi;
    .locals 2
    .param p0    # Lizo;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1184
    new-instance v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    invoke-direct {v0}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;-><init>()V

    const/4 v1, 0x1

    invoke-static {p1, p2, p3, v0, v1}, Lfbi;->a(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;Z)Lfbi;

    move-result-object v0

    .line 1187
    if-eqz p0, :cond_0

    .line 1188
    invoke-virtual {v0, p0}, Lfbi;->j(Lizo;)V

    .line 1190
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final D(IZ)V
    .locals 1

    .prologue
    .line 1461
    iget-object v0, p0, Lfaq;->cmM:Lfba;

    invoke-virtual {v0, p1, p2}, Lfba;->E(IZ)V

    .line 1462
    return-void
.end method

.method public final a(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;)V
    .locals 8
    .param p1    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 1079
    .line 1082
    iget-object v3, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v3

    .line 1086
    :try_start_0
    iget-object v0, p0, Lfaq;->cnp:Lizo;

    if-eqz v0, :cond_6

    .line 1087
    iget-object v0, p0, Lfaq;->cnp:Lizo;

    new-instance v2, Lizo;

    invoke-direct {v2}, Lizo;-><init>()V

    invoke-static {v0, v2}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizo;

    .line 1088
    const/4 v2, 0x1

    invoke-static {p1, p2, p3, p4, v2}, Lfbi;->a(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;Z)Lfbi;

    move-result-object v2

    .line 1090
    invoke-virtual {v2, v0}, Lfbi;->j(Lizo;)V

    .line 1091
    iput-object v0, p0, Lfaq;->cnp:Lizo;

    .line 1093
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1096
    if-eqz v2, :cond_5

    .line 1097
    iget-object v3, p0, Lfaq;->mAppContext:Landroid/content/Context;

    iget-object v0, v2, Lfbi;->cnR:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    iget-object v0, v0, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    iget-object v0, v0, Lfiu;->ctl:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljie;

    new-instance v1, Ljie;

    invoke-direct {v1}, Ljie;-><init>()V

    iget-object v4, v0, Ljie;->end:[I

    iput-object v4, v1, Ljie;->end:[I

    invoke-virtual {v0}, Ljie;->bmY()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljie;->am([B)Ljie;

    :cond_0
    iget-boolean v0, v2, Lfbi;->cnU:Z

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "com.google.android.apps.sidekick.TYPE"

    const/4 v5, 0x7

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz v1, :cond_1

    const-string v4, "com.google.android.apps.sidekick.TC"

    invoke-static {v1}, Ljsr;->m(Ljsr;)[B

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_1
    invoke-virtual {v3, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_2
    iget-object v0, v2, Lfbi;->cnT:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbj;

    if-eqz v1, :cond_3

    iget-object v5, v2, Lfbi;->mFreshenRequestManager:Lfil;

    invoke-virtual {v5, v3, v0, v1}, Lfil;->a(Landroid/content/Context;Ljbj;Ljie;)V

    goto :goto_1

    .line 1093
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1097
    :cond_3
    iget-object v5, v2, Lfbi;->mFreshenRequestManager:Lfil;

    invoke-virtual {v5, v3, v0, v6}, Lfil;->a(Landroid/content/Context;Ljbj;Z)V

    goto :goto_1

    .line 1098
    :cond_4
    invoke-virtual {p0, v2}, Lfaq;->a(Lfbi;)V

    .line 1100
    :cond_5
    return-void

    :cond_6
    move-object v2, v1

    goto :goto_0
.end method

.method public final a(Lfbb;)V
    .locals 1

    .prologue
    .line 1372
    iget-object v0, p0, Lfaq;->cmM:Lfba;

    invoke-virtual {v0, p1}, Lfba;->registerObserver(Ljava/lang/Object;)V

    .line 1373
    return-void
.end method

.method final a(Lfbi;)V
    .locals 5

    .prologue
    .line 1200
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1201
    iget-object v0, p0, Lfaq;->mTaskRunnerExecutors:Lerm;

    new-instance v1, Lfav;

    const-string v2, "Reschedule trigger condition evaluation"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3, p1}, Lfav;-><init>(Lfaq;Ljava/lang/String;[ILfbi;)V

    invoke-interface {v0, v1}, Lerm;->a(Leri;)V

    .line 1215
    :goto_0
    return-void

    .line 1210
    :cond_0
    iget-object v0, p0, Lfaq;->mAppContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/sidekick/main/entry/TriggerConditionReevaluationService;->aE(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1212
    iget-object v1, p0, Lfaq;->mTriggerConditionScheduler:Lfiv;

    iget-object v2, p0, Lfaq;->mAppContext:Landroid/content/Context;

    const-string v3, "EntryProvider"

    iget-object v4, p1, Lfbi;->cnR:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    iget-object v4, v4, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;->csY:Lfiu;

    invoke-virtual {v1, v2, v3, v4, v0}, Lfiv;->a(Landroid/content/Context;Ljava/lang/String;Lfiu;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 1201
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public final a(Lfbn;)V
    .locals 2

    .prologue
    .line 362
    new-instance v0, Lfbm;

    iget-object v1, p0, Lfaq;->cmM:Lfba;

    invoke-direct {v0, v1, p1}, Lfbm;-><init>(Lfba;Lfbn;)V

    invoke-direct {p0, v0}, Lfaq;->a(Lfbk;)V

    .line 363
    return-void
.end method

.method public final a(Lfga;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 1110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1113
    sget-object v1, Lfgb;->cqz:Lfgb;

    invoke-interface {p1, v1}, Lfga;->a(Lfgb;)V

    .line 1117
    invoke-virtual {p0}, Lfaq;->awW()Lizo;

    move-result-object v1

    .line 1118
    if-eqz v1, :cond_0

    iget-object v3, v1, Lizo;->dUQ:Lizq;

    if-eqz v3, :cond_0

    .line 1119
    iget-object v1, v1, Lizo;->dUQ:Lizq;

    .line 1120
    invoke-direct {p0, p1, v1, v0}, Lfaq;->a(Lfga;Lizq;Ljava/util/List;)V

    .line 1125
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lfey;

    .line 1126
    const/4 v3, 0x0

    .line 1127
    invoke-interface {v1}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v0

    .line 1128
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 1129
    iget-object v6, p0, Lfaq;->mNotificationStore:Lffp;

    invoke-virtual {v6, v0}, Lffp;->j(Lizj;)I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v2

    .line 1135
    :goto_0
    if-nez v0, :cond_1

    .line 1140
    invoke-direct {p0, p1, v1, v2}, Lfaq;->a(Lfga;Lfey;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1148
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfaq;->mAppContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/sidekick/main/notifications/NotificationRefreshService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1149
    const-string v1, "com.google.android.apps.sidekick.notifications.RESET_ALARMS_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1150
    iget-object v1, p0, Lfaq;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1151
    return-void

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method public final a(Lfga;Ljava/util/List;Lfey;)V
    .locals 8
    .param p3    # Lfey;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1256
    if-eqz p3, :cond_0

    .line 1257
    invoke-interface {p3}, Lfey;->ayQ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1258
    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1298
    :cond_0
    :goto_0
    return-void

    .line 1260
    :cond_1
    invoke-interface {p3}, Lfey;->ayT()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v4

    .line 1263
    :goto_1
    if-eqz v0, :cond_8

    .line 1268
    invoke-interface {p3}, Lfey;->ayP()Ljava/util/Collection;

    move-result-object v5

    .line 1271
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v2

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 1272
    iget-object v7, p0, Lfaq;->mNotificationStore:Lffp;

    invoke-virtual {v7, v0}, Lffp;->j(Lizj;)I

    move-result v0

    .line 1273
    and-int/lit8 v7, v0, 0x1

    if-eqz v7, :cond_2

    .line 1275
    add-int/lit8 v3, v3, 0x1

    .line 1277
    :cond_2
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    .line 1279
    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    .line 1281
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1260
    goto :goto_1

    .line 1285
    :cond_4
    if-nez v3, :cond_5

    move v0, v4

    .line 1287
    :goto_4
    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v3

    if-ne v1, v3, :cond_6

    .line 1292
    :goto_5
    if-nez v4, :cond_0

    .line 1294
    invoke-direct {p0, p1, p3, v0}, Lfaq;->a(Lfga;Lfey;Z)Z

    goto :goto_0

    :cond_5
    move v0, v2

    .line 1285
    goto :goto_4

    :cond_6
    move v4, v2

    .line 1287
    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move v0, v4

    move v4, v2

    goto :goto_5
.end method

.method public final a(Lizj;Ljava/util/Collection;)V
    .locals 4

    .prologue
    .line 343
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Liqs;->mm(I)Ljava/util/HashSet;

    move-result-object v1

    .line 344
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 345
    if-eqz v0, :cond_0

    .line 346
    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 352
    :cond_1
    invoke-static {p1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    .line 353
    new-instance v2, Lfab;

    iget-object v3, p0, Lfaq;->cmM:Lfba;

    invoke-direct {v2, v3, v0, v1}, Lfab;-><init>(Lfba;Lgbg;Ljava/util/Collection;)V

    invoke-direct {p0, v2}, Lfaq;->a(Lfbk;)V

    .line 354
    return-void
.end method

.method public final a(Lizk;)V
    .locals 18

    .prologue
    .line 733
    move-object/from16 v0, p0

    iget-object v2, v0, Lfaq;->mLocationOracle:Lfdr;

    invoke-interface {v2}, Lfdr;->zH()Landroid/location/Location;

    move-result-object v5

    .line 734
    invoke-direct/range {p0 .. p0}, Lfaq;->axc()J

    move-result-wide v6

    .line 735
    new-instance v2, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;

    invoke-direct {v2}, Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;-><init>()V

    const/4 v3, 0x0

    invoke-static {v5, v6, v7, v2, v3}, Lfbi;->a(Landroid/location/Location;JLcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator$APriori;Z)Lfbi;

    move-result-object v3

    .line 743
    new-instance v2, Lizk;

    invoke-direct {v2}, Lizk;-><init>()V

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v2

    check-cast v2, Lizk;

    .line 744
    iget-object v4, v3, Lfbi;->cnR:Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;

    new-instance v8, Lfbj;

    invoke-direct {v8, v3, v4}, Lfbj;-><init>(Lfbi;Lcom/google/android/sidekick/main/trigger/TriggerConditionEvaluator;)V

    .line 745
    move-object/from16 v0, p1

    iget-object v3, v0, Lizk;->dUB:[Lizj;

    invoke-static {v3, v8}, Leqh;->a([Ljava/lang/Object;Lifw;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lizj;

    iput-object v3, v2, Lizk;->dUB:[Lizj;

    .line 748
    move-object/from16 v0, p1

    iget-object v3, v0, Lizk;->dUC:[Lizj;

    invoke-static {v3, v8}, Leqh;->a([Ljava/lang/Object;Lifw;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lizj;

    iput-object v3, v2, Lizk;->dUC:[Lizj;

    .line 754
    move-object/from16 v0, p0

    iget-object v8, v0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v8

    .line 757
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lfaq;->cnp:Lizo;

    if-nez v3, :cond_1

    .line 758
    monitor-exit v8

    .line 795
    :cond_0
    :goto_0
    return-void

    .line 760
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lfaq;->cnp:Lizo;

    new-instance v4, Lizo;

    invoke-direct {v4}, Lizo;-><init>()V

    invoke-static {v3, v4}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v3

    check-cast v3, Lizo;

    .line 761
    invoke-static {}, Lijm;->aWY()Lijm;

    move-result-object v9

    const/4 v4, 0x0

    iget-object v10, v2, Lizk;->dUB:[Lizj;

    array-length v10, v10

    if-lez v10, :cond_2

    new-instance v4, Lfaz;

    move-object/from16 v0, p0

    iget-object v10, v0, Lfaq;->cmM:Lfba;

    iget-object v11, v2, Lizk;->dUB:[Lizj;

    invoke-static {v11}, Lfaq;->a([Lizj;)Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {v4, v10, v11, v9, v12}, Lfaz;-><init>(Lfba;Ljava/lang/Iterable;Ljava/util/Map;Z)V

    invoke-virtual {v4, v3}, Lfaz;->i(Lizo;)V

    :cond_2
    iget-object v10, v2, Lizk;->dUC:[Lizj;

    array-length v10, v10

    if-lez v10, :cond_3

    new-instance v10, Lfaz;

    move-object/from16 v0, p0

    iget-object v11, v0, Lfaq;->cmM:Lfba;

    iget-object v2, v2, Lizk;->dUC:[Lizj;

    invoke-static {v2}, Lfaq;->a([Lizj;)Ljava/util/List;

    move-result-object v2

    const/4 v12, 0x1

    invoke-direct {v10, v11, v2, v9, v12}, Lfaz;-><init>(Lfba;Ljava/lang/Iterable;Ljava/util/Map;Z)V

    invoke-virtual {v10, v3}, Lfaz;->i(Lizo;)V

    :cond_3
    if-eqz v4, :cond_e

    iget-object v2, v4, Lfaz;->cnI:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_e

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v4, Lfaz;->cnI:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lizj;

    move-object v4, v0

    invoke-virtual {v4}, Lizj;->bdd()Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lfaq;->cnq:[B

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lfaq;->cnq:[B

    invoke-virtual {v4, v2}, Lizj;->af([B)Lizj;

    :cond_5
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Lizj;->hu(Z)Lizj;

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lizj;->ht(Z)Lizj;

    new-instance v2, Lfax;

    invoke-direct {v2, v4}, Lfax;-><init>(Lizj;)V

    iget-object v11, v4, Lizj;->dSu:Ljdc;

    if-nez v11, :cond_6

    iget-object v11, v4, Lizj;->dTS:Ljho;

    if-eqz v11, :cond_4

    :cond_6
    iget-object v2, v2, Lfax;->cnD:Lifq;

    invoke-virtual {v4}, Lizj;->bcQ()J

    move-result-wide v12

    invoke-virtual {v2}, Lifq;->isPresent()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-virtual {v2}, Lifq;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljdb;

    iget-object v11, v2, Ljdb;->ebi:[J

    array-length v14, v11

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v14, :cond_9

    aget-wide v16, v11, v2

    cmp-long v15, v16, v12

    if-nez v15, :cond_8

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_a

    const/4 v2, 0x1

    :goto_4
    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lfaq;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->Sh()Lfcr;

    move-result-object v2

    new-instance v11, Liwk;

    invoke-direct {v11}, Liwk;-><init>()V

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Liwk;->nb(I)Liwk;

    move-result-object v11

    invoke-interface {v2, v4, v11}, Lfcr;->d(Lizj;Liwk;)Z

    move-result v2

    :cond_7
    if-nez v2, :cond_4

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 766
    :catchall_0
    move-exception v2

    monitor-exit v8

    throw v2

    .line 761
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_9
    const/4 v2, 0x0

    goto :goto_3

    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    :cond_b
    :try_start_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_e

    iget-object v4, v3, Lizo;->dUQ:Lizq;

    iget-object v2, v4, Lizq;->dUW:[Lizq;

    invoke-static {v2}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lizj;

    new-instance v11, Lizq;

    invoke-direct {v11}, Lizq;-><init>()V

    const/4 v12, 0x1

    new-array v12, v12, [Lizj;

    const/4 v13, 0x0

    aput-object v2, v12, v13

    iput-object v12, v11, Lizq;->dUX:[Lizj;

    const/4 v2, 0x0

    invoke-interface {v10, v2, v11}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_5

    :cond_c
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lizq;

    invoke-interface {v10, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lizq;

    iput-object v2, v4, Lizq;->dUW:[Lizq;

    const/4 v2, 0x1

    .line 762
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lfaq;->mClock:Lemp;

    invoke-interface {v4}, Lemp;->currentTimeMillis()J

    move-result-wide v10

    move-object/from16 v0, p0

    iput-wide v10, v0, Lfaq;->cnt:J

    .line 763
    move-object/from16 v0, p0

    iput-object v3, v0, Lfaq;->cnp:Lizo;

    .line 764
    move-object/from16 v0, p0

    iget-object v3, v0, Lfaq;->cnp:Lizo;

    invoke-static {v3, v5, v6, v7}, Lfaq;->b(Lizo;Landroid/location/Location;J)Lfbi;

    move-result-object v3

    .line 766
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 769
    if-eqz v2, :cond_d

    .line 771
    move-object/from16 v0, p0

    iget-object v2, v0, Lfaq;->cmM:Lfba;

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual {v2, v4, v5}, Lfba;->g(Landroid/os/Bundle;I)V

    .line 775
    :cond_d
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lfaq;->a(Lfbi;)V

    .line 778
    move-object/from16 v0, p0

    iget-object v2, v0, Lfaq;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axU()Lfjo;

    move-result-object v2

    invoke-virtual {v2}, Lfjo;->aAt()V

    .line 781
    invoke-direct/range {p0 .. p0}, Lfaq;->awU()V

    .line 785
    move-object/from16 v0, p0

    iget-object v2, v0, Lfaq;->mPredictiveCardsPreferences:Lcxs;

    invoke-virtual {v2}, Lcxs;->TJ()Ljel;

    move-result-object v2

    .line 786
    if-eqz v2, :cond_f

    invoke-virtual {v2}, Ljel;->TB()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    const/4 v2, 0x1

    .line 788
    :goto_7
    if-nez v2, :cond_0

    .line 793
    move-object/from16 v0, p0

    iget-object v2, v0, Lfaq;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axO()Lfga;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lfaq;->a(Lfga;)V

    goto/16 :goto_0

    .line 761
    :cond_e
    const/4 v2, 0x0

    goto :goto_6

    .line 786
    :cond_f
    const/4 v2, 0x0

    goto :goto_7
.end method

.method public final a(Lizn;ILandroid/location/Location;Ljava/util/Locale;Z[BZ)V
    .locals 13
    .param p3    # Landroid/location/Location;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/Locale;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 907
    if-eqz p3, :cond_2

    invoke-static/range {p3 .. p3}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v5

    .line 910
    :goto_0
    iget-object v2, p0, Lfaq;->mClock:Lemp;

    invoke-interface {v2}, Lemp;->currentTimeMillis()J

    move-result-wide v6

    .line 913
    invoke-static {p1}, Leqh;->f(Ljsr;)Ljsr;

    move-result-object v4

    check-cast v4, Lizn;

    move-object v3, p0

    move v8, p2

    move-object/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    .line 914
    invoke-virtual/range {v3 .. v12}, Lfaq;->a(Lizn;Ljbp;JILjava/util/Locale;Z[BZ)V

    .line 917
    iget-object v2, p0, Lfaq;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axO()Lfga;

    move-result-object v2

    .line 922
    invoke-virtual {p0, v2}, Lfaq;->a(Lfga;)V

    .line 929
    new-instance v2, Lamf;

    invoke-direct {v2}, Lamf;-><init>()V

    invoke-virtual {v2, v6, v7}, Lamf;->o(J)Lamf;

    move-result-object v2

    invoke-static {p2}, Lflk;->iT(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Lamf;->aD(Z)Lamf;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v0}, Lamf;->aE(Z)Lamf;

    move-result-object v2

    .line 934
    if-eqz p3, :cond_0

    .line 935
    invoke-static/range {p3 .. p3}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v3

    iput-object v3, v2, Lamf;->aeB:Ljbp;

    .line 938
    iput-object p1, v2, Lamf;->aeC:Lizn;

    .line 941
    :cond_0
    if-eqz p4, :cond_1

    .line 942
    invoke-virtual/range {p4 .. p4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamf;->P(Ljava/lang/String;)Lamf;

    .line 945
    :cond_1
    invoke-static {v2}, Ljsr;->m(Ljsr;)[B

    move-result-object v2

    .line 946
    iget-object v3, p0, Lfaq;->mAsyncFileStorage:Lfbz;

    const-string v4, "entry_provider"

    invoke-interface {v3, v4, v2}, Lfbz;->g(Ljava/lang/String;[B)V

    .line 947
    return-void

    .line 907
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final a(Lizn;Landroid/location/Location;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 957
    iget-object v0, p0, Lfaq;->mEntryTreePruner:Lfbh;

    invoke-virtual {v0, p1}, Lfbh;->d(Lizn;)V

    .line 958
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lfaq;->mClock:Lemp;

    invoke-interface {v1}, Lemp;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    .line 962
    iget-object v4, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v4

    .line 967
    :try_start_0
    iget-object v0, p0, Lfaq;->cnp:Lizo;

    if-nez v0, :cond_0

    monitor-exit v4

    .line 1002
    :goto_0
    return-void

    .line 971
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lizn;->dUI:[Lizo;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p1, Lizn;->dUI:[Lizo;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Lizo;->dUQ:Lizq;

    if-eqz v0, :cond_1

    .line 974
    iget-object v0, p0, Lfaq;->cnp:Lizo;

    new-instance v1, Lizo;

    invoke-direct {v1}, Lizo;-><init>()V

    invoke-static {v0, v1}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizo;

    .line 975
    iget-object v1, p1, Lizn;->dUI:[Lizo;

    const/4 v5, 0x0

    aget-object v1, v1, v5

    .line 976
    invoke-static {v1, p2, v2, v3}, Lfaq;->a(Lizo;Landroid/location/Location;J)V

    .line 977
    iget-object v1, p1, Lizn;->dUI:[Lizo;

    const/4 v5, 0x0

    aget-object v1, v1, v5

    iget-object v1, v1, Lizo;->dUQ:Lizq;

    .line 979
    iget-object v5, v0, Lizo;->dUQ:Lizq;

    iget-object v6, v0, Lizo;->dUQ:Lizq;

    iget-object v6, v6, Lizq;->dUW:[Lizq;

    iget-object v1, v1, Lizq;->dUW:[Lizq;

    invoke-static {v6, v1}, Leqh;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lizq;

    iput-object v1, v5, Lizq;->dUW:[Lizq;

    .line 982
    iput-object v0, p0, Lfaq;->cnp:Lizo;

    .line 983
    iget-object v0, p0, Lfaq;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lfaq;->cnt:J

    .line 987
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfaq;->cnr:Z

    .line 988
    iget-object v0, p0, Lfaq;->cnp:Lizo;

    invoke-static {v0, p2, v2, v3}, Lfaq;->b(Lizo;Landroid/location/Location;J)Lfbi;

    move-result-object v0

    .line 990
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 993
    invoke-virtual {p0, v0}, Lfaq;->a(Lfbi;)V

    .line 996
    if-eqz p1, :cond_2

    iget-object v0, p1, Lizn;->dUI:[Lizo;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 997
    iget-object v0, p0, Lfaq;->cmM:Lfba;

    iget-object v1, p1, Lizn;->dUI:[Lizo;

    aget-object v1, v1, v7

    invoke-virtual {v0, v1}, Lfba;->e(Lizo;)V

    .line 1001
    :cond_2
    iget-object v0, p0, Lfaq;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axU()Lfjo;

    move-result-object v0

    invoke-virtual {v0}, Lfjo;->aAt()V

    goto :goto_0

    .line 990
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method final a(Lizn;Ljbp;JILjava/util/Locale;Z[BZ)V
    .locals 13
    .param p6    # Ljava/util/Locale;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # [B
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1014
    iget-object v2, p0, Lfaq;->mEntryTreePruner:Lfbh;

    invoke-virtual {v2, p1}, Lfbh;->d(Lizn;)V

    .line 1015
    invoke-static {p2}, Lgay;->d(Ljbp;)Landroid/location/Location;

    move-result-object v5

    .line 1017
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    .line 1020
    iget-object v8, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v8

    .line 1021
    :try_start_0
    iget-object v2, p1, Lizn;->dUK:[Ljcn;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1022
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lfaq;->cnv:Ljava/util/List;

    .line 1023
    iget-object v2, p1, Lizn;->dUL:[Lixc;

    array-length v2, v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    .line 1024
    :goto_0
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    iget-object v3, p1, Lizn;->dUK:[Ljcn;

    array-length v3, v3

    if-ge v4, v3, :cond_2

    .line 1025
    iget-object v3, p1, Lizn;->dUK:[Ljcn;

    aget-object v9, v3, v4

    .line 1026
    if-eqz v2, :cond_1

    iget-object v3, p1, Lizn;->dUL:[Lixc;

    aget-object v3, v3, v4

    .line 1029
    :goto_2
    iget-object v10, p0, Lfaq;->cnv:Ljava/util/List;

    new-instance v11, Lfcq;

    invoke-direct {v11, v9, v3}, Lfcq;-><init>(Ljcn;Lixc;)V

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1024
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1023
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1026
    :cond_1
    new-instance v3, Lixc;

    invoke-direct {v3}, Lixc;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1049
    :catchall_0
    move-exception v2

    monitor-exit v8

    throw v2

    .line 1034
    :cond_2
    :try_start_1
    iget-object v2, p1, Lizn;->dUI:[Lizo;

    array-length v2, v2

    if-lez v2, :cond_4

    iget-object v2, p1, Lizn;->dUI:[Lizo;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    .line 1036
    :goto_3
    invoke-static {v2, v5, v6, v7}, Lfaq;->a(Lizo;Landroid/location/Location;J)V

    .line 1037
    iput-object v2, p0, Lfaq;->cnp:Lizo;

    .line 1039
    move-object/from16 v0, p8

    iput-object v0, p0, Lfaq;->cnq:[B

    .line 1040
    invoke-static/range {p5 .. p5}, Lflk;->iT(I)Z

    move-result v2

    iput-boolean v2, p0, Lfaq;->cnr:Z

    .line 1041
    move-wide/from16 v0, p3

    iput-wide v0, p0, Lfaq;->cns:J

    .line 1042
    iput-object p2, p0, Lfaq;->cnu:Ljbp;

    .line 1043
    move-object/from16 v0, p6

    iput-object v0, p0, Lfaq;->cnw:Ljava/util/Locale;

    .line 1044
    move/from16 v0, p9

    iput-boolean v0, p0, Lfaq;->cnx:Z

    .line 1047
    iget-object v2, p0, Lfaq;->cnp:Lizo;

    invoke-static {v2, v5, v6, v7}, Lfaq;->b(Lizo;Landroid/location/Location;J)Lfbi;

    move-result-object v2

    .line 1049
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1052
    invoke-virtual {p0, v2}, Lfaq;->a(Lfbi;)V

    .line 1055
    const/4 v2, 0x0

    .line 1056
    if-eqz p7, :cond_3

    .line 1057
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1058
    const-string v3, "reminder_updated"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1060
    :cond_3
    iget-object v3, p0, Lfaq;->cmM:Lfba;

    move/from16 v0, p5

    invoke-virtual {v3, v2, v0}, Lfba;->g(Landroid/os/Bundle;I)V

    .line 1063
    iget-object v2, p0, Lfaq;->mSidekickInjector:Lfdb;

    invoke-virtual {v2}, Lfdb;->axU()Lfjo;

    move-result-object v2

    invoke-virtual {v2}, Lfjo;->aAt()V

    .line 1064
    return-void

    .line 1034
    :cond_4
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public final awS()V
    .locals 4

    .prologue
    .line 225
    iget-object v0, p0, Lfaq;->mTaskRunnerExecutors:Lerm;

    new-instance v1, Lfas;

    const-string v2, "Initialize from storage"

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, p0, v2, v3}, Lfas;-><init>(Lfaq;Ljava/lang/String;[I)V

    invoke-interface {v0, v1}, Lerm;->a(Leri;)V

    .line 232
    return-void

    .line 225
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method final awT()V
    .locals 4

    .prologue
    .line 236
    iget-object v0, p0, Lfaq;->mClock:Lemp;

    .line 239
    iget-object v1, p0, Lfaq;->mPredictiveCardsPreferences:Lcxs;

    invoke-virtual {v1}, Lcxs;->TG()Z

    move-result v1

    if-nez v1, :cond_0

    .line 241
    iget-object v0, p0, Lfaq;->cnm:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 242
    const/16 v0, 0x1f

    invoke-virtual {p0, v0}, Lfaq;->iu(I)V

    .line 309
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v1, p0, Lfaq;->mAsyncFileStorage:Lfbz;

    const-string v2, "entry_provider"

    new-instance v3, Lfat;

    invoke-direct {v3, p0, v0}, Lfat;-><init>(Lfaq;Lemp;)V

    invoke-interface {v1, v2, v3}, Lfbz;->a(Ljava/lang/String;Lifg;)V

    goto :goto_0
.end method

.method public final awV()Z
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lfaq;->cnm:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final awW()Lizo;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 482
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 483
    :try_start_0
    iget-object v0, p0, Lfaq;->cnp:Lizo;

    new-instance v2, Lizo;

    invoke-direct {v2}, Lizo;-><init>()V

    invoke-static {v0, v2}, Leqh;->d(Ljsr;Ljsr;)Ljsr;

    move-result-object v0

    check-cast v0, Lizo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final awX()Lizo;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 497
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 498
    :try_start_0
    iget-object v0, p0, Lfaq;->cnp:Lizo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 499
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final awY()Z
    .locals 2

    .prologue
    .line 506
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 507
    :try_start_0
    iget-boolean v0, p0, Lfaq;->cnr:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 508
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final awZ()V
    .locals 3

    .prologue
    .line 606
    iget-object v1, p0, Lfaq;->cno:Ljava/lang/Object;

    monitor-enter v1

    .line 607
    :try_start_0
    iget-object v0, p0, Lfaq;->cny:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lfaq;->cny:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 610
    const/4 v0, 0x0

    iput-object v0, p0, Lfaq;->cny:Ljava/util/concurrent/ScheduledFuture;

    .line 612
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axa()J
    .locals 4

    .prologue
    .line 650
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 651
    :try_start_0
    iget-wide v2, p0, Lfaq;->cns:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 652
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axb()J
    .locals 6

    .prologue
    .line 663
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 664
    :try_start_0
    iget-wide v2, p0, Lfaq;->cns:J

    iget-wide v4, p0, Lfaq;->cnt:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 665
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axd()I
    .locals 2

    .prologue
    .line 1342
    invoke-virtual {p0}, Lfaq;->awX()Lizo;

    move-result-object v0

    .line 1345
    if-eqz v0, :cond_0

    iget-object v1, v0, Lizo;->dUQ:Lizq;

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 1346
    :goto_0
    return v0

    :cond_1
    iget-object v0, v0, Lizo;->dUQ:Lizq;

    invoke-direct {p0, v0}, Lfaq;->b(Lizq;)I

    move-result v0

    goto :goto_0
.end method

.method public final axe()Ljava/util/List;
    .locals 2

    .prologue
    .line 1388
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 1389
    :try_start_0
    iget-object v0, p0, Lfaq;->cnv:Ljava/util/List;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1390
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axf()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1398
    invoke-virtual {p0}, Lfaq;->axg()Ljbp;

    move-result-object v1

    .line 1400
    if-nez v1, :cond_1

    .line 1410
    :cond_0
    :goto_0
    return v0

    .line 1402
    :cond_1
    iget-object v2, p0, Lfaq;->mLocationOracle:Lfdr;

    invoke-interface {v2}, Lfdr;->ayy()Landroid/location/Location;

    move-result-object v2

    .line 1403
    if-eqz v2, :cond_0

    .line 1405
    invoke-static {v1}, Lgay;->d(Ljbp;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v2, v1}, Lgay;->d(Landroid/location/Location;Landroid/location/Location;)F

    move-result v1

    .line 1408
    iget-object v2, p0, Lfaq;->mConfiguration:Lcjs;

    invoke-virtual {v2}, Lcjs;->LO()I

    move-result v2

    .line 1410
    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final axg()Ljbp;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1452
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 1453
    :try_start_0
    iget-object v0, p0, Lfaq;->cnu:Ljbp;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1454
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axh()Z
    .locals 2

    .prologue
    .line 1694
    iget-object v1, p0, Lfaq;->cno:Ljava/lang/Object;

    monitor-enter v1

    .line 1695
    :try_start_0
    iget-object v0, p0, Lfaq;->cny:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1696
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final axi()Z
    .locals 6

    .prologue
    .line 1707
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 1711
    :try_start_0
    invoke-virtual {p0}, Lfaq;->axa()J

    move-result-wide v2

    .line 1712
    iget-object v0, p0, Lfaq;->mClock:Lemp;

    invoke-interface {v0}, Lemp;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {p0}, Lfaq;->axj()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 1713
    invoke-virtual {p0}, Lfaq;->invalidate()V

    .line 1714
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1716
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 1717
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final axj()J
    .locals 4

    .prologue
    .line 1724
    iget-object v0, p0, Lfaq;->mConfiguration:Lcjs;

    invoke-virtual {v0}, Lcjs;->Ms()I

    move-result v0

    .line 1725
    if-gez v0, :cond_1

    .line 1726
    iget-object v0, p0, Lfaq;->cnj:Lfdg;

    invoke-virtual {v0}, Lfdg;->ayq()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfaq;->mConfiguration:Lcjs;

    invoke-virtual {v0}, Lcjs;->LM()I

    move-result v0

    .line 1729
    :goto_0
    mul-int/lit8 v0, v0, 0x2

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    .line 1731
    :goto_1
    return-wide v0

    .line 1726
    :cond_0
    iget-object v0, p0, Lfaq;->mConfiguration:Lcjs;

    invoke-virtual {v0}, Lcjs;->LL()I

    move-result v0

    goto :goto_0

    .line 1731
    :cond_1
    int-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    goto :goto_1
.end method

.method public final b(Lizo;)V
    .locals 12

    .prologue
    .line 677
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    .line 678
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v2

    .line 679
    iget-object v1, p0, Lfaq;->mLocationOracle:Lfdr;

    invoke-interface {v1}, Lfdr;->zH()Landroid/location/Location;

    move-result-object v3

    .line 680
    invoke-direct {p0}, Lfaq;->axc()J

    move-result-wide v4

    .line 682
    invoke-static {p1, v3, v4, v5}, Lfaq;->a(Lizo;Landroid/location/Location;J)V

    .line 683
    iget-object v1, p1, Lizo;->dUQ:Lizq;

    .line 684
    if-eqz v1, :cond_0

    .line 685
    new-instance v0, Lfay;

    invoke-direct {v0}, Lfay;-><init>()V

    .line 686
    invoke-virtual {v0, p1}, Lfay;->j(Lizo;)V

    .line 687
    iget-object v0, v0, Lfay;->cnE:Ljava/util/List;

    .line 690
    :cond_0
    iget-object v6, p1, Lizo;->dUS:[Lizp;

    array-length v7, v6

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v7, :cond_2

    aget-object v8, v6, v1

    .line 691
    iget-object v9, v8, Lizp;->dRY:Ljie;

    if-eqz v9, :cond_1

    iget-object v9, v8, Lizp;->afA:Ljbj;

    if-eqz v9, :cond_1

    .line 693
    iget-object v9, v8, Lizp;->afA:Ljbj;

    .line 694
    invoke-virtual {v9}, Ljbj;->nK()I

    move-result v9

    const/4 v10, 0x3

    if-eq v9, v10, :cond_1

    .line 695
    iget-object v8, v8, Lizp;->dRY:Ljie;

    .line 696
    invoke-virtual {v8}, Ljie;->bmU()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 697
    invoke-virtual {v8}, Ljie;->bmT()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v2, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 690
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 703
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 704
    :cond_3
    new-instance v1, Lfaz;

    iget-object v6, p0, Lfaq;->cmM:Lfba;

    const/4 v7, 0x1

    invoke-direct {v1, v6, v0, v2, v7}, Lfaz;-><init>(Lfba;Ljava/lang/Iterable;Ljava/util/Map;Z)V

    invoke-direct {p0, v3, v4, v5, v1}, Lfaq;->a(Landroid/location/Location;JLfbk;)V

    .line 708
    :cond_4
    return-void
.end method

.method public final c(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 1415
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 1416
    :try_start_0
    invoke-static {p1}, Lgay;->o(Landroid/location/Location;)Ljbp;

    move-result-object v0

    iput-object v0, p0, Lfaq;->cnu:Ljbp;

    .line 1417
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Lizo;)V
    .locals 2

    .prologue
    .line 1441
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 1442
    :try_start_0
    iput-object p1, p0, Lfaq;->cnp:Lizo;

    .line 1443
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d(ILandroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1678
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfaq;->mAppContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/sidekick/main/entry/EntriesRefreshIntentService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1679
    const-string v1, "com.google.android.apps.sidekick.REFRESH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1681
    const-string v1, "com.google.android.apps.sidekick.TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1683
    const-string v1, "com.google.android.apps.sidekick.TRACE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1684
    if-eqz p2, :cond_0

    .line 1685
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1687
    :cond_0
    iget-object v1, p0, Lfaq;->mAppContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1688
    return-void
.end method

.method public final e(Ljava/util/Locale;)Z
    .locals 2
    .param p1    # Ljava/util/Locale;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1424
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 1426
    :try_start_0
    iget-object v0, p0, Lfaq;->cnw:Ljava/util/Locale;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfaq;->cnw:Ljava/util/Locale;

    invoke-virtual {v0, p1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1427
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h(Ljava/lang/Iterable;)V
    .locals 7

    .prologue
    .line 717
    invoke-static {p1}, Likm;->v(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 718
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    .line 719
    iget-object v1, p0, Lfaq;->mLocationOracle:Lfdr;

    invoke-interface {v1}, Lfdr;->zH()Landroid/location/Location;

    move-result-object v1

    .line 720
    invoke-direct {p0}, Lfaq;->axc()J

    move-result-wide v2

    .line 721
    new-instance v4, Lfaz;

    iget-object v5, p0, Lfaq;->cmM:Lfba;

    const/4 v6, 0x1

    invoke-direct {v4, v5, p1, v0, v6}, Lfaz;-><init>(Lfba;Ljava/lang/Iterable;Ljava/util/Map;Z)V

    invoke-direct {p0, v1, v2, v3, v4}, Lfaq;->a(Landroid/location/Location;JLfbk;)V

    .line 725
    :cond_0
    return-void
.end method

.method public final invalidate()V
    .locals 4

    .prologue
    .line 515
    iget-object v0, p0, Lfaq;->mNetworkClient:Lfcx;

    invoke-interface {v0}, Lfcx;->awo()Z

    move-result v0

    if-nez v0, :cond_0

    .line 539
    :goto_0
    return-void

    .line 520
    :cond_0
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 521
    const-wide/16 v2, 0x0

    :try_start_0
    iput-wide v2, p0, Lfaq;->cns:J

    .line 522
    const/4 v0, 0x0

    iput-object v0, p0, Lfaq;->cnp:Lizo;

    .line 523
    const/4 v0, 0x0

    iput-object v0, p0, Lfaq;->cnq:[B

    .line 524
    const/4 v0, 0x0

    iput-object v0, p0, Lfaq;->cnv:Ljava/util/List;

    .line 525
    const/4 v0, 0x0

    iput-object v0, p0, Lfaq;->cnu:Ljbp;

    .line 526
    const/4 v0, 0x0

    iput-object v0, p0, Lfaq;->cnw:Ljava/util/Locale;

    .line 527
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    iget-object v0, p0, Lfaq;->cmM:Lfba;

    invoke-virtual {v0}, Lfba;->notifyInvalidated()V

    .line 533
    iget-object v0, p0, Lfaq;->mSidekickInjector:Lfdb;

    invoke-virtual {v0}, Lfdb;->axU()Lfjo;

    move-result-object v0

    invoke-virtual {v0}, Lfjo;->aAt()V

    .line 538
    iget-object v0, p0, Lfaq;->mAsyncFileStorage:Lfbz;

    const-string v1, "entry_provider"

    invoke-interface {v0, v1}, Lfbz;->lH(Ljava/lang/String;)V

    goto :goto_0

    .line 527
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final iu(I)V
    .locals 1

    .prologue
    .line 546
    invoke-virtual {p0}, Lfaq;->invalidate()V

    .line 548
    invoke-virtual {p0}, Lfaq;->awZ()V

    .line 549
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lfaq;->d(ILandroid/os/Bundle;)V

    .line 550
    return-void
.end method

.method public final iv(I)V
    .locals 3

    .prologue
    .line 559
    invoke-virtual {p0}, Lfaq;->awZ()V

    .line 561
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 563
    invoke-virtual {p0}, Lfaq;->awY()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 564
    const-string v1, "com.google.android.apps.sidekick.TYPE"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 571
    :goto_0
    invoke-virtual {p0, p1, v0}, Lfaq;->d(ILandroid/os/Bundle;)V

    .line 572
    return-void

    .line 567
    :cond_0
    const-string v1, "com.google.android.apps.sidekick.TYPE"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final iw(I)V
    .locals 6

    .prologue
    .line 579
    invoke-virtual {p0}, Lfaq;->invalidate()V

    .line 582
    iget-object v1, p0, Lfaq;->cno:Ljava/lang/Object;

    monitor-enter v1

    .line 583
    :try_start_0
    new-instance v0, Lfaw;

    const/16 v2, 0x20

    invoke-direct {v0, p0, v2}, Lfaw;-><init>(Lfaq;I)V

    .line 584
    invoke-virtual {p0}, Lfaq;->awZ()V

    .line 585
    iget-object v2, p0, Lfaq;->mTaskRunnerExecutors:Lerm;

    invoke-interface {v2}, Lerm;->auv()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    const-wide/16 v4, 0x2710

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v0, v4, v5, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lfaq;->cny:Ljava/util/concurrent/ScheduledFuture;

    .line 587
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ix(I)V
    .locals 2

    .prologue
    .line 595
    iget-object v1, p0, Lfaq;->cno:Ljava/lang/Object;

    monitor-enter v1

    .line 596
    :try_start_0
    iget-object v0, p0, Lfaq;->cny:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 597
    invoke-virtual {p0, p1}, Lfaq;->iu(I)V

    .line 599
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final iy(I)V
    .locals 1

    .prologue
    .line 1468
    iget-object v0, p0, Lfaq;->cmM:Lfba;

    invoke-virtual {v0, p1}, Lfba;->iA(I)V

    .line 1469
    return-void
.end method

.method public final iz(I)V
    .locals 1

    .prologue
    .line 1475
    iget-object v0, p0, Lfaq;->cmM:Lfba;

    invoke-virtual {v0, p1}, Lfba;->iB(I)V

    .line 1476
    return-void
.end method

.method public final mP()Z
    .locals 2

    .prologue
    .line 1434
    iget-object v1, p0, Lfaq;->cnn:Ljava/lang/Object;

    monitor-enter v1

    .line 1435
    :try_start_0
    iget-boolean v0, p0, Lfaq;->cnx:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 1436
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final s(Ljava/util/Collection;)V
    .locals 3

    .prologue
    .line 316
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Liqs;->mm(I)Ljava/util/HashSet;

    move-result-object v1

    .line 323
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    .line 324
    if-eqz v0, :cond_2

    .line 325
    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 330
    :cond_3
    new-instance v0, Lfbf;

    iget-object v2, p0, Lfaq;->cmM:Lfba;

    invoke-direct {v0, v2, v1}, Lfbf;-><init>(Lfba;Ljava/util/Collection;)V

    invoke-direct {p0, v0}, Lfaq;->a(Lfbk;)V

    goto :goto_0
.end method
