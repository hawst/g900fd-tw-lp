.class public final Lgat;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final cEv:[Ljava/lang/String;

.field public static final cEw:[Ljava/lang/String;


# instance fields
.field private aTr:Ljava/lang/String;

.field private bLI:Ljava/lang/String;

.field private cAf:Z

.field private cEx:Z

.field private cEy:[Ljava/lang/String;

.field private cjp:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "*"

    aput-object v1, v0, v2

    .line 26
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "mail.google."

    aput-object v1, v0, v2

    sput-object v0, Lgat;->cEv:[Ljava/lang/String;

    .line 27
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "wallet.google."

    aput-object v1, v0, v2

    sput-object v0, Lgat;->cEw:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-boolean v0, p0, Lgat;->cAf:Z

    .line 36
    iput-boolean v0, p0, Lgat;->cEx:Z

    return-void
.end method


# virtual methods
.method public final N(Landroid/net/Uri;)Lgat;
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lgat;->cjp:Landroid/net/Uri;

    .line 41
    return-object p0
.end method

.method public final aEc()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 70
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lgat;->cjp:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 71
    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.sidekick.main.GoogleServiceWebviewWrapper"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v1, "enable_javascript"

    iget-boolean v2, p0, Lgat;->cAf:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 73
    const-string v1, "enable_zoom_controls"

    iget-boolean v2, p0, Lgat;->cEx:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 75
    iget-object v1, p0, Lgat;->aTr:Ljava/lang/String;

    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    const-string v1, "webview_service"

    iget-object v2, p0, Lgat;->aTr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    :cond_0
    iget-object v1, p0, Lgat;->bLI:Ljava/lang/String;

    invoke-static {v1}, Ligh;->pu(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 80
    const-string v1, "webview_title"

    iget-object v2, p0, Lgat;->bLI:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    :cond_1
    iget-object v1, p0, Lgat;->cEy:[Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgat;->cEy:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_2

    .line 84
    const-string v1, "webview_url_prefixes"

    iget-object v2, p0, Lgat;->cEy:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    :cond_2
    return-object v0
.end method

.method public final fC(Z)Lgat;
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lgat;->cAf:Z

    .line 56
    return-object p0
.end method

.method public final fD(Z)Lgat;
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Lgat;->cEx:Z

    .line 61
    return-object p0
.end method

.method public final mj(Ljava/lang/String;)Lgat;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lgat;->aTr:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method public final mk(Ljava/lang/String;)Lgat;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lgat;->bLI:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method public final s([Ljava/lang/String;)Lgat;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lgat;->cEy:[Ljava/lang/String;

    .line 66
    return-object p0
.end method
