.class public final Ljqz;
.super Ljsl;
.source "PG"


# static fields
.field public static final ezM:Ljsm;


# instance fields
.field private aez:I

.field private eqC:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 6406
    const/16 v0, 0xb

    const-class v1, Ljqz;

    const v2, 0x1f67c82a

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljqz;->ezM:Ljsm;

    .line 6412
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6441
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6442
    const/4 v0, 0x0

    iput v0, p0, Ljqz;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljqz;->eqC:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljqz;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqz;->eCz:I

    .line 6443
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 6399
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqz;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqz;->eqC:Ljava/lang/String;

    iget v0, p0, Ljqz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqz;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6456
    iget v0, p0, Ljqz;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6457
    const/4 v0, 0x1

    iget-object v1, p0, Ljqz;->eqC:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6459
    :cond_0
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6460
    return-void
.end method

.method public final aiw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6422
    iget-object v0, p0, Ljqz;->eqC:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6464
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6465
    iget v1, p0, Ljqz;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6466
    const/4 v1, 0x1

    iget-object v2, p0, Ljqz;->eqC:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6469
    :cond_0
    return v0
.end method

.method public final yr(Ljava/lang/String;)Ljqz;
    .locals 1

    .prologue
    .line 6425
    if-nez p1, :cond_0

    .line 6426
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6428
    :cond_0
    iput-object p1, p0, Ljqz;->eqC:Ljava/lang/String;

    .line 6429
    iget v0, p0, Ljqz;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqz;->aez:I

    .line 6430
    return-object p0
.end method
