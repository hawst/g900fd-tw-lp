.class public final enum Lgba;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cEA:Lgba;

.field public static final enum cEB:Lgba;

.field public static final enum cEC:Lgba;

.field private static final synthetic cEF:[Lgba;

.field public static final enum cEz:Lgba;


# instance fields
.field private final cED:Ljava/lang/String;

.field private final cEE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 118
    new-instance v0, Lgba;

    const-string v1, "DRIVING"

    const-string v2, "http://maps.google.com/maps/?myl=saddr&daddr=%s&entry=r&dirflg=d"

    const-string v3, "d"

    invoke-direct {v0, v1, v4, v2, v3}, Lgba;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lgba;->cEz:Lgba;

    .line 119
    new-instance v0, Lgba;

    const-string v1, "WALKING"

    const-string v2, "http://maps.google.com/maps/?myl=saddr&daddr=%s&entry=r&dirflg=w"

    const-string v3, "w"

    invoke-direct {v0, v1, v5, v2, v3}, Lgba;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lgba;->cEA:Lgba;

    .line 120
    new-instance v0, Lgba;

    const-string v1, "TRANSIT"

    const-string v2, "http://maps.google.com/maps/?myl=saddr&daddr=%s&entry=r&dirflg=r"

    const-string v3, "r"

    invoke-direct {v0, v1, v6, v2, v3}, Lgba;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lgba;->cEB:Lgba;

    .line 121
    new-instance v0, Lgba;

    const-string v1, "BIKING"

    const-string v2, "http://maps.google.com/maps/?myl=saddr&daddr=%s&entry=r&dirflg=b"

    const-string v3, "b"

    invoke-direct {v0, v1, v7, v2, v3}, Lgba;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lgba;->cEC:Lgba;

    .line 117
    const/4 v0, 0x4

    new-array v0, v0, [Lgba;

    sget-object v1, Lgba;->cEz:Lgba;

    aput-object v1, v0, v4

    sget-object v1, Lgba;->cEA:Lgba;

    aput-object v1, v0, v5

    sget-object v1, Lgba;->cEB:Lgba;

    aput-object v1, v0, v6

    sget-object v1, Lgba;->cEC:Lgba;

    aput-object v1, v0, v7

    sput-object v0, Lgba;->cEF:[Lgba;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 127
    iput-object p3, p0, Lgba;->cED:Ljava/lang/String;

    .line 128
    iput-object p4, p0, Lgba;->cEE:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public static jQ(I)Lgba;
    .locals 2

    .prologue
    .line 140
    packed-switch p0, :pswitch_data_0

    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown travel type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :pswitch_0
    sget-object v0, Lgba;->cEz:Lgba;

    .line 148
    :goto_0
    return-object v0

    .line 144
    :pswitch_1
    sget-object v0, Lgba;->cEB:Lgba;

    goto :goto_0

    .line 146
    :pswitch_2
    sget-object v0, Lgba;->cEA:Lgba;

    goto :goto_0

    .line 148
    :pswitch_3
    sget-object v0, Lgba;->cEC:Lgba;

    goto :goto_0

    .line 140
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lgba;
    .locals 1

    .prologue
    .line 117
    const-class v0, Lgba;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgba;

    return-object v0
.end method

.method public static values()[Lgba;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lgba;->cEF:[Lgba;

    invoke-virtual {v0}, [Lgba;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgba;

    return-object v0
.end method


# virtual methods
.method final aEd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lgba;->cED:Ljava/lang/String;

    return-object v0
.end method

.method final aEe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lgba;->cEE:Ljava/lang/String;

    return-object v0
.end method
