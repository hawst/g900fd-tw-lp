.class public final Ljpi;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile exH:[Ljpi;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private ajk:Ljava/lang/String;

.field private exI:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3914
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 3915
    iput v1, p0, Ljpi;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljpi;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljpi;->ajk:Ljava/lang/String;

    iput v1, p0, Ljpi;->exI:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljpi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpi;->eCz:I

    .line 3916
    return-void
.end method

.method public static brU()[Ljpi;
    .locals 2

    .prologue
    .line 3838
    sget-object v0, Ljpi;->exH:[Ljpi;

    if-nez v0, :cond_1

    .line 3839
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 3841
    :try_start_0
    sget-object v0, Ljpi;->exH:[Ljpi;

    if-nez v0, :cond_0

    .line 3842
    const/4 v0, 0x0

    new-array v0, v0, [Ljpi;

    sput-object v0, Ljpi;->exH:[Ljpi;

    .line 3844
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3846
    :cond_1
    sget-object v0, Ljpi;->exH:[Ljpi;

    return-object v0

    .line 3844
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 3832
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpi;->agq:Ljava/lang/String;

    iget v0, p0, Ljpi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljpi;->ajk:Ljava/lang/String;

    iget v0, p0, Ljpi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpi;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljpi;->exI:I

    iget v0, p0, Ljpi;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljpi;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 3931
    iget v0, p0, Ljpi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 3932
    const/4 v0, 0x1

    iget-object v1, p0, Ljpi;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3934
    :cond_0
    iget v0, p0, Ljpi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 3935
    const/4 v0, 0x2

    iget-object v1, p0, Ljpi;->ajk:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 3937
    :cond_1
    iget v0, p0, Ljpi;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 3938
    const/4 v0, 0x3

    iget v1, p0, Ljpi;->exI:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 3940
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 3941
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 3945
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 3946
    iget v1, p0, Ljpi;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 3947
    const/4 v1, 0x1

    iget-object v2, p0, Ljpi;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3950
    :cond_0
    iget v1, p0, Ljpi;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 3951
    const/4 v1, 0x2

    iget-object v2, p0, Ljpi;->ajk:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3954
    :cond_1
    iget v1, p0, Ljpi;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 3955
    const/4 v1, 0x3

    iget v2, p0, Ljpi;->exI:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3958
    :cond_2
    return v0
.end method
