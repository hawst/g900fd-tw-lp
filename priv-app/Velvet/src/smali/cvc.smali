.class public abstract Lcvc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcxt;


# instance fields
.field private final bkP:Ljava/util/Map;

.field private bkQ:Landroid/preference/PreferenceScreen;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    .line 158
    return-void
.end method

.method private g(Landroid/preference/Preference;)Lcxt;
    .locals 3

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lcvc;->e(Landroid/preference/Preference;)Ljava/lang/String;

    move-result-object v1

    .line 95
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxt;

    .line 96
    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0, p1}, Lcvc;->f(Landroid/preference/Preference;)Lcxt;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_0

    .line 99
    iget-object v2, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :cond_0
    return-object v0
.end method


# virtual methods
.method public a(Landroid/preference/Preference;)V
    .locals 3

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcvc;->g(Landroid/preference/Preference;)Lcxt;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_1

    .line 141
    invoke-interface {v0, p1}, Lcxt;->a(Landroid/preference/Preference;)V

    .line 151
    :cond_0
    return-void

    .line 142
    :cond_1
    instance-of v0, p1, Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_3

    .line 143
    check-cast p1, Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcvc;->d(Landroid/preference/Preference;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v1}, Lcvc;->a(Landroid/preference/Preference;)V

    goto :goto_1

    .line 146
    :cond_3
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    new-instance v0, Lcvd;

    invoke-direct {v0, p1}, Lcvd;-><init>(Landroid/preference/Preference;)V

    throw v0
.end method

.method public a(Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcvc;->bkQ:Landroid/preference/PreferenceScreen;

    .line 156
    return-void
.end method

.method public d(Landroid/preference/Preference;)Z
    .locals 2

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    instance-of v1, p1, Landroid/preference/PreferenceGroup;

    if-nez v1, :cond_1

    .line 110
    invoke-direct {p0, p1}, Lcvc;->g(Landroid/preference/Preference;)Lcxt;

    move-result-object v0

    move-object v1, v0

    .line 112
    :goto_0
    const/4 v0, 0x0

    .line 113
    if-eqz v1, :cond_0

    .line 114
    invoke-interface {v1, p1}, Lcxt;->d(Landroid/preference/Preference;)Z

    move-result v0

    .line 117
    :cond_0
    return v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method protected e(Landroid/preference/Preference;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract f(Landroid/preference/Preference;)Lcxt;
.end method

.method public final i(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxt;

    .line 45
    invoke-interface {v0, p1}, Lcxt;->i(Landroid/os/Bundle;)V

    goto :goto_0

    .line 47
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxt;

    .line 81
    invoke-interface {v0}, Lcxt;->onDestroy()V

    goto :goto_0

    .line 83
    :cond_0
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 84
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxt;

    .line 67
    invoke-interface {v0}, Lcxt;->onPause()V

    goto :goto_0

    .line 69
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxt;

    .line 59
    iget-object v2, p0, Lcvc;->bkQ:Landroid/preference/PreferenceScreen;

    invoke-interface {v0, v2}, Lcxt;->a(Landroid/preference/PreferenceScreen;)V

    .line 60
    invoke-interface {v0}, Lcxt;->onResume()V

    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxt;

    .line 89
    invoke-interface {v0, p1}, Lcxt;->onSaveInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxt;

    .line 52
    invoke-interface {v0}, Lcxt;->onStart()V

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcvc;->bkP:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxt;

    .line 74
    invoke-interface {v0}, Lcxt;->onStop()V

    goto :goto_0

    .line 76
    :cond_0
    return-void
.end method
