.class public abstract Lbir;
.super Ljava/lang/Object;

# interfaces
.implements Lbha;
.implements Lbjb;


# instance fields
.field private final aBk:Landroid/os/Looper;

.field private final aBu:Lbiz;

.field private aCR:Landroid/os/IInterface;

.field private final aCS:Ljava/util/ArrayList;

.field private aCT:Lbiw;

.field private volatile aCU:I

.field private aCV:Z

.field public final mContext:Landroid/content/Context;

.field final mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbir;->aCS:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, Lbir;->aCU:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbir;->aCV:Z

    invoke-static {p1}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbir;->mContext:Landroid/content/Context;

    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, Lbjr;->f(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lbir;->aBk:Landroid/os/Looper;

    new-instance v0, Lbiz;

    invoke-direct {v0, p2, p0}, Lbiz;-><init>(Landroid/os/Looper;Lbjb;)V

    iput-object v0, p0, Lbir;->aBu:Lbiz;

    new-instance v0, Lbis;

    invoke-direct {v0, p0, p2}, Lbis;-><init>(Lbir;Landroid/os/Looper;)V

    iput-object v0, p0, Lbir;->mHandler:Landroid/os/Handler;

    invoke-static {p3}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhk;

    iget-object v1, p0, Lbir;->aBu:Lbiz;

    invoke-virtual {v1, v0}, Lbiz;->a(Lbhk;)V

    invoke-static {p4}, Lbjr;->ap(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhl;

    invoke-virtual {p0, v0}, Lbir;->a(Lbhl;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lbgp;Lbgq;[Ljava/lang/String;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lbiu;

    invoke-direct {v3, p2}, Lbiu;-><init>(Lbgp;)V

    new-instance v4, Lbix;

    invoke-direct {v4, p3}, Lbix;-><init>(Lbgq;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbir;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lbir;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, Lbir;->aCR:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(Lbir;Lbiw;)Lbiw;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbir;->aCT:Lbiw;

    return-object v0
.end method

.method static synthetic a(Lbir;)Lbiz;
    .locals 1

    iget-object v0, p0, Lbir;->aBu:Lbiz;

    return-object v0
.end method

.method static synthetic a(Lbir;I)V
    .locals 0

    invoke-direct {p0, p1}, Lbir;->dK(I)V

    return-void
.end method

.method static synthetic b(Lbir;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lbir;->aCS:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lbir;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lbir;->aCR:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic d(Lbir;)Lbiw;
    .locals 1

    iget-object v0, p0, Lbir;->aCT:Lbiw;

    return-object v0
.end method

.method private dK(I)V
    .locals 1

    iget v0, p0, Lbir;->aCU:I

    iput p1, p0, Lbir;->aCU:I

    return-void
.end method

.method static synthetic e(Lbir;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lbir;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lbir;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lbir;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lbiy;

    invoke-direct {v3, p0, p1, p2, p3}, Lbiy;-><init>(Lbir;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Lbhl;)V
    .locals 1

    iget-object v0, p0, Lbir;->aBu:Lbiz;

    invoke-virtual {v0, p1}, Lbiz;->a(Lbgq;)V

    return-void
.end method

.method public final a(Lbit;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v1, p0, Lbir;->aCS:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lbir;->aCS:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbir;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lbir;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract a(Lbjm;Lbiv;)V
.end method

.method protected abstract c(Landroid/os/IBinder;)Landroid/os/IInterface;
.end method

.method public final connect()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lbir;->aCV:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lbir;->dK(I)V

    iget-object v0, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lbgt;->T(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lbir;->dK(I)V

    iget-object v1, p0, Lbir;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lbir;->mHandler:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbir;->aCT:Lbiw;

    if-eqz v0, :cond_2

    const-string v0, "GmsClient"

    const-string v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lbir;->aCR:Landroid/os/IInterface;

    iget-object v0, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lbjc;->V(Landroid/content/Context;)Lbjc;

    move-result-object v0

    invoke-virtual {p0}, Lbir;->yQ()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbir;->aCT:Lbiw;

    invoke-virtual {v0, v1, v2}, Lbjc;->b(Ljava/lang/String;Lbiw;)V

    :cond_2
    new-instance v0, Lbiw;

    invoke-direct {v0, p0}, Lbiw;-><init>(Lbir;)V

    iput-object v0, p0, Lbir;->aCT:Lbiw;

    iget-object v0, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lbjc;->V(Landroid/content/Context;)Lbjc;

    move-result-object v0

    invoke-virtual {p0}, Lbir;->yQ()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbir;->aCT:Lbiw;

    invoke-virtual {v0, v1, v2}, Lbjc;->a(Ljava/lang/String;Lbiw;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lbir;->yQ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lbir;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lbir;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected final d(Landroid/os/IBinder;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lbjn;->g(Landroid/os/IBinder;)Lbjm;

    move-result-object v0

    new-instance v1, Lbiv;

    invoke-direct {v1, p0}, Lbiv;-><init>(Lbir;)V

    invoke-virtual {p0, v0, v1}, Lbir;->a(Lbjm;Lbiv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GmsClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final dL(I)V
    .locals 4

    iget-object v0, p0, Lbir;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lbir;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public disconnect()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbir;->aCV:Z

    iget-object v2, p0, Lbir;->aCS:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lbir;->aCS:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lbir;->aCS:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbit;

    invoke-virtual {v0}, Lbit;->yW()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbir;->aCS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbir;->dK(I)V

    iput-object v4, p0, Lbir;->aCR:Landroid/os/IInterface;

    iget-object v0, p0, Lbir;->aCT:Lbiw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lbjc;->V(Landroid/content/Context;)Lbjc;

    move-result-object v0

    invoke-virtual {p0}, Lbir;->yQ()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbir;->aCT:Lbiw;

    invoke-virtual {v0, v1, v2}, Lbjc;->b(Ljava/lang/String;Lbiw;)V

    iput-object v4, p0, Lbir;->aCT:Lbiw;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lbir;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public final getLooper()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lbir;->aBk:Landroid/os/Looper;

    return-object v0
.end method

.method public final isConnected()Z
    .locals 2

    iget v0, p0, Lbir;->aCU:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isConnecting()Z
    .locals 2

    iget v0, p0, Lbir;->aCU:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final yD()Z
    .locals 1

    iget-boolean v0, p0, Lbir;->aCV:Z

    return v0
.end method

.method public yE()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract yQ()Ljava/lang/String;
.end method

.method protected abstract yR()Ljava/lang/String;
.end method

.method public final yS()V
    .locals 2

    invoke-virtual {p0}, Lbir;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final yT()Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0}, Lbir;->yS()V

    iget-object v0, p0, Lbir;->aCR:Landroid/os/IInterface;

    return-object v0
.end method
