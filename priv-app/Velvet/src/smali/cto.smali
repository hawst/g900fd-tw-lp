.class public final Lcto;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final biy:Z


# instance fields
.field private final biz:Lcub;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    sget-boolean v0, Lcub;->biy:Z

    sput-boolean v0, Lcto;->biy:Z

    return-void
.end method

.method constructor <init>(Lcub;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput-object p1, p0, Lcto;->biz:Lcub;

    .line 148
    return-void
.end method

.method static A(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 259
    const-string v0, "ALTER TABLE contacts ADD COLUMN phone_number_types TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 261
    const-string v0, "ALTER TABLE contacts ADD COLUMN phone_number_labels TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 263
    return-void
.end method

.method static B(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 266
    const-string v0, "ALTER TABLE contacts ADD COLUMN emails_types TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 268
    const-string v0, "ALTER TABLE contacts ADD COLUMN emails_labels TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 270
    const-string v0, "ALTER TABLE contacts ADD COLUMN postal_address_types TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 272
    const-string v0, "ALTER TABLE contacts ADD COLUMN postal_address_labels TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 274
    return-void
.end method

.method static D(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 284
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 285
    const-string v1, "emails_labels"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 286
    const-string v1, "emails_types"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 287
    const-string v1, "phone_number_labels"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 288
    const-string v1, "phone_number_types"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 289
    const-string v1, "postal_address_labels"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 290
    const-string v1, "postal_address_types"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 291
    const-string v1, "contacts"

    invoke-virtual {p0, v1, v0, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 292
    return-void
.end method

.method static E(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 295
    const-string v0, "ALTER TABLE contacts ADD COLUMN phonetic_name TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 297
    return-void
.end method

.method private static SR()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 300
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJs()Lchr;

    move-result-object v0

    invoke-virtual {v0}, Lchr;->Kt()Lcyg;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;ZLjava/util/Collection;)I
    .locals 24

    .prologue
    .line 320
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    const-string v5, "Can not do a delta update with filterContactIds"

    invoke-static {v4, v5}, Lifv;->c(ZLjava/lang/Object;)V

    .line 323
    if-nez p3, :cond_2

    if-nez p4, :cond_2

    const/4 v4, 0x1

    move v6, v4

    .line 325
    :goto_1
    invoke-static {}, Lcto;->SR()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "key_last_contacts_delta_delete_timestamp"

    const-wide/16 v8, 0x0

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 327
    invoke-static {}, Lcto;->SR()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "key_last_contacts_delta_update_timestamp"

    const-wide/16 v10, 0x0

    invoke-interface {v4, v5, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 329
    if-eqz p3, :cond_3

    sget-boolean v4, Lcub;->biy:Z

    if-nez v4, :cond_3

    .line 330
    const-string v4, "IcingContactsHelper"

    const-string v5, "Delta update requested but no delta API present"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x5

    invoke-static {v7, v4, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 331
    const/4 v4, 0x0

    .line 508
    :goto_2
    return v4

    .line 320
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 323
    :cond_2
    const/4 v4, 0x0

    move v6, v4

    goto :goto_1

    .line 334
    :cond_3
    if-eqz p3, :cond_4

    invoke-static {}, Lcto;->SR()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "key_last_contacts_delta_update_timestamp"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 335
    const/16 p3, 0x0

    .line 336
    const-string v4, "IcingContactsHelper"

    const-string v5, "Delta update with no prior full sync - doing full sync instead."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x4

    invoke-static {v12, v4, v5, v7}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 339
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 344
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 349
    if-eqz p3, :cond_6

    .line 351
    move-object/from16 v0, p0

    iget-object v4, v0, Lcto;->biz:Lcub;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v10, v11}, Lcub;->a(Landroid/content/res/Resources;J)Landroid/util/Pair;

    move-result-object v5

    .line 353
    iget-object v4, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcts;

    .line 354
    iget-object v5, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    .line 356
    move-object/from16 v0, p0

    iget-object v5, v0, Lcto;->biz:Lcub;

    invoke-virtual {v5, v8, v9, v14}, Lcub;->a(JLjava/util/Set;)J

    move-result-wide v8

    move-object v5, v4

    .line 375
    :goto_3
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 377
    :cond_5
    :goto_4
    :try_start_0
    invoke-interface {v5}, Lcts;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 378
    invoke-interface {v5}, Lcts;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lctp;

    .line 379
    iget-object v7, v4, Lctp;->biA:Landroid/content/ContentValues;

    .line 380
    invoke-static {v7}, Lcto;->d(Landroid/content/ContentValues;)J

    move-result-wide v12

    .line 381
    invoke-static {v7}, Lcto;->d(Landroid/content/ContentValues;)J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-nez v16, :cond_8

    const/4 v7, 0x0

    :goto_5
    if-eqz v7, :cond_5

    .line 382
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v15, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 387
    :catchall_0
    move-exception v4

    invoke-interface {v5}, Lcts;->close()V

    throw v4

    .line 358
    :cond_6
    if-eqz p4, :cond_7

    .line 360
    move-object/from16 v0, p0

    iget-object v4, v0, Lcto;->biz:Lcub;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v4, v0, v1}, Lcub;->a(Landroid/content/res/Resources;Ljava/util/Collection;)Lcts;

    move-result-object v4

    .line 364
    move-object/from16 v0, p4

    invoke-interface {v14, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object v5, v4

    goto :goto_3

    .line 367
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcto;->biz:Lcub;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcub;->d(Landroid/content/res/Resources;)Landroid/util/Pair;

    move-result-object v5

    .line 369
    iget-object v4, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcts;

    .line 370
    iget-object v5, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    move-wide v10, v8

    move-object v5, v4

    .line 371
    goto :goto_3

    .line 381
    :cond_8
    :try_start_1
    const-string v16, "lookup_key"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    if-nez v16, :cond_9

    const/4 v7, 0x0

    goto :goto_5

    :cond_9
    const-string v16, "display_name"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    if-nez v7, :cond_a

    const/4 v7, 0x0

    goto :goto_5

    :cond_a
    const/4 v7, 0x1

    goto :goto_5

    .line 387
    :cond_b
    invoke-interface {v5}, Lcts;->close()V

    .line 390
    invoke-interface {v5}, Lcts;->SS()Z

    move-result v4

    if-nez v4, :cond_c

    .line 392
    const/4 v4, -0x1

    goto/16 :goto_2

    .line 395
    :cond_c
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 396
    const/4 v13, 0x0

    .line 397
    const/4 v12, 0x0

    .line 398
    :try_start_2
    invoke-interface {v15}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 403
    if-eqz v6, :cond_e

    .line 405
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "contact_id NOT IN ("

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, ","

    invoke-static {v7, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 410
    :goto_6
    const-string v7, "contacts"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v4, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x0

    .line 412
    const-string v14, "emails"

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v14, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v14

    add-int/2addr v7, v14

    .line 413
    const-string v14, "phones"

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v14, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v14

    add-int/2addr v7, v14

    .line 414
    const-string v14, "postals"

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v14, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v7, v4

    .line 420
    if-eqz v6, :cond_f

    .line 422
    const/4 v4, 0x0

    .line 427
    :goto_7
    new-instance v6, Lcup;

    move-object/from16 v0, p1

    invoke-direct {v6, v0, v4}, Lcup;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 429
    new-instance v14, Lcua;

    const-string v4, "contacts"

    const-string v5, "contact_id"

    invoke-direct {v14, v4, v5}, Lcua;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    new-instance v16, Lcua;

    const-string v4, "emails"

    const-string v5, "data_id"

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v5}, Lcua;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    new-instance v17, Lcua;

    const-string v4, "phones"

    const-string v5, "data_id"

    move-object/from16 v0, v17

    invoke-direct {v0, v4, v5}, Lcua;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    new-instance v18, Lcua;

    const-string v4, "postals"

    const-string v5, "data_id"

    move-object/from16 v0, v18

    invoke-direct {v0, v4, v5}, Lcua;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 435
    :goto_8
    :try_start_3
    invoke-interface {v6}, Lcts;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 436
    invoke-interface {v6}, Lcts;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lctp;

    .line 437
    iget-object v0, v4, Lctp;->biA:Landroid/content/ContentValues;

    move-object/from16 v19, v0

    .line 438
    invoke-static/range {v19 .. v19}, Lcto;->d(Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 440
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v15, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lctp;

    .line 441
    iget-object v0, v5, Lctp;->biA:Landroid/content/ContentValues;

    move-object/from16 v22, v0

    .line 442
    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_d

    .line 444
    iget-object v0, v14, Lcua;->biJ:Ljava/util/Map;

    move-object/from16 v19, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    :cond_d
    iget-object v0, v5, Lctp;->biB:Ljava/util/Map;

    move-object/from16 v19, v0

    iget-object v0, v4, Lctp;->biB:Ljava/util/Map;

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcto;->a(Lcua;Ljava/util/Map;Ljava/util/Map;)V

    .line 447
    iget-object v0, v5, Lctp;->biC:Ljava/util/Map;

    move-object/from16 v19, v0

    iget-object v0, v4, Lctp;->biC:Ljava/util/Map;

    move-object/from16 v22, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcto;->a(Lcua;Ljava/util/Map;Ljava/util/Map;)V

    .line 448
    iget-object v5, v5, Lctp;->biD:Ljava/util/Map;

    iget-object v4, v4, Lctp;->biD:Ljava/util/Map;

    move-object/from16 v0, v18

    invoke-static {v0, v5, v4}, Lcto;->a(Lcua;Ljava/util/Map;Ljava/util/Map;)V

    .line 451
    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v15, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_8

    .line 454
    :catchall_1
    move-exception v4

    :try_start_4
    invoke-interface {v6}, Lcts;->close()V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 500
    :catchall_2
    move-exception v4

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4

    .line 408
    :cond_e
    :try_start_5
    const-string v4, "contact_id"

    invoke-static {v4, v14}, Lcto;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    .line 425
    :cond_f
    const-string v4, "contact_id"

    invoke-static {v4, v5}, Lcto;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_7

    .line 454
    :cond_10
    invoke-interface {v6}, Lcts;->close()V

    .line 458
    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-static {v14, v0, v1, v2}, Lijp;->c(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lijp;

    move-result-object v4

    invoke-virtual {v4}, Lijp;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move v5, v13

    move v13, v7

    move v7, v12

    :goto_9
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcua;

    .line 460
    iget-object v0, v4, Lcua;->biF:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 463
    iget-object v6, v4, Lcua;->biH:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v12, v5

    :goto_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_12

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ContentValues;

    .line 465
    const/16 v17, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v18

    .line 466
    const-wide/16 v20, -0x1

    cmp-long v5, v18, v20

    if-nez v5, :cond_11

    const/4 v5, 0x0

    :goto_b
    add-int/2addr v5, v12

    move v12, v5

    .line 467
    goto :goto_a

    .line 466
    :cond_11
    const/4 v5, 0x1

    goto :goto_b

    .line 468
    :cond_12
    iget-object v5, v4, Lcua;->biJ:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move v6, v7

    :goto_c
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 470
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v4, Lcua;->biG:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v18, "="

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 471
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/ContentValues;

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v5, v7, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    add-int/2addr v5, v6

    move v6, v5

    .line 472
    goto :goto_c

    .line 474
    :cond_13
    iget-object v5, v4, Lcua;->biG:Ljava/lang/String;

    iget-object v4, v4, Lcua;->biI:Ljava/util/Set;

    invoke-static {v5, v4}, Lcto;->a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    add-int/2addr v4, v13

    move v13, v4

    move v7, v6

    move v5, v12

    .line 476
    goto/16 :goto_9

    .line 479
    :cond_14
    invoke-interface {v15}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v6, v5

    :goto_d
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lctp;

    .line 480
    iget-object v5, v4, Lctp;->biA:Landroid/content/ContentValues;

    .line 482
    const-string v14, "contacts"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v14

    .line 483
    const-wide/16 v16, -0x1

    cmp-long v5, v14, v16

    if-nez v5, :cond_15

    const/4 v5, 0x0

    :goto_e
    add-int/2addr v5, v6

    .line 484
    const-string v6, "emails"

    iget-object v14, v4, Lctp;->biB:Ljava/util/Map;

    invoke-interface {v14}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v6, v14}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/Iterable;)I

    move-result v6

    add-int/2addr v5, v6

    .line 485
    const-string v6, "phones"

    iget-object v14, v4, Lctp;->biC:Ljava/util/Map;

    invoke-interface {v14}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v6, v14}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/Iterable;)I

    move-result v6

    add-int/2addr v5, v6

    .line 486
    const-string v6, "postals"

    iget-object v4, v4, Lctp;->biD:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v6, v4}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/Iterable;)I

    move-result v4

    add-int/2addr v4, v5

    move v6, v4

    .line 487
    goto :goto_d

    .line 483
    :cond_15
    const/4 v5, 0x1

    goto :goto_e

    .line 494
    :cond_16
    invoke-static {}, Lcto;->SR()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "key_last_contacts_delta_update_timestamp"

    invoke-interface {v4, v5, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "key_last_contacts_delta_delete_timestamp"

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 498
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 500
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 502
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 508
    add-int v4, v6, v7

    add-int/2addr v4, v13

    goto/16 :goto_2
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/Iterable;)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 512
    .line 513
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 515
    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 516
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v2

    :goto_1
    add-int/2addr v0, v1

    move v1, v0

    .line 517
    goto :goto_0

    .line 516
    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    .line 518
    :cond_1
    return v1
.end method

.method static a(JJLjava/lang/String;ILjava/lang/String;I)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 625
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 626
    const-string v1, "contact_id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 627
    const-string v1, "data_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 628
    const-string v1, "email"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const-string v1, "type"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 630
    const-string v1, "label"

    invoke-virtual {v0, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    const-string v1, "score"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 632
    return-object v0
.end method

.method static a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 605
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 606
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 607
    const-string v2, "contact_id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 608
    const-string v2, "lookup_key"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    const-string v2, "icon_uri"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const-string v2, "display_name"

    invoke-virtual {v1, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    const-string v2, "given_names"

    invoke-virtual {v1, v2, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    const-string v2, "score"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 613
    const-string v2, "emails"

    invoke-virtual {v1, v2, p8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const-string v2, "nickname"

    invoke-virtual {v1, v2, p9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string v2, "note"

    invoke-virtual {v1, v2, p10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    const-string v2, "organization"

    invoke-virtual {v1, v2, p11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const-string v2, "phone_numbers"

    move-object/from16 v0, p12

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const-string v2, "postal_address"

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    const-string v2, "phonetic_name"

    move-object/from16 v0, p14

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    return-object v1
.end method

.method static a(Ljava/lang/String;Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-static {v1, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcua;Ljava/util/Map;Ljava/util/Map;)V
    .locals 6

    .prologue
    .line 523
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 524
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 525
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 526
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 527
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 528
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 529
    iget-object v1, p0, Lcua;->biH:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 530
    :cond_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 531
    iget-object v1, p0, Lcua;->biJ:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 534
    :cond_2
    iget-object v0, p0, Lcua;->biI:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 535
    return-void
.end method

.method static b(JJLjava/lang/String;ILjava/lang/String;I)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 637
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 638
    const-string v1, "contact_id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 639
    const-string v1, "data_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 640
    const-string v1, "phone"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    const-string v1, "type"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 642
    const-string v1, "label"

    invoke-virtual {v0, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    const-string v1, "score"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 644
    return-object v0
.end method

.method static c(JJLjava/lang/String;ILjava/lang/String;I)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 649
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 650
    const-string v1, "contact_id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 651
    const-string v1, "data_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 652
    const-string v1, "postal"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const-string v1, "type"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 654
    const-string v1, "label"

    invoke-virtual {v0, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    const-string v1, "score"

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 656
    return-object v0
.end method

.method private static d(Landroid/content/ContentValues;)J
    .locals 2

    .prologue
    .line 589
    const-string v0, "contact_id"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 590
    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 577
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT COUNT(*) FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 579
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 583
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 585
    :goto_0
    return v0

    .line 583
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 585
    const/4 v0, -0x1

    goto :goto_0

    .line 583
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static l(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 249
    const-string v0, "DROP TABLE IF EXISTS contacts_appdatasearch_seqno_table"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 250
    const-string v0, "DROP TRIGGER IF EXISTS contacts_appdatasearch_insert_trigger"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 252
    const-string v0, "DROP TRIGGER IF EXISTS contacts_appdatasearch_delete_trigger"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 254
    const-string v0, "DROP TRIGGER IF EXISTS contacts_appdatasearch_update_trigger"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 256
    return-void
.end method

.method private static u(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 185
    const-string v0, "DROP TABLE IF EXISTS emails"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 186
    const-string v0, "CREATE TABLE emails (_id INTEGER PRIMARY KEY AUTOINCREMENT,contact_id INTEGER,data_id INTEGER,email TEXT,label TEXT,type INTEGER,score INTEGER)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 194
    return-void
.end method

.method private static v(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 197
    const-string v0, "DROP TABLE IF EXISTS phones"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 198
    const-string v0, "CREATE TABLE phones (_id INTEGER PRIMARY KEY AUTOINCREMENT,contact_id INTEGER,data_id INTEGER,phone TEXT,label TEXT,type INTEGER,score INTEGER)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 206
    return-void
.end method

.method private static w(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 209
    const-string v0, "DROP TABLE IF EXISTS postals"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 210
    const-string v0, "CREATE TABLE postals (_id INTEGER PRIMARY KEY AUTOINCREMENT,contact_id INTEGER,data_id INTEGER,postal TEXT,label TEXT,type INTEGER,score INTEGER)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method static x(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 221
    const-string v0, "DROP TABLE IF EXISTS contacts"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 222
    const-string v0, "CREATE TABLE contacts (_id INTEGER PRIMARY KEY AUTOINCREMENT,contact_id INTEGER,lookup_key TEXT,icon_uri TEXT,display_name TEXT,times_contacted TEXT,emails TEXT,nickname TEXT,note TEXT,organization TEXT,phone_numbers TEXT,postal_address TEXT)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method static y(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 238
    const-string v0, "ALTER TABLE contacts ADD COLUMN score INTEGER"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method static z(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 243
    const-string v0, "ALTER TABLE contacts ADD COLUMN given_names TEXT"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 245
    return-void
.end method


# virtual methods
.method final C(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 277
    invoke-static {p1}, Lcto;->u(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 278
    invoke-static {p1}, Lcto;->v(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 279
    invoke-static {p1}, Lcto;->w(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 280
    return-void
.end method

.method final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 305
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;ZLjava/util/Collection;)I

    move-result v0

    return v0
.end method

.method final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;Ljava/util/Collection;)I
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;ZLjava/util/Collection;)I

    move-result v0

    return v0
.end method

.method final a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 550
    if-eqz p4, :cond_1

    const-string v0, "extensive"

    .line 551
    :goto_0
    new-array v1, v10, [Ljava/lang/Object;

    aput-object p2, v1, v8

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContactsHelper ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") state:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v9

    invoke-static {p3, v1}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 554
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Contact count: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "contacts"

    invoke-static {p1, v4}, Lcto;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {p3, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 555
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Email count: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "emails"

    invoke-static {p1, v4}, Lcto;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {p3, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 556
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Phone count: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "phones"

    invoke-static {p1, v4}, Lcto;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {p3, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 557
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Postal count: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "postals"

    invoke-static {p1, v4}, Lcto;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {p3, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 558
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v2, 0x1

    const-string v3, "Last delta update timestamp: "

    aput-object v3, v0, v2

    const/4 v2, 0x2

    invoke-static {}, Lcto;->SR()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "key_last_contacts_delta_delete_timestamp"

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Leth;->bc(J)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {p3, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 561
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v2, 0x1

    const-string v3, "Last delta delete timestamp: "

    aput-object v3, v0, v2

    const/4 v2, 0x2

    invoke-static {}, Lcto;->SR()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "key_last_contacts_delta_update_timestamp"

    const-wide/16 v6, 0x0

    invoke-interface {v3, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Leth;->bc(J)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {p3, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 564
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p3, v0}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    .line 565
    if-eqz p4, :cond_0

    .line 566
    const-string v0, "contacts"

    invoke-static {p1, v1, p3, v0}, Leth;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 567
    const-string v0, "emails"

    invoke-static {p1, v1, p3, v0}, Leth;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 568
    const-string v0, "phones"

    invoke-static {p1, v1, p3, v0}, Leth;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 569
    const-string v0, "postals"

    invoke-static {p1, v1, p3, v0}, Leth;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/io/PrintWriter;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 574
    :cond_0
    :goto_1
    return-void

    .line 550
    :cond_1
    const-string v0, "simple"

    goto/16 :goto_0

    .line 571
    :catch_0
    move-exception v0

    .line 572
    new-array v2, v10, [Ljava/lang/Object;

    aput-object v1, v2, v8

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Exception while dumping state"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v9

    invoke-static {p3, v2}, Leth;->a(Ljava/io/PrintWriter;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method final b(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;)I
    .locals 2

    .prologue
    .line 315
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcto;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/res/Resources;ZLjava/util/Collection;)I

    move-result v0

    return v0
.end method

.method final t(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 151
    const-string v0, "DROP TABLE IF EXISTS contacts"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE contacts (_id INTEGER PRIMARY KEY AUTOINCREMENT,contact_id INTEGER,lookup_key TEXT,icon_uri TEXT,display_name TEXT,given_names TEXT,times_contacted TEXT,score INTEGER,emails TEXT,emails_types TEXT,emails_labels TEXT,nickname TEXT,note TEXT,organization TEXT,phone_numbers TEXT,phone_number_types TEXT,phone_number_labels TEXT,postal_address TEXT,postal_address_types TEXT,postal_address_labels TEXT,phonetic_name TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 153
    invoke-static {p1}, Lcto;->u(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 154
    invoke-static {p1}, Lcto;->v(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 155
    invoke-static {p1}, Lcto;->w(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 156
    return-void
.end method
