.class final Linh;
.super Ljava/util/AbstractQueue;
.source "PG"


# instance fields
.field final dFe:Linr;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 3156
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3157
    new-instance v0, Lini;

    invoke-direct {v0, p0}, Lini;-><init>(Linh;)V

    iput-object v0, p0, Linh;->dFe:Linr;

    return-void
.end method

.method private aXM()Linr;
    .locals 2

    .prologue
    .line 3200
    iget-object v0, p0, Linh;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXK()Linr;

    move-result-object v0

    .line 3201
    iget-object v1, p0, Linh;->dFe:Linr;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .prologue
    .line 3250
    iget-object v0, p0, Linh;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXK()Linr;

    move-result-object v0

    .line 3251
    :goto_0
    iget-object v1, p0, Linh;->dFe:Linr;

    if-eq v0, v1, :cond_0

    .line 3252
    invoke-interface {v0}, Linr;->aXK()Linr;

    move-result-object v1

    .line 3253
    invoke-static {v0}, Limn;->e(Linr;)V

    move-object v0, v1

    .line 3255
    goto :goto_0

    .line 3257
    :cond_0
    iget-object v0, p0, Linh;->dFe:Linr;

    iget-object v1, p0, Linh;->dFe:Linr;

    invoke-interface {v0, v1}, Linr;->h(Linr;)V

    .line 3258
    iget-object v0, p0, Linh;->dFe:Linr;

    iget-object v1, p0, Linh;->dFe:Linr;

    invoke-interface {v0, v1}, Linr;->i(Linr;)V

    .line 3259
    return-void
.end method

.method public final contains(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3230
    check-cast p1, Linr;

    .line 3231
    invoke-interface {p1}, Linr;->aXK()Linr;

    move-result-object v0

    sget-object v1, Linq;->dFs:Linq;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 2

    .prologue
    .line 3236
    iget-object v0, p0, Linh;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXK()Linr;

    move-result-object v0

    iget-object v1, p0, Linh;->dFe:Linr;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 3263
    new-instance v0, Linj;

    invoke-direct {p0}, Linh;->aXM()Linr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Linj;-><init>(Linh;Linr;)V

    return-object v0
.end method

.method public final synthetic offer(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3156
    check-cast p1, Linr;

    invoke-interface {p1}, Linr;->aXL()Linr;

    move-result-object v0

    invoke-interface {p1}, Linr;->aXK()Linr;

    move-result-object v1

    invoke-static {v0, v1}, Limn;->b(Linr;Linr;)V

    iget-object v0, p0, Linh;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXL()Linr;

    move-result-object v0

    invoke-static {v0, p1}, Limn;->b(Linr;Linr;)V

    iget-object v0, p0, Linh;->dFe:Linr;

    invoke-static {p1, v0}, Limn;->b(Linr;Linr;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 3156
    invoke-direct {p0}, Linh;->aXM()Linr;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 3156
    iget-object v0, p0, Linh;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXK()Linr;

    move-result-object v0

    iget-object v1, p0, Linh;->dFe:Linr;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0}, Linh;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 3218
    check-cast p1, Linr;

    .line 3219
    invoke-interface {p1}, Linr;->aXL()Linr;

    move-result-object v0

    .line 3220
    invoke-interface {p1}, Linr;->aXK()Linr;

    move-result-object v1

    .line 3221
    invoke-static {v0, v1}, Limn;->b(Linr;Linr;)V

    .line 3222
    invoke-static {p1}, Limn;->e(Linr;)V

    .line 3224
    sget-object v0, Linq;->dFs:Linq;

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 3241
    const/4 v1, 0x0

    .line 3242
    iget-object v0, p0, Linh;->dFe:Linr;

    invoke-interface {v0}, Linr;->aXK()Linr;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Linh;->dFe:Linr;

    if-eq v0, v2, :cond_0

    .line 3243
    add-int/lit8 v1, v1, 0x1

    .line 3242
    invoke-interface {v0}, Linr;->aXK()Linr;

    move-result-object v0

    goto :goto_0

    .line 3245
    :cond_0
    return v1
.end method
