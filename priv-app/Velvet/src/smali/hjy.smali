.class public abstract Lhjy;
.super Lhjs;
.source "PG"

# interfaces
.implements Lelc;


# instance fields
.field final TAG:Ljava/lang/String;

.field private aor:Lhzw;

.field private final bRf:Z

.field public final diQ:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhjy;-><init>(Landroid/content/Context;Z)V

    .line 81
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-direct {p0, p1, v0, v0}, Lhjy;-><init>(Landroid/content/Context;ZZ)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 3

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lhjs;-><init>(Landroid/content/Context;)V

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ClassicAbstractCardView."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhjy;->TAG:Ljava/lang/String;

    .line 93
    iput-boolean p2, p0, Lhjy;->bRf:Z

    .line 94
    iput-boolean p3, p0, Lhjy;->diQ:Z

    .line 95
    invoke-virtual {p0}, Lhjy;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lhjy;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, p0, v2}, Lhjy;->a(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lhjz;

    invoke-direct {v1, p0}, Lhjz;-><init>(Lhjy;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-virtual {p0, v0}, Lhjy;->addView(Landroid/view/View;)V

    .line 96
    return-void
.end method

.method public static varargs a([Landroid/widget/TextView;)V
    .locals 4

    .prologue
    .line 339
    array-length v1, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p0, v0

    .line 340
    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    :cond_0
    return-void
.end method

.method protected static b(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 185
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    return-void

    .line 186
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end method

.method public final varargs a(I[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {p0}, Lhjy;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhzw;->oR(Ljava/lang/String;)V

    .line 237
    :cond_0
    return-void
.end method

.method public final a(Lesj;)V
    .locals 1

    .prologue
    .line 410
    invoke-virtual {p0}, Lhjy;->aQg()Lhjo;

    move-result-object v0

    check-cast v0, Lhjx;

    invoke-virtual {v0, p1}, Lhjx;->b(Lesj;)V

    .line 411
    return-void
.end method

.method public final aL(J)V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p1, p2}, Lhzw;->aL(J)V

    .line 297
    :cond_0
    return-void
.end method

.method public final aQh()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lhjy;->cXh:Lhjo;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    new-instance v1, Lhka;

    invoke-direct {v1, p0}, Lhka;-><init>(Lhjy;)V

    invoke-virtual {v0, v1}, Lhzw;->a(Lhzx;)V

    .line 161
    :cond_0
    iget-object v0, p0, Lhjy;->cXh:Lhjo;

    check-cast v0, Lhjx;

    invoke-virtual {v0, p0}, Lhjx;->a(Lelc;)V

    .line 163
    :cond_1
    return-void
.end method

.method public final auj()V
    .locals 2

    .prologue
    .line 274
    invoke-static {p0}, Lehd;->aG(Landroid/view/View;)I

    move-result v0

    .line 275
    if-ltz v0, :cond_1

    .line 276
    invoke-static {}, Legs;->aoH()Legs;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Legs;->q(Landroid/view/View;I)V

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0}, Lhzw;->auj()V

    goto :goto_0
.end method

.method public final auk()V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0}, Lhzw;->auk()V

    .line 304
    :cond_0
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I)Lhzw;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-boolean v0, p0, Lhjy;->bRf:Z

    if-eqz v0, :cond_0

    .line 171
    const v0, 0x7f04003e

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhzw;

    iput-object v0, p0, Lhjy;->aor:Lhzw;

    .line 177
    :goto_0
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p3}, Lhzw;->lF(I)V

    .line 180
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    const v1, 0x7f0c008a

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    .line 181
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    return-object v0

    .line 174
    :cond_0
    const v0, 0x7f04003b

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhzw;

    iput-object v0, p0, Lhjy;->aor:Lhzw;

    goto :goto_0
.end method

.method public final cZ(I)V
    .locals 2

    .prologue
    .line 345
    invoke-virtual {p0}, Lhjy;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 346
    return-void
.end method

.method public dc(I)V
    .locals 2

    .prologue
    .line 325
    packed-switch p1, :pswitch_data_0

    .line 336
    :goto_0
    return-void

    .line 327
    :pswitch_0
    iget-object v0, p0, Lhjy;->cXh:Lhjo;

    check-cast v0, Lhjx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhjx;->gu(Z)V

    goto :goto_0

    .line 330
    :pswitch_1
    iget-object v0, p0, Lhjy;->cXh:Lhjo;

    check-cast v0, Lhjx;

    invoke-virtual {v0}, Lhjx;->tQ()V

    goto :goto_0

    .line 333
    :pswitch_2
    iget-object v0, p0, Lhjy;->cXh:Lhjo;

    check-cast v0, Lhjx;

    invoke-virtual {v0}, Lhjx;->aQd()V

    goto :goto_0

    .line 325
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final fc(Z)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p1}, Lhzw;->fc(Z)V

    .line 260
    :cond_0
    return-void
.end method

.method public final g(Lcom/google/android/shared/util/App;)V
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {p0}, Lhjy;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/shared/util/App;->ar(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lhzw;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 209
    :cond_0
    return-void
.end method

.method public final gv(Z)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p1}, Lhzw;->gv(Z)V

    .line 191
    return-void
.end method

.method public final lc(I)V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p1}, Lhzw;->lc(I)V

    .line 197
    :cond_0
    return-void
.end method

.method public final ld(I)V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p1}, Lhzw;->ld(I)V

    .line 227
    :cond_0
    return-void
.end method

.method public final le(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 247
    if-eqz p1, :cond_0

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/16 v1, 0x64

    if-lt p1, v1, :cond_2

    :cond_0
    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 250
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p1}, Lhzw;->le(I)V

    .line 253
    :cond_1
    return-void

    .line 247
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lf(I)V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p1}, Lhzw;->lG(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhjs;->diO:Z

    .line 269
    :cond_0
    return-void
.end method

.method protected final lg(I)V
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0, p1}, Lhzw;->lH(I)V

    .line 390
    return-void
.end method

.method public uD()V
    .locals 0

    .prologue
    .line 375
    invoke-virtual {p0}, Lhjy;->uh()V

    .line 376
    return-void
.end method

.method public uh()V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    invoke-virtual {v0}, Lhzw;->uh()V

    .line 366
    :cond_0
    return-void
.end method

.method public ux()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lhjy;->aor:Lhzw;

    if-eqz v0, :cond_0

    .line 351
    const v0, 0x7f0a0969

    invoke-virtual {p0, v0}, Lhjy;->lg(I)V

    .line 353
    :cond_0
    return-void
.end method
