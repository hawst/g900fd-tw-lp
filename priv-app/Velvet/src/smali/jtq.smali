.class public final Ljtq;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eDV:[Ljtq;


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private agv:I

.field private dzl:F

.field private eBD:Ljava/lang/String;

.field public eDW:Ljty;

.field private eDX:Ljtw;

.field public eDY:Ljua;

.field private eDZ:Ljtv;

.field private eEa:Ljts;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1345
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1346
    iput v0, p0, Ljtq;->aez:I

    iput v0, p0, Ljtq;->agv:I

    const-string v0, ""

    iput-object v0, p0, Ljtq;->agq:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Ljtq;->dzl:F

    const-string v0, ""

    iput-object v0, p0, Ljtq;->eBD:Ljava/lang/String;

    iput-object v1, p0, Ljtq;->eDW:Ljty;

    iput-object v1, p0, Ljtq;->eDX:Ljtw;

    iput-object v1, p0, Ljtq;->eDY:Ljua;

    iput-object v1, p0, Ljtq;->eDZ:Ljtv;

    iput-object v1, p0, Ljtq;->eEa:Ljts;

    iput-object v1, p0, Ljtq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljtq;->eCz:I

    .line 1347
    return-void
.end method

.method public static buy()[Ljtq;
    .locals 2

    .prologue
    .line 1235
    sget-object v0, Ljtq;->eDV:[Ljtq;

    if-nez v0, :cond_1

    .line 1236
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 1238
    :try_start_0
    sget-object v0, Ljtq;->eDV:[Ljtq;

    if-nez v0, :cond_0

    .line 1239
    const/4 v0, 0x0

    new-array v0, v0, [Ljtq;

    sput-object v0, Ljtq;->eDV:[Ljtq;

    .line 1241
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1243
    :cond_1
    sget-object v0, Ljtq;->eDV:[Ljtq;

    return-object v0

    .line 1241
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljtq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljtq;->agv:I

    iget v0, p0, Ljtq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljtq;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljtq;->agq:Ljava/lang/String;

    iget v0, p0, Ljtq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljtq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Ljtq;->dzl:F

    iget v0, p0, Ljtq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljtq;->aez:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljtq;->eDW:Ljty;

    if-nez v0, :cond_1

    new-instance v0, Ljty;

    invoke-direct {v0}, Ljty;-><init>()V

    iput-object v0, p0, Ljtq;->eDW:Ljty;

    :cond_1
    iget-object v0, p0, Ljtq;->eDW:Ljty;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljtq;->eDX:Ljtw;

    if-nez v0, :cond_2

    new-instance v0, Ljtw;

    invoke-direct {v0}, Ljtw;-><init>()V

    iput-object v0, p0, Ljtq;->eDX:Ljtw;

    :cond_2
    iget-object v0, p0, Ljtq;->eDX:Ljtw;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljtq;->eDY:Ljua;

    if-nez v0, :cond_3

    new-instance v0, Ljua;

    invoke-direct {v0}, Ljua;-><init>()V

    iput-object v0, p0, Ljtq;->eDY:Ljua;

    :cond_3
    iget-object v0, p0, Ljtq;->eDY:Ljua;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljtq;->eDZ:Ljtv;

    if-nez v0, :cond_4

    new-instance v0, Ljtv;

    invoke-direct {v0}, Ljtv;-><init>()V

    iput-object v0, p0, Ljtq;->eDZ:Ljtv;

    :cond_4
    iget-object v0, p0, Ljtq;->eDZ:Ljtv;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Ljtq;->eEa:Ljts;

    if-nez v0, :cond_5

    new-instance v0, Ljts;

    invoke-direct {v0}, Ljts;-><init>()V

    iput-object v0, p0, Ljtq;->eEa:Ljts;

    :cond_5
    iget-object v0, p0, Ljtq;->eEa:Ljts;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljtq;->eBD:Ljava/lang/String;

    iget v0, p0, Ljtq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljtq;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 1368
    iget v0, p0, Ljtq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1369
    const/4 v0, 0x1

    iget v1, p0, Ljtq;->agv:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 1371
    :cond_0
    iget v0, p0, Ljtq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1372
    const/4 v0, 0x2

    iget-object v1, p0, Ljtq;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1374
    :cond_1
    iget v0, p0, Ljtq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 1375
    const/4 v0, 0x3

    iget v1, p0, Ljtq;->dzl:F

    invoke-virtual {p1, v0, v1}, Ljsj;->a(IF)V

    .line 1377
    :cond_2
    iget-object v0, p0, Ljtq;->eDW:Ljty;

    if-eqz v0, :cond_3

    .line 1378
    const/4 v0, 0x4

    iget-object v1, p0, Ljtq;->eDW:Ljty;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1380
    :cond_3
    iget-object v0, p0, Ljtq;->eDX:Ljtw;

    if-eqz v0, :cond_4

    .line 1381
    const/4 v0, 0x5

    iget-object v1, p0, Ljtq;->eDX:Ljtw;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1383
    :cond_4
    iget-object v0, p0, Ljtq;->eDY:Ljua;

    if-eqz v0, :cond_5

    .line 1384
    const/4 v0, 0x6

    iget-object v1, p0, Ljtq;->eDY:Ljua;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1386
    :cond_5
    iget-object v0, p0, Ljtq;->eDZ:Ljtv;

    if-eqz v0, :cond_6

    .line 1387
    const/4 v0, 0x7

    iget-object v1, p0, Ljtq;->eDZ:Ljtv;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1389
    :cond_6
    iget-object v0, p0, Ljtq;->eEa:Ljts;

    if-eqz v0, :cond_7

    .line 1390
    const/16 v0, 0x8

    iget-object v1, p0, Ljtq;->eEa:Ljts;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 1392
    :cond_7
    iget v0, p0, Ljtq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    .line 1393
    const/16 v0, 0x9

    iget-object v1, p0, Ljtq;->eBD:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 1395
    :cond_8
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1396
    return-void
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 1400
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1401
    iget v1, p0, Ljtq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1402
    const/4 v1, 0x1

    iget v2, p0, Ljtq;->agv:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1405
    :cond_0
    iget v1, p0, Ljtq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 1406
    const/4 v1, 0x2

    iget-object v2, p0, Ljtq;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1409
    :cond_1
    iget v1, p0, Ljtq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 1410
    const/4 v1, 0x3

    iget v2, p0, Ljtq;->dzl:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1413
    :cond_2
    iget-object v1, p0, Ljtq;->eDW:Ljty;

    if-eqz v1, :cond_3

    .line 1414
    const/4 v1, 0x4

    iget-object v2, p0, Ljtq;->eDW:Ljty;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1417
    :cond_3
    iget-object v1, p0, Ljtq;->eDX:Ljtw;

    if-eqz v1, :cond_4

    .line 1418
    const/4 v1, 0x5

    iget-object v2, p0, Ljtq;->eDX:Ljtw;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1421
    :cond_4
    iget-object v1, p0, Ljtq;->eDY:Ljua;

    if-eqz v1, :cond_5

    .line 1422
    const/4 v1, 0x6

    iget-object v2, p0, Ljtq;->eDY:Ljua;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1425
    :cond_5
    iget-object v1, p0, Ljtq;->eDZ:Ljtv;

    if-eqz v1, :cond_6

    .line 1426
    const/4 v1, 0x7

    iget-object v2, p0, Ljtq;->eDZ:Ljtv;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1429
    :cond_6
    iget-object v1, p0, Ljtq;->eEa:Ljts;

    if-eqz v1, :cond_7

    .line 1430
    const/16 v1, 0x8

    iget-object v2, p0, Ljtq;->eEa:Ljts;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1433
    :cond_7
    iget v1, p0, Ljtq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_8

    .line 1434
    const/16 v1, 0x9

    iget-object v2, p0, Ljtq;->eBD:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1437
    :cond_8
    return v0
.end method

.method public final sx(I)Ljtq;
    .locals 1

    .prologue
    .line 1254
    iput p1, p0, Ljtq;->agv:I

    .line 1255
    iget v0, p0, Ljtq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljtq;->aez:I

    .line 1256
    return-object p0
.end method

.method public final yU(Ljava/lang/String;)Ljtq;
    .locals 1

    .prologue
    .line 1273
    if-nez p1, :cond_0

    .line 1274
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1276
    :cond_0
    iput-object p1, p0, Ljtq;->agq:Ljava/lang/String;

    .line 1277
    iget v0, p0, Ljtq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljtq;->aez:I

    .line 1278
    return-object p0
.end method

.method public final yV(Ljava/lang/String;)Ljtq;
    .locals 1

    .prologue
    .line 1314
    if-nez p1, :cond_0

    .line 1315
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1317
    :cond_0
    iput-object p1, p0, Ljtq;->eBD:Ljava/lang/String;

    .line 1318
    iget v0, p0, Ljtq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljtq;->aez:I

    .line 1319
    return-object p0
.end method
