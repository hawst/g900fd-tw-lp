.class public final Ljxd;
.super Ljsl;
.source "PG"


# static fields
.field public static final eKc:Ljsm;


# instance fields
.field private aez:I

.field private dzf:Ljava/lang/String;

.field private eKd:Z

.field private eKe:[I

.field private eKf:Ljoa;

.field public eKg:Ljnz;

.field private eKh:Ljob;

.field private eKi:Ljow;

.field private eKj:I

.field private eKk:J

.field private eKl:Ljava/lang/String;

.field private eKm:I

.field private eKn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xb

    const-class v1, Ljxd;

    const v2, 0xe2db382

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljxd;->eKc:Ljsm;

    .line 21
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 185
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 186
    iput v3, p0, Ljxd;->aez:I

    iput-boolean v3, p0, Ljxd;->eKd:Z

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljxd;->eKe:[I

    iput-object v2, p0, Ljxd;->eKf:Ljoa;

    iput-object v2, p0, Ljxd;->eKg:Ljnz;

    iput-object v2, p0, Ljxd;->eKh:Ljob;

    iput-object v2, p0, Ljxd;->eKi:Ljow;

    const/4 v0, 0x1

    iput v0, p0, Ljxd;->eKj:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljxd;->eKk:J

    const-string v0, ""

    iput-object v0, p0, Ljxd;->eKl:Ljava/lang/String;

    iput v3, p0, Ljxd;->eKm:I

    const-string v0, ""

    iput-object v0, p0, Ljxd;->dzf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljxd;->eKn:Ljava/lang/String;

    iput-object v2, p0, Ljxd;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljxd;->eCz:I

    .line 187
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljxd;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Ljxd;->eKd:Z

    iget v0, p0, Ljxd;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljxd;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljsi;->btN()I

    :cond_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v6

    sparse-switch v6, :sswitch_data_1

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :sswitch_3
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljxd;->eKe:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Ljxd;->eKe:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljxd;->eKe:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Ljxd;->eKe:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljxd;->eKe:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    sparse-switch v4, :sswitch_data_2

    goto :goto_4

    :sswitch_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Ljsi;->rX(I)V

    iget-object v1, p0, Ljxd;->eKe:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Ljxd;->eKe:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v5

    sparse-switch v5, :sswitch_data_3

    goto :goto_6

    :sswitch_6
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Ljxd;->eKe:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Ljxd;->eKe:[I

    :cond_a
    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ljxd;->eKi:Ljow;

    if-nez v0, :cond_b

    new-instance v0, Ljow;

    invoke-direct {v0}, Ljow;-><init>()V

    iput-object v0, p0, Ljxd;->eKi:Ljow;

    :cond_b
    iget-object v0, p0, Ljxd;->eKi:Ljow;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Ljxd;->eKj:I

    iget v0, p0, Ljxd;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljxd;->aez:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Ljxd;->eKf:Ljoa;

    if-nez v0, :cond_c

    new-instance v0, Ljoa;

    invoke-direct {v0}, Ljoa;-><init>()V

    iput-object v0, p0, Ljxd;->eKf:Ljoa;

    :cond_c
    iget-object v0, p0, Ljxd;->eKf:Ljoa;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljxd;->eKg:Ljnz;

    if-nez v0, :cond_d

    new-instance v0, Ljnz;

    invoke-direct {v0}, Ljnz;-><init>()V

    iput-object v0, p0, Ljxd;->eKg:Ljnz;

    :cond_d
    iget-object v0, p0, Ljxd;->eKg:Ljnz;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljxd;->eKk:J

    iget v0, p0, Ljxd;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljxd;->aez:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljxd;->eKl:Ljava/lang/String;

    iget v0, p0, Ljxd;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljxd;->aez:I

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Ljxd;->eKh:Ljob;

    if-nez v0, :cond_e

    new-instance v0, Ljob;

    invoke-direct {v0}, Ljob;-><init>()V

    iput-object v0, p0, Ljxd;->eKh:Ljob;

    :cond_e
    iget-object v0, p0, Ljxd;->eKh:Ljob;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljxd;->eKm:I

    iget v0, p0, Ljxd;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljxd;->aez:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljxd;->dzf:Ljava/lang/String;

    iget v0, p0, Ljxd;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljxd;->aez:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljxd;->eKn:Ljava/lang/String;

    iget v0, p0, Ljxd;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljxd;->aez:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_4
        0x1a -> :sswitch_7
        0x20 -> :sswitch_8
        0x2a -> :sswitch_9
        0x32 -> :sswitch_a
        0x38 -> :sswitch_b
        0x42 -> :sswitch_c
        0x4a -> :sswitch_d
        0x50 -> :sswitch_e
        0x5a -> :sswitch_f
        0x62 -> :sswitch_10
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0x1 -> :sswitch_3
        0x2 -> :sswitch_3
        0x3 -> :sswitch_3
        0x4 -> :sswitch_3
        0x5 -> :sswitch_3
        0x6 -> :sswitch_3
        0x7 -> :sswitch_3
        0x8 -> :sswitch_3
        0x9 -> :sswitch_3
        0xa -> :sswitch_3
        0xb -> :sswitch_3
        0xc -> :sswitch_3
        0xd -> :sswitch_3
        0xe -> :sswitch_3
        0xf -> :sswitch_3
        0x10 -> :sswitch_3
        0x11 -> :sswitch_3
        0x12 -> :sswitch_3
        0x13 -> :sswitch_3
        0x14 -> :sswitch_3
        0x75bcd15 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_5
        0x1 -> :sswitch_5
        0x2 -> :sswitch_5
        0x3 -> :sswitch_5
        0x4 -> :sswitch_5
        0x5 -> :sswitch_5
        0x6 -> :sswitch_5
        0x7 -> :sswitch_5
        0x8 -> :sswitch_5
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0xb -> :sswitch_5
        0xc -> :sswitch_5
        0xd -> :sswitch_5
        0xe -> :sswitch_5
        0xf -> :sswitch_5
        0x10 -> :sswitch_5
        0x11 -> :sswitch_5
        0x12 -> :sswitch_5
        0x13 -> :sswitch_5
        0x14 -> :sswitch_5
        0x75bcd15 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x0 -> :sswitch_6
        0x1 -> :sswitch_6
        0x2 -> :sswitch_6
        0x3 -> :sswitch_6
        0x4 -> :sswitch_6
        0x5 -> :sswitch_6
        0x6 -> :sswitch_6
        0x7 -> :sswitch_6
        0x8 -> :sswitch_6
        0x9 -> :sswitch_6
        0xa -> :sswitch_6
        0xb -> :sswitch_6
        0xc -> :sswitch_6
        0xd -> :sswitch_6
        0xe -> :sswitch_6
        0xf -> :sswitch_6
        0x10 -> :sswitch_6
        0x11 -> :sswitch_6
        0x12 -> :sswitch_6
        0x13 -> :sswitch_6
        0x14 -> :sswitch_6
        0x75bcd15 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 211
    iget v0, p0, Ljxd;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 212
    const/4 v0, 0x1

    iget-boolean v1, p0, Ljxd;->eKd:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 214
    :cond_0
    iget-object v0, p0, Ljxd;->eKe:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljxd;->eKe:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 215
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljxd;->eKe:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 216
    const/4 v1, 0x2

    iget-object v2, p0, Ljxd;->eKe:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_1
    iget-object v0, p0, Ljxd;->eKi:Ljow;

    if-eqz v0, :cond_2

    .line 220
    const/4 v0, 0x3

    iget-object v1, p0, Ljxd;->eKi:Ljow;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 222
    :cond_2
    iget v0, p0, Ljxd;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_3

    .line 223
    const/4 v0, 0x4

    iget v1, p0, Ljxd;->eKj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 225
    :cond_3
    iget-object v0, p0, Ljxd;->eKf:Ljoa;

    if-eqz v0, :cond_4

    .line 226
    const/4 v0, 0x5

    iget-object v1, p0, Ljxd;->eKf:Ljoa;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 228
    :cond_4
    iget-object v0, p0, Ljxd;->eKg:Ljnz;

    if-eqz v0, :cond_5

    .line 229
    const/4 v0, 0x6

    iget-object v1, p0, Ljxd;->eKg:Ljnz;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 231
    :cond_5
    iget v0, p0, Ljxd;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 232
    const/4 v0, 0x7

    iget-wide v2, p0, Ljxd;->eKk:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 234
    :cond_6
    iget v0, p0, Ljxd;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_7

    .line 235
    const/16 v0, 0x8

    iget-object v1, p0, Ljxd;->eKl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 237
    :cond_7
    iget-object v0, p0, Ljxd;->eKh:Ljob;

    if-eqz v0, :cond_8

    .line 238
    const/16 v0, 0x9

    iget-object v1, p0, Ljxd;->eKh:Ljob;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 240
    :cond_8
    iget v0, p0, Ljxd;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_9

    .line 241
    const/16 v0, 0xa

    iget v1, p0, Ljxd;->eKm:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 243
    :cond_9
    iget v0, p0, Ljxd;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_a

    .line 244
    const/16 v0, 0xb

    iget-object v1, p0, Ljxd;->dzf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 246
    :cond_a
    iget v0, p0, Ljxd;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_b

    .line 247
    const/16 v0, 0xc

    iget-object v1, p0, Ljxd;->eKn:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 249
    :cond_b
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 250
    return-void
.end method

.method protected final lF()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 254
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 255
    iget v2, p0, Ljxd;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 256
    const/4 v2, 0x1

    iget-boolean v3, p0, Ljxd;->eKd:Z

    invoke-static {v2}, Ljsj;->sc(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 259
    :cond_0
    iget-object v2, p0, Ljxd;->eKe:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljxd;->eKe:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 261
    :goto_0
    iget-object v3, p0, Ljxd;->eKe:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 262
    iget-object v3, p0, Ljxd;->eKe:[I

    aget v3, v3, v1

    .line 263
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 261
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 266
    :cond_1
    add-int/2addr v0, v2

    .line 267
    iget-object v1, p0, Ljxd;->eKe:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 269
    :cond_2
    iget-object v1, p0, Ljxd;->eKi:Ljow;

    if-eqz v1, :cond_3

    .line 270
    const/4 v1, 0x3

    iget-object v2, p0, Ljxd;->eKi:Ljow;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_3
    iget v1, p0, Ljxd;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_4

    .line 274
    const/4 v1, 0x4

    iget v2, p0, Ljxd;->eKj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    :cond_4
    iget-object v1, p0, Ljxd;->eKf:Ljoa;

    if-eqz v1, :cond_5

    .line 278
    const/4 v1, 0x5

    iget-object v2, p0, Ljxd;->eKf:Ljoa;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    :cond_5
    iget-object v1, p0, Ljxd;->eKg:Ljnz;

    if-eqz v1, :cond_6

    .line 282
    const/4 v1, 0x6

    iget-object v2, p0, Ljxd;->eKg:Ljnz;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 285
    :cond_6
    iget v1, p0, Ljxd;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_7

    .line 286
    const/4 v1, 0x7

    iget-wide v2, p0, Ljxd;->eKk:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 289
    :cond_7
    iget v1, p0, Ljxd;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_8

    .line 290
    const/16 v1, 0x8

    iget-object v2, p0, Ljxd;->eKl:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    :cond_8
    iget-object v1, p0, Ljxd;->eKh:Ljob;

    if-eqz v1, :cond_9

    .line 294
    const/16 v1, 0x9

    iget-object v2, p0, Ljxd;->eKh:Ljob;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 297
    :cond_9
    iget v1, p0, Ljxd;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_a

    .line 298
    const/16 v1, 0xa

    iget v2, p0, Ljxd;->eKm:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 301
    :cond_a
    iget v1, p0, Ljxd;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_b

    .line 302
    const/16 v1, 0xb

    iget-object v2, p0, Ljxd;->dzf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    :cond_b
    iget v1, p0, Ljxd;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_c

    .line 306
    const/16 v1, 0xc

    iget-object v2, p0, Ljxd;->eKn:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    :cond_c
    return v0
.end method
