.class public final Liat;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/ui/LanguagePreference;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 486
    iput-object p1, p0, Liat;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487
    iput-object p2, p0, Liat;->mContext:Landroid/content/Context;

    .line 488
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 493
    iget-object v0, p0, Liat;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwJ:Ljava/util/List;

    invoke-static {v0}, Lilw;->x(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 494
    iget-object v1, p0, Liat;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v1, v1, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 495
    iget-object v1, p0, Liat;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v1, v1, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwM:Liaw;

    iget-object v2, p0, Liat;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v2, v2, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwK:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Liaw;->h(Ljava/lang/String;Ljava/util/List;)V

    .line 496
    iget-object v0, p0, Liat;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v0, v0, Lcom/google/android/voicesearch/ui/LanguagePreference;->dwM:Liaw;

    invoke-virtual {v0}, Liaw;->aUH()Ljava/util/List;

    move-result-object v0

    .line 497
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    iget-object v0, p0, Liat;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    invoke-virtual {v0, p1}, Lcom/google/android/voicesearch/ui/LanguagePreference;->a(Landroid/content/DialogInterface;)V

    .line 502
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v1, p0, Liat;->dwN:Lcom/google/android/voicesearch/ui/LanguagePreference;

    iget-object v2, p0, Liat;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, p1, v0}, Lcom/google/android/voicesearch/ui/LanguagePreference;->a(Landroid/content/Context;Landroid/content/DialogInterface;Ljava/util/List;)V

    goto :goto_0
.end method
