.class public final Lczj;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private bob:Lczm;

.field private boc:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bod:Lczk;

.field private boe:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 368
    return-void
.end method

.method private Uj()V
    .locals 4

    .prologue
    .line 218
    invoke-virtual {p0}, Lczj;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 221
    new-instance v1, Lczn;

    invoke-direct {v1}, Lczn;-><init>()V

    .line 222
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 223
    invoke-virtual {p0}, Lczj;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "zipcode_dialog_tag"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 224
    return-void
.end method

.method public static a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljava/lang/String;Ljava/lang/String;)Lczj;
    .locals 4

    .prologue
    .line 71
    const/4 v0, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v1

    iget-object v1, v1, Ljdf;->ebH:Ljdg;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v1

    iget-object v1, v1, Ljdf;->ebH:Ljdg;

    iget-object v1, v1, Ljdg;->ebV:Ljfs;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v1

    iget-object v1, v1, Ljdf;->ebH:Ljdg;

    iget-object v1, v1, Ljdg;->ebV:Ljfs;

    invoke-virtual {v1}, Ljfs;->bjK()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCk()Ljdf;

    move-result-object v0

    iget-object v0, v0, Ljdf;->ebH:Ljdg;

    iget-object v0, v0, Ljdg;->ebV:Ljfs;

    invoke-virtual {v0}, Ljfs;->bjJ()Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_0
    :goto_0
    new-instance v1, Lczj;

    invoke-direct {v1}, Lczj;-><init>()V

    .line 82
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 83
    const-string v3, "zipcode"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "question"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 85
    const-string v0, "worker_fragment_tag"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v0, "zipcode_dialog_tag"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {v1, v2}, Lczj;->setArguments(Landroid/os/Bundle;)V

    .line 88
    return-object v1

    .line 77
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v1

    invoke-virtual {v1}, Ljde;->bgZ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;->aCj()Ljde;

    move-result-object v0

    invoke-virtual {v0}, Ljde;->bgY()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method final Uk()V
    .locals 3

    .prologue
    .line 249
    invoke-virtual {p0}, Lczj;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p0}, Lczj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0a04a7

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 252
    invoke-virtual {p0}, Lczj;->dismiss()V

    .line 254
    :cond_0
    return-void
.end method

.method final a([Ljfs;)V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lczj;->bod:Lczk;

    invoke-virtual {v0}, Lczk;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 242
    iget-object v0, p0, Lczj;->bod:Lczk;

    invoke-virtual {v0, p1}, Lczk;->addAll([Ljava/lang/Object;)V

    .line 243
    return-void

    .line 241
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final jh(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 232
    iput-object p1, p0, Lczj;->boc:Ljava/lang/String;

    .line 233
    iget-object v0, p0, Lczj;->bod:Lczk;

    invoke-virtual {v0}, Lczk;->clear()V

    .line 234
    iget-object v0, p0, Lczj;->bob:Lczm;

    iget-object v1, p0, Lczj;->boc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lczm;->ji(Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method public final onBackStackChanged()V
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lczj;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    .line 167
    iget v1, p0, Lczj;->boe:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lczj;->boc:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {p0}, Lczj;->dismiss()V

    .line 170
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 196
    packed-switch p2, :pswitch_data_0

    .line 202
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 203
    invoke-virtual {p0}, Lczj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "question"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    .line 204
    iget-object v1, p0, Lczj;->bob:Lczm;

    iget-object v1, v1, Lczm;->boh:[Ljfs;

    aget-object v1, v1, p2

    .line 205
    new-instance v2, Ljdg;

    invoke-direct {v2}, Ljdg;-><init>()V

    invoke-virtual {v1}, Ljfs;->bjI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljdg;->tz(Ljava/lang/String;)Ljdg;

    move-result-object v2

    .line 206
    iput-object v1, v2, Ljdg;->ebV:Ljfs;

    .line 207
    new-instance v3, Ljdf;

    invoke-direct {v3}, Ljdf;-><init>()V

    .line 208
    iput-object v2, v3, Ljdf;->ebH:Ljdg;

    .line 209
    invoke-virtual {p0}, Lczj;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lfhs;

    invoke-virtual {v1, v0, v3}, Lfhs;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V

    .line 210
    invoke-virtual {p0}, Lczj;->dismiss()V

    .line 213
    :goto_1
    return-void

    .line 198
    :pswitch_0
    invoke-direct {p0}, Lczj;->Uj()V

    goto :goto_1

    .line 202
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 196
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lczj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "worker_fragment_tag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    invoke-virtual {p0}, Lczj;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 98
    if-nez v0, :cond_1

    .line 99
    new-instance v0, Lczm;

    invoke-direct {v0}, Lczm;-><init>()V

    iput-object v0, p0, Lczj;->bob:Lczm;

    .line 100
    invoke-virtual {p0}, Lczj;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v2, p0, Lczj;->bob:Lczm;

    invoke-virtual {v0, v2, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 105
    :goto_0
    iget-object v0, p0, Lczj;->bob:Lczm;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lczm;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 108
    new-instance v0, Lczk;

    invoke-virtual {p0}, Lczj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lczk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lczj;->bod:Lczk;

    .line 109
    if-nez p1, :cond_3

    .line 110
    invoke-virtual {p0}, Lczj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "zipcode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lczj;->boc:Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lczj;->boc:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113
    invoke-direct {p0}, Lczj;->Uj()V

    .line 130
    :cond_0
    :goto_1
    return-void

    .line 103
    :cond_1
    check-cast v0, Lczm;

    iput-object v0, p0, Lczj;->bob:Lczm;

    goto :goto_0

    .line 115
    :cond_2
    iget-object v0, p0, Lczj;->bob:Lczm;

    iget-object v1, p0, Lczj;->boc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lczm;->ji(Ljava/lang/String;)V

    goto :goto_1

    .line 118
    :cond_3
    const-string v0, "zipcode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lczj;->boc:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lczj;->boc:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    iget-object v0, p0, Lczj;->bob:Lczm;

    iget-object v0, v0, Lczm;->boc:Ljava/lang/String;

    iget-object v1, p0, Lczj;->boc:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 121
    iget-object v0, p0, Lczj;->bob:Lczm;

    iget-object v0, v0, Lczm;->boh:[Ljfs;

    .line 122
    if-eqz v0, :cond_0

    .line 123
    iget-object v1, p0, Lczj;->bod:Lczk;

    invoke-virtual {v1, v0}, Lczk;->addAll([Ljava/lang/Object;)V

    goto :goto_1

    .line 126
    :cond_4
    iget-object v0, p0, Lczj;->bob:Lczm;

    iget-object v1, p0, Lczj;->boc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lczm;->ji(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 184
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lczj;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a04a4

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lczj;->boc:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lczj;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a04a5

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lczj;->bod:Lczk;

    invoke-virtual {v0, v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 190
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 191
    return-object v0
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    .line 134
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 135
    iget-object v0, p0, Lczj;->bob:Lczm;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lczm;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 136
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 174
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 177
    iget-object v0, p0, Lczj;->bob:Lczm;

    iget-boolean v0, v0, Lczm;->boi:Z

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lczj;->Uk()V

    .line 180
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 144
    const-string v0, "zipcode"

    iget-object v1, p0, Lczj;->boc:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 151
    invoke-virtual {p0}, Lczj;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    iput v0, p0, Lczj;->boe:I

    .line 152
    invoke-virtual {p0}, Lczj;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 153
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Landroid/app/DialogFragment;->onStop()V

    .line 158
    invoke-virtual {p0}, Lczj;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 159
    return-void
.end method
