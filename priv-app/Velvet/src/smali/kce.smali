.class public final Lkce;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkcz;


# instance fields
.field final eQU:Lkcc;

.field final eRv:Ljava/io/InputStream;

.field private final eRw:Ljava/io/OutputStream;

.field private eRx:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Lkcc;Ljava/io/OutputStream;Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lkce;->eQU:Lkcc;

    .line 56
    iput-object p2, p0, Lkce;->eRw:Ljava/io/OutputStream;

    .line 57
    iput-object p2, p0, Lkce;->eRx:Ljava/io/OutputStream;

    .line 58
    iput-object p3, p0, Lkce;->eRv:Ljava/io/InputStream;

    .line 59
    return-void
.end method

.method static a(Lkcc;Ljava/io/InputStream;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 186
    iget-object v1, p0, Lkcc;->eRf:Lkac;

    .line 187
    if-nez v1, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v0

    .line 188
    :cond_1
    invoke-virtual {v1}, Lkac;->getSocket()Ljava/net/Socket;

    move-result-object v1

    .line 189
    if-eqz v1, :cond_0

    .line 191
    :try_start_0
    invoke-virtual {v1}, Ljava/net/Socket;->getSoTimeout()I

    move-result v2

    .line 192
    const/16 v3, 0x64

    invoke-virtual {v1, v3}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :try_start_1
    invoke-static {p1}, Lkbt;->o(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    :try_start_2
    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 200
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/net/CacheRequest;)Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->byw()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Lkch;

    iget-object v1, p0, Lkce;->eRv:Ljava/io/InputStream;

    iget-object v2, p0, Lkce;->eQU:Lkcc;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p1, v2, v3}, Lkch;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lkcc;I)V

    .line 221
    :goto_0
    return-object v0

    .line 209
    :cond_0
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v0}, Lkcu;->isChunked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    new-instance v0, Lkcf;

    iget-object v1, p0, Lkce;->eRv:Ljava/io/InputStream;

    invoke-direct {v0, v1, p1, p0}, Lkcf;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lkce;)V

    goto :goto_0

    .line 213
    :cond_1
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRq:Lkcu;

    invoke-virtual {v0}, Lkcu;->getContentLength()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 214
    new-instance v0, Lkch;

    iget-object v1, p0, Lkce;->eRv:Ljava/io/InputStream;

    iget-object v2, p0, Lkce;->eQU:Lkcc;

    iget-object v3, p0, Lkce;->eQU:Lkcc;

    iget-object v3, v3, Lkcc;->eRq:Lkcu;

    invoke-virtual {v3}, Lkcu;->getContentLength()I

    move-result v3

    invoke-direct {v0, v1, p1, v2, v3}, Lkch;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lkcc;I)V

    goto :goto_0

    .line 221
    :cond_2
    new-instance v0, Lkda;

    iget-object v1, p0, Lkce;->eRv:Ljava/io/InputStream;

    iget-object v2, p0, Lkce;->eQU:Lkcc;

    invoke-direct {v0, v1, p1, v2}, Lkda;-><init>(Ljava/io/InputStream;Ljava/net/CacheRequest;Lkcc;)V

    goto :goto_0
.end method

.method public final a(Lkcw;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lkce;->eRx:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lkcw;->b(Ljava/io/OutputStream;)V

    .line 113
    return-void
.end method

.method public final a(ZLjava/io/OutputStream;Ljava/io/InputStream;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 146
    if-eqz p1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    if-eqz p2, :cond_2

    check-cast p2, Lkaz;

    invoke-virtual {p2}, Lkaz;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    :cond_2
    iget-object v1, p0, Lkce;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRp:Lkcs;

    const-string v2, "close"

    iget-object v1, v1, Lkcs;->eSi:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 161
    iget-object v1, p0, Lkce;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRq:Lkcu;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lkce;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRq:Lkcu;

    invoke-virtual {v1}, Lkcu;->byW()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    :cond_3
    instance-of v1, p3, Lkda;

    if-nez v1, :cond_0

    .line 169
    if-eqz p3, :cond_4

    .line 170
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    invoke-static {v0, p3}, Lkce;->a(Lkcc;Ljava/io/InputStream;)Z

    move-result v0

    goto :goto_0

    .line 173
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final byC()Ljava/io/OutputStream;
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    .line 62
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRp:Lkcs;

    const-string v1, "chunked"

    iget-object v0, v0, Lkcs;->eSh:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 63
    if-nez v0, :cond_1

    iget-object v1, p0, Lkce;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRc:Lkcp;

    invoke-interface {v1}, Lkcp;->byM()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lkce;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRf:Lkac;

    invoke-virtual {v1}, Lkac;->bxG()I

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRp:Lkcs;

    iget-object v1, v0, Lkcs;->eSh:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lkcs;->eRZ:Lkcq;

    const-string v2, "Transfer-Encoding"

    invoke-virtual {v1, v2}, Lkcq;->AB(Ljava/lang/String;)V

    :cond_0
    iget-object v1, v0, Lkcs;->eRZ:Lkcq;

    const-string v2, "Transfer-Encoding"

    const-string v3, "chunked"

    invoke-virtual {v1, v2, v3}, Lkcq;->bk(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "chunked"

    iput-object v1, v0, Lkcs;->eSh:Ljava/lang/String;

    .line 67
    const/4 v0, 0x1

    .line 71
    :cond_1
    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRc:Lkcp;

    invoke-interface {v0}, Lkcp;->byM()I

    move-result v0

    .line 73
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 74
    const/16 v0, 0x400

    .line 76
    :cond_2
    invoke-virtual {p0}, Lkce;->byE()V

    .line 77
    new-instance v1, Lkcg;

    iget-object v2, p0, Lkce;->eRx:Ljava/io/OutputStream;

    invoke-direct {v1, v2, v0, v4}, Lkcg;-><init>(Ljava/io/OutputStream;IB)V

    move-object v0, v1

    .line 103
    :goto_0
    return-object v0

    .line 81
    :cond_3
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRc:Lkcp;

    invoke-interface {v0}, Lkcp;->byL()J

    move-result-wide v2

    .line 82
    cmp-long v0, v2, v6

    if-eqz v0, :cond_4

    .line 83
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRp:Lkcs;

    invoke-virtual {v0, v2, v3}, Lkcs;->setContentLength(J)V

    .line 84
    invoke-virtual {p0}, Lkce;->byE()V

    .line 85
    new-instance v0, Lkci;

    iget-object v1, p0, Lkce;->eRx:Ljava/io/OutputStream;

    invoke-direct {v0, v1, v2, v3, v4}, Lkci;-><init>(Ljava/io/OutputStream;JB)V

    goto :goto_0

    .line 88
    :cond_4
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRp:Lkcs;

    iget-wide v2, v0, Lkcs;->eSg:J

    .line 89
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-lez v0, :cond_5

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_5
    cmp-long v0, v2, v6

    if-eqz v0, :cond_6

    .line 96
    invoke-virtual {p0}, Lkce;->byE()V

    .line 97
    new-instance v0, Lkcw;

    long-to-int v1, v2

    invoke-direct {v0, v1}, Lkcw;-><init>(I)V

    goto :goto_0

    .line 103
    :cond_6
    new-instance v0, Lkcw;

    invoke-direct {v0}, Lkcw;-><init>()V

    goto :goto_0
.end method

.method public final byD()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lkce;->eRx:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    .line 108
    iget-object v0, p0, Lkce;->eRw:Ljava/io/OutputStream;

    iput-object v0, p0, Lkce;->eRx:Ljava/io/OutputStream;

    .line 109
    return-void
.end method

.method public final byE()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    invoke-virtual {v0}, Lkcc;->bym()V

    .line 129
    iget-object v0, p0, Lkce;->eQU:Lkcc;

    iget-object v0, v0, Lkcc;->eRp:Lkcs;

    iget-object v0, v0, Lkcs;->eRZ:Lkcq;

    .line 130
    invoke-virtual {v0}, Lkcq;->byQ()[B

    move-result-object v0

    .line 131
    iget-object v1, p0, Lkce;->eRx:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 132
    return-void
.end method

.method public final byF()Lkcu;
    .locals 3

    .prologue
    .line 135
    iget-object v0, p0, Lkce;->eRv:Ljava/io/InputStream;

    invoke-static {v0}, Lkcq;->r(Ljava/io/InputStream;)Lkcq;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lkce;->eQU:Lkcc;

    iget-object v1, v1, Lkcc;->eRf:Lkac;

    invoke-virtual {v0}, Lkcq;->bxG()I

    move-result v2

    invoke-virtual {v1, v2}, Lkac;->tE(I)V

    .line 137
    iget-object v1, p0, Lkce;->eQU:Lkcc;

    invoke-virtual {v1, v0}, Lkcc;->a(Lkcq;)V

    .line 139
    new-instance v1, Lkcu;

    iget-object v2, p0, Lkce;->eQU:Lkcc;

    iget-object v2, v2, Lkcc;->eRo:Ljava/net/URI;

    invoke-direct {v1, v2, v0}, Lkcu;-><init>(Ljava/net/URI;Lkcq;)V

    .line 140
    const-string v0, "http/1.1"

    invoke-virtual {v1, v0}, Lkcu;->AC(Ljava/lang/String;)V

    .line 141
    return-object v1
.end method
