.class final Lbmq;
.super Lbmg;


# instance fields
.field private aFH:Lbrk;

.field private aFI:Lbrl;

.field private aFJ:Lbmo;


# direct methods
.method public constructor <init>(Lbrk;Lbmo;)V
    .locals 1

    invoke-direct {p0}, Lbmg;-><init>()V

    iput-object p1, p0, Lbmq;->aFH:Lbrk;

    const/4 v0, 0x0

    iput-object v0, p0, Lbmq;->aFI:Lbrl;

    iput-object p2, p0, Lbmq;->aFJ:Lbmo;

    return-void
.end method

.method public constructor <init>(Lbrl;Lbmo;)V
    .locals 1

    invoke-direct {p0}, Lbmg;-><init>()V

    iput-object p1, p0, Lbmq;->aFI:Lbrl;

    const/4 v0, 0x0

    iput-object v0, p0, Lbmq;->aFH:Lbrk;

    iput-object p2, p0, Lbmq;->aFJ:Lbmo;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/app/PendingIntent;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lbmq;->aFJ:Lbmo;

    if-nez v0, :cond_0

    const-string v0, "LocationClientImpl"

    const-string v1, "onRemoveGeofencesByPendingIntentResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lbmq;->aFJ:Lbmo;

    new-instance v0, Lbms;

    iget-object v1, p0, Lbmq;->aFJ:Lbmo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v3, p0, Lbmq;->aFI:Lbrl;

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lbms;-><init>(Lbmo;ILbrl;ILandroid/app/PendingIntent;)V

    invoke-virtual {v6, v0}, Lbmo;->a(Lbit;)V

    iput-object v7, p0, Lbmq;->aFJ:Lbmo;

    iput-object v7, p0, Lbmq;->aFH:Lbrk;

    iput-object v7, p0, Lbmq;->aFI:Lbrl;

    goto :goto_0
.end method

.method public final a(I[Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbmq;->aFJ:Lbmo;

    if-nez v0, :cond_0

    const-string v0, "LocationClientImpl"

    const-string v1, "onAddGeofenceResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbmq;->aFJ:Lbmo;

    new-instance v1, Lbmp;

    iget-object v2, p0, Lbmq;->aFJ:Lbmo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v3, p0, Lbmq;->aFH:Lbrk;

    invoke-direct {v1, v2, v3, p1, p2}, Lbmp;-><init>(Lbmo;Lbrk;I[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbmo;->a(Lbit;)V

    iput-object v4, p0, Lbmq;->aFJ:Lbmo;

    iput-object v4, p0, Lbmq;->aFH:Lbrk;

    iput-object v4, p0, Lbmq;->aFI:Lbrl;

    goto :goto_0
.end method

.method public final b(I[Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lbmq;->aFJ:Lbmo;

    if-nez v0, :cond_0

    const-string v0, "LocationClientImpl"

    const-string v1, "onRemoveGeofencesByRequestIdsResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lbmq;->aFJ:Lbmo;

    new-instance v0, Lbms;

    iget-object v1, p0, Lbmq;->aFJ:Lbmo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x2

    iget-object v3, p0, Lbmq;->aFI:Lbrl;

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lbms;-><init>(Lbmo;ILbrl;I[Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Lbmo;->a(Lbit;)V

    iput-object v7, p0, Lbmq;->aFJ:Lbmo;

    iput-object v7, p0, Lbmq;->aFH:Lbrk;

    iput-object v7, p0, Lbmq;->aFI:Lbrl;

    goto :goto_0
.end method
