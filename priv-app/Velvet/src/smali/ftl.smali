.class public final Lftl;
.super Lfro;
.source "PG"


# instance fields
.field private mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 32
    return-void
.end method

.method private a(Ljhg;[Ljhg;IIILesm;Z)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;
    .locals 4
    .param p1    # Ljhg;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 72
    iget-object v0, p0, Lftl;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040111

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;

    .line 74
    invoke-virtual {v0, p3}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->jr(I)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;

    .line 75
    invoke-virtual {v0, p4}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->jt(I)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;

    .line 76
    invoke-virtual {v0, p5}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->js(I)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;

    .line 77
    invoke-virtual {v0, p7}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->fz(Z)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;

    .line 78
    if-eqz p1, :cond_0

    .line 79
    invoke-virtual {v0, p1, p6}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->a(Ljhg;Lesm;)V

    .line 81
    :cond_0
    array-length v2, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, p2, v1

    .line 82
    invoke-virtual {v0, v3, p6}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->a(Ljhg;Lesm;)V

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_1
    return-object v0
.end method


# virtual methods
.method protected final aCZ()V
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 42
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    move-object v8, v0

    check-cast v8, Landroid/widget/TableLayout;

    .line 45
    invoke-virtual {v8}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 47
    iget-object v0, p0, Lfro;->cBc:Lanh;

    .line 48
    iget-object v0, v0, Lanh;->agU:Lanv;

    .line 49
    iget-object v10, v0, Lanv;->aiG:Ljax;

    .line 50
    invoke-virtual {v0}, Lanv;->pW()Z

    move-result v7

    .line 52
    invoke-virtual {v0}, Lanv;->getColumnWidth()I

    move-result v4

    .line 53
    invoke-virtual {v0}, Lanv;->pV()I

    move-result v5

    .line 54
    iget-object v0, p0, Lftl;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    iget-object v6, v0, Lfml;->cvY:Lfnh;

    .line 56
    const/4 v1, 0x0

    iget-object v2, v10, Ljax;->dXu:[Ljhg;

    const v3, 0x7f09006a

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lftl;->a(Ljhg;[Ljhg;IIILesm;Z)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v9, v9, v9, v9}, Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;->setPadding(IIII)V

    .line 61
    invoke-virtual {v8, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 62
    iget-object v10, v10, Ljax;->dXv:[Ljay;

    array-length v11, v10

    :goto_0
    if-ge v9, v11, :cond_0

    aget-object v0, v10, v9

    .line 63
    iget-object v1, v0, Ljay;->dXx:Ljhg;

    iget-object v2, v0, Ljay;->dXy:[Ljhg;

    const v3, 0x7f090068

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lftl;->a(Ljhg;[Ljhg;IIILesm;Z)Lcom/google/android/sidekick/shared/ui/TextOrIconTableRow;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 62
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 67
    :cond_0
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 36
    iput-object p1, p0, Lftl;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 37
    const v0, 0x7f040110

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
