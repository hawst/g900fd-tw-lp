.class public final Lffz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aMx:Ljava/lang/String;

.field private aMy:Ljava/lang/String;

.field private aMz:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lffz;->aMx:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Ljbp;Landroid/content/Context;)Lffz;
    .locals 2
    .param p1    # Ljbp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 41
    if-eqz p1, :cond_1

    .line 42
    invoke-virtual {p1}, Ljbp;->bar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {p1}, Ljbp;->baq()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffz;->aMz:Ljava/lang/String;

    .line 48
    const v0, 0x7f0a0171

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffz;->aMy:Ljava/lang/String;

    .line 50
    :cond_0
    invoke-virtual {p1}, Ljbp;->oK()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    invoke-virtual {p1}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffz;->aMy:Ljava/lang/String;

    .line 56
    :cond_1
    :goto_0
    return-object p0

    .line 52
    :cond_2
    invoke-virtual {p1}, Ljbp;->bfb()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {p1}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffz;->aMy:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 63
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.e100.NOTIFICATION_RECEIVED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    const-string v1, "com.google.android.e100.MESSAGE"

    iget-object v2, p0, Lffz;->aMx:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    iget-object v1, p0, Lffz;->aMy:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    const-string v1, "com.google.android.e100.LOCATION_TITLE"

    iget-object v2, p0, Lffz;->aMy:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 67
    iget-object v1, p0, Lffz;->aMz:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    const-string v1, "com.google.android.e100.LOCATION_CID"

    iget-object v2, p0, Lffz;->aMz:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    :cond_0
    return-object v0
.end method
