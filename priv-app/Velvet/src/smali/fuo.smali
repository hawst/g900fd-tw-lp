.class public final Lfuo;
.super Lfro;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 5

    .prologue
    const v4, 0x7f110324

    .line 31
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    .line 32
    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agz:Laoj;

    .line 33
    invoke-virtual {v1}, Laoj;->pb()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    const v2, 0x7f1101a9

    invoke-virtual {v1}, Laoj;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 36
    :cond_0
    invoke-virtual {v1}, Laoj;->qI()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 37
    invoke-virtual {v1}, Laoj;->qH()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v4, v2}, Lgab;->a(Landroid/view/View;ILjava/lang/CharSequence;)Landroid/widget/TextView;

    .line 39
    iget-object v2, v1, Laoj;->ajx:Lani;

    if-eqz v2, :cond_1

    .line 40
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, v1, Laoj;->ajx:Lani;

    invoke-virtual {p0, v0, v1}, Lfuo;->a(Landroid/view/View;Lani;)V

    .line 44
    :cond_1
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 25
    const v0, 0x7f040123

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
