.class public final Ldgd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bwS:Ljava/util/List;

.field final bwT:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mClickLog:Lcks;

.field private final mSettings:Lcke;

.field private final mSources:Ldgh;


# direct methods
.method public constructor <init>(Ldgh;Lcke;Lcks;)V
    .locals 3

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Ldgd;->mSources:Ldgh;

    .line 66
    iput-object p2, p0, Ldgd;->mSettings:Lcke;

    .line 67
    iput-object p3, p0, Ldgd;->mClickLog:Lcks;

    .line 68
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldgd;->bwT:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 69
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    iput-object v0, p0, Ldgd;->bwS:Ljava/util/List;

    .line 70
    iget-object v0, p0, Ldgd;->mSources:Ldgh;

    new-instance v1, Ldgf;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Ldgf;-><init>(Ldgd;B)V

    invoke-interface {v0, v1}, Ldgh;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 71
    return-void
.end method


# virtual methods
.method public final abR()Ljava/util/List;
    .locals 5

    .prologue
    .line 74
    iget-object v0, p0, Ldgd;->bwT:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Ldgd;->mClickLog:Lcks;

    invoke-interface {v0}, Lcks;->OU()Ljava/util/Map;

    move-result-object v1

    iget-object v0, p0, Ldgd;->mSources:Ldgh;

    invoke-interface {v0}, Ldgh;->abS()Ljava/util/Collection;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgb;

    iget-object v4, p0, Ldgd;->mSettings:Lcke;

    invoke-interface {v4, v0}, Lcke;->c(Ldgb;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Ldge;

    invoke-direct {v0, v1}, Ldge;-><init>(Ljava/util/Map;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldgd;->bwS:Ljava/util/List;

    .line 77
    :cond_2
    iget-object v0, p0, Ldgd;->bwS:Ljava/util/List;

    return-object v0
.end method
