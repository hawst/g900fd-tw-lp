.class public final Ldss;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# static fields
.field private static final bIX:I

.field private static final bIY:I

.field private static final bIZ:I

.field private static final bJa:I


# instance fields
.field private Oq:I

.field private bIQ:F

.field private bIR:F

.field private final bJb:Landroid/graphics/drawable/Drawable;

.field private final bJc:I

.field private final bJd:I

.field private final bJe:Landroid/graphics/Paint;

.field private final bJf:Landroid/graphics/Paint;

.field private final bJg:I

.field private final bJh:Landroid/graphics/Paint;

.field private final bJi:Z

.field private bJj:Ljava/lang/Float;

.field private bJk:I

.field private bJl:I

.field private bJm:F

.field private bJn:F

.field private bJo:F

.field private bJp:Landroid/graphics/RectF;

.field private bJq:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const v0, 0x7f0202af

    sput v0, Ldss;->bIX:I

    .line 37
    const v0, 0x7f0d0021

    sput v0, Ldss;->bIY:I

    .line 39
    const v0, 0x7f0b0022

    sput v0, Ldss;->bIZ:I

    .line 41
    const v0, 0x7f0d0020

    sput v0, Ldss;->bJa:I

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Z)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    .line 73
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 52
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ldss;->bJe:Landroid/graphics/Paint;

    .line 53
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ldss;->bJf:Landroid/graphics/Paint;

    .line 55
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ldss;->bJh:Landroid/graphics/Paint;

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Ldss;->Oq:I

    .line 62
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldss;->bJj:Ljava/lang/Float;

    .line 69
    iput v3, p0, Ldss;->bJo:F

    .line 70
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Ldss;->bJp:Landroid/graphics/RectF;

    .line 74
    sget v0, Ldss;->bIY:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ldss;->bJc:I

    .line 75
    sget v0, Ldss;->bJa:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ldss;->bJd:I

    .line 76
    sget v0, Ldss;->bIX:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    .line 77
    iget-object v0, p0, Ldss;->bJe:Landroid/graphics/Paint;

    sget v1, Ldss;->bIZ:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iput-boolean p2, p0, Ldss;->bJi:Z

    .line 80
    const v0, 0x7f0d000c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v1, p0, Ldss;->bJc:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Ldss;->bJk:I

    .line 82
    const v0, 0x7f0d000a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v1, p0, Ldss;->bJc:I

    add-int/2addr v0, v1

    iput v0, p0, Ldss;->bJl:I

    .line 84
    const v0, 0x7f0d0022

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Ldss;->bJg:I

    .line 85
    iget-object v0, p0, Ldss;->bJe:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 89
    iget-object v0, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 90
    invoke-virtual {p0}, Ldss;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v0}, Ldss;->e(Landroid/graphics/Rect;)V

    .line 93
    iget-object v0, p0, Ldss;->bJh:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    iget-object v0, p0, Ldss;->bJh:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 95
    iget-object v0, p0, Ldss;->bJf:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    iget-object v0, p0, Ldss;->bJf:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 97
    iget-object v0, p0, Ldss;->bJf:Landroid/graphics/Paint;

    iget v1, p0, Ldss;->bJg:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 98
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 194
    invoke-virtual {p0}, Ldss;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 195
    iget-object v1, p0, Ldss;->bJp:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 196
    iget-object v1, p0, Ldss;->bJp:Landroid/graphics/RectF;

    iget v2, p0, Ldss;->bJo:F

    iget v3, p0, Ldss;->bJc:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Ldss;->bJo:F

    iget v4, p0, Ldss;->bJc:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    .line 198
    iget-object v1, p0, Ldss;->bJp:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Ldss;->bJc:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 199
    iget-object v0, p0, Ldss;->bJp:Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Landroid/graphics/RectF;->offset(FF)V

    .line 200
    iget-object v0, p0, Ldss;->bJp:Landroid/graphics/RectF;

    iget v1, p0, Ldss;->bJd:I

    int-to-float v1, v1

    iget v2, p0, Ldss;->bJd:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, p3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 201
    return-void
.end method

.method private b(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V
    .locals 6

    .prologue
    .line 206
    invoke-virtual {p0}, Ldss;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 207
    iget-object v1, p0, Ldss;->bJj:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 209
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 210
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    iget v4, p0, Ldss;->bJc:I

    int-to-float v4, v4

    sub-float/2addr v0, v4

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 211
    iget v0, p0, Ldss;->bIQ:F

    iget v1, p0, Ldss;->bIR:F

    iget-object v2, p0, Ldss;->bJj:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget v3, p0, Ldss;->bJm:F

    mul-float/2addr v2, v3

    add-float/2addr v2, p2

    invoke-virtual {p1, v0, v1, v2, p3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 213
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 218
    :goto_0
    return-void

    .line 216
    :cond_0
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    iget v4, p0, Ldss;->bJc:I

    int-to-float v4, v4

    sub-float v4, v0, v4

    move-object v0, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private e(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 229
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 230
    iget-boolean v0, p0, Ldss;->bJi:Z

    if-eqz v0, :cond_1

    iget v0, p0, Ldss;->bJk:I

    add-int/2addr v0, v1

    .line 233
    :goto_0
    iget v2, p0, Ldss;->Oq:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 234
    iget v2, p0, Ldss;->bJd:I

    add-int/2addr v0, v2

    .line 238
    :cond_0
    :goto_1
    iget-object v2, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget v4, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 239
    return-void

    .line 230
    :cond_1
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 235
    :cond_2
    iget v2, p0, Ldss;->Oq:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 236
    iget v2, p0, Ldss;->bJd:I

    sub-int/2addr v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final H(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 127
    iput p1, p0, Ldss;->bJo:F

    .line 129
    iget v0, p0, Ldss;->bJo:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    iget v0, p0, Ldss;->bJo:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 130
    :cond_0
    iget v0, p0, Ldss;->bJo:F

    sub-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldss;->bJj:Ljava/lang/Float;

    .line 131
    iput v2, p0, Ldss;->bJn:F

    .line 133
    :cond_1
    invoke-virtual {p0}, Ldss;->invalidateSelf()V

    .line 134
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 146
    iget-boolean v0, p0, Ldss;->bJq:Z

    if-eqz v0, :cond_0

    .line 148
    iget v0, p0, Ldss;->Oq:I

    if-nez v0, :cond_2

    .line 149
    iget-object v0, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 174
    :cond_0
    :goto_0
    iget v0, p0, Ldss;->bJo:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_4

    iget v0, p0, Ldss;->bJo:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    .line 176
    iget-object v0, p0, Ldss;->bJf:Landroid/graphics/Paint;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 177
    iget v0, p0, Ldss;->bJg:I

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v1, p0, Ldss;->bJf:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Ldss;->a(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V

    .line 178
    iget-object v0, p0, Ldss;->bJf:Landroid/graphics/Paint;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 179
    iget v0, p0, Ldss;->bJg:I

    int-to-float v0, v0

    iget-object v1, p0, Ldss;->bJf:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Ldss;->a(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V

    .line 180
    iget-object v0, p0, Ldss;->bJh:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 181
    iget-object v0, p0, Ldss;->bJh:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v6, v0}, Ldss;->a(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V

    .line 191
    :cond_1
    :goto_1
    return-void

    .line 154
    :cond_2
    invoke-virtual {p0}, Ldss;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 155
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Ldss;->bJk:I

    add-int/2addr v1, v2

    .line 156
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 157
    iget v2, p0, Ldss;->Oq:I

    if-ne v2, v7, :cond_3

    .line 158
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, p0, Ldss;->bJc:I

    sub-int v5, v1, v5

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 162
    :goto_2
    iget-object v2, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 163
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 166
    iget v2, p0, Ldss;->Oq:I

    if-ne v2, v7, :cond_0

    .line 167
    iget v2, p0, Ldss;->bJc:I

    sub-int v4, v1, v2

    .line 168
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Ldss;->bJc:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v2, v4

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v3, p0, Ldss;->bJc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v4

    iget-object v5, p0, Ldss;->bJe:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 160
    :cond_3
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, p0, Ldss;->bJc:I

    add-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1, v2, v3, v4, v1}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    goto :goto_2

    .line 182
    :cond_4
    iget-object v0, p0, Ldss;->bJj:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v0, v6

    if-lez v0, :cond_1

    .line 184
    iget-object v0, p0, Ldss;->bJf:Landroid/graphics/Paint;

    iget v1, p0, Ldss;->bJn:F

    const/high16 v2, 0x41880000    # 17.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 185
    iget v0, p0, Ldss;->bJg:I

    mul-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v1, p0, Ldss;->bJf:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Ldss;->b(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V

    .line 186
    iget-object v0, p0, Ldss;->bJf:Landroid/graphics/Paint;

    iget v1, p0, Ldss;->bJn:F

    const/high16 v2, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 187
    iget v0, p0, Ldss;->bJg:I

    int-to-float v0, v0

    iget-object v1, p0, Ldss;->bJf:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Ldss;->b(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V

    .line 188
    iget-object v0, p0, Ldss;->bJh:Landroid/graphics/Paint;

    iget v1, p0, Ldss;->bJn:F

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 189
    iget-object v0, p0, Ldss;->bJh:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v6, v0}, Ldss;->b(Landroid/graphics/Canvas;FLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method public final eb(Z)V
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Ldss;->bJq:Z

    if-ne p1, v0, :cond_0

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iput-boolean p1, p0, Ldss;->bJq:Z

    .line 141
    invoke-virtual {p0}, Ldss;->invalidateSelf()V

    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 103
    invoke-direct {p0, p1}, Ldss;->e(Landroid/graphics/Rect;)V

    .line 104
    return-void
.end method

.method public final r(FF)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 107
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Ldss;->bJj:Ljava/lang/Float;

    .line 108
    iput p2, p0, Ldss;->bJn:F

    .line 110
    iget-object v0, p0, Ldss;->bJj:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldss;->bJj:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 111
    :cond_0
    iget-object v0, p0, Ldss;->bJj:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v2, v0

    iput v0, p0, Ldss;->bJo:F

    .line 113
    :cond_1
    invoke-virtual {p0}, Ldss;->invalidateSelf()V

    .line 114
    return-void
.end method

.method public final s(FF)V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 117
    iput p1, p0, Ldss;->bIQ:F

    .line 118
    iput p2, p0, Ldss;->bIR:F

    .line 119
    iget v0, p0, Ldss;->bJm:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Ldss;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Ldss;->bJl:I

    iget v2, p0, Ldss;->bIQ:F

    float-to-int v2, v2

    iget v3, p0, Ldss;->bIR:F

    float-to-int v3, v3

    sub-int/2addr v0, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-double v4, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    sub-int v0, v1, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-double v0, v0

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    add-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Ldss;->bJm:F

    .line 124
    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 249
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Ldss;->bJb:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 254
    return-void
.end method

.method public final setMode(I)V
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Ldss;->Oq:I

    if-eq v0, p1, :cond_0

    .line 222
    iput p1, p0, Ldss;->Oq:I

    .line 223
    invoke-virtual {p0}, Ldss;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0, v0}, Ldss;->e(Landroid/graphics/Rect;)V

    .line 224
    invoke-virtual {p0}, Ldss;->invalidateSelf()V

    .line 226
    :cond_0
    return-void
.end method
