.class public final Lhlz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlk;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lhlz;->mContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method private static og(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failing because: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final synthetic G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avc()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 28
    move-object v3, p1

    check-cast v3, Lcom/google/android/search/shared/actions/MediaControlAction;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "You need a new device. Your current SDK version is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhlz;->og(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhlz;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.MEDIA_CONTENT_CONTROL"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_1
    if-nez v0, :cond_2

    const-string v0, "Do not have the permission."

    invoke-static {v0}, Lhlz;->og(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "The main looper was null"

    invoke-static {v0}, Lhlz;->og(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_3
    new-instance v4, Landroid/media/RemoteController;

    iget-object v1, p0, Lhlz;->mContext:Landroid/content/Context;

    new-instance v2, Ledz;

    invoke-direct {v2}, Ledz;-><init>()V

    invoke-direct {v4, v1, v2, v0}, Landroid/media/RemoteController;-><init>(Landroid/content/Context;Landroid/media/RemoteController$OnClientUpdateListener;Landroid/os/Looper;)V

    iget-object v0, p0, Lhlz;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/media/AudioManager;

    if-nez v5, :cond_4

    const-string v0, "Could not get audio manager."

    invoke-static {v0}, Lhlz;->og(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_4
    invoke-virtual {v5, v4}, Landroid/media/AudioManager;->registerRemoteController(Landroid/media/RemoteController;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Could not register remote controller."

    invoke-static {v0}, Lhlz;->og(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :cond_5
    new-instance v0, Lhma;

    const-string v2, "Music Command and Control"

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lhma;-><init>(Lhlz;Ljava/lang/String;Lcom/google/android/search/shared/actions/MediaControlAction;Landroid/media/RemoteController;Landroid/media/AudioManager;)V

    new-instance v1, Leoe;

    invoke-direct {v1}, Leoe;-><init>()V

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v0, v2, v3}, Leoe;->a(Ljava/lang/Runnable;J)V

    move v0, v6

    goto :goto_0
.end method
