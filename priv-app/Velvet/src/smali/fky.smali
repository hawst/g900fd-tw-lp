.class public final Lfky;
.super Lesj;
.source "PG"

# interfaces
.implements Lesk;


# instance fields
.field private cbC:Z

.field private final cko:Ljava/util/List;

.field private final cuo:Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

.field private final cup:Z

.field private final cuq:Z

.field private final cur:Ljava/util/Map;

.field private final cus:Ljava/util/List;

.field private final cut:Z

.field private final cuu:Z

.field private final cuv:Landroid/os/Parcelable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cuw:I

.field private final cux:[Lflu;

.field private cuy:I

.field private final mCreator:Lfld;

.field private final mIcebreakerCallback:Lfpu;

.field private final mParentView:Lflr;

.field private final mRunner:Lerp;


# direct methods
.method public constructor <init>(Lerp;Lflr;Lfld;Ljava/util/List;ZZLjava/util/Map;Ljava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZLandroid/os/Parcelable;Lfpu;)V
    .locals 1
    .param p9    # Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Landroid/os/Parcelable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-direct {p0}, Lesj;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfky;->cbC:Z

    .line 74
    iput-object p1, p0, Lfky;->mRunner:Lerp;

    .line 75
    iput-object p2, p0, Lfky;->mParentView:Lflr;

    .line 76
    iput-object p3, p0, Lfky;->mCreator:Lfld;

    .line 78
    iput-object p4, p0, Lfky;->cko:Ljava/util/List;

    .line 79
    iput-boolean p5, p0, Lfky;->cup:Z

    .line 80
    iput-boolean p6, p0, Lfky;->cuq:Z

    .line 81
    iput-object p9, p0, Lfky;->cuo:Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    .line 82
    iput-object p7, p0, Lfky;->cur:Ljava/util/Map;

    .line 83
    iput-object p8, p0, Lfky;->cus:Ljava/util/List;

    .line 84
    iput-boolean p10, p0, Lfky;->cut:Z

    .line 85
    iput-boolean p11, p0, Lfky;->cuu:Z

    .line 86
    iput-object p12, p0, Lfky;->cuv:Landroid/os/Parcelable;

    .line 87
    iput-object p13, p0, Lfky;->mIcebreakerCallback:Lfpu;

    .line 89
    iget-object v0, p0, Lfky;->cko:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lfky;->cuw:I

    .line 90
    iget v0, p0, Lfky;->cuw:I

    new-array v0, v0, [Lflu;

    iput-object v0, p0, Lfky;->cux:[Lflu;

    .line 91
    return-void
.end method


# virtual methods
.method public final a(Lejm;)V
    .locals 21

    .prologue
    .line 214
    move-object/from16 v0, p0

    iget-object v9, v0, Lfky;->mParentView:Lflr;

    move-object/from16 v0, p0

    iget v10, v0, Lfky;->cuw:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lfky;->cux:[Lflu;

    move-object/from16 v0, p0

    iget-object v7, v0, Lfky;->cus:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, Lfky;->cuo:Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lfky;->cup:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lfky;->cuq:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lfky;->cut:Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lfky;->cuu:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lfky;->cuv:Landroid/os/Parcelable;

    move-object/from16 v0, p0

    iget-object v15, v0, Lfky;->cko:Ljava/util/List;

    invoke-static {}, Lenu;->auR()V

    iget-object v6, v9, Lflr;->cvg:Lfky;

    move-object/from16 v0, p0

    if-ne v6, v0, :cond_15

    const/4 v6, 0x0

    iput-object v6, v9, Lflr;->cvg:Lfky;

    if-eqz v14, :cond_0

    iget-object v6, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->eZ(Z)V

    :cond_0
    iget-object v6, v9, Lflr;->cuJ:Lfkz;

    invoke-virtual {v6}, Lfkz;->aAG()Z

    if-eqz v8, :cond_6

    if-eqz v4, :cond_1

    iget-object v4, v9, Lflr;->mCardContainer:Lfmt;

    invoke-interface {v4}, Lfmt;->wq()Lfnm;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lfnm;->cancel()V

    :cond_1
    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aue()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aud()V

    :cond_2
    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    const v6, 0x7f110009

    invoke-virtual {v4, v6}, Lcom/google/android/shared/ui/SuggestionGridLayout;->ia(I)V

    :cond_3
    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getColumnCount()I

    move-result v16

    if-eqz v8, :cond_b

    const/4 v4, 0x0

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    move v8, v7

    move v7, v4

    :goto_1
    if-ge v8, v10, :cond_f

    aget-object v4, v11, v8

    if-eqz v4, :cond_5

    aget-object v4, v11, v8

    iget-object v4, v4, Lflu;->cdr:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    if-eqz v17, :cond_c

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    move-object/from16 v0, v17

    instance-of v0, v0, Lekm;

    move/from16 v17, v0

    if-eqz v17, :cond_c

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lekm;

    iget v4, v4, Lekm;->column:I

    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v4, v0, :cond_c

    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_d

    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    aget-object v17, v11, v8

    move-object/from16 v0, v17

    iget-object v0, v0, Lflu;->cdr:Landroid/view/View;

    move-object/from16 v17, v0

    aget-object v18, v11, v8

    move-object/from16 v0, v18

    iget-object v0, v0, Lflu;->bXk:Landroid/view/View;

    move-object/from16 v18, v0

    aget-object v19, v11, v8

    move-object/from16 v0, v19

    iget-object v0, v0, Lflu;->bXl:Landroid/view/View;

    move-object/from16 v19, v0

    const/16 v20, -0x3

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;I)V

    :goto_3
    if-nez v6, :cond_4

    aget-object v4, v11, v8

    iget-object v6, v4, Lflu;->cdr:Landroid/view/View;

    :cond_4
    if-nez v13, :cond_5

    aget-object v4, v11, v8

    iget-object v0, v4, Lflu;->cdr:Landroid/view/View;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    if-eqz v4, :cond_e

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    instance-of v4, v4, Lekm;

    if-eqz v4, :cond_e

    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lekm;

    :goto_4
    const/16 v18, 0x0

    move/from16 v0, v18

    iput-boolean v0, v4, Lekm;->cdu:Z

    const/16 v18, 0x0

    move/from16 v0, v18

    iput-boolean v0, v4, Lekm;->cdv:Z

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_5
    move-object v4, v6

    move v6, v7

    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move v7, v6

    move-object v6, v4

    goto/16 :goto_1

    :cond_6
    const/4 v4, 0x0

    :goto_5
    iget-object v6, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v6}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v6

    if-ge v4, v6, :cond_a

    iget-object v6, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v6, v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-static {v6}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v16

    move-object/from16 v0, v16

    instance-of v0, v0, Lfkm;

    move/from16 v16, v0

    if-eqz v16, :cond_9

    move-object v4, v6

    :goto_6
    if-eqz v4, :cond_7

    iget-object v6, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v6, v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    :cond_7
    if-eqz v7, :cond_3

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_8
    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    if-eqz v4, :cond_8

    iget-object v7, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v7, v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    goto :goto_7

    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_a
    const/4 v4, 0x0

    goto :goto_6

    :cond_b
    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aub()I

    move-result v4

    goto/16 :goto_0

    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_d
    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    aget-object v17, v11, v8

    move-object/from16 v0, v17

    iget-object v0, v0, Lflu;->cdr:Landroid/view/View;

    move-object/from16 v17, v0

    aget-object v18, v11, v8

    move-object/from16 v0, v18

    iget-object v0, v0, Lflu;->bXk:Landroid/view/View;

    move-object/from16 v18, v0

    aget-object v19, v11, v8

    move-object/from16 v0, v19

    iget-object v0, v0, Lflu;->bXl:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v4, v0, v1, v2, v7}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;I)V

    add-int/lit8 v4, v7, 0x1

    rem-int v7, v4, v16

    goto/16 :goto_3

    :cond_e
    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lekm;

    goto/16 :goto_4

    :cond_f
    if-eqz v5, :cond_18

    iget-object v6, v9, Lflr;->cuJ:Lfkz;

    invoke-virtual {v6}, Lfkz;->aAH()Z

    move-result v4

    if-nez v4, :cond_11

    const/4 v4, 0x1

    :goto_8
    invoke-static {v4}, Lifv;->gY(Z)V

    move-object v4, v5

    check-cast v4, Landroid/os/Bundle;

    const-string v5, "entry-hash"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_10
    :goto_9
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_12

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lfkd;

    invoke-interface {v5}, Lfkd;->getEntry()Lizj;

    move-result-object v8

    invoke-static {v8}, Lgbm;->N(Lizj;)J

    move-result-wide v12

    cmp-long v8, v12, v10

    if-nez v8, :cond_10

    iput-object v5, v6, Lfkz;->cuA:Lfkd;

    goto :goto_9

    :cond_11
    const/4 v4, 0x0

    goto :goto_8

    :cond_12
    iget-object v5, v6, Lfkz;->cuA:Lfkd;

    if-nez v5, :cond_16

    const-string v4, "CardSettingsController"

    const-string v5, "Couldn\'t restore settings, because the card is missing."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    :goto_a
    iget-object v4, v9, Lflr;->cvd:Lflv;

    if-eqz v4, :cond_14

    iget-object v4, v9, Lflr;->cvd:Lflv;

    invoke-interface {v4}, Lflv;->wh()V

    :cond_14
    if-eqz v14, :cond_15

    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/shared/ui/SuggestionGridLayout;->eZ(Z)V

    .line 218
    :cond_15
    return-void

    .line 214
    :cond_16
    const-string v5, "back-of-card"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v4

    iput-object v4, v6, Lfkz;->cuC:Landroid/util/SparseArray;

    const/4 v4, 0x1

    iput-boolean v4, v6, Lfkz;->cuD:Z

    iget-object v4, v6, Lfkz;->cuC:Landroid/util/SparseArray;

    if-nez v4, :cond_17

    invoke-virtual {v6}, Lfkz;->aAF()V

    goto :goto_a

    :cond_17
    const/4 v4, 0x0

    invoke-virtual {v6, v4}, Lfkz;->Z(Ljava/util/List;)V

    goto :goto_a

    :cond_18
    if-eqz v12, :cond_13

    invoke-virtual {v12}, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->aAO()Lizj;

    move-result-object v4

    if-eqz v4, :cond_19

    invoke-virtual {v12}, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->aAP()I

    move-result v5

    invoke-virtual {v9, v4, v5}, Lflr;->i(Lizj;I)V

    goto :goto_a

    :cond_19
    invoke-virtual {v12}, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;->aAN()Z

    move-result v4

    if-eqz v4, :cond_13

    if-eqz v6, :cond_13

    iget-object v4, v9, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v4}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d01b5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, v9, Lflr;->mScrollView:Lekf;

    invoke-interface {v5, v6, v4}, Lekf;->t(Landroid/view/View;I)V

    goto :goto_a
.end method

.method public final avD()Z
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v2, 0x1

    .line 182
    iget v0, p0, Lfky;->cuy:I

    iget v1, p0, Lfky;->cuw:I

    if-lt v0, v1, :cond_0

    move v0, v2

    .line 209
    :goto_0
    return v0

    .line 186
    :cond_0
    :try_start_0
    iget v3, p0, Lfky;->cuy:I

    iget-object v0, p0, Lfky;->cko:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    const/4 v1, 0x0

    iget-object v4, p0, Lfky;->cur:Ljava/util/Map;

    if-eqz v4, :cond_1

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    invoke-static {v1}, Lgbm;->N(Lizj;)J

    move-result-wide v4

    iget-object v1, p0, Lfky;->cur:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    :cond_1
    if-eqz v1, :cond_2

    const-string v4, "card:adapterState"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v0, v4}, Lfkd;->T(Landroid/os/Bundle;)V

    :cond_2
    iget-object v4, p0, Lfky;->mCreator:Lfld;

    iget-object v5, p0, Lfky;->mIcebreakerCallback:Lfpu;

    invoke-virtual {v4, v0, v5}, Lfld;->a(Lfkd;Lfpu;)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lfky;->mParentView:Lflr;

    invoke-static {v4}, Lflr;->bm(Landroid/view/View;)V

    if-eqz v1, :cond_4

    const-string v5, "card:views"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v5}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_3
    const-string v5, "card:focusedViewId"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v7, :cond_4

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    :cond_4
    iget-object v1, p0, Lfky;->cux:[Lflu;

    new-instance v5, Lflu;

    invoke-direct {v5, v4}, Lflu;-><init>(Landroid/view/View;)V

    aput-object v5, v1, v3

    iget-object v1, p0, Lfky;->mCreator:Lfld;

    invoke-virtual {v1, v0}, Lfld;->c(Lfkd;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lfky;->cux:[Lflu;

    aget-object v1, v1, v3

    iget-object v5, p0, Lfky;->mCreator:Lfld;

    invoke-virtual {v5, v0}, Lfld;->b(Lfkd;)Landroid/view/View;

    move-result-object v5

    iput-object v5, v1, Lflu;->bXk:Landroid/view/View;

    iget-object v1, p0, Lfky;->cux:[Lflu;

    aget-object v1, v1, v3

    iget-object v3, p0, Lfky;->mCreator:Lfld;

    invoke-virtual {v3, v0, v4}, Lfld;->a(Lfkd;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lflu;->bXl:Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    :cond_5
    iget v0, p0, Lfky;->cuy:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfky;->cuy:I

    .line 209
    :goto_1
    iget v0, p0, Lfky;->cuy:I

    iget v1, p0, Lfky;->cuw:I

    if-lt v0, v1, :cond_7

    move v0, v2

    goto/16 :goto_0

    .line 187
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 188
    :try_start_1
    iget-boolean v0, p0, Lfky;->cbC:Z

    if-eqz v0, :cond_6

    .line 193
    const-string v0, "AddNowCardsTransaction"

    const-string v3, "Caught exception in canceled CardViewCreator"

    invoke-static {v0, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    iget v0, p0, Lfky;->cuy:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfky;->cuy:I

    move v0, v2

    goto/16 :goto_0

    .line 198
    :cond_6
    :try_start_2
    const-string v3, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 200
    :try_start_3
    iget-object v0, p0, Lfky;->cko:Ljava/util/List;

    iget v4, p0, Lfky;->cuy:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 204
    :goto_2
    :try_start_4
    iget-object v3, p0, Lfky;->mCreator:Lfld;

    const-string v3, "CardViewCreator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Caught exception while creating Now cards for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 207
    iget v0, p0, Lfky;->cuy:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfky;->cuy:I

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v0, v3

    goto :goto_2

    :catchall_0
    move-exception v0

    iget v1, p0, Lfky;->cuy:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lfky;->cuy:I

    throw v0

    .line 209
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfky;->cbC:Z

    .line 95
    return-void
.end method

.method public final run()V
    .locals 8

    .prologue
    .line 99
    iget-boolean v0, p0, Lfky;->cbC:Z

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 108
    :cond_2
    invoke-virtual {p0}, Lfky;->avD()Z

    move-result v2

    .line 113
    if-eqz v2, :cond_3

    .line 115
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lfky;->a(Lejm;)V

    .line 118
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    .line 119
    if-nez v2, :cond_4

    const-wide/16 v6, 0x5

    cmp-long v3, v4, v6

    if-ltz v3, :cond_2

    .line 127
    :cond_4
    if-nez v2, :cond_0

    .line 128
    iget-object v0, p0, Lfky;->mRunner:Lerp;

    invoke-interface {v0, p0}, Lerp;->a(Lesk;)V

    goto :goto_0
.end method
