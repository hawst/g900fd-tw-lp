.class public final Lhbt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final mIntentStarter:Leoj;

.field private final mUrlHelper:Lcpn;

.field private final mVelvetFactory:Lgpu;


# direct methods
.method public constructor <init>(Lgpu;Leoj;Lcpn;)V
    .locals 0

    .prologue
    .line 533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534
    iput-object p1, p0, Lhbt;->mVelvetFactory:Lgpu;

    .line 535
    iput-object p2, p0, Lhbt;->mIntentStarter:Leoj;

    .line 536
    iput-object p3, p0, Lhbt;->mUrlHelper:Lcpn;

    .line 537
    return-void
.end method


# virtual methods
.method public final a(Lhfp;Lcom/google/android/velvet/presenter/inappwebpage/Request;Lhao;)Lhbq;
    .locals 5

    .prologue
    .line 541
    new-instance v0, Lhbq;

    iget-object v1, p0, Lhbt;->mUrlHelper:Lcpn;

    invoke-direct {v0, v1, p1, p2, p3}, Lhbq;-><init>(Lcpn;Lhfp;Lcom/google/android/velvet/presenter/inappwebpage/Request;Lhao;)V

    .line 543
    iget-object v1, p0, Lhbt;->mVelvetFactory:Lgpu;

    iget-object v2, p0, Lhbt;->mIntentStarter:Leoj;

    iget-object v3, v0, Lhbq;->dbQ:Lhfp;

    new-instance v4, Lhbv;

    invoke-direct {v4, v0}, Lhbv;-><init>(Lhbq;)V

    invoke-virtual {v3, v4}, Lhfp;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v3, v0, Lhbq;->dbQ:Lhfp;

    new-instance v4, Lhbu;

    invoke-direct {v4, v0}, Lhbu;-><init>(Lhbq;)V

    invoke-virtual {v3, v4}, Lhfp;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    invoke-static {}, Lcid;->permissiveTrustPolicy()Lcil;

    move-result-object v3

    new-instance v4, Lhbw;

    invoke-direct {v4, v0}, Lhbw;-><init>(Lhbq;)V

    invoke-virtual {v1, v2, v3, v4}, Lgpu;->a(Leoj;Lcil;Lcik;)Lcey;

    move-result-object v1

    iget-object v2, v0, Lhbq;->dbQ:Lhfp;

    const-string v3, "agsa_ext"

    invoke-virtual {v2, v1, v3}, Lhfp;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 544
    return-object v0
.end method
