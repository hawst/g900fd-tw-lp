.class public final Lacq;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field zG:I

.field zH:I

.field private zO:I

.field private zP:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method


# virtual methods
.method public final Q(II)V
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lacq;->zO:I

    .line 57
    iput p2, p0, Lacq;->zP:I

    .line 58
    invoke-virtual {p0}, Lacq;->requestLayout()V

    .line 59
    return-void
.end method

.method public final cancelLongPress()V
    .locals 3

    .prologue
    .line 45
    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    .line 48
    invoke-virtual {p0}, Lacq;->getChildCount()I

    move-result v1

    .line 49
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 50
    invoke-virtual {p0, v0}, Lacq;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 51
    invoke-virtual {v2}, Landroid/view/View;->cancelLongPress()V

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 112
    invoke-virtual {p0}, Lacq;->getChildCount()I

    move-result v2

    .line 114
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 133
    invoke-virtual {p0, v1}, Lacq;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 134
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_0

    .line 135
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lacp;

    .line 138
    iget v4, v0, Lacp;->x:I

    add-int/lit8 v4, v4, 0x0

    .line 139
    iget v5, v0, Lacp;->y:I

    .line 140
    iget v6, v0, Lacp;->width:I

    add-int/2addr v6, v4

    iget v0, v0, Lacp;->height:I

    add-int/2addr v0, v5

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/view/View;->layout(IIII)V

    .line 132
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 143
    :cond_1
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 79
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 80
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 82
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 83
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 85
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "CellLayout cannot have UNSPECIFIED dimensions"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_1
    invoke-virtual {p0}, Lacq;->getChildCount()I

    move-result v10

    .line 90
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v10, :cond_2

    .line 91
    invoke-virtual {p0, v7}, Lacq;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 92
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lacp;

    .line 94
    invoke-virtual {p0}, Lacq;->getContext()Landroid/content/Context;

    iget v1, p0, Lacq;->zG:I

    iget v2, p0, Lacq;->zH:I

    iget v3, p0, Lacq;->zO:I

    iget v4, p0, Lacq;->zP:I

    invoke-virtual {p0}, Lacq;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lacq;->getPaddingTop()I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lacp;->a(IIIIII)V

    .line 99
    iget v1, v0, Lacp;->width:I

    invoke-static {v1, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 101
    iget v0, v0, Lacp;->height:I

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 104
    invoke-virtual {v11, v1, v0}, Landroid/view/View;->measure(II)V

    .line 90
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 107
    :cond_2
    invoke-virtual {p0, v8, v9}, Lacq;->setMeasuredDimension(II)V

    .line 108
    return-void
.end method

.method public final requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 70
    if-eqz p1, :cond_0

    .line 71
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 72
    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 73
    invoke-virtual {p0, v0}, Lacq;->requestRectangleOnScreen(Landroid/graphics/Rect;)Z

    .line 75
    :cond_0
    return-void
.end method

.method protected final setChildrenDrawingCacheEnabled(Z)V
    .locals 4

    .prologue
    .line 151
    invoke-virtual {p0}, Lacq;->getChildCount()I

    move-result v1

    .line 152
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 153
    invoke-virtual {p0, v0}, Lacq;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 154
    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 156
    invoke-virtual {v2}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v3

    if-nez v3, :cond_0

    .line 157
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 152
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_1
    return-void
.end method
