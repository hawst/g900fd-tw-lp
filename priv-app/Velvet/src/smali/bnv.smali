.class public final Lbnv;
.super Lbir;


# static fields
.field private static volatile aGs:Landroid/os/Bundle;

.field private static volatile aGt:Landroid/os/Bundle;


# instance fields
.field private aBh:Ljava/lang/String;

.field private aGq:Ljava/lang/String;

.field private final aGr:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbir;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;[Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbnv;->aGr:Ljava/util/HashMap;

    iput-object p5, p0, Lbnv;->aGq:Ljava/lang/String;

    iput-object p6, p0, Lbnv;->aBh:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbgp;Lbgq;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lbiu;

    invoke-direct {v3, p2}, Lbiu;-><init>(Lbgp;)V

    new-instance v4, Lbix;

    invoke-direct {v4, p3}, Lbix;-><init>(Lbgq;)V

    move-object v0, p0

    move-object v1, p1

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lbnv;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbhk;Lbhl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lbnv;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lbnv;->aGr:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic b(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;
    .locals 3

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    if-nez p2, :cond_0

    move-object v0, v1

    :goto_0
    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    return-object v2

    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method private declared-synchronized r(Landroid/os/Bundle;)V
    .locals 2

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    const-string v0, "use_contactables_api"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lbof;->bK(Z)V

    sget-object v0, Lbnu;->aGn:Lbnu;

    invoke-virtual {v0, p1}, Lbnu;->q(Landroid/os/Bundle;)V

    const-string v0, "config.email_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lbnv;->aGs:Landroid/os/Bundle;

    const-string v0, "config.phone_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lbnv;->aGt:Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lbhh;Ljava/lang/String;Ljava/lang/String;I)Lbjg;
    .locals 3

    const/4 v1, 0x0

    new-instance v2, Lbod;

    invoke-direct {v2, p0, p1}, Lbod;-><init>(Lbnv;Lbhh;)V

    :try_start_0
    invoke-super {p0}, Lbir;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnp;

    invoke-interface {v0, v2, p2, p3, p4}, Lbnp;->c(Lbnm;Ljava/lang/String;Ljava/lang/String;I)Lbjg;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v1, v1}, Lbod;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lbhh;Ljava/lang/String;Ljava/lang/String;II)Lbjg;
    .locals 7

    const/4 v6, 0x0

    new-instance v1, Lbod;

    invoke-direct {v1, p0, p1}, Lbod;-><init>(Lbnv;Lbhh;)V

    :try_start_0
    invoke-super {p0}, Lbir;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnp;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lbnp;->b(Lbnm;Ljava/lang/String;Ljava/lang/String;II)Lbjg;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lbod;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    move-object v0, v6

    goto :goto_0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "post_init_configuration"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lbnv;->r(Landroid/os/Bundle;)V

    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, p1, p2, v0}, Lbir;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_1
    const-string v0, "post_init_resolution"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lbhh;ZZLjava/lang/String;Ljava/lang/String;I)V
    .locals 8

    const/4 v7, 0x0

    invoke-super {p0}, Lbir;->yS()V

    new-instance v1, Lboc;

    invoke-direct {v1, p0, p1}, Lboc;-><init>(Lbnv;Lbhh;)V

    :try_start_0
    invoke-super {p0}, Lbir;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnp;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v3, p3

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lbnp;->a(Lbnm;ZZLjava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lboc;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method protected final a(Lbjm;Lbiv;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "social_client_application_id"

    iget-object v2, p0, Lbnv;->aGq:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "real_client_package_name"

    iget-object v2, p0, Lbnv;->aBh:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x5d3f18

    iget-object v2, p0, Lbir;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lbjm;->b(Lbjj;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final b(Lbhh;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lbir;->yS()V

    new-instance v1, Lboa;

    invoke-direct {v1, p0, p1}, Lboa;-><init>(Lbnv;Lbhh;)V

    :try_start_0
    invoke-super {p0}, Lbir;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnp;

    const/4 v2, 0x3

    invoke-interface {v0, v1, p2, p3, v2}, Lbnp;->a(Lbnm;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v3, v3}, Lboa;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public final b(Lbit;)V
    .locals 0

    invoke-super {p0, p1}, Lbir;->a(Lbit;)V

    return-void
.end method

.method protected final synthetic c(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lbnq;->q(Landroid/os/IBinder;)Lbnp;

    move-result-object v0

    return-object v0
.end method

.method public final disconnect()V
    .locals 8

    iget-object v6, p0, Lbnv;->aGr:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    invoke-virtual {p0}, Lbnv;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbnv;->aGr:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbob;

    invoke-super {p0}, Lbir;->yT()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lbnp;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lbnp;->a(Lbnm;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "PeopleClient"

    const-string v2, "Failed to unregister listener"

    invoke-static {v1, v2, v0}, Lboe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lbnv;->aGr:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Lbir;->disconnect()V

    return-void

    :catch_1
    move-exception v0

    :try_start_2
    const-string v1, "PeopleClient"

    const-string v2, "PeopleService is in unexpected state"

    invoke-static {v1, v2, v0}, Lboe;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method protected final yQ()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.people.service.START"

    return-object v0
.end method

.method protected final yR()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.people.internal.IPeopleService"

    return-object v0
.end method
