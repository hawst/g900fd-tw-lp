.class public final Laoq;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private agq:Ljava/lang/String;

.field private aib:Ljava/lang/String;

.field private aid:Z

.field private ajW:Ljava/lang/String;

.field private ajX:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6048
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6049
    iput v1, p0, Laoq;->aez:I

    const-string v0, ""

    iput-object v0, p0, Laoq;->aib:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laoq;->ajW:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laoq;->agq:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Laoq;->ajX:Ljava/lang/String;

    iput-boolean v1, p0, Laoq;->aid:Z

    const/4 v0, 0x0

    iput-object v0, p0, Laoq;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Laoq;->eCz:I

    .line 6050
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5922
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Laoq;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoq;->aib:Ljava/lang/String;

    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laoq;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoq;->ajW:Ljava/lang/String;

    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laoq;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoq;->agq:Ljava/lang/String;

    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laoq;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laoq;->ajX:Ljava/lang/String;

    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laoq;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Laoq;->aid:Z

    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laoq;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6067
    iget v0, p0, Laoq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6068
    const/4 v0, 0x1

    iget-object v1, p0, Laoq;->aib:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6070
    :cond_0
    iget v0, p0, Laoq;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6071
    const/4 v0, 0x2

    iget-object v1, p0, Laoq;->ajW:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6073
    :cond_1
    iget v0, p0, Laoq;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 6074
    const/4 v0, 0x3

    iget-object v1, p0, Laoq;->agq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6076
    :cond_2
    iget v0, p0, Laoq;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 6077
    const/4 v0, 0x4

    iget-object v1, p0, Laoq;->ajX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 6079
    :cond_3
    iget v0, p0, Laoq;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 6080
    const/4 v0, 0x5

    iget-boolean v1, p0, Laoq;->aid:Z

    invoke-virtual {p1, v0, v1}, Ljsj;->N(IZ)V

    .line 6082
    :cond_4
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6083
    return-void
.end method

.method public final be(Z)Laoq;
    .locals 1

    .prologue
    .line 6035
    iput-boolean p1, p0, Laoq;->aid:Z

    .line 6036
    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Laoq;->aez:I

    .line 6037
    return-object p0
.end method

.method public final ch(Ljava/lang/String;)Laoq;
    .locals 1

    .prologue
    .line 5947
    if-nez p1, :cond_0

    .line 5948
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5950
    :cond_0
    iput-object p1, p0, Laoq;->aib:Ljava/lang/String;

    .line 5951
    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laoq;->aez:I

    .line 5952
    return-object p0
.end method

.method public final ci(Ljava/lang/String;)Laoq;
    .locals 1

    .prologue
    .line 5969
    if-nez p1, :cond_0

    .line 5970
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5972
    :cond_0
    iput-object p1, p0, Laoq;->ajW:Ljava/lang/String;

    .line 5973
    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laoq;->aez:I

    .line 5974
    return-object p0
.end method

.method public final cj(Ljava/lang/String;)Laoq;
    .locals 1

    .prologue
    .line 5991
    if-nez p1, :cond_0

    .line 5992
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5994
    :cond_0
    iput-object p1, p0, Laoq;->agq:Ljava/lang/String;

    .line 5995
    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Laoq;->aez:I

    .line 5996
    return-object p0
.end method

.method public final ck(Ljava/lang/String;)Laoq;
    .locals 1

    .prologue
    .line 6013
    if-nez p1, :cond_0

    .line 6014
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6016
    :cond_0
    iput-object p1, p0, Laoq;->ajX:Ljava/lang/String;

    .line 6017
    iget v0, p0, Laoq;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Laoq;->aez:I

    .line 6018
    return-object p0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5988
    iget-object v0, p0, Laoq;->agq:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6087
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6088
    iget v1, p0, Laoq;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6089
    const/4 v1, 0x1

    iget-object v2, p0, Laoq;->aib:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6092
    :cond_0
    iget v1, p0, Laoq;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6093
    const/4 v1, 0x2

    iget-object v2, p0, Laoq;->ajW:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6096
    :cond_1
    iget v1, p0, Laoq;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 6097
    const/4 v1, 0x3

    iget-object v2, p0, Laoq;->agq:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6100
    :cond_2
    iget v1, p0, Laoq;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 6101
    const/4 v1, 0x4

    iget-object v2, p0, Laoq;->ajX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6104
    :cond_3
    iget v1, p0, Laoq;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 6105
    const/4 v1, 0x5

    iget-boolean v2, p0, Laoq;->aid:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6108
    :cond_4
    return v0
.end method

.method public final ps()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5944
    iget-object v0, p0, Laoq;->aib:Ljava/lang/String;

    return-object v0
.end method

.method public final pt()Z
    .locals 1

    .prologue
    .line 5955
    iget v0, p0, Laoq;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pu()Z
    .locals 1

    .prologue
    .line 6032
    iget-boolean v0, p0, Laoq;->aid:Z

    return v0
.end method

.method public final re()Ljava/lang/String;
    .locals 1

    .prologue
    .line 5966
    iget-object v0, p0, Laoq;->ajW:Ljava/lang/String;

    return-object v0
.end method

.method public final rf()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6010
    iget-object v0, p0, Laoq;->ajX:Ljava/lang/String;

    return-object v0
.end method
