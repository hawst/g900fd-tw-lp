.class public final Liwn;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private dMe:I

.field private dMf:Liwo;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v1, p0, Liwn;->aez:I

    const-string v0, ""

    iput-object v0, p0, Liwn;->afh:Ljava/lang/String;

    iput v1, p0, Liwn;->dMe:I

    iput-object v2, p0, Liwn;->dMf:Liwo;

    iput-object v2, p0, Liwn;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Liwn;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liwn;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwn;->afh:Ljava/lang/String;

    iget v0, p0, Liwn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwn;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Liwn;->dMe:I

    iget v0, p0, Liwn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwn;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Liwn;->dMf:Liwo;

    if-nez v0, :cond_1

    new-instance v0, Liwo;

    invoke-direct {v0}, Liwo;-><init>()V

    iput-object v0, p0, Liwn;->dMf:Liwo;

    :cond_1
    iget-object v0, p0, Liwn;->dMf:Liwo;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    iget v0, p0, Liwn;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liwn;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Liwn;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Liwn;->dMe:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_1
    iget-object v0, p0, Liwn;->dMf:Liwo;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Liwn;->dMf:Liwo;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final aZu()I
    .locals 1

    iget v0, p0, Liwn;->dMe:I

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liwn;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v1, p0, Liwn;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Liwn;->afh:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Liwn;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Liwn;->dMe:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Liwn;->dMf:Liwo;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Liwn;->dMf:Liwo;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    return v0
.end method

.method public final ne(I)Liwn;
    .locals 1

    iput p1, p0, Liwn;->dMe:I

    iget v0, p0, Liwn;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Liwn;->aez:I

    return-object p0
.end method

.method public final pZ(Ljava/lang/String;)Liwn;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Liwn;->afh:Ljava/lang/String;

    iget v0, p0, Liwn;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwn;->aez:I

    return-object p0
.end method
