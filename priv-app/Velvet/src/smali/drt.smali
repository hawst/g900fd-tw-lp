.class public final Ldrt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic bHA:Lcom/google/android/search/searchplate/RecognizerView;


# direct methods
.method public constructor <init>(Lcom/google/android/search/searchplate/RecognizerView;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Ldrt;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 266
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 267
    iget-object v1, p0, Ldrt;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setAlpha(F)V

    .line 268
    iget-object v1, p0, Ldrt;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setScaleX(F)V

    .line 269
    iget-object v1, p0, Ldrt;->bHA:Lcom/google/android/search/searchplate/RecognizerView;

    invoke-virtual {v1, v0}, Lcom/google/android/search/searchplate/RecognizerView;->setScaleY(F)V

    .line 270
    return-void
.end method
