.class public final Lccy;
.super Lecp;
.source "PG"


# instance fields
.field private synthetic aRY:Lcom/google/android/remotesearch/RemoteSearchService;


# direct methods
.method public constructor <init>(Lcom/google/android/remotesearch/RemoteSearchService;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    invoke-direct {p0}, Lecp;-><init>()V

    return-void
.end method


# virtual methods
.method public final CV()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 443
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRX:Lcom/google/android/search/shared/actions/VoiceAction;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRX:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 445
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lccl;->ey(I)V

    .line 447
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iput-object v2, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRX:Lcom/google/android/search/shared/actions/VoiceAction;

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 448
    :cond_1
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRX:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agr()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 449
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lccl;->ey(I)V

    .line 451
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iput-object v2, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRX:Lcom/google/android/search/shared/actions/VoiceAction;

    goto :goto_0

    .line 452
    :cond_2
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRX:Lcom/google/android/search/shared/actions/VoiceAction;

    invoke-interface {v0}, Lcom/google/android/search/shared/actions/VoiceAction;->agt()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lccl;->ey(I)V

    .line 455
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iput-object v2, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRX:Lcom/google/android/search/shared/actions/VoiceAction;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V
    .locals 2

    .prologue
    .line 418
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->ahN()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    check-cast v0, Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 419
    iget-object v1, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v1, v1, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->ajb()I

    move-result v0

    invoke-interface {v1, v0}, Lccl;->ex(I)V

    .line 420
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/ParcelableVoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 2

    .prologue
    .line 409
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->ahN()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    if-nez v0, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->ahN()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v1

    invoke-static {v0, v1, p3}, Lcom/google/android/remotesearch/RemoteSearchService;->a(Lcom/google/android/remotesearch/RemoteSearchService;Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;[Ljava/lang/String;[F)V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    invoke-interface {v0, p2, p3}, Lccl;->a([Ljava/lang/String;[F)V

    .line 465
    return-void
.end method

.method public final a([Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    invoke-virtual {v0, p1}, Lcom/google/android/remotesearch/RemoteSearchService;->b([Landroid/content/Intent;)Z

    .line 438
    return-void
.end method

.method public final gb(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    invoke-interface {v0, p1}, Lccl;->gb(Ljava/lang/String;)V

    .line 426
    return-void
.end method

.method public final j([B)V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    invoke-interface {v0, p1}, Lccl;->j([B)V

    .line 432
    return-void
.end method

.method public final x(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lccy;->aRY:Lcom/google/android/remotesearch/RemoteSearchService;

    iget-object v0, v0, Lcom/google/android/remotesearch/RemoteSearchService;->aRW:Lccl;

    invoke-interface {v0, p1, p2}, Lccl;->x(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    return-void
.end method
