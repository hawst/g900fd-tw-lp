.class final enum Lgkx;
.super Ljava/lang/Enum;
.source "PG"


# static fields
.field public static final enum cOs:Lgkx;

.field public static final enum cOt:Lgkx;

.field public static final enum cOu:Lgkx;

.field public static final enum cOv:Lgkx;

.field private static final synthetic cOw:[Lgkx;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lgkx;

    const-string v1, "NO_SPEECH_DETECTED"

    invoke-direct {v0, v1, v2}, Lgkx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgkx;->cOs:Lgkx;

    .line 20
    new-instance v0, Lgkx;

    const-string v1, "SPEECH_DETECTED"

    invoke-direct {v0, v1, v3}, Lgkx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgkx;->cOt:Lgkx;

    .line 21
    new-instance v0, Lgkx;

    const-string v1, "DELAY_END_OF_SPEECH"

    invoke-direct {v0, v1, v4}, Lgkx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgkx;->cOu:Lgkx;

    .line 22
    new-instance v0, Lgkx;

    const-string v1, "END_OF_SPEECH"

    invoke-direct {v0, v1, v5}, Lgkx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgkx;->cOv:Lgkx;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Lgkx;

    sget-object v1, Lgkx;->cOs:Lgkx;

    aput-object v1, v0, v2

    sget-object v1, Lgkx;->cOt:Lgkx;

    aput-object v1, v0, v3

    sget-object v1, Lgkx;->cOu:Lgkx;

    aput-object v1, v0, v4

    sget-object v1, Lgkx;->cOv:Lgkx;

    aput-object v1, v0, v5

    sput-object v0, Lgkx;->cOw:[Lgkx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgkx;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lgkx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgkx;

    return-object v0
.end method

.method public static values()[Lgkx;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lgkx;->cOw:[Lgkx;

    invoke-virtual {v0}, [Lgkx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgkx;

    return-object v0
.end method
