.class public final Ljck;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private aim:Ljava/lang/String;

.field private ajs:I

.field private eaG:Ljava/lang/String;

.field public eaH:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    iput v1, p0, Ljck;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljck;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljck;->eaG:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljck;->aim:Ljava/lang/String;

    iput v1, p0, Ljck;->ajs:I

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Ljck;->eaH:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljck;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljck;->eCz:I

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljck;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljck;->afh:Ljava/lang/String;

    iget v0, p0, Ljck;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljck;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljck;->eaG:Ljava/lang/String;

    iget v0, p0, Ljck;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljck;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljck;->aim:Ljava/lang/String;

    iget v0, p0, Ljck;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljck;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljck;->ajs:I

    iget v0, p0, Ljck;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljck;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljck;->eaH:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljck;->eaH:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljck;->eaH:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljck;->eaH:[Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    iget v0, p0, Ljck;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljck;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Ljck;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljck;->eaG:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Ljck;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljck;->aim:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_2
    iget v0, p0, Ljck;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Ljck;->ajs:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    :cond_3
    iget-object v0, p0, Ljck;->eaH:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljck;->eaH:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljck;->eaH:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Ljck;->eaH:[Ljava/lang/String;

    aget-object v1, v1, v0

    if-eqz v1, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Ljsj;->p(ILjava/lang/String;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final bgC()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljck;->eaG:Ljava/lang/String;

    return-object v0
.end method

.method public final bgD()Z
    .locals 1

    iget v0, p0, Ljck;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getStatusCode()I
    .locals 1

    iget v0, p0, Ljck;->ajs:I

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljck;->afh:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v2, p0, Ljck;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Ljck;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Ljck;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Ljck;->eaG:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget v2, p0, Ljck;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iget-object v3, p0, Ljck;->aim:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Ljck;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    iget v3, p0, Ljck;->ajs:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Ljck;->eaH:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljck;->eaH:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    :goto_0
    iget-object v4, p0, Ljck;->eaH:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    iget-object v4, p0, Ljck;->eaH:[Ljava/lang/String;

    aget-object v4, v4, v1

    if-eqz v4, :cond_4

    add-int/lit8 v3, v3, 0x1

    invoke-static {v4}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    :cond_6
    return v0
.end method

.method public final oE(I)Ljck;
    .locals 1

    iput p1, p0, Ljck;->ajs:I

    iget v0, p0, Ljck;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljck;->aez:I

    return-object p0
.end method

.method public final pB()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljck;->aim:Ljava/lang/String;

    return-object v0
.end method

.method public final pC()Z
    .locals 1

    iget v0, p0, Ljck;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final pb()Z
    .locals 1

    iget v0, p0, Ljck;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final tg(Ljava/lang/String;)Ljck;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljck;->afh:Ljava/lang/String;

    iget v0, p0, Ljck;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljck;->aez:I

    return-object p0
.end method

.method public final th(Ljava/lang/String;)Ljck;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljck;->eaG:Ljava/lang/String;

    iget v0, p0, Ljck;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljck;->aez:I

    return-object p0
.end method

.method public final ti(Ljava/lang/String;)Ljck;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljck;->aim:Ljava/lang/String;

    iget v0, p0, Ljck;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljck;->aez:I

    return-object p0
.end method
