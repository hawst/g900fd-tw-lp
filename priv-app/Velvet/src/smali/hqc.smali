.class public final Lhqc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private synthetic dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;


# direct methods
.method public constructor <init>(Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lhqc;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lhqc;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lhqc;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    iget-object v0, v0, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->dnP:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 148
    :cond_0
    iget-object v0, p0, Lhqc;->dnZ:Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;

    invoke-virtual {v0}, Lcom/google/android/voicesearch/fragments/reminders/EditReminderView;->onUserInteraction()V

    .line 149
    const/4 v0, 0x0

    return v0
.end method
