.class public final Lemw;
.super Ljava/util/concurrent/ScheduledThreadPoolExecutor;
.source "PG"

# interfaces
.implements Leti;


# instance fields
.field private final cfC:Z

.field private final cfD:Z

.field private final cfE:I

.field private final cfF:I

.field private final cfG:Ljava/util/Map;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final cfH:Ljava/util/Queue;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cfI:J

.field private cfJ:J

.field private final dK:Ljava/lang/Object;

.field private final mName:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/util/concurrent/ThreadFactory;ZZZ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 385
    invoke-direct {p0, p2, p3}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(ILjava/util/concurrent/ThreadFactory;)V

    .line 274
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lemw;->dK:Ljava/lang/Object;

    .line 390
    if-lez p2, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifv;->gX(Z)V

    .line 392
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lemw;->mName:Ljava/lang/String;

    .line 393
    iput-boolean p6, p0, Lemw;->cfD:Z

    .line 394
    if-eqz p5, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lemw;->cfC:Z

    .line 396
    invoke-static {}, Lior;->aYd()Ljava/util/IdentityHashMap;

    move-result-object v0

    iput-object v0, p0, Lemw;->cfG:Ljava/util/Map;

    .line 397
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v2, 0x28

    invoke-direct {v0, v2}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lemw;->cfH:Ljava/util/Queue;

    .line 402
    mul-int/lit8 v0, p2, 0x2

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lemw;->cfE:I

    .line 403
    mul-int/lit8 v0, p2, 0xa

    add-int/lit8 v0, v0, 0x5

    iput v0, p0, Lemw;->cfF:I

    .line 405
    if-eqz p4, :cond_1

    .line 406
    const-wide/16 v2, 0x12c

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v2, v3, v0}, Lemw;->setKeepAliveTime(JLjava/util/concurrent/TimeUnit;)V

    .line 407
    invoke-virtual {p0, v1}, Lemw;->allowCoreThreadTimeOut(Z)V

    .line 409
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 390
    goto :goto_0
.end method

.method private static a(ILemx;Ljava/lang/Runnable;J)Ljava/lang/String;
    .locals 11
    .param p1    # Lemx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Runnable;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v6, 0x0

    .line 780
    if-eqz p1, :cond_a

    .line 781
    invoke-static {p2}, Lemw;->m(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lemx;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 783
    :goto_0
    invoke-virtual {p1}, Lemx;->isPeriodic()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 784
    const-string v1, "repeat"

    move-object v4, v1

    .line 791
    :goto_1
    if-eqz v0, :cond_6

    .line 792
    const-string v0, "cancel"

    .line 799
    :goto_2
    iget-wide v2, p1, Lemx;->cfM:J

    invoke-static {v2, v3, p3, p4}, Lemw;->f(JJ)Ljava/lang/String;

    move-result-object v2

    .line 800
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    int-to-long v6, p0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/16 v8, 0xa

    cmp-long v1, v6, v8

    if-gez v1, :cond_9

    const-string v1, " "

    :goto_3
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lemx;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 803
    :goto_4
    return-object v0

    .line 781
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 785
    :cond_2
    if-eqz v0, :cond_4

    .line 786
    iget-wide v4, p1, Lemx;->cfK:J

    iget-wide v2, p1, Lemx;->cfM:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    iget-wide v2, p1, Lemx;->cfM:J

    :goto_5
    invoke-static {v4, v5, v2, v3}, Lemw;->f(JJ)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    goto :goto_1

    :cond_3
    move-wide v2, p3

    goto :goto_5

    .line 788
    :cond_4
    iget-wide v4, p1, Lemx;->cfK:J

    iget-wide v2, p1, Lemx;->cfL:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_5

    iget-wide v2, p1, Lemx;->cfL:J

    :goto_6
    invoke-static {v4, v5, v2, v3}, Lemw;->f(JJ)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    goto/16 :goto_1

    :cond_5
    move-wide v2, p3

    goto :goto_6

    .line 793
    :cond_6
    iget-wide v0, p1, Lemx;->cfL:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_7

    iget-wide v0, p1, Lemx;->bBZ:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_7

    .line 795
    iget-wide v0, p1, Lemx;->cfK:J

    iget-wide v2, p1, Lemx;->bBZ:J

    add-long/2addr v0, v2

    move-wide v2, p3

    .line 797
    :goto_7
    invoke-static {v2, v3, v0, v1}, Lemw;->f(JJ)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_7
    iget-wide v2, p1, Lemx;->cfL:J

    iget-wide v0, p1, Lemx;->cfM:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_8

    iget-wide v0, p1, Lemx;->cfM:J

    goto :goto_7

    :cond_8
    move-wide v0, p3

    goto :goto_7

    .line 800
    :cond_9
    const-string v1, ""

    goto/16 :goto_3

    .line 803
    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "unknown: no task info"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p2, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_b
    const-string v0, ""

    goto :goto_8
.end method

.method private a(Ljava/lang/Object;Ljava/util/concurrent/RunnableScheduledFuture;)V
    .locals 7

    .prologue
    .line 592
    new-instance v1, Lemx;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, Ljava/util/concurrent/RunnableScheduledFuture;->isPeriodic()Z

    move-result v3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v0}, Ljava/util/concurrent/RunnableScheduledFuture;->getDelay(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v0, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Legq;

    invoke-direct/range {v1 .. v6}, Lemx;-><init>(Ljava/lang/String;ZJLegq;)V

    .line 594
    iget-object v2, p0, Lemw;->dK:Ljava/lang/Object;

    monitor-enter v2

    .line 598
    :try_start_0
    iget-object v0, p0, Lemw;->cfG:Ljava/util/Map;

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 599
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/util/Collection;ILjava/util/Map;JLetj;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 745
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 746
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 747
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s (First %d of %d)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v0

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 750
    :cond_0
    invoke-virtual {p6, p0}, Letj;->lt(Ljava/lang/String;)V

    .line 751
    const-string v1, " #  queue   exec    age  (queue, execution, age are in milliseconds)"

    invoke-virtual {p6, v1}, Letj;->lv(Ljava/lang/String;)V

    .line 752
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    .line 753
    :goto_0
    if-ge v2, p2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 754
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 755
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lemx;

    .line 756
    invoke-static {v2, v1, v0, p4, p5}, Lemw;->a(ILemx;Ljava/lang/Runnable;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p6, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 753
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 759
    :cond_1
    invoke-virtual {p6, p0}, Letj;->lt(Ljava/lang/String;)V

    .line 760
    const-string v0, "None"

    invoke-virtual {p6, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 762
    :cond_2
    return-void
.end method

.method private static a(Ljava/lang/String;[Lemx;JLetj;)V
    .locals 4

    .prologue
    .line 765
    array-length v0, p1

    if-lez v0, :cond_0

    .line 766
    invoke-virtual {p4, p0}, Letj;->lt(Ljava/lang/String;)V

    .line 767
    const-string v0, " #  queue   exec    age  (queue, execution, age are in milliseconds)"

    invoke-virtual {p4, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 768
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 769
    array-length v1, p1

    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p1, v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p2, p3}, Lemw;->a(ILemx;Ljava/lang/Runnable;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Letj;->lv(Ljava/lang/String;)V

    .line 768
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 773
    :cond_0
    invoke-virtual {p4, p0}, Letj;->lt(Ljava/lang/String;)V

    .line 774
    const-string v0, "None"

    invoke-virtual {p4, v0}, Letj;->lv(Ljava/lang/String;)V

    .line 776
    :cond_1
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/util/List;[Lemx;Letj;)V
    .locals 7
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # [Lemx;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 698
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 701
    invoke-virtual {p0}, Lemw;->getPoolSize()I

    move-result v0

    .line 702
    const-string v1, "SafeScheduledThreadPoolExecutor"

    invoke-virtual {p4, v1}, Letj;->lt(Ljava/lang/String;)V

    .line 703
    const-string v1, "current threads"

    invoke-virtual {p4, v1}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Letn;->be(J)V

    .line 704
    const-string v0, "maximum threads"

    invoke-virtual {p4, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-direct {p0}, Lemw;->auF()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Letn;->d(Ljava/lang/CharSequence;Z)V

    .line 707
    if-eqz p1, :cond_0

    .line 708
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 709
    invoke-interface {v1, p2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 710
    const-string v0, "Executing tasks"

    const v2, 0x7fffffff

    invoke-virtual {p4}, Letj;->avJ()Letj;

    move-result-object v6

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lemw;->a(Ljava/lang/String;Ljava/util/Collection;ILjava/util/Map;JLetj;)V

    .line 715
    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 716
    const-string v0, "Queued tasks"

    const/16 v2, 0x32

    invoke-virtual {p4}, Letj;->avJ()Letj;

    move-result-object v6

    move-object v1, p2

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lemw;->a(Ljava/lang/String;Ljava/util/Collection;ILjava/util/Map;JLetj;)V

    .line 721
    :cond_1
    if-eqz p3, :cond_2

    .line 722
    const-string v0, "Finished tasks"

    invoke-virtual {p4}, Letj;->avJ()Letj;

    move-result-object v1

    invoke-static {v0, p3, v4, v5, v1}, Lemw;->a(Ljava/lang/String;[Lemx;JLetj;)V

    .line 724
    :cond_2
    return-void
.end method

.method private auE()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 538
    invoke-virtual {p0}, Lemw;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->size()I

    move-result v0

    .line 539
    iget v1, p0, Lemw;->cfE:I

    if-le v0, v1, :cond_1

    .line 544
    iget-object v5, p0, Lemw;->dK:Ljava/lang/Object;

    monitor-enter v5

    .line 545
    :try_start_0
    invoke-virtual {p0}, Lemw;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-static {v0}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v6

    .line 547
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 548
    invoke-static {v0}, Lemw;->m(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 549
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 551
    goto :goto_0

    .line 554
    :cond_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    sub-int v7, v0, v1

    .line 555
    iget v0, p0, Lemw;->cfE:I

    if-gt v7, v0, :cond_2

    .line 556
    monitor-exit v5

    .line 584
    :cond_1
    :goto_2
    return-void

    .line 560
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 561
    iget-wide v8, p0, Lemw;->cfI:J

    const-wide/16 v10, 0x1388

    add-long/2addr v8, v10

    cmp-long v4, v0, v8

    if-gtz v4, :cond_3

    .line 562
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 570
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 564
    :cond_3
    :try_start_1
    iput-wide v0, p0, Lemw;->cfI:J

    .line 565
    iget-boolean v0, p0, Lemw;->cfC:Z

    if-eqz v0, :cond_5

    .line 567
    iget-object v0, p0, Lemw;->cfG:Ljava/util/Map;

    invoke-static {v0}, Lemw;->n(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 568
    iget-object v0, p0, Lemw;->cfH:Ljava/util/Queue;

    invoke-static {v0}, Lemw;->b(Ljava/util/Queue;)[Lemx;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    .line 570
    :goto_3
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572
    iget v0, p0, Lemw;->cfF:I

    if-le v7, v0, :cond_4

    const/4 v0, 0x1

    .line 573
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Queue length for executor "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lemw;->mName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " with "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lemw;->auF()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " threads is now "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ". Perhaps some tasks are too long, or the pool is too small."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lemv;->b(ZLjava/lang/String;)V

    .line 578
    iget-boolean v2, p0, Lemw;->cfC:Z

    if-eqz v2, :cond_1

    .line 579
    new-instance v2, Letj;

    invoke-direct {v2, v3}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 580
    invoke-direct {p0, v4, v6, v1, v2}, Lemw;->a(Ljava/util/Map;Ljava/util/List;[Lemx;Letj;)V

    .line 581
    invoke-virtual {v2}, Letj;->avN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lemv;->b(ZLjava/lang/String;)V

    goto :goto_2

    :cond_4
    move v0, v2

    .line 572
    goto :goto_4

    :cond_5
    move-object v1, v3

    move-object v4, v3

    goto :goto_3

    :cond_6
    move v0, v1

    goto/16 :goto_1
.end method

.method private auF()Ljava/lang/String;
    .locals 2

    .prologue
    .line 728
    invoke-virtual {p0}, Lemw;->getCorePoolSize()I

    move-result v0

    .line 729
    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const-string v0, "unbounded"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Ljava/util/Queue;)[Lemx;
    .locals 1
    .param p0    # Ljava/util/Queue;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 529
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Queue;->size()I

    move-result v0

    new-array v0, v0, [Lemx;

    invoke-interface {p0, v0}, Ljava/util/Queue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lemx;

    goto :goto_0
.end method

.method private static final f(JJ)Ljava/lang/String;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 815
    cmp-long v0, p0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    .line 816
    :goto_0
    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 817
    const-string v0, "     -"

    .line 829
    :goto_1
    return-object v0

    .line 815
    :cond_0
    sub-long v0, p2, p0

    goto :goto_0

    .line 818
    :cond_1
    const-wide/16 v2, 0xa

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 819
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "     "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 820
    :cond_2
    const-wide/16 v2, 0x64

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 821
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 822
    :cond_3
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-gez v2, :cond_4

    .line 823
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "   "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 824
    :cond_4
    const-wide/16 v2, 0x2710

    cmp-long v2, v0, v2

    if-gez v2, :cond_5

    .line 825
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 826
    :cond_5
    const-wide/32 v2, 0x186a0

    cmp-long v2, v0, v2

    if-gez v2, :cond_6

    .line 827
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 829
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private l(Ljava/lang/Runnable;)Lemx;
    .locals 14
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 605
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    .line 607
    const/4 v9, 0x0

    .line 608
    const/4 v8, 0x0

    .line 609
    const/4 v7, 0x0

    .line 611
    iget-object v12, p0, Lemw;->dK:Ljava/lang/Object;

    monitor-enter v12

    .line 612
    :try_start_0
    iget-object v0, p0, Lemw;->cfG:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemx;

    .line 613
    if-nez v0, :cond_1

    .line 615
    const-string v0, "ConcurrentUtils"

    const-string v1, "Task not decorated on %s executor: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lemw;->mName:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 616
    new-instance v1, Lemx;

    const-string v2, "unknown: task not decorated"

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lemx;-><init>(Ljava/lang/String;ZJLegq;)V

    .line 617
    iget-object v0, p0, Lemw;->cfG:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v7

    move-object v2, v9

    move-object v3, v1

    move-object v1, v8

    .line 630
    :goto_0
    iput-wide v10, v3, Lemx;->cfL:J

    .line 631
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 635
    if-eqz v2, :cond_0

    .line 636
    const-string v4, "ConcurrentUtils"

    const-string v5, "Task %s was queued for %dms before starting on executor %s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v3, Lemx;->mName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, v3, Lemx;->cfK:J

    sub-long v8, v10, v8

    iget-wide v10, v3, Lemx;->bBZ:J

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lemw;->mName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    invoke-static {v7, v4, v5, v6}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 638
    iget-boolean v4, p0, Lemw;->cfC:Z

    if-eqz v4, :cond_0

    .line 639
    new-instance v4, Letj;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 640
    invoke-direct {p0, v2, v1, v0, v4}, Lemw;->a(Ljava/util/Map;Ljava/util/List;[Lemx;Letj;)V

    .line 641
    const/4 v0, 0x0

    invoke-virtual {v4}, Letj;->avN()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lemv;->b(ZLjava/lang/String;)V

    .line 645
    :cond_0
    return-object v3

    .line 618
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lemx;->isPeriodic()Z

    move-result v1

    if-nez v1, :cond_2

    iget-wide v2, p0, Lemw;->cfJ:J

    const-wide/16 v4, 0x1388

    add-long/2addr v2, v4

    cmp-long v1, v10, v2

    if-lez v1, :cond_2

    iget-wide v2, v0, Lemx;->cfK:J

    iget-wide v4, v0, Lemx;->bBZ:J

    add-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v1, v10, v2

    if-lez v1, :cond_2

    .line 622
    iput-wide v10, p0, Lemw;->cfJ:J

    .line 623
    iget-boolean v1, p0, Lemw;->cfC:Z

    if-eqz v1, :cond_2

    .line 624
    iget-object v1, p0, Lemw;->cfG:Ljava/util/Map;

    invoke-static {v1}, Lemw;->n(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 625
    invoke-virtual {p0}, Lemw;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v1

    invoke-static {v1}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v2

    .line 626
    iget-object v1, p0, Lemw;->cfH:Ljava/util/Queue;

    invoke-static {v1}, Lemw;->b(Ljava/util/Queue;)[Lemx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    move-object v13, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v13

    goto :goto_0

    .line 631
    :catchall_0
    move-exception v0

    monitor-exit v12

    throw v0

    :cond_2
    move-object v1, v8

    move-object v2, v9

    move-object v3, v0

    move-object v0, v7

    goto :goto_0
.end method

.method private static m(Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 649
    instance-of v0, p0, Ljava/util/concurrent/RunnableScheduledFuture;

    if-eqz v0, :cond_0

    .line 650
    check-cast p0, Ljava/util/concurrent/RunnableScheduledFuture;

    .line 651
    invoke-interface {p0}, Ljava/util/concurrent/RunnableScheduledFuture;->isCancelled()Z

    move-result v0

    .line 653
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static n(Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .param p0    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 525
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lijm;->s(Ljava/util/Map;)Lijm;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 418
    iget-object v0, p0, Lemw;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 422
    iget-object v1, p0, Lemw;->dK:Ljava/lang/Object;

    monitor-enter v1

    .line 423
    :try_start_0
    iget-object v0, p0, Lemw;->cfG:Ljava/util/Map;

    invoke-static {v0}, Lemw;->n(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 424
    invoke-virtual {p0}, Lemw;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v2

    invoke-static {v2}, Lijj;->E(Ljava/util/Collection;)Lijj;

    move-result-object v2

    .line 425
    iget-object v3, p0, Lemw;->cfH:Ljava/util/Queue;

    invoke-static {v3}, Lemw;->b(Ljava/util/Queue;)[Lemx;

    move-result-object v3

    .line 426
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    invoke-virtual {p1}, Letj;->avJ()Letj;

    move-result-object v1

    invoke-direct {p0, v0, v2, v3, v1}, Lemw;->a(Ljava/util/Map;Ljava/util/List;[Lemx;Letj;)V

    .line 429
    return-void

    .line 426
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    const-wide/32 v6, 0x493e0

    const/4 v0, 0x0

    .line 483
    sget-object v1, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 485
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lemw;->dK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v4, p0, Lemw;->cfG:Ljava/util/Map;

    if-eqz v4, :cond_0

    iget-object v0, p0, Lemw;->cfG:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemx;

    invoke-virtual {v0}, Lemx;->isPeriodic()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lemw;->cfG:Ljava/util/Map;

    new-instance v5, Lemx;

    invoke-direct {v5, v0}, Lemx;-><init>(Lemx;)V

    invoke-interface {v4, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v4, p0, Lemw;->cfH:Ljava/util/Queue;

    if-eqz v4, :cond_3

    :goto_0
    iget-object v4, p0, Lemw;->cfH:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    const/16 v5, 0x28

    if-lt v4, v5, :cond_1

    iget-object v4, p0, Lemw;->cfH:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->poll()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    if-eqz v0, :cond_3

    :try_start_1
    iput-wide v2, v0, Lemx;->cfM:J

    invoke-static {p1}, Lemw;->m(Ljava/lang/Runnable;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-wide/16 v4, -0x1

    iput-wide v4, v0, Lemx;->cfL:J

    :cond_2
    iget-object v4, p0, Lemw;->cfH:Ljava/util/Queue;

    invoke-interface {v4, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-boolean v1, p0, Lemw;->cfD:Z

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lemx;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_4

    iget-wide v4, v0, Lemx;->cfL:J

    sub-long/2addr v2, v4

    cmp-long v1, v2, v6

    if-lez v1, :cond_4

    const-string v1, "ConcurrentUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Task "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lemx;->mName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " took "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ms, which is over the "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ms threshold"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v1, v0, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 487
    :cond_4
    instance-of v0, p2, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_5

    check-cast p2, Ljava/lang/RuntimeException;

    throw p2

    :cond_5
    instance-of v0, p1, Ljava/util/concurrent/Future;

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v1

    if-eqz v1, :cond_6

    :try_start_2
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 488
    :cond_6
    :goto_1
    invoke-super {p0, p1, p2}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->afterExecute(Ljava/lang/Runnable;Ljava/lang/Throwable;)V

    .line 489
    return-void

    .line 487
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method protected final beforeExecute(Ljava/lang/Thread;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 466
    invoke-direct {p0, p2}, Lemw;->l(Ljava/lang/Runnable;)Lemx;

    move-result-object v0

    .line 469
    if-eqz v0, :cond_0

    iget-object v0, v0, Lemx;->aSM:Legq;

    .line 471
    :goto_0
    sget-object v1, Lege;->bYk:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 473
    return-void

    .line 469
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final decorateTask(Ljava/lang/Runnable;Ljava/util/concurrent/RunnableScheduledFuture;)Ljava/util/concurrent/RunnableScheduledFuture;
    .locals 0

    .prologue
    .line 439
    invoke-direct {p0, p1, p2}, Lemw;->a(Ljava/lang/Object;Ljava/util/concurrent/RunnableScheduledFuture;)V

    .line 441
    invoke-direct {p0}, Lemw;->auE()V

    .line 442
    return-object p2
.end method

.method protected final decorateTask(Ljava/util/concurrent/Callable;Ljava/util/concurrent/RunnableScheduledFuture;)Ljava/util/concurrent/RunnableScheduledFuture;
    .locals 0

    .prologue
    .line 453
    invoke-direct {p0, p1, p2}, Lemw;->a(Ljava/lang/Object;Ljava/util/concurrent/RunnableScheduledFuture;)V

    .line 455
    invoke-direct {p0}, Lemw;->auE()V

    .line 456
    return-object p2
.end method
