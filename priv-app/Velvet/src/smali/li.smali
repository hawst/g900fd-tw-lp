.class final Lli;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private final kI:Ljava/util/ArrayList;

.field private synthetic kJ:Llh;


# direct methods
.method private constructor <init>(Llh;)V
    .locals 1

    .prologue
    .line 2201
    iput-object p1, p0, Lli;->kJ:Llh;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 2202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lli;->kI:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Llh;B)V
    .locals 0

    .prologue
    .line 2201
    invoke-direct {p0, p1}, Lli;-><init>(Llh;)V

    return-void
.end method


# virtual methods
.method public final b(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2222
    invoke-virtual {p0, p1, p2}, Lli;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 2223
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 2227
    iget v6, p1, Landroid/os/Message;->what:I

    .line 2228
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2231
    packed-switch v6, :pswitch_data_0

    .line 2237
    :goto_0
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lli;->kJ:Llh;

    iget-object v1, v1, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    add-int/lit8 v3, v1, -0x1

    if-ltz v3, :cond_1

    .line 2238
    iget-object v1, p0, Lli;->kJ:Llh;

    iget-object v1, v1, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lld;

    .line 2239
    if-nez v1, :cond_0

    .line 2240
    iget-object v1, p0, Lli;->kJ:Llh;

    iget-object v1, v1, Llh;->kw:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v3

    goto :goto_1

    .line 2231
    :pswitch_1
    iget-object v1, p0, Lli;->kJ:Llh;

    iget-object v3, v1, Llh;->kC:Lmo;

    move-object v1, v2

    check-cast v1, Llm;

    invoke-virtual {v3, v1}, Lmo;->d(Llm;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lli;->kJ:Llh;

    iget-object v3, v1, Llh;->kC:Lmo;

    move-object v1, v2

    check-cast v1, Llm;

    invoke-virtual {v3, v1}, Lmo;->e(Llm;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lli;->kJ:Llh;

    iget-object v3, v1, Llh;->kC:Lmo;

    move-object v1, v2

    check-cast v1, Llm;

    invoke-virtual {v3, v1}, Lmo;->f(Llm;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lli;->kJ:Llh;

    iget-object v3, v1, Llh;->kC:Lmo;

    move-object v1, v2

    check-cast v1, Llm;

    invoke-virtual {v3, v1}, Lmo;->g(Llm;)V

    goto :goto_0

    .line 2242
    :cond_0
    :try_start_1
    iget-object v4, p0, Lli;->kI:Ljava/util/ArrayList;

    iget-object v1, v1, Lld;->ku:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v1, v3

    .line 2244
    goto :goto_1

    .line 2246
    :cond_1
    iget-object v1, p0, Lli;->kI:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v5

    .line 2247
    :goto_2
    if-ge v4, v7, :cond_5

    .line 2248
    iget-object v1, p0, Lli;->kI:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Llf;

    move-object v3, v0

    iget-object v8, v3, Llf;->ju:Lld;

    iget-object v9, v3, Llf;->kv:Lle;

    const v1, 0xff00

    and-int/2addr v1, v6

    sparse-switch v1, :sswitch_data_0

    .line 2247
    :cond_2
    :goto_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 2248
    :sswitch_0
    move-object v0, v2

    check-cast v0, Llm;

    move-object v1, v0

    iget v10, v3, Llf;->cZ:I

    and-int/lit8 v10, v10, 0x2

    if-nez v10, :cond_3

    iget-object v3, v3, Llf;->jv:Llb;

    invoke-virtual {v1, v3}, Llm;->e(Llb;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_2

    packed-switch v6, :pswitch_data_1

    goto :goto_3

    :pswitch_5
    invoke-virtual {v9, v8, v1}, Lle;->a(Lld;Llm;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 2251
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lli;->kI:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    throw v1

    :cond_4
    move v3, v5

    .line 2248
    goto :goto_4

    :pswitch_6
    :try_start_2
    invoke-virtual {v9, v8}, Lle;->a(Lld;)V

    goto :goto_3

    :pswitch_7
    invoke-virtual {v9, v8}, Lle;->b(Lld;)V

    goto :goto_3

    :sswitch_1
    packed-switch v6, :pswitch_data_2

    goto :goto_3

    :pswitch_8
    invoke-virtual {v9, v8}, Lle;->c(Lld;)V

    goto :goto_3

    :pswitch_9
    invoke-virtual {v9, v8}, Lle;->d(Lld;)V

    goto :goto_3

    :pswitch_a
    invoke-virtual {v9, v8}, Lle;->e(Lld;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 2251
    :cond_5
    iget-object v1, p0, Lli;->kI:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2252
    return-void

    .line 2231
    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 2248
    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x101
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x201
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
