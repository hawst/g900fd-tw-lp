.class public final Lww;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnDrawListener;


# instance fields
.field final synthetic KX:Lcom/android/launcher3/Launcher;

.field private cq:Z


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Launcher;)V
    .locals 1

    .prologue
    .line 1724
    iput-object p1, p0, Lww;->KX:Lcom/android/launcher3/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1725
    const/4 v0, 0x0

    iput-boolean v0, p0, Lww;->cq:Z

    return-void
.end method


# virtual methods
.method public final onDraw()V
    .locals 4

    .prologue
    .line 1727
    iget-boolean v0, p0, Lww;->cq:Z

    if-eqz v0, :cond_0

    .line 1745
    :goto_0
    return-void

    .line 1728
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lww;->cq:Z

    .line 1734
    iget-object v0, p0, Lww;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->c(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/Workspace;

    move-result-object v0

    iget-object v1, p0, Lww;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v1}, Lcom/android/launcher3/Launcher;->k(Lcom/android/launcher3/Launcher;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher3/Workspace;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1736
    iget-object v0, p0, Lww;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->c(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/Workspace;

    move-result-object v0

    new-instance v1, Lwx;

    invoke-direct {v1, p0, p0}, Lwx;-><init>(Lww;Landroid/view/ViewTreeObserver$OnDrawListener;)V

    invoke-virtual {v0, v1}, Lcom/android/launcher3/Workspace;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
