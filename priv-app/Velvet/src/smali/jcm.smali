.class public final Ljcm;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile eaM:[Ljcm;


# instance fields
.field private aez:I

.field private dHr:Ljava/lang/String;

.field private dQQ:Liyr;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22234
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 22235
    const/4 v0, 0x0

    iput v0, p0, Ljcm;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljcm;->dHr:Ljava/lang/String;

    iput-object v1, p0, Ljcm;->dQQ:Liyr;

    iput-object v1, p0, Ljcm;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljcm;->eCz:I

    .line 22236
    return-void
.end method

.method public static bgF()[Ljcm;
    .locals 2

    .prologue
    .line 22196
    sget-object v0, Ljcm;->eaM:[Ljcm;

    if-nez v0, :cond_1

    .line 22197
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 22199
    :try_start_0
    sget-object v0, Ljcm;->eaM:[Ljcm;

    if-nez v0, :cond_0

    .line 22200
    const/4 v0, 0x0

    new-array v0, v0, [Ljcm;

    sput-object v0, Ljcm;->eaM:[Ljcm;

    .line 22202
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22204
    :cond_1
    sget-object v0, Ljcm;->eaM:[Ljcm;

    return-object v0

    .line 22202
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 22190
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljcm;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljcm;->dHr:Ljava/lang/String;

    iget v0, p0, Ljcm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcm;->aez:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljcm;->dQQ:Liyr;

    if-nez v0, :cond_1

    new-instance v0, Liyr;

    invoke-direct {v0}, Liyr;-><init>()V

    iput-object v0, p0, Ljcm;->dQQ:Liyr;

    :cond_1
    iget-object v0, p0, Ljcm;->dQQ:Liyr;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 22250
    iget v0, p0, Ljcm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 22251
    const/4 v0, 0x1

    iget-object v1, p0, Ljcm;->dHr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 22253
    :cond_0
    iget-object v0, p0, Ljcm;->dQQ:Liyr;

    if-eqz v0, :cond_1

    .line 22254
    const/4 v0, 0x2

    iget-object v1, p0, Ljcm;->dQQ:Liyr;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 22256
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 22257
    return-void
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22212
    iget-object v0, p0, Ljcm;->dHr:Ljava/lang/String;

    return-object v0
.end method

.method public final hasValue()Z
    .locals 1

    .prologue
    .line 22223
    iget v0, p0, Ljcm;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 22261
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 22262
    iget v1, p0, Ljcm;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 22263
    const/4 v1, 0x1

    iget-object v2, p0, Ljcm;->dHr:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22266
    :cond_0
    iget-object v1, p0, Ljcm;->dQQ:Liyr;

    if-eqz v1, :cond_1

    .line 22267
    const/4 v1, 0x2

    iget-object v2, p0, Ljcm;->dQQ:Liyr;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22270
    :cond_1
    return v0
.end method

.method public final tj(Ljava/lang/String;)Ljcm;
    .locals 1

    .prologue
    .line 22215
    if-nez p1, :cond_0

    .line 22216
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22218
    :cond_0
    iput-object p1, p0, Ljcm;->dHr:Ljava/lang/String;

    .line 22219
    iget v0, p0, Ljcm;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcm;->aez:I

    .line 22220
    return-object p0
.end method
