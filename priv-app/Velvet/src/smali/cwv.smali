.class final Lcwv;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic blZ:Lcwm;


# direct methods
.method private constructor <init>(Lcwm;)V
    .locals 0

    .prologue
    .line 660
    iput-object p1, p0, Lcwv;->blZ:Lcwm;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcwm;B)V
    .locals 0

    .prologue
    .line 660
    invoke-direct {p0, p1}, Lcwv;-><init>(Lcwm;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 665
    invoke-virtual {p0, v4}, Lcwv;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 666
    iget-object v1, p0, Lcwv;->blZ:Lcwm;

    invoke-static {v1}, Lcwm;->f(Lcwm;)Lcry;

    move-result-object v1

    const-string v2, "com.sec.action.QUERY_DSP_INFO"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const-string v2, "preferenceEnabled"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcry;->bhp:Z

    const-string v2, "dspSupported"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcry;->bhq:Z

    iget-object v2, v1, Lcry;->mVoiceSettings:Lhym;

    iget-boolean v3, v1, Lcry;->bhq:Z

    invoke-virtual {v2, v3}, Lhym;->fJ(Z)V

    const-string v2, "languageSupported"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcry;->bhr:Z

    const-string v2, "speakerModelPresent"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v1, Lcry;->bhs:Z

    iget-boolean v0, v1, Lcry;->bhr:Z

    iput-boolean v4, v1, Lcry;->bht:Z

    .line 667
    :cond_0
    iget-object v0, p0, Lcwv;->blZ:Lcwm;

    iget-object v1, p0, Lcwv;->blZ:Lcwm;

    invoke-static {v1}, Lcwm;->a(Lcwm;)Landroid/preference/SwitchPreference;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcwm;->c(Landroid/preference/SwitchPreference;)V

    .line 668
    return-void
.end method
