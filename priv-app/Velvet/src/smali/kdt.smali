.class final Lkdt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field eTO:I

.field private eTP:I

.field private eTQ:I

.field final eTR:[I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lkdt;->eTR:[I

    return-void
.end method


# virtual methods
.method final isSet(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 93
    shl-int v1, v0, p1

    .line 94
    iget v2, p0, Lkdt;->eTO:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final set(III)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lkdt;->eTR:[I

    array-length v0, v0

    if-lt p1, v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 75
    :cond_0
    const/4 v0, 0x1

    shl-int/2addr v0, p1

    .line 76
    iget v1, p0, Lkdt;->eTO:I

    or-int/2addr v1, v0

    iput v1, p0, Lkdt;->eTO:I

    .line 77
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    .line 78
    iget v1, p0, Lkdt;->eTP:I

    or-int/2addr v1, v0

    iput v1, p0, Lkdt;->eTP:I

    .line 82
    :goto_1
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_2

    .line 83
    iget v1, p0, Lkdt;->eTQ:I

    or-int/2addr v0, v1

    iput v0, p0, Lkdt;->eTQ:I

    .line 88
    :goto_2
    iget-object v0, p0, Lkdt;->eTR:[I

    aput p3, v0, p1

    goto :goto_0

    .line 80
    :cond_1
    iget v1, p0, Lkdt;->eTP:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lkdt;->eTP:I

    goto :goto_1

    .line 85
    :cond_2
    iget v1, p0, Lkdt;->eTQ:I

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lkdt;->eTQ:I

    goto :goto_2
.end method

.method final tR(I)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 104
    .line 105
    shl-int v0, v2, p1

    iget v3, p0, Lkdt;->eTQ:I

    and-int/2addr v0, v3

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    const/4 v0, 0x2

    .line 106
    :goto_1
    shl-int v3, v2, p1

    iget v4, p0, Lkdt;->eTP:I

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x1

    .line 107
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 105
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
