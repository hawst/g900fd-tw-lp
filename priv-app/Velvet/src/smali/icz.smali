.class public final Licz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final clH:[Ljava/lang/String;

.field private static final dyw:[Ljava/lang/String;

.field private static final dyx:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "attendeeName"

    aput-object v1, v0, v2

    const-string v1, "attendeeEmail"

    aput-object v1, v0, v3

    sput-object v0, Licz;->clH:[Ljava/lang/String;

    .line 67
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "event_id"

    aput-object v1, v0, v2

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "eventLocation"

    aput-object v1, v0, v4

    const/4 v1, 0x3

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "end"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "calendar_displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "startDay"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "endDay"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "hasAttendeeData"

    aput-object v2, v0, v1

    sput-object v0, Licz;->dyw:[Ljava/lang/String;

    .line 106
    new-instance v0, Lida;

    invoke-direct {v0}, Lida;-><init>()V

    sput-object v0, Licz;->dyx:Ljava/util/Comparator;

    return-void
.end method

.method public static M(IZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(visible=1) AND (calendar_access_level=700) AND (selfAttendeeStatus!=2) AND end > ?"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 349
    if-nez p0, :cond_0

    .line 350
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 363
    :goto_0
    return-object v0

    .line 353
    :cond_0
    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    const-string v1, "title"

    invoke-static {p0, v1}, Licz;->o(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    if-nez p1, :cond_1

    .line 357
    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    const-string v1, "description"

    invoke-static {p0, v1}, Licz;->o(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    :cond_1
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ZJJZLandroid/content/ContentResolver;ILandroid/content/Context;Z)Ljava/util/List;
    .locals 24

    .prologue
    .line 122
    sget-object v4, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 123
    move-wide/from16 v0, p2

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 124
    move-wide/from16 v0, p4

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 125
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 127
    const-string v4, " "

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 131
    if-eqz p6, :cond_1

    const-string v4, "begin DESC"

    .line 134
    :goto_0
    sget-object v6, Licz;->dyw:[Ljava/lang/String;

    move-object/from16 v0, v17

    array-length v7, v0

    move/from16 v0, p1

    invoke-static {v7, v0}, Licz;->M(IZ)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v17

    move-wide/from16 v1, p2

    move/from16 v3, p1

    invoke-static {v0, v1, v2, v3}, Licz;->a([Ljava/lang/String;JZ)[Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " LIMIT "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v9, 0x1f4

    move/from16 v0, p8

    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v4, p7

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 139
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 140
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 141
    if-eqz v18, :cond_f

    .line 142
    :cond_0
    :goto_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 149
    const-string v4, "title"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 152
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 153
    const-string v4, "QueryCalendarUtil"

    const-string v5, "Found an event with no title :("

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 131
    :cond_1
    const-string v4, "begin ASC"

    goto :goto_0

    .line 158
    :cond_2
    const-string v4, "description"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 160
    const-string v4, ""

    .line 161
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 162
    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v16, v4

    .line 165
    :goto_2
    move-object/from16 v0, v17

    invoke-static {v5, v0}, Licz;->g(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    if-nez p1, :cond_0

    invoke-static/range {v16 .. v17}, Licz;->g(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 170
    :cond_3
    new-instance v22, Ljnm;

    invoke-direct/range {v22 .. v22}, Ljnm;-><init>()V

    .line 173
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljnm;->xm(Ljava/lang/String;)Ljnm;

    .line 175
    new-instance v23, Lizj;

    invoke-direct/range {v23 .. v23}, Lizj;-><init>()V

    .line 176
    const/16 v4, 0xe

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Lizj;->nV(I)Lizj;

    .line 178
    new-instance v4, Lixr;

    invoke-direct {v4}, Lixr;-><init>()V

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lixr;->qP(Ljava/lang/String;)Lixr;

    move-result-object v4

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lixr;->hh(Z)Lixr;

    move-result-object v4

    move-object/from16 v0, v23

    iput-object v4, v0, Lizj;->dSf:Lixr;

    .line 182
    const-string v4, "eventLocation"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 186
    move-object/from16 v0, v23

    iget-object v4, v0, Lizj;->dSf:Lixr;

    new-instance v6, Ljck;

    invoke-direct {v6}, Ljck;-><init>()V

    iput-object v6, v4, Lixr;->dOp:Ljck;

    .line 187
    move-object/from16 v0, v23

    iget-object v4, v0, Lizj;->dSf:Lixr;

    iget-object v4, v4, Lixr;->dOp:Ljck;

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljck;->tg(Ljava/lang/String;)Ljck;

    .line 188
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 189
    move-object/from16 v0, v23

    iget-object v4, v0, Lizj;->dSf:Lixr;

    iget-object v4, v4, Lixr;->dOp:Ljck;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v11, v5, v6

    iput-object v5, v4, Ljck;->eaH:[Ljava/lang/String;

    .line 192
    :cond_4
    new-instance v4, Ljnn;

    invoke-direct {v4}, Ljnn;-><init>()V

    move-object/from16 v0, v22

    iput-object v4, v0, Ljnm;->evb:Ljnn;

    .line 194
    const-string v4, "begin"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 196
    const-string v4, "end"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 198
    const-string v8, "allDay"

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_11

    .line 200
    const/4 v4, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljnm;->iM(Z)Ljnm;

    .line 202
    const-string v4, "startDay"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 204
    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    .line 205
    invoke-virtual {v5, v4}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 206
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    .line 208
    const-string v4, "endDay"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 212
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v5, v4}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 213
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    .line 215
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 216
    const v4, 0x7f0a0978

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 222
    :goto_3
    move-object/from16 v0, v23

    iget-object v5, v0, Lizj;->dSf:Lixr;

    iget-object v5, v5, Lixr;->dOp:Ljck;

    const v10, 0x7f0a0978

    move-object/from16 v0, p9

    invoke-virtual {v0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljck;->th(Ljava/lang/String;)Ljck;

    move-wide v12, v6

    move-wide v14, v8

    .line 226
    :goto_4
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 227
    move-object/from16 v0, v22

    iget-object v5, v0, Ljnm;->evb:Ljnn;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    iput-object v6, v5, Ljnn;->evl:[Ljava/lang/String;

    .line 229
    :cond_5
    invoke-static {v14, v15}, Lelu;->aN(J)Ljno;

    move-result-object v4

    move-object/from16 v0, v22

    iput-object v4, v0, Ljnm;->euY:Ljno;

    .line 230
    invoke-static {v12, v13}, Lelu;->aN(J)Ljno;

    move-result-object v4

    move-object/from16 v0, v22

    iput-object v4, v0, Ljnm;->euZ:Ljno;

    .line 232
    move-object/from16 v0, v23

    iget-object v4, v0, Lizj;->dSf:Lixr;

    iget-object v4, v4, Lixr;->dOp:Ljck;

    invoke-virtual {v4}, Ljck;->bgD()Z

    move-result v4

    if-nez v4, :cond_6

    .line 233
    move-object/from16 v0, v23

    iget-object v4, v0, Lizj;->dSf:Lixr;

    iget-object v4, v4, Lixr;->dOp:Ljck;

    move-object/from16 v0, v22

    iget-object v5, v0, Ljnm;->euY:Ljno;

    move-object/from16 v0, v22

    iget-object v8, v0, Ljnm;->euZ:Ljno;

    invoke-static {v5}, Lelu;->c(Ljno;)J

    move-result-wide v6

    invoke-static {v8}, Lelu;->c(Ljno;)J

    move-result-wide v8

    const/4 v10, 0x1

    move-object/from16 v5, p9

    invoke-static/range {v5 .. v10}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljck;->th(Ljava/lang/String;)Ljck;

    .line 238
    :cond_6
    move-object/from16 v0, v23

    iget-object v4, v0, Lizj;->dSf:Lixr;

    const-wide/16 v6, 0x3e8

    div-long v6, v14, v6

    invoke-virtual {v4, v6, v7}, Lixr;->ch(J)Lixr;

    move-result-object v4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lixr;->qQ(Ljava/lang/String;)Lixr;

    move-result-object v4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v5

    invoke-virtual {v4, v5}, Lixr;->no(I)Lixr;

    .line 242
    move-object/from16 v0, v23

    iget-object v4, v0, Lizj;->dSf:Lixr;

    const-wide/16 v6, 0x3e8

    div-long v6, v12, v6

    invoke-virtual {v4, v6, v7}, Lixr;->ci(J)Lixr;

    move-result-object v4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lixr;->qR(Ljava/lang/String;)Lixr;

    move-result-object v4

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v5

    invoke-virtual {v4, v5}, Lixr;->np(I)Lixr;

    .line 246
    const-string v4, "event_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 248
    new-instance v6, Ljnu;

    invoke-direct {v6}, Ljnu;-><init>()V

    .line 249
    invoke-virtual {v6, v4, v5}, Ljnu;->dD(J)Ljnu;

    .line 251
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 252
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Ljnu;->xt(Ljava/lang/String;)Ljnu;

    .line 255
    :cond_7
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 256
    invoke-virtual {v6, v11}, Ljnu;->xu(Ljava/lang/String;)Ljnu;

    .line 257
    move-object/from16 v0, v23

    iget-object v7, v0, Lizj;->dSf:Lixr;

    new-instance v8, Ljbp;

    invoke-direct {v8}, Ljbp;-><init>()V

    iput-object v8, v7, Lixr;->aeB:Ljbp;

    .line 258
    move-object/from16 v0, v23

    iget-object v7, v0, Lizj;->dSf:Lixr;

    iget-object v7, v7, Lixr;->aeB:Ljbp;

    invoke-virtual {v7, v11}, Ljbp;->su(Ljava/lang/String;)Ljbp;

    .line 261
    :cond_8
    const-string v7, "calendar_displayName"

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 263
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 264
    invoke-virtual {v6, v7}, Ljnu;->xv(Ljava/lang/String;)Ljnu;

    .line 267
    :cond_9
    const-string v7, "hasAttendeeData"

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_a

    .line 269
    move-object/from16 v0, v23

    iget-object v7, v0, Lizj;->dSf:Lixr;

    move-object/from16 v0, p7

    invoke-static {v0, v4, v5, v6, v7}, Licz;->a(Landroid/content/ContentResolver;JLjnu;Lixr;)V

    .line 271
    :cond_a
    move-object/from16 v0, v22

    iput-object v6, v0, Ljnm;->evg:Ljnu;

    .line 273
    if-eqz p10, :cond_b

    move-object/from16 v0, v22

    iget-object v4, v0, Ljnm;->euY:Ljno;

    invoke-virtual {v4}, Ljno;->Pu()J

    move-result-wide v4

    cmp-long v4, v4, v20

    if-lez v4, :cond_c

    .line 274
    :cond_b
    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    :cond_c
    move-object/from16 v0, v22

    iget-object v4, v0, Ljnm;->evg:Ljnu;

    invoke-virtual {v4}, Ljnu;->nj()J

    move-result-wide v4

    invoke-static {v4, v5}, Ledv;->aE(J)Landroid/content/Intent;

    move-result-object v4

    .line 278
    const-string v5, "beginTime"

    invoke-virtual {v4, v5, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 279
    const-string v5, "endTime"

    invoke-virtual {v4, v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 280
    new-instance v5, Lixx;

    invoke-direct {v5}, Lixx;-><init>()V

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lixx;->ns(I)Lixx;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lixx;->rc(Ljava/lang/String;)Lixx;

    move-result-object v4

    move-object/from16 v0, v23

    iput-object v4, v0, Lizj;->dUp:Lixx;

    .line 283
    sget-object v4, Ljmz;->euH:Ljsm;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v4, v1}, Ljnm;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    goto/16 :goto_1

    .line 218
    :cond_d
    const v4, 0x7f0a0979

    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v11, v5, v10

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 285
    :cond_e
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 288
    :cond_f
    return-object v19

    :cond_10
    move-object/from16 v16, v4

    goto/16 :goto_2

    :cond_11
    move-wide v12, v4

    move-wide v14, v6

    move-object v4, v11

    goto/16 :goto_4
.end method

.method public static a([Ljnm;[Ljnm;)Ljava/util/List;
    .locals 14
    .param p0    # [Ljnm;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # [Ljnm;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 430
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 431
    const/4 v3, 0x0

    .line 432
    const/4 v2, 0x0

    .line 433
    const-wide/32 v0, -0x80000000

    .line 435
    if-eqz p0, :cond_0

    .line 436
    sget-object v4, Licz;->dyx:Ljava/util/Comparator;

    invoke-static {p0, v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 438
    :cond_0
    if-eqz p1, :cond_1

    .line 439
    sget-object v4, Licz;->dyx:Ljava/util/Comparator;

    invoke-static {p1, v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 442
    :cond_1
    if-eqz p1, :cond_e

    if-eqz p0, :cond_e

    move v4, v2

    move v5, v3

    .line 445
    :goto_0
    array-length v2, p1

    if-le v2, v5, :cond_b

    array-length v2, p0

    if-le v2, v4, :cond_b

    .line 446
    aget-object v7, p1, v5

    .line 447
    aget-object v8, p0, v4

    .line 449
    iget-object v2, v7, Ljnm;->euY:Ljno;

    invoke-static {v2}, Lelu;->c(Ljno;)J

    move-result-wide v10

    .line 450
    iget-object v2, v8, Ljnm;->euY:Ljno;

    invoke-static {v2}, Lelu;->c(Ljno;)J

    move-result-wide v12

    .line 458
    cmp-long v2, v0, v10

    if-gtz v2, :cond_6

    cmp-long v0, v0, v12

    if-gtz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Calendar events are out of order."

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 461
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 464
    cmp-long v0, v10, v12

    if-nez v0, :cond_9

    invoke-virtual {v7}, Ljnm;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8}, Ljnm;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 468
    iget-object v0, v7, Ljnm;->evg:Ljnu;

    if-eqz v0, :cond_2

    .line 469
    iget-object v0, v7, Ljnm;->evg:Ljnu;

    iget-object v1, v8, Ljnm;->evg:Ljnu;

    invoke-virtual {v1}, Ljnu;->nj()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Ljnu;->dD(J)Ljnu;

    .line 473
    :cond_2
    sget-object v0, Ljmz;->euH:Ljsm;

    invoke-virtual {v7, v0}, Ljnm;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    sget-object v1, Ljmz;->euH:Ljsm;

    invoke-virtual {v8, v1}, Ljnm;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizj;

    if-eqz v0, :cond_3

    if-nez v1, :cond_7

    .line 478
    :cond_3
    :goto_2
    invoke-virtual {v7}, Ljnm;->bqK()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 479
    invoke-virtual {v8}, Ljnm;->bqK()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljnm;->xn(Ljava/lang/String;)Ljnm;

    .line 481
    const-string v0, "QueryCalendarUtil"

    const-string v1, "Using client TTS single item description."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_4
    invoke-virtual {v7}, Ljnm;->bqL()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 484
    invoke-virtual {v8}, Ljnm;->bqL()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljnm;->xo(Ljava/lang/String;)Ljnm;

    .line 486
    const-string v0, "QueryCalendarUtil"

    const-string v1, "Using client TTS multiple item description."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    :cond_5
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 489
    add-int/lit8 v1, v5, 0x1

    .line 490
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v5, v1

    move-wide v0, v2

    goto/16 :goto_0

    .line 458
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 473
    :cond_7
    iget-object v9, v0, Lizj;->dSf:Lixr;

    if-eqz v9, :cond_8

    iget-object v9, v0, Lizj;->dSf:Lixr;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lixr;->hh(Z)Lixr;

    :cond_8
    iget-object v9, v1, Lizj;->dUp:Lixx;

    if-eqz v9, :cond_3

    iget-object v1, v1, Lizj;->dUp:Lixx;

    iput-object v1, v0, Lizj;->dUp:Lixx;

    goto :goto_2

    .line 491
    :cond_9
    cmp-long v0, v10, v12

    if-gez v0, :cond_a

    .line 493
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move-wide v0, v2

    goto/16 :goto_0

    .line 497
    :cond_a
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 498
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-wide v0, v2

    .line 500
    goto/16 :goto_0

    :cond_b
    move v1, v4

    move v0, v5

    .line 503
    :goto_3
    if-eqz p1, :cond_c

    .line 505
    :goto_4
    array-length v2, p1

    if-ge v0, v2, :cond_c

    .line 506
    aget-object v2, p1, v0

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 505
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 510
    :cond_c
    if-eqz p0, :cond_d

    move v0, v1

    .line 512
    :goto_5
    array-length v1, p0

    if-ge v0, v1, :cond_d

    .line 513
    aget-object v1, p0, v0

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 512
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 517
    :cond_d
    return-object v6

    :cond_e
    move v1, v2

    move v0, v3

    goto :goto_3
.end method

.method private static a(Landroid/content/ContentResolver;JLjnu;Lixr;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 380
    .line 382
    :try_start_0
    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Licz;->clH:[Ljava/lang/String;

    const-string v0, "(event_id=%d)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 385
    if-eqz v1, :cond_2

    .line 387
    :try_start_1
    iget-object v0, p4, Lixr;->dOo:[Ljava/lang/String;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v0, v2}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p4, Lixr;->dOo:[Ljava/lang/String;

    .line 388
    iget-object v0, p4, Lixr;->dOo:[Ljava/lang/String;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v0, v2}, Leqh;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p3, Ljnu;->dOo:[Ljava/lang/String;

    move v0, v7

    .line 389
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 390
    const-string v2, "attendeeName"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 392
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 393
    const-string v2, "attendeeEmail"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 396
    :cond_0
    iget-object v3, p4, Lixr;->dOo:[Ljava/lang/String;

    aput-object v2, v3, v0

    .line 397
    iget-object v3, p3, Ljnu;->dOo:[Ljava/lang/String;

    aput-object v2, v3, v0

    .line 398
    add-int/lit8 v0, v0, 0x1

    .line 399
    goto :goto_0

    .line 400
    :cond_1
    invoke-virtual {p3, v0}, Ljnu;->qX(I)Ljnu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 403
    :cond_2
    invoke-static {v1}, Lhgl;->n(Landroid/database/Cursor;)V

    .line 404
    return-void

    .line 403
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_1
    invoke-static {v1}, Lhgl;->n(Landroid/database/Cursor;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static a([Ljava/lang/String;JZ)[Ljava/lang/String;
    .locals 7

    .prologue
    const/16 v5, 0x25

    const/4 v1, 0x0

    .line 326
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    .line 327
    if-nez p3, :cond_0

    .line 330
    array-length v2, p0

    add-int/2addr v0, v2

    .line 333
    :cond_0
    new-array v2, v0, [Ljava/lang/String;

    .line 334
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    move v0, v1

    .line 335
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_2

    .line 336
    add-int/lit8 v1, v0, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "%"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, p0, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 337
    if-nez p3, :cond_1

    .line 338
    add-int/lit8 v1, v0, 0x1

    array-length v3, p0

    add-int/2addr v1, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "%"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, p0, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 335
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 341
    :cond_2
    return-object v2
.end method

.method public static g(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 299
    array-length v2, p1

    if-nez v2, :cond_1

    .line 320
    :cond_0
    :goto_0
    return v0

    .line 302
    :cond_1
    invoke-static {}, Ljava/text/BreakIterator;->getWordInstance()Ljava/text/BreakIterator;

    move-result-object v4

    .line 303
    invoke-virtual {v4, p0}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 304
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 305
    invoke-virtual {v4}, Ljava/text/BreakIterator;->first()I

    move-result v3

    .line 306
    invoke-virtual {v4}, Ljava/text/BreakIterator;->next()I

    move-result v2

    .line 307
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    .line 308
    :goto_1
    const/4 v7, -0x1

    if-eq v2, v7, :cond_2

    .line 309
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 311
    invoke-virtual {v4}, Ljava/text/BreakIterator;->next()I

    move-result v3

    move v8, v3

    move v3, v2

    move v2, v8

    goto :goto_1

    .line 314
    :cond_2
    array-length v3, p1

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_0

    aget-object v4, p1, v2

    .line 315
    invoke-virtual {v4, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    move v0, v1

    .line 316
    goto :goto_0

    .line 314
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method private static o(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 368
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p0, :cond_1

    .line 369
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    const-string v2, " LIKE ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    add-int/lit8 v2, p0, -0x1

    if-eq v0, v2, :cond_0

    .line 372
    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 375
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
