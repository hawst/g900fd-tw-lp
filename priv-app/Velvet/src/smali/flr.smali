.class public final Lflr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lejx;
.implements Lfpu;


# instance fields
.field public final cuJ:Lfkz;

.field final cvd:Lflv;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

.field private final cvf:Lfld;

.field cvg:Lfky;

.field private cvh:Ljava/util/Map;

.field private cvi:Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

.field private cvj:Landroid/os/Parcelable;

.field cvk:I

.field final mCardContainer:Lfmt;

.field private final mRunner:Lerp;

.field final mScrollView:Lekf;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lerp;Lflv;Lcom/google/android/shared/ui/SuggestionGridLayout;Lekf;Lfmt;Lgbr;Lflb;)V
    .locals 7
    .param p3    # Lflv;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lflb;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p2, p0, Lflr;->mRunner:Lerp;

    .line 102
    iput-object p3, p0, Lflr;->cvd:Lflv;

    .line 103
    iput-object p4, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    .line 104
    iput-object p5, p0, Lflr;->mScrollView:Lekf;

    .line 105
    iput-object p6, p0, Lflr;->mCardContainer:Lfmt;

    .line 106
    new-instance v0, Lfkz;

    invoke-direct {v0, p6, p5, p4, p8}, Lfkz;-><init>(Lfmt;Lekf;Lcom/google/android/shared/ui/SuggestionGridLayout;Lflb;)V

    iput-object v0, p0, Lflr;->cuJ:Lfkz;

    .line 108
    new-instance v0, Lfld;

    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iget-object v6, p0, Lflr;->cuJ:Lfkz;

    move-object v1, p1

    move-object v3, p4

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v6}, Lfld;-><init>(Landroid/app/Activity;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lfmt;Lgbr;Lfkz;)V

    iput-object v0, p0, Lflr;->cvf:Lfld;

    .line 110
    return-void
.end method

.method public static bm(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 198
    const v0, 0x7f110009

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 199
    return-void
.end method

.method static bn(Landroid/view/View;)Lfkd;
    .locals 1
    .param p0    # Landroid/view/View;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 583
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f11000a

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkd;

    goto :goto_0
.end method

.method private c(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 565
    invoke-static {p1}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v0

    .line 566
    if-eqz v0, :cond_0

    .line 567
    invoke-static {p1, p2}, Lfln;->b(Landroid/view/View;Z)V

    .line 568
    const v0, 0x7f110004

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 569
    if-eqz v0, :cond_0

    .line 570
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 571
    invoke-static {v0, p2}, Lfln;->b(Landroid/view/View;Z)V

    goto :goto_0

    .line 575
    :cond_0
    return-void
.end method

.method private e(Lfkd;)Landroid/view/View;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 464
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 465
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 466
    invoke-static {v1}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 470
    :goto_1
    return-object v0

    .line 464
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 470
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static h(Lizj;Lizj;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 688
    invoke-virtual {p1}, Lizj;->bcQ()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 689
    invoke-virtual {p0}, Lizj;->bcQ()J

    move-result-wide v2

    invoke-virtual {p1}, Lizj;->bcQ()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 691
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 689
    goto :goto_0

    .line 691
    :cond_2
    invoke-virtual {p0}, Lizj;->getType()I

    move-result v2

    invoke-virtual {p1}, Lizj;->getType()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final U(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 387
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v1

    .line 388
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 389
    iget-object v2, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v2, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 390
    invoke-static {v2}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v3

    .line 391
    if-eqz v3, :cond_3

    .line 392
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 393
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 394
    invoke-virtual {v2, v5}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 395
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 396
    const-string v6, "card:views"

    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 398
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 399
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 400
    const-string v5, "card:focusedViewId"

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 402
    :cond_1
    invoke-interface {v3}, Lfkd;->aAz()Landroid/os/Bundle;

    move-result-object v2

    .line 403
    if-eqz v2, :cond_2

    .line 404
    const-string v5, "card:adapterState"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 406
    :cond_2
    invoke-virtual {v4}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 407
    invoke-interface {v3}, Lfkd;->getEntry()Lizj;

    move-result-object v2

    invoke-static {v2}, Lgbm;->N(Lizj;)J

    move-result-wide v2

    .line 409
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413
    :cond_4
    const-string v0, "NowCardsViewWrapper.card_state_map"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 415
    iget-object v1, p0, Lflr;->cuJ:Lfkz;

    invoke-virtual {v1}, Lfkz;->aAH()Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x0

    .line 416
    :cond_5
    :goto_1
    if-eqz v0, :cond_9

    .line 417
    const-string v1, "NowCardsViewWrapper.settings"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 421
    :cond_6
    :goto_2
    return-void

    .line 415
    :cond_7
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "entry-hash"

    iget-object v3, v1, Lfkz;->cuA:Lfkd;

    invoke-interface {v3}, Lfkd;->getEntry()Lizj;

    move-result-object v3

    invoke-static {v3}, Lgbm;->N(Lizj;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v2, v1, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    if-eqz v2, :cond_8

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iget-object v1, v1, Lfkz;->cuE:Lcom/google/android/sidekick/shared/training/CardSettingsView;

    invoke-virtual {v1, v2}, Lcom/google/android/sidekick/shared/training/CardSettingsView;->saveHierarchyState(Landroid/util/SparseArray;)V

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "back-of-card"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    goto :goto_1

    :cond_8
    iget-object v2, v1, Lfkz;->cuC:Landroid/util/SparseArray;

    if-eqz v2, :cond_5

    const-string v2, "back-of-card"

    iget-object v1, v1, Lfkz;->cuC:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    goto :goto_1

    .line 419
    :cond_9
    iget-object v0, p0, Lflr;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->getScrollY()I

    move-result v0

    if-lez v0, :cond_6

    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    iget v2, p0, Lflr;->cvk:I

    sub-int v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->ic(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {v2}, Lfkd;->getEntry()Lizj;

    move-result-object v2

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget v3, p0, Lflr;->cvk:I

    add-int/2addr v1, v3

    sub-int v0, v1, v0

    new-instance v1, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    invoke-direct {v1, v2, v0}, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;-><init>(Lizj;I)V

    const-string v0, "NowCardsViewWrapper.scroll_pos"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_2
.end method

.method public final V(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 426
    if-eqz p1, :cond_0

    .line 427
    const-string v0, "NowCardsViewWrapper.card_state_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lflr;->cvh:Ljava/util/Map;

    .line 429
    const-string v0, "NowCardsViewWrapper.scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    iput-object v0, p0, Lflr;->cvi:Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    .line 430
    const-string v0, "NowCardsViewWrapper.settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lflr;->cvj:Landroid/os/Parcelable;

    .line 432
    :cond_0
    return-void
.end method

.method public final a(Lejw;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 511
    const/4 v3, 0x0

    .line 512
    const/4 v1, 0x0

    .line 513
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 514
    invoke-static {v0}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v5

    .line 515
    if-eqz v5, :cond_3

    .line 517
    iget-object v3, p0, Lflr;->mCardContainer:Lfmt;

    invoke-interface {v5}, Lfkd;->aAy()Lizj;

    move-result-object v6

    invoke-interface {v3, v6}, Lfmt;->u(Lizj;)V

    .line 519
    iget-object v3, p1, Lejw;->cco:Lijj;

    invoke-virtual {v3}, Lijj;->size()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 522
    invoke-interface {v5}, Lfkd;->aAy()Lizj;

    move-result-object v1

    .line 526
    :cond_0
    invoke-direct {p0, v0, v2}, Lflr;->c(Landroid/view/View;Z)V

    move v0, v2

    :goto_1
    move v3, v0

    .line 528
    goto :goto_0

    .line 530
    :cond_1
    if-eqz v3, :cond_2

    .line 531
    invoke-virtual {p1, p0}, Lejw;->a(Lejx;)V

    .line 532
    iget-object v0, p0, Lflr;->mCardContainer:Lfmt;

    invoke-interface {v0, p1, v1}, Lfmt;->a(Lejw;Lizj;)V

    .line 534
    iget-object v0, p0, Lflr;->mCardContainer:Lfmt;

    invoke-interface {v0}, Lfmt;->aAD()Lfml;

    move-result-object v0

    invoke-virtual {v0}, Lfml;->aBa()Lfor;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    invoke-interface {v0}, Lfor;->azL()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 536
    :cond_2
    :goto_2
    return-void

    .line 534
    :catch_0
    move-exception v0

    const-string v1, "NowRemoteClient"

    const-string v2, "Error recording card swiped for dismiss"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final a(Lfkd;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 776
    iget-object v0, p0, Lflr;->cvf:Lfld;

    invoke-virtual {v0, p1}, Lfld;->a(Lfkd;)Landroid/view/View;

    move-result-object v0

    .line 779
    new-instance v1, Lekm;

    const/4 v2, -0x2

    invoke-direct {v1, v4, v2, v3}, Lekm;-><init>(III)V

    .line 781
    iput-boolean v3, v1, Lekm;->cdu:Z

    .line 782
    iput-boolean v3, v1, Lekm;->cdv:Z

    .line 783
    iput v4, v1, Lekm;->column:I

    .line 784
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 785
    invoke-static {v0}, Lflr;->bm(Landroid/view/View;)V

    .line 786
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->addView(Landroid/view/View;I)V

    .line 787
    return-void
.end method

.method public final a(Lfkd;ZLepp;)V
    .locals 5
    .param p3    # Lepp;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 801
    invoke-direct {p0, p1}, Lflr;->e(Lfkd;)Landroid/view/View;

    move-result-object v0

    .line 802
    if-nez p2, :cond_0

    invoke-interface {p1}, Lfkd;->getEntry()Lizj;

    move-result-object v1

    iget-object v1, v1, Lizj;->dSa:Ljdj;

    if-nez v1, :cond_1

    .line 803
    :cond_0
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    .line 812
    :goto_0
    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, p3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->post(Ljava/lang/Runnable;)Z

    .line 813
    return-void

    .line 805
    :cond_1
    iget-object v1, p0, Lflr;->cvf:Lfld;

    invoke-virtual {v1, p1}, Lfld;->a(Lfkd;)Landroid/view/View;

    move-result-object v1

    .line 806
    iget-object v2, p0, Lflr;->cvf:Lfld;

    invoke-virtual {v2, p1}, Lfld;->b(Lfkd;)Landroid/view/View;

    move-result-object v2

    .line 807
    iget-object v3, p0, Lflr;->cvf:Lfld;

    invoke-virtual {v3, p1, v1}, Lfld;->a(Lfkd;Landroid/view/View;)Landroid/view/View;

    move-result-object v3

    .line 808
    iget-object v4, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 809
    invoke-static {v1}, Lflr;->bm(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)V
    .locals 9
    .param p5    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 135
    invoke-static {}, Lenu;->auR()V

    .line 136
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lflr;->b(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)Lesj;

    move-result-object v0

    check-cast v0, Lesk;

    .line 140
    iget-object v1, p0, Lflr;->mRunner:Lerp;

    invoke-interface {v1, v0}, Lerp;->a(Lesk;)V

    .line 141
    return-void
.end method

.method public final aAQ()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 710
    iget-object v0, p0, Lflr;->cuJ:Lfkz;

    invoke-virtual {v0}, Lfkz;->aAH()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lflr;->cuJ:Lfkz;

    invoke-virtual {v0}, Lfkz;->aAG()Z

    .line 712
    const-string v0, "BACK_BUTTON_CLOSE_FEEDBACK"

    .line 714
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ak(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 790
    invoke-static {p1}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v0

    .line 791
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 792
    iget-object v2, p0, Lflr;->mCardContainer:Lfmt;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, Lfkd;->a(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/view/View;)V

    .line 795
    return-void
.end method

.method public final b(Ljava/util/List;Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;ZZLjava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZ)Lesj;
    .locals 15
    .param p5    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 163
    iget-object v1, p0, Lflr;->cvg:Lfky;

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Lflr;->cvg:Lfky;

    invoke-virtual {v1}, Lfky;->cancel()V

    .line 169
    :cond_0
    iget-object v1, p0, Lflr;->cvi:Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    if-eqz v1, :cond_1

    .line 170
    iget-object v10, p0, Lflr;->cvi:Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    .line 173
    :goto_0
    iget-object v1, p0, Lflr;->mCardContainer:Lfmt;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Lfmt;->a(Lcom/google/android/sidekick/shared/remoteapi/CardRenderingContext;)V

    .line 174
    new-instance v1, Lfky;

    iget-object v2, p0, Lflr;->mRunner:Lerp;

    iget-object v4, p0, Lflr;->cvf:Lfld;

    iget-object v8, p0, Lflr;->cvh:Ljava/util/Map;

    iget-object v13, p0, Lflr;->cvj:Landroid/os/Parcelable;

    move-object v3, p0

    move-object/from16 v5, p1

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v9, p5

    move/from16 v11, p7

    move/from16 v12, p8

    move-object v14, p0

    invoke-direct/range {v1 .. v14}, Lfky;-><init>(Lerp;Lflr;Lfld;Ljava/util/List;ZZLjava/util/Map;Ljava/util/List;Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;ZZLandroid/os/Parcelable;Lfpu;)V

    iput-object v1, p0, Lflr;->cvg:Lfky;

    .line 179
    const/4 v1, 0x0

    iput-object v1, p0, Lflr;->cvh:Ljava/util/Map;

    .line 180
    const/4 v1, 0x0

    iput-object v1, p0, Lflr;->cvi:Lcom/google/android/sidekick/shared/client/NowCardsViewScrollState;

    .line 181
    const/4 v1, 0x0

    iput-object v1, p0, Lflr;->cvj:Landroid/os/Parcelable;

    .line 183
    iget-object v1, p0, Lflr;->cvg:Lfky;

    return-object v1

    :cond_1
    move-object/from16 v10, p6

    goto :goto_0
.end method

.method public final b(Lifw;)Lfkd;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 843
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 844
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 845
    invoke-static {v1}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v1

    .line 846
    if-eqz v1, :cond_0

    invoke-interface {p1, v1}, Lifw;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 850
    :goto_1
    return-object v0

    .line 843
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 850
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(Lejw;)V
    .locals 4

    .prologue
    .line 540
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 541
    invoke-static {v0}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v0

    .line 542
    if-eqz v0, :cond_0

    .line 543
    iget-object v2, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v2}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lflr;->mCardContainer:Lfmt;

    invoke-interface {v0, v2, v3}, Lfkd;->b(Landroid/content/Context;Lfmt;)V

    goto :goto_0

    .line 546
    :cond_1
    return-void
.end method

.method public final c(Lizj;Ljava/util/Collection;)V
    .locals 13
    .param p2    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v12, 0x7f050000

    const/16 v11, 0x8

    const/4 v10, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 592
    invoke-static {p1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v4

    move v0, v2

    :goto_0
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v5}, Lfkd;->getEntry()Lizj;

    move-result-object v5

    invoke-static {v5}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v5

    invoke-virtual {v4, v5}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 595
    :goto_1
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v11, :cond_6

    .line 596
    invoke-static {v1}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v4

    move-object v0, v1

    .line 597
    check-cast v0, Landroid/view/ViewGroup;

    .line 598
    if-eqz p2, :cond_3

    .line 601
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizj;

    .line 602
    invoke-interface {v4, v0, v1}, Lfkd;->a(Landroid/view/View;Lizj;)Landroid/view/View;

    move-result-object v1

    .line 604
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eq v5, v11, :cond_0

    .line 605
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    .line 606
    invoke-virtual {v1, v10, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 607
    iget-object v6, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v6}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v12}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v6

    .line 609
    const-wide/16 v8, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 610
    invoke-virtual {v6, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 611
    new-instance v7, Lfls;

    invoke-direct {v7, p0, v5, v1}, Lfls;-><init>(Lflr;Landroid/view/ViewParent;Landroid/view/View;)V

    invoke-virtual {v6, v7}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 619
    invoke-virtual {v6}, Landroid/animation/Animator;->start()V

    goto :goto_2

    .line 592
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object v1, v3

    goto :goto_1

    .line 623
    :cond_3
    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-static {v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->aN(Landroid/view/View;)Ljava/util/List;

    move-result-object v4

    .line 624
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v5, v0, [Landroid/animation/Animator;

    .line 625
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v12}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v5, v2

    .line 627
    :goto_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 628
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 629
    invoke-virtual {v0, v10, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 630
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_4

    .line 631
    add-int/lit8 v6, v2, 0x1

    aget-object v7, v5, v2

    invoke-virtual {v7}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v7

    aput-object v7, v5, v6

    .line 633
    :cond_4
    aget-object v6, v5, v2

    invoke-virtual {v6, v0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 627
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 635
    :cond_5
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 636
    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 638
    iget-object v2, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    .line 639
    new-instance v3, Lflt;

    invoke-direct {v3, p0, v2, v1}, Lflt;-><init>(Lflr;Lcom/google/android/shared/ui/SuggestionGridLayout;Landroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 645
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 648
    :cond_6
    return-void
.end method

.method public final d(Lejw;)V
    .locals 4

    .prologue
    .line 550
    iget-object v0, p1, Lejw;->cco:Lijj;

    invoke-virtual {v0}, Lijj;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 551
    invoke-static {v0}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v2

    .line 552
    if-eqz v2, :cond_0

    .line 553
    invoke-interface {v2}, Lfkd;->aAy()Lizj;

    move-result-object v2

    .line 554
    iget-object v3, p0, Lflr;->mCardContainer:Lfmt;

    invoke-interface {v3, v2}, Lfmt;->v(Lizj;)V

    .line 557
    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lflr;->c(Landroid/view/View;Z)V

    goto :goto_0

    .line 559
    :cond_1
    return-void
.end method

.method public final d(Lfkd;)V
    .locals 2

    .prologue
    .line 372
    invoke-direct {p0, p1}, Lflr;->e(Lfkd;)Landroid/view/View;

    move-result-object v0

    .line 373
    if-eqz v0, :cond_0

    .line 374
    iget-object v1, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->removeView(Landroid/view/View;)V

    .line 376
    :cond_0
    return-void
.end method

.method public final g(Lizj;Lizj;)V
    .locals 8

    .prologue
    .line 654
    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 656
    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    .line 657
    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 658
    invoke-static {v0}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v3

    .line 659
    if-eqz v3, :cond_2

    .line 660
    invoke-interface {v3}, Lfkd;->getEntry()Lizj;

    move-result-object v0

    invoke-static {v0, p1}, Lflr;->h(Lizj;Lizj;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667
    invoke-interface {v3, p1}, Lfkd;->s(Lizj;)V

    .line 668
    iget-object v0, p0, Lflr;->mCardContainer:Lfmt;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-interface {v3, v2}, Lfkd;->aP(Landroid/content/Context;)V

    .line 672
    :cond_0
    invoke-interface {v3}, Lfkd;->aAv()Lizq;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 673
    invoke-interface {v3}, Lfkd;->aAv()Lizq;

    move-result-object v0

    iget-object v4, v0, Lizq;->dUX:[Lizj;

    array-length v5, v4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    .line 674
    invoke-static {v6, p1}, Lflr;->h(Lizj;Lizj;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 675
    invoke-interface {v3, v6, p1}, Lfkd;->f(Lizj;Lizj;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 676
    iget-object v6, p0, Lflr;->mCardContainer:Lfmt;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    iget-object v6, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-interface {v3, v2}, Lfkd;->aP(Landroid/content/Context;)V

    .line 673
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 656
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 685
    :cond_3
    return-void
.end method

.method public final i(Lizj;I)V
    .locals 7

    .prologue
    .line 315
    invoke-static {p1}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildCount()I

    move-result v0

    if-ge v3, v0, :cond_6

    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lflr;->bn(Landroid/view/View;)Lfkd;

    move-result-object v0

    if-eqz v0, :cond_5

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Lfkd;->getEntry()Lizj;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Lfkd;->aAv()Lizq;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Lfkd;->aAv()Lizq;

    move-result-object v0

    iget-object v0, v0, Lizq;->dUX:[Lizj;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizj;

    invoke-static {v0}, Lgbg;->j(Ljsr;)Lgbg;

    move-result-object v6

    invoke-virtual {v4, v6}, Lgbg;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 316
    :goto_2
    if-eqz v1, :cond_3

    .line 317
    iget-object v0, p0, Lflr;->mScrollView:Lekf;

    invoke-interface {v0}, Lekf;->ate()I

    move-result v2

    .line 318
    iget-object v0, p0, Lflr;->cve:Lcom/google/android/shared/ui/SuggestionGridLayout;

    invoke-virtual {v0}, Lcom/google/android/shared/ui/SuggestionGridLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d01b5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 326
    sub-int/2addr v2, v0

    if-gt p2, v2, :cond_1

    const v2, 0x7fffffff

    if-ne p2, v2, :cond_2

    :cond_1
    move p2, v0

    .line 330
    :cond_2
    iget-object v0, p0, Lflr;->mScrollView:Lekf;

    invoke-interface {v0, v1, p2}, Lekf;->t(Landroid/view/View;I)V

    .line 335
    :cond_3
    return-void

    .line 315
    :cond_4
    invoke-virtual {p1}, Lizj;->getType()I

    move-result v6

    invoke-virtual {v0}, Lizj;->getType()I

    move-result v0

    if-ne v6, v0, :cond_7

    if-nez v2, :cond_7

    move-object v0, v1

    :goto_3
    move-object v2, v0

    goto :goto_1

    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :cond_6
    move-object v1, v2

    goto :goto_2

    :cond_7
    move-object v0, v2

    goto :goto_3
.end method
