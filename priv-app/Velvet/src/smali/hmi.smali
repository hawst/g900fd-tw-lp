.class public final Lhmi;
.super Lhlu;
.source "PG"


# instance fields
.field private final dkU:Ljava/lang/String;

.field private final dku:Libs;

.field private final dkw:Lglm;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Leoj;Lglm;Libs;Ljava/lang/String;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lhlu;-><init>(Leoj;)V

    .line 36
    iput-object p2, p0, Lhmi;->dkw:Lglm;

    .line 37
    iput-object p3, p0, Lhmi;->dku:Libs;

    .line 38
    iput-object p4, p0, Lhmi;->dkU:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lhmi;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 40
    return-void
.end method

.method private a(Lcom/google/android/search/shared/actions/SelfNoteAction;Z)[Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 63
    new-instance v2, Libu;

    invoke-direct {v2}, Libu;-><init>()V

    .line 64
    iget-object v0, p0, Lhmi;->dkU:Ljava/lang/String;

    iput-object v0, v2, Libu;->dxt:Ljava/lang/CharSequence;

    .line 65
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SelfNoteAction;->aik()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Libu;->dxu:Ljava/lang/CharSequence;

    .line 67
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SelfNoteAction;->ail()Ljava/util/concurrent/Future;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    .line 70
    :try_start_0
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, v2, Libu;->dxv:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 83
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 84
    new-instance v0, Lefl;

    invoke-direct {v0}, Lefl;-><init>()V

    .line 86
    iget-object v3, p0, Lhmi;->dkw:Lglm;

    invoke-interface {v3, v0}, Lglm;->a(Lefk;)V

    .line 88
    :try_start_1
    invoke-virtual {v0}, Lefl;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_3

    .line 89
    if-eqz v0, :cond_1

    .line 90
    const/4 v1, 0x1

    :try_start_2
    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v1, v3

    iput-object v1, v2, Libu;->dxs:[Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_4

    .line 97
    :cond_1
    :goto_1
    iget-object v1, p0, Lhmi;->dku:Libs;

    invoke-virtual {v1, v2, p2, v0, v4}, Libs;->a(Libu;ZLjava/lang/String;Z)[Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    const-string v1, "SelfNoteActionExecutor"

    const-string v3, "Unable to attach the audio"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 73
    :catch_1
    move-exception v0

    .line 74
    const-string v1, "SelfNoteActionExecutor"

    const-string v3, "Unable to attach the audio"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 93
    :catch_2
    move-exception v0

    move-object v0, v1

    :goto_2
    const-string v1, "SelfNoteActionExecutor"

    const-string v3, "Unable to get account"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 95
    :catch_3
    move-exception v0

    move-object v0, v1

    :goto_3
    const-string v1, "SelfNoteActionExecutor"

    const-string v3, "Unable to get account"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_3

    .line 93
    :catch_5
    move-exception v1

    goto :goto_2
.end method


# virtual methods
.method public final bridge synthetic H(Lcom/google/android/search/shared/actions/VoiceAction;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lhmi;->dku:Libs;

    const-string v0, "com.google.android.gm"

    return-object v0
.end method

.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 24
    check-cast p1, Lcom/google/android/search/shared/actions/SelfNoteAction;

    iget-object v0, p0, Lhmi;->dku:Libs;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SelfNoteAction;->canExecute()Z

    move-result v1

    new-array v2, v4, [Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v4}, Libs;->K(ZZ)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v2, v3

    return-object v2
.end method

.method protected final synthetic K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 24
    check-cast p1, Lcom/google/android/search/shared/actions/SelfNoteAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SelfNoteAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->avd()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    iget-object v2, p0, Lhmi;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1}, Lcom/google/android/shared/util/MatchingAppInfo;->ave()Lcom/google/android/shared/util/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v3, [Landroid/content/Intent;

    goto :goto_0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/search/shared/actions/SelfNoteAction;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhmi;->a(Lcom/google/android/search/shared/actions/SelfNoteAction;Z)[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/search/shared/actions/SelfNoteAction;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lhmi;->a(Lcom/google/android/search/shared/actions/SelfNoteAction;Z)[Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    check-cast p1, Lcom/google/android/search/shared/actions/SelfNoteAction;

    const/16 v0, 0x65

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lhmi;->mIntentStarter:Leoj;

    new-array v1, v4, [Landroid/content/Intent;

    iget-object v2, p0, Lhmi;->dku:Libs;

    invoke-virtual {v2, v3, v4}, Libs;->K(ZZ)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lhlu;->d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z

    move-result v0

    goto :goto_0
.end method
