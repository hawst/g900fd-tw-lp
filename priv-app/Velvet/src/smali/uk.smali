.class final Luk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private synthetic FQ:F

.field private synthetic FR:F

.field private synthetic FS:F

.field private synthetic FT:F

.field private synthetic FU:Luj;


# direct methods
.method constructor <init>(Luj;FFFF)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Luk;->FU:Luj;

    iput p2, p0, Luk;->FQ:F

    iput p3, p0, Luk;->FR:F

    iput p4, p0, Luk;->FS:F

    iput p5, p0, Luk;->FT:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 88
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 90
    iget v1, p0, Luk;->FQ:F

    mul-float/2addr v1, v0

    iget-object v2, p0, Luk;->FU:Luj;

    invoke-static {v2}, Luj;->b(Luj;)F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 91
    iget v2, p0, Luk;->FR:F

    mul-float/2addr v2, v0

    iget-object v3, p0, Luk;->FU:Luj;

    invoke-static {v3}, Luj;->c(Luj;)F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 93
    iget-object v3, p0, Luk;->FU:Luj;

    int-to-float v4, v1

    invoke-static {v3, v4}, Luj;->a(Luj;F)F

    .line 94
    iget-object v3, p0, Luk;->FU:Luj;

    int-to-float v4, v2

    invoke-static {v3, v4}, Luj;->b(Luj;F)F

    .line 95
    iget-object v3, p0, Luk;->FU:Luj;

    iget v4, p0, Luk;->FS:F

    iget v5, p0, Luk;->FT:F

    iget v6, p0, Luk;->FS:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    invoke-virtual {v3, v4}, Luj;->setScaleX(F)V

    .line 96
    iget-object v3, p0, Luk;->FU:Luj;

    iget v4, p0, Luk;->FS:F

    iget v5, p0, Luk;->FT:F

    iget v6, p0, Luk;->FS:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    invoke-virtual {v3, v4}, Luj;->setScaleY(F)V

    .line 97
    invoke-static {}, Luj;->gg()F

    move-result v3

    cmpl-float v3, v3, v7

    if-eqz v3, :cond_0

    .line 98
    iget-object v3, p0, Luk;->FU:Luj;

    invoke-static {}, Luj;->gg()F

    move-result v4

    mul-float/2addr v4, v0

    sub-float v0, v7, v0

    add-float/2addr v0, v4

    invoke-virtual {v3, v0}, Luj;->setAlpha(F)V

    .line 101
    :cond_0
    iget-object v0, p0, Luk;->FU:Luj;

    invoke-virtual {v0}, Luj;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 102
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 107
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Luk;->FU:Luj;

    iget-object v3, p0, Luk;->FU:Luj;

    invoke-virtual {v3}, Luj;->getTranslationX()F

    move-result v3

    int-to-float v1, v1

    add-float/2addr v1, v3

    invoke-virtual {v0, v1}, Luj;->setTranslationX(F)V

    .line 105
    iget-object v0, p0, Luk;->FU:Luj;

    iget-object v1, p0, Luk;->FU:Luj;

    invoke-virtual {v1}, Luj;->getTranslationY()F

    move-result v1

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Luj;->setTranslationY(F)V

    goto :goto_0
.end method
