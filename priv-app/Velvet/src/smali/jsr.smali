.class public abstract Ljsr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field protected volatile eCz:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Ljsr;->eCz:I

    return-void
.end method

.method public static final a(Ljsr;[BII)V
    .locals 3

    .prologue
    .line 114
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0, p3}, Ljsj;->m([BII)Ljsj;

    move-result-object v0

    .line 116
    invoke-virtual {p0, v0}, Ljsr;->a(Ljsj;)V

    .line 117
    iget v1, v0, Ljsj;->bBF:I

    iget v0, v0, Ljsj;->position:I

    sub-int v0, v1, v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :catch_0
    move-exception v0

    .line 119
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 121
    :cond_0
    return-void
.end method

.method public static final b(Ljsr;[BII)Ljsr;
    .locals 2

    .prologue
    .line 140
    :try_start_0
    invoke-static {p1, p2, p3}, Ljsi;->l([BII)Ljsi;

    move-result-object v0

    .line 142
    invoke-virtual {p0, v0}, Ljsr;->a(Ljsi;)Ljsr;

    .line 143
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljsi;->rT(I)V
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 144
    return-object p0

    .line 145
    :catch_0
    move-exception v0

    .line 146
    throw v0

    .line 148
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final c(Ljsr;[B)Ljsr;
    .locals 2

    .prologue
    .line 130
    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Ljsr;->b(Ljsr;[BII)Ljsr;

    move-result-object v0

    return-object v0
.end method

.method public static final m(Ljsr;)[B
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Ljsr;->buf()I

    move-result v0

    new-array v0, v0, [B

    .line 100
    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, v0, v1, v2}, Ljsr;->a(Ljsr;[BII)V

    .line 101
    return-object v0
.end method


# virtual methods
.method public abstract a(Ljsi;)Ljsr;
.end method

.method public a(Ljsj;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public final bue()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Ljsr;->eCz:I

    if-gez v0, :cond_0

    .line 53
    invoke-virtual {p0}, Ljsr;->buf()I

    .line 55
    :cond_0
    iget v0, p0, Ljsr;->eCz:I

    return v0
.end method

.method public final buf()I
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Ljsr;->lF()I

    move-result v0

    .line 65
    iput v0, p0, Ljsr;->eCz:I

    .line 66
    return v0
.end method

.method public lF()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    invoke-static {p0}, Ljss;->n(Ljsr;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
