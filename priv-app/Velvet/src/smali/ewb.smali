.class public final Lewb;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field private asz:Lfmq;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 109
    return-void
.end method

.method public static a(Lang;)Lewb;
    .locals 4

    .prologue
    .line 33
    new-instance v0, Lewb;

    invoke-direct {v0}, Lewb;-><init>()V

    .line 34
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 35
    invoke-static {p0}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->i(Ljsr;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    move-result-object v2

    .line 36
    const-string v3, "card"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 37
    invoke-virtual {v0, v1}, Lewb;->setArguments(Landroid/os/Bundle;)V

    .line 38
    return-object v0
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 103
    const/4 v0, 0x2

    const v1, 0x7f090178

    invoke-virtual {p0, v0, v1}, Lewb;->setStyle(II)V

    .line 104
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13

    .prologue
    .line 63
    invoke-virtual {p0}, Lewb;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "card"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;

    .line 64
    const-class v1, Lang;

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;->e(Ljava/lang/Class;)Ljsr;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lang;

    .line 67
    invoke-virtual {p0}, Lewb;->getActivity()Landroid/app/Activity;

    move-result-object v11

    .line 68
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v7

    .line 69
    new-instance v12, Lftz;

    invoke-virtual {v7}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->axW()Lfzw;

    move-result-object v0

    iget-object v1, v7, Lgql;->mAsyncServices:Lema;

    iget-object v2, v7, Lgql;->mClock:Lemp;

    invoke-direct {v12, v0, v1, v2}, Lftz;-><init>(Lfzw;Lerk;Lemp;)V

    .line 73
    new-instance v0, Lflj;

    invoke-virtual {v11}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v7}, Lgql;->aJr()Lfdb;

    move-result-object v3

    invoke-virtual {v3}, Lfdb;->ayg()Lfml;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v7}, Lgql;->aJr()Lfdb;

    move-result-object v5

    iget-object v5, v5, Lfdb;->mUndoDismissManager:Lfnn;

    iget-object v6, v7, Lgql;->mAsyncServices:Lema;

    invoke-virtual {v7}, Lgql;->SC()Lcfo;

    move-result-object v7

    invoke-virtual {v7}, Lcfo;->DC()Lemp;

    move-result-object v7

    sget-object v8, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-static {v8}, Ligj;->bh(Ljava/lang/Object;)Ligi;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lflj;-><init>(Landroid/content/Context;Leoj;Lfml;Lfnm;Lfnn;Lern;Lemp;Ligi;Z)V

    .line 83
    const v1, 0x7f040119

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    .line 85
    invoke-virtual {v10}, Lang;->oN()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    invoke-virtual {v1}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->aDs()V

    .line 88
    :cond_0
    new-instance v3, Lewc;

    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    iget-object v2, v2, Lgql;->mClock:Lemp;

    invoke-direct {v3, v12, v2, v10}, Lewc;-><init>(Lftz;Lemp;Lang;)V

    .line 90
    iget-object v4, v10, Lang;->ags:[Lanh;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 91
    invoke-virtual {v12, v11, v0, v6}, Lftz;->a(Landroid/content/Context;Lfmt;Lanh;)Lfro;

    move-result-object v7

    .line 93
    invoke-virtual {v7, v3, v6}, Lfro;->a(Lfuz;Lanh;)Landroid/view/View;

    move-result-object v6

    .line 94
    invoke-virtual {v1, v6}, Lcom/google/android/sidekick/shared/ui/qp/ModularCard;->addView(Landroid/view/View;)V

    .line 90
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 97
    :cond_1
    return-object v1
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 44
    iget-object v0, p0, Lewb;->asz:Lfmq;

    if-nez v0, :cond_0

    .line 45
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->aJr()Lfdb;

    move-result-object v0

    invoke-virtual {v0}, Lfdb;->ayg()Lfml;

    move-result-object v0

    const-string v1, "test_launcher"

    invoke-virtual {v0, v1}, Lfml;->lQ(Ljava/lang/String;)Lfmq;

    move-result-object v0

    iput-object v0, p0, Lewb;->asz:Lfmq;

    .line 47
    iget-object v0, p0, Lewb;->asz:Lfmq;

    invoke-virtual {v0}, Lfmq;->aBf()Z

    .line 49
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lewb;->asz:Lfmq;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lewb;->asz:Lfmq;

    invoke-virtual {v0}, Lfmq;->release()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lewb;->asz:Lfmq;

    .line 57
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onStop()V

    .line 58
    return-void
.end method
