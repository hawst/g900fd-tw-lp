.class public Lfnx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;
.implements Lbhk;
.implements Lbhl;
.implements Lbhp;
.implements Lbtg;
.implements Lbth;
.implements Lbti;
.implements Lbtj;
.implements Lbtk;
.implements Lejr;


# instance fields
.field private aKb:I

.field private bbH:Ljava/lang/String;

.field private cxi:Z

.field private final cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

.field private final cxk:Lfon;

.field private cxl:Lfoh;

.field private cxm:Z

.field private cxn:Lesk;

.field private cxo:Z

.field private cxp:Lbup;

.field cxq:Z

.field private cxr:Lfof;

.field private cxs:Lfoj;

.field private cxt:Lfoi;

.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private mActivity:Landroid/app/Activity;

.field private mClient:Lbhi;

.field private final mDrawer:Lfom;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfom;Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;Lfon;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput-boolean v1, p0, Lfnx;->cxm:Z

    .line 189
    const/4 v0, 0x0

    iput v0, p0, Lfnx;->aKb:I

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lfnx;->cxn:Lesk;

    .line 194
    iput-boolean v1, p0, Lfnx;->cxq:Z

    .line 205
    iput-object p2, p0, Lfnx;->mDrawer:Lfom;

    .line 206
    iput-object p3, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    .line 207
    iput-object p1, p0, Lfnx;->mActivity:Landroid/app/Activity;

    .line 208
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lfnx;->mAccountManager:Landroid/accounts/AccountManager;

    .line 209
    iput-object p4, p0, Lfnx;->cxk:Lfon;

    .line 211
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, p0}, Lfom;->g(Lfnx;)V

    .line 212
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->a(Lejr;)V

    .line 213
    return-void
.end method

.method static synthetic a(Lfnx;)Lfoh;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lfnx;->cxl:Lfoh;

    return-object v0
.end method

.method static synthetic a(Lfnx;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lfnx;->bbH:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lfnx;Z)Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfnx;->cxo:Z

    return v0
.end method

.method private aBA()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 491
    iget-boolean v0, p0, Lfnx;->cxi:Z

    if-eqz v0, :cond_1

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 495
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfnx;->cxi:Z

    .line 496
    invoke-virtual {p0}, Lfnx;->aBB()V

    .line 498
    iget-object v0, p0, Lfnx;->cxk:Lfon;

    invoke-interface {v0}, Lfon;->onStart()V

    .line 500
    iget-object v0, p0, Lfnx;->cxs:Lfoj;

    if-nez v0, :cond_2

    .line 501
    new-instance v0, Lfoj;

    invoke-direct {v0, p0, v3}, Lfoj;-><init>(Lfnx;B)V

    iput-object v0, p0, Lfnx;->cxs:Lfoj;

    .line 502
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 503
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 504
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 505
    iget-object v1, p0, Lfnx;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lfnx;->cxs:Lfoj;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 508
    :cond_2
    iget-object v0, p0, Lfnx;->cxt:Lfoi;

    if-nez v0, :cond_0

    .line 509
    new-instance v0, Lfoi;

    invoke-direct {v0, p0, v3}, Lfoi;-><init>(Lfnx;B)V

    iput-object v0, p0, Lfnx;->cxt:Lfoi;

    .line 510
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.apps.now.gel_prefs_synced_broadcast"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 512
    iget-object v1, p0, Lfnx;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lfnx;->cxt:Lfoi;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private aBC()V
    .locals 3

    .prologue
    .line 574
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    new-instance v0, Lbsp;

    invoke-direct {v0}, Lbsp;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbsp;->bM(Z)Lbsp;

    move-result-object v0

    .line 576
    sget-object v1, Lbst;->aIM:Lbsn;

    iget-object v2, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v1, v2, v0}, Lbsn;->a(Lbhi;Lbsp;)Lbhm;

    move-result-object v0

    invoke-interface {v0, p0}, Lbhm;->a(Lbhp;)V

    .line 578
    :cond_0
    return-void
.end method

.method private aBD()Landroid/accounts/Account;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 581
    iget-object v1, p0, Lfnx;->bbH:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 594
    :cond_0
    :goto_0
    return-object v0

    .line 585
    :cond_1
    iget-object v1, p0, Lfnx;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 588
    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    .line 589
    iget-object v5, p0, Lfnx;->bbH:Ljava/lang/String;

    iget-object v6, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    .line 590
    goto :goto_0

    .line 588
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method private aBF()V
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lfnx;->cxp:Lbup;

    if-eqz v0, :cond_0

    .line 689
    iget-object v0, p0, Lfnx;->cxp:Lbup;

    invoke-virtual {v0}, Lbup;->release()V

    .line 690
    const/4 v0, 0x0

    iput-object v0, p0, Lfnx;->cxp:Lbup;

    .line 692
    :cond_0
    return-void
.end method

.method static synthetic aQ(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    new-instance v0, Lenz;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lenz;-><init>(Landroid/content/Context;)V

    const-string v2, "GSAPrefs.google_account"

    invoke-virtual {v0, v2, v1}, Lenz;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    array-length v5, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v0, v3, v2

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lfnx;)Lfom;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 812
    const-string v0, "AccountSwitcherDrawerPresenter.Prefs"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 813
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 815
    if-nez p2, :cond_1

    .line 816
    const-string v0, "recent_accounts"

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 841
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 842
    return-void

    .line 817
    :cond_1
    if-eqz p1, :cond_0

    .line 819
    invoke-static {v0}, Lfnx;->e(Landroid/content/SharedPreferences;)Ljava/util/List;

    move-result-object v0

    .line 822
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 823
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 827
    :cond_2
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 828
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 832
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 833
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 836
    :cond_4
    const/4 v2, 0x0

    invoke-interface {v0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 837
    const-string v2, ","

    invoke-static {v2}, Lifj;->pp(Ljava/lang/String;)Lifj;

    move-result-object v2

    invoke-virtual {v2, v0}, Lifj;->n(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 838
    const-string v2, "recent_accounts"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method static synthetic c(Lfnx;)Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    return-object v0
.end method

.method static synthetic d(Lfnx;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lfnx;->cxm:Z

    return v0
.end method

.method private static e(Landroid/content/SharedPreferences;)Ljava/util/List;
    .locals 3

    .prologue
    .line 796
    const-string v0, "recent_accounts"

    const-string v1, ""

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 799
    new-instance v1, Ljava/util/ArrayList;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 800
    return-object v1
.end method

.method static synthetic e(Lfnx;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lfnx;->cxo:Z

    return v0
.end method

.method private f(Lesk;)V
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atI()V

    .line 304
    iput-object p1, p0, Lfnx;->cxn:Lesk;

    .line 305
    return-void
.end method

.method static synthetic f(Lfnx;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lfnx;->aBC()V

    return-void
.end method

.method private lS(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 626
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfnx;->cxo:Z

    .line 628
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.now.switch_account_broadcast"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 629
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 631
    iget-object v1, p0, Lfnx;->mActivity:Landroid/app/Activity;

    const-string v2, "com.google.android.apps.now.CURRENT_ACCOUNT_ACCESS"

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 632
    return-void
.end method

.method private setNavigationMode(I)V
    .locals 2

    .prologue
    .line 734
    iput p1, p0, Lfnx;->aKb:I

    .line 735
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    iget v1, p0, Lfnx;->aKb:I

    invoke-interface {v0, v1}, Lfom;->setNavigationMode(I)V

    .line 736
    return-void
.end method


# virtual methods
.method public final AL()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 644
    iget-object v0, p0, Lfnx;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    iget-object v5, p0, Lfnx;->mActivity:Landroid/app/Activity;

    move-object v3, v2

    move-object v4, v2

    move-object v6, p0

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 646
    return-void
.end method

.method public final AM()V
    .locals 3

    .prologue
    .line 650
    iget-object v0, p0, Lfnx;->mActivity:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.SYNC_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 651
    return-void
.end method

.method public final AN()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 636
    iget v1, p0, Lfnx;->aKb:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-direct {p0, v0}, Lfnx;->setNavigationMode(I)V

    .line 639
    return-void
.end method

.method public final B(F)V
    .locals 0

    .prologue
    .line 862
    return-void
.end method

.method public final Cn()V
    .locals 0

    .prologue
    .line 847
    return-void
.end method

.method public final Co()V
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lfnx;->cxn:Lesk;

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lfnx;->cxn:Lesk;

    invoke-interface {v0}, Lesk;->run()V

    .line 853
    const/4 v0, 0x0

    iput-object v0, p0, Lfnx;->cxn:Lesk;

    .line 856
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lfnx;->setNavigationMode(I)V

    .line 857
    return-void
.end method

.method public final D(F)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 471
    const v0, 0x3f7fffef    # 0.999999f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iget-boolean v0, p0, Lfnx;->cxi:Z

    if-nez v0, :cond_0

    .line 472
    invoke-direct {p0}, Lfnx;->aBA()V

    .line 475
    :cond_0
    const v0, 0x358637bd    # 1.0E-6f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    .line 476
    :goto_0
    iget-boolean v2, p0, Lfnx;->cxq:Z

    if-ne v0, v2, :cond_2

    .line 484
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 475
    goto :goto_0

    .line 480
    :cond_2
    iput-boolean v0, p0, Lfnx;->cxq:Z

    .line 483
    iget-object v2, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v2, v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->setVisibility(I)V

    goto :goto_1

    :cond_3
    const/16 v1, 0x8

    goto :goto_2
.end method

.method public final a(Lbgm;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 344
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfom;->bN(Z)V

    .line 345
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, v2, v2}, Lfom;->a(Ljava/util/List;Lbuo;)V

    .line 346
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, v2, v2}, Lfom;->a(Lbuo;Lbuo;)V

    .line 347
    invoke-virtual {p0}, Lfnx;->aBJ()V

    .line 349
    invoke-virtual {p1}, Lbgm;->yj()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    :try_start_0
    iget-object v0, p0, Lfnx;->mActivity:Landroid/app/Activity;

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Lbgm;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 353
    :catch_0
    move-exception v0

    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->connect()V

    goto :goto_0
.end method

.method public final synthetic a(Lbho;)V
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 61
    check-cast p1, Lbsq;

    invoke-interface {p1}, Lbsq;->zX()Lbup;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0}, Lfnx;->aBD()Landroid/accounts/Account;

    move-result-object v8

    iget-object v0, p0, Lfnx;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "AccountSwitcherDrawerPresenter.Prefs"

    const/4 v3, 0x4

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lfnx;->e(Landroid/content/SharedPreferences;)Ljava/util/List;

    move-result-object v9

    invoke-virtual {v6}, Lbup;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-object v2, v1

    move-object v3, v1

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuo;

    if-eqz v8, :cond_0

    iget-object v11, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0}, Lbuo;->yM()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    move-object v3, v0

    goto :goto_0

    :cond_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_1

    invoke-interface {v0}, Lbuo;->yM()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    move-object v2, v0

    :cond_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x2

    if-lt v11, v12, :cond_2

    invoke-interface {v0}, Lbuo;->yM()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move-object v1, v0

    :cond_2
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    if-eqz v3, :cond_4

    invoke-interface {v7, v5, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_4
    iget-object v8, p0, Lfnx;->mDrawer:Lfom;

    if-nez v3, :cond_5

    move v0, v4

    :goto_1
    invoke-interface {v8, v0}, Lfom;->bN(Z)V

    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, v7, v3}, Lfom;->a(Ljava/util/List;Lbuo;)V

    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, v2, v1}, Lfom;->a(Lbuo;Lbuo;)V

    invoke-direct {p0}, Lfnx;->aBF()V

    iput-object v6, p0, Lfnx;->cxp:Lbup;

    invoke-virtual {p0}, Lfnx;->aBJ()V

    return-void

    :cond_5
    move v0, v5

    goto :goto_1
.end method

.method public final a(Lfoh;)V
    .locals 0

    .prologue
    .line 555
    iput-object p1, p0, Lfnx;->cxl:Lfoh;

    .line 556
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Lfok;)V
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, p1, p2}, Lfom;->a(Ljava/lang/CharSequence;Lfok;)V

    .line 571
    return-void
.end method

.method public final aAS()V
    .locals 2

    .prologue
    .line 430
    iget-boolean v0, p0, Lfnx;->cxq:Z

    if-eqz v0, :cond_0

    .line 438
    :goto_0
    return-void

    .line 434
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfnx;->cxq:Z

    .line 437
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method final aBB()V
    .locals 1

    .prologue
    .line 523
    iget-boolean v0, p0, Lfnx;->cxi:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_0

    .line 525
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->connect()V

    .line 527
    :cond_0
    return-void
.end method

.method public final aBE()Ljava/lang/String;
    .locals 3

    .prologue
    .line 619
    new-instance v0, Lenz;

    iget-object v1, p0, Lfnx;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lenz;-><init>(Landroid/content/Context;)V

    .line 620
    invoke-virtual {v0}, Lenz;->acJ()V

    .line 621
    const-string v1, "GSAPrefs.google_account"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lenz;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final aBG()V
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atH()V

    .line 696
    return-void
.end method

.method public final aBH()V
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atI()V

    .line 700
    return-void
.end method

.method public final aBI()F
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atK()F

    move-result v0

    return v0
.end method

.method public final aBJ()V
    .locals 4

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 711
    iget-object v2, p0, Lfnx;->mClient:Lbhi;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v2}, Lbhi;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 712
    :cond_0
    iget-object v2, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v2, v1}, Lfom;->ft(Z)V

    .line 713
    iget-object v2, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v2, v1}, Lfom;->fu(Z)V

    .line 714
    iget-object v1, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v1, v0}, Lfom;->iY(I)V

    .line 729
    :goto_0
    return-void

    .line 718
    :cond_1
    iget-object v2, p0, Lfnx;->mDrawer:Lfom;

    iget-object v3, p0, Lfnx;->cxk:Lfon;

    invoke-interface {v3}, Lfon;->aBN()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    :cond_2
    invoke-interface {v2, v0}, Lfom;->iY(I)V

    .line 721
    iget-object v0, p0, Lfnx;->cxk:Lfon;

    invoke-interface {v0}, Lfon;->aBL()Z

    move-result v0

    if-nez v0, :cond_3

    .line 722
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, v1}, Lfom;->ft(Z)V

    .line 723
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, v1}, Lfom;->fu(Z)V

    goto :goto_0

    .line 727
    :cond_3
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    iget-object v1, p0, Lfnx;->cxk:Lfon;

    invoke-interface {v1}, Lfon;->aBM()Z

    move-result v1

    invoke-interface {v0, v1}, Lfom;->ft(Z)V

    .line 728
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lfom;->fu(Z)V

    goto :goto_0
.end method

.method public final aBr()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    if-eqz v0, :cond_2

    .line 220
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnecting()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    :cond_0
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->disconnect()V

    .line 223
    :cond_1
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0, p0}, Lbhi;->b(Lbhk;)V

    .line 224
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0, p0}, Lbhi;->b(Lbhl;)V

    .line 226
    :cond_2
    invoke-virtual {p0}, Lfnx;->aBs()Lbhi;

    move-result-object v0

    iput-object v0, p0, Lfnx;->mClient:Lbhi;

    .line 227
    return-void
.end method

.method aBs()Lbhi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 231
    new-instance v2, Lbsw;

    invoke-direct {v2}, Lbsw;-><init>()V

    const/16 v0, 0x7d

    iput v0, v2, Lbsw;->aIO:I

    iget v0, v2, Lbsw;->aIO:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Must provide valid client application ID!"

    invoke-static {v0, v3}, Lbjr;->b(ZLjava/lang/Object;)V

    new-instance v0, Lbsv;

    invoke-direct {v0, v2, v1}, Lbsv;-><init>(Lbsw;B)V

    .line 233
    new-instance v1, Lbhj;

    iget-object v2, p0, Lfnx;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lbhj;-><init>(Landroid/content/Context;)V

    sget-object v2, Lbst;->aIL:Lbgx;

    invoke-virtual {v1, v2, v0}, Lbhj;->a(Lbgx;Lbgz;)Lbhj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbhj;->c(Lbhk;)Lbhj;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbhj;->c(Lbhl;)Lbhj;

    move-result-object v0

    invoke-virtual {v0}, Lbhj;->yx()Lbhi;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 231
    goto :goto_0
.end method

.method public final aBt()V
    .locals 1

    .prologue
    .line 241
    new-instance v0, Lfny;

    invoke-direct {v0, p0}, Lfny;-><init>(Lfnx;)V

    invoke-direct {p0, v0}, Lfnx;->f(Lesk;)V

    .line 249
    return-void
.end method

.method public final aBu()V
    .locals 1

    .prologue
    .line 252
    new-instance v0, Lfnz;

    invoke-direct {v0, p0}, Lfnz;-><init>(Lfnx;)V

    invoke-direct {p0, v0}, Lfnx;->f(Lesk;)V

    .line 260
    return-void
.end method

.method public final aBv()V
    .locals 1

    .prologue
    .line 263
    new-instance v0, Lfoa;

    invoke-direct {v0, p0}, Lfoa;-><init>(Lfnx;)V

    invoke-direct {p0, v0}, Lfnx;->f(Lesk;)V

    .line 271
    return-void
.end method

.method public final aBw()V
    .locals 1

    .prologue
    .line 274
    new-instance v0, Lfob;

    invoke-direct {v0, p0}, Lfob;-><init>(Lfnx;)V

    invoke-direct {p0, v0}, Lfnx;->f(Lesk;)V

    .line 282
    return-void
.end method

.method public final aBx()V
    .locals 1

    .prologue
    .line 285
    new-instance v0, Lfoc;

    invoke-direct {v0, p0}, Lfoc;-><init>(Lfnx;)V

    invoke-direct {p0, v0}, Lfnx;->f(Lesk;)V

    .line 293
    return-void
.end method

.method public final aBy()V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atI()V

    .line 297
    return-void
.end method

.method public final aBz()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 447
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->setVisibility(I)V

    .line 448
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->requestLayout()V

    .line 449
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    new-instance v1, Lfod;

    invoke-direct {v1, p0}, Lfod;-><init>(Lfnx;)V

    invoke-virtual {v0, v1}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->post(Ljava/lang/Runnable;)Z

    .line 458
    :cond_0
    return-void
.end method

.method public final b(Lbuo;)V
    .locals 1

    .prologue
    .line 655
    invoke-interface {p1}, Lbuo;->yM()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lfnx;->lS(Ljava/lang/String;)V

    .line 656
    return-void
.end method

.method public final dw(I)V
    .locals 0

    .prologue
    .line 339
    return-void
.end method

.method public final eh(I)V
    .locals 0

    .prologue
    .line 660
    iput p1, p0, Lfnx;->aKb:I

    .line 661
    return-void
.end method

.method public final j(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 360
    invoke-direct {p0}, Lfnx;->aBC()V

    .line 361
    return-void
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 410
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0}, Lfom;->disconnect()V

    .line 411
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, v2, v2}, Lfom;->a(Ljava/util/List;Lbuo;)V

    .line 412
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, v2, v2}, Lfom;->a(Lbuo;Lbuo;)V

    .line 413
    iget-object v0, p0, Lfnx;->cxr:Lfof;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lfnx;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lfnx;->cxr:Lfof;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 415
    iput-object v2, p0, Lfnx;->cxr:Lfof;

    .line 418
    :cond_0
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0, p0}, Lbhi;->b(Lbhk;)V

    .line 420
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0, p0}, Lbhi;->b(Lbhl;)V

    .line 423
    :cond_1
    invoke-direct {p0}, Lfnx;->aBF()V

    .line 424
    return-void
.end method

.method public final onPostCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 308
    iget-object v0, p0, Lfnx;->cxr:Lfof;

    if-nez v0, :cond_0

    .line 309
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.apps.now.account_update_broadcast"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 311
    new-instance v1, Lfof;

    invoke-direct {v1, p0, v3}, Lfof;-><init>(Lfnx;B)V

    iput-object v1, p0, Lfnx;->cxr:Lfof;

    .line 312
    iget-object v1, p0, Lfnx;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lfnx;->cxr:Lfof;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 315
    :cond_0
    invoke-virtual {p0}, Lfnx;->aBE()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfnx;->bbH:Ljava/lang/String;

    .line 316
    if-eqz p1, :cond_1

    .line 317
    const-string v0, "shared:old_account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 318
    iget-object v1, p0, Lfnx;->bbH:Ljava/lang/String;

    invoke-static {v0, v1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfnx;->cxl:Lfoh;

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lfnx;->bbH:Ljava/lang/String;

    invoke-virtual {p0, v0, v3}, Lfnx;->v(Ljava/lang/String;Z)V

    .line 324
    :cond_1
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, p0}, Lfom;->a(Lbti;)V

    .line 325
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, p0}, Lfom;->a(Lbtk;)V

    .line 326
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, p0}, Lfom;->a(Lbth;)V

    .line 327
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, p0}, Lfom;->a(Lbtg;)V

    .line 328
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, p0}, Lfom;->a(Lbtj;)V

    .line 330
    invoke-virtual {p0}, Lfnx;->aBJ()V

    .line 332
    invoke-virtual {p0}, Lfnx;->aBr()V

    .line 333
    iget-object v0, p0, Lfnx;->mDrawer:Lfom;

    iget-object v1, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0, v1}, Lfom;->f(Lbhi;)V

    .line 334
    return-void
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 550
    invoke-virtual {p0}, Lfnx;->aBJ()V

    .line 551
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 518
    const-string v0, "shared:old_account_name"

    iget-object v1, p0, Lfnx;->bbH:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    return-void
.end method

.method public final onStart()V
    .locals 0

    .prologue
    .line 487
    invoke-direct {p0}, Lfnx;->aBA()V

    .line 488
    return-void
.end method

.method public final onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 530
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfnx;->cxi:Z

    .line 532
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->isConnecting()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    :cond_0
    iget-object v0, p0, Lfnx;->mClient:Lbhi;

    invoke-interface {v0}, Lbhi;->disconnect()V

    .line 536
    :cond_1
    iget-object v0, p0, Lfnx;->cxk:Lfon;

    invoke-interface {v0}, Lfon;->onStop()V

    .line 538
    iget-object v0, p0, Lfnx;->cxs:Lfoj;

    if-eqz v0, :cond_2

    .line 539
    iget-object v0, p0, Lfnx;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lfnx;->cxs:Lfoj;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 540
    iput-object v2, p0, Lfnx;->cxs:Lfoj;

    .line 543
    :cond_2
    iget-object v0, p0, Lfnx;->cxt:Lfoi;

    if-eqz v0, :cond_3

    .line 544
    iget-object v0, p0, Lfnx;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lfnx;->cxt:Lfoi;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 545
    iput-object v2, p0, Lfnx;->cxt:Lfoi;

    .line 547
    :cond_3
    return-void
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 669
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 670
    if-eqz v0, :cond_1

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 672
    :goto_0
    const-string v1, "AccountSwitcherDrawerPr"

    const-string v2, "Add account activity returned account name: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 674
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 675
    invoke-direct {p0, v0}, Lfnx;->lS(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 685
    :cond_0
    :goto_1
    return-void

    .line 670
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 677
    :catch_0
    move-exception v0

    .line 679
    const-string v1, "AccountSwitcherDrawerPr"

    const-string v2, "Adding new account cancelled"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v1, v2, v3}, Leor;->a(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_1

    .line 680
    :catch_1
    move-exception v0

    .line 681
    const-string v1, "AccountSwitcherDrawerPr"

    const-string v2, "Account not found"

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 682
    :catch_2
    move-exception v0

    .line 683
    const-string v1, "AccountSwitcherDrawerPr"

    const-string v2, "Failed to add new account"

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method final v(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lfnx;->bbH:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 759
    :goto_0
    return-void

    .line 744
    :cond_0
    iput-object p1, p0, Lfnx;->bbH:Ljava/lang/String;

    .line 747
    invoke-direct {p0}, Lfnx;->aBC()V

    .line 749
    iget-object v0, p0, Lfnx;->cxj:Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/ui/AccountNavigationDrawerLayout;->atJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 750
    new-instance v0, Lfoe;

    invoke-direct {v0, p0, p1, p2}, Lfoe;-><init>(Lfnx;Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lfnx;->f(Lesk;)V

    goto :goto_0

    .line 757
    :cond_1
    invoke-virtual {p0, p1, p2}, Lfnx;->w(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method final w(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 764
    invoke-virtual {p0}, Lfnx;->aBJ()V

    .line 766
    if-eqz p2, :cond_0

    .line 767
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lfnx;->mActivity:Landroid/app/Activity;

    const-string v3, "com.google.android.velvet.tg.FirstRunActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "skip_to_end"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "skip_launch_velvet"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "source"

    const-string v2, "SELECT_ACCOUNT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 768
    iget-object v1, p0, Lfnx;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 771
    :cond_0
    iget-object v0, p0, Lfnx;->cxl:Lfoh;

    if-eqz v0, :cond_1

    .line 772
    iget-object v0, p0, Lfnx;->cxl:Lfoh;

    iget-object v1, p0, Lfnx;->mDrawer:Lfom;

    invoke-interface {v0, p1}, Lfoh;->eF(Ljava/lang/String;)V

    .line 774
    :cond_1
    return-void
.end method
