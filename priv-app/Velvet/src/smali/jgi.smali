.class public final Ljgi;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private afh:Ljava/lang/String;

.field private aiS:Ljcn;

.field private dXy:[Ljhg;

.field private dXz:Ljava/lang/String;

.field private ejJ:Ljava/lang/String;

.field private ejK:[Ljhg;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49801
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 49802
    const/4 v0, 0x0

    iput v0, p0, Ljgi;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljgi;->afh:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljgi;->dXz:Ljava/lang/String;

    iput-object v1, p0, Ljgi;->aiS:Ljcn;

    const-string v0, ""

    iput-object v0, p0, Ljgi;->ejJ:Ljava/lang/String;

    invoke-static {}, Ljhg;->blD()[Ljhg;

    move-result-object v0

    iput-object v0, p0, Ljgi;->ejK:[Ljhg;

    invoke-static {}, Ljhg;->blD()[Ljhg;

    move-result-object v0

    iput-object v0, p0, Ljgi;->dXy:[Ljhg;

    iput-object v1, p0, Ljgi;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljgi;->eCz:I

    .line 49803
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49707
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljgi;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgi;->afh:Ljava/lang/String;

    iget v0, p0, Ljgi;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgi;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgi;->dXz:Ljava/lang/String;

    iget v0, p0, Ljgi;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljgi;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljgi;->aiS:Ljcn;

    if-nez v0, :cond_1

    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Ljgi;->aiS:Ljcn;

    :cond_1
    iget-object v0, p0, Ljgi;->aiS:Ljcn;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljgi;->ejJ:Ljava/lang/String;

    iget v0, p0, Ljgi;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljgi;->aez:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljgi;->ejK:[Ljhg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhg;

    if-eqz v0, :cond_2

    iget-object v3, p0, Ljgi;->ejK:[Ljhg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Ljhg;

    invoke-direct {v3}, Ljhg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljgi;->ejK:[Ljhg;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Ljhg;

    invoke-direct {v3}, Ljhg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljgi;->ejK:[Ljhg;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljgi;->dXy:[Ljhg;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljhg;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljgi;->dXy:[Ljhg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Ljhg;

    invoke-direct {v3}, Ljhg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljgi;->dXy:[Ljhg;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Ljhg;

    invoke-direct {v3}, Ljhg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljgi;->dXy:[Ljhg;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 49821
    iget v0, p0, Ljgi;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 49822
    const/4 v0, 0x1

    iget-object v2, p0, Ljgi;->afh:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 49824
    :cond_0
    iget v0, p0, Ljgi;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 49825
    const/4 v0, 0x2

    iget-object v2, p0, Ljgi;->dXz:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 49827
    :cond_1
    iget-object v0, p0, Ljgi;->aiS:Ljcn;

    if-eqz v0, :cond_2

    .line 49828
    const/4 v0, 0x3

    iget-object v2, p0, Ljgi;->aiS:Ljcn;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    .line 49830
    :cond_2
    iget v0, p0, Ljgi;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    .line 49831
    const/4 v0, 0x4

    iget-object v2, p0, Ljgi;->ejJ:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 49833
    :cond_3
    iget-object v0, p0, Ljgi;->ejK:[Ljhg;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljgi;->ejK:[Ljhg;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 49834
    :goto_0
    iget-object v2, p0, Ljgi;->ejK:[Ljhg;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 49835
    iget-object v2, p0, Ljgi;->ejK:[Ljhg;

    aget-object v2, v2, v0

    .line 49836
    if-eqz v2, :cond_4

    .line 49837
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    .line 49834
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49841
    :cond_5
    iget-object v0, p0, Ljgi;->dXy:[Ljhg;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljgi;->dXy:[Ljhg;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 49842
    :goto_1
    iget-object v0, p0, Ljgi;->dXy:[Ljhg;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 49843
    iget-object v0, p0, Ljgi;->dXy:[Ljhg;

    aget-object v0, v0, v1

    .line 49844
    if-eqz v0, :cond_6

    .line 49845
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    .line 49842
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 49849
    :cond_7
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 49850
    return-void
.end method

.method protected final lF()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 49854
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 49855
    iget v2, p0, Ljgi;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 49856
    const/4 v2, 0x1

    iget-object v3, p0, Ljgi;->afh:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 49859
    :cond_0
    iget v2, p0, Ljgi;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 49860
    const/4 v2, 0x2

    iget-object v3, p0, Ljgi;->dXz:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 49863
    :cond_1
    iget-object v2, p0, Ljgi;->aiS:Ljcn;

    if-eqz v2, :cond_2

    .line 49864
    const/4 v2, 0x3

    iget-object v3, p0, Ljgi;->aiS:Ljcn;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 49867
    :cond_2
    iget v2, p0, Ljgi;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_3

    .line 49868
    const/4 v2, 0x4

    iget-object v3, p0, Ljgi;->ejJ:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 49871
    :cond_3
    iget-object v2, p0, Ljgi;->ejK:[Ljhg;

    if-eqz v2, :cond_6

    iget-object v2, p0, Ljgi;->ejK:[Ljhg;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 49872
    :goto_0
    iget-object v3, p0, Ljgi;->ejK:[Ljhg;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 49873
    iget-object v3, p0, Ljgi;->ejK:[Ljhg;

    aget-object v3, v3, v0

    .line 49874
    if-eqz v3, :cond_4

    .line 49875
    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    .line 49872
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v2

    .line 49880
    :cond_6
    iget-object v2, p0, Ljgi;->dXy:[Ljhg;

    if-eqz v2, :cond_8

    iget-object v2, p0, Ljgi;->dXy:[Ljhg;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 49881
    :goto_1
    iget-object v2, p0, Ljgi;->dXy:[Ljhg;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 49882
    iget-object v2, p0, Ljgi;->dXy:[Ljhg;

    aget-object v2, v2, v1

    .line 49883
    if-eqz v2, :cond_7

    .line 49884
    const/4 v3, 0x6

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    .line 49881
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 49889
    :cond_8
    return v0
.end method
