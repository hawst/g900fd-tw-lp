.class public final Lapu;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile alQ:[Lapu;


# instance fields
.field private aez:I

.field private aib:Ljava/lang/String;

.field private akf:Ljava/lang/String;

.field private alR:Ljava/lang/String;

.field private alS:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2492
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2493
    const/4 v0, 0x0

    iput v0, p0, Lapu;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lapu;->akf:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lapu;->aib:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lapu;->alR:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lapu;->alS:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lapu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lapu;->eCz:I

    .line 2494
    return-void
.end method

.method public static ts()[Lapu;
    .locals 2

    .prologue
    .line 2391
    sget-object v0, Lapu;->alQ:[Lapu;

    if-nez v0, :cond_1

    .line 2392
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 2394
    :try_start_0
    sget-object v0, Lapu;->alQ:[Lapu;

    if-nez v0, :cond_0

    .line 2395
    const/4 v0, 0x0

    new-array v0, v0, [Lapu;

    sput-object v0, Lapu;->alQ:[Lapu;

    .line 2397
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2399
    :cond_1
    sget-object v0, Lapu;->alQ:[Lapu;

    return-object v0

    .line 2397
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2385
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lapu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapu;->akf:Ljava/lang/String;

    iget v0, p0, Lapu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapu;->aib:Ljava/lang/String;

    iget v0, p0, Lapu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapu;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapu;->alR:Ljava/lang/String;

    iget v0, p0, Lapu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lapu;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapu;->alS:Ljava/lang/String;

    iget v0, p0, Lapu;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lapu;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2510
    iget v0, p0, Lapu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 2511
    const/4 v0, 0x1

    iget-object v1, p0, Lapu;->akf:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2513
    :cond_0
    iget v0, p0, Lapu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 2514
    const/4 v0, 0x2

    iget-object v1, p0, Lapu;->aib:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2516
    :cond_1
    iget v0, p0, Lapu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 2517
    const/4 v0, 0x3

    iget-object v1, p0, Lapu;->alR:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2519
    :cond_2
    iget v0, p0, Lapu;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 2520
    const/4 v0, 0x4

    iget-object v1, p0, Lapu;->alS:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 2522
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2523
    return-void
.end method

.method public final dK(Ljava/lang/String;)Lapu;
    .locals 1

    .prologue
    .line 2410
    if-nez p1, :cond_0

    .line 2411
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2413
    :cond_0
    iput-object p1, p0, Lapu;->akf:Ljava/lang/String;

    .line 2414
    iget v0, p0, Lapu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lapu;->aez:I

    .line 2415
    return-object p0
.end method

.method public final dL(Ljava/lang/String;)Lapu;
    .locals 1

    .prologue
    .line 2432
    if-nez p1, :cond_0

    .line 2433
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2435
    :cond_0
    iput-object p1, p0, Lapu;->aib:Ljava/lang/String;

    .line 2436
    iget v0, p0, Lapu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lapu;->aez:I

    .line 2437
    return-object p0
.end method

.method public final dM(Ljava/lang/String;)Lapu;
    .locals 1

    .prologue
    .line 2454
    if-nez p1, :cond_0

    .line 2455
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2457
    :cond_0
    iput-object p1, p0, Lapu;->alR:Ljava/lang/String;

    .line 2458
    iget v0, p0, Lapu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lapu;->aez:I

    .line 2459
    return-object p0
.end method

.method public final dN(Ljava/lang/String;)Lapu;
    .locals 1

    .prologue
    .line 2476
    if-nez p1, :cond_0

    .line 2477
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2479
    :cond_0
    iput-object p1, p0, Lapu;->alS:Ljava/lang/String;

    .line 2480
    iget v0, p0, Lapu;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lapu;->aez:I

    .line 2481
    return-object p0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2407
    iget-object v0, p0, Lapu;->akf:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2527
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2528
    iget v1, p0, Lapu;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 2529
    const/4 v1, 0x1

    iget-object v2, p0, Lapu;->akf:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2532
    :cond_0
    iget v1, p0, Lapu;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 2533
    const/4 v1, 0x2

    iget-object v2, p0, Lapu;->aib:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2536
    :cond_1
    iget v1, p0, Lapu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 2537
    const/4 v1, 0x3

    iget-object v2, p0, Lapu;->alR:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2540
    :cond_2
    iget v1, p0, Lapu;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 2541
    const/4 v1, 0x4

    iget-object v2, p0, Lapu;->alS:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2544
    :cond_3
    return v0
.end method

.method public final ps()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2429
    iget-object v0, p0, Lapu;->aib:Ljava/lang/String;

    return-object v0
.end method

.method public final tt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2451
    iget-object v0, p0, Lapu;->alR:Ljava/lang/String;

    return-object v0
.end method

.method public final tu()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2473
    iget-object v0, p0, Lapu;->alS:Ljava/lang/String;

    return-object v0
.end method
