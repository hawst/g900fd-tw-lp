.class public final Lcyf;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile bmH:[Lcyf;


# instance fields
.field private aez:I

.field private bmI:Ljava/lang/String;

.field private bmJ:Z

.field private bmK:F

.field private bmL:I

.field private bmM:J

.field private bmN:Ljava/lang/String;

.field public bmO:[Ljava/lang/String;

.field private bmP:[B

.field public bmQ:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 178
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 179
    iput v1, p0, Lcyf;->aez:I

    const-string v0, ""

    iput-object v0, p0, Lcyf;->bmI:Ljava/lang/String;

    iput-boolean v1, p0, Lcyf;->bmJ:Z

    const/4 v0, 0x0

    iput v0, p0, Lcyf;->bmK:F

    iput v1, p0, Lcyf;->bmL:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcyf;->bmM:J

    const-string v0, ""

    iput-object v0, p0, Lcyf;->bmN:Ljava/lang/String;

    sget-object v0, Ljsu;->eCE:[Ljava/lang/String;

    iput-object v0, p0, Lcyf;->bmO:[Ljava/lang/String;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Lcyf;->bmP:[B

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Lcyf;->bmQ:[I

    const/4 v0, 0x0

    iput-object v0, p0, Lcyf;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lcyf;->eCz:I

    .line 180
    return-void
.end method

.method public static TN()[Lcyf;
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lcyf;->bmH:[Lcyf;

    if-nez v0, :cond_1

    .line 18
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 20
    :try_start_0
    sget-object v0, Lcyf;->bmH:[Lcyf;

    if-nez v0, :cond_0

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Lcyf;

    sput-object v0, Lcyf;->bmH:[Lcyf;

    .line 23
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_1
    sget-object v0, Lcyf;->bmH:[Lcyf;

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final A([B)Lcyf;
    .locals 1

    .prologue
    .line 159
    if-nez p1, :cond_0

    .line 160
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 162
    :cond_0
    iput-object p1, p0, Lcyf;->bmP:[B

    .line 163
    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcyf;->aez:I

    .line 164
    return-object p0
.end method

.method public final C(F)Lcyf;
    .locals 1

    .prologue
    .line 77
    iput p1, p0, Lcyf;->bmK:F

    .line 78
    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcyf;->aez:I

    .line 79
    return-object p0
.end method

.method public final TO()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcyf;->bmJ:Z

    return v0
.end method

.method public final TP()Z
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TQ()F
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcyf;->bmK:F

    return v0
.end method

.method public final TR()Z
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TS()Z
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TT()J
    .locals 2

    .prologue
    .line 112
    iget-wide v0, p0, Lcyf;->bmM:J

    return-wide v0
.end method

.method public final TU()Z
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcyf;->bmN:Ljava/lang/String;

    return-object v0
.end method

.method public final TW()Z
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final TX()[B
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcyf;->bmP:[B

    return-object v0
.end method

.method public final TY()Z
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcyf;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcyf;->bmI:Ljava/lang/String;

    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcyf;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btO()Z

    move-result v0

    iput-boolean v0, p0, Lcyf;->bmJ:Z

    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcyf;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcyf;->bmK:F

    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcyf;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lcyf;->bmL:I

    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcyf;->aez:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Lcyf;->bmM:J

    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcyf;->aez:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcyf;->bmN:Ljava/lang/String;

    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcyf;->aez:I

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lcyf;->bmO:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcyf;->bmO:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcyf;->bmO:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcyf;->bmO:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcyf;->bmP:[B

    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcyf;->aez:I

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x48

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Lcyf;->bmQ:[I

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcyf;->bmQ:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcyf;->bmQ:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcyf;->bmQ:[I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Lcyf;->bmQ:[I

    if-nez v2, :cond_9

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_8

    iget-object v4, p0, Lcyf;->bmQ:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_a

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_9
    iget-object v2, p0, Lcyf;->bmQ:[I

    array-length v2, v2

    goto :goto_6

    :cond_a
    iput-object v0, p0, Lcyf;->bmQ:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x4a -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 201
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 202
    const/4 v0, 0x1

    iget-object v2, p0, Lcyf;->bmI:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 204
    :cond_0
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 205
    const/4 v0, 0x2

    iget-boolean v2, p0, Lcyf;->bmJ:Z

    invoke-virtual {p1, v0, v2}, Ljsj;->N(IZ)V

    .line 207
    :cond_1
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 208
    const/4 v0, 0x3

    iget v2, p0, Lcyf;->bmK:F

    invoke-virtual {p1, v0, v2}, Ljsj;->a(IF)V

    .line 210
    :cond_2
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 211
    const/4 v0, 0x4

    iget v2, p0, Lcyf;->bmL:I

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 213
    :cond_3
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_4

    .line 214
    const/4 v0, 0x5

    iget-wide v2, p0, Lcyf;->bmM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 216
    :cond_4
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    .line 217
    const/4 v0, 0x6

    iget-object v2, p0, Lcyf;->bmN:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 219
    :cond_5
    iget-object v0, p0, Lcyf;->bmO:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcyf;->bmO:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 220
    :goto_0
    iget-object v2, p0, Lcyf;->bmO:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 221
    iget-object v2, p0, Lcyf;->bmO:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 222
    if-eqz v2, :cond_6

    .line 223
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Ljsj;->p(ILjava/lang/String;)V

    .line 220
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_7
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_8

    .line 228
    const/16 v0, 0x8

    iget-object v2, p0, Lcyf;->bmP:[B

    invoke-virtual {p1, v0, v2}, Ljsj;->c(I[B)V

    .line 230
    :cond_8
    iget-object v0, p0, Lcyf;->bmQ:[I

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcyf;->bmQ:[I

    array-length v0, v0

    if-lez v0, :cond_9

    .line 231
    :goto_1
    iget-object v0, p0, Lcyf;->bmQ:[I

    array-length v0, v0

    if-ge v1, v0, :cond_9

    .line 232
    const/16 v0, 0x9

    iget-object v2, p0, Lcyf;->bmQ:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljsj;->bq(II)V

    .line 231
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 235
    :cond_9
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 236
    return-void
.end method

.method public final ai(J)Lcyf;
    .locals 1

    .prologue
    .line 115
    iput-wide p1, p0, Lcyf;->bmM:J

    .line 116
    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcyf;->aez:I

    .line 117
    return-object p0
.end method

.method public final cP(Z)Lcyf;
    .locals 1

    .prologue
    .line 58
    iput-boolean p1, p0, Lcyf;->bmJ:Z

    .line 59
    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcyf;->aez:I

    .line 60
    return-object p0
.end method

.method public final ft(I)Lcyf;
    .locals 1

    .prologue
    .line 96
    iput p1, p0, Lcyf;->bmL:I

    .line 97
    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcyf;->aez:I

    .line 98
    return-object p0
.end method

.method public final getIntValue()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcyf;->bmL:I

    return v0
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcyf;->bmI:Ljava/lang/String;

    return-object v0
.end method

.method public final hasKey()Z
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcyf;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iZ(Ljava/lang/String;)Lcyf;
    .locals 1

    .prologue
    .line 36
    if-nez p1, :cond_0

    .line 37
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Lcyf;->bmI:Ljava/lang/String;

    .line 40
    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcyf;->aez:I

    .line 41
    return-object p0
.end method

.method public final ja(Ljava/lang/String;)Lcyf;
    .locals 1

    .prologue
    .line 134
    if-nez p1, :cond_0

    .line 135
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 137
    :cond_0
    iput-object p1, p0, Lcyf;->bmN:Ljava/lang/String;

    .line 138
    iget v0, p0, Lcyf;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcyf;->aez:I

    .line 139
    return-object p0
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 240
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 241
    iget v1, p0, Lcyf;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 242
    const/4 v1, 0x1

    iget-object v3, p0, Lcyf;->bmI:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_0
    iget v1, p0, Lcyf;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 246
    const/4 v1, 0x2

    iget-boolean v3, p0, Lcyf;->bmJ:Z

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 249
    :cond_1
    iget v1, p0, Lcyf;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 250
    const/4 v1, 0x3

    iget v3, p0, Lcyf;->bmK:F

    invoke-static {v1}, Ljsj;->sc(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 253
    :cond_2
    iget v1, p0, Lcyf;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 254
    const/4 v1, 0x4

    iget v3, p0, Lcyf;->bmL:I

    invoke-static {v1, v3}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_3
    iget v1, p0, Lcyf;->aez:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 258
    const/4 v1, 0x5

    iget-wide v4, p0, Lcyf;->bmM:J

    invoke-static {v1, v4, v5}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_4
    iget v1, p0, Lcyf;->aez:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 262
    const/4 v1, 0x6

    iget-object v3, p0, Lcyf;->bmN:Ljava/lang/String;

    invoke-static {v1, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_5
    iget-object v1, p0, Lcyf;->bmO:[Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcyf;->bmO:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    move v4, v2

    .line 268
    :goto_0
    iget-object v5, p0, Lcyf;->bmO:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_7

    .line 269
    iget-object v5, p0, Lcyf;->bmO:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 270
    if-eqz v5, :cond_6

    .line 271
    add-int/lit8 v4, v4, 0x1

    .line 272
    invoke-static {v5}, Ljsj;->yK(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 268
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 276
    :cond_7
    add-int/2addr v0, v3

    .line 277
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 279
    :cond_8
    iget v1, p0, Lcyf;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_9

    .line 280
    const/16 v1, 0x8

    iget-object v3, p0, Lcyf;->bmP:[B

    invoke-static {v1, v3}, Ljsj;->d(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    :cond_9
    iget-object v1, p0, Lcyf;->bmQ:[I

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcyf;->bmQ:[I

    array-length v1, v1

    if-lez v1, :cond_b

    move v1, v2

    .line 285
    :goto_1
    iget-object v3, p0, Lcyf;->bmQ:[I

    array-length v3, v3

    if-ge v2, v3, :cond_a

    .line 286
    iget-object v3, p0, Lcyf;->bmQ:[I

    aget v3, v3, v2

    .line 287
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 285
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 290
    :cond_a
    add-int/2addr v0, v1

    .line 291
    iget-object v1, p0, Lcyf;->bmQ:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 293
    :cond_b
    return v0
.end method
