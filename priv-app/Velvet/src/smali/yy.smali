.class public final Lyy;
.super Lwq;
.source "PG"


# instance fields
.field public LT:I

.field public Mp:Landroid/content/ComponentName;

.field public Mq:I

.field public Mr:I

.field public Ms:Z

.field public Mt:Landroid/appwidget/AppWidgetHostView;

.field public minHeight:I

.field public minWidth:I


# direct methods
.method public constructor <init>(ILandroid/content/ComponentName;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 88
    invoke-direct {p0}, Lwq;-><init>()V

    .line 62
    iput v1, p0, Lyy;->LT:I

    .line 67
    iput v1, p0, Lyy;->minWidth:I

    .line 68
    iput v1, p0, Lyy;->minHeight:I

    .line 78
    iput v1, p0, Lyy;->Mr:I

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    .line 89
    const/4 v0, 0x4

    iput v0, p0, Lyy;->Jz:I

    .line 90
    iput p1, p0, Lyy;->LT:I

    .line 91
    iput-object p2, p0, Lyy;->Mp:Landroid/content/ComponentName;

    .line 95
    iput v1, p0, Lyy;->AY:I

    .line 96
    iput v1, p0, Lyy;->AZ:I

    .line 98
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v0

    iput-object v0, p0, Lyy;->Jl:Lahz;

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lyy;->Mq:I

    .line 100
    return-void
.end method


# virtual methods
.method final a(Landroid/content/Context;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0, p1, p2}, Lwq;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    .line 105
    const-string v0, "appWidgetId"

    iget v1, p0, Lyy;->LT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 106
    const-string v0, "appWidgetProvider"

    iget-object v1, p0, Lyy;->Mp:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v0, "restored"

    iget v1, p0, Lyy;->Mq:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 108
    return-void
.end method

.method final bX()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Lwq;->bX()V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    .line 137
    return-void
.end method

.method public final bn(I)Z
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lyy;->Mq:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t(Lcom/android/launcher3/Launcher;)V
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lyy;->Mt:Landroid/appwidget/AppWidgetHostView;

    iget v1, p0, Lyy;->AY:I

    iget v2, p0, Lyy;->AZ:I

    invoke-static {v0, p1, v1, v2}, Lrs;->a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher3/Launcher;II)V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lyy;->Ms:Z

    .line 126
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppWidget(id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lyy;->LT:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
