.class public final Lhwq;
.super Lecp;
.source "PG"


# instance fields
.field private final dtA:Lhwp;

.field private dtB:Z

.field private final mUi:Lhkq;


# direct methods
.method public constructor <init>(Lhkq;Lhwp;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lecp;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhwq;->dtB:Z

    .line 41
    iput-object p1, p0, Lhwq;->mUi:Lhkq;

    .line 42
    iput-object p2, p0, Lhwq;->dtA:Lhwp;

    .line 43
    return-void
.end method

.method private or(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 187
    iget-object v1, p0, Lhwq;->dtA:Lhwp;

    new-array v0, v3, [Ljava/lang/String;

    aput-object p1, v0, v4

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v0, v1, Lhwp;->dtz:Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;

    invoke-static {v0}, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->b(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;)Lhwj;

    move-result-object v0

    const v1, 0x7f0a082c

    invoke-virtual {v0, v1, v3, v4, v3}, Lhwj;->b(IIZZ)V

    .line 188
    :goto_0
    return-void

    .line 187
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.speech.extra.RESULTS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v3, "query"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v1, Lhwp;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->yy()Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, v1, Lhwp;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v5, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :goto_1
    iget-object v0, v1, Lhwp;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v0, v1, Lhwp;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->aTe()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v1, Lhwp;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->aTe()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_3
    :try_start_0
    iget-object v0, v1, Lhwp;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->yy()Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v3, v1, Lhwp;->mActivity:Landroid/app/Activity;

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4, v2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "VoiceCommandActivity"

    const-string v3, "Not possible to start pending intent"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;)V
    .locals 5

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->ahN()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v0

    .line 50
    instance-of v1, v0, Lcom/google/android/search/shared/actions/errors/SearchError;

    if-eqz v1, :cond_0

    .line 51
    check-cast v0, Lcom/google/android/search/shared/actions/errors/SearchError;

    .line 53
    iget-object v1, p0, Lhwq;->dtA:Lhwp;

    invoke-virtual {v1, v0}, Lhwp;->f(Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 54
    iget-object v1, p0, Lhwq;->mUi:Lhkq;

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiT()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->ajc()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->aiZ()Z

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/errors/SearchError;->ajd()Z

    move-result v0

    invoke-interface {v1, v2, v3, v4, v0}, Lhkq;->b(IIZZ)V

    .line 62
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lhwq;->dtA:Lhwp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhwp;->f(Lcom/google/android/search/shared/actions/errors/SearchError;)V

    .line 59
    iget-object v0, p0, Lhwq;->mUi:Lhkq;

    const v1, 0x7f0a082e

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lhkq;->b(IIZZ)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Query;Lcom/google/android/search/shared/actions/ParcelableVoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 104
    iget-boolean v0, p0, Lhwq;->dtB:Z

    if-eqz v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 108
    :cond_0
    iput-boolean v1, p0, Lhwq;->dtB:Z

    .line 110
    invoke-virtual {p2}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;->ahN()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v3

    .line 113
    if-nez v3, :cond_1

    move v0, v1

    .line 124
    :goto_1
    if-eqz v0, :cond_5

    .line 127
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lhwq;->or(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p0, Lhwq;->dtA:Lhwp;

    iget-object v0, v0, Lhwp;->mIntentApiParams:Lhwg;

    invoke-virtual {v0}, Lhwg;->getCallingPackage()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->ahT()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/shared/util/App;

    invoke-virtual {v0}, Lcom/google/android/shared/util/App;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Lcom/google/android/search/shared/actions/VoiceAction;->agu()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/shared/util/MatchingAppInfo;->l(Lcom/google/android/shared/util/App;)Z

    move v0, v1

    :goto_2
    if-nez v0, :cond_4

    move v0, v1

    .line 118
    goto :goto_1

    :cond_3
    move v0, v2

    .line 116
    goto :goto_2

    .line 119
    :cond_4
    invoke-interface {v3}, Lcom/google/android/search/shared/actions/VoiceAction;->canExecute()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 121
    goto :goto_1

    .line 130
    :cond_5
    iget-object v0, p0, Lhwq;->dtA:Lhwp;

    iget-object v0, v0, Lhwp;->dtz:Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;

    invoke-static {v0}, Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;->c(Lcom/google/android/voicesearch/intentapi/VoiceCommandActivity;)Lecq;

    move-result-object v0

    invoke-virtual {v0, v3, v1}, Lecq;->b(Lcom/google/android/search/shared/actions/VoiceAction;I)V

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public final a([Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lhwq;->dtA:Lhwp;

    iget-object v0, v0, Lhwp;->mIntentStarter:Leoj;

    invoke-interface {v0, p1}, Leoj;->b([Landroid/content/Intent;)Z

    .line 173
    return-void
.end method

.method public final en(I)V
    .locals 3

    .prologue
    .line 69
    packed-switch p1, :pswitch_data_0

    .line 91
    const-string v0, "VoiceCommandSSCallback"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unrecognized state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Leor;->e(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 94
    :goto_0
    :pswitch_0
    return-void

    .line 71
    :pswitch_1
    iget-object v0, p0, Lhwq;->mUi:Lhkq;

    invoke-interface {v0}, Lhkq;->aQv()V

    goto :goto_0

    .line 74
    :pswitch_2
    iget-object v0, p0, Lhwq;->mUi:Lhkq;

    invoke-interface {v0}, Lhkq;->aQp()V

    goto :goto_0

    .line 77
    :pswitch_3
    iget-object v0, p0, Lhwq;->mUi:Lhkq;

    invoke-interface {v0}, Lhkq;->aQx()V

    goto :goto_0

    .line 80
    :pswitch_4
    iget-object v0, p0, Lhwq;->mUi:Lhkq;

    invoke-interface {v0}, Lhkq;->aQw()V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final eo(I)V
    .locals 0

    .prologue
    .line 180
    return-void
.end method

.method public final w(Lcom/google/android/shared/search/Query;)V
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lhwq;->dtB:Z

    if-eqz v0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhwq;->dtB:Z

    .line 166
    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apQ()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lhwq;->or(Ljava/lang/String;)V

    goto :goto_0
.end method
