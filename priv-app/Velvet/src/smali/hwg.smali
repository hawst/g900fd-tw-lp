.class public Lhwg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final aAJ:Landroid/app/PendingIntent;

.field private final byK:I

.field private final cGd:Ljava/lang/String;

.field private final cRZ:Ljava/lang/String;

.field private final dsZ:Z

.field private final dta:Landroid/os/Bundle;

.field private final dtb:Ljava/lang/String;

.field private final dtc:Z

.field private final dtd:[Ljava/lang/String;

.field private final dte:Ljava/lang/Boolean;

.field private final dtf:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const-string v0, "EXPERIMENTAL_AUTO_SCRIPT"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lhwg;->dsZ:Z

    .line 82
    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lhwg;->aAJ:Landroid/app/PendingIntent;

    .line 84
    const-string v0, "android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lhwg;->dta:Landroid/os/Bundle;

    .line 87
    const-string v0, "android.speech.extra.GET_AUDIO_FORMAT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lgff;->cIt:Lgff;

    invoke-virtual {v2}, Lgff;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhwg;->dtc:Z

    .line 88
    const-string v0, "android.speech.extra.PROMPT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwg;->cGd:Ljava/lang/String;

    .line 90
    const-string v0, "android.speech.extra.MAX_RESULTS"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lhwg;->byK:I

    .line 91
    const-string v0, "android.speech.extra.LANGUAGE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwg;->cRZ:Ljava/lang/String;

    .line 92
    const-string v0, "android.speech.extra.EXTRA_ADDITIONAL_LANGUAGES"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhwg;->dtd:[Ljava/lang/String;

    .line 94
    iget-boolean v0, p0, Lhwg;->dsZ:Z

    if-eqz v0, :cond_1

    .line 95
    const-string v0, "auto-script"

    move-object v2, p0

    .line 97
    :goto_1
    iput-object v0, v2, Lhwg;->dtb:Ljava/lang/String;

    .line 100
    const-string v0, "android.speech.extra.PROFANITY_FILTER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "android.speech.extra.PROFANITY_FILTER"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lhwg;->dte:Ljava/lang/Boolean;

    .line 103
    invoke-static {p1}, Lhwg;->aj(Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lhwg;->dtf:Landroid/net/Uri;

    .line 104
    return-void

    .line 87
    :cond_0
    const-string v2, "IntentApiParams"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "The audio format is not supported [requested="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " supported="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v3, Lgff;->cIt:Lgff;

    invoke-virtual {v3}, Lgff;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Lhwg;->aAJ:Landroid/app/PendingIntent;

    if-nez p2, :cond_2

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object p2

    :cond_2
    const-string v0, "android"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "calling_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "calling_package"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :cond_3
    invoke-static {p2}, Ligh;->pt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, p0

    goto :goto_1

    :cond_4
    const-string v0, ""

    move-object v2, p0

    goto :goto_1

    .line 100
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static aj(Landroid/content/Intent;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 195
    sget-object v0, Lcgg;->aVh:Lcgg;

    invoke-virtual {v0}, Lcgg;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    :try_start_0
    const-string v0, "com.google.android.voicesearch.extra.AUDIO_SOURCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_0
    return-object v0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    const-string v2, "IntentApiParams"

    const-string v3, "Unexpected value for EXTRA_AUDIO_SOURCE"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 200
    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 203
    goto :goto_0
.end method


# virtual methods
.method public final aTd()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lhwg;->dsZ:Z

    return v0
.end method

.method public final aTe()Landroid/os/Bundle;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lhwg;->dta:Landroid/os/Bundle;

    return-object v0
.end method

.method public final aTf()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lhwg;->dtc:Z

    return v0
.end method

.method public final aTg()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lhwg;->byK:I

    return v0
.end method

.method public final aTh()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lhwg;->dtd:[Ljava/lang/String;

    return-object v0
.end method

.method public final aTi()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lhwg;->dtf:Landroid/net/Uri;

    return-object v0
.end method

.method public final aTj()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lhwg;->dte:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final getCallingPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lhwg;->dtb:Ljava/lang/String;

    return-object v0
.end method

.method public final getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lhwg;->cRZ:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrompt()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lhwg;->cGd:Ljava/lang/String;

    return-object v0
.end method

.method public final yy()Landroid/app/PendingIntent;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lhwg;->aAJ:Landroid/app/PendingIntent;

    return-object v0
.end method
