.class public Larv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final anA:Ljava/lang/String;

.field protected anB:I

.field protected final anC:I

.field protected final anD:Ljava/lang/String;

.field anE:Ljava/util/List;

.field anF:Ljava/util/List;

.field anG:I

.field private anH:Lcsm;

.field anI:Lary;

.field private final anz:Larx;

.field private final mAsyncServices:Lema;

.field final mContext:Landroid/content/Context;

.field private final mGsaConfigFlags:Lchk;

.field private final mHotwordDataManager:Lcse;

.field private final mVss:Lhhq;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhhq;Lema;Lchk;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Larv;->mContext:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Larv;->mVss:Lhhq;

    .line 59
    iput-object p3, p0, Larv;->mAsyncServices:Lema;

    .line 60
    iput-object p4, p0, Larv;->mGsaConfigFlags:Lchk;

    .line 61
    iput-object p5, p0, Larv;->anA:Ljava/lang/String;

    .line 62
    iput-object p6, p0, Larv;->anD:Ljava/lang/String;

    .line 63
    iput p7, p0, Larv;->anB:I

    .line 64
    iput p8, p0, Larv;->anC:I

    .line 65
    new-instance v0, Larx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Larx;-><init>(Larv;B)V

    iput-object v0, p0, Larv;->anz:Larx;

    .line 66
    invoke-virtual {p0}, Larv;->ue()Lcse;

    move-result-object v0

    iput-object v0, p0, Larv;->mHotwordDataManager:Lcse;

    .line 67
    return-void
.end method

.method static synthetic a(Larv;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Larv;->uc()V

    return-void
.end method

.method private uc()V
    .locals 3

    .prologue
    .line 104
    iget v0, p0, Larv;->anC:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 106
    iget-object v0, p0, Larv;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Larv;->anA:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcse;->a([BLjava/lang/String;)V

    .line 108
    :cond_0
    invoke-virtual {p0}, Larv;->ud()V

    .line 109
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Lary;)V
    .locals 4

    .prologue
    .line 70
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iput-object p1, p0, Larv;->anE:Ljava/util/List;

    .line 76
    iput-object p2, p0, Larv;->anI:Lary;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Larv;->anF:Ljava/util/List;

    .line 80
    invoke-static {}, Leoh;->auT()V

    .line 81
    iget v0, p0, Larv;->anC:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 84
    iget-object v0, p0, Larv;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.googlequicksearchbox.action.PAUSE_HOTWORD"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.googlequicksearchbox.extra.PAUSE_HOTWORD_REQUESTING_PACKAGE"

    iget-object v3, p0, Larv;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 89
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Larv;->anG:I

    .line 90
    iget-object v0, p0, Larv;->mHotwordDataManager:Lcse;

    iget-object v1, p0, Larv;->anD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcse;->iE(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    invoke-direct {p0}, Larv;->uc()V

    goto :goto_0

    .line 93
    :cond_3
    iget-object v0, p0, Larv;->mHotwordDataManager:Lcse;

    iget-object v1, p0, Larv;->anD:Ljava/lang/String;

    new-instance v2, Larw;

    const-string v3, "VerificationTaskDataManager"

    invoke-direct {v2, p0, v3}, Larw;-><init>(Larv;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcse;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method final close()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Larv;->anH:Lcsm;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Larv;->anH:Lcsm;

    invoke-virtual {v0}, Lcsm;->close()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Larv;->anH:Lcsm;

    .line 142
    :cond_0
    return-void
.end method

.method protected e(Ljava/io/InputStream;)Lcsm;
    .locals 13

    .prologue
    .line 122
    iget-object v0, p0, Larv;->mAsyncServices:Lema;

    const-string v1, "VerificationTaskHotword"

    invoke-virtual {v0, v1}, Lema;->ld(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    .line 124
    new-instance v0, Lcsm;

    iget-object v2, p0, Larv;->anz:Larx;

    iget v4, p0, Larv;->anB:I

    invoke-virtual {p0}, Larv;->ue()Lcse;

    move-result-object v5

    iget-object v6, p0, Larv;->anD:Ljava/lang/String;

    iget v7, p0, Larv;->anC:I

    iget-object v1, p0, Larv;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->HN()F

    move-result v8

    iget-object v1, p0, Larv;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->HK()[F

    move-result-object v9

    iget-object v1, p0, Larv;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->HL()[F

    move-result-object v10

    iget-object v1, p0, Larv;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->HM()[F

    move-result-object v11

    iget-object v12, p0, Larv;->anA:Ljava/lang/String;

    move-object v1, p1

    invoke-direct/range {v0 .. v12}, Lcsm;-><init>(Ljava/io/InputStream;Lggg;Ljava/util/concurrent/ExecutorService;ILcrz;Ljava/lang/String;IF[F[F[FLjava/lang/String;)V

    return-object v0
.end method

.method protected final ud()V
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Larv;->close()V

    .line 116
    iget-object v0, p0, Larv;->anE:Ljava/util/List;

    iget v1, p0, Larv;->anG:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/InputStream;

    invoke-virtual {p0, v0}, Larv;->e(Ljava/io/InputStream;)Lcsm;

    move-result-object v0

    iput-object v0, p0, Larv;->anH:Lcsm;

    .line 117
    iget-object v0, p0, Larv;->anH:Lcsm;

    invoke-virtual {v0}, Lcsm;->start()V

    .line 118
    return-void
.end method

.method protected ue()Lcse;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Larv;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    return-object v0
.end method
