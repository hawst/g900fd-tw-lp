.class public final Ldiz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final bvm:Lcom/google/android/shared/search/Query;

.field private final bzn:Ldjn;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bzo:I

.field private final bzp:I

.field private final bzq:I

.field private final bzr:I

.field private final bzs:I

.field private final mCursor:Landroid/database/Cursor;

.field private final mSource:Ldiq;


# direct methods
.method public constructor <init>(Ldiq;Lcom/google/android/shared/search/Query;Landroid/database/Cursor;Ldjn;)V
    .locals 1
    .param p4    # Ldjn;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Ldiz;->mSource:Ldiq;

    .line 85
    iput-object p2, p0, Ldiz;->bvm:Lcom/google/android/shared/search/Query;

    .line 86
    iput-object p3, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    .line 87
    iput-object p4, p0, Ldiz;->bzn:Ldjn;

    .line 88
    const-string v0, "suggest_format"

    invoke-virtual {p0, v0}, Ldiz;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldiz;->bzo:I

    .line 89
    const-string v0, "suggest_text_1"

    invoke-virtual {p0, v0}, Ldiz;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldiz;->bzp:I

    .line 90
    const-string v0, "suggest_text_2"

    invoke-virtual {p0, v0}, Ldiz;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldiz;->bzq:I

    .line 91
    const-string v0, "suggest_text_2_url"

    invoke-virtual {p0, v0}, Ldiz;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldiz;->bzr:I

    .line 92
    const-string v0, "suggest_icon_1"

    invoke-virtual {p0, v0}, Ldiz;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldiz;->bzs:I

    .line 93
    return-void
.end method

.method private acm()I
    .locals 4

    .prologue
    .line 156
    const/4 v0, -0x1

    .line 158
    :try_start_0
    const-string v1, "suggest_log_type"

    invoke-direct {p0, v1}, Ldiz;->jI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 162
    :goto_0
    return v0

    .line 159
    :catch_0
    move-exception v1

    .line 160
    const-string v2, "QSB.CursorSuggestionBuilder"

    const-string v3, "NumberFormatException retriving suggestion log type: %s"

    invoke-static {v2, v3, v1}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private d(IJ)J
    .locals 4

    .prologue
    .line 271
    iget-object v0, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_1

    .line 280
    :cond_0
    :goto_0
    return-wide p2

    .line 272
    :cond_1
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 276
    :try_start_0
    iget-object v0, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 279
    const-string v1, "QSB.CursorSuggestionBuilder"

    const-string v2, "getLong() failed, "

    invoke-static {v1, v2, v0}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private fV(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 250
    iget-object v1, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-object v0

    .line 251
    :cond_1
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 255
    :try_start_0
    iget-object v1, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 256
    :catch_0
    move-exception v1

    .line 258
    const-string v2, "QSB.CursorSuggestionBuilder"

    const-string v3, "getString() failed, "

    invoke-static {v2, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private jH(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 285
    if-eqz p1, :cond_0

    const-string v0, "html"

    iget v1, p0, Ldiz;->bzo:I

    invoke-direct {p0, v1}, Ldiz;->fV(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    .line 288
    :cond_0
    return-object p1
.end method

.method private jI(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    invoke-virtual {p0, p1}, Ldiz;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 300
    invoke-direct {p0, v0}, Ldiz;->fV(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final acl()Lcom/google/android/shared/search/Suggestion;
    .locals 4

    .prologue
    .line 96
    new-instance v0, Lded;

    invoke-direct {v0}, Lded;-><init>()V

    iget-object v1, p0, Ldiz;->mSource:Ldiq;

    invoke-virtual {v0, v1}, Lded;->h(Ldgb;)Lded;

    move-result-object v0

    iget v1, p0, Ldiz;->bzp:I

    invoke-direct {p0, v1}, Ldiz;->fV(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Ldiz;->jH(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lded;->bux:Ljava/lang/CharSequence;

    iget v1, p0, Ldiz;->bzq:I

    invoke-direct {p0, v1}, Ldiz;->fV(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Ldiz;->jH(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Lded;->buy:Ljava/lang/CharSequence;

    iget v1, p0, Ldiz;->bzr:I

    invoke-direct {p0, v1}, Ldiz;->fV(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buz:Ljava/lang/String;

    iget-object v1, p0, Ldiz;->mSource:Ldiq;

    invoke-interface {v1}, Ldiq;->ack()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Ldiz;->bzs:I

    invoke-direct {p0, v2}, Ldiz;->fV(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lesp;->aG(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buA:Ljava/lang/String;

    const-string v1, "suggest_last_access_hint"

    invoke-virtual {p0, v1}, Ldiz;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const-wide/16 v2, 0x0

    invoke-direct {p0, v1, v2, v3}, Ldiz;->d(IJ)J

    move-result-wide v2

    iput-wide v2, v0, Lded;->buC:J

    invoke-virtual {p0}, Ldiz;->acn()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buE:Ljava/lang/String;

    invoke-virtual {p0}, Ldiz;->aco()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buF:Ljava/lang/String;

    const-string v1, "suggest_intent_extra_data"

    invoke-direct {p0, v1}, Ldiz;->jI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buG:Ljava/lang/String;

    iget-object v1, p0, Ldiz;->mSource:Ldiq;

    invoke-interface {v1}, Ldiq;->aci()Landroid/content/ComponentName;

    move-result-object v1

    iput-object v1, v0, Lded;->buI:Landroid/content/ComponentName;

    const-string v1, "suggest_intent_query"

    invoke-direct {p0, v1}, Ldiz;->jI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lded;->buJ:Ljava/lang/String;

    invoke-direct {p0}, Ldiz;->acm()I

    move-result v1

    invoke-virtual {v0, v1}, Lded;->fI(I)Lded;

    move-result-object v0

    invoke-virtual {v0}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public final acn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    const-string v0, "suggest_intent_action"

    invoke-direct {p0, v0}, Ldiz;->jI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_0

    .line 192
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldiz;->mSource:Ldiq;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldiz;->mSource:Ldiq;

    invoke-interface {v0}, Ldiq;->abJ()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final aco()Ljava/lang/String;
    .locals 3

    .prologue
    .line 205
    const-string v0, "suggest_intent_data"

    invoke-direct {p0, v0}, Ldiz;->jI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    if-nez v0, :cond_0

    iget-object v1, p0, Ldiz;->mSource:Ldiq;

    if-eqz v1, :cond_0

    .line 207
    iget-object v0, p0, Ldiz;->mSource:Ldiq;

    invoke-interface {v0}, Ldiq;->abK()Ljava/lang/String;

    move-result-object v0

    .line 210
    :cond_0
    if-eqz v0, :cond_1

    .line 211
    const-string v1, "suggest_intent_data_id"

    invoke-direct {p0, v1}, Ldiz;->jI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 212
    if-eqz v1, :cond_1

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    :cond_1
    return-object v0
.end method

.method public final b(Ljava/util/List;Z)Ldef;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Ldiz;->mSource:Ldiq;

    invoke-interface {v0}, Ldiq;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldiz;->bvm:Lcom/google/android/shared/search/Query;

    invoke-static {v0, v1, p1, p2}, Ldeg;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;Z)Ldef;

    move-result-object v0

    return-object v0
.end method

.method public final fU(I)Ldef;
    .locals 9
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    iget-object v0, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    .line 114
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Ldiz;->b(Ljava/util/List;Z)Ldef;

    move-result-object v0

    .line 130
    :goto_0
    return-object v0

    .line 116
    :cond_0
    iget-object v0, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 117
    iget-object v0, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 118
    iget-object v0, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    const-string v3, "suggest_last_access_hint"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_3

    move v0, v1

    .line 120
    :goto_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v4, v2

    .line 121
    :goto_3
    if-ge v4, v5, :cond_7

    .line 122
    iget-object v3, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 123
    invoke-virtual {p0}, Ldiz;->acl()Lcom/google/android/shared/search/Suggestion;

    move-result-object v7

    .line 124
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Lcom/google/android/shared/search/Suggestion;->arY()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "content://applications/applications/com.google.android.googlequicksearchbox/com.google.android.googlequicksearchbox.SearchActivity"

    invoke-virtual {v7}, Lcom/google/android/shared/search/Suggestion;->ash()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v7}, Lcom/google/android/shared/search/Suggestion;->asf()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_4
    if-nez v3, :cond_6

    iget-object v3, p0, Ldiz;->bzn:Ldjn;

    if-eqz v3, :cond_5

    iget-object v3, p0, Ldiz;->bzn:Ldjn;

    invoke-interface {v3, v7}, Ldjn;->i(Lcom/google/android/shared/search/Suggestion;)Z

    move-result v3

    :goto_5
    if-eqz v3, :cond_1

    .line 125
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    :cond_2
    move v0, v2

    .line 116
    goto :goto_1

    :cond_3
    move v0, v2

    .line 118
    goto :goto_2

    :cond_4
    move v3, v2

    .line 124
    goto :goto_4

    :cond_5
    move v3, v1

    goto :goto_5

    :cond_6
    move v3, v2

    goto :goto_5

    .line 130
    :cond_7
    invoke-virtual {p0, v6, v0}, Ldiz;->b(Ljava/util/List;Z)Ldef;

    move-result-object v0

    goto :goto_0
.end method

.method public final getColumnIndex(Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 233
    iget-object v1, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_0

    .line 239
    :goto_0
    return v0

    .line 235
    :cond_0
    :try_start_0
    iget-object v1, p0, Ldiz;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 236
    :catch_0
    move-exception v1

    .line 238
    const-string v2, "QSB.CursorSuggestionBuilder"

    const-string v3, "getColumnIndex() failed, "

    invoke-static {v2, v3, v1}, Leor;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 317
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CursorSuggestionBuilder["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldiz;->bvm:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
