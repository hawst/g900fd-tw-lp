.class public final Lbcf;
.super Ljava/lang/Object;


# instance fields
.field public auJ:Ljava/lang/String;

.field public awp:Landroid/net/Uri;

.field public final awq:Ljava/util/List;

.field public awr:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

.field public aws:Z

.field public final mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbcf;->mName:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbcf;->awq:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)Lbcf;
    .locals 3

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v2, p0, Lbcf;->awq:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p0
.end method
