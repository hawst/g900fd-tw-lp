.class public abstract Lhqy;
.super Lfv;
.source "PG"


# instance fields
.field private final clj:Landroid/app/FragmentManager;

.field private doL:Landroid/app/FragmentTransaction;

.field private doM:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 180
    invoke-direct {p0}, Lfv;-><init>()V

    .line 177
    iput-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    .line 178
    iput-object v0, p0, Lhqy;->doM:Landroid/app/Fragment;

    .line 181
    iput-object p1, p0, Lhqy;->clj:Landroid/app/FragmentManager;

    .line 182
    return-void
.end method


# virtual methods
.method public final V()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 195
    iget-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lhqy;->clj:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iput-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    .line 200
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    invoke-static {v0, p2}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->u(II)Ljava/lang/String;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lhqy;->clj:Landroid/app/FragmentManager;

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 202
    if-eqz v0, :cond_2

    .line 204
    iget-object v1, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 211
    :goto_0
    iget-object v1, p0, Lhqy;->doM:Landroid/app/Fragment;

    if-eq v0, v1, :cond_1

    .line 212
    invoke-virtual {v0, v4}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    .line 213
    invoke-virtual {v0, v4}, Landroid/app/Fragment;->setUserVisibleHint(Z)V

    .line 216
    :cond_1
    return-object v0

    .line 206
    :cond_2
    invoke-virtual {p0, p2}, Lhqy;->ls(I)Landroid/app/Fragment;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v3

    invoke-static {v3, p2}, Lcom/google/android/voicesearch/greco3/languagepack/InstallActivity;->u(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 249
    const/4 v0, 0x0

    iput-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    .line 250
    iget-object v0, p0, Lhqy;->clj:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 252
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lhqy;->clj:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iput-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    .line 226
    :cond_0
    iget-object v0, p0, Lhqy;->doL:Landroid/app/FragmentTransaction;

    check-cast p3, Landroid/app/Fragment;

    invoke-virtual {v0, p3}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 227
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 256
    check-cast p2, Landroid/app/Fragment;

    invoke-virtual {p2}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 231
    check-cast p3, Landroid/app/Fragment;

    .line 232
    iget-object v0, p0, Lhqy;->doM:Landroid/app/Fragment;

    if-eq p3, v0, :cond_2

    .line 233
    iget-object v0, p0, Lhqy;->doM:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lhqy;->doM:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    .line 235
    iget-object v0, p0, Lhqy;->doM:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/Fragment;->setUserVisibleHint(Z)V

    .line 237
    :cond_0
    if-eqz p3, :cond_1

    .line 238
    invoke-virtual {p3, v2}, Landroid/app/Fragment;->setMenuVisibility(Z)V

    .line 239
    invoke-virtual {p3, v2}, Landroid/app/Fragment;->setUserVisibleHint(Z)V

    .line 241
    :cond_1
    iput-object p3, p0, Lhqy;->doM:Landroid/app/Fragment;

    .line 243
    :cond_2
    return-void
.end method

.method public abstract ls(I)Landroid/app/Fragment;
.end method
