.class public Lecq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Leti;


# static fields
.field private static final bUV:Lecm;

.field private static bUW:J


# instance fields
.field private aOR:Landroid/os/Bundle;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aOT:Z

.field private final abz:Landroid/content/ServiceConnection;

.field private final anp:Lecr;

.field private final bUX:Lecm;

.field private final bUY:Lecm;

.field private final bUZ:Lect;

.field bVa:Lecj;

.field protected bVb:Lecg;

.field public final bpc:J

.field private bpg:J

.field private bph:Landroid/os/Bundle;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bpk:Z

.field private bpm:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bpn:Z

.field private bpo:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private cZ:I

.field private cq:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lecp;

    invoke-direct {v0}, Lecp;-><init>()V

    sput-object v0, Lecq;->bUV:Lecm;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lecr;Lecm;Lcom/google/android/search/shared/service/ClientConfig;Landroid/os/Bundle;)V
    .locals 3
    .param p5    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    iput-object p1, p0, Lecq;->mContext:Landroid/content/Context;

    .line 160
    if-eqz p5, :cond_0

    const-string v0, "search_service_client:id"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    const-string v0, "search_service_client:id"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lecq;->bpc:J

    .line 165
    :goto_0
    iput-object p2, p0, Lecq;->anp:Lecr;

    .line 168
    new-instance v0, Leoe;

    invoke-direct {v0}, Leoe;-><init>()V

    .line 169
    const-class v1, Lecm;

    invoke-static {v0, v1, p3}, Lers;->a(Ljava/util/concurrent/Executor;Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lecm;

    iput-object v0, p0, Lecq;->bUX:Lecm;

    .line 171
    iput-object p3, p0, Lecq;->bUY:Lecm;

    .line 172
    new-instance v0, Lect;

    invoke-direct {v0}, Lect;-><init>()V

    iput-object v0, p0, Lecq;->bUZ:Lect;

    .line 173
    iget-object v0, p0, Lecq;->bUZ:Lect;

    sget-object v1, Lecq;->bUV:Lecm;

    sget-object v2, Lecq;->bUV:Lecm;

    invoke-virtual {v0, v1, v2}, Lect;->a(Lecm;Lecm;)V

    .line 174
    new-instance v0, Lecs;

    iget-object v1, p0, Lecq;->bUZ:Lect;

    invoke-direct {v0, p0, v1, p4}, Lecs;-><init>(Lecq;Lect;Lcom/google/android/search/shared/service/ClientConfig;)V

    iput-object v0, p0, Lecq;->abz:Landroid/content/ServiceConnection;

    .line 175
    return-void

    .line 163
    :cond_0
    invoke-static {}, Lecq;->anw()J

    move-result-wide v0

    iput-wide v0, p0, Lecq;->bpc:J

    goto :goto_0
.end method

.method private UI()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 537
    iput-object v0, p0, Lecq;->bpo:Lcom/google/android/shared/search/Query;

    .line 538
    iput-object v0, p0, Lecq;->bpm:Lcom/google/android/shared/search/Query;

    .line 539
    const/4 v0, 0x0

    iput-boolean v0, p0, Lecq;->bpn:Z

    .line 540
    return-void
.end method

.method private static declared-synchronized anw()J
    .locals 6

    .prologue
    .line 185
    const-class v1, Lecq;

    monitor-enter v1

    :try_start_0
    sget-wide v2, Lecq;->bUW:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 189
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    int-to-long v2, v0

    .line 190
    const/16 v0, 0x20

    shl-long/2addr v2, v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    add-long/2addr v2, v4

    sput-wide v2, Lecq;->bUW:J

    .line 192
    :cond_0
    sget-wide v2, Lecq;->bUW:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    sput-wide v2, Lecq;->bUW:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-wide v2

    .line 185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(JLandroid/os/Bundle;Landroid/os/Bundle;I)V
    .locals 9
    .param p3    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 310
    iget-boolean v0, p0, Lecq;->cq:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lecq;->cZ:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 311
    :cond_0
    iput-boolean v1, p0, Lecq;->cq:Z

    .line 312
    iput-wide p1, p0, Lecq;->bpg:J

    .line 313
    iput-object p3, p0, Lecq;->aOR:Landroid/os/Bundle;

    .line 314
    iput-object p4, p0, Lecq;->bph:Landroid/os/Bundle;

    .line 315
    iput p5, p0, Lecq;->cZ:I

    .line 316
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    :try_start_0
    iget-object v1, p0, Lecq;->bVa:Lecj;

    iget-wide v2, p0, Lecq;->bpc:J

    iget-wide v4, p0, Lecq;->bpg:J

    iget-object v6, p0, Lecq;->aOR:Landroid/os/Bundle;

    iget-object v7, p0, Lecq;->bph:Landroid/os/Bundle;

    iget v8, p0, Lecq;->cZ:I

    invoke-interface/range {v1 .. v8}, Lecj;->a(JJLandroid/os/Bundle;Landroid/os/Bundle;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    :cond_1
    :goto_1
    return-void

    .line 310
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 317
    :catch_0
    move-exception v0

    const-string v1, "SearchServiceClient"

    const-string v2, "startClient failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private eI(Z)V
    .locals 4

    .prologue
    .line 370
    iget-boolean v0, p0, Lecq;->cq:Z

    if-eqz v0, :cond_0

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lecq;->cq:Z

    .line 372
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 374
    :try_start_0
    iget-object v0, p0, Lecq;->bVa:Lecj;

    iget-wide v2, p0, Lecq;->bpc:J

    invoke-interface {v0, v2, v3, p1}, Lecj;->c(JZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 375
    :catch_0
    move-exception v0

    .line 376
    const-string v1, "SearchServiceClient"

    const-string v2, "stopClient failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 378
    :cond_1
    if-eqz p1, :cond_0

    .line 379
    const-string v0, "SearchServiceClient"

    const-string v1, "Attempt to handover from detached client"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final B(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 281
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lecq;->e(Landroid/os/Bundle;I)V

    .line 282
    return-void
.end method

.method public final Bq()V
    .locals 3

    .prologue
    .line 505
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0}, Lecg;->Bq()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 508
    :catch_0
    move-exception v0

    .line 509
    const-string v1, "SearchServiceClient"

    const-string v2, "stopSpeaking() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final C(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 297
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lecq;->f(Landroid/os/Bundle;I)V

    .line 298
    return-void
.end method

.method protected H(Landroid/os/IBinder;)Lecj;
    .locals 1

    .prologue
    .line 181
    invoke-static {p1}, Leck;->F(Landroid/os/IBinder;)Lecj;

    move-result-object v0

    return-object v0
.end method

.method public final UF()V
    .locals 3

    .prologue
    .line 418
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0}, Lecg;->UF()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 428
    :goto_0
    return-void

    .line 421
    :catch_0
    move-exception v0

    .line 422
    const-string v1, "SearchServiceClient"

    const-string v2, "startQueryEdit() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 425
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lecq;->bpn:Z

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lecq;->bpo:Lcom/google/android/shared/search/Query;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Z)V
    .locals 4

    .prologue
    .line 207
    const-string v0, "search_service_client:id"

    iget-wide v2, p0, Lecq;->bpc:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 208
    return-void
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 3

    .prologue
    .line 453
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0, p1, p2}, Lecg;->a(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 456
    :catch_0
    move-exception v0

    .line 457
    const-string v1, "SearchServiceClient"

    const-string v2, "onSuggestionClicked() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/shared/search/Suggestion;ZLcom/google/android/shared/search/SearchBoxStats;)V
    .locals 3

    .prologue
    .line 474
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0, p1, p2, p3}, Lecg;->a(Lcom/google/android/shared/search/Suggestion;ZLcom/google/android/shared/search/SearchBoxStats;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :cond_0
    :goto_0
    return-void

    .line 477
    :catch_0
    move-exception v0

    .line 478
    const-string v1, "SearchServiceClient"

    const-string v2, "onSuggestionShortcutCreated() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Letj;)V
    .locals 4

    .prologue
    .line 544
    const-string v0, "ID"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-wide v2, p0, Lecq;->bpc:J

    invoke-virtual {v0, v2, v3}, Letn;->be(J)V

    .line 545
    const-string v0, "Connected"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v1

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 546
    const-string v0, "Started"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-boolean v1, p0, Lecq;->cq:Z

    invoke-virtual {v0, v1}, Letn;->ff(Z)V

    .line 547
    return-void
.end method

.method public final aD(J)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 289
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v5, v4

    invoke-direct/range {v1 .. v6}, Lecq;->b(JLandroid/os/Bundle;Landroid/os/Bundle;I)V

    .line 290
    return-void
.end method

.method final anA()Z
    .locals 9

    .prologue
    .line 345
    :try_start_0
    iget-object v0, p0, Lecq;->bpm:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lecq;->bVb:Lecg;

    iget-object v1, p0, Lecq;->bpm:Lcom/google/android/shared/search/Query;

    invoke-interface {v0, v1}, Lecg;->y(Lcom/google/android/shared/search/Query;)V

    .line 348
    :cond_0
    iget-boolean v0, p0, Lecq;->bpn:Z

    if-eqz v0, :cond_1

    .line 349
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0}, Lecg;->UF()V

    .line 351
    :cond_1
    iget-object v0, p0, Lecq;->bpo:Lcom/google/android/shared/search/Query;

    if-eqz v0, :cond_2

    .line 352
    iget-object v0, p0, Lecq;->bVb:Lecg;

    iget-object v1, p0, Lecq;->bpo:Lcom/google/android/shared/search/Query;

    invoke-interface {v0, v1}, Lecg;->x(Lcom/google/android/shared/search/Query;)V

    .line 354
    :cond_2
    iget-boolean v0, p0, Lecq;->cq:Z

    if-eqz v0, :cond_3

    .line 355
    iget-object v1, p0, Lecq;->bVa:Lecj;

    iget-wide v2, p0, Lecq;->bpc:J

    iget-wide v4, p0, Lecq;->bpg:J

    iget-object v6, p0, Lecq;->aOR:Landroid/os/Bundle;

    iget-object v7, p0, Lecq;->bph:Landroid/os/Bundle;

    iget v8, p0, Lecq;->cZ:I

    invoke-interface/range {v1 .. v8}, Lecj;->a(JJLandroid/os/Bundle;Landroid/os/Bundle;I)V

    .line 358
    :cond_3
    invoke-direct {p0}, Lecq;->UI()V

    .line 360
    iget-object v0, p0, Lecq;->bVb:Lecg;

    iget-boolean v1, p0, Lecq;->bpk:Z

    invoke-interface {v0, v1}, Lecg;->bq(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    const/4 v0, 0x1

    .line 365
    :goto_0
    return v0

    .line 363
    :catch_0
    move-exception v0

    .line 364
    const-string v1, "SearchServiceClient"

    const-string v2, "executePendingActions failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 365
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final anx()V
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Lecq;->bUZ:Lect;

    sget-object v1, Lecq;->bUV:Lecm;

    sget-object v2, Lecq;->bUV:Lecm;

    invoke-virtual {v0, v1, v2}, Lect;->a(Lecm;Lecm;)V

    .line 264
    return-void
.end method

.method public final any()V
    .locals 2

    .prologue
    .line 276
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lecq;->e(Landroid/os/Bundle;I)V

    .line 277
    return-void
.end method

.method public final anz()V
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lecq;->eI(Z)V

    .line 341
    return-void
.end method

.method public final b(Lcom/google/android/search/shared/actions/VoiceAction;I)V
    .locals 3

    .prologue
    .line 515
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    new-instance v1, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;

    invoke-direct {v1, p1}, Lcom/google/android/search/shared/actions/ParcelableVoiceAction;-><init>(Lcom/google/android/search/shared/actions/VoiceAction;)V

    invoke-interface {v0, v1, p2}, Lecg;->a(Lcom/google/android/search/shared/actions/ParcelableVoiceAction;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 522
    :cond_0
    :goto_0
    return-void

    .line 518
    :catch_0
    move-exception v0

    .line 519
    const-string v1, "SearchServiceClient"

    const-string v2, "requestExecution() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final b(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V
    .locals 3

    .prologue
    .line 463
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0, p1, p2}, Lecg;->b(Lcom/google/android/shared/search/Suggestion;Lcom/google/android/shared/search/SearchBoxStats;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    :cond_0
    :goto_0
    return-void

    .line 466
    :catch_0
    move-exception v0

    .line 467
    const-string v1, "SearchServiceClient"

    const-string v2, "onQuickContactClicked() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final bq(Z)V
    .locals 3

    .prologue
    .line 494
    iput-boolean p1, p0, Lecq;->bpk:Z

    .line 495
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0, p1}, Lecg;->bq(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 502
    :cond_0
    :goto_0
    return-void

    .line 498
    :catch_0
    move-exception v0

    .line 499
    const-string v1, "SearchServiceClient"

    const-string v2, "setHotwordDetectionEnabled() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final c(Lcom/google/android/shared/search/Suggestion;)V
    .locals 3

    .prologue
    .line 484
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0, p1}, Lecg;->c(Lcom/google/android/shared/search/Suggestion;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 491
    :cond_0
    :goto_0
    return-void

    .line 487
    :catch_0
    move-exception v0

    .line 488
    const-string v1, "SearchServiceClient"

    const-string v2, "removeSuggestionFromHistory() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final cancel()V
    .locals 3

    .prologue
    .line 431
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0}, Lecg;->cancel()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 440
    :goto_0
    return-void

    .line 434
    :catch_0
    move-exception v0

    .line 435
    const-string v1, "SearchServiceClient"

    const-string v2, "cancel() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 438
    :cond_0
    invoke-direct {p0}, Lecq;->UI()V

    goto :goto_0
.end method

.method public final connect()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 215
    iget-boolean v0, p0, Lecq;->aOT:Z

    if-eqz v0, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iput-boolean v3, p0, Lecq;->aOT:Z

    .line 220
    iget-object v0, p0, Lecq;->bUZ:Lect;

    iget-object v1, p0, Lecq;->bUX:Lecm;

    iget-object v2, p0, Lecq;->bUY:Lecm;

    invoke-virtual {v0, v1, v2}, Lect;->a(Lecm;Lecm;)V

    .line 228
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 229
    iget-object v1, p0, Lecq;->mContext:Landroid/content/Context;

    const-string v2, "com.google.android.search.core.service.SearchService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    iget-object v1, p0, Lecq;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lecq;->abz:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    const-string v0, "SearchServiceClient"

    const-string v1, "Unable to bind to the search service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v0, 0x0

    iput-boolean v0, p0, Lecq;->aOT:Z

    goto :goto_0
.end method

.method public final disconnect()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 239
    iget-boolean v0, p0, Lecq;->aOT:Z

    if-eqz v0, :cond_1

    .line 240
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    :try_start_0
    iget-object v0, p0, Lecq;->bVa:Lecj;

    iget-wide v2, p0, Lecq;->bpc:J

    invoke-interface {v0, v2, v3}, Lecj;->aj(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :cond_0
    :goto_0
    iget-object v0, p0, Lecq;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lecq;->abz:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 248
    iput-object v4, p0, Lecq;->bVb:Lecg;

    .line 249
    iput-object v4, p0, Lecq;->bVa:Lecj;

    .line 250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lecq;->aOT:Z

    .line 252
    :cond_1
    return-void

    .line 243
    :catch_0
    move-exception v0

    .line 244
    const-string v1, "SearchServiceClient"

    const-string v2, "detachClient failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;I)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 285
    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    move-object v1, p0

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lecq;->b(JLandroid/os/Bundle;Landroid/os/Bundle;I)V

    .line 286
    return-void
.end method

.method public final f(Landroid/os/Bundle;I)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 304
    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-object v4, p1

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lecq;->b(JLandroid/os/Bundle;Landroid/os/Bundle;I)V

    .line 305
    return-void
.end method

.method public final getId()J
    .locals 2

    .prologue
    .line 196
    iget-wide v0, p0, Lecq;->bpc:J

    return-wide v0
.end method

.method public final hg(I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 272
    const-wide/16 v2, 0x0

    move-object v1, p0

    move-object v5, v4

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lecq;->b(JLandroid/os/Bundle;Landroid/os/Bundle;I)V

    .line 273
    return-void
.end method

.method public final isConnected()Z
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lecq;->bVb:Lecg;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConnected()V
    .locals 1

    .prologue
    .line 529
    iget-object v0, p0, Lecq;->anp:Lecr;

    invoke-interface {v0}, Lecr;->onServiceConnected()V

    .line 530
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lecq;->anp:Lecr;

    invoke-interface {v0}, Lecr;->tY()V

    .line 534
    return-void
.end method

.method public final start()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 268
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lecq;->hg(I)V

    .line 269
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lecq;->eI(Z)V

    .line 337
    return-void
.end method

.method public final stopListening()V
    .locals 3

    .prologue
    .line 443
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0}, Lecg;->stopListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 446
    :catch_0
    move-exception v0

    .line 447
    const-string v1, "SearchServiceClient"

    const-string v2, "stopListening() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final x(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 386
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0, p1}, Lecg;->x(Lcom/google/android/shared/search/Query;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 389
    :catch_0
    move-exception v0

    .line 390
    const-string v1, "SearchServiceClient"

    const-string v2, "set() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 393
    :cond_1
    iput-object p1, p0, Lecq;->bpo:Lcom/google/android/shared/search/Query;

    .line 395
    iget-boolean v0, p0, Lecq;->bpn:Z

    if-nez v0, :cond_0

    .line 396
    const/4 v0, 0x0

    iput-object v0, p0, Lecq;->bpm:Lcom/google/android/shared/search/Query;

    goto :goto_0
.end method

.method public final y(Lcom/google/android/shared/search/Query;)V
    .locals 3

    .prologue
    .line 403
    invoke-virtual {p0}, Lecq;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    :try_start_0
    iget-object v0, p0, Lecq;->bVb:Lecg;

    invoke-interface {v0, p1}, Lecg;->y(Lcom/google/android/shared/search/Query;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    :goto_0
    return-void

    .line 406
    :catch_0
    move-exception v0

    .line 407
    const-string v1, "SearchServiceClient"

    const-string v2, "commit() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 410
    :cond_0
    iput-object p1, p0, Lecq;->bpm:Lcom/google/android/shared/search/Query;

    .line 411
    const/4 v0, 0x0

    iput-object v0, p0, Lecq;->bpo:Lcom/google/android/shared/search/Query;

    .line 412
    const/4 v0, 0x0

    iput-boolean v0, p0, Lecq;->bpn:Z

    goto :goto_0
.end method
