.class public Ledc;
.super Landroid/animation/ValueAnimator;
.source "PG"


# instance fields
.field private final bVu:Ledf;

.field private bVv:I

.field protected hB:Landroid/view/View;


# direct methods
.method public constructor <init>(ZLedf;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 24
    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 25
    iput-object p2, p0, Ledc;->bVu:Ledf;

    .line 26
    sget-object v0, Ldqs;->bGc:Ldqs;

    invoke-virtual {p0, v0}, Ledc;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 27
    new-instance v0, Ledd;

    invoke-direct {v0, p0}, Ledd;-><init>(Ledc;)V

    invoke-virtual {p0, v0}, Ledc;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 33
    new-instance v0, Lede;

    invoke-direct {v0, p0}, Lede;-><init>(Ledc;)V

    invoke-virtual {p0, v0}, Ledc;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 39
    if-eqz p1, :cond_0

    .line 40
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    invoke-virtual {p0, v0}, Ledc;->setFloatValues([F)V

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-virtual {p0, v0}, Ledc;->setFloatValues([F)V

    goto :goto_0

    .line 40
    nop

    :array_0
    .array-data 4
        0x0
        -0x40800000    # -1.0f
    .end array-data

    .line 42
    :array_1
    .array-data 4
        -0x40800000    # -1.0f
        0x0
    .end array-data
.end method

.method private setTranslationY(F)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Ledc;->bVu:Ledf;

    iget-object v1, p0, Ledc;->hB:Landroid/view/View;

    invoke-interface {v0, v1}, Ledf;->aB(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v0

    add-float/2addr p1, v0

    .line 67
    :cond_0
    iget-object v0, p0, Ledc;->hB:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    .line 68
    return-void
.end method


# virtual methods
.method public I(F)V
    .locals 2

    .prologue
    .line 47
    iget v0, p0, Ledc;->bVv:I

    if-eqz v0, :cond_0

    .line 48
    iget v0, p0, Ledc;->bVv:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-direct {p0, v0}, Ledc;->setTranslationY(F)V

    .line 60
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Ledc;->hB:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 52
    iget-object v0, p0, Ledc;->hB:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Ledc;->bVv:I

    .line 53
    iget v0, p0, Ledc;->bVv:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-direct {p0, v0}, Ledc;->setTranslationY(F)V

    .line 54
    iget-object v0, p0, Ledc;->hB:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 57
    :cond_1
    iget-object v0, p0, Ledc;->hB:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public afE()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Ledc;->hB:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 72
    iget-object v0, p0, Ledc;->hB:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 73
    return-void
.end method

.method public setTarget(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 77
    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Ledc;->hB:Landroid/view/View;

    .line 78
    const/4 v0, 0x0

    iput v0, p0, Ledc;->bVv:I

    .line 79
    return-void
.end method
