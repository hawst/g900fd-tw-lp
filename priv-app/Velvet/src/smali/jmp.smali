.class public final Ljmp;
.super Ljsl;
.source "PG"


# static fields
.field public static final eui:Ljsm;


# instance fields
.field private aez:I

.field private epX:Ljava/lang/String;

.field private erv:Ljsc;

.field private euj:Ljava/lang/String;

.field private euk:Ljsc;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 7838
    const/16 v0, 0xb

    const-class v1, Ljmp;

    const v2, 0xc2197da

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljmp;->eui:Ljsm;

    .line 7844
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7901
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7902
    const/4 v0, 0x0

    iput v0, p0, Ljmp;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljmp;->euj:Ljava/lang/String;

    iput-object v1, p0, Ljmp;->euk:Ljsc;

    const-string v0, ""

    iput-object v0, p0, Ljmp;->epX:Ljava/lang/String;

    iput-object v1, p0, Ljmp;->erv:Ljsc;

    iput-object v1, p0, Ljmp;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljmp;->eCz:I

    .line 7903
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 7831
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljmp;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmp;->euj:Ljava/lang/String;

    iget v0, p0, Ljmp;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljmp;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljmp;->epX:Ljava/lang/String;

    iget v0, p0, Ljmp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmp;->aez:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljmp;->euk:Ljsc;

    if-nez v0, :cond_1

    new-instance v0, Ljsc;

    invoke-direct {v0}, Ljsc;-><init>()V

    iput-object v0, p0, Ljmp;->euk:Ljsc;

    :cond_1
    iget-object v0, p0, Ljmp;->euk:Ljsc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ljmp;->erv:Ljsc;

    if-nez v0, :cond_2

    new-instance v0, Ljsc;

    invoke-direct {v0}, Ljsc;-><init>()V

    iput-object v0, p0, Ljmp;->erv:Ljsc;

    :cond_2
    iget-object v0, p0, Ljmp;->erv:Ljsc;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 7919
    iget v0, p0, Ljmp;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7920
    const/4 v0, 0x1

    iget-object v1, p0, Ljmp;->euj:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7922
    :cond_0
    iget v0, p0, Ljmp;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7923
    const/4 v0, 0x2

    iget-object v1, p0, Ljmp;->epX:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7925
    :cond_1
    iget-object v0, p0, Ljmp;->euk:Ljsc;

    if-eqz v0, :cond_2

    .line 7926
    const/4 v0, 0x3

    iget-object v1, p0, Ljmp;->euk:Ljsc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 7928
    :cond_2
    iget-object v0, p0, Ljmp;->erv:Ljsc;

    if-eqz v0, :cond_3

    .line 7929
    const/4 v0, 0x4

    iget-object v1, p0, Ljmp;->erv:Ljsc;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 7931
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7932
    return-void
.end method

.method public final getBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7879
    iget-object v0, p0, Ljmp;->epX:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 7936
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 7937
    iget v1, p0, Ljmp;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 7938
    const/4 v1, 0x1

    iget-object v2, p0, Ljmp;->euj:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7941
    :cond_0
    iget v1, p0, Ljmp;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 7942
    const/4 v1, 0x2

    iget-object v2, p0, Ljmp;->epX:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7945
    :cond_1
    iget-object v1, p0, Ljmp;->euk:Ljsc;

    if-eqz v1, :cond_2

    .line 7946
    const/4 v1, 0x3

    iget-object v2, p0, Ljmp;->euk:Ljsc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7949
    :cond_2
    iget-object v1, p0, Ljmp;->erv:Ljsc;

    if-eqz v1, :cond_3

    .line 7950
    const/4 v1, 0x4

    iget-object v2, p0, Ljmp;->erv:Ljsc;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7953
    :cond_3
    return v0
.end method

.method public final xh(Ljava/lang/String;)Ljmp;
    .locals 1

    .prologue
    .line 7882
    if-nez p1, :cond_0

    .line 7883
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7885
    :cond_0
    iput-object p1, p0, Ljmp;->epX:Ljava/lang/String;

    .line 7886
    iget v0, p0, Ljmp;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljmp;->aez:I

    .line 7887
    return-object p0
.end method
