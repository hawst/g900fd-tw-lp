.class public final Lqm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static sX:[J


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x100

    const/4 v3, 0x0

    .line 39
    new-array v0, v8, [J

    sput-object v0, Lqm;->sX:[J

    .line 41
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "userdebug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_0
    move v6, v3

    .line 145
    :goto_0
    if-ge v6, v8, :cond_3

    .line 146
    int-to-long v0, v6

    move v2, v3

    move-wide v4, v0

    .line 147
    :goto_1
    const/16 v0, 0x8

    if-ge v2, v0, :cond_2

    .line 148
    long-to-int v0, v4

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    const-wide v0, -0x6a536cd653b4364bL    # -2.848111467964452E-204

    .line 149
    :goto_2
    const/4 v7, 0x1

    shr-long/2addr v4, v7

    xor-long/2addr v4, v0

    .line 147
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 148
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_2

    .line 151
    :cond_2
    sget-object v0, Lqm;->sX:[J

    aput-wide v4, v0, v6

    .line 145
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 153
    :cond_3
    return-void
.end method

.method public static a(Ljava/io/Closeable;)V
    .locals 3

    .prologue
    .line 174
    if-nez p0, :cond_0

    .line 180
    :goto_0
    return-void

    .line 176
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    :catch_0
    move-exception v0

    .line 178
    const-string v1, "Utils"

    const-string v2, "close fail "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static assertTrue(Z)V
    .locals 1

    .prologue
    .line 48
    if-nez p0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 51
    :cond_0
    return-void
.end method

.method public static ay(I)I
    .locals 3

    .prologue
    .line 80
    if-lez p0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    if-le p0, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "n is invalid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_1
    add-int/lit8 v0, p0, -0x1

    .line 82
    shr-int/lit8 v1, v0, 0x10

    or-int/2addr v0, v1

    .line 83
    shr-int/lit8 v1, v0, 0x8

    or-int/2addr v0, v1

    .line 84
    shr-int/lit8 v1, v0, 0x4

    or-int/2addr v0, v1

    .line 85
    shr-int/lit8 v1, v0, 0x2

    or-int/2addr v0, v1

    .line 86
    shr-int/lit8 v1, v0, 0x1

    or-int/2addr v0, v1

    .line 87
    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static az(I)I
    .locals 1

    .prologue
    .line 94
    if-gtz p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 95
    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    return v0
.end method

.method public static f(F)I
    .locals 2

    .prologue
    .line 188
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x1f

    if-ge v0, v1, :cond_0

    .line 189
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    int-to-float v1, v1

    cmpl-float v1, v1, p0

    if-gez v1, :cond_0

    .line 188
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    :cond_0
    return v0
.end method

.method public static f(III)I
    .locals 0

    .prologue
    .line 100
    if-le p0, p2, :cond_0

    .line 102
    :goto_0
    return p2

    .line 101
    :cond_0
    if-gez p0, :cond_1

    const/4 p2, 0x0

    goto :goto_0

    :cond_1
    move p2, p0

    .line 102
    goto :goto_0
.end method

.method public static g(F)I
    .locals 2

    .prologue
    .line 196
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x1f

    if-ge v0, v1, :cond_0

    .line 197
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    int-to-float v1, v1

    cmpl-float v1, v1, p0

    if-gtz v1, :cond_0

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    :cond_0
    add-int/lit8 v0, v0, -0x1

    return v0
.end method
