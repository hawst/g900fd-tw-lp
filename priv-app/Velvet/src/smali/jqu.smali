.class public final Ljqu;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ezv:[Ljqu;


# instance fields
.field private aez:I

.field private bmN:Ljava/lang/String;

.field private euN:I

.field private ezw:I

.field public ezx:[I

.field public ezy:Ljra;

.field public ezz:Ljqm;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 7186
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 7187
    iput v0, p0, Ljqu;->aez:I

    iput v0, p0, Ljqu;->euN:I

    iput v0, p0, Ljqu;->ezw:I

    const-string v0, ""

    iput-object v0, p0, Ljqu;->bmN:Ljava/lang/String;

    sget-object v0, Ljsu;->eCA:[I

    iput-object v0, p0, Ljqu;->ezx:[I

    iput-object v1, p0, Ljqu;->ezy:Ljra;

    iput-object v1, p0, Ljqu;->ezz:Ljqm;

    iput-object v1, p0, Ljqu;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljqu;->eCz:I

    .line 7188
    return-void
.end method

.method public static bsD()[Ljqu;
    .locals 2

    .prologue
    .line 7104
    sget-object v0, Ljqu;->ezv:[Ljqu;

    if-nez v0, :cond_1

    .line 7105
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 7107
    :try_start_0
    sget-object v0, Ljqu;->ezv:[Ljqu;

    if-nez v0, :cond_0

    .line 7108
    const/4 v0, 0x0

    new-array v0, v0, [Ljqu;

    sput-object v0, Ljqu;->ezv:[Ljqu;

    .line 7110
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7112
    :cond_1
    sget-object v0, Ljqu;->ezv:[Ljqu;

    return-object v0

    .line 7110
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final TV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 7158
    iget-object v0, p0, Ljqu;->bmN:Ljava/lang/String;

    return-object v0
.end method

.method public final TW()Z
    .locals 1

    .prologue
    .line 7169
    iget v0, p0, Ljqu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic a(Ljsi;)Ljsr;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 7068
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljqu;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljqu;->euN:I

    iget v0, p0, Ljqu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqu;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljqu;->ezw:I

    iget v0, p0, Ljqu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqu;->aez:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljqu;->ezx:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljqu;->ezx:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljqu;->ezx:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Ljqu;->ezx:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    invoke-virtual {p1, v0}, Ljsi;->rV(I)I

    move-result v3

    invoke-virtual {p1}, Ljsi;->getPosition()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Ljsi;->btV()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Ljsi;->btQ()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Ljsi;->rX(I)V

    iget-object v2, p0, Ljqu;->ezx:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Ljqu;->ezx:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Ljqu;->ezx:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Ljqu;->ezx:[I

    invoke-virtual {p1, v3}, Ljsi;->rW(I)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Ljqu;->ezy:Ljra;

    if-nez v0, :cond_8

    new-instance v0, Ljra;

    invoke-direct {v0}, Ljra;-><init>()V

    iput-object v0, p0, Ljqu;->ezy:Ljra;

    :cond_8
    iget-object v0, p0, Ljqu;->ezy:Ljra;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljqu;->bmN:Ljava/lang/String;

    iget v0, p0, Ljqu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqu;->aez:I

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Ljqu;->ezz:Ljqm;

    if-nez v0, :cond_9

    new-instance v0, Ljqm;

    invoke-direct {v0}, Ljqm;-><init>()V

    iput-object v0, p0, Ljqu;->ezz:Ljqm;

    :cond_9
    iget-object v0, p0, Ljqu;->ezz:Ljqm;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 3

    .prologue
    .line 7206
    iget v0, p0, Ljqu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 7207
    const/4 v0, 0x1

    iget v1, p0, Ljqu;->euN:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 7209
    :cond_0
    iget v0, p0, Ljqu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 7210
    const/4 v0, 0x2

    iget v1, p0, Ljqu;->ezw:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 7212
    :cond_1
    iget-object v0, p0, Ljqu;->ezx:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljqu;->ezx:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 7213
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljqu;->ezx:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 7214
    const/4 v1, 0x3

    iget-object v2, p0, Ljqu;->ezx:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Ljsj;->bq(II)V

    .line 7213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7217
    :cond_2
    iget-object v0, p0, Ljqu;->ezy:Ljra;

    if-eqz v0, :cond_3

    .line 7218
    const/4 v0, 0x4

    iget-object v1, p0, Ljqu;->ezy:Ljra;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 7220
    :cond_3
    iget v0, p0, Ljqu;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    .line 7221
    const/4 v0, 0x5

    iget-object v1, p0, Ljqu;->bmN:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 7223
    :cond_4
    iget-object v0, p0, Ljqu;->ezz:Ljqm;

    if-eqz v0, :cond_5

    .line 7224
    const/4 v0, 0x6

    iget-object v1, p0, Ljqu;->ezz:Ljqm;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 7226
    :cond_5
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 7227
    return-void
.end method

.method public final bqC()I
    .locals 1

    .prologue
    .line 7120
    iget v0, p0, Ljqu;->euN:I

    return v0
.end method

.method public final bqD()Z
    .locals 1

    .prologue
    .line 7128
    iget v0, p0, Ljqu;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bsE()I
    .locals 1

    .prologue
    .line 7139
    iget v0, p0, Ljqu;->ezw:I

    return v0
.end method

.method public final bsF()Z
    .locals 1

    .prologue
    .line 7147
    iget v0, p0, Ljqu;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7231
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 7232
    iget v2, p0, Ljqu;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 7233
    const/4 v2, 0x1

    iget v3, p0, Ljqu;->euN:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7236
    :cond_0
    iget v2, p0, Ljqu;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1

    .line 7237
    const/4 v2, 0x2

    iget v3, p0, Ljqu;->ezw:I

    invoke-static {v2, v3}, Ljsj;->bv(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7240
    :cond_1
    iget-object v2, p0, Ljqu;->ezx:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljqu;->ezx:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 7242
    :goto_0
    iget-object v3, p0, Ljqu;->ezx:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 7243
    iget-object v3, p0, Ljqu;->ezx:[I

    aget v3, v3, v1

    .line 7244
    invoke-static {v3}, Ljsj;->sa(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 7242
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7247
    :cond_2
    add-int/2addr v0, v2

    .line 7248
    iget-object v1, p0, Ljqu;->ezx:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7250
    :cond_3
    iget-object v1, p0, Ljqu;->ezy:Ljra;

    if-eqz v1, :cond_4

    .line 7251
    const/4 v1, 0x4

    iget-object v2, p0, Ljqu;->ezy:Ljra;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7254
    :cond_4
    iget v1, p0, Ljqu;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_5

    .line 7255
    const/4 v1, 0x5

    iget-object v2, p0, Ljqu;->bmN:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7258
    :cond_5
    iget-object v1, p0, Ljqu;->ezz:Ljqm;

    if-eqz v1, :cond_6

    .line 7259
    const/4 v1, 0x6

    iget-object v2, p0, Ljqu;->ezz:Ljqm;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7262
    :cond_6
    return v0
.end method

.method public final rB(I)Ljqu;
    .locals 1

    .prologue
    .line 7123
    iput p1, p0, Ljqu;->euN:I

    .line 7124
    iget v0, p0, Ljqu;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljqu;->aez:I

    .line 7125
    return-object p0
.end method

.method public final rC(I)Ljqu;
    .locals 1

    .prologue
    .line 7142
    iput p1, p0, Ljqu;->ezw:I

    .line 7143
    iget v0, p0, Ljqu;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljqu;->aez:I

    .line 7144
    return-object p0
.end method

.method public final yk(Ljava/lang/String;)Ljqu;
    .locals 1

    .prologue
    .line 7161
    if-nez p1, :cond_0

    .line 7162
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7164
    :cond_0
    iput-object p1, p0, Ljqu;->bmN:Ljava/lang/String;

    .line 7165
    iget v0, p0, Ljqu;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljqu;->aez:I

    .line 7166
    return-object p0
.end method
