.class final Lilr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private synthetic dDS:Lilf;

.field private dDZ:Ljava/util/Set;

.field private dEa:Lils;

.field private dEb:Lils;


# direct methods
.method private constructor <init>(Lilf;)V
    .locals 1

    .prologue
    .line 364
    iput-object p1, p0, Lilr;->dDS:Lilf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    iget-object v0, p0, Lilr;->dDS:Lilf;

    invoke-virtual {v0}, Lilf;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Liqs;->mm(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lilr;->dDZ:Ljava/util/Set;

    .line 366
    iget-object v0, p0, Lilr;->dDS:Lilf;

    invoke-static {v0}, Lilf;->a(Lilf;)Lils;

    move-result-object v0

    iput-object v0, p0, Lilr;->dEa:Lils;

    return-void
.end method

.method synthetic constructor <init>(Lilf;B)V
    .locals 0

    .prologue
    .line 364
    invoke-direct {p0, p1}, Lilr;-><init>(Lilf;)V

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lilr;->dEa:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lilr;->dEa:Lils;

    invoke-static {v0}, Lilf;->bA(Ljava/lang/Object;)V

    .line 376
    iget-object v0, p0, Lilr;->dEa:Lils;

    iput-object v0, p0, Lilr;->dEb:Lils;

    .line 377
    iget-object v0, p0, Lilr;->dDZ:Ljava/util/Set;

    iget-object v1, p0, Lilr;->dEb:Lils;

    iget-object v1, v1, Lils;->dCo:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 379
    :cond_0
    iget-object v0, p0, Lilr;->dEa:Lils;

    iget-object v0, v0, Lils;->dEa:Lils;

    iput-object v0, p0, Lilr;->dEa:Lils;

    .line 380
    iget-object v0, p0, Lilr;->dEa:Lils;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lilr;->dDZ:Ljava/util/Set;

    iget-object v1, p0, Lilr;->dEa:Lils;

    iget-object v1, v1, Lils;->dCo:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 381
    :cond_1
    iget-object v0, p0, Lilr;->dEb:Lils;

    iget-object v0, v0, Lils;->dCo:Ljava/lang/Object;

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lilr;->dEb:Lils;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lifv;->gY(Z)V

    .line 386
    iget-object v0, p0, Lilr;->dDS:Lilf;

    iget-object v1, p0, Lilr;->dEb:Lils;

    iget-object v1, v1, Lils;->dCo:Ljava/lang/Object;

    invoke-static {v0, v1}, Lilf;->a(Lilf;Ljava/lang/Object;)V

    .line 387
    const/4 v0, 0x0

    iput-object v0, p0, Lilr;->dEb:Lils;

    .line 388
    return-void

    .line 385
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
