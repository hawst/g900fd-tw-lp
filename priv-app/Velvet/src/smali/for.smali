.class public interface abstract Lfor;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract K(Landroid/net/Uri;)V
.end method

.method public abstract Kz()Z
.end method

.method public abstract P(Landroid/os/Bundle;)V
.end method

.method public abstract U(Ljava/util/List;)Ljava/util/List;
.end method

.method public abstract V(Ljava/util/List;)V
.end method

.method public abstract a(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)Landroid/graphics/Bitmap;
.end method

.method public abstract a(Lcom/google/android/sidekick/shared/remoteapi/LoggingRequest;)V
.end method

.method public abstract a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;I)V
.end method

.method public abstract a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;JIZ)V
.end method

.method public abstract a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
.end method

.method public abstract a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
.end method

.method public abstract a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V
.end method

.method public abstract a(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Z
.end method

.method public abstract awf()V
.end method

.method public abstract ayp()V
.end method

.method public abstract ayr()Landroid/graphics/Bitmap;
.end method

.method public abstract azF()V
.end method

.method public abstract azG()Lcom/google/android/sidekick/shared/remoteapi/CardsResponse;
.end method

.method public abstract azH()V
.end method

.method public abstract azI()V
.end method

.method public abstract azJ()V
.end method

.method public abstract azK()V
.end method

.method public abstract azL()V
.end method

.method public abstract azM()Z
.end method

.method public abstract azN()Landroid/os/Bundle;
.end method

.method public abstract azO()V
.end method

.method public abstract azP()Landroid/accounts/Account;
.end method

.method public abstract b(Ljava/util/List;I)Landroid/content/Intent;
.end method

.method public abstract b(Landroid/location/Location;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)Landroid/graphics/Bitmap;
.end method

.method public abstract b(Lcom/google/android/sidekick/shared/remoteapi/StaticMapOptions;)Landroid/graphics/Bitmap;
.end method

.method public abstract b(JLjava/lang/String;JJLjava/lang/String;)Landroid/net/Uri;
.end method

.method public abstract b(JZ)V
.end method

.method public abstract b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
.end method

.method public abstract b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;I)V
.end method

.method public abstract b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
.end method

.method public abstract b(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V
.end method

.method public abstract bl(J)V
.end method

.method public abstract c(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)V
.end method

.method public abstract c(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;Z)V
.end method

.method public abstract d(Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;
.end method

.method public abstract e(Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
.end method

.method public abstract e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract fl(Z)V
.end method

.method public abstract getVersion()Ljava/lang/String;
.end method

.method public abstract iH(I)Z
.end method

.method public abstract iI(I)V
.end method

.method public abstract iJ(I)V
.end method

.method public abstract iK(I)V
.end method

.method public abstract iL(I)Lcom/google/android/sidekick/shared/remoteapi/ProtoParcelable;
.end method

.method public abstract lL(Ljava/lang/String;)Z
.end method

.method public abstract lM(Ljava/lang/String;)Z
.end method

.method public abstract lN(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract wt()V
.end method
