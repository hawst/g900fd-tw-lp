.class public final Lahd;
.super Ljsr;
.source "PG"


# static fields
.field private static volatile Yd:[Lahd;


# instance fields
.field public XM:J

.field public id:J

.field public name:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 43
    invoke-direct {p0}, Ljsr;-><init>()V

    .line 44
    const/4 v0, 0x1

    iput v0, p0, Lahd;->type:I

    const-string v0, ""

    iput-object v0, p0, Lahd;->name:Ljava/lang/String;

    iput-wide v2, p0, Lahd;->id:J

    iput-wide v2, p0, Lahd;->XM:J

    const/4 v0, -0x1

    iput v0, p0, Lahd;->eCz:I

    .line 45
    return-void
.end method

.method public static c([B)Lahd;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lahd;

    invoke-direct {v0}, Lahd;-><init>()V

    invoke-static {v0, p0}, Ljsr;->c(Ljsr;[B)Ljsr;

    move-result-object v0

    check-cast v0, Lahd;

    return-object v0
.end method

.method public static lH()[Lahd;
    .locals 2

    .prologue
    .line 20
    sget-object v0, Lahd;->Yd:[Lahd;

    if-nez v0, :cond_1

    .line 21
    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    .line 23
    :try_start_0
    sget-object v0, Lahd;->Yd:[Lahd;

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x0

    new-array v0, v0, [Lahd;

    sput-object v0, Lahd;->Yd:[Lahd;

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :cond_1
    sget-object v0, Lahd;->Yd:[Lahd;

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Ljsu;->b(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lahd;->type:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lahd;->name:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lahd;->id:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Lahd;->XM:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 59
    const/4 v0, 0x1

    iget v1, p0, Lahd;->type:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 60
    iget-object v0, p0, Lahd;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x2

    iget-object v1, p0, Lahd;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 63
    :cond_0
    iget-wide v0, p0, Lahd;->id:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 64
    const/4 v0, 0x3

    iget-wide v2, p0, Lahd;->id:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 66
    :cond_1
    iget-wide v0, p0, Lahd;->XM:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 67
    const/4 v0, 0x4

    iget-wide v2, p0, Lahd;->XM:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 69
    :cond_2
    invoke-super {p0, p1}, Ljsr;->a(Ljsj;)V

    .line 70
    return-void
.end method

.method protected final lF()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 74
    invoke-super {p0}, Ljsr;->lF()I

    move-result v0

    .line 75
    const/4 v1, 0x1

    iget v2, p0, Lahd;->type:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    iget-object v1, p0, Lahd;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    const/4 v1, 0x2

    iget-object v2, p0, Lahd;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_0
    iget-wide v2, p0, Lahd;->id:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 82
    const/4 v1, 0x3

    iget-wide v2, p0, Lahd;->id:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_1
    iget-wide v2, p0, Lahd;->XM:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 86
    const/4 v1, 0x4

    iget-wide v2, p0, Lahd;->XM:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_2
    return v0
.end method
