.class public final Lbce;
.super Ljava/lang/Object;


# instance fields
.field private avz:I

.field public awb:Z

.field private awc:Ljava/util/List;

.field public awd:Z

.field private awe:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lbce;->avz:I

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/appdatasearch/Section;)Lbce;
    .locals 2

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/Section;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/Section;->eR(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbce;->awc:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbce;->awe:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbce;->awc:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lbce;->awc:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object p0

    :cond_1
    iget-boolean v0, p0, Lbce;->awe:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot mix literal and semantic sections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lbce;->awc:Ljava/util/List;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbce;->awe:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbce;->awc:Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lbce;->awc:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lbce;->awe:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot mix literal and semantic sections"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final xd()Lcom/google/android/gms/appdatasearch/QuerySpecification;
    .locals 10

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;

    const/4 v1, 0x2

    iget-boolean v2, p0, Lbce;->awb:Z

    const/4 v3, 0x0

    iget-object v4, p0, Lbce;->awc:Ljava/util/List;

    iget-boolean v5, p0, Lbce;->awd:Z

    iget-boolean v8, p0, Lbce;->awe:Z

    move v7, v6

    move v9, v6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/QuerySpecification;-><init>(IZLjava/util/List;Ljava/util/List;ZIIZI)V

    return-object v0
.end method
