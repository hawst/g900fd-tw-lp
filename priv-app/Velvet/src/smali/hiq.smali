.class final Lhiq;
.super Lepm;
.source "PG"


# instance fields
.field private synthetic dik:Lhip;


# direct methods
.method varargs constructor <init>(Lhip;Ljava/lang/String;[I)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lhiq;->dik:Lhip;

    invoke-direct {p0, p2, p3}, Lepm;-><init>(Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 65
    iget-object v1, p0, Lhiq;->dik:Lhip;

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, v1, Lhip;->dgx:Landroid/media/MediaPlayer;

    iget-object v0, v1, Lhip;->dgx:Landroid/media/MediaPlayer;

    iget-object v2, v1, Lhip;->mAudioRouter:Lhhu;

    invoke-interface {v2}, Lhhu;->aPq()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    :try_start_0
    iget-object v0, v1, Lhip;->dgx:Landroid/media/MediaPlayer;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "data:;base64,"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v1, Lhip;->dig:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v0, v1, Lhip;->dgx:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, v1, Lhip;->mAudioRouter:Lhhu;

    invoke-interface {v0}, Lhhu;->aPo()V

    iget-object v0, v1, Lhip;->dgx:Landroid/media/MediaPlayer;

    new-instance v2, Lhir;

    invoke-direct {v2, v1}, Lhir;-><init>(Lhip;)V

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, v1, Lhip;->dgx:Landroid/media/MediaPlayer;

    new-instance v2, Lhis;

    invoke-direct {v2, v1}, Lhis;-><init>(Lhip;)V

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, v1, Lhip;->dih:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    iget-boolean v0, v1, Lhip;->bf:Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lhip;->finish()V

    iget-object v0, v1, Lhip;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lhit;

    const-string v3, "Playback complete callback"

    invoke-direct {v2, v1, v3}, Lhit;-><init>(Lhip;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "VS.ByteArrayPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "I/O Exception initializing media player :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lhip;->finish()V

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v0, v1, Lhip;->dgx:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, v1, Lhip;->dii:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v1}, Lhip;->finish()V

    iget-object v0, v1, Lhip;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lhit;

    const-string v3, "Playback complete callback"

    invoke-direct {v2, v1, v3}, Lhit;-><init>(Lhip;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v1}, Lhip;->finish()V

    iget-object v0, v1, Lhip;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v2, Lhit;

    const-string v3, "Playback complete callback"

    invoke-direct {v2, v1, v3}, Lhit;-><init>(Lhip;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lhip;->finish()V

    iget-object v2, v1, Lhip;->mUiExecutor:Ljava/util/concurrent/Executor;

    new-instance v3, Lhit;

    const-string v4, "Playback complete callback"

    invoke-direct {v3, v1, v4}, Lhit;-><init>(Lhip;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    throw v0
.end method
