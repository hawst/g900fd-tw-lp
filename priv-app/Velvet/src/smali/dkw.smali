.class public final Ldkw;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljlj;Libv;Lenm;I)Lcom/google/android/search/shared/actions/HelpAction;
    .locals 10

    .prologue
    .line 99
    iget-object v0, p0, Ljlj;->erF:[Ljlk;

    array-length v0, v0

    invoke-static {v0}, Lilw;->mf(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 100
    invoke-static {}, Lior;->aXZ()Ljava/util/HashMap;

    move-result-object v3

    .line 101
    iget-object v0, p0, Ljlj;->erE:Ljmu;

    invoke-virtual {v0}, Ljmu;->getText()Ljava/lang/String;

    move-result-object v4

    .line 103
    iget-object v5, p0, Ljlj;->erF:[Ljlk;

    array-length v6, v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    .line 109
    invoke-static {v7, p2, p3}, Ldkw;->a(Ljlk;Lenm;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    invoke-static {v7}, Lcom/google/android/search/shared/actions/HelpAction;->a(Ljlk;)I

    move-result v8

    .line 114
    if-eqz v8, :cond_1

    .line 116
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 117
    if-nez v0, :cond_0

    .line 119
    invoke-static {v8}, Ldkw;->fW(I)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Libv;->U(Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    .line 122
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 123
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v3, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :cond_1
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 134
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 135
    invoke-static {v2}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 138
    new-instance v0, Lcom/google/android/search/shared/actions/HelpAction;

    invoke-direct {v0, v4, v2, v3}, Lcom/google/android/search/shared/actions/HelpAction;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)V

    .line 141
    :goto_1
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljli;Libv;Lenm;ILjava/util/List;)V
    .locals 4

    .prologue
    .line 168
    iget-object v1, p0, Ljli;->erA:[Ljlj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 169
    invoke-static {v3, p1, p2, p3}, Ldkw;->a(Ljlj;Libv;Lenm;I)Lcom/google/android/search/shared/actions/HelpAction;

    move-result-object v3

    .line 171
    if-eqz v3, :cond_0

    .line 172
    invoke-interface {p4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175
    :cond_1
    return-void
.end method

.method public static a(Ljli;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 159
    new-instance v0, Lcom/google/android/search/shared/actions/HelpAction;

    iget-object v1, p0, Ljli;->ery:Ljmu;

    invoke-virtual {v1}, Ljmu;->getText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ljli;->erz:Ljmu;

    invoke-virtual {v2}, Ljmu;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/search/shared/actions/HelpAction;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    return-void
.end method

.method public static a(Ljlk;Lenm;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 46
    invoke-virtual {p0}, Ljlk;->bhT()Z

    move-result v2

    if-nez v2, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    .line 51
    :cond_1
    invoke-virtual {p0}, Ljlk;->boU()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Ljlk;->boT()I

    move-result v2

    if-lt p2, v2, :cond_0

    :cond_2
    invoke-virtual {p0}, Ljlk;->boW()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Ljlk;->boV()I

    move-result v2

    if-ge p2, v2, :cond_0

    .line 58
    :cond_3
    iget-object v3, p0, Ljlk;->eoQ:[I

    array-length v4, v3

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_6

    aget v5, v3, v2

    .line 59
    if-nez v5, :cond_5

    .line 60
    invoke-interface {p1}, Lenm;->auH()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 58
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 63
    :cond_5
    if-ne v5, v1, :cond_0

    .line 64
    invoke-interface {p1}, Lenm;->auI()Z

    move-result v5

    if-nez v5, :cond_4

    goto :goto_0

    :cond_6
    move v0, v1

    .line 73
    goto :goto_0
.end method

.method public static fW(I)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 82
    packed-switch p0, :pswitch_data_0

    .line 88
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown HelpAction.CONTACT_SUBSTITUTION value"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 84
    :pswitch_0
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 86
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
