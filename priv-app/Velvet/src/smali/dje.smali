.class public final Ldje;
.super Ldhn;
.source "PG"


# instance fields
.field private final buj:Lepo;

.field final bzI:Ldir;

.field private final bzJ:Ldgd;

.field private bzK:Ldiu;

.field bzL:Z

.field bzM:Z

.field private final mContext:Landroid/content/Context;

.field private final mGsaConfig:Lchk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldfz;Lcjs;Lchk;Lepo;Leqo;Ldir;Ldhu;Ldgd;Lddv;)V
    .locals 9

    .prologue
    .line 54
    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    invoke-direct/range {v1 .. v8}, Ldhn;-><init>(Lcjs;Lchk;Ldfz;Lepo;Leqo;Ldhu;Lddv;)V

    .line 56
    iput-object p1, p0, Ldje;->mContext:Landroid/content/Context;

    .line 57
    iput-object p4, p0, Ldje;->mGsaConfig:Lchk;

    .line 58
    iput-object p5, p0, Ldje;->buj:Lepo;

    .line 59
    move-object/from16 v0, p7

    iput-object v0, p0, Ldje;->bzI:Ldir;

    .line 60
    move-object/from16 v0, p9

    iput-object v0, p0, Ldje;->bzJ:Ldgd;

    .line 61
    return-void
.end method

.method static synthetic a(Ldje;)Leqo;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ldje;->bwW:Leqo;

    return-object v0
.end method

.method static synthetic b(Ldje;)Leqo;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ldje;->bwW:Leqo;

    return-object v0
.end method

.method static synthetic c(Ldje;)Leqo;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ldje;->bwW:Leqo;

    return-object v0
.end method

.method static synthetic d(Ldje;)Leqo;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ldje;->bwW:Leqo;

    return-object v0
.end method

.method static synthetic e(Ldje;)Leqo;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ldje;->bwW:Leqo;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/shared/search/Query;Ldgk;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Ldje;->bzJ:Ldgd;

    invoke-virtual {v1}, Ldgd;->abR()Ljava/util/List;

    move-result-object v1

    .line 66
    iget-object v2, p0, Ldje;->bzI:Ldir;

    invoke-virtual {v2, p1, v1}, Ldir;->a(Lcom/google/android/shared/search/Query;Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 68
    iput-boolean v0, p0, Ldje;->bzL:Z

    .line 69
    iget-object v1, p0, Ldje;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v1}, Ldfz;->abD()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Ldje;->bzM:Z

    .line 72
    new-instance v0, Ldiu;

    iget-object v1, p0, Ldje;->mContext:Landroid/content/Context;

    iget-object v2, p0, Ldje;->buj:Lepo;

    iget-object v3, p0, Ldje;->bwW:Leqo;

    iget-object v4, p0, Ldje;->mGsaConfig:Lchk;

    invoke-virtual {v4}, Lchk;->Iq()I

    move-result v4

    iget-object v5, p0, Ldje;->mGsaConfig:Lchk;

    invoke-virtual {v5}, Lchk;->Is()I

    move-result v5

    iget-object v6, p0, Ldje;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v6}, Ldfz;->abB()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Ldiu;-><init>(Landroid/content/Context;Lepo;Leqo;IIZ)V

    iput-object v0, p0, Ldje;->bzK:Ldiu;

    .line 79
    new-instance v0, Ldjf;

    iget-object v2, p0, Ldje;->bzK:Ldiu;

    iget-object v1, p0, Ldje;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->Is()I

    move-result v1

    int-to-long v4, v1

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Ldjf;-><init>(Ldje;Ldiu;Ldgk;J)V

    .line 85
    iget-object v1, p0, Ldje;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v1}, Ldfz;->abB()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldjf;->dw(Z)Ldgk;

    move-result-object v1

    invoke-super {p0, p1, v1}, Ldhn;->a(Lcom/google/android/shared/search/Query;Ldgk;)V

    .line 88
    iget-boolean v1, p0, Ldje;->bzM:Z

    if-nez v1, :cond_3

    .line 89
    iget-object v1, p0, Ldje;->mGsaConfig:Lchk;

    invoke-virtual {v1}, Lchk;->Ir()I

    move-result v5

    .line 90
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldiq;

    .line 91
    iget-object v2, p0, Ldje;->bzK:Ldiu;

    invoke-static {v3}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, v2, Ldiu;->bzd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v2, Ldiu;->bzd:I

    new-instance v6, Landroid/os/CancellationSignal;

    invoke-direct {v6}, Landroid/os/CancellationSignal;-><init>()V

    iget-object v1, v2, Ldiu;->byY:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v9, v2, Ldiu;->byZ:Ldig;

    new-instance v1, Ldiy;

    move-object v4, p1

    move-object v7, v0

    invoke-direct/range {v1 .. v7}, Ldiy;-><init>(Ldiu;Ldiq;Lcom/google/android/shared/search/Query;ILandroid/os/CancellationSignal;Lemy;)V

    invoke-virtual {v9, v1}, Ldig;->a(Lepn;)V

    goto :goto_0

    .line 97
    :cond_2
    iget-object v0, p0, Ldje;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abC()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 98
    iget-object v0, p0, Ldje;->bzK:Ldiu;

    invoke-virtual {v0}, Ldiu;->start()V

    .line 101
    :cond_3
    return-void
.end method

.method public final abT()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Ldje;->bzK:Ldiu;

    if-eqz v0, :cond_1

    .line 109
    iget-object v0, p0, Ldje;->bzK:Ldiu;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldiu;->OF:Z

    iget-object v1, v0, Ldiu;->byZ:Ldig;

    invoke-virtual {v1}, Ldig;->ace()V

    iget-object v0, v0, Ldiu;->byY:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    goto :goto_0

    .line 110
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldje;->bzK:Ldiu;

    .line 112
    :cond_1
    return-void
.end method
