.class public final Ldfi;
.super Lepm;
.source "PG"


# instance fields
.field private final bwa:Ljava/lang/String;

.field private final bwb:[Ljava/lang/String;

.field private final bwc:Z

.field private final bwd:Z

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/pm/PackageManager;Ljava/lang/String;[Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 117
    const-string v0, "Initialize Icing"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, v1}, Lepm;-><init>(Ljava/lang/String;[I)V

    .line 118
    iput-object p1, p0, Ldfi;->mContentResolver:Landroid/content/ContentResolver;

    .line 119
    iput-object p2, p0, Ldfi;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 120
    iput-object p3, p0, Ldfi;->bwa:Ljava/lang/String;

    .line 121
    iput-object p4, p0, Ldfi;->bwb:[Ljava/lang/String;

    .line 122
    iput-boolean p5, p0, Ldfi;->bwc:Z

    .line 123
    iput-boolean p6, p0, Ldfi;->bwd:Z

    .line 124
    return-void

    .line 117
    :array_0
    .array-data 4
        0x2
        0x1
    .end array-data
.end method


# virtual methods
.method public final abs()Ldfh;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 142
    iget-object v0, p0, Ldfi;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjG:Landroid/net/Uri;

    iget-object v4, p0, Ldfi;->bwb:[Ljava/lang/String;

    move-object v3, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 146
    if-eqz v1, :cond_1

    .line 147
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 149
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 150
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 151
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 153
    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 159
    new-instance v2, Ldfh;

    iget-boolean v1, p0, Ldfi;->bwc:Z

    invoke-direct {v2, v0, v1}, Ldfh;-><init>(Landroid/os/Bundle;Z)V

    .line 162
    :cond_1
    return-object v2
.end method

.method public final run()V
    .locals 12

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v5, -0x1

    .line 128
    iget-object v0, p0, Ldfi;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v6, p0, Ldfi;->bwa:Ljava/lang/String;

    iget-boolean v7, p0, Ldfi;->bwd:Z

    invoke-static {v0, v6, v7}, Lcom/google/android/search/core/icingsync/ApplicationLaunchReceiver;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Z)V

    .line 131
    invoke-virtual {p0}, Ldfi;->abs()Ldfh;

    move-result-object v7

    .line 133
    if-eqz v7, :cond_9

    .line 134
    new-instance v8, Litw;

    invoke-direct {v8}, Litw;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v7, Ldfh;->bvY:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v6, v7, Ldfh;->bvY:Landroid/os/Bundle;

    invoke-virtual {v6, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    sget-object v6, Lcum;->bjr:Lcum;

    invoke-virtual {v6}, Lcum;->xF()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v6, v1

    :goto_1
    if-nez v11, :cond_3

    move v0, v1

    :goto_2
    if-eq v6, v5, :cond_0

    if-eq v0, v5, :cond_0

    new-instance v11, Litx;

    invoke-direct {v11}, Litx;-><init>()V

    invoke-virtual {v11, v6}, Litx;->mO(I)Litx;

    invoke-virtual {v11, v0}, Litx;->mP(I)Litx;

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    sget-object v6, Lcum;->bjs:Lcum;

    invoke-virtual {v6}, Lcum;->xF()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v6, v2

    goto :goto_1

    :cond_2
    move v6, v5

    goto :goto_1

    :cond_3
    if-ne v11, v2, :cond_4

    move v0, v2

    goto :goto_2

    :cond_4
    if-ne v11, v3, :cond_5

    move v0, v3

    goto :goto_2

    :cond_5
    if-ne v11, v4, :cond_6

    move v0, v4

    goto :goto_2

    :cond_6
    const/4 v0, 0x4

    if-ne v11, v0, :cond_7

    const/4 v0, 0x4

    goto :goto_2

    :cond_7
    move v0, v5

    goto :goto_2

    :cond_8
    const-class v0, Litx;

    invoke-static {v9, v0}, Likm;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Litx;

    iput-object v0, v8, Litw;->dJn:[Litx;

    iget-boolean v0, v7, Ldfh;->bvZ:Z

    invoke-virtual {v8, v0}, Litw;->hd(Z)Litw;

    const/16 v0, 0x79

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    iput-object v8, v0, Litu;->dIL:Litw;

    invoke-static {v0}, Lege;->a(Litu;)V

    .line 137
    :cond_9
    iget-object v0, p0, Ldfi;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/search/core/icingsync/InternalIcingCorporaProvider;->bjE:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Ldfi;->bwb:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 138
    return-void
.end method
