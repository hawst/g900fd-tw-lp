.class public final Lkaa;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eNH:I

.field public eNm:Ljzk;

.field private eOK:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2893
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 2894
    iput v0, p0, Lkaa;->aez:I

    iput-object v1, p0, Lkaa;->eNm:Ljzk;

    iput v0, p0, Lkaa;->eOK:I

    iput v0, p0, Lkaa;->eNH:I

    iput-object v1, p0, Lkaa;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Lkaa;->eCz:I

    .line 2895
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 2833
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lkaa;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lkaa;->eNm:Ljzk;

    if-nez v0, :cond_1

    new-instance v0, Ljzk;

    invoke-direct {v0}, Ljzk;-><init>()V

    iput-object v0, p0, Lkaa;->eNm:Ljzk;

    :cond_1
    iget-object v0, p0, Lkaa;->eNm:Ljzk;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkaa;->eOK:I

    iget v0, p0, Lkaa;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkaa;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Lkaa;->eNH:I

    iget v0, p0, Lkaa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkaa;->aez:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 2910
    iget-object v0, p0, Lkaa;->eNm:Ljzk;

    if-eqz v0, :cond_0

    .line 2911
    const/4 v0, 0x1

    iget-object v1, p0, Lkaa;->eNm:Ljzk;

    invoke-virtual {p1, v0, v1}, Ljsj;->a(ILjsr;)V

    .line 2913
    :cond_0
    iget v0, p0, Lkaa;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 2914
    const/4 v0, 0x2

    iget v1, p0, Lkaa;->eOK:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2916
    :cond_1
    iget v0, p0, Lkaa;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 2917
    const/4 v0, 0x3

    iget v1, p0, Lkaa;->eNH:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 2919
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 2920
    return-void
.end method

.method public final bwW()I
    .locals 1

    .prologue
    .line 2877
    iget v0, p0, Lkaa;->eNH:I

    return v0
.end method

.method public final bxB()I
    .locals 1

    .prologue
    .line 2858
    iget v0, p0, Lkaa;->eOK:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 2924
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 2925
    iget-object v1, p0, Lkaa;->eNm:Ljzk;

    if-eqz v1, :cond_0

    .line 2926
    const/4 v1, 0x1

    iget-object v2, p0, Lkaa;->eNm:Ljzk;

    invoke-static {v1, v2}, Ljsj;->c(ILjsr;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2929
    :cond_0
    iget v1, p0, Lkaa;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 2930
    const/4 v1, 0x2

    iget v2, p0, Lkaa;->eOK:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2933
    :cond_1
    iget v1, p0, Lkaa;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    .line 2934
    const/4 v1, 0x3

    iget v2, p0, Lkaa;->eNH:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2937
    :cond_2
    return v0
.end method

.method public final tD(I)Lkaa;
    .locals 1

    .prologue
    .line 2880
    const/16 v0, 0x1c8

    iput v0, p0, Lkaa;->eNH:I

    .line 2881
    iget v0, p0, Lkaa;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lkaa;->aez:I

    .line 2882
    return-object p0
.end method
