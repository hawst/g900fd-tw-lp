.class public final Ljjo;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private ajm:Ljava/lang/String;

.field private dON:Ljava/lang/String;

.field private eoX:J

.field private eoZ:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 112
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 113
    const/4 v0, 0x0

    iput v0, p0, Ljjo;->aez:I

    const-string v0, ""

    iput-object v0, p0, Ljjo;->ajm:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljjo;->eoX:J

    const-string v0, ""

    iput-object v0, p0, Ljjo;->dON:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljjo;->eoZ:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljjo;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljjo;->eCz:I

    .line 114
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljjo;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjo;->ajm:Ljava/lang/String;

    iget v0, p0, Ljjo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjo;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v0

    iput-wide v0, p0, Ljjo;->eoX:J

    iget v0, p0, Ljjo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjo;->aez:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjo;->dON:Ljava/lang/String;

    iget v0, p0, Ljjo;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljjo;->aez:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjo;->eoZ:Ljava/lang/String;

    iget v0, p0, Ljjo;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljjo;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 130
    iget v0, p0, Ljjo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x1

    iget-object v1, p0, Ljjo;->ajm:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 133
    :cond_0
    iget v0, p0, Ljjo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 134
    const/4 v0, 0x2

    iget-wide v2, p0, Ljjo;->eoX:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 136
    :cond_1
    iget v0, p0, Ljjo;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 137
    const/4 v0, 0x3

    iget-object v1, p0, Ljjo;->dON:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 139
    :cond_2
    iget v0, p0, Ljjo;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    .line 140
    const/4 v0, 0x4

    iget-object v1, p0, Ljjo;->eoZ:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljsj;->p(ILjava/lang/String;)V

    .line 142
    :cond_3
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 143
    return-void
.end method

.method public final bnU()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Ljjo;->eoX:J

    return-wide v0
.end method

.method public final bnW()Z
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Ljjo;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bnX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Ljjo;->eoZ:Ljava/lang/String;

    return-object v0
.end method

.method public final dv(J)Ljjo;
    .locals 1

    .prologue
    .line 55
    iput-wide p1, p0, Ljjo;->eoX:J

    .line 56
    iget v0, p0, Ljjo;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljjo;->aez:I

    .line 57
    return-object p0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Ljjo;->ajm:Ljava/lang/String;

    return-object v0
.end method

.method public final getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Ljjo;->dON:Ljava/lang/String;

    return-object v0
.end method

.method protected final lF()I
    .locals 4

    .prologue
    .line 147
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 148
    iget v1, p0, Ljjo;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 149
    const/4 v1, 0x1

    iget-object v2, p0, Ljjo;->ajm:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_0
    iget v1, p0, Ljjo;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 153
    const/4 v1, 0x2

    iget-wide v2, p0, Ljjo;->eoX:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_1
    iget v1, p0, Ljjo;->aez:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 157
    const/4 v1, 0x3

    iget-object v2, p0, Ljjo;->dON:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_2
    iget v1, p0, Ljjo;->aez:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 161
    const/4 v1, 0x4

    iget-object v2, p0, Ljjo;->eoZ:Ljava/lang/String;

    invoke-static {v1, v2}, Ljsj;->q(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_3
    return v0
.end method

.method public final qA()Z
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Ljjo;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final wl(Ljava/lang/String;)Ljjo;
    .locals 1

    .prologue
    .line 33
    if-nez p1, :cond_0

    .line 34
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36
    :cond_0
    iput-object p1, p0, Ljjo;->ajm:Ljava/lang/String;

    .line 37
    iget v0, p0, Ljjo;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljjo;->aez:I

    .line 38
    return-object p0
.end method

.method public final wm(Ljava/lang/String;)Ljjo;
    .locals 1

    .prologue
    .line 96
    if-nez p1, :cond_0

    .line 97
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 99
    :cond_0
    iput-object p1, p0, Ljjo;->eoZ:Ljava/lang/String;

    .line 100
    iget v0, p0, Ljjo;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljjo;->aez:I

    .line 101
    return-object p0
.end method
