.class public final Ljre;
.super Ljsl;
.source "PG"


# static fields
.field public static final eAh:Ljsm;


# instance fields
.field private aez:I

.field private eAi:I

.field private eAj:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 5987
    const/16 v0, 0xb

    const-class v1, Ljre;

    const v2, 0x212e3182

    invoke-static {v0, v1, v2}, Ljsm;->a(ILjava/lang/Class;I)Ljsm;

    move-result-object v0

    sput-object v0, Ljre;->eAh:Ljsm;

    .line 5998
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6043
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 6044
    iput v0, p0, Ljre;->aez:I

    iput v0, p0, Ljre;->eAi:I

    iput v0, p0, Ljre;->eAj:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljre;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljre;->eCz:I

    .line 6045
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 5980
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljre;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    iput v0, p0, Ljre;->eAi:I

    iget v0, p0, Ljre;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljre;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljre;->eAj:I

    iget v0, p0, Ljre;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljre;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 6059
    iget v0, p0, Ljre;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 6060
    const/4 v0, 0x1

    iget v1, p0, Ljre;->eAi:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6062
    :cond_0
    iget v0, p0, Ljre;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 6063
    const/4 v0, 0x2

    iget v1, p0, Ljre;->eAj:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 6065
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 6066
    return-void
.end method

.method public final btc()I
    .locals 1

    .prologue
    .line 6008
    iget v0, p0, Ljre;->eAi:I

    return v0
.end method

.method public final btd()Z
    .locals 1

    .prologue
    .line 6016
    iget v0, p0, Ljre;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bte()I
    .locals 1

    .prologue
    .line 6027
    iget v0, p0, Ljre;->eAj:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 6070
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 6071
    iget v1, p0, Ljre;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 6072
    const/4 v1, 0x1

    iget v2, p0, Ljre;->eAi:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6075
    :cond_0
    iget v1, p0, Ljre;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 6076
    const/4 v1, 0x2

    iget v2, p0, Ljre;->eAj:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6079
    :cond_1
    return v0
.end method

.method public final rI(I)Ljre;
    .locals 1

    .prologue
    .line 6011
    iput p1, p0, Ljre;->eAi:I

    .line 6012
    iget v0, p0, Ljre;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljre;->aez:I

    .line 6013
    return-object p0
.end method

.method public final rJ(I)Ljre;
    .locals 1

    .prologue
    .line 6030
    iput p1, p0, Ljre;->eAj:I

    .line 6031
    iget v0, p0, Ljre;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljre;->aez:I

    .line 6032
    return-object p0
.end method
