.class public final Ldye;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljpe;Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Ljpe;->oY()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 89
    const-string v0, "com.android.phone"

    .line 96
    :goto_0
    return-object v0

    .line 91
    :cond_0
    iget-object v0, p0, Ljpe;->exC:[Ljpd;

    array-length v0, v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljpe;->exC:[Ljpd;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 93
    :goto_1
    invoke-static {p0, v0, p1}, Ldye;->c(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;

    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    const-string v0, "com.google.android.apps.maps"

    goto :goto_0

    .line 91
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;
    .locals 5
    .param p0    # Ljpe;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1    # Ljpd;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-virtual {p0}, Ljpe;->oY()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 55
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    invoke-virtual {p1}, Ljpd;->Ar()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ldye;->kq(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    .line 60
    :goto_0
    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    array-length v2, v0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 62
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 61
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 57
    :cond_0
    invoke-static {p0, p1, p2}, Ldye;->c(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 64
    :cond_1
    return-object v0
.end method

.method public static b(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;
    .locals 3
    .param p0    # Ljpe;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 74
    invoke-virtual {p0}, Ljpe;->oY()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 75
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, ""

    invoke-static {v2}, Ldye;->kq(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    .line 77
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2}, Ldye;->c(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private static c(Ljpe;Ljpd;Landroid/content/Context;)[Landroid/content/Intent;
    .locals 3
    .param p0    # Ljpe;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 164
    if-eqz p1, :cond_1

    .line 165
    iget-object v0, p0, Ljpe;->exD:[Ljpd;

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljpe;->exD:[Ljpd;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 170
    :goto_0
    invoke-virtual {p0}, Ljpe;->oY()I

    move-result v1

    invoke-virtual {p0}, Ljpe;->brT()I

    move-result v2

    invoke-static {v1, v0, p1, v2, p2}, Ledx;->a(ILjpd;Ljpd;ILandroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    .line 174
    :goto_1
    return-object v0

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 174
    :cond_1
    invoke-virtual {p0}, Ljpe;->brF()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Ledx;->b(Ljava/lang/String;Landroid/content/Context;)[Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method public static gT(I)I
    .locals 1

    .prologue
    .line 179
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 180
    const/4 p0, 0x1

    .line 182
    :cond_0
    packed-switch p0, :pswitch_data_0

    .line 191
    const v0, 0x7f0a0104

    :goto_0
    return v0

    .line 184
    :pswitch_0
    const v0, 0x7f0a0103

    goto :goto_0

    .line 186
    :pswitch_1
    const v0, 0x7f0a0106

    goto :goto_0

    .line 188
    :pswitch_2
    const v0, 0x7f0a0105

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static gU(I)I
    .locals 1

    .prologue
    .line 196
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    .line 197
    const/4 p0, 0x1

    .line 199
    :cond_0
    packed-switch p0, :pswitch_data_0

    .line 208
    const v0, 0x7f0200c4

    :goto_0
    return v0

    .line 201
    :pswitch_0
    const v0, 0x7f02009b

    goto :goto_0

    .line 203
    :pswitch_1
    const v0, 0x7f02009e

    goto :goto_0

    .line 205
    :pswitch_2
    const v0, 0x7f0200b6

    goto :goto_0

    .line 199
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static gV(I)I
    .locals 1

    .prologue
    .line 213
    packed-switch p0, :pswitch_data_0

    .line 225
    const/16 v0, 0x32

    :goto_0
    return v0

    .line 215
    :pswitch_0
    const/16 v0, 0x71

    goto :goto_0

    .line 218
    :pswitch_1
    const/16 v0, 0x72

    goto :goto_0

    .line 221
    :pswitch_2
    const/16 v0, 0x73

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static kq(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CALL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 42
    const-string v1, "tel"

    const/4 v2, 0x0

    invoke-static {v1, p0, v2}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 44
    return-object v0
.end method
