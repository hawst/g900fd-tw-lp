.class public final Lhqp;
.super Landroid/os/AsyncTask;
.source "PG"


# instance fields
.field private final doo:Lhqq;

.field private final mCallback:Lefk;

.field private final mNetworkClient:Lfcx;


# direct methods
.method public constructor <init>(Lefk;Lhqq;Lfcx;)V
    .locals 0

    .prologue
    .line 395
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 396
    iput-object p1, p0, Lhqp;->mCallback:Lefk;

    .line 397
    iput-object p2, p0, Lhqp;->doo:Lhqq;

    .line 398
    iput-object p3, p0, Lhqp;->mNetworkClient:Lfcx;

    .line 399
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 389
    const/16 v0, 0x1b

    invoke-static {v0}, Lfjw;->iQ(I)Ljed;

    move-result-object v0

    new-instance v1, Ljcv;

    invoke-direct {v1}, Ljcv;-><init>()V

    iput-object v1, v0, Ljed;->edA:Ljcv;

    iget-object v1, v0, Ljed;->edA:Ljcv;

    iget-object v2, p0, Lhqp;->doo:Lhqq;

    invoke-virtual {v2}, Lhqq;->aRy()Ljcx;

    move-result-object v2

    iput-object v2, v1, Ljcv;->eaZ:Ljcx;

    iget-object v1, p0, Lhqp;->mNetworkClient:Lfcx;

    invoke-interface {v1, v0}, Lfcx;->a(Ljed;)Ljeh;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Ljeh;->eem:Ljcw;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ljeh;->eem:Ljcw;

    iget-object v1, v1, Ljcw;->aeB:Ljbp;

    if-nez v1, :cond_1

    :cond_0
    const-string v0, "PlacesApiFetcher"

    const-string v1, "Error fetching place details"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, v0, Ljeh;->eem:Ljcw;

    iget-object v4, v0, Ljcw;->aeB:Ljbp;

    new-instance v0, Lhqo;

    invoke-virtual {v4}, Ljbp;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Ljbp;->getAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lhqp;->doo:Lhqq;

    invoke-virtual {v3}, Lhqq;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Ljbp;->mR()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4}, Ljbp;->mS()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    iget-object v5, p0, Lhqp;->doo:Lhqq;

    invoke-virtual {v5}, Lhqq;->aRB()Ljbq;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lhqo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Ljbq;)V

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 389
    check-cast p1, Lhqo;

    iget-object v2, p0, Lhqp;->mCallback:Lefk;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v2, v0}, Lefk;->ar(Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance v1, Ljpd;

    invoke-direct {v1}, Ljpd;-><init>()V

    iget-object v0, p1, Lhqo;->mName:Ljava/lang/String;

    iget-object v3, p1, Lhqo;->mAddress:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1, v0}, Ljpd;->xV(Ljava/lang/String;)Ljpd;

    :cond_1
    invoke-virtual {v1, v3}, Ljpd;->xY(Ljava/lang/String;)Ljpd;

    iget-object v0, p1, Lhqo;->dom:Landroid/util/Pair;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lhqo;->dom:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljpd;->u(D)Ljpd;

    iget-object v0, p1, Lhqo;->dom:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljpd;->v(D)Ljpd;

    :cond_2
    iget-object v0, p1, Lhqo;->don:Ljbq;

    if-eqz v0, :cond_3

    new-instance v0, Ljph;

    invoke-direct {v0}, Ljph;-><init>()V

    iget-object v3, p1, Lhqo;->don:Ljbq;

    invoke-virtual {v3}, Ljbq;->bfi()I

    move-result v3

    invoke-virtual {v0, v3}, Ljph;->re(I)Ljph;

    iget-object v3, p1, Lhqo;->don:Ljbq;

    invoke-virtual {v3}, Ljbq;->bfj()I

    move-result v3

    invoke-virtual {v0, v3}, Ljph;->rf(I)Ljph;

    iget-object v3, p1, Lhqo;->don:Ljbq;

    invoke-virtual {v3}, Ljbq;->bfk()I

    move-result v3

    invoke-virtual {v0, v3}, Ljph;->rg(I)Ljph;

    iget-object v3, p1, Lhqo;->don:Ljbq;

    invoke-virtual {v3}, Ljbq;->bfl()I

    move-result v3

    invoke-virtual {v0, v3}, Ljph;->rh(I)Ljph;

    iput-object v0, v1, Ljpd;->exk:Ljph;

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
