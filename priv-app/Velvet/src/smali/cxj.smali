.class public final Lcxj;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field bmo:Lcxn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method final cN(Z)V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcxj;->bmo:Lcxn;

    invoke-virtual {v0}, Lcxn;->TD()V

    .line 58
    iget-object v0, p0, Lcxj;->bmo:Lcxn;

    invoke-virtual {v0, p1}, Lcxn;->cO(Z)V

    .line 59
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcxj;->bmo:Lcxn;

    invoke-virtual {v0}, Lcxn;->TD()V

    .line 54
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 25
    invoke-static {p0}, Lcxn;->a(Landroid/app/Fragment;)Lcxn;

    move-result-object v0

    iput-object v0, p0, Lcxj;->bmo:Lcxn;

    .line 27
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcxj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {p0}, Lcxj;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0400e2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 30
    const v0, 0x7f11029e

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 32
    const v3, 0x7f0a0325

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0324

    new-instance v4, Lcxl;

    invoke-direct {v4, p0, v0}, Lcxl;-><init>(Lcxj;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v2, 0x1040000

    new-instance v3, Lcxk;

    invoke-direct {v3, p0}, Lcxk;-><init>(Lcxj;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 48
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
