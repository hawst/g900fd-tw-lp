.class public final Licx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final djY:Z

.field private final mAppSelectionHelper:Libo;

.field private final mContactLabelConverter:Ldyv;

.field private final mContext:Landroid/content/Context;

.field private final mDeviceCapabilityManager:Lenm;

.field private final mGsaConfigFlags:Lchk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lchk;Libo;Ldyv;Lenm;Z)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Licx;->mContext:Landroid/content/Context;

    .line 81
    iput-object p2, p0, Licx;->mGsaConfigFlags:Lchk;

    .line 82
    iput-object p3, p0, Licx;->mAppSelectionHelper:Libo;

    .line 83
    iput-object p4, p0, Licx;->mContactLabelConverter:Ldyv;

    .line 84
    iput-object p5, p0, Licx;->mDeviceCapabilityManager:Lenm;

    .line 85
    iput-boolean p6, p0, Licx;->djY:Z

    .line 86
    return-void
.end method

.method private static N(Ljava/lang/String;I)Ljqv;
    .locals 4

    .prologue
    .line 518
    new-instance v0, Ljqu;

    invoke-direct {v0}, Ljqu;-><init>()V

    .line 519
    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljqu;->rC(I)Ljqu;

    .line 520
    new-instance v1, Ljqv;

    invoke-direct {v1}, Ljqv;-><init>()V

    .line 521
    const/4 v2, 0x1

    new-array v2, v2, [Ljqu;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v1, Ljqv;->ezC:[Ljqu;

    .line 522
    invoke-virtual {v1, p0}, Ljqv;->yl(Ljava/lang/String;)Ljqv;

    .line 523
    return-object v1
.end method

.method private static a(Ljqi;)I
    .locals 1

    .prologue
    .line 89
    if-eqz p0, :cond_0

    iget-object v0, p0, Ljqi;->eyI:Ljoq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljqi;->eyI:Ljoq;

    invoke-virtual {v0}, Ljoq;->brw()Z

    move-result v0

    if-nez v0, :cond_1

    .line 92
    :cond_0
    const v0, 0x7f0c0124

    .line 94
    :goto_0
    return v0

    :cond_1
    const v0, 0x7f0c0135

    goto :goto_0
.end method

.method private static varargs a(Ljrc;ILjava/lang/String;Ljpw;[Ljqw;)Ljqg;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 404
    const/4 v0, 0x0

    invoke-static {p0, v0}, Licx;->b(Ljrc;I)Ljqg;

    move-result-object v0

    .line 405
    new-instance v1, Ljqq;

    invoke-direct {v1}, Ljqq;-><init>()V

    .line 406
    sget-object v2, Ljqq;->ezg:Ljsm;

    invoke-virtual {v0, v2, v1}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 407
    invoke-virtual {v1, p2}, Ljqq;->yj(Ljava/lang/String;)Ljqq;

    .line 408
    iput-object p4, v1, Ljqq;->ezh:[Ljqw;

    .line 409
    iput-object p3, v1, Ljqq;->ezi:Ljpw;

    .line 411
    return-object v0
.end method

.method private a(Ljrc;Lcom/google/android/speech/embedded/TaggerResult;)Ljqg;
    .locals 5

    .prologue
    .line 385
    const/4 v0, 0x2

    invoke-static {p1, v0}, Licx;->b(Ljrc;I)Ljqg;

    move-result-object v0

    .line 387
    new-instance v1, Ljqi;

    invoke-direct {v1}, Ljqi;-><init>()V

    .line 388
    sget-object v2, Ljqi;->eyH:Ljsm;

    invoke-virtual {v0, v2, v1}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 389
    iget-object v2, p0, Licx;->mContactLabelConverter:Ldyv;

    iget-object v3, p0, Licx;->mGsaConfigFlags:Lchk;

    sget-object v4, Ldzb;->bRq:Ldzb;

    invoke-static {v2, v3, p2, v4}, Licv;->a(Ldyv;Lchk;Lcom/google/android/speech/embedded/TaggerResult;Ldzb;)Ljoq;

    move-result-object v2

    iput-object v2, v1, Ljqi;->eyI:Ljoq;

    .line 396
    return-object v0
.end method

.method private static a(Ljqv;ILjqd;Ljqd;I)Ljqs;
    .locals 2

    .prologue
    .line 630
    new-instance v0, Ljqs;

    invoke-direct {v0}, Ljqs;-><init>()V

    sget-object v1, Ljqd;->eyo:Ljsm;

    invoke-virtual {v0, v1, p2}, Ljqs;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    sget-object v1, Ljqd;->eyp:Ljsm;

    invoke-virtual {v0, v1, p3}, Ljqs;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    invoke-virtual {v0, p4}, Ljqs;->rA(I)Ljqs;

    .line 632
    invoke-virtual {v0, p1}, Ljqs;->ry(I)Ljqs;

    .line 633
    iput-object p0, v0, Ljqs;->ezn:Ljqv;

    .line 634
    return-object v0
.end method

.method private static varargs a(Ljqv;Ljqv;[Ljqs;)Ljqt;
    .locals 2
    .param p0    # Ljqv;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljqv;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 610
    new-instance v0, Ljqt;

    invoke-direct {v0}, Ljqt;-><init>()V

    .line 611
    new-instance v1, Ljrg;

    invoke-direct {v1}, Ljrg;-><init>()V

    iput-object v1, v0, Ljqt;->ezs:Ljrg;

    .line 612
    iget-object v1, v0, Ljqt;->ezs:Ljrg;

    iput-object p0, v1, Ljrg;->eAn:Ljqv;

    .line 613
    iput-object p1, v0, Ljqt;->ezu:Ljqv;

    .line 614
    iput-object p2, v0, Ljqt;->ezt:[Ljqs;

    .line 615
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljqg;I)Ljqv;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 550
    new-instance v0, Ljqu;

    invoke-direct {v0}, Ljqu;-><init>()V

    .line 551
    invoke-virtual {p1}, Ljqg;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljqu;->rB(I)Ljqu;

    .line 552
    invoke-virtual {v0, p2}, Ljqu;->rC(I)Ljqu;

    .line 554
    new-instance v1, Ljqv;

    invoke-direct {v1}, Ljqv;-><init>()V

    .line 555
    const/4 v2, 0x1

    new-array v2, v2, [Ljqu;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v1, Ljqv;->ezC:[Ljqu;

    .line 556
    if-eqz p0, :cond_0

    .line 557
    invoke-virtual {v1, p0}, Ljqv;->yl(Ljava/lang/String;)Ljqv;

    .line 560
    :cond_0
    return-object v1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;ILjqs;Ljqs;Ljqt;Ljqt;Ljqt;Ljqt;[ILjava/lang/String;ZLjava/lang/String;)Ljrl;
    .locals 3
    .param p4    # Ljqs;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 573
    new-instance v0, Ljrl;

    invoke-direct {v0}, Ljrl;-><init>()V

    .line 574
    invoke-virtual {v0, p0}, Ljrl;->yx(Ljava/lang/String;)Ljrl;

    .line 575
    invoke-virtual {v0, p1}, Ljrl;->yy(Ljava/lang/String;)Ljrl;

    .line 576
    invoke-virtual {v0, p2}, Ljrl;->rR(I)Ljrl;

    .line 577
    const/4 v1, 0x1

    new-array v1, v1, [Ljqs;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    iput-object v1, v0, Ljrl;->eAH:[Ljqs;

    .line 578
    if-eqz p4, :cond_0

    .line 579
    const/4 v1, 0x1

    new-array v1, v1, [Ljqs;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    iput-object v1, v0, Ljrl;->eAI:[Ljqs;

    .line 581
    :cond_0
    iput-object p5, v0, Ljrl;->eAJ:Ljqt;

    .line 582
    iput-object p6, v0, Ljrl;->eAK:Ljqt;

    .line 583
    iput-object p7, v0, Ljrl;->eAL:Ljqt;

    .line 584
    iput-object p8, v0, Ljrl;->eAM:Ljqt;

    .line 586
    iput-object p9, v0, Ljrl;->ezI:[I

    .line 587
    iput-object p9, v0, Ljrl;->ezJ:[I

    .line 588
    if-eqz p10, :cond_1

    .line 589
    invoke-virtual {v0, p10}, Ljrl;->yz(Ljava/lang/String;)Ljrl;

    .line 591
    :cond_1
    invoke-virtual {v0, p11}, Ljrl;->iX(Z)Ljrl;

    .line 592
    if-eqz p12, :cond_2

    .line 593
    new-instance v1, Ljqf;

    invoke-direct {v1}, Ljqf;-><init>()V

    invoke-virtual {v1, p12}, Ljqf;->yg(Ljava/lang/String;)Ljqf;

    move-result-object v1

    iput-object v1, v0, Ljrl;->eAU:Ljqf;

    .line 597
    :cond_2
    return-object v0
.end method

.method private static a(Ljkt;ILjava/lang/String;)Ljrp;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 490
    new-instance v0, Ljrp;

    invoke-direct {v0}, Ljrp;-><init>()V

    .line 491
    new-instance v1, Ljku;

    invoke-direct {v1}, Ljku;-><init>()V

    iput-object v1, p0, Ljkt;->eqn:Ljku;

    .line 492
    iget-object v1, p0, Ljkt;->eqn:Ljku;

    invoke-virtual {v1, p1}, Ljku;->qx(I)Ljku;

    .line 493
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 494
    iget-object v1, p0, Ljkt;->eqn:Ljku;

    invoke-virtual {v1, p2}, Ljku;->ws(Ljava/lang/String;)Ljku;

    .line 496
    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljkt;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    iput-object v1, v0, Ljrp;->eBt:[Ljkt;

    .line 497
    return-object v0
.end method

.method private static a(Ljrc;ILjava/lang/String;)Ljrp;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 471
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    .line 473
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljkt;->is(Z)Ljkt;

    .line 474
    sget-object v1, Ljrc;->ezW:Ljsm;

    invoke-virtual {v0, v1, p0}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 475
    invoke-static {v0, p1, p2}, Licx;->a(Ljkt;ILjava/lang/String;)Ljrp;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljrp;I)Ljrp;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 479
    if-eqz p1, :cond_1

    .line 480
    iget-object v0, p0, Ljrp;->eBt:[Ljkt;

    aget-object v0, v0, v2

    iget-object v0, v0, Ljkt;->eqo:Ljlm;

    if-nez v0, :cond_0

    .line 481
    iget-object v0, p0, Ljrp;->eBt:[Ljkt;

    aget-object v0, v0, v2

    new-instance v1, Ljlm;

    invoke-direct {v1}, Ljlm;-><init>()V

    iput-object v1, v0, Ljkt;->eqo:Ljlm;

    .line 483
    :cond_0
    iget-object v0, p0, Ljrp;->eBt:[Ljkt;

    aget-object v0, v0, v2

    iget-object v0, v0, Ljkt;->eqo:Ljlm;

    invoke-virtual {v0, p1}, Ljlm;->qJ(I)Ljlm;

    .line 485
    :cond_1
    return-object p0
.end method

.method private static a(Ljrc;)V
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Ljrc;->eAc:Ljpu;

    if-nez v0, :cond_0

    .line 683
    new-instance v0, Ljpu;

    invoke-direct {v0}, Ljpu;-><init>()V

    iput-object v0, p0, Ljrc;->eAc:Ljpu;

    .line 685
    :cond_0
    iget-object v0, p0, Ljrc;->eAc:Ljpu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljpu;->iR(Z)Ljpu;

    .line 686
    return-void
.end method

.method private static varargs a([Ljqg;)[I
    .locals 3

    .prologue
    .line 666
    array-length v0, p0

    new-array v1, v0, [I

    .line 667
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 668
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljqg;->getId()I

    move-result v2

    aput v2, v1, v0

    .line 667
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 670
    :cond_0
    return-object v1
.end method

.method private static b(Ljrc;I)Ljqg;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 436
    move v1, v0

    .line 438
    :goto_0
    iget-object v2, p0, Ljrc;->ezX:[Ljqg;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 439
    iget-object v2, p0, Ljrc;->ezX:[Ljqg;

    aget-object v2, v2, v0

    if-nez v2, :cond_0

    .line 451
    :goto_1
    new-instance v2, Ljqg;

    invoke-direct {v2}, Ljqg;-><init>()V

    .line 452
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljqg;->rq(I)Ljqg;

    .line 453
    invoke-virtual {v2, p1}, Ljqg;->rr(I)Ljqg;

    .line 456
    if-ne v0, v3, :cond_2

    .line 457
    iget-object v0, p0, Ljrc;->ezX:[Ljqg;

    invoke-static {v0, v2}, Leqh;->a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljqg;

    iput-object v0, p0, Ljrc;->ezX:[Ljqg;

    .line 462
    :goto_2
    return-object v2

    .line 444
    :cond_0
    iget-object v2, p0, Ljrc;->ezX:[Ljqg;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljqg;->getId()I

    move-result v2

    .line 445
    if-le v2, v1, :cond_1

    move v1, v2

    .line 438
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 459
    :cond_2
    iget-object v1, p0, Ljrc;->ezX:[Ljqg;

    aput-object v2, v1, v0

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method private static lK(I)Ljrc;
    .locals 2

    .prologue
    .line 421
    new-instance v0, Ljrc;

    invoke-direct {v0}, Ljrc;-><init>()V

    .line 422
    new-array v1, p0, [Ljqg;

    iput-object v1, v0, Ljrc;->ezX:[Ljqg;

    .line 423
    return-object v0
.end method

.method private lL(I)Ljqs;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 654
    new-instance v0, Ljqd;

    invoke-direct {v0}, Ljqd;-><init>()V

    .line 655
    new-instance v1, Ljqu;

    invoke-direct {v1}, Ljqu;-><init>()V

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Ljqu;->rC(I)Ljqu;

    new-instance v2, Ljqv;

    invoke-direct {v2}, Ljqv;-><init>()V

    new-array v3, v5, [Ljqu;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    iput-object v3, v2, Ljqv;->ezC:[Ljqu;

    iput-object v2, v0, Ljqd;->eyr:Ljqv;

    .line 657
    invoke-virtual {v0, v5}, Ljqd;->iS(Z)Ljqd;

    .line 658
    iget-object v1, p0, Licx;->mContext:Landroid/content/Context;

    const v2, 0x7f0a08cf

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v1, v2}, Licx;->N(Ljava/lang/String;I)Ljqv;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v1, v2, v0, v0, p1}, Licx;->a(Ljqv;ILjqd;Ljqd;I)Ljqs;

    move-result-object v0

    return-object v0
.end method

.method private static pe(Ljava/lang/String;)Ljqv;
    .locals 1

    .prologue
    .line 530
    new-instance v0, Ljqv;

    invoke-direct {v0}, Ljqv;-><init>()V

    .line 531
    invoke-virtual {v0, p0}, Ljqv;->yl(Ljava/lang/String;)Ljqv;

    .line 532
    return-object v0
.end method

.method private pf(Ljava/lang/String;)Ljrp;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 696
    new-instance v0, Ljkt;

    invoke-direct {v0}, Ljkt;-><init>()V

    .line 697
    sget-object v1, Ljmv;->euA:Ljsm;

    new-instance v2, Ljmv;

    invoke-direct {v2}, Ljmv;-><init>()V

    iget-object v3, p0, Licx;->mContext:Landroid/content/Context;

    const v4, 0x7f0a064b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljmv;->xk(Ljava/lang/String;)Ljmv;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljkt;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    .line 700
    const/16 v1, 0x19

    invoke-static {v0, v1, p1}, Licx;->a(Ljkt;ILjava/lang/String;)Ljrp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/speech/embedded/TaggerResult;ZZLjava/lang/String;)Ljrp;
    .locals 20
    .param p4    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 105
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v2

    .line 106
    const-string v3, "CallContact"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "CallNumber"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "AmbiguousCommunicationAction"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 109
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v2}, Lenm;->auH()Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Licx;->pf(Ljava/lang/String;)Ljrp;

    move-result-object v2

    .line 118
    :goto_0
    return-object v2

    .line 109
    :cond_1
    const/4 v2, 0x1

    invoke-static {v2}, Licx;->lK(I)Ljrc;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0127

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v15, v2}, Ljrc;->rF(I)Ljrc;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v15, v1}, Licx;->a(Ljrc;Lcom/google/android/speech/embedded/TaggerResult;)Ljqg;

    move-result-object v8

    sget-object v2, Ljqi;->eyH:Ljsm;

    invoke-virtual {v8, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljqi;

    new-instance v3, Ljpv;

    invoke-direct {v3}, Ljpv;-><init>()V

    iput-object v3, v2, Ljqi;->eyJ:Ljpv;

    iget-object v3, v2, Ljqi;->eyJ:Ljpv;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljpv;->ri(I)Ljpv;

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v2}, Licx;->a(Ljqi;)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v8, v2}, Ljqg;->rs(I)Ljqg;

    new-instance v2, Licy;

    const-string v3, "android.intent.action.CALL"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Licy;-><init>(Ljava/lang/String;I)V

    const-string v3, "tel:%1$s"

    const/4 v4, 0x2

    invoke-static {v3, v8, v4}, Licx;->a(Ljava/lang/String;Ljqg;I)Ljqv;

    move-result-object v3

    invoke-virtual {v2, v3}, Licy;->a(Ljqv;)Licy;

    move-result-object v4

    new-instance v2, Licy;

    const-string v3, "android.intent.action.CALL"

    const/4 v5, 0x0

    invoke-direct {v2, v3, v5}, Licy;-><init>(Ljava/lang/String;I)V

    const-string v3, "tel:123456789"

    invoke-static {v3}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v3

    invoke-virtual {v2, v3}, Licy;->a(Ljqv;)Licy;

    move-result-object v5

    new-instance v2, Licy;

    const-string v3, "android.intent.action.VIEW"

    const/4 v6, 0x0

    invoke-direct {v2, v3, v6}, Licy;-><init>(Ljava/lang/String;I)V

    const/16 v3, 0xd

    const/4 v6, 0x0

    invoke-static {v6, v8, v3}, Licx;->a(Ljava/lang/String;Ljqg;I)Ljqv;

    move-result-object v3

    invoke-virtual {v2, v3}, Licy;->a(Ljqv;)Licy;

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    const v3, 0x7f0a090a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mContext:Landroid/content/Context;

    const v6, 0x7f0a090b

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v6

    const/4 v7, 0x3

    iget-object v4, v4, Licy;->dyu:Ljqd;

    iget-object v5, v5, Licy;->dyu:Ljqd;

    move-object/from16 v0, p0

    iget-object v9, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c0077

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    invoke-static {v6, v7, v4, v5, v9}, Licx;->a(Ljqv;ILjqd;Ljqd;I)Ljqs;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [Ljqs;

    const/4 v9, 0x0

    aput-object v5, v7, v9

    invoke-static {v4, v6, v7}, Licx;->a(Ljqv;Ljqv;[Ljqs;)Ljqt;

    move-result-object v7

    const/4 v4, 0x1

    new-array v0, v4, [Ljrl;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Licx;->mContext:Landroid/content/Context;

    const v10, 0x7f0a0801

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x1

    new-array v11, v11, [Ljqs;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Licx;->lL(I)Ljqs;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Licx;->a(Ljqv;Ljqv;[Ljqs;)Ljqt;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljqg;

    const/4 v11, 0x0

    aput-object v8, v10, v11

    invoke-static {v10}, Licx;->a([Ljqg;)[I

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Licx;->mContext:Landroid/content/Context;

    const v10, 0x7f0a08dc

    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object v8, v7

    move-object v10, v7

    invoke-static/range {v2 .. v14}, Licx;->a(Ljava/lang/String;Ljava/lang/String;ILjqs;Ljqs;Ljqt;Ljqt;Ljqt;Ljqt;[ILjava/lang/String;ZLjava/lang/String;)Ljrl;

    move-result-object v2

    aput-object v2, v16, v17

    move-object/from16 v0, v16

    iput-object v0, v15, Ljrc;->ezY:[Ljrl;

    new-instance v2, Lgtk;

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Licx;->mGsaConfigFlags:Lchk;

    move/from16 v0, p3

    invoke-direct {v2, v3, v4, v0}, Lgtk;-><init>(Landroid/content/res/Resources;Lchk;Z)V

    move/from16 v0, p2

    invoke-virtual {v2, v15, v0}, Lgtk;->a(Ljrc;Z)Ljqb;

    move-result-object v2

    iput-object v2, v15, Ljrc;->eAb:Ljqb;

    invoke-static {v15}, Licx;->a(Ljrc;)V

    const-string v2, "CallNumber"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/speech/embedded/TaggerResult;->aGX()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0x1c

    :goto_1
    move-object/from16 v0, p4

    invoke-static {v15, v2, v0}, Licx;->a(Ljrc;ILjava/lang/String;)Ljrp;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mGsaConfigFlags:Lchk;

    invoke-virtual {v3}, Lchk;->Kg()I

    move-result v3

    invoke-static {v2, v3}, Licx;->a(Ljrp;I)Ljrp;

    move-result-object v2

    goto/16 :goto_0

    :cond_2
    const/16 v2, 0xa

    goto :goto_1

    .line 111
    :cond_3
    const-string v3, "SendTextToContact"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mDeviceCapabilityManager:Lenm;

    invoke-interface {v2}, Lenm;->auH()Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1}, Licx;->pf(Ljava/lang/String;)Ljrp;

    move-result-object v2

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x2

    invoke-static {v2}, Licx;->lK(I)Ljrc;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0159

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v15, v2}, Ljrc;->rF(I)Ljrc;

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0108

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v15, v1}, Licx;->a(Ljrc;Lcom/google/android/speech/embedded/TaggerResult;)Ljqg;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    const v4, 0x7f0a08fa

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljqg;->yh(Ljava/lang/String;)Ljqg;

    sget-object v2, Ljqi;->eyH:Ljsm;

    invoke-virtual {v11, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljqi;

    move-object/from16 v0, p0

    iget-object v4, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v2}, Licx;->a(Ljqi;)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v11, v2}, Ljqg;->rs(I)Ljqg;

    const-string v2, "Message"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x4

    invoke-static {v15, v4}, Licx;->b(Ljrc;I)Ljqg;

    move-result-object v12

    new-instance v4, Ljrh;

    invoke-direct {v4}, Ljrh;-><init>()V

    sget-object v5, Ljrh;->eAt:Ljsm;

    invoke-virtual {v12, v5, v4}, Ljqg;->a(Ljsm;Ljava/lang/Object;)Ljsl;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v12, v3}, Ljqg;->yh(Ljava/lang/String;)Ljqg;

    :cond_5
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v4, v2}, Ljrh;->yw(Ljava/lang/String;)Ljrh;

    :cond_6
    sget-object v2, Ljrh;->eAt:Ljsm;

    invoke-virtual {v12, v2}, Ljqg;->a(Ljsm;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljrh;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljrh;->rO(I)Ljrh;

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0117

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v12, v2}, Ljqg;->rs(I)Ljqg;

    new-instance v2, Licy;

    const-string v3, "com.google.android.apps.googlevoice.action.AUTO_SEND"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Licy;-><init>(Ljava/lang/String;I)V

    const-string v3, "smsto:%1$s"

    const/4 v4, 0x2

    invoke-static {v3, v11, v4}, Licx;->a(Ljava/lang/String;Ljqg;I)Ljqv;

    move-result-object v3

    invoke-virtual {v2, v3}, Licy;->a(Ljqv;)Licy;

    move-result-object v2

    const-string v3, "android.intent.extra.TEXT"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v6, v12, v5}, Licx;->a(Ljava/lang/String;Ljqg;I)Ljqv;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Licy;->a(Ljava/lang/String;ILjqv;)Licy;

    move-result-object v5

    if-nez p2, :cond_7

    iget-object v2, v5, Licy;->dyu:Ljqd;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljqd;->iT(Z)Ljqd;

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Licx;->djY:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v2

    iget-object v3, v5, Licy;->dyu:Ljqd;

    iput-object v2, v3, Ljqd;->eyr:Ljqv;

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Licx;->djY:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    new-instance v2, Ljqd;

    invoke-direct {v2}, Ljqd;-><init>()V

    invoke-static {v3}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v3

    iput-object v3, v2, Ljqd;->eyr:Ljqv;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljqd;->iS(Z)Ljqd;

    move-object v4, v2

    :goto_2
    new-instance v2, Licy;

    const-string v3, "android.intent.action.SENDTO"

    const/4 v6, 0x1

    invoke-direct {v2, v3, v6}, Licy;-><init>(Ljava/lang/String;I)V

    const-string v3, "smsto:%1$s"

    const/4 v6, 0x2

    invoke-static {v3, v11, v6}, Licx;->a(Ljava/lang/String;Ljqg;I)Ljqv;

    move-result-object v3

    invoke-virtual {v2, v3}, Licy;->a(Ljqv;)Licy;

    move-result-object v2

    const-string v3, "sms_body"

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v8, v12, v7}, Licx;->a(Ljava/lang/String;Ljqg;I)Ljqv;

    move-result-object v7

    invoke-virtual {v2, v3, v6, v7}, Licy;->a(Ljava/lang/String;ILjqv;)Licy;

    move-result-object v6

    new-instance v2, Licy;

    const-string v3, "android.intent.action.SENDTO"

    const/4 v7, 0x0

    invoke-direct {v2, v3, v7}, Licy;-><init>(Ljava/lang/String;I)V

    const-string v3, "smsto:12345"

    invoke-static {v3}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v3

    invoke-virtual {v2, v3}, Licy;->a(Ljqv;)Licy;

    move-result-object v2

    iget-object v7, v2, Licy;->dyu:Ljqd;

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0903

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mContext:Landroid/content/Context;

    const v8, 0x7f0a0906

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v8

    const/4 v9, 0x1

    iget-object v5, v5, Licy;->dyu:Ljqd;

    move-object/from16 v0, p0

    iget-object v10, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v13, 0x7f0c0149

    invoke-virtual {v10, v13}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    invoke-static {v8, v9, v5, v4, v10}, Licx;->a(Ljqv;ILjqd;Ljqd;I)Ljqs;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Licx;->mContext:Landroid/content/Context;

    const v8, 0x7f0a08d0

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x7

    invoke-static {v4, v8}, Licx;->N(Ljava/lang/String;I)Ljqv;

    move-result-object v4

    const/4 v8, 0x2

    iget-object v6, v6, Licy;->dyu:Ljqd;

    move-object/from16 v0, p0

    iget-object v9, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c00f4

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    invoke-static {v4, v8, v6, v7, v9}, Licx;->a(Ljqv;ILjqd;Ljqd;I)Ljqs;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0c011c

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Licx;->lL(I)Ljqs;

    move-result-object v10

    const/4 v4, 0x1

    new-array v0, v4, [Ljrl;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Licx;->mContext:Landroid/content/Context;

    const v8, 0x7f0a060c

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Licx;->mContext:Landroid/content/Context;

    const v9, 0x7f0a060d

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x4

    invoke-static {v8, v11, v9}, Licx;->a(Ljava/lang/String;Ljqg;I)Ljqv;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljqs;

    const/4 v13, 0x0

    aput-object v10, v9, v13

    invoke-static {v7, v8, v9}, Licx;->a(Ljqv;Ljqv;[Ljqs;)Ljqt;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v13, 0x1

    new-array v13, v13, [Ljqs;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    invoke-static {v8, v9, v13}, Licx;->a(Ljqv;Ljqv;[Ljqs;)Ljqt;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Licx;->mContext:Landroid/content/Context;

    const v13, 0x7f0a080c

    invoke-virtual {v9, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v9

    const/4 v13, 0x0

    const/4 v14, 0x1

    new-array v14, v14, [Ljqs;

    const/16 v18, 0x0

    aput-object v10, v14, v18

    invoke-static {v9, v13, v14}, Licx;->a(Ljqv;Ljqv;[Ljqs;)Ljqt;

    move-result-object v9

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljqs;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v10, v18, v19

    move-object/from16 v0, v18

    invoke-static {v13, v14, v0}, Licx;->a(Ljqv;Ljqv;[Ljqs;)Ljqt;

    move-result-object v10

    const/4 v13, 0x2

    new-array v13, v13, [Ljqg;

    const/4 v14, 0x0

    aput-object v11, v13, v14

    const/4 v11, 0x1

    aput-object v12, v13, v11

    invoke-static {v13}, Licx;->a([Ljqg;)[I

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Licx;->mContext:Landroid/content/Context;

    const v13, 0x7f0a091e

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static/range {v2 .. v14}, Licx;->a(Ljava/lang/String;Ljava/lang/String;ILjqs;Ljqs;Ljqt;Ljqt;Ljqt;Ljqt;[ILjava/lang/String;ZLjava/lang/String;)Ljrl;

    move-result-object v2

    aput-object v2, v16, v17

    move-object/from16 v0, v16

    iput-object v0, v15, Ljrc;->ezY:[Ljrl;

    new-instance v2, Lgtq;

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Licx;->mGsaConfigFlags:Lchk;

    move/from16 v0, p3

    invoke-direct {v2, v3, v4, v0}, Lgtq;-><init>(Landroid/content/res/Resources;Lchk;Z)V

    move/from16 v0, p2

    invoke-virtual {v2, v15, v0}, Lgtq;->a(Ljrc;Z)Ljqb;

    move-result-object v2

    iput-object v2, v15, Ljrc;->eAb:Ljqb;

    invoke-static {v15}, Licx;->a(Ljrc;)V

    const/4 v2, 0x1

    move-object/from16 v0, p4

    invoke-static {v15, v2, v0}, Licx;->a(Ljrc;ILjava/lang/String;)Ljrp;

    move-result-object v2

    goto/16 :goto_0

    :cond_9
    new-instance v2, Licy;

    const-string v3, "com.google.android.apps.googlevoice.action.AUTO_SEND"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Licy;-><init>(Ljava/lang/String;I)V

    const-string v3, "smsto:123456790"

    invoke-static {v3}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v3

    invoke-virtual {v2, v3}, Licy;->a(Ljqv;)Licy;

    move-result-object v2

    iget-object v2, v2, Licy;->dyu:Ljqd;

    move-object v4, v2

    goto/16 :goto_2

    .line 114
    :cond_a
    const-string v3, "OpenApp"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 115
    const-string v2, "AppName"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/speech/embedded/TaggerResult;->nb(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x1

    invoke-static {v2}, Licx;->lK(I)Ljrc;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c00a0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v15, v2}, Ljrc;->rF(I)Ljrc;

    new-instance v4, Ljpw;

    invoke-direct {v4}, Ljpw;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {v4, v2}, Ljpw;->rk(I)Ljpw;

    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Ljpw;->rj(I)Ljpw;

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mAppSelectionHelper:Libo;

    invoke-virtual {v2, v3}, Libo;->oW(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    new-instance v6, Ljqw;

    invoke-direct {v6}, Ljqw;-><init>()V

    const/4 v2, 0x0

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/shared/util/App;

    invoke-virtual {v2}, Lcom/google/android/shared/util/App;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljqw;->ym(Ljava/lang/String;)Ljqw;

    const/4 v2, 0x4

    invoke-virtual {v6, v2}, Ljqw;->rD(I)Ljqw;

    const/4 v2, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [Ljqw;

    const/4 v8, 0x0

    aput-object v6, v7, v8

    invoke-static {v15, v2, v3, v4, v7}, Licx;->a(Ljrc;ILjava/lang/String;Ljpw;[Ljqw;)Ljqg;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00a1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v8, v2}, Ljqg;->rs(I)Ljqg;

    move-object/from16 v0, p0

    iget-object v2, v0, Licx;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0842

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v4

    const/4 v6, 0x7

    const/4 v3, 0x0

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/shared/util/App;

    invoke-virtual {v3}, Lcom/google/android/shared/util/App;->auq()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljqd;

    invoke-direct {v5}, Ljqd;-><init>()V

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljqd;->iS(Z)Ljqd;

    move-result-object v5

    invoke-static {v3}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v3

    iput-object v3, v5, Ljqd;->eyr:Ljqv;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f0c009f

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    invoke-static {v4, v6, v5, v3, v7}, Licx;->a(Ljqv;ILjqd;Ljqd;I)Ljqs;

    move-result-object v5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljqs;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-static {v3, v4, v6}, Licx;->a(Ljqv;Ljqv;[Ljqs;)Ljqt;

    move-result-object v7

    const/4 v3, 0x1

    new-array v0, v3, [Ljrl;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const-string v3, ""

    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    new-array v11, v11, [Ljqs;

    invoke-static {v9, v10, v11}, Licx;->a(Ljqv;Ljqv;[Ljqs;)Ljqt;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljqg;

    const/4 v11, 0x0

    aput-object v8, v10, v11

    invoke-static {v10}, Licx;->a([Ljqg;)[I

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    move-object v8, v7

    move-object v10, v7

    invoke-static/range {v2 .. v14}, Licx;->a(Ljava/lang/String;Ljava/lang/String;ILjqs;Ljqs;Ljqt;Ljqt;Ljqt;Ljqt;[ILjava/lang/String;ZLjava/lang/String;)Ljrl;

    move-result-object v2

    aput-object v2, v16, v17

    move-object/from16 v0, v16

    iput-object v0, v15, Ljrc;->ezY:[Ljrl;

    new-instance v2, Ljqb;

    invoke-direct {v2}, Ljqb;-><init>()V

    iput-object v2, v15, Ljrc;->eAb:Ljqb;

    iget-object v2, v15, Ljrc;->eAb:Ljqb;

    new-instance v3, Ljrg;

    invoke-direct {v3}, Ljrg;-><init>()V

    iput-object v3, v2, Ljqb;->eyg:Ljrg;

    iget-object v2, v15, Ljrc;->eAb:Ljqb;

    iget-object v2, v2, Ljqb;->eyg:Ljrg;

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0856

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Licx;->pe(Ljava/lang/String;)Ljqv;

    move-result-object v3

    iput-object v3, v2, Ljrg;->eAn:Ljqv;

    :goto_3
    const/4 v2, 0x3

    move-object/from16 v0, p4

    invoke-static {v15, v2, v0}, Licx;->a(Ljrc;ILjava/lang/String;)Ljrp;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mGsaConfigFlags:Lchk;

    invoke-virtual {v3}, Lchk;->Kh()I

    move-result v3

    invoke-static {v2, v3}, Licx;->a(Ljrp;I)Ljrp;

    move-result-object v2

    goto/16 :goto_0

    :cond_b
    const-string v2, "PumpkinModularActionFac"

    const-string v5, "Pumpkin triggered OpenApp for query: \"%s\", but no app found"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-static {v2, v5, v6}, Leor;->d(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    const/4 v5, 0x0

    new-array v5, v5, [Ljqw;

    invoke-static {v15, v2, v3, v4, v5}, Licx;->a(Ljrc;ILjava/lang/String;Ljpw;[Ljqw;)Ljqg;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Licx;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00a1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljqg;->rs(I)Ljqg;

    goto :goto_3

    .line 118
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_0
.end method
