.class public Ldcu;
.super Lddj;
.source "PG"


# instance fields
.field private aNU:Z

.field private aNV:Z

.field private aNd:Z

.field private bfy:Lgnu;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bpH:Z

.field private bph:Landroid/os/Bundle;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final bqt:Lcom/google/android/shared/util/BitFlags;

.field private bsA:J

.field private bsB:Z

.field private bsC:Z

.field private bsD:Z

.field private bsE:Z

.field private bsF:I

.field private bsG:J

.field private bsH:I

.field private final bsI:Ljava/util/Set;

.field private bsJ:Z

.field private bsK:Z

.field private final bsL:Z

.field private bsM:I

.field private bsN:I

.field private bsO:I

.field private bsP:Z

.field private bsQ:Z

.field private bsR:Z

.field private bsS:Z

.field private bsT:Z

.field private bsU:Z

.field private bsV:Ljava/util/List;

.field private bsW:Ljava/util/List;

.field private bsX:I

.field private bsY:Z

.field private bsZ:Z

.field private bsv:I

.field private bsw:I

.field private bsx:I

.field private bsy:Z

.field private bsz:Z

.field private bta:I

.field private btb:I

.field private btc:Z

.field private mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

.field private final mEventBus:Ldda;

.field private final mGsaConfigFlags:Lchk;

.field private final mNetworkInformation:Lgno;

.field private final mScreenStateHelper:Ldmh;

.field private final mSearchSettings:Lcke;

.field private final mSettings:Lhym;

.field private final mSettingsUtil:Lbyl;


# direct methods
.method public constructor <init>(Ldda;Lbyl;Lhym;Lcke;Lchk;ZLdmh;Lgno;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 304
    const/16 v0, 0x40

    invoke-direct {p0, p1, v0}, Lddj;-><init>(Ldda;I)V

    .line 224
    iput v2, p0, Ldcu;->bsw:I

    .line 226
    iput v2, p0, Ldcu;->bsx:I

    .line 238
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldcu;->bsI:Ljava/util/Set;

    .line 250
    iput-boolean v2, p0, Ldcu;->bsT:Z

    .line 254
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ldcu;->bsV:Ljava/util/List;

    .line 255
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ldcu;->bsW:Ljava/util/List;

    .line 256
    iput v2, p0, Ldcu;->bsX:I

    .line 269
    iput-boolean v2, p0, Ldcu;->bsY:Z

    .line 290
    const/4 v0, -0x1

    iput v0, p0, Ldcu;->btb:I

    .line 296
    iput-boolean v2, p0, Ldcu;->btc:Z

    .line 298
    new-instance v0, Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/shared/util/BitFlags;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    .line 305
    iput-object p1, p0, Ldcu;->mEventBus:Ldda;

    .line 306
    iput-object p2, p0, Ldcu;->mSettingsUtil:Lbyl;

    .line 307
    iput-object p3, p0, Ldcu;->mSettings:Lhym;

    .line 308
    iput-object p4, p0, Ldcu;->mSearchSettings:Lcke;

    .line 309
    sget-object v0, Lcom/google/android/search/shared/service/ClientConfig;->bUT:Lcom/google/android/search/shared/service/ClientConfig;

    iput-object v0, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    .line 310
    iput-object p5, p0, Ldcu;->mGsaConfigFlags:Lchk;

    .line 311
    iput-boolean p6, p0, Ldcu;->bsL:Z

    .line 312
    iput v2, p0, Ldcu;->bsM:I

    .line 313
    iput v2, p0, Ldcu;->bsN:I

    .line 314
    iput v2, p0, Ldcu;->bsO:I

    .line 315
    iput-object p7, p0, Ldcu;->mScreenStateHelper:Ldmh;

    .line 316
    iput-object p8, p0, Ldcu;->mNetworkInformation:Lgno;

    .line 317
    iget-object v0, p0, Ldcu;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aUi()Z

    move-result v0

    iput-boolean v0, p0, Ldcu;->aNU:Z

    .line 318
    return-void
.end method

.method private YH()V
    .locals 1

    .prologue
    .line 621
    invoke-direct {p0}, Ldcu;->YI()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 624
    :cond_0
    return-void
.end method

.method private YI()Z
    .locals 14

    .prologue
    const-wide/16 v8, 0x1

    const-wide/16 v12, 0x4

    const/4 v10, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 632
    iget-object v0, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xe()Lcom/google/android/shared/search/Query;

    move-result-object v4

    .line 636
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aau()Ldbh;

    move-result-object v0

    invoke-virtual {v0}, Ldbh;->Wg()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 637
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 642
    :goto_0
    iget-object v1, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    invoke-virtual {v1}, Ldbd;->VI()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 643
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x2

    invoke-virtual {v1, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 648
    :goto_1
    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v1}, Lbyl;->BT()Z

    move-result v1

    if-eqz v1, :cond_19

    iget v1, p0, Ldcu;->bsM:I

    if-eqz v1, :cond_16

    iget v1, p0, Ldcu;->bsM:I

    if-ne v1, v3, :cond_15

    move v1, v3

    :goto_2
    if-eqz v1, :cond_1a

    .line 649
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v12, v13}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v5

    .line 650
    if-eqz v5, :cond_4

    .line 651
    iput-boolean v3, p0, Ldcu;->bsR:Z

    .line 652
    iget v1, p0, Ldcu;->bsM:I

    if-nez v1, :cond_2

    invoke-virtual {p0}, Ldcu;->YL()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    iput v1, p0, Ldcu;->bsN:I

    :cond_0
    invoke-direct {p0}, Ldcu;->YM()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x6

    iput v1, p0, Ldcu;->bsN:I

    :cond_1
    invoke-virtual {p0}, Ldcu;->YO()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x7

    iput v1, p0, Ldcu;->bsN:I

    :cond_2
    const/16 v1, 0x138

    iget v6, p0, Ldcu;->bsN:I

    packed-switch v6, :pswitch_data_0

    :goto_3
    iget-object v6, p0, Ldcu;->bsV:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/16 v7, 0x14

    if-lt v6, v7, :cond_3

    iget-object v6, p0, Ldcu;->bsV:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_3
    iget-object v6, p0, Ldcu;->bsV:Ljava/util/List;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget v8, p0, Ldcu;->bsN:I

    invoke-static {v8}, Lbwx;->el(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Ldcu;->bsX:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Ldcu;->bsX:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput v2, p0, Ldcu;->bsO:I

    invoke-static {v1}, Lege;->ht(I)V

    .line 654
    :cond_4
    or-int/2addr v0, v5

    .line 666
    :goto_4
    invoke-direct {p0}, Ldcu;->YJ()Z

    move-result v1

    if-nez v1, :cond_5

    iget v1, p0, Ldcu;->bsM:I

    if-ne v1, v10, :cond_5

    .line 667
    iput v2, p0, Ldcu;->bsM:I

    .line 674
    :cond_5
    iget v1, p0, Ldcu;->bsM:I

    if-ne v1, v10, :cond_1c

    .line 675
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x100

    invoke-virtual {v1, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 680
    :goto_5
    iget-object v1, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v1}, Lcom/google/android/search/shared/service/ClientConfig;->amV()Z

    move-result v1

    if-eqz v1, :cond_24

    iget-object v1, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aaj()Ldbd;

    move-result-object v1

    iget-object v5, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v5}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/search/core/state/QueryState;->Yi()Lcom/google/android/search/shared/actions/errors/SearchError;

    move-result-object v5

    if-nez v5, :cond_24

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqo()Z

    move-result v5

    if-nez v5, :cond_24

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aiM()Z

    move-result v5

    if-nez v5, :cond_24

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v5

    if-nez v5, :cond_1e

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apL()Z

    move-result v5

    if-eqz v5, :cond_1d

    invoke-direct {p0}, Ldcu;->Zh()I

    move-result v1

    .line 681
    :goto_6
    iget v5, p0, Ldcu;->bsF:I

    if-eq v1, v5, :cond_6

    .line 682
    iput v1, p0, Ldcu;->bsF:I

    move v0, v3

    .line 688
    :cond_6
    iget-wide v6, p0, Ldcu;->bsG:J

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-eqz v1, :cond_7

    .line 689
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqJ()J

    move-result-wide v6

    iput-wide v6, p0, Ldcu;->bsG:J

    .line 691
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->aqu()Z

    move-result v1

    if-nez v1, :cond_3f

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apO()Z

    move-result v1

    if-nez v1, :cond_3f

    .line 692
    iget-object v1, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v1}, Lcom/google/android/search/shared/service/ClientConfig;->YA()Z

    move-result v1

    if-eqz v1, :cond_25

    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apV()I

    move-result v1

    .line 694
    :goto_7
    iget v4, p0, Ldcu;->bsH:I

    if-eq v4, v1, :cond_7

    .line 695
    iput v1, p0, Ldcu;->bsH:I

    move v0, v3

    .line 704
    :cond_7
    iget-object v1, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aal()Ldby;

    move-result-object v1

    invoke-virtual {v1}, Ldby;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_26

    invoke-virtual {p0}, Ldcu;->Zj()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 705
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x8

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 710
    :goto_8
    iget-boolean v1, p0, Ldcu;->bsL:Z

    if-nez v1, :cond_27

    move v1, v2

    .line 711
    :goto_9
    iget-boolean v4, p0, Ldcu;->bsP:Z

    if-eq v4, v1, :cond_8

    .line 716
    iput-boolean v1, p0, Ldcu;->bsP:Z

    move v0, v3

    .line 720
    :cond_8
    iget-object v1, p0, Ldcu;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->HP()Z

    move-result v1

    if-nez v1, :cond_2d

    move v1, v2

    .line 721
    :goto_a
    iget-boolean v4, p0, Ldcu;->bsQ:Z

    if-eq v4, v1, :cond_9

    .line 726
    iput-boolean v1, p0, Ldcu;->bsQ:Z

    move v0, v3

    .line 730
    :cond_9
    iget-boolean v1, p0, Ldcu;->bsC:Z

    if-eqz v1, :cond_35

    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v1}, Lbyl;->BQ()Z

    move-result v1

    if-eqz v1, :cond_35

    .line 732
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x10

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 737
    :goto_b
    iget-boolean v1, p0, Ldcu;->bsE:Z

    if-eqz v1, :cond_36

    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v1}, Lbyl;->BQ()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 738
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x20

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v1

    or-int/2addr v0, v1

    .line 743
    :goto_c
    iget-object v1, p0, Ldcu;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTT()I

    move-result v1

    if-eq v1, v10, :cond_37

    .line 744
    :goto_d
    iget-boolean v1, p0, Ldcu;->bsU:Z

    if-eq v1, v2, :cond_a

    .line 749
    iput-boolean v2, p0, Ldcu;->bsU:Z

    move v0, v3

    .line 754
    :cond_a
    iget-object v1, p0, Ldcu;->bfy:Lgnu;

    if-eqz v1, :cond_b

    iget-object v1, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->Xc()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 755
    const/4 v0, 0x0

    iput-object v0, p0, Ldcu;->bph:Landroid/os/Bundle;

    .line 756
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldcu;->u(Landroid/os/Bundle;)Lgnu;

    move-result-object v0

    iput-object v0, p0, Ldcu;->bfy:Lgnu;

    move v0, v3

    .line 763
    :cond_b
    iget-boolean v1, p0, Ldcu;->bsL:Z

    if-eqz v1, :cond_38

    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v1}, Lbyl;->BN()Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v1}, Lbyl;->BO()Z

    move-result v1

    if-eqz v1, :cond_38

    :cond_c
    iget-object v1, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    or-int/lit8 v1, v1, 0x0

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.USER_PRESENT"

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    :goto_e
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v2

    invoke-virtual {v2}, Lgql;->aJq()Lhhq;

    move-result-object v2

    invoke-virtual {v2}, Lhhq;->isLowRamDevice()Z

    move-result v2

    iget-object v4, p0, Ldcu;->mGsaConfigFlags:Lchk;

    invoke-virtual {v4}, Lchk;->JK()Z

    move-result v4

    if-eqz v4, :cond_39

    iget-object v4, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v4}, Lbyl;->BU()Z

    move-result v4

    if-eqz v4, :cond_39

    iget-object v4, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v5, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    or-int/2addr v1, v4

    :goto_f
    iget-object v4, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v4}, Lbyl;->BT()Z

    move-result v4

    if-eqz v4, :cond_3a

    iget-object v4, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v5, "com.google.android.apps.gmm.NAVIGATION_STATE"

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    or-int/2addr v1, v4

    :goto_10
    iget-object v4, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v4}, Lbyl;->BQ()Z

    move-result v4

    if-eqz v4, :cond_d

    iget-boolean v4, p0, Ldcu;->bsE:Z

    if-nez v4, :cond_e

    iget-boolean v4, p0, Ldcu;->bsC:Z

    if-nez v4, :cond_e

    :cond_d
    iget-object v4, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v4}, Lbyl;->BP()Z

    move-result v4

    if-eqz v4, :cond_3b

    iget-object v4, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v4, v12, v13}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v4

    if-eqz v4, :cond_3b

    :cond_e
    iget-object v4, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v5, "android.provider.Telephony.SMS_RECEIVED"

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    or-int/2addr v1, v4

    :cond_f
    :goto_11
    if-nez v2, :cond_10

    iget-object v2, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v2}, Lbyl;->BR()Z

    move-result v2

    if-nez v2, :cond_11

    :cond_10
    iget-object v2, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v2}, Lbyl;->BQ()Z

    move-result v2

    if-eqz v2, :cond_3c

    :cond_11
    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.HEADSET_PLUG"

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    :goto_12
    iget-object v2, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v2}, Lbyl;->BT()Z

    move-result v2

    if-eqz v2, :cond_3d

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "com.google.android.apps.gmm.NAVIGATION_STATE"

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    :goto_13
    if-eqz v1, :cond_12

    iput-boolean v3, p0, Ldcu;->bsJ:Z

    :cond_12
    if-eqz v1, :cond_3e

    .line 768
    :goto_14
    return v3

    .line 639
    :cond_13
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto/16 :goto_0

    .line 645
    :cond_14
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x2

    invoke-virtual {v1, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    or-int/2addr v0, v1

    goto/16 :goto_1

    :cond_15
    move v1, v2

    .line 648
    goto/16 :goto_2

    :cond_16
    invoke-virtual {p0}, Ldcu;->YL()Z

    move-result v1

    if-nez v1, :cond_17

    invoke-direct {p0}, Ldcu;->YM()Z

    move-result v1

    if-nez v1, :cond_17

    invoke-virtual {p0}, Ldcu;->YO()Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_17
    move v1, v3

    goto/16 :goto_2

    :cond_18
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v12, v13}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v1

    if-eqz v1, :cond_19

    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v8, v9}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v1

    if-eqz v1, :cond_19

    move v1, v3

    goto/16 :goto_2

    :cond_19
    move v1, v2

    goto/16 :goto_2

    .line 652
    :pswitch_0
    const/16 v1, 0x139

    goto/16 :goto_3

    :pswitch_1
    const/16 v1, 0x13b

    goto/16 :goto_3

    :pswitch_2
    const/16 v1, 0x13c

    goto/16 :goto_3

    :pswitch_3
    const/16 v1, 0x140

    goto/16 :goto_3

    :pswitch_4
    const/16 v1, 0x13d

    goto/16 :goto_3

    :pswitch_5
    const/16 v1, 0x13e

    goto/16 :goto_3

    :pswitch_6
    const/16 v1, 0x15a

    goto/16 :goto_3

    .line 656
    :cond_1a
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v12, v13}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    .line 657
    if-eqz v1, :cond_1b

    .line 658
    iput-boolean v3, p0, Ldcu;->bsS:Z

    .line 659
    invoke-direct {p0}, Ldcu;->YK()V

    .line 661
    :cond_1b
    or-int/2addr v0, v1

    goto/16 :goto_4

    .line 677
    :cond_1c
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x100

    invoke-virtual {v1, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    or-int/2addr v0, v1

    goto/16 :goto_5

    .line 680
    :cond_1d
    invoke-virtual {v4}, Lcom/google/android/shared/search/Query;->apK()Z

    move-result v5

    if-eqz v5, :cond_24

    invoke-virtual {v1, v4}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v1

    if-eqz v1, :cond_24

    invoke-direct {p0}, Ldcu;->Zh()I

    move-result v1

    goto/16 :goto_6

    :cond_1e
    invoke-virtual {v1, v4}, Ldbd;->A(Lcom/google/android/shared/search/Query;)Z

    move-result v5

    if-eqz v5, :cond_24

    invoke-virtual {v1}, Ldbd;->Qs()Lcom/google/android/velvet/ActionData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/velvet/ActionData;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1f

    invoke-direct {p0}, Ldcu;->Zh()I

    move-result v1

    goto/16 :goto_6

    :cond_1f
    invoke-virtual {v1}, Ldbd;->yo()Z

    move-result v5

    if-eqz v5, :cond_24

    invoke-virtual {v1}, Ldbd;->VK()Z

    move-result v5

    if-nez v5, :cond_20

    invoke-direct {p0}, Ldcu;->Zh()I

    move-result v1

    goto/16 :goto_6

    :cond_20
    invoke-virtual {v1}, Ldbd;->VQ()Lcom/google/android/search/shared/actions/VoiceAction;

    move-result-object v5

    if-eqz v5, :cond_23

    invoke-virtual {v1, v5}, Ldbd;->q(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/search/shared/actions/utils/CardDecision;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v6

    if-eqz v6, :cond_21

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->ali()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_21

    invoke-virtual {v1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->Zv()Z

    move-result v1

    if-nez v1, :cond_24

    :cond_21
    invoke-interface {v5}, Lcom/google/android/search/shared/actions/VoiceAction;->agq()Z

    move-result v1

    if-nez v1, :cond_22

    invoke-interface {v5}, Lcom/google/android/search/shared/actions/VoiceAction;->agr()Z

    move-result v1

    if-eqz v1, :cond_23

    :cond_22
    move v1, v2

    goto/16 :goto_6

    :cond_23
    invoke-direct {p0}, Ldcu;->Zh()I

    move-result v1

    goto/16 :goto_6

    :cond_24
    move v1, v2

    goto/16 :goto_6

    :cond_25
    move v1, v2

    .line 692
    goto/16 :goto_7

    .line 707
    :cond_26
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x8

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    or-int/2addr v0, v1

    goto/16 :goto_8

    .line 710
    :cond_27
    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v1}, Lbyl;->BN()Z

    move-result v1

    if-nez v1, :cond_28

    move v1, v2

    goto/16 :goto_9

    :cond_28
    iget-object v1, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aal()Ldby;

    move-result-object v1

    invoke-virtual {v1}, Ldby;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_29

    move v1, v2

    goto/16 :goto_9

    :cond_29
    iget-boolean v1, p0, Ldcu;->bsz:Z

    if-eqz v1, :cond_2a

    iget-boolean v1, p0, Ldcu;->bsB:Z

    if-nez v1, :cond_2b

    :cond_2a
    move v1, v2

    goto/16 :goto_9

    :cond_2b
    iget-object v1, p0, Ldcu;->mGsaConfigFlags:Lchk;

    invoke-virtual {v1}, Lchk;->Jg()Z

    move-result v1

    if-nez v1, :cond_2c

    move v1, v2

    goto/16 :goto_9

    :cond_2c
    move v1, v3

    goto/16 :goto_9

    .line 720
    :cond_2d
    iget-boolean v1, p0, Ldcu;->bsL:Z

    if-nez v1, :cond_2e

    move v1, v2

    goto/16 :goto_a

    :cond_2e
    iget-object v1, p0, Ldcu;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTS()I

    move-result v1

    if-eq v1, v10, :cond_2f

    move v1, v2

    goto/16 :goto_a

    :cond_2f
    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-static {}, Lbyl;->BM()Z

    move-result v1

    if-nez v1, :cond_30

    move v1, v2

    goto/16 :goto_a

    :cond_30
    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v1}, Lbyl;->BN()Z

    move-result v1

    if-eqz v1, :cond_31

    move v1, v2

    goto/16 :goto_a

    :cond_31
    iget-object v1, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-static {}, Lbyl;->BJ()Lhym;

    move-result-object v1

    invoke-static {}, Lbyl;->BK()Lcke;

    move-result-object v4

    invoke-interface {v4}, Lcke;->ND()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lhym;->oD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_32

    move v1, v2

    goto/16 :goto_a

    :cond_32
    iget-object v1, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aal()Ldby;

    move-result-object v1

    invoke-virtual {v1}, Ldby;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_33

    move v1, v2

    goto/16 :goto_a

    :cond_33
    iget-object v1, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aal()Ldby;

    move-result-object v1

    invoke-virtual {v1}, Ldby;->Wy()Z

    move-result v1

    if-nez v1, :cond_34

    move v1, v2

    goto/16 :goto_a

    :cond_34
    move v1, v3

    goto/16 :goto_a

    .line 734
    :cond_35
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x10

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    or-int/2addr v0, v1

    goto/16 :goto_b

    .line 740
    :cond_36
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x20

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v1

    or-int/2addr v0, v1

    goto/16 :goto_c

    :cond_37
    move v2, v3

    .line 743
    goto/16 :goto_d

    .line 763
    :cond_38
    iget-object v1, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    or-int/lit8 v1, v1, 0x0

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.USER_PRESENT"

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    goto/16 :goto_e

    :cond_39
    iget-object v4, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v5, "com.google.android.velvet.location.ACTIVITY_DETECTION"

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v4

    or-int/2addr v1, v4

    goto/16 :goto_f

    :cond_3a
    iget-object v4, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v5, "com.google.android.apps.gmm.NAVIGATION_STATE"

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v4

    or-int/2addr v1, v4

    goto/16 :goto_10

    :cond_3b
    iget-object v4, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v5, "android.provider.Telephony.SMS_RECEIVED"

    invoke-interface {v4, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    iput-boolean v3, p0, Ldcu;->bsK:Z

    move v1, v3

    goto/16 :goto_11

    :cond_3c
    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "android.intent.action.HEADSET_PLUG"

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    goto/16 :goto_12

    :cond_3d
    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    const-string v4, "com.google.android.apps.gmm.NAVIGATION_STATE"

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    or-int/2addr v1, v2

    goto/16 :goto_13

    :cond_3e
    move v3, v0

    goto/16 :goto_14

    :cond_3f
    move v1, v2

    goto/16 :goto_7

    .line 652
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private YJ()Z
    .locals 1

    .prologue
    .line 776
    invoke-direct {p0}, Ldcu;->YM()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ldcu;->YL()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ldcu;->YO()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private YK()V
    .locals 4

    .prologue
    .line 848
    iget-object v0, p0, Ldcu;->bsW:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x14

    if-lt v0, v1, :cond_0

    .line 849
    iget-object v0, p0, Ldcu;->bsW:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 851
    :cond_0
    iget-object v0, p0, Ldcu;->bsW:Ljava/util/List;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Ldcu;->bsO:I

    invoke-static {v2}, Lbwy;->em(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ldcu;->bsX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 853
    return-void
.end method

.method private YM()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 865
    iget-object v2, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v2}, Lbyl;->BV()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Ldcu;->bpH:Z

    if-eqz v2, :cond_0

    iget v2, p0, Ldcu;->bta:I

    if-ne v2, v0, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private YN()Z
    .locals 2

    .prologue
    .line 870
    iget-object v0, p0, Ldcu;->mSettings:Lhym;

    iget-object v1, p0, Ldcu;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhym;->oK(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private YS()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 980
    iget-object v0, p0, Ldcu;->mSearchSettings:Lcke;

    invoke-interface {v0}, Lcke;->ND()Ljava/lang/String;

    move-result-object v0

    .line 981
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private Zh()I
    .locals 1

    .prologue
    .line 1691
    iget-object v0, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v0}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/search/core/state/QueryState;->Xc()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private Zk()Ljava/util/Map;
    .locals 4

    .prologue
    .line 1889
    invoke-static {}, Lior;->aYa()Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 1890
    const-string v0, "mCurrentStartedState"

    iget v2, p0, Ldcu;->bsw:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1891
    const-string v0, "mNotificationFlags"

    iget v2, p0, Ldcu;->bsx:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1893
    const-string v0, "mEyesFreeBluetoothDeviceConnected"

    iget-boolean v2, p0, Ldcu;->bsC:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1894
    const-string v0, "mBluetoothDeviceSupportsBvra"

    iget-boolean v2, p0, Ldcu;->bsD:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895
    const-string v0, "Car Bluetooth connected"

    invoke-direct {p0}, Ldcu;->YN()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896
    const-string v0, "Connected BT address"

    iget-object v2, p0, Ldcu;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aUe()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1897
    const-string v0, "mWiredHeadsetConnected"

    iget-boolean v2, p0, Ldcu;->bsE:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1899
    const-string v0, "mCharging"

    iget-boolean v2, p0, Ldcu;->bsy:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1900
    const-string v0, "mScreenOn"

    iget-boolean v2, p0, Ldcu;->bsz:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1901
    const-string v0, "mLocked"

    iget-boolean v2, p0, Ldcu;->bsB:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1902
    const-string v0, "mDriving"

    iget-boolean v2, p0, Ldcu;->aNd:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1903
    const-string v2, "mManualCarModeState"

    iget v0, p0, Ldcu;->bsM:I

    if-nez v0, :cond_0

    const-string v0, "unspecified"

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1905
    const-string v0, "isInCarMode()"

    invoke-virtual {p0}, Ldcu;->YZ()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1906
    const-string v0, "car mode entry points"

    iget-object v2, p0, Ldcu;->bsV:Ljava/util/List;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1907
    const-string v0, "car mode exit points"

    iget-object v2, p0, Ldcu;->bsW:Ljava/util/List;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1908
    const-string v0, "mCanSafelyPerformHeadlessHotword"

    iget-boolean v2, p0, Ldcu;->bsL:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910
    const-string v0, "mShowHotwordPromoNotification"

    iget-boolean v2, p0, Ldcu;->bsP:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1911
    const-string v0, "mShowHotwordOnboardingNotification"

    iget-boolean v2, p0, Ldcu;->bsQ:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1912
    const-string v0, "mRegisteredActions"

    iget-object v2, p0, Ldcu;->bsI:Ljava/util/Set;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1913
    const-string v0, "mRegisteredActionsChanged"

    iget-boolean v2, p0, Ldcu;->bsJ:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1914
    const-string v0, "mActivityToSwitchToForResults"

    iget v2, p0, Ldcu;->bsF:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1915
    const-string v0, "mUiToLaunchForVoiceSearch"

    iget v2, p0, Ldcu;->bsH:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1916
    const-string v0, "mHandsFreeOverlayIsStarted"

    iget-boolean v2, p0, Ldcu;->bsT:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1917
    const-string v0, "mHandsFreeOverlayIsSuppressed"

    iget-boolean v2, p0, Ldcu;->bsY:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918
    const-string v0, "mGmmIsForegrounded"

    iget-boolean v2, p0, Ldcu;->aNV:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919
    const-string v0, "mGmmIsNavigating"

    iget-boolean v2, p0, Ldcu;->bpH:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920
    const-string v0, "mGmmNavigationTravelMode"

    iget v2, p0, Ldcu;->bta:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1921
    const-string v0, "mShowHotwordRetrainingNotification"

    iget-boolean v2, p0, Ldcu;->bsU:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1922
    const-string v0, "mCurrentNetworkType"

    iget v2, p0, Ldcu;->btb:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1924
    const-string v0, "Should Foreground for auto driving"

    invoke-virtual {p0}, Ldcu;->YL()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1925
    const-string v0, "Should Foreground for gmm navigation"

    invoke-direct {p0}, Ldcu;->YM()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1926
    const-string v0, "Should Foreground for in-car Bluetooth"

    invoke-virtual {p0}, Ldcu;->YO()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1928
    const-string v0, "Assist Package"

    invoke-virtual {p0}, Ldcu;->YP()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1929
    return-object v1

    .line 1903
    :cond_0
    iget v0, p0, Ldcu;->bsM:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    const-string v0, "on"

    goto/16 :goto_0

    :cond_1
    const-string v0, "off"

    goto/16 :goto_0
.end method

.method public static ak(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 990
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static ar(Lcom/google/android/shared/search/Query;)Z
    .locals 1

    .prologue
    .line 1687
    invoke-virtual {p0}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    return v0
.end method

.method private t(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0x200

    .line 926
    iget-object v0, p0, Ldcu;->bph:Landroid/os/Bundle;

    if-eq p1, v0, :cond_0

    .line 927
    iput-object p1, p0, Ldcu;->bph:Landroid/os/Bundle;

    .line 928
    iget-object v0, p0, Ldcu;->bph:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Ldcu;->u(Landroid/os/Bundle;)Lgnu;

    move-result-object v0

    iput-object v0, p0, Ldcu;->bfy:Lgnu;

    .line 930
    invoke-virtual {p0}, Ldcu;->YR()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Ldcu;->YS()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldcu;->mSearchSettings:Lcke;

    invoke-virtual {p0}, Ldcu;->YT()I

    move-result v1

    invoke-interface {v0, v1}, Lcke;->eY(I)I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 931
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    .line 942
    :goto_1
    invoke-direct {p0}, Ldcu;->YI()Z

    .line 943
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 945
    :cond_0
    return-void

    .line 930
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 933
    :cond_2
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    goto :goto_1
.end method

.method private u(Landroid/os/Bundle;)Lgnu;
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1832
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 1840
    :cond_0
    :goto_0
    return-object v0

    .line 1835
    :cond_1
    invoke-static {p1}, Ldcu;->v(Landroid/os/Bundle;)Lgnu;

    move-result-object v0

    .line 1836
    if-nez v0, :cond_2

    .line 1837
    new-instance v0, Lgnu;

    invoke-direct {v0}, Lgnu;-><init>()V

    .line 1840
    :cond_2
    invoke-static {p1}, Lhgn;->ae(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.extra.ASSIST_CONTEXT"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Lckx;->a(Ljava/lang/String;Landroid/os/Bundle;J)Ljjz;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v1, Lgnu;

    invoke-direct {v1}, Lgnu;-><init>()V

    iput-object v2, v1, Lgnu;->cQy:Ljjz;

    invoke-static {v1, v0}, Leqh;->b(Ljsr;Ljsr;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private static v(Landroid/os/Bundle;)Lgnu;
    .locals 6
    .param p0    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1873
    if-nez p0, :cond_1

    .line 1884
    :cond_0
    :goto_0
    return-object v0

    .line 1876
    :cond_1
    const-string v1, "com.google.android.ssb.extra.SSB_CONTEXT"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 1877
    if-eqz v1, :cond_0

    .line 1881
    :try_start_0
    invoke-static {v1}, Lgnu;->U([B)Lgnu;
    :try_end_0
    .catch Ljsq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1882
    :catch_0
    move-exception v1

    .line 1883
    const-string v2, "ServiceState"

    const-string v3, "Error while parsing ssb context proto %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljsq;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x5

    invoke-static {v1, v2, v3, v4}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final Bm()Lcom/google/android/search/shared/service/ClientConfig;
    .locals 1

    .prologue
    .line 948
    iget-object v0, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    return-object v0
.end method

.method public final UB()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Ldcu;->bph:Landroid/os/Bundle;

    return-object v0
.end method

.method public final UZ()V
    .locals 1

    .prologue
    .line 1579
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldcu;->bsY:Z

    .line 1581
    invoke-direct {p0}, Ldcu;->YI()Z

    .line 1582
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 1583
    return-void
.end method

.method public final Va()V
    .locals 1

    .prologue
    .line 1589
    const/4 v0, 0x0

    iput v0, p0, Ldcu;->bsM:I

    .line 1590
    invoke-direct {p0}, Ldcu;->YH()V

    .line 1591
    return-void
.end method

.method public final Vb()V
    .locals 0

    .prologue
    .line 1088
    invoke-direct {p0}, Ldcu;->YH()V

    .line 1089
    return-void
.end method

.method public final Ve()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1418
    iput-boolean v1, p0, Ldcu;->bsT:Z

    .line 1419
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldcu;->bsY:Z

    .line 1424
    invoke-virtual {p0}, Ldcu;->YZ()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1425
    const/4 v0, 0x3

    invoke-virtual {p0, v1, v0}, Ldcu;->c(ZI)V

    .line 1427
    :cond_0
    return-void
.end method

.method public final YA()Z
    .locals 1

    .prologue
    .line 367
    iget v0, p0, Ldcu;->bsv:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    iget-object v0, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v0}, Lcom/google/android/search/shared/service/ClientConfig;->YA()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final YB()I
    .locals 4

    .prologue
    .line 381
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x1ff

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    const/4 v0, 0x1

    .line 400
    :goto_0
    iget v1, p0, Ldcu;->bsw:I

    if-ne v0, v1, :cond_1

    .line 403
    const/4 v0, 0x2

    .line 407
    :goto_1
    return v0

    .line 397
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 406
    :cond_1
    iput v0, p0, Ldcu;->bsw:I

    goto :goto_1
.end method

.method public final YC()I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 428
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x7

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aP(J)Z

    move-result v3

    .line 433
    if-eqz v3, :cond_a

    .line 435
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x4

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 436
    const/4 v0, 0x5

    .line 439
    :goto_0
    iget-object v4, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x10

    invoke-virtual {v4, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x20

    invoke-virtual {v4, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 441
    :cond_0
    or-int/lit8 v0, v0, 0x10

    .line 443
    :cond_1
    iget-object v4, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v6, 0x8

    invoke-virtual {v4, v6, v7}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 444
    or-int/lit8 v0, v0, 0x8

    .line 452
    :cond_2
    :goto_1
    if-nez v3, :cond_7

    iget-boolean v3, p0, Ldcu;->bsP:Z

    if-nez v3, :cond_3

    iget-boolean v3, p0, Ldcu;->bsQ:Z

    if-nez v3, :cond_3

    iget-boolean v3, p0, Ldcu;->bsU:Z

    if-eqz v3, :cond_7

    .line 457
    :cond_3
    :goto_2
    if-eqz v1, :cond_6

    .line 459
    or-int/lit8 v0, v0, 0x2

    .line 460
    iget-boolean v1, p0, Ldcu;->bsQ:Z

    if-eqz v1, :cond_4

    .line 461
    or-int/lit8 v0, v0, 0x20

    .line 463
    :cond_4
    iget-boolean v1, p0, Ldcu;->bsP:Z

    if-eqz v1, :cond_5

    .line 464
    or-int/lit8 v0, v0, 0x40

    .line 466
    :cond_5
    iget-boolean v1, p0, Ldcu;->bsU:Z

    if-eqz v1, :cond_6

    .line 467
    or-int/lit16 v0, v0, 0x80

    .line 472
    :cond_6
    iget v1, p0, Ldcu;->bsx:I

    if-ne v1, v0, :cond_8

    .line 474
    const/4 v0, -0x1

    .line 478
    :goto_3
    return v0

    :cond_7
    move v1, v2

    .line 452
    goto :goto_2

    .line 477
    :cond_8
    iput v0, p0, Ldcu;->bsx:I

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_0

    :cond_a
    move v0, v2

    goto :goto_1
.end method

.method public final YD()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 565
    iget-boolean v1, p0, Ldcu;->bsR:Z

    if-eqz v1, :cond_0

    .line 566
    iput-boolean v0, p0, Ldcu;->bsR:Z

    .line 567
    const/4 v0, 0x1

    .line 569
    :cond_0
    return v0
.end method

.method public final YE()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 576
    iget-boolean v1, p0, Ldcu;->bsS:Z

    if-eqz v1, :cond_0

    .line 577
    iput-boolean v0, p0, Ldcu;->bsS:Z

    .line 578
    const/4 v0, 0x1

    .line 580
    :cond_0
    return v0
.end method

.method public final YF()Ljava/util/Set;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 598
    iget-boolean v0, p0, Ldcu;->bsJ:Z

    if-eqz v0, :cond_0

    .line 599
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldcu;->bsJ:Z

    .line 601
    iget-object v0, p0, Ldcu;->bsI:Ljava/util/Set;

    .line 604
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final YG()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 609
    iget-boolean v1, p0, Ldcu;->bsK:Z

    if-eqz v1, :cond_0

    .line 610
    iput-boolean v0, p0, Ldcu;->bsK:Z

    .line 611
    const/4 v0, 0x1

    .line 613
    :cond_0
    return v0
.end method

.method public final YL()Z
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v0}, Lbyl;->BU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldcu;->aNd:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldcu;->bsy:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final YO()Z
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v0}, Lbyl;->BV()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ldcu;->YN()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final YP()Ljava/lang/String;
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Ldcu;->bfy:Lgnu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcu;->bfy:Lgnu;

    iget-object v0, v0, Lgnu;->cQy:Ljjz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcu;->bfy:Lgnu;

    iget-object v0, v0, Lgnu;->cQy:Ljjz;

    iget-object v0, v0, Ljjz;->epG:Ljjo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcu;->bfy:Lgnu;

    iget-object v0, v0, Lgnu;->cQy:Ljjz;

    iget-object v0, v0, Ljjz;->epG:Ljjo;

    invoke-virtual {v0}, Ljjo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 964
    :cond_0
    const/4 v0, 0x0

    .line 966
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Ldcu;->bfy:Lgnu;

    iget-object v0, v0, Lgnu;->cQy:Ljjz;

    iget-object v0, v0, Ljjz;->epG:Ljjo;

    invoke-virtual {v0}, Ljjo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final YQ()Lgnu;
    .locals 1

    .prologue
    .line 970
    iget-object v0, p0, Ldcu;->bfy:Lgnu;

    return-object v0
.end method

.method public final YR()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 975
    iget-object v0, p0, Ldcu;->bfy:Lgnu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcu;->bfy:Lgnu;

    invoke-virtual {v0}, Lgnu;->aIb()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final YT()I
    .locals 2

    .prologue
    .line 985
    invoke-virtual {p0}, Ldcu;->YR()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Ldcu;->YS()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldcu;->ak(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final YU()Z
    .locals 4

    .prologue
    .line 1012
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x200

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    return v0
.end method

.method public final YV()Z
    .locals 1

    .prologue
    .line 1019
    iget-boolean v0, p0, Ldcu;->bsC:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldcu;->bsD:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final YW()I
    .locals 2

    .prologue
    .line 1556
    iget v0, p0, Ldcu;->btb:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1557
    iget v0, p0, Ldcu;->btb:I

    .line 1569
    :goto_0
    return v0

    .line 1562
    :cond_0
    iget-boolean v0, p0, Ldcu;->btc:Z

    if-nez v0, :cond_1

    .line 1563
    const v0, 0xf629c9

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 1564
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldcu;->btc:Z

    .line 1569
    :cond_1
    iget-object v0, p0, Ldcu;->mNetworkInformation:Lgno;

    invoke-virtual {v0}, Lgno;->aAn()I

    move-result v0

    goto :goto_0
.end method

.method public final YX()Z
    .locals 1

    .prologue
    .line 1599
    iget-boolean v0, p0, Ldcu;->bsC:Z

    return v0
.end method

.method public final YY()Z
    .locals 1

    .prologue
    .line 1607
    iget-boolean v0, p0, Ldcu;->bsE:Z

    return v0
.end method

.method public final YZ()Z
    .locals 4

    .prologue
    .line 1614
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    return v0
.end method

.method public final Yz()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 351
    iget-object v1, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    if-nez v1, :cond_1

    .line 362
    :cond_0
    :goto_0
    return v0

    .line 355
    :cond_1
    iget-object v1, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v1}, Lcom/google/android/search/shared/service/ClientConfig;->amU()Z

    move-result v1

    if-nez v1, :cond_0

    .line 362
    iget-object v1, p0, Ldcu;->mEventBus:Ldda;

    invoke-virtual {v1}, Ldda;->aai()Lcom/google/android/search/core/state/QueryState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/search/core/state/QueryState;->XW()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v1}, Lcom/google/android/search/shared/service/ClientConfig;->amT()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v2, 0x80

    invoke-virtual {v1, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final Za()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1621
    iget v1, p0, Ldcu;->bsM:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Zb()Z
    .locals 1

    .prologue
    .line 1628
    iget-boolean v0, p0, Ldcu;->aNd:Z

    return v0
.end method

.method public final Zc()Z
    .locals 1

    .prologue
    .line 1644
    iget-boolean v0, p0, Ldcu;->aNU:Z

    if-eqz v0, :cond_0

    .line 1646
    iget-boolean v0, p0, Ldcu;->bpH:Z

    .line 1653
    :goto_0
    return v0

    .line 1648
    :cond_0
    iget-object v0, p0, Ldcu;->mScreenStateHelper:Ldmh;

    invoke-virtual {v0}, Ldmh;->adr()Z

    move-result v0

    goto :goto_0
.end method

.method public final Zd()Z
    .locals 1

    .prologue
    .line 1659
    iget-boolean v0, p0, Ldcu;->bsT:Z

    return v0
.end method

.method public final Ze()Z
    .locals 1

    .prologue
    .line 1664
    iget-boolean v0, p0, Ldcu;->bsY:Z

    return v0
.end method

.method public final Zf()Z
    .locals 1

    .prologue
    .line 1671
    iget v0, p0, Ldcu;->bsF:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Zg()I
    .locals 1

    .prologue
    .line 1675
    iget v0, p0, Ldcu;->bsF:I

    return v0
.end method

.method public final Zi()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1785
    iget v0, p0, Ldcu;->bsH:I

    if-eqz v0, :cond_0

    .line 1786
    iget v0, p0, Ldcu;->bsH:I

    .line 1787
    iput v1, p0, Ldcu;->bsH:I

    .line 1790
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final Zj()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1811
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    const-wide/16 v4, 0x4

    invoke-virtual {v0, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v0}, Lbyl;->BO()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1817
    :goto_0
    iget-boolean v3, p0, Ldcu;->bsy:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Ldcu;->bsz:Z

    if-eqz v3, :cond_3

    :cond_0
    iget-object v3, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v3}, Lbyl;->BN()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v1

    .line 1820
    :goto_1
    iget-boolean v4, p0, Ldcu;->bsL:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    invoke-virtual {v4}, Lcom/google/android/search/shared/service/ClientConfig;->anf()Z

    move-result v4

    if-nez v4, :cond_4

    if-nez v0, :cond_1

    if-eqz v3, :cond_4

    :cond_1
    :goto_2
    return v1

    :cond_2
    move v0, v2

    .line 1811
    goto :goto_0

    :cond_3
    move v3, v2

    .line 1817
    goto :goto_1

    :cond_4
    move v1, v2

    .line 1820
    goto :goto_2
.end method

.method protected final a(Landroid/os/Bundle;I)V
    .locals 1

    .prologue
    .line 339
    iput p2, p0, Ldcu;->bsv:I

    .line 340
    const-string v0, "ServiceState:session_context"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Ldcu;->t(Landroid/os/Bundle;)V

    .line 341
    return-void
.end method

.method public final a(Lcom/google/android/search/shared/service/ClientConfig;)V
    .locals 1

    .prologue
    .line 914
    iget-object v0, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    if-eq p1, v0, :cond_0

    .line 915
    iput-object p1, p0, Ldcu;->mClientConfig:Lcom/google/android/search/shared/service/ClientConfig;

    .line 916
    invoke-direct {p0}, Ldcu;->YI()Z

    .line 917
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 919
    :cond_0
    return-void
.end method

.method public final a(Lddb;)V
    .locals 1

    .prologue
    .line 322
    invoke-virtual {p1}, Lddb;->aaB()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaz()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aaw()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lddb;->aax()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    :cond_0
    invoke-direct {p0}, Ldcu;->YH()V

    .line 328
    :cond_1
    return-void
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    .line 1939
    const-string v0, "ServiceState"

    invoke-virtual {p1, v0}, Letj;->lt(Ljava/lang/String;)V

    .line 1940
    invoke-direct {p0}, Ldcu;->Zk()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1941
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 1942
    instance-of v3, v1, Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    .line 1943
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v3

    move-object v0, v1

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v3, v0}, Letn;->ff(Z)V

    goto :goto_0

    .line 1944
    :cond_0
    instance-of v3, v1, Ljava/lang/Integer;

    if-eqz v3, :cond_1

    .line 1945
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Letn;->be(J)V

    goto :goto_0

    .line 1949
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Letn;->d(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 1952
    :cond_2
    return-void
.end method

.method public final a(ZJ)V
    .locals 2

    .prologue
    .line 1049
    iget-boolean v0, p0, Ldcu;->bsz:Z

    if-eq v0, p1, :cond_1

    .line 1050
    iput-boolean p1, p0, Ldcu;->bsz:Z

    .line 1051
    if-nez p1, :cond_0

    .line 1052
    iput-wide p2, p0, Ldcu;->bsA:J

    .line 1054
    :cond_0
    invoke-direct {p0}, Ldcu;->YH()V

    .line 1056
    :cond_1
    return-void
.end method

.method public final b(Landroid/os/Bundle;I)V
    .locals 0

    .prologue
    .line 345
    iput p2, p0, Ldcu;->bsv:I

    .line 346
    invoke-direct {p0, p1}, Ldcu;->t(Landroid/os/Bundle;)V

    .line 347
    return-void
.end method

.method public final b(ZZI)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1380
    iget-boolean v2, p0, Ldcu;->aNU:Z

    if-nez v2, :cond_0

    .line 1381
    iput-boolean v0, p0, Ldcu;->aNU:Z

    .line 1382
    iget-object v2, p0, Ldcu;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aUj()V

    .line 1385
    :cond_0
    iget-boolean v2, p0, Ldcu;->aNV:Z

    if-ne v2, p1, :cond_1

    iget-boolean v2, p0, Ldcu;->bpH:Z

    if-ne v2, p2, :cond_1

    iget v2, p0, Ldcu;->bta:I

    if-eq v2, p3, :cond_4

    .line 1389
    :cond_1
    iget-boolean v2, p0, Ldcu;->bpH:Z

    if-eqz v2, :cond_5

    if-nez p2, :cond_5

    .line 1391
    :goto_0
    if-eqz v0, :cond_2

    iget-boolean v2, p0, Ldcu;->bsY:Z

    if-eqz v2, :cond_2

    .line 1393
    iput-boolean v1, p0, Ldcu;->bsY:Z

    .line 1396
    :cond_2
    iput-boolean p1, p0, Ldcu;->aNV:Z

    .line 1397
    iput-boolean p2, p0, Ldcu;->bpH:Z

    .line 1398
    iput p3, p0, Ldcu;->bta:I

    .line 1402
    invoke-direct {p0}, Ldcu;->YI()Z

    move-result v1

    .line 1403
    if-nez v1, :cond_3

    if-eqz v0, :cond_4

    .line 1404
    :cond_3
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 1407
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 1389
    goto :goto_0
.end method

.method public final c(ZI)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1508
    if-eqz p1, :cond_0

    .line 1510
    const/4 v0, 0x1

    iput v0, p0, Ldcu;->bsM:I

    .line 1511
    iput p2, p0, Ldcu;->bsN:I

    .line 1525
    :goto_0
    iput-boolean v1, p0, Ldcu;->bsY:Z

    .line 1527
    invoke-direct {p0}, Ldcu;->YI()Z

    .line 1528
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 1529
    return-void

    .line 1512
    :cond_0
    invoke-direct {p0}, Ldcu;->YJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1515
    const/4 v0, 0x2

    iput v0, p0, Ldcu;->bsM:I

    .line 1516
    iput p2, p0, Ldcu;->bsO:I

    goto :goto_0

    .line 1520
    :cond_1
    iput v1, p0, Ldcu;->bsM:I

    .line 1521
    iput p2, p0, Ldcu;->bsO:I

    goto :goto_0
.end method

.method public final c(JJ)Z
    .locals 5

    .prologue
    .line 1064
    sub-long v0, p3, p1

    iget-wide v2, p0, Ldcu;->bsA:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final de(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1348
    iget-boolean v0, p0, Ldcu;->bsZ:Z

    if-eq v0, p1, :cond_2

    .line 1351
    iget-boolean v0, p0, Ldcu;->bsZ:Z

    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    const/4 v0, 0x1

    .line 1353
    :goto_0
    if-eqz v0, :cond_0

    iget-boolean v2, p0, Ldcu;->bsY:Z

    if-eqz v2, :cond_0

    .line 1355
    iput-boolean v1, p0, Ldcu;->bsY:Z

    .line 1358
    :cond_0
    iput-boolean p1, p0, Ldcu;->bsZ:Z

    .line 1361
    invoke-direct {p0}, Ldcu;->YI()Z

    move-result v1

    .line 1362
    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    .line 1363
    :cond_1
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 1366
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 1351
    goto :goto_0
.end method

.method public final dm(Z)V
    .locals 1

    .prologue
    .line 1032
    iget-boolean v0, p0, Ldcu;->bsy:Z

    if-eq v0, p1, :cond_0

    .line 1033
    iput-boolean p1, p0, Ldcu;->bsy:Z

    .line 1034
    invoke-direct {p0}, Ldcu;->YH()V

    .line 1036
    :cond_0
    return-void
.end method

.method public final dn(Z)V
    .locals 1

    .prologue
    .line 1074
    iget-boolean v0, p0, Ldcu;->bsB:Z

    if-eq v0, p1, :cond_0

    .line 1075
    iput-boolean p1, p0, Ldcu;->bsB:Z

    .line 1076
    invoke-direct {p0}, Ldcu;->YH()V

    .line 1078
    :cond_0
    return-void
.end method

.method public final do(Z)V
    .locals 1

    .prologue
    .line 1316
    iget-boolean v0, p0, Ldcu;->bsE:Z

    if-eq p1, v0, :cond_0

    .line 1317
    iput-boolean p1, p0, Ldcu;->bsE:Z

    .line 1318
    iget-object v0, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v0}, Lbyl;->BQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319
    invoke-direct {p0}, Ldcu;->YH()V

    .line 1322
    :cond_0
    return-void
.end method

.method public final dp(Z)V
    .locals 0

    .prologue
    .line 1330
    iput-boolean p1, p0, Ldcu;->aNd:Z

    .line 1331
    invoke-direct {p0}, Ldcu;->YI()Z

    .line 1338
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 1339
    return-void
.end method

.method public final dq(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1440
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldcu;->bsT:Z

    .line 1450
    if-eqz p1, :cond_0

    .line 1454
    iput v1, p0, Ldcu;->bsO:I

    .line 1455
    invoke-direct {p0}, Ldcu;->YK()V

    .line 1456
    iput-boolean v1, p0, Ldcu;->bsY:Z

    .line 1457
    invoke-direct {p0}, Ldcu;->YH()V

    .line 1459
    :cond_0
    return-void
.end method

.method public final dr(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x40

    .line 1474
    if-eqz p1, :cond_1

    .line 1476
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 1480
    :goto_0
    if-eqz v0, :cond_0

    .line 1481
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 1483
    :cond_0
    return-void

    .line 1478
    :cond_1
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method

.method public final ds(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x80

    .line 1490
    if-eqz p1, :cond_1

    .line 1492
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aR(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 1496
    :goto_0
    if-eqz v0, :cond_0

    .line 1497
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 1499
    :cond_0
    return-void

    .line 1494
    :cond_1
    iget-object v0, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/shared/util/BitFlags;->aQ(J)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method

.method public final fF(I)V
    .locals 1

    .prologue
    .line 1540
    iget v0, p0, Ldcu;->btb:I

    if-eq p1, v0, :cond_0

    .line 1541
    iput p1, p0, Ldcu;->btb:I

    .line 1542
    invoke-virtual {p0}, Ldcu;->notifyChanged()V

    .line 1544
    :cond_0
    return-void
.end method

.method public final l(ZZ)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x4

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 490
    iget-object v2, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v2}, Lbyl;->BT()Z

    move-result v2

    if-nez v2, :cond_1

    .line 557
    :cond_0
    :goto_0
    return v0

    .line 495
    :cond_1
    invoke-virtual {p0}, Ldcu;->Zc()Z

    move-result v2

    .line 507
    iget-boolean v3, p0, Ldcu;->bsY:Z

    if-nez v3, :cond_0

    .line 513
    iget-boolean v3, p0, Ldcu;->bsT:Z

    if-eqz v3, :cond_2

    .line 514
    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 521
    iput-boolean v0, p0, Ldcu;->bsT:Z

    .line 522
    iput-boolean v0, p0, Ldcu;->bsY:Z

    .line 523
    const/4 v0, 0x3

    goto :goto_0

    .line 528
    :cond_2
    iget v3, p0, Ldcu;->bsM:I

    if-eq v3, v1, :cond_3

    .line 531
    iget-object v3, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/shared/util/BitFlags;->aO(J)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 541
    if-nez p2, :cond_4

    if-eqz p1, :cond_4

    if-nez v2, :cond_4

    iget-boolean v2, p0, Ldcu;->bsz:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Ldcu;->bsy:Z

    if-eqz v2, :cond_4

    :cond_3
    move v2, v1

    .line 549
    :goto_1
    if-eqz v2, :cond_0

    .line 556
    iput-boolean v1, p0, Ldcu;->bsT:Z

    move v0, v1

    .line 557
    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1
.end method

.method public final m(ZZ)V
    .locals 1

    .prologue
    .line 1301
    iput-boolean p2, p0, Ldcu;->bsD:Z

    .line 1302
    iget-boolean v0, p0, Ldcu;->bsC:Z

    if-eq v0, p1, :cond_1

    .line 1303
    iput-boolean p1, p0, Ldcu;->bsC:Z

    .line 1304
    iget-object v0, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v0}, Lbyl;->BQ()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldcu;->mSettingsUtil:Lbyl;

    invoke-virtual {v0}, Lbyl;->BV()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1306
    :cond_0
    invoke-direct {p0}, Ldcu;->YH()V

    .line 1309
    :cond_1
    return-void
.end method

.method protected final s(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Ldcu;->bph:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 333
    const-string v0, "ServiceState:session_context"

    iget-object v1, p0, Ldcu;->bph:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 335
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1934
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ServiceState(state="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Ldcu;->Zk()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldcu;->bqt:Lcom/google/android/shared/util/BitFlags;

    invoke-virtual {v1}, Lcom/google/android/shared/util/BitFlags;->auA()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
