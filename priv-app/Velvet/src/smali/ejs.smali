.class public final Lejs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private OD:F

.field private final cbO:I

.field private final cbP:Lejt;

.field private final cbQ:I

.field private cbR:Z

.field private cbS:Z

.field private cbT:Z

.field private cbU:F

.field private cbV:F

.field private oS:F

.field private oT:F


# direct methods
.method public constructor <init>(IILejt;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lejs;->cbO:I

    .line 40
    iput-object p3, p0, Lejs;->cbP:Lejt;

    .line 41
    mul-int v0, p2, p2

    iput v0, p0, Lejs;->cbQ:I

    .line 42
    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 47
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 48
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 110
    :goto_1
    return v0

    .line 50
    :pswitch_0
    iput-boolean v0, p0, Lejs;->cbR:Z

    .line 51
    iput-boolean v0, p0, Lejs;->cbS:Z

    .line 52
    iput-boolean v0, p0, Lejs;->cbT:Z

    .line 53
    iput v2, p0, Lejs;->oS:F

    .line 54
    iput v3, p0, Lejs;->oT:F

    .line 55
    iput v2, p0, Lejs;->cbV:F

    goto :goto_1

    .line 58
    :pswitch_1
    iget-boolean v4, p0, Lejs;->cbR:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lejs;->cbS:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lejs;->cbT:Z

    if-eqz v4, :cond_5

    .line 60
    :cond_1
    iget v0, p0, Lejs;->cbV:F

    sub-float v0, v2, v0

    iput v0, p0, Lejs;->OD:F

    .line 61
    iget v0, p0, Lejs;->OD:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_2

    .line 62
    iget-object v0, p0, Lejs;->cbP:Lejt;

    iget v4, p0, Lejs;->OD:F

    invoke-interface {v0, v4}, Lejt;->z(F)V

    .line 64
    :cond_2
    iput v2, p0, Lejs;->cbV:F

    .line 65
    iget v0, p0, Lejs;->cbU:F

    iget v4, p0, Lejs;->OD:F

    add-float/2addr v0, v4

    iput v0, p0, Lejs;->cbU:F

    .line 67
    iget v0, p0, Lejs;->oS:F

    sub-float v0, v2, v0

    .line 68
    iget v2, p0, Lejs;->oT:F

    sub-float v2, v3, v2

    .line 69
    mul-float v3, v0, v0

    mul-float v4, v2, v2

    add-float/2addr v3, v4

    .line 71
    iget v4, p0, Lejs;->cbQ:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 72
    iput-boolean v1, p0, Lejs;->cbR:Z

    .line 76
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 77
    iput-boolean v1, p0, Lejs;->cbS:Z

    .line 78
    iput-boolean v1, p0, Lejs;->cbT:Z

    goto :goto_0

    .line 79
    :cond_3
    cmpg-float v0, v0, v5

    if-gez v0, :cond_4

    .line 80
    iput-boolean v1, p0, Lejs;->cbT:Z

    goto :goto_0

    .line 82
    :cond_4
    iput-boolean v1, p0, Lejs;->cbS:Z

    goto :goto_0

    .line 89
    :cond_5
    :pswitch_2
    iget-boolean v2, p0, Lejs;->cbR:Z

    if-eqz v2, :cond_7

    .line 90
    iput-boolean v1, p0, Lejs;->cbR:Z

    .line 91
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lejs;->cbO:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    iget v4, p0, Lejs;->cbO:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_9

    :cond_6
    :goto_2
    if-nez v0, :cond_7

    .line 93
    iget-object v0, p0, Lejs;->cbP:Lejt;

    invoke-interface {v0}, Lejt;->onClick()V

    .line 96
    :cond_7
    iget v0, p0, Lejs;->cbU:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_8

    .line 97
    iget-object v0, p0, Lejs;->cbP:Lejt;

    iget v2, p0, Lejs;->cbU:F

    invoke-interface {v0, v2}, Lejt;->A(F)V

    .line 98
    iput v5, p0, Lejs;->cbU:F

    .line 100
    :cond_8
    iget-boolean v0, p0, Lejs;->cbS:Z

    if-eqz v0, :cond_a

    .line 101
    iget-object v0, p0, Lejs;->cbP:Lejt;

    invoke-interface {v0}, Lejt;->Bu()V

    goto/16 :goto_0

    .line 91
    :cond_9
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lejs;->cbO:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    iget v4, p0, Lejs;->cbO:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_6

    move v0, v1

    goto :goto_2

    .line 102
    :cond_a
    iget-boolean v0, p0, Lejs;->cbT:Z

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lejs;->cbP:Lejt;

    invoke-interface {v0}, Lejt;->Bv()V

    goto/16 :goto_0

    .line 107
    :pswitch_3
    iput-boolean v1, p0, Lejs;->cbR:Z

    goto/16 :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
