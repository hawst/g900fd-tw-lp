.class public final Leds;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private bKr:Ljava/lang/CharSequence;

.field private bVR:Z

.field public bVS:Z

.field public bVT:Z

.field public bVU:Landroid/widget/TextView;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public bbz:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/search/shared/actions/VoiceAction;Lcom/google/android/search/shared/actions/utils/CardDecision;Z)I
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 207
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    .line 209
    instance-of v0, p0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    if-nez v0, :cond_1

    .line 210
    if-eqz v5, :cond_0

    move v0, v1

    .line 253
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 213
    goto :goto_0

    :cond_1
    move-object v0, p0

    .line 218
    check-cast v0, Lcom/google/android/search/shared/actions/modular/ModularAction;

    .line 220
    if-eqz p2, :cond_2

    invoke-interface {p0}, Lcom/google/android/search/shared/actions/VoiceAction;->agz()Z

    move-result v6

    if-nez v6, :cond_2

    .line 222
    const/4 v0, 0x4

    goto :goto_0

    .line 223
    :cond_2
    if-eqz v5, :cond_3

    move v0, v1

    .line 225
    goto :goto_0

    .line 226
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ago()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alk()I

    move-result v5

    if-eq v5, v2, :cond_4

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alg()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v0, v3

    .line 231
    goto :goto_0

    .line 235
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->alm()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 236
    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->all()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/search/shared/actions/modular/ModularAction;->gC(I)Lcom/google/android/search/shared/actions/modular/arguments/Argument;

    move-result-object v2

    .line 237
    if-nez v2, :cond_6

    .line 238
    invoke-virtual {v0}, Lcom/google/android/search/shared/actions/modular/ModularAction;->ajj()Ldvv;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/utils/CardDecision;->all()I

    move-result v1

    invoke-virtual {v0, v1}, Ldvv;->gE(I)V

    move v0, v3

    .line 239
    goto :goto_0

    .line 242
    :cond_6
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->ajC()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v4

    .line 243
    goto :goto_0

    .line 246
    :cond_7
    invoke-virtual {v2}, Lcom/google/android/search/shared/actions/modular/arguments/Argument;->Pd()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v4

    .line 247
    goto :goto_0

    :cond_8
    move v0, v1

    .line 253
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 87
    iget-object v0, p0, Leds;->bVU:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move v0, v1

    .line 107
    :goto_0
    return v0

    .line 91
    :cond_0
    const v0, 0x7f04003f

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leds;->bVU:Landroid/widget/TextView;

    .line 95
    new-instance v0, Lekm;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v0, v3, v4, v1}, Lekm;-><init>(III)V

    .line 98
    sget-object v3, Lekn;->cdM:Lekn;

    iput-object v3, v0, Lekm;->cdz:Lekn;

    .line 99
    const/high16 v3, -0x3df00000    # -36.0f

    iget-object v4, p0, Leds;->bVU:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    iput v3, v0, Lekm;->bottomMargin:I

    .line 103
    iput-boolean v1, v0, Lekm;->cdu:Z

    .line 105
    iget-object v1, p0, Leds;->bVU:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    iget-object v0, p0, Leds;->bVU:Landroid/widget/TextView;

    const v1, 0x7f0c0164

    invoke-static {v0, v1}, Lehd;->r(Landroid/view/View;I)V

    move v0, v2

    .line 107
    goto :goto_0
.end method

.method public final apply()V
    .locals 2

    .prologue
    .line 184
    iget-boolean v0, p0, Leds;->bbz:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leds;->bVU:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Leds;->bVS:Z

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Leds;->bVU:Landroid/widget/TextView;

    iget-object v1, p0, Leds;->bKr:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Leds;->bVU:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setActivated(Z)V

    .line 188
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Leds;->bbz:Z

    .line 189
    return-void
.end method

.method public final eL(Z)Z
    .locals 2

    .prologue
    .line 128
    iget-boolean v0, p0, Leds;->bVR:Z

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    .line 129
    :goto_0
    iget-boolean v1, p0, Leds;->bbz:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Leds;->bbz:Z

    .line 130
    iput-boolean p1, p0, Leds;->bVR:Z

    .line 131
    return v0

    .line 128
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final eM(Z)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 142
    iget-boolean v0, p0, Leds;->bVS:Z

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    .line 143
    :goto_0
    iget-boolean v2, p0, Leds;->bbz:Z

    or-int/2addr v2, v0

    iput-boolean v2, p0, Leds;->bbz:Z

    .line 144
    iput-boolean p1, p0, Leds;->bVS:Z

    .line 145
    if-nez p1, :cond_0

    .line 146
    iput-boolean v1, p0, Leds;->bVT:Z

    .line 148
    :cond_0
    return v0

    :cond_1
    move v0, v1

    .line 142
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VoiceOfGooglePresenter[text="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Leds;->bKr:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", speaking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Leds;->bVR:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", shouldShow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Leds;->bVS:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", shouldRedeal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Leds;->bVT:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", changed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Leds;->bbz:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v(Ljava/lang/CharSequence;)Z
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Leds;->bKr:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 119
    :goto_0
    iget-boolean v1, p0, Leds;->bbz:Z

    or-int/2addr v1, v0

    iput-boolean v1, p0, Leds;->bbz:Z

    .line 120
    iput-object p1, p0, Leds;->bKr:Ljava/lang/CharSequence;

    .line 121
    return v0

    .line 118
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
