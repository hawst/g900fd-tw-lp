.class public Lcmq;
.super Ldmo;
.source "PG"

# interfaces
.implements Lcmz;
.implements Letz;


# instance fields
.field private So:I

.field private final bch:Lcnb;

.field private final bci:I

.field private final bcj:D

.field private final bck:Lcmy;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bcl:Ljava/lang/Object;

.field private bcm:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private bcn:Z

.field private final bco:Lcom/google/android/shared/search/Query;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final bcp:Lcmr;

.field private bcq:Lcqt;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bcr:Ljyl;

.field private bcs:I

.field private bct:I

.field private mActionData:Lcom/google/android/velvet/ActionData;

.field private mCorporaOrder:[I
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private mScrollPaddingConfirmed:Z

.field private mSrpMetadata:Lcms;

.field private mWebPage:Ldyo;


# direct methods
.method private constructor <init>(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;JLcmy;Ljava/util/concurrent/Executor;I)V
    .locals 11
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lcmy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 294
    invoke-static/range {p6 .. p6}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v2}, Ldmo;-><init>(Ljava/util/concurrent/Executor;)V

    .line 99
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcmq;->bcl:Ljava/lang/Object;

    .line 133
    new-instance v2, Lcmr;

    invoke-direct {v2}, Lcmr;-><init>()V

    iput-object v2, p0, Lcmq;->bcp:Lcmr;

    .line 175
    const/16 v2, -0xa

    iput v2, p0, Lcmq;->bct:I

    .line 295
    if-eqz p1, :cond_0

    const/4 v2, 0x1

    move v3, v2

    :goto_0
    if-eqz p2, :cond_1

    const/4 v2, 0x1

    :goto_1
    xor-int/2addr v2, v3

    invoke-static {v2}, Lifv;->gX(Z)V

    .line 297
    if-eqz p1, :cond_2

    .line 299
    iput-object p1, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    .line 300
    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v2, p0, Lcmq;->bco:Lcom/google/android/shared/search/Query;

    .line 303
    new-instance v3, Lcnb;

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    const/4 v10, 0x0

    move-wide v8, p3

    invoke-direct/range {v3 .. v10}, Lcnb;-><init>(JJJZ)V

    iput-object v3, p0, Lcmq;->bch:Lcnb;

    .line 315
    :goto_2
    invoke-static/range {p5 .. p5}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcmy;

    iput-object v2, p0, Lcmq;->bck:Lcmy;

    .line 317
    if-eqz p7, :cond_3

    .line 318
    move/from16 v0, p7

    iput v0, p0, Lcmq;->bci:I

    .line 319
    const-wide v2, 0x3fb999999999999aL    # 0.1

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    iget v4, p0, Lcmq;->bci:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcmq;->bcj:D

    .line 324
    :goto_3
    return-void

    .line 295
    :cond_0
    const/4 v2, 0x0

    move v3, v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 306
    :cond_2
    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    iput-object v2, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    .line 307
    iput-object p2, p0, Lcmq;->bco:Lcom/google/android/shared/search/Query;

    .line 311
    new-instance v3, Lcnb;

    sget-object v2, Leoi;->cgG:Leoi;

    invoke-virtual {v2}, Leoi;->auU()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/google/android/shared/search/Query;->AF()J

    move-result-wide v6

    const/4 v10, 0x1

    move-wide v8, p3

    invoke-direct/range {v3 .. v10}, Lcnb;-><init>(JJJZ)V

    iput-object v3, p0, Lcmq;->bch:Lcnb;

    goto :goto_2

    .line 321
    :cond_3
    const/4 v2, 0x0

    iput v2, p0, Lcmq;->bci:I

    .line 322
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcmq;->bcj:D

    goto :goto_3
.end method

.method private Qy()V
    .locals 0

    .prologue
    .line 513
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 514
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 515
    return-void
.end method

.method public static a(Lcom/google/android/shared/search/Query;JLcmy;Ljava/util/concurrent/Executor;I)Lcmq;
    .locals 9
    .param p0    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcmy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 271
    new-instance v1, Lcmq;

    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/shared/search/Query;

    const/4 v3, 0x0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lcmq;-><init>(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;JLcmy;Ljava/util/concurrent/Executor;I)V

    return-object v1
.end method

.method public static b(Lcom/google/android/shared/search/Query;JLcmy;Ljava/util/concurrent/Executor;I)Lcmq;
    .locals 9
    .param p0    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lcmy;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 287
    new-instance v1, Lcmq;

    const/4 v2, 0x0

    move-object v3, p0

    move-wide v4, p1

    move-object v6, p3

    move-object v7, p4

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lcmq;-><init>(Lcom/google/android/shared/search/Query;Lcom/google/android/shared/search/Query;JLcmy;Ljava/util/concurrent/Executor;I)V

    return-object v1
.end method

.method private ff(I)V
    .locals 4

    .prologue
    .line 505
    invoke-static {p1}, Lege;->hs(I)Litu;

    move-result-object v0

    iget-object v1, p0, Lcmq;->bch:Lcnb;

    iget-wide v2, v1, Lcnb;->amT:J

    invoke-static {v2, v3}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pL(Ljava/lang/String;)Litu;

    move-result-object v0

    iget-object v1, p0, Lcmq;->bch:Lcnb;

    iget-wide v2, v1, Lcnb;->bcK:J

    invoke-static {v2, v3}, Leoi;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Litu;->pM(Ljava/lang/String;)Litu;

    move-result-object v1

    .line 508
    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    iget-object v0, v0, Lcms;->bcv:Ljava/lang/String;

    :goto_0
    invoke-static {v1, v0}, Lege;->a(Litu;Ljava/lang/String;)V

    .line 510
    return-void

    .line 508
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final OO()Ldyo;
    .locals 2

    .prologue
    .line 413
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 414
    :try_start_0
    iget-object v0, p0, Lcmq;->mWebPage:Ldyo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qh()Z
    .locals 2

    .prologue
    .line 331
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 332
    :try_start_0
    iget-object v0, p0, Lcmq;->bcq:Lcqt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qi()Lcom/google/android/shared/search/Query;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 341
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 342
    :try_start_0
    iget-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qj()Z
    .locals 2

    .prologue
    .line 350
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 351
    :try_start_0
    iget-boolean v0, p0, Lcmq;->mScrollPaddingConfirmed:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 352
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qk()[I
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 360
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 361
    :try_start_0
    iget-object v0, p0, Lcmq;->mCorporaOrder:[I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Ql()Lcnb;
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcmq;->bch:Lcnb;

    return-object v0
.end method

.method public final Qm()Z
    .locals 2

    .prologue
    .line 370
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 371
    :try_start_0
    iget-boolean v0, p0, Lcmq;->bcn:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 372
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qn()Z
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->Qn()Z

    move-result v0

    return v0
.end method

.method public final Qo()Lefq;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->Qo()Lefq;

    move-result-object v0

    return-object v0
.end method

.method public final Qp()Z
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QB()Z

    move-result v0

    return v0
.end method

.method public final Qq()V
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcmq;->bck:Lcmy;

    invoke-virtual {v0, p0}, Lcmy;->a(Lcmz;)V

    .line 407
    return-void
.end method

.method public final Qr()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 424
    iget-object v1, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v1}, Lcmr;->QB()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 428
    :goto_0
    return v0

    .line 427
    :cond_0
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 428
    :try_start_0
    iget-object v2, p0, Lcmq;->mWebPage:Ldyo;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    sget-object v3, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-eq v2, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 429
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qs()Lcom/google/android/velvet/ActionData;
    .locals 2

    .prologue
    .line 433
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 434
    :try_start_0
    iget-object v0, p0, Lcmq;->mActionData:Lcom/google/android/velvet/ActionData;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 435
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qt()Lcms;
    .locals 2

    .prologue
    .line 439
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 440
    :try_start_0
    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 441
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qu()Lcqt;
    .locals 2

    .prologue
    .line 445
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 446
    :try_start_0
    iget-object v0, p0, Lcmq;->bcq:Lcqt;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 447
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qv()Ljyl;
    .locals 2

    .prologue
    .line 451
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 452
    :try_start_0
    iget-object v0, p0, Lcmq;->bcr:Ljyl;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final Qw()V
    .locals 3

    .prologue
    .line 467
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcmr;->a(ILefq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    const/16 v0, 0xfd

    invoke-direct {p0, v0}, Lcmq;->ff(I)V

    .line 469
    invoke-direct {p0}, Lcmq;->Qy()V

    .line 470
    iget-object v0, p0, Lcmq;->bck:Lcmy;

    invoke-virtual {v0}, Lcmy;->cancel()V

    .line 472
    :cond_0
    return-void
.end method

.method public final Qx()V
    .locals 3

    .prologue
    .line 486
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcmr;->a(ILefq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 487
    const/16 v0, 0xfb

    invoke-direct {p0, v0}, Lcmq;->ff(I)V

    .line 488
    invoke-direct {p0}, Lcmq;->Qy()V

    .line 490
    :cond_0
    return-void
.end method

.method public final Qz()V
    .locals 5

    .prologue
    .line 705
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 707
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 708
    :try_start_0
    iget-boolean v0, p0, Lcmq;->mScrollPaddingConfirmed:Z

    if-eqz v0, :cond_0

    .line 709
    const-string v0, "Velvet.SearchResult"

    const-string v2, "Confirmed scroll padding multiple times."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 711
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmq;->mScrollPaddingConfirmed:Z

    .line 712
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 713
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 714
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 716
    :cond_1
    return-void

    .line 712
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcqt;)V
    .locals 5

    .prologue
    .line 605
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 607
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 608
    :try_start_0
    iget-object v0, p0, Lcmq;->bcq:Lcqt;

    if-eqz v0, :cond_0

    .line 611
    const v0, 0xaeb92d

    invoke-static {v0}, Lhwt;->lx(I)V

    .line 612
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Old: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcmq;->bcq:Lcqt;

    iget-object v2, v2, Lcqt;->bgF:Ljava/lang/String;

    const/16 v3, 0x1f4

    invoke-static {v2, v3}, Lesp;->z(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", New: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcqt;->bgF:Ljava/lang/String;

    const/16 v3, 0x1f4

    invoke-static {v2, v3}, Lesp;->z(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 615
    iget-object v2, p0, Lcmq;->bcq:Lcqt;

    iget-object v2, v2, Lcqt;->baD:Ljava/lang/String;

    iget-object v3, p1, Lcqt;->baD:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 618
    const-string v2, "Velvet.SearchResult"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received multiple SuggestionResponse objects in the same response. "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v2, v0, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 626
    :cond_0
    :goto_0
    iput-object p1, p0, Lcmq;->bcq:Lcqt;

    .line 627
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 628
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 629
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 631
    :cond_1
    return-void

    .line 622
    :cond_2
    :try_start_1
    const-string v2, "Velvet.SearchResult"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received multiple SuggestionResponse objects from different responses. "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v2, v0, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 627
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ldyo;)V
    .locals 3

    .prologue
    .line 494
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcmr;->a(ILefq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    invoke-virtual {p0, p1}, Lcmq;->b(Ldyo;)V

    .line 496
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 497
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcmq;->bcn:Z

    .line 498
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    :cond_0
    invoke-direct {p0}, Lcmq;->Qy()V

    .line 501
    return-void

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Letj;)V
    .locals 6

    .prologue
    .line 748
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 749
    :try_start_0
    iget-object v0, p0, Lcmq;->bco:Lcom/google/android/shared/search/Query;

    .line 750
    iget-object v2, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    .line 751
    iget-object v3, p0, Lcmq;->mWebPage:Ldyo;

    .line 752
    iget-object v4, p0, Lcmq;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 753
    iget-object v5, p0, Lcmq;->mSrpMetadata:Lcms;

    .line 754
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 756
    const-string v1, "SearchResult"

    invoke-virtual {p1, v1}, Letj;->lt(Ljava/lang/String;)V

    .line 757
    const-string v1, "mSuggestionsQuery"

    invoke-virtual {p1, v1}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v1

    invoke-virtual {v1, v0}, Letn;->b(Leti;)V

    .line 758
    const-string v0, "mSrpQuery"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {v0, v2}, Letn;->b(Leti;)V

    .line 759
    const-string v0, "mActionData"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {v0, v4}, Letn;->b(Leti;)V

    .line 760
    const-string v0, "mWebPage"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {v0, v3}, Letn;->b(Leti;)V

    .line 761
    const-string v0, "mState"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    iget-object v1, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0, v1}, Letn;->b(Leti;)V

    .line 762
    const-string v0, "mSrpMetadta"

    invoke-virtual {p1, v0}, Letj;->lw(Ljava/lang/String;)Letn;

    move-result-object v0

    invoke-virtual {v0, v5}, Letn;->b(Leti;)V

    .line 763
    return-void

    .line 754
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcms;)V
    .locals 5

    .prologue
    .line 540
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QB()Z

    move-result v0

    if-nez v0, :cond_3

    .line 545
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 546
    :try_start_0
    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    if-eqz v0, :cond_0

    .line 547
    const-string v0, "Velvet.SearchResult"

    const-string v2, "Received multiple SrpMetadata objects"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 549
    :cond_0
    iput-object p1, p0, Lcmq;->mSrpMetadata:Lcms;

    .line 551
    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    iget-object v0, v0, Lcms;->bcw:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    iget-object v0, v0, Lcms;->bcw:Ljava/lang/String;

    iget-object v2, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 554
    iget-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    iget-object v2, p0, Lcmq;->mSrpMetadata:Lcms;

    iget-object v2, v2, Lcms;->bcw:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/search/Query;->z(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    .line 556
    :cond_1
    iget-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->apF()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 557
    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    iget-object v0, v0, Lcms;->bcx:Ljtg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    const-class v2, Lcom/google/android/shared/util/VoiceCorrectionSpan;

    invoke-virtual {v0, v2}, Lcom/google/android/shared/search/Query;->b(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 559
    iget-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    iget-object v2, p0, Lcmq;->mSrpMetadata:Lcms;

    iget-object v2, v2, Lcms;->bcx:Ljtg;

    invoke-static {v0, v2}, Ldmt;->a(Lcom/google/android/shared/search/Query;Ljtg;)Landroid/text/Spanned;

    move-result-object v0

    .line 561
    if-eqz v0, :cond_2

    .line 562
    iget-object v2, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    invoke-virtual {v2, v0}, Lcom/google/android/shared/search/Query;->y(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    .line 566
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 567
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 568
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 570
    :cond_3
    return-void

    .line 566
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/velvet/ActionData;)V
    .locals 5

    .prologue
    .line 590
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 592
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 593
    :try_start_0
    iget-object v0, p0, Lcmq;->mActionData:Lcom/google/android/velvet/ActionData;

    if-eqz v0, :cond_0

    .line 594
    const-string v0, "Velvet.SearchResult"

    const-string v2, "Received multiple ActionData objects"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 596
    :cond_0
    iput-object p1, p0, Lcmq;->mActionData:Lcom/google/android/velvet/ActionData;

    .line 597
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 598
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 599
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 601
    :cond_1
    return-void

    .line 597
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ldyo;)V
    .locals 5
    .param p1    # Ldyo;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 522
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 525
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 526
    :try_start_0
    iget-object v0, p0, Lcmq;->mWebPage:Ldyo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QA()Z

    move-result v0

    if-nez v0, :cond_0

    .line 527
    const-string v0, "Velvet.SearchResult"

    const-string v2, "Received multiple WebPage objects when not reloading"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 529
    :cond_0
    iput-object p1, p0, Lcmq;->mWebPage:Ldyo;

    .line 530
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 531
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 532
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 536
    :goto_0
    return-void

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 534
    :cond_1
    const-string v0, "Velvet.SearchResult"

    const-string v1, "Received WebPage after failed or cancelled"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Ljyl;)V
    .locals 2

    .prologue
    .line 665
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QB()Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 668
    :try_start_0
    iput-object p1, p0, Lcmq;->bcr:Ljyl;

    .line 669
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 671
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 673
    :cond_0
    return-void

    .line 669
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljyw;)V
    .locals 4

    .prologue
    .line 574
    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    if-nez v0, :cond_0

    .line 579
    const-string v0, "Velvet.SearchResult"

    const-string v1, "Update metadata (MDP) before setting metadata (EOC)!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 586
    :goto_0
    return-void

    .line 582
    :cond_0
    iget-object v0, p0, Lcmq;->mSrpMetadata:Lcms;

    invoke-virtual {v0, p1}, Lcms;->a(Ljyw;)V

    goto :goto_0
.end method

.method public final c(Lefq;)V
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcmr;->a(ILefq;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    const/16 v0, 0xfc

    invoke-direct {p0, v0}, Lcmq;->ff(I)V

    .line 479
    invoke-direct {p0}, Lcmq;->Qy()V

    .line 481
    :cond_0
    return-void
.end method

.method public final fe(I)V
    .locals 8

    .prologue
    .line 678
    iget v0, p0, Lcmq;->bci:I

    if-eqz v0, :cond_0

    .line 679
    iget v0, p0, Lcmq;->bcs:I

    add-int/2addr v0, p1

    iput v0, p0, Lcmq;->bcs:I

    .line 680
    iget v0, p0, Lcmq;->bcs:I

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    int-to-double v0, v0

    iget-wide v6, p0, Lcmq;->bcj:D

    mul-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    sub-double v0, v4, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcmq;->So:I

    .line 682
    iget v0, p0, Lcmq;->So:I

    iget v1, p0, Lcmq;->bct:I

    sub-int/2addr v0, v1

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 683
    iget v0, p0, Lcmq;->So:I

    iput v0, p0, Lcmq;->bct:I

    .line 684
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 685
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 688
    :cond_0
    return-void
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 901
    const-string v0, "Velvet.SearchResult"

    return-object v0
.end method

.method public final getProgress()I
    .locals 1

    .prologue
    .line 457
    iget v0, p0, Lcmq;->So:I

    return v0
.end method

.method public final h([I)V
    .locals 5

    .prologue
    .line 724
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->QB()Z

    move-result v0

    if-nez v0, :cond_1

    .line 726
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 727
    :try_start_0
    iget-object v0, p0, Lcmq;->mCorporaOrder:[I

    if-eqz v0, :cond_0

    .line 728
    const-string v0, "Velvet.SearchResult"

    const-string v2, "Received multiple setCorporaOrder calls"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 730
    :cond_0
    iput-object p1, p0, Lcmq;->mCorporaOrder:[I

    .line 731
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 732
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 733
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 735
    :cond_1
    return-void

    .line 731
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final hr(Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 635
    iget-object v2, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v2}, Lcmr;->QB()Z

    move-result v2

    if-nez v2, :cond_1

    .line 641
    iget-object v2, p0, Lcmq;->bco:Lcom/google/android/shared/search/Query;

    sget-object v3, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-eq v2, v3, :cond_2

    .line 642
    :goto_0
    if-eqz v0, :cond_1

    .line 643
    iget-object v1, p0, Lcmq;->bcl:Ljava/lang/Object;

    monitor-enter v1

    .line 644
    :try_start_0
    iget-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    sget-object v2, Lcom/google/android/shared/search/Query;->bZl:Lcom/google/android/shared/search/Query;

    if-ne v0, v2, :cond_3

    .line 647
    iget-object v0, p0, Lcmq;->bco:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, p1}, Lcom/google/android/shared/search/Query;->B(Ljava/lang/CharSequence;)Lcom/google/android/shared/search/Query;

    move-result-object v0

    iput-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    .line 656
    :cond_0
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 657
    invoke-virtual {p0}, Lcmq;->setChanged()V

    .line 658
    invoke-virtual {p0}, Lcmq;->notifyObservers()V

    .line 661
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 641
    goto :goto_0

    .line 649
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0}, Lcom/google/android/shared/search/Query;->aqN()Ljava/lang/String;

    move-result-object v0

    .line 650
    const/16 v2, 0x20

    invoke-static {p1, v2}, Letd;->d(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v2

    .line 651
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 652
    const-string v3, "Velvet.SearchResult"

    const-string v4, "Prefetched query strings don\'t match: \"%s\", \"%s\""

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v2, v5, v0

    const/4 v0, 0x5

    invoke-static {v0, v3, v4, v5}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 656
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isCancelled()Z
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public final isComplete()Z
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->isComplete()Z

    move-result v0

    return v0
.end method

.method public final isFailed()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0}, Lcmr;->isFailed()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 767
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchResult["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcmq;->bcp:Lcmr;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSuggestionsQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcmq;->bco:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSrpQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcmq;->bcm:Lcom/google/android/shared/search/Query;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mActionData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcmq;->mActionData:Lcom/google/android/velvet/ActionData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSrpMetadata="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcmq;->mSrpMetadata:Lcms;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
