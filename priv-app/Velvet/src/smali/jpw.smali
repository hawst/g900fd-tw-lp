.class public final Ljpw;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eya:I

.field private eyb:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 306
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 307
    iput v0, p0, Ljpw;->aez:I

    iput v0, p0, Ljpw;->eyb:I

    iput v0, p0, Ljpw;->eya:I

    const/4 v0, 0x0

    iput-object v0, p0, Ljpw;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljpw;->eCz:I

    .line 308
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 1

    .prologue
    .line 249
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljpw;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljpw;->eyb:I

    iget v0, p0, Ljpw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpw;->aez:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Ljsi;->btQ()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljpw;->eya:I

    iget v0, p0, Ljpw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpw;->aez:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljsj;)V
    .locals 2

    .prologue
    .line 322
    iget v0, p0, Ljpw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 323
    const/4 v0, 0x1

    iget v1, p0, Ljpw;->eyb:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 325
    :cond_0
    iget v0, p0, Ljpw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 326
    const/4 v0, 0x2

    iget v1, p0, Ljpw;->eya:I

    invoke-virtual {p1, v0, v1}, Ljsj;->bq(II)V

    .line 328
    :cond_1
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 329
    return-void
.end method

.method public final akf()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Ljpw;->eyb:I

    return v0
.end method

.method public final brY()Z
    .locals 1

    .prologue
    .line 279
    iget v0, p0, Ljpw;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final brZ()Z
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Ljpw;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getSize()I
    .locals 1

    .prologue
    .line 290
    iget v0, p0, Ljpw;->eya:I

    return v0
.end method

.method protected final lF()I
    .locals 3

    .prologue
    .line 333
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 334
    iget v1, p0, Ljpw;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 335
    const/4 v1, 0x1

    iget v2, p0, Ljpw;->eyb:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 338
    :cond_0
    iget v1, p0, Ljpw;->aez:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 339
    const/4 v1, 0x2

    iget v2, p0, Ljpw;->eya:I

    invoke-static {v1, v2}, Ljsj;->bv(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 342
    :cond_1
    return v0
.end method

.method public final rj(I)Ljpw;
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x1

    iput v0, p0, Ljpw;->eyb:I

    .line 275
    iget v0, p0, Ljpw;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljpw;->aez:I

    .line 276
    return-object p0
.end method

.method public final rk(I)Ljpw;
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x3

    iput v0, p0, Ljpw;->eya:I

    .line 294
    iget v0, p0, Ljpw;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljpw;->aez:I

    .line 295
    return-object p0
.end method
