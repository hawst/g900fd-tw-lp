.class final Lhxf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field data:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private synthetic dtY:Lhxa;

.field timestamp:J

.field type:I


# direct methods
.method constructor <init>(Lhxa;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lhxf;->dtY:Lhxa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 586
    new-instance v0, Lhxf;

    iget-object v1, p0, Lhxf;->dtY:Lhxa;

    invoke-direct {v0, v1}, Lhxf;-><init>(Lhxa;)V

    iget v1, p0, Lhxf;->type:I

    iput v1, v0, Lhxf;->type:I

    iget-wide v2, p0, Lhxf;->timestamp:J

    iput-wide v2, v0, Lhxf;->timestamp:J

    iget-object v1, p0, Lhxf;->data:Ljava/lang/Object;

    iput-object v1, v0, Lhxf;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 623
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LoggedEvent[t="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lhxf;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lhxf;->timestamp:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
