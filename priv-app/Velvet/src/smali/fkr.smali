.class public final Lfkr;
.super Lfke;
.source "PG"


# instance fields
.field private final cug:Lfss;

.field private final mModulePresenterFactory:Lftz;

.field private final mTimeToLeaveFactory:Lfyk;


# direct methods
.method public constructor <init>(Lemp;Lgah;Landroid/net/wifi/WifiManager;Lfzw;Lerk;Landroid/content/pm/PackageManager;Landroid/content/ContentResolver;ZLfyk;)V
    .locals 3

    .prologue
    .line 92
    invoke-direct/range {p0 .. p7}, Lfke;-><init>(Lemp;Lgah;Landroid/net/wifi/WifiManager;Lfzw;Lerk;Landroid/content/pm/PackageManager;Landroid/content/ContentResolver;)V

    .line 95
    new-instance v0, Lftz;

    iget-object v1, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p4, p5, v1}, Lftz;-><init>(Lfzw;Lerk;Lemp;)V

    iput-object v0, p0, Lfkr;->mModulePresenterFactory:Lftz;

    .line 96
    new-instance v0, Lfss;

    iget-object v1, p0, Lfkr;->mClock:Lemp;

    iget-object v2, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, v1, v2}, Lfss;-><init>(Lemp;Lgbd;)V

    iput-object v0, p0, Lfkr;->cug:Lfss;

    .line 97
    iput-object p9, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    .line 99
    return-void
.end method


# virtual methods
.method protected final synthetic b(Lizj;Ljal;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1, p2, p3}, Lfkr;->d(Lizj;Ljal;I)Lfkd;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic c(ILizj;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1, p2}, Lfkr;->d(ILizj;)Lfkd;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic c(ILizq;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1, p2}, Lfkr;->d(ILizq;)Lfkd;

    move-result-object v0

    return-object v0
.end method

.method protected final d(ILizj;)Lfkd;
    .locals 6

    .prologue
    .line 103
    sparse-switch p1, :sswitch_data_0

    .line 257
    :cond_0
    invoke-super {p0, p1, p2}, Lfke;->d(ILizj;)Lfkd;

    move-result-object v0

    :goto_0
    return-object v0

    .line 105
    :sswitch_0
    new-instance v0, Lfuq;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2, v3}, Lfuq;-><init>(Lizj;Lftz;Lemp;Lgbd;)V

    goto :goto_0

    .line 109
    :sswitch_1
    new-instance v0, Lfur;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfur;-><init>(Lizj;Lftz;Lemp;)V

    goto :goto_0

    .line 112
    :sswitch_2
    new-instance v0, Lfvk;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfvk;-><init>(Lizj;Lftz;Lemp;)V

    goto :goto_0

    .line 115
    :sswitch_3
    new-instance v0, Lfuu;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mIntentUtils:Leom;

    iget-object v5, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfuu;-><init>(Lizj;Lftz;Lemp;Leom;Lgan;)V

    goto :goto_0

    .line 119
    :sswitch_4
    new-instance v0, Lfuv;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->cug:Lfss;

    invoke-direct {v0, p2, v1, v2, v3}, Lfuv;-><init>(Lizj;Lftz;Lemp;Lfss;)V

    goto :goto_0

    .line 123
    :sswitch_5
    new-instance v0, Lfxk;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2, v3}, Lfxk;-><init>(Lizj;Lftz;Lemp;Lgbd;)V

    goto :goto_0

    .line 127
    :sswitch_6
    new-instance v0, Lfuw;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->cug:Lfss;

    invoke-direct {v0, p2, v1, v2, v3}, Lfuw;-><init>(Lizj;Lftz;Lemp;Lfss;)V

    goto :goto_0

    .line 131
    :sswitch_7
    new-instance v0, Lfux;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfux;-><init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V

    goto :goto_0

    .line 135
    :sswitch_8
    new-instance v0, Lfuy;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfuy;-><init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V

    goto :goto_0

    .line 139
    :sswitch_9
    new-instance v0, Lfwv;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfwv;-><init>(Lizj;Lftz;Lemp;)V

    goto :goto_0

    .line 142
    :sswitch_a
    new-instance v0, Lfve;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfve;-><init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V

    goto/16 :goto_0

    .line 146
    :sswitch_b
    new-instance v0, Lfvf;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mStringEvaluator:Lgbr;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfvf;-><init>(Lizj;Lftz;Lemp;Lgbr;Lgbd;)V

    goto/16 :goto_0

    .line 150
    :sswitch_c
    new-instance v0, Lfvg;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfvg;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 153
    :sswitch_d
    iget-object v0, p2, Lizj;->dSK:Lizr;

    invoke-virtual {v0}, Lizr;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 154
    new-instance v0, Lfvh;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2, v3}, Lfvh;-><init>(Lizj;Lftz;Lemp;Lgbd;)V

    goto/16 :goto_0

    .line 160
    :sswitch_e
    new-instance v0, Lfvj;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2, v3}, Lfvj;-><init>(Lizj;Lftz;Lemp;Lgbd;)V

    goto/16 :goto_0

    .line 164
    :sswitch_f
    new-instance v0, Lfvo;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    invoke-direct {v0, p2, v1, v2, v3}, Lfvo;-><init>(Lizj;Lftz;Lemp;Lgan;)V

    goto/16 :goto_0

    .line 168
    :sswitch_10
    new-instance v0, Lfvl;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mStringEvaluator:Lgbr;

    iget-object v5, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfvl;-><init>(Lizj;Lftz;Lemp;Lgbr;Lfyk;)V

    goto/16 :goto_0

    .line 172
    :sswitch_11
    new-instance v0, Lfvp;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfvp;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 175
    :sswitch_12
    new-instance v0, Lfvq;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2, v3}, Lfvq;-><init>(Lizj;Lftz;Lemp;Lgbd;)V

    goto/16 :goto_0

    .line 179
    :sswitch_13
    new-instance v0, Lfvt;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfvt;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 183
    :sswitch_14
    new-instance v0, Lfvx;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfvx;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 186
    :sswitch_15
    new-instance v0, Lfvw;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2, v3}, Lfvw;-><init>(Lizj;Lftz;Lemp;Lgbd;)V

    goto/16 :goto_0

    .line 190
    :sswitch_16
    new-instance v0, Lfvy;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfvy;-><init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V

    goto/16 :goto_0

    .line 194
    :sswitch_17
    new-instance v0, Lfwd;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfwd;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 197
    :sswitch_18
    new-instance v0, Lfwe;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfwe;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 200
    :sswitch_19
    new-instance v0, Lfwk;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfwk;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 203
    :sswitch_1a
    new-instance v0, Lfwo;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfwo;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 206
    :sswitch_1b
    new-instance v0, Lfwr;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfwr;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 210
    :sswitch_1c
    new-instance v0, Lfxf;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfxf;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 213
    :sswitch_1d
    new-instance v0, Lfxg;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfxg;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 216
    :sswitch_1e
    new-instance v0, Lfxi;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfxi;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 219
    :sswitch_1f
    new-instance v0, Lfxm;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2, v3}, Lfxm;-><init>(Lizj;Lftz;Lemp;Lgbd;)V

    goto/16 :goto_0

    .line 223
    :sswitch_20
    new-instance v0, Lfxn;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    invoke-direct {v0, p2, v1, v2, v3}, Lfxn;-><init>(Lizj;Lftz;Lemp;Lgan;)V

    goto/16 :goto_0

    .line 227
    :sswitch_21
    new-instance v0, Lfxo;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mStringEvaluator:Lgbr;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfxo;-><init>(Lizj;Lftz;Lemp;Lgbr;Lgbd;)V

    goto/16 :goto_0

    .line 231
    :sswitch_22
    new-instance v0, Lfxp;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfxp;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 234
    :sswitch_23
    new-instance v0, Lfxn;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    invoke-direct {v0, p2, v1, v2, v3}, Lfxn;-><init>(Lizj;Lftz;Lemp;Lgan;)V

    goto/16 :goto_0

    .line 238
    :sswitch_24
    new-instance v0, Lfxr;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfxr;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 241
    :sswitch_25
    new-instance v0, Lfxs;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfxs;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 245
    :sswitch_26
    new-instance v0, Lfwf;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfwf;-><init>(Lizj;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 250
    :sswitch_27
    new-instance v0, Lfws;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    invoke-direct {v0, p2, v1, v2, v3}, Lfws;-><init>(Lizj;Lftz;Lemp;Lgbd;)V

    goto/16 :goto_0

    .line 103
    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_25
        0x9 -> :sswitch_1d
        0xb -> :sswitch_11
        0xc -> :sswitch_10
        0xd -> :sswitch_1a
        0xe -> :sswitch_7
        0xf -> :sswitch_1e
        0x10 -> :sswitch_c
        0x11 -> :sswitch_9
        0x13 -> :sswitch_18
        0x14 -> :sswitch_14
        0x1a -> :sswitch_17
        0x1b -> :sswitch_3
        0x1c -> :sswitch_d
        0x1d -> :sswitch_15
        0x22 -> :sswitch_2
        0x2a -> :sswitch_16
        0x2b -> :sswitch_26
        0x32 -> :sswitch_0
        0x34 -> :sswitch_1f
        0x35 -> :sswitch_19
        0x38 -> :sswitch_13
        0x3b -> :sswitch_22
        0x3d -> :sswitch_24
        0x40 -> :sswitch_8
        0x41 -> :sswitch_12
        0x42 -> :sswitch_a
        0x45 -> :sswitch_21
        0x46 -> :sswitch_20
        0x4b -> :sswitch_4
        0x4c -> :sswitch_5
        0x4e -> :sswitch_27
        0x4f -> :sswitch_6
        0x5e -> :sswitch_1c
        0x65 -> :sswitch_1b
        0x68 -> :sswitch_b
        0x6b -> :sswitch_1
        0x77 -> :sswitch_f
        0x78 -> :sswitch_e
        0x84 -> :sswitch_27
        0x85 -> :sswitch_27
        0x87 -> :sswitch_23
    .end sparse-switch
.end method

.method protected final d(ILizq;)Lfkd;
    .locals 7

    .prologue
    .line 262
    sparse-switch p1, :sswitch_data_0

    .line 357
    invoke-super {p0, p1, p2}, Lfke;->d(ILizq;)Lfkd;

    move-result-object v0

    :goto_0
    return-object v0

    .line 264
    :sswitch_0
    new-instance v0, Lfvu;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    iget-object v5, p0, Lfkr;->mPlaceDataHelper:Lgbe;

    iget-object v6, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lfvu;-><init>(Lizq;Lftz;Lemp;Lgan;Lgbe;Lgbr;)V

    goto :goto_0

    .line 268
    :sswitch_1
    new-instance v0, Lfwi;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v1, p2, Lizq;->dUZ:Lizj;

    iget-object v4, v1, Lizj;->dTw:Ljeg;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v6, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lfwi;-><init>(Lizq;Lftz;Lemp;Ljeg;Lgbd;Lgbr;)V

    goto :goto_0

    .line 273
    :sswitch_2
    new-instance v0, Lfwi;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v1, p2, Lizq;->dUZ:Lizj;

    iget-object v4, v1, Lizj;->dTz:Ljeg;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v6, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lfwi;-><init>(Lizq;Lftz;Lemp;Ljeg;Lgbd;Lgbr;)V

    goto :goto_0

    .line 278
    :sswitch_3
    new-instance v0, Lfwi;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v1, p2, Lizq;->dUZ:Lizj;

    iget-object v4, v1, Lizj;->dTw:Ljeg;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v6, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lfwi;-><init>(Lizq;Lftz;Lemp;Ljeg;Lgbd;Lgbr;)V

    goto :goto_0

    .line 283
    :sswitch_4
    new-instance v0, Lfwi;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v1, p2, Lizq;->dUZ:Lizj;

    iget-object v4, v1, Lizj;->dTB:Ljeg;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v6, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lfwi;-><init>(Lizq;Lftz;Lemp;Ljeg;Lgbd;Lgbr;)V

    goto :goto_0

    .line 288
    :sswitch_5
    new-instance v0, Lfvf;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mStringEvaluator:Lgbr;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfvf;-><init>(Lizq;Lftz;Lemp;Lgbr;Lgbd;)V

    goto :goto_0

    .line 292
    :sswitch_6
    new-instance v0, Lfvz;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfvz;-><init>(Lizq;Lftz;Lemp;Lgan;Lgbr;)V

    goto :goto_0

    .line 296
    :sswitch_7
    new-instance v0, Lfvl;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mStringEvaluator:Lgbr;

    iget-object v5, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfvl;-><init>(Lizq;Lftz;Lemp;Lgbr;Lfyk;)V

    goto/16 :goto_0

    .line 300
    :sswitch_8
    new-instance v0, Lfwa;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    iget-object v5, p0, Lfkr;->mPlaceDataHelper:Lgbe;

    iget-object v6, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lfwa;-><init>(Lizq;Lftz;Lemp;Lgan;Lgbe;Lgbr;)V

    goto/16 :goto_0

    .line 304
    :sswitch_9
    new-instance v0, Lfwb;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfwb;-><init>(Lizq;Lftz;Lemp;Lgan;Lgbr;)V

    goto/16 :goto_0

    .line 308
    :sswitch_a
    new-instance v0, Lfwh;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfwh;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 312
    :sswitch_b
    new-instance v0, Lfwn;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfwn;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 316
    :sswitch_c
    new-instance v0, Lfwc;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mFifeImageUrlUtil:Lgan;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfwc;-><init>(Lizq;Lftz;Lemp;Lgan;Lgbr;)V

    goto/16 :goto_0

    .line 320
    :sswitch_d
    new-instance v0, Lfwr;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfwr;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 324
    :sswitch_e
    new-instance v0, Lfxo;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mStringEvaluator:Lgbr;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lfxo;-><init>(Lizq;Lftz;Lemp;Lgbr;Lgbd;)V

    goto/16 :goto_0

    .line 328
    :sswitch_f
    new-instance v0, Lfwi;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v1, p2, Lizq;->dUZ:Lizj;

    iget-object v4, v1, Lizj;->dTY:Ljeg;

    iget-object v5, p0, Lfkr;->mPhotoWithAttributionDecorator:Lgbd;

    iget-object v6, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lfwi;-><init>(Lizq;Lftz;Lemp;Ljeg;Lgbd;Lgbr;)V

    goto/16 :goto_0

    .line 333
    :sswitch_10
    new-instance v0, Lfxq;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfxq;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 337
    :sswitch_11
    new-instance v0, Lfxs;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfxs;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 341
    :sswitch_12
    new-instance v0, Lfup;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfup;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 345
    :sswitch_13
    new-instance v0, Lfwu;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    invoke-direct {v0, p2, v1, v2}, Lfwu;-><init>(Lizq;Lftz;Lemp;)V

    goto/16 :goto_0

    .line 349
    :sswitch_14
    new-instance v0, Lfxj;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfxj;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 353
    :sswitch_15
    new-instance v0, Lfut;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p2, v1, v2, v3}, Lfut;-><init>(Lizq;Lftz;Lemp;Lgbr;)V

    goto/16 :goto_0

    .line 262
    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_d
        0x16 -> :sswitch_0
        0x1f -> :sswitch_6
        0x26 -> :sswitch_8
        0x2c -> :sswitch_a
        0x4d -> :sswitch_1
        0x50 -> :sswitch_2
        0x52 -> :sswitch_4
        0x5c -> :sswitch_e
        0x60 -> :sswitch_15
        0x62 -> :sswitch_9
        0x63 -> :sswitch_f
        0x67 -> :sswitch_5
        0x6c -> :sswitch_14
        0x71 -> :sswitch_b
        0x76 -> :sswitch_11
        0x7a -> :sswitch_10
        0x7b -> :sswitch_13
        0x7c -> :sswitch_7
        0x7d -> :sswitch_3
        0x89 -> :sswitch_12
        0x8a -> :sswitch_c
    .end sparse-switch
.end method

.method protected final d(Lizj;Ljal;I)Lfkd;
    .locals 6

    .prologue
    .line 363
    packed-switch p3, :pswitch_data_0

    .line 382
    invoke-super {p0, p1, p2, p3}, Lfke;->d(Lizj;Ljal;I)Lfkd;

    move-result-object v0

    :goto_0
    return-object v0

    .line 365
    :pswitch_0
    new-instance v0, Lfww;

    iget-object v1, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v2, p0, Lfkr;->mClock:Lemp;

    iget-object v3, p0, Lfkr;->mStringEvaluator:Lgbr;

    invoke-direct {v0, p1, v1, v2, v3}, Lfww;-><init>(Lizj;Lftz;Lemp;Lgbr;)V

    goto :goto_0

    .line 369
    :pswitch_1
    new-instance v0, Lfvs;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lfvs;-><init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V

    goto :goto_0

    .line 373
    :pswitch_2
    new-instance v0, Lfwj;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lfwj;-><init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V

    goto :goto_0

    .line 378
    :pswitch_3
    new-instance v0, Lfvi;

    iget-object v2, p0, Lfkr;->mModulePresenterFactory:Lftz;

    iget-object v3, p0, Lfkr;->mClock:Lemp;

    iget-object v4, p0, Lfkr;->mTimeToLeaveFactory:Lfyk;

    iget-object v5, p0, Lfkr;->mStringEvaluator:Lgbr;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lfvi;-><init>(Lizj;Lftz;Lemp;Lfyk;Lgbr;)V

    goto :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
