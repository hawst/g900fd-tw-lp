.class public Laqm;
.super Lcyc;
.source "PG"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final amD:Landroid/database/DataSetObserver;

.field private final amE:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final amF:Lcry;

.field private final amG:Z

.field private amH:Lcxd;

.field amI:Landroid/preference/SwitchPreference;

.field private amJ:Landroid/preference/Preference;

.field public amK:Landroid/preference/PreferenceGroup;

.field protected amL:Ljava/lang/Boolean;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field protected amM:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mActivity:Landroid/app/Activity;

.field private final mAlwaysOnHotwordAdapter:Lcsq;

.field private final mContext:Landroid/content/Context;

.field private final mFlags:Lchk;

.field private final mLoginHelper:Lcrh;

.field private final mSearchHistoryHelper:Lcrr;

.field private final mSettings:Lcke;

.field private final mUrlHelper:Lcpn;

.field final mVss:Lhhq;


# direct methods
.method public constructor <init>(Lcke;Lcrh;Lchk;Landroid/app/Activity;Lcpn;Lcrr;Lhhq;Landroid/content/Context;Lcsq;)V
    .locals 4
    .param p9    # Lcsq;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0, p1}, Lcyc;-><init>(Lcke;)V

    .line 63
    new-instance v1, Laqn;

    invoke-direct {v1, p0}, Laqn;-><init>(Laqm;)V

    iput-object v1, p0, Laqm;->amD:Landroid/database/DataSetObserver;

    .line 71
    new-instance v1, Laqo;

    invoke-direct {v1, p0}, Laqo;-><init>(Laqm;)V

    iput-object v1, p0, Laqm;->amE:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 101
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Laqm;->amM:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 113
    iput-object p1, p0, Laqm;->mSettings:Lcke;

    .line 114
    iget-object v1, p0, Laqm;->mSettings:Lcke;

    iget-object v2, p0, Laqm;->amE:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Lcke;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 115
    iput-object p2, p0, Laqm;->mLoginHelper:Lcrh;

    .line 116
    iput-object p4, p0, Laqm;->mActivity:Landroid/app/Activity;

    .line 117
    iput-object p3, p0, Laqm;->mFlags:Lchk;

    .line 118
    iput-object p5, p0, Laqm;->mUrlHelper:Lcpn;

    .line 119
    iput-object p6, p0, Laqm;->mSearchHistoryHelper:Lcrr;

    .line 120
    iput-object p7, p0, Laqm;->mVss:Lhhq;

    .line 121
    iput-object p8, p0, Laqm;->mContext:Landroid/content/Context;

    .line 122
    new-instance v1, Lcry;

    iget-object v2, p0, Laqm;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Laqm;->mVss:Lhhq;

    iget-object v3, v3, Lhhq;->mSettings:Lhym;

    invoke-direct {v1, v2, v3}, Lcry;-><init>(Landroid/content/Context;Lhym;)V

    iput-object v1, p0, Laqm;->amF:Lcry;

    .line 123
    iput-object p9, p0, Laqm;->mAlwaysOnHotwordAdapter:Lcsq;

    .line 124
    iget-object v1, p0, Laqm;->mAlwaysOnHotwordAdapter:Lcsq;

    if-eqz v1, :cond_0

    iget-object v1, p0, Laqm;->mVss:Lhhq;

    iget-object v1, v1, Lhhq;->mSettings:Lhym;

    invoke-virtual {v1}, Lhym;->aTR()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Laqm;->amG:Z

    .line 126
    return-void
.end method

.method private a(Landroid/preference/Preference;Z)V
    .locals 2

    .prologue
    .line 388
    if-nez p2, :cond_1

    .line 389
    iget-object v0, p0, Laqm;->amK:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    iget-object v0, p0, Laqm;->amK:Landroid/preference/PreferenceGroup;

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-nez v0, :cond_0

    .line 391
    iget-object v0, p0, Laqm;->amK:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method static synthetic a(Laqm;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 58
    iget-object v0, p0, Laqm;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    iget-object v1, p0, Laqm;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcse;->a([BLjava/lang/String;)V

    iget-object v0, p0, Laqm;->mVss:Lhhq;

    invoke-virtual {v0}, Lhhq;->ue()Lcse;

    move-result-object v0

    iget-object v1, p0, Laqm;->mLoginHelper:Lcrh;

    invoke-virtual {v1}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcse;->iJ(Ljava/lang/String;)V

    iget-boolean v0, p0, Laqm;->amG:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Laqm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->SO()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laqm;->mAlwaysOnHotwordAdapter:Lcsq;

    new-instance v1, Laqq;

    invoke-direct {v1, p0}, Laqq;-><init>(Laqm;)V

    invoke-interface {v0, v1}, Lcsq;->f(Lcsr;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Laqm;->mVss:Lhhq;

    iget-object v0, v0, Lhhq;->mSettings:Lhym;

    invoke-virtual {v0}, Lhym;->aTR()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqm;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Laqm;->amF:Lcry;

    invoke-static {}, Lcry;->SA()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.googlequicksearchbox.LAUNCH_FROM_DSP_HOTWORD"

    new-instance v3, Laqv;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v5}, Laqv;-><init>(Laqm;B)V

    const/4 v5, -0x1

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/app/Activity;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method static synthetic a(Laqm;I)V
    .locals 1

    .prologue
    .line 58
    invoke-static {p1}, Lege;->hs(I)Litu;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Litu;)V

    return-void
.end method

.method static synthetic a(Laqm;ZLemy;)V
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Laqm;->a(ZLemy;)V

    return-void
.end method

.method private a(ZLemy;)V
    .locals 5
    .param p2    # Lemy;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 431
    iget-object v0, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 432
    iget-object v0, p0, Laqm;->amM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    .line 434
    iget-object v1, p0, Laqm;->mSearchHistoryHelper:Lcrr;

    iget-object v2, p0, Laqm;->mLoginHelper:Lcrh;

    invoke-virtual {v2}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v2

    const/4 v3, 0x1

    new-instance v4, Laqu;

    invoke-direct {v4, p0, p1, v0, p2}, Laqu;-><init>(Laqm;ZILemy;)V

    invoke-virtual {v1, v2, p1, v3, v4}, Lcrr;->a(Landroid/accounts/Account;ZZLemy;)V

    .line 462
    return-void
.end method

.method static synthetic b(Laqm;)Lcsq;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Laqm;->mAlwaysOnHotwordAdapter:Lcsq;

    return-object v0
.end method

.method static synthetic c(Laqm;)Lcry;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Laqm;->amF:Lcry;

    return-object v0
.end method

.method static synthetic d(Laqm;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Laqm;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public static ea(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 281
    const-string v0, "audio_history"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "manage_audio_history"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tO()Z
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Laqm;->amL:Ljava/lang/Boolean;

    .line 155
    if-nez p1, :cond_0

    .line 156
    iget-object v0, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 189
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 159
    iget-object v0, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    const v1, 0x7f0a059e

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(I)V

    .line 161
    iget-object v0, p0, Laqm;->amM:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 162
    iget-object v0, p0, Laqm;->mSearchHistoryHelper:Lcrr;

    const/4 v1, 0x1

    new-instance v2, Laqp;

    invoke-direct {v2, p0}, Laqp;-><init>(Laqm;)V

    invoke-virtual {v0, p1, v1, v2}, Lcrr;->a(Landroid/accounts/Account;ZLemy;)V

    goto :goto_0
.end method

.method public final a(Landroid/preference/Preference;)V
    .locals 2

    .prologue
    .line 287
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 289
    const-string v1, "audio_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 290
    invoke-virtual {p0, p1}, Laqm;->b(Landroid/preference/Preference;)V

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    const-string v1, "manage_audio_history"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    invoke-virtual {p0, p1}, Laqm;->c(Landroid/preference/Preference;)V

    .line 293
    invoke-virtual {p0}, Laqm;->tP()V

    goto :goto_0
.end method

.method public final a(Landroid/preference/PreferenceScreen;)V
    .locals 0

    .prologue
    .line 276
    invoke-super {p0, p1}, Lcyc;->a(Landroid/preference/PreferenceScreen;)V

    .line 277
    iput-object p1, p0, Laqm;->amK:Landroid/preference/PreferenceGroup;

    .line 278
    return-void
.end method

.method public final b(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 299
    check-cast p1, Landroid/preference/SwitchPreference;

    iput-object p1, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    .line 300
    iget-object v0, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 301
    return-void
.end method

.method public final c(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 305
    iput-object p1, p0, Laqm;->amJ:Landroid/preference/Preference;

    .line 306
    iget-object v0, p0, Laqm;->amJ:Landroid/preference/Preference;

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 307
    return-void
.end method

.method protected cZ(I)V
    .locals 2

    .prologue
    .line 378
    iget-object v0, p0, Laqm;->mActivity:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 379
    return-void
.end method

.method public final i(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcyc;->i(Landroid/os/Bundle;)V

    .line 142
    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    iget-object v1, p0, Laqm;->amD:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcrh;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 145
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    iget-object v1, p0, Laqm;->amD:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcrh;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 262
    :cond_0
    iget-object v0, p0, Laqm;->mSettings:Lcke;

    iget-object v1, p0, Laqm;->amE:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Lcke;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 263
    invoke-super {p0}, Lcyc;->onDestroy()V

    .line 264
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 324
    iget-object v0, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    if-ne p1, v0, :cond_0

    .line 325
    iget-object v0, p0, Laqm;->amL:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqm;->amL:Ljava/lang/Boolean;

    invoke-virtual {v0, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 328
    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Laqm;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a06fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 331
    iget-object v1, p0, Laqm;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a06f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 333
    iget-object v2, p0, Laqm;->mVss:Lhhq;

    iget-object v2, v2, Lhhq;->mSettings:Lhym;

    invoke-virtual {v2}, Lhym;->aFc()Ljava/lang/String;

    move-result-object v2

    .line 334
    iget-object v3, p0, Laqm;->mVss:Lhhq;

    invoke-virtual {v3}, Lhhq;->ue()Lcse;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcse;->iI(Ljava/lang/String;)Lgcq;

    move-result-object v2

    invoke-virtual {v2}, Lgcq;->getPrompt()Ljava/lang/String;

    move-result-object v2

    .line 336
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Laqm;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a0711

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Laqm;->mContext:Landroid/content/Context;

    const v5, 0x7f0a06f7

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v2, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Laqs;

    invoke-direct {v3, p0}, Laqs;-><init>(Laqm;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Laqr;

    invoke-direct {v2, p0}, Laqr;-><init>(Laqm;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 373
    :cond_0
    :goto_0
    return v8

    .line 359
    :cond_1
    const v0, 0x7f0a05a1

    invoke-virtual {p0, v0}, Laqm;->cZ(I)V

    .line 360
    new-instance v0, Laqt;

    invoke-direct {v0, p0}, Laqt;-><init>(Laqm;)V

    invoke-direct {p0, v7, v0}, Laqm;->a(ZLemy;)V

    goto :goto_0
.end method

.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    .line 313
    iget-object v0, p0, Laqm;->amJ:Landroid/preference/Preference;

    if-ne p1, v0, :cond_0

    .line 314
    new-instance v0, Lcxd;

    iget-object v1, p0, Laqm;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Laqm;->mFlags:Lchk;

    iget-object v3, p0, Laqm;->mLoginHelper:Lcrh;

    iget-object v4, p0, Laqm;->mUrlHelper:Lcpn;

    iget-object v5, p0, Laqm;->mFlags:Lchk;

    invoke-virtual {v5}, Lchk;->GZ()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcxd;-><init>(Landroid/content/Context;Lchk;Lcrh;Lcpn;Ljava/lang/String;)V

    iput-object v0, p0, Laqm;->amH:Lcxd;

    .line 316
    iget-object v0, p0, Laqm;->amH:Lcxd;

    invoke-virtual {v0}, Lcxd;->start()V

    .line 318
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Lcyc;->onResume()V

    .line 131
    iget-object v0, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqm;->amJ:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Laqm;->tN()V

    .line 135
    :cond_0
    invoke-virtual {p0}, Laqm;->tP()V

    .line 136
    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Laqm;->a(Landroid/accounts/Account;)V

    .line 137
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 238
    invoke-super {p0}, Lcyc;->onStart()V

    .line 240
    iget-boolean v0, p0, Laqm;->amG:Z

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Laqm;->mAlwaysOnHotwordAdapter:Lcsq;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcsq;->a(Lcsr;)V

    .line 243
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Laqm;->amH:Lcxd;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Laqm;->amH:Lcxd;

    invoke-virtual {v0}, Lcxd;->Tw()V

    .line 249
    const/4 v0, 0x0

    iput-object v0, p0, Laqm;->amH:Lcxd;

    .line 251
    :cond_0
    iget-boolean v0, p0, Laqm;->amG:Z

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Laqm;->mAlwaysOnHotwordAdapter:Lcsq;

    invoke-interface {v0}, Lcsq;->disconnect()V

    .line 254
    :cond_1
    invoke-super {p0}, Lcyc;->onStop()V

    .line 255
    return-void
.end method

.method public final tN()V
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    invoke-direct {p0}, Laqm;->tO()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Laqm;->a(Landroid/preference/Preference;Z)V

    .line 384
    iget-object v0, p0, Laqm;->amJ:Landroid/preference/Preference;

    invoke-direct {p0}, Laqm;->tO()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Laqm;->a(Landroid/preference/Preference;Z)V

    .line 385
    return-void
.end method

.method final tP()V
    .locals 7

    .prologue
    const v4, 0x7f0a0713

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 404
    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 406
    iget-object v0, p0, Laqm;->amJ:Landroid/preference/Preference;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setTitle(I)V

    .line 407
    iget-object v0, p0, Laqm;->amJ:Landroid/preference/Preference;

    const v1, 0x7f0a05a9

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 409
    iget-object v0, p0, Laqm;->amJ:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 427
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 404
    goto :goto_0

    .line 411
    :cond_2
    invoke-direct {p0}, Laqm;->tO()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 412
    iget-object v0, p0, Laqm;->mLoginHelper:Lcrh;

    invoke-virtual {v0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v0

    .line 414
    iget-object v3, p0, Laqm;->amJ:Landroid/preference/Preference;

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(I)V

    .line 415
    iget-object v3, p0, Laqm;->amJ:Landroid/preference/Preference;

    iget-object v4, p0, Laqm;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0714

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v0}, Lemg;->unicodeWrap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 418
    iget-object v0, p0, Laqm;->amJ:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_1

    .line 421
    :cond_3
    iget-object v0, p0, Laqm;->amK:Landroid/preference/PreferenceGroup;

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Laqm;->amK:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Laqm;->amI:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 423
    iget-object v0, p0, Laqm;->amK:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Laqm;->amJ:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method
