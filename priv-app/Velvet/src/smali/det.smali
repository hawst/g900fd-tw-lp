.class public final Ldet;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final bvD:Lemy;


# instance fields
.field private final buj:Lepo;

.field final buk:Leqo;

.field private final bvE:Ldgi;

.field private final bvF:Lddp;

.field private final mActionDiscoverySource:Lddl;

.field private final mGsaConfigFlags:Lchk;

.field private final mNowOptInSettings:Lcin;

.field private mShouldQueryStrategy:Ldfz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lepq;

    invoke-direct {v0}, Lepq;-><init>()V

    sput-object v0, Ldet;->bvD:Lemy;

    return-void
.end method

.method public constructor <init>(Ldfz;Lepo;Leqo;Lcin;Ldgi;Lddp;Lddl;Lchk;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Ldet;->mShouldQueryStrategy:Ldfz;

    .line 69
    iput-object p2, p0, Ldet;->buj:Lepo;

    .line 70
    iput-object p3, p0, Ldet;->buk:Leqo;

    .line 71
    iput-object p4, p0, Ldet;->mNowOptInSettings:Lcin;

    .line 72
    iput-object p5, p0, Ldet;->bvE:Ldgi;

    .line 73
    iput-object p6, p0, Ldet;->bvF:Lddp;

    .line 74
    iput-object p7, p0, Ldet;->mActionDiscoverySource:Lddl;

    .line 75
    iput-object p8, p0, Ldet;->mGsaConfigFlags:Lchk;

    .line 76
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/shared/search/Query;Lcns;Ljava/lang/String;)Ldem;
    .locals 12
    .param p1    # Lcom/google/android/shared/search/Query;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 97
    new-instance v3, Ldem;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    new-instance v0, Ldfd;

    iget-object v6, p0, Ldet;->mGsaConfigFlags:Lchk;

    invoke-virtual {v6}, Lchk;->Fu()[J

    move-result-object v6

    iget-object v7, p0, Ldet;->mGsaConfigFlags:Lchk;

    invoke-virtual {v7}, Lchk;->Fw()[I

    move-result-object v7

    iget-object v8, p0, Ldet;->mGsaConfigFlags:Lchk;

    invoke-virtual {v8}, Lchk;->Fv()[J

    move-result-object v8

    iget-object v9, p0, Ldet;->mGsaConfigFlags:Lchk;

    invoke-virtual {v9}, Lchk;->Fx()[[I

    move-result-object v9

    invoke-direct {v0, v6, v7, v8, v9}, Ldfd;-><init>([J[I[J[[I)V

    invoke-direct {v3, p1, v4, v5, v0}, Ldem;-><init>(Lcom/google/android/shared/search/Query;JLdfd;)V

    .line 103
    iget-object v0, p0, Ldet;->bvF:Lddp;

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    const/4 v0, 0x6

    invoke-virtual {v3, v0}, Ldem;->fL(I)V

    .line 104
    :goto_0
    if-nez p2, :cond_3

    invoke-virtual {v3, v2}, Ldem;->fL(I)V

    .line 105
    :cond_1
    :goto_1
    iget-object v0, p0, Ldet;->bvE:Ldgi;

    invoke-virtual {v0, p1, v3}, Ldgi;->a(Lcom/google/android/shared/search/Query;Ldem;)V

    .line 106
    iget-object v0, p0, Ldet;->mActionDiscoverySource:Lddl;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldet;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abH()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldet;->mActionDiscoverySource:Lddl;

    invoke-virtual {v0, p1}, Lddl;->aC(Lcom/google/android/shared/search/Query;)Ldef;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ldef;->getCount()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {v3, v10, v0, v1}, Ldem;->a(ILdef;Z)V

    .line 107
    :goto_2
    invoke-virtual {v3}, Ldem;->isClosed()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    invoke-static {v0}, Lifv;->gY(Z)V

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->aqP()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/android/shared/search/Query;->apZ()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Ldet;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->KF()I

    move-result v0

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Ldet;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Ldet;->mNowOptInSettings:Lcin;

    invoke-interface {v0}, Lcin;->KC()Z

    move-result v0

    if-nez v0, :cond_9

    new-array v0, v1, [Lcom/google/android/shared/search/Suggestion;

    new-instance v4, Lded;

    invoke-direct {v4}, Lded;-><init>()V

    const-string v5, "now-promo"

    iput-object v5, v4, Lded;->buw:Ljava/lang/String;

    iput-boolean v1, v4, Lded;->buV:Z

    invoke-virtual {v4}, Lded;->aba()Lcom/google/android/shared/search/Suggestion;

    move-result-object v4

    aput-object v4, v0, v2

    invoke-static {v0}, Lilw;->k([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v4, "now-promo"

    invoke-static {v4, p1, v0, v2}, Ldeg;->a(Ljava/lang/String;Lcom/google/android/shared/search/Query;Ljava/util/List;Z)Ldef;

    move-result-object v0

    invoke-virtual {v3, v11, v0, v1}, Ldem;->a(ILdef;Z)V

    .line 108
    :goto_4
    return-object v3

    .line 103
    :cond_2
    new-instance v4, Lddq;

    invoke-direct {v4, v0, v3}, Lddq;-><init>(Lddp;Ldem;)V

    iget-object v5, v0, Lddp;->buj:Lepo;

    new-instance v6, Lddr;

    invoke-direct {v6, v0, v4, p1, p3}, Lddr;-><init>(Lddp;Lemy;Lcom/google/android/shared/search/Query;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Lepo;->a(Lepn;)V

    goto/16 :goto_0

    .line 104
    :cond_3
    invoke-interface {p2, p1}, Lcns;->h(Lcom/google/android/shared/search/Query;)Lddw;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v2, v4, v1}, Ldem;->a(ILdef;Z)V

    :cond_4
    iget-object v0, p0, Ldet;->mShouldQueryStrategy:Ldfz;

    invoke-virtual {v0}, Ldfz;->abE()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Ldet;->bvD:Lemy;

    if-nez v4, :cond_5

    new-instance v0, Ldeu;

    invoke-direct {v0, p0, v3}, Ldeu;-><init>(Ldet;Ldem;)V

    :cond_5
    iget-object v4, p0, Ldet;->buj:Lepo;

    new-instance v5, Ldev;

    invoke-direct {v5, p0, p2, p1, v0}, Ldev;-><init>(Ldet;Lcns;Lcom/google/android/shared/search/Query;Lemy;)V

    invoke-interface {v4, v5}, Lepo;->a(Lepn;)V

    goto/16 :goto_1

    .line 106
    :cond_6
    invoke-virtual {v3, v10}, Ldem;->fL(I)V

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v3, v10}, Ldem;->fL(I)V

    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 107
    goto/16 :goto_3

    :cond_9
    invoke-virtual {v3, v11}, Ldem;->fL(I)V

    goto :goto_4
.end method

.method public final abr()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Ldet;->bvE:Ldgi;

    invoke-virtual {v0}, Ldgi;->cancel()V

    .line 84
    iget-object v0, p0, Ldet;->bvF:Lddp;

    iget-object v1, v0, Lddp;->bul:Landroid/os/CancellationSignal;

    invoke-virtual {v1}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lddp;->bul:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    .line 85
    :cond_0
    iget-object v0, p0, Ldet;->buj:Lepo;

    invoke-interface {v0}, Lepo;->ace()V

    .line 86
    return-void
.end method
