.class public Lcrh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lglm;
.implements Lgln;


# instance fields
.field private final aMD:Leqo;

.field private final aTW:Ljava/util/concurrent/Executor;

.field private final bgY:Landroid/database/DataSetObservable;

.field private final bgZ:Lcrf;

.field private final bha:Ligi;

.field private bhb:[Landroid/accounts/Account;

.field private mAccount:Landroid/accounts/Account;

.field private final mAccountManager:Landroid/accounts/AccountManager;

.field private final mContext:Landroid/content/Context;

.field private final mSettings:Lcke;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcke;Leqo;Ljava/util/concurrent/Executor;Lcrf;Landroid/accounts/AccountManager;Ligi;)V
    .locals 4

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcrh;->bgY:Landroid/database/DataSetObservable;

    .line 139
    iput-object p1, p0, Lcrh;->mContext:Landroid/content/Context;

    .line 140
    iput-object p2, p0, Lcrh;->mSettings:Lcke;

    .line 141
    iput-object p6, p0, Lcrh;->mAccountManager:Landroid/accounts/AccountManager;

    .line 142
    iput-object p7, p0, Lcrh;->bha:Ligi;

    .line 143
    iput-object p3, p0, Lcrh;->aMD:Leqo;

    .line 144
    iput-object p4, p0, Lcrh;->aTW:Ljava/util/concurrent/Executor;

    .line 145
    iput-object p5, p0, Lcrh;->bgZ:Lcrf;

    .line 146
    iget-object v0, p0, Lcrh;->mAccountManager:Landroid/accounts/AccountManager;

    if-nez v0, :cond_0

    .line 147
    const-string v0, "Search.LoginHelper"

    const-string v1, "Missing account manager."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x5

    invoke-static {v3, v0, v1, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 150
    :cond_0
    return-void
.end method

.method private Sq()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcrh;->bhb:[Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 155
    invoke-virtual {p0}, Lcrh;->refresh()V

    .line 157
    :cond_0
    return-void
.end method

.method private Z(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 321
    new-instance v0, Legk;

    invoke-direct {v0, p1, p2}, Legk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Legm;->a(Legk;)Legm;

    move-result-object v0

    invoke-static {v0}, Lege;->a(Legm;)V

    .line 325
    invoke-static {}, Lgql;->aJN()Lgql;

    move-result-object v0

    invoke-virtual {v0}, Lgql;->SC()Lcfo;

    move-result-object v0

    invoke-virtual {v0}, Lcfo;->DO()Lgpp;

    move-result-object v0

    const-string v1, "refresh_search_domain_and_cookies"

    invoke-interface {v0, v1}, Lgpp;->nw(Ljava/lang/String;)V

    .line 331
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.apps.now.account_update_broadcast"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 332
    const-string v0, "account_name"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    const-string v0, "old_account_name"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    const-string v2, "account_opted_into_now"

    iget-object v0, p0, Lcrh;->bha:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcin;

    invoke-interface {v0}, Lcin;->Kz()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 339
    iget-object v0, p0, Lcrh;->mContext:Landroid/content/Context;

    const-string v2, "com.google.android.apps.now.CURRENT_ACCOUNT_ACCESS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 340
    return-void
.end method

.method static synthetic a(Lcrh;)Landroid/database/DataSetObservable;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcrh;->bgY:Landroid/database/DataSetObservable;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Leua;)Lepl;
    .locals 7

    .prologue
    .line 561
    new-instance v0, Lcrl;

    const-string v2, "Get token"

    const/4 v1, 0x1

    new-array v3, v1, [I

    const/4 v1, 0x0

    const/4 v4, 0x2

    aput v4, v3, v1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcrl;-><init>(Lcrh;Ljava/lang/String;[ILjava/lang/String;Ljava/lang/String;Leua;)V

    invoke-static {v0}, Lepl;->a(Lerh;)Lepl;

    move-result-object v0

    .line 568
    iget-object v1, p0, Lcrh;->aTW:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 569
    return-object v0
.end method

.method private static a(Ljava/util/concurrent/Future;JZLeua;Landroid/content/res/Resources;)Ljava/lang/Object;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 642
    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, p1, p2, v0}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 663
    :goto_0
    return-object v0

    .line 643
    :catch_0
    move-exception v0

    .line 644
    const-string v1, "Search.LoginHelper"

    const-string v2, "InterruptedException while waiting for token."

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 645
    invoke-static {v4}, Lcrh;->fp(I)V

    .line 646
    if-eqz p3, :cond_0

    .line 647
    invoke-interface {p0, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 663
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 649
    :catch_1
    move-exception v0

    .line 651
    const-string v1, "Search.LoginHelper"

    const-string v2, "ExecutionException while waiting for token."

    invoke-static {v1, v2, v0}, Leor;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 652
    const v0, 0x99a82f

    invoke-static {v0}, Lhwt;->lx(I)V

    goto :goto_1

    .line 653
    :catch_2
    move-exception v0

    .line 654
    const/4 v1, 0x4

    invoke-static {v1}, Lcrh;->fp(I)V

    .line 655
    const-string v1, "Search.LoginHelper"

    const-string v2, "TimeoutException while waiting for token."

    invoke-static {v1, v2, v0}, Leor;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 656
    new-instance v0, Letj;

    invoke-direct {v0, p5}, Letj;-><init>(Landroid/content/res/Resources;)V

    .line 657
    invoke-virtual {p4, v0}, Leua;->a(Letj;)V

    .line 658
    const-string v1, "Search.LoginHelper"

    invoke-virtual {v0}, Letj;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4, v1, v0, v2}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 659
    if-eqz p3, :cond_0

    .line 660
    invoke-interface {p0, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_1
.end method

.method private declared-synchronized a(Landroid/accounts/Account;Z)V
    .locals 3
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcrh;->mAccount:Landroid/accounts/Account;

    .line 278
    iget-object v0, p0, Lcrh;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->ND()Ljava/lang/String;

    move-result-object v2

    .line 279
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v1, v0

    .line 280
    :goto_0
    invoke-static {v2, v1}, Lifo;->b(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    :goto_1
    monitor-exit p0

    return-void

    .line 279
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 285
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcrh;->mSettings:Lcke;

    invoke-interface {v0, v1, p2}, Lcke;->i(Ljava/lang/String;Z)V

    .line 287
    iget-object v0, p0, Lcrh;->bha:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcin;

    invoke-interface {v0, p1}, Lcin;->h(Landroid/accounts/Account;)V

    .line 288
    invoke-direct {p0, v2, v1}, Lcrh;->Z(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a([Landroid/accounts/Account;)V
    .locals 2
    .param p1    # [Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iput-object p1, p0, Lcrh;->bhb:[Landroid/accounts/Account;

    .line 191
    array-length v0, p1

    if-nez v0, :cond_0

    .line 192
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcrh;->a(Landroid/accounts/Account;Z)V

    .line 193
    invoke-direct {p0}, Lcrh;->notifyChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    :goto_0
    monitor-exit p0

    return-void

    .line 197
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcrh;->mSettings:Lcke;

    invoke-interface {v0}, Lcke;->ND()Ljava/lang/String;

    move-result-object v0

    .line 198
    invoke-virtual {p0, v0}, Lcrh;->iy(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcrh;->mSettings:Lcke;

    invoke-interface {v1}, Lcke;->NE()Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    .line 201
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 203
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcrh;->a(Landroid/accounts/Account;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static fp(I)V
    .locals 2

    .prologue
    .line 709
    const/16 v0, 0x7a

    invoke-static {v0}, Lege;->hs(I)Litu;

    move-result-object v0

    .line 711
    new-instance v1, Liti;

    invoke-direct {v1}, Liti;-><init>()V

    invoke-virtual {v1, p0}, Liti;->ms(I)Liti;

    move-result-object v1

    iput-object v1, v0, Litu;->dIK:Liti;

    .line 712
    invoke-static {v0}, Lege;->a(Litu;)V

    .line 713
    return-void
.end method

.method private notifyChanged()V
    .locals 3

    .prologue
    .line 467
    iget-object v0, p0, Lcrh;->aMD:Leqo;

    new-instance v1, Lcrk;

    const-string v2, "notifyAccountsChanged"

    invoke-direct {v1, p0, v2}, Lcrk;-><init>(Lcrh;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Leqo;->execute(Ljava/lang/Runnable;)V

    .line 473
    return-void
.end method


# virtual methods
.method public final declared-synchronized Az()Landroid/accounts/Account;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 399
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcrh;->Sq()V

    .line 402
    iget-object v0, p0, Lcrh;->mAccount:Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 399
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final KE()I
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcrh;->bha:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcin;

    invoke-interface {v0}, Lcin;->KE()I

    move-result v0

    return v0
.end method

.method public final declared-synchronized Sr()[Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 160
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcrh;->Sq()V

    .line 161
    iget-object v0, p0, Lcrh;->bhb:[Landroid/accounts/Account;

    if-nez v0, :cond_1

    .line 162
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_0
    monitor-exit p0

    return-object v0

    .line 164
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcrh;->bhb:[Landroid/accounts/Account;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    .line 165
    :goto_0
    iget-object v2, p0, Lcrh;->bhb:[Landroid/accounts/Account;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 166
    iget-object v2, p0, Lcrh;->bhb:[Landroid/accounts/Account;

    aget-object v2, v2, v1

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v2, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized Ss()[Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcrh;->Sq()V

    .line 173
    iget-object v0, p0, Lcrh;->bhb:[Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized St()Z
    .locals 1

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final Su()Z
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcrh;->bha:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcin;

    invoke-virtual {p0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v0, v1}, Lcin;->e(Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method public final Sv()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 252
    invoke-virtual {p0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v3

    .line 253
    if-nez v3, :cond_0

    move v0, v1

    .line 257
    :goto_0
    return v0

    .line 256
    :cond_0
    iget-object v0, p0, Lcrh;->bha:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcin;

    .line 257
    invoke-interface {v0, v3}, Lcin;->i(Landroid/accounts/Account;)I

    move-result v0

    if-ne v0, v2, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized Sw()V
    .locals 2

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcrh;->Sq()V

    .line 262
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcrh;->a(Landroid/accounts/Account;Z)V

    .line 263
    invoke-direct {p0}, Lcrh;->notifyChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    monitor-exit p0

    return-void

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final Sx()V
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcrh;->bgZ:Lcrf;

    invoke-interface {v0}, Lcrf;->So()V

    .line 478
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;Lemy;)V
    .locals 9

    .prologue
    .line 417
    invoke-virtual {p0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v7

    .line 418
    iget-object v8, p0, Lcrh;->aTW:Ljava/util/concurrent/Executor;

    new-instance v0, Lcrj;

    const-string v2, "get link"

    const/4 v1, 0x1

    new-array v3, v1, [I

    const/4 v1, 0x0

    const/4 v4, 0x2

    aput v4, v3, v1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v7}, Lcrj;-><init>(Lcrh;Ljava/lang/String;[ILandroid/net/Uri;Ljava/lang/String;Lemy;Landroid/accounts/Account;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 425
    return-void
.end method

.method public final a(Lefk;)V
    .locals 5

    .prologue
    .line 344
    iget-object v0, p0, Lcrh;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "mail"

    invoke-static {v4}, Lbwf;->fI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Lcri;

    invoke-direct {v3, p0, p1}, Lcri;-><init>(Lcrh;Lefk;)V

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/accounts/AccountManager;->getAccountsByTypeAndFeatures(Ljava/lang/String;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 376
    return-void
.end method

.method public final b(Landroid/accounts/Account;Ljava/lang/String;J)Lcrm;
    .locals 3
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 518
    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, p1, p2, v0, v1}, Lcrh;->c(Landroid/accounts/Account;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    .line 519
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 520
    new-instance v0, Lcrm;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Lcrm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(Ljava/lang/String;Ljava/lang/String;Leua;)Ljava/lang/String;
    .locals 9
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 671
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v6

    .line 704
    :goto_0
    return-object v0

    :cond_0
    move v7, v0

    .line 675
    :goto_1
    if-gt v7, v8, :cond_1

    .line 677
    :try_start_0
    const-string v0, "LoginHelper: get token"

    invoke-virtual {p3, v0}, Leua;->beginSection(Ljava/lang/String;)V

    .line 678
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 679
    iget-object v0, p0, Lcrh;->bgZ:Lcrf;

    iget-object v1, p0, Lcrh;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move-object v2, p2

    move-object v3, p1

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcrf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Leua;)Ljava/lang/String;
    :try_end_0
    .catch Lbfb; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbew; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 701
    invoke-virtual {p3}, Leua;->endSection()V

    goto :goto_0

    .line 686
    :catch_0
    move-exception v0

    .line 687
    :try_start_1
    const-string v1, "Search.LoginHelper"

    const-string v2, "User recoverable exception for scope: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x5

    invoke-static {v0, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 688
    const/4 v0, 0x1

    invoke-static {v0}, Lcrh;->fp(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 690
    invoke-virtual {p3}, Leua;->endSection()V

    move-object v0, v6

    goto :goto_0

    .line 691
    :catch_1
    move-exception v0

    .line 692
    :try_start_2
    const-string v1, "Search.LoginHelper"

    const-string v2, "Google auth exception for scope: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x5

    invoke-static {v0, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 693
    const/4 v0, 0x2

    invoke-static {v0}, Lcrh;->fp(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 695
    invoke-virtual {p3}, Leua;->endSection()V

    move-object v0, v6

    goto :goto_0

    .line 696
    :catch_2
    move-exception v0

    .line 697
    :try_start_3
    const-string v1, "Search.LoginHelper"

    const-string v2, "IO exception for scope: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x5

    invoke-static {v0, v1, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 698
    const/4 v0, 0x3

    invoke-static {v0}, Lcrh;->fp(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 701
    invoke-virtual {p3}, Leua;->endSection()V

    .line 675
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 701
    :catchall_0
    move-exception v0

    invoke-virtual {p3}, Leua;->endSection()V

    throw v0

    :cond_1
    move-object v0, v6

    .line 704
    goto :goto_0
.end method

.method public final c(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 429
    invoke-static {}, Lenu;->auQ()V

    .line 430
    invoke-virtual {p0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v2

    .line 431
    if-nez v2, :cond_0

    .line 432
    const-string v0, "Search.LoginHelper"

    const-string v2, "blockingGetGaiaWebLoginLink: account null, returning."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x5

    invoke-static {v4, v0, v2, v3}, Leor;->a(ILjava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 448
    :goto_0
    return-object v0

    .line 438
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 440
    if-nez p2, :cond_1

    .line 441
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "weblogin:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "continue="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&de=1"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 447
    :goto_1
    const-wide/16 v4, 0x7530

    invoke-virtual {p0, v2, v0, v4, v5}, Lcrh;->c(Landroid/accounts/Account;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 448
    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    .line 444
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "weblogin:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "service="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&continue="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "&de=1"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 448
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Landroid/accounts/Account;Ljava/lang/String;J)Ljava/lang/String;
    .locals 7
    .param p1    # Landroid/accounts/Account;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 549
    invoke-static {}, Lenu;->auQ()V

    .line 550
    if-nez p1, :cond_0

    .line 551
    const/4 v0, 0x0

    .line 554
    :goto_0
    return-object v0

    .line 553
    :cond_0
    new-instance v5, Leua;

    invoke-direct {v5}, Leua;-><init>()V

    .line 554
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, p2, v0, v5}, Lcrh;->a(Ljava/lang/String;Ljava/lang/String;Leua;)Lepl;

    move-result-object v1

    const/4 v4, 0x1

    iget-object v0, p0, Lcrh;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move-wide v2, p3

    invoke-static/range {v1 .. v6}, Lcrh;->a(Ljava/util/concurrent/Future;JZLeua;Landroid/content/res/Resources;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final g(Ljava/util/Collection;)V
    .locals 2

    .prologue
    .line 634
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 635
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcrh;->iC(Ljava/lang/String;)V

    goto :goto_0

    .line 637
    :cond_0
    return-void
.end method

.method public final declared-synchronized iA(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 298
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v1

    .line 302
    iget-object v0, p0, Lcrh;->mSettings:Lcke;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v2}, Lcke;->i(Ljava/lang/String;Z)V

    .line 303
    invoke-virtual {p0}, Lcrh;->refresh()V

    .line 306
    invoke-virtual {p0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v2

    .line 307
    iget-object v0, p0, Lcrh;->bha:Ligi;

    invoke-interface {v0}, Ligi;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcin;

    invoke-interface {v0, v1, v2}, Lcin;->a(Landroid/accounts/Account;Landroid/accounts/Account;)V

    .line 309
    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcrh;->Z(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    monitor-exit p0

    return-void

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final iB(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 391
    invoke-virtual {p0}, Lcrh;->yM()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final iC(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 620
    iget-object v0, p0, Lcrh;->bgZ:Lcrf;

    iget-object v1, p0, Lcrh;->mContext:Landroid/content/Context;

    invoke-interface {v0, v1, p1}, Lcrf;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 621
    return-void
.end method

.method public final declared-synchronized iy(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 212
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcrh;->Sq()V

    .line 213
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcrh;->bhb:[Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 221
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 216
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcrh;->bhb:[Landroid/accounts/Account;

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v0, v3, v2

    .line 217
    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-nez v5, :cond_1

    .line 216
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 221
    goto :goto_0

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized iz(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcrh;->iy(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 233
    if-eqz v0, :cond_0

    .line 234
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcrh;->a(Landroid/accounts/Account;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    .line 236
    :cond_0
    :try_start_1
    new-instance v0, Landroid/accounts/AccountsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setAccountToUseByName: Account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final k(Ljava/lang/String;J)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 535
    invoke-virtual {p0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcrh;->c(Landroid/accounts/Account;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l(Ljava/lang/String;J)Ljava/util/Collection;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 583
    invoke-static {}, Lenu;->auQ()V

    .line 584
    invoke-virtual {p0}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v7

    .line 587
    new-instance v5, Leua;

    invoke-direct {v5}, Leua;-><init>()V

    .line 588
    invoke-virtual {p0}, Lcrh;->Ss()[Landroid/accounts/Account;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v3, v1

    move v0, v4

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v6, v1, v0

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5}, Leua;->avT()Leua;

    move-result-object v8

    invoke-direct {p0, p1, v6, v8}, Lcrh;->a(Ljava/lang/String;Ljava/lang/String;Leua;)Lepl;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Livg;->A(Ljava/lang/Iterable;)Livq;

    move-result-object v1

    iget-object v0, p0, Lcrh;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move-wide v2, p2

    invoke-static/range {v1 .. v6}, Lcrh;->a(Ljava/util/concurrent/Future;JZLeua;Landroid/content/res/Resources;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 590
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 591
    if-eqz v0, :cond_3

    .line 592
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_2

    .line 593
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 594
    aget-object v1, v7, v4

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 592
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 599
    :goto_2
    return-object v0

    :cond_3
    invoke-static {}, Lijj;->aWW()Lijj;

    move-result-object v0

    goto :goto_2
.end method

.method public final declared-synchronized refresh()V
    .locals 2

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcrh;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 185
    invoke-direct {p0, v0}, Lcrh;->a([Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    monitor-exit p0

    return-void

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcrh;->bgY:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 457
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcrh;->bgY:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 464
    return-void
.end method

.method public final yM()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 384
    invoke-virtual {p0}, Lcrh;->Az()Landroid/accounts/Account;

    move-result-object v0

    .line 385
    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
