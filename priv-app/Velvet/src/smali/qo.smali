.class final Lqo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final tb:[Lqx;

.field private tc:[B

.field private td:Ljava/util/ArrayList;

.field private final te:Ljava/nio/ByteOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v2, 0x49

    const/4 v3, 0x0

    .line 36
    const/16 v0, 0x8

    new-array v0, v0, [B

    const/16 v1, 0x41

    aput-byte v1, v0, v3

    const/16 v1, 0x53

    aput-byte v1, v0, v4

    const/16 v1, 0x43

    aput-byte v1, v0, v5

    aput-byte v2, v0, v6

    const/4 v1, 0x4

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    const/4 v1, 0x6

    aput-byte v3, v0, v1

    const/4 v1, 0x7

    aput-byte v3, v0, v1

    .line 39
    const/16 v0, 0x8

    new-array v0, v0, [B

    const/16 v1, 0x4a

    aput-byte v1, v0, v3

    aput-byte v2, v0, v4

    const/16 v1, 0x53

    aput-byte v1, v0, v5

    aput-byte v3, v0, v6

    const/4 v1, 0x4

    aput-byte v3, v0, v1

    const/4 v1, 0x5

    aput-byte v3, v0, v1

    const/4 v1, 0x6

    aput-byte v3, v0, v1

    const/4 v1, 0x7

    aput-byte v3, v0, v1

    .line 42
    const/16 v0, 0x8

    new-array v0, v0, [B

    const/16 v1, 0x55

    aput-byte v1, v0, v3

    const/16 v1, 0x4e

    aput-byte v1, v0, v4

    aput-byte v2, v0, v5

    const/16 v1, 0x43

    aput-byte v1, v0, v6

    const/4 v1, 0x4

    const/16 v2, 0x4f

    aput-byte v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x44

    aput-byte v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x45

    aput-byte v2, v0, v1

    const/4 v1, 0x7

    aput-byte v3, v0, v1

    return-void
.end method

.method constructor <init>(Ljava/nio/ByteOrder;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x5

    new-array v0, v0, [Lqx;

    iput-object v0, p0, Lqo;->tb:[Lqx;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lqo;->td:Ljava/util/ArrayList;

    .line 52
    iput-object p1, p0, Lqo;->te:Ljava/nio/ByteOrder;

    .line 53
    return-void
.end method


# virtual methods
.method protected final a(SI)Lqw;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lqo;->tb:[Lqx;

    aget-object v0, v0, p2

    .line 161
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, Lqx;->e(S)Lqw;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(I[B)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 84
    iget-object v0, p0, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 91
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_1
    if-ge v0, p1, :cond_1

    .line 87
    iget-object v1, p0, Lqo;->td:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 89
    :cond_1
    iget-object v0, p0, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected final a(Lqx;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lqo;->tb:[Lqx;

    invoke-virtual {p1}, Lqx;->getId()I

    move-result v1

    aput-object p1, v0, v1

    .line 140
    return-void
.end method

.method protected final a([B)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lqo;->tc:[B

    .line 70
    return-void
.end method

.method protected final aA(I)Lqx;
    .locals 1

    .prologue
    .line 128
    invoke-static {p1}, Lqw;->aF(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lqo;->tb:[Lqx;

    aget-object v0, v0, p1

    .line 131
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 318
    if-ne p0, p1, :cond_1

    move v3, v4

    .line 345
    :cond_0
    :goto_0
    return v3

    .line 321
    :cond_1
    if-eqz p1, :cond_0

    .line 324
    instance-of v0, p1, Lqo;

    if-eqz v0, :cond_0

    .line 325
    check-cast p1, Lqo;

    .line 326
    iget-object v0, p1, Lqo;->te:Ljava/nio/ByteOrder;

    iget-object v1, p0, Lqo;->te:Ljava/nio/ByteOrder;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lqo;->tc:[B

    iget-object v1, p0, Lqo;->tc:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v3

    .line 331
    :goto_1
    iget-object v0, p0, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 332
    iget-object v0, p1, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v1, p0, Lqo;->td:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 336
    :goto_2
    const/4 v1, 0x5

    if-ge v0, v1, :cond_4

    .line 337
    invoke-virtual {p1, v0}, Lqo;->aA(I)Lqx;

    move-result-object v1

    .line 338
    invoke-virtual {p0, v0}, Lqo;->aA(I)Lqx;

    move-result-object v2

    .line 339
    if-eq v1, v2, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1, v2}, Lqx;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 336
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v3, v4

    .line 343
    goto :goto_0
.end method
