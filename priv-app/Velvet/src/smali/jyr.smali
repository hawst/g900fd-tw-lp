.class public final Ljyr;
.super Ljsl;
.source "PG"


# instance fields
.field private aez:I

.field private eMp:J

.field public eMq:[Ljyo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1207
    invoke-direct {p0}, Ljsl;-><init>()V

    .line 1208
    const/4 v0, 0x0

    iput v0, p0, Ljyr;->aez:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljyr;->eMp:J

    invoke-static {}, Ljyo;->bwi()[Ljyo;

    move-result-object v0

    iput-object v0, p0, Ljyr;->eMq:[Ljyo;

    const/4 v0, 0x0

    iput-object v0, p0, Ljyr;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljyr;->eCz:I

    .line 1209
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1166
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljyr;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljyr;->eMp:J

    iget v0, p0, Ljyr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyr;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljyr;->eMq:[Ljyo;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljyo;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljyr;->eMq:[Ljyo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljyo;

    invoke-direct {v3}, Ljyo;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljyr;->eMq:[Ljyo;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljyo;

    invoke-direct {v3}, Ljyo;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljyr;->eMq:[Ljyo;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    .prologue
    .line 1223
    iget v0, p0, Ljyr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 1224
    const/4 v0, 0x1

    iget-wide v2, p0, Ljyr;->eMp:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    .line 1226
    :cond_0
    iget-object v0, p0, Ljyr;->eMq:[Ljyo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljyr;->eMq:[Ljyo;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1227
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljyr;->eMq:[Ljyo;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1228
    iget-object v1, p0, Ljyr;->eMq:[Ljyo;

    aget-object v1, v1, v0

    .line 1229
    if-eqz v1, :cond_1

    .line 1230
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljsj;->a(ILjsr;)V

    .line 1227
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1234
    :cond_2
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    .line 1235
    return-void
.end method

.method public final bwv()Z
    .locals 1

    .prologue
    .line 1196
    iget v0, p0, Ljyr;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dT(J)Ljyr;
    .locals 1

    .prologue
    .line 1191
    iput-wide p1, p0, Ljyr;->eMp:J

    .line 1192
    iget v0, p0, Ljyr;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljyr;->aez:I

    .line 1193
    return-object p0
.end method

.method protected final lF()I
    .locals 5

    .prologue
    .line 1239
    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    .line 1240
    iget v1, p0, Ljyr;->aez:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 1241
    const/4 v1, 0x1

    iget-wide v2, p0, Ljyr;->eMp:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1244
    :cond_0
    iget-object v1, p0, Ljyr;->eMq:[Ljyo;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljyr;->eMq:[Ljyo;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 1245
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Ljyr;->eMq:[Ljyo;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1246
    iget-object v2, p0, Ljyr;->eMq:[Ljyo;

    aget-object v2, v2, v0

    .line 1247
    if-eqz v2, :cond_1

    .line 1248
    const/4 v3, 0x2

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1245
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1253
    :cond_3
    return v0
.end method
