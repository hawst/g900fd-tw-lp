.class public final Lhmq;
.super Lhlu;
.source "PG"


# instance fields
.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Leoj;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lhlu;-><init>(Leoj;)V

    .line 25
    iput-object p2, p0, Lhmq;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 26
    return-void
.end method

.method private static aX(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 76
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 81
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    :cond_0
    return-object v0
.end method

.method private static oh(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://play.google.com/store/apps/details?id=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;
    .locals 2

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/search/shared/actions/SocialUpdateAction;

    invoke-super {p0, p1}, Lhlu;->G(Lcom/google/android/search/shared/actions/VoiceAction;)Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/shared/util/MatchingAppInfo;->avi()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/shared/util/MatchingAppInfo;->avc()Lcom/google/android/shared/util/MatchingAppInfo;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected final synthetic J(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/search/shared/actions/SocialUpdateAction;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SocialUpdateAction;->aiN()Ldvl;

    move-result-object v2

    invoke-virtual {v2}, Ldvl;->aiO()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lhmq;->aX(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic K(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/search/shared/actions/SocialUpdateAction;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    iget-object v2, p0, Lhmq;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SocialUpdateAction;->aiN()Ldvl;

    move-result-object v3

    invoke-virtual {v3}, Ldvl;->aiO()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected final synthetic L(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 4

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/search/shared/actions/SocialUpdateAction;

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SocialUpdateAction;->aiN()Ldvl;

    move-result-object v0

    invoke-virtual {v0}, Ldvl;->aiO()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SocialUpdateAction;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lhmq;->aX(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {v0}, Lhmq;->oh(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v1, v2

    return-object v1
.end method

.method protected final bridge synthetic M(Lcom/google/android/search/shared/actions/VoiceAction;)[Landroid/content/Intent;
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z
    .locals 4

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/search/shared/actions/SocialUpdateAction;

    const/16 v0, 0x65

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lhmq;->mIntentStarter:Leoj;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/search/shared/actions/SocialUpdateAction;->aiN()Ldvl;

    move-result-object v3

    invoke-virtual {v3}, Ldvl;->aiO()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lhmq;->oh(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Leoj;->b([Landroid/content/Intent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lhlu;->d(Lcom/google/android/search/shared/actions/VoiceAction;I)Z

    move-result v0

    goto :goto_0
.end method
