.class public Lcmg;
.super Ldjz;
.source "PG"


# static fields
.field private static final bbF:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final bbG:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lens;

    invoke-direct {v0}, Lens;-><init>()V

    sput-object v0, Lcmg;->bbF:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Ldkc;Legl;)V
    .locals 3

    .prologue
    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [Ldkc;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lilw;->y(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lcmg;->bbF:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0, v1, v2, p2}, Ldjz;-><init>(Ljava/util/Queue;Lcng;Ljava/util/concurrent/ExecutorService;Legl;)V

    .line 29
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcmg;->bbG:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 34
    return-void
.end method


# virtual methods
.method protected final PX()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "runReadTask should not be called on ForwardingChunkProducer"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final PY()V
    .locals 0

    .prologue
    .line 63
    invoke-virtual {p0}, Lcmg;->Qd()Z

    .line 64
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 59
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/io/IOException;

    :goto_0
    invoke-virtual {p0, p1}, Lcmg;->c(Ljava/lang/Exception;)Z

    .line 60
    return-void

    .line 59
    :cond_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public final start()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public final stop()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public final w([B)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcmg;->bbG:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    .line 53
    new-instance v1, Ldkd;

    invoke-direct {v1, p1, v0}, Ldkd;-><init>([BI)V

    invoke-virtual {p0, v1}, Lcmg;->b(Ldkd;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lefs;

    const v1, 0x30009

    invoke-direct {v0, v1}, Lefs;-><init>(I)V

    throw v0

    .line 56
    :cond_0
    return-void
.end method
