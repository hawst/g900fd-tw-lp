.class final Lfhg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfqc;


# instance fields
.field private synthetic crC:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

.field private synthetic crD:Lfhf;


# direct methods
.method constructor <init>(Lfhf;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lfhg;->crD:Lfhf;

    iput-object p2, p0, Lfhg;->crC:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;I)V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Lfoz;)V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public final a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;Lizj;)V
    .locals 2
    .param p3    # Lizj;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 182
    iget-object v0, p0, Lfhg;->crC:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0}, Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;->aCy()Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;

    move-result-object v0

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Unexpected answered question"

    invoke-static {v0, v1}, Lifv;->d(ZLjava/lang/Object;)V

    .line 184
    iget-object v0, p0, Lfhg;->crD:Lfhf;

    invoke-static {v0}, Lfhf;->a(Lfhf;)Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;

    move-result-object v0

    iget-object v1, p0, Lfhg;->crC:Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/sidekick/main/training/IcebreakerSectionAdapter;->a(Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestionNode;Ljdf;)V

    .line 185
    iget-object v0, p0, Lfhg;->crD:Lfhf;

    invoke-static {v0}, Lfhf;->b(Lfhf;)Lfhi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lfhg;->crD:Lfhf;

    invoke-static {v0}, Lfhf;->b(Lfhf;)Lfhi;

    move-result-object v0

    iget-object v1, p0, Lfhg;->crD:Lfhf;

    invoke-static {v1}, Lfhf;->c(Lfhf;)Lfhn;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Lfhi;->a(Lfhn;Lcom/google/android/sidekick/shared/remoteapi/TrainingQuestion;Ljdf;)V

    .line 188
    :cond_0
    return-void

    .line 182
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
