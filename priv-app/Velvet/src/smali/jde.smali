.class public final Ljde;
.super Ljsl;
.source "PG"


# static fields
.field private static volatile ebt:[Ljde;


# instance fields
.field private aez:I

.field private dLZ:[B

.field public dUY:[Liwk;

.field private dVN:J

.field private ebA:J

.field private ebB:J

.field private ebC:[B

.field private ebu:J

.field public ebv:[Ljdg;

.field public ebw:[Ljdg;

.field public ebx:[Ljdg;

.field private eby:Ljava/lang/String;

.field public ebz:Ljdf;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Ljsl;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ljde;->aez:I

    iput-wide v2, p0, Ljde;->ebu:J

    invoke-static {}, Ljdg;->bhl()[Ljdg;

    move-result-object v0

    iput-object v0, p0, Ljde;->ebv:[Ljdg;

    invoke-static {}, Ljdg;->bhl()[Ljdg;

    move-result-object v0

    iput-object v0, p0, Ljde;->ebw:[Ljdg;

    invoke-static {}, Ljdg;->bhl()[Ljdg;

    move-result-object v0

    iput-object v0, p0, Ljde;->ebx:[Ljdg;

    const-string v0, ""

    iput-object v0, p0, Ljde;->eby:Ljava/lang/String;

    iput-object v1, p0, Ljde;->ebz:Ljdf;

    iput-wide v2, p0, Ljde;->ebA:J

    iput-wide v2, p0, Ljde;->ebB:J

    invoke-static {}, Liwk;->aZn()[Liwk;

    move-result-object v0

    iput-object v0, p0, Ljde;->dUY:[Liwk;

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljde;->ebC:[B

    sget-object v0, Ljsu;->eCG:[B

    iput-object v0, p0, Ljde;->dLZ:[B

    iput-wide v2, p0, Ljde;->dVN:J

    iput-object v1, p0, Ljde;->eCq:Ljsn;

    const/4 v0, -0x1

    iput v0, p0, Ljde;->eCz:I

    return-void
.end method

.method public static bgW()[Ljde;
    .locals 2

    sget-object v0, Ljde;->ebt:[Ljde;

    if-nez v0, :cond_1

    sget-object v1, Ljsp;->eCy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljde;->ebt:[Ljde;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljde;

    sput-object v0, Ljde;->ebt:[Ljde;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Ljde;->ebt:[Ljde;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final synthetic a(Ljsi;)Ljsr;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljsi;->btN()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ljde;->a(Ljsi;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljde;->ebu:J

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljde;->aez:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljde;->ebv:[Ljdg;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdg;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljde;->ebv:[Ljdg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Ljdg;

    invoke-direct {v3}, Ljdg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljde;->ebv:[Ljdg;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Ljdg;

    invoke-direct {v3}, Ljdg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljde;->ebv:[Ljdg;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljde;->ebz:Ljdf;

    if-nez v0, :cond_4

    new-instance v0, Ljdf;

    invoke-direct {v0}, Ljdf;-><init>()V

    iput-object v0, p0, Ljde;->ebz:Ljdf;

    :cond_4
    iget-object v0, p0, Ljde;->ebz:Ljdf;

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljde;->ebx:[Ljdg;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdg;

    if-eqz v0, :cond_5

    iget-object v3, p0, Ljde;->ebx:[Ljdg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Ljdg;

    invoke-direct {v3}, Ljdg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Ljde;->ebx:[Ljdg;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Ljdg;

    invoke-direct {v3}, Ljdg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljde;->ebx:[Ljdg;

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljde;->dUY:[Liwk;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Liwk;

    if-eqz v0, :cond_8

    iget-object v3, p0, Ljde;->dUY:[Liwk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    new-instance v3, Liwk;

    invoke-direct {v3}, Liwk;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Ljde;->dUY:[Liwk;

    array-length v0, v0

    goto :goto_5

    :cond_a
    new-instance v3, Liwk;

    invoke-direct {v3}, Liwk;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljde;->dUY:[Liwk;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljde;->ebC:[B

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljde;->aez:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljde;->ebA:J

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljde;->aez:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Ljsi;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Ljde;->dLZ:[B

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Ljde;->aez:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljde;->ebB:J

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljde;->aez:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Ljsi;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljde;->eby:Ljava/lang/String;

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljde;->aez:I

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Ljsu;->c(Ljsi;I)I

    move-result v2

    iget-object v0, p0, Ljde;->ebw:[Ljdg;

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Ljdg;

    if-eqz v0, :cond_b

    iget-object v3, p0, Ljde;->ebw:[Ljdg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Ljdg;

    invoke-direct {v3}, Ljdg;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Ljsi;->k(Ljsr;)V

    invoke-virtual {p1}, Ljsi;->btN()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Ljde;->ebw:[Ljdg;

    array-length v0, v0

    goto :goto_7

    :cond_d
    new-instance v3, Ljdg;

    invoke-direct {v3}, Ljdg;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljsi;->k(Ljsr;)V

    iput-object v2, p0, Ljde;->ebw:[Ljdg;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Ljsi;->btR()J

    move-result-wide v2

    iput-wide v2, p0, Ljde;->dVN:J

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Ljde;->aez:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Ljsj;)V
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v2, p0, Ljde;->ebu:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_0
    iget-object v0, p0, Ljde;->ebv:[Ljdg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljde;->ebv:[Ljdg;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljde;->ebv:[Ljdg;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ljde;->ebv:[Ljdg;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ljde;->ebz:Ljdf;

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    iget-object v2, p0, Ljde;->ebz:Ljdf;

    invoke-virtual {p1, v0, v2}, Ljsj;->a(ILjsr;)V

    :cond_3
    iget-object v0, p0, Ljde;->ebx:[Ljdg;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljde;->ebx:[Ljdg;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v2, p0, Ljde;->ebx:[Ljdg;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Ljde;->ebx:[Ljdg;

    aget-object v2, v2, v0

    if-eqz v2, :cond_4

    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Ljde;->dUY:[Liwk;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ljde;->dUY:[Liwk;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    :goto_2
    iget-object v2, p0, Ljde;->dUY:[Liwk;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Ljde;->dUY:[Liwk;

    aget-object v2, v2, v0

    if-eqz v2, :cond_6

    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Ljsj;->a(ILjsr;)V

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_8

    const/4 v0, 0x6

    iget-object v2, p0, Ljde;->ebC:[B

    invoke-virtual {p1, v0, v2}, Ljsj;->c(I[B)V

    :cond_8
    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_9

    const/4 v0, 0x7

    iget-wide v2, p0, Ljde;->ebA:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_9
    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_a

    const/16 v0, 0x8

    iget-object v2, p0, Ljde;->dLZ:[B

    invoke-virtual {p1, v0, v2}, Ljsj;->c(I[B)V

    :cond_a
    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_b

    const/16 v0, 0x9

    iget-wide v2, p0, Ljde;->ebB:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_b
    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_c

    const/16 v0, 0xa

    iget-object v2, p0, Ljde;->eby:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Ljsj;->p(ILjava/lang/String;)V

    :cond_c
    iget-object v0, p0, Ljde;->ebw:[Ljdg;

    if-eqz v0, :cond_e

    iget-object v0, p0, Ljde;->ebw:[Ljdg;

    array-length v0, v0

    if-lez v0, :cond_e

    :goto_3
    iget-object v0, p0, Ljde;->ebw:[Ljdg;

    array-length v0, v0

    if-ge v1, v0, :cond_e

    iget-object v0, p0, Ljde;->ebw:[Ljdg;

    aget-object v0, v0, v1

    if-eqz v0, :cond_d

    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Ljsj;->a(ILjsr;)V

    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_e
    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_f

    const/16 v0, 0xc

    iget-wide v2, p0, Ljde;->dVN:J

    invoke-virtual {p1, v0, v2, v3}, Ljsj;->h(IJ)V

    :cond_f
    invoke-super {p0, p1}, Ljsl;->a(Ljsj;)V

    return-void
.end method

.method public final ai([B)Ljde;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Ljde;->ebC:[B

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Ljde;->aez:I

    return-object p0
.end method

.method public final bgX()J
    .locals 2

    iget-wide v0, p0, Ljde;->ebu:J

    return-wide v0
.end method

.method public final bgY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljde;->eby:Ljava/lang/String;

    return-object v0
.end method

.method public final bgZ()Z
    .locals 1

    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bha()J
    .locals 2

    iget-wide v0, p0, Ljde;->ebA:J

    return-wide v0
.end method

.method public final bhb()Z
    .locals 1

    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhc()J
    .locals 2

    iget-wide v0, p0, Ljde;->ebB:J

    return-wide v0
.end method

.method public final bhd()Z
    .locals 1

    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bhe()[B
    .locals 1

    iget-object v0, p0, Ljde;->ebC:[B

    return-object v0
.end method

.method public final bhf()Z
    .locals 1

    iget v0, p0, Ljde;->aez:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final cR(J)Ljde;
    .locals 1

    iput-wide p1, p0, Ljde;->ebu:J

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljde;->aez:I

    return-object p0
.end method

.method public final cS(J)Ljde;
    .locals 1

    iput-wide p1, p0, Ljde;->ebA:J

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ljde;->aez:I

    return-object p0
.end method

.method public final cT(J)Ljde;
    .locals 2

    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Ljde;->ebB:J

    iget v0, p0, Ljde;->aez:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ljde;->aez:I

    return-object p0
.end method

.method protected final lF()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Ljsl;->lF()I

    move-result v0

    iget v2, p0, Ljde;->aez:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-wide v4, p0, Ljde;->ebu:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Ljde;->ebv:[Ljdg;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljde;->ebv:[Ljdg;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Ljde;->ebv:[Ljdg;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Ljde;->ebv:[Ljdg;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    :cond_3
    iget-object v2, p0, Ljde;->ebz:Ljdf;

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    iget-object v3, p0, Ljde;->ebz:Ljdf;

    invoke-static {v2, v3}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Ljde;->ebx:[Ljdg;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ljde;->ebx:[Ljdg;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v0

    move v0, v1

    :goto_1
    iget-object v3, p0, Ljde;->ebx:[Ljdg;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    iget-object v3, p0, Ljde;->ebx:[Ljdg;

    aget-object v3, v3, v0

    if-eqz v3, :cond_5

    const/4 v4, 0x4

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    move v0, v2

    :cond_7
    iget-object v2, p0, Ljde;->dUY:[Liwk;

    if-eqz v2, :cond_a

    iget-object v2, p0, Ljde;->dUY:[Liwk;

    array-length v2, v2

    if-lez v2, :cond_a

    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Ljde;->dUY:[Liwk;

    array-length v3, v3

    if-ge v0, v3, :cond_9

    iget-object v3, p0, Ljde;->dUY:[Liwk;

    aget-object v3, v3, v0

    if-eqz v3, :cond_8

    const/4 v4, 0x5

    invoke-static {v4, v3}, Ljsj;->c(ILjsr;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    move v0, v2

    :cond_a
    iget v2, p0, Ljde;->aez:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_b

    const/4 v2, 0x6

    iget-object v3, p0, Ljde;->ebC:[B

    invoke-static {v2, v3}, Ljsj;->d(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget v2, p0, Ljde;->aez:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_c

    const/4 v2, 0x7

    iget-wide v4, p0, Ljde;->ebA:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget v2, p0, Ljde;->aez:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_d

    const/16 v2, 0x8

    iget-object v3, p0, Ljde;->dLZ:[B

    invoke-static {v2, v3}, Ljsj;->d(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget v2, p0, Ljde;->aez:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_e

    const/16 v2, 0x9

    iget-wide v4, p0, Ljde;->ebB:J

    invoke-static {v2, v4, v5}, Ljsj;->k(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    iget v2, p0, Ljde;->aez:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_f

    const/16 v2, 0xa

    iget-object v3, p0, Ljde;->eby:Ljava/lang/String;

    invoke-static {v2, v3}, Ljsj;->q(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_f
    iget-object v2, p0, Ljde;->ebw:[Ljdg;

    if-eqz v2, :cond_11

    iget-object v2, p0, Ljde;->ebw:[Ljdg;

    array-length v2, v2

    if-lez v2, :cond_11

    :goto_3
    iget-object v2, p0, Ljde;->ebw:[Ljdg;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    iget-object v2, p0, Ljde;->ebw:[Ljdg;

    aget-object v2, v2, v1

    if-eqz v2, :cond_10

    const/16 v3, 0xb

    invoke-static {v3, v2}, Ljsj;->c(ILjsr;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_11
    iget v1, p0, Ljde;->aez:I

    and-int/lit8 v1, v1, 0x40

    if-eqz v1, :cond_12

    const/16 v1, 0xc

    iget-wide v2, p0, Ljde;->dVN:J

    invoke-static {v1, v2, v3}, Ljsj;->k(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    return v0
.end method
