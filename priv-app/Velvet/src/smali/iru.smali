.class public final Liru;
.super Lihy;
.source "PG"


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private transient dGL:Ljava/util/Comparator;

.field private transient dGM:Ljava/util/Comparator;


# direct methods
.method private constructor <init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
    .locals 1

    .prologue
    .line 113
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0, p1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    invoke-direct {p0, v0}, Lihy;-><init>(Ljava/util/Map;)V

    .line 114
    iput-object p1, p0, Liru;->dGL:Ljava/util/Comparator;

    .line 115
    iput-object p2, p0, Liru;->dGM:Ljava/util/Comparator;

    .line 116
    return-void
.end method

.method public static a(Ljava/util/Comparator;Ljava/util/Comparator;)Liru;
    .locals 3

    .prologue
    .line 95
    new-instance v2, Liru;

    invoke-static {p0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {p1}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Comparator;

    invoke-direct {v2, v0, v1}, Liru;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    return-object v2
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 189
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Liru;->dGL:Ljava/util/Comparator;

    .line 190
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v0}, Lifv;->bf(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    iput-object v0, p0, Liru;->dGM:Ljava/util/Comparator;

    .line 191
    new-instance v0, Ljava/util/TreeMap;

    iget-object v1, p0, Liru;->dGL:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    invoke-virtual {p0, v0}, Liru;->r(Ljava/util/Map;)V

    .line 192
    invoke-static {p0, p1}, Liqq;->a(Liph;Ljava/io/ObjectInputStream;)V

    .line 193
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 179
    iget-object v0, p0, Liru;->dGL:Ljava/util/Comparator;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 180
    iget-object v0, p0, Liru;->dGM:Ljava/util/Comparator;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 181
    invoke-static {p0, p1}, Liqq;->a(Liph;Ljava/io/ObjectOutputStream;)V

    .line 182
    return-void
.end method


# virtual methods
.method final synthetic aWC()Ljava/util/Set;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Liru;->aWE()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic aWD()Ljava/util/Set;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lihy;->aWD()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method final aWE()Ljava/util/SortedSet;
    .locals 2

    .prologue
    .line 134
    new-instance v0, Ljava/util/TreeSet;

    iget-object v1, p0, Liru;->dGM:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    return-object v0
.end method

.method final synthetic aWm()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Liru;->aWE()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic aWp()Ljava/util/Map;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lihy;->aWp()Ljava/util/Map;

    move-result-object v0

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public final bridge synthetic bp(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lihy;->bp(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic bq(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lihy;->bq(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Lihy;->clear()V

    return-void
.end method

.method public final bridge synthetic containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lihy;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic containsValue(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lihy;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lihy;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic hashCode()I
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lihy;->hashCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lihy;->keySet()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/util/SortedSet;

    return-object v0
.end method

.method public final bridge synthetic p(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1, p2}, Lihy;->p(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic q(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1, p2}, Lihy;->q(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1, p2}, Lihy;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic size()I
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lihy;->size()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lihy;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic values()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lihy;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
