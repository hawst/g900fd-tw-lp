.class public final Lfzh;
.super Lfro;
.source "PG"


# instance fields
.field private final mClock:Lemp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfmt;Lfzw;Lemp;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lfro;-><init>(Landroid/content/Context;Lfmt;Lfzw;)V

    .line 22
    iput-object p4, p0, Lfzh;->mClock:Lemp;

    .line 23
    return-void
.end method


# virtual methods
.method protected final aCZ()V
    .locals 8

    .prologue
    .line 32
    iget-object v0, p0, Lfro;->mView:Landroid/view/View;

    move-object v7, v0

    check-cast v7, Landroid/view/ViewGroup;

    .line 33
    new-instance v0, Lfze;

    iget-object v1, p0, Lfzh;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lfzh;->mCardContainer:Lfmt;

    invoke-virtual {p0}, Lfzh;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lfzh;->mActivityHelper:Lfzw;

    iget-object v5, p0, Lfro;->cBc:Lanh;

    iget-object v5, v5, Lanh;->ahu:Lizj;

    iget-object v6, p0, Lfzh;->mClock:Lemp;

    invoke-direct/range {v0 .. v6}, Lfze;-><init>(Landroid/content/Context;Lfmt;Landroid/view/LayoutInflater;Lfzw;Lizj;Lemp;)V

    iget-object v1, p0, Lfro;->cBc:Lanh;

    iget-object v1, v1, Lanh;->agX:Lapj;

    const/4 v2, 0x0

    invoke-virtual {v0, v7, v1, v2}, Lfze;->a(Landroid/view/ViewGroup;Lapj;Z)V

    .line 36
    return-void
.end method

.method protected final b(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 27
    const v0, 0x7f040139

    iget-object v1, p0, Lfro;->cBb:Lfuz;

    invoke-virtual {v1}, Lfuz;->aDf()Lcom/google/android/sidekick/shared/ui/qp/ModularCard;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
