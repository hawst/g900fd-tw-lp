.class public final Lwv;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# instance fields
.field private synthetic KX:Lcom/android/launcher3/Launcher;


# direct methods
.method public constructor <init>(Lcom/android/launcher3/Launcher;)V
    .locals 0

    .prologue
    .line 1603
    iput-object p1, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1606
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1607
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1608
    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0, v4}, Lcom/android/launcher3/Launcher;->a(Lcom/android/launcher3/Launcher;Z)Z

    .line 1609
    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->h(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/DragLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher3/DragLayer;->fL()V

    .line 1610
    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->i(Lcom/android/launcher3/Launcher;)V

    .line 1614
    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->j(Lcom/android/launcher3/Launcher;)Lcom/android/launcher3/AppsCustomizeTabHost;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->e(Lcom/android/launcher3/Launcher;)Lwq;

    move-result-object v0

    iget-wide v0, v0, Lwq;->JA:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1615
    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0, v4}, Lcom/android/launcher3/Launcher;->X(Z)V

    .line 1633
    :cond_0
    :goto_0
    return-void

    .line 1617
    :cond_1
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1618
    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/launcher3/Launcher;->a(Lcom/android/launcher3/Launcher;Z)Z

    .line 1619
    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-static {v0}, Lcom/android/launcher3/Launcher;->i(Lcom/android/launcher3/Launcher;)V

    goto :goto_0

    .line 1629
    :cond_2
    const-string v1, "android.intent.action.MANAGED_PROFILE_ADDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "android.intent.action.MANAGED_PROFILE_REMOVED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1631
    :cond_3
    iget-object v0, p0, Lwv;->KX:Lcom/android/launcher3/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher3/Launcher;->hs()Lzi;

    move-result-object v0

    invoke-virtual {v0}, Lzi;->iv()V

    goto :goto_0
.end method
