.class public final Lwi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final Je:Ljava/util/HashMap;

.field private final Jf:Laia;

.field private final Jg:Lahn;

.field public final Jh:Ljava/util/HashMap;

.field public Ji:I

.field public final mContext:Landroid/content/Context;

.field private final mPackageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lwi;->Je:Ljava/util/HashMap;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lwi;->Jh:Ljava/util/HashMap;

    .line 106
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 109
    iput-object p1, p0, Lwi;->mContext:Landroid/content/Context;

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lwi;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 111
    iget-object v1, p0, Lwi;->mContext:Landroid/content/Context;

    invoke-static {v1}, Laia;->D(Landroid/content/Context;)Laia;

    move-result-object v1

    iput-object v1, p0, Lwi;->Jf:Laia;

    .line 112
    iget-object v1, p0, Lwi;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lahn;->B(Landroid/content/Context;)Lahn;

    move-result-object v1

    iput-object v1, p0, Lwi;->Jg:Lahn;

    .line 113
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconDensity()I

    move-result v0

    iput v0, p0, Lwi;->Ji:I

    .line 116
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lwi;->Je:Ljava/util/HashMap;

    invoke-direct {p0, v0}, Lwi;->a(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    return-void
.end method

.method private a(Lahz;)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 179
    invoke-direct {p0}, Lwi;->gZ()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lwi;->Jf:Laia;

    invoke-virtual {v1, v0, p1}, Laia;->a(Landroid/graphics/drawable/Drawable;Lahz;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 184
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 185
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 186
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 187
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 188
    return-object v1
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;Lahz;Z)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 261
    iget-object v6, p0, Lwi;->Jh:Ljava/util/HashMap;

    monitor-enter v6

    .line 262
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 265
    if-nez v1, :cond_0

    .line 266
    invoke-virtual {p0, p3}, Lwi;->b(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    monitor-exit v6

    .line 271
    :goto_0
    return-object v0

    .line 269
    :cond_0
    iget-object v0, p0, Lwi;->Jg:Lahn;

    invoke-virtual {v0, p1, p3}, Lahn;->c(Landroid/content/Intent;Lahz;)Lahk;

    move-result-object v2

    .line 270
    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lwi;->a(Landroid/content/ComponentName;Lahk;Ljava/util/HashMap;Lahz;Z)Lwj;

    move-result-object v0

    .line 271
    iget-object v0, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method static a(Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 559
    invoke-virtual {p0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    .line 560
    sget-char v1, Ljava/io/File;->separatorChar:C

    const/16 v2, 0x5f

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 561
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "icon_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/ComponentName;Lahk;Ljava/util/HashMap;Lahz;Z)Lwj;
    .locals 3

    .prologue
    .line 330
    new-instance v2, Lwk;

    invoke-direct {v2, p1, p4}, Lwk;-><init>(Landroid/content/ComponentName;Lahz;)V

    .line 331
    iget-object v0, p0, Lwi;->Jh:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwj;

    .line 332
    if-nez v0, :cond_1

    .line 333
    new-instance v1, Lwj;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lwj;-><init>(B)V

    .line 335
    iget-object v0, p0, Lwi;->Jh:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    if-eqz p2, :cond_3

    .line 338
    invoke-virtual {p2}, Lahk;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    .line 339
    if-eqz p3, :cond_2

    invoke-virtual {p3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 340
    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lwj;->title:Ljava/lang/CharSequence;

    .line 348
    :cond_0
    :goto_0
    iget-object v0, p0, Lwi;->Jf:Laia;

    iget-object v2, v1, Lwj;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, p4}, Laia;->a(Ljava/lang/CharSequence;Lahz;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lwj;->Jk:Ljava/lang/CharSequence;

    .line 349
    iget v0, p0, Lwi;->Ji:I

    invoke-virtual {p2, v0}, Lahk;->getBadgedIcon(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lwi;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lwj;->Jj:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 377
    :cond_1
    :goto_1
    return-object v0

    .line 342
    :cond_2
    invoke-virtual {p2}, Lahk;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lwj;->title:Ljava/lang/CharSequence;

    .line 343
    if-eqz p3, :cond_0

    .line 344
    iget-object v2, v1, Lwj;->title:Ljava/lang/CharSequence;

    invoke-virtual {p3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 352
    :cond_3
    const-string v0, ""

    iput-object v0, v1, Lwj;->title:Ljava/lang/CharSequence;

    .line 353
    invoke-direct {p0, p1, p4}, Lwi;->b(Landroid/content/ComponentName;Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 354
    if-eqz v0, :cond_4

    .line 357
    iput-object v0, v1, Lwj;->Jj:Landroid/graphics/Bitmap;

    move-object v0, v1

    goto :goto_1

    .line 359
    :cond_4
    if-eqz p5, :cond_5

    .line 360
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4}, Lwi;->b(Ljava/lang/String;Lahz;)Lwj;

    move-result-object v0

    .line 362
    if-eqz v0, :cond_5

    .line 365
    iget-object v2, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    iput-object v2, v1, Lwj;->Jj:Landroid/graphics/Bitmap;

    .line 366
    iget-object v0, v0, Lwj;->title:Ljava/lang/CharSequence;

    iput-object v0, v1, Lwj;->title:Ljava/lang/CharSequence;

    .line 369
    :cond_5
    iget-object v0, v1, Lwj;->Jj:Landroid/graphics/Bitmap;

    if-nez v0, :cond_6

    .line 372
    invoke-virtual {p0, p4}, Lwi;->b(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Lwj;->Jj:Landroid/graphics/Bitmap;

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    .line 451
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 452
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 459
    :catch_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v2

    .line 460
    const/4 v1, 0x0

    .line 462
    :try_start_1
    invoke-static {p1}, Lwi;->a(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 464
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 465
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x4b

    invoke-virtual {p2, v3, v4, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 466
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 467
    const/4 v3, 0x0

    array-length v4, v0

    invoke-virtual {v1, v0, v3, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 477
    if-eqz v1, :cond_0

    .line 479
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 481
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to save restored icon for: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 469
    :cond_1
    :try_start_3
    const-string v0, "Launcher.IconCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "failed to encode cache for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 477
    if-eqz v1, :cond_0

    .line 479
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 481
    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to save restored icon for: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 472
    :catch_3
    move-exception v0

    .line 473
    :try_start_5
    const-string v3, "Launcher.IconCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "failed to pre-load cache for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 477
    if-eqz v1, :cond_0

    .line 479
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 481
    :catch_4
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to save restored icon for: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 474
    :catch_5
    move-exception v0

    .line 475
    :try_start_7
    const-string v3, "Launcher.IconCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "failed to pre-load cache for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 477
    if-eqz v1, :cond_0

    .line 479
    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_0

    .line 481
    :catch_6
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to save restored icon for: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 477
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 479
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 482
    :cond_2
    :goto_1
    throw v0

    .line 481
    :catch_7
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "failed to save restored icon for: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private b(Landroid/content/ComponentName;Lahz;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 494
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    .line 497
    invoke-static {}, Lahz;->lN()Lahz;

    move-result-object v1

    invoke-virtual {p2, v1}, Lahz;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 532
    :cond_0
    :goto_0
    return-object v0

    .line 505
    :cond_1
    :try_start_0
    iget-object v1, p0, Lwi;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lwi;->a(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 506
    const/16 v4, 0x400

    :try_start_1
    new-array v4, v4, [B

    .line 507
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 509
    :goto_1
    if-ltz v2, :cond_2

    .line 510
    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 511
    const/4 v2, 0x0

    array-length v6, v4

    invoke-virtual {v1, v4, v2, v6}, Ljava/io/FileInputStream;->read([BII)I

    move-result v2

    goto :goto_1

    .line 514
    :cond_2
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v5

    invoke-static {v2, v4, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 515
    if-nez v0, :cond_3

    .line 516
    const-string v2, "Launcher.IconCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "failed to decode pre-load icon for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 523
    :cond_3
    if-eqz v1, :cond_0

    .line 525
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 527
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed to manage pre-load icon file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 523
    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_2
    if-eqz v1, :cond_0

    .line 525
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 527
    :catch_2
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed to manage pre-load icon file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 520
    :catch_3
    move-exception v1

    move-object v2, v0

    .line 521
    :goto_3
    :try_start_4
    const-string v4, "Launcher.IconCache"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to read pre-load icon for: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 523
    if-eqz v2, :cond_0

    .line 525
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 527
    :catch_4
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed to manage pre-load icon file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 523
    :catchall_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_4
    if-eqz v1, :cond_4

    .line 525
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 528
    :cond_4
    :goto_5
    throw v0

    .line 527
    :catch_5
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed to manage pre-load icon file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 523
    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_4

    .line 520
    :catch_6
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    goto :goto_3

    .line 523
    :catch_7
    move-exception v2

    goto :goto_2
.end method

.method private gZ()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 121
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10d0000

    invoke-virtual {p0, v0, v1}, Lwi;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/ComponentName;Lahk;Ljava/util/HashMap;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 314
    iget-object v6, p0, Lwi;->Jh:Ljava/util/HashMap;

    monitor-enter v6

    .line 315
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 316
    :cond_0
    const/4 v0, 0x0

    monitor-exit v6

    .line 320
    :goto_0
    return-object v0

    .line 319
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Lahk;->lI()Lahz;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lwi;->a(Landroid/content/ComponentName;Lahk;Ljava/util/HashMap;Lahz;Z)Lwj;

    move-result-object v0

    .line 320
    iget-object v0, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 321
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a(Landroid/content/Intent;Lahz;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 257
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, p2, v1}, Lwi;->a(Landroid/content/Intent;Ljava/lang/String;Lahz;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/pm/ActivityInfo;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 163
    :try_start_0
    iget-object v0, p0, Lwi;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v1, p1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 168
    :goto_0
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {p1}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v1

    .line 170
    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {p0, v0, v1}, Lwi;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 175
    :goto_1
    return-object v0

    .line 166
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 175
    :cond_0
    invoke-direct {p0}, Lwi;->gZ()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 128
    :try_start_0
    iget v0, p0, Lwi;->Ji:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 133
    :goto_0
    if-eqz v0, :cond_0

    :goto_1
    return-object v0

    .line 130
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 133
    :cond_0
    invoke-direct {p0}, Lwi;->gZ()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 139
    :try_start_0
    iget-object v0, p0, Lwi;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 143
    :goto_0
    if-eqz v0, :cond_0

    .line 144
    if-eqz p2, :cond_0

    .line 145
    invoke-virtual {p0, v0, p2}, Lwi;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 148
    :goto_1
    return-object v0

    .line 141
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 148
    :cond_0
    invoke-direct {p0}, Lwi;->gZ()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Ladh;Landroid/content/Intent;Lahz;Z)V
    .locals 7

    .prologue
    .line 284
    iget-object v6, p0, Lwi;->Jh:Ljava/util/HashMap;

    monitor-enter v6

    .line 285
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 288
    if-nez v1, :cond_0

    .line 289
    invoke-virtual {p0, p3}, Lwi;->b(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Ladh;->h(Landroid/graphics/Bitmap;)V

    .line 290
    const-string v0, ""

    iput-object v0, p1, Ladh;->title:Ljava/lang/CharSequence;

    .line 291
    const/4 v0, 0x1

    iput-boolean v0, p1, Ladh;->SU:Z

    .line 301
    :goto_0
    monitor-exit v6

    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Lwi;->Jg:Lahn;

    invoke-virtual {v0, p2, p3}, Lahn;->c(Landroid/content/Intent;Lahz;)Lahk;

    move-result-object v2

    .line 295
    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lwi;->a(Landroid/content/ComponentName;Lahk;Ljava/util/HashMap;Lahz;Z)Lwj;

    move-result-object v0

    .line 297
    iget-object v1, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v1}, Ladh;->h(Landroid/graphics/Bitmap;)V

    .line 298
    iget-object v1, v0, Lwj;->title:Ljava/lang/CharSequence;

    iput-object v1, p1, Ladh;->title:Ljava/lang/CharSequence;

    .line 299
    iget-object v0, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0, p3}, Lwi;->a(Landroid/graphics/Bitmap;Lahz;)Z

    move-result v0

    iput-boolean v0, p1, Ladh;->SU:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 301
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a(Landroid/content/ComponentName;Lahz;)V
    .locals 3

    .prologue
    .line 195
    iget-object v1, p0, Lwi;->Jh:Ljava/util/HashMap;

    monitor-enter v1

    .line 196
    :try_start_0
    iget-object v0, p0, Lwi;->Jh:Ljava/util/HashMap;

    new-instance v2, Lwk;

    invoke-direct {v2, p1, p2}, Lwk;-><init>(Landroid/content/ComponentName;Lahz;)V

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Lahz;)V
    .locals 4

    .prologue
    .line 204
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 205
    iget-object v0, p0, Lwi;->Jh:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwk;

    .line 206
    iget-object v3, v0, Lwk;->xr:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lwk;->Jl:Lahz;

    invoke-virtual {v3, p2}, Lahz;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 208
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 211
    :cond_1
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwk;

    .line 212
    iget-object v2, p0, Lwi;->Jh:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 214
    :cond_2
    return-void
.end method

.method public final a(Lrr;Lahk;Ljava/util/HashMap;)V
    .locals 7

    .prologue
    .line 246
    iget-object v6, p0, Lwi;->Jh:Ljava/util/HashMap;

    monitor-enter v6

    .line 247
    :try_start_0
    iget-object v1, p1, Lrr;->xr:Landroid/content/ComponentName;

    invoke-virtual {p2}, Lahk;->lI()Lahz;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lwi;->a(Landroid/content/ComponentName;Lahk;Ljava/util/HashMap;Lahz;Z)Lwj;

    move-result-object v0

    .line 250
    iget-object v1, v0, Lwj;->title:Ljava/lang/CharSequence;

    iput-object v1, p1, Lrr;->title:Ljava/lang/CharSequence;

    .line 251
    iget-object v1, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    iput-object v1, p1, Lrr;->xq:Landroid/graphics/Bitmap;

    .line 252
    iget-object v0, v0, Lwj;->Jk:Ljava/lang/CharSequence;

    iput-object v0, p1, Lrr;->Jk:Ljava/lang/CharSequence;

    .line 253
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a(Landroid/graphics/Bitmap;Lahz;)Z
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lwi;->Je:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lahz;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 306
    iget-object v0, p0, Lwi;->Je:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    iget-object v0, p0, Lwi;->Je:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lwi;->a(Lahz;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_0
    iget-object v0, p0, Lwi;->Je:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public b(Ljava/lang/String;Lahz;)Lwj;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 402
    new-instance v1, Landroid/content/ComponentName;

    const-string v0, "."

    invoke-direct {v1, p1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    new-instance v2, Lwk;

    invoke-direct {v2, v1, p2}, Lwk;-><init>(Landroid/content/ComponentName;Lahz;)V

    .line 404
    iget-object v0, p0, Lwi;->Jh:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwj;

    .line 405
    if-nez v0, :cond_0

    .line 406
    new-instance v0, Lwj;

    invoke-direct {v0, v3}, Lwj;-><init>(B)V

    .line 407
    const-string v3, ""

    iput-object v3, v0, Lwj;->title:Ljava/lang/CharSequence;

    .line 408
    iget-object v3, p0, Lwi;->Jh:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    :try_start_0
    iget-object v2, p0, Lwi;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 412
    iget-object v3, p0, Lwi;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v0, Lwj;->title:Ljava/lang/CharSequence;

    .line 413
    iget-object v3, p0, Lwi;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v3}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lwi;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Ladp;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v0, Lwj;->Jj:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    :goto_0
    iget-object v2, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    .line 419
    invoke-direct {p0, v1, p2}, Lwi;->b(Landroid/content/ComponentName;Lahz;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    .line 422
    :cond_0
    return-object v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public final b(Ltu;)V
    .locals 5

    .prologue
    .line 229
    iget-object v1, p0, Lwi;->Jh:Ljava/util/HashMap;

    monitor-enter v1

    .line 230
    :try_start_0
    iget-object v0, p0, Lwi;->Jh:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 231
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwj;

    .line 233
    iget-object v3, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget v4, p1, Ltu;->DI:I

    if-lt v3, v4, :cond_1

    iget-object v0, v0, Lwj;->Jj:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget v3, p1, Ltu;->DI:I

    if-ge v0, v3, :cond_0

    .line 235
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
